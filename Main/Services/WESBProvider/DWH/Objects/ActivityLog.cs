﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider.DWH.Objects
{
    public class ActivityLog
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("tsInsert")]
        public long? TsInsert { get; set; }
        [JsonProperty("activityName")]
        public string ActivityName { get; set; }
        [JsonProperty("operatorId")]
        public string OperatorId { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("deliveryId")]
        public long? DeliveryId { get; set; }
        [JsonProperty("fuelBalanceId")]
        public long? FuelBalanceId { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? TsInsertUtc { get { return TsInsert.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(TsInsert) : null; } }
        [JsonIgnore]
        public DateTime? TsInsertLocal { get { return TsInsert.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(TsInsert) : null; } }
        #endregion
    }
}
