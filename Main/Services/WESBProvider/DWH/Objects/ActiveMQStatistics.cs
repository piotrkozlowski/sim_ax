﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.DWH.Objects
{
    /// <summary>
    /// One ActiveMQ statistic.
    /// </summary>
    public class QueueStatistic
    {
        [JsonProperty("timestamp")]
        public long? Timestamp { get; set; }

        [JsonProperty("size")]
        public long? Size { get; set; }

        [JsonProperty("consumer_count")]
        public int? ConsumerCount { get; set; }

        [JsonProperty("enqueue_count")]
        public long? EnqueueCount { get; set; }

        [JsonProperty("dequeue_count")]
        public long? DequeueCount { get; set; }

        #region Custom properties
        [JsonIgnore]
        public DateTime? TimestampUtc { get { return Timestamp.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Timestamp * 1000) : null; } }
        [JsonIgnore]
        public DateTime? TimestampLocal { get { return Timestamp.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Timestamp * 1000) : null; } }
        #endregion
    }

    /// <summary>
    /// Contains all ActiveMQ statistics
    /// </summary>
    public class ActiveMQStatistics
    {
        //TODO: [JsonProperty("type")]
        [JsonIgnore]
        public string AggregatonType { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("stat")]
        public IList<QueueStatistic> QueueStatistics { get; set; }
    }
}
