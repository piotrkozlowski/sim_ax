﻿using IMR.Suite.Common;

using Newtonsoft.Json;
using RestSharp;

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {
        // ESB
        #region GetTankData
        public List<Objects.DataArch> GetTankData(long locationId, DateTime? startTime, DateTime? endTime)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "dataarch/getTankData";
                request.RootElement = "getTankData";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                AddParameter(ref request, locationId, "locationId");
                AddParameter(ref request, startTime, "startTime");
                AddParameter(ref request, endTime, "endTime");
                
                var client = ConectRestClient();

                IRestResponse response = null;
                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });
                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.DataArch>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetTankMeasurements
        public List<Objects.DataArch> GetTankMeasurements(long[] tankId, DateTime? startTime, DateTime? endTime)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "dataarch/getTankMeasurements";
                request.RootElement = "getTankMeasurements";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                AddCollectionToParameters(ref request, tankId, "tankId");
                AddParameter(ref request, startTime, "startTime");
                AddParameter(ref request, endTime, "endTime");

                var client = ConectRestClient();

                IRestResponse response = null;
                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });
                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.DataArch>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion

        #region GetDataArch
        public List<Objects.DataArch> GetDataArch(long[] idDataArch)
        {
            return GetDataArchFilter(idDataArch: idDataArch);
        }
        #endregion
        #region GetDataArchByDataType
        public List<Objects.DataArch> GetDataArchByDataType(long[] idMeter, long[] idDataType, DateTime? startTime, DateTime? endTime)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getDataArchByDataType", methodName);

                AddCollectionToParameters(ref request, idMeter, "idMeter");
                AddCollectionToParameters(ref request, idDataType, "idDataType");
                AddDateTimeParameter(ref request, startTime, "startTime");
                AddDateTimeParameter(ref request, endTime, "endTime");

                return ExecuteRequest<List<Objects.DataArch>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return new List<Objects.DataArch>();
            }
        }
        #endregion
        #region GetDataArchFilter
        public List<Objects.DataArch> GetDataArchFilter(long[] idDataArch = null, long[] serialNbr = null, long[] idMeter = null, long[] idLocation = null, long[] idDataType = null, int[] indexNbr = null,
                            DateTime? startTime = null, DateTime? endTime = null, long[] idDataSource = null, int[] idDataSourceType = null, long[] idAlarmEvent = null, int[] status = null,
                             long? topCount = null, string customWhereClause = null, int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getDataArchFilter", methodName, commandTimeout);

                AddCollectionToParameters(ref request, idDataArch, "idDataArch");
                AddCollectionToParameters(ref request, serialNbr, "serialNbr");
                AddCollectionToParameters(ref request, idMeter, "idMeter");
                AddCollectionToParameters(ref request, idLocation, "idLocation");
                AddCollectionToParameters(ref request, idDataType, "idDataType");
                AddCollectionToParameters(ref request, indexNbr, "indexNbr");
                AddDateTimeParameter(ref request, startTime, "startTime");
                AddDateTimeParameter(ref request, endTime, "endTime");
                AddCollectionToParameters(ref request, idDataSource, "idDataSource");
                AddCollectionToParameters(ref request, idDataSourceType, "idDataSourceType");
                AddCollectionToParameters(ref request, idAlarmEvent, "idAlarmEvent");
                AddCollectionToParameters(ref request, status, "status");
                AddParameter(ref request, topCount, "topCount");
                AddParameter(ref request, customWhereClause, "customWhere");

                return ExecuteRequest<List<Objects.DataArch>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return new List<Objects.DataArch>();
            }            
        }
        #endregion
    }
}
