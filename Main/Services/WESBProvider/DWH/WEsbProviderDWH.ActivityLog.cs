﻿using IMR.Suite.Common;

using Newtonsoft.Json;
using RestSharp;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {
        public List<Objects.ActivityLog> GetActivityLogFilter(long locationId, DateTime? startTime, DateTime? endTime)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "activitylog/getActivityLogFilter";
                request.RootElement = "getActivityLogFilter";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetActivityLogFilter"), request.Resource);

                //if (locationIds != null)
                //{
                //    foreach (var loc in locationIds)
                //        request.AddParameter("locationId", loc);
                //}
                
                        request.AddParameter("locationId", locationId);
                

                if (startTime.HasValue)
                {
                    long msStart = DateTimer.GetMsUtcFromDtLocal(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = DateTimer.GetMsUtcFromDtLocal(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetActivityLogFilter"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.ActivityLog>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetActivityLogFilter"), ex.Message, ex.StackTrace);
                return null;
            }
        }

        public List<Objects.ActivityLog> GetActivityLogFilter(long[] locationId, DateTime? startTime, DateTime? endTime, int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "activitylog", "getActivityLogFilter", methodName, commandTimeout);
                AddCollectionToParameters(ref request, locationId, "idAlarmEvent");
                AddDateTimeParameter(ref request, startTime, "startTime");
                AddDateTimeParameter(ref request, endTime, "endTime");
                return ExecuteRequest<List<Objects.ActivityLog>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetActivityLogFilter"), ex.Message, ex.StackTrace);
                return null;
            }
        }
    }
}
