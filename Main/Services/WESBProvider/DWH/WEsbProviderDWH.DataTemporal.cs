﻿using IMR.Suite.Common;
using RestSharp;

using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {      
        #region GetDataTemporal
        public Objects.DataTemporal GetDataTemporal(long idDataTemporal)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getDataTemporalById", methodName);
                AddParameter(ref request, idDataTemporal, "id");
                return ExecuteRequest<Objects.DataTemporal>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetDataTemporalFilter
        public List<Objects.DataTemporal> GetDataTemporalFilter(long[] idLocation = null, long[] idMeter = null, long[] idDataType = null, int[] idAggregationType = null, int[] idDataSourceType = null, long[] idDataSource = null,
            long[] serialNbr = null, int[] indexNbr = null, int[] status = null, long[] idAlarmEvent = null, DateTime? startTimeFrom = null, DateTime? startTimeTo = null, DateTime? endTimeFrom = null, DateTime? endTimeTo = null,
            int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getDataTemporalFilter", methodName, commandTimeout);

                AddCollectionToParameters(ref request, idLocation, "idLoc");
                AddCollectionToParameters(ref request, idMeter, "idMet");
                AddCollectionToParameters(ref request, idDataType, "idDT");
                AddCollectionToParameters(ref request, idAggregationType, "idAggr");
                AddCollectionToParameters(ref request, idDataSourceType, "idDST");
                AddCollectionToParameters(ref request, idDataSource, "idDS");
                AddCollectionToParameters(ref request, serialNbr, "serial");
                AddCollectionToParameters(ref request, indexNbr, "idxNbr");
                AddCollectionToParameters(ref request, status, "status");
                AddCollectionToParameters(ref request, idAlarmEvent, "idAlarmEvent");
                AddDateTimeParameter(ref request, startTimeFrom, "startTimeFrom");
                AddDateTimeParameter(ref request, startTimeTo, "startTimeTo");
                AddDateTimeParameter(ref request, endTimeFrom, "endTimeFrom");
                AddDateTimeParameter(ref request, endTimeTo, "endTimeTo");

                return ExecuteRequest<List<Objects.DataTemporal>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return new List<Objects.DataTemporal>();
            }
        }
        #endregion
    }
}
