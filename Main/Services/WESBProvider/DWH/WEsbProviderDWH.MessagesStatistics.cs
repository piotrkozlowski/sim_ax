﻿using IMR.Suite.Common;
using RestSharp;
using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {
        public Objects.MessagesStatisticsOneDay GetMessagesStatisticsOneDay()
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            return GetMessagesStatistics<Objects.MessagesStatisticsOneDay>("mesgStat1d", methodName);
        }

        public Objects.MessagesStatisticsFourDays GetMessagesStatisticsFourDays()
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            return GetMessagesStatistics<Objects.MessagesStatisticsFourDays>("mesgStat4d", methodName);
        }

        private T GetMessagesStatistics<T>(string type, string methodName)
        {
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getStat", methodName);
                AddParameter(ref request, type, "stat");
                return ExecuteRequest<T>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return default(T);
            }
        }
    }
}
