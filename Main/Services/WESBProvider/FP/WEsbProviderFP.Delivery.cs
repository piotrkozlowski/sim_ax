﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using IMR.Suite.Common;
using IMR.Suite.Services.WEsbProvider.FP.Objects;
using RestSharp;
using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetTerminals
        public List<Objects.Terminal> GetTerminals()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "delivery/getTerminals";
                request.RootElement = "getTerminals";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Terminal>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveTerminals
        public bool SaveTerminals(List<Objects.Terminal> terminals)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "terminals/saveTerminals";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveTerminals"), request.Resource);

                request.AddJsonBody(terminals);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveTerminals"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveTerminals"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveTerminalsWithDataReturn
        public List<Objects.Terminal> SaveTerminalsWithDataReturn(List<Objects.Terminal> terminals)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveTerminalsWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveTerminalsWithDataReturn"), request.Resource);

                request.AddJsonBody(terminals);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Terminal>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveTerminalsWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveTerminalsWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 

        #region GetDrivers
        public List<Objects.Driver> GetDrivers()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "delivery/getDrivers";
                request.RootElement = "getDrivers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Driver>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveDrivers
        public bool SaveDrivers(List<Objects.Driver> drivers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "terminals/saveDrivers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveDrivers"), request.Resource);

                request.AddJsonBody(drivers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveDrivers"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveDrivers"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveDriversWithDataReturn
        public List<Objects.Driver> SaveDriversWithDataReturn(List<Objects.Driver> drivers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveDriversWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveDriversWithDataReturn"), request.Resource);

                request.AddJsonBody(drivers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Driver>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveDriversWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveDriversWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 

        #region GetCarriers
        public List<Objects.Carrier> GetCarriers()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "delivery/getCarriers";
                request.RootElement = "getCarriers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Carrier>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveCarriers
        public bool SaveCarriers(List<Objects.Carrier> carriers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "terminals/saveCarriers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveCarriers"), request.Resource);

                request.AddJsonBody(carriers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveCarriers"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveCarriers"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveCarriersWithDataReturn
        public List<Objects.Carrier> SaveCarriersWithDataReturn(List<Objects.Carrier> carriers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveCarriersWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveCarriersWithDataReturn"), request.Resource);

                request.AddJsonBody(carriers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Carrier>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveCarriersWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveCarriersWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 

        #region GetTrucksTrailers
        public List<Objects.TruckTrailer> GetTrucksTrailers()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "delivery/getTruckTrailers";
                request.RootElement = "getTruckTrailers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.TruckTrailer>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveTrucksTrailers
        public bool SaveTrucksTrailers(List<Objects.TruckTrailer> truckTrailers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "terminals/saveTruckTrailers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveTrucksTrailers"), request.Resource);

                request.AddJsonBody(truckTrailers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveTrucksTrailers"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveTrucksTrailers"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveTrucksTrailersWithDataReturn
        public List<Objects.TruckTrailer> SaveTrucksTrailersWithDataReturn(List<Objects.TruckTrailer> truckTrailers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveTruckTrailersWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveTrucksTrailersWithDataReturn"), request.Resource);

                request.AddJsonBody(truckTrailers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.TruckTrailer>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveTrucksTrailersWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveTrucksTrailersWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 

        #region GetDeliveries
        public List<List<Objects.Delivery>> GetDeliveries(long[] idLocations, DateTime? startTime, DateTime? endTime, string orderNumber, int commandTimeout = 0)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/getDeliveries";
                request.RootElement = "getDeliveries";

                if (commandTimeout > 0)
                {
                    request.ReadWriteTimeout =
                    request.Timeout = commandTimeout * 1000;
                }

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                IRestResponse response = null;

                if (idLocations != null)
                    foreach (var loc in idLocations)
                        request.AddParameter("locationId", loc);

                if (startTime.HasValue)
                {
                    long msStart = MilisecondsFrom1970(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = MilisecondsFrom1970(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }
                if (!string.IsNullOrEmpty(orderNumber))
                    request.AddParameter("orderNumber", orderNumber);

                var client = ConectRestClient();
                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });

                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                {
                    if ((response.ErrorException is WebException && (response.ErrorException as WebException).Status == WebExceptionStatus.Timeout)
                        || (response.ErrorException is UriFormatException))
                        throw response.ErrorException;
                    else
                        throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));
                }

                var result = JsonConvert.DeserializeObject<List<List<Objects.Delivery>>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                throw ex;
            }
        }
        #endregion
        #region SaveDeliveries
        public bool SaveDeliveries(List<List<Objects.Delivery>> deliveries)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveDeliveries";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                //request.AddBody(deliveries);
                request.AddJsonBody(deliveries);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion 
        #region SaveDeliveriesWithDataReturn
        public List<List<Objects.Delivery>> SaveDeliveriesWithDataReturn(List<List<Objects.Delivery>> deliveries)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveDeliveriesWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                //request.AddBody(deliveries);
                request.AddJsonBody(deliveries);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<List<Objects.Delivery>>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion

        #region GetDeliveryFilter
        public List<List<Delivery>> GetDeliveryFilter(long[] idLocations = null, DateTime? startTime = null, DateTime? endTime = null, int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "delivery", "getDeliveryFilter", methodName, commandTimeout);

                AddCollectionToParameters(ref request, idLocations, "locId");
                AddDateTimeParameter(ref request, startTime, "startTime");
                AddDateTimeParameter(ref request, endTime, "endTime");

                return ExecuteRequest<List<List<Delivery>>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                throw ex;
            }
        }
        #endregion

        #region GetRefuels
        public List<Objects.Delivery> GetRefuels(long[] idLocations, long[] idMeters, int[] types, DateTime? startTime, DateTime? endTime)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/getRefuelsFilter";
                request.RootElement = "getRefuelsFilter";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                if (idLocations != null)
                    foreach (var loc in idLocations)
                        request.AddParameter("locationId", loc);

                if (idMeters != null)
                    foreach (var met in idMeters)
                        request.AddParameter("tankId", met);

                if (types != null)
                    foreach (var t in types)
                        request.AddParameter("dropType", t);

                if (startTime.HasValue)
                {
                    long msStart = MilisecondsFrom1970(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = MilisecondsFrom1970(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Delivery>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveRefuels
        public bool SaveRefuels(List<Objects.Delivery> deliveries)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveRefuels";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                //request.AddBody(deliveries);
                request.AddJsonBody(deliveries);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError && !string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponseError, "SaveRefuels", response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                    return false;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponse, "SaveRefuels", response.StatusCode, response.ContentLength, response.ResponseUri);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveRefuelsWithDataReturn
        public List<Objects.Delivery> SaveRefuelsWithDataReturn(List<Objects.Delivery> deliveries)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/saveRefuelsWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                //request.AddBody(deliveries);
                request.AddJsonBody(deliveries);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Delivery>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError && !string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponseError, "SaveRefuelsWithDataReturn", response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                    return null;
                }
                else
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponse, "SaveRefuelsWithDataReturn", response.StatusCode, response.ContentLength, response.ResponseUri);
                    return result;
                }

            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 
        #region DeleteRefuels
        public bool DeleteRefuels(List<int> deliveriesId)
        {
            return DeleteRefuels(deliveriesId.Select(id => (long)id).ToList());
        }
        public bool DeleteRefuels(List<long> deliveriesId)
        {
            try
            {
                var request = new RestRequest(Method.DELETE);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/deleteRefuels";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                request.AddBody(deliveriesId);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError && !string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponseError, "DeleteRefuels", response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                    return false;
                }
                else if (string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponse, "DeleteRefuels", response.StatusCode, response.ContentLength, response.ResponseUri);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region GetCounterValues
        public List<Objects.CounterValue> GetCounterValues(long[] balanceCalibrations, long[] idDispensers)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-type", "application/json");
                request.Resource = "delivery/getCounterValues";
                request.RootElement = "getCounterValues";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                if (balanceCalibrations != null)
                    foreach (var cId in balanceCalibrations)
                        request.AddParameter("cId", cId);

                if (idDispensers != null)
                    foreach (var dId in idDispensers)
                        request.AddParameter("dId", dId);


                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.CounterValue>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
    }
}
