﻿using System;


namespace IMR.Suite.Services.WEsbProvider.FP
{
    public static class Enums
    {
        public enum AlarmClassType
        {
            PROBE_DROWNED = 1,
            PROBE_HANG = 2,
            SINGLE_LOSS = 3,
            SINGLE_SURPLUS = 4,
            SINGLE_SURPLUS_WATER_INFLUX = 5,
            SINGLE_SURPLUS_MICROMIXING = 6,
            PERMANENT_LOSS_FUEL_LEAK = 7,
            PERMANENT_LOSS_INNER_LEAK = 8,
            PERMANENT_LOSS_NOZZLE_DECALIBRATION = 9,
            PERMANENT_SURPLUS_WATER_INFLUX = 10,
            PERMANENT_SURPLUS_INNER_LEAK = 11,
            PERMANENT_SURPLUS_NOZZLE_DECALIBRATION = 12,
            CALIBRATION_CURVE_ERROR = 13,
            FUEL_HIGH_LEVEL = 14,
            FUEL_LOW_LEVEL = 15,
            DEADSTOCK = 16,
            WATER_HIGH_LEVEL = 17,
            SIREN_FAILED = 18,
            DENSITY = 19,
            OVERFILL = 20,
            FUEL_LEAKAGE_DW = 21,
            FUEL_LEAKAGE_P = 22,
            UNDER_COUNTING_GAS_METER = 25,
            OVER_COUNTING_GAS_METER = 26,
            COUNTER_MALFUNCTION = 27,
            ILLEGAL_DELIVERY = 29,
            TOO_SMALL_DETECTED_DELIVERY = 30,
            TOO_LARGE_DETECTED_DELIVERY = 31,
            PERMANENT_SURPLUS = 32,
            PERMANENT_LOSS = 33,
            IRC_SIR2_LOSS = 34,
            CONSTANT_LEVEL = 35,
            PROBE_FAILURE = 36,
            DISPENSER_FAILURE = 37,
            ATG_SENSOR_LIQUID = 38,
            ATG_SENSOR_VAPOR = 39,
            MISSING_ALEVEL_DATA = 40,
            INVALID_ALEVEL_DATA = 41,
            MISSING_OKO_PACKET = 42,
            STOCK_OUT_RISK = 43,
            OVERFILL_2 = 44,
            NONVALUE = 999
        }

        public enum AlarmCriticalityType
        {
            UNKNOWN = 0,
            INFO = 1,
            WARNING = 2,
            ERROR = 3,
            ERROR_MAIL = 4,
            ERROR_SMS = 5,
            WARNING_MAIL = 6,
            WARNING_SMS = 7,
            DAILYERROR = 8,
            DAILYWARNING = 9,
            SM = 10,
            CM = 11,
            TM = 12,
            RSAA = 13,
            PLIP_REPORTED_EMAIL = 14,
            PLIP_REPORTED_SMS = 15,
            PLIP_ESCALATED_EMAIL = 16,
            PLIP_ESCALATED_SMS = 17,
            PLIP_CONFIRMED_EMAIL = 18,
            PLIP_CONFIRMED_SMS = 19,
            PLIP_B_LEAK = 20,
            NONVALUE = 999
        }

        public enum AlarmType
        {
            ACTIVE,
            HISTORICAL
        }

        public enum DeliveryType
        {
            PLANNED = 5,
            TERMINAL = 12,
            VERIFIED = 9,
            MANUAL = 7,
            CALCULATED = 10,
            OKO = 1,
            FITTER = 2,
            FLOWMETER = 14
        }

        public enum EventReasonType
        {
            OTHER = 1,
            CONTROL_PUMP_OUT = 2,
            LEGALISATION = 3,
            SAMPLES = 4,
            NEW_HOSES = 5,
            DISPENSER_SERVICE = 6,
            TANK_RECONSTRUCTION = 7,
            TANK_REVISION = 8,
            SEASONAL_EXCHANGE = 9,
            TANK_CALIBRATION = 10,
            GHOST_TRANSACTION = 11,
            MISSING_TRANSACTION = 12,
            PLIP_B = 13,
            UNKNOWN = 0
        }

        public enum EventTableType
        {
            VOLUME_FUEL_PUMP_FROM_TANK = 1,
            CORRECT_METER_DISTRIBUTORS = 2,
            CORRECT_POS = 3,
            VOLUME_FUEL_NOT_RETURNED_TO_TANKS = 4,
            COUNTERS_AND_FLASK_MEASUREMENT = 5
        }

        public enum EventType
        {
            PUMP_OUT_THROUGH_POS = 1,
            PUMP_OUT_EXCLUDING_POS = 2,
            PUMP_OUT_EXCLUDING_DISPENSERS = 3,
            REPUMPING_FROM_TANK = 4,
            POS_ERROR = 5,
            DISPENSER_ERROR = 6,
            ATG_ERROR = 7
        }

        public enum FuelBalanceType
        {
            MANUAL,
            AUTO,
            GROSS,
            NET
        }

        public enum GrossNetType
        {
            GROSS,
            NET
        }

        public enum GroupDeliveryType
        {
            DECLARED,
            DETECTED
        }

        public enum LocationStatus
        {
            NEW,
            SCHEDULED,
            PENDING,
            OPERATIONAL,
            SUSPENDED,
            DELETED,
            UNKNOWN
        }

        public enum LocationType
        {
            AUTO,
            MANUAL
        }

        public enum MeterType
        {
            PRODUCT,
            TANK,
            NOZZLE
        }

        public enum PlipAnswerType
        {
            TEXT,
            DATETIME,
            BOOLEAN,
            BINARY,
            UNKNOWN
        }

        public enum PlipPriority
        {
            STANDARD,
            URGENT,
            UNKNOWN
        }

        public enum PlipState
        {
            ESCALATED,
            REPORTED,
            CONFIRMED,
            UNKNOWN
        }
        public enum PlipType
        {
            PLIP_A,
            PLIP_B,
            PLIP_C,
            UNKNOWN
        }

        public enum SalePosSourceType
        {
            MANUAL,
            SLS,
            HOIS
        }

        public enum TruckTrailerType
        {
            NONE,
            TRUCK,
            TRAILER
        }

        public enum ParamType
        {
            ORDER_PRODUCT = 2,
            NUMBER_OF_DAYS_SHOW_DELIVERY = 3
        }
    }

    #region Class EnumsConverter
    public static class EnumsConverter
    {
        #region FpDeliveryTypeToDataSourceType
        public static Common.Enums.DataSourceType FpDeliveryTypeToDataSourceType(string deliveryType)
        {
            if (deliveryType == FP.Enums.DeliveryType.MANUAL.ToString())
                return Common.Enums.DataSourceType.ManualDataEntry;
            else if (deliveryType == FP.Enums.DeliveryType.PLANNED.ToString())
                return Common.Enums.DataSourceType.Estimation;
            else if (deliveryType == FP.Enums.DeliveryType.TERMINAL.ToString())
                return Common.Enums.DataSourceType.Interface;
            else if (deliveryType == FP.Enums.DeliveryType.VERIFIED.ToString())
                return Common.Enums.DataSourceType.File;
            else if (deliveryType == FP.Enums.DeliveryType.CALCULATED.ToString())
                return Common.Enums.DataSourceType.AWSR;
            else if (deliveryType == FP.Enums.DeliveryType.OKO.ToString())
                return Common.Enums.DataSourceType.OKO;
            else if (deliveryType == FP.Enums.DeliveryType.FITTER.ToString())
                return Common.Enums.DataSourceType.Fitter;
            else if (deliveryType == FP.Enums.DeliveryType.FLOWMETER.ToString())
                return Common.Enums.DataSourceType.Flowmeter;
            else
                return Common.Enums.DataSourceType.Unknown;
        }
        #endregion
        #region FpDeliveryTypeToIdDataSourceType
        public static int FpDeliveryTypeToIdDataSourceType(string deliveryType)
        {
            return (int)FpDeliveryTypeToDataSourceType(deliveryType);
        }
        #endregion

        #region DataSourceTypeToFpDeliveryType
        public static string DataSourceTypeToFpDeliveryType(Common.Enums.DataSourceType dataSourceType)
        {
            switch (dataSourceType)
            {
                case Common.Enums.DataSourceType.OKO:
                    return FP.Enums.DeliveryType.OKO.ToString();
                case Common.Enums.DataSourceType.Fitter:
                    return FP.Enums.DeliveryType.FITTER.ToString();
                case Common.Enums.DataSourceType.Estimation:
                    return FP.Enums.DeliveryType.PLANNED.ToString();
                case Common.Enums.DataSourceType.ManualDataEntry:
                    return FP.Enums.DeliveryType.MANUAL.ToString();
                case Common.Enums.DataSourceType.File:
                    return FP.Enums.DeliveryType.VERIFIED.ToString();
                case Common.Enums.DataSourceType.AWSR:
                    return FP.Enums.DeliveryType.CALCULATED.ToString();
                case Common.Enums.DataSourceType.Interface:
                    return FP.Enums.DeliveryType.TERMINAL.ToString();
                case Common.Enums.DataSourceType.Flowmeter:
                    return FP.Enums.DeliveryType.FLOWMETER.ToString();
                default:
                    return string.Empty;
            }
        }
        #endregion
        #region IdDataSourceTypeToFpDeliveryType
        public static string IdDataSourceTypeToFpDeliveryType(int idDataSourceType)
        {
            return DataSourceTypeToFpDeliveryType((Common.Enums.DataSourceType)idDataSourceType);
        }
        #endregion

        #region FpSirDecisionToSir2Decision
        public static Common.Enums.SIR2Decision FpSirDecisionToSir2Decision(string decision)
        {
            if (string.IsNullOrEmpty(decision)) return Common.Enums.SIR2Decision.Inconclusive;

            if (decision == Common.Enums.SIR2Decision.Fail.ToString().ToUpper())
                return Common.Enums.SIR2Decision.Fail;
            else if (decision == Common.Enums.SIR2Decision.Pass.ToString().ToUpper())
                return Common.Enums.SIR2Decision.Pass;
            else
                return Common.Enums.SIR2Decision.Inconclusive;
        }
        #endregion
        #region Sir2DecisionToFpSirDecision
        public static string Sir2DecisionToFpSirDecision(Common.Enums.SIR2Decision decision)
        {
            switch (decision)
            {
                case Common.Enums.SIR2Decision.Inconclusive:
                    return Common.Enums.SIR2Decision.Inconclusive.ToString().ToUpper();
                case Common.Enums.SIR2Decision.Fail:
                    return Common.Enums.SIR2Decision.Fail.ToString().ToUpper();
                case Common.Enums.SIR2Decision.Pass:
                    return Common.Enums.SIR2Decision.Pass.ToString().ToUpper();
                default:
                    return Common.Enums.SIR2Decision.Inconclusive.ToString().ToUpper();
            }
        }
        #endregion

        #region FpSirInconclusiveCauseToSir2InconclusiveCause
        public static Common.Enums.SIR2InconclusiveCause FpSirInconclusiveCauseToSir2InconclusiveCause(string cause)
        {
            if (string.IsNullOrEmpty(cause)) return Common.Enums.SIR2InconclusiveCause.RunError;

            if (cause == Common.Enums.SIR2InconclusiveCause.DataFormat.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.DataFormat;
            else if (cause == Common.Enums.SIR2InconclusiveCause.MissingDays.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.MissingDays;
            else if (cause == Common.Enums.SIR2InconclusiveCause.Outliers.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.Outliers;
            else if (cause == Common.Enums.SIR2InconclusiveCause.Parametres.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.Parametres;
            else if (cause == Common.Enums.SIR2InconclusiveCause.TrendChanging.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.TrendChanging;
            else if (cause == Common.Enums.SIR2InconclusiveCause.TrendRising.ToString().ToUpper())
                return Common.Enums.SIR2InconclusiveCause.TrendRising;
            else
                return Common.Enums.SIR2InconclusiveCause.RunError;
        }
        #endregion
        #region Sir2InconclusiveCauseToFpSirInconclusiveCause
        public static string Sir2InconclusiveCauseToFpSirInconclusiveCause(Common.Enums.SIR2InconclusiveCause cause)
        {
            switch (cause)
            {
                case Common.Enums.SIR2InconclusiveCause.RunError:
                    return Common.Enums.SIR2InconclusiveCause.RunError.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.Parametres:
                    return Common.Enums.SIR2InconclusiveCause.Parametres.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.DataFormat:
                    return Common.Enums.SIR2InconclusiveCause.DataFormat.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.MissingDays:
                    return Common.Enums.SIR2InconclusiveCause.MissingDays.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.Outliers:
                    return Common.Enums.SIR2InconclusiveCause.Outliers.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.TrendRising:
                    return Common.Enums.SIR2InconclusiveCause.TrendRising.ToString().ToUpper();
                case Common.Enums.SIR2InconclusiveCause.TrendChanging:
                    return Common.Enums.SIR2InconclusiveCause.TrendChanging.ToString().ToUpper();
                default:
                    return Common.Enums.SIR2InconclusiveCause.RunError.ToString().ToUpper();
            }
        }
        #endregion

        #region FpAlarmClassTypeToProblemClass
        public static Common.Enums.ProblemClass FpAlarmClassToProblemClass(string alarmClass)
        {
            switch (alarmClass)
            {
                case "PROBE_DROWNED":
                    return Common.Enums.ProblemClass.ProbeDrowned;
                case "PROBE_HANG":
                    return Common.Enums.ProblemClass.ProbeHang;
                case "SINGLE_LOSS":
                    return Common.Enums.ProblemClass.SingleLoss;
                case "SINGLE_SURPLUS":
                    return Common.Enums.ProblemClass.SingleSurplus;
                case "SINGLE_SURPLUS_WATER_INFLUX":
                    return Common.Enums.ProblemClass.SingleSurplusWaterInflux;
                case "SINGLE_SURPLUS_MICROMIXING":
                    return Common.Enums.ProblemClass.SingleSurplusMicromixing;
                case "PERMANENT_LOSS_FUEL_LEAK":
                    return Common.Enums.ProblemClass.PermanentLossFuelLeak;
                case "PERMANENT_LOSS_INNER_LEAK":
                    return Common.Enums.ProblemClass.PermanentLossInnerLeak;
                case "PERMANENT_LOSS_NOZZLE_DECALIBRATION":
                    return Common.Enums.ProblemClass.PermanentLossNozzleDecalibration;
                case "PERMANENT_SURPLUS_WATER_INFLUX":
                    return Common.Enums.ProblemClass.PermanentSurplusWaterInflux;
                case "PERMANENT_SURPLUS_INNER_LEAK":
                    return Common.Enums.ProblemClass.PermanentSurplusInnerLeak;
                case "PERMANENT_SURPLUS_NOZZLE_DECALIBRATION":
                    return Common.Enums.ProblemClass.PermanentSurplusNozzleDecalibration;
                case "CALIBRATION_CURVE_ERROR":
                    return Common.Enums.ProblemClass.CalibrationCurveError;
                case "FUEL_HIGH_LEVEL":
                    return Common.Enums.ProblemClass.FuelHighLevel;
                case "FUEL_LOW_LEVEL":
                    return Common.Enums.ProblemClass.FuelLowLevel;
                case "DEADSTOCK":
                    return Common.Enums.ProblemClass.Deadstock;
                case "WATER_HIGH_LEVEL":
                    return Common.Enums.ProblemClass.WaterHighLevel;
                case "SIREN_FAILED":
                    return Common.Enums.ProblemClass.SirenFailed;
                case "DENSITY":
                    return Common.Enums.ProblemClass.Density;
                case "OVERFILL":
                    return Common.Enums.ProblemClass.Overfill;
                case "FUEL_LEAKAGE_DW":
                    return Common.Enums.ProblemClass.FuelLeakageDW;
                case "FUEL_LEAKAGE_P":
                    return Common.Enums.ProblemClass.FuelLeakageP;
                case "UNDER_COUNTING_GAS_METER":
                    return Common.Enums.ProblemClass.UnderCountingGasMeter;
                case "OVER_COUNTING_GAS_METER":
                    return Common.Enums.ProblemClass.OverCountingGasMeter;
                case "COUNTER_MALFUNCTION":
                    return Common.Enums.ProblemClass.CounterMalfunction;
                case "ILLEGAL_DELIVERY":
                    return Common.Enums.ProblemClass.IllegalDelivery;
                case "TOO_SMALL_DETECTED_DELIVERY":
                    return Common.Enums.ProblemClass.TooSmallDetectedDelivery;
                case "TOO_LARGE_DETECTED_DELIVERY":
                    return Common.Enums.ProblemClass.TooLargeDetectedDelivery;
                case "PERMANENT_SURPLUS":
                    return Common.Enums.ProblemClass.PermanentSurplus;
                case "PERMANENT_LOSS":
                    return Common.Enums.ProblemClass.PermanentLoss;
                case "IRC_SIR2_LOSS":
                    return Common.Enums.ProblemClass.IrcSir2Loss;
                case "CONSTANT_LEVEL":
                    return Common.Enums.ProblemClass.ConstantLevel;
                case "PROBE_FAILURE":
                    return Common.Enums.ProblemClass.ProbeFailure;
                case "DISPENSER_FAILURE":
                    return Common.Enums.ProblemClass.DispenserFailure;
                case "ATG_SENSOR_LIQUID":
                    return Common.Enums.ProblemClass.ATGSensorLiquid;
                case "ATG_SENSOR_VAPOR":
                    return Common.Enums.ProblemClass.ATGSensorVapor;
                case "MISSING_ALEVEL_DATA":
                    return Common.Enums.ProblemClass.MissingAlevelData;
                case "INVALID_ALEVEL_DATA":
                    return Common.Enums.ProblemClass.InvalidAlevelData;
                case "MISSING_OKO_PACKET":
                    return Common.Enums.ProblemClass.MissingOKOPacket;
                case "STOCK_OUT_RISK":
                    return Common.Enums.ProblemClass.StockOutRisk;
                case "OVERFILL_2":
                    return Common.Enums.ProblemClass.Overfill2;

                case "NONVALUE":
                default:
                    return Common.Enums.ProblemClass.NoProblem;
            }
        }
        #endregion
        #region ProblemClassIdToFpAlarmClass
        public static FP.Enums.AlarmClassType ProblemClassIdToFpAlarmClass(int? problemClassId)
        {
            var alarmClassType = FP.Enums.AlarmClassType.NONVALUE;
            if (!problemClassId.HasValue)
                return alarmClassType;
            if (Enum.IsDefined(typeof(Common.Enums.ProblemClass), problemClassId))
            {
                var problemClass = (Common.Enums.ProblemClass)problemClassId;
                alarmClassType = ProblemClassToFpAlarmClass(problemClass);
            }
            return alarmClassType;
        }
        #endregion
        #region ProblemClassToFpAlarmClass
        public static FP.Enums.AlarmClassType ProblemClassToFpAlarmClass(Common.Enums.ProblemClass problemClass)
        {
            switch (problemClass)
            {
                case Common.Enums.ProblemClass.ProbeDrowned:
                    return FP.Enums.AlarmClassType.PROBE_DROWNED;
                case Common.Enums.ProblemClass.ProbeHang:
                    return FP.Enums.AlarmClassType.PROBE_HANG;
                case Common.Enums.ProblemClass.SingleLoss:
                    return FP.Enums.AlarmClassType.SINGLE_LOSS;
                case Common.Enums.ProblemClass.SingleSurplus:
                    return FP.Enums.AlarmClassType.SINGLE_SURPLUS;
                case Common.Enums.ProblemClass.SingleSurplusWaterInflux:
                    return FP.Enums.AlarmClassType.SINGLE_SURPLUS_WATER_INFLUX;
                case Common.Enums.ProblemClass.SingleSurplusMicromixing:
                    return FP.Enums.AlarmClassType.SINGLE_SURPLUS_MICROMIXING;
                case Common.Enums.ProblemClass.PermanentLossFuelLeak:
                    return FP.Enums.AlarmClassType.PERMANENT_LOSS_FUEL_LEAK;
                case Common.Enums.ProblemClass.PermanentLossInnerLeak:
                    return FP.Enums.AlarmClassType.PERMANENT_LOSS_INNER_LEAK;
                case Common.Enums.ProblemClass.PermanentLossNozzleDecalibration:
                    return FP.Enums.AlarmClassType.PERMANENT_LOSS_NOZZLE_DECALIBRATION;
                case Common.Enums.ProblemClass.PermanentSurplusWaterInflux:
                    return FP.Enums.AlarmClassType.PERMANENT_SURPLUS_WATER_INFLUX;
                case Common.Enums.ProblemClass.PermanentSurplusInnerLeak:
                    return FP.Enums.AlarmClassType.PERMANENT_SURPLUS_INNER_LEAK;
                case Common.Enums.ProblemClass.PermanentSurplusNozzleDecalibration:
                    return FP.Enums.AlarmClassType.PERMANENT_SURPLUS_NOZZLE_DECALIBRATION;
                case Common.Enums.ProblemClass.CalibrationCurveError:
                    return FP.Enums.AlarmClassType.CALIBRATION_CURVE_ERROR;
                case Common.Enums.ProblemClass.FuelHighLevel:
                    return FP.Enums.AlarmClassType.FUEL_HIGH_LEVEL;
                case Common.Enums.ProblemClass.FuelLowLevel:
                    return FP.Enums.AlarmClassType.FUEL_LOW_LEVEL;
                case Common.Enums.ProblemClass.Deadstock:
                    return FP.Enums.AlarmClassType.DEADSTOCK;
                case Common.Enums.ProblemClass.WaterHighLevel:
                    return FP.Enums.AlarmClassType.WATER_HIGH_LEVEL;
                case Common.Enums.ProblemClass.SirenFailed:
                    return FP.Enums.AlarmClassType.SIREN_FAILED;
                case Common.Enums.ProblemClass.Density:
                    return FP.Enums.AlarmClassType.DENSITY;
                case Common.Enums.ProblemClass.Overfill:
                    return FP.Enums.AlarmClassType.OVERFILL;
                case Common.Enums.ProblemClass.FuelLeakageDW:
                    return FP.Enums.AlarmClassType.FUEL_LEAKAGE_DW;
                case Common.Enums.ProblemClass.FuelLeakageP:
                    return FP.Enums.AlarmClassType.FUEL_LEAKAGE_P;
                case Common.Enums.ProblemClass.UnderCountingGasMeter:
                    return FP.Enums.AlarmClassType.UNDER_COUNTING_GAS_METER;
                case Common.Enums.ProblemClass.OverCountingGasMeter:
                    return FP.Enums.AlarmClassType.OVER_COUNTING_GAS_METER;
                case Common.Enums.ProblemClass.CounterMalfunction:
                    return FP.Enums.AlarmClassType.COUNTER_MALFUNCTION;
                case Common.Enums.ProblemClass.NoProblem:
                    return FP.Enums.AlarmClassType.NONVALUE;
                case Common.Enums.ProblemClass.IllegalDelivery:
                    return FP.Enums.AlarmClassType.ILLEGAL_DELIVERY;
                case Common.Enums.ProblemClass.TooSmallDetectedDelivery:
                    return FP.Enums.AlarmClassType.TOO_SMALL_DETECTED_DELIVERY;
                case Common.Enums.ProblemClass.TooLargeDetectedDelivery:
                    return FP.Enums.AlarmClassType.TOO_LARGE_DETECTED_DELIVERY;
                case Common.Enums.ProblemClass.PermanentSurplus:
                    return FP.Enums.AlarmClassType.PERMANENT_SURPLUS;
                case Common.Enums.ProblemClass.PermanentLoss:
                    return FP.Enums.AlarmClassType.PERMANENT_LOSS;
                case Common.Enums.ProblemClass.IrcSir2Loss:
                    return FP.Enums.AlarmClassType.IRC_SIR2_LOSS;
                case Common.Enums.ProblemClass.ConstantLevel:
                    return FP.Enums.AlarmClassType.CONSTANT_LEVEL;
                case Common.Enums.ProblemClass.ProbeFailure:
                    return FP.Enums.AlarmClassType.PROBE_FAILURE;
                case Common.Enums.ProblemClass.DispenserFailure:
                    return FP.Enums.AlarmClassType.DISPENSER_FAILURE;
                case Common.Enums.ProblemClass.ATGSensorLiquid:
                    return FP.Enums.AlarmClassType.ATG_SENSOR_LIQUID;
                case Common.Enums.ProblemClass.ATGSensorVapor:
                    return FP.Enums.AlarmClassType.ATG_SENSOR_VAPOR;
                case Common.Enums.ProblemClass.MissingAlevelData:
                    return FP.Enums.AlarmClassType.MISSING_ALEVEL_DATA;
                case Common.Enums.ProblemClass.InvalidAlevelData:
                    return FP.Enums.AlarmClassType.INVALID_ALEVEL_DATA;
                case Common.Enums.ProblemClass.MissingOKOPacket:
                    return FP.Enums.AlarmClassType.MISSING_OKO_PACKET;
                case Common.Enums.ProblemClass.StockOutRisk:
                    return FP.Enums.AlarmClassType.STOCK_OUT_RISK;
                case Common.Enums.ProblemClass.Overfill2:
                    return FP.Enums.AlarmClassType.OVERFILL_2;

                default:
                    return FP.Enums.AlarmClassType.NONVALUE;
            }
        }
        #endregion

        #region FpAlarmCriticality
        public static Common.Enums.ProblemLevel FpAlarmCriticalityToProblemLevel(string alarmCriticality)
        {
            switch (alarmCriticality)
            {
                case "UNKNOWN":
                    return Common.Enums.ProblemLevel.Unknown;
                case "INFO":
                    return Common.Enums.ProblemLevel.Info;
                case "WARNING":
                    return Common.Enums.ProblemLevel.Warning;
                case "ERROR":
                    return Common.Enums.ProblemLevel.Error;
                case "ERROR_MAIL":
                    return Common.Enums.ProblemLevel.ErrorMail;
                case "ERROR_SMS":
                    return Common.Enums.ProblemLevel.ErrorSMS;
                case "WARNING_MAIL":
                    return Common.Enums.ProblemLevel.WarningMail;
                case "WARNING_SMS":
                    return Common.Enums.ProblemLevel.WarningSMS;
                case "DAILYERROR":
                    return Common.Enums.ProblemLevel.DailyError;
                case "DAILYWARNING":
                    return Common.Enums.ProblemLevel.DailyWarning;
                case "SM":
                    return Common.Enums.ProblemLevel.SM;
                case "CM":
                    return Common.Enums.ProblemLevel.CM;
                case "TM":
                    return Common.Enums.ProblemLevel.TM;
                case "RSAA":
                    return Common.Enums.ProblemLevel.RSAA;
                case "PLIP_REPORTED_EMAIL":
                    return Common.Enums.ProblemLevel.PlipReportedEmail;
                case "PLIP_REPORTED_SMS":
                    return Common.Enums.ProblemLevel.PlipReportedSMS;
                case "PLIP_ESCALATED_EMAIL":
                    return Common.Enums.ProblemLevel.PlipEscalatedEmail;
                case "PLIP_ESCALATED_SMS":
                    return Common.Enums.ProblemLevel.PlipEscalatedSMS;
                case "PLIP_CONFIRMED_EMAIL":
                    return Common.Enums.ProblemLevel.PlipConfirmedEmail;
                case "PLIP_CONFIRMED_SMS":
                    return Common.Enums.ProblemLevel.PlipConfirmedSMS;
                case "PLIP_B_LEAK":
                    return Common.Enums.ProblemLevel.PlipBLeak;
                default:
                    return Common.Enums.ProblemLevel.Unknown;
            }
        }
        #endregion
        #region ProblemLevelToFpAlarmCriticality
        public static FP.Enums.AlarmCriticalityType ProblemLevelToFpAlarmCriticality(Common.Enums.ProblemLevel problemLevel)
        {
            switch (problemLevel)
            {
                case Common.Enums.ProblemLevel.Unknown:
                    return Enums.AlarmCriticalityType.UNKNOWN;
                case Common.Enums.ProblemLevel.Info:
                    return Enums.AlarmCriticalityType.INFO;
                case Common.Enums.ProblemLevel.Warning:
                    return Enums.AlarmCriticalityType.WARNING;
                case Common.Enums.ProblemLevel.Error:
                    return Enums.AlarmCriticalityType.ERROR;
                case Common.Enums.ProblemLevel.ErrorMail:
                    return Enums.AlarmCriticalityType.ERROR_MAIL;
                case Common.Enums.ProblemLevel.ErrorSMS:
                    return Enums.AlarmCriticalityType.ERROR_SMS;
                case Common.Enums.ProblemLevel.WarningMail:
                    return Enums.AlarmCriticalityType.WARNING_MAIL;
                case Common.Enums.ProblemLevel.WarningSMS:
                    return Enums.AlarmCriticalityType.WARNING_SMS;
                case Common.Enums.ProblemLevel.DailyError:
                    return Enums.AlarmCriticalityType.DAILYERROR;
                case Common.Enums.ProblemLevel.DailyWarning:
                    return Enums.AlarmCriticalityType.DAILYWARNING;
                case Common.Enums.ProblemLevel.SM:
                    return Enums.AlarmCriticalityType.SM;
                case Common.Enums.ProblemLevel.CM:
                    return Enums.AlarmCriticalityType.CM;
                case Common.Enums.ProblemLevel.TM:
                    return Enums.AlarmCriticalityType.TM;
                case Common.Enums.ProblemLevel.RSAA:
                    return Enums.AlarmCriticalityType.RSAA;
                case Common.Enums.ProblemLevel.PlipReportedEmail:
                    return Enums.AlarmCriticalityType.PLIP_REPORTED_EMAIL;
                case Common.Enums.ProblemLevel.PlipReportedSMS:
                    return Enums.AlarmCriticalityType.PLIP_REPORTED_SMS;
                case Common.Enums.ProblemLevel.PlipEscalatedEmail:
                    return Enums.AlarmCriticalityType.PLIP_ESCALATED_EMAIL;
                case Common.Enums.ProblemLevel.PlipEscalatedSMS:
                    return Enums.AlarmCriticalityType.PLIP_ESCALATED_SMS;
                case Common.Enums.ProblemLevel.PlipConfirmedEmail:
                    return Enums.AlarmCriticalityType.PLIP_CONFIRMED_EMAIL;
                case Common.Enums.ProblemLevel.PlipConfirmedSMS:
                    return Enums.AlarmCriticalityType.PLIP_CONFIRMED_SMS;
                case Common.Enums.ProblemLevel.PlipBLeak:
                    return Enums.AlarmCriticalityType.PLIP_B_LEAK;
                default:
                    return Enums.AlarmCriticalityType.NONVALUE;
            }
        }
        #endregion
    }
    #endregion
}
