﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetIssues
        public List<Objects.Issue> GetIssues(long[] locationIds, DateTime? startTime, DateTime? endTime)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "issue/getIssues";
                request.RootElement = "getIssues";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetIssues"), request.Resource);

                if (locationIds != null)
                {
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);
                }

                if (startTime.HasValue)
                {
                    long msStart = DateTimer.GetMsUtcFromDtLocal(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = DateTimer.GetMsUtcFromDtLocal(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetIssues"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Issue>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetIssues"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetIssuesForId
        public List<Objects.Issue> GetIssuesForId(long[] ids)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "issue/getIssuesForId";
                request.RootElement = "getIssuesForId";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetIssuesForId"), request.Resource);

                if (ids != null)
                {
                    foreach (var id in ids)
                        request.AddParameter("id", id);
                }
                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetIssuesForId"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Issue>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetIssuesForId"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SaveIssuesWithDataReturn
        public List<Objects.Issue> SaveIssuesWithDataReturn(List<Objects.Issue> issues)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "issue/saveIssuesWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveIssuesWithDataReturn"), request.Resource);

                request.AddJsonBody(issues);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Issue>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveIssuesWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError && !string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponseError, "SaveIssuesWithDataReturn", response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                    return null;
                }
                else
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponse, "SaveIssuesWithDataReturn", response.StatusCode, response.ContentLength, response.ResponseUri);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveIssuesWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region DeleteIssues
        public bool DeleteIssues(long[] ids)
        {
            try
            {
                var request = new RestRequest(Method.DELETE);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "issue/deleteIssues";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("DeleteIssues"), request.Resource);

                request.AddBody(ids);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("DeleteIssues"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError && !string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponseError, "DeleteIssues", response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                    return false;
                }
                else if (string.IsNullOrEmpty(content))
                {
                    Logger.Add(EventID.WEsbProviderFP.ClientResponse, "DeleteIssues", response.StatusCode, response.ContentLength, response.ResponseUri);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("DeleteIssues"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
