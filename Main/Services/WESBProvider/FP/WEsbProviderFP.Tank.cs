﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using IMR.Suite.Common;
using RestSharp;
using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetTanks
        public List<Objects.Tank> GetTanks(long[] locationIds, long[] tankIds)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "tank/getTanks";
                request.RootElement = "getTanks";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                IRestResponse response = null;

                if (locationIds != null)
                {
                    foreach (var id in locationIds)
                        request.AddParameter("locationId", id);
                }

                if (tankIds != null)
                {
                    foreach (var id in tankIds)
                        request.AddParameter("tankId", id);
                }

                var client = ConectRestClient();
                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });

                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                {
                    if ((response.ErrorException is WebException && (response.ErrorException as WebException).Status == WebExceptionStatus.Timeout)
                        || (response.ErrorException is UriFormatException))
                        throw response.ErrorException;
                    else
                        throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));
                }

                var result = JsonConvert.DeserializeObject<List<Objects.Tank>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                throw ex;
            }
            return null;
        }
        #endregion

        #region SaveTank
        public bool? SaveTank(Objects.Tank tank)
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareSaveRequest(Method.POST, "tank", "saveTank", methodName);
                request.AddJsonBody(tank);
                return ExecuteRequest<bool?>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveTanks -- todo
        public bool SaveTanks(List<Objects.Tank> tanks)
        {
            bool result = false;
            foreach (var tank in tanks)
            {
                bool? mRes = SaveTank(tank);
                result = mRes.HasValue ? result && mRes.Value : false;
            }
            return result;
        }
        #endregion
    }
}
