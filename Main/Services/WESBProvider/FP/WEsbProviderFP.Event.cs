﻿using IMR.Suite.Common;
using IMR.Suite.Services.WEsbProvider;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetEvents
        public List<Objects.Event> GetEvents(long[] locationIds, DateTime? startTime, DateTime? endTime, int commandTimeout = 0)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "event/getEvents";
                request.RootElement = "getEvents";

                if (commandTimeout > 0)
                {
                    request.ReadWriteTimeout =
                    request.Timeout = commandTimeout * 1000;
                }

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetEvents"), request.Resource);

                IRestResponse response = null;

                if (locationIds != null)
                {
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);
                }

                if (startTime.HasValue)
                {
                    long msStart = MilisecondsFrom1970(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = MilisecondsFrom1970(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();

                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });

                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetEvents"), response.StatusCode, response.ContentLength, response.ResponseUri);

                if (response.ErrorException != null)
                {
                    if ((response.ErrorException is WebException && (response.ErrorException as WebException).Status == WebExceptionStatus.Timeout)
                        || (response.ErrorException is UriFormatException))
                        throw response.ErrorException;
                    else
                        throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));
                }

                var result = JsonConvert.DeserializeObject<List<Objects.Event>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetEvents"), ex.Message, ex.StackTrace);
                throw ex;
            }
        }
        #endregion
        #region SaveEvents
        public bool SaveEvents(List<Objects.Event> events)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "event/saveEvents";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveEvents"), request.Resource);

                request.AddJsonBody(events);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveEvents"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveEvents"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
