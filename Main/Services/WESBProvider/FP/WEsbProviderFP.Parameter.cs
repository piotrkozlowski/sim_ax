﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetParameters
        public List<Objects.ParameterDefinition> GetParameters(FP.Enums.ParamType[] paramTypes, int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "parameter", "getParameters", methodName, commandTimeout);
                AddCollectionToParameters(ref request, paramTypes, "paramTypes");
                return ExecuteRequest<List<Objects.ParameterDefinition>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region GetParameterValues
        public List<Objects.ParameterValue> GetParameterValues(long[] idLocation = null, int[] parameterType = null, int commandTimeout = 0)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "parameter", "getParameterValues", methodName, commandTimeout);
                AddCollectionToParameters(ref request, idLocation, "locId");
                AddCollectionToParameters(ref request, parameterType, "paramTypeId");
                return ExecuteRequest<List<Objects.ParameterValue>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region SaveParameterValue
        public bool? SaveParameterValue(FP.Objects.ParameterValue parameterValue)
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareSaveRequest(Method.POST, "parameter", "saveParameterValue", methodName);
                request.AddJsonBody(parameterValue);
                return ExecuteRequest<bool?>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
