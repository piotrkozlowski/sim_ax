﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {        
        #region GetTemperatureCoefficient
        public List<Objects.TemperatureCoefficient> GetTemperatureCoefficient()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "temperatureCoefficient/getTemperatureCoefficient";
                request.RootElement = "getTemperatureCoefficient";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.TemperatureCoefficient>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
    }
}
