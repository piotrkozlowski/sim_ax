﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetAlarms
        public List<Objects.Alarm> GetAlarms(long[] locationIds, long[] tankIds, DateTime? startTime, DateTime? endTime)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "alarm/getAlarms";
                request.RootElement = "getAlarms";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                IRestResponse response = null;

                if (locationIds != null)
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);

                if (tankIds != null)
                    foreach (var tank in tankIds)
                        request.AddParameter("tankId", tank);

                if (startTime.HasValue)
                {
                    long msStart = MilisecondsFrom1970(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = MilisecondsFrom1970(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();

                dataFillTask = new Task(() =>
                    {
                        try
                        {
                            response = client.Execute(request);
                        }
                        catch (ThreadInterruptedException) { }
                    });

                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                {
                    if ((response.ErrorException is WebException && (response.ErrorException as WebException).Status == WebExceptionStatus.Timeout)
                        || (response.ErrorException is UriFormatException))
                        throw response.ErrorException;
                    else
                        throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));
                }

                var result = JsonConvert.DeserializeObject<List<Objects.Alarm>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                throw ex;
            }
            return null;
        }
        #endregion
        #region SaveAlarms
        public bool SaveAlarms(List<Objects.Alarm> alarms)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "alarm/saveAlarms";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                request.AddJsonBody(alarms);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
