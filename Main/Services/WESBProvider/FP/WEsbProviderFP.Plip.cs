﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetPlips
        public List<Objects.Plip> GetPlips(long[] locationIds, DateTime? startTime, DateTime? endTime)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "plip/getPlips";
                request.RootElement = "getPlips";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetPlips"), request.Resource);

                if (locationIds != null)
                {
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);
                }

                if (startTime.HasValue)
                {
                    long msStart = MilisecondsFrom1970(startTime.Value);
                    request.AddParameter("startTime", msStart);
                }
                if (endTime.HasValue)
                {
                    long msEnd = MilisecondsFrom1970(endTime.Value);
                    request.AddParameter("endTime", msEnd);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetPlips"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Plip>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetPlips"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetPlipsForId
        public List<Objects.Plip> GetPlipsForId(long[] ids)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "plip/getPlipsForId";
                request.RootElement = "getPlipsForId";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetPlipsForId"), request.Resource);

                if (ids != null)
                {
                    foreach (var loc in ids)
                        request.AddParameter("id", loc);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetPlipsForId"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Plip>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetPlipsForId"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SavePlips
        public bool SavePlips(List<Objects.Plip> plips)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "plip/savePlips";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SavePlips"), request.Resource);

                request.AddJsonBody(plips);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SavePlips"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SavePlips"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SavePlipsWithDataReturn
        public List<Objects.Plip> SaveDeliveriesWithDataReturn(List<Objects.Plip> plips)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "plip/savePlipsWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SavePlipsWithDataReturn"), request.Resource);

                request.AddJsonBody(plips);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.Plip>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SavePlipsWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SavePlipsWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 

        #region GetPlipQuestions
        public List<Objects.PlipQuestion> GetPlipQuestions()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "plip/getPlipQuestions";
                request.RootElement = "getPlipQuestions";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetPlipQuestions"), request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetPlipQuestions"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.PlipQuestion>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetPlipQuestions"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region SavePlipQuestions
        public bool SavePlipQuestions(List<Objects.PlipQuestion> plipQuestions)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "plip/savePlipQuestions";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SavePlipQuestions"), request.Resource);

                request.AddJsonBody(plipQuestions);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SavePlipQuestions"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SavePlipQuestions"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SavePlipQuestionsWithDataReturn
        public List<Objects.PlipQuestion> SavePlipQuestionsWithDataReturn(List<Objects.PlipQuestion> plipQuestions)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "plip/savePlipQuestionsWithDataReturn";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SavePlipQuestionsWithDataReturn"), request.Resource);

                request.AddJsonBody(plipQuestions);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var result = JsonConvert.DeserializeObject<List<Objects.PlipQuestion>>(content);

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SavePlipQuestionsWithDataReturn"), response.StatusCode, response.ContentLength, response.ResponseUri, response.Content);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SavePlipQuestionsWithDataReturn"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion 
    }
}
