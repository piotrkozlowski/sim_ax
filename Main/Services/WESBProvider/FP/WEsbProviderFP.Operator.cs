﻿using IMR.Suite.Common;
using RestSharp;
using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {        
        #region GetOperator
        public Objects.Operator GetOperator(long idOperator)
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "operator", "getOperator", methodName);
                AddParameter(ref request, idOperator, "operatorId");
                return ExecuteRequest<Objects.Operator>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetOperators
        public List<Objects.Operator> GetOperators(long[] operatorIds)
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "operator", "getOperators", methodName);
                AddCollectionToParameters(ref request, operatorIds, "id");
                return ExecuteRequest<List<Objects.Operator>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return new List<Objects.Operator>();
            }
        }
        #endregion
        #region GetAllOperators
        public List<Objects.Operator> GetAllOperators()
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "operator", "getAllOperators", methodName);
                return ExecuteRequest<List<Objects.Operator>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return new List<Objects.Operator>();
            }
        }
        #endregion

        #region SaveOperator
        public bool? SaveOperator(Objects.Operator o)
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareSaveRequest(Method.POST, "operator", "saveOperator", methodName);
                request.AddJsonBody(o);
                return ExecuteRequest<bool?>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
