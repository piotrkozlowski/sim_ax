﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetProducts
        public List<Objects.Product> GetProducts()
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "product/getProducts";
                request.RootElement = "getProducts";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetProducts"), request.Resource);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetProducts"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Product>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetProducts"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
    }
}
