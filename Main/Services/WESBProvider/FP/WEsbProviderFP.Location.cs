﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using RestSharp;
using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetLocations
        public List<Objects.Location> GetLocations(long[] locationIds, string[] customerNumbers, string[] stationCodes, long idDistributor = -1)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "location/getLocations";
                request.RootElement = "getLocations";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, System.Reflection.MethodBase.GetCurrentMethod().Name, request.Resource);

                if (locationIds != null)
                {
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);
                }

                if (customerNumbers != null)
                {
                    foreach (var loc in customerNumbers)
                        request.AddParameter("customerNo", loc);
                }

                if (stationCodes != null)
                {
                    foreach (var loc in stationCodes)
                        request.AddParameter("stationCode", loc);
                }

                if (idDistributor > -1)
                    request.AddParameter("distributorId", idDistributor);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, System.Reflection.MethodBase.GetCurrentMethod().Name, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Location>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion

        #region SaveLocation
        public bool? SaveLocation(Objects.Location location)
        {
            String methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {            
                RestRequest request = PrepareSaveRequest(Method.POST, "location", "saveLocation", methodName);
                request.AddJsonBody(location);
                return ExecuteRequest<bool?>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveLocations -- todo
        public bool SaveLocations(List<Objects.Location> locations)
        {
            bool result = false;
            foreach (var location in locations)
            {
                bool? mRes = SaveLocation(location);
                result = mRes.HasValue ? result && mRes.Value : false;
            }
            return result;
        }
        #endregion
    }
}
