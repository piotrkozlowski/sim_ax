﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace IMR.Suite.Services.WEsbProvider.FP
{
    public partial class WEsbProviderFP
    {
        #region GetDispensers
        public List<Objects.Dispenser> GetDispensers(long[] locationIds, long[] dispenserIds)
        {
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "dispenser/getDispensers";
                request.RootElement = "getDispensers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetDispensers"), request.Resource);

                if (locationIds != null)
                {
                    foreach (var loc in locationIds)
                        request.AddParameter("locationId", loc);
                }

                if (dispenserIds != null)
                {
                    foreach (var dis in dispenserIds)
                        request.AddParameter("dispenserId", dis);
                }

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetDispensers"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<List<Objects.Dispenser>>(content);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetDispensers"), ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion
        #region GetDispenserMeasurements
        public List<Objects.DispenserMeasurement> GetDispenserMeasurements(long[] fuelBalanceIds, long[] dispenserIds, int commandTimeout = 0)
        {
            Task dataFillTask = null;
            try
            {
                var request = new RestRequest(Method.GET);
                request.Resource = "dispenser/getDispenserMeasurements";
                request.RootElement = "getDispenserMeasurements";

                if (commandTimeout > 0)
                {
                    request.ReadWriteTimeout =
                    request.Timeout = commandTimeout * 1000;
                }

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("GetDispenserMeasurements"), request.Resource);

                IRestResponse response = null;

                if (fuelBalanceIds != null)
                {
                    foreach (var fb in fuelBalanceIds)
                        request.AddParameter("fId", fb);
                }

                if (dispenserIds != null)
                {
                    foreach (var dis in dispenserIds)
                        request.AddParameter("dId", dis);
                }

                var client = ConectRestClient();

                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });

                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("GetDispenserMeasurements"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                {
                    if ((response.ErrorException is WebException && (response.ErrorException as WebException).Status == WebExceptionStatus.Timeout)
                        || (response.ErrorException is UriFormatException))
                        throw response.ErrorException;
                    else
                        throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));
                }

                var result = JsonConvert.DeserializeObject<List<Objects.DispenserMeasurement>>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("GetDispenserMeasurements"), ex.Message, ex.StackTrace);
                throw ex;
            }
        }
        #endregion

        #region GetAllDispencersCalibrationErrors
        /// <summary>
        /// Method gets calibration error value for each active dispenser.
        /// </summary>
        /// <returns>Dictionary where key is dispenserId (IdMeter) and value is calibration error.</returns>
        public Dictionary<long, decimal?> GetAllDispencersCalibrationErrors()
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dispenser", "getAllDispencersCalibrationErrors", methodName);
                return ExecuteRequest<Dictionary<long, decimal?>>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion

        #region SaveDispensers
        public bool SaveDispensers(List<Objects.Dispenser> dispensers)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "dispenser/saveDispensers";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveDispensers"), request.Resource);

                request.AddJsonBody(dispensers);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveDispensers"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveDispensers"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region SaveDispenserMeasurements
        public bool SaveDispenserMeasurements(List<Objects.DispenserMeasurement> dispenserMeasurements)
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                request.AddHeader("Content-type", "application/json");
                request.Resource = "dispenser/saveDispenserMeasurements";

                Logger.Add(EventID.WEsbProviderFP.ClientRequest, String.Format("SaveDispenserMeasurements"), request.Resource);

                request.AddJsonBody(dispenserMeasurements);

                var client = ConectRestClient();
                IRestResponse response = client.Execute(request);
                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, String.Format("SaveDispenserMeasurements"), response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(content))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, String.Format("SaveDispenserMeasurements"), ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
