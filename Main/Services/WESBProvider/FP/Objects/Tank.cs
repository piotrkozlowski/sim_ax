﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Tank
    public class Tank
    {
        #region Properties
        [JsonIgnore]
        private long _id;
        [JsonProperty("id")]
        public long? Id { get { return _id; } set { _id = value == null ? 0 : value.Value; } }

        [JsonProperty("actualCalibration")]
        public long? ActualCalibration { get; set; }

        private double _capacity;
        [JsonProperty("capacity")]
        public double? Capacity { get { return _capacity; } set { _capacity = value == null ? 0.0 : value.Value; } }

        private bool _cumTank;
        [JsonProperty("cumTank")]
        public bool? CumTank { get { return _cumTank; } set { _cumTank = value == null ? false : value.Value; } }

        [JsonIgnore]
        private long _date;
        [JsonProperty("date")]
        public long? Date { get { return _date; } set { _date = value == null ? 0 : value.Value; } }

        [JsonProperty("deadstock")]
        public double? Deadstock { get; set; }

        [JsonIgnore]
        private double _height;
        [JsonProperty("height")]
        public double? Height { get { return _height; } set { _height = value == null ? 0 : value.Value; } }


        [JsonProperty("highWater")]
        public double? HighWater { get; set; }
        [JsonProperty("lowLevel")]
        public double? LowLevel { get; set; }
        [JsonProperty("overfill")]
        public double? Overfill { get; set; }
        [JsonProperty("serialNbr")]
        public string SerialNbr { get; set; }

        [JsonIgnore]
        private string _tankName;
        [JsonProperty("tankName")]
        public string TankName { get { return _tankName ?? ""; } set { _tankName = value == null ? "" : value; } }

        private int _tankNumber;
        [JsonProperty("tankNumber")]
        public int? TankNumber { get { return _tankNumber; } set { _tankNumber = value == null ? 0 : value.Value; } }

        [JsonProperty("tankSn")]
        public string TankSn { get; set; }
        [JsonProperty("isVertical")]
        public bool? IsVertical { get; set; }
        [JsonProperty("isUnderground")]
        public bool? IsUnderground { get; set; }
        [JsonProperty("isShapeSquare")]
        public bool? IsShapeSquare { get; set; }
        [JsonProperty("isHihiLevelEnable")]
        public bool? IsHihiLevelEnable { get; set; }
        [JsonProperty("hihiLevel")]
        public double? HihiLevel { get; set; }
        [JsonProperty("hiLevel")]
        public double? HiLevel { get; set; }
        [JsonProperty("isLoloLevelEnable")]
        public bool? IsLoloLevelEnable { get; set; }
        [JsonProperty("loloLevel")]
        public double? LoloLevel { get; set; }
        [JsonProperty("loLevel")]
        public double? LoLevel { get; set; }

        private bool _active;
        [JsonProperty("active")]
        public bool? Active { get { return _active; } set { _active = value == null ? false : value.Value; } }

        [JsonProperty("locationId")]
        public long? LocationId { get; set; }

        [JsonIgnore]
        private long _productId;
        [JsonProperty("productId")]
        public long? ProductId { get { return _productId; } set { _productId = value == null ? 0 : value.Value; } }

        [JsonProperty("tankId")]
        public long? TankId { get; set; }

        [JsonIgnore]
        public LastTankData LastTankData { get; set; }
        [JsonProperty("lastTankData")]
        public LastTankData LastTankDataAlternateSetter { set { LastTankData = value; } }

        [JsonProperty("useTill")]
        public long? UseTill { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? ActualCalibrationUTC { get { return ActualCalibration.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(ActualCalibration) : null; } }
        [JsonIgnore]
        public DateTime? ActualCalibrationLocal { get { return ActualCalibration.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(ActualCalibration) : null; } }

        [JsonIgnore]
        public DateTime? DateUTC { get { return Date.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Date) : null; } }
        [JsonIgnore]
        public DateTime? DateLocal { get { return Date.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Date) : null; } }

        [JsonIgnore]
        public DateTime? UseTillUTC
        {
            get { return UseTill.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(UseTill) : null; }
            set { UseTill = Common.DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? UseTillLocal
        {
            get { return UseTill.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(UseTill) : null; }
            set { UseTill = Common.DateTimer.GetMsUtcFromDtLocal(value); }
        }
        #endregion

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(typeof(Tank)))
            {
                string name = descriptor.Name;
                sb.AppendFormat("{0}={1};", name, descriptor.GetValue(this));
            }
            return sb.ToString();
        }
    }
    #endregion

    #region LastTankData
    public class LastTankData
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("fuelDepth")]
        public double? FuelDepth { get; set; }
        [JsonProperty("fuelPercent")]
        public double? FuelPercent { get; set; }
        [JsonProperty("fuelVolume")]
        public double? FuelVolume { get; set; }

        [JsonIgnore]
        private long _lastReadOut;
        [JsonProperty("lastReadout")]
        public long? LastReadout { get { return _lastReadOut; } set { _lastReadOut = value == null ? 0 : value.Value; } }

        [JsonProperty("temperature")]
        public double? Temperature { get; set; }
        [JsonProperty("ullage")]
        public double? Ullage { get; set; }
        [JsonProperty("waterDepth")]
        public double? WaterDepth { get; set; }
        [JsonProperty("waterVolume")]
        public double? WaterVolume { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        #endregion

        #region CustomProperties
        [JsonIgnore]
        public DateTime? LastReadoutUTC { get { return LastReadout.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(LastReadout) : null; } }
        [JsonIgnore]
        public DateTime? LastReadoutLocal { get { return LastReadout.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(LastReadout) : null; } }
        #endregion
    }
    #endregion
}
