﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Dispenser
    public class Dispenser
    {
        #region Properties

        private long _id;
        [JsonProperty("id")]
        public long? Id { get { return _id; } set { _id = value == null ? 0 : value.Value; } }

        [JsonProperty("imrscId")]
        public long? ImrscId { get; set; }

        private bool _isActive;
        [JsonProperty("isActive")]
        public bool? IsActive { get { return _isActive; } set { _isActive = value == null ? false : value.Value; } }

        [JsonProperty("nozzleName")]
        public string NozzleName { get; set; }

        private int _nozzlePosition;
        [JsonProperty("nozzlePosition")]
        public int? NozzlePosition { get { return _nozzlePosition; } set { _nozzlePosition = value == null ? 0 : value.Value; } }

        [JsonProperty("serialNbr")]
        public long? SerialNbr { get; set; }

        private long _locationId;
        [JsonProperty("locationId")]
        public long? LocationId { get { return _locationId; } set { _locationId = value == null ? 0 : value.Value; } }

        private long _tankId;
        [JsonProperty("tankId")]
        public long? TankId { get { return _tankId; } set { _tankId = value == null ? 0 : value.Value; } }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public decimal? CalibrationError { get; set; }
        #endregion

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(typeof(Dispenser)))
            {
                string name = descriptor.Name;
                sb.AppendFormat("{0}={1};", name, descriptor.GetValue(this));
            }
            return sb.ToString();
        }
    }
    #endregion

    #region DispenserMeasurement
    public class DispenserMeasurement
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("counter")]
        public double? Counter { get; set; }
        [JsonProperty("dispenserId")]
        public long? DispenserId { get; set; }
        [JsonProperty("fuelBalanceId")]
        public long? FuelBalanceId { get; set; }
        #endregion

        #region ToOpDataTemporal
        public static OpDataTemporal ToOpDataTemporal(IDataProvider dataProvider, DispenserMeasurement nozzle, FuelBalance fuelBalance)
        {
            int idDataSourceType = (int)Common.Enums.DataSourceType.Unknown;
            string dataSourceType = fuelBalance.AggregationType.ToLower();
            if (dataSourceType.Equals("manual"))
                idDataSourceType = (int)Common.Enums.DataSourceType.ManualDataEntry;
            else if (dataSourceType.Equals("auto"))
                idDataSourceType = (int)Common.Enums.DataSourceType.OKO;

            return new OpDataTemporal()
            {
                IdDataTemporal = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0),
                IdLocation = fuelBalance.LocationId,
                IdMeter = nozzle.DispenserId,
                IdDataSourceType = idDataSourceType,
                IdDataType = DataType.NOZZLE_TOTAL_COUNTER,
                StartTime = dataProvider.ConvertUtcTimeToTimeZone(DateTimer.GetDtUtcFromMsUtc(fuelBalance.OpeningTime)),
                EndTime = dataProvider.ConvertUtcTimeToTimeZone(DateTimer.GetDtUtcFromMsUtc(fuelBalance.ClosingTime)),
                Value = nozzle.Counter.HasValue ? nozzle.Counter / 1000 : null,
                IdAggregationType = (int)Common.Enums.AggregationType.Day,
                DataStatus = Common.Enums.DataStatus.Normal,
                IndexNbr = 0
            };
        }
        #endregion
        #region ToListOpDataTemporal
        public static List<OpDataTemporal> ToListOpDataTemporal(IDataProvider dataProvider, List<DispenserMeasurement> nozzleList, List<FuelBalance> fuelBalanceList)
        {
            List<OpDataTemporal> returnList = new List<OpDataTemporal>();
            if (nozzleList != null && nozzleList.Count > 0 && fuelBalanceList != null && fuelBalanceList.Count > 0)
            {
                Dictionary<long, FuelBalance> fuelBalanceDict = fuelBalanceList.Where(q => q.Id.HasValue).Distinct(q => q.Id.Value).ToDictionary(q => q.Id.Value);
                foreach (DispenserMeasurement nozzle in nozzleList)
                {
                    if (nozzle.FuelBalanceId.HasValue && fuelBalanceDict.ContainsKey(nozzle.FuelBalanceId.Value) && fuelBalanceDict[nozzle.FuelBalanceId.Value] != null)
                        returnList.Add(ToOpDataTemporal(dataProvider, nozzle, fuelBalanceDict[nozzle.FuelBalanceId.Value]));
                }
            }
            return returnList;
        }
        #endregion
    }
    #endregion
}
