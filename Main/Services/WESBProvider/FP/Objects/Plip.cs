﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Plip
    public class Plip
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("confirmedDate")]
        public long? ConfirmedDate { get; set; }
        [JsonProperty("confirmedName")]
        public string ConfirmedName { get; set; }
        [JsonProperty("confirmedNotes")]
        public string ConfirmedNotes { get; set; }
        [JsonProperty("deadline")]
        public long? Deadline { get; set; }
        [JsonProperty("endDate")]
        public long? EndDate { get; set; }
        [JsonProperty("escalatedDate")]
        public long? EscalatedDate { get; set; }
        [JsonProperty("escalatedName")]
        public string EscalatedName { get; set; }
        [JsonProperty("escalatedNotes")]
        public string EscalatedNotes { get; set; }
        [JsonProperty("escalatedReason")]
        public string EscalatedReason { get; set; }
        [JsonProperty("plipState")]
        public string PlipState { get; set; }
        [JsonProperty("plipType")]
        public string PlipType { get; set; }
        [JsonProperty("priority")]
        public string Priority { get; set; }
        [JsonProperty("productId")]
        public long? ProductId { get; set; }
        [JsonProperty("raportedDate")]
        public long? RaportedDate { get; set; }
        [JsonProperty("raportedName")]
        public string RaportedName { get; set; }
        [JsonProperty("raportedNotes")]
        public string RaportedNotes { get; set; }
        [JsonProperty("relatedPlips")]
        public List<long> RelatedPlips { get; set; }
        [JsonProperty("startDate")]
        public long? StartDate { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("tankId")]
        public long? TankId { get; set; }
        [JsonProperty("plipAnswers")]
        public List<PlipAnswer> PlipAnswers { get; set; }
        [JsonProperty("grossBalanceLitre")]
        public double? GrossBalanceLitre { get; set; }
        [JsonProperty("grossBalancePercent")]
        public double? GrossBalancePercent { get; set; }
        [JsonProperty("netBalanceLitre")]
        public double? NetBalanceLitre { get; set; }
        [JsonProperty("netBalancePercent")]
        public double? NetBalancePercent { get; set; }
        [JsonProperty("issues")]
        public List<long> Issues { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartDateUTC { get { return StartDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(StartDate) : null; } }
        [JsonIgnore]
        public DateTime? StartDateLocal { get { return StartDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(StartDate) : null; } }

        [JsonIgnore]
        public DateTime? EndDateUTC { get { return EndDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EndDate) : null; } }
        [JsonIgnore]
        public DateTime? EndDateLocal { get { return EndDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EndDate) : null; } }

        [JsonIgnore]
        public DateTime? RaportedDateUTC { get { return RaportedDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(RaportedDate) : null; } }
        [JsonIgnore]
        public DateTime? RaportedDateLocal { get { return RaportedDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(RaportedDate) : null; } }

        [JsonIgnore]
        public DateTime? EscalatedDateUTC { get { return EscalatedDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EscalatedDate) : null; } }
        [JsonIgnore]
        public DateTime? EscalatedDateLocal { get { return EscalatedDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EscalatedDate) : null; } }

        [JsonIgnore]
        public DateTime? ConfirmedDateUTC { get { return ConfirmedDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(ConfirmedDate) : null; } }
        [JsonIgnore]
        public DateTime? ConfirmedDateLocal { get { return ConfirmedDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(ConfirmedDate) : null; } }

        [JsonIgnore]
        public DateTime? DeadlineUTC { get { return Deadline.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Deadline) : null; } }
        [JsonIgnore]
        public DateTime? DeadlineLocal { get { return Deadline.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Deadline) : null; } }
        #endregion
    }
    #endregion

    #region PlipAnswer
    public class PlipAnswer
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("logicalAnswer")]
        public bool? LogicalAnswer { get; set; }
        [JsonProperty("dateAnswer")]
        public long? DateAnswer { get; set; }
        [JsonProperty("textAnswer")]
        public string TextAnswer { get; set; }
        [JsonProperty("plipQuestionId")]
        public long? PlipQuestionId { get; set; }
        [JsonProperty("attachments")]
        public IList<object> Attachments { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? DateAnswerUTC { get { return DateAnswer.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(DateAnswer) : null; } }
        [JsonIgnore]
        public DateTime? DateAnswerLocal { get { return DateAnswer.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(DateAnswer) : null; } }
        #endregion
    }
    #endregion

    #region PlipQuestion
    public class PlipQuestion
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("active")]
        public bool? Active { get; set; }
        [JsonProperty("date")]
        public long? Date { get; set; }
        [JsonProperty("answerType")]
        public string AnswerType { get; set; }
        [JsonProperty("plipType")]
        public string PlipType { get; set; }
        [JsonProperty("questionNumber")]
        public long? QuestionNumber { get; set; }
        [JsonProperty("questionText")]
        public string QuestionText { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? DateUTC { get { return Date.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Date) : null; } }
        [JsonIgnore]
        public DateTime? DateLocal { get { return Date.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Date) : null; } }
        #endregion
    }
    #endregion
}
