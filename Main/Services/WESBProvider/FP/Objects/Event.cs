﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Event
    public class Event
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("eventDate")]
        public string EventDate { get; set; }
        [JsonProperty("startTime")]
        public long? StartTime { get; set; }
        [JsonProperty("endTime")]
        public long? EndTime { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("fuelBack")]
        public bool? FuelBack { get; set; }
        [JsonProperty("eventReasonType")]
        public string EventReasonType { get; set; }
        [JsonProperty("eventType")]
        public string EventType { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("eventMeters")]
        public IList<EventMeter> EventMeters { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }

        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }
        #endregion

        #region ToListOpDataArch
        public static List<OpDataArch> ToListOpDataArch(IDataProvider dataProvider, List<Event> eventList)
        {
            List<OpDataArch> returnList = new List<OpDataArch>();
            foreach (Event eventObject in eventList)
            {
                DateTime time;
                if (DateTime.TryParse(eventObject.EventDate, out time) && eventObject.EventMeters != null)
                {
                    foreach (EventMeter eventMeter in eventObject.EventMeters)
                    {
                        if (eventMeter.FlaskMeasurement.HasValue && eventMeter.DispenserId.HasValue)
                            returnList.Add(EventMeter.ToOpDataArch(dataProvider, eventMeter.DispenserId, eventObject.LocationId, time, DataType.AWSR_FP_EVENT_DISCHARGED_VOLUME, 0, eventMeter.FlaskMeasurement.Value / 1000));
                        if (eventMeter.CounterValue.HasValue && eventMeter.DispenserId.HasValue)
                            returnList.Add(EventMeter.ToOpDataArch(dataProvider, eventMeter.DispenserId, eventObject.LocationId, time, DataType.AWSR_FP_EVENT_DISCHARGED, 0, eventMeter.CounterValue.Value / 1000));
                    }
                }
            }
            return returnList;
        }
        #endregion
    }
    #endregion

    #region EventMeter
    public class EventMeter
    {
        #region Propreties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("gridPosition")]
        public int? GridPosition { get; set; }
        [JsonProperty("meterType")]
        public string MeterType { get; set; }
        [JsonProperty("counterValue")]
        public double? CounterValue { get; set; }
        [JsonProperty("calibrationError")]
        public double? CalibrationError { get; set; }
        [JsonProperty("flaskMeasurement")]
        public double? FlaskMeasurement { get; set; }
        [JsonProperty("volume")]
        public double? Volume { get; set; }
        [JsonProperty("volumeLost")]
        public double? VolumeLost { get; set; }
        [JsonProperty("eventTableType")]
        public string EventTableType { get; set; }
        [JsonProperty("dispenserId")]
        public long? DispenserId { get; set; }
        [JsonProperty("productId")]
        public long? ProductId { get; set; }
        [JsonProperty("tankId")]
        public long? TankId { get; set; }
        #endregion

        #region ToOpDataArch
        public static OpDataArch ToOpDataArch(IDataProvider dataProvider, long? idMeter, long? idLocation, DateTime utcTime, long idDataType, int index, object value)
        {
            return new OpDataArch()
            {
                IdDataArch = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0),
                IdMeter = idMeter,
                IdLocation = idLocation,
                IdDataType = idDataType,
                IndexNbr = index,
                Value = value,
                Time = dataProvider.ConvertUtcTimeToTimeZone(utcTime),
                DataStatus = Common.Enums.DataStatus.Normal
            };
        }
        #endregion
    }
    #endregion
}
