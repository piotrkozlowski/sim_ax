﻿using System;
using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Location
    /// <summary>
    /// Represents FuelPrime location.
    /// </summary>
    public class Location
    {
        #region Properties
        [JsonIgnore]
        private long _id;
        [JsonProperty("id")]
        public long? Id { get { return _id; } set { _id = value == null ? 0 : value.Value; } }

        [JsonIgnore]
        public string _address;
        [JsonProperty("address")]
        public string Address { get { return _address ?? ""; } set { _address = value == null ? "" : value; } }

        [JsonIgnore]
        public string _locationType;
        [JsonProperty("locationType")]
        public string LocationType { get { return _locationType ?? ""; } set { _locationType = value == null ? "" : value; } }

        [JsonProperty("atgType")]
        public string AtgType { get; set; }

        [JsonProperty("connectionType")]
        public string ConnectionType { get; set; }

        [JsonIgnore]
        public string _country;
        [JsonProperty("country")]
        public string Country { get { return _country ?? ""; } set { _country = value == null ? "" : value; } }

        [JsonIgnore]
        public string _customerNo;
        [JsonProperty("customerNo")]
        public string CustomerNo { get { return _customerNo ?? ""; } set { _customerNo = value == null ? "" : value; } }

        [JsonProperty("date")]
        public long? Date { get; set; }

        [JsonIgnore]
        private bool _inKpi;
        [JsonProperty("inKpi")]
        public bool? InKpi { get { return _inKpi; } set { _inKpi = value == null ? false : value.Value; } }

        [JsonProperty("lastNozzlesCalibration")]
        public long? LastNozzlesCalibration { get; set; }

        [JsonIgnore]
        public string _name;
        [JsonProperty("name")]
        public string Name { get { return _name ?? ""; } set { _name = value == null ? "" : value; } }

        [JsonProperty("nextNozzlesCalibration")]
        public long? NextNozzlesCalibration { get; set; }

        [JsonProperty("postCode")]
        public string PostCode { get; set; }

        [JsonIgnore]
        public string _stationCode;
        [JsonProperty("stationCode")]
        public string StationCode { get { return _stationCode ?? ""; } set { _stationCode = value == null ? "" : value; } }

        [JsonIgnore]
        public string _status;
        [JsonProperty("status")]
        public string Status { get { return _status ?? ""; } set { _status = value == null ? "NEW" : value; } }

        [JsonIgnore]
        public string _townCity;
        [JsonProperty("townCity")]
        public string TownCity { get { return _townCity ?? ""; } set { _townCity = value == null ? "" : value; } }

        [JsonIgnore]
        private double _latitude;
        [JsonProperty("latitude")]
        public double? Latitude { get { return _latitude; } set { _latitude = value == null ? 0.0 : value.Value; } }

        [JsonIgnore]
        private double _longitude;
        [JsonProperty("longitude")]
        public double? Longitude { get { return _longitude; } set { _longitude = value == null ? 0.0 : value.Value; } }

        [JsonProperty("measuredLatitude")]
        public string MeasuredLatitude { get; set; }

        [JsonProperty("measuredLongitude")]
        public string MeasuredLongitude { get; set; }

        [JsonIgnore]
        private long _distributorId;
        [JsonProperty("distributorId")]
        public long? DistributorId { get { return _distributorId; } set { _distributorId = value == null ? 0 : value.Value; } }

        [JsonProperty("territoryOperatorId")]
        public long? TerritoryOperatorId { get; set; }

        [JsonProperty("siteOperatorId")]
        public long? SiteOperatorId { get; set; }

        [JsonProperty("clusterOperatorId")]
        public long? ClusterOperatorId { get; set; }

        [JsonIgnore]
        private bool _isCounterSales;
        [JsonProperty("isCounterSales")]
        public bool? IsCounterSales { get { return _isCounterSales; } set { _isCounterSales = value == null ? false : value.Value; } }

        [JsonIgnore]
        private int _dispenserPumpType;
        [JsonProperty("dispenserPumpType")]
        public int? DispenserPumpType { get { return _dispenserPumpType; } set { _dispenserPumpType = value == null ? 0 : value.Value; } }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? DateUTC
        {
            get { return Common.DateTimer.GetDtUtcFromMsUtc(Date); }
            set { Date = Common.DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? DateLocal
        {
            get { return Common.DateTimer.GetDtLocalFromMsUtc(Date); }
            set { Date = Common.DateTimer.GetMsUtcFromDtLocal(value); }
        }

        [JsonIgnore]
        public DateTime? LastNozzlesCalibrationUTC
        {
            get { return Common.DateTimer.GetDtUtcFromMsUtc(LastNozzlesCalibration); }
            set { LastNozzlesCalibration = Common.DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? LastNozzlesCalibrationLocal
        {
            get { return Common.DateTimer.GetDtLocalFromMsUtc(LastNozzlesCalibration); }
            set { LastNozzlesCalibration = Common.DateTimer.GetMsUtcFromDtLocal(value); }
        }

        [JsonIgnore]
        public DateTime? NextNozzlesCalibrationUTC
        {
            get { return Common.DateTimer.GetDtUtcFromMsUtc(NextNozzlesCalibration); }
            set { NextNozzlesCalibration = Common.DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? NextNozzlesCalibrationLocal
        {
            get { return Common.DateTimer.GetDtLocalFromMsUtc(NextNozzlesCalibration); }
            set { NextNozzlesCalibration = Common.DateTimer.GetMsUtcFromDtLocal(value); }
        }
        #endregion

        #region Ctor
        public Location()
        {
            Id = 0;
            InKpi = false;
            Latitude = 0.0;
            Longitude = 0.0;
            DistributorId = 0;
            IsCounterSales = false;
            DispenserPumpType = 0;
            StationCode = "";
            Address = "";
            Status = "NEW";
            TownCity = "";
            Country = "";
            LocationType = "";
            Name = "";
            CustomerNo = "";
        }
        #endregion

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(typeof(Location)))
            {
                string name = descriptor.Name;
                sb.AppendFormat("{0}={1};", name, descriptor.GetValue(this));
            }
            return sb.ToString();
        }
    }
    #endregion
}
