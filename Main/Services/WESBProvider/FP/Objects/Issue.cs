﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Issue
    public class Issue
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("tankId")]
        public long? TankId { get; set; }
        [JsonProperty("issueDate")]
        public long? IssueDate { get; set; }
        [JsonProperty("issueType")]
        public string IssueType { get; set; }
        [JsonProperty("issueStatus")]
        public string IssueStatus { get; set; }
        [JsonProperty("owner")]
        public string Owner { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("recomendation")]
        public string Recomendation { get; set; }
        [JsonProperty("result")]
        public string Result { get; set; }
        [JsonProperty("resultDate")]
        public long? ResultDate { get; set; }
        [JsonProperty("plips")]
        public List<long> Plips { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public List<Plip> PlipsObj { get; set; }

        [JsonIgnore]
        public DateTime? ResultDateUTC { get { return ResultDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(ResultDate) : null; } }
        [JsonIgnore]
        public DateTime? ResultDateLocal { get { return ResultDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(ResultDate) : null; } }
        [JsonIgnore]
        public DateTime? IssueDateUTC { get { return IssueDate.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(IssueDate) : null; } }
        [JsonIgnore]
        public DateTime? IssueDateLocal { get { return IssueDate.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(IssueDate) : null; } }
        #endregion
    }
    #endregion
}