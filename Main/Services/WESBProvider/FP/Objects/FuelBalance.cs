﻿using System;
using System.Linq;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;

using Newtonsoft.Json;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region FuelBalanceData
    public class FuelBalanceData
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("balanceLitre")]
        public double? BalanceLitre { get; set; }
        [JsonProperty("balancePercent")]
        public double? BalancePercent { get; set; }
        [JsonProperty("closingStock")]
        public double? ClosingStock { get; set; }
        [JsonProperty("exceptCash")]
        public double? ExceptCash { get; set; }
        [JsonProperty("fuelDis")]
        public double? FuelDis { get; set; }
        [JsonProperty("grossNet")]
        public string GrossNet { get; set; }
        [JsonProperty("openingStock")]
        public double? OpeningStock { get; set; }
        [JsonProperty("deliveries")]
        public double? Deliveries { get; set; }
        [JsonProperty("outcome")]
        public double? Outcome { get; set; }
        [JsonProperty("deliveriesCalculated")]
        public double? DeliveriesCalculated { get; set; }
        [JsonProperty("balanceLitreFlow")]
        public double? BalanceLitreFlow { get; set; }
        [JsonProperty("balancePercentFlow")]
        public double? BalancePercentFlow { get; set; }
        [JsonProperty("fuelDisFlow")]
        public double? FuelDisFlow { get; set; }
        #endregion
    }
    #endregion

    #region FuelBalance
    public class FuelBalance
    {
        #region Propreties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("closingTime")]
        public long? ClosingTime { get; set; }
        [JsonProperty("fuelHeight")]
        public double? FuelHeight { get; set; }
        [JsonProperty("fuelVolume")]
        public double? FuelVolume { get; set; }
        [JsonProperty("aggregationType")]
        public string AggregationType { get; set; }
        [JsonProperty("openingTime")]
        public long? OpeningTime { get; set; }
        [JsonProperty("salesPos")]
        public double? SalesPos { get; set; }
        [JsonProperty("salesPosManual")]
        public double? SalesPosManual { get; set; }
        [JsonProperty("salesPosHois")]
        public double? SalesPosHois { get; set; }
        [JsonProperty("salesPosSls")]
        public double? SalesPosSls { get; set; }
        [JsonProperty("temperature")]
        public double? Temperature { get; set; }
        [JsonProperty("posOffset")]
        public double? PosOffset { get; set; }
        [JsonProperty("dispOffset")]
        public double? DispOffset { get; set; }
        [JsonProperty("outcomeReturnOffset")]
        public double? OutcomeReturnOffset { get; set; }
        [JsonProperty("outcomeLostOffset")]
        public double? OutcomeLostOffset { get; set; }
        [JsonProperty("referenceDensity")]
        public double? ReferenceDensity { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("tankId")]
        public long? TankId { get; set; }
        [JsonProperty("salePosSourceType")]
        public string SalePosSourceType { get; set; }
        [JsonProperty("fuelBalanceData")]
        public List<FuelBalanceData> FuelBalanceData { get; set; }
        [JsonProperty("fuelDisComp")]
        public double? FuelDisComp { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? ClosingTimeUTC { get { return ClosingTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(ClosingTime) : null; } }
        [JsonIgnore]
        public DateTime? ClosingTimeLocal { get { return ClosingTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(ClosingTime) : null; } }

        [JsonIgnore]
        public DateTime? OpeningTimeUTC { get { return OpeningTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(OpeningTime) : null; } }
        [JsonIgnore]
        public DateTime? OpeningTimeLocal { get { return OpeningTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(OpeningTime) : null; } }
        #endregion

        #region ToOpDataTemporal
        public static OpDataTemporal ToOpDataTemporal(long idDataType, FuelBalance fuelBalance, object value, int idDataSourceType, int index = 0)
        {
            Common.Enums.DataStatus dataStatus = Common.Enums.DataStatus.Normal;
            if (idDataType == DataType.AWSR_FUEL_POS_VOLUME_DISPENCED)
            {
                // HOIS & MANUAL = Normal
                if (!string.IsNullOrEmpty(fuelBalance.SalePosSourceType))
                {
                    string posSourceType = fuelBalance.SalePosSourceType.ToLower();
                    if (posSourceType.Equals("sls"))
                        dataStatus = (Common.Enums.DataStatus)Convert.ToInt64(Common.Enums.DataStatus.Synchronized) + Convert.ToInt64(Common.Enums.DataStatus.Normal);
                }
            }

            var dt = new OpDataTemporal()
            {
                IdDataTemporal = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0),
                IdLocation = fuelBalance.LocationId,
                IdMeter = fuelBalance.TankId,
                IdDataSourceType = idDataSourceType,
                IdDataType = idDataType,
                StartTime = DateTimer.GetDtLocalFromMsUtc(fuelBalance.OpeningTime),
                EndTime = DateTimer.GetDtLocalFromMsUtc(fuelBalance.ClosingTime),
                Value = value,
                IdAggregationType = (int)Common.Enums.AggregationType.Day,
                DataStatus = dataStatus,
                IndexNbr = index
            };
            return dt;
        }
        #endregion

    }
    #endregion

    #region FuelBalanceHeader
    public class FuelBalanceHeader
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("balanceDate")]
        public long? BalanceDate { get; set; }
        [JsonProperty("comments")]
        public string Comments { get; set; }
        [JsonProperty("autoComments")]
        public string AutoComments { get; set; }
        [JsonProperty("confirm")]
        public bool? Confirm { get; set; }
        [JsonProperty("locationId")]
        public long? LocationId { get; set; }
        [JsonProperty("fuelBalances")]
        public List<FuelBalance> FuelBalances { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? BalanceDateUTC { get { return BalanceDate.HasValue ? DateTimer.GetDtUtcFromMsUtc(BalanceDate) : null; } }
        [JsonIgnore]
        public DateTime? BalanceDateLocal { get { return BalanceDate.HasValue ? DateTimer.GetDtLocalFromMsUtc(BalanceDate) : null; } }
        #endregion

        #region ToListOpDataTemporal
        public static List<OpDataTemporal> ToListOpDataTemporal(IDataProvider dataProvider, List<FuelBalanceHeader> fuelBalancesHeaders)
        {
            List<OpDataTemporal> list = new List<OpDataTemporal>();

            foreach (FuelBalanceHeader fuelBalanceHeader in fuelBalancesHeaders)
            {
                foreach (FuelBalance fuelBalance in fuelBalanceHeader.FuelBalances)
                {
                    int idDataSourceType = (int)Common.Enums.DataSourceType.Unknown;
                    string dataSourceType = fuelBalance.AggregationType.ToLower();
                    if (dataSourceType.Equals("manual"))
                        idDataSourceType = (int)Common.Enums.DataSourceType.ManualDataEntry;
                    else if (dataSourceType.Equals("auto"))
                        idDataSourceType = (int)Common.Enums.DataSourceType.OKO;

                    // Pobranie typu metera dla wysokosci w zbiorniku (inny typ w LPG)
                    OpMeter meter = fuelBalance.TankId.HasValue ? dataProvider.GetMeter(fuelBalance.TankId.Value) : null;
                    OpMeterType meterType = meter != null ? dataProvider.GetMeterType(meter.IdMeterType) : null;

                    long fuelHightDataType = DataType.FUEL_DEPTH;
                    if (meterType != null && meterType.IdMeterType == (int)Common.Enums.MeterType.LPGTank)
                        fuelHightDataType = DataType.GAS_LEVEL_PERCENTAGE_VALUE;

                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_STATUS, fuelBalance, fuelBalanceHeader.Confirm, idDataSourceType));
                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_DAY, fuelBalance, fuelBalanceHeader.BalanceDateLocal, idDataSourceType));
                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_NOTES, fuelBalance, fuelBalanceHeader.AutoComments, idDataSourceType));

                    if (fuelBalance.OpeningTime.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.TEMPORAL_START_TIME, fuelBalance, DateTimer.GetDtLocalFromMsUtc(fuelBalance.OpeningTime.Value), idDataSourceType));
                    if (fuelBalance.ClosingTime.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.TEMPORAL_END_TIME, fuelBalance, DateTimer.GetDtLocalFromMsUtc(fuelBalance.ClosingTime.Value), idDataSourceType));
                    if (fuelBalance.FuelHeight.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(fuelHightDataType, fuelBalance, fuelBalance.FuelHeight / 1000, idDataSourceType));
                    if (fuelBalance.FuelVolume.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.FUEL_VOLUME, fuelBalance, fuelBalance.FuelVolume / 1000, idDataSourceType));
                    if (fuelBalance.Temperature.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.FUEL_TEMPERATURE, fuelBalance, fuelBalance.Temperature, idDataSourceType));
                    if (fuelBalance.SalesPos.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_POS_VOLUME_DISPENCED, fuelBalance, fuelBalance.SalesPos / 1000, idDataSourceType));
                    if (fuelBalance.DispOffset.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_DISPENSED_DISTR_OFFSET, fuelBalance, fuelBalance.DispOffset / 1000, idDataSourceType));
                    if (fuelBalance.OutcomeLostOffset.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_OUTCOME_LOST_OFFSET, fuelBalance, fuelBalance.OutcomeLostOffset / 1000, idDataSourceType));
                    if (fuelBalance.OutcomeReturnOffset.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_OUTCOME_RETURN_OFFSET, fuelBalance, fuelBalance.OutcomeReturnOffset / 1000, idDataSourceType));
                    if (fuelBalance.PosOffset.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_DISPENSED_POS_OFFSET, fuelBalance, fuelBalance.PosOffset / 1000, idDataSourceType));
                    if (fuelBalance.ReferenceDensity.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.METER_MEDIUM_REFERENCE_DENSITY, fuelBalance, fuelBalance.ReferenceDensity, idDataSourceType));
                    if (fuelBalance.FuelDisComp.HasValue)
                        list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_COMP_VOLUME_DISPENCED, fuelBalance, fuelBalance.FuelDisComp / 1000, idDataSourceType));

                    if (fuelBalance.FuelBalanceData != null && fuelBalance.FuelBalanceData.Any())
                    {
                        foreach (var data in fuelBalance.FuelBalanceData)
                        {
                            if (data == null || string.IsNullOrEmpty(data.GrossNet)) continue;

                            bool isNet = data.GrossNet.ToLower().Equals("net");
                            if (isNet)
                            {
                                if (data.OpeningStock.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.FUEL_VOLUME, fuelBalance, data.OpeningStock / 1000, idDataSourceType, 2)); //Poczarowane - nie ma odpowiednika open w imr
                                if (data.ClosingStock.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_REF_VOLUME, fuelBalance, data.ClosingStock / 1000, idDataSourceType));
                                if (data.BalanceLitreFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_REF_CURRENT_ERROR, fuelBalance, data.BalanceLitreFlow / 1000, idDataSourceType));
                                if (data.BalancePercentFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_REF_CURRENT_PER_ERROR, fuelBalance, data.BalancePercentFlow / 100, idDataSourceType));
                                if (data.BalanceLitre.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_SALES_REF_CURRENT_ERROR, fuelBalance, data.BalanceLitre / 1000, idDataSourceType));
                                if (data.BalancePercent.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_SALES_REF_CURRENT_PER_ERROR, fuelBalance, data.BalancePercent / 100, idDataSourceType));

                                if (idDataSourceType == (int)Common.Enums.DataSourceType.ManualDataEntry && data.Deliveries.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_REF_VOLUME_DELIVERED, fuelBalance, data.Deliveries / 1000, idDataSourceType));
                                if (idDataSourceType == (int)Common.Enums.DataSourceType.OKO && data.DeliveriesCalculated.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_REF_VOLUME_DELIVERED, fuelBalance, data.DeliveriesCalculated / 1000, idDataSourceType));

                                if (data.FuelDis.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_RAW_REF_VOLUME_DISPENCED, fuelBalance, data.FuelDis / 1000, idDataSourceType));
                                if (data.FuelDisFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_REF_VOLUME_DISPENCED, fuelBalance, data.FuelDisFlow / 1000, idDataSourceType));
                                if (data.Outcome.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_REF_VOLUME_OUTCOME, fuelBalance, data.Outcome / 1000, idDataSourceType));
                            }
                            else
                            {
                                if (data.OpeningStock.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.FUEL_VOLUME, fuelBalance, data.OpeningStock / 1000, idDataSourceType, 1)); //Poczarowane - nie ma odpowiednika open w imr
                                if (data.ClosingStock.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_VOLUME, fuelBalance, data.ClosingStock / 1000, idDataSourceType));
                                if (data.BalanceLitreFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_CURRENT_ERROR, fuelBalance, data.BalanceLitreFlow / 1000, idDataSourceType));
                                if (data.BalancePercentFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_CURRENT_PER_ERROR, fuelBalance, data.BalancePercentFlow / 100, idDataSourceType));
                                if (data.BalanceLitre.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_SALES_CURRENT_ERROR, fuelBalance, data.BalanceLitre / 1000, idDataSourceType));
                                if (data.BalancePercent.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_RECONCILIATION_SALES_CURRENT_PER_ERROR, fuelBalance, data.BalancePercent / 100, idDataSourceType));

                                if (idDataSourceType == (int)Common.Enums.DataSourceType.ManualDataEntry && data.Deliveries.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_VOLUME_DELIVERED, fuelBalance, data.Deliveries / 1000, idDataSourceType));
                                if (idDataSourceType == (int)Common.Enums.DataSourceType.OKO && data.DeliveriesCalculated.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_VOLUME_DELIVERED, fuelBalance, data.DeliveriesCalculated / 1000, idDataSourceType));

                                if (data.FuelDis.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_RAW_VOLUME_DISPENCED, fuelBalance, data.FuelDis / 1000, idDataSourceType));
                                if (data.FuelDisFlow.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_VOLUME_DISPENCED, fuelBalance, data.FuelDisFlow / 1000, idDataSourceType));
                                if (data.Outcome.HasValue)
                                    list.Add(FuelBalance.ToOpDataTemporal(DataType.AWSR_FUEL_VOLUME_OUTCOME, fuelBalance, data.Outcome / 1000, idDataSourceType));
                            }
                        }
                    }
                }
            }
            return list;
        }
        #endregion

    }
    #endregion

    #region FuelBalanceSir
    /// <summary>
    /// Class representing single SIR2 decision and corelated operator decision.
    /// </summary>
    public class FuelBalanceSir
    {
        #region Properties
        /// <summary>
        /// Decision Id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Linked fuel balance. 
        /// </summary>
        [JsonProperty("fuelBalanceId")]
        public long? FuelBalanceId { get; set; }

        /// <summary>
        /// Decisions range start date.
        /// </summary>
        [JsonProperty("rangeStartDate")]
        public long? RangeStartDate { get; set; }

        /// <summary>
        /// Decisions range end date.
        /// </summary>
        [JsonProperty("rangeEndDate")]
        public long? RangeEndDate { get; set; }

        /// <summary>
        /// Tank id (id meter)
        /// </summary>
        [JsonProperty("tankId")]
        public long TankId { get; set; }

        /// <summary>
        /// Location id.
        /// </summary>
        [JsonProperty("locationId")]
        public long LocationId { get; set; }

        /// <summary>
        /// Main SIR2 leak rate.
        /// </summary>
        [JsonProperty("leakRate0")]
        public double? LeakRate0 { get; set; }

        /// <summary>
        /// SIR2 leak rate from method 1.
        /// </summary>
        [JsonProperty("leakRate1")]
        public double? LeakRate1 { get; set; }

        /// <summary>
        /// SIR2 leak rate from method 2.
        /// </summary>
        [JsonProperty("leakRate2")]
        public double? LeakRate2 { get; set; }

        /// <summary>
        /// SIR2 leak rate from method 3.
        /// </summary>
        [JsonProperty("leakRate3")]
        public double? LeakRate3 { get; set; }

        /// <summary>
        /// SIR2 leak rate from method 4.
        /// </summary>
        [JsonProperty("leakRate4")]
        public double? LeakRate4 { get; set; }

        /// <summary>
        /// SIR2 leak rate from method 5.
        /// </summary>
        [JsonProperty("leakRate5")]
        public double? LeakRate5 { get; set; }

        /// <summary>
        /// SIR2 decision.
        /// </summary>
        [JsonProperty("decision")]
        public int? Decision { get; set; }

        /// <summary>
        /// Decision date.
        /// </summary>
        [JsonProperty("decisionDate")]
        public long? DecisionDate { get; set; }

        /// <summary>
        /// Current offset for decision.
        /// </summary>
        [JsonProperty("sir2Offset")]
        public double? Sir2Offset { get; set; }

        /// <summary>
        /// Cause of inconclusive decision.
        /// </summary>
        [JsonProperty("inconclusiveCause")]
        public int? InconclusiveCause { get; set; }

        /// <summary>
        /// Current outlier threshold.
        /// </summary>
        [JsonProperty("outlierThreshold")]
        public double? OutlierThreshold { get; set; }

        /// <summary>
        /// True if method was used to get result for main leak rate.
        /// </summary>
        [JsonProperty("method1")]
        public bool? Method1 { get; set; }

        /// <summary>
        /// True if method was used to get result for main leak rate.
        /// </summary>
        [JsonProperty("method2")]
        public bool? Method2 { get; set; }

        /// <summary>
        /// True if method was used to get result for main leak rate.
        /// </summary>
        [JsonProperty("method3")]
        public bool? Method3 { get; set; }

        /// <summary>
        /// True if method was used to get result for main leak rate.
        /// </summary>
        [JsonProperty("method4")]
        public bool? Method4 { get; set; }

        /// <summary>
        /// True if method was used to get result for main leak rate.
        /// </summary>
        [JsonProperty("method5")]
        public bool? Method5 { get; set; }

        /// <summary>
        /// HTML SIR2 report link.
        /// </summary>
        [JsonProperty("reportLink")]
        public string ReportLink { get; set; }

        /// <summary>
        /// Minimum Detectable Leak Rate.
        /// </summary>
        [JsonProperty("mdlr")]
        public double? Mdlr { get; set; }

        /// <summary>
        /// Alarm PLIP id for decision.
        /// </summary>
        [JsonProperty("alarmPlipId")]
        public long? AlarmPlipId { get; set; }

        /// <summary>
        /// Alarm issue added by operator for this decision.
        /// </summary>
        [JsonProperty("alarmIssueId")]
        public long? AlarmIssueId { get; set; }

        /// <summary>
        /// Operator decision for SIR2 decision.
        /// </summary>
        [JsonProperty("alarmOperatorDecision")]
        public int? AlarmOperatorDecision { get; set; }

        /// <summary>
        /// Operator description for his decision about SIR2 result.
        /// </summary>
        [JsonProperty("alarmOperatorDecisionDescr")]
        public string AlarmOperatorDecisionDescr { get; set; }
        
        /// <summary>
        /// Date of operator decision.
        /// </summary>
        [JsonProperty("alarmOperatorDecisionTime")]
        public long? AlarmOperatorDecisionTime { get; set; }

        /// <summary>
        /// Id operator that set decision.
        /// </summary>
        [JsonProperty("alarmOperatorDecisionId")]
        public int? AlarmOperatorDecisionId { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? RangeStartDateUTC { get { return RangeStartDate.HasValue ? DateTimer.GetDtUtcFromMsUtc(RangeStartDate) : null; } }
        [JsonIgnore]
        public DateTime? RangeStartDateLocal { get { return RangeStartDate.HasValue ? DateTimer.GetDtLocalFromMsUtc(RangeStartDate) : null; } }

        [JsonIgnore]
        public DateTime? RangeEndDateUTC { get { return RangeEndDate.HasValue ? DateTimer.GetDtUtcFromMsUtc(RangeEndDate) : null; } }
        [JsonIgnore]
        public DateTime? RangeEndDateLocal { get { return RangeEndDate.HasValue ? DateTimer.GetDtLocalFromMsUtc(RangeEndDate) : null; } }

        [JsonIgnore]
        public DateTime? DecisionDateUTC { get { return DecisionDate.HasValue ? DateTimer.GetDtUtcFromMsUtc(DecisionDate) : null; } }
        [JsonIgnore]
        public DateTime? DecisionDateLocal { get { return DecisionDate.HasValue ? DateTimer.GetDtLocalFromMsUtc(DecisionDate) : null; } }

        [JsonIgnore]
        public DateTime? AlarmOperatorDecisionTimeUTC { get { return AlarmOperatorDecisionTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(AlarmOperatorDecisionTime) : null; } }
        [JsonIgnore]
        public DateTime? AlarmOperatorDecisionTimeLocal { get { return AlarmOperatorDecisionTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(AlarmOperatorDecisionTime) : null; } }
        #endregion

        #region Methods

        #region ToOpDataTemporal
        public static OpDataTemporal ToOpDataTemporal(long idDataType, FuelBalanceSir fbSir, object value, int index = 0)
        {
            var opDataTemporal = new OpDataTemporal()
            {
                //IdDataTemporal = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0),                
                IdDataTemporal = CreateIdDataTemporal(fbSir, idDataType, index, value),
                IdLocation = fbSir.LocationId,
                IdMeter = fbSir.TankId,
                StartTime = DateTimer.GetDtLocalFromMsUtc(fbSir.RangeStartDate),
                EndTime = DateTimer.GetDtLocalFromMsUtc(fbSir.RangeEndDate),
                IdDataSourceType = (int)Common.Enums.DataSourceType.SIR2,
                IdDataType = idDataType,
                IdAggregationType = (int)Common.Enums.AggregationType.Month,
                IdDataSource = fbSir.Id, // IdDatasource jest wykorzystywany jako jednoznaczy identyfikator całej analizy
                IndexNbr = index,
                Value = value,
                DataStatus = Common.Enums.DataStatus.Normal
            };
            
            return opDataTemporal;
        }
        #endregion

        #region ToListOpDataTemporal
        public static List<OpDataTemporal> ToListOpDataTemporal(IDataProvider dataProvider, List<FuelBalanceSir> fuelBalancesSir)
        {
            List<OpDataTemporal> list = new List<OpDataTemporal>();

            foreach (var fuelBalanceSir in fuelBalancesSir)
            {
                if (fuelBalanceSir == null) continue;

                if (fuelBalanceSir.AlarmIssueId.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_ISSUE_ID, fuelBalanceSir, fuelBalanceSir.AlarmIssueId));
                if (fuelBalanceSir.AlarmOperatorDecision.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_OPERATOR_DECISION, fuelBalanceSir, fuelBalanceSir.AlarmOperatorDecision));
                if (!string.IsNullOrEmpty(fuelBalanceSir.AlarmOperatorDecisionDescr))
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_OPERATOR_DECISION_DESCR, fuelBalanceSir, fuelBalanceSir.AlarmOperatorDecisionDescr));
                if (fuelBalanceSir.AlarmOperatorDecisionTime.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_OPERATOR_DECISION_TIME, fuelBalanceSir, DateTimer.GetDtLocalFromMsUtc(fuelBalanceSir.AlarmOperatorDecisionTime)));
                if (fuelBalanceSir.AlarmOperatorDecisionId.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_OPERATOR_ID, fuelBalanceSir, fuelBalanceSir.AlarmOperatorDecisionId));
                if (fuelBalanceSir.AlarmPlipId.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_PROBLEM_ALARM_ID, fuelBalanceSir, fuelBalanceSir.AlarmPlipId));

                if (fuelBalanceSir.Decision.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_DECISION, fuelBalanceSir, fuelBalanceSir.Decision));
                if (fuelBalanceSir.DecisionDate.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_DECISION_DATE, fuelBalanceSir, DateTimer.GetDtLocalFromMsUtc(fuelBalanceSir.DecisionDate)));
                if (fuelBalanceSir.InconclusiveCause.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_INCONCLUSIVE_CAUSE, fuelBalanceSir, fuelBalanceSir.InconclusiveCause));
                if (fuelBalanceSir.LeakRate0.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate0, 0));
                if (fuelBalanceSir.LeakRate1.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate1, 1));
                if (fuelBalanceSir.LeakRate2.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate2, 2));
                if (fuelBalanceSir.LeakRate3.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate3, 3));
                if (fuelBalanceSir.LeakRate4.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate4, 4));
                if (fuelBalanceSir.LeakRate5.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_LEAK_RATE, fuelBalanceSir, fuelBalanceSir.LeakRate5, 5));
                if (fuelBalanceSir.Mdlr.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_MDLR, fuelBalanceSir, fuelBalanceSir.Mdlr));
                if (fuelBalanceSir.Method1.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_METHOD_USED, fuelBalanceSir, fuelBalanceSir.Method1, 1));
                if (fuelBalanceSir.Method2.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_METHOD_USED, fuelBalanceSir, fuelBalanceSir.Method2, 2));
                if (fuelBalanceSir.Method3.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_METHOD_USED, fuelBalanceSir, fuelBalanceSir.Method3, 3));
                if (fuelBalanceSir.Method4.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_METHOD_USED, fuelBalanceSir, fuelBalanceSir.Method4, 4));
                if (fuelBalanceSir.Method5.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_METHOD_USED, fuelBalanceSir, fuelBalanceSir.Method5, 5));
                if (fuelBalanceSir.Sir2Offset.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_OFFSET, fuelBalanceSir, fuelBalanceSir.Sir2Offset));
                if (fuelBalanceSir.OutlierThreshold.HasValue)
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD, fuelBalanceSir, fuelBalanceSir.OutlierThreshold));
                if (!string.IsNullOrEmpty(fuelBalanceSir.ReportLink))
                    list.Add(ToOpDataTemporal(DataType.AWSR_IRC_SIR2_REPORT, fuelBalanceSir, fuelBalanceSir.ReportLink));
            }

            return list;
        }
        #endregion

        #region ToFuelBalanceSir
        public static FuelBalanceSir ToFuelBalanceSir(List<OpDataTemporal> sir2DataTemporals)
        {
            OpDataTemporal temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_DECISION_DATE); ;
            if (temporal != null && temporal.IdLocation.HasValue && temporal.IdMeter.HasValue)
            {
                FuelBalanceSir fuelBalanceSir = new FuelBalanceSir()
                {
                    LocationId = temporal.IdLocation.Value,
                    TankId = temporal.IdMeter.Value,
                    RangeEndDate = temporal.EndTime.HasValue ? DateTimer.GetMsUtcFromDtUtc(temporal.EndTime) : null,
                    RangeStartDate = temporal.StartTime.HasValue ? DateTimer.GetMsUtcFromDtUtc(temporal.StartTime) : null
                };

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_ISSUE_ID);
                fuelBalanceSir.AlarmIssueId = temporal != null ? temporal.ValueLong : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION);
                fuelBalanceSir.AlarmOperatorDecision = temporal != null ? temporal.ValueInt : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION_DESCR);
                fuelBalanceSir.AlarmOperatorDecisionDescr = temporal != null ? temporal.ValueString : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION_TIME);
                fuelBalanceSir.AlarmOperatorDecisionTime = temporal != null && temporal.ValueDateTime.HasValue ? (long?)DateTimer.GetMsUtcFromDtLocal(temporal.ValueDateTime.Value) : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_ID);
                fuelBalanceSir.AlarmOperatorDecisionId = temporal != null ? temporal.ValueInt : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_ALARM_ID);
                fuelBalanceSir.AlarmPlipId = temporal != null ? temporal.ValueLong : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_DECISION);
                fuelBalanceSir.Decision = temporal != null ? temporal.ValueInt : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_DECISION_DATE);
                fuelBalanceSir.DecisionDate = temporal != null && temporal.ValueDateTime.HasValue ? (long?)DateTimer.GetMsUtcFromDtLocal(temporal.ValueDateTime.Value) : null; // todo
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_INCONCLUSIVE_CAUSE);
                fuelBalanceSir.InconclusiveCause = temporal != null ? temporal.ValueInt : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 0);
                fuelBalanceSir.LeakRate0 = temporal != null ? temporal.ValueDouble : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 1);
                fuelBalanceSir.LeakRate1 = temporal != null ? temporal.ValueDouble : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 2);
                fuelBalanceSir.LeakRate2 = temporal != null ? temporal.ValueDouble : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 3);
                fuelBalanceSir.LeakRate3 = temporal != null ? temporal.ValueDouble : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 4);
                fuelBalanceSir.LeakRate4 = temporal != null ? temporal.ValueDouble : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_LEAK_RATE && d.IndexNbr == 5);
                fuelBalanceSir.LeakRate5 = temporal != null ? temporal.ValueDouble : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_MDLR);
                fuelBalanceSir.Mdlr = temporal != null ? temporal.ValueDouble : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_METHOD_USED && d.IndexNbr == 1);
                fuelBalanceSir.Method1 = temporal != null ? temporal.ValueBool : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_METHOD_USED && d.IndexNbr == 2);
                fuelBalanceSir.Method2 = temporal != null ? temporal.ValueBool : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_METHOD_USED && d.IndexNbr == 3);
                fuelBalanceSir.Method3 = temporal != null ? temporal.ValueBool : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_METHOD_USED && d.IndexNbr == 4);
                fuelBalanceSir.Method4 = temporal != null ? temporal.ValueBool : null;
                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_METHOD_USED && d.IndexNbr == 5);
                fuelBalanceSir.Method5 = temporal != null ? temporal.ValueBool : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_OFFSET);
                fuelBalanceSir.Sir2Offset = temporal != null ? temporal.ValueDouble : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD);
                fuelBalanceSir.OutlierThreshold = temporal != null ? temporal.ValueDouble : null;

                temporal = sir2DataTemporals.FirstOrDefault(d => d.IdDataType == DataType.AWSR_IRC_SIR2_REPORT);
                fuelBalanceSir.ReportLink = temporal != null ? temporal.ValueString : null;

                return fuelBalanceSir;
            }

            return null;
        }
        #endregion

        #region UpdateOperatorDecision
        public void UpdateOperatorDecision(IEnumerable<OpDataTemporal> operatorDecisionData)
        {
            OpDataTemporal temporal = null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION);
            this.AlarmOperatorDecision = temporal != null ? temporal.ValueInt : null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION_DESCR);
            this.AlarmOperatorDecisionDescr = temporal != null ? temporal.ValueString : null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_DECISION_TIME);
            this.AlarmOperatorDecisionTime = temporal != null ? DateTimer.GetMsUtcFromDtLocal(temporal.ValueDateTime) : null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_OPERATOR_ID);
            this.AlarmOperatorDecisionId = temporal != null ? temporal.ValueInt : null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_ALARM_ID);
            this.AlarmPlipId = temporal != null ? temporal.ValueLong : null;

            temporal = operatorDecisionData.FirstOrDefault(d => d.IdDataType == DataType.AWSR_PROBLEM_ISSUE_ID);
            this.AlarmIssueId = temporal != null ? temporal.ValueLong : null;
        }
        #endregion

        #region CreateIdDataTemporal
        private static long CreateIdDataTemporal(FuelBalanceSir fbObj, long idDataType, int index, object value)
        {
            unchecked
            {
                long hash = 2166136261;
                hash = (hash * 16777619) ^ fbObj.Id;
                hash = (hash * 16777619) ^ idDataType;
                hash = (hash * 16777619) ^ index;                
                return hash;
            }
        }

        //private static long CreateIdDataTemporal(FuelBalanceSir fbObj, long idDataType, int index, object value)
        //{
            //unchecked
            //{
            //    long hash = 2166136261;
            //    hash = (hash * 16777619) ^ fbObj.GetHashCode();
            //    hash = (hash * 16777619) ^ idDataType.GetHashCode();
            //    hash = (hash * 16777619) ^ index.GetHashCode();
            //    hash = value != null ? (hash * 29) ^ value.GetHashCode() : (hash * 29);

            //    return hash;
            //}
        //}
        #endregion

        #endregion
    }
    #endregion

    #region FuelBalanceGainLoss
    public class FuelBalanceGainLoss
    {
        #region Properties
        public long Id { get; set; }
        public long? StartTime { get; set; }
        public long? EndTime { get; set; }
        public long TankId { get; set; }
        public long LocationId { get; set; }
        public double? TotalVolume { get; set; }
        public double? DeliveryLoadingTemperature { get; set; }
        public double? DeliveryLoadingDiscrepancy { get; set; }
        public double? DeliveryTransportTemperature { get; set; }
        public double? DeliveryTransportDiscrepancy { get; set; }
        public double? DeliveryTankCalibrationMin { get; set; }
        public double? DeliveryTankCalibrationMax { get; set; }
        public double? DeliveryDropDiscrepancy { get; set; }
        public double? TankTemperature { get; set; }
        public double? TankDiscrepancy { get; set; }
        public double? SalesTemperature { get; set; }
        public double? TankSalesCalibrationMin { get; set; }
        public double? TankSalesCalibrationMax { get; set; }
        public double? SalesTankDiscrepancy { get; set; }
        public double? SalesNozzles { get; set; }
        public double? SalesPos { get; set; }
        public double? TerminalFuelVirtVolumeDispensed { get; set; }
        public double? DispensedFuelVirtVolumeDelivered { get; set; }
        public double? DispensedFuelVirtTemperature { get; set; }
        
        List<FuelBalanceGainLossProblem> Problems { get; set; }

        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }

        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }
        #endregion

        #region Methods
        #region ToOpDataTemporal
        protected OpDataTemporal ToOpDataTemporal(long idDataType, FuelBalanceGainLoss fpGl, object value, int index = 0)
        {
            var opDataTemporal = new OpDataTemporal()
            {
                //IdDataTemporal = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0),
                IdDataTemporal = CreateIdDataTemporal(fpGl, idDataType, index, value),
                IdLocation = fpGl.LocationId,
                IdMeter = fpGl.TankId,
                StartTime = DateTimer.GetDtLocalFromMsUtc(fpGl.StartTime),
                EndTime = DateTimer.GetDtLocalFromMsUtc(fpGl.EndTime),
                IdDataSourceType = (int)Common.Enums.DataSourceType.GainLoss,
                IdDataType = idDataType,
                IdAggregationType = (int)Common.Enums.AggregationType.Day, 
                IndexNbr = index,
                Value = value,
                DataStatus = Common.Enums.DataStatus.Normal
            };
            return opDataTemporal;
        }
        #endregion

        #region ToListOpDataTemporal
        public List<OpDataTemporal> ToListOpDataTemporal(FuelBalanceGainLoss fpGl)
        {
            var list = new List<OpDataTemporal>();
            if (fpGl == null) return list;

            if (fpGl.DeliveryDropDiscrepancy.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_DROP_DISCREPANCY, fpGl, fpGl.DeliveryDropDiscrepancy.Value));
            if (fpGl.DeliveryLoadingDiscrepancy.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_LOADING_DISCREPANCY, fpGl, fpGl.DeliveryLoadingDiscrepancy.Value));
            if (fpGl.DeliveryLoadingTemperature.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_LOADING_TEMPERATURE, fpGl, fpGl.DeliveryLoadingTemperature.Value));
            if (fpGl.DeliveryTankCalibrationMax.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_TANK_CALIBRATION_MAX, fpGl, fpGl.DeliveryTankCalibrationMax.Value));
            if (fpGl.DeliveryTankCalibrationMin.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_TANK_CALIBRATION_MIN, fpGl, fpGl.DeliveryTankCalibrationMin.Value));
            if (fpGl.DeliveryTransportDiscrepancy.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_TRANSPORT_DISCREPANCY, fpGl, fpGl.DeliveryTransportDiscrepancy.Value));
            if (fpGl.DeliveryTransportTemperature.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_DELIVERY_TRANSPORT_TEMPERATURE, fpGl, fpGl.DeliveryTransportTemperature.Value));
            if (fpGl.DispensedFuelVirtTemperature.HasValue)
                list.Add(ToOpDataTemporal(DataType.AWSR_DISPENSED_FUEL_VIRT_TEMPERATURE, fpGl, fpGl.DispensedFuelVirtTemperature.Value));
            if (fpGl.DispensedFuelVirtVolumeDelivered.HasValue)
                list.Add(ToOpDataTemporal(DataType.AWSR_DISPENSED_FUEL_VIRT_VOLUME_DELIVERED, fpGl, fpGl.DispensedFuelVirtVolumeDelivered.Value));
            if (fpGl.SalesNozzles.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_SALES_NOZZLES, fpGl, fpGl.SalesNozzles.Value));
            if (fpGl.SalesPos.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_SALES_POS, fpGl, fpGl.SalesPos.Value));
            if (fpGl.SalesTankDiscrepancy.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_SALES_TANK_DISCREPANCY, fpGl, fpGl.SalesTankDiscrepancy.Value));
            if (fpGl.SalesTemperature.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_SALES_TEMPERATURE, fpGl, fpGl.SalesTemperature.Value));
            if (fpGl.TankDiscrepancy.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_TANK_DISCREPANCY, fpGl, fpGl.TankDiscrepancy.Value));
            if (fpGl.TankSalesCalibrationMax.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_TANK_SALES_CALIBRATION_MAX, fpGl, fpGl.TankSalesCalibrationMax.Value));
            if (fpGl.TankSalesCalibrationMin.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_TANK_SALES_CALIBRATION_MIN, fpGl, fpGl.TankSalesCalibrationMin.Value));
            if (fpGl.TankTemperature.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_TANK_TEMPERATURE, fpGl, fpGl.TankTemperature.Value));
            if (fpGl.TotalVolume.HasValue)
                list.Add(ToOpDataTemporal(DataType.GL_TOTAL_VOLUME, fpGl, fpGl.TotalVolume.Value));
            
            return list;
        }

        public List<OpDataTemporal> ToListOpDataTemporal(List<FuelBalanceGainLoss> fpGlList)
        {
            return fpGlList.SelectMany(gl => ToListOpDataTemporal(gl)).ToList();
        }
        #endregion

        #region CreateIdDataTemporal
        private static long CreateIdDataTemporal(FuelBalanceGainLoss fbObj, long idDataType, int index, object value)
        {
            unchecked
            {
                long hash = 2166136261;
                hash = (hash * 16777619) ^ fbObj.Id;
                hash = (hash * 16777619) ^ idDataType;
                hash = (hash * 16777619) ^ index;
                return hash;
            }
        }

        //private static long CreateIdDataTemporal(FuelBalanceGainLoss fbObj, long idDataType, int index, object value)
        //{
        //    unchecked
        //    {
        //        long hash = 2166136261;
        //        hash = (hash * 16777619) ^ fbObj.GetHashCode();
        //        hash = (hash * 16777619) ^ idDataType.GetHashCode();
        //        hash = (hash * 16777619) ^ index.GetHashCode();
        //        hash = value != null ? (hash * 29) ^ value.GetHashCode() : (hash * 29);

        //        return hash;
        //    }
        //}
        #endregion

        #endregion
    }
    #endregion

    #region FuelBalanceGainLossProblem
    public class FuelBalanceGainLossProblem
    {
        #region Properties
        public long Id { get; set; }
        public long FuelBalanceGainLossId { get; set; }
        public int ProblemId { get; set; }
        public string Description { get; set; }
        public long? Time { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? TimeUTC { get { return Time.HasValue ? DateTimer.GetDtUtcFromMsUtc(Time) : null; } }
        [JsonIgnore]
        public DateTime? TimeLocal { get { return Time.HasValue ? DateTimer.GetDtLocalFromMsUtc(Time) : null; } }
        #endregion

        #region Methods

        #endregion
    }
    #endregion
}