﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    public class Operator
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("surname")]
        public string Surname { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("postCode")]
        public string PostCode { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        #endregion

    }
}
