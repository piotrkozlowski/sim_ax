﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    public class TemperatureCoefficient
    {

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("temperature")]
        public double? Temperature { get; set; }

        [JsonProperty("density")]
        public double? Density { get; set; }

        [JsonProperty("value")]
        public double? Value { get; set; }
    }
}
