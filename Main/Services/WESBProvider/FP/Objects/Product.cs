﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Product
    public class Product
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("distributorId")]
        public long? DistributorId { get; set; }
        [JsonProperty("productName")]
        public string ProductName { get; set; }
        [JsonProperty("productType")]
        public string ProductType { get; set; }
        #endregion
    }
    #endregion
}
