﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    public class ParameterDefinition
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("paramType")]
        public string ParamType { get; set; }

        [JsonProperty("parameterValues")]
        public IList<ParameterValue> ParameterValues { get; set; }
    }

    public class ParameterValue
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("date")]
        public long? Date { get; set; }

        [JsonProperty("noValue")]
        public double? NoValue { get; set; }

        [JsonProperty("textValue")]
        public string TextValue { get; set; }

        [JsonProperty("locationId")]
        public long LocationId { get; set; }

        [JsonProperty("parameterDefinitionId")]
        public long ParameterDefinitionId { get; set; }
    }
}
