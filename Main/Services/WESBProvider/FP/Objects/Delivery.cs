﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.DW;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom.Metadata;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Terminal
    public class Terminal
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("terminalName")]
        public string TerminalName { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("orderView")]
        public int? OrderView { get; set; }
        #endregion
    }
    #endregion

    #region Carrier
    public class Carrier
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("carrierName")]
        public string CarrierName { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("orderView")]
        public int? OrderView { get; set; }
        #endregion
    }
    #endregion

    #region Driver
    public class Driver
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("driverName")]
        public string DriverName { get; set; }

        [JsonProperty("carrierId")]
        public long? CarrierId { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("orderView")]
        public int? OrderView { get; set; }
        #endregion
    }
    #endregion

    #region TruckTrailer
    public class TruckTrailer
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("registrationNo")]
        public string RegistrationNo { get; set; }

        [JsonProperty("carrierId")]
        public long? CarrierId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("orderView")]
        public int? OrderView { get; set; }
        #endregion
    }
    #endregion

    #region AtgRefuel
    public class AtgRefuel
    {
        #region Properties

        [JsonProperty("atgRefuelId")]
        public long? AtgRefuelId { get; set; }

        [JsonProperty("atgTempAfterDelivery")]
        public double? AtgTempAfterDelivery { get; set; }

        [JsonProperty("atgTempBeforeDelivery")]
        public double? AtgTempBeforeDelivery { get; set; }

        [JsonProperty("atgVolume15cAfterDelivery")]
        public double? AtgVolume15cAfterDelivery { get; set; }

        [JsonProperty("atgVolume15cBeforeDelivery")]
        public double? AtgVolume15cBeforeDelivery { get; set; }

        [JsonProperty("atgVolumeAfterDelivery")]
        public double? AtgVolumeAfterDelivery { get; set; }

        [JsonProperty("atgVolumeBeforeDelivery")]
        public double? AtgVolumeBeforeDelivery { get; set; }

        [JsonProperty("volume")]
        public double? Volume { get; set; }

        [JsonProperty("volume15c")]
        public double? Volume15c { get; set; }

        [JsonProperty("dropDate")]
        public long? DropDate { get; set; }

        [JsonProperty("endTime")]
        public long? EndTime { get; set; }

        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("tankId")]
        public long? TankId { get; set; }

        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }

        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }
        #endregion

        #region ToOpRefuel
        public static OpRefuel ToOpRefuel(AtgRefuel refuel, int idDeliverySourceType, string relatedId,
            List<Sale> saleDuringDelivery,
            List<Tank> tanks,
            IDataProvider dataProvider = null,
            long? idLocation = null,
            bool loadCustomData = true, List<long> customDataTypes = null)
        {
            if (refuel == null) return null;

            OpRefuel opRefuel = new OpRefuel()
            {
                IdRefuel = refuel.AtgRefuelId ?? -1,
                IdMeter = refuel.TankId,
                StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
            };

            if (dataProvider != null)
            {
                if (refuel.TankId.HasValue) opRefuel.Meter = dataProvider.GetMeter(refuel.TankId.Value);
                if (idLocation.HasValue) opRefuel.Location = dataProvider.GetLocation(idLocation.Value);
            }

            opRefuel.DataSource = new OpDataSource() { IdDataSourceType = idDeliverySourceType };

            if (loadCustomData)
            {
                Dictionary<long, long> dataTypes = customDataTypes != null ? customDataTypes.Distinct().ToDictionary(q => q) : new Dictionary<long, long>()
                    {
                        { DataType.REFUEL_TOTAL_VOLUME, DataType.REFUEL_TOTAL_VOLUME },
                        { DataType.REFUEL_TOTAL_REFERENCE_VOLUME, DataType.REFUEL_TOTAL_REFERENCE_VOLUME },
                        { DataType.REFUEL_START_TEMPERATURE, DataType.REFUEL_START_TEMPERATURE },
                        { DataType.REFUEL_END_TEMPERATURE, DataType.REFUEL_END_TEMPERATURE },
                        { DataType.REFUEL_START_VOLUME, DataType.REFUEL_START_VOLUME },
                        { DataType.REFUEL_END_VOLUME, DataType.REFUEL_END_VOLUME },
                        { DataType.REFUEL_START_REF_VOLUME, DataType.REFUEL_START_REF_VOLUME },
                        { DataType.REFUEL_END_REF_VOLUME, DataType.REFUEL_END_REF_VOLUME },
                        { DataType.REFUEL_START_TIME, DataType.REFUEL_START_TIME },
                        { DataType.REFUEL_END_TIME, DataType.REFUEL_END_TIME },
                        { DataType.REFUEL_SOLD_VOLUME_GROSS, DataType.REFUEL_SOLD_VOLUME_GROSS },
                        { DataType.REFUEL_SOLD_VOLUME_NET, DataType.REFUEL_SOLD_VOLUME_NET },
                        { DataType.REFUEL_SOLD_VOLUME_START, DataType.REFUEL_SOLD_VOLUME_START },
                        { DataType.REFUEL_SOLD_VOLUME_END, DataType.REFUEL_SOLD_VOLUME_END }
                    };

                // Wartości sprzedaży w trakcie dostawy automatycznej.
                // Wartości te przychodzą na cały produkt więc trzeba wyliczyć wartość per zbiornik.
                double? meterSaleGross = null;
                double? meterSaleNet = null;
                double? meterSaleStart = null;
                double? meterSaleEnd = null;

                if (tanks != null && refuel.TankId.HasValue
                    && (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_GROSS) || dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_NET)
                    || dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_START) || dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_END)))
                {
                    // Ile jest zbiornikow w tym produkcie na ktory przyszedł zrzut?
                    Dictionary<long, List<Tank>> tanksPerProduct = tanks.Where(q => q.ProductId.HasValue).GroupBy(t => t.ProductId.Value).ToDictionary(q => q.Key, q => q.ToList());
                    // Potrzeba wyszukac zbionik, zeby zobaczyc jaki to jest produkt
                    Tank tank = tanks.FirstOrDefault(t => t.Id.HasValue && t.Id.Value == refuel.TankId.Value);
                    if (tank != null && tank.ProductId.HasValue)
                    {
                        Sale sale = saleDuringDelivery.FirstOrDefault(s => s.ProductId.HasValue
                            && s.ProductId.Value == tank.ProductId.Value && s.StartTime == refuel.StartTime
                            && s.EndTime == refuel.EndTime);
                        List<Tank> tanksOnThatProduct = tanksPerProduct.ContainsKey(tank.ProductId.Value) ? tanksPerProduct[tank.ProductId.Value] : null;
                        if (sale != null && tanksOnThatProduct != null)
                        {
                            // Na ile zbiornikow sprzedaż powinna zostać rozdzielona
                            int tankCount = tanksOnThatProduct.Count;
                            if (tankCount != 0)
                            {
                                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_GROSS))
                                {
                                    // Sprzedaż gorss per product
                                    double productSaleGross = sale.PosVolumeDispenced ?? 0;
                                    if (productSaleGross != 0) meterSaleGross = productSaleGross / tankCount;
                                }
                                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_NET))
                                {
                                    // Sprzedaż net per product 
                                    double productSaleNet = sale.PosVolumeDispenced15c ?? 0;
                                    if (productSaleNet != 0) meterSaleNet = productSaleNet / tankCount;
                                }
                                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_START))
                                {
                                    // Wartość początkowa sprzedazy
                                    double productSaleStart = sale.StartState ?? 0;
                                    if (productSaleStart != 0) meterSaleStart = productSaleStart / tankCount;
                                }
                                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_END))
                                {
                                    // Wartość koncowa sprzedazy
                                    double productSaleEnd = sale.EndState ?? 0;
                                    if (productSaleEnd != 0) meterSaleEnd = productSaleEnd / tankCount;
                                }
                            }
                        }
                    }

                }

                if (dataTypes.ContainsKey(DataType.REFUEL_TOTAL_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Volume / 1000, IdDataType = DataType.REFUEL_TOTAL_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_TOTAL_REFERENCE_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Volume15c / 1000, IdDataType = DataType.REFUEL_TOTAL_REFERENCE_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_START_TEMPERATURE))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgTempBeforeDelivery, IdDataType = DataType.REFUEL_START_TEMPERATURE });
                if (dataTypes.ContainsKey(DataType.REFUEL_END_TEMPERATURE))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgTempAfterDelivery, IdDataType = DataType.REFUEL_END_TEMPERATURE });
                if (dataTypes.ContainsKey(DataType.REFUEL_START_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgVolumeBeforeDelivery / 1000, IdDataType = DataType.REFUEL_START_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_END_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgVolumeAfterDelivery / 1000, IdDataType = DataType.REFUEL_END_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_START_REF_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgVolume15cBeforeDelivery / 1000, IdDataType = DataType.REFUEL_START_REF_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_END_REF_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = refuel.AtgVolume15cAfterDelivery / 1000, IdDataType = DataType.REFUEL_END_REF_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_START_TIME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime), IdDataType = DataType.REFUEL_START_TIME });
                if (dataTypes.ContainsKey(DataType.REFUEL_END_TIME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime), IdDataType = DataType.REFUEL_END_TIME });
                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_GROSS))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = meterSaleGross / 1000, IdDataType = DataType.REFUEL_SOLD_VOLUME_GROSS });
                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_NET))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = meterSaleNet / 1000, IdDataType = DataType.REFUEL_SOLD_VOLUME_NET });
                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_START))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = meterSaleStart / 1000, IdDataType = DataType.REFUEL_SOLD_VOLUME_START });
                if (dataTypes.ContainsKey(DataType.REFUEL_SOLD_VOLUME_END))
                    opRefuel.DataList.Add(new OpRefuelData { IdRefuel = opRefuel.IdRefuel, Value = meterSaleEnd / 1000, IdDataType = DataType.REFUEL_SOLD_VOLUME_END });
            }

            opRefuel.DataList.ForEach(d => d.IdRefuelData = DeliveryUtils.CreateRefuelDataId(d.IdRefuel, d.IdDataType, d.Index));

            opRefuel.DynamicProperties.AddProperty(new OpDynamicProperty(MdDynamicProperty.RefuelRelatedId, relatedId));

            return opRefuel;
        }
        #endregion

        #region ToFpAtgRefuel
        public static AtgRefuel ToFpAtgRefuel(OpRefuel opRefuel)
        {
            if (opRefuel == null) return null;

            AtgRefuel fpRefuel = new AtgRefuel()
            {
                AtgRefuelId = (int)opRefuel.IdRefuel,
                StartTime = DateTimer.GetMsUtcFromDtLocal(opRefuel.StartTime),
                EndTime = DateTimer.GetMsUtcFromDtLocal(opRefuel.EndTime),
                TankId = opRefuel.IdMeter.HasValue ? (int?)opRefuel.IdMeter.Value : null
            };

            var refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_TOTAL_VOLUME);
            fpRefuel.Volume = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_TOTAL_REFERENCE_VOLUME);
            fpRefuel.Volume15c = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_END_TEMPERATURE);
            fpRefuel.AtgTempAfterDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_START_TEMPERATURE);
            fpRefuel.AtgTempBeforeDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_END_REF_VOLUME);
            fpRefuel.AtgVolume15cAfterDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_START_REF_VOLUME);
            fpRefuel.AtgVolume15cBeforeDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_END_VOLUME);
            fpRefuel.AtgVolumeAfterDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_START_VOLUME);
            fpRefuel.AtgVolumeBeforeDelivery = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            return fpRefuel;
        }
        #endregion
    }
    #endregion

    #region Refuel
    public class Refuel
    {
        #region Properties

        [JsonProperty("refuelId")]
        public long? RefuelId { get; set; }

        [JsonProperty("dropType")]
        public string DropType { get; set; }

        [JsonProperty("orderNumber")]
        public string OrderNumber { get; set; }

        [JsonProperty("temperature")]
        public double? Temperature { get; set; }

        [JsonProperty("volume")]
        public double? Volume { get; set; }

        [JsonProperty("volume15c")]
        public double? Volume15c { get; set; }

        [JsonProperty("weight")]
        public double? Weight { get; set; }

        [JsonProperty("endTime")]
        public long? EndTime { get; set; }

        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("deliveryDate")]
        public long? DeliveryDate { get; set; }

        [JsonProperty("compartment")]
        public long? Compartment { get; set; }

        [JsonProperty("density")]
        public double? Density { get; set; }

        [JsonProperty("tankId")]
        public long? TankId { get; set; }

        [JsonProperty("productId")]
        public long? ProductId { get; set; }

        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }

        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }

        [JsonIgnore]
        public DateTime? DeliveryDateUTC { get { return EndTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(DeliveryDate) : null; } }
        [JsonIgnore]
        public DateTime? DeliveryDateLocal { get { return EndTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(DeliveryDate) : null; } }
        #endregion

        #region ToOpRefuel
        public static OpRefuel ToOpRefuel(Refuel refuel, int? idDeliverySourceType, string relatedId, IDataProvider dataProvider = null, long? idLocation = null, 
            bool loadCustomData = true, List<long> customDataTypes = null)
        {
            if (refuel == null) return null;

            OpRefuel opRefuel = new OpRefuel()
            {
                IdRefuel = refuel.RefuelId.HasValue ? refuel.RefuelId.Value : 0,
                IdMeter = refuel.TankId,
                StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime.Value),
                EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime.Value),
            };

            if (dataProvider != null)
            {
                if (refuel.TankId.HasValue) opRefuel.Meter = dataProvider.GetMeter(refuel.TankId.Value);
                if (idLocation.HasValue) opRefuel.Location = dataProvider.GetLocation(idLocation.Value);
            }

            opRefuel.DataSource = idDeliverySourceType.HasValue ?
                new OpDataSource() { IdDataSourceType = idDeliverySourceType.Value } :
                new OpDataSource() { IdDataSourceType = EnumsConverter.FpDeliveryTypeToIdDataSourceType(refuel.DropType) };

            if (loadCustomData)
            {
                Dictionary<long, long> dataTypes = customDataTypes != null ? customDataTypes.Distinct().ToDictionary(q => q) : new Dictionary<long, long>()
                    {
                        { DataType.REFUEL_TOTAL_VOLUME, DataType.REFUEL_TOTAL_VOLUME },
                        { DataType.REFUEL_TOTAL_REFERENCE_VOLUME, DataType.REFUEL_TOTAL_REFERENCE_VOLUME },
                        { DataType.ATG_PARAMS_PRODUCT_CODE, DataType.ATG_PARAMS_PRODUCT_CODE },
                        { DataType.REFUEL_ORDER_NUMBER, DataType.REFUEL_ORDER_NUMBER },
                        { DataType.REFUEL_START_TIME, DataType.REFUEL_START_TIME },
                        { DataType.REFUEL_END_TIME, DataType.REFUEL_END_TIME },
                        { DataType.REFUEL_LOADING_DENSITY, DataType.REFUEL_LOADING_DENSITY },
                        { DataType.REFUEL_TOTAL_WEIGHT, DataType.REFUEL_TOTAL_WEIGHT },
                        { DataType.REFUEL_LOADING_TEMPERATURE, DataType.REFUEL_LOADING_TEMPERATURE }
                    };
                if (dataTypes.ContainsKey(DataType.REFUEL_TOTAL_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Volume / 1000, IdDataType = DataType.REFUEL_TOTAL_VOLUME });
                if (dataTypes.ContainsKey(DataType.REFUEL_TOTAL_REFERENCE_VOLUME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Volume15c / 1000, IdDataType = DataType.REFUEL_TOTAL_REFERENCE_VOLUME });
                if (dataTypes.ContainsKey(DataType.ATG_PARAMS_PRODUCT_CODE))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.ProductId, IdDataType = DataType.ATG_PARAMS_PRODUCT_CODE });
                if (dataTypes.ContainsKey(DataType.REFUEL_ORDER_NUMBER))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.OrderNumber, IdDataType = DataType.REFUEL_ORDER_NUMBER });
                if (dataTypes.ContainsKey(DataType.REFUEL_START_TIME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime), IdDataType = DataType.REFUEL_START_TIME });
                if (dataTypes.ContainsKey(DataType.REFUEL_END_TIME))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime), IdDataType = DataType.REFUEL_END_TIME });
                if (dataTypes.ContainsKey(DataType.REFUEL_LOADING_DENSITY))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Density, IdDataType = DataType.REFUEL_LOADING_DENSITY });
                if (dataTypes.ContainsKey(DataType.REFUEL_TOTAL_WEIGHT))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Weight, IdDataType = DataType.REFUEL_TOTAL_WEIGHT });
                if (dataTypes.ContainsKey(DataType.REFUEL_LOADING_TEMPERATURE))
                    opRefuel.DataList.Add(new OpRefuelData() { IdRefuel = opRefuel.IdRefuel, Value = refuel.Temperature, IdDataType = DataType.REFUEL_LOADING_TEMPERATURE });
            }

            opRefuel.DataList.ForEach(d => d.IdRefuelData = DeliveryUtils.CreateRefuelDataId(d.IdRefuel, d.IdDataType, d.Index));

            opRefuel.DynamicProperties.AddProperty(new OpDynamicProperty(MdDynamicProperty.RefuelRelatedId, relatedId));

            return opRefuel;
        }
        #endregion

        #region ToFpRefuel
        public static Refuel ToFpRefuel(OpRefuel opRefuel)
        {
            if (opRefuel == null) return null;

            Refuel fpRefuel = new Refuel()
            {
                RefuelId = (int)opRefuel.IdRefuel,
                StartTime = DateTimer.GetMsUtcFromDtLocal(opRefuel.StartTime),
                EndTime = DateTimer.GetMsUtcFromDtLocal(opRefuel.EndTime),
                TankId = opRefuel.IdMeter.HasValue ? (int?)opRefuel.IdMeter.Value : null
            };

            if (opRefuel.DataSource != null)
                fpRefuel.DropType = EnumsConverter.IdDataSourceTypeToFpDeliveryType(opRefuel.DataSource.IdDataSourceType);

            var refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_TOTAL_VOLUME);
            fpRefuel.Volume = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_TOTAL_REFERENCE_VOLUME);
            fpRefuel.Volume15c = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.ATG_PARAMS_PRODUCT_CODE);
            fpRefuel.ProductId = refuelData != null && refuelData.Value != null ? (int?)refuelData.Value : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_ORDER_NUMBER);
            fpRefuel.OrderNumber = refuelData != null && refuelData.Value != null ? refuelData.Value.ToString() : null;

            refuelData = opRefuel.DataList.FirstOrDefault(r => r.IdDataType == DataType.REFUEL_LOADING_DENSITY);
            fpRefuel.Density = refuelData != null && refuelData.Value != null ? (double?)refuelData.Value / 1000 : null;

            return fpRefuel;
        }
        #endregion
    }
    #endregion

    #region Sale
    public class Sale
    {
        #region Properties

        [JsonProperty("saleId")]
        public long? SaleId { get; set; }

        [JsonProperty("endState")]
        public double? EndState { get; set; }

        [JsonProperty("startState")]
        public double? StartState { get; set; }

        [JsonProperty("endTime")]
        public long? EndTime { get; set; }

        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("posVolumeDispenced")]
        public double? PosVolumeDispenced { get; set; }

        [JsonProperty("posVolumeDispenced15c")]
        public double? PosVolumeDispenced15c { get; set; }

        [JsonProperty("productId")]
        public long? ProductId { get; set; }

        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC
        {
            get { return DateTimer.GetDtUtcFromMsUtc(StartTime); }
            set { StartTime = DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? StartTimeLocal
        {
            get { return DateTimer.GetDtLocalFromMsUtc(StartTime); }
            set { StartTime = DateTimer.GetMsUtcFromDtLocal(value); }
        }

        [JsonIgnore]
        public DateTime? EndTimeUTC
        {
            get { return DateTimer.GetDtUtcFromMsUtc(EndTime); }
            set { EndTime = DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? EndTimeLocal
        {
            get { return DateTimer.GetDtLocalFromMsUtc(EndTime); }
            set { EndTime = DateTimer.GetMsUtcFromDtLocal(value); }
        }
        #endregion
    }
    #endregion

    #region Delivery
    public class Delivery
    {
        #region Properties

        [JsonProperty("deliveryId")]
        public long? DeliveryId { get; set; }

        [JsonProperty("canceled")]
        public bool? Canceled { get; set; }

        [JsonProperty("confirmed")]
        public bool? Confirmed { get; set; }

        [JsonProperty("deliveryDate")]
        public long? DeliveryDate { get; set; }

        [JsonProperty("deliveryNo")]
        public long? DeliveryNo { get; set; }

        [JsonProperty("deliveryType")]
        public string DeliveryType { get; set; }

        [JsonProperty("orderNumber")]
        public string OrderNumber { get; set; }

        [JsonProperty("shipmentNumber")]
        public string ShipmentNumber { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("endTime")]
        public long? EndTime { get; set; }

        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("enable")]
        public bool? Enable { get; set; }

        [JsonProperty("carrierId")]
        public long? CarrierId { get; set; }

        [JsonProperty("relatedDeliveryId")]
        public long? RelatedDeliveryId { get; set; }

        [JsonProperty("driverId")]
        public long? DriverId { get; set; }

        [JsonProperty("locationId")]
        public long? LocationId { get; set; }

        [JsonProperty("terminalId")]
        public long? TerminalId { get; set; }

        [JsonProperty("trailerId")]
        public long? TrailerId { get; set; }

        [JsonProperty("truckId")]
        public long? TruckId { get; set; }

        [JsonProperty("atgRefuels")]
        public List<AtgRefuel> AtgRefuels { get; set; }

        [JsonProperty("refuels")]
        public List<Refuel> Refuels { get; set; }

        [JsonProperty("sales")]
        public List<Sale> Sales { get; set; }

        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC
        {
            get { return DateTimer.GetDtUtcFromMsUtc(StartTime); }
            set { StartTime = DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? StartTimeLocal
        {
            get { return DateTimer.GetDtLocalFromMsUtc(StartTime); }
            set { StartTime = DateTimer.GetMsUtcFromDtLocal(value); }
        }

        [JsonIgnore]
        public DateTime? EndTimeUTC
        {
            get { return DateTimer.GetDtUtcFromMsUtc(EndTime); }
            set { EndTime = DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? EndTimeLocal
        {
            get { return DateTimer.GetDtLocalFromMsUtc(EndTime); }
            set { EndTime = DateTimer.GetMsUtcFromDtLocal(value); }
        }

        [JsonIgnore]
        public DateTime? DeliveryDateUTC
        {
            get { return DateTimer.GetDtUtcFromMsUtc(DeliveryDate); }
            set { DeliveryDate = DateTimer.GetMsUtcFromDtUtc(value); }
        }
        [JsonIgnore]
        public DateTime? DeliveryDateLocal
        {
            get { return DateTimer.GetDtLocalFromMsUtc(DeliveryDate); }
            set { DeliveryDate = DateTimer.GetMsUtcFromDtLocal(value); }
        }
        #endregion

        #region ToOpRefuelList
        public static List<OpRefuel> ToOpRefuelList(FP.WEsbProviderFP WEsbFP, List<Delivery> deliveryList, IDataProvider dataProvider,
            Dictionary<long, OpMeter> metersDict,
            Dictionary<long, Driver> drivers = null,
            Dictionary<long, Carrier> carriers = null,
            Dictionary<long, Terminal> terminals = null,
            Dictionary<long, TruckTrailer> trucksAndTrilers = null,
            List<Tank> tanks = null,
            bool loadCustomData = true, List<long> customDataTypes = null)
        {
            List<OpRefuel> refuelList = new List<OpRefuel>();
            if (deliveryList == null || dataProvider == null) return refuelList;

            if (drivers == null) drivers = WEsbFP.GetDrivers().Where(d => d.Id.HasValue).ToDictionary(k => k.Id.Value);
            if (carriers == null) carriers = WEsbFP.GetCarriers().Where(d => d.Id.HasValue).ToDictionary(k => k.Id.Value);
            if (terminals == null) terminals = WEsbFP.GetTerminals().Where(d => d.Id.HasValue).ToDictionary(k => k.Id.Value);
            if (trucksAndTrilers == null) trucksAndTrilers = WEsbFP.GetTrucksTrailers().Where(d => d.Id.HasValue).ToDictionary(k => k.Id.Value);
            if (tanks == null) tanks = WEsbFP.GetTanks(null, null).Where(d => d.Id.HasValue).ToList();

            // Przetwarzanie dostaw na refuele
            foreach (Delivery delivery in deliveryList)
            {
                refuelList.AddRange(ToOpRefuelList(WEsbFP, delivery, drivers, carriers, terminals, trucksAndTrilers, tanks, loadCustomData, customDataTypes));
            }

            // Pobieranie OpMeter dla OpRefuel. Potrzebne do wyciągnięcia produktów przy łączeniu
            if (metersDict != null)
            {
                refuelList.ForEach(r =>
                {
                    if (r != null && r.IdMeter.HasValue)
                    {
                        OpMeter meter = metersDict.TryGetValue(r.IdMeter.Value);
                        if (meter != null && meter.IdProductCode.HasValue)
                            r.DynamicProperties.AddOrUpdateProperty(UI.Business.Objects.CORE.Metadata.MdMeter.IdProductCode, meter.IdProductCode.Value);
                    }
                });
            }

            // Prawidłowe dostawy połączone zostaną obdarzone boskim GUIDem
            if (deliveryList.Count > 1)
            {
                // Tworzenie wpólnego 'GUID' dla refueli z tego samego produktu, jak w systemie IMR
                // Wstawienie spreparowanego GUIDa
                foreach (IGrouping<int, OpRefuel> refuels in refuelList
                    .Where(r => r.DynamicProperties.GetNullableValue<int>(UI.Business.Objects.CORE.Metadata.MdMeter.IdProductCode).HasValue)
                    .GroupBy(r => r.DynamicProperties.GetNullableValue<int>(UI.Business.Objects.CORE.Metadata.MdMeter.IdProductCode).Value))
                {
                    int productId = refuels.Key;
                    foreach (OpRefuel r in refuels)
                    {
                        //string relatedId = r.DataList.TryGetValue<string>(DataType.REFUEL_GUID);
                        //r.DataList.SetValue(DataType.REFUEL_GUID, string.Format("{0}-{1}", relatedId, productId));
                        if (r.DynamicProperties.ContainsKey(MdDynamicProperty.RefuelRelatedId))
                        {
                            string relatedId = r.DynamicProperties[MdDynamicProperty.RefuelRelatedId] as string;
                            if (!String.IsNullOrWhiteSpace(relatedId))
                            {
                                r.DataList.Add(new OpRefuelData()
                                {
                                    IdRefuel = r.IdRefuel,
                                    IdRefuelData = DeliveryUtils.CreateRefuelDataId(r.IdRefuel, DataType.REFUEL_GUID, 0),
                                    Index = 0,
                                    IdDataType = DataType.REFUEL_GUID,
                                    StartTime = r.StartTime,
                                    EndTime = r.EndTime,
                                    Value = String.Format("{0}-{1}", relatedId, productId)
                                });
                            }
                        }
                    }
                }
            }

            return refuelList;
        }
        #endregion

        #region ToOpRefuelList
        private static List<OpRefuel> ToOpRefuelList(WEsbProviderFP WEsbFP, Delivery delivery,
            Dictionary<long, Driver> drivers = null,
            Dictionary<long, Carrier> carriers = null,
            Dictionary<long, Terminal> terminals = null,
            Dictionary<long, TruckTrailer> trucksAndTrilers = null,
            List<Tank> tanks = null,
            bool loadCustomData = true, List<long> customDataTypes = null)
        {
            List<OpRefuel> refuelList = new List<OpRefuel>();
            if (delivery == null) return refuelList;

            if (drivers == null) drivers = WEsbFP.GetDrivers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
            if (carriers == null) carriers = WEsbFP.GetCarriers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
            if (terminals == null) terminals = WEsbFP.GetTerminals().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
            if (trucksAndTrilers == null) trucksAndTrilers = WEsbFP.GetTrucksTrailers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
            if (tanks == null) tanks = WEsbFP.GetTanks(null, null).Where(d => d.Id.HasValue).ToList();

            List<Sale> saleDuringDelivery = delivery.Sales;

            // Ustalanie related ID
            // Master nie ma RelatedDeliveryID - trzeba wybrac id Dostawy.
            long relatedDeliveryId = delivery.RelatedDeliveryId == null ? delivery.DeliveryId.GetValueOrDefault() : delivery.RelatedDeliveryId.GetValueOrDefault();

            Common.Enums.DataSourceType deliverySourceType = EnumsConverter.FpDeliveryTypeToDataSourceType(delivery.DeliveryType);
            switch (deliverySourceType)
            {
                case Common.Enums.DataSourceType.ManualDataEntry:
                case Common.Enums.DataSourceType.Estimation:
                case Common.Enums.DataSourceType.Interface:
                case Common.Enums.DataSourceType.File:
                    {
                        if (delivery.Refuels != null && delivery.Refuels.Count > 0)
                        {
                            refuelList.AddRange(delivery.Refuels.Select(refuel =>
                                {
                                    return Refuel.ToOpRefuel(refuel, (int)deliverySourceType,
                                        relatedDeliveryId.ToString(), loadCustomData: loadCustomData, customDataTypes: customDataTypes);
                                }));
                        }
                        break;
                    }
                case Common.Enums.DataSourceType.AWSR:
                case Common.Enums.DataSourceType.OKO:
                case Common.Enums.DataSourceType.Fitter:
                case Common.Enums.DataSourceType.Flowmeter:
                    {
                        // Wyszukanie wszystkich zbiornikow z jednego produktu na który przyszła dostawa
                        var refuelTankIds = delivery.AtgRefuels.Where(r => r.TankId.HasValue).Select(r => r.TankId.Value).ToArray();
                        var refuelTanks = tanks.Where(t => refuelTankIds.Contains(t.Id.Value)).ToList();

                        if (delivery.AtgRefuels != null && delivery.AtgRefuels.Count > 0)
                        {
                            refuelList.AddRange(delivery.AtgRefuels.Select(atgRefuel =>
                                {
                                    return AtgRefuel.ToOpRefuel(atgRefuel, (int)deliverySourceType,
                                        relatedDeliveryId.ToString(), saleDuringDelivery, refuelTanks, loadCustomData: loadCustomData, customDataTypes: customDataTypes);
                                }));
                        }
                        break;
                    }
            }

            Dictionary<long, long> dataTypes = customDataTypes != null ? customDataTypes.Distinct().ToDictionary(q => q) : new Dictionary<long, long>()
            {
                { DataType.REFUEL_ORDER_NUMBER, DataType.REFUEL_ORDER_NUMBER },
                { DataType.REFUEL_DELIVERY_DAY, DataType.REFUEL_DELIVERY_DAY },
                { DataType.REFUEL_DRIVER_NAME, DataType.REFUEL_DRIVER_NAME },
                { DataType.REFUEL_CARRIER_NAME, DataType.REFUEL_CARRIER_NAME },
                { DataType.REFUEL_TERMINAL_NAME, DataType.REFUEL_TERMINAL_NAME },
                { DataType.REFUEL_TRAILER_NUMBER, DataType.REFUEL_TRAILER_NUMBER },
                { DataType.REFUEL_TRUCK_NUMBER, DataType.REFUEL_TRUCK_NUMBER }
            };

            foreach (var refuel in refuelList)
            {
                refuel.IdLocation = delivery.LocationId;

                if (loadCustomData)
                {
                    List<OpRefuelData> additionalDataList = new List<OpRefuelData>();
                    if (dataTypes.ContainsKey(DataType.REFUEL_ORDER_NUMBER))
                    {
                        additionalDataList.Add(new OpRefuelData()
                        {
                            IdRefuel = refuel.IdRefuel,
                            Value = delivery.OrderNumber,
                            IdDataType = DataType.REFUEL_ORDER_NUMBER
                        });
                    }

                    if (dataTypes.ContainsKey(DataType.REFUEL_DELIVERY_DAY) && delivery.DeliveryDate.HasValue)
                        additionalDataList.Add(new OpRefuelData()
                        {
                            IdRefuel = refuel.IdRefuel,
                            Value = DateTimer.GetDtUtcFromMsUtc(delivery.DeliveryDate.Value),
                            IdDataType = DataType.REFUEL_DELIVERY_DAY
                        });
                    if (dataTypes.ContainsKey(DataType.REFUEL_DRIVER_NAME))
                    {
                        Driver driver = delivery.DriverId.HasValue ? drivers.TryGetValue(delivery.DriverId.Value) : null;
                        if (driver != null)
                            additionalDataList.Add(new OpRefuelData()
                            {
                                IdRefuel = refuel.IdRefuel,
                                Value = driver.DriverName,
                                IdDataType = DataType.REFUEL_DRIVER_NAME
                            });
                    }

                    if (dataTypes.ContainsKey(DataType.REFUEL_CARRIER_NAME))
                    {
                        Carrier carrier = delivery.CarrierId.HasValue ? carriers.TryGetValue(delivery.CarrierId.Value) : null;
                        if (carrier != null)
                            additionalDataList.Add(new OpRefuelData()
                            {
                                IdRefuel = refuel.IdRefuel,
                                Value = carrier.CarrierName,
                                IdDataType = DataType.REFUEL_CARRIER_NAME
                            });
                    }

                    if (dataTypes.ContainsKey(DataType.REFUEL_TERMINAL_NAME))
                    {
                        Terminal terminal = delivery.TerminalId.HasValue ? terminals.TryGetValue(delivery.TerminalId.Value) : null;
                        if (terminal != null)
                            additionalDataList.Add(new OpRefuelData()
                            {
                                IdRefuel = refuel.IdRefuel,
                                Value = terminal.TerminalName,
                                IdDataType = DataType.REFUEL_TERMINAL_NAME
                            });
                    }

                    if (dataTypes.ContainsKey(DataType.REFUEL_TRAILER_NUMBER))
                    {
                        TruckTrailer trailer = delivery.TrailerId.HasValue ? trucksAndTrilers.TryGetValue(delivery.TrailerId.Value) : null;
                        if (trailer != null)
                            additionalDataList.Add(new OpRefuelData()
                            {
                                IdRefuel = refuel.IdRefuel,
                                Value = trailer.RegistrationNo,
                                IdDataType = DataType.REFUEL_TRAILER_NUMBER
                            });
                    }

                    if (dataTypes.ContainsKey(DataType.REFUEL_TRUCK_NUMBER))
                    {
                        TruckTrailer truck = delivery.TruckId.HasValue ? trucksAndTrilers.TryGetValue(delivery.TruckId.Value) : null;
                        if (truck != null)
                            additionalDataList.Add(new OpRefuelData()
                            {
                                IdRefuel = refuel.IdRefuel,
                                Value = truck.RegistrationNo,
                                IdDataType = DataType.REFUEL_TRUCK_NUMBER
                            });
                    }

                    // Generowanie IdRefuelData wszystkim dodanym wyżej wartosciom oraz dodanie ich do danych refuela.
                    additionalDataList.ForEach(d => d.IdRefuelData = DeliveryUtils.CreateRefuelDataId(d.IdRefuel, d.IdDataType, d.Index));
                    refuel.DataList.AddRange(additionalDataList);
                }

                // Ustawienie każdemu RefuelData danych z refuela oraz statusu
                refuel.DataList.ForEach(d =>
                {
                    d.StartTime = refuel.StartTime;
                    d.EndTime = refuel.EndTime;
                    d.Status = (int)Common.Enums.DataStatus.Normal;
                });

                // Refuel jest anulowany, jeżeli jakikolwiek jego RefuelData ma status == 0, więc ustawiamy dla wszystkich z anulowanej.
                if (delivery.Canceled.HasValue && delivery.Canceled.Value == true)
                {
                    refuel.DataList.ForEach(d => d.Status = (int)Common.Enums.DataStatus.Unknown);
                }
            }

            return refuelList;
        }
        #endregion

        #region ToFpDelivery
        public static Delivery ToFpDelivery(FP.WEsbProviderFP WEsbFP, List<OpRefuel> opRefuelsList)
        {
            if (opRefuelsList == null || !opRefuelsList.Any()) return null;

            OpRefuel referenceRefuel = opRefuelsList.FirstOrDefault();

            var manualSourceTypes = new List<int>()
            {
                (int)Common.Enums.DataSourceType.ManualDataEntry,
                (int)Common.Enums.DataSourceType.Estimation,
                (int)Common.Enums.DataSourceType.Interface,
                (int)Common.Enums.DataSourceType.File
            };
            var autoSourceTypes = new List<int>()
            {
                (int)Common.Enums.DataSourceType.AWSR,
                (int)Common.Enums.DataSourceType.OKO,
                (int)Common.Enums.DataSourceType.Fitter,
                (int)Common.Enums.DataSourceType.Flowmeter
            };

            var opManualRefuels = opRefuelsList.Where(r => r.DataSource != null && manualSourceTypes.Contains(r.DataSource.IdDataSourceType)).ToList();
            var fpManualRefuels = new List<Refuel>();
            opManualRefuels.ForEach(r => fpManualRefuels.Add(Refuel.ToFpRefuel(r)));

            var opAtgRefuels = opRefuelsList.Where(r => r.DataSource != null && autoSourceTypes.Contains(r.DataSource.IdDataSourceType)).ToList();
            var fpAtgRefuels = new List<AtgRefuel>();
            opAtgRefuels.ForEach(r => AtgRefuel.ToFpAtgRefuel(r));

            Delivery delivery = new Delivery()
            {
                AtgRefuels = fpAtgRefuels,
                Refuels = fpManualRefuels,
                StartTime = DateTimer.GetMsUtcFromDtLocal(referenceRefuel.StartTime),
                EndTime = DateTimer.GetMsUtcFromDtLocal(referenceRefuel.EndTime),
            };

            OpRefuelData refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_ORDER_NUMBER);
            delivery.OrderNumber = refuelData != null && refuelData.Value != null ? refuelData.Value.ToString() : null;

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_DELIVERY_DAY);
            delivery.DeliveryDate = refuelData != null && refuelData.Value != null ? (long?)DateTimer.GetMsUtcFromDtLocal((DateTime)refuelData.Value) : null;

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_DRIVER_NAME);
            if (refuelData != null && refuelData.Value != null)
            {
                Driver driver = WEsbFP.GetDrivers().FirstOrDefault(d => d.DriverName == refuelData.Value.ToString());
                delivery.DriverId = driver != null ? driver.Id : null;
            }

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_CARRIER_NAME);
            if (refuelData != null && refuelData.Value != null)
            {
                FP.Objects.Carrier carier = WEsbFP.GetCarriers().FirstOrDefault(c => c.CarrierName == refuelData.Value.ToString());
                delivery.CarrierId = carier != null ? carier.Id : null;
            }

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_TERMINAL_NAME);
            if (refuelData != null && refuelData.Value != null)
            {
                Terminal terminal = WEsbFP.GetTerminals().FirstOrDefault(t => t.TerminalName == refuelData.Value.ToString());
                delivery.TerminalId = terminal != null ? terminal.Id : null;
            }

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_TRAILER_NUMBER);
            if (refuelData != null && refuelData.Value != null)
            {
                TruckTrailer trailer = WEsbFP.GetTrucksTrailers().FirstOrDefault(tt => tt.RegistrationNo == refuelData.Value.ToString() && tt.Type == FP.Enums.TruckTrailerType.TRAILER.ToString());
                delivery.TrailerId = trailer != null ? trailer.Id : null;
            }

            refuelData = referenceRefuel.DataList.FirstOrDefault(d => d.IdDataType == DataType.REFUEL_TRUCK_NUMBER);
            if (refuelData != null && refuelData.Value != null)
            {
                TruckTrailer truck = WEsbFP.GetTrucksTrailers().FirstOrDefault(tt => tt.RegistrationNo == refuelData.Value.ToString() && tt.Type == FP.Enums.TruckTrailerType.TRUCK.ToString());
                delivery.TrailerId = truck != null ? truck.Id : null;
            }

            return delivery;
        }
        #endregion        
    }
    #endregion

    #region CounterValue
    public class CounterValue
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("calibrationError")]
        public double? CalibrationError { get; set; }

        [JsonProperty("counterValue")]
        public double? CounterVal { get; set; }

        [JsonProperty("flaskMeasurement")]
        public double? FlaskMeasurement { get; set; }

        [JsonProperty("balanceCalibrationId")]
        public long? BalanceCalibrationId { get; set; }

        [JsonProperty("dispenserId")]
        public long? DispenserId { get; set; }
    }
    #endregion

    #region internal DeliveryUtils
    public class DeliveryUtils
    {
        #region CreateRefuelDataId
        public static long CreateRefuelDataId(long idRefuel, long idDataType, int index)
        {
            unchecked
            {
                long hash = 2166136261;
                hash = (hash * 16777619) ^ idRefuel;
                hash = (hash * 16777619) ^ idDataType;
                hash = (hash * 16777619) ^ index;
                return hash;
            }
        }
        #endregion
    }
    #endregion
}
