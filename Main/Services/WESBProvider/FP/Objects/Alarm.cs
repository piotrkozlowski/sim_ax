﻿using IMR.Suite.UI.Business.Objects.DW;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider.FP.Objects
{
    #region Alarm
    public class Alarm
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("alarmClass")]
        public string AlarmClass { get; set; }

        [JsonProperty("alarmCriticality")]
        public string AlarmCriticality { get; set; }

        [JsonProperty("assigned")]
        public string Assigned { get; set; }

        [JsonProperty("confirmed")]
        public bool? Confirmed { get; set; }

        [JsonProperty("createId")]
        public long? CreateId { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("endTime")]
        public long? EndTime { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("alarmType")]
        public string AlarmType { get; set; }

        [JsonProperty("toView")]
        public bool? ToView { get; set; }

        [JsonProperty("locationId")]
        public long? LocationId { get; set; }

        [JsonProperty("tankId")]
        public long? TankId { get; set; }
        #endregion
        #region Custom Properties
        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }

        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }
        #endregion

        #region Methods

        #region ToListOpDataTemporal
        public static List<OpDataTemporal> ToListOpDataTemporal(List<Alarm> alarms)
        {
            if (alarms == null) throw new ArgumentNullException("Alarms collection is null");

            List<OpDataTemporal> list = new List<OpDataTemporal>();

            foreach (var alarm in alarms)
            {
                if (!string.IsNullOrEmpty(alarm.AlarmClass))                
                    list.Add(ToOpDataTemporal(alarm, DataType.AWSR_PROBLEM_CLASS, (int)EnumsConverter.FpAlarmClassToProblemClass(alarm.AlarmClass)));
                if (!string.IsNullOrEmpty(alarm.AlarmCriticality))
                    list.Add(ToOpDataTemporal(alarm, DataType.AWSR_PROBLEM_CRITICALITY, (int)EnumsConverter.FpAlarmCriticalityToProblemLevel(alarm.AlarmCriticality)));
                if (!string.IsNullOrEmpty(alarm.Notes))
                    list.Add(ToOpDataTemporal(alarm, DataType.AWSR_PROBLEM_NOTES, alarm.AlarmCriticality));
            }

            return list;
        }
        #endregion

        #region ToOpDataTemporal
        private static OpDataTemporal ToOpDataTemporal(Alarm alarm, long idDataType, object value, int index = 0)
        {
            return new OpDataTemporal(new Data.DB.DB_DATA_TEMPORAL()
            {
                END_TIME = alarm.EndTimeLocal,
                ID_AGGREGATION_TYPE = null,
                ID_ALARM_EVENT = null,
                ID_DATA_SOURCE = null,
                ID_DATA_SOURCE_TYPE = null,
                ID_DATA_TEMPORAL = 0,
                ID_DATA_TYPE = idDataType,
                ID_LOCATION = alarm.LocationId,
                ID_METER = alarm.TankId,
                INDEX_NBR = index,
                SERIAL_NBR = null,
                START_TIME = alarm.StartTimeLocal,
                STATUS = (int)Common.Enums.DataStatus.Normal,
                VALUE = value
            });
        }
        #endregion


        #endregion

        #region CreateIdDataTemporal
        private static long CreateIdDataTemporal(Alarm alarm, long idDataType, int index, object value)
        {
            unchecked
            {
                long hash = 2166136261;
                hash = (hash * 16777619) ^ alarm.Id.Value;
                hash = (hash * 16777619) ^ idDataType;
                hash = (hash * 16777619) ^ index;
                return hash;
            }
        }
        #endregion
    }
    #endregion
}
