﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using WebSimaxServiceReference = IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SESSION = IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference.SESSION;
using SESSION_STATUS = IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference.SESSION_STATUS;
using DBCollectorClient = IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference.DBCollectorClient;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace IMR.Suite.Services.WebSimaxInterface
{
    public class WebSimaxComponent
    {
        public static Dictionary<string, Tuple<DBCollectorClient, SESSION>> ConnectionDict { get; set; }

        public static string Culture { get; set; }
        public static Enums.Module Module { get; set; }

        private const string DefaultBinding = "netTcpBinding_IDBCollector";

        #region InitWcfConnection

        public static Tuple<DBCollectorClient, SESSION> InitWcfConnection(string imrServerAddress, string binding = null)
        {
            if (String.IsNullOrEmpty(binding))
                binding = DefaultBinding;

            SESSION dbCollectorSession = new SESSION();
            DBCollectorClient dbCollectorClient = new DBCollectorClient(binding, imrServerAddress);
            Tuple<DBCollectorClient, SESSION> tItem = new Tuple<DBCollectorClient, SESSION>(dbCollectorClient, dbCollectorSession);

            return tItem;
        }

        #endregion
        #region CreateWcfConnection
        public static Tuple<DBCollectorClient, SESSION> CreateWcfConnection(
            string imrServerAddress, int[] distributorFilter, string login, string password, Enums.Module module,
            string userCulture = null, string binding = null, object[] connectionParams = null, bool encryptPassword = true)
        {
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateRemoteCertificate);
            Tuple<DBCollectorClient, SESSION> tItem = TryGetWcfConnection(imrServerAddress);
            if (tItem != null && tItem.Item1 != null && tItem.Item2 != null)
                return tItem;
            if (String.IsNullOrEmpty(imrServerAddress))
                return null;
            if (String.IsNullOrEmpty(binding))
                binding = DefaultBinding;
            SESSION dbCollectorSession = new SESSION();
            DBCollectorClient dbCollectorClient = new DBCollectorClient(binding, imrServerAddress);
            return CreateWcfConnection(dbCollectorClient, dbCollectorSession, distributorFilter, login, password, module, userCulture: userCulture, connectionParams: connectionParams, encryptPassword: encryptPassword);
        }
        public static Tuple<DBCollectorClient, SESSION> CreateWcfConnection(
            DBCollectorClient dbCollectorClient, SESSION dbCollectorSession, int[] distributorFilter, string login, string password, Enums.Module module,
            string userCulture = null, object[] connectionParams = null, bool encryptPassword = true)
        {
            Guid guid = Guid.NewGuid();
            Utils.Utils.Log(Utils.EventID.WebSimaxComponent.CreateConnection.EnterFunction, guid);
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateRemoteCertificate);

            if (dbCollectorClient == null || String.IsNullOrEmpty(dbCollectorClient.Endpoint.Address.Uri.AbsoluteUri))
                return null;

            if (module == Enums.Module.Unknown)
            {
                if (Module == Enums.Module.Unknown)
                    module = Enums.Module.DBCollector;
                else
                    module = Module;
            }
            if (String.IsNullOrEmpty(userCulture))
            {
                if (String.IsNullOrEmpty(Culture))
                    userCulture = "en-GB";
                else
                    userCulture = Culture;
            }
            string imrServerAddress = dbCollectorClient.Endpoint.Address.Uri.AbsoluteUri;
            string binding = dbCollectorClient.Endpoint.Binding.Name;
            Utils.Utils.Log(Utils.EventID.WebSimaxComponent.CreateConnection.ParamsInfo, guid, imrServerAddress, (distributorFilter == null ? "<null>" : distributorFilter.Count().ToString()), login, module.ToString(), userCulture, binding);

            Tuple<DBCollectorClient, SESSION> tItem = TryGetWcfConnection(imrServerAddress);
            if (tItem != null && tItem.Item1 != null && tItem.Item2 != null)
                return tItem;

            if(dbCollectorSession == null)
                dbCollectorSession = new SESSION();

            dbCollectorClient.Endpoint.Binding.OpenTimeout = TimeSpan.FromMinutes(2);
            dbCollectorClient.Endpoint.Binding.CloseTimeout = TimeSpan.FromMinutes(2);
            dbCollectorClient.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromMinutes(2);
            dbCollectorClient.Endpoint.Binding.SendTimeout = TimeSpan.FromMinutes(2);
            if (dbCollectorClient.Endpoint.Binding is System.ServiceModel.NetTcpBinding)
                (dbCollectorClient.Endpoint.Binding as System.ServiceModel.NetTcpBinding).ReliableSession.InactivityTimeout = TimeSpan.FromMinutes(15);

            dbCollectorSession.Status = SESSION_STATUS.OK;
            dbCollectorSession.Time = DateTime.UtcNow;

            dbCollectorClient.InitDBCollector(null, null, distributorFilter, module);

            if (encryptPassword)
                password = RSA.Encrypt(DataConverter.EncodePassword(password));

            dbCollectorSession = dbCollectorClient.Login(dbCollectorSession, login, password, userCulture, 0);
            CheckWcfDbCollectorSessionState(dbCollectorSession, "Login");

            if (connectionParams != null)
            {
                //ustawiamy dodatkowe parametry połaczenia
                dbCollectorClient.InitDBCollectorProperties(ref dbCollectorSession, connectionParams);
                CheckWcfDbCollectorSessionState(dbCollectorSession, "InitDBCollectorProperties");
            }

            ConnectionDict[imrServerAddress] = new Tuple<DBCollectorClient, SESSION>(dbCollectorClient, dbCollectorSession);
            // dbCollectorClient.GetTaskDataFilter
            Utils.Utils.Log(Utils.EventID.WebSimaxComponent.CreateConnection.ExitFunction, guid);
            return new Tuple<DBCollectorClient, SESSION>(dbCollectorClient, dbCollectorSession);
        }

        #endregion

        private static Tuple<DBCollectorClient, SESSION> TryGetWcfConnection(string imrServerAddress)
        {
            if (ConnectionDict == null)
                ConnectionDict = new Dictionary<string, Tuple<DBCollectorClient, SESSION>>();
            if (!ConnectionDict.ContainsKey(imrServerAddress))
                ConnectionDict.Add(imrServerAddress, null);
            else
            {
                Tuple<DBCollectorClient, SESSION> tItem = ConnectionDict[imrServerAddress] as Tuple<DBCollectorClient, SESSION>;
                if (tItem != null && tItem.Item1 != null && tItem.Item2 != null)
                {
                    if (tItem.Item1.State == System.ServiceModel.CommunicationState.Opened && tItem.Item2.Status == SESSION_STATUS.OK)
                    {
                        bool connectionSuccess = true;
                        try
                        {
                            SESSION sTmp = tItem.Item2;
                            connectionSuccess = tItem.Item1.ConnectionCheck(ref sTmp);
                            CheckWcfDbCollectorSessionState(sTmp, "ConnectionCheck");

                            //   connectionSuccess = tItem.Item1.ConnectionDBCheck(ref sTmp);
                            //   CheckWcfDbCollectorSessionState(sTmp, "ConnectionDBCheck");
                        }
                        catch (Exception ex)
                        {
                            connectionSuccess = false;
                        }
                        if (connectionSuccess)
                            return tItem;
                    }
                    else
                    {
                        if (tItem.Item1.State == System.ServiceModel.CommunicationState.Opened)
                        {
                            try
                            {
                                if (tItem.Item2 != null)
                                {
                                    SESSION sTmp = tItem.Item2;
                                    tItem.Item1.Logout(sTmp);
                                }
                                tItem.Item1.Abort();
                            }
                            catch (Exception ex)
                            {
                            }
                            finally
                            {
                                ConnectionDict[imrServerAddress] = null;
                            }
                        }
                    }
                }
            }
            return new Tuple<DBCollectorClient, SESSION>(null, null);
        }

        #region CloseWcfConnecion

        public static void CloseWcfConnecion(string imrServerAddress = null)
        {
            if (ConnectionDict != null && ConnectionDict.Count > 0)
            {
                List<string> linksToClose = new List<string>();
                if (!String.IsNullOrEmpty(imrServerAddress))
                {
                    if (ConnectionDict.ContainsKey(imrServerAddress))
                        linksToClose.Add(imrServerAddress);
                }
                else
                    ConnectionDict.Keys.ToList().ForEach(k => linksToClose.Add(k));
                foreach (string sItem in linksToClose)
                {
                    if (ConnectionDict[sItem] != null)
                    {
                        if (ConnectionDict[sItem].Item1 != null)
                        {
                            SESSION sTmp = ConnectionDict[sItem].Item2;
                            if (sTmp != null)
                                ConnectionDict[sItem].Item1.Logout(sTmp);
                            ConnectionDict[sItem].Item1.Abort();
                        }
                    }
                    ConnectionDict.Remove(sItem);
                }
            }
        }

        #endregion

        #region CheckWcfDbCollectorSessionState

        public static void CheckWcfDbCollectorSessionState(SESSION session, string method)
        {
            if (session.Status != SESSION_STATUS.OK)
                throw new Exception("wcfDBCollectorClient error execute " + method + "." + (session.Error != null ? " Details: " + session.Error.Msg : ""));
        }

        #endregion

        #region AddToLog

        public static void AddToLog(Enums.Module module, LogData logData, params object[] parameters)
        {
            AddToLog(Module, logData, parameters);
        }
        public static void AddToLog(LogData logData, params object[] parameter)
        {
            IMRLog.AddToLog(Module, logData, parameter);
        }

        #endregion

        #region ValidateRemoteCertificate

        private static bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool certIsValid = sslPolicyErrors == SslPolicyErrors.None;
            int? clientCertificatesCount = null;

            Utils.Utils.Log(Utils.EventID.WebSimaxComponent.CreateConnection.EnterRemoteCertificateValidation, "-");
            //only check if the cert is not valid and cert provided
            if (!certIsValid && certificate != null)
            {
                X509CertificateCollection clientCertificates = null;
                if (sender is System.Net.HttpWebRequest)
                    clientCertificates = (sender as System.Net.HttpWebRequest).ClientCertificates;

                if (clientCertificates != null && clientCertificates.Count > 0)
                {
                    clientCertificatesCount = clientCertificates.Count;
                    foreach (X509Certificate clientCertificate in clientCertificates)
                    {
                        if (!clientCertificate.Equals(certificate))
                            break;

                        X509Certificate2 trustedClientCertificate = new X509Certificate2(clientCertificate);
                        X509Certificate2 serviceCertificate = new X509Certificate2(certificate);

                        X509Chain chainToValidate = new X509Chain();
                        chainToValidate.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
                        chainToValidate.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
                        chainToValidate.ChainPolicy.VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority;
                        chainToValidate.ChainPolicy.VerificationTime = DateTime.Now;
                        chainToValidate.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 0, 0);

                        chainToValidate.ChainPolicy.ExtraStore.Add(trustedClientCertificate);
                        bool isChainValid = chainToValidate.Build(serviceCertificate);
                        if (!isChainValid)
                        {
                            // If the certificate chain is not valid, get all returned errors.
                            string[] errors = chainToValidate.ChainStatus.Select(x => String.Format("{0} ({1})", x.StatusInformation.Trim(), x.Status)).ToArray();
                            string serviceCertChainErrors = "No detailed errors are available.";
                            if (errors != null && errors.Length > 0) serviceCertChainErrors = String.Join(", ", errors);

                            throw new System.IdentityModel.Tokens.SecurityTokenValidationException(String.Format(
                                     "The chain of service certificate '{0}' is not valid. Errors: {1}",
                                     serviceCertificate.Thumbprint, serviceCertChainErrors));
                        }

                        if (!chainToValidate.ChainElements.Cast<X509ChainElement>().Any(x => x.Certificate.Thumbprint == serviceCertificate.Thumbprint))
                        {
                            throw new System.IdentityModel.Tokens.SecurityTokenValidationException(String.Format(
                                    "The chain of Service Certificate '{0}' is not valid. " +
                                    " Service Certificate Authority Thumbprint does not match " +
                                    "Trusted CA's Thumbprint '{1}'",
                                    serviceCertificate.Thumbprint, trustedClientCertificate.Thumbprint));
                        }
                        else
                        {
                            Console.WriteLine(String.Format("Service Certificate Authority '{0}' matches the Trusted CA's '{1}'", serviceCertificate.IssuerName.Name, trustedClientCertificate.SubjectName.Name));
                        }

                        certIsValid = true;
                        break;
                    }
                }

            }
            Utils.Utils.Log(Utils.EventID.WebSimaxComponent.CreateConnection.ExitRemoteCertificateValidation, "-", certIsValid, 
                clientCertificatesCount.HasValue ? clientCertificatesCount.ToString() : "<null>");
            return certIsValid;

            //bool result = false;
            //if (cert.Subject.Contains("IMR-Web"))
            //    result = true;
            //return result;
        }

        #endregion
    }
}
