﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WebSimaxInterface.Utils
{
    public class Structs
    {
        public class MeterTypeMapping
        {
            public int ImrIdMeterType { get; private set; }
            public int? StarkIdMeterType { get; private set; }

            public MeterTypeMapping(int ImrIdMeterType, int? StarkIdMeterType)
            {
            }
        }
    }
}
