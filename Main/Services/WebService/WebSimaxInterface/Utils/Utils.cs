﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WebSimaxInterface.Utils
{
    public class Utils
    {
        #region static Log

        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(Enums.Module.DBCollector, EventData, Parameters);
        }

        public static void Log(Enums.Module Module, LogData EventData, params object[] Parameters)
        {
            Logger.Add(Module, EventData, Parameters);
        }

        #endregion
    }
}
