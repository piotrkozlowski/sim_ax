﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.WebSimaxInterface.Utils
{
    public class EventID
    {
        #region WebSimaxComponent 2200000-2299999
        public class WebSimaxComponent
        {
            #region CreateConnection 2200000-2200009
            public class CreateConnection
            {
                public static readonly LogData EnterFunction = new LogData(2200000, LogLevel.Info, "[{0}]-EnterFunction.");
                public static readonly LogData ExitFunction = new LogData(2200001, LogLevel.Info, "[{0}]-ExitFunction.");
                public static readonly LogData ParamsInfo = new LogData(2200002, LogLevel.Info, "[{0}]-ParamsInfo. ImrServerAddres: {1}, DistributorFilter count: {2}, Login: {3}, Module: {4}, UserCulture: {5}, Binding: {6}.");
                public static readonly LogData EnterRemoteCertificateValidation = new LogData(2200003, LogLevel.Info, "[{0}]-Enter remote certificate validation.");
                public static readonly LogData ExitRemoteCertificateValidation = new LogData(2200004, LogLevel.Info, "[{0}]-Exit remote certificate validation. IsValid: {1}, ClientCerificatesCount: {2}.");
            }
            #endregion
        }
        #endregion
    }
}
