﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using IMR.Suite.Common;
using IMR.Suite.Common.Code;
using IMR.Suite.Common.MSMQ;

using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.Services.Worker.Library
{
    #region WorkResult
    [Serializable]
    public class WorkResult
    {
        #region Constructor
        public WorkResult(Enums.WorkStatus Status)
        {
            this.Status = Status;
            this.IdWorkHistoryDescr = null;
            this.WorkHistoryData = null;
        }
        public WorkResult(Enums.WorkStatus Status, long? IdWorkHistoryDescr, params object[] WorkHistoryData)
        {
            this.Status = Status;
            this.IdWorkHistoryDescr = IdWorkHistoryDescr;
            this.WorkHistoryData = WorkHistoryData;
        }
        #endregion

        public Enums.WorkStatus Status;
        public long? IdWorkHistoryDescr;
        public object[] WorkHistoryData;

        #region NewWorkAdded
        public static WorkResult NewWorkAdded
        {
            get { return new WorkResult(Enums.WorkStatus.New, DiagDescrID.Works.NewWorkAdded); }
        }
        #endregion
        #region InitWorkOK
        public static WorkResult InitWorkOK
        {
            get { return new WorkResult(Enums.WorkStatus.New, DiagDescrID.Works.InitWorkOK); }
        }
        #endregion
        #region InitWorkError
        public static WorkResult InitWorkError
        {
            get { return new WorkResult(Enums.WorkStatus.Error, DiagDescrID.Works.InitWorkError); }
        }
        #endregion
        #region Running
        public static WorkResult Running
        {
            get { return new WorkResult(Enums.WorkStatus.Running, DiagDescrID.Works.Running); }
        }
        #endregion
        #region Succeeded
        public static WorkResult Succeeded
        {
            get { return new WorkResult(Enums.WorkStatus.Succeeded, DiagDescrID.Works.Succeeded); }
        }
        #endregion
        #region Error
        public static WorkResult Error
        {
            get { return new WorkResult(Enums.WorkStatus.Error, DiagDescrID.Works.Error); }
        }
        #endregion
        #region Partly
        public static WorkResult Partly
        {
            get { return new WorkResult(Enums.WorkStatus.Partly, DiagDescrID.Works.Partly); }
        }
        #endregion
        #region Stopped
        public static WorkResult Stopped
        {
            get { return new WorkResult(Enums.WorkStatus.Stopped, DiagDescrID.Works.Stopped); }
        }
        #endregion
        #region Aborted
        public static WorkResult Aborted
        {
            get { return new WorkResult(Enums.WorkStatus.Aborted, DiagDescrID.Works.Aborted); }
        }
        #endregion
        #region UnknownState
        public static WorkResult UnknownState
        {
            get { return new WorkResult(Enums.WorkStatus.Unknown, DiagDescrID.Works.UnknownState); }
        }
        #endregion
    }
    #endregion

    #region BaseWork
    [Serializable]
    public abstract partial class BaseWork : MarshalByRefObjectDisposable, IWorkPlugIn
    {
        #region Consts
        #endregion

        #region internal Members
        protected IDataProvider DataProvider { get; private set; }
        public Work Work { get; set; }

        internal readonly AutoResetEvent NotifyWorkEvent = new AutoResetEvent(false);
        internal readonly AutoResetEvent StopWorkEvent = new AutoResetEvent(false); // ?? ManualResetEvent ??
        internal readonly AutoResetEvent AbortWorkEvent = new AutoResetEvent(false);
        internal readonly AutoResetEvent SysMessageEvent = new AutoResetEvent(false);

        internal CacheDictionary<Work, Work> ChildWorks = new CacheDictionary<Work, Work>();
        internal ConcurrentQueue<Tuple<Work, EventIndex>> NotifiedWorkQueue = new ConcurrentQueue<Tuple<Work, EventIndex>>();

        #region Queues
        // Kolejka wokrów do modułu Worker
        internal ConcurrentQueue<Work> WorksFromWorkQueue { get; private set; }
        #endregion

        #region WorkThread
        internal Thread WorkThread = null;
        #endregion
        #region WorkInsertThread
        internal static Thread WorkInsertThread = null;
        internal static object SyncRootWorkInsert = new object();
        internal DateTime WorkInsertLastWriteTime = DateTime.UtcNow;

        internal readonly ManualResetEvent StopWorkInsertEvent = new ManualResetEvent(false);
        internal static ConcurrentQueue<WorkInfo> WorkInsert = new ConcurrentQueue<WorkInfo>();
        #endregion
        #region WorkDataInsertThread
        internal static Thread WorkDataInsertThread = null;
        internal static object SyncRootWorkDataInsert = new object();
        internal DateTime WorkDataInsertLastWriteTime = DateTime.UtcNow;

        internal readonly ManualResetEvent StopWorkDataInsertEvent = new ManualResetEvent(false);
        internal static ConcurrentQueue<WorkDataInfo> WorkDataInsert = new ConcurrentQueue<WorkDataInfo>();
        #endregion
        #region WorkStatusThread
        internal static Thread WorkStatusThread = null;
        internal static object SyncRootWorkStatus = new object();
        internal DateTime WorkStatusLastWriteTime = DateTime.UtcNow;

        internal readonly ManualResetEvent StopWorkStatusEvent = new ManualResetEvent(false);
        internal static ConcurrentQueue<WorkStatusInfo> WorkStatus = new ConcurrentQueue<WorkStatusInfo>();
        #endregion
        #region WorkHistoryThread
        internal static Thread WorkHistoryThread = null;
        internal static object SyncRootWorkHistory = new object();
        internal DateTime WorkHistoryLastWriteTime = DateTime.UtcNow;

        internal readonly ManualResetEvent StopWorkHistoryEvent = new ManualResetEvent(false);
        internal static ConcurrentQueue<WorkHistoryInfo> WorkHistory = new ConcurrentQueue<WorkHistoryInfo>();
        #endregion
        #endregion
        #region internal Classes
        internal class WorkInfo
        {
            public Work Work;
            public long RetryInsertCount;
            public AutoResetEvent AddedWorkEvent;
            public AutoResetEvent AddWorkErrorEvent;
        }
        internal class WorkDataInfo
        {
            public Work Work;
            public long RetryInsertCount;
            public AutoResetEvent AddedWorkDataEvent;
            public AutoResetEvent AddWorkDataErrorEvent;
        }
        internal class WorkStatusInfo
        {
            public Work Work;
            public Enums.WorkStatus WorkStatus;
            public long RetryInsertCount;
        }
        public class WorkHistoryInfo
        {
            public OpWorkHistory OpWorkHistory;
            public long RetryInsertCount;

            #region static GetWorkHistoryInfo
            public static WorkHistoryInfo GetWorkHistoryInfo(DiagnosticInfo info, long idWork, Enums.WorkStatus status)
            {
                WorkHistoryInfo workHistoryInfo = new WorkHistoryInfo();
                workHistoryInfo.OpWorkHistory = new OpWorkHistory()
                {
                    IdWork = idWork,
                    IdWorkStatus = (int)status,
                    StartTime = info.Timestamp,
                    IdDescr = info.IdHistoryDescr,
                    Assembly = info.Assembly,
                    IdModule = (int)info.Module
                };
                if (info.HistoryDescrArgs != null && info.HistoryDescrArgs.Length > 0)
                {
                    for (int i = 0; i < info.HistoryDescrArgs.Length; i++)
                        workHistoryInfo.OpWorkHistory.DataList.Add(new OpWorkHistoryData() { ArgNbr = i, Value = info.HistoryDescrArgs[i] });
                }
                return workHistoryInfo;
            }
            #endregion
        }
        #endregion
        #region internal Enums
        internal enum EventIndex : int
        {
            Unknown = -1,
            Running = 0,
            Stop = 1,
            Abort = 2,
        }
        #endregion

        public abstract WorkResult OnInit();
        public abstract WorkResult OnExecute();
        public virtual void OnNotify(Work notifiedWork) { return; }
        public virtual void OnNotifySysMsg(SysMessage sysMessage) { return; }
        public virtual void OnStop() { return; }
        public virtual object OnProcessCommand(Enums.ModuleCommandType commandType, params object[] args) { return null; }


        #region WorkerConfig (ModuleData)
        //WORKER_AMOUNT_OF_SIMULTANEOUS_HISTORY_INFO	- ilosc HistoryInfo(zebranych w buf) po ktorej nastapi zapis do DB
        //WORKER_FLUSH_TIME_HISTORY_INFO				- [sec] czas po ktorym nastapi zapis HistoryInfo do DB (niezaleznie od ilosci zebranych w buf)
        //WORKER_RETRY_INSERT_HISTORY_INFO			    - ilosc prob wstawienia HistoryInfo w przypadku blednych insertow
        //WORKER_MAX_THREADS_IN_POLL					- maksymalna liczba watkow jaka moze zostac utworzona podczas obsulgi akcji
        private List<DataValue> WorkerConfig = new List<DataValue>();
        #endregion

        #region Destructor
        ~BaseWork()
        {
            StopWorkHistoryThread();
            StopWorkInsertThread();
            StopWorkDataInsertThread();
            StopWorkStatusThread();
            StopWorkThread();
            base.Dispose();
        }
        #endregion
        #region InternalInit
        internal void InternalInit(params object[] args)
        {
            WorkerConfig = (List<DataValue>)args[0];

            DataProvider = (IDataProvider)args[1];

            WorksFromWorkQueue = (ConcurrentQueue<Work>)args[2]; // ConcurrentQueue from Worker module, to send new actions directly from module, bypassing MSMQ

            Cache Cache = new Cache();

            Log(EventID.BaseWork.InitStarting, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);

            StartWorkHistoryThread();
            StartWorkInsertThread();
            StartWorkDataInsertThread();
            StartWorkStatusThread();
        }
        #endregion

        #region IWorkPlugIn.Init
        bool IWorkPlugIn.Init(params object[] args)
        {
            InternalInit(args);

            if (Work.IdWork == null)
            {
                Work.Status = Enums.WorkStatus.New;
                Work.IdWork = AddNewWork(Work);
                if (Work.IdWork == null)
                {
                    Log(EventID.BaseWork.InitAddNewError, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);
                    return false;
                }
                Log(EventID.BaseWork.InitAddedToDB, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);
            }

            AddWorkHistoryInfo(WorkResult.NewWorkAdded);
            if (Work.DiagnosticInfos.Count > 0)
            {
                AddWorkHistoryInfo(Work.DiagnosticInfos.ToArray(), WorkResult.NewWorkAdded.Status);
                Work.DiagnosticInfos.Clear();
            }

            // Przygotowanie WorkData
            if (Work.IdWorkData == null && (Work.WorkData == null || (Work.WorkData != null && Work.WorkData.Length == 0)))
            {
                Log(EventID.BaseWork.NoWorkData, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);
            }

            DataValue[] workDataFromDB = null;
            if (Work.IdWorkData != null)
            {
                workDataFromDB = DataProvider.GetWorkDataFilter(IdWorkData: new long[] { Work.IdWorkData.Value }).Select(s => new DataValue(s.IdDataType, s.IndexNbr, s.Value)).ToArray();
            }

            WorkResult initResult = WorkResult.UnknownState;
            try
            {
                #region OnInit()
                initResult = OnInit();
                if (initResult.Status == Enums.WorkStatus.Error)
                {
                    Log(EventID.BaseWork.OnInitError, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);
                    if (initResult.IdWorkHistoryDescr == null)
                        initResult = new WorkResult(Work.Status = Enums.WorkStatus.Error, DiagDescrID.Works.InitWorkError);

                    ChangeWorkStatus(initResult);
                    return false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                initResult = new WorkResult(Work.Status = Enums.WorkStatus.Error, DiagDescrID.Works.InitWorkException, ex.Message);
                Log(EventID.BaseWork.OnInitException, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, ex.Message);
                ChangeWorkStatus(initResult);
                return false;
            }

            CheckAndSaveWorkData(workDataFromDB);

            if (!Work.IdWorkData.HasValue)
                Log(EventID.BaseWork.NoWorkData, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);

            Log(EventID.BaseWork.InitEnding, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);

            return true;
        }
        #endregion
        #region IWorkPlugIn.Clear
        void IWorkPlugIn.Clear()
        {
        }
        #endregion
        #region IWorkPlugIn.Execute
        bool IWorkPlugIn.Execute()
        {
            WorkResult executeResult = WorkResult.Running;
            bool bExecute = false;

            Log(EventID.BaseWork.ExecuteStarting, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status);
            Work.Status = executeResult.Status;
            ChangeWorkStatus(executeResult);

            try
            {
                executeResult = OnExecute();
            }
            catch (Exception ex)
            {
                Log(EventID.BaseWork.OnExecuteException, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, ex.Message);
                executeResult = new WorkResult(Enums.WorkStatus.Error, DiagDescrID.Works.ExecuteException, ex.Message);
            }
            finally
            {
                if (executeResult.Status == Enums.WorkStatus.Succeeded)
                    bExecute = true;
            }
            Work.Status = executeResult.Status;
            Log(EventID.BaseWork.ExecuteEnding, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, bExecute);
            ChangeWorkStatus(executeResult);

            StopWorkThread();
            return bExecute;
        }
        #endregion
        #region IWorkPlugIn.Notify
        void IWorkPlugIn.Notify(NotifyWork notify)
        {
            Work NotifiedWork = null;

            switch (notify.Type)
            {
                #region Work
                case NotifyWork.NotifyType.Work:
                    NotifiedWork = (Work)notify.Notify;

                    if (NotifiedWork.DiagnosticInfos.Count > 0)
                    {
                        AddWorkHistoryInfo(NotifiedWork.DiagnosticInfos.ToArray(), NotifiedWork.Status, NotifiedWork.IdWork);
                        NotifiedWork.DiagnosticInfos.Clear();
                    }

                    #region New - Update IdWork one of child work
                    if (NotifiedWork.Status.In(Enums.WorkStatus.New, Enums.WorkStatus.Waiting))
                    {
                        Log(EventID.BaseWork.NotifiedNew, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, NotifiedWork.IdWork, NotifiedWork.IdWorkType, DataProvider.GetWorkType(NotifiedWork.IdWorkType)?.Name);
                        // child work informuje parent'a ze zostala dodana do STP
                        // szukamy w ChildWorks i aktualizyjemy ze wzgledu na pozyskane IdWork
                        foreach (Work work in ChildWorks.Where(w => w.Key == NotifiedWork).Select(s => s.Key))
                            work.IdWork = NotifiedWork.IdWork;

                        EnqueueWork(NotifiedWork, EventIndex.Running);
                        return;
                    }
                    #endregion
                    #region Stop - someone request Stop this work OR one of child work was Stopped
                    if (NotifiedWork.Status.In(Enums.WorkStatus.Stop, Enums.WorkStatus.Stopped))
                    {
                        Log(EventID.BaseWork.NotifiedStop, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, NotifiedWork.IdWork, NotifiedWork.IdWorkType, DataProvider.GetWorkType(NotifiedWork.IdWorkType)?.Name);
                        EnqueueWork(NotifiedWork, EventIndex.Stop);
                    }
                    #endregion
                    #region Abort - someone reloaded PlugIn
                    if (NotifiedWork.Status.In(Enums.WorkStatus.Abort, Enums.WorkStatus.Aborted))
                    {
                        Log(EventID.BaseWork.NotifiedAbort, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, NotifiedWork.IdWork, NotifiedWork.IdWorkType, DataProvider.GetWorkType(NotifiedWork.IdWorkType)?.Name);
                        EnqueueWork(NotifiedWork, EventIndex.Abort);
                    }
                    #endregion
                    #region Rest - Finish work (with status Succeded, Error or Partly) or EventData
                    if (ChildWorks.Any(p => p.Key.IdWork.HasValue && p.Key.IdWork.Value == NotifiedWork.IdWork.Value) ||
                        ChildWorks.ContainsKey(NotifiedWork))
                    {
                        Log(EventID.BaseWork.NotifiedFinish, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, NotifiedWork.IdWork, NotifiedWork.IdWorkType, DataProvider.GetWorkType(NotifiedWork.IdWorkType)?.Name, NotifiedWork.Status);
                        Work keyWork = null;
                        if ((keyWork = ChildWorks.Keys.FirstOrDefault<Work>(p => p.IdWork.HasValue && p.IdWork.Value == NotifiedWork.IdWork.Value)) == null)
                            keyWork = NotifiedWork;
                        ChildWorks[keyWork] = NotifiedWork;
                        if (NotifiedWork.Status.In(Enums.WorkStatus.Succeeded, Enums.WorkStatus.Error, Enums.WorkStatus.Partly))
                            EnqueueWork(NotifiedWork, EventIndex.Running);
                    }
                    #endregion

                    break;
                #endregion
                #region ChangeInTable
                case NotifyWork.NotifyType.ChangeInTable:
                    #region Decode and check if msg is ChangeInTable[]
                    ChangeInTable[] msgChangeInTables = ChangeInTable.DecodeMessage(notify.Notify);
                    if (msgChangeInTables == null)
                    {
                        return;
                    }
                    #endregion

                    #region ChangeInDataTable
                    ChangeInDataTable[] changesInDataTable = msgChangeInTables.Where(p => p is ChangeInDataTable).Select(s => (ChangeInDataTable)s).ToArray();
                    #endregion
                    #region ChangeInDictTable
                    ChangeInDictTable[] changesInDictTables = msgChangeInTables.Where(p => p is ChangeInDictTable).Select(s => (ChangeInDictTable)s).ToArray();
                    #endregion
                    #region ChangeInObjectDataTable
                    ChangeInObjectDataTable[] changesInObjectDataTables = msgChangeInTables.Where(p => p is ChangeInObjectDataTable).Select(s => (ChangeInObjectDataTable)s).ToArray();

                    #region MODULE_DATA
                    ChangeInObjectDataTable[] changesInObjectDataTable_MODULE_DATA = changesInObjectDataTables.Where(p => p.TableName == "MODULE_DATA" && p.ObjectDataValue.IdObject == (int)Enums.Module.Worker).ToArray();
                    foreach (ChangeInObjectDataTable change in changesInObjectDataTable_MODULE_DATA)
                    {
                        Log(EventID.BaseWork.GotCIT_ModuleDataChanged, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, change.ObjectDataValue.Type.IdDataType, change.ObjectDataValue.Value);

                        #region Updated/Inserted
                        if (change.IsValueNull == false)
                        {
                            DataValue dv = WorkerConfig.FirstOrDefault(w => w.IdDataType == change.ObjectDataValue.Type.IdDataType);
                            if (dv != null)
                                WorkerConfig.Remove(dv);
                            WorkerConfig.Add(change.ObjectDataValue);
                        }
                        #endregion
                        #region Deleted
                        else
                        {
                            DataValue dv = WorkerConfig.FirstOrDefault(w => w.IdDataType == change.ObjectDataValue.Type.IdDataType);
                            if (dv != null)
                                WorkerConfig.Remove(dv);
                        }
                        #endregion
                    }
                    if (changesInObjectDataTable_MODULE_DATA.Length > 0)
                        ModuleDataParametersChanged();
                    #endregion
                    #endregion
                    break;
                #endregion
                #region SysMessage
                case NotifyWork.NotifyType.SysMessage:
                    SysMessage sysMsg = (SysMessage)notify.Notify;
                    switch (sysMsg.Type)
                    {
                        #region SysMessageType.ForceInsertDataToDataBase
                        case SysMessageType.ForceInsertDataArch:
                            if (sysMsg.Source == Enums.Module.DAQ_Controller)
                            {
                                long? idWork = (long?)sysMsg.Msg;
                                if (ChildWorks.Any(p => p.Key.IdWork.HasValue && p.Key.IdWork.Value == idWork.Value))
                                {
                                    SysMessageEvent.Set();
                                }
                                if (ChildWorks.Any(p => p.Key.IdWork.HasValue && p.Key.IdWork.Value == idWork.Value))
                                {
                                    SysMessageEvent.Set();
                                }
                            }
                            break;
                        #endregion
                        #region SysMessageType.GetDeviceDriverData
                        case SysMessageType.GetDeviceDriverData:
                            OnNotifySysMsg(sysMsg);
                            break;
                        #endregion
                        default:
                            break;
                    }
                    break;
                    #endregion
            }
        }
        #endregion
        #region IWorkPlugIn.ProcessCommand
        object IWorkPlugIn.ProcessCommand(Enums.ModuleCommandType commandType, params object[] args)
        {
            switch (commandType)
            {
                #region SaveState
                case Enums.ModuleCommandType.SaveState:
                    byte[] result = null;
                    try
                    {
                        List<byte[]> serializedData = new List<byte[]>();
                        #region Serialize 'ChildWorks'
                        using (MemoryStream stream = new MemoryStream())
                        {
                            BinaryFormatter bin = new BinaryFormatter();
                            bin.Serialize(stream, ChildWorks);
                            serializedData.Add(stream.GetBuffer());
                        }
                        #endregion

                        serializedData.Add((byte[])OnProcessCommand(Enums.ModuleCommandType.SaveState));

                        #region Serialize 'serializedData'
                        using (MemoryStream stream = new MemoryStream())
                        {
                            new BinaryFormatter().Serialize(stream, serializedData);
                            result = stream.GetBuffer();
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //Logger.Add(new LogData(6201, LogLevel.Fatal, "Exception during serialization!!!\r\nMESSAGE: " + ex.Message + "\r\nSTACK: " + ex.StackTrace));
                    }
                    return result;
                #endregion
                #region LoadState
                case Enums.ModuleCommandType.LoadState:
                    try
                    {
                        InternalInit(args.ToList().GetRange(1, args.Count() - 1).ToArray());

                        #region Deserialize 'serializedData'
                        List<byte[]> serializedData = new List<byte[]>();
                        using (MemoryStream stream = new MemoryStream((byte[])args[0]))
                        {
                            serializedData = (List<byte[]>)(new BinaryFormatter()).Deserialize(stream);
                        }
                        #endregion
                        #region Deserialize 'ChildWorks'
                        using (MemoryStream stream = new MemoryStream(serializedData[0]))
                        {
                            BinaryFormatter bin = new BinaryFormatter();
                            ChildWorks = (CacheDictionary<Work, Work>)bin.Deserialize(stream);
                        }
                        #endregion
                        OnProcessCommand(Enums.ModuleCommandType.LoadState, args[3]);
                    }
                    catch (Exception ex)
                    {
                        //Logger.Add(new LogData(6201, LogLevel.Fatal, "Exception during DeSerialization!!\r\nMESSAGE: " + ex.Message + "\r\nSTACK: " + ex.StackTrace));
                        return false;
                    }
                    return true;
                    #endregion
            }
            return null;
        }
        #endregion

        #region internal ModuleDataParametersChanged
        internal void ModuleDataParametersChanged()
        {
        }
        #endregion
        #region internal CheckAndSaveWorkData
        internal void CheckAndSaveWorkData(DataValue[] workDataFromDB)
        {
            if (Work.IdWorkData.HasValue && Work.WorkData != null)
            {
                bool isDifferent = Work.WorkData.FullOuterJoinJoin(workDataFromDB, a => a, d => d, (a, d) => new { Ad = a, Db = d }).Any(t => t.Ad == null || t.Db == null);
                if (isDifferent)
                {
                    // WorkData rozne od tego co jest w DB, musimy uruchomic akcje z nowymi parametrami i zapisac je w bazie
                    Work.IdWorkData = null;
                }
            }
            if (Work.IdWorkData == null)
                Work.IdWorkData = AddWorkData(Work);
        }
        #endregion

        #region ChangeWorkStatus
        private void ChangeWorkStatus(WorkResult workResult)
        {
            WorkStatusInfo workStatusInfo = new WorkStatusInfo()
            {
                Work = Work,
                WorkStatus = workResult.Status,
                RetryInsertCount = 0
            };

            WorkStatus.Enqueue(workStatusInfo);

            if (workResult != null)
            {
                AddWorkHistoryInfo(workResult);
            }
            if (Work.DiagnosticInfos.Count > 0)
            {
                AddWorkHistoryInfo(Work.DiagnosticInfos.ToArray(), workResult.Status);
                Work.DiagnosticInfos.Clear();
            }
        }
        #endregion
        #region AddWorkHistoryInfo
        public void AddWorkHistoryInfo(WorkResult workResult)
        {
            AddWorkHistoryInfo(workResult.Status, workResult.IdWorkHistoryDescr, workResult.WorkHistoryData);
        }
        public void AddWorkHistoryInfo(long? idDescr, params object[] args)
        {
            AddWorkHistoryInfo(Enums.WorkStatus.Running, idDescr, args);
        }
        public void AddWorkHistoryInfo(Enums.WorkStatus status, long? idDescr, params object[] args)
        {
            OpWorkHistory opWorkHistory = new OpWorkHistory()
            {
                IdWork = Work.IdWork.Value,
                IdWorkStatus = (int)status,
                StartTime = DateTime.UtcNow,
            };
            switch (status)
            {
                case Enums.WorkStatus.Error:
                case Enums.WorkStatus.Partly:
                case Enums.WorkStatus.Aborted:
                case Enums.WorkStatus.Stopped:
                case Enums.WorkStatus.Succeeded:
                    opWorkHistory.EndTime = DateTime.UtcNow;
                    break;
                default:
                    opWorkHistory.EndTime = null;
                    break;
            }
            opWorkHistory.IdDescr = idDescr;
            for (int i = 0; i < args.Length; i++)
                opWorkHistory.DataList.Add(new OpWorkHistoryData() { ArgNbr = i, Value = args[i] });

            WorkHistory.Enqueue(new WorkHistoryInfo() { OpWorkHistory = opWorkHistory });
        }
        public void AddWorkHistoryInfo(DiagnosticInfo info, Enums.WorkStatus status)
        {
            AddWorkHistoryInfo(new DiagnosticInfo[] { info }, status);
        }
        public void AddWorkHistoryInfo(DiagnosticInfo[] infos, Enums.WorkStatus status, long? idWork = null)
        {
            long selectedIdWork = idWork ?? Work.IdWork ?? 0;
            infos.Distinct().OrderBy(info => info.Timestamp).Select(d => WorkHistoryInfo.GetWorkHistoryInfo(d, selectedIdWork, status)).Where(a => a.OpWorkHistory.IdWork > 0).ToList().ForEach(ahi => WorkHistory.Enqueue(ahi));
        }
        #endregion
        #region AddNewWork
        public long? AddNewWork(Work work)
        {
            WorkInfo workInfo = new WorkInfo()
            {
                Work = work,
                RetryInsertCount = 0,
                AddedWorkEvent = new AutoResetEvent(false),
                AddWorkErrorEvent = new AutoResetEvent(false)
            };
            WorkInsert.Enqueue(workInfo);

            WaitHandle[] Events = new WaitHandle[] { workInfo.AddedWorkEvent, StopWorkEvent, AbortWorkEvent, workInfo.AddWorkErrorEvent };
            while (true)
            {
                switch (WaitHandle.WaitAny(Events))
                {
                    #region AddedWorkEvent
                    case 0:
                        return workInfo.Work.IdWork;
                    #endregion
                    #region StopWorkEvent
                    case 1:
                        //OnStop();
                        return null;
                    #endregion
                    #region AbortWorkEvent
                    case 2:
                        return null;
                    #endregion
                    #region AddWorkErrorEvent
                    case 3:
                        return null;
                    #endregion
                    default:
                        return null;
                }
            }
        }
        #endregion
        #region AddWorkData
        public long? AddWorkData(Work work)
        {
            WorkDataInfo workDataInfo = new WorkDataInfo()
            {
                Work = work,
                RetryInsertCount = 0,
                AddedWorkDataEvent = new AutoResetEvent(false),
                AddWorkDataErrorEvent = new AutoResetEvent(false)
            };

            WorkDataInsert.Enqueue(workDataInfo);

            WaitHandle[] Events = new WaitHandle[] { workDataInfo.AddedWorkDataEvent, StopWorkEvent, AbortWorkEvent, workDataInfo.AddWorkDataErrorEvent };
            while (true)
            {
                switch (WaitHandle.WaitAny(Events))
                {
                    #region AddedWorkEvent
                    case 0:
                        return workDataInfo.Work.IdWorkData;
                    #endregion
                    #region StopWorkEvent
                    case 1:
                        //OnStop();
                        return null;
                    #endregion
                    #region AbortWorkEvent
                    case 2:
                        return null;
                    #endregion
                    #region AddWorkDataErrorEvent
                    case 3:
                        return null;
                    #endregion
                    default:
                        return null;
                }
            }
        }
        #endregion

        #region RunWork
        public WorkResult RunWork(Work work, bool waitForEnd = true)
        {
            return RunWorks(new Work[] { work }, waitForEnd);
        }
        #endregion
        #region RunWorks
        public WorkResult RunWorks(Work[] works, bool waitForEnd = true)
        {
            foreach (Work work in works)
            {
                work.IdWorkParent = Work.IdWork;
                ChildWorks.Add(work, null);
                WorksFromWorkQueue.Enqueue(work);
            }

            if (!waitForEnd)
            {
                StartWorkThread();
                return new WorkResult(Work.Status);
            }

            WaitHandle[] Events = new WaitHandle[] { NotifyWorkEvent, StopWorkEvent, AbortWorkEvent };
            while (true)
            {
                Work NotifiedWork = null;
                EventIndex currentEvent = EventIndex.Unknown;
                if (NotifiedWorkQueue.Count > 0)
                {
                    Tuple<Work, EventIndex> itemFromQueue = null;
                    NotifiedWorkQueue.TryDequeue(out itemFromQueue);
                    NotifiedWork = itemFromQueue.Item1;
                    currentEvent = itemFromQueue.Item2;
                }
                else
                {
                    WaitHandle.WaitAny(Events);
                    if (NotifiedWorkQueue.Count > 0)
                    {
                        Tuple<Work, EventIndex> itemFromQueue = null;
                        NotifiedWorkQueue.TryDequeue(out itemFromQueue);
                        NotifiedWork = itemFromQueue.Item1;
                        currentEvent = itemFromQueue.Item2;
                    }
                    else
                        continue;
                }

                switch (currentEvent)
                {
                    #region NotifyWorkEvent
                    case EventIndex.Running:
                        if (ReceivedNotifyEvent(false, ref ChildWorks, NotifiedWork))
                        {
                            if (NotifiedWork.Status == Enums.WorkStatus.Succeeded)
                                return WorkResult.Succeeded;
                            if (NotifiedWork.Status == Enums.WorkStatus.Partly)
                                return WorkResult.Partly;
                            if (NotifiedWork.Status == Enums.WorkStatus.Error)
                                return WorkResult.Error;
                        }
                        break;
                    #endregion
                    #region StopWorkEvent
                    case EventIndex.Stop:
                        if (ReceivedStopEvent(NotifiedWork))
                            return WorkResult.Stopped;
                        break;
                    #endregion
                    #region AbortWorkEvent
                    case EventIndex.Abort:
                        if (ReceivedAbortEvent(NotifiedWork))
                            return WorkResult.Aborted;
                        break;
                    #endregion
                    default:
                        break;
                }
            }
        }
        #endregion

        #region internal StartWorkInsertThread
        internal void StartWorkInsertThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkInsert, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StartWorkInsertThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkInsertThread == null)
                {
                    WorkInsertThread = new Thread(new ThreadStart(WorkInsertProc));
                    WorkInsertThread.IsBackground = true;
                    WorkInsertThread.Start();
                }
                else
                {
                    if (!WorkInsertThread.IsAlive)
                    {
                        Log(EventID.BaseWork.WIThread_ThreadNotAlive, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.IdWork);
                        WorkInsertThread.Abort();
                        WorkInsertThread = new Thread(new ThreadStart(WorkInsertProc));
                        WorkInsertThread.IsBackground = true;
                        WorkInsertThread.Start();
                    }
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkInsert);
            }
        }
        #endregion
        #region internal StopWorkInsertThread
        internal void StopWorkInsertThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkInsert, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StopWorkInsertThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkInsertThread != null && WorkInsertThread.IsAlive)
                {
                    StopWorkInsertEvent.Set();

                    if (!WorkInsertThread.Join(10000))
                    {
                        Log(EventID.BaseWork.WIThread_Aborted, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
                        WorkInsertThread.Abort();
                    }
                    WorkInsertThread = null;
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkInsert);
            }
        }
        #endregion
        #region WorkInsertProc
        internal void WorkInsertProc()
        {
            Log(EventID.BaseWork.WIThread_Start, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);

            Queue<WorkInfo> workInsertToRetryInsert = new Queue<WorkInfo>();

            while (!StopWorkInsertEvent.WaitOne(5, false))
            {
                try
                {
                    WorkInfo[] workInsertToInsert = null;
                    if (WorkInsert.Count >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_ACTION_INFO, 50) ||
                        DateTime.UtcNow.Subtract(WorkInsertLastWriteTime).TotalSeconds >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_FLUSH_TIME_ACTION_INFO, 2))
                    {
                        int worksToTake = WorkInsert.Count;
                        List<WorkInfo> worksTaken = new List<WorkInfo>();
                        while (worksTaken.Count < worksToTake && WorkInsert.IsEmpty == false)
                        {
                            WorkInfo tempWorkInfo = null;
                            if (WorkInsert.TryDequeue(out tempWorkInfo) && tempWorkInfo != null)
                                worksTaken.Add(tempWorkInfo);
                        }
                        workInsertToInsert = worksTaken.ToArray();
                    }

                    if (workInsertToInsert != null)
                    {
                        if (workInsertToInsert.Length > 0)
                        {
                            try
                            {
                                List<Tuple<WorkInfo, OpWork>> works = workInsertToInsert.Select(s => Tuple.Create<WorkInfo, OpWork>(s, s.Work.FromWork())).ToList();
                                DataProvider.SaveWork(works.Select(s => s.Item2).ToArray());
                                foreach (Tuple<WorkInfo, OpWork> tuple in works)
                                {
                                    tuple.Item1.Work.IdWork = tuple.Item2.IdWork;
                                    tuple.Item1.AddedWorkEvent.Set();
                                }
                            }
                            catch (Exception ex)
                            {
                                Log(EventID.BaseWork.WIThread_BulkInsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, workInsertToInsert.Length, ex.Message);

                                foreach (WorkInfo wii in workInsertToInsert)
                                {
                                    wii.RetryInsertCount = 0;
                                    workInsertToRetryInsert.Enqueue(wii);
                                }
                                //continue;
                            }
                        }

                        WorkInsertLastWriteTime = DateTime.UtcNow;
                    }

                    if (workInsertToRetryInsert.Count > 0)
                    {
                        WorkInfo workInfo = workInsertToRetryInsert.Dequeue();
                        try
                        {
                            workInfo.Work.IdWork = DataProvider.SaveWork(workInfo.Work.FromWork());
                            workInfo.AddedWorkEvent.Set();
                        }
                        catch (Exception ex)
                        {
                            if (workInfo.RetryInsertCount >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_ACTION, 5))
                            {
                                Log(EventID.BaseWork.WIThread_InsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_ACTION, 5), workInfo.Work.IdWork, ex.Message);
                                workInfo.AddWorkErrorEvent.Set();
                            }
                            else
                            {
                                workInfo.RetryInsertCount++;
                                workInsertToRetryInsert.Enqueue(workInfo);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.BaseWork.WIThread_Exception, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, ex.Message);
                }
            }
            Log(EventID.BaseWork.WIThread_End, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
        }
        #endregion

        #region internal StartWorkDataInsertThread
        internal void StartWorkDataInsertThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkDataInsert, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StartWorkDataInsertThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkDataInsertThread == null)
                {
                    WorkDataInsertThread = new Thread(new ThreadStart(WorkDataInsertProc));
                    WorkDataInsertThread.IsBackground = true;
                    WorkDataInsertThread.Start();
                }
                else
                {
                    if (!WorkDataInsertThread.IsAlive)
                    {
                        Log(EventID.BaseWork.WDIThread_ThreadNotAlive, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.IdWork);
                        WorkDataInsertThread.Abort();
                        WorkDataInsertThread = new Thread(new ThreadStart(WorkDataInsertProc));
                        WorkDataInsertThread.IsBackground = true;
                        WorkDataInsertThread.Start();
                    }
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkDataInsert);
            }
        }
        #endregion
        #region internal StopWorkDataInsertThread
        internal void StopWorkDataInsertThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkDataInsert, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StopWorkDataInsertThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkDataInsertThread != null && WorkDataInsertThread.IsAlive)
                {
                    StopWorkDataInsertEvent.Set();

                    if (!WorkDataInsertThread.Join(10000))
                    {
                        Log(EventID.BaseWork.WDIThread_Aborted, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
                        WorkDataInsertThread.Abort();
                    }
                    WorkDataInsertThread = null;
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkDataInsert);
            }
        }
        #endregion
        #region WorkDataInsertProc
        internal void WorkDataInsertProc()
        {
            Log(EventID.BaseWork.WDIThread_Start, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);

            Queue<WorkDataInfo> workDataInsertToRetryInsert = new Queue<WorkDataInfo>();

            while (!StopWorkDataInsertEvent.WaitOne(5, false))
            {
                try
                {
                    WorkDataInfo[] workDataInsertToInsert = null;

                    if (WorkDataInsert.Count >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_ACTION_DATA_INFO, 50) ||
                        DateTime.UtcNow.Subtract(WorkDataInsertLastWriteTime).TotalSeconds >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_FLUSH_TIME_ACTION_DATA_INFO, 1))
                    {
                        int worksToTake = WorkDataInsert.Count;
                        List<WorkDataInfo> worksTaken = new List<WorkDataInfo>();
                        while (worksTaken.Count < worksToTake && WorkDataInsert.IsEmpty == false)
                        {
                            WorkDataInfo tempWorkInfo = null;
                            if (WorkDataInsert.TryDequeue(out tempWorkInfo) && tempWorkInfo != null)
                                worksTaken.Add(tempWorkInfo);
                        }
                        workDataInsertToInsert = worksTaken.ToArray();
                    }

                    if (workDataInsertToInsert != null)
                    {
                        if (workDataInsertToInsert.Length > 0)
                        {
                            try
                            {
                                foreach(WorkDataInfo workDataInfo in workDataInsertToInsert)
                                {
                                    OpWorkData[] opWorkData = workDataInfo.Work.WorkData.Select(s => s.FromDataValue()).ToArray();
                                    if (opWorkData != null && opWorkData.Length > 0)
                                    {
                                        DataProvider.SaveWorkData(opWorkData);
                                        workDataInfo.Work.IdWorkData = opWorkData[0].IdWorkData;
                                        workDataInfo.AddedWorkDataEvent.Set();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log(EventID.BaseWork.WDIThread_BulkInsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, workDataInsertToInsert.Length, ex.Message);

                                foreach (WorkDataInfo wdi in workDataInsertToInsert)
                                {
                                    wdi.RetryInsertCount = 0;
                                    workDataInsertToRetryInsert.Enqueue(wdi);
                                }
                            }
                        }

                        WorkDataInsertLastWriteTime = DateTime.UtcNow;
                    }

                    if (workDataInsertToRetryInsert.Count > 0)
                    {
                        WorkDataInfo workDataInfo = workDataInsertToRetryInsert.Dequeue();
                        try
                        {
                            OpWorkData[] opWorkData = workDataInfo.Work.WorkData.Select(s => s.FromDataValue()).ToArray();
                            if (opWorkData != null && opWorkData.Length > 0)
                            {
                                DataProvider.SaveWorkData(opWorkData);
                                workDataInfo.Work.IdWorkData = opWorkData[0].IdWorkData;
                                workDataInfo.AddedWorkDataEvent.Set();
                            }
                        }
                        catch (Exception ex)
                        {
                            if (workDataInfo.RetryInsertCount >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_ACTION_DATA, 5))
                            {
                                Log(EventID.BaseWork.WDIThread_InsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_ACTION_DATA, 5), workDataInfo.Work.IdWork, ex.Message);
                                workDataInfo.AddWorkDataErrorEvent.Set();
                            }
                            else
                            {
                                workDataInfo.RetryInsertCount++;
                                workDataInsertToRetryInsert.Enqueue(workDataInfo);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.BaseWork.WDIThread_Exception, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, ex.Message);
                }
            }
            Log(EventID.BaseWork.WDIThread_End, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
        }
        #endregion

        #region internal StartWorkStatusThread
        internal void StartWorkStatusThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkStatus, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StartWorkStatusThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkStatusThread == null)
                {
                    WorkStatusThread = new Thread(new ThreadStart(WorkStatusProc));
                    WorkStatusThread.IsBackground = true;
                    WorkStatusThread.Start();
                }
                else
                {
                    if (!WorkStatusThread.IsAlive)
                    {
                        Log(EventID.BaseWork.WSThread_ThreadNotAlive, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.IdWork);
                        WorkStatusThread.Abort();
                        WorkStatusThread = new Thread(new ThreadStart(WorkStatusProc));
                        WorkStatusThread.IsBackground = true;
                        WorkStatusThread.Start();
                    }
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkStatus);
            }
        }
        #endregion
        #region internal StopWorkStatusThread
        internal void StopWorkStatusThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkStatus, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StopWorkStatusThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkStatusThread != null && WorkStatusThread.IsAlive)
                {
                    StopWorkStatusEvent.Set();

                    if (!WorkStatusThread.Join(10000))
                    {
                        Log(EventID.BaseWork.WSThread_Aborted, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
                        WorkStatusThread.Abort();
                    }
                    WorkStatusThread = null;
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkStatus);
            }
        }
        #endregion
        #region WorkStatusProc
        internal void WorkStatusProc()
        {
            Log(EventID.BaseWork.WSThread_Start, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);

            Queue<WorkStatusInfo> workStatusToRetryChange = new Queue<WorkStatusInfo>();

            while (!StopWorkStatusEvent.WaitOne(100, false))
            {
                try
                {
                    if (StopWorkStatusEvent.WaitOne(0, false))	// StopEvent
                        break;

                    WorkStatusInfo[] worksStatusToChange = null;
                    if (WorkStatus.Count >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_STATUS_INFO, 30) ||
                        DateTime.UtcNow.Subtract(WorkStatusLastWriteTime).TotalSeconds >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_FLUSH_TIME_STATUS_INFO, 5))
                    {
                        int worksToTake = WorkStatus.Count;
                        List<WorkStatusInfo> worksTaken = new List<WorkStatusInfo>();
                        while (worksTaken.Count < worksToTake && WorkStatus.IsEmpty == false)
                        {
                            WorkStatusInfo tempWorkStatusInfo = null;
                            if (WorkStatus.TryDequeue(out tempWorkStatusInfo) && tempWorkStatusInfo != null)
                                worksTaken.Add(tempWorkStatusInfo);
                        }
                        worksStatusToChange = worksTaken.ToArray();
                    }

                    if (worksStatusToChange != null)
                    {
                        if (worksStatusToChange.Length > 0)
                        {
                            try
                            {
                                DataProvider.ChangeWorkStatus(worksStatusToChange.Select(s => new OpWork() { IdWork = s.Work.IdWork.Value, IdWorkStatus = (int)s.WorkStatus, IdWorkData = s.Work.IdWorkData }).ToArray());
                            }
                            catch (Exception ex)
                            {
                                Log(EventID.BaseWork.WSThread_BulkInsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, worksStatusToChange.Length, ex.Message);

                                foreach (WorkStatusInfo wsi in worksStatusToChange)
                                {
                                    wsi.RetryInsertCount = 0;
                                    workStatusToRetryChange.Enqueue(wsi);
                                }
                                //continue;
                            }
                        }

                        WorkStatusLastWriteTime = DateTime.UtcNow;
                    }

                    if (workStatusToRetryChange.Count > 0)
                    {
                        WorkStatusInfo wsi = workStatusToRetryChange.Dequeue();
                        try
                        {
                            DataProvider.ChangeWorkStatus(new OpWork[] { new OpWork() { IdWork = wsi.Work.IdWork.Value, IdWorkStatus = (int)wsi.WorkStatus, IdWorkData = wsi.Work.IdWorkData } });
                        }
                        catch (Exception ex)
                        {
                            if (wsi.RetryInsertCount >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_CHANGE_STATUS, 5))
                            {
                                Log(EventID.BaseWork.WSThread_InsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_CHANGE_STATUS, 5), wsi.Work.IdWork, ex.Message);
                            }
                            else
                            {
                                wsi.RetryInsertCount++;
                                workStatusToRetryChange.Enqueue(wsi);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.BaseWork.WSThread_Exception, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, ex.Message);
                }
            }
            Log(EventID.BaseWork.WSThread_End, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
        }
        #endregion

        #region internal StartWorkHistoryThread
        internal void StartWorkHistoryThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkHistory, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StartWorkHistoryThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkHistoryThread == null)
                {
                    WorkHistoryThread = new Thread(new ThreadStart(WorkHistoryProc));
                    WorkHistoryThread.IsBackground = true;
                    WorkHistoryThread.Start();
                }
                else
                {
                    if (!WorkHistoryThread.IsAlive)
                    {
                        Log(EventID.BaseWork.WHThread_ThreadNotAlive, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.IdWork);
                        WorkHistoryThread.Abort();
                        WorkHistoryThread = new Thread(new ThreadStart(WorkHistoryProc));
                        WorkHistoryThread.IsBackground = true;
                        WorkHistoryThread.Start();
                    }
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkHistory);
            }
        }
        #endregion
        #region internal StopWorkHistoryThread
        internal void StopWorkHistoryThread()
        {
            if (!Monitor.TryEnter(SyncRootWorkHistory, TimeSpan.FromSeconds(30)))
            {
                Log(EventID.BaseWork.LockTimeout, Work.IdWork, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, Work.Status, "StopWorkHistoryThread LOCK TIMEOUT !!!");
                return;
            }

            try
            {
                if (WorkHistoryThread != null && WorkHistoryThread.IsAlive)
                {
                    StopWorkHistoryEvent.Set();

                    if (!WorkHistoryThread.Join(10000))
                    {
                        Log(EventID.BaseWork.WHThread_Aborted, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
                        WorkHistoryThread.Abort();
                    }
                    WorkHistoryThread = null;
                }
            }
            finally
            {
                Monitor.Exit(SyncRootWorkHistory);
            }
        }
        #endregion
        #region WorkHistoryProc
        internal void WorkHistoryProc()
        {
            Log(EventID.BaseWork.WHThread_Start, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);

            Queue<WorkHistoryInfo> workHistoryToRetryInsert = new Queue<WorkHistoryInfo>();

            while (!StopWorkHistoryEvent.WaitOne(100, false))
            {
                try
                {
                    if (StopWorkHistoryEvent.WaitOne(0, false))	// StopEvent
                        break;

                    WorkHistoryInfo[] workHistoryToInsert = null;
                    if (WorkHistory.Count >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_HISTORY_INFO, 30) ||
                        DateTime.UtcNow.Subtract(WorkHistoryLastWriteTime).TotalSeconds >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_FLUSH_TIME_HISTORY_INFO, 5))
                    {
                        int worksToTake = WorkHistory.Count;
                        List<WorkHistoryInfo> worksTaken = new List<WorkHistoryInfo>();
                        while (worksTaken.Count < worksToTake && WorkHistory.IsEmpty == false)
                        {
                            WorkHistoryInfo tempWorkInfo = null;
                            if (WorkHistory.TryDequeue(out tempWorkInfo) && tempWorkInfo != null)
                                worksTaken.Add(tempWorkInfo);
                        }
                        workHistoryToInsert = worksTaken.ToArray();
                    }

                    if (workHistoryToInsert != null)
                    {
                        if (workHistoryToInsert.Length > 0)
                        {
                            try
                            {
                                WorkHistoryComponent.SaveWorkHistory(DataProvider, workHistoryToInsert.Select(s => s.OpWorkHistory).ToArray());
                            }
                            catch (Exception ex)
                            {
                                Log(EventID.BaseWork.WHThread_BulkInsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, workHistoryToInsert.Length, ex.Message);

                                foreach (WorkHistoryInfo ahi in workHistoryToInsert)
                                {
                                    ahi.RetryInsertCount = 0;
                                    workHistoryToRetryInsert.Enqueue(ahi);
                                }
                                //continue;
                            }
                        }

                        WorkHistoryLastWriteTime = DateTime.UtcNow;
                    }

                    if (workHistoryToRetryInsert.Count > 0)
                    {
                        WorkHistoryInfo whi = workHistoryToRetryInsert.Dequeue();
                        try
                        {
                            WorkHistoryComponent.SaveWorkHistory(DataProvider, new OpWorkHistory[] { whi.OpWorkHistory });
                        }
                        catch (Exception ex)
                        {
                            if (whi.RetryInsertCount >= WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_HISTORY_INFO, 5))
                            {
                                Log(EventID.BaseWork.WHThread_InsertError, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_RETRY_INSERT_HISTORY_INFO, 5), whi.OpWorkHistory.IdWork, whi.OpWorkHistory.IdDescr, ex.Message);
                            }
                            else
                            {
                                whi.RetryInsertCount++;
                                workHistoryToRetryInsert.Enqueue(whi);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.BaseWork.WHThread_Exception, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, ex.Message);
                }
            }
            Log(EventID.BaseWork.WHThread_End, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
        }
        #endregion

        #region internal ReceivedNotifyEvent
        internal bool ReceivedNotifyEvent(bool invokeOnNotify, ref CacheDictionary<Work, Work> childWorks, Work notifiedWork)
        {
            #region All of run works finished with Succeeded
            if (childWorks.All(p => p.Value != null && p.Value.Status == Enums.WorkStatus.Succeeded))
            {
                foreach (KeyValuePair<Work, Work> kvp in childWorks)
                {
                    if (kvp.Value != null)
                    {
                        kvp.Key.Status = kvp.Value.Status; // przepisujemy statusy uruchamianych akcji do odbiorcy
                        kvp.Key.WorkData = kvp.Value.WorkData; // przepisujemy odebrane dane do obiorcy
                    }
                }

                if (invokeOnNotify)
                    OnNotify(notifiedWork);

                childWorks.Clear();
                Work.Status = Enums.WorkStatus.Succeeded;
                return true;
            }
            #endregion
            #region All of run works finished with Error
            if (childWorks.All(p => p.Value != null && p.Value.Status == Enums.WorkStatus.Error))
            {
                foreach (KeyValuePair<Work, Work> kvp in childWorks)
                {
                    if (kvp.Value != null)
                    {
                        kvp.Key.Status = kvp.Value.Status; // przepisujemy statusy uruchamianych akcji do odbiorcy
                        kvp.Key.WorkData = kvp.Value.WorkData; // przepisujemy odebrane dane do obiorcy
                    }
                }

                if (invokeOnNotify)
                    OnNotify(notifiedWork);

                childWorks.Clear();
                Work.Status = Enums.WorkStatus.Error;
                return true;
            }
            #endregion
            #region All of run works finished
            if (childWorks.All(p => p.Value != null && !p.Value.Status.In(Enums.WorkStatus.New, Enums.WorkStatus.Running, Enums.WorkStatus.Waiting)))
            {
                foreach (KeyValuePair<Work, Work> kvp in childWorks)
                {
                    if (kvp.Value != null)
                    {
                        kvp.Key.Status = kvp.Value.Status; // przepisujemy statusy uruchamianych akcji do odbiorcy
                        kvp.Key.WorkData = kvp.Value.WorkData; // przepisujemy odebrane dane do obiorcy
                    }
                }

                if (invokeOnNotify)
                    OnNotify(notifiedWork);

                childWorks.Clear();
                Work.Status = Enums.WorkStatus.Partly;
                return true;
            }
            #endregion

            // One of run works still running
            return false;
        }
        #endregion
        #region internal ReceivedStopEvent
        internal bool ReceivedStopEvent(Work notifiedWork)
        {
            if (Work.IdWork == notifiedWork.IdWork)
            {
                foreach (KeyValuePair<Work, Work> kvp in ChildWorks)
                {
                    kvp.Key.Status = Enums.WorkStatus.Stop;
                    WorksFromWorkQueue.Enqueue(kvp.Key);
                }
                ChildWorks.Clear();

                OnStop();

                Work.Status = Enums.WorkStatus.Stopped;
                return true;
            }
            else
            {
                if (ChildWorks.Keys.All(w => w.Status != Enums.WorkStatus.Running))
                    return true;
            }
            return false;
        }
        #endregion
        #region internal ReceivedAbortEvent
        internal bool ReceivedAbortEvent(Work notifiedWork)
        {
            if (Work.IdWork == notifiedWork.IdWork)
            {
                foreach (KeyValuePair<Work, Work> kvp in ChildWorks)
                {
                    kvp.Key.Status = Enums.WorkStatus.Abort;
                    WorksFromWorkQueue.Enqueue(kvp.Key);
                }
                ChildWorks.Clear();

                Work.Status = Enums.WorkStatus.Aborted;
                return true;
            }
            return false;
        }
        #endregion

        #region internal StartWorkThread
        internal void StartWorkThread()
        {
            if (WorkThread == null)
            {
                WorkThread = new Thread(new ThreadStart(WorkProc));
                WorkThread.IsBackground = true;
                WorkThread.Start();
            }
            else
            {
                if (!WorkThread.IsAlive)
                {
                    WorkThread.Abort();
                    WorkThread = new Thread(new ThreadStart(WorkProc));
                    WorkThread.IsBackground = true;
                    WorkThread.Start();
                }
            }
        }
        #endregion
        #region internal WorkThread
        internal void StopWorkThread()
        {
            if (WorkThread != null && WorkThread.IsAlive)
            {
                StopWorkEvent.Set();

                //czekamy na wątek 10s bo nic sie nie stanie jak poczekamy tyle, a jednak jak serwer ma górke akcji to może nie ogarnąć tego szybciej
                //dzięki temu nie wystąpi tak często błąd "Thread was being aborted"
                if (!WorkThread.Join(10000))
                {
                    Log(EventID.BaseWork.WorkThread_Aborted, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
                    WorkThread.Abort();
                }
                WorkThread = null;
            }
        }
        #endregion
        #region WorkProc
        internal void WorkProc()
        {
            Log(EventID.BaseWork.WorkThread_Start, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);

            WaitHandle[] Events = new WaitHandle[] { NotifyWorkEvent, StopWorkEvent, AbortWorkEvent };
            while (!StopWorkEvent.WaitOne(5, false))
            {
                try
                {
                    Work NotifiedWork = null;
                    EventIndex currentEvent = EventIndex.Unknown;
                    if (NotifiedWorkQueue.Count > 0)
                    {
                        Tuple<Work, EventIndex> itemFromQueue = null;
                        NotifiedWorkQueue.TryDequeue(out itemFromQueue);
                        NotifiedWork = itemFromQueue.Item1;
                        currentEvent = itemFromQueue.Item2;
                    }
                    else
                    {
                        currentEvent = (EventIndex)WaitHandle.WaitAny(Events);
                        if (NotifiedWorkQueue.Count > 0)
                        {
                            Tuple<Work, EventIndex> itemFromQueue = null;
                            NotifiedWorkQueue.TryDequeue(out itemFromQueue);
                            NotifiedWork = itemFromQueue.Item1;
                            currentEvent = itemFromQueue.Item2;
                        }
                        else
                            continue;
                    }

                    switch (currentEvent)
                    {
                        #region NotifyWorkEvent
                        case EventIndex.Running:
                            //foreach (KeyValuePair<Work, Work> kvp in ChildWorks)
                            //  Log(EventID.BaseWork.Debug1, string.Format("CA: {0} - {1}", kvp.Key, kvp.Value));
                            //foreach (KeyValuePair<Work, Work> kvp in ChildWorks)
                            //  Log(EventID.BaseWork.Debug1, string.Format("CA: {0} - {1}", kvp.Key, kvp.Value));
                            //Log(EventID.BaseWork.Debug3, "ASYNC_THREAD NOTIFY", Work.IdWork, NotifiedWork.IdWork);
                            //Log(EventID.BaseWork.Debug1, String.Format("!!!!!!!!!! ASYNC_THREAD NOTIFY: WORK: {0}={1} NOTIFIED: {2}={3} ", Work.IdWork, Work.Status,NotifiedWork.IdWork, NotifiedWork.Status));

                            OnNotify(NotifiedWork);

                            ReceivedNotifyEvent(true, ref ChildWorks, NotifiedWork);
                            break;
                        #endregion
                        #region StopWorkEvent
                        case EventIndex.Stop:
                            OnNotify(NotifiedWork);

                            if (ReceivedStopEvent(NotifiedWork))
                                StopWorkEvent.Set();
                            break;
                        #endregion
                        #region AbortWorkEvent
                        case EventIndex.Abort:
                            OnNotify(NotifiedWork);

                            if (ReceivedAbortEvent(NotifiedWork))
                                AbortWorkEvent.Set();
                            break;
                        #endregion
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.BaseWork.WorkThread_Exception, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name, ex.Message);
                }
            }

            Log(EventID.BaseWork.WorkThread_End, Work.IdWorkType, DataProvider.GetWorkType(Work.IdWorkType)?.Name);
        }
        #endregion
        #region EnqueueWork
        private void EnqueueWork(Work notifiedWork, EventIndex eventIndex)
        {
            NotifiedWorkQueue.Enqueue(new Tuple<Work, EventIndex>(notifiedWork, eventIndex));
            switch (eventIndex)
            {
                case EventIndex.Running:
                    NotifyWorkEvent.Set();
                    break;
                case EventIndex.Stop:
                    StopWorkEvent.Set();
                    break;
                case EventIndex.Abort:
                    AbortWorkEvent.Set();
                    break;
            }
        }
        #endregion


        #region static Log
        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(EventData, Parameters);
        }
        #endregion
        #region override MarshalByRefObjectDisposable
        public override object InitializeLifetimeService()
        {
            return null;
        }
        #endregion
    }
    #endregion

    #region OpWorkExtension
    public static class OpWorkExtension
    {
        public static Work ToWork(this OpWork opWork)
        {
            return new Work()
            {
                IdWork = opWork.IdWork,
                IdWorkType = opWork.IdWorkType,
                Status = (Enums.WorkStatus)opWork.IdWorkStatus,
                IdWorkData = opWork.IdWorkData,
                IdWorkParent = opWork.IdWorkParent,
                //WorkData = opWork.WorkData,
            };
        }

        public static OpWork FromWork(this Work work)
        {
            OpWork opWork = new OpWork()
            {
                IdWorkType = work.IdWorkType,
                IdWorkStatus = (int)work.Status,
                IdWorkParent = work.IdWorkParent,
                IdWorkData = work.IdWorkData,
            };
            if (work.IdWork.HasValue)
                opWork.IdWork = work.IdWork.Value;
            return opWork;
        }
    }
    #endregion
    #region OpWorkDataExtension
    public static class OpWorkDataExtension
    {
        public static DataValue ToDataValue(this OpWorkData opWorkData)
        {
            return new DataValue(opWorkData.IdDataType, opWorkData.IndexNbr, opWorkData.Value);
        }

        public static OpWorkData FromDataValue(this DataValue dataValue)
        {
            OpWorkData opWorkData = new OpWorkData()
            {
                IdWorkData = 0,
                IdDataType = dataValue.IdDataType,
                IndexNbr = dataValue.Index,
                Value = dataValue.Value
            };
            return opWorkData;
        }
    } 
    #endregion
}