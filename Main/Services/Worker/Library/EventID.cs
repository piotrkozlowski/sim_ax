﻿using System;

using IMR.Suite.Common;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Services.Worker.Library
{
    [Serializable]
    public class EventID
    {
        #region BaseWork 5000-5500
        public class BaseWork
        {
            public static readonly LogData InitStarting     = new LogData(5000, LogLevel.Trace, "[{0}][{1}][{2}]-Start initializing work");
            public static readonly LogData InitAddedToDB    = new LogData(5001, LogLevel.Trace, "[{0}][{1},{2}][{3}]-New work added to DB");
            public static readonly LogData InitAddNewError  = new LogData(5002, LogLevel.Error, "[{0}][{1},{2}][{3}]-Could not Add New work into DB");
            public static readonly LogData OnInitError      = new LogData(5003, LogLevel.Error, "[{0}][{1},{2}][{3}]-OnInit() in work returned false");
            public static readonly LogData OnInitException  = new LogData(5004, LogLevel.Error, "[{0}][{1},{2}][{3}]-Exception during OnInit(): {4}");
            public static readonly LogData NoWorkData       = new LogData(5005, LogLevel.Warn,  "[{0}][{1},{2}][{3}]-Work has NO IdWorkData defined and NO WorkData (even one) values");
            public static readonly LogData AddWorkDataError = new LogData(5006, LogLevel.Error, "[{0}][{1},{2}][{3}]-Could not insert WorkData into DB");
            public static readonly LogData InitEnding       = new LogData(5007, LogLevel.Trace, "[{0}][{1},{2}][{3}]-End initializing work with true");
            public static readonly LogData LockTimeout      = new LogData(5009, LogLevel.Fatal, "[{0}][{1},{2}][{3}]-{4} LOCK TIMEOUT !!!");

            public static readonly LogData ExecuteStarting    = new LogData(5020, LogLevel.Trace, "[{0}][{1},{2}][{3}]-Starting execute work");
            public static readonly LogData OnExecuteException = new LogData(5021, LogLevel.Error, "[{0}][{1},{2}][{3}]-Exception during OnExecute(): {4}");
            public static readonly LogData ExecuteEnding      = new LogData(5022, LogLevel.Trace, "[{0}][{1},{2}][{3}]-End execute work with {4}");

            public static readonly LogData NotifiedNew    = new LogData(5030, LogLevel.Trace, "[{0}][{1},{2}][{3}]-Received notify New (in order to obtain child IdWork) from work: ({4},{5}-{6})");
            public static readonly LogData NotifiedStop   = new LogData(5031, LogLevel.Trace, "[{0}][{1},{2}][{3}]-Received notify Stop from work: ({4},{5}-{6})");
            public static readonly LogData NotifiedFinish = new LogData(5032, LogLevel.Trace, "[{0}][{1},{2}][{3}]-Received notify Finish from work: ({4},{5}-{6}) with status: {7}");
            public static readonly LogData NotifiedAbort  = new LogData(5033, LogLevel.Trace, "[{0}][{1},{2}][{3}]-Received notify Abort from action: ({4},{5}-{6})");

            public static readonly LogData WorkThread_Start = new LogData(5800, LogLevel.Trace, "[{0},{1}]-Work Thread start");
            public static readonly LogData WorkThread_End = new LogData(5801, LogLevel.Trace, "[{0},{1}]-Work Thread end");
            public static readonly LogData WorkThread_Aborted = new LogData(5802, LogLevel.Info, "[{0},{1}]-Work Thread aborted");
            public static readonly LogData WorkThread_Exception = new LogData(5803, LogLevel.Fatal, "[{0},{1}]-WorkThread Exception: {2}");

            public static readonly LogData WHThread_Start = new LogData(5850, LogLevel.Trace, "[{0},{1}]-WorkHistory Thread start");
            public static readonly LogData WHThread_End = new LogData(5851, LogLevel.Trace, "[{0},{1}]-WorkHistory Thread end");
            public static readonly LogData WHThread_Aborted = new LogData(5852, LogLevel.Info, "[{0},{1}]-WorkHistory Thread aborted");
            public static readonly LogData WHThread_Exception = new LogData(5853, LogLevel.Fatal, "[{0},{1}]-WorkHistory Thread Exception: {2}");
            public static readonly LogData WHThread_BulkInsertError = new LogData(5854, LogLevel.Error, "[{0},{1}]-WorkHistory BulkInsert Error(Count={2}): {3}");
            public static readonly LogData WHThread_InsertError = new LogData(5855, LogLevel.Fatal, "[{0},{1}]-Could not insert (retries={2}) for IdWork:{3} and IdDescr:{4}-{5}");
            public static readonly LogData WHThread_ThreadNotAlive = new LogData(5856, LogLevel.Error, "[{0},{1}]-WorkHistory Thread is not alive! Restarting thread in new Wokr object: IdWork[{2}]");

            public static readonly LogData GotCIT_ModuleDataChanged = new LogData(5900, LogLevel.Debug, "[{0},{1}]-ModuleData changed: DataType[{2}], Value[{3}], reloading module parameter(s)...");

            public static readonly LogData WSThread_Start = new LogData(5960, LogLevel.Trace, "[{0},{1}]-WorkStatus Thread start");
            public static readonly LogData WSThread_End = new LogData(5961, LogLevel.Trace, "[{0},{1}]-WorkStatus Thread end");
            public static readonly LogData WSThread_Aborted = new LogData(5962, LogLevel.Info, "[{0},{1}]-WorkStatus Thread aborted");
            public static readonly LogData WSThread_Exception = new LogData(5963, LogLevel.Fatal, "[{0},{1}]-WorkStatus Thread Exception: {2}");
            public static readonly LogData WSThread_BulkInsertError = new LogData(5964, LogLevel.Error, "[{0},{1}]-WorkStatus BulkInsert Error(Count={2}): {3}");
            public static readonly LogData WSThread_InsertError = new LogData(5965, LogLevel.Fatal, "[{0},{1}]-Could not change status (retries={2}) for IdWork:{3} - {4}");
            public static readonly LogData WSThread_ThreadNotAlive = new LogData(5966, LogLevel.Error, "[{0},{1}]-WorkStatus Thread is not alive! Restarting thread in new Work object: IdWork[{2}]");

            public static readonly LogData WIThread_Start = new LogData(5970, LogLevel.Trace, "[{0},{1}]-WorkInsert Thread start");
            public static readonly LogData WIThread_End = new LogData(5971, LogLevel.Trace, "[{0},{1}]-WorkInsert Thread end");
            public static readonly LogData WIThread_Aborted = new LogData(5972, LogLevel.Info, "[{0},{1}]-WorkInsert Thread aborted");
            public static readonly LogData WIThread_Exception = new LogData(5973, LogLevel.Fatal, "[{0},{1}]-WorkInsert Thread Exception: {2}");
            public static readonly LogData WIThread_BulkInsertError = new LogData(5974, LogLevel.Error, "[{0},{1}]-WorkInsert BulkInsert Error(Count={2}): {3}");
            public static readonly LogData WIThread_InsertError = new LogData(5975, LogLevel.Fatal, "[{0},{1}]-Could not insert work (retries={2}) for IdWork:{3} - {4}");
            public static readonly LogData WIThread_ThreadNotAlive = new LogData(5976, LogLevel.Error, "[{0},{1}]-WorkInsert Thread is not alive! Restarting thread in new Work object: IdWork[{2}]");

            public static readonly LogData WDIThread_Start = new LogData(5980, LogLevel.Trace, "[{0},{1}]-WorkDataInsert Thread start");
            public static readonly LogData WDIThread_End = new LogData(5981, LogLevel.Trace, "[{0},{1}]-WorkDataInsert Thread end");
            public static readonly LogData WDIThread_Aborted = new LogData(5982, LogLevel.Info, "[{0},{1}]-WorkDataInsert Thread aborted");
            public static readonly LogData WDIThread_Exception = new LogData(5983, LogLevel.Fatal, "[{0},{1}]-WorkDataInsert Thread Exception: {2}");
            public static readonly LogData WDIThread_BulkInsertError = new LogData(5984, LogLevel.Error, "[{0},{1}]-WorkDataInsert BulkInsert Error(Count={2}): {3}");
            public static readonly LogData WDIThread_InsertError = new LogData(5985, LogLevel.Fatal, "[{0},{1}]-Could not insert work data (retries={2}) for IdWork:{3} - {4}");
            public static readonly LogData WDIThread_ThreadNotAlive = new LogData(5986, LogLevel.Error, "[{0},{1}]-WorkDataInsert Thread is not alive! Restarting thread in new Work object: IdWork[{2}]");
        }
        #endregion

        #region FirstWorker            10000-10099
        [Serializable]
        public class FirstWorker
        {
            public static readonly LogData FirstLog = new LogData(10000, LogLevel.Error, "FirstLog");
        }
        #endregion
        #region SecondWorker  10100-10109
        [Serializable]
        public class SecondWorker
        {
            public static readonly LogData SecondLog = new LogData(10100, LogLevel.Error, "SecondLog");
        }
        #endregion

    }
}
