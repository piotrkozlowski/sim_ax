﻿using IMR.Suite.Common;
using System;

namespace IMR.Suite.Services.Worker
{
    public partial class WorkerSrvc : dotService
    {
        #region Members
        Worker worker { get; set; }
        #endregion

        #region Constructor
        public WorkerSrvc()
        {
            m_Srvc = this;

            m_sServiceName = "Worker";
            m_sDisplayName = "IMR.Suite.Services.Worker";
            m_sDescription = "Worker Service";
            m_sDependedOn = new string[] { "MSSQLServer", "MSMQ" };
        }
        public WorkerSrvc(string[] dependedOn) : this()
        {
            m_sDependedOn = dependedOn;
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            try
            {
                Worker.Log(EventID.Worker.ModuleUp, AssemblyWrapper.NameAndVersion, Environment.MachineName);
                worker = new Worker();
                worker.Start();
            }
            catch(Exception ex)
            {
                Worker.Log(EventID.Worker.ModuleUpError, ex.Message);
                if (worker != null)
                    worker.Stop();
                Stop(1);
            }
        }
        #endregion
        #region OnStop
        protected override void OnStop()
        {
            Worker.Log(EventID.Worker.ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
            if (worker != null)
                worker.Stop();
        }
        #endregion
        #region OnShutdown
        protected override void OnShutdown()
        {
            OnStop();
        }
        #endregion

        #region static MainHandlerException
        static void MainHandlerException(object sender, UnhandledExceptionEventArgs args)
        {
            string Message = "MainHandlerExcpetion: ";

            Exception e = (Exception)args.ExceptionObject;

            Message += "\nMessage: ";
            Message += e.Message;
            if (e.InnerException != null)
            {
                Message += "\nInnerMessage: ";
                Message += e.InnerException.Message;
            }
            Message += "\n:StackSource ";
            Message += e.StackTrace;
            Console.WriteLine(Message);
            System.Diagnostics.EventLog.WriteEntry(
                 "ViewsManager", Message, System.Diagnostics.EventLogEntryType.Error);

            System.Environment.Exit(-1);
        }
        #endregion

        #region Entry Function - Main - do not touch
        [MTAThread]
        static void Main(string[] args)
        {
            IMR.Suite.Common.CmdLineArgs cmdLine = new IMR.Suite.Common.CmdLineArgs(args);

            // Instalacja: 
            // 1. IMR.Suite.Services.WorkerSrvc.exe -i -ServiceNoDep    -->Dla serwisu na zdalnej maszynie bez żadnych Dependencies
            // 2. IMR.Suite.Services.WorkerSrvc.exe -i -ServiceNoSqlDep -->Dla serwisu na zdalnej maszynie bez Dependencies do SQL
            // 3. IMR.Suite.Services.WorkerSrvc.exe -i                  -->Dla serwisu gdzie SQL i MSMQ jest zainstalowane

            if (cmdLine["ServiceNoSqlDep"] != null)
                new WorkerSrvc(new string[] { "MSMQ" }).RegisterService(args);
            else if (cmdLine["ServiceNoDep"] == null)
                new WorkerSrvc(new string[] { "MSSQLServer", "MSMQ" }).RegisterService(args);
            else
                new WorkerSrvc(null).RegisterService(args);
        }
        #endregion Entry Function - Main - do not touch
    }
    #region WorkerSrvcInstaller - potrzebne dla 'zewnętrznych' instalatorów jak np. InstallUtil
    [System.ComponentModel.RunInstallerAttribute(true)]
    public class WorkerSrvcInstaller : dotService.dotServiceInstaller
    {
        public WorkerSrvcInstaller() { this.m_Srvc = new WorkerSrvc(); }
    }
    #endregion
}
