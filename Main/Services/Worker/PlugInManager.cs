﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using IMR.Suite.Common;
using IMR.Suite.Services.Worker.Library;

namespace IMR.Suite.Services.Worker
{
    public class PlugInManager
    {
        #region Members
        private CacheDictionary<string, WorkPlugInWrapper> PlugIns = new CacheDictionary<string, WorkPlugInWrapper>();
        private CacheDictionary<IWorkPlugIn, string> PlugInInstances = new CacheDictionary<IWorkPlugIn, string>();

        private FileSystemWatcher PlugInsPathWatcher;
        private string PlugInsPathShort;
        private string PlugInsPath;
        private DateTime LastEventTimeForPlugInsPath;
        #endregion

        #region EventArgs
        #region AbortWorkEventArgs
        [Serializable]
        public class AbortWorkEventArgs : EventArgs
        {
            public readonly IWorkPlugIn PlugIn;

            public AbortWorkEventArgs(IWorkPlugIn plugIn)
            {
                this.PlugIn = plugIn;
            }
        }
        #endregion
        #endregion

        #region Events
        public event EventHandler<AbortWorkEventArgs> AbortRunningWorksEvent;
        #endregion

        #region Constructor
        public PlugInManager(string plugInsPath)
        {
            this.PlugInsPath = this.PlugInsPathShort = plugInsPath;
            if (!System.IO.Path.IsPathRooted(this.PlugInsPath))
                this.PlugInsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, this.PlugInsPath);

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += OnReflectionOnlyAssemblyResolve;
        }
        #endregion
        #region Destructor
        ~PlugInManager()
        {
            try
            {
                Stop();
            }
            catch (Exception) { }
        }
        #endregion

        #region Start
        public void Start()
        {
            PlugInsPathWatcher = new FileSystemWatcher(this.PlugInsPath, "*.dll");
            PlugInsPathWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;

            PlugInsPathWatcher.Changed += OnPlugInChanged;
            PlugInsPathWatcher.Deleted += OnPlugInDeleted;
            PlugInsPathWatcher.Created += OnPlugInCreated;
            PlugInsPathWatcher.Renamed += OnPlugInRenamed;

            LastEventTimeForPlugInsPath = DateTime.Now.Subtract(new TimeSpan(0, 0, 5));
            PlugInsPathWatcher.EnableRaisingEvents = true;
        }
        #endregion
        #region Stop
        public void Stop()
        {
            if (PlugInsPathWatcher != null)
            {
                PlugInsPathWatcher.EnableRaisingEvents = false;
                PlugInsPathWatcher.Changed -= OnPlugInChanged;
                PlugInsPathWatcher.Deleted -= OnPlugInDeleted;
                PlugInsPathWatcher.Created -= OnPlugInCreated;
                PlugInsPathWatcher.Renamed -= OnPlugInRenamed;
                PlugInsPathWatcher = null;
            }

            foreach (KeyValuePair<string, WorkPlugInWrapper> kvp in PlugIns)
            {
                kvp.Value.Unload(true);
            }
        }
        #endregion

        #region CreateInstance
        public IWorkPlugIn CreateInstance(string plugInName, params object[] args)
        {
            if (!PlugIns.ContainsKey(plugInName))
            {
                string[] files = System.IO.Directory.GetFiles(PlugInsPath, "*.dll", SearchOption.TopDirectoryOnly);
                string workPluginFile = files.FirstOrDefault(p => String.Equals(Path.GetFileNameWithoutExtension(p), plugInName, StringComparison.InvariantCultureIgnoreCase));
                if (string.IsNullOrEmpty(workPluginFile))
                    return null;

                PlugIns.GetOrAdd(Path.GetFileNameWithoutExtension(workPluginFile), new WorkPlugInWrapper(workPluginFile));
            }

            IWorkPlugIn plugIn = PlugIns[plugInName].CreateInstance(args);
            PlugInInstances.GetOrAdd(plugIn, plugInName);
            return plugIn;
        }
        #endregion
        #region FreeInstance
        public void FreeInstance(IWorkPlugIn plugIn)
        {
            lock (this)
            {
                if (!PlugInInstances.ContainsKey(plugIn))
                    return;

                string plugInName = PlugInInstances[plugIn];
                if (!PlugIns.ContainsKey(plugInName))
                    return;

                PlugInInstances.Remove(plugIn);
                PlugIns[plugInName].FreeInstance(plugIn);

                plugIn.Work = null;
                if (plugIn is BaseWork)
                {
                   ((BaseWork)plugIn).Dispose();
                }
                plugIn.Clear();

                plugIn.Work = null;
                plugIn = null;
            }
        }
        #endregion

        #region OnAssemblyResolve
        private Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            #region func findAssembliesInDirectory
            Func<string, string, Assembly> findAssembliesInDirectory = new Func<string, string, Assembly>((name, directory) =>
            {
                AssemblyName assemblyName = new AssemblyName(name);
                string assemblyFile = Directory.GetFiles(directory).FirstOrDefault(w => w.Contains(assemblyName.Name));
                if (!string.IsNullOrWhiteSpace(assemblyFile))
                {
                    try
                    {
                        // UWAGA !!!
                        // sprawdzamy tylko nazwe, bez wersji !!!!! (jak będzie potrzeba to 'if (AssemblyName.GetAssemblyName(fileFullName).FullName == assemblyName)')
                        if (AssemblyName.GetAssemblyName(assemblyFile).Name == assemblyName.Name)
                        {
                            // !!! UWAGA !!!!
                            // !!! ładowanie przez 'ręczne' wczytanie pliku jako talicy bajtów ponieważ tylko w ten sposób Framework nie blokuje pliku (AccessDenied przy zmianie, usunięciu) !!!
                            return Assembly.Load(File.ReadAllBytes(assemblyFile));
                        }
                    }
                    catch
                    {
                        // ignored
                    }
                }
                return null;
            });
            #endregion

            Assembly assemblyAlreadyInDomain = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(w => w.FullName == args.Name);
            if (assemblyAlreadyInDomain != null)
                return assemblyAlreadyInDomain;

            return findAssembliesInDirectory(args.Name, PlugInsPath);
        }
        #endregion
        #region OnReflectionOnlyAssemblyResolve
        static Assembly OnReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return Assembly.ReflectionOnlyLoad(args.Name);
        }
        #endregion

        #region AbortRunningActions
        private void AbortRunningActions(string plugInName)
        {
            foreach (KeyValuePair<IWorkPlugIn, string> kvp in PlugInInstances)
            {
                if (kvp.Value == plugInName)
                {
                    EventHandler<AbortWorkEventArgs> handler = AbortRunningWorksEvent;
                    if (handler != null)
                    {
                        handler(this, new AbortWorkEventArgs(kvp.Key));
                    }
                }
            }
        }
        #endregion
        #region NotifyChangeInTable
        public void NotifyChangeInTable(ChangeInTable[] changesInTable)
        {
            foreach (KeyValuePair<IWorkPlugIn, string> kvp in PlugInInstances)
            {
                kvp.Key.Notify(new NotifyWork(changesInTable));
            }
        }
        #endregion
        #region NotifySysMessage
        public void NotifySysMessage(SysMessage sysMessage)
        {
            switch (sysMessage.Type)
            {
                #region default
                default:
                    foreach (KeyValuePair<IWorkPlugIn, string> kvp in PlugInInstances)
                    {
                        kvp.Key.Notify(new NotifyWork(sysMessage));
                    }
                    break;
                    #endregion
            }
        }
        #endregion

        #region OnPlugInChanged
        void OnPlugInChanged(object source, FileSystemEventArgs e)
        {
            // Some apps create multiple events when writing files. We
            // make sure we only catch one by ignoring events less than
            // 1 ms appart.
            TimeSpan duration = DateTime.Now - LastEventTimeForPlugInsPath;
            if (duration.TotalMilliseconds > 1)
            {
                Worker.Log(EventID.PlugInManager.PlugInChanged, e.Name, PlugInsPathShort);

                if (e.ChangeType == WatcherChangeTypes.Changed)
                {
                    string plugInName = Path.GetFileNameWithoutExtension(e.FullPath);
                    if (PlugIns.ContainsKey(plugInName))
                    {
                        // Najpierw abortujemy dzialajace akcje tego PlugIn'a
                        AbortRunningActions(plugInName);
                        // Nastepnie dispose'ujmy
                        PlugIns[plugInName].Unload(false);
                        // A na koncu tworzymy na nowo
                        PlugIns[plugInName] = new WorkPlugInWrapper(e.FullPath);
                    }
                }
            }
            LastEventTimeForPlugInsPath = DateTime.Now;
        }
        #endregion
        #region OnPlugInCreated
        void OnPlugInCreated(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                Worker.Log(EventID.PlugInManager.PlugInCreated, e.Name, PlugInsPathShort);

                string plugInName = Path.GetFileNameWithoutExtension(e.FullPath);
                if (!PlugIns.ContainsKey(plugInName))
                {
                    WorkPlugInWrapper wrapper = new WorkPlugInWrapper(e.FullPath);
                    PlugIns.TryAdd(plugInName, wrapper);
                }
            }
            LastEventTimeForPlugInsPath = DateTime.Now;
        }
        #endregion
        #region OnPlugInDeleted
        void OnPlugInDeleted(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                Worker.Log(EventID.PlugInManager.PlugInDeleted, e.Name, PlugInsPathShort);

                string plugInName = Path.GetFileNameWithoutExtension(e.FullPath);
                if (PlugIns.ContainsKey(plugInName))
                {
                    PlugIns[plugInName].Unload(false);
                    WorkPlugInWrapper dummyWrapper;
                    PlugIns.TryRemove(plugInName, out dummyWrapper);
                }
            }
            LastEventTimeForPlugInsPath = DateTime.Now;
        }
        #endregion
        #region OnPlugInRenamed
        void OnPlugInRenamed(object source, RenamedEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Renamed)
            {
                Worker.Log(EventID.PlugInManager.PlugInRenamed, e.OldName, PlugInsPathShort, e.Name);

                string oldPlugInName = Path.GetFileNameWithoutExtension(e.OldFullPath);
                if (PlugIns.ContainsKey(oldPlugInName))
                {
                    PlugIns[oldPlugInName].Unload(false);
                    WorkPlugInWrapper dummyWrapper;
                    PlugIns.TryRemove(oldPlugInName, out dummyWrapper);
                }

                WorkPlugInWrapper wrapper = new WorkPlugInWrapper(e.FullPath);
                string newPlugInName = Path.GetFileNameWithoutExtension(e.FullPath);
                PlugIns.TryAdd(newPlugInName, wrapper);
            }
            LastEventTimeForPlugInsPath = DateTime.Now;
        }
        #endregion
    }
}
