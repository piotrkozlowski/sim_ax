﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Concurrent;

using IMR.Suite.Common;
using IMR.Suite.Common.MSMQ;
using IMR.Suite.Common.Code;
using IMR.Suite.Common.Threading;

using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

using DW = IMR.Suite.UI.Business.Objects.DW;

using IMR.Suite.Services.Worker.Library;

namespace IMR.Suite.Services.Worker
{
    public class Worker
    {
        #region Members
        private IDataProvider DataProvider { get; set; }
        private string CultureName { get; set; }
        private int TimeZoneOffset = 0;

        #region Queues
        // Kolejka worków do modułu worker
        private MSMQ QueueWorksFromAny = null;

        // Kolejka zmian w DB od Controller
        private MSMQ QueueChangeInTableFromController = null;

        // Kolejka komunikatow systemowych od Controller
        private MSMQ QueueSysMsgFromController = null;
        // Kolejka komunikatow systemowych do Controller
        private MSMQ QueueSysMsgToController = null;
        #endregion

        private string AssemblyName = String.Empty;
        private string PlugInsPath = String.Empty;
        private PlugInManager PlugInManager = null;

        #region WorkerConfig (ModuleData)
        //WORKER_AMOUNT_OF_SIMULTANEOUS_HISTORY_INFO	- ilosc HistoryInfo(zebranych w buf) po ktorej nastapi zapis do DB
        //WORKER_FLUSH_TIME_HISTORY_INFO				- [sec] czas po ktorym nastapi zapis HistoryInfo do DB (niezaleznie od ilosci zebranych w buf)
        //WORKER_RETRY_INSERT_HISTORY_INFO			    - ilosc prob wstawienia HistoryInfo w przypadku blednych insertow
        //WORKER_MAX_THREADS_IN_POLL					- maksymalna liczba watkow jaka moze zostac utworzona podczas obsulgi akcji
        private List<DataValue> WorkerConfig = new List<DataValue>();
        #endregion

        #region SmartThreadPool Members
        public SmartThreadPool MainThreadPool = null;
        #endregion
        #region CheckOldWorksThread Members
        private Thread CheckOldWorksThread = null;
        private readonly ManualResetEvent CheckOldWorksStopEvent = new ManualResetEvent(false);
        int DaysExpiredToAbort = 30;
        int HoursBetweenChecking = 6;
        #endregion
        #region WorksFromWorkThread Members
        private ConcurrentQueue<Work> WorksFromWorkQueue = new ConcurrentQueue<Work>();
        private Thread WorksFromWorkThread = null;
        private readonly ManualResetEvent WorksFromWorkStopEvent = new ManualResetEvent(false);
        #endregion

        private CacheDictionary<long, IWorkPlugIn> RunningWorks = new CacheDictionary<long, IWorkPlugIn>();

        internal Task WorkerTask = null;
        internal readonly ManualResetEvent StopEvent = new ManualResetEvent(false);
        internal DateTime LastWorkedTime = DateTime.UtcNow;
        #endregion
        #region Properties

        #endregion

        #region Constructor
        public Worker()
        {
            AssemblyName = AssemblyWrapper.NameAndVersion;

            // pobranie sciezki do katalogu z PlugIn'ami
            PlugInsPath = ConfigurationManager.AppSettings["PlugInsPath"];
            if (!System.IO.Path.IsPathRooted(PlugInsPath))
                PlugInsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PlugInsPath);
        }
        #endregion
        #region Start
        public void Start()
        {
            #region DBConnection
            DBCommonCORE dbConnectionCore = null;
            DBCommonDW dbConnectionDw = null;
            DBCommonDAQ dbConnectionDaq = null;

            dbConnectionCore = new DBCommonCORE("Suite.CORE", 60, 720);
            if (!dbConnectionCore.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB CORE on server: {0}", dbConnectionCore.DB.Server));
            }
            Log(EventID.Worker.DBConnectionCreated, dbConnectionCore.DB.Server, "CORE");

            dbConnectionDw = new DBCommonDW("Suite.DW", 60, 720); ;
            if (!dbConnectionDw.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DW on server: {0}", dbConnectionDw.DB.Server));
            }
            Log(EventID.Worker.DBConnectionCreated, dbConnectionDw.DB.Server, "DW");

            dbConnectionDaq = new DBCommonDAQ("Suite.DAQ", 60, 720); ;
            if (!dbConnectionDaq.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DAQ on server: {0}", dbConnectionDaq.DB.Server));
            }
            Log(EventID.Worker.DBConnectionCreated, dbConnectionDaq.DB.Server, "DAQ");

            this.DataProvider = new DataProvider(dbConnectionCore, dbConnectionDw, dbConnectionDaq,
                !String.IsNullOrEmpty(this.CultureName) ? Enums.Language.Default.GetLanguage(this.CultureName) : Enums.Language.Default,
                (this.TimeZoneOffset == 0 ? null : (TimeSpan?)TimeSpan.FromMinutes(this.TimeZoneOffset)));

            Log(EventID.Worker.DataProviderCreated);
            #endregion
            #region DataProviderInitialization
            //this.DataProvider.IDeviceDataTypes = new List<long>()
            //    {
            //    };
            #endregion
            #region Słowniki
            System.Threading.Tasks.Task tt = System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                DataProvider.GetAllDescr();
                DataProvider.GetAllUnit();
                DataProvider.GetAllLanguage();
                DataProvider.GetAllReferenceType();
                DataProvider.GetAllDataTypeClass();
                DataProvider.GetAllDataType();
                DataProvider.GetAllMeterTypeClass();
                DataProvider.GetAllMeterType();
                DataProvider.GetAllLocationType();
            });
            System.Threading.Tasks.Task.WaitAll(tt);
            #endregion

            PlugInManager = new PlugInManager(PlugInsPath);
            PlugInManager.AbortRunningWorksEvent += OnAbortRunningWorks;
            PlugInManager.Start();

            #region SmartThreadPool
            STPStartInfo info = new STPStartInfo
            {
                CallToPostExecute = CallToPostExecute.Always,
                IdleTimeout = 5000, //900000, // 15min
                MinWorkerThreads = 10,
                MaxWorkerThreads = 64,
                FillStateWithArgs = true,
                ThreadPriority = ThreadPriority.Normal,
            };
            MainThreadPool = new SmartThreadPool(info);
            MainThreadPool.MaxThreads = WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_MAX_THREADS_IN_POOL, 64);
            MainThreadPool.Start();
            #endregion

            #region CheckOldWorksThread
            DaysExpiredToAbort = WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_DAYS_ACTION_EXPIRED, 30);
            HoursBetweenChecking = WorkerConfig.GetValueOrDefault<int>(DataType.CORE_ACTIONS_CHECK_OLD_ACTIONS_PERIOD, 6);
            CheckOldWorksThread = new Thread(new ThreadStart(CheckOldWorksThreadProc));
            CheckOldWorksThread.Start();
            #endregion
            #region WorksFromWorkThread
            WorksFromWorkThread = new Thread(new ThreadStart(WorksFromWorkThreadProc));
            WorksFromWorkThread.Start();
            #endregion

            #region Queues Init
            QueueWorksFromAny = new MSMQ("WorksFromAny");

            QueueChangeInTableFromController = new MSMQ("ChangeInTableFromController");

            QueueSysMsgFromController = new MSMQ("SysMsgFromController");
            QueueSysMsgToController = new MSMQ("SysMsgToController");
            #endregion

            RestoreState();

            #region Queues Start
            QueueWorksFromAny.ReceiveCompletedEvent += OnRecevieWorkFromAny;
            QueueChangeInTableFromController.ReceiveCompletedEvent += OnReceiveChangeInTableFromController;
            QueueSysMsgFromController.ReceiveCompletedEvent += OnReceiveSysMsgFromController;
            #endregion

            DataProvider.GetAllWorkType();
        }
        #endregion
        #region Stop
        public void Stop()
        {
            SaveState();

            PlugInManager.Stop();
            PlugInManager.AbortRunningWorksEvent -= OnAbortRunningWorks;
            PlugInManager = null;

            #region CheckOldWorksThread
            CheckOldWorksStopEvent.Set();
            if (CheckOldWorksThread != null && CheckOldWorksThread.ThreadState == ThreadState.Running && !CheckOldWorksThread.Join(3000))
            {
                CheckOldWorksThread.Abort();
                Log(EventID.Worker.CheckOldWorksThreadAborted);
            }
            CheckOldWorksThread = null;
            #endregion
            #region WorksFromWorkThread
            WorksFromWorkStopEvent.Set();
            if (WorksFromWorkThread != null && WorksFromWorkThread.ThreadState == ThreadState.Running && !WorksFromWorkThread.Join(3000))
            {
                WorksFromWorkThread.Abort();
                Log(EventID.Worker.WorksFromWorkThreadAborted);
            }
            WorksFromWorkThread = null;
            #endregion

            #region Queues
            if (QueueWorksFromAny != null)
            {
                QueueWorksFromAny.ReceiveCompletedEvent -= OnRecevieWorkFromAny;
                QueueWorksFromAny = null;
            }

            QueueSysMsgToController = null;

            if (QueueChangeInTableFromController != null)
            {
                QueueChangeInTableFromController.ReceiveCompletedEvent -= OnReceiveChangeInTableFromController;
                QueueChangeInTableFromController = null;
            }

            if (QueueSysMsgFromController != null)
            {
                QueueSysMsgFromController.ReceiveCompletedEvent -= OnReceiveSysMsgFromController;
                QueueSysMsgFromController = null;
            }
            QueueSysMsgToController = null;
            #endregion

            #region SmartThreadPool
            if (MainThreadPool != null)
            {
                MainThreadPool.Shutdown();//true, 1000);
                MainThreadPool = null;
            }
            #endregion

            DataProvider.Close();
            DataProvider = null;
        }
        #endregion

        #region PlugIn_Init
        public object PlugIn_Init(object state)
        {
            IWorkPlugIn plugIn = (IWorkPlugIn)state;
            if (plugIn.Init(WorkerConfig, DataProvider, WorksFromWorkQueue))
            {
                Log(EventID.WorkFromAny.NewWorkAdded, plugIn.Work.IdWork, plugIn.Work.IdWorkType, DataProvider.GetWorkType(plugIn.Work.IdWorkType)?.Name, plugIn.Work.Status);

                RunningWorks.Add(plugIn.Work.IdWork.Value, plugIn);

                if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                    Log(EventID.WorkFromAny.ActiveThreadsPeaksMax, MainThreadPool.ActiveThreads, MainThreadPool.MaxThreads);
                NotifyParent(plugIn.Work);
                MainThreadPool.QueueWorkItem(new WorkItemCallback(PlugIn_Execute), plugIn);
            }
            else
            {
                Log(EventID.WorkFromAny.NewWorkInitError, plugIn.Work.IdWork, plugIn.Work.IdWorkType, DataProvider.GetWorkType(plugIn.Work.IdWorkType)?.Name, plugIn.Work.Status);
                RemoveFromRunningAndNotifyParent(plugIn, Enums.WorkStatus.Error);
            }
            return plugIn;
        }
        #endregion
        #region PlugIn_Execute
        public object PlugIn_Execute(object state)
        {
            IWorkPlugIn plugIn = (IWorkPlugIn)state;

            Log(EventID.WorkFromAny.ExecuteWorkStarting, plugIn.Work.IdWork, plugIn.Work.IdWorkType, DataProvider.GetWorkType(plugIn.Work.IdWorkType)?.Name, plugIn.Work.Status);

            plugIn.Execute();

            Log(EventID.WorkFromAny.ExecuteWorkEnded, plugIn.Work.IdWork, plugIn.Work.IdWorkType, DataProvider.GetWorkType(plugIn.Work.IdWorkType)?.Name, plugIn.Work.Status);

            RemoveFromRunningAndNotifyParent(plugIn, plugIn.Work.Status);
            return plugIn;
        }
        #endregion

        #region ProcessReceivedWork
        private void ProcessReceivedWork(Work receivedWork)
        {
            try
            {
                if (receivedWork.Status == Enums.WorkStatus.New || receivedWork.IdWork == null)
                {
                    #region Nowy work
                    Log(EventID.WorkFromAny.GotNewWork, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status, receivedWork.Module, receivedWork.IdOperator);
                    string plugInName = DataProvider.GetWorkType(receivedWork.IdWorkType)?.PluginName;
                    if (string.IsNullOrEmpty(plugInName))
                    {
                        Log(EventID.WorkFromAny.NewWorkPlugInNotConfigured, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status);
                        return;
                    }

                    IWorkPlugIn newPlugInWork = null;
                    try
                    {
                        newPlugInWork = PlugInManager.CreateInstance(Path.GetFileNameWithoutExtension(plugInName), null);
                        if (newPlugInWork == null)
                        {
                            Log(EventID.WorkFromAny.WorkPlugInNotCreated, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.WorkFromAny.CreateWorkPlugInException, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status, ex.Message);
                        return;
                    }

                    Log(EventID.WorkFromAny.NewWorkQueued, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status);
                    #region if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                    if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                    {
                        Log(EventID.WorkFromAny.ActiveThreadsPeaksMax, MainThreadPool.ActiveThreads, MainThreadPool.MaxThreads);
                    }
                    #endregion

                    receivedWork.DiagnosticInfos.UpdateNewItemsModule(Enums.Module.Worker, AssemblyName);
                    newPlugInWork.Work = receivedWork;
                    MainThreadPool.QueueWorkItem(new WorkItemCallback(PlugIn_Init), newPlugInWork);
                    return;
                    #endregion
                }

                if (receivedWork.IdWork != null)
                {
                    #region Work do zatrzymania
                    StopWorkHandler(receivedWork);
                    #endregion
                    #region Work zakonczona
                    Log(EventID.WorkFromAny.GotFinishWork, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status);

                    IWorkPlugIn plugInWork = null;
                    if (RunningWorks.TryGetValue(receivedWork.IdWork.Value, out plugInWork))
                    {
                        Log(EventID.WorkFromAny.NotifyFinishWork, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status);
                        plugInWork.Notify(new NotifyWork(receivedWork));
                    }
                    else
                    {
                        #region if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                        if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                        {
                            Log(EventID.WorkFromAny.ActiveThreadsPeaksMax, MainThreadPool.ActiveThreads, MainThreadPool.MaxThreads);
                        }
                        #endregion
                        // jesli nie ma w RunningWorks to pobieramy status worka z DB 
                        // sprawdzamy czy nie jest czasem "chodzacy" jesli tak to ustawiamy na receivedWork.Status
                        // poniewaz zmiania status w DB moze potrwac (z doswiadczenia nawet najmniejszy update moze dluuugo sie krecic)
                        // to zmiane ta uruchamiamy za pomoca SmartThreadPool jak jedno z zadan
                        MainThreadPool.QueueWorkItem(new WorkItemCallback(CheckAndChangeWorkStatus), Tuple.Create<Work, Enums.WorkStatus>(receivedWork, receivedWork.Status));
                    }
                    return;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log(EventID.WorkFromAny.Exception, ex.Message);
            }
        }
        #endregion
        #region OnRecevieWorkFromAny
        private void OnRecevieWorkFromAny(object sender, MSMQ.ReceiveCompletedArgs e)
        {
            try
            {
                #region Decode and check if msg is Work
                Work receivedWork = Work.DecodeMessage(e.Message);
                if (receivedWork == null)
                {
                    Log(EventID.WorkFromAny.WrongMessageFromAny, e.Message.GetType(), typeof(Work));
                    return;
                }
                #endregion

                Log(EventID.WorkFromAny.GotWorkFromAny, receivedWork.IdWork, receivedWork.IdWorkType, DataProvider.GetWorkType(receivedWork.IdWorkType)?.Name, receivedWork.Status, receivedWork.Module, receivedWork.IdOperator);

                WorksFromWorkQueue.Enqueue(receivedWork);
            }
            catch (Exception ex)
            {
                Log(EventID.WorkFromAny.Exception, ex.Message);
            }
        }
        #endregion
        #region OnReceiveChangeInTableFromController
        void OnReceiveChangeInTableFromController(object sender, MSMQ.ReceiveCompletedArgs e)
        {
            try
            {
                #region Decode and check if msg is ChangeInTable[]
                ChangeInTable[] msgChangeInTables = ChangeInTable.DecodeMessage(e.Message);
                if (msgChangeInTables == null)
                {
                    Log(EventID.ChangeInTable.WrongCITMessageFromController, e.Message.GetType());
                    return;
                }
                #endregion

                // Przekazujemy do pluginow (kazda dllka wyczysci co potrzebuje)
                PlugInManager.NotifyChangeInTable(msgChangeInTables);

                #region ChangeInDataTable
                //ChangeInDataTable[] changesInDataTable = msgChangeInTables.Where(p => p is ChangeInDataTable).Select(s => (ChangeInDataTable)s).ToArray();
                #endregion
                #region ChangeInDictTable
                ChangeInDictTable[] changesInDictTables = msgChangeInTables.Where(p => p is ChangeInDictTable).Select(s => (ChangeInDictTable)s).ToArray();
                #endregion
                #region ChangeInObjectDataTable
                ChangeInObjectDataTable[] changesInObjectDataTables = msgChangeInTables.Where(p => p is ChangeInObjectDataTable).Select(s => (ChangeInObjectDataTable)s).ToArray();

                #region MODULE_DATA
                ChangeInObjectDataTable[] changesInObjectDataTable_MODULE_DATA = changesInObjectDataTables.Where(p => p.TableName == "MODULE_DATA" && p.ObjectDataValue.IdObject == (int)Enums.Module.Worker).ToArray();
                foreach (ChangeInObjectDataTable change in changesInObjectDataTable_MODULE_DATA)
                {
                    Log(EventID.ChangeInTable.GotCIT_ModuleDataChanged, change.ObjectDataValue.Type.IdDataType, change.IsValueNull, change.ObjectDataValue.Value);

                    #region Updated/Inserted
                    if (change.IsValueNull == false)
                    {
                        DataValue dv = WorkerConfig.FirstOrDefault(w => w.IdDataType == change.ObjectDataValue.Type.IdDataType);
                        if (dv != null)
                            WorkerConfig.Remove(dv);
                        WorkerConfig.Add(change.ObjectDataValue);
                    }
                    #endregion
                    #region Deleted
                    else
                    {
                        DataValue dv = WorkerConfig.FirstOrDefault(w => w.IdDataType == change.ObjectDataValue.Type.IdDataType);
                        if (dv != null)
                            WorkerConfig.Remove(dv);
                    }
                    #endregion
                }
                //if (changesInObjectDataTable_MODULE_DATA.Length > 0)
                //    ModuleDataParametersChanged();
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log(EventID.ChangeInTable.ExceptionCITMessageFromController, ex.Message);
            }
        }
        #endregion
        #region OnReceiveSysMsgFromController
        void OnReceiveSysMsgFromController(object sender, MSMQ.ReceiveCompletedArgs e)
        {
            try
            {
                #region Check if msg is SysMessage
                if (!(e.Message is SysMessage))
                {
                    Log(EventID.SysMessage.WrongSysMessageFromController, e.Message.GetType());
                    return;
                }
                #endregion

                SysMessage receivedSysMsg = (SysMessage)e.Message;

                Log(EventID.SysMessage.GotSysMessageFromController, receivedSysMsg.Source, receivedSysMsg.Destination, receivedSysMsg.Type, receivedSysMsg.RequestAck);

                // Przekazujemy do pluginow, moze ktorys oczekuje na zdarzenie systemowe (lub na odpowiedz)
                PlugInManager.NotifySysMessage(receivedSysMsg);
            }
            catch (Exception ex)
            {
                Log(EventID.SysMessage.ExceptionSysMessageFromController, ex.Message);
            }
        }
        #endregion
        #region OnAbortRunningWorks
        void OnAbortRunningWorks(object sender, PlugInManager.AbortWorkEventArgs e)
        {
            foreach (KeyValuePair<long, IWorkPlugIn> kvp in RunningWorks.Where(w => w.Value == e.PlugIn))
            {
                Log(EventID.Worker.AbortingWork, kvp.Key);
                kvp.Value.Notify(new NotifyWork(new Work() { IdWork = kvp.Key, Status = Enums.WorkStatus.Abort }));
            }
        }
        #endregion

        #region CheckAndChangeWorkStatus
        internal object CheckAndChangeWorkStatus(object state)
        {
            Tuple<Work, Enums.WorkStatus> tuple = (Tuple<Work, Enums.WorkStatus>)state;
            Work work = tuple.Item1;
            OpWork opWorkInDB = DataProvider.GetWorkFilter(IdWork: new long[] { work.IdWork.Value }, loadNavigationProperties: true, autoTransaction: false).FirstOrDefault();
            if (opWorkInDB == null)
            {
                // ktos chce zmienic status nieistniejacego worka nawet w DB!!!
                Log(EventID.WorkFromAny.WorkNotExistInDB, work.IdWork, DataProvider.GetWorkType(work.IdWorkType)?.Name, work.Status, work.IdWorkParent);
            }
            else
            {
                if (opWorkInDB.IdWorkStatus.In((int)Enums.WorkStatus.New, (int)Enums.WorkStatus.Running, (int)Enums.WorkStatus.Waiting))
                {
                    Log(EventID.WorkFromAny.WorkInDBStillRun, tuple.Item2, opWorkInDB.IdWork, opWorkInDB.WorkType.Name, (Enums.WorkStatus)opWorkInDB.IdWorkStatus, opWorkInDB.IdWorkParent);

                    work = (Work)ChangeWorkStatus(state);
                }

                // jesli ma 'Parent' to szukamy 'Parent' w RunningWorks i wysylamy do niego 'notify' z receivedWork ('Parent' ma "w sobie" childy i powinien sie zakonczyc)
                NotifyParent(work);
            }
            return work;
        }
        #endregion
        #region ChangeWorkStatus
        internal object ChangeWorkStatus(object state)
        {
            Tuple<Work, Enums.WorkStatus> tuple = (Tuple<Work, Enums.WorkStatus>)state;
            if (tuple.Item1 != null)
            {
                tuple.Item1.Status = tuple.Item2;
                tuple.Item1.IdWork = DataProvider.SaveWork(tuple.Item1.FromWork());

                #region DB.AddWorkHistory
                BaseWork.WorkHistoryInfo workHistoryInfo = new BaseWork.WorkHistoryInfo();
                workHistoryInfo.OpWorkHistory = new OpWorkHistory()
                {
                    IdWork = tuple.Item1.IdWork.Value,
                    IdWorkStatus = (int)tuple.Item2,
                    StartTime = DateTime.UtcNow,
                    EndTime = DateTime.UtcNow
                };
                switch (tuple.Item2)
                {
                    case Enums.WorkStatus.Error:
                        workHistoryInfo.OpWorkHistory.IdDescr = DiagDescrID.Works.Error;
                        break;
                    case Enums.WorkStatus.Aborted:
                        workHistoryInfo.OpWorkHistory.IdDescr = DiagDescrID.Works.Aborted;
                        break;
                    case Enums.WorkStatus.Stopped:
                        workHistoryInfo.OpWorkHistory.IdDescr = DiagDescrID.Works.Stopped;
                        break;
                    case Enums.WorkStatus.Succeeded:
                        workHistoryInfo.OpWorkHistory.IdDescr = DiagDescrID.Works.Succeeded;
                        break;
                    default:
                        workHistoryInfo.OpWorkHistory.IdDescr = DiagDescrID.Works.UnknownState;
                        break;
                }
                workHistoryInfo.OpWorkHistory.DataList = null;

                List<BaseWork.WorkHistoryInfo> diagnosticInfos = new List<BaseWork.WorkHistoryInfo>();
                if (tuple.Item1.DiagnosticInfos.Count > 0)
                {
                    diagnosticInfos.AddRange(tuple.Item1.DiagnosticInfos.Distinct().OrderBy(info => info.Timestamp).ToList().Select(info => BaseWork.WorkHistoryInfo.GetWorkHistoryInfo(info, tuple.Item1.IdWork.Value, tuple.Item2)));
                    tuple.Item1.DiagnosticInfos.Clear();
                }

                WorkHistoryComponent.SaveWorkHistory(DataProvider, diagnosticInfos.Concat(new BaseWork.WorkHistoryInfo[1] { workHistoryInfo }).Select(s => s.OpWorkHistory).ToArray());
                #endregion
            }
            return tuple.Item1;
        }
        #endregion

        #region internal RemoveFromRunningAndNotifyParent
        internal void RemoveFromRunningAndNotifyParent(IWorkPlugIn plugIn, Enums.WorkStatus endStatus)
        {
            NotifyParent(plugIn.Work);

            if (RunningWorks.ContainsKey(plugIn.Work.IdWork.Value))
            {
                RunningWorks.Remove(plugIn.Work.IdWork.Value);
                PlugInManager.FreeInstance(plugIn);
            }
            plugIn = null;
        }
        #endregion
        #region internal NotifyParent
        internal void NotifyParent(Work work)
        {
            IWorkPlugIn parentPlugIn = null;

            if (work.IdWorkParent.HasValue && RunningWorks.TryGetValue(work.IdWorkParent.Value, out parentPlugIn))
                parentPlugIn.Notify(new NotifyWork(work));
        }
        #endregion

        #region Save/Restore State
        [Serializable]
        internal class WorkState
        {
            public Work Work;
            public object State;
        }
        internal const string stateFileName = @"IMR.Suite.Services.Worker.dat";
        #region SaveState
        internal void SaveState()
        {
            //return;
            List<WorkState> runningWorksState = new List<WorkState>();
            foreach (IWorkPlugIn item in RunningWorks.Values)
            {
                try
                {
                    WorkState workState = new WorkState();
                    workState.Work = item.Work;
                    workState.State = item.ProcessCommand(Enums.ModuleCommandType.SaveState);
                    //runningWorksState.Add(workState);
                }
                catch (Exception ex)
                {
                }
            }

            byte[] runningWorksBytes = new byte[0];

            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, runningWorksState);
                    runningWorksBytes = stream.GetBuffer();
                }
                catch (Exception ex)
                { }
            }

            try
            {
                using (Stream stream = File.Open(Path.Combine(Path.GetDirectoryName(AssemblyWrapper.ExecutingAssembly.Location), stateFileName), FileMode.Create, FileAccess.Write))
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(runningWorksBytes.Length);
                    writer.Write(runningWorksBytes);

                    stream.Flush();
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region RestoreState
        internal void RestoreState()
        {
            //return;
            List<WorkState> runningWorksState = new List<WorkState>();

            byte[] runningWorksBytes = new byte[0];

            string fileName = Path.Combine(Path.GetDirectoryName(AssemblyWrapper.ExecutingAssembly.Location), stateFileName);
            if (File.Exists(fileName))
            {
                try
                {
                    // Read byte array(s) for RunningWorks dictionary 
                    using (Stream stream = File.Open(fileName, FileMode.Open))
                    using (BinaryReader reader = new BinaryReader(stream))
                    {
                        int runningWorksBytesLength = reader.ReadInt32();
                        runningWorksBytes = reader.ReadBytes(runningWorksBytesLength);
                    }
                    // Deserialize runningWorksState List from byte array
                    using (MemoryStream stream = new MemoryStream(runningWorksBytes))
                    {
                        BinaryFormatter bin = new BinaryFormatter();
                        var list = bin.Deserialize(stream);
                        runningWorksState = (List<WorkState>)list;
                    }
                }
                catch
                {
                    RunningWorks = new CacheDictionary<long, IWorkPlugIn>();
                }
            }
        }
        #endregion
        #region RestoreWork
        internal IWorkPlugIn RestoreWork(WorkState workState)
        {
            string plugInName = DataProvider.GetWorkType(workState.Work.IdWorkType)?.Name;
            if (string.IsNullOrEmpty(plugInName))
            {
                Log(EventID.WorkState.WorkPlugInNotConfigured, workState.Work.IdWork, workState.Work.IdWorkType, DataProvider.GetWorkType(workState.Work.IdWorkType)?.Name, workState.Work.Status);
                return null;
            }

            IWorkPlugIn newPlugInWork = null;
            try
            {
                newPlugInWork = PlugInManager.CreateInstance(Path.GetFileNameWithoutExtension(plugInName), null);
                if (newPlugInWork == null)
                {
                    Log(EventID.WorkState.WorkPlugInNotCreated, workState.Work.IdWork, workState.Work.IdWorkType, DataProvider.GetWorkType(workState.Work.IdWorkType)?.Name, workState.Work.Status);
                    return null;
                }

                newPlugInWork.Work= workState.Work;
                if ((bool)newPlugInWork.ProcessCommand(Enums.ModuleCommandType.LoadState, workState.State, WorkerConfig, DataProvider, WorksFromWorkQueue))
                    return newPlugInWork;
            }
            catch (Exception ex)
            {
                Log(EventID.WorkState.CreateWorkPlugInException, workState.Work.IdWork, workState.Work.IdWorkType, DataProvider.GetWorkType(workState.Work.IdWorkType)?.Name, workState.Work.Status, ex.Message);
                return null;
            }
            return null;
        }
        #endregion
        #endregion

        #region CheckOldWorksThreadProc
        internal void CheckOldWorksThreadProc()
        {
            try
            {
                Log(EventID.Worker.CheckOldWorksThreadStart);

                while (!CheckOldWorksStopEvent.WaitOne(1000))
                {
                    try
                    {
                        List<OpWork> opWorks = DataProvider.GetWorkFilter(IdWorkStatus: new int[] { (int)Enums.WorkStatus.New, (int)Enums.WorkStatus.Running, (int)Enums.WorkStatus.Unknown, (int)Enums.WorkStatus.Waiting },
                                                                              CreationDate: new TypeDateTimeCode() { DateTime1 = DateTime.UtcNow.Date.AddDays(-DaysExpiredToAbort), Mode = TypeDateTimeCode.ModeType.Lesser }).ToList();
                        foreach (OpWork opWork in opWorks)
                        {
                            while ((MainThreadPool.ActiveThreads + 100) >= MainThreadPool.MaxThreads)
                            {
                                if (CheckOldWorksStopEvent.WaitOne(1000))
                                    return;
                            }
                            opWork.IdWorkStatus = (int)Enums.WorkStatus.Stop;
                            StopWorkHandler(opWork.ToWork());
                        }

                        DateTime dateLastCheck = DateTime.Now;
                        DateTime dateNextCheck = dateLastCheck.AddHours(HoursBetweenChecking);
                        while (!CheckOldWorksStopEvent.WaitOne(1000) && DateTime.Now < dateNextCheck)
                        {
                            dateNextCheck = dateLastCheck.AddHours(HoursBetweenChecking);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.Worker.CheckOldWorksThreadException, ex.Message);
                    }
                }

                if (CheckOldWorksStopEvent.WaitOne(0, false))
                    Log(EventID.Worker.CheckOldWorksStopEventSignaled);

                Log(EventID.Worker.CheckOldWorksThreadEnd);
            }
            catch (Exception ex)
            {
                Log(EventID.Worker.CheckOldWorksThreadException, ex.Message);
            }
        }
        #endregion
        #region WorksFromWorkThreadProc
        internal void WorksFromWorkThreadProc()
        {
            try
            {
                Log(EventID.Worker.WorksFromWorkThreadStart);

                while (!WorksFromWorkStopEvent.WaitOne(10))
                {
                    try
                    {
                        if (WorksFromWorkQueue.Count > 0)
                        {
                            while (WorksFromWorkQueue.IsEmpty == false)
                            {
                                Work tempWork = null;
                                if (WorksFromWorkQueue.TryDequeue(out tempWork) && tempWork != null)
                                {
                                    ProcessReceivedWork(tempWork);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.Worker.WorksFromWorkThreadException, ex.Message);
                    }
                }

                if (WorksFromWorkStopEvent.WaitOne(0, false))
                    Log(EventID.Worker.WorksFromWorkStopEventSignaled);

                Log(EventID.Worker.WorksFromWorkThreadEnd);
            }
            catch (Exception ex)
            {
                Log(EventID.Worker.WorksFromWorkThreadException, ex.Message);
            }
        }
        #endregion
        #region StopWorkHandler
        internal void StopWorkHandler(Work work)
        {
            if (work.Status != Enums.WorkStatus.Stop)
                return;

            IWorkPlugIn plugIn = null;

            Log(EventID.WorkFromAny.GotStopWork, work.IdWork, work.IdWorkType, DataProvider.GetWorkType(work.IdWorkType)?.Name, work.Status, work.Module, work.IdOperator);
            if (RunningWorks.TryGetValue(work.IdWork.Value, out plugIn))
            {
                Log(EventID.WorkFromAny.StopWorkQueued, work.IdWork, work.IdWorkType, DataProvider.GetWorkType(work.IdWorkType)?.Name, work.Status);
                plugIn.Notify(new NotifyWork(work));
            }
            else
            {
                #region if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                if (MainThreadPool.ActiveThreads >= MainThreadPool.MaxThreads)
                    Log(EventID.WorkFromAny.ActiveThreadsPeaksMax, MainThreadPool.ActiveThreads, MainThreadPool.MaxThreads);
                #endregion
                // jesli nie ma RunningWorks to pobieramy status worka z DB 
                // sprawdzamy czy nie jest czasem "chodzacy" jesli tak to ustawiamy na ??aborted??
                // poniewaz zmiania status w DB moze potrwac (z doswiadczenia nawet najmniejszy update moze dluuugo sie krecic)
                // to zmiane ta uruchamiamy za pomoca SmartThreadPool jak jedno z zadan
                MainThreadPool.QueueWorkItem(new WorkItemCallback(CheckAndChangeWorkStatus), Tuple.Create<Work, Enums.WorkStatus>(work, Enums.WorkStatus.Aborted));
            }
        }
        #endregion

        #region static Log
        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(Enums.Module.Worker, EventData, Parameters);
        }
        #endregion
    }
}
