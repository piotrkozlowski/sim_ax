﻿using System;
using System.Linq;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.Services.Worker.Library;

namespace IMR.Suite.Services.Worker.PlugIn
{
	[Serializable]
	public class FooWork : BaseWork
	{
		public override WorkResult OnInit()
		{
			return WorkResult.InitWorkOK;
		}

		public override WorkResult OnExecute()
		{
			return WorkResult.Succeeded;
		}

		public override void OnStop()
		{
		}
	}
}
