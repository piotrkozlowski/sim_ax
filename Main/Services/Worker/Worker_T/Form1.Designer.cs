﻿namespace IMR.Suite.Services.Worker.Worker_T
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.bttn_Work1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bttn_Work1
            // 
            this.bttn_Work1.Location = new System.Drawing.Point(12, 12);
            this.bttn_Work1.Name = "bttn_Work1";
            this.bttn_Work1.Size = new System.Drawing.Size(75, 23);
            this.bttn_Work1.TabIndex = 0;
            this.bttn_Work1.Text = "Work 1";
            this.bttn_Work1.UseVisualStyleBackColor = true;
            this.bttn_Work1.Click += new System.EventHandler(this.bttn_Work1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 146);
            this.Controls.Add(this.bttn_Work1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

		}

        #endregion

        private System.Windows.Forms.Button bttn_Work1;
    }
}

