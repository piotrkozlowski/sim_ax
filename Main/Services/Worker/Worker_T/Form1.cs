﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using System.Windows.Forms;
using IMR.Suite.Common;
using IMR.Suite.Common.MSMQ;

namespace IMR.Suite.Services.Worker.Worker_T
{
	public partial class Form1 : Form
	{
		private MSMQ WorksFromAny = null;

		public Form1()
		{
            WorksFromAny = new MSMQ("WorksFromAny");
			InitializeComponent();
		}

        private void bttn_Work1_Click(object sender, EventArgs e)
        {
            Work work = new Work() { IdWorkType = 1 };
            work.WorkData = new DataValue[] { new DataValue(DataType.ACTION_START_TIME, 0, DateTime.UtcNow) };
            WorksFromAny.Send(work);
        }
    }
}
