﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.Worker
{
    public class EventID
    {
        #region Worker        1000-1099
        public class Worker
        {
            public static readonly LogData ModuleUp            = new LogData(1000, LogLevel.Info, "Module {0} started at {1}.");
            public static readonly LogData ModuleUpError       = new LogData(1001, LogLevel.Fatal, "Initialization failed. Exception: '{0}'.");
            public static readonly LogData ModuleDown          = new LogData(1002, LogLevel.Info, "Module {0} stopped at {1}.");
            public static readonly LogData DBConnectionCreated = new LogData(1003, LogLevel.Info, "Successfully created connection to {0} server {1} database.");
            public static readonly LogData DataProviderCreated = new LogData(1004, LogLevel.Info, "Successfully created DataProvider.");

            public static readonly LogData AbortingWork        = new LogData(1010, LogLevel.Info, "Aborting Work, IdWork={0}");


            public static readonly LogData CheckOldWorksThreadStart       = new LogData(1014, LogLevel.Debug, "CheckOldWorks thread start");
            public static readonly LogData CheckOldWorksThreadEnd         = new LogData(1015, LogLevel.Debug, "CheckOldWorks thread end");
            public static readonly LogData CheckOldWorksThreadException   = new LogData(1016, LogLevel.Fatal, "CheckOldWorksThreadException: {0}");
            public static readonly LogData CheckOldWorksThreadAborted     = new LogData(1017, LogLevel.Info, "CheckOldWorks thread aborted");
            public static readonly LogData CheckOldWorksStopEventSignaled = new LogData(1018, LogLevel.Info, "Stop Event are signaled");

            public static readonly LogData WorksFromWorkThreadStart       = new LogData(1019, LogLevel.Debug, "WorksFromWork thread start");
            public static readonly LogData WorksFromWorkThreadEnd         = new LogData(1020, LogLevel.Debug, "WorksFromWork thread end");
            public static readonly LogData WorksFromWorkThreadException   = new LogData(1021, LogLevel.Fatal, "WorksFromWorkThreadException: {0}");
            public static readonly LogData WorksFromWorkThreadAborted     = new LogData(1022, LogLevel.Info, "WorksFromWork thread aborted");
            public static readonly LogData WorksFromWorkStopEventSignaled = new LogData(1023, LogLevel.Info, "Stop Event are signaled");

        }
        #endregion
        #region WorkFromAny   1100-1199
        public class WorkFromAny
        {
			public static readonly LogData Exception			= new LogData(1100, LogLevel.Fatal, "Exception: {0}");
			public static readonly LogData WrongMessageFromAny	= new LogData(1101, LogLevel.Fatal, "Got wrong message in Queue {0}. Should be {1}");

			public static readonly LogData GotWorkFromAny		= new LogData(1102, LogLevel.Debug, "[{0}][{1},{2}][{3}]-Got action from queue. Module: {4}, IdOperator: {5}");

			public static readonly LogData ActiveThreadsPeaksMax = new LogData(1103, LogLevel.Warn, "ActiveThreads={0} in STP peaks Max={1}");

			public static readonly LogData NewWorkAdded				= new LogData(1110, LogLevel.Info, "[{0}][{1},{2}][{3}]-New work added and initialized ok, queue execute action via STP");
			public static readonly LogData NewWorkInitError			= new LogData(1111, LogLevel.Error, "[{0}][{1},{2}][{3}]-Error durring Init new work, removing from STP and notify parent");
			//public static readonly LogData NewActionScheduled			= new LogData(1112, LogLevel.Info, "[{0}][{1},{2}][{3}]-New action initialized and scheduled, action will start at: {4} ({5})");
			//public static readonly LogData NewActionScheduledAndWait	= new LogData(1113, LogLevel.Info, "[{0}][{1},{2}][{3}]-New action initialized and scheduled for wait for ({4}) or will start at: {5} ({6})");
			//public static readonly LogData NewActionScheduledForWait	= new LogData(1114, LogLevel.Info, "[{0}][{1},{2}][{3}]-New action initialized and wait for ({4}) or will time out at: {5} ({6})");
			//public static readonly LogData NewScheduledActionInitError	= new LogData(1115, LogLevel.Error, "[{0}][{1},{2}][{3}]-Error durring Init Scheduled action, removing from STP and notify parent ({4})");

			public static readonly LogData ExecuteWorkStarting	= new LogData(1120, LogLevel.Debug, "[{0}][{1},{2}][{3}]-Execute work start");
			public static readonly LogData ExecuteWorkEnded		= new LogData(1121, LogLevel.Info, "[{0}][{1},{2}][{3}]-Execute work ended, removing from STP and notify parent");

			public static readonly LogData WorkNotExistInDB	= new LogData(1190, LogLevel.Fatal, "Someone want to chnage status for work witch does NOT exist in DB: Id[{0}] Name[{1}] Status[{2}] IdParent[{3}]");
			public static readonly LogData WorkInDBStillRun	= new LogData(1191, LogLevel.Warn, "Work not exist in STP but in DB still running, changing status to: {0}: Id[{1}] Name[{2}] Status[{3}] IdParent[{4}]");

			public static readonly LogData GotNewWork			      = new LogData(1200, LogLevel.Info, "[{0}][{1},{2}][{3}]-Got request NEW work from queue, Module: {4}, IdOperator: {5}");
			public static readonly LogData NewWorkQueued			  = new LogData(1201, LogLevel.Debug, "[{0}][{1},{2}][{3}]-Request NEW work queued in STP");
			public static readonly LogData NewWorkPlugInNotConfigured = new LogData(1202, LogLevel.Error, "[{0}][{1},{2}][{3}]-PlugIn Work NOT configured !!! NEW work not started");
			public static readonly LogData WorkPlugInNotCreated		  = new LogData(1203, LogLevel.Error, "[{0}][{1},{2}][{3}]-PlugIn Work could NOT create !!! NEW work not started");
			public static readonly LogData CreateWorkPlugInException  = new LogData(1204, LogLevel.Fatal, "[{0}][{1},{2}][{3}]-Exception during create PlugIn Work !!! NEW work not started: {4}");

			public static readonly LogData GotFinishWork		= new LogData(1210, LogLevel.Info, "[{0}][{1},{2}][{3}]-Got FINISH work from queue");
			public static readonly LogData NotifyFinishWork		= new LogData(1211, LogLevel.Debug, "[{0}][{1},{2}][{3}]-Work Finised, notify finish in STP");

			public static readonly LogData GotStopWork			= new LogData(1230, LogLevel.Info, "[{0}][{1},{2}][{3}]-Got request STOP work from queue, Module: {4}, IdOperator: {5}");
			public static readonly LogData StopWorkQueued   	= new LogData(1231, LogLevel.Debug, "[{0}][{1},{2}][{4}]-Request STOP work queued in STP");
			//public static readonly LogData StopWaitingAction		= new LogData(1231, LogLevel.Debug, "[{0}][{1},{2}][{3}]-Request STOP waiting action ({4})");
		}
		#endregion
		#region WorkState     1400-1499
		public class WorkState
		{
			public static readonly LogData WorkPlugInNotConfigured	= new LogData(1400, LogLevel.Error, "[{0}][{1},{2}][{3}]-PlugIn Work NOT configured !!! RESTORED work not started");
			public static readonly LogData WorkPlugInNotCreated		= new LogData(1401, LogLevel.Error, "[{0}][{1},{2}][{3}]-PlugIn Work could NOT create !!! RESTORED work not started");
			public static readonly LogData CreateWorkPlugInException= new LogData(1402, LogLevel.Fatal, "[{0}][{1},{2}][{3}]-Exception during create PlugIn Work !!! RESTORED work not started: {4}");
			public static readonly LogData ActiveThreadsPeaksMax	= new LogData(1403, LogLevel.Warn, "ActiveThreads={0} in STP peaks Max={1}");
		}
        #endregion
        #region ChangeInTable 3000-3999
        public class ChangeInTable
		{
			public static readonly LogData ExceptionCITMessageFromController	= new LogData(3000, LogLevel.Fatal, "Exception: {0}");
			public static readonly LogData WrongCITMessageFromController		= new LogData(3001, LogLevel.Fatal, "Got wrong message type[{0}] from Controller in ChangeInTable queue");
			public static readonly LogData GotCIT_ModuleDataChanged				= new LogData(3002, LogLevel.Info, "ModuleData was changed: DataType={0}, IsValueNull={1}, Value={2}");
		}
		#endregion
		#region SysMessage    4000-4099
		public class SysMessage
		{
			public static readonly LogData WrongSysMessageFromController	= new LogData(4000, LogLevel.Fatal, "Got wrong message type[{0}] from Controller in SysMsg queue");
			public static readonly LogData GotSysMessageFromController		= new LogData(4001, LogLevel.Trace, "Got SysMsg from {0} to {1} type {2} with RequestAck {3}");
			public static readonly LogData ExceptionSysMessageFromController= new LogData(4002, LogLevel.Fatal, "Exception: {0}");
		}
		#endregion
        #region PlugInManager 2000-2049
        public class PlugInManager
        {
            public static readonly LogData LoadingPlugIns   = new LogData(2000, LogLevel.Info, "{0} PlugIn(s) in path {1}, loading..");
            public static readonly LogData LoadingException = new LogData(2001, LogLevel.Fatal, "Exception durring loading PlugIn[{0}]: {1}");

            public static readonly LogData PlugInChanged    = new LogData(2005, LogLevel.Info, "PlugIn[{0}] in Path[{1}] changed, reloading...");
            public static readonly LogData PlugInCreated    = new LogData(2006, LogLevel.Info, "New PlugIn[{0}] in Path[{1}] detected, loading...");
            public static readonly LogData PlugInDeleted    = new LogData(2007, LogLevel.Info, "PlugIn[{0}] in Path[{1}] deleted, unloading...");
            public static readonly LogData PlugInRenamed    = new LogData(2008, LogLevel.Info, "PlugIn[{0}] in Path[{1}] renamed to [{2}], unloading/loading...");
        }
        #endregion
    }
}
