﻿using System;
using System.ServiceModel;
using System.Threading;
using ATGUpdater;
using IMR.Suite.Common;

namespace IMR.Suite.PcapTransmission
{
    public class ATGUpdaterSrvc : dotService
	{
        ManualResetEvent StopEvent;
        //UpdaterCommon Engine;

		#region constructor
        public ATGUpdaterSrvc(string[] dependedOn)
            : base()
		{
			m_Srvc = this;

            m_sServiceName = "IMR.ATGUpdater";
            m_sDisplayName = "IMR.ATGUpdater";
            m_sDescription = "IMR ATGUpdater Service";
			m_sDependedOn = dependedOn;
		}
		#endregion

		#region OnStart
		protected override void OnStart(string[] args)
		{
            base.OnStart(args);

			try
			{
                Logger.Add(EventID.SchedulerModule.Begin);

                #region To pewnie bedzie potrzebne w przyszlosci
                //string Source = m_sServiceName;
                //if (!EventLog.SourceExists(Source))
                //	EventLog.CreateEventSource(Source, "AIUT.IMR");

                //EventLog.Source = Source;
                //EventLog.Log = EventLog.LogNameFromSourceName(Source, ".");
                #endregion

                StopEvent = new ManualResetEvent(false);
                StopEvent.Reset();
                InitializeWCFConnection();
                Logger.Add(EventID.ATGUpdaterLog.ModuleUp, AssemblyWrapper.NameAndVersion, Environment.MachineName);
			}
			catch (Exception ex)
			{
				// zatrzymanie watkow serwisu

                Logger.Add(EventID.ATGUpdaterLog.ModuleUpError, ex.Message);

				Stop(1);
			}
			finally
			{
			}
		}
		#endregion
		#region OnStop
		protected override void OnStop()
		{
            Logger.Add(EventID.SchedulerModule.Stopping);

            StopEvent.Set();
			base.OnStop();
            svh.Close();
            //Engine.StopListen();

			// zatrzymanie watkow serwisu

            //AddToLog(EventID.PcapFD.ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
            Logger.Add(EventID.ATGUpdaterLog.ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
		}
		#endregion
		#region OnShutdown
		protected override void OnShutdown()
		{
			OnStop();
		}
        #endregion

        private bool _connectionExists = false;
        private Uri _baseAddress = new Uri("net.pipe://localhost/IMRATGUpdater/Service");
        private ServiceHost svh;

        private void InitializeWCFConnection()
        {
            try
            {
                svh = new ServiceHost(typeof(IATGUpdater));
                svh.AddServiceEndpoint(typeof(IIATGUpdater), new NetTcpBinding(), "net.tcp://localhost:8000/IMRATGUpdater");
                svh.Open();
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.ATGUpdaterLog.WCFException, ex.Message, ex.StackTrace);
                svh.Abort();
            }
        }

        #region Entry Function - Main - do not touch
        [MTAThread]
		static void Main(string[] args)
		{
            CmdLineArgs cmdLine = new CmdLineArgs(args);
            if (cmdLine["MSMQ"] != null)
                new ATGUpdaterSrvc(new string[] { "MSMQ" }).RegisterService(args);
            else
                new ATGUpdaterSrvc(null).RegisterService(args);
		}
		#endregion Entry Function - Main - do not touch
	}
}
