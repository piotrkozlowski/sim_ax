﻿using IMR.Suite.Common;

namespace ATGUpdater
{
    public class EventID
	{
        public class ATGUpdaterLog
		{
			public static readonly LogData ModuleUp		= new LogData(1000, LogLevel.Info, "Module {0} started at {1}");
			public static readonly LogData ModuleDown	= new LogData(1001, LogLevel.Info, "Module {0} stopped at {1}");
			public static readonly LogData ModuleUpError= new LogData(1002, LogLevel.Fatal, "Exception: Initialization failed ({0}).");
            public static readonly LogData MatlabCommad = new LogData(1003, LogLevel.Info, "Matlab Execute Command: {0} with output: {1}.");
            public static readonly LogData DictMissingKey = new LogData(1004, LogLevel.Error, "Server not present in dictionary: {0}");
            public static readonly LogData ReceiveError = new LogData(1005, LogLevel.Error, "TCP Receiver Error: {0}");
            public static readonly LogData LoadingDictError = new LogData(1006, LogLevel.Error, "Loading from server dictionary error: {0}");
            public static readonly LogData WCFException = new LogData(1007, LogLevel.Error, "Error initializing WCF connection with FPConsole. Exception: {0}. {1}");
            public static readonly LogData Debug = new LogData(1008, LogLevel.Debug, "Debug: {0}");
        }

        #region SchedulerModule EventID: 30xx
        public class SchedulerModule
        {
            public static readonly LogData Begin = new LogData(3000, LogLevel.Info, "Started");
            public static readonly LogData Stopped = new LogData(3002, LogLevel.Info, "Stopped");
            public static readonly LogData Aborted = new LogData(3003, LogLevel.Info, "Aborted");
            public static readonly LogData Stopping = new LogData(3004, LogLevel.Info, "Stopping");

            public static readonly LogData Exception = new LogData(3005, LogLevel.Fatal, "Exception:{0}; StackTrace:{1}");
            public static readonly LogData TimestampParseError = new LogData(3006, LogLevel.Error, "Errorr parsing Start Time. Default value will be picked {0}. Error message: {1}. Value {2}");

            public static readonly LogData CheckVersionLocal = new LogData(3007, LogLevel.Info, "Newest version in local folder: {0}; Installed: {1}");
            public static readonly LogData CheckVersionServer = new LogData(3008, LogLevel.Info, "Newest version on server: {0}; Installed: {1}");
            public static readonly LogData DownloadUpdateError = new LogData(3009, LogLevel.Error, "Error during downloading update files: {0}");
            public static readonly LogData UpdateStarted = new LogData(3010, LogLevel.Info, "Application update started");
            public static readonly LogData UpdateStartError = new LogData(3011, LogLevel.Error, "Error during starting update: {0}");

            public static readonly LogData AutoUpdaterGetUpdateUri = new LogData(3048, LogLevel.Debug, "Get update URI defined: {0}");
            public static readonly LogData AutoUpdaterFilesNotFound = new LogData(3049, LogLevel.Error, "Files for update not found on server. Aborting operation...");
            public static readonly LogData AutoUpdaterStartingAutoupdate = new LogData(3050, LogLevel.Debug, "Starting auto update. {0}");
            public static readonly LogData AutoUpdaterLastFileDownloaded = new LogData(3051, LogLevel.Trace, "Downloaded file: {0}");
        }
        #endregion
    }
}
