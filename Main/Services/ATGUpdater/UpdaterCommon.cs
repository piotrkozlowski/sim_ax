﻿using System.Threading;

namespace ATGUpdater
{
    public class UpdaterCommon
    {
        private readonly ManualResetEvent StopEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent UpdateEvent = new ManualResetEvent(false);
        private Thread MainThread = null;
        bool JobInProgress = false;
        private string UpdatePath;
        private int AutoUpdateFrequency;
        private string LocationSN;
        private int AutoUpdateUseLocal;
        private string AutoUpdatePath;

        public UpdaterCommon()
        {
            MainThread = new Thread(new ThreadStart(MainThreadProcess));
            MainThread.Start();
        }

        public void StopListen()
        {
            StopEvent.Set();
        }

        #region Callback
        public void UpdaterCallback(string UpdatePath, int AutoUpdateFrequency, string LocationSN, int AutoUpdateUseLocal, string AutoUpdatePath)
        {
            if (JobInProgress)
                return;

            this.UpdatePath = UpdatePath;
            this.AutoUpdateFrequency = AutoUpdateFrequency;
            this.LocationSN = LocationSN;
            this.AutoUpdateUseLocal = AutoUpdateUseLocal;
            this.AutoUpdatePath = AutoUpdatePath;

            UpdateEvent.Set();
        }
        #endregion
        
        #region MainThreadProcess
        private void MainThreadProcess()
        {
            WaitHandle[] Events = new WaitHandle[2] { StopEvent, UpdateEvent };

            while (true)
            {
                int waitResult = WaitForEvent(Events, 60000);
                if (waitResult == 0)
                    return;
                else if (waitResult == 1)
                {
                    UpdateEvent.Reset();

                    JobInProgress = true;

                    AutoUpdater.DoAutoUpdate_Silent(new AutoUpdater.AutoUpdateParams(UpdatePath, AutoUpdateFrequency, LocationSN, AutoUpdateUseLocal, AutoUpdatePath));

                    JobInProgress = false;
                }
            }
        }
        #endregion

        #region WaitForEvent
        private int WaitForEvent(WaitHandle[] Events, int sleepTime)
        {
            return WaitHandle.WaitAny(Events, sleepTime, false);
        }
        #endregion
        public void TryUpdate(string UpdatePath, int AutoUpdateFrequency, string LocationSN, int AutoUpdateUseLocal, string AutoUpdatePath)
        {
            if (!JobInProgress)
            {
                JobInProgress = true;

                AutoUpdater.DoAutoUpdate_Silent(new AutoUpdater.AutoUpdateParams(UpdatePath, AutoUpdateFrequency, LocationSN, AutoUpdateUseLocal, AutoUpdatePath));

                JobInProgress = false;
            }
        }
    }
}
