﻿using System.ServiceModel;

namespace ATGUpdater
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class IATGUpdater : IIATGUpdater
    {
        bool JobInProgress = false;

        UpdaterCommon common = new UpdaterCommon();

        public void TryUpdate(string UpdatePath, int AutoUpdateFrequency, string LocationSN, int AutoUpdateUseLocal, string AutoUpdatePath)
        {
            common.UpdaterCallback(UpdatePath, AutoUpdateFrequency, LocationSN, AutoUpdateUseLocal, AutoUpdatePath);
        }
    }
}
