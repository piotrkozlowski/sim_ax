﻿using System.ServiceModel;

namespace ATGUpdater
{
    [ServiceContract]
    public interface IIATGUpdater
    {
        [OperationContract]
        void TryUpdate(string UpdatePath, int AutoUpdateFrequency, string LocationSN, int AutoUpdateUseLocal, string AutoUpdatePath);
    }
}
