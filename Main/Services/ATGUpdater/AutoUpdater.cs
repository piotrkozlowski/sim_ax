﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using IMR.Suite.Common;
using Microsoft.Win32;

namespace ATGUpdater
{
    //! Application (ATG) and service (ATGService) updating module
    public class AutoUpdater
    {
        #region Members
        //! Version installed
        private static Version installedVersion = null;
        //! Main locking primitive
        private static readonly object autoupd_Lock = new object();
        #endregion

        #region Constructor
        //! Dummy Constructor
        internal AutoUpdater()
        {
        }
        #endregion

        #region Download Functions
        //! Deletes given directory with subfolders and files
        private static void DeleteDirectory(string dir)
        {
            try
            {
                string[] files = Directory.GetFiles(dir);
                string[] dirs = Directory.GetDirectories(dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string child in dirs)
                {
                    DeleteDirectory(child);
                }

                Directory.Delete(dir, false);
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
            }
        }


        //! Downloads entire distribution
        public static System.Net.WebClient GetApplicationFilesFromServer(out int totalBytes, string getUpdateUri, int autoUpdateFrequency, string locationSN)
        {
            // Exception handling
            string lastFileDloaded = "<operation not started>";

            try
            {
                totalBytes = 0;
                
                if (String.IsNullOrEmpty(getUpdateUri))
                    return null;

                string foundVersionFolderName;
                bool selectiveVersionFound = GetSelectiveUpdateVersionFolderName(out foundVersionFolderName, getUpdateUri, locationSN);
                if (selectiveVersionFound)
                {
                    getUpdateUri += "/ArchVersions/";
                    getUpdateUri += foundVersionFolderName;
                }
                else
                {
                    switch (autoUpdateFrequency)
                    {
                        case 2:
                            getUpdateUri += "/Often";
                            break;
                        case 1:
                            getUpdateUri += "/Sometimes";
                            break;
                        case 0:
                        default:
                            //default folder - rarely
                            break;
                    }
                }
                Logger.Add(EventID.SchedulerModule.AutoUpdaterGetUpdateUri, getUpdateUri);

                string tempPath = Path.Combine(Path.GetTempPath(), "IMRATGUpdate");
                if (Directory.Exists(tempPath))
                    DeleteDirectory(tempPath);
                Directory.CreateDirectory(tempPath);
                Directory.CreateDirectory(Path.Combine(tempPath, "References"));

                string msiTempPath = Path.Combine(tempPath, "IMRATGSetup.msi");
                string exeTempPath = Path.Combine(tempPath, "setup.exe");

                #region Get sizes
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(getUpdateUri + "/");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string html = reader.ReadToEnd();
                        Regex regex = new Regex("(?<size>\\d+) <A HREF=\"(\\w|\\.|/|%20)*IMRATGSetup.msi\">IMRATGSetup.msi</A>", RegexOptions.IgnoreCase);
                        Match match = regex.Match(html);
                        if (match.Success)
                            totalBytes += int.Parse(match.Groups["size"].Value);
                        else
                            return null;
                    }
                }

                List<string> dllsToDownload = new List<string>();
                request = (HttpWebRequest)WebRequest.Create(getUpdateUri + "/References/");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string html = reader.ReadToEnd();
                        Regex regex = new Regex("(?<size>\\d+) <A HREF=\"(\\w|\\.|/|%20)*\">(?<name>(\\w|\\.)*)</A>", RegexOptions.IgnoreCase);
                        MatchCollection matches = regex.Matches(html);
                        if (matches.Count > 0)
                        {
                            foreach (Match match in matches)
                            {
                                if (match.Success)
                                {
                                    totalBytes += int.Parse(match.Groups["size"].Value);
                                    dllsToDownload.Add(match.Groups["name"].Value);
                                }
                            }
                        }
                    }
                }
                #endregion

                //download
                var client = new System.Net.WebClient();

                // make it synchronous
                // main install files
                lastFileDloaded = "IMRATGSetup.msi";
                client.DownloadFile(new Uri(getUpdateUri + "/IMRATGSetup.msi"), msiTempPath);
                Logger.Add(EventID.SchedulerModule.AutoUpdaterLastFileDownloaded, lastFileDloaded);
                lastFileDloaded = "setup.exe";
                client.DownloadFile(new Uri(getUpdateUri + "/setup.exe"), exeTempPath);
                Logger.Add(EventID.SchedulerModule.AutoUpdaterLastFileDownloaded, lastFileDloaded);

                // references
                foreach (string fileToDLName in dllsToDownload)
                {
                    lastFileDloaded = fileToDLName;
                    client.DownloadFile(new Uri(getUpdateUri + "/References/" + fileToDLName), Path.Combine(Path.GetTempPath(), "IMRATGUpdate", "References", fileToDLName));
                    Logger.Add(EventID.SchedulerModule.AutoUpdaterLastFileDownloaded, lastFileDloaded);
                }

                return client;
            }
            catch (Exception)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.DownloadUpdateError, lastFileDloaded);
                totalBytes = 0;
                return null;
            }
        }

        private static bool GetSelectiveUpdateVersionFolderName(out string foundVersionFolderName, string getUpdateUri, string locationSN)
        {
            foundVersionFolderName = "";
            string versionsTempPath = Path.Combine(Path.GetTempPath(), "FPCVersion.xml");
            
            if (!string.IsNullOrEmpty(locationSN))
            {
                // Download version file
                using (var client = new System.Net.WebClient())
                    client.DownloadFile(getUpdateUri + "/FPCVersion.xml", versionsTempPath);

                XmlDocument versionsFile = new XmlDocument();
                versionsFile.Load(versionsTempPath);
                XmlNode selectiveUpdateNode = versionsFile.GetElementsByTagName("SelectiveUpdate").Item(0);
                if (selectiveUpdateNode.InnerText != "true")
                {
                    return false;
                }
                XmlNodeList nodeList = versionsFile.GetElementsByTagName("StationVersion");
                foreach (XmlNode node in nodeList)
                {
                    if (node.FirstChild.InnerText.Equals(locationSN))
                    {
                        foundVersionFolderName = node.LastChild.InnerText;
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region Private Functions
        //! Returns update type
        private static bool ShouldIUseLocalUpdate(int useLocalGP)
        {
            try
            {
                // Combine temporary path
                if (useLocalGP > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                return false;
            }
        }

        //! Returns path where we should look for update
        private static string GetLocalUpdateSourcePath(string autoUpdPath)
        {
            try
            {
                // Combine temporary path
                char[] trimCh = { ' ', '\r', '\n', '\\', '/' };
                
                if (autoUpdPath.Length <= 0)
                    return "Z:\\InvalidPath";
                else return autoUpdPath;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                return "Z:\\InvalidPath";
            }
        }

        //! Checks server application version
        private static Version CheckServerVersion(string checkUpdateUri, int autoUpdateFrequency, string locationSN)
        {
            // Returned value
            Version serverVersion = null;

            try
            {
                // Combine temporary path
                string tempPath = Path.Combine(Path.GetTempPath(), "NewestVersion.txt");

                if (string.IsNullOrEmpty(checkUpdateUri))
                    return null;

                string foundVersion;
                bool selectiveVersionFound = CheckSelectiveUpdateVersion(out foundVersion, checkUpdateUri, locationSN);

                if (selectiveVersionFound)
                {
                    serverVersion = new Version(foundVersion);
                }
                else
                {
                    switch (autoUpdateFrequency)
                    {
                        case 2:
                            checkUpdateUri += "/Often";
                            break;
                        case 1:
                            checkUpdateUri += "/Sometimes";
                            break;
                        case 0:
                        default:
                            //default folder - rarely
                            break;
                    }
                    // Download version file
                    using (var client = new System.Net.WebClient())
                        client.DownloadFile(checkUpdateUri + "/NewestVersion.txt", tempPath);
                    serverVersion = new Version(File.ReadAllText(tempPath));
                }

                // Verbose version
                if (installedVersion.CompareTo(serverVersion) < 0)
                    Logger.Add(EventID.SchedulerModule.CheckVersionServer, serverVersion, installedVersion);

                // Return version
                return serverVersion;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                return null;
            }
        }

        private static bool CheckSelectiveUpdateVersion(out string foundVersion, string checkUpdateUri, string locationSN)
        {
            foundVersion = "";
            string versionsTempPath = Path.Combine(Path.GetTempPath(), "FPCVersion.xml");

            if (!string.IsNullOrEmpty(locationSN))
            {
                // Download version file
                using (var client = new System.Net.WebClient())
                    client.DownloadFile(checkUpdateUri + "/FPCVersion.xml", versionsTempPath);

                XmlDocument versionsFile = new XmlDocument();
                versionsFile.Load(versionsTempPath);
                XmlNode selectiveUpdateNode = versionsFile.GetElementsByTagName("SelectiveUpdate").Item(0);
                if (selectiveUpdateNode.InnerText == "true")
                {
                    XmlNodeList nodeList = versionsFile.GetElementsByTagName("StationVersion");

                    foreach (XmlNode node in nodeList)
                    {
                        if (node.FirstChild.InnerText.Equals(locationSN))
                        {
                            foundVersion = node.FirstChild.NextSibling.InnerText;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //! Checks local version
        private static Version CheckLocalVersion(string AutoUpdatePath)
        {
            // Returned value
            Version localVersion = null;
            
            try
            {
                // Combine temporary path
                string versionFilePath = Path.Combine(GetLocalUpdateSourcePath(AutoUpdatePath), "NewestVersion.txt");

                // Read version file
                localVersion = new Version(File.ReadAllText(versionFilePath));

                // Verbose version
                if (installedVersion.CompareTo(localVersion) < 0)
                    Logger.Add(EventID.SchedulerModule.CheckVersionLocal, localVersion, installedVersion);

                // Return version
                return localVersion;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                return null;
            }
        }

        //! Downloads version stored on the server
        private static bool DownloadServerVersion(string updatePath, int autoUpdateFrequency, string locationSN)
        {
            // Following method doesn't throw
            int totalBytes = -1;
            if ((GetApplicationFilesFromServer(out totalBytes, updatePath, autoUpdateFrequency, locationSN) == null) || (totalBytes <= 0))
            {
                Logger.Add(EventID.SchedulerModule.AutoUpdaterFilesNotFound);
                return false;
            }

            // Success!
            return true;
        }

        //! Kills application
        private static bool KillClient()
        {
            bool result = false;

            try
            {
                // Invoke taskkill
                System.Diagnostics.Process procKill = System.Diagnostics.Process.Start("taskkill", "/F /IM IMR-ATG.exe");
                
                int i = 0;

                while (!result && i < 3)
                {
                    result = procKill.WaitForExit(5000);
                    i++;
                }
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
            }

            return result;
        }

        //! Kills application
        private static bool KillService()
        {
            bool result = false;

            try
            {
                // Invoke taskkill
                System.Diagnostics.Process procKill = System.Diagnostics.Process.Start("taskkill", "/F /IM IMRATGService.exe");

                int i = 0;

                while (!result && i < 3)
                {
                    result = procKill.WaitForExit(60000);
                    i++;
                }
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
            }

            return result;
        }

        //! Spawns right instalation
        private static bool SpawnSilentMSI(bool useLocal, string AutoUpdatePath)
        {
            try
            {
                // MSI file path
                string MSIFilePath;
                if (useLocal)
                    MSIFilePath = Path.Combine(GetLocalUpdateSourcePath(AutoUpdatePath), "IMRATGSetup.msi");
                else
                    MSIFilePath = Path.Combine(Path.GetTempPath(), "IMRATGUpdate", "IMRATGSetup.msi");

                // Run process MSI process
                if (System.Diagnostics.Process.Start(MSIFilePath, "/qn /quiet") == null)
                    return false;

                // Update log and return
                Logger.Add(EventID.SchedulerModule.UpdateStarted);
                return true;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.UpdateStartError, ex.Message);
                return false;
            }
        }

        //! Spawns right instalation
        private static bool SpawnSilentEXE(bool useLocal, string AutoUpdatePath)
        {
            try
            {
                // EXE file path
                string EXEFilePath;
                if (useLocal)
                    EXEFilePath = Path.Combine(GetLocalUpdateSourcePath(AutoUpdatePath), "setup.exe");
                else
                    EXEFilePath = Path.Combine(Path.GetTempPath(), "IMRATGUpdate", "setup.exe");

                // Run process EXE process
                if (System.Diagnostics.Process.Start(EXEFilePath, "/qn /quiet") == null)
                    return false;

                // Update log and return
                Logger.Add(EventID.SchedulerModule.UpdateStarted);
                return true;
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.UpdateStartError, ex.Message);
                return false;
            }
        }

        //! Checks if update is needed (whether it's local or global
        private static bool IsAutoUpdateNeeded(out bool useLocal, int autoUpdateUseLocal, string AutoUpdatePath, string UpdatePath, int autoUpdateFrequency, string locationSN)
        {
            // Local or global
            useLocal = ShouldIUseLocalUpdate(autoUpdateUseLocal);

            // Get version
            Version updateVersion = (useLocal) ? CheckLocalVersion(AutoUpdatePath) : CheckServerVersion(UpdatePath, autoUpdateFrequency, locationSN);
            if (installedVersion.CompareTo(updateVersion) >= 0)
                return false; // Update not needed

            // Otherwise - update is needed
            return true;
        }

        //! Autoupdate - silent version (internal version)
        private static bool DoAutoUpdate_Silent_Internal(AutoUpdateParams inParams)
        {
            // Is update needed
            bool useLocal;
            if (IsAutoUpdateNeeded(out useLocal, inParams.AutoUpdateUseLocal, inParams.AutoUpdatePath, inParams.UpdatePath, inParams.AutoUpdateFrequency, inParams.LocationSN) == false)
                return true; // Update not needed

            // Get files if we use remote version
            if (useLocal == false)
            {
                // Download failed
                if (DownloadServerVersion(inParams.UpdatePath, inParams.AutoUpdateFrequency, inParams.LocationSN) == false)
                    return false; // Update failed
            }

            // Kill application
            bool appResult = KillClient();

            bool serviceResult = true;

            // Kill service
            try
            {
                System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("IMRATGService");
                sc.Stop();
            }
            catch (Exception ex)
            {
                // Something went wrong - failure
                serviceResult = KillService();
                if (!serviceResult)
                {
                    Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                    return false;
                }
            }
            
            // Spawn right instalation
            //if (SpawnSilentMSI(useLocal) == false)
            if (SpawnSilentEXE(useLocal, inParams.AutoUpdatePath) == false)
                return false; // Update failed; we don't restart client, successful autoupdate will bring him up
            
            // We are done (success!)
            return true;
        }


        #endregion

        #region Public Functions
        //! Defines operation parameters
        public class AutoUpdateParams
        {
            public string UpdatePath;
            public int AutoUpdateFrequency;
            public string LocationSN;
            public int AutoUpdateUseLocal;
            public string AutoUpdatePath;

            //! No parameters - default autoupdate from server (comes usually from scheduler)
            public AutoUpdateParams(string UpdatePath, int AutoUpdateFrequency, string LocationSN, int AutoUpdateUseLocal, string AutoUpdatePath)
            {
                this.UpdatePath = UpdatePath;
                this.AutoUpdateFrequency = AutoUpdateFrequency;
                this.LocationSN = LocationSN;
                this.AutoUpdateUseLocal = AutoUpdateUseLocal;
                this.AutoUpdatePath = AutoUpdatePath;
            }
        }

        private static Version GetInstalledVersion(out bool dllInconclusive)
        {
            RegistryView registryView = new RegistryView();
            string serviceName = "IMRATGService";

            string registryPath = @"SYSTEM\CurrentControlSet\Services\" + serviceName;
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView).OpenSubKey(registryPath);
            if (key == null)
            {
                    throw new ArgumentException("Non-existent service: " + serviceName, "serviceName");
            }
            string value = key.GetValue("ImagePath").ToString();
            key.Close();
            if (value.StartsWith("\""))
            {
                value = Regex.Match(value, "\"([^\"]+)\"").Groups[1].Value;
            }

            dllInconclusive = false;
            
            //Get file version of service executubles
            Version serviceVersion = new Version(FileVersionInfo.GetVersionInfo(Environment.ExpandEnvironmentVariables(value)).FileVersion);

            //Get service innstall folder
            string serviceFolder = Path.GetDirectoryName(value);

            string[] libraries = Directory.GetFiles(serviceFolder, "*.dll", SearchOption.TopDirectoryOnly);

            foreach(string lib in libraries)
            {
                Version libVersion = new Version(FileVersionInfo.GetVersionInfo(Environment.ExpandEnvironmentVariables(lib)).FileVersion);

                if (serviceVersion != libVersion)
                {
                    dllInconclusive = true;
                    break;
                }
            }

            return serviceVersion;
        }

        //! Autoupdate - silent version
        public static bool DoAutoUpdate_Silent(AutoUpdateParams inParams)
        {
            Logger.Add(EventID.SchedulerModule.AutoUpdaterStartingAutoupdate, "Silent version");
            // We do not throw, we do not overlap
            try
            {
                lock (autoupd_Lock)
                {
                    bool dllInconclusive;
                    installedVersion = GetInstalledVersion(out dllInconclusive);
                    return DoAutoUpdate_Silent_Internal(inParams);
                }
            }
            catch (Exception ex)
            {
                // Something went wrong - suppress
                Logger.Add(EventID.SchedulerModule.Exception, ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
    }
}
