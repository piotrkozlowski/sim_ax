﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.ViewsManager
{
    #region TableSection
    public class TableSection : ConfigurationSection
    {
        [ConfigurationProperty("Tables", IsDefaultCollection = false),
         ConfigurationCollection(typeof(TableCollection), AddItemName = "Table", ClearItemsName = "clearTables", RemoveItemName = "removeTables")]
        public TableCollection Tables
        {
            get { return this["Tables"] as TableCollection; }
        }
    }
    #endregion
    #region Table
    public class Table : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name
        {
            get { return this["Name"] as string; }
        }

        [ConfigurationProperty("Columns", IsRequired = false, IsDefaultCollection = false),
         ConfigurationCollection(typeof(ColumnCollection), AddItemName = "Column", ClearItemsName = "clearColumns", RemoveItemName = "removeColumns")]
        public ColumnCollection Columns
        {
            get { return this["Columns"] as ColumnCollection; }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
    #endregion
    #region ColumnElement
    public class Column : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name
        {
            get { return this["Name"] as string; }
        }
        [ConfigurationProperty("IdDataType", IsRequired = true)]
        public long IdDataType
        {
            get { return Convert.ToInt64(this["IdDataType"]); }
        }
        [ConfigurationProperty("IsPrimaryKey", IsRequired = false, DefaultValue = false)]
        public bool IsPrimaryKey
        {
            get { return Convert.ToBoolean(this["IsPrimaryKey"]); }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
    #endregion
    #region TableCollection
    public class TableCollection : ConfigurationElementCollection
    {
        public Table this[int index]
        {
            get { return base.BaseGet(index) as Table; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }
        public new Table this[string key]
        {
            get { return base.BaseGet(key) as Table; }
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new Table();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Table)element).Name;
        }
    }
    #endregion
    #region ColumnCollection
    public class ColumnCollection : ConfigurationElementCollection
    {
        public Column this[int index]
        {
            get { return base.BaseGet(index) as Column; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }
        public new Column this[string key]
        {
            get { return base.BaseGet(key) as Column; }
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new Column();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Column)element).Name;
        }
    }
    #endregion
}
