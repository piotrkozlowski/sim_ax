﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;


namespace IMR.Suite.Services.ViewsManager
{
    public partial class ViewsManagerSrvc : dotService
    {
        #region Members

        ViewsManager viewsManager { get; set; }

        #endregion

        #region Constructor
        public ViewsManagerSrvc()
        {
            m_Srvc = this;

            m_sServiceName = "ViewsManager";
            m_sDisplayName = "IMR.Suite.Services.ViewsManager";
            m_sDescription = "ViewsManager Service";
            m_sDependedOn = new string[] { "MSSQLServer", "MSMQ" };
        }
        public ViewsManagerSrvc(string[] dependedOn) : this()
        {
            m_sDependedOn = dependedOn;
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            try
            {
                ViewsManager.Log(EventID.ViewsManager.ModuleUp, AssemblyWrapper.NameAndVersion, Environment.MachineName);
                viewsManager = new ViewsManager();
                viewsManager.Start();
            }
            catch(Exception ex)
            {
                ViewsManager.Log(EventID.ViewsManager.ModuleUpError, ex.Message);
                if (viewsManager != null)
                    viewsManager.Stop();
                Stop(1);
            }
        }
        #endregion
        #region OnStop
        protected override void OnStop()
        {
            ViewsManager.Log(EventID.ViewsManager.ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
            if (viewsManager != null)
                viewsManager.Stop();
        }
        #endregion
        #region OnShutdown
        protected override void OnShutdown()
        {
            OnStop();
        }
        #endregion

        #region static MainHandlerException
        static void MainHandlerException(object sender, UnhandledExceptionEventArgs args)
        {
            string Message = "MainHandlerExcpetion: ";

            Exception e = (Exception)args.ExceptionObject;

            Message += "\nMessage: ";
            Message += e.Message;
            if (e.InnerException != null)
            {
                Message += "\nInnerMessage: ";
                Message += e.InnerException.Message;
            }
            Message += "\n:StackSource ";
            Message += e.StackTrace;
            Console.WriteLine(Message);
            System.Diagnostics.EventLog.WriteEntry(
                 "ViewsManager", Message, System.Diagnostics.EventLogEntryType.Error);

            System.Environment.Exit(-1);
        }
        #endregion

        #region Entry Function - Main - do not touch
        [MTAThread]
        static void Main(string[] args)
        {
            IMR.Suite.Common.CmdLineArgs cmdLine = new IMR.Suite.Common.CmdLineArgs(args);

            // Instalacja: 
            // 1. ESBDriver.exe -i -ServiceNoDep    -->Dla serwisu na zdalnej maszynie bez żadnych Dependencies
            // 2. ESBDriver.exe -i -ServiceNoSqlDep -->Dla serwisu na zdalnej maszynie bez Dependencies do SQL
            // 3. ESBDriver.exe -i                  -->Dla serwisu gdzie SQL i MSMQ jest zainstalowane

            if (cmdLine["ServiceNoSqlDep"] != null)
                new ViewsManagerSrvc(new string[] { "MSMQ" }).RegisterService(args);
            else if (cmdLine["ServiceNoDep"] == null)
                new ViewsManagerSrvc(new string[] { "MSSQLServer", "MSMQ" }).RegisterService(args);
            else
                new ViewsManagerSrvc(null).RegisterService(args);
        }

        #endregion Entry Function - Main - do not touch
    }
    #region ViewsManagerSrvcInstaller - potrzebne dla 'zewnętrznych' instalatorów jak np. InstallUtil
    [System.ComponentModel.RunInstallerAttribute(true)]
    public class ViewsManagerSrvcInstaller : dotService.dotServiceInstaller
    {
        public ViewsManagerSrvcInstaller() { this.m_Srvc = new ViewsManagerSrvc(); }
    }
    #endregion
}
