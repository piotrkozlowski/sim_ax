﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Services.Common.Components;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

using IMR.Suite.Services.Common.ImrWcfCallbackServiceReference;
using SESSION = IMR.Suite.Services.Common.ImrWcfCallbackServiceReference.SESSION;
using SESSION_STATUS = IMR.Suite.Services.Common.ImrWcfCallbackServiceReference.SESSION_STATUS;
using DBCollectorClient = IMR.Suite.Services.Common.ImrWcfCallbackServiceReference.DBCollectorClient;

using Action = IMR.Suite.Common.Action;
using System.Threading.Tasks;
using System.Threading;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.Services.ViewsManager
{
    public class ViewsManager
    {
        #region Members

        private IDataProvider DataProvider { get; set; }
        private string CultureName { get; set; }
        private int TimeZoneOffset = 0;

#if DEBUG
        int MaxOperationBatch = 100;
#else
        int MaxOperationBatch = 10;
#endif

        Task ViewOperationProceedTask = null;
        DateTime LastViewOperationProceedTime = DateTime.UtcNow;
        private readonly ManualResetEvent ViewOperationProceedStopEvent = new ManualResetEvent(false);

        private Queue<OpViewOperation> ViewOperationQueue { get; set; }

        private readonly ManualResetEvent DBCollectorKeepAliveStopEvent = new ManualResetEvent(false);
        private Task DBCollectorKeepAliveTask = null;

        private int ViewsManagerStatusTimerIntevral = 60 * 1000;
        private System.Timers.Timer ViewsManagerStatusTimer = null;
        private System.Diagnostics.PerformanceCounter CurrentRamPerformanceCounter = null;
        private System.Diagnostics.PerformanceCounter CurrentCPUPerformanceCounter = null;

        #region Cache

        // <server, <key, obj>>
        private Dictionary<int, Dictionary<int, OpSimCard>> SimCardCache = new Dictionary<int, Dictionary<int, OpSimCard>>();
        private Dictionary<int, Dictionary<long, OpLocation>> LocationCache = new Dictionary<int, Dictionary<long, OpLocation>>();
        private Dictionary<int, Dictionary<long, OpDevice>> DeviceCache = new Dictionary<int, Dictionary<long, OpDevice>>();
        private Dictionary<int, Dictionary<long, UI.Business.Objects.CORE.OpAction>> ActionCache = new Dictionary<int, Dictionary<long, UI.Business.Objects.CORE.OpAction>>();
        private Dictionary<int, Dictionary<int, OpActor>> ActorCache = new Dictionary<int, Dictionary<int, OpActor>>();
        private Dictionary<int, Dictionary<long, UI.Business.Objects.DW.OpAlarm>> AlarmCache  = new Dictionary<int, Dictionary<long, UI.Business.Objects.DW.OpAlarm>>();
        private Dictionary<int, Dictionary<long, UI.Business.Objects.DW.OpAlarmEvent>> AlarmEventCache = new Dictionary<int, Dictionary<long, UI.Business.Objects.DW.OpAlarmEvent>>();
        private Dictionary<int, Dictionary<long, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection>> DeviceConnectionCache = new Dictionary<int, Dictionary<long, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection>>();
        private Dictionary<int, Dictionary<int, OpDistributor>> DistributorCache = new Dictionary<int, Dictionary<int, OpDistributor>>();
        private Dictionary<int, Dictionary<int, OpIssue>> IssueCache = new Dictionary<int, Dictionary<int, OpIssue>>();
        private Dictionary<int, Dictionary<long, OpMeter>> MeterCache = new Dictionary<int, Dictionary<long, OpMeter>>();
        private Dictionary<int, Dictionary<int, OpOperator>> OperatorCache = new Dictionary<int, Dictionary<int, OpOperator>>();
        private Dictionary<int, Dictionary<int, IMR.Suite.UI.Business.Objects.DW.OpReport>> ReportCache = new Dictionary<int, Dictionary<int, IMR.Suite.UI.Business.Objects.DW.OpReport>>();
        private Dictionary<int, Dictionary<long, OpRoute>> RouteCache = new Dictionary<int, Dictionary<long, OpRoute>>();
        private Dictionary<int, Dictionary<int, OpTask>> TaskCache = new Dictionary<int, Dictionary<int, OpTask>>();
        private Dictionary<int, Dictionary<int, OpTariff>> TariffCache = new Dictionary<int, Dictionary<int, OpTariff>>();

        // Ten słownik ma za zadanie dostarczać klucze pobranych obiektów
        // by uniknąć pobierania wszystkiego z bazy.
        // Wartości z tego słownika są wykorzystywane w metodach Filter z DataProvider.
        // <id ref type, <id imr server, <PK>>>
        private Dictionary<int, Dictionary<int, HashSet<long>>> PrimaryKeyCache = new Dictionary<int, Dictionary<int, HashSet<long>>>();

        #endregion

        #endregion
        #region Properties

        /// <summary>
        ///  Używane przed inicjalizacją. Określa max liczbę wierszy inicjalizowanych w jednym przejściu
        ///  views managera.
        /// </summary>
        private const string InitializationBatchSizeConfigurationKey = "InitializationBatchSize";

        private const string ViewsManagerOperatorLogin = "ViewsManager";
        private const string DataColumn = "DATA_COLUMN";
        private string ViewsManagerOperatorPassword { get; set; }

        private List<OpView> Views { get; set; }
        private Dictionary<string, OpImrServer> Servers { get; set; }

        private List<Table> TableDefinition { get; set; }
        // Ten słownik mapuje datatypy, które mają faktycznie inni odpowiednik, np.
        // <DEVICE_CONNECTION, <HELPER_PARENT_SERIAL_NBR, SERIAL_NBR>>
        private Dictionary<Enums.Tables, Dictionary<long, long>> HelperDataTypeMappings { get; set; }
        // <Type, <IdDataType, <HelperName, FieldInfo>>>
        private Dictionary<Type, Dictionary<long, Tuple<string, System.Reflection.FieldInfo>>> ReflectedFields { get; set; }

        private bool EmailNotificationsEnabled { get; set; }
        private List<string> EmailRecipients { get; set; }
        private string ViewsManagerHostIdentification { get; set; }

        #endregion

        #region Constructor

        public ViewsManager()
        {
            this.ViewsManagerOperatorPassword = null;
            EmailRecipients = new List<string>();

            foreach (string key in ConfigurationManager.AppSettings.Keys)
            {
                try
                {
                    switch (key)
                    {
                        case "ViewsManagerOperatorPassword":
                            ViewsManagerOperatorPassword = ConfigurationManager.AppSettings["ViewsManagerOperatorPassword"].ToString();
                            break;
                        case "MaxOperationBatch":
                            MaxOperationBatch = int.Parse(ConfigurationManager.AppSettings["MaxOperationBatch"].ToString());
                            break;
                        case "ViewsManagerStatusTimerIntevral":
                            ViewsManagerStatusTimerIntevral = int.Parse(ConfigurationManager.AppSettings["ViewsManagerStatusTimerIntevral"].ToString());
                            break;
                        case "EmailNotificationsEnabled":
                            EmailNotificationsEnabled = int.Parse(ConfigurationManager.AppSettings["EmailNotificationsEnabled"].ToString()) == 1;
                            break;
                        case "EmailRecipients":
                            EmailRecipients.AddRange(ConfigurationManager.AppSettings["EmailRecipients"].ToString().Split(';'));
                            break;
                        case "EmailPassword":
                            EmailComponent.HashedPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.ExceptionOccured, ex.ToString());
                }
            }

            #region Machine identification

            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<b>ViewsManager version:</b> " + System.Reflection.Assembly.GetAssembly(this.GetType()).GetName().Version);

                sb.AppendLine("<b>ViewsManager host identifiaction:</b>");
                sb.AppendLine("Machine name: " + Environment.MachineName);
                sb.AppendLine("User name: " + Environment.UserName);

                try
                {
                    sb.AppendLine("User domain name: " + Environment.UserDomainName);
                }
                catch (Exception iex)
                {
                    Log(EventID.ViewsManager.ExceptionOccured, iex.ToString());
                }

                try
                {
                    var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                    foreach (var ip in host.AddressList)
                    {
                        if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            sb.AppendLine("IP address: " + ip.ToString());
                        }
                    }
                }
                catch (Exception iex)
                {
                    Log(EventID.ViewsManager.ExceptionOccured, iex.ToString());
                }

                ViewsManagerHostIdentification = sb.ToString();
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ExceptionOccured, ex.ToString());
            }

            #endregion

            this.Servers = new Dictionary<string, OpImrServer>();
        }

        #endregion
        #region Start

        public void Start()
        {
            #region HelperDataTypeMappings
            HelperDataTypeMappings = new Dictionary<Enums.Tables, Dictionary<long, long>>();
            HelperDataTypeMappings.Add(Enums.Tables.DEVICE_CONNECTION, new Dictionary<long, long>());
            HelperDataTypeMappings[Enums.Tables.DEVICE_CONNECTION].Add(DataType.HELPER_SERIAL_NBR_PARENT, DataType.HELPER_SERIAL_NBR);
            HelperDataTypeMappings.Add(Enums.Tables.DEVICE, new Dictionary<long, long>());
            HelperDataTypeMappings[Enums.Tables.DEVICE].Add(DataType.HELPER_SERIAL_NBR_PARENT, DataType.HELPER_SERIAL_NBR);
            #endregion

            #region ReflectedFields

            ReflectedFields = new Dictionary<Type, Dictionary<long, Tuple<string, System.Reflection.FieldInfo>>>();
            ReflectedFields.Add(typeof(UI.Business.Objects.DW.OpAlarmMessage), typeof(UI.Business.Objects.DW.OpAlarmMessage).GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).ToDictionary(k => DataType.GetId(string.Format("HELPER_{0}", k.Name)), v => Tuple.Create(string.Format("HELPER_{0}", v.Name), v)));

            #endregion

            #region DBConnection
            DBCommonCORE dbConnectionCore = null;
            DBCommonDW dbConnectionDw = null;
            DBCommonDAQ dbConnectionDaq = null;

            dbConnectionCore = new DBCommonCORE("Suite.CORE", 60, 720);
            if (!dbConnectionCore.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB CORE on server: {0}", dbConnectionCore.DB.Server));
            }
            Log(EventID.ViewsManager.DBConnectionCreated, dbConnectionCore.DB.Server, "CORE");

            dbConnectionDw = new DBCommonDW("Suite.DW", 60, 720); ;
            if (!dbConnectionDw.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DW on server: {0}", dbConnectionDw.DB.Server));
            }
            Log(EventID.ViewsManager.DBConnectionCreated, dbConnectionDw.DB.Server, "DW");

            dbConnectionDaq = new DBCommonDAQ("Suite.DAQ", 60, 720); ;
            if (!dbConnectionDaq.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DAQ on server: {0}", dbConnectionDaq.DB.Server));
            }
            Log(EventID.ViewsManager.DBConnectionCreated, dbConnectionDaq.DB.Server, "DAQ");

            this.DataProvider = new DataProvider(dbConnectionCore, dbConnectionDw, dbConnectionDaq,
                !String.IsNullOrEmpty(this.CultureName) ? Enums.Language.Default.GetLanguage(this.CultureName) : Enums.Language.Default,
                (this.TimeZoneOffset == 0 ? null : (TimeSpan?)TimeSpan.FromMinutes(this.TimeZoneOffset)));

            Log(EventID.ViewsManager.DataProviderCreated);
            #endregion
            #region DataProviderInitialization

            this.DataProvider.IImrSeverDataTypes = new List<long>()
            {
                DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS,
                DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_BINDING,
            };

            this.DataProvider.IViewDataTypes = new List<long>()
            {
                DataType.VIEW_ID_SOURCE_IMR_SERVER,
            };
            this.DataProvider.IViewColumnDataTypes = new List<long>()
            {
                DataType.VIEW_COLUMN_COMBINED_ID_DATA_TYPE,
                DataType.VIEW_COLUMN_COMBINED_INDEX_NBR,
                DataType.VIEW_COLUMN_COMBINED_SEPARATOR,
            };

            this.DataProvider.GetAllDataType();

            #endregion

            #region LoadViewConfiguration

            this.Views = ViewComponent.GetViewConfiguration(this.DataProvider);
            Log(EventID.ViewsManager.ViewsConfigurationLoaded, this.Views == null ? "<null>" : this.Views.Count.ToString());

            #endregion
            #region LoadTableDefinitions

            //TableSection tableSection = (TableSection)ConfigurationManager.GetSection("TablesSection");
            //if (tableSection != null && tableSection.Tables != null)
            //    TableDefinition = tableSection.Tables.Cast<Table>().ToList();

            #endregion
            #region DBCollectorConnections

            OpOperator viewsManagerOperator = OperatorComponent.GetOperatorWithCreation(this.DataProvider, ViewsManagerOperatorLogin, password: this.ViewsManagerOperatorPassword);
            foreach (OpImrServer isItem in this.Views.SelectMany(v => v.ImrServers).Distinct())
            {
                InitializeDBCollector(isItem, viewsManagerOperator);
            }

            DBCollectorKeepAliveTask = new Task(() => DBCollectorKeepAlive(), TaskCreationOptions.LongRunning);
            DBCollectorKeepAliveTask.Start();

            #endregion
            #region ViewOperationProceedTask

            ViewOperationQueue = new Queue<OpViewOperation>();

            StartViewOperationProceedTask(null);

            #endregion

            #region ViewsManagerStatusTimer

            using (System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess())
            {
                CurrentRamPerformanceCounter = new System.Diagnostics.PerformanceCounter("Process", "Working Set - Private", proc.ProcessName, true);
                CurrentCPUPerformanceCounter = new System.Diagnostics.PerformanceCounter("Process", "% Processor Time", proc.ProcessName, true);
            }

            ViewsManagerStatusTimer = new System.Timers.Timer();
            ViewsManagerStatusTimer.Elapsed += new System.Timers.ElapsedEventHandler(ViewsManagerStatusTimer_Elapsed);
            ViewsManagerStatusTimer.Interval = ViewsManagerStatusTimerIntevral;
            ViewsManagerStatusTimer.AutoReset = true;
            ViewsManagerStatusTimer.Enabled = ViewsManagerStatusTimerIntevral > 0;

            #endregion

            #region VerifyViews

            VerifyViews(this.Views);

#if !DEBUG
            DeleteNonExistingRowsFromViews(this.Views);
#endif

            #endregion
        }

        #endregion
        #region Stop

        public void Stop()
        {
            ViewOperationProceedStopEvent.Set();
            
            foreach (OpImrServer isItem in this.Views.SelectMany(v => v.ImrServers).Distinct())
            {
                try
                {
                    DBCollectorCloseSession(isItem);
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.DBCollectorOnStopException, ex.ToString());
                }
            }
        }

        #endregion
        
        #region ViewsManagerStatusTimer_Elapsed

        private void ViewsManagerStatusTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            int queueSize = ViewOperationQueue.Count;
            float currentRam = (int)(CurrentRamPerformanceCounter.NextValue() / 1024.0f);
            float currentCpu = CurrentCPUPerformanceCounter.NextValue();

            string dictsStatus = "<null>";
            string cacheStatus = "<null>";

            try
            {
                dictsStatus = this.DataProvider
                    .GetType()
                    .GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic)
                    .Where(x => x.FieldType.Name.Contains("Dictionary"))
                    .Select(x => (x.GetValue(this.DataProvider) as System.Collections.IDictionary).Count).Sum()
                    .ToString();

                dictsStatus += " " + string.Join(", ",
                    string.Join(",",
                    this.DataProvider
                    .GetType()
                    .GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic)
                    .Where(x => x.FieldType.Name.Contains("Dictionary"))
                    .Select(x => (x.GetValue(this.DataProvider) as System.Collections.IDictionary).Count > 0 ? (x.Name + " " + (x.GetValue(this.DataProvider) as System.Collections.IDictionary).Count) : string.Empty))
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ExceptionOccured, ex.ToString());
            }

            try
            {
                cacheStatus =
                    this
                    .GetType()
                    .GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic)
                    .Where(x => x.FieldType.Name.Contains("Dictionary") && x.Name.Contains("Cache"))
                    .Select(x => (x.GetValue(this) as System.Collections.IDictionary).Values.Cast<System.Collections.IDictionary>().Select(y => y.Count).Sum()).Sum()
                    .ToString();
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ExceptionOccured, ex.ToString());
            }

            Log(EventID.ViewsManager.ViewsManagerStatus, queueSize, currentRam, currentCpu, dictsStatus, cacheStatus);
        }
        
        #endregion

        #region StartViewOperationProceedTask

        private void StartViewOperationProceedTask(Task prevTask)
        {
            if (ViewOperationProceedStopEvent.WaitOne(0))
            {
                Log(EventID.ViewsManager.ViewOperationProceedTaskFinishedStopEventIsSet);
                return;
            }

            if (prevTask != null)
            {
                SendEmail(string.Format("ViewOperationProceedTask has ended on {0}! Status: {1}, Exception: {2}",
                        Environment.MachineName, prevTask == null ? "<null>" : prevTask.Status.ToString(),
                        prevTask == null || prevTask.Exception == null ? "<null>" : prevTask.Exception.ToString()));

                Log(EventID.ViewsManager.ViewOperationProceedTaskFinished, prevTask == null ? "<null>" : prevTask.Status.ToString(), prevTask == null || prevTask.Exception == null ? "<null>" : prevTask.Exception.ToString());
            }

            ViewOperationProceedTask = new Task(() => ProceedViewOperation(), TaskCreationOptions.LongRunning);
            ViewOperationProceedTask.ContinueWith(StartViewOperationProceedTask);
            ViewOperationProceedTask.Start();            

            Log(EventID.ViewsManager.ViewOperationProceedTaskStarted);
        }

        #endregion

        #region OnReceiveMessage

        private void OnReceiveMessage(string guid, MESSAGE[] message)
        {
            DateTime receiveTime = DateTime.Now;

            OpImrServer isItem = !String.IsNullOrEmpty(guid) ? this.Servers.TryGetValue(guid) : null;
            long? bytesCnt = message == null || message.Length == 0 ? null : (long?)message.Where(m => m.MessageByte != null).Sum(w => w.MessageByte.Length);
            Log(EventID.ViewsManager.ReceivedMessage, message == null ? "<null>" : message.Count().ToString(),
                bytesCnt.HasValue ? bytesCnt.ToString() : "<null>",
                message == null || message.Length == 0 ? "<null>" : (receiveTime - message[0].SentTime).TotalMilliseconds.ToString(),
                isItem == null ? "<null>" : isItem.StandardDescription, guid);
            
            if (message != null && isItem != null && message.Count() > 0)
            {
                foreach (MESSAGE mItem in message)
                {
                    if (mItem.Type != MESSAGE_TYPE.CONTROL)
                    {
                        object oItem = DeserializeMessage(mItem.MessageByte);
                        Type tItem = oItem.GetType();
                        int? elementsCount = null;
                        //foreach (OpView vItem in this.Views)
                        // Tu jest for ponieważ this.View może zostać zmodyfikowane w innej części VM więc foreach wtedy sypnie.
                        for (int i = 0; i < this.Views.Count; ++i)
                        {
                            OpView vItem = this.Views[i];

                            if (!vItem.IsSynchronizable)
                            {
                                Log(EventID.ViewsManager.ReceivedMessageViewNotSynchronizable, vItem.IdView, vItem.Name, "-");
                                continue;
                            }

                            if (vItem.ImrServers.Exists(s => s.IdImrServer == isItem.IdImrServer))
                            {

                                switch (mItem.Type)
                                {
                                    case MESSAGE_TYPE.MSMQ:
                                        #region Action
                                        if (tItem == typeof(Action) || tItem == typeof(Action[]))
                                        {
                                            Action[] actions = null;
                                            if (tItem == typeof(Action))
                                                actions = new Action[1] { oItem as Action };
                                            if (tItem == typeof(Action[]))
                                                actions = oItem as Action[];

                                            elementsCount = message == null ? null : (int?)actions.Count();
                                            ProceedMessage(vItem, isItem, actions);
                                        }
                                        #endregion
                                        #region ChangeInTable
                                        else if (tItem == typeof(ChangeInTable) || tItem == typeof(ChangeInTable[]))
                                        {
                                            ChangeInTable[] changeInTable = null;
                                            if (tItem == typeof(ChangeInTable))
                                                changeInTable = new ChangeInTable[1] { oItem as ChangeInTable };
                                            if (tItem == typeof(ChangeInTable[]))
                                                changeInTable = oItem as ChangeInTable[];

                                            elementsCount = changeInTable.Count();
                                            ProceedMessage(vItem, isItem, changeInTable);
                                        }
                                        #endregion
                                        #region DataCurrentValue
                                        else if (tItem == typeof(DataCurrentValue) || tItem == typeof(DataCurrentValue[]))
                                        {
                                            DataCurrentValue[] dataCurrentValue = null;
                                            if (tItem == typeof(DataCurrentValue))
                                                dataCurrentValue = new DataCurrentValue[1] { oItem as DataCurrentValue };
                                            if (tItem == typeof(DataCurrentValue[]))
                                                dataCurrentValue = oItem as DataCurrentValue[];

                                            elementsCount = dataCurrentValue.Count();
                                            ProceedMessage(vItem, isItem, dataCurrentValue);
                                        }
                                        #endregion
                                        else
                                            Log(EventID.ViewsManager.UnsupportedMessageContentType, mItem.Type, tItem);
                                        break;
                                    default:
                                    
                                            Log(EventID.ViewsManager.UnsupportedMessageType, mItem.Type);
                                    
                                        break;
                                }
                            }                        
                        }

                        Log(EventID.ViewsManager.MessageReceiveTime, !elementsCount.HasValue ? "<null>" : elementsCount.ToString(),
                                                                 mItem.MessageByte == null ? "<null>" : mItem.MessageByte.Count().ToString(),
                                                                 mItem.Type.ToString(),
                                                                 tItem == null ? "<null>" : tItem.Name,
                                                                 Math.Round((receiveTime - mItem.SentTime).TotalMilliseconds, 4));
                    }
                    else
                    {
                        #region MESSAGE_CONTROL
                        MESSAGE_CONTROL mControl = mItem as MESSAGE_CONTROL;
                        if (mControl != null)
                        {
                            ProceedMessage(mControl.Command.ToString());
                        }
                        #endregion
                    }
                }
            }
        }

        #endregion

        #region DeserializeMessage

        private object DeserializeMessage(byte[] message)
        {

            if (message == null)
                return null;
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(message, 0, message.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            return binForm.Deserialize(memStream);
        }

        #endregion

        #region ProceedMessage

        private void ProceedMessage(string command)
        {
            try
            {
                OpViewOperation viewOperation = null;

                if (!string.IsNullOrWhiteSpace(command))
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(OpViewOperation));
                    try
                    {
                        using (TextReader reader = new StringReader(command))
                        {
                            viewOperation = serializer.Deserialize(reader) as OpViewOperation;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.ViewsManager.ProceedMessageFailedToDeserializeCommand, ex.ToString());
                        return;
                    }
                }
                else
                {
                    Log(EventID.ViewsManager.ProceedMessageCommandNullOrWhitespace, command);
                    return;
                }

                if (viewOperation == null)
                {
                    Log(EventID.ViewsManager.ProceedMessageViewOperation, "<null>", "<null>", "<null>", "<null>", "<null>", "<null>");
                }
                else
                {
                    Log(EventID.ViewsManager.ProceedMessageViewOperation, viewOperation.ChangeAction, viewOperation.IdView, viewOperation.IdImrServer, viewOperation.KeyColumnName, viewOperation.ObjectId, viewOperation.CellValues == null ? "<null>" : viewOperation.CellValues.Count.ToString());
                    if (viewOperation.CellValues != null)
                    {
                        foreach (OpViewOperation.OpViewOperationCellValue cellValue in viewOperation.CellValues)
                        {
                            Log(EventID.ViewsManager.ProceedMessageViewOperationCellValue, viewOperation.IdView, viewOperation.IdImrServer, cellValue.ColumnName, cellValue.IdDataType, cellValue.Index, cellValue.Value);
                        }
                    }
                }

                OpView view = TryGetView(viewOperation.IdView, viewOperation.View);
                if (view == null)
                {
                    Log(EventID.ViewsManager.ProceedMessageMissingViewConfiguration, viewOperation.IdView);
                    return;
                }

                if (!view.IsSynchronizable)
                {
                    Log(EventID.ViewsManager.ProceedMessageViewNotSynchronizable, viewOperation.IdView, view.Name, view.ImrServers.First().IdImrServer);
                    return;
                }

                if (viewOperation != null)
                {
                    switch (viewOperation.ChangeAction)
                    {
                        case Enums.ViewOperation.InsertRow:
                        case Enums.ViewOperation.UpdateRow:
                        case Enums.ViewOperation.DeleteRow:
                        case Enums.ViewOperation.MergeRow:
                        {
                            #region Row operations

                            if (viewOperation.View == null)
                            {
                                viewOperation.View = view;
                            }
                            if (viewOperation.ImrServer == null)
                            {
                                viewOperation.ImrServer = DataProvider.GetImrServer(viewOperation.IdImrServer);
                            }

                            foreach (OpViewOperation.OpViewOperationCellValue cellValue in viewOperation.CellValues)
                            {
                                if (cellValue.DataType == null)
                                {
                                    cellValue.DataType = DataProvider.GetDataType(cellValue.IdDataType);
                                }
                            }

                            lock (this.ViewOperationQueue)
                            {
                                this.ViewOperationQueue.Enqueue(viewOperation);
                            }

                            #endregion
                            break;
                        }
                        case Enums.ViewOperation.RebuildView:
                        {
                            #region RebuildView

                            Dictionary<OpView, List<OpViewColumn>> viewsToCreate = GetViewsToBuild(new List<OpView>() { view });

                            Log(EventID.ViewsManager.ProceedMessageBuildingView, viewOperation.IdView);

                            // Być może widok nie istniał lub kolumny nie istniały więc próbujemy robić rebuild
                            BuildViews(viewsToCreate);

                            Log(EventID.ViewsManager.ProceedMessageInitializeView, viewOperation.IdView);

                            try
                            {
#if !DEBUG
                                DeleteNonExistingRowsFromViews(new OpView[] { view });
#endif
                            }
                            catch (Exception iex)
                            {
                                Log(EventID.ViewsManager.ProceedMessageCommandException, iex.ToString());
                            }

                            // Inicjalizujemy widok
                            foreach (OpImrServer isItem in view.ImrServers)
                            {
                                InitializeView(view, isItem, viewOperation.ColumnCustomWhereClauses.Select(x => Tuple.Create((int)x[0], (string)x[1])).ToList());
                            }

                            Log(EventID.ViewsManager.ProceedMessageViewInitialized, viewOperation.IdView);

                            #endregion
                            break;
                        }
                        case Enums.ViewOperation.RebuildColumn:
                        {
                            #region RebuildColumn

                            Dictionary<OpView, List<OpViewColumn>> viewsToCreate = GetViewsToBuild(new List<OpView>() { view });

                            if (viewsToCreate.Count > 0 && (viewsToCreate.First().Value == null || viewsToCreate.First().Value.Count == 0))
                            {
                                Log(EventID.ViewsManager.ProceedMessageViewDoesNotExistsCantRebuildColumn, view.Name, viewOperation.IdView);
                                break;
                            }

                            if (viewOperation.CellValues == null || viewOperation.CellValues.Count == 0)
                            {
                                Log(EventID.ViewsManager.ProceedMessageNoColumnsToRebuildInMessage, view.Name, viewOperation.IdView);
                            }

                            if (viewsToCreate.Count == 0)
                            {
                                viewsToCreate.Add(view, new List<OpViewColumn>());
                            }

                            Log(EventID.ViewsManager.ProceedMessageBuildingView, viewOperation.IdView);

                            viewsToCreate.First().Value.Clear();

                            foreach (OpViewOperation.OpViewOperationCellValue cellValue in viewOperation.CellValues)
                            {
                                if (cellValue.DataType == null)
                                {
                                    viewsToCreate.First().Value.Add(view.Columns.FirstOrDefault(x => x.Name.Equals(cellValue.ColumnName)));
                                }
                            }

                            if (viewsToCreate.First().Value == null || viewsToCreate.First().Value.Count == 0)
                            {
                                Log(EventID.ViewsManager.ProceedMessageCantRebuildColumnsMissingInViewColumn, view.Name, viewOperation.IdView);
                            }

                            // Być może widok nie istniał lub kolumny nie istniały więc próbujemy robić rebuild
                            BuildViews(viewsToCreate);

                            Log(EventID.ViewsManager.ProceedMessageInitializeView, viewOperation.IdView);

                            // Inicjalizujemy poszczególne kolumny
                            foreach (OpImrServer isItem in view.ImrServers)
                            {
                                InitializeView(view, isItem, viewsToCreate.First().Value, viewOperation.ColumnCustomWhereClauses.Select(x => Tuple.Create((int)x[0], (string)x[1])).ToList(), true);
                            }

                            Log(EventID.ViewsManager.ProceedMessageViewInitialized, viewOperation.IdView);

                            #endregion
                            break;
                        }
                        case Enums.ViewOperation.AddColumn:
                        case Enums.ViewOperation.RemoveColumn:
                        {
                            #region AddRemove column

                            List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>> columnsToProceed = new List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>>();
                            foreach (OpViewOperation.OpViewOperationCellValue cellValue in viewOperation.CellValues)
                            {
                                if (String.Equals(cellValue.ColumnName.Trim().ToUpper(), view.PrimaryKeyColumn.Name.Trim().ToUpper()))
                                {
                                    continue;
                                }

                                OpDataType dataType = cellValue.DataType == null ? DataProvider.GetDataType(cellValue.IdDataType) : cellValue.DataType;

                                columnsToProceed.Add(Tuple.Create(viewOperation.ChangeAction, view.Name, cellValue.ColumnName, (Enums.DataTypeClass)dataType.IdDataTypeClass, true));
                            }

                            Log(EventID.ViewsManager.ProceedMessageColumnsToProceed, viewOperation.IdView, columnsToProceed.Count);

                            DataProvider.ProceedViewChange(view, columnsToProceed.ToArray());

                            #endregion
                            break;
                        }
                        default:
                            Log(EventID.ViewsManager.ProceedMessageInvalidViewOperation, viewOperation.ChangeAction.ToString());
                            break;
                    }

                }

                Log(EventID.ViewsManager.ViewOperationQueueState, ViewOperationQueue == null ? "<null>" : ViewOperationQueue.Count.ToString());
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ProceedMessageCommandException, ex.ToString());
            }
        }

        private void ProceedMessage(OpView view, OpImrServer server, Action[] message)
        {
            if (message == null)
                return;

            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedMessageViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            // Dictionary<ObjectId, <IdDataTypeObjectId, Dictionary<KeyColemnName, Dictionary<SetColumnName, Tuple<SetColumnName, CellValue, OpDataType, IndexNbr>>>>>
            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> deviceUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> meterUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> locationUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();

            List<OpViewColumn> viewDeviceKeyColumns = new List<OpViewColumn>();
            List<OpViewColumn> viewMeterKeyColumns = new List<OpViewColumn>();
            List<OpViewColumn> viewLocationKeyColumns = new List<OpViewColumn>();
            try
            {
                if (message.Any(c => c.SerialNbr != null && c.SerialNbr.DBValue.HasValue))
                    viewDeviceKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.SerialNbr));
                if (message.Any(c => c.IdMeter.HasValue))
                    viewMeterKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.IdMeter));
                if (message.Any(c => c.IdLocation.HasValue))
                    viewLocationKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.IdLocation));

                foreach (Action aItem in message)
                {
                    if (aItem.ActionData == null)
                        continue;
                    foreach (DataValue adItem in aItem.ActionData)
                    {
                        foreach (OpViewColumn vcDataItem in view.Columns.Where(c => c.IdDataType == adItem.IdDataType))
                        {
                            List<OpViewColumn> vcExtendedHelpers = view.Columns.Where(x => x.IdKeyViewColumn == vcDataItem.IdViewColumn && x.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
                                                                               .ToList();

                            if (aItem.SerialNbr != null && aItem.SerialNbr.DBValue.HasValue && viewDeviceKeyColumns.Count > 0)
                            {
                                long serialNbr = Convert.ToInt64(aItem.SerialNbr.DBValue);
                                PrepareUpdateDict(new GenericKey(serialNbr), DataType.HELPER_SERIAL_NBR, adItem, vcDataItem, viewDeviceKeyColumns, ref deviceUpdates);
                                foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { adItem, vcDataItem, serialNbr, viewDeviceKeyColumns, deviceUpdates }); }
                            }
                            if (aItem.IdMeter.HasValue && viewMeterKeyColumns.Count > 0)
                            {
                                PrepareUpdateDict(new GenericKey(aItem.IdMeter.Value), DataType.HELPER_ID_METER, adItem, vcDataItem, viewMeterKeyColumns, ref meterUpdates);
                                foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { adItem, vcDataItem, aItem.IdMeter.Value, viewMeterKeyColumns, meterUpdates }); }
                            }
                            if (aItem.IdLocation.HasValue)
                            {
                                PrepareUpdateDict(new GenericKey(aItem.IdLocation.Value), DataType.HELPER_ID_LOCATION, adItem, vcDataItem, viewLocationKeyColumns, ref locationUpdates);
                                foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { adItem, vcDataItem, aItem.IdLocation.Value, viewLocationKeyColumns, locationUpdates }); }
                            }
                        }
                    }
                }

                List<string> updateClauses = new List<string>();
                updateClauses.AddRange(PrepareUpdateClauses(view, server, deviceUpdates));
                updateClauses.AddRange(PrepareUpdateClauses(view, server, meterUpdates));
                updateClauses.AddRange(PrepareUpdateClauses(view, server, locationUpdates));

                if (updateClauses.Count > 0)
                {
                    foreach (string sItem in updateClauses)
                        Log(EventID.ViewsManager.PreparedActionUpdateClause, sItem);
                }
                else
                    Log(EventID.ViewsManager.PreparedActionUpdateClause, "<null>");


                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in deviceUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in locationUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in meterUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }

                Log(EventID.ViewsManager.ViewOperationQueueState, ViewOperationQueue == null ? "<null>" : ViewOperationQueue.Count.ToString());
            }
            catch(Exception ex)
            {
                Log(EventID.ViewsManager.ProceedActionMessageException, ex.Message, ex.StackTrace);
            }
        }

        private void ProceedMessage(OpView view, OpImrServer server, ChangeInTable[] message)
        {
            if (message == null)
                return;

            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedMessageViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            if (view.DatabaseType == Enums.DatabaseType.Unknown)
            {
                Log(EventID.ViewsManager.ProceedMessageViewHasUnknownDatabaseType, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            try
            {
                string updateFormat = "UPDATE [{0}] SET {1} WHERE [" + view.ImrServerColumn.Name + "] = {2} AND {3}";

                // Kolumny które zostały dodane do analizowanego widoku
                List<ChangeInTable> changedViewColumns = new List<ChangeInTable>();

                // Dictionary<ObjectId, Dictionary<KeyColemnName, Dictionary<SetColumnName, Tuple<SetColumnName, CellValue, OpDataType, IndexNbr>>>>
                Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> update = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
                // Dictionary<ObjectId, Dictionary<KeyColemnName, Dictionary<SetColumnName, Tuple<SetColumnName, CellValue, OpDataType, IndexNbr>>>>
                Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> merge = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
                // Dictionary<ObjectId, Dictionary<ColumnName, Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>
                Dictionary<GenericKey, Tuple<long, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>> insert = new Dictionary<GenericKey, Tuple<long, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>();
                //List<ObjectId, IdDataTypeObjectId>
                List<Tuple<GenericKey, long>> delete = new List<Tuple<GenericKey, long>>();

                foreach (ChangeInTable citItem in message)
                {
                    if (citItem.ChangeAction == ChangeAction.Unknown)
                    {
                        Log(EventID.ViewsManager.UnknownActionType);
                        continue;
                    }

                    Enums.ViewOperation translatedChangeAction = TranslateChangeActionToViewOperation(citItem.ChangeAction);

                    if (String.Equals(citItem.TableName.ToUpper(), Enums.Tables.VIEW_COLUMN.ToString().ToUpper()))
                    {
                        if (citItem.Columns.Any(x => x.Item1 == "ID_VIEW" && x.Item2 != null && Convert.ToInt32(x.Item2) == view.IdView))
                        {
                            changedViewColumns.Add(citItem);
                        }

                        continue;
                    }
                    else if (String.Equals(citItem.TableName.ToUpper(), Enums.Tables.VIEW.ToString().ToUpper()))
                    {
                        if (citItem.Columns.Any(x => x.Item1 == "ID_VIEW" && x.Item2 != null))
                        {
                            changedViewColumns.Add(citItem);
                        }

                        continue;
                    }

                    Dictionary<string, List<Tuple<OpViewColumn, object>>> keyColumns = new Dictionary<string, List<Tuple<OpViewColumn, object>>>();
                    Dictionary<string, List<OpViewColumn>> columns = new Dictionary<string, List<OpViewColumn>>();

                    long dataIdDataType = -1;
                    int dataIndexNbr = -1;
                    bool dataValueSet = false;
                    object dataValue = null;

                    string citContent = String.Empty;

                    foreach (Tuple<string, object> tItem in citItem.Columns)
                    {
                        citContent += String.Format("{0}: {1}; ", tItem.Item1, tItem.Item2);
                        long idDataType = DataType.GetId(String.Format("HELPER_{0}", tItem.Item1));//to przerobicna lokalny słownik
                        if (idDataType != -1)
                        {
                            List<OpViewColumn> vcList = view.Columns.FindAll(c => c.IdDataType == idDataType);
                            bool isEmptyKey = false;
                            if (vcList != null && vcList.Count(c => c.Key) > 0)
                            {
                                if (tItem.Item2 != null && !String.IsNullOrEmpty(tItem.Item2.ToString()))
                                    keyColumns[tItem.Item1] = vcList.Where(c => c.Key).Select(c => new Tuple<OpViewColumn, object>(c, tItem.Item2)).ToList();
                                else
                                    isEmptyKey = true;//zadziała przy data, a przy reszcie?
                            }

                            if (vcList != null && vcList.Count > 0 && !isEmptyKey)
                            {
                                if (!columns.ContainsKey(tItem.Item1))
                                    columns[tItem.Item1] = new List<OpViewColumn>();
                                columns[tItem.Item1].AddRange(vcList);
                            }

                        }
                        else
                        {
                            if (String.Equals(citItem.TableName, Enums.Tables.DATA.ToString()) || citItem.TableName.EndsWith("_DATA"))
                            {
                                if (String.Equals(tItem.Item1, "ID_DATA_TYPE") && tItem.Item2 != null)
                                    long.TryParse(tItem.Item2.ToString(), out dataIdDataType);
                                if (String.Equals(tItem.Item1, "INDEX_NBR") && tItem.Item2 != null)
                                    int.TryParse(tItem.Item2.ToString(), out dataIndexNbr);
                                if (String.Equals(tItem.Item1, "VALUE"))
                                {
                                    dataValue = tItem.Item2;
                                    dataValueSet = true;
                                }
                            }
                            else
                                Log(EventID.ViewsManager.UnknownHelperDataType, tItem.Item1, citItem.TableName);
                        }
                    }
                    if (dataIdDataType != -1 && dataIndexNbr != -1 && dataValueSet)
                    {
                        foreach (OpViewColumn vcKeyItem in keyColumns.Values.SelectMany(c => c).Distinct().Select(k => k.Item1))
                        {
                            if (!columns.ContainsKey("ID_DATA_TYPE"))
                                columns["ID_DATA_TYPE"] = new List<OpViewColumn>();
                            columns["ID_DATA_TYPE"].Add(new OpViewColumn() { Name = DataColumn, IdKeyViewColumn = vcKeyItem.IdViewColumn });
                        }
                    }

                    if (!ProceedAssociativeTable(view, server, citItem, ref translatedChangeAction, keyColumns, columns))
                    {
                        #region Szukanie po tabelce łącznikowej (VIEW_COLUMN_ASSOCIATIVE_TABLE)

                        if (view.Columns.Count > 0)
                        {
                            List<OpViewColumn> columnWithAssocTable = view.ColumnsWithAssociativeTable.ToList(); 
                            if (columnWithAssocTable.Count > 0)
                            {
                                // parent, current, assoc table, assoc dt, assoc idx
                                List<Tuple<int, int, string, long?, int?>> associativeTables = new List<Tuple<int, int, string, long?, int?>>();
                                foreach (OpViewColumn column in columnWithAssocTable)
                                {
                                    if (column.IdKeyViewColumn.HasValue)
                                    {
                                        string tableName = column.AssociativeTable;
                                        long? idDataType = column.AssociativeTableIdDataType;
                                        int? indexNbr = column.AssociativeTableIndexNbr;

                                        if (!indexNbr.HasValue)
                                        {
                                            indexNbr = 0;
                                        }

                                        associativeTables.Add(Tuple.Create(column.IdKeyViewColumn.Value, column.IdViewColumn, tableName, idDataType, indexNbr));
                                    }
                                }

                                if (associativeTables.Any(x => x.Item3.ToUpper().Trim() == citItem.TableName.ToUpper().Trim()))
                                {
                                    Tuple<int, int, string, long?, int?> assocTable = default(Tuple<int, int, string, long?, int?>);
                                    object assocVal = null;

                                    if (citItem.TableName.ToUpper().Trim().EndsWith("_DATA") || String.Equals(citItem.TableName.ToUpper().Trim(), Enums.Tables.DATA.ToString()))
                                    {
                                        long idDataType = 0;
                                        int idxNbr = 0;

                                        foreach (Tuple<string, object> tItem in citItem.Columns)
                                        {
                                            if (String.Equals(tItem.Item1, "ID_DATA_TYPE") && tItem.Item2 != null)
                                                long.TryParse(tItem.Item2.ToString(), out idDataType);
                                            if (String.Equals(tItem.Item1, "INDEX_NBR") && tItem.Item2 != null)
                                                int.TryParse(tItem.Item2.ToString(), out idxNbr);
                                            if (String.Equals(tItem.Item1, "VALUE"))
                                            {
                                                assocVal = tItem.Item2;
                                            }
                                        }

                                        assocTable = associativeTables.FirstOrDefault(x => x.Item3 == citItem.TableName && x.Item4 == idDataType && x.Item5 == idxNbr);
                                    }
                                    else
                                    {
                                        foreach (Tuple<string, object> tItem in citItem.Columns)
                                        {
                                            long idDataType = DataType.GetId(String.Format("HELPER_{0}", tItem.Item1));//to przerobicna lokalny słownik

                                            if (idDataType != -1)
                                            {
                                                List<OpViewColumn> vcList = view.Columns.FindAll(c => c.IdDataType == idDataType);

                                                foreach (OpViewColumn vc in vcList)
                                                {
                                                    assocTable = associativeTables.FirstOrDefault(x => x.Item3 == citItem.TableName && (x.Item4.HasValue ? x.Item4 == vc.IdDataType : true) && x.Item5 == vc.ColumnIndexNbr);
                                                    if (assocTable != default(Tuple<int, int, string, long?, int?>))
                                                    {
                                                        assocVal = tItem.Item2;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (assocTable != default(Tuple<int, int, string, long?, int?>))
                                    {
                                        OpViewColumn parentColumn = view.Columns.First(x => x.IdViewColumn == assocTable.Item1);
                                        keyColumns[parentColumn.DataType.Name.Replace("HELPER_", "")] = new List<Tuple<OpViewColumn, object>>() { Tuple.Create(parentColumn, assocVal) };


                                    }
                                }
                            }
                        }

                        #endregion
                    }

                    Log(EventID.ViewsManager.ChangeInTableContent, citItem.TableName, citContent);

                    if (keyColumns.Count > 0)
                    {
                        foreach (Tuple<string, object> tItem in citItem.Columns)
                        {
                            if (columns.ContainsKey(tItem.Item1))
                            {
                                foreach (OpViewColumn vcItem in columns[tItem.Item1])
                                {
                                    List<Tuple<OpViewColumn, object>> vcMatchKey = new List<Tuple<OpViewColumn, object>>();
                                    if (vcItem.IdKeyViewColumn.HasValue)//Zwykla kolumna ze zdefiniowana kolumną kluczem
                                        vcMatchKey.AddRange(keyColumns.Values.SelectMany(c => c).Where(c => c.Item1.IdViewColumn == vcItem.IdKeyViewColumn).Distinct());
                                    else if (vcItem.Key && vcItem.PrimaryKey && String.Equals(citItem.TableName, vcItem.Table)
                                        && (translatedChangeAction == Enums.ViewOperation.DeleteRow || translatedChangeAction == Enums.ViewOperation.InsertRow))
                                    {
                                        vcMatchKey.Add(new Tuple<OpViewColumn, object>(vcItem, tItem.Item2));
                                    }

                                    foreach (Tuple<OpViewColumn, object> vcMatchedKeyItem in vcMatchKey)
                                    {
                                        GenericKey idObject = new GenericKey(vcMatchedKeyItem.Item2);
                                        object valueToSet = null;
                                        OpDataType dataTypeToSet = null;
                                        int indexNbrToSet = 0;

                                        string columnName = String.Empty;

                                        if (String.Equals(vcItem.Name, DataColumn))
                                        {
                                            valueToSet = dataValue;
                                            dataTypeToSet = DataProvider.GetDataType(dataIdDataType);
                                            indexNbrToSet = dataIndexNbr;
                                            columnName = String.Format("{0}_{1}_{2}", vcMatchedKeyItem.Item1.ColumnPrefix, dataIdDataType, indexNbrToSet);
                                            if (!view.Columns.Exists(c => String.Equals(c.Name, columnName)))
                                                continue;//przydatne np w przypadku DATA z wypełnioną więcej niz jedną kolumną kluczową
                                        }
                                        else
                                        {
                                            valueToSet = tItem.Item2;
                                            dataTypeToSet = vcItem.DataType;
                                            columnName = vcItem.Name;
                                        }

                                        List<Tuple<string, object, OpDataType, int>> operationsToAdd = new List<Tuple<string, object, OpDataType, int>>();

                                        Enums.ViewOperation caItem = translatedChangeAction;
                                        if (translatedChangeAction == Enums.ViewOperation.DeleteRow &&
                                            (!vcMatchedKeyItem.Item1.PrimaryKey || !String.Equals(vcMatchedKeyItem.Item1.Table, citItem.TableName)))
                                        {
                                            caItem = Enums.ViewOperation.UpdateRow;//trzeba wynulować komórkę
                                            valueToSet = null;

                                            foreach (OpViewColumn dependantColumn in view.Columns.Where(x => x.IdKeyViewColumn == vcItem.IdViewColumn))
                                            {
                                                operationsToAdd.Add(Tuple.Create(dependantColumn.Name, (object)null, dependantColumn.DataType, dependantColumn.ColumnIndexNbr));
                                            }                                            
                                        }
                                        if (translatedChangeAction == Enums.ViewOperation.InsertRow &&
                                            (!vcMatchedKeyItem.Item1.PrimaryKey || !String.Equals(vcMatchedKeyItem.Item1.Table, citItem.TableName)))
                                        {
                                            caItem = Enums.ViewOperation.UpdateRow;//nie jest to nasz klucz główny więc wiersz do którego przenzaczona jest wartość już istnieje
                                        }

                                        // Jeśli nie mamy PK nie możemy zrobić merge. Insert implikuje merge, ale przy update możemy
                                        // dostać FK który zostanie w tej chwili uznany za PK i nie zadziała.
                                        if (caItem == Enums.ViewOperation.InsertRow || (caItem == Enums.ViewOperation.UpdateRow && vcMatchedKeyItem.Item1.PrimaryKey))
                                        {
                                            caItem = Enums.ViewOperation.MergeRow;
                                        }

                                        // Na poczatek wstawiamy faktyczna wartosc z kolejki ktora przyszla.
                                        // W przypadku gdy zostanie wnullowany FK w odpowiednim ifie powyzej sa dodawane
                                        // operacje ktore nulluja kolumny podrzedne.
                                        operationsToAdd.Insert(0, Tuple.Create(columnName, valueToSet, dataTypeToSet, indexNbrToSet));

                                        foreach (Tuple<string, object, OpDataType, int> operation in operationsToAdd)
                                        {
                                            string opColumnName = operation.Item1;
                                            object opValueToSet = operation.Item2;
                                            OpDataType opDataType = operation.Item3;
                                            int opIndexNbr = operation.Item4;

                                            string clause = String.Empty;
                                            switch (caItem)
                                            {
                                                case Enums.ViewOperation.InsertRow:
                                                    if (!insert.ContainsKey(idObject))
                                                    {
                                                        insert[idObject] = Tuple.Create(vcMatchedKeyItem.Item1.IdDataType, new Dictionary<string, OpViewOperation.OpViewOperationCellValue>());
                                                        foreach (OpViewColumn vcDefault in view.Columns.Where(c => c.DefaultValue != null))
                                                            insert[idObject].Item2[vcDefault.Name] = new OpViewOperation.OpViewOperationCellValue(vcDefault.Name, vcDefault.DefaultValue, vcDefault.DataType, 0);
                                                        insert[idObject].Item2[view.PrimaryKeyColumn.Name] = new OpViewOperation.OpViewOperationCellValue(view.PrimaryKeyColumn.Name, idObject, view.PrimaryKeyColumn.DataType, 0);
                                                        insert[idObject].Item2[view.ImrServerColumn.Name] = new OpViewOperation.OpViewOperationCellValue(view.ImrServerColumn.Name, server.IdImrServer, view.ImrServerColumn.DataType, 0);
                                                    }
                                                    insert[idObject].Item2[opColumnName] = new OpViewOperation.OpViewOperationCellValue(opColumnName, opValueToSet, opDataType, opIndexNbr);
                                                    break;
                                                case Enums.ViewOperation.UpdateRow:
                                                    if (!update.ContainsKey(idObject))
                                                        update[idObject] = Tuple.Create(vcMatchedKeyItem.Item1.IdDataType, new Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>());

                                                    if (!update[idObject].Item2.ContainsKey(vcMatchedKeyItem.Item1.Name))
                                                        update[idObject].Item2[vcMatchedKeyItem.Item1.Name] = new Dictionary<string, OpViewOperation.OpViewOperationCellValue>();

                                                    update[idObject].Item2[vcMatchedKeyItem.Item1.Name][opColumnName] = new OpViewOperation.OpViewOperationCellValue(opColumnName, opValueToSet, opDataType, opIndexNbr);
                                                    break;
                                                case Enums.ViewOperation.MergeRow:
                                                    if (!merge.ContainsKey(idObject))
                                                    {
                                                        merge[idObject] = Tuple.Create(vcMatchedKeyItem.Item1.IdDataType, new Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>());
                                                    }
                                                    if (!merge[idObject].Item2.ContainsKey(vcMatchedKeyItem.Item1.Name))
                                                    {
                                                        merge[idObject].Item2[vcMatchedKeyItem.Item1.Name] = new Dictionary<string, OpViewOperation.OpViewOperationCellValue>();
                                                    }
                                                    foreach (OpViewColumn vcDefault in view.Columns.Where(c => c.DefaultValue != null))
                                                    {
                                                        merge[idObject].Item2[vcMatchedKeyItem.Item1.Name][vcDefault.Name] = new OpViewOperation.OpViewOperationCellValue(vcDefault.Name, vcDefault.DefaultValue, vcDefault.DataType, 0);
                                                    }
                                                    merge[idObject].Item2[vcMatchedKeyItem.Item1.Name][opColumnName] = new OpViewOperation.OpViewOperationCellValue(opColumnName, opValueToSet, opDataType, opIndexNbr);
                                                    break;
                                                case Enums.ViewOperation.DeleteRow:
                                                    delete.Add(Tuple.Create(idObject, vcMatchedKeyItem.Item1.IdDataType));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                        Log(EventID.ViewsManager.MissingTableConfiguration, view.Name, citItem.TableName);
                }
                List<string> updateClauses = new List<string>();
                updateClauses.AddRange(PrepareUpdateClauses(view, server, update));

                List<string> insertClauses = new List<string>();
                insertClauses.AddRange(PrepareInsertClauses(view, server, insert));

                List<string> mergeClauses = new List<string>();
                mergeClauses.AddRange(PrepareMergeClauses(view, server, merge));

                List<string> deleteClauses = new List<string>();
                deleteClauses.AddRange(PrepareDeleteClauses(view, server, delete));

                foreach (string sItem in insertClauses)
                    Log(EventID.ViewsManager.PreparedChangeInTableClause, "INSERT", sItem);
                foreach (string sItem in updateClauses)
                    Log(EventID.ViewsManager.PreparedChangeInTableClause, "UPDATE", sItem);
                foreach (string sItem in mergeClauses)
                    Log(EventID.ViewsManager.PreparedChangeInTableClause, "MERGE", sItem);
                foreach (string sItem in deleteClauses)
                    Log(EventID.ViewsManager.PreparedChangeInTableClause, "DELETE", sItem);


                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>> kvDevItem in insert)
                {
                    OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.InsertRow, kvDevItem.Value.Item2.Values.ToList());
                    lock (this.ViewOperationQueue)
                    {
                        this.ViewOperationQueue.Enqueue(vroItem);
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in update)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in merge)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.MergeRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (Tuple<GenericKey, long> lItem in delete)
                {
                    OpViewOperation vroItem = new OpViewOperation(lItem.Item1.KeyValue, DataProvider.GetDataType(lItem.Item2), server, view, Enums.ViewOperation.DeleteRow, new List<OpViewOperation.OpViewOperationCellValue>());
                    lock (this.ViewOperationQueue)
                    {
                        this.ViewOperationQueue.Enqueue(vroItem);
                    }
                }

                ProceedViewChange(view, server, changedViewColumns.Where(x => String.Equals(x.TableName.Trim().ToUpper(), Enums.Tables.VIEW_COLUMN.ToString().Trim().ToUpper())).ToList());
                ProceedViewCreation(changedViewColumns.Where(x => String.Equals(x.TableName.Trim().ToUpper(), Enums.Tables.VIEW.ToString().Trim().ToUpper())).ToList());

                Log(EventID.ViewsManager.ViewOperationQueueState, ViewOperationQueue == null ? "<null>" : ViewOperationQueue.Count.ToString());
            }
            catch(Exception ex)
            {
                Log(EventID.ViewsManager.ProceedChangeInTableMessageException, ex.Message, ex.StackTrace);
            }
        }

        private void ProceedMessage(OpView view, OpImrServer server, DataCurrentValue[] message)
        {
            if (message == null)
                return;

            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedMessageViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> deviceUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> meterUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
            Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> locationUpdates = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();

            List<OpViewColumn> viewDeviceKeyColumns = new List<OpViewColumn>();
            List<OpViewColumn> viewMeterKeyColumns = new List<OpViewColumn>();
            List<OpViewColumn> viewLocationKeyColumns = new List<OpViewColumn>();
            try
            {
                if (message.Any(c => c.SerialNbr != null && c.SerialNbr.DBValue.HasValue))
                    viewDeviceKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.SerialNbr));
                if (message.Any(c => c.IdMeter.HasValue))
                    viewMeterKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.IdMeter));
                if (message.Any(c => c.IdLocation.HasValue))
                    viewLocationKeyColumns.AddRange(view.Columns.Where(c => c.Key && c.DataType.IdReferenceType == (int)Enums.ReferenceType.IdLocation));

                foreach (DataCurrentValue dcvItem in message)
                {
                    foreach (OpViewColumn vcDataItem in view.Columns.Where(c => c.IdDataType == dcvItem.IdDataType))
                    {
                        List<OpViewColumn> vcExtendedHelpers = view.Columns.Where(x => x.IdKeyViewColumn == vcDataItem.IdViewColumn && x.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
                                                                           .ToList();

                        if (dcvItem.SerialNbr != null && dcvItem.SerialNbr.DBValue.HasValue && viewDeviceKeyColumns.Count > 0)
                        {
                            long serialNbr = Convert.ToInt64(dcvItem.SerialNbr.DBValue);
                            PrepareUpdateDict(new GenericKey(serialNbr), DataType.HELPER_SERIAL_NBR, dcvItem, vcDataItem, viewDeviceKeyColumns, ref deviceUpdates);
                            foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { dcvItem, vcDataItem, serialNbr, viewDeviceKeyColumns, deviceUpdates }); }
                        }
                        if (dcvItem.IdMeter.HasValue && viewMeterKeyColumns.Count > 0)
                        {
                            PrepareUpdateDict(new GenericKey(dcvItem.IdMeter.Value), DataType.HELPER_ID_METER, dcvItem, vcDataItem, viewMeterKeyColumns, ref meterUpdates);
                            foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { dcvItem, vcDataItem, dcvItem.IdMeter.Value, viewMeterKeyColumns, meterUpdates }); }
                        }
                        if (dcvItem.IdLocation.HasValue)
                        {
                            PrepareUpdateDict(new GenericKey(dcvItem.IdLocation.Value), DataType.HELPER_ID_LOCATION, dcvItem, vcDataItem, viewLocationKeyColumns, ref locationUpdates);
                            foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers) { ProceedExtendedHelper(view, server, vcExtendedHelper, new object[] { dcvItem, vcDataItem, dcvItem.IdLocation.Value, viewLocationKeyColumns, locationUpdates }); }
                        }
                    }
                }

                List<string> updateClauses = new List<string>();
                updateClauses.AddRange(PrepareUpdateClauses(view, server, deviceUpdates));
                updateClauses.AddRange(PrepareUpdateClauses(view, server, meterUpdates));
                updateClauses.AddRange(PrepareUpdateClauses(view, server, locationUpdates));

                if (updateClauses.Count > 0)
                {
                    foreach (string sItem in updateClauses)
                        Log(EventID.ViewsManager.PreparedDataCurrentValueUpdateClause, sItem);
                }
                else
                    Log(EventID.ViewsManager.PreparedDataCurrentValueUpdateClause, "<null>");


                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in deviceUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in locationUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
                foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in meterUpdates)
                {
                    foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                    {
                        OpViewOperation vroItem = new OpViewOperation(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1), server, view, Enums.ViewOperation.UpdateRow, kvUpdItem.Value.Values.ToList()) { KeyColumnName = kvUpdItem.Key };
                        lock (this.ViewOperationQueue)
                        {
                            this.ViewOperationQueue.Enqueue(vroItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ProceedDataCurrentValueMessageException, ex.ToString());
            }

            Log(EventID.ViewsManager.ViewOperationQueueState, ViewOperationQueue == null ? "<null>" : ViewOperationQueue.Count.ToString());
        }

        #endregion
        #region ProceedViewOperation

        private void ProceedViewOperation()
        {
#if DEBUG
            while (!ViewOperationProceedStopEvent.WaitOne(10, false))
#else
            while (!ViewOperationProceedStopEvent.WaitOne(100, false))
#endif
            {
                List<OpViewOperation> viewOperation = null;
                try
                {
                    if (ViewOperationProceedStopEvent.WaitOne(0, false))
                        break;

                    lock (this.ViewOperationQueue)
                    {
                        if (this.ViewOperationQueue != null && this.ViewOperationQueue.Count > 0)
                        {
                            viewOperation = new List<OpViewOperation>();
                            for(int i = 0; i < MaxOperationBatch; i++)
                            {
                                if (this.ViewOperationQueue.Count > 0)
                                    viewOperation.Add(this.ViewOperationQueue.Dequeue());
                                else
                                    break;
                            }

                            Log(EventID.ViewsManager.ViewOperationQueueState, ViewOperationQueue == null ? "<null>" : ViewOperationQueue.Count.ToString());
                        }
                    }
                   
                    if(viewOperation != null && viewOperation.Count > 0)
                    {
                        Log(EventID.ViewsManager.ViewOperationsToProceedCount, viewOperation == null ? "<null>" : viewOperation.Count.ToString());

                        this.DataProvider.ProceedViewOperation(viewOperation);

                        foreach (OpViewOperation vo in viewOperation)
                        {
                            Log(EventID.ViewsManager.ProceedViewOperationStatement, vo.Statement);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.ProceedViewOperationException, ex.Message, ex.StackTrace);
                    if(viewOperation != null)
                    {
                        foreach(OpViewOperation voItem in viewOperation)
                        {
                            if (voItem == null)
                            {
                                Log(EventID.ViewsManager.FailedViewOperationIsNull);
                            }
                            else
                            {
                                Log(EventID.ViewsManager.FailedViewOperation, voItem.Statement);
                                lock (this.ViewOperationQueue)
                                {
                                    // Skoro sie wywalily to dodajemy je ponownie do kolejki by sprobowac do skutku.
                                    this.ViewOperationQueue.Enqueue(voItem);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion
        #region ProceedViewChange

        private void ProceedViewChange(OpView view, OpImrServer server, List<ChangeInTable> changedViewColumns)
        {
            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedViewChangeViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            if (changedViewColumns.Count > 0)
            {
                try
                {
                    List<int> idInsertedViewColumns = new List<int>();
                    List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>> changes = new List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>>();
                    foreach (ChangeInTable citItem in changedViewColumns)
                    {
                        Enums.ViewOperation translatedChangeAction = TranslateChangeActionToViewOperation(citItem.ChangeAction);

                        int columnId = Convert.ToInt32(citItem.Columns.First(y => String.Equals(y.Item1.ToUpper(), "ID_VIEW_COLUMN")).Item2);

                        string viewName = view.Name;
                        string columnName = string.Empty;
                        Enums.DataTypeClass dataTypeClass = Enums.DataTypeClass.Unknown;

                        if (translatedChangeAction == Enums.ViewOperation.InsertRow)
                        {
                            idInsertedViewColumns.Add(columnId);

                            dataTypeClass = (Enums.DataTypeClass)DataProvider.GetDataType(Convert.ToInt64(citItem.Columns.FirstOrDefault(x => String.Equals(x.Item1.ToUpper(), "ID_DATA_TYPE")).Item2)).IdDataTypeClass;
                        }
                        else if (translatedChangeAction == Enums.ViewOperation.DeleteRow)
                        {
                            // Aktualizujemy widok
                            view.Columns.RemoveAll(x => x.IdViewColumn == columnId);
                        }

                        if (citItem.Columns.Any(x => String.Equals(x.Item1.ToUpper(), "NAME")))
                        {
                            columnName = citItem.Columns.FirstOrDefault(x => String.Equals(x.Item1.ToUpper(), "NAME")).Item2.ToString();
                        }
                        else
                        {
                            OpViewColumn column = DataProvider.GetViewColumn(columnId);
                            if (column != null)
                            {
                                columnName = column.Name;
                            }
                        }

                        changes.Add(Tuple.Create(translatedChangeAction, viewName, columnName, dataTypeClass, true));
                    }

                    if (changes.Count > 0)
                    {
                        DataProvider.ProceedViewChange(view, changes.ToArray());
                    }

                    if (idInsertedViewColumns.Count > 0)
                    {
                        List<OpViewColumn> columns = DataProvider.GetViewColumn(idInsertedViewColumns.ToArray());
                        view.Columns.AddRange(columns); // Aktualizujemy widok
                        InitializeView(view, server, columns, null, particularColumns: true);
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.ProceedViewChangeException, ex.ToString());
                }
            }
        }

        #endregion

        #region ProceedAssociativeTable

        private bool ProceedAssociativeTable(OpView view, OpImrServer server, ChangeInTable message, ref Enums.ViewOperation viewOperation, Dictionary<string, List<Tuple<OpViewColumn, object>>> keyColumns, Dictionary<string, List<OpViewColumn>> columns)
        {
            switch (message.TableName.Trim().ToUpper())
            {
                #region LOCATION_EQUIPMENT
                case "LOCATION_EQUIPMENT":
                {
                    IEnumerable<OpViewColumn> columnsWithLEAssocTable = view.ColumnsWithAssociativeTable.Where(x => String.Equals(x.AssociativeTable.ToUpper().Trim(), message.TableName.Trim().ToUpper()));

                    foreach (OpViewColumn columnWithLEAssocTable in columnsWithLEAssocTable)
                    {
                        Tuple<string, object> endTimeColumn = message.Columns.FirstOrDefault(x => String.Equals(x.Item1.ToUpper().Trim(), "END_TIME"));

                        if (endTimeColumn != default(Tuple<string, object>) && endTimeColumn.Item2 != null)
                        {
                            // Mamy END_TIME wiec wpis zostal zamkniety, chcemy go obsluzyc jako "delete"

                            columns.Add(columnWithLEAssocTable.DataType.Name.Replace("HELPER_", string.Empty), columnWithLEAssocTable);
                            
                            return true;
                        }
                    }

                    break;
                }                
                #endregion
                #region DATA
                case "DATA":
                {
                    Tuple<string, object> idDataTypeTuple = message.Columns.FirstOrDefault(x => String.Equals(x.Item1.Trim().ToUpper(), "ID_DATA_TYPE"));
                    if (idDataTypeTuple != default(Tuple<string, object>) && idDataTypeTuple.Item2 != null)
                    {
                        if (Convert.ToInt64(idDataTypeTuple.Item2) == DataType.DEVICE_PHONE_NUMBER)
                        {
                            IEnumerable<OpViewColumn> columnsWithLEAssocTable = view.ColumnsWithAssociativeTable.Where(x => String.Equals(x.AssociativeTable.ToUpper().Trim(), message.TableName.Trim().ToUpper()));
                            foreach (OpViewColumn columnWithLEAssocTable in columnsWithLEAssocTable)
                            {
                                // Ta kolumna powinna zawierac nr tel
                                OpViewColumn parentColumnForColuumnWithAssocTable = view.Columns.FirstOrDefault(x => x.IdViewColumn == columnWithLEAssocTable.IdKeyViewColumn);
                                if (parentColumnForColuumnWithAssocTable != null)
                                {
                                    Tuple<string, object> serialNbrTuple = message.Columns.FirstOrDefault(x => String.Equals(x.Item1.Trim().ToUpper(), "SERIAL_NBR"));
                                    Tuple<string, object> valueTuple = message.Columns.FirstOrDefault(x => String.Equals(x.Item1.Trim().ToUpper(), "VALUE"));
                                    if (serialNbrTuple != default(Tuple<string, object>) && valueTuple != default(Tuple<string, object>) && serialNbrTuple.Item2 != null)
                                    {
                                        string selectClause = string.Format("[{0}],[{1}],[{2}]", view.PrimaryKeyColumn.Name, view.ImrServerColumn.Name, parentColumnForColuumnWithAssocTable.Name);
                                        string whereClause = string.Format("[{0}] = {1} OR [{2}] = {3}", columnWithLEAssocTable.Name, serialNbrTuple.Item2, parentColumnForColuumnWithAssocTable.Name, valueTuple.Item2 == null ? "null" : "'" + valueTuple.Item2 + "'");

                                        Tuple<long, List<Tuple<long, Dictionary<string, object>>>> dataRows = DataProvider.GetFilterFlexView(1, int.MaxValue - 5, view, selectClause, whereClause, null);

                                        if (dataRows.Item1 == 1)
                                        {
                                            string phoneNbr = dataRows.Item2.First().Item2[parentColumnForColuumnWithAssocTable.Name].ToString();
                                            keyColumns[parentColumnForColuumnWithAssocTable.Name] = new List<Tuple<OpViewColumn, object>>();
                                            keyColumns[parentColumnForColuumnWithAssocTable.Name].Add(Tuple.Create(parentColumnForColuumnWithAssocTable, (object)phoneNbr));

                                            columns.Add(parentColumnForColuumnWithAssocTable.Name, parentColumnForColuumnWithAssocTable);

                                            message.Columns.Add(Tuple.Create(parentColumnForColuumnWithAssocTable.Name, (object)phoneNbr));

                                            if (valueTuple.Item2 == null)
                                            {
                                                message.Columns[message.Columns.IndexOf(serialNbrTuple)] = Tuple.Create<string, object>(serialNbrTuple.Item1, null);
                                            }
                                        }
                                        else
                                        {
                                            Log(EventID.ViewsManager.ProceedAssociativeTableMoreThanOneMatch, "DATA", string.Join(" | ", message.Columns.Select(x => string.Format("Column: {0}, Value: {1}", x.Item1, x.Item2))));
                                        }

                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    
                    break;
                }
                #endregion
            }

            return false;
        }

        #endregion        

        #region PrepareDataValue
        //PrepareSqlDataValueClause
        private string PrepareDataValue(object value, OpDataType dataType)
        {
            string retVal = "null";
            if (value == null)
                return retVal;
            switch (dataType.IdDataTypeClass)
            {
                case (int)Enums.DataTypeClass.Integer:
                    //Do obsłużenia ,. w zależności od kultury serwera
                case (int)Enums.DataTypeClass.Real:
                case (int)Enums.DataTypeClass.Decimal:
                    retVal = value.ToString().Replace(',','.');
                    break;
                case (int)Enums.DataTypeClass.Date:
                case (int)Enums.DataTypeClass.Datetime:
                case (int)Enums.DataTypeClass.Time:
                case (int)Enums.DataTypeClass.Text:
                    retVal = "'" + value.ToString() + "'";
                    break;
                case (int)Enums.DataTypeClass.Boolean:
                    retVal = "cast(" + Convert.ToInt32(value) + " as bit)";
                    break;
                default:
                    //throw
                    Log(EventID.ViewsManager.UnsupportedDataTypeClass, dataType.IdDataTypeClass);
                    break;
            }
            return retVal;
        }

        #endregion
        #region PrepareUpdateDict

        private void PrepareUpdateDict(GenericKey idObject, long idObjectIdDataType, DataValue analyzedObj, OpViewColumn currentColumn, List<OpViewColumn> keyColumns,
            ref Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> updateDict)
        {
            if (!updateDict.ContainsKey(idObject))
                updateDict[idObject] = Tuple.Create(idObjectIdDataType, new Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>());
            //Sprawdzamy czy w widoku mamy kolumny które odnoszą się do klucza głównego w tabeli DEVICE
            //inaczej nie da się skontruować warunku na update widoku
            foreach (OpViewColumn vcKeyItem in keyColumns)
            {
                if (!updateDict[idObject].Item2.ContainsKey(vcKeyItem.Name))
                    updateDict[idObject].Item2[vcKeyItem.Name] = new Dictionary<string, OpViewOperation.OpViewOperationCellValue>();

                updateDict[idObject].Item2[vcKeyItem.Name][currentColumn.Name] = new OpViewOperation.OpViewOperationCellValue(currentColumn.Name, analyzedObj.Value, DataProvider.GetDataType(analyzedObj.IdDataType), 0);
            }
        }

        #endregion
        #region PrepareUpdateClauses

        private List<string> PrepareUpdateClauses(OpView view, OpImrServer server, Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> updateDict)
        {
            string updateFormat = "UPDATE [{0}] SET {1} WHERE [" + view.ImrServerColumn.Name + "] = {2} AND {3}";
            List<string> updateClauses = new List<string>();
            foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in updateDict)
            {
                foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                {
                    List<string> setClause = new List<string>();
                    foreach (KeyValuePair<string, OpViewOperation.OpViewOperationCellValue> kvSetClauseItem in kvUpdItem.Value)
                        setClause.Add(String.Format("[{0}] = {1}", kvSetClauseItem.Key, PrepareDataValue(kvSetClauseItem.Value.Value, kvSetClauseItem.Value.DataType)));

                    //"UPDATE [{0}] SET {1} WHERE [" + view.ImrServerColumn.Name + "] = {2} AND {3}";
                    string updateClause = String.Format(updateFormat,
                        view.Name, String.Join(",", setClause), server.IdImrServer, String.Format("[{0}] = {1}", kvUpdItem.Key, PrepareDataValue(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1))));
                    updateClauses.Add(updateClause);
                }
            }
            return updateClauses;
        }

        #endregion
        #region PrepareInsertClauses

        private List<string> PrepareInsertClauses(OpView view, OpImrServer server, Dictionary<GenericKey, Tuple<long, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>> insertDict)
        {
            List<string> insertClauses = new List<string>();

            string insertFormat = "INSERT INTO [{0}] ({1}) VALUES ({2})";
            List<string> updateClauses = new List<string>();
            foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>> kvDevItem in insertDict)
            {
                string columnClause = String.Format("[{0}]", String.Join("],[", kvDevItem.Value.Item2.Keys));
                string valuesClause = String.Empty;
                foreach (OpViewOperation.OpViewOperationCellValue tItem in kvDevItem.Value.Item2.Values)
                    valuesClause += PrepareDataValue(tItem.Value, tItem.DataType) + ",";
                if (valuesClause.Length > 0)
                    valuesClause = valuesClause.Substring(0, valuesClause.Length - 1);

                string insertClause = String.Format(insertFormat, view.Name, columnClause, valuesClause);
                insertClauses.Add(insertClause);                
            }
            return insertClauses;
        }

        #endregion

        #region PrepareMergeClauses

        private List<string> PrepareMergeClauses(OpView view, OpImrServer server, Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> mergeDict)
        {
            string mergeFormat = "MERGE [{0}] AS v USING (SELECT {1}) AS vu ON [{2}] = {3} AND {4} WHEN MATCHED THEN UPDATE SET {5} WHEN NOT MATCHED THEN INSERT ({6}) VALUES ({7});";

            List<string> mergeClauses = new List<string>();
            foreach (KeyValuePair<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> kvDevItem in mergeDict)
            {
                foreach (KeyValuePair<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>> kvUpdItem in kvDevItem.Value.Item2)
                {
                    string temp = string.Empty;

                    string mergeSelectClause = String.Empty;
                    string mergeUpdateClause = String.Empty;
                    string mergeInsertClause = String.Format(",[{0}],[{1}]", view.PrimaryKeyColumn.Name, view.ImrServerColumn.Name);
                    string mergeInsertValuesClause = String.Format(",{0},{1}", PrepareDataValue(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1)), server.IdImrServer);

                    mergeSelectClause = String.Format("{0} AS [{1}], {2} AS [{3}]",
                                server.IdImrServer,
                                view.ImrServerColumn.Name,
                                PrepareDataValue(kvDevItem.Key.KeyValue, DataProvider.GetDataType(kvDevItem.Value.Item1)),
                                view.PrimaryKeyColumn.Name);

                    mergeSelectClause = (String.IsNullOrEmpty(mergeSelectClause) ? "" : mergeSelectClause);
                    temp = String.Join(",", kvUpdItem.Value.Values.Where(v => !String.Equals(v.ColumnName, view.ImrServerColumn.Name) && !String.Equals(v.ColumnName, view.PrimaryKeyColumn.Name))
                                                                  .Select(v => String.Format("{0} as [{1}]", PrepareDataValue(v.Value, v.DataType), v.ColumnName)));
                    if (!string.IsNullOrWhiteSpace(temp))
                    {
                        mergeSelectClause += "," + temp;
                    }

                    mergeInsertClause = String.Format("[{0}],[{1}]", view.ImrServerColumn.Name, view.PrimaryKeyColumn.Name);
                    temp = String.Join(",", kvUpdItem.Value.Values.Where(v => !String.Equals(v.ColumnName, view.ImrServerColumn.Name) && !String.Equals(v.ColumnName, view.PrimaryKeyColumn.Name))
                                                                  .Select(v => String.Format("[{0}]", v.ColumnName)));
                    if (!string.IsNullOrWhiteSpace(temp))
                    {
                        mergeInsertClause += "," + temp;
                    }

                    mergeInsertValuesClause = String.Format("{0},{1}", server.IdImrServer, kvDevItem.Key.KeyValue);
                    temp = String.Join(",", kvUpdItem.Value.Values.Where(v => !String.Equals(v.ColumnName, view.ImrServerColumn.Name) && !String.Equals(v.ColumnName, view.PrimaryKeyColumn.Name))
                                                                  .Select(v => PrepareDataValue(v.Value, v.DataType)));
                    if (!string.IsNullOrWhiteSpace(temp))
                    {
                        mergeInsertValuesClause += "," + temp;
                    }

                    mergeUpdateClause = String.Join(",", kvUpdItem.Value.Values.Select(v => String.Format("[{0}] = {1}", v.ColumnName, PrepareDataValue(v.Value, v.DataType))));

                    string mergeClause = String.Format(mergeFormat, view.Name, mergeSelectClause,
                        view.ImrServerColumn.Name, server.IdImrServer, String.Format("[{0}] = {1}", kvUpdItem.Key, kvDevItem.Key.KeyValue),//OnClause z wymuszeniem warunku na IdImrServer
                        mergeUpdateClause, mergeInsertClause, mergeInsertValuesClause);

                    mergeClauses.Add(mergeClause);
                }
            }

            /* Przykład
                MERGE ACTOR_DETAILS as v
                USING (SELECT 9 as [10045]) as vu
                on v.[10045] = vu.[10045]
                WHEN MATCHED THEN UPDATE SET v.[10080] = 'test';
            */

            return mergeClauses;
        }

        #endregion

        #region PrepareDeleteClauses

        private List<string> PrepareDeleteClauses(OpView view, OpImrServer server, List<Tuple<GenericKey, long>> deleteDict)
        {
            List<string> deleteClauses = new List<string>();

            string deleteFormat = "DELETE FROM [{0}] WHERE [{1}] = {2} AND [{3}] = {4}";
            List<string> updateClauses = new List<string>();
            foreach (Tuple<GenericKey, long> keyItem in deleteDict)
            {
                string deleteClause = String.Format(deleteFormat, view.Name, view.PrimaryKeyColumn.Name, PrepareDataValue(keyItem.Item1.KeyValue, DataProvider.GetDataType(keyItem.Item2)), view.ImrServerColumn.Name, server.IdImrServer);
                deleteClauses.Add(deleteClause);

            }
            return deleteClauses;
        }

        #endregion

        #region ProceedExtendedHelper

        private void ProceedExtendedHelper(OpView view, OpImrServer isItem, OpViewColumn vcExtendedHelper, object[] columnData)
        {
            if (vcExtendedHelper == null)
            {
                return;
            }

            switch (vcExtendedHelper.IdDataType)
            {
                #region HELPER_TIME
                case DataType.HELPER_TIME:
                    {
                        if (columnData == null
                            || columnData.Length < 5
                            || !(columnData[0] is DataValue)
                            || !(columnData[1] is OpViewColumn)
                            || !(columnData[2] is long)
                            || !(columnData[3] is List<OpViewColumn>)
                            || !(columnData[4] is Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> || columnData[4] is Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>>))
                        {
                            break;
                        }

                        DataValue dataValue = (columnData[0] as DataValue);
                        OpViewColumn vcDataItem = columnData[1] as OpViewColumn;
                        long objectKey = (long)columnData[2];
                        List<OpViewColumn> keyColumns = (columnData[3] as List<OpViewColumn>);
                        // Slownik z proceed message
                        // <ObjKey, <KeyColName, <Colname, <ColName, value, datatype, int>>>>
                        Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>> updateDict = columnData[4] as Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>;
                        // Slownik z initialize view
                        // Dictionary<Tuple<TableName, item key>, List<Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>>
                        Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> initializeDict = columnData[4] as Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>>;

                        // Jesli to jest null, tzn. ze prawdopodobnie jestesmy w InitializeView, wiec zeby miec zgodnosc z PrepareUpdateDict tworzymy sobie lokalny slownik,
                        // by potem go przetlumaczyc na ten z InitializeView
                        if (updateDict == null)
                        {
                            updateDict = new Dictionary<GenericKey, Tuple<long, Dictionary<string, Dictionary<string, OpViewOperation.OpViewOperationCellValue>>>>();
                        }

                        int index = (columnData.Length == 6 && columnData[5] is int) ? ((int)columnData[5]) : 0;
                        DateTime? time = null;

                        if (dataValue != null)
                        {
                            if (dataValue is DataCurrentValue)
                            {
                                time = (dataValue as DataCurrentValue).Time;
                            }
                            else if (dataValue is DataMeasureValue)
                            {
                                time = (dataValue as DataMeasureValue).Time;
                            }
                            else
                            {
                                time = DataProvider.DateTimeUtcNow;
                                Log(EventID.ViewsManager.ProceedExtendedHelperHelperTimeLocalTimeUsed, DataType.HELPER_TIME, dataValue.GetType().Name, vcDataItem.Name, objectKey, time.ToString());
                            }

                            if (time.HasValue)
                            {
                                time = DataProvider.ConvertTimeZoneToUtcTime(time.Value);
                            }
                        }

                        dataValue = new DataValue(vcExtendedHelper.IdDataType, index, time);
                        PrepareUpdateDict(new GenericKey(objectKey), keyColumns.First().IdDataType, dataValue, vcExtendedHelper, keyColumns, ref updateDict);

                        if (initializeDict != null)
                        {
                            // InitializeView obsługuje klucz główny, więc powinien tu byc tylko 1 klucz, nie mniej, nie więcej
                            Tuple<string, GenericKey> key = Tuple.Create(keyColumns.First().Name, new GenericKey(objectKey));
                            // Raczej nie powinno się tak zdarzyć
                            if (!initializeDict.ContainsKey(key))
                            {
                                initializeDict.Add(key, new List<OpViewOperation.OpViewOperationCellValue>());
                            }

                            foreach (var kvKeyColData in updateDict[new GenericKey(objectKey)].Item2)
                            {
                                foreach (var kvColumnData in kvKeyColData.Value)
                                {
                                    initializeDict[key].Add(kvColumnData.Value);
                                }
                            }
                        }

                        break;
                    }
                #endregion
            }
        }

        #endregion
        
        #region TranslateChangeActionToViewOperation

        private Enums.ViewOperation TranslateChangeActionToViewOperation(IMR.Suite.Common.ChangeAction changeAction)
        {
            switch (changeAction)
            {
                case IMR.Suite.Common.ChangeAction.Insert: return Enums.ViewOperation.InsertRow; //return Enums.ViewOperation.MergeRow
                case IMR.Suite.Common.ChangeAction.Update: return Enums.ViewOperation.UpdateRow;
                case IMR.Suite.Common.ChangeAction.Delete: return Enums.ViewOperation.DeleteRow;
            }

            throw new InvalidOperationException(string.Format("Can't map ChangeAction.{0} to ViewOperation", changeAction.ToString()));
        }

        private IMR.Suite.Common.ChangeAction TranslateChangeActionToViewOperation(Enums.ViewOperation viewOperation)
        {
            switch (viewOperation)
            {
                case Enums.ViewOperation.InsertRow: return IMR.Suite.Common.ChangeAction.Insert;
                case Enums.ViewOperation.UpdateRow: return IMR.Suite.Common.ChangeAction.Update;
                case Enums.ViewOperation.DeleteRow: return IMR.Suite.Common.ChangeAction.Delete;
            }

            throw new InvalidOperationException(string.Format("Can't map ViewOperation.{0} to ChangeAction", viewOperation.ToString()));
        }

        #endregion

        #region InitializeView

        #region PrepareDistributorFilter

        private void PrepareDistributorFilter(OpView view)
        {
            List<OpViewData> initializeRolesViewData = new List<OpViewData>();
            if (!view.DataList.Any(x => x.ID_DATA_TYPE == DataType.VIEW_INITIALIZE_ID_ROLE))
            {
                initializeRolesViewData = DataProvider.GetViewDataFilter(loadNavigationProperties: false,
                                                                         IdView: new int[] { view.IdView },
                                                                         IdDataType: new long[] { DataType.VIEW_INITIALIZE_ID_ROLE });
            }

            if (initializeRolesViewData != null && initializeRolesViewData.Count > 0 && initializeRolesViewData.First().Value != null)
            {
                view.DataList.Add(initializeRolesViewData.First());

                int idRole = Convert.ToInt32(initializeRolesViewData.First().Value);

                OpOperator tempOp = new OpOperator();

                tempOp.Roles = OperatorComponent.GetRoles((DataProvider)this.DataProvider, tempOp);

                if (!tempOp.Roles.Any(x => x.IdRole == idRole))
                {
                    tempOp.Roles.Add(DataProvider.GetRole(idRole));
                }

                tempOp.Activities = OperatorComponent.GetActivities((DataProvider)this.DataProvider, tempOp, tempOp.Roles);

                List<OpDistributor> allowedDistributor = RoleComponent.FilterByPermission<OpDistributor>(DataProvider.GetAllDistributor(), Activity.ALLOWED_DISTRIBUTOR, tempOp);

                this.DataProvider.DistributorFilter = allowedDistributor.Select(x => x.IdDistributor).ToArray();
            }
        }

        #endregion

        #region InitializeView

        private void InitializeView(OpView view, OpImrServer server, List<Tuple<int, string>> columnCustomWhereClauses)
        {
            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.InitializeViewViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }
            
            InitializeView(view, server, view.Columns, columnCustomWhereClauses, particularColumns: false);
        }
        
        #endregion

        #region InitializeView

        private void InitializeView(OpView view, OpImrServer server, List<OpViewColumn> columnsToInitialize, List<Tuple<int, string>> columnCustomWhereClauses, bool particularColumns = false)
        {
            long viewOperationTotalCount = 0;
            System.Diagnostics.Stopwatch initializationStopwatch = new System.Diagnostics.Stopwatch();
            initializationStopwatch.Start();

            try
            {
                Log(EventID.ViewsManager.InitializeViewStarted, view, server);

                if (!view.IsSynchronizable)
                {
                    Log(EventID.ViewsManager.InitializeViewViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                    return;
                }

                PrepareDistributorFilter(view);

                Enums.ViewOperation changeAction = Enums.ViewOperation.MergeRow; // Zawsze chcemy Merge
                List<OpViewColumn> viewColumnsList = columnsToInitialize;

                if (viewColumnsList == null || viewColumnsList.Count == 0)
                {
                    viewColumnsList = DataProvider.GetViewColumnFilter(loadNavigationProperties: true, loadCustomData: false, IdView: new int[] { view.IdView });
                }

                // Upewniamy się czy mamy klucze dla kolumn do zainicjalizowania
                // oraz czy mamy PK by updatować wiersze.
                if (particularColumns)
                {
                    List<OpViewColumn> keyColumnsToAdd = new List<OpViewColumn>();
                    foreach (OpViewColumn column in viewColumnsList)
                    {
                        if (column.IdKeyViewColumn.HasValue)
                        {
                            if (!viewColumnsList.Any(x => x.IdViewColumn == column.IdKeyViewColumn))
                            {
                                keyColumnsToAdd.Add(DataProvider.GetViewColumn(column.IdKeyViewColumn.Value));
                            }
                        }
                    }
                    viewColumnsList.AddRange(keyColumnsToAdd);

                    if (!viewColumnsList.Any(x => x.PrimaryKey && x.IdDataType != DataType.HELPER_ID_IMR_SERVER))
                    {
                        OpViewColumn pk = view.Columns.FirstOrDefault(x => x.PrimaryKey && x.IdDataType != DataType.HELPER_ID_IMR_SERVER);

                        if (pk == null)
                        {
                            pk = DataProvider.GetViewColumnFilter(loadNavigationProperties: true, loadCustomData: false, IdView: new int[] { view.IdView }, PrimaryKey: true).First(x => x.IdDataType != DataType.HELPER_ID_IMR_SERVER);
                            view.Columns.Add(pk);
                        }

                        viewColumnsList.Add(pk);
                    }

                    //changeAction = Enums.ViewOperation.UpdateRow; // Wyżej jest ustawione Merge, zawsze chcemy Merge
                }

                OpViewColumn pkColumn = viewColumnsList.Find(x => x.PrimaryKey);

                // Czy tylko PK widoku ma ID_KEY_VIEW_COLUMN == null?
                Dictionary<int, List<OpViewColumn>> dependantViewColumnsDict = viewColumnsList.Where(x => x.IdKeyViewColumn.HasValue)
                                                                                              .GroupBy(k => k.IdKeyViewColumn.Value)
                                                                                              .ToDictionary(k => k.Key, v => v.ToList());

                if (pkColumn != null)
                {
                    Log(EventID.ViewsManager.InitializeViewViewPrimaryKeyColumn, pkColumn);

                    Enums.Tables pkTable = Enum.GetValues(typeof(Enums.Tables)).Cast<Enums.Tables>().FirstOrDefault(x => String.Equals(x.ToString().ToUpper(), pkColumn.Table.ToUpper()));

                    if (pkTable != default(Enums.Tables))
                    {
                        Log(EventID.ViewsManager.InitializeViewPrimaryKeyTable, pkTable);

                        ClearCaches();

                        #region Parse customWhereClauses

                        Dictionary<int, string> columnCustomWhereClausesDict = null;
                        if (columnCustomWhereClauses != null && columnCustomWhereClauses.Count > 0)
                        {
                            columnCustomWhereClausesDict = dependantViewColumnsDict.Select(x => new KeyValuePair<int, string>(x.Key,
                                string.Join(" AND ", columnCustomWhereClauses.Where(y => y.Item1.In(x.Value.Where(g => g.IdViewColumnType == (int)Enums.ViewColumnType.BaseHelper).Select(z => z.IdViewColumn).ToArray())).Select(v => v.Item2))))
                            .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                            .ToDictionary(k => k.Key, v => v.Value);

                            foreach (OpViewColumn viewColumn in viewColumnsList.Where(x => x.IdViewColumnType == (int)Enums.ViewColumnType.Standard && x.IdViewColumn.In(columnCustomWhereClauses.Select(y => y.Item1).ToArray())))
                            {
                                if (!columnCustomWhereClausesDict.ContainsKey(viewColumn.IdViewColumn))
                                {
                                    columnCustomWhereClausesDict.Add(viewColumn.IdViewColumn, columnCustomWhereClauses.First(x => x.Item1 == viewColumn.IdViewColumn).Item2);
                                }
                            }   
                        }

                        #endregion
                        
                        GenericKey initializationPkValue = new GenericKey(0);

                        // Chcemy brać batch size co inicjalizacje by jak coś zmienic sobie wartość w locie
                        // bez potrzeby resetowania VM
                        int initializationBatchSize = 0;
                        if (System.Configuration.ConfigurationManager.AppSettings.Keys.Cast<string>().Any(x => x == InitializationBatchSizeConfigurationKey))
                        {
                            string batchSizeStr = System.Configuration.ConfigurationManager.AppSettings[InitializationBatchSizeConfigurationKey].ToString();

                            int.TryParse(batchSizeStr, out initializationBatchSize);
                        }

                        if (initializationBatchSize == 0)
                        {
                            initializationBatchSize = int.MaxValue;
                        }

                        GenericKey lastPkValue = null;

                        do
                        {
                            ClearCaches();

                            lastPkValue = initializationPkValue;

                            // Dictionary<Tuple<TableName, item key>, List<Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>>
                            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result = ProceedViewInitialization(view,
                                server,
                                pkColumn,
                                pkColumn,
                                dependantViewColumnsDict,
                                IsTreeView(view),
                                initializationBatchSize,
                                columnCustomWhereClausesDict,
                                ref initializationPkValue);

                            foreach (var resultKvPair in result)
                            {
                                // na tym poziomie item key to powinien być PK więc interesuje nas datatype z PK
                                OpViewOperation vroItem = new OpViewOperation(resultKvPair.Key.Item2.KeyValue, pkColumn.DataType, server, view, changeAction, resultKvPair.Value)
                                {
                                    KeyColumnName = pkColumn.Name,
                                };

                                viewOperationTotalCount++;

                                lock (this.ViewOperationQueue)
                                {
                                    this.ViewOperationQueue.Enqueue(vroItem);
                                }
                            }
                        }
                        while (initializationPkValue > lastPkValue);
                    }
                    else
                    {
                        Log(EventID.ViewsManager.InitializeViewUnknownTable, pkColumn.Table);
                    }
                }
                else
                {
                    Log(EventID.ViewsManager.InitializeViewMissingPrimaryKeyColumn, view.Name);
                }

                ClearCaches();
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.InitializeViewException, ex.ToString());
            }
            finally
            {
                initializationStopwatch.Stop();
                Log(EventID.ViewsManager.InitializeViewFinished, view, server, viewOperationTotalCount, initializationStopwatch.Elapsed.TotalSeconds);
            }
        }
        
        #endregion

        #region ProceedViewInitialization

        private Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> ProceedViewInitialization(
            OpView view,
            OpImrServer server,
            OpViewColumn primaryKeyColumn,
            OpViewColumn parentViewColumn,
            Dictionary<int, List<OpViewColumn>> dependantViewColumnsDict,
            bool isTreeView,
            int initializationBatchSize,
            Dictionary<int, string> columnCustomWhereClausesDict,
            ref GenericKey initializationPkValue)
        {
            // Dictionary<Tuple<TableName, item key>, List<Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>>
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result = new Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>>();

            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedViewInitializationViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return result;
            }

            Log(EventID.ViewsManager.ProceedViewInitializationInitializeColumn, view, parentViewColumn);

            if (parentViewColumn == null || dependantViewColumnsDict == null)
            {
                Log(EventID.ViewsManager.ProceedViewInitializationMissingParentViewColumnOrDependantColumns, parentViewColumn == null, dependantViewColumnsDict == null);
                return result;
            }

            if (!DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.HasValue)
            {
                Log(EventID.ViewsManager.ProceedViewInitializationMissingReferenceTypeForParentColumn, parentViewColumn.IdViewColumn, parentViewColumn.IdDataType);
            }

            List<OpViewColumn> columnsToProceed = new List<OpViewColumn>();
            columnsToProceed.Add(parentViewColumn); // Zawsze pierwszego chcemy obrobić parenta
            if (dependantViewColumnsDict.ContainsKey(parentViewColumn.IdViewColumn))
            {
                columnsToProceed.AddRange(dependantViewColumnsDict[parentViewColumn.IdViewColumn]);
            }

            Log(EventID.ViewsManager.ProceedViewInitializationBeginColumnAnalyze, columnsToProceed.Count);

                                                                           // Wszystkie base helpery które nie są kluczami ustawiamy po ściągnięciu danych
            foreach (OpViewColumn analyzedColumn in columnsToProceed.Except(columnsToProceed.Where(x => !x.Key && x.IdViewColumnType == (int)Enums.ViewColumnType.BaseHelper)))
            {
                OpViewColumn columnToProceed = new OpViewColumn(analyzedColumn);
                columnToProceed.DataType = analyzedColumn.DataType;
                columnToProceed.DataList.AddRange(analyzedColumn.DataList);
                IEnumerable<IOpData> joiningData = null;
                Func<IOpData, GenericKey> joiningKeySelector = null;

                //IEnumerable<OpViewColumn> baseHelperColumns = columnsToProceed.Where(x => !x.Key && x.IdViewColumnType == (int)Enums.ViewColumnType.BaseHelper && x.IdKeyViewColumn == columnToProceed.IdViewColumn);
                IEnumerable<OpViewColumn> baseHelperColumns = columnsToProceed.Where(x => !x.Key
                    && x.IdViewColumnType == (int)Enums.ViewColumnType.BaseHelper
                    && x.IdKeyViewColumn == columnToProceed.IdViewColumn
                    && String.Equals(x.Table.ToUpper().Trim(), GetAssociativeTableName(columnToProceed).ToUpper().Trim()));

                if (TableNameToEnum(columnToProceed.Table) == Enums.Tables.UNKNOWN)
                {
                    Log(EventID.ViewsManager.ProceedViewInitializationUnknownTable, columnToProceed.Table);
                    continue;
                }

                DateTime beginAnalyze = DateTime.Now;

                GenericKey lastPkValue = initializationPkValue;
                
                try
                {
                    if (isTreeView && IsHierarchyColumn(view, columnToProceed))
                    {
                        ProceedHierarchyColumn(view, server, columnToProceed, result);
                    }
                    else
                    {
                        #region Tables

                        //switch (TableNameToEnum(columnToProceed.Table))
                        switch (TableNameToEnum(GetAssociativeTableName(columnToProceed)))
                        {
                            case Enums.Tables.ACTION:
                            {
                                #region ACTION

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpAction, UI.Business.Objects.CORE.OpActionData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdAction,
                                    baseHelperColumns,
                                    ActionCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.ActionComponent.SetHelperValues,
                                    op => new GenericKey((op as OpAction).IdAction),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetActionFilter(IdAction: keys,
                                        SerialNbr: GetKeysFromCache(server, Enums.ReferenceType.SerialNbr),
                                        IdMeter: GetKeysFromCache(server, Enums.ReferenceType.IdMeter),
                                        IdLocation: GetKeysFromCache(server, Enums.ReferenceType.IdLocation),
                                        IdOperator: GetKeysFromCacheInt(server, Enums.ReferenceType.IdOperator),
                                        IdActionType: null, IdActionStatus: null, IdActionData: null, IdActionParent: null, IdDataArch: null, IdModule: null,
                                        CreationDate: null,
                                        loadNavigationProperties: false,
                                        mergeIntoCache: false,
                                        transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ACTOR:
                            {
                                #region ACTOR

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpActor, UI.Business.Objects.Custom.OpActorData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdActor,
                                    baseHelperColumns,
                                    ActorCache,
                                    result,
                                    ActorComponent.SetHelperValues,
                                    op => new GenericKey((op as OpActor).IdActor),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetActorFilter(IdActor: keys,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ALARM:
                            {
                                #region ALARM

                                ProceedColumnData<DataProvider, UI.Business.Objects.DW.OpAlarm, UI.Business.Objects.Custom.OpAlarmData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdAlarm,
                                    baseHelperColumns,
                                    AlarmCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.DW.AlarmComponent.SetHelperValues,
                                    op => new GenericKey((op as UI.Business.Objects.DW.OpAlarm).IdAlarm),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetAlarmFilterDW(IdAlarm: keys,
                                        IdAlarmEvent: GetKeysFromCache(server, Enums.ReferenceType.IdAlarmEvent),
                                        IdAlarmDef: GetKeysFromCacheInt(server, Enums.ReferenceType.IdAlarmDef),
                                        SerialNbr: GetKeysFromCache(server, Enums.ReferenceType.SerialNbr),
                                        IdMeter: GetKeysFromCache(server, Enums.ReferenceType.IdMeter),
                                        IdLocation: GetKeysFromCache(server, Enums.ReferenceType.IdLocation),
                                        loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ALARM_EVENT:
                            {
                                #region ALARM_EVENT

                                ProceedColumnData<DataProvider, UI.Business.Objects.DW.OpAlarmEvent, UI.Business.Objects.Custom.OpAlarmEventData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdAlarmEvent,
                                    baseHelperColumns,
                                    AlarmEventCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.DW.AlarmComponent.SetHelperValues,
                                    op => new GenericKey((op as UI.Business.Objects.DW.OpAlarmEvent).IdAlarmEvent),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetAlarmEventFilterDW(IdAlarmEvent: keys,
                                        IdAlarmDef: GetKeysFromCache(server, Enums.ReferenceType.IdAlarmDef),
                                        SerialNbr: GetKeysFromCache(server, Enums.ReferenceType.SerialNbr),
                                        IdMeter: GetKeysFromCache(server, Enums.ReferenceType.IdMeter),
                                        IdLocation: GetKeysFromCache(server, Enums.ReferenceType.IdLocation),
                                        loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ALARM_MESSAGE:
                            {
                                #region ALARM_MESSAGE

                                ProceedColumnData<DataProvider, UI.Business.Objects.DW.OpAlarmMessage>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    (int)Enums.ReferenceType.IdAlarmEvent,
                                    ref initializationPkValue,
                                    result,
                                    op => new GenericKey((op as UI.Business.Objects.DW.OpAlarmMessage).IdAlarmEvent),
                                    keys => this.DataProvider.GetAlarmMessageFilterDW(IdAlarmEvent: SelectKeys<long>(keys, server, Enums.ReferenceType.IdAlarmEvent),
                                        loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                        topCount: GetTopCountValue(server, columnToProceed, initializationBatchSize),
                                        customWhereClause: CreateCustomWhereClause(server, columnToProceed, "ID_ALARM_EVENT", lastPkValue, columnCustomWhereClausesDict)));

                                break;
                                #endregion
                            }
                            case Enums.Tables.DEVICE:
                            {
                                #region DEVICE

                                ProceedColumnData<DataProvider, OpDevice, OpData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.SerialNbr,
                                    baseHelperColumns,
                                    DeviceCache,
                                    result,
                                    DeviceComponent.SetHelperValues,
                                    op => new GenericKey((op as OpDevice).SerialNbr),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetDeviceFilter(SerialNbr: keys,
                                        loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                CacheLocationEquipment(server, (int)Enums.ReferenceType.SerialNbr);

                                break;
                                #endregion
                            }
                            case Enums.Tables.DEVICE_CONNECTION:
                            {
                                #region DEVICE_CONNECTION

                                ProceedColumnData<DataProvider, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection, OpDeviceConnectionData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdDeviceConnection,
                                    baseHelperColumns,
                                    DeviceConnectionCache,
                                    result,
                                    DeviceComponent.SetHelperValues,
                                    op => new GenericKey((op as IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection).IdDeviceConnection),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetDeviceConnectionFilter(IdDeviceConnection: keys,
                                        SerialNbr: GetKeysFromCache(server, Enums.ReferenceType.SerialNbr),
                                        SerialNbrParent: GetKeysFromCache(server, Enums.ReferenceType.SerialNbr),
                                        loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.DISTRIBUTOR:
                            {
                                #region DISTRIBUTOR

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpDistributor, UI.Business.Objects.CORE.OpDistributorData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdDistributor,
                                    baseHelperColumns,
                                    DistributorCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.DistributorComponent.SetHelperValues,
                                    op => new GenericKey((op as OpDistributor).IdDistributor),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetDistributorFilter(IdDistributor: keys,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ISSUE:
                            {
                                #region ISSUE

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpIssue, UI.Business.Objects.CORE.OpIssueData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdIssue,
                                    baseHelperColumns,
                                    IssueCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.IssueComponent.SetHelperValues,
                                    op => new GenericKey(op.IdIssue),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetIssueFilter(IdIssue: keys,
                                        IdActor: GetKeysFromCacheInt(server, Enums.ReferenceType.IdActor),
                                        IdOperatorRegistering: GetKeysFromCacheInt(server, Enums.ReferenceType.IdOperator),
                                        IdOperatorPerformer: GetKeysFromCacheInt(server, Enums.ReferenceType.IdOperator),
                                        IdLocation: GetKeysFromCache(server, Enums.ReferenceType.IdLocation),
                                        topCount: topCount,
                                        customWhereClause: customWhere,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180));

                                break;
                                #endregion
                            }
                            case Enums.Tables.LOCATION:
                            {
                                #region LOCATION

                                ProceedColumnData<DataProvider, OpLocation, OpData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdLocation,
                                    baseHelperColumns,
                                    LocationCache,
                                    result,
                                    LocationComponent.SetHelperValues,
                                    op => new GenericKey((op as OpLocation).IdLocation),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetLocationFilter(IdLocation: SelectKeys<long>(keys, server, Enums.ReferenceType.IdLocation),
                                        loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                CacheLocationEquipment(server, (int)Enums.ReferenceType.IdLocation);

                                break;
                                #endregion
                            }
                            case Enums.Tables.METER:
                            {
                                #region METER

                                ProceedColumnData<DataProvider, OpMeter, OpData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdMeter,
                                    baseHelperColumns,
                                    MeterCache,
                                    result,
                                    MeterComponent.SetHelperValues,
                                    op => new GenericKey((op as OpMeter).IdMeter),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetMeterFilter(IdMeter: keys,
                                            loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                            topCount: topCount,
                                            customWhereClause: customWhere));

                                CacheLocationEquipment(server, (int)Enums.ReferenceType.IdMeter);

                                break;

                                #endregion
                            }
                            case Enums.Tables.OPERATOR:
                            {
                                #region OPERATOR

                                ProceedColumnData<DataProvider, OpOperator, OpOperatorData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdOperator,
                                    baseHelperColumns,
                                    OperatorCache,
                                    result,
                                    OperatorComponent.SetHelperValues,
                                    op => new GenericKey((op as OpOperator).IdOperator),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetOperatorFilter(IdOperator: keys,
                                        IdActor: GetKeysFromCacheInt(server, Enums.ReferenceType.IdActor),
                                        loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.REPORT:
                            {
                                #region REPORT

                                ProceedColumnData<DataProvider, UI.Business.Objects.DW.OpReport, UI.Business.Objects.DW.OpReportData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdReport,
                                    baseHelperColumns,
                                    ReportCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.ReportComponent.SetHelperValues,
                                    op => new GenericKey((op as IMR.Suite.UI.Business.Objects.DW.OpReport).IdReport),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetReportFilter(IdReport: keys,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.ROUTE:
                            {
                                #region ROUTE

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpRoute, UI.Business.Objects.CORE.OpRouteData, long>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdRoute,
                                    baseHelperColumns,
                                    RouteCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.RouteComponent.SetHelperValues,
                                    op => new GenericKey((op as OpRoute).IdRoute),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetRouteFilter(IdRoute: keys,
                                        IdRouteDef: GetKeysFromCacheInt(server, Enums.ReferenceType.IdRouteDef),
                                        IdOperatorExecutor: GetKeysFromCacheInt(server, Enums.ReferenceType.IdOperator),
                                        IdOperatorApproved: GetKeysFromCacheInt(server, Enums.ReferenceType.IdOperator),
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.SIM_CARD:
                            {
                                #region SIM_CARD

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpSimCard, UI.Business.Objects.CORE.OpSimCardData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdSimCard,
                                    baseHelperColumns,
                                    SimCardCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.SimCardComponent.SetHelperValues,
                                    op => new GenericKey((op as OpSimCard).IdSimCard),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetSimCardFilter(IdSimCard: keys,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.TARIFF:
                            {
                                #region TARIFF

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpTariff, UI.Business.Objects.CORE.OpTariffData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdTariff,
                                    baseHelperColumns,
                                    TariffCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.TariffComponent.SetHelperValues,
                                    op => new GenericKey((op as OpTariff).IdTariff),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere)  => this.DataProvider.GetTariffFilter(IdTariff: keys,
                                        loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            case Enums.Tables.TASK:
                            {
                                #region TASK

                                ProceedColumnData<DataProvider, UI.Business.Objects.CORE.OpTask, UI.Business.Objects.CORE.OpTaskData, int>(
                                    view,
                                    server,
                                    columnToProceed,
                                    parentViewColumn,
                                    ref initializationPkValue,
                                    Enums.ReferenceType.IdTask,
                                    baseHelperColumns,
                                    TaskCache,
                                    result,
                                    IMR.Suite.UI.Business.Components.CORE.TaskComponent.SetHelperValues,
                                    op => new GenericKey((op as OpTask).IdTask),
                                    initializationBatchSize,
                                    columnCustomWhereClausesDict,
                                    (keys, topCount, customWhere) => this.DataProvider.GetTaskFilter(IdTask: keys,
                                        IdIssue: GetKeysFromCacheInt(server, Enums.ReferenceType.IdIssue),
                                        IdLocation: GetKeysFromCache(server, Enums.ReferenceType.IdLocation),
                                        loadNavigationProperties: false, mergeIntoCache: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                        topCount: topCount,
                                        customWhereClause: customWhere));

                                break;
                                #endregion
                            }
                            // === *DATA
                            case Enums.Tables.DATA:
                            {
                                #region DATA

                                int idParentReferenceType = DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value;
                                long[] dataKeys = GetKeysFromCache(server, idParentReferenceType);

                                if (dataKeys == null || dataKeys.Length == 0)
                                {
                                    break;
                                }

                                if (columnToProceed.IdViewColumnSource == (int)Enums.ViewColumnSource.DW)
                                {
                                    #region DW

                                    List<IMR.Suite.UI.Business.Objects.DW.OpData> dataList = new List<IMR.Suite.UI.Business.Objects.DW.OpData>();
                                    switch ((Enums.ReferenceType)idParentReferenceType)
                                    {
                                        case Enums.ReferenceType.IdLocation:
                                            dataList = DataProvider.GetDataFilterDW(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                IdLocation: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                        case Enums.ReferenceType.IdMeter:
                                            dataList = DataProvider.GetDataFilterDW(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                IdMeter: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                        case Enums.ReferenceType.SerialNbr:
                                            dataList = DataProvider.GetDataFilterDW(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                SerialNbr: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                    }

                                    // Skoro data jest kluczem to znaczy, że zawiera tak naprawde ID który będzie kluczem
                                    if (columnToProceed.Key)
                                    {
                                        switch ((Enums.ReferenceType)idParentReferenceType)
                                        {
                                            case Enums.ReferenceType.IdLocation:
                                                joiningKeySelector = o => new GenericKey((o as IMR.Suite.UI.Business.Objects.DW.OpData).IdLocation.Value);
                                                break;
                                            case Enums.ReferenceType.IdMeter:
                                                joiningKeySelector = o => new GenericKey((o as IMR.Suite.UI.Business.Objects.DW.OpData).IdMeter.Value);
                                                break;
                                            case Enums.ReferenceType.SerialNbr:
                                                joiningKeySelector = o => new GenericKey((o as IMR.Suite.UI.Business.Objects.DW.OpData).SerialNbr.Value);
                                                break;
                                        }

                                        joiningData = dataList.Cast<IOpData>();

                                        // Tutaj nie korzystamy z Table ani datatypu ponieważ tabele źródłową determinuje reference type
                                        columnToProceed.Table = ReferenceTypeToTable(columnToProceed.DataType.IdReferenceType.Value);
                                        columnToProceed.DataType = ReferenceTypeToKeyDataType(columnToProceed.DataType.IdReferenceType.Value);

                                        Log(EventID.ViewsManager.ProceedViewInitializationProceedingDataColumnKeyTable, columnToProceed.Table, columnToProceed.DataType.IdDataType);
                                    }
                                    else
                                    {
                                        #region ExtendedHelper

                                        List<OpViewColumn> vcExtendedHelpers = view.Columns.Where(x => x.IdKeyViewColumn == columnToProceed.IdViewColumn && x.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
                                                                                           .ToList();
                                        object[] extendedHelperData = new object[6];
                                        extendedHelperData[1] = columnToProceed;
                                        extendedHelperData[3] = new List<OpViewColumn>() { parentViewColumn };
                                        extendedHelperData[4] = result;

                                        #endregion

                                        int columnIdx = columnToProceed.ColumnIndexNbr;
                                        foreach (IMR.Suite.UI.Business.Objects.DW.OpData data in dataList)
                                        {
                                            try
                                            {
                                                GenericKey parentKey;
                                                switch ((Enums.ReferenceType)idParentReferenceType)
                                                {
                                                    case Enums.ReferenceType.IdLocation:
                                                        if (!data.IdLocation.HasValue) continue;
                                                        parentKey = new GenericKey(data.IdLocation.Value);
                                                        break;
                                                    case Enums.ReferenceType.IdMeter:
                                                        if (!data.IdMeter.HasValue) continue;
                                                        parentKey = new GenericKey(data.IdMeter.Value);
                                                        break;
                                                    case Enums.ReferenceType.SerialNbr:
                                                        if (!data.SerialNbr.HasValue) continue;
                                                        parentKey = new GenericKey(data.SerialNbr.Value);
                                                        break;
                                                    default:
                                                        throw new Exception("Invalid OpData key");
                                                }

                                                Tuple<string, GenericKey> key = Tuple.Create(parentViewColumn.Table, parentKey);

                                                if (result.ContainsKey(key))
                                                {
                                                    if (result[key].FirstOrDefault(x => x.ColumnName == columnToProceed.Name && x.Index == data.Index) == null
                                                        && columnIdx == data.Index)
                                                    {
                                                        result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, data.Value, columnToProceed.DataType, data.Index));

                                                        #region ExtendedHelper

                                                        foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers)
                                                        {
                                                            extendedHelperData[0] = new DataCurrentValue(data.IdData,
                                                                data.SerialNbr.HasValue ? new SN((ulong)data.SerialNbr.Value) : null, data.IdMeter, data.IdLocation, data.IdDataType, data.Index, data.Value, data.Time,
                                                                null, null, null, null);
                                                            extendedHelperData[2] = parentKey;
                                                            extendedHelperData[5] = data.Index;

                                                            ProceedExtendedHelper(view, server, vcExtendedHelper, extendedHelperData);
                                                        }

                                                        #endregion
                                                    }
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                Log(EventID.ViewsManager.ProceedViewInitializationException, ex.ToString());
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Core

                                    List<OpData> dataList = new List<OpData>();
                                    switch ((Enums.ReferenceType)idParentReferenceType)
                                    {
                                        case Enums.ReferenceType.IdLocation:
                                            dataList = DataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                IdLocation: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                        case Enums.ReferenceType.IdMeter:
                                            dataList = DataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                IdMeter: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                        case Enums.ReferenceType.SerialNbr:
                                            dataList = DataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                SerialNbr: dataKeys, IdDataType: new long[] { columnToProceed.IdDataType }, commandTimeout: 180,
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict));
                                            break;
                                    }

                                    // Skoro data jest kluczem to znaczy, że zawiera tak naprawde ID który będzie kluczem
                                    if (columnToProceed.Key)
                                    {
                                        switch ((Enums.ReferenceType)idParentReferenceType)
                                        {
                                            case Enums.ReferenceType.IdLocation:
                                                joiningKeySelector = o => new GenericKey((o as OpData).IdLocation.Value);
                                                break;
                                            case Enums.ReferenceType.IdMeter:
                                                joiningKeySelector = o => new GenericKey((o as OpData).IdMeter.Value);
                                                break;
                                            case Enums.ReferenceType.SerialNbr:
                                                joiningKeySelector = o => new GenericKey((o as OpData).SerialNbr.Value);
                                                break;
                                        }

                                        joiningData = dataList.Cast<IOpData>();

                                        // Tutaj nie korzystamy z Table ani datatypu ponieważ tabele źródłową determinuje reference type
                                        columnToProceed.Table = ReferenceTypeToTable(columnToProceed.DataType.IdReferenceType.Value);
                                        columnToProceed.DataType = ReferenceTypeToKeyDataType(columnToProceed.DataType.IdReferenceType.Value);

                                        Log(EventID.ViewsManager.ProceedViewInitializationProceedingDataColumnKeyTable, columnToProceed.Table, columnToProceed.DataType.IdDataType);
                                    }
                                    else
                                    {
                                        #region ExtendedHelper

                                        List<OpViewColumn> vcExtendedHelpers = view.Columns.Where(x => x.IdKeyViewColumn == columnToProceed.IdViewColumn && x.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
                                                                                           .ToList();
                                        object[] extendedHelperData = new object[6];
                                        extendedHelperData[0] = new DataValue();
                                        extendedHelperData[1] = columnToProceed;
                                        extendedHelperData[3] = new List<OpViewColumn>() { parentViewColumn };
                                        extendedHelperData[4] = result;

                                        #endregion

                                        int columnIdx = columnToProceed.ColumnIndexNbr;
                                        foreach (OpData data in dataList)
                                        {
                                            try
                                            {
                                                GenericKey parentKey;
                                                switch ((Enums.ReferenceType)idParentReferenceType)
                                                {
                                                    case Enums.ReferenceType.IdLocation:
                                                        if (!data.IdLocation.HasValue) continue;
                                                        parentKey = new GenericKey(data.IdLocation.Value);
                                                        break;
                                                    case Enums.ReferenceType.IdMeter:
                                                        if (!data.IdMeter.HasValue) continue;
                                                        parentKey = new GenericKey(data.IdMeter.Value);
                                                        break;
                                                    case Enums.ReferenceType.SerialNbr:
                                                        if (!data.SerialNbr.HasValue) continue;
                                                        parentKey = new GenericKey(data.SerialNbr.Value);
                                                        break;
                                                    default:
                                                        throw new Exception("Invalid OpData key");
                                                }

                                                Tuple<string, GenericKey> key = Tuple.Create(parentViewColumn.Table, parentKey);

                                                if (result.ContainsKey(key))
                                                {
                                                    if (result[key].FirstOrDefault(x => x.ColumnName == columnToProceed.Name && x.Index == data.Index) == null
                                                        && columnIdx == data.Index)
                                                    {
                                                        result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, data.Value, columnToProceed.DataType, data.Index));

                                                        #region ExtendedHelper

                                                        foreach (OpViewColumn vcExtendedHelper in vcExtendedHelpers)
                                                        {
                                                            extendedHelperData[2] = parentKey;
                                                            extendedHelperData[5] = data.Index;

                                                            ProceedExtendedHelper(view, server, vcExtendedHelper, extendedHelperData);
                                                        }
                                                        
                                                        #endregion
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log(EventID.ViewsManager.ProceedViewInitializationException, ex.ToString());
                                            }
                                        }
                                    }

                                    #endregion
                                }

                                break;

                                #endregion
                            }
                            case Enums.Tables.DISTRIBUTOR_DATA:
                            {
                                #region DISTRIBUTOR_DATA

                                ProceedDataColumnData<OpDistributorData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpDistributorData).IdDistributor),
                                    keys => DataProvider.GetDistributorDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdDistributor: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.ISSUE_DATA:
                            {
                                #region ISSUE_DATA

                                ProceedDataColumnData<OpIssueData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpIssueData).IdIssue),
                                    keys => DataProvider.GetIssueDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdIssue: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.OPERATOR_DATA:
                            {
                                #region OPERATOR_DATA

                                ProceedDataColumnData<OpOperatorData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpOperatorData).IdOperator),
                                    keys => DataProvider.GetOperatorDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                                IdOperator: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.REPORT_DATA:
                            {
                                #region REPORT_DATA

                                ProceedDataColumnData<IMR.Suite.UI.Business.Objects.DW.OpReportData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as IMR.Suite.UI.Business.Objects.DW.OpReportData).IdReport),
                                    keys => DataProvider.GetReportDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdReport: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.ROUTE_DATA:
                            {
                                #region ROUTE_DATA

                                ProceedDataColumnData<OpRouteData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpRouteData).IdRoute),
                                    keys => DataProvider.GetRouteDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdRoute: keys.Select(x => x.AsLong()).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.SIM_CARD_DATA:
                            {
                                #region SIM_CARD_DATA

                                ProceedDataColumnData<OpSimCardData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpSimCardData).IdSimCard),
                                    keys => DataProvider.GetSimCardDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                                IdSimCard: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.TARIFF_DATA:
                            {
                                #region TARIFF_DATA

                                ProceedDataColumnData<OpTariffData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpTariffData).IdTariff),
                                    keys => DataProvider.GetTariffDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdTariff: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                            case Enums.Tables.TASK_DATA:
                            {
                                #region TASK_DATA

                                ProceedDataColumnData<OpTaskData>(server,
                                    columnToProceed,
                                    parentViewColumn,
                                    DataProvider.GetDataType(parentViewColumn.IdDataType).IdReferenceType.Value,
                                    result,
                                    out joiningKeySelector,
                                    out joiningData,
                                    o => new GenericKey((o as OpTaskData).IdTask),
                                    keys => DataProvider.GetTaskDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180,
                                                                            IdTask: keys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { columnToProceed.IdDataType },
                                                                            customWhereClause: CreateCustomWhereClause(server, columnToProceed, columnCustomWhereClausesDict))
                                    );

                                break;
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.ProceedViewInitializationProceedingColumn, columnToProceed.Name, columnToProceed.Key, columnToProceed.ColumnIndexNbr, columnToProceed.Table, (DateTime.Now - beginAnalyze).TotalSeconds);
                    throw ex;
                }
                Log(EventID.ViewsManager.ProceedViewInitializationProceedingColumn, columnToProceed.Name, columnToProceed.Key, columnToProceed.ColumnIndexNbr, columnToProceed.Table, (DateTime.Now - beginAnalyze).TotalSeconds);

                // Jeśli po pobraniu danych w obecnym batchu mamy ten sam największy klucz, tzn., że
                // nic nowego się nie pobrało i nie kontynuujemy inicjalizacji.
                // Zapis !(>) jest podyktowany tym, że nie ma przeciązonego operatora <= w GenericKey.
                if (columnToProceed.PrimaryKey && columnToProceed.IdDataType != DataType.HELPER_ID_IMR_SERVER && !(initializationPkValue > lastPkValue))
                {
                    break;
                }

                //if (columnToProceed.Key && !String.Equals(parentViewColumn.Table.ToUpper().Trim(), columnToProceed.Table.ToUpper().Trim()))
                if (columnToProceed.Key && parentViewColumn.IdViewColumn != columnToProceed.IdViewColumn && view.ImrServerColumn != columnToProceed)
                {
                    Log(EventID.ViewsManager.ProceedViewInitializationProceedingKeyColumn, columnToProceed.Name, columnToProceed.Table);

                    Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> recurentDict = ProceedViewInitialization(view,
                        server,
                        primaryKeyColumn,
                        columnToProceed,
                        dependantViewColumnsDict,
                        isTreeView,
                        initializationBatchSize,
                        columnCustomWhereClausesDict,
                        ref initializationPkValue);
                    
                    // Tabelka DATA i pewnie wszystkie inne *_DATA mogą się zachowywać jako tabelki złączeniowe
                    // jeśli sa kluczem, więc można je potraktować inaczej
                    if (analyzedColumn.Table.ToUpper().Trim().EndsWith("DATA"))
                    {
                        Log(EventID.ViewsManager.ProceedViewInitializationMergingType, "by joining data collection");
                        result = MergeTableDictionaries(server, result, recurentDict, joiningData, joiningKeySelector);
                    }
                    else
                    {
                        Log(EventID.ViewsManager.ProceedViewInitializationMergingType, "by deduction");
                        result = MergeTableDictionaries(server, result, recurentDict,
                            dependantViewColumnsDict.ContainsKey(columnToProceed.IdViewColumn) ? dependantViewColumnsDict[columnToProceed.IdViewColumn].Union(columnsToProceed).ToList() : columnsToProceed,
                            columnToProceed);
                    }
                }
            }

            Log(EventID.ViewsManager.ProceedViewInitializationEndColumnAnalyze, result.Count);

            return result;
        }
        
        #endregion
        
        #region MergeTableDictionaries

        #region MergeTableDictionaries

        private Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> MergeTableDictionaries(
            OpImrServer isItem,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> sourceTableDict,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> joinedTableDict,
            IEnumerable<IOpData> joiningDataCollection,
            Func<IOpData, GenericKey> joiningKeySelector)
        {
            Log(EventID.ViewsManager.InitializeViewMergeTableDictionaries, sourceTableDict.Count, joinedTableDict.Count, joiningDataCollection == null ? "<null>" : joiningDataCollection.Count().ToString(), joiningKeySelector == null ? "<null>" : "<not null>");

            if (sourceTableDict != null && sourceTableDict.Count > 0 && joinedTableDict != null && joinedTableDict.Count > 0 && joiningDataCollection != null && joiningKeySelector != null)
            {
                Dictionary<string, Enums.Tables> tableNameEnumDict = Enum.GetValues(typeof(Enums.Tables))
                                                                         .Cast<Enums.Tables>()
                                                                         .ToDictionary(k => k.ToString().ToUpper(), v => v);

                GenericKey[] sourceTableKeys = sourceTableDict.Keys.Select(x => x.Item2).ToArray();
                GenericKey[] joinedTableKeys = joinedTableDict.Keys.Select(x => x.Item2).ToArray();

                Enums.Tables sourceTable = tableNameEnumDict[sourceTableDict.First().Key.Item1];
                Enums.Tables joinedTable = tableNameEnumDict[joinedTableDict.First().Key.Item1];

                List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>> joinedItemsList = new List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>>();
                Dictionary<GenericKey, List<IOpData>> joiningDict = joiningDataCollection.GroupBy(joiningKeySelector).ToDictionary(k => k.Key, v => v.ToList());

                foreach (GenericKey sourceTableKey in sourceTableKeys)
                {
                    if (joiningDict.ContainsKey(sourceTableKey))
                    {
                        IOpData dataWithKey = joiningDict[sourceTableKey].FirstOrDefault();
                        if (dataWithKey != null && dataWithKey.Value != null)
                        {
                            joinedItemsList.Add(Tuple.Create(Tuple.Create(sourceTable, sourceTableKey), Tuple.Create(joinedTable, new GenericKey(dataWithKey.Value))));
                        }
                    }
                }

                Log(EventID.ViewsManager.InitializeViewMergeTableJoinedItems, joinedItemsList.Count);

                foreach (Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>> joinedItems in joinedItemsList)
                {
                    List<OpViewOperation.OpViewOperationCellValue> sourceValuesList = sourceTableDict.TryGetValue(Tuple.Create(joinedItems.Item1.Item1.ToString(), joinedItems.Item1.Item2));
                    List<OpViewOperation.OpViewOperationCellValue> joinedValuesList = joinedTableDict.TryGetValue(Tuple.Create(joinedItems.Item2.Item1.ToString(), joinedItems.Item2.Item2));
                    
                    // Dictionary<Tuple<TableName, item key>, List<Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>>
                    
                    if (sourceValuesList != null && joinedValuesList != null)
                    {
                        sourceValuesList.AddRange(joinedValuesList);
                    }
                }
            }

            Log(EventID.ViewsManager.InitializeViewMergeTableDictionariesResult, sourceTableDict.Count);

            return sourceTableDict;
        }
        
        #endregion

        #region MergeTableDictionaries

        private Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> MergeTableDictionaries(
            OpImrServer isItem,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> sourceTableDict,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> joinedTableDict,
            List<OpViewColumn> columnDefinitions,
            OpViewColumn columnToProceed)
        {
            Log(EventID.ViewsManager.InitializeViewMergeTableDictionaries, sourceTableDict.Count, joinedTableDict.Count);

            if (sourceTableDict != null && sourceTableDict.Count > 0 && joinedTableDict != null && joinedTableDict.Count > 0)
            {
                Dictionary<string, OpViewColumn> columnDefinitionsByNamesDict = columnDefinitions.ToDictionary(k => k.Name);

                Dictionary<string, Enums.Tables> tableNameEnumDict = Enum.GetValues(typeof(Enums.Tables))
                                                                         .Cast<Enums.Tables>()
                                                                         .ToDictionary(k => k.ToString().ToUpper(), v => v);

                GenericKey[] sourceTableKeys = sourceTableDict.Keys.Select(x => x.Item2).ToArray();
                GenericKey[] joinedTableKeys = joinedTableDict.Keys.Select(x => x.Item2).ToArray();

                Enums.Tables sourceTable = tableNameEnumDict[sourceTableDict.First().Key.Item1];
                Enums.Tables joinedTable = tableNameEnumDict[joinedTableDict.First().Key.Item1];

                List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>> joinedItemsList = JoinTableValues(isItem, sourceTable, joinedTable, sourceTableKeys, joinedTableKeys, columnToProceed);

                Log(EventID.ViewsManager.InitializeViewMergeTableJoinedItems, joinedItemsList.Count);

                foreach (Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>> joinedItems in joinedItemsList)
                {
                    List<OpViewOperation.OpViewOperationCellValue> sourceValuesList = sourceTableDict.TryGetValue(Tuple.Create(joinedItems.Item1.Item1.ToString(), joinedItems.Item1.Item2));
                    List<OpViewOperation.OpViewOperationCellValue> joinedValuesList = joinedTableDict.TryGetValue(Tuple.Create(joinedItems.Item2.Item1.ToString(), joinedItems.Item2.Item2));

                    if (sourceValuesList != null && joinedValuesList != null)
                    {
                        foreach (OpViewOperation.OpViewOperationCellValue value in joinedValuesList)
                        {
                            // Jeśli taka kolumna nie została jeszcze dodana, to ją możemy dodać.
                            // Taki if jest na przypadki, gdy np. dana lokalizacja ma przypisane dwa urządzenia.
                            if (!sourceValuesList.Any(x => String.Equals(x.ColumnName, value.ColumnName)))
                            {
                                // Sprawdzamy czy istnieją takie same kolumny na tym samym poziomie ale o innych nazwach
                                List<OpViewOperation.OpViewOperationCellValue> siblingColumnsList = FindAllSiblingColumns(columnDefinitionsByNamesDict, sourceValuesList, value);

                                if (siblingColumnsList == null || siblingColumnsList.Count == 0)
                                {
                                    // Jeśli nie istnieją takie same kolumny z innymi indeksami to dodajemy na spokojnie.
                                    sourceValuesList.Add(value);
                                }
                                else
                                {
                                    // Jeśli istnieją takie kolumny, to sprawdzamy czy jakaś ma już taką samą wartość.
                                    if (!siblingColumnsList.Any(x => x.Value.Equals(value.Value)))
                                    {
                                        // Jeśli nie ma to dodajemy.
                                        sourceValuesList.Add(value);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }

            Log(EventID.ViewsManager.InitializeViewMergeTableDictionariesResult, sourceTableDict.Count);

            return sourceTableDict;
        }
        
        #endregion

        #endregion

        #region JoinTableValues

        private List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>> JoinTableValues(OpImrServer isItem,
            Enums.Tables sourceTable,
            Enums.Tables joinedTable,
            GenericKey[] sourceTableKeys,
            GenericKey[] joinedTableKeys,
            OpViewColumn columnToProceed)
        {
            Log(EventID.ViewsManager.InitializeViewJoinTableValues, sourceTable, joinedTable, sourceTableKeys == null ? "<null>" : sourceTableKeys.Length.ToString(), joinedTableKeys == null ? "<null>" : joinedTableKeys.Length.ToString());

            List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>> joinedItems = new List<Tuple<Tuple<Enums.Tables, GenericKey>, Tuple<Enums.Tables, GenericKey>>>();

            Enums.Tables associativeTable = Enums.Tables.UNKNOWN;
            bool customAssociation = false; // To być może będzie potrzebne po to by stwierdzić czy używać odpowiedniego datatypu i indeksu VIEW_COLUMN_ASSOCIATIVE_TABLE_ID_DATA_TYPE VIEW_COLUMN_ASSOCIATIVE_TABLE_INDEX

            if (columnToProceed.DataList.FirstOrDefault(x => x.IdDataType == DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE) != null)
            {
                customAssociation = true;
                string tableName = columnToProceed.DataList.TryGetValue<string>(DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE);
                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    tableName = tableName.ToUpper().Trim();
                    associativeTable = TableNameToEnum(tableName);
                }
            }
            else if ((sourceTable == Enums.Tables.LOCATION && (joinedTable == Enums.Tables.METER || joinedTable == Enums.Tables.DEVICE))
                || (sourceTable == Enums.Tables.METER && (joinedTable == Enums.Tables.LOCATION || joinedTable == Enums.Tables.DEVICE))
                || (sourceTable == Enums.Tables.DEVICE && (joinedTable == Enums.Tables.METER || joinedTable == Enums.Tables.LOCATION)))
            {
                associativeTable = Enums.Tables.LOCATION_EQUIPMENT;
            }
            else if (sourceTable == Enums.Tables.ISSUE && joinedTable == Enums.Tables.LOCATION)
            {
                associativeTable = Enums.Tables.ISSUE;
            }
            else if (sourceTable == Enums.Tables.TASK && joinedTable == Enums.Tables.LOCATION)
            {
                associativeTable = Enums.Tables.TASK;
            }
            else if (sourceTable == Enums.Tables.DEVICE_CONNECTION && joinedTable == Enums.Tables.DEVICE)
            {
                associativeTable = Enums.Tables.DEVICE_CONNECTION;
            }
            else if ((sourceTable == Enums.Tables.ALARM_EVENT && joinedTable == Enums.Tables.DEVICE)
                || (sourceTable == Enums.Tables.ALARM_EVENT && joinedTable == Enums.Tables.METER)
                || (sourceTable == Enums.Tables.ALARM_EVENT && joinedTable == Enums.Tables.LOCATION)
                || (sourceTable == Enums.Tables.ALARM_EVENT && joinedTable == Enums.Tables.ALARM_MESSAGE))
            {
                associativeTable = Enums.Tables.ALARM_EVENT;
            }
            else if ((sourceTable == Enums.Tables.OPERATOR && joinedTable == Enums.Tables.ACTOR))
            {
                associativeTable = Enums.Tables.OPERATOR;
            }
            else if (sourceTable == Enums.Tables.SIM_CARD && joinedTable == Enums.Tables.DEVICE)
            {
                associativeTable = Enums.Tables.DATA;
            }
            else if (sourceTable == Enums.Tables.SIM_CARD && joinedTable == Enums.Tables.SIM_CARD)
            {
                associativeTable = Enums.Tables.SIM_CARD;
            }

            Log(EventID.ViewsManager.InitializeViewJoinTablesAssociativeTable, associativeTable);

            switch (associativeTable)
            {
                case Enums.Tables.LOCATION_EQUIPMENT:
                {
                    #region LOCATION_EQUIPMENT

                    List<OpLocationEquipment> leList = null;
                    Dictionary<GenericKey, List<OpLocationEquipment>> leDict = null;

                    int tries = 5;
                    bool querySucceded = false;
                    do
                    {
                        try
                        {
                            switch (sourceTable)
                            {
                                case Enums.Tables.LOCATION:
                                    leList = DataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                                                        IdLocation: sourceTableKeys.Select(x => x.AsLong()).ToArray(), customWhereClause: "[END_TIME] is null", commandTimeout: 600);
                                    leDict = leList.GroupBy(k => k.IdLocation.Value).ToDictionary(k => new GenericKey(k.Key), v => v.ToList());
                                    break;
                                case Enums.Tables.METER:
                                    leList = DataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                                                        IdMeter: sourceTableKeys.Select(x => x.AsLong()).ToArray(), customWhereClause: "[END_TIME] is null", commandTimeout: 600);
                                    leDict = leList.GroupBy(k => k.IdMeter.Value).ToDictionary(k => new GenericKey(k.Key), v => v.ToList());
                                    break;
                                case Enums.Tables.DEVICE:
                                    leList = DataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                                                        SerialNbr: sourceTableKeys.Select(x => x.AsLong()).ToArray(), customWhereClause: "[END_TIME] is null", commandTimeout: 600);
                                    leDict = leList.GroupBy(k => k.SerialNbr.Value).ToDictionary(k => new GenericKey(k.Key), v => v.ToList());
                                    break;
                            }
                            querySucceded = true;
                        }
                        catch (Exception iex)
                        {
                            Log(EventID.ViewsManager.ExceptionOccured, iex.ToString());
                        }

                        tries--;
                    }
                    while (tries > 0 && !querySucceded);

                    if (tries == 0)
                    {
                        throw new Exception("Failed to get LOCATION_EQUIPMENT");
                    }

                    foreach (GenericKey sourceKey in sourceTableKeys)
                    {
                        List<OpLocationEquipment> associatedLocEq = new List<OpLocationEquipment>();
                        if (leDict.ContainsKey(sourceKey))
                        {
                            associatedLocEq = leDict[sourceKey].OrderByDescending(x => x.StartTime).ToList();
                        }

                        // Szukamy pierwszego dopasowania które jest najnowsze oraz należy do dołączanych kluczy które analizujemy
                        foreach (OpLocationEquipment leItem in associatedLocEq)
                        {
                            long? joinedKey = null;
                            switch (joinedTable)
                            {
                                case Enums.Tables.LOCATION:
                                    joinedKey = leItem.IdLocation;
                                    break;
                                case Enums.Tables.METER:
                                    joinedKey = leItem.IdMeter;
                                    break;
                                case Enums.Tables.DEVICE:
                                    joinedKey = leItem.SerialNbr;
                                    break;
                            }

                            if (joinedKey.HasValue && new GenericKey(joinedKey.Value).In(joinedTableKeys))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(joinedKey.Value))));
                                //break; // Bierzemy wszystkie bo potem i tak to sprawdzamy podczas łaczenia i szukania kolumn o tym samym iddatatype
                            }
                        }
                    }

                    break;

                    #endregion
                }
                case Enums.Tables.TASK:
                {
                    #region TASK

                    if (joinedTable == Enums.Tables.LOCATION)
                    {
                        //Dictionary<GenericKey, OpTask> taskDict = DataProvider.GetTask(sourceTableKeys.Select(x => Convert.ToInt32(x)).ToArray()).ToDictionary(k => new GenericKey(k.IdTask));
                        Dictionary<GenericKey, OpTask> taskDict = TryGetDataFromCache<OpTask>(isItem, (int)Enums.ReferenceType.IdTask, null, o => (o as OpTask).IdTask).ToDictionary(k => new GenericKey(k.IdTask));
                        if (taskDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, GenericKey> locationKeys = joinedTableKeys.ToDictionary(k => k, v => v);
                        foreach (GenericKey sourceKey in sourceTableKeys)
                        {
                            OpTask task = taskDict.TryGetValue(sourceKey);

                            if (task != null && task.IdLocation.HasValue && locationKeys.ContainsKey(new GenericKey(task.IdLocation)))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(task.IdLocation.Value))));
                            }
                        }
                    }

                    break;
                    
                    #endregion
                }
                case Enums.Tables.ISSUE:
                {
                    #region ISSUE

                    if (joinedTable == Enums.Tables.LOCATION)
                    {
                        //Dictionary<GenericKey, OpIssue> issueDict = DataProvider.GetIssue(sourceTableKeys.Select(x => Convert.ToInt32(x)).ToArray()).ToDictionary(k => new GenericKey(k.IdIssue));
                        Dictionary<GenericKey, OpIssue> issueDict = TryGetDataFromCache<OpIssue>(isItem, (int)Enums.ReferenceType.IdIssue, null, o => (o as OpIssue).IdIssue).ToDictionary(k => new GenericKey(k.IdIssue));
                        if (issueDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, GenericKey> locationKeys = joinedTableKeys.ToDictionary(k => new GenericKey(k), v => new GenericKey(v));
                        foreach (GenericKey sourceKey in sourceTableKeys)
                        {
                            OpIssue issue = issueDict.TryGetValue(new GenericKey(sourceKey));

                            if (issue != null && issue.IdLocation.HasValue && locationKeys.ContainsKey(new GenericKey(issue.IdLocation.Value)))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(issue.IdLocation.Value))));
                            }
                        }
                    }

                    break;
                    
                    #endregion
                }
                case Enums.Tables.DEVICE_CONNECTION:
                {
                    #region DEVICE_CONNECTION

                    if (joinedTable == Enums.Tables.DEVICE)
                    {
                        //Dictionary<GenericKey, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection> deviceConnectionDict = DataProvider.GetDeviceConnection(sourceTableKeys.Select(x => x.AsLong()).ToArray()).ToDictionary(k => new GenericKey(k.IdDeviceConnection));
                        Dictionary<GenericKey, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection> deviceConnectionDict = TryGetDataFromCache<IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection>(isItem, (int)Enums.ReferenceType.IdDeviceConnection, null, o => (o as IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection).IdDeviceConnection).ToDictionary(k => new GenericKey(k.IdDeviceConnection));
                        if (deviceConnectionDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, GenericKey> deviceKeys = joinedTableKeys.ToDictionary(k => new GenericKey(k), v => new GenericKey(v));
                        foreach (GenericKey sourceKey in sourceTableKeys)
                        {
                            IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection deviceConnection = deviceConnectionDict.TryGetValue(sourceKey);

                            if (columnToProceed.IdDataType == DataType.HELPER_SERIAL_NBR)
                            {
                                if (deviceConnection != null && deviceKeys.ContainsKey(new GenericKey(deviceConnection.SerialNbr)))
                                {
                                    joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(deviceConnection.SerialNbr))));
                                }
                            }
                            else
                            {
                                if (deviceConnection != null && deviceKeys.ContainsKey(new GenericKey(deviceConnection.SerialNbrParent)))
                                {
                                    joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(deviceConnection.SerialNbrParent))));
                                }
                            }
                        }
                    }

                    break;
	                #endregion
                }
                case Enums.Tables.ALARM_EVENT:
                {
                    #region ALARM_EVENT

                    //Dictionary<GenericKey, IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent> alarmEventDict = DataProvider.GetAlarmEventDW(sourceTableKeys.Select(x => x.AsLong()).ToArray()).ToDictionary(k => new GenericKey(k.IdAlarmEvent));
                    Dictionary<GenericKey, IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent> alarmEventDict = TryGetDataFromCache<IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent>(isItem, (int)Enums.ReferenceType.IdAlarmEvent, null, o => (o as IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent).IdAlarmEvent).ToDictionary(k => new GenericKey(k.IdAlarmEvent));
                    if (alarmEventDict.Count != sourceTableKeys.Length)
                    {
                        throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                    }

                    Dictionary<GenericKey, GenericKey> dataKeys = joinedTableKeys.ToDictionary(k => k, v => v);

                    System.Func<IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent, long?> keyPicker = null;

                    if (joinedTable == Enums.Tables.DEVICE)
                    {
                        keyPicker = o => o.SerialNbr;
                    }
                    else if (joinedTable == Enums.Tables.METER)
                    {
                        keyPicker = o => o.IdMeter;
                    }
                    else if (joinedTable == Enums.Tables.LOCATION)
                    {
                        keyPicker = o => o.IdLocation;
                    }
                    else if (joinedTable == Enums.Tables.ALARM_MESSAGE)
                    {
                        keyPicker = o => o.IdAlarmEvent;
                    }
                    else
                    {
                        break;
                    }

                    foreach (GenericKey sourceKey in sourceTableKeys)
                    {
                        IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent alarmEvent = alarmEventDict.TryGetValue(sourceKey);
                        long? joinedKey = keyPicker(alarmEvent);

                        if (joinedKey.HasValue)
                        {
                            joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(joinedKey.Value))));
                        }
                    }

                    break;
                    #endregion
                }
                case Enums.Tables.OPERATOR:
                {
                    #region OPERATOR
		            
                    if (joinedTable == Enums.Tables.ACTOR)
                    {
                        //Dictionary<GenericKey, OpOperator> operatorDict = DataProvider.GetOperator(sourceTableKeys.Select(x => Convert.ToInt32(x)).ToArray()).ToDictionary(k => new GenericKey(k.IdOperator));
                        Dictionary<GenericKey, OpOperator> operatorDict = TryGetDataFromCache<OpOperator>(isItem, (int)Enums.ReferenceType.IdOperator, null, o => (o as OpOperator).IdOperator).ToDictionary(k => new GenericKey(k.IdOperator));
                        if (operatorDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, GenericKey> actorKeys = joinedTableKeys.ToDictionary(k => new GenericKey(k), v => new GenericKey(v));
                        foreach (GenericKey sourceKey in sourceTableKeys)
                        {
                            OpOperator oper = operatorDict.TryGetValue(sourceKey);

                            if (oper != null && actorKeys.ContainsKey(new GenericKey(oper.IdActor)))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(oper.IdActor))));
                            }
                        }
                    }

                    break;
	                #endregion
                }
                case Enums.Tables.DATA:
                {
                    #region DATA

                    if (sourceTable == Enums.Tables.SIM_CARD && joinedTable == Enums.Tables.DEVICE)
                    {
                        Dictionary<GenericKey, OpData> phoneNumbers = DataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                                                                                             SerialNbr: joinedTableKeys.Select(x => x.AsLong()).ToArray(), IdDataType: new long[] { DataType.DEVICE_PHONE_NUMBER })
                                                                              .Where(x => x.SerialNbr.HasValue && x.Value != null && !string.IsNullOrWhiteSpace(x.Value.ToString()))
                                                                              .GroupBy(k => k.Value.ToString().Trim())
                                                                              .ToDictionary(k => new GenericKey(k.Key), v => v.First());

                        //Dictionary<GenericKey, OpSimCard> simCardDict = DataProvider.GetSimCard(sourceTableKeys.Select(x => x.AsInt()).ToArray()).ToDictionary(k => new GenericKey(k.IdSimCard));
                        Dictionary<GenericKey, OpSimCard> simCardDict = TryGetDataFromCache<OpSimCard>(isItem, (int)Enums.ReferenceType.IdSimCard, null, o => (o as OpSimCard).IdSimCard).ToDictionary(k => new GenericKey(k.IdSimCard));
                        if (simCardDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, bool> simCardIsActiveDict = DataProvider.GetSimCardDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                                               IdSimCard: sourceTableKeys.Select(x => Convert.ToInt32(x)).ToArray(), IdDataType: new long[] { DataType.SIM_CARD_IS_ACTIVE }
                                                                                                               ,commandTimeout:600)
                                                                                .GroupBy(k => k.IdSimCard)
                                                                                .ToDictionary(k => new GenericKey(k.Key), v => v.First().Value == null ? true : (bool)v.First().Value);

                        IEnumerable<GenericKey> missingIsActiveKeys = simCardDict.Keys.Except(simCardIsActiveDict.Keys);
                        foreach (GenericKey idSimCard in missingIsActiveKeys)
                        {
                            simCardIsActiveDict.Add(idSimCard, false);
                        }

                        foreach (KeyValuePair<GenericKey, OpSimCard> kvIdSimCard in simCardDict)
                        {
                            if (simCardIsActiveDict[kvIdSimCard.Key] && phoneNumbers.ContainsKey(new GenericKey(kvIdSimCard.Value.Phone)))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, kvIdSimCard.Key), Tuple.Create(joinedTable, new GenericKey(phoneNumbers[new GenericKey(kvIdSimCard.Value.Phone)].SerialNbr.Value))));
                            }
                        }
                    }

                    break;

                    #endregion
                }
                case Enums.Tables.SIM_CARD:
                {
                    #region SIM_CARD

                    if (columnToProceed.IdDataType == DataType.HELPER_PHONE)
                    {
                        List<OpSimCard> simCards = GetDataFromCache(isItem, (int)Enums.ReferenceType.IdSimCard,
                            keys => this.DataProvider.GetSimCardFilter(IdSimCard: keys == null ? null : keys.Select(x => Convert.ToInt32(x)).ToArray(), loadNavigationProperties: false, mergeIntoCache: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false, commandTimeout: 180),
                            SimCardCache,
                            o => (o as OpSimCard).IdSimCard);

                        foreach (OpSimCard simCard in simCards)
                        {
                            joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, new GenericKey(simCard.IdSimCard)), Tuple.Create(joinedTable, new GenericKey(simCard.IdSimCard))));
                        }
                    }

                    break;

                    #endregion
                }
                case Enums.Tables.ACTOR:
                {
                    #region ACTOR

                    if (sourceTable == Enums.Tables.OPERATOR && joinedTable == Enums.Tables.ACTOR)
                    {
                        //Dictionary<GenericKey, OpOperator> operatorDict = DataProvider.GetOperator(sourceTableKeys.Select(x => Convert.ToInt32(x)).ToArray()).ToDictionary(k => new GenericKey(k.IdOperator));
                        Dictionary<GenericKey, OpOperator> operatorDict = TryGetDataFromCache<OpOperator>(isItem, (int)Enums.ReferenceType.IdOperator, null, o => (o as OpOperator).IdOperator).ToDictionary(k => new GenericKey(k.IdOperator));
                        if (operatorDict.Count != sourceTableKeys.Length)
                        {
                            throw new InvalidOperationException(string.Format("Cache contains less objects than requested by keys: {0}", associativeTable));
                        }

                        Dictionary<GenericKey, GenericKey> actorKeys = joinedTableKeys.ToDictionary(k => new GenericKey(k), v => new GenericKey(v));
                        foreach (GenericKey sourceKey in sourceTableKeys)
                        {
                            OpOperator oper = operatorDict.TryGetValue(sourceKey);

                            if (oper != null && actorKeys.ContainsKey(new GenericKey(oper.IdActor)))
                            {
                                joinedItems.Add(Tuple.Create(Tuple.Create(sourceTable, sourceKey), Tuple.Create(joinedTable, new GenericKey(oper.IdActor))));
                            }
                        }
                    }

                    break;

	                #endregion
                }
                case Enums.Tables.UNKNOWN:
                {
                    #region UNKNOWN



                    break;
                    #endregion
                }
            }

            Log(EventID.ViewsManager.InitializeViewJoinTablesResult, joinedItems.Count);

            return joinedItems;
        }
        
        #endregion

        #region //ProceedColumnData - old

        //private void ProceedColumnData<TDataProvider, TObjectType, TOpData>(
        //        OpView view,
        //        OpImrServer server,
        //        OpViewColumn columnToProceed,
        //        OpViewColumn parentViewColumn,
        //        int idReferneceType,
        //        ref GenericKey lastPkValue,
        //        Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result,
        //        System.Action<TDataProvider, TObjectType, List<long>> componentDelegate,
        //        System.Func<object, GenericKey> keyPicker,
        //        Func<long[], List<TObjectType>> dataSource)
        //    where TDataProvider : IDataProvider
        //    where TObjectType : IOpObject<TOpData>
        //    where TOpData : IOpData, IOpDataProvider, new()
        //{
        //    if (!view.IsSynchronizable)
        //    {
        //        Log(EventID.ViewsManager.ProceedColumnDataViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
        //        return;
        //    }

        //    if (columnToProceed.Key && parentViewColumn.IdViewColumn != columnToProceed.IdViewColumn && view.ImrServerColumn != columnToProceed)
        //    {
        //        Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsKeyButAnotherTableIsAnalzyed, columnToProceed.Name, columnToProceed.Table, parentViewColumn.Table);
        //        return;
        //    }

        //    if (columnToProceed.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
        //    {
        //        Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsExtendedHelper, columnToProceed.Name, columnToProceed.Table, parentViewColumn.Table);
        //        return;
        //    }

        //    List<TObjectType> dataList = null;

        //    int tryCount = 5;
        //    bool gotDataSuccess = false;
        //    do
        //    {
        //        try
        //        {
        //            dataList = TryGetDataFromCache<TObjectType>(server, idReferneceType, keys => dataSource(keys).Cast<object>());
        //            gotDataSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            Log(EventID.ViewsManager.InitializeViewProceedColumnDataGetDataException, columnToProceed.Name, columnToProceed.Table, ex.ToString());
        //        }
        //    }
        //    while (--tryCount > 0 && !gotDataSuccess);

        //    Log(EventID.ViewsManager.InitializeViewProceedColumnDataDataListCount, dataList.Count);

        //    if (!gotDataSuccess)
        //    {
        //        throw new InvalidOperationException(string.Format("Failed to get data for column: {0} table: {1}", columnToProceed.Name, columnToProceed.Table));
        //    }

        //    // Jeśli kolumna była kolumną z id serwera to ją uzupełniamy i wychodzimy.
        //    if (EnsureImrServerDatatype(server.IdImrServer, columnToProceed, dataList.Cast<object>(), keyPicker, result))
        //    {
        //        Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsImrServer, columnToProceed.Name, columnToProceed.Table);
        //        return;
        //    }

        //    long idDataTypeToProceed = columnToProceed.IdDataType;
        //    OpDataType dataTypeToProceed = columnToProceed.DataType;
        //    Enums.Tables tableEnum = TableNameToEnum(columnToProceed.Table); // Parent ponieważ current może mieć inne table niż ta z której wyszliśmy
        //    if (HelperDataTypeMappings.ContainsKey(tableEnum) && HelperDataTypeMappings[tableEnum].ContainsKey(idDataTypeToProceed))
        //    {
        //        idDataTypeToProceed = HelperDataTypeMappings[tableEnum][idDataTypeToProceed];
        //        dataTypeToProceed = DataProvider.GetDataType(idDataTypeToProceed);
        //    }

        //    SetHelperValues<TObjectType, TDataProvider>(componentDelegate, dataList.Cast<TObjectType>().ToList(), new List<long>() { idDataTypeToProceed });

        //    foreach (object obj in dataList)
        //    {
        //        //Tuple<string, GenericKey> key = Tuple.Create(columnToProceed.Table, keyPicker(obj));
        //        Tuple<string, GenericKey> key = Tuple.Create(GetAssociativeTableName(columnToProceed), keyPicker(obj));

        //        // Szukamy nowej maksymalnej wartosci dla PK.
        //        if (columnToProceed.PrimaryKey && key.Item2 > lastPkValue)
        //        {
        //            lastPkValue = key.Item2;
        //        }

        //        EnsureKeyExists(result, key);

        //        // [2017-02-14]
        //        // Założyliśmy, że wszystkie czasy jakie dostajemy są w strefie lokalnej, więc zawsze
        //        // wszystkie wartości czasowe konwertujemy do UTC przed zapisem.
        //        object value = ((TObjectType)obj).DataList.First(x => x.IdDataType == idDataTypeToProceed).Value;
        //        if (dataTypeToProceed.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime && value is DateTime)
        //        {
        //            value = DataProvider.ConvertTimeZoneToUtcTime((DateTime)value);
        //        }

        //        result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, value, dataTypeToProceed, 0));
        //    }
        //}
        
        #endregion

        #region ProceedColumnData

        private void ProceedColumnData<TDataProvider, TObjectType, TObjectData, TCacheKey>(OpView view,
            OpImrServer isItem,
            OpViewColumn columnToProceed,
            OpViewColumn parentViewColumn,
            ref GenericKey initializationPkValue,
            Enums.ReferenceType referenceType,
            IEnumerable<OpViewColumn> baseHelperColumns,
            Dictionary<int, Dictionary<TCacheKey, TObjectType>> cache,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result,
            System.Action<TDataProvider, TObjectType, List<long>> componentDelegate,
            System.Func<TObjectType, GenericKey> keyPicker,
            int initializationBatchSize,
            Dictionary<int, string> columnCustomWhereClausesDict,
            Func<TCacheKey[], long?, string, List<TObjectType>> dataSource)
            where TDataProvider : IDataProvider
            where TObjectType : IOpObject<TObjectData>
            where TObjectData : IOpData, IOpDataProvider, new()
        {
            GenericKey lastPkValue = new GenericKey(initializationPkValue.KeyValue);

            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedColumnDataViewNotSynchronizable, view.IdView, view.Name, isItem.IdImrServer);
                return;
            }

            if (columnToProceed.Key && parentViewColumn.IdViewColumn != columnToProceed.IdViewColumn && view.ImrServerColumn != columnToProceed)
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsKeyButAnotherTableIsAnalzyed, columnToProceed.Name, columnToProceed.Table, parentViewColumn.Table);
                return;
            }

            if (columnToProceed.IdViewColumnType == (int)Enums.ViewColumnType.ExtendedHelper)
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsExtendedHelper, columnToProceed.Name, columnToProceed.Table, parentViewColumn.Table);
                return;
            }

            string customWhereClause = CreateCustomWhereClause(isItem, columnToProceed, columnToProceed.DataType.Name.Replace("HELPER_", string.Empty), lastPkValue, columnCustomWhereClausesDict);
            long? topCount = GetTopCountValue(isItem, columnToProceed, initializationBatchSize);

            List<TObjectType> dataList = null;

            int tryCount = 5;
            bool gotDataSuccess = false;
            do
            {
                try
                {
                    dataList = TryGetDataFromCache<TObjectType, TCacheKey>(isItem, referenceType, dataSource, customWhereClause, topCount);
                    gotDataSuccess = true;
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.InitializeViewProceedColumnDataGetDataException, columnToProceed.Name, columnToProceed.Table, ex.ToString());
                }
            }
            while (--tryCount > 0 && !gotDataSuccess);

            Log(EventID.ViewsManager.InitializeViewProceedColumnDataDataListCount, dataList.Count);

            if (!gotDataSuccess)
            {
                throw new InvalidOperationException(string.Format("Failed to get data for column: {0} table: {1}", columnToProceed.Name, columnToProceed.Table));
            }

            // Jeśli kolumna była kolumną z id serwera to ją uzupełniamy i wychodzimy.
            if (EnsureImrServerDatatype(isItem.IdImrServer, columnToProceed, dataList.Cast<object>(), keyPicker, result))
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsImrServer, columnToProceed.Name, columnToProceed.Table);
                return;
            }

            long idDataTypeToProceed = columnToProceed.IdDataType;
            OpDataType dataTypeToProceed = columnToProceed.DataType;
            Enums.Tables tableEnum = TableNameToEnum(columnToProceed.Table); // Parent ponieważ current może mieć inne table niż ta z której wyszliśmy
            if (HelperDataTypeMappings.ContainsKey(tableEnum) && HelperDataTypeMappings[tableEnum].ContainsKey(idDataTypeToProceed))
            {
                idDataTypeToProceed = HelperDataTypeMappings[tableEnum][idDataTypeToProceed];
                dataTypeToProceed = DataProvider.GetDataType(idDataTypeToProceed);
            }

            SetHelperValues<TObjectType, TDataProvider>(componentDelegate, dataList.Cast<TObjectType>().ToList(), new List<long>() { idDataTypeToProceed });

            foreach (TObjectType obj in dataList)
            {
                //Tuple<string, GenericKey> key = Tuple.Create(columnToProceed.Table, keyPicker(obj));
                Tuple<string, GenericKey> key = Tuple.Create(GetAssociativeTableName(columnToProceed), keyPicker(obj));

                // Szukamy nowej maksymalnej wartosci dla PK.
                if (columnToProceed.PrimaryKey && key.Item2 > lastPkValue)
                {
                    initializationPkValue = key.Item2;
                }

                EnsureKeyExists(result, key);

                // [2017-02-14]
                // Założyliśmy, że wszystkie czasy jakie dostajemy są w strefie lokalnej, więc zawsze
                // wszystkie wartości czasowe konwertujemy do UTC przed zapisem.
                object value = ((TObjectType)obj).DataList.First(x => x.IdDataType == idDataTypeToProceed).Value;
                if (dataTypeToProceed.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime && value is DateTime)
                {
                    value = DataProvider.ConvertTimeZoneToUtcTime((DateTime)value);
                }

                result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, value, dataTypeToProceed, 0));
            }

            InitializeBaseHelpers<TDataProvider, TObjectType, TObjectData, TCacheKey>(isItem, columnToProceed, cache, baseHelperColumns, componentDelegate, keyPicker, result);
        }

        #endregion

        #region ProceedColumnData

        private void ProceedColumnData<TDataProvider, TObjectType>(
                OpView view,
                OpImrServer server,
                OpViewColumn columnToProceed,
                OpViewColumn parentViewColumn,
                int idReferneceType,
                ref GenericKey lastPkValue,
                Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result,
                System.Func<object, GenericKey> keyPicker,
                Func<long[], List<TObjectType>> dataSource)
            where TDataProvider : IDataProvider
        {
            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedColumnDataViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            if (columnToProceed.Key && parentViewColumn.IdViewColumn != columnToProceed.IdViewColumn && view.ImrServerColumn != columnToProceed)
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsKeyButAnotherTableIsAnalzyed, columnToProceed.Name, columnToProceed.Table, parentViewColumn.Table);
                return;
            }

            List<TObjectType> dataList = null;

            int tryCount = 5;
            bool gotDataSuccess = false;
            do
            {
                try
                {
                    dataList = TryGetDataFromCache<TObjectType>(server, idReferneceType, keys => dataSource(keys).Cast<object>());
                    gotDataSuccess = true;
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.InitializeViewProceedColumnDataGetDataException, columnToProceed.Name, columnToProceed.Table, ex.ToString());
                }
            }
            while (--tryCount > 0 && !gotDataSuccess);

            if (!gotDataSuccess)
            {
                throw new InvalidOperationException(string.Format("Failed to get data for column: {0} table: {1}", columnToProceed.Name, columnToProceed.Table));
            }

            // Jeśli kolumna była kolumną z id serwera to ją uzupełniamy i wychodzimy.
            if (EnsureImrServerDatatype(server.IdImrServer, columnToProceed, dataList.Cast<object>(), keyPicker, result))
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataColumnIsImrServer, columnToProceed.Name, columnToProceed.Table);
                return;
            }

            long idDataTypeToProceed = columnToProceed.IdDataType;
            OpDataType dataTypeToProceed = columnToProceed.DataType;
            Enums.Tables tableEnum = TableNameToEnum(columnToProceed.Table); // Parent ponieważ current może mieć inne table niż ta z której wyszliśmy
            if (HelperDataTypeMappings.ContainsKey(tableEnum) && HelperDataTypeMappings[tableEnum].ContainsKey(idDataTypeToProceed))
            {
                idDataTypeToProceed = HelperDataTypeMappings[tableEnum][idDataTypeToProceed];
                dataTypeToProceed = DataProvider.GetDataType(idDataTypeToProceed);
            }

            Log(EventID.ViewsManager.InitializeViewProceedColumnDataByReflection, columnToProceed.Name, columnToProceed.Table, view.Name);

            Type objectType = typeof(TObjectType);

            if (!ReflectedFields.ContainsKey(objectType))
            {
                Log(EventID.ViewsManager.InitializeViewProceedColumnDataByReflectionMissingReflectedType, objectType.FullName);

                ReflectedFields.Add(objectType, objectType.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                                                          .ToDictionary(k => dataTypeToProceed.IdDataType, v => Tuple.Create(string.Format("HELPER_{0}", v.Name), v)));
            }
            
            foreach (object obj in dataList)
            {
                GenericKey objectId = keyPicker(obj);
                Tuple<string, GenericKey> key = Tuple.Create(GetAssociativeTableName(columnToProceed), objectId);

                // Szukamy nowej maksymalnej wartosci dla PK.
                if (columnToProceed.PrimaryKey && key.Item2 > lastPkValue)
                {
                    lastPkValue = key.Item2;
                }

                EnsureKeyExists(result, key);

                if (!ReflectedFields[objectType].ContainsKey(dataTypeToProceed.IdDataType))
                {
                    System.Reflection.FieldInfo missingField = objectType.GetField(dataTypeToProceed.Name.Replace("HELPER_", string.Empty));

                    Log(EventID.ViewsManager.InitializeViewProceedColumnDataByReflectionMissingReflectedFieldForType, dataTypeToProceed.IdDataType, objectType.FullName, missingField == null ? "<null>" : missingField.Name);

                    if (missingField == null)
                    {
                        continue;
                    }

                    ReflectedFields[objectType].Add(dataTypeToProceed.IdDataType, Tuple.Create(dataTypeToProceed.Name, missingField));
                }

                object value = ReflectedFields[objectType][dataTypeToProceed.IdDataType].Item2.GetValue(obj);

                // [2017-02-14]
                // Założyliśmy, że wszystkie czasy jakie dostajemy są w strefie lokalnej, więc zawsze
                // wszystkie wartości czasowe konwertujemy do UTC przed zapisem.
                if (dataTypeToProceed.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime && value is DateTime)
                {
                    value = DataProvider.ConvertTimeZoneToUtcTime((DateTime)value);
                }

                // Ten log może nieźle trzaskać po pliku
                //Log(EventID.ViewsManager.InitializeViewProceedColumnDataByReflectionSettingFieldValueForItem, columnToProceed.Name, view.Name, objectType.FullName, columnToProceed.Table, dataTypeToProceed.IdDataType, objectId, value);

                result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, value, dataTypeToProceed, 0));
            }
        }

        #endregion

        #region ProceedDataColumnData

        private void ProceedDataColumnData<TOpData>(
                OpImrServer server,
                OpViewColumn columnToProceed,
                OpViewColumn parentViewColumn,
                int idParentReferenceType,
                Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result,
                out Func<IOpData, GenericKey> joiningKeySelector,
                out IEnumerable<IOpData> joiningData,
                System.Func<object, GenericKey> keyPicker,
                Func<GenericKey[], List<TOpData>> dataSource
            )
            where TOpData : IOpData, IOpDataProvider, new()
        {
            GenericKey[] dataKeys = null;
            long[] keysFromCache = GetKeysFromCache(server, idParentReferenceType);
            if (keysFromCache != null)
            {
                dataKeys = keysFromCache.Select(x => new GenericKey(x)).ToArray();
            }

            joiningKeySelector = null;
            joiningData = null;

            if (dataKeys == null || dataKeys.Length == 0)
            {
                return;
            }

            List<TOpData> dataList = null;

            int tryCount = 5;
            bool gotDataSuccess = false;
            do
            {
                try
                {
                    dataList = dataSource(dataKeys);
                    gotDataSuccess = true;
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.InitializeViewProceedColumnDataGetDataException, columnToProceed.Name, columnToProceed.Table, ex.ToString());
                }
            }
            while (--tryCount > 0 && !gotDataSuccess);

            if (!gotDataSuccess)
            {
                throw new InvalidOperationException(string.Format("Failed to get data for column: {0} table: {1}", columnToProceed.Name, columnToProceed.Table));
            }

            // Skoro data jest kluczem to znaczy, że zawiera tak naprawde ID który będzie kluczem
            if (columnToProceed.Key)
            {
                joiningKeySelector = keyPicker;
                joiningData = dataList.Cast<IOpData>();

                // Tutaj nie korzystalym z Table ani DataSourceTable ponieważ tabele źródłową determinuje reference type
                columnToProceed.Table = ReferenceTypeToTable(columnToProceed.DataType.IdReferenceType.Value);
                columnToProceed.DataType = ReferenceTypeToKeyDataType(columnToProceed.DataType.IdReferenceType.Value);

                Log(EventID.ViewsManager.InitializeViewProceedingDataColumnKeyTable, columnToProceed.Table, columnToProceed.DataType.IdDataType);
            }
            else
            {
                long idDataTypeToProceed = columnToProceed.IdDataType;
                OpDataType dataTypeToProceed = columnToProceed.DataType;
                Enums.Tables tableEnum = TableNameToEnum(columnToProceed.Table); // Parent ponieważ current może mieć inne table niż ta z której wyszliśmy
                if (HelperDataTypeMappings.ContainsKey(tableEnum) && HelperDataTypeMappings[tableEnum].ContainsKey(idDataTypeToProceed))
                {
                    idDataTypeToProceed = HelperDataTypeMappings[tableEnum][idDataTypeToProceed];
                    dataTypeToProceed = DataProvider.GetDataType(idDataTypeToProceed);
                }

                int columnIdx = columnToProceed.ColumnIndexNbr;
                foreach (TOpData data in dataList)
                {
                    try
                    {
                        GenericKey parentKey = keyPicker(data);

                        Tuple<string, GenericKey> key = Tuple.Create(GetAssociativeTableName(parentViewColumn), parentKey);

                        if (result.ContainsKey(key))
                        {
                            if (result[key].FirstOrDefault(x => x.ColumnName == columnToProceed.Name && x.Index == data.Index) == null
                                && columnIdx == data.Index)
                            {
                                // [2017-02-14]
                                // Założyliśmy, że wszystkie czasy jakie dostajemy są w strefie lokalnej, więc zawsze
                                // wszystkie wartości czasowe konwertujemy do UTC przed zapisem.
                                object value = data.Value;
                                if (dataTypeToProceed.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime && value is DateTime)
                                {
                                    value = DataProvider.ConvertTimeZoneToUtcTime((DateTime)value);
                                }

                                result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, value, dataTypeToProceed, data.Index));

                                if (columnToProceed.Key)
                                {
                                    AddKeyToPrimarykeyCache(server, columnToProceed.DataType.IdReferenceType.Value, value == null ? null : (long?)Convert.ToInt64(value));
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Log(EventID.ViewsManager.InitializeViewException, ex.ToString());
                    }
                }
            }
        }
        
        #endregion

        #region InitializeBaseHelpers

        private void InitializeBaseHelpers<TDataProvider, TObjectType, TOpData, TKey>(
                OpImrServer isItem,
                OpViewColumn analyzedParentColumn,
                Dictionary<int, Dictionary<TKey, TObjectType>>cache,
                IEnumerable<OpViewColumn> columnsToInitialzie,
                System.Action<TDataProvider, TObjectType, List<long>> componentDelegate,
                System.Func<TObjectType, GenericKey> keyProvider,
                Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> resultDict
            )
            where TDataProvider : IDataProvider
            where TObjectType : IOpObject<TOpData>
            where TOpData : IOpData, IOpDataProvider, new()
        {
            // Nie ustawiamy tutaj tych helperów
            //if (!analyzedParentColumn.PrimaryKey || analyzedParentColumn.IdDataType == DataType.HELPER_ID_IMR_SERVER)
            if (analyzedParentColumn.IdDataType == DataType.HELPER_ID_IMR_SERVER)
            {
                return;
            }

            // <Zmapowany, kolumna>
            List<Tuple<OpDataType, OpViewColumn>> dataTypeToInitializeList = columnsToInitialzie.Select(x => Tuple.Create(
                                                                                                HelperDataTypeMappings.ContainsKey(TableNameToEnum(x.Table)) && HelperDataTypeMappings[TableNameToEnum(x.Table)].ContainsKey(x.IdDataType)
                                                                                                    ? DataProvider.GetDataType(HelperDataTypeMappings[TableNameToEnum(x.Table)][x.IdDataType])
                                                                                                    : x.DataType,
                                                                                                    x))
                                                                                                .ToList();

            IEnumerable<object> itemsToInitialize = cache.ContainsKey(isItem.IdImrServer)
                                                  ? cache[isItem.IdImrServer].Values.Cast<object>()
                                                  : new object[0];

            SetHelperValues<TObjectType, TDataProvider>(componentDelegate, itemsToInitialize.Cast<TObjectType>().ToList(), dataTypeToInitializeList.Select(x => x.Item1.IdDataType).ToList());

            foreach (TObjectType item in itemsToInitialize)
            {
                foreach (Tuple<OpDataType, OpViewColumn> dataTypeToProceed in dataTypeToInitializeList)
                {
                    IOpData data = ((TObjectType)item).DataList.First(x => x.IdDataType == dataTypeToProceed.Item1.IdDataType);
                    if (data != null)
                    {
                        // [2017-02-14]
                        // Założyliśmy, że wszystkie czasy jakie dostajemy są w strefie lokalnej, więc zawsze
                        // wszystkie wartości czasowe konwertujemy do UTC przed zapisem.
                        if (dataTypeToProceed.Item1.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime)
                        {
                            if (data.Value is DateTime)
                            {
                                data.Value = DataProvider.ConvertTimeZoneToUtcTime((DateTime)data.Value);
                            }
                        }

                        FillResultDictionary(keyProvider(item), data.Value, dataTypeToProceed.Item2, resultDict);
                    }
                }
            }
        }

        #endregion

        #region EnsureImrServerDatatype

        private bool EnsureImrServerDatatype<TObjectType>(int idImrServer, OpViewColumn columnToProceed, IEnumerable<object> items, System.Func<TObjectType, GenericKey> keyPicker, Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> dict)
        {
            Log(EventID.ViewsManager.InitializeViewIsColumnImrServer, columnToProceed.IdDataType == DataType.HELPER_ID_IMR_SERVER);

            if (columnToProceed.IdDataType == DataType.HELPER_ID_IMR_SERVER)
            {
                foreach (TObjectType item in items)
                {
                    Tuple<string, GenericKey> key = Tuple.Create(columnToProceed.Table, keyPicker(item));
                    EnsureKeyExists(dict, key);
                    dict[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, (object)idImrServer, columnToProceed.DataType, 0));
                }

                return true;
            }

            return false;
        }
        
        #endregion

        #region EnsureKeyExists

        private Dictionary<TKey, TValue> EnsureKeyExists<TKey, TValue>(Dictionary<TKey, TValue> dict, TKey key)
            where TValue : new()
        {
            if (!dict.ContainsKey(key))
            {
                dict.Add(key, new TValue());
            }

            return dict;
        }
        
        #endregion

        #region SetHelperValues

        private List<TObjectType> SetHelperValues<TObjectType, TDataProvider>(System.Action<TDataProvider, TObjectType, List<long>> componentDelegate, List<TObjectType> objectList, List<long> idDataTypeToSet = null)
            where TDataProvider : IDataProvider
        {
            foreach (TObjectType obj in objectList)
            {
                componentDelegate((TDataProvider)this.DataProvider, obj, idDataTypeToSet);
            }

            return objectList;
        }
        
        #endregion

        #region FindAllSiblingColumns

        List<OpViewOperation.OpViewOperationCellValue> FindAllSiblingColumns(Dictionary<string, OpViewColumn> columnDefinitionsByNamesDict,
            List<OpViewOperation.OpViewOperationCellValue> sourceList,
            OpViewOperation.OpViewOperationCellValue value)
        {
            List<OpViewOperation.OpViewOperationCellValue> result = new List<OpViewOperation.OpViewOperationCellValue>();

            if (columnDefinitionsByNamesDict.ContainsKey(value.ColumnName))
            {
                OpViewColumn valueColumn = columnDefinitionsByNamesDict[value.ColumnName];

                foreach (OpViewOperation.OpViewOperationCellValue sourceItem in sourceList)
                {
                    if (columnDefinitionsByNamesDict.ContainsKey(sourceItem.ColumnName))
                    {
                        OpViewColumn sourceColumn = columnDefinitionsByNamesDict[sourceItem.ColumnName];

                        if (valueColumn.IdDataType == sourceColumn.IdDataType
                            && String.Equals(valueColumn.Table.ToUpper().Trim(), sourceColumn.Table.ToUpper().Trim())
                            && value.Index == sourceItem.Index)
                        {
                            result.Add(sourceItem);
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region Hierarchy column

        #region ProceedHierarchyColumn

        // Dictionary<Tuple<TableName, item key>, List<Tuple<ColumnName, ColumnValue, OpDataType, IndexNbr>>>
        private void ProceedHierarchyColumn(OpView view,
            OpImrServer server,
            OpViewColumn columnToProceed,
            Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> inputData)
        {
            if (!view.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedHierarchyColumnViewNotSynchronizable, view.IdView, view.Name, server.IdImrServer);
                return;
            }

            // hierarchy col to powinna być ta sama która jest columnToProceed
            // <hierarchy col, parent col>
            Tuple<OpViewColumn, OpViewColumn> hierarchyColumns = GetHierarchyColumns(view);

            if (hierarchyColumns.Item1 == null || hierarchyColumns.Item2 == null)
            {
                throw new InvalidOperationException(string.Format("View does not contain defined hierarchy columns, unable to proceed. Hierarchy column: {0}, Parent column: {1}",
                    hierarchyColumns.Item1 == null ? "<null>" : hierarchyColumns.Item1.IdViewColumn.ToString(),
                    hierarchyColumns.Item2 == null ? "<null>" : hierarchyColumns.Item2.IdViewColumn.ToString()));
            }

            // Tworzymy sobie listę kluczy i spłaszczony słownik do szybszego i łatwiejszego wyszukiwania.
            // Jedna pętla jest lepsza od 2x linq.
            List<GenericKey> itemsFromTableKeyList = new List<GenericKey>();
            Dictionary<GenericKey, List<OpViewOperation.OpViewOperationCellValue>> data = new Dictionary<GenericKey, List<OpViewOperation.OpViewOperationCellValue>>();

            foreach (var kvInputData in inputData)
            {
                if (String.Equals(kvInputData.Key.Item1, columnToProceed.Table))
                {
                    itemsFromTableKeyList.Add(kvInputData.Key.Item2);
                    data.Add(kvInputData.Key.Item2, kvInputData.Value);
                }
            }

            // Teoretycznie for powinien być szybszy od foreach.
            // Dodatkowo postawiłem założenie, iż istnieje duża szansa na to, że wiersz rodzica będzie w kolekcji przed wierszem dziecka
            // więc w późniejszym wywołaniu rekurencyjnym przydaje się indeks analizowanego elementu.
            // Oczywiście nie jest to pewnik więc pod uwagę są brane oba przypadki.
            for (int i = 0; i < itemsFromTableKeyList.Count; ++i)
            {
                string hierarchy = string.Empty;

                OpViewOperation.OpViewOperationCellValue parentColumnValuesForRow = data[itemsFromTableKeyList[i]].FirstOrDefault(x => String.Equals(x.Value, hierarchyColumns.Item2.Name));
                if (parentColumnValuesForRow == default(OpViewOperation.OpViewOperationCellValue) || parentColumnValuesForRow.Value == null) // Find tez zwraca default
                {
                    // nie ma parenta lub ma null - najwyższy parent
                    hierarchy = string.Format(".{0}.", itemsFromTableKeyList[i]);
                }
                else
                {
                    List<GenericKey> parentsList = GetAllParents(new GenericKey(parentColumnValuesForRow.Value), hierarchyColumns, itemsFromTableKeyList, data, i);
                    parentsList.Reverse(); // Po wyjściu z metody parent najwyżej w hierarchi będzie ostatni więc odwracamy kolekcje by kolejne klucze były ustawione w kolejności od korzenia do liścia
                    parentsList.Add(itemsFromTableKeyList[i]); // Dodajemy na koniec obecny wiersz
                    hierarchy = string.Format(".{0}.", string.Join(".", parentsList.Select(x => x.ToString())));
                }

                data[itemsFromTableKeyList[i]].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, (object)hierarchy, columnToProceed.DataType, 0));
            }
        }

        #endregion

        #region GetAllParents

        private List<GenericKey> GetAllParents(GenericKey key,
            Tuple<OpViewColumn, OpViewColumn> hierarchyColumns,
            List<GenericKey> itemsFromTableKeyList,
            Dictionary<GenericKey, List<OpViewOperation.OpViewOperationCellValue>> data,
            int beginIndex)
        {
            bool found = false;
            List<GenericKey> result = new List<GenericKey>();

            // W pierwszej kolejności wyszukujemy rodzica wiersza wstecz,
            // ponieważ jest duża szansa, że tam będzie (ale nie musi).
            // W przypadku jak go tam nie znajdziemy, to będzie trzeba wyszukać w przód,
            // ale nie ma sensu przeszukiwać całej kolekcji tylko od momentu wystąpienia
            // analizowanego dziecka.

            for (int i = beginIndex; i >= 0; --i)
            {
                if (itemsFromTableKeyList[i] == key)
                {
                    // Znaleźliśmy parenta dla poprzedniego wywołania

                    // Szukamy czy ten wiersz także ma parenta.
                    OpViewOperation.OpViewOperationCellValue parentColumnValuesForRow = data[itemsFromTableKeyList[i]].FirstOrDefault(x => String.Equals(x.ColumnName, hierarchyColumns.Item2.Name));
                    if (parentColumnValuesForRow == default(OpViewOperation.OpViewOperationCellValue) || parentColumnValuesForRow.Value == null)
                    {
                        // nie ma parenta lub ma null - najwyższy parent
                        result.Add(key);
                    }
                    else
                    {
                        // ma parenta więc szukamy dalej
                        result.Add(key);
                        result.AddRange(GetAllParents(new GenericKey(parentColumnValuesForRow.Value), hierarchyColumns, itemsFromTableKeyList, data, i));
                    }

                    found = true;

                    break;
                }
            }

            if (!found)
            {
                for (int i = beginIndex; i < itemsFromTableKeyList.Count; ++i)
                {
                    if (itemsFromTableKeyList[i] == key)
                    {
                        // Znaleźliśmy parenta dla poprzedniego wywołania

                        // Szukamy czy ten wiersz także ma parenta.
                        OpViewOperation.OpViewOperationCellValue parentColumnValuesForRow = data[itemsFromTableKeyList[i]].FirstOrDefault(x => String.Equals(x.ColumnName, hierarchyColumns.Item2.Name));
                        if (parentColumnValuesForRow == default(OpViewOperation.OpViewOperationCellValue) || parentColumnValuesForRow.Value == null)
                        {
                            // nie ma parenta lub ma null - najwyższy parent
                            result.Add(key);
                        }
                        else
                        {
                            // ma parenta więc szukamy dalej
                            result.Add(key);
                            result.AddRange(GetAllParents(new GenericKey(parentColumnValuesForRow.Value), hierarchyColumns, itemsFromTableKeyList, data, i));
                        }

                        break;
                    }
                }
            }

            return result;
        }

        #endregion

        #region IsTreeView

        private bool IsTreeView(OpView view)
        {
            return view != null && view.DataList != null && view.DataList.Any(x => x.IdDataType == DataType.VIEW_IS_TREE_VIEW && (((x.Value is bool) && (bool)x.Value == true) || Convert.ToInt32(x.Value) == 1));
        }

        #endregion

        #region IsHierarchyColumn

        private bool IsHierarchyColumn(OpView view, OpViewColumn column)
        {
            return view != null && view.DataList != null && view.DataList.Any(x => x.IdDataType == DataType.VIEW_ID_HIERARCHY_COLUMN && Convert.ToInt32(x.Value) == column.IdViewColumn);
        }

        #endregion

        #region IsParentColumn

        private bool IsParentColumn(OpView view, OpViewColumn column)
        {
            return view != null && view.DataList != null && view.DataList.Any(x => x.IdDataType == DataType.VIEW_ID_HIERARCHY_PARENT_COLUMN && Convert.ToInt32(x.Value) == column.IdViewColumn);
        }

        #endregion

        #region GetHierarchyColumns

        // <hierarchy col, parent col>
        private Tuple<OpViewColumn, OpViewColumn> GetHierarchyColumns(OpView view)
        {
            return (view == null || view.DataList == null)
                ? new Tuple<OpViewColumn, OpViewColumn>(null, null)
                : Tuple.Create(view.Columns.FirstOrDefault(x => IsHierarchyColumn(view, x)), view.Columns.FirstOrDefault(x => IsParentColumn(view, x)));
        }

        #endregion

        #endregion

        #endregion

        #region DeleteNonExistingRowsFromViews

        private void DeleteNonExistingRowsFromViews(IEnumerable<OpView> views)
        {
            Log(EventID.ViewsManager.DeleteNonExistingRowsEnterFunction);

            foreach (OpView view in views)
            {
                OpViewColumn pkColumn = view.PrimaryKeyColumn;
                OpViewColumn serverColumn = view.ImrServerColumn;

                Log(EventID.ViewsManager.DeleteNonExistingRowsAnalyzingView, view == null ? "<null>" : view.Name);

                if (ViewExists(view) != true)
                {
                    Log(EventID.ViewsManager.DeleteNonExistingRowsViewDoesNotExist, view.IdView, view.Name);
                    continue;
                }

                if (pkColumn == null || serverColumn == null)
                {
                    Log(EventID.ViewsManager.DeleteNonExistingRowsAnalyzingViewMissingPKColumns,
                        view.Name,
                        pkColumn == null ? "<null>" : pkColumn.Name,
                        serverColumn == null ? "<null>" : serverColumn.Name);

                    continue;
                }

                string selectClause = string.Format("[{0}],[{1}]", pkColumn.Name, serverColumn.Name);
                Tuple<long, long>[] viewPrimaryKeyServer = DataProvider.GetFilterFlexView(1, int.MaxValue - 5, view, selectClause, null, null).Item2.Select(x => Tuple.Create(x.Item1, Convert.ToInt64(x.Item2.First(y => y.Key == serverColumn.Name).Value))).ToArray();
                long[] viewPrimaryKeys = viewPrimaryKeyServer.Select(x => x.Item1).ToArray();

                Log(EventID.ViewsManager.DeleteNonExistingRowsViewPrimaryKeys, viewPrimaryKeys == null ? "<null>" : viewPrimaryKeys.Count().ToString());

                // Zakładamy, że jeśli nic nie dostaniemy z tabelki, to tablica będzie pusta.
                if (viewPrimaryKeys == null)
                {
                    return;
                }

                long[] keysFromTable = GetKeysFromTable(TableNameToEnum(pkColumn.Table), viewPrimaryKeys, 180);
                Log(EventID.ViewsManager.DeleteNonExistingRowsTablePrimaryKeys, keysFromTable == null ? "<null>" : keysFromTable.Count().ToString());

                if (viewPrimaryKeys != null && keysFromTable != null)
                {
                    // Jesli jakiś kluczy nie było w tabelce to znaczy
                    // że powinniśmy usunąć nadmiarowe klucze z widoku
                    List<Tuple<long, long>> keysToDeleteFromView = new List<Tuple<long, long>>();

                    HashSet<long> keysFromTableHS = new HashSet<long>();
                    foreach (var x in keysFromTable)
                    {
                        keysFromTableHS.Add(x);
                    }

                    foreach (Tuple<long, long> pkIdServer in viewPrimaryKeyServer)
                    {
                        if (!keysFromTableHS.Contains(pkIdServer.Item1))
                        {
                            keysToDeleteFromView.Add(pkIdServer);
                        }
                    }

                    if (keysToDeleteFromView.Count > 0)
                    {
                        Dictionary<long, OpImrServer> imrServers = DataProvider.GetImrServer(keysToDeleteFromView.Select(x => Convert.ToInt32(x.Item2)).Distinct().ToArray()).ToDictionary(k => (long)k.IdImrServer, v => v);

                        Log(EventID.ViewsManager.DeleteNonExistingKeysToDeleteFromView, keysToDeleteFromView == null ? "<null>" : keysToDeleteFromView.Count.ToString());

                        List<OpViewOperation.OpViewOperationCellValue> dummyColumnList = new List<OpViewOperation.OpViewOperationCellValue>();

                        lock (this.ViewOperationQueue)
                        {
                            foreach (Tuple<long, long> pkIdServer in keysToDeleteFromView)
                            {
                                this.ViewOperationQueue.Enqueue(new OpViewOperation(pkIdServer.Item1, view.PrimaryKeyColumn.DataType, imrServers[pkIdServer.Item2], view, Enums.ViewOperation.DeleteRow, dummyColumnList));
                            }
                        }
                    }
                }
            }

            Log(EventID.ViewsManager.DeleteNonExistingRowsExitFunction);
        }

        #region GetKeysFromTable

        private long[] GetKeysFromTable(Enums.Tables table, long[] keys, int commandTimeout)
        {
            switch (table)
            {
                case Enums.Tables.ACTION: return DataProvider.GetActionFilter(loadNavigationProperties: false, mergeIntoCache: false, IdAction: keys, SerialNbr: null, IdMeter: null, IdLocation: null, IdActionType: null, IdActionStatus: null, IdActionData: null, IdActionParent: null, IdDataArch: null, IdModule: null, IdOperator: null, CreationDate: null, topCount: null, customWhereClause: null, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false).Select(x => x.IdAction).ToArray();
                case Enums.Tables.ACTION_DATA: return DataProvider.GetActionDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdActionData: keys).Select(x => x.IdActionData).ToArray();
                case Enums.Tables.ACTOR: return DataProvider.GetActorFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdActor: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdActor).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ACTOR_GROUP: return DataProvider.GetActorGroupFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdActorGroup: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdActorGroup).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ALARM: return DataProvider.GetAlarmFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarm: keys).Select(x => x.IdAlarm).ToArray();
                case Enums.Tables.ALARM_DATA: return DataProvider.GetAlarmDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmData: keys).Select(x => x.IdAlarmData).ToArray();
                case Enums.Tables.ALARM_DEF: return DataProvider.GetAlarmDefFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmDef: keys).Select(x => x.IdAlarmDef).ToArray();
                case Enums.Tables.ALARM_EVENT: return DataProvider.GetAlarmEventFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmEvent: keys).Select(x => x.IdAlarmEvent).ToArray();
                case Enums.Tables.ALARM_HISTORY: return DataProvider.GetAlarmHistoryFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmHistory: keys).Select(x => x.IdAlarmHistory).ToArray();
                case Enums.Tables.ALARM_STATUS: return DataProvider.GetAlarmStatusFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmStatus: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdAlarmStatus).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ALARM_TEXT: return DataProvider.GetAlarmTextFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmText: keys).Select(x => x.IdAlarmText).ToArray();
                case Enums.Tables.ALARM_TYPE: return DataProvider.GetAlarmTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAlarmType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdAlarmType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ARTICLE: return DataProvider.GetArticleFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdArticle: keys).Select(x => x.IdArticle).ToArray();
                case Enums.Tables.AUDIT: return DataProvider.GetAuditFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdAudit: keys).Select(x => x.IdAudit).ToArray();
                case Enums.Tables.CODE: return DataProvider.GetCodeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdCode: keys).Select(x => x.IdCode).ToArray();
                case Enums.Tables.COMPONENT: return DataProvider.GetComponentFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdComponent: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdComponent).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.CONFIGURATION_PROFILE: return DataProvider.GetConfigurationProfileFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdConfigurationProfile: keys).Select(x => x.IdConfigurationProfile).ToArray();
                case Enums.Tables.DATA: return DataProvider.GetDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdData: keys).Select(x => x.IdData).ToArray();
                case Enums.Tables.DATA_ARCH: return DataProvider.GetDataArchFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDataArch: keys).Select(x => x.IdDataArch).ToArray();
                case Enums.Tables.DATA_TEMPORAL: return DataProvider.GetDataTemporalFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDataTemporal: keys).Select(x => x.IdDataTemporal).ToArray();
                case Enums.Tables.DATA_TRANSFER: return DataProvider.GetDataTransferFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDataTransfer: keys).Select(x => x.IdDataTransfer).ToArray();
                case Enums.Tables.DATA_TYPE: return DataProvider.GetDataTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDataType: keys).Select(x => x.IdDataType).ToArray();
                case Enums.Tables.DEPOSITORY_ELEMENT: return DataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDepositoryElement: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDepositoryElement).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DESCR: return DataProvider.GetDescrFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDescr: keys).Select(x => x.IdDescr).ToArray();
                case Enums.Tables.DEVICE: return DataProvider.GetDeviceFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, SerialNbr: keys).Select(x => x.SerialNbr).ToArray();
                case Enums.Tables.DEVICE_CONNECTION: return DataProvider.GetDeviceConnectionFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceConnection: keys).Select(x => x.IdDeviceConnection).ToArray();
                case Enums.Tables.DEVICE_DISTRIBUTOR_HISTORY: return DataProvider.GetDeviceDistributorHistoryFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceDistributorHistory: keys).Select(x => x.IdDeviceDistributorHistory).ToArray();
                case Enums.Tables.DEVICE_DRIVER: return DataProvider.GetDeviceDriverFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceDriver: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceDriver).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DEVICE_HIERARCHY: return DataProvider.GetDeviceHierarchyFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceHierarchy: keys).Select(x => x.IdDeviceHierarchy).ToArray();
                case Enums.Tables.DEVICE_ORDER_NUMBER: return DataProvider.GetDeviceOrderNumberFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceOrderNumber: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceOrderNumber).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DEVICE_PATTERN: return DataProvider.GetDevicePatternFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, SerialNbr: keys).Select(x => x.SerialNbr).ToArray();
                case Enums.Tables.DEVICE_SIM_CARD_HISTORY: return DataProvider.GetDeviceSimCardHistoryFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceSimCardHistory: keys).Select(x => x.IdDeviceSimCardHistory).ToArray();
                case Enums.Tables.DEVICE_STATE_TYPE: return DataProvider.GetDeviceStateTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceStateType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceStateType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DEVICE_TYPE: return DataProvider.GetDeviceTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DEVICE_TYPE_CLASS: return DataProvider.GetDeviceTypeClassFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceTypeClass: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceTypeClass).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DEVICE_WARRANTY: return DataProvider.GetDeviceWarrantyFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDeviceWarranty: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDeviceWarranty).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DIAGNOSTIC_ACTION: return DataProvider.GetDiagnosticActionFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDiagnosticAction: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDiagnosticAction).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DIAGNOSTIC_ACTION_DATA: return DataProvider.GetDiagnosticActionDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDiagnosticActionData: keys).Select(x => x.IdDiagnosticActionData).ToArray();
                case Enums.Tables.DIAGNOSTIC_ACTION_RESULT: return DataProvider.GetDiagnosticActionResultFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDiagnosticActionResult: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDiagnosticActionResult).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DISTRIBUTOR: return DataProvider.GetDistributorFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDistributor: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdDistributor).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.DISTRIBUTOR_DATA: return DataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDistributorData: keys).Select(x => x.IdDistributorData).ToArray();
                case Enums.Tables.DISTRIBUTOR_PERFORMANCE: return DataProvider.GetDistributorPerformanceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdDistributorPerformance: keys).Select(x => x.IdDistributorPerformance).ToArray();
                case Enums.Tables.ETL_PERFORMANCE: return DataProvider.GetEtlPerformanceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdEtlPerformance: keys).Select(x => x.IdEtlPerformance).ToArray();
                case Enums.Tables.IMR_SERVER: return DataProvider.GetImrServerFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdImrServer: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdImrServer).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.IMR_SERVER_PERFORMANCE: return DataProvider.GetImrServerPerformanceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdImrServerPerformance: keys).Select(x => x.IdImrServerPerformance).ToArray();
                case Enums.Tables.ISSUE: return DataProvider.GetIssueFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdIssue: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdIssue).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ISSUE_DATA: return DataProvider.GetIssueDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdIssueData: keys).Select(x => x.IdIssueData).ToArray();
                case Enums.Tables.ISSUE_HISTORY: return DataProvider.GetIssueHistoryFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdIssueHistory: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdIssueHistory).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ISSUE_STATUS: return DataProvider.GetIssueStatusFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdIssueStatus: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdIssueStatus).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ISSUE_TYPE: return DataProvider.GetIssueTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdIssueType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdIssueType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.LANGUAGE: return DataProvider.GetLanguageFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLanguage: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdLanguage).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.LOCATION: return DataProvider.GetLocationFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLocation: keys).Select(x => x.IdLocation).ToArray();
                case Enums.Tables.LOCATION_EQUIPMENT: return DataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLocationEquipment: keys).Select(x => x.IdLocationEquipment).ToArray();
                case Enums.Tables.LOCATION_HIERARCHY: return DataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLocationHierarchy: keys).Select(x => x.IdLocationHierarchy).ToArray();
                case Enums.Tables.LOCATION_STATE_TYPE: return DataProvider.GetLocationStateTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLocationStateType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdLocationStateType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.LOCATION_TYPE: return DataProvider.GetLocationTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdLocationType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdLocationType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.METER: return DataProvider.GetMeterFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdMeter: keys).Select(x => x.IdMeter).ToArray();
                case Enums.Tables.METER_TYPE: return DataProvider.GetMeterTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdMeterType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdMeterType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.METER_TYPE_CLASS: return DataProvider.GetMeterTypeClassFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdMeterTypeClass: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdMeterTypeClass).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.MODULE: return DataProvider.GetModuleFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdModule: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdModule).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.OPERATOR: return DataProvider.GetOperatorFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdOperator: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdOperator).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.OPERATOR_DATA: return DataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdOperatorData: keys).Select(x => x.IdOperatorData).ToArray();
                case Enums.Tables.PRIORITY: return DataProvider.GetPriorityFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdPriority: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdPriority).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.PRODUCT_CODE: return DataProvider.GetProductCodeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdProductCode: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdProductCode).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.PROFILE: return DataProvider.GetProfileFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdProfile: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdProfile).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.PROFILE_DATA: return DataProvider.GetProfileDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdProfileData: keys).Select(x => x.IdProfileData).ToArray();
                case Enums.Tables.PROTOCOL: return DataProvider.GetProtocolFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdProtocol: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdProtocol).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.PROTOCOL_DRIVER: return DataProvider.GetProtocolDriverFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdProtocolDriver: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdProtocolDriver).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.REFUEL: return DataProvider.GetRefuelFilter(loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRefuel: keys).Select(x => x.IdRefuel).ToArray();
                case Enums.Tables.REFUEL_DATA: return DataProvider.GetRefuelDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRefuelData: keys).Select(x => x.IdRefuelData).ToArray();
                case Enums.Tables.REPORT: return DataProvider.GetReportFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdReport: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdReport).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.REPORT_DATA: return DataProvider.GetReportDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdReportData: keys).Select(x => x.IdReportData).ToArray();
                case Enums.Tables.REPORT_PERFORMANCE: return DataProvider.GetReportPerformanceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdReportPerformance: keys).Select(x => x.IdReportPerformance).ToArray();
                case Enums.Tables.ROLE: return DataProvider.GetRoleFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRole: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdRole).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ROLE_ACTIVITY: return DataProvider.GetRoleActivityFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRoleActivity: keys).Select(x => x.IdRoleActivity).ToArray();
                case Enums.Tables.ROUTE: return DataProvider.GetRouteFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRoute: keys).Select(x => x.IdRoute).ToArray();
                case Enums.Tables.ROUTE_DATA: return DataProvider.GetRouteDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRouteData: keys).Select(x => x.IdRouteData).ToArray();
                case Enums.Tables.ROUTE_POINT: return DataProvider.GetRoutePointFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRoutePoint: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdRoutePoint).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ROUTE_STATUS: return DataProvider.GetRouteStatusFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRouteStatus: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdRouteStatus).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.ROUTE_TYPE: return DataProvider.GetRouteTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdRouteType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdRouteType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.SHIPPING_LIST: return DataProvider.GetShippingListFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdShippingList: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdShippingList).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.SHIPPING_LIST_DATA: return DataProvider.GetShippingListDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdShippingListData: keys).Select(x => x.IdShippingListData).ToArray();
                case Enums.Tables.SHIPPING_LIST_DEVICE: return DataProvider.GetShippingListDeviceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdShippingListDevice: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdShippingListDevice).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.SHIPPING_LIST_TYPE: return DataProvider.GetShippingListTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdShippingListType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdShippingListType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.SIM_CARD: return DataProvider.GetSimCardFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdSimCard: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdSimCard).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.SIM_CARD_DATA: return DataProvider.GetSimCardDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdSimCardData: keys).Select(x => x.IdSimCardData).ToArray();
                case Enums.Tables.SLOT_TYPE: return DataProvider.GetSlotTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdSlotType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdSlotType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK: return DataProvider.GetTaskFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTask: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTask).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK_DATA: return DataProvider.GetTaskDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskData: keys).Select(x => x.IdTaskData).ToArray();
                case Enums.Tables.TASK_GROUP: return DataProvider.GetTaskGroupFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskGroup: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTaskGroup).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK_HISTORY: return DataProvider.GetTaskHistoryFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskHistory: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTaskHistory).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK_STATUS: return DataProvider.GetTaskStatusFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskStatus: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTaskStatus).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK_TYPE: return DataProvider.GetTaskTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTaskType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TASK_TYPE_GROUP: return DataProvider.GetTaskTypeGroupFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTaskTypeGroup: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTaskTypeGroup).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TRANSMISSION_DRIVER: return DataProvider.GetTransmissionDriverFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTransmissionDriver: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTransmissionDriver).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.TRANSMISSION_DRIVER_DATA: return DataProvider.GetTransmissionDriverDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTransmissionDriverData: keys).Select(x => x.IdTransmissionDriverData).ToArray();
                case Enums.Tables.TRANSMISSION_DRIVER_PERFORMANCE: return DataProvider.GetTransmissionDriverPerformanceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTransmissionDriverPerformance: keys).Select(x => x.IdTransmissionDriverPerformance).ToArray();
                case Enums.Tables.TRANSMISSION_TYPE: return DataProvider.GetTransmissionTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdTransmissionType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdTransmissionType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.UNIT: return DataProvider.GetUnitFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdUnit: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdUnit).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.VIEW: return DataProvider.GetViewFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdView: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdView).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.VIEW_COLUMN: return DataProvider.GetViewColumnFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdViewColumn: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdViewColumn).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.VIEW_COLUMN_DATA: return DataProvider.GetViewColumnDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdViewColumnData: keys).Select(x => x.IdViewColumnData).ToArray();
                case Enums.Tables.VIEW_COLUMN_SOURCE: return DataProvider.GetViewColumnSourceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdViewColumnSource: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdViewColumnSource).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.VIEW_COLUMN_TYPE: return DataProvider.GetViewColumnTypeFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdViewColumnType: keys.Select(x => Convert.ToInt32(x)).ToArray()).Select(x => x.IdViewColumnType).Select(x => Convert.ToInt64(x)).ToArray();
                case Enums.Tables.VIEW_DATA: return DataProvider.GetViewDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, commandTimeout: commandTimeout, IdViewData: keys).Select(x => x.IdViewData).ToArray();
            }

            return null;
        }

        #endregion

        #endregion

        #region VerifyViews

        #region ViewExists

        private bool? ViewExists(OpView view)
        {
            if (view == null)
            {
                return null;
            }

            switch (view.DatabaseType)
            {
                case Enums.DatabaseType.CORE:
                    return DataProvider.CheckIfTableExists(view.Name);
                case Enums.DatabaseType.DW:
                    return DataProvider.CheckIfTableExistsDW(view.Name);
                case Enums.DatabaseType.Unknown:
                    view.DatabaseType = Enums.DatabaseType.DW;
                    Log(EventID.ViewsManager.GetViewsToBuildDatabaseTypeIsUnknownSettingDefault, view.IdView, view.Name, view.DatabaseType);
                    return DataProvider.CheckIfTableExistsDW(view.Name);
                default:
                    Log(EventID.ViewsManager.GetViewsToBuildDatabaseTypeNotSupported, view.IdView, view.Name, view.DatabaseType);
                    return null;
            }
        }

        #endregion

        #region GetViewsToBuild

        private Dictionary<OpView, List<OpViewColumn>> GetViewsToBuild(List<OpView> views)
        {
            // <Widok do inicjalizacji, kolumny do inicjalizacji>
            // Jeśli wartosć (lista kolumn) jest null lub pusta to inicjalizujemy caly widok
            Dictionary<OpView, List<OpViewColumn>> viewsToCreate = new Dictionary<OpView, List<OpViewColumn>>();

            foreach (OpView view in views)
            {
                if (!view.IsSynchronizable)
                {
                    Log(EventID.ViewsManager.GetViewsToBuildNotSynchronizable, view.IdView, view.Name, "-");
                    continue;
                }

                Log(EventID.ViewsManager.GetViewsToBuildVeryfingView, view.IdView, view.Name);

                bool? viewExists = ViewExists(view);

                Log(EventID.ViewsManager.GetViewsToBuildViewExists, view.Name, viewExists);

                if (viewExists == true)
                {
                    List<OpViewColumn> missingColumns = new List<OpViewColumn>();

                    foreach (OpViewColumn column in view.Columns)
                    {
                        if (!ColumnExists(view, column.Name))
                        {
                            missingColumns.Add(column);
                        }
                    }

                    if (missingColumns.Count > 0)
                    {
                        viewsToCreate.Add(view, missingColumns);
                        Log(EventID.ViewsManager.GetViewsToBuildExistsMissingColumns, view.Name, viewExists, missingColumns.Count);
                    }
                }
                else if (viewExists == false)
                {
                    viewsToCreate.Add(view, null);
                }

            }

            return viewsToCreate;
        }

        #endregion

        #region BuildViews

        private void BuildViews(Dictionary<OpView, List<OpViewColumn>> viewsToCreate)
        {
            foreach (KeyValuePair<OpView, List<OpViewColumn>> kvViewToCreate in viewsToCreate)
            {
                if (!kvViewToCreate.Key.IsSynchronizable)
                {
                    Log(EventID.ViewsManager.BuildViewsViewNotSynchronizable, kvViewToCreate.Key.IdView, kvViewToCreate.Key.Name, "-");
                    continue;
                }

                if (kvViewToCreate.Value == null || kvViewToCreate.Value.Count == 0)
                {
                    Log(EventID.ViewsManager.BuildViewsCreatingView, kvViewToCreate.Key.Name);

                    ProceedViewCreation(kvViewToCreate.Key);
                }
                else
                {
                    // Ważne!
                    // Jeśli kolumna która tu będzie, zależy od innej kolumny ale tamta inna kolumna nie ma Key=1 to ta kolumna
                    // nie zostanie zainicjalizowana bo warunku w GroupBy tego nie złapią.
                    // Przykładoow HELPER_TIME jako exteneded helper i jakieś DATA dla którego ten czas jest wpisywany
                    // To DATA nie jest Key=1 więc HELPER_TIME jako tako nie zostanie ruszony ale dzięki temu, że pod uwage zostanie
                    // wzięte DATA bo jest kluczem (IdKeyViewColumn) dla HELPER_TIME, to HELPER_TIME zostanie zainicjalizowany
                    // bo jest extended helperem w tym przykładzie.
                    Log(EventID.ViewsManager.BuildViewsInitializingViewColumns, kvViewToCreate.Key.Name, string.Join(",", kvViewToCreate.Value));

                    List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>> columnsToAdd = new List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>>();
                    foreach (OpViewColumn column in kvViewToCreate.Value.Where(x => !x.PrimaryKey))
                    {
                        columnsToAdd.Add(Tuple.Create(Enums.ViewOperation.AddColumn, kvViewToCreate.Key.Name, column.Name, (Enums.DataTypeClass)column.DataType.IdDataTypeClass, true));
                    }
                    DataProvider.ProceedViewChange(kvViewToCreate.Key, columnsToAdd.ToArray());
                }
            }
        }

        #endregion

        #region VerifyViews

        private void VerifyViews(List<OpView> views)
        {
            try
            {
                Log(EventID.ViewsManager.VerifingViews, views == null ? "<null>" : views.Count.ToString());

                // <Widok do inicjalizacji, kolumny do inicjalizacji>
                // Jeśli wartosć (lista kolumn) jest null lub pusta to inicjalizujemy caly widok
                Dictionary<OpView, List<OpViewColumn>> viewsToCreate = GetViewsToBuild(views);

                // <Widok do inicjalizacji, kolumny do inicjalizacji>
                // Jeśli wartosć (lista kolumn) jest null lub pusta to inicjalizujemy caly widok
                BuildViews(viewsToCreate);

                foreach (KeyValuePair<OpView, List<OpViewColumn>> kvViewToCreate in viewsToCreate)
                {
                    if (!kvViewToCreate.Key.IsSynchronizable)
                    {
                        Log(EventID.ViewsManager.VerifyViewsViewNotSynchronizable, kvViewToCreate.Key.IdView, kvViewToCreate.Key.Name, "-");
                        continue;
                    }

                    if (kvViewToCreate.Value == null || kvViewToCreate.Value.Count == 0)
                    {
                        Log(EventID.ViewsManager.VerifyViewInitializingView, kvViewToCreate.Key.Name);

                        InitializeView(kvViewToCreate.Key, kvViewToCreate.Key.ImrServers.First(), null);

                        Log(EventID.ViewsManager.VerifyViewInitializingViewFinished, kvViewToCreate.Key.Name);
                    }
                    else
                    {
                        Log(EventID.ViewsManager.BuildViewsInitializingViewColumns, kvViewToCreate.Key.Name, string.Join(",", kvViewToCreate.Value));

                        InitializeView(kvViewToCreate.Key, kvViewToCreate.Key.ImrServers.First(), kvViewToCreate.Value, null, true);

                        Log(EventID.ViewsManager.VerifyViewInitializingViewColumnsFinished, kvViewToCreate.Key.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.VerifyViewsException, ex.ToString());
                throw ex;
            }
            finally
            {
                Log(EventID.ViewsManager.ViewsVerified);
            }
        }

        #endregion

        #region ColumnExists

        private bool ColumnExists(OpView view, string columnName)
        {
            try
            {
                System.Data.DataTable dt = null;

                if (view.DatabaseType == Enums.DatabaseType.CORE)
                {
                    dt = Cache.TableSchema.Get(view.Name, DataProvider.dbConnectionCore);
                }
                else if (view.DatabaseType == Enums.DatabaseType.DW)
                {
                    dt = Cache.TableSchema.Get(view.Name, DataProvider.dbConnectionDw);
                }

                return dt != null && dt.Columns != null && dt.Columns.Cast<System.Data.DataColumn>().Any(x => String.Equals(x.ColumnName, columnName));
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region ProceedViewCreation

        private void ProceedViewCreation(OpView viewToCreate)
        {
            if (!viewToCreate.IsSynchronizable)
            {
                Log(EventID.ViewsManager.ProceedViewCreationViewNotSynchronizable, viewToCreate.IdView, viewToCreate.Name, "-");
                return;
            }

            if (viewToCreate != null)
            {
                DataProvider.ProceedViewCreation(viewToCreate);
            }
        }

        private void ProceedViewCreation(List<ChangeInTable> changeInTable)
        {
            int[] existingViewsIds = this.Views.Select(x => x.IdView).ToArray();

            int[] newViewIds = changeInTable.Where(x => TranslateChangeActionToViewOperation(x.ChangeAction) == Enums.ViewOperation.InsertRow)
                                            .SelectMany(x => x.Columns.Where(y => y.Item1 == "ID_VIEW" && y.Item2 != null).Select(y => Convert.ToInt32(y.Item2)))
                                            .Where(x => !x.In(existingViewsIds)).ToArray();

            if (newViewIds.Length > 0)
            {
                Log(EventID.ViewsManager.ProceedViewCreationViewsToCreate, newViewIds.Length, string.Join(",", newViewIds));

                List<OpView> viewsToCreate = DataProvider.GetView(newViewIds);

                OpView.LoadNavigationProperties(ref viewsToCreate, DataProvider);
                //OpView.LoadCustomData(ref viewsToCreate, DataProvider);
                DataProvider.GetViewDataFilter(IdView: newViewIds).GroupBy(x => x.IdView).ToList().ForEach(g => viewsToCreate.First(x => x.IdView == g.Key).DataList.AddRange(g));
                OpView.LoadViewColumns(ref viewsToCreate, DataProvider);

                ViewComponent.LoadImrServers(DataProvider, viewsToCreate);

                foreach (OpView view in viewsToCreate)
                {
                    if (!view.IsSynchronizable)
                    {
                        Log(EventID.ViewsManager.ProceedViewCreationViewNotSynchronizable, view.IdView, view.Name, "-");
                        continue;
                    }

                    try
                    {
                        Log(EventID.ViewsManager.ProceedViewCreationCreatingView, view.Name);

                        ProceedViewCreation(view);

                        Log(EventID.ViewsManager.ProceedViewCreationInitializingView, view.Name);

                        InitializeView(view, view.ImrServers.FirstOrDefault(), null);

                        Log(EventID.ViewsManager.ProceedViewCreationInitializingViewFinished, view.Name);

                        this.Views.Add(view);
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.ViewsManager.ProceedViewCreationException, view.Name, ex.ToString());
                    }
                }
            }
        }

        #endregion

        #endregion

        #region DBCollector connections

        #region InitializeDBCollector

        private bool InitializeDBCollector(OpImrServer isItem, OpOperator viewsManagerOperator)
        {
            try
            {
                Log(EventID.ViewsManager.DBCollectorConnectionOpenStart, isItem.IdImrServer, isItem.StandardDescription);

                if (String.IsNullOrEmpty(isItem.DBCollectorWebServiceAddress))
                {
                    Log(EventID.ViewsManager.IMRServerMissingDBCollectorAddress, isItem.StandardDescription, isItem.IdImrServer);
                }
                else
                {
                    DBCollectorComponent.DBCollectorCallbackHandler.ReceiveMessageDelegate rmdMethod = new DBCollectorComponent.DBCollectorCallbackHandler.ReceiveMessageDelegate(OnReceiveMessage);

                    List<object> callbackMethods = new List<object>();
                    callbackMethods.Add(rmdMethod);

                    string webServiceAddress = isItem.DBCollectorWebServiceAddress;
                    string webServiceBinding = isItem.DBCollectorWebServiceBinding;
                    #region Custom DBCollector webservice addresss

                    // Zostało to dodane ponieważ ViewManager wymaga określonego adresu by połączyć się z ESBDriver.
                    // Ten adres jest trzymany w datatypie który trzyma adres DBCollector a na imrprod nie chcieliśmy
                    // tego zmieniać.
                    OpViewData customWebServiceAddress = this.Views.SelectMany(x => x.DataList).FirstOrDefault(x => x.IdDataType == DataType.VIEW_IMR_SERVER_WEBSERVICE_ADDRESS);
                    if (customWebServiceAddress != null && customWebServiceAddress.Value != null && !string.IsNullOrWhiteSpace(customWebServiceAddress.Value.ToString()))
                    {
                        webServiceAddress = customWebServiceAddress.Value.ToString();
                    }

                    OpViewData customWebServiceBinding = this.Views.SelectMany(x => x.DataList).FirstOrDefault(x => x.IdDataType == DataType.VIEW_IMR_SERVER_WEBSERVICE_BINDING);
                    if (customWebServiceBinding != null && customWebServiceBinding.Value != null && !string.IsNullOrWhiteSpace(customWebServiceBinding.Value.ToString()))
                    {
                        webServiceBinding = customWebServiceBinding.Value.ToString();
                    }
                    
                    #endregion

                    Tuple<DBCollectorClient, SESSION> connection = DBCollectorComponent.CreateWcfConnection(webServiceAddress, this.DataProvider.DistributorFilter,
                        viewsManagerOperator.Login, viewsManagerOperator.Password, Enums.Module.ViewsManager, callbackMethods.ToArray(),
                        binding: String.IsNullOrEmpty(webServiceBinding) ? null : webServiceBinding);

                    if (connection == null || connection.Item2 == null || connection.Item1 == null)
                    {
                        Log(EventID.ViewsManager.DBCollectorConnectionUnexpectedError, isItem.IdImrServer, isItem.StandardDescription);
                        return false;
                    }

                    if (connection.Item2.Status != SESSION_STATUS.OK)
                    {
                        Log(EventID.ViewsManager.DBCollectorConnectionFailed, isItem.IdImrServer, isItem.StandardDescription, connection.Item2.Status, connection.Item2.Error.Msg);
                        return false;
                    }

                    this.Servers[connection.Item2.Guid] = isItem;

                    foreach (OpView view in Views)
                    {
                        view.DataList.SetValue(DataType.VIEW_CURRENT_SESSION_GUID, connection.Item2.Guid.ToString());
                        OpViewData viewData = view.DataList.First(x => x.IdDataType == DataType.VIEW_CURRENT_SESSION_GUID);
                        viewData.IdView = view.IdView;
                        DataProvider.SaveViewData(viewData);
                    }
                }

            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.DBCollectorConnectionException, ex.ToString());
            }

            Log(EventID.ViewsManager.DBCollectorConnectionOpenEnd, isItem.IdImrServer, isItem.StandardDescription);

            return true;
        }
        
        #endregion

        #region DBCollectorKeepAlive

        private void DBCollectorKeepAlive()
        {
            // 90s by sprostac wymaganiam receive timeout - jesli nie da rady to bedzie sie rozlaczac z powodu tego timeouta
            const int waitIntevralMs = 90 * 1000;
            //const int waitIntevralMs = 30 * 1000;

            OpOperator viewsManagerOperator = OperatorComponent.GetOperatorWithCreation(this.DataProvider, ViewsManagerOperatorLogin, password: this.ViewsManagerOperatorPassword);

            while (!DBCollectorKeepAliveStopEvent.WaitOne(waitIntevralMs, false))
            {
                foreach (OpImrServer isItem in this.Views.SelectMany(v => v.ImrServers).Distinct())
                {
                    string webServiceAddress = isItem.DBCollectorWebServiceAddress;
                    #region Custom DBCollector webservice addresss

                    // Zostało to dodane ponieważ ViewManager wymaga określonego adresu by połączyć się z ESBDriver.
                    // Ten adres jest trzymany w datatypie który trzyma adres DBCollector a na imrprod nie chcieliśmy
                    // tego zmieniać.
                    OpViewData customWebServiceAddress = this.Views.SelectMany(x => x.DataList).FirstOrDefault(x => x.IdDataType == DataType.VIEW_IMR_SERVER_WEBSERVICE_ADDRESS);
                    if (customWebServiceAddress != null && customWebServiceAddress.Value != null && !string.IsNullOrWhiteSpace(customWebServiceAddress.Value.ToString()))
                    {
                        webServiceAddress = customWebServiceAddress.Value.ToString();
                    }

                    #endregion

                    try
                    {
                        if (DBCollectorComponent.ConnectionCallbackDict != null && DBCollectorComponent.ConnectionCallbackDict.ContainsKey(webServiceAddress))
                        {
                            Tuple<DBCollectorClient, SESSION> clientSessionTuple = DBCollectorComponent.ConnectionCallbackDict[webServiceAddress];

                            if (IsConnected(clientSessionTuple.Item1, clientSessionTuple.Item2))
                            {
                                SESSION session = clientSessionTuple.Item2;
                                clientSessionTuple.Item1.KeepAlive(ref session);
                            }
                            else
                            {
                                DBCollectorReconnect(isItem, viewsManagerOperator, 0);
                            }
                        }
                        else
                        {
                            if (!InitializeDBCollector(isItem, viewsManagerOperator) && DBCollectorComponent.ConnectionCallbackDict != null && DBCollectorComponent.ConnectionCallbackDict.ContainsKey(webServiceAddress))
                            {
                                DBCollectorComponent.ConnectionCallbackDict.Remove(webServiceAddress);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(EventID.ViewsManager.DBCollectorKeepAliveException, ex.ToString());
                        if (DBCollectorComponent.ConnectionCallbackDict != null && DBCollectorComponent.ConnectionCallbackDict.ContainsKey(webServiceAddress))
                        {
                            DBCollectorComponent.ConnectionCallbackDict.Remove(webServiceAddress);
                        }
                    }
                }
            }
        }
        
        #endregion

        #region IsConnected

        private bool IsConnected(DBCollectorClient dbCollectorClient, SESSION dbCollectorSession)
        {
            bool isConnectedException = false;
            SESSION dummySession = null; // musi być null
            if (dbCollectorClient != null)
            {
                try
                {
                    dbCollectorClient.ConnectionCheck(ref dummySession);
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.DBCollectorIsConnectedException, ex.ToString());
                    isConnectedException = true;
                }
            }

            if (!isConnectedException && dbCollectorClient != null && dbCollectorSession != null && dbCollectorClient.State == System.ServiceModel.CommunicationState.Opened)
            {
                return true;
            }

            Log(EventID.ViewsManager.DBCollectorNotConnected,
                dbCollectorClient == null ? "<null>" : "OK",
                dbCollectorSession == null ? "<null>" : "OK",
                dbCollectorClient == null ? "<null>" : dbCollectorClient.State.ToString(),
                isConnectedException);

            return false;
        }
        
        #endregion

        #region DBCollectorReconnect

        private void DBCollectorReconnect(OpImrServer isItem, OpOperator viewsManagerOperator, int Retry)
        {
            Log(EventID.ViewsManager.DBCollectorReconnectInfo, Retry);

            if (Retry < 1)
            {
                try
                {
                    if (!InitializeDBCollector(isItem, viewsManagerOperator))
                    {
                        Thread.Sleep(1000);
                        DBCollectorReconnect(isItem, viewsManagerOperator, Retry + 1);
                    }
                }
                catch (Exception ex)
                {
                    Log(EventID.ViewsManager.DBCollectorReconnectExcepion, ex.ToString());
                    DBCollectorCloseSession(isItem);
                }
            }
            else
            {
                DBCollectorCloseSession(isItem);
            }
        }
        
        #endregion

        #region DBCollectorCloseSession

        private void DBCollectorCloseSession(OpImrServer isItem)
        {
            try
            {
                if (DBCollectorComponent.ConnectionCallbackDict != null)
                {
                    Tuple<DBCollectorClient, SESSION> clientSessionTuple = DBCollectorComponent.ConnectionCallbackDict[isItem.DBCollectorWebServiceAddress];

                    SESSION session = clientSessionTuple.Item2;
                    session = clientSessionTuple.Item1.Logout(session);
                    Log(EventID.ViewsManager.DBCollectorLoggedOut,
                        session == null ? "<null>" : session.Guid.ToString(),
                        session == null ? "<null>" : session.Status.ToString());

                    if (clientSessionTuple.Item1 != null)
                    {
                        clientSessionTuple.Item1.Abort();
                    }
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.DBCollectorCloseError, ex.ToString());
            }
            finally
            {
                if (DBCollectorComponent.ConnectionCallbackDict != null && DBCollectorComponent.ConnectionCallbackDict.ContainsKey(isItem.DBCollectorWebServiceAddress))
                {
                    DBCollectorComponent.ConnectionCallbackDict.Remove(isItem.DBCollectorWebServiceAddress);
                }

                Log(EventID.ViewsManager.DBCollectorClosed);
            }
        }
        
        #endregion

        #endregion

        #region Utils

        #region DataProviderFilterAction

        private List<TOpObject> DataProviderFilterAction<TOpObject, TKey>(OpImrServer isItem, OpViewColumn currentColumn, Enums.ReferenceType refType, TKey[] idObject, Func<TKey[], List<TOpObject>> deleg)
        {
            TKey[] keys = SelectKeys<TKey>(idObject == null ? null : idObject.Select(x => Convert.ToInt64(x)).ToArray(), isItem, refType);

            if (!currentColumn.PrimaryKey && (keys == null || keys.Length == 0))
            {
                return new List<TOpObject>();
            }

            List<TOpObject> result = deleg(keys);


            return result;
        }
        
        #endregion

        #region TryGetView

        private OpView TryGetView(int idView, OpView view = null)
        {
            if (view == null)
            {
                // Szukamy w zdefiniowanych widokach
                view = Views.FirstOrDefault(x => x.IdView == idView);

                if (view == null)
                {
                    // Próbujemy pobrać.
                    view = ViewComponent.GetViewConfiguration(this.DataProvider, new int[] { idView }).FirstOrDefault();

                    if (view != null)
                    {
                        Views.Add(view);
                    }
                }
            }

            return view;
        }

        #endregion

        #region TableNameToEnum

        private Enums.Tables TableNameToEnum(string tableName)
        {
            // Szukamy odpowiednika po nazwie w enumie, gdybyśmy go nie znaleźli to nie chcemy się wywalać tylko zwrócić UNKNOWN, a nie ma poco pisać super ifowanego kodu stąd concat i first.
            // Concat zawsze dopisze na koniec kolekcji więc UNKNOWN zostanie zwrócony tylko jak nie znajdziemy odpowiednika
            // http://stackoverflow.com/questions/42221928/does-enumerable-concat-always-append-at-the-end-of-the-first-collection
            return Enum.GetValues(typeof(Enums.Tables)).Cast<Enums.Tables>().Where(x => String.Equals(x.ToString().ToUpper().Trim(), tableName.ToUpper().Trim())).Concat(new Enums.Tables[] { Enums.Tables.UNKNOWN }).First();
        }

        #endregion

        #region ReferenceTypeToTable

        private string ReferenceTypeToTable(int idReferenceType)
        {
            switch ((Enums.ReferenceType)idReferenceType)
            {
                case Enums.ReferenceType.IdOperator: return Enums.Tables.OPERATOR.ToString();
                case Enums.ReferenceType.SerialNbr: return Enums.Tables.DEVICE.ToString();
                case Enums.ReferenceType.IdMeter: return Enums.Tables.METER.ToString();
                case Enums.ReferenceType.IdLocation: return Enums.Tables.LOCATION.ToString();
                case Enums.ReferenceType.IdDistributor: return Enums.Tables.DISTRIBUTOR.ToString();
                case Enums.ReferenceType.IdMeterType: return Enums.Tables.METER_TYPE.ToString();
                case Enums.ReferenceType.IdDeviceType: return Enums.Tables.DEVICE_TYPE.ToString();
                case Enums.ReferenceType.IdIssueStatus: return Enums.Tables.ISSUE_STATUS.ToString();
                case Enums.ReferenceType.IdIssueType: return Enums.Tables.ISSUE_TYPE.ToString();
                case Enums.ReferenceType.IdTaskStatus: return Enums.Tables.TASK_STATUS.ToString();
                case Enums.ReferenceType.IdTaskType: return Enums.Tables.TASK_TYPE.ToString();
                case Enums.ReferenceType.IdRoute: return Enums.Tables.ROUTE.ToString();
                case Enums.ReferenceType.IdRouteStatus: return Enums.Tables.ROUTE_STATUS.ToString();
                case Enums.ReferenceType.IdRole: return Enums.Tables.ROLE.ToString();
                case Enums.ReferenceType.IdIMRServer: return Enums.Tables.IMR_SERVER.ToString();
                case Enums.ReferenceType.IdProfile: return Enums.Tables.PROFILE.ToString();
                case Enums.ReferenceType.IdLocationStateType: return Enums.Tables.LOCATION_STATE_TYPE.ToString();
                case Enums.ReferenceType.IdLocationType: return Enums.Tables.LOCATION_TYPE.ToString();
                case Enums.ReferenceType.IdArticle: return Enums.Tables.ARTICLE.ToString();
                case Enums.ReferenceType.IdTaskGroup: return Enums.Tables.TASK_GROUP.ToString();
                case Enums.ReferenceType.IdIssue: return Enums.Tables.ISSUE.ToString();
                case Enums.ReferenceType.IdTask: return Enums.Tables.TASK.ToString();
                case Enums.ReferenceType.IdTransmissionDriver: return Enums.Tables.TRANSMISSION_DRIVER.ToString();
                case Enums.ReferenceType.IdSimCard: return Enums.Tables.SIM_CARD.ToString();
                case Enums.ReferenceType.IdActor: return Enums.Tables.ACTOR.ToString();
                case Enums.ReferenceType.IdDepositoryElement: return Enums.Tables.DEPOSITORY_ELEMENT.ToString();
                case Enums.ReferenceType.IdDeviceOrderNumber: return Enums.Tables.DEVICE_ORDER_NUMBER.ToString();
                case Enums.ReferenceType.IdRefuel: return Enums.Tables.REFUEL.ToString();
                case Enums.ReferenceType.IdPriority: return Enums.Tables.PRIORITY.ToString();
                case Enums.ReferenceType.IdAlarm: return Enums.Tables.ALARM.ToString();
                case Enums.ReferenceType.IdRouteType: return Enums.Tables.ROUTE_TYPE.ToString();
                case Enums.ReferenceType.IdAlarmType: return Enums.Tables.ALARM_TYPE.ToString();
                case Enums.ReferenceType.IdAlarmDef: return Enums.Tables.ALARM_DEF.ToString();
                case Enums.ReferenceType.IdTariff: return Enums.Tables.TARIFF.ToString();
            }

            throw new ArgumentException(string.Format("No table defined for id reference type: {0}", idReferenceType));
        }

        #endregion

        #region ReferenceTypeToKeyDataType

        private OpDataType ReferenceTypeToKeyDataType(int idReferenceType)
        {
            switch ((Enums.ReferenceType)idReferenceType)
            {
                case Enums.ReferenceType.IdOperator: return DataProvider.GetDataType(DataType.HELPER_ID_OPERATOR);
                case Enums.ReferenceType.SerialNbr: return DataProvider.GetDataType(DataType.HELPER_SERIAL_NBR);
                case Enums.ReferenceType.IdMeter: return DataProvider.GetDataType(DataType.HELPER_ID_METER);
                case Enums.ReferenceType.IdLocation: return DataProvider.GetDataType(DataType.HELPER_ID_LOCATION);
                case Enums.ReferenceType.IdDistributor: return DataProvider.GetDataType(DataType.HELPER_ID_DISTRIBUTOR);
                case Enums.ReferenceType.IdMeterType: return DataProvider.GetDataType(DataType.HELPER_ID_METER_TYPE);
                case Enums.ReferenceType.IdDeviceType: return DataProvider.GetDataType(DataType.HELPER_ID_DEVICE_TYPE);
                case Enums.ReferenceType.IdIssueStatus: return DataProvider.GetDataType(DataType.HELPER_ID_ISSUE_STATUS);
                case Enums.ReferenceType.IdIssueType: return DataProvider.GetDataType(DataType.HELPER_ID_ISSUE_STATUS);
                case Enums.ReferenceType.IdTaskStatus: return DataProvider.GetDataType(DataType.HELPER_ID_TASK_STATUS);
                case Enums.ReferenceType.IdTaskType: return DataProvider.GetDataType(DataType.HELPER_ID_TASK_TYPE);
                case Enums.ReferenceType.IdRoute: return DataProvider.GetDataType(DataType.HELPER_ID_ROUTE);
                case Enums.ReferenceType.IdRouteStatus: return DataProvider.GetDataType(DataType.HELPER_ID_ROUTE_STATUS);
                case Enums.ReferenceType.IdIMRServer: return DataProvider.GetDataType(DataType.HELPER_ID_IMR_SERVER);
                case Enums.ReferenceType.IdLocationStateType: return DataProvider.GetDataType(DataType.HELPER_ID_LOCATION_STATE_TYPE);
                case Enums.ReferenceType.IdLocationType: return DataProvider.GetDataType(DataType.HELPER_ID_LOCATION_TYPE);
                case Enums.ReferenceType.IdTaskGroup: return DataProvider.GetDataType(DataType.HELPER_ID_TASK_GROUP);
                case Enums.ReferenceType.IdIssue: return DataProvider.GetDataType(DataType.HELPER_ID_ISSUE);
                case Enums.ReferenceType.IdTask: return DataProvider.GetDataType(DataType.HELPER_ID_TASK);
                case Enums.ReferenceType.IdActor: return DataProvider.GetDataType(DataType.HELPER_ID_ACTOR);
                case Enums.ReferenceType.IdDeviceOrderNumber: return DataProvider.GetDataType(DataType.HELPER_ID_DEVICE_ORDER_NUMBER);
                case Enums.ReferenceType.IdAlarm: return DataProvider.GetDataType(DataType.HELPER_ID_ALARM);
                case Enums.ReferenceType.IdRouteType: return DataProvider.GetDataType(DataType.HELPER_ID_ROUTE_TYPE);
                case Enums.ReferenceType.IdRouteDef: return DataProvider.GetDataType(DataType.HELPER_ID_ROUTE_DEF);
                case Enums.ReferenceType.IdAlarmType: return DataProvider.GetDataType(DataType.HELPER_ID_ALARM_TYPE);
                case Enums.ReferenceType.IdAlarmDef: return DataProvider.GetDataType(DataType.HELPER_ID_ALARM_DEF);
                case Enums.ReferenceType.IdTariff: return DataProvider.GetDataType(DataType.HELPER_ID_TARIFF);
            }

            throw new ArgumentException(string.Format("No helper defined for id reference type: {0}", idReferenceType));
        }

        #endregion

        #region FillResultDictionary

        private void FillResultDictionary(GenericKey idObject, object value, OpViewColumn columnToProceed, Dictionary<Tuple<string, GenericKey>, List<OpViewOperation.OpViewOperationCellValue>> result)
        {
            Tuple<string, GenericKey> key = Tuple.Create(columnToProceed.Table, idObject);
            EnsureKeyExists(result, key);

            if (result[key].Any(x => x.ColumnName == columnToProceed.Name && x.IdDataType == columnToProceed.IdDataType && x.Index == 0))
            {
                return;
            }

            result[key].Add(new OpViewOperation.OpViewOperationCellValue(columnToProceed.Name, value, columnToProceed.DataType, 0));
        }
        
        #endregion

        #region GetAssociativeTableName

        private string GetAssociativeTableName(OpViewColumn viewColumn)
        {
            return string.IsNullOrWhiteSpace(viewColumn.AssociativeTable) ? viewColumn.Table : viewColumn.AssociativeTable;
        }

        #endregion

        #region CreateCustomWhereClause

        private string CreateCustomWhereClause(OpImrServer isItem, OpViewColumn column, string pkName, GenericKey lastId, Dictionary<int, string> customWhereClausesDict)
        {
            // To jest PK więc nie trzeba pilnować '' itp w zapytaniu.
            // Dodatkowo mamy tutaj troche chamskiego SQL ponieważ
            // procedury przy top count mają często order by, raz asc raz desc.
            // Nam bardzo zależy na ASC. '--' na końcu wymazuje order by z procedury składowanej.
            return column.PrimaryKey
                ? string.Format("{0}[{1}] > {2} ORDER BY [{1}] ASC --",
                    ((customWhereClausesDict != null && customWhereClausesDict.ContainsKey(column.IdViewColumn)) ? string.Format("{0} AND ", customWhereClausesDict[column.IdViewColumn]) : string.Empty),
                    pkName, lastId.KeyValue)
                : null;                                                                      
        }

        private string CreateCustomWhereClause(OpImrServer isItem, OpViewColumn column, Dictionary<int, string> customWhereClausesDict)
        {
            return customWhereClausesDict != null && customWhereClausesDict.ContainsKey(column.IdViewColumn)
                ? customWhereClausesDict[column.IdViewColumn]
                : null;
        }
        
        #endregion

        #region GetTopCountValue

        private long? GetTopCountValue(OpImrServer isItem, OpViewColumn column, int batchSize)
        {
            return column.PrimaryKey
                ? (long?)batchSize
                : null;
        }

        #endregion

        #region Cache methods

        private void ClearCaches()
        {
            SimCardCache.Clear();
            LocationCache.Clear();
            DeviceCache.Clear();
            ActionCache.Clear();
            ActorCache.Clear();
            AlarmCache.Clear();
            AlarmEventCache.Clear();
            DeviceConnectionCache.Clear();
            DistributorCache.Clear();
            IssueCache.Clear();
            MeterCache.Clear();
            OperatorCache.Clear();
            ReportCache.Clear();
            RouteCache.Clear();
            TaskCache.Clear();
            TariffCache.Clear();

            PrimaryKeyCache.Clear();
        }

        private List<TObjectType> GetDataFromCache<TObjectType, TKey>(OpImrServer isItem, int idReferenceType, Func<long[], IEnumerable<object>> dataSource, Dictionary<int, Dictionary<TKey, TObjectType>> cache, Func<object, long> keyProvider)
        {
            List<TObjectType> result = null;

            if (cache.ContainsKey(isItem.IdImrServer))
            {
                result = cache[isItem.IdImrServer].Values.Cast<TObjectType>().ToList();

                long[] keys = GetKeysFromCache(isItem, idReferenceType);

                if (keys != null)
                {
                    long[] keysFromObjCache = result.Select(x => keyProvider(x)).ToArray();

                    // Jeśli keys > 0 to znaczy, że w cacheu kluczy mamy ich więcej niż w cache obiektów
                    // więc trzeba dociagnąć obiekty.
                    keys = keysFromObjCache == null || keysFromObjCache.Length == 0 ? keys : keys.Except(keysFromObjCache).ToArray();
                    if (keys.Length > 0)
                    {
                        List<TObjectType> missingObjList = dataSource(keys).Cast<TObjectType>().ToList();
                        foreach (TObjectType obj in missingObjList)
                        {
                            cache[isItem.IdImrServer].Add((TKey)Convert.ChangeType(keyProvider(obj), typeof(TKey)), obj);
                            UpdateKeysCache(isItem, obj, idReferenceType);
                        }
                    }
                }
            }

            if (result == null || result.Count == 0)
            {
                result = dataSource(null).Cast<TObjectType>().ToList();

                if (!cache.ContainsKey(isItem.IdImrServer))
                {
                    cache.Add(isItem.IdImrServer, new Dictionary<TKey, TObjectType>());
                }

                foreach (TObjectType obj in result)
                {
                    cache[isItem.IdImrServer].Add((TKey)Convert.ChangeType(keyProvider(obj), typeof(TKey)), obj);
                    UpdateKeysCache(isItem, obj, idReferenceType);
                }
            }

            return result;
        }

        private List<TObjectType> GetDataFromCache<TObjectType, TKey>(OpImrServer isItem,
            Enums.ReferenceType referneceType,
            Func<TKey[], long?, string, List<TObjectType>> dataSource,
            System.Collections.IDictionary iCache,
            Func<object, long> keyProvider,
            string customWhere = null,
            long? topCount = null)
        {
            List<TObjectType> result = null;

            Dictionary<int, Dictionary<TKey, TObjectType>> cache = (Dictionary<int, Dictionary<TKey, TObjectType>>) iCache;

            if (cache.ContainsKey(isItem.IdImrServer))
            {
                result = cache[isItem.IdImrServer].Values.Cast<TObjectType>().ToList();

                long[] keys = GetKeysFromCache(isItem, (int)referneceType);

                if (keys != null)
                {
                    long[] keysFromObjCache = result.Select(x => keyProvider(x)).ToArray();

                    // Jeśli keys > 0 to znaczy, że w cacheu kluczy mamy ich więcej niż w cache obiektów
                    // więc trzeba dociagnąć obiekty.
                    keys = keysFromObjCache == null || keysFromObjCache.Length == 0 ? keys : keys.Except(keysFromObjCache).ToArray();
                    if (keys.Length > 0)
                    {
                        TKey[] tKeys = SelectKeys<TKey>(keys, isItem, referneceType);
                        if (tKeys == null)
                        {
                            keys = null;
                        }
                        else
                        {
                            keys = tKeys.Select(x => Convert.ToInt64(x)).ToArray();
                        }
                        
                        List<TObjectType> missingObjList = dataSource(keys.Select(x => (TKey)Convert.ChangeType(x, typeof(TKey))).ToArray(), topCount, customWhere).Cast<TObjectType>().ToList();
                        foreach (TObjectType obj in missingObjList)
                        {
                            cache[isItem.IdImrServer].Add((TKey)Convert.ChangeType(keyProvider(obj), typeof(TKey)), obj);
                            UpdateKeysCache(isItem, obj, (int)referneceType);
                        }
                    }
                }
            }

            if (result == null || result.Count == 0)
            {
                result = dataSource(SelectKeys<TKey>(null, isItem, referneceType), topCount, customWhere).Cast<TObjectType>().ToList();

                if (!cache.ContainsKey(isItem.IdImrServer))
                {
                    cache.Add(isItem.IdImrServer, new Dictionary<TKey, TObjectType>());
                }

                foreach (TObjectType obj in result)
                {
                    cache[isItem.IdImrServer].Add((TKey)Convert.ChangeType(keyProvider(obj), typeof(TKey)), obj);
                    UpdateKeysCache(isItem, obj, (int)referneceType);
                }
            }

            return result;
        }

        private List<TObjectType> TryGetDataFromCache<TObjectType, TCacheKey>(OpImrServer isItem, Enums.ReferenceType referneceType, Func<TCacheKey[], long?, string, List<TObjectType>> dataSource, string customWhere = null, long? topCount = null)
        {
            try
            {
                switch (referneceType)
                {
                    case Enums.ReferenceType.IdLocation: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, LocationCache, o => ((OpLocation)o).IdLocation, customWhere, topCount).Cast<TObjectType>().ToList();
                    case Enums.ReferenceType.SerialNbr: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, DeviceCache, o => ((OpDevice)o).SerialNbr, customWhere, topCount);
                    case Enums.ReferenceType.IdSimCard: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, SimCardCache, o => ((OpSimCard)o).IdSimCard, customWhere, topCount);
                    case Enums.ReferenceType.IdAction: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, ActionCache, o => ((UI.Business.Objects.CORE.OpAction)o).IdAction, customWhere, topCount);
                    case Enums.ReferenceType.IdActor: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, ActorCache, o => ((OpActor)o).IdActor, customWhere, topCount);
                    case Enums.ReferenceType.IdAlarm: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, AlarmCache, o => ((UI.Business.Objects.DW.OpAlarm)o).IdAlarm, customWhere, topCount);
                    case Enums.ReferenceType.IdAlarmEvent: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, AlarmEventCache, o => ((IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent)o).IdAlarmEvent, customWhere, topCount);
                    case Enums.ReferenceType.IdDeviceConnection: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, DeviceConnectionCache, o => ((IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection)o).IdDeviceConnection, customWhere, topCount);
                    case Enums.ReferenceType.IdDistributor: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, DistributorCache, o => ((OpDistributor)o).IdDistributor, customWhere, topCount);
                    case Enums.ReferenceType.IdIssue: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, IssueCache, o => ((OpIssue)o).IdIssue, customWhere, topCount);
                    case Enums.ReferenceType.IdMeter: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, MeterCache, o => ((OpMeter)o).IdMeter, customWhere, topCount);
                    case Enums.ReferenceType.IdOperator: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, OperatorCache, o => ((OpOperator)o).IdOperator, customWhere, topCount);
                    case Enums.ReferenceType.IdReport: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, ReportCache, o => ((IMR.Suite.UI.Business.Objects.DW.OpReport)o).IdReport, customWhere, topCount);
                    case Enums.ReferenceType.IdRoute: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, RouteCache, o => ((OpRoute)o).IdRoute, customWhere, topCount);
                    case Enums.ReferenceType.IdTariff: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, TariffCache, o => ((OpTariff)o).IdTariff, customWhere, topCount);
                    case Enums.ReferenceType.IdTask: return GetDataFromCache<TObjectType, TCacheKey>(isItem, referneceType, dataSource, TaskCache, o => ((OpTask)o).IdTask, customWhere, topCount);
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.FailedToGetDataFromCache, isItem == null ? "<null>" : isItem.IdImrServer.ToString(), referneceType, ex);
            }

            return dataSource(null, topCount, customWhere).ToList();
        }

        private List<TObjectType> TryGetDataFromCache<TObjectType>(OpImrServer isItem, int idReferneceType, Func<long[], IEnumerable<object>> dataSource)
        {
            try
            {
                switch (idReferneceType)
                {
                    case (int)Enums.ReferenceType.IdLocation: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpLocation)o).IdLocation);
                    case (int)Enums.ReferenceType.SerialNbr: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpDevice)o).SerialNbr);
                    case (int)Enums.ReferenceType.IdSimCard: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpSimCard)o).IdSimCard);
                    case (int)Enums.ReferenceType.IdAction: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((UI.Business.Objects.CORE.OpAction)o).IdAction);
                    case (int)Enums.ReferenceType.IdActor: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpActor)o).IdActor);
                    case (int)Enums.ReferenceType.IdAlarm: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((UI.Business.Objects.DW.OpAlarm)o).IdAlarm);
                    case (int)Enums.ReferenceType.IdAlarmEvent: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent)o).IdAlarmEvent);
                    case (int)Enums.ReferenceType.IdDeviceConnection: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection)o).IdDeviceConnection);
                    case (int)Enums.ReferenceType.IdDistributor: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpDistributor)o).IdDistributor);
                    case (int)Enums.ReferenceType.IdIssue: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpIssue)o).IdIssue);
                    case (int)Enums.ReferenceType.IdMeter: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpMeter)o).IdMeter);
                    case (int)Enums.ReferenceType.IdOperator: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpOperator)o).IdOperator);
                    case (int)Enums.ReferenceType.IdReport: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((IMR.Suite.UI.Business.Objects.DW.OpReport)o).IdReport);
                    case (int)Enums.ReferenceType.IdRoute: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpRoute)o).IdRoute);
                    case (int)Enums.ReferenceType.IdTariff: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpTariff)o).IdTariff);
                    case (int)Enums.ReferenceType.IdTask: return TryGetDataFromCache<TObjectType>(isItem, idReferneceType, dataSource, o => ((OpTask)o).IdTask);
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.FailedToGetDataFromCache, isItem == null ? "<null>" : isItem.IdImrServer.ToString(), idReferneceType, ex);
            }

            return dataSource(null).Cast<TObjectType>().ToList();
        }

        private List<TObjectType> TryGetDataFromCache<TObjectType>(OpImrServer isItem, int idReferneceType, Func<long[], IEnumerable<object>> dataSource, Func<object, long> keyProvider)
        {
            try
            {
                switch (idReferneceType)
                {
                    case (int)Enums.ReferenceType.IdLocation: return GetDataFromCache<OpLocation, long>(isItem, idReferneceType, dataSource, LocationCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.SerialNbr: return GetDataFromCache<OpDevice, long>(isItem, idReferneceType, dataSource, DeviceCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdSimCard: return GetDataFromCache<OpSimCard, int>(isItem, idReferneceType, dataSource, SimCardCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdAction: return GetDataFromCache<UI.Business.Objects.CORE.OpAction, long>(isItem, idReferneceType, dataSource, ActionCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdActor: return GetDataFromCache<OpActor, int>(isItem, idReferneceType, dataSource, ActorCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdAlarm: return GetDataFromCache<UI.Business.Objects.DW.OpAlarm, long>(isItem, idReferneceType, dataSource, AlarmCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdAlarmEvent: return GetDataFromCache<UI.Business.Objects.DW.OpAlarmEvent, long>(isItem, idReferneceType, dataSource, AlarmEventCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdDeviceConnection: return GetDataFromCache<IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection, long>(isItem, idReferneceType, dataSource, DeviceConnectionCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdDistributor: return GetDataFromCache<OpDistributor, int>(isItem, idReferneceType, dataSource, DistributorCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdIssue: return GetDataFromCache<OpIssue, int>(isItem, idReferneceType, dataSource, IssueCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdMeter: return GetDataFromCache<OpMeter, long>(isItem, idReferneceType, dataSource, MeterCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdOperator: return GetDataFromCache<OpOperator, int>(isItem, idReferneceType, dataSource, OperatorCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdReport: return GetDataFromCache<IMR.Suite.UI.Business.Objects.DW.OpReport, int>(isItem, idReferneceType, dataSource, ReportCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdRoute: return GetDataFromCache<OpRoute, long>(isItem, idReferneceType, dataSource, RouteCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdTariff: return GetDataFromCache<OpTariff, int>(isItem, idReferneceType, dataSource, TariffCache, keyProvider).Cast<TObjectType>().ToList();
                    case (int)Enums.ReferenceType.IdTask: return GetDataFromCache<OpTask, int>(isItem, idReferneceType, dataSource, TaskCache, keyProvider).Cast<TObjectType>().ToList();
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.FailedToGetDataFromCache, isItem == null ? "<null>" : isItem.IdImrServer.ToString(), idReferneceType, ex);
            }

            return dataSource(null).Cast<TObjectType>().ToList();
        }

        private void UpdateKeysCache(OpImrServer isItem, object obj, int idReferneceType)
        {
            switch (idReferneceType)
            {
                case (int)Enums.ReferenceType.IdLocation:
                {
                    OpLocation loc = obj as OpLocation;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, loc.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocationType, loc.IdLocationType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, loc.IdDistributor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIMRServer, loc.IdImrServer);
                    
                    break;
                }
                case (int)Enums.ReferenceType.SerialNbr:
                {
                    OpDevice dev = obj as OpDevice;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, dev.SerialNbr);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDeviceType, dev.IdDeviceType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, dev.SerialNbrPattern);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, dev.IdDistributor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDeviceStateType, dev.IdDeviceStateType);
                    
                    break;
                }

                case (int)Enums.ReferenceType.IdMeter:
                {
                    OpMeter met = obj as OpMeter;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeter, met.IdMeter);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeterType, met.IdMeterType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, met.IdDistributor);

                    break;
                }
                case (int)Enums.ReferenceType.IdSimCard:
                {
                    OpSimCard sim = obj as OpSimCard;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdSimCard, sim.IdSimCard);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, sim.IdDistributor);
                    
                    break;
                }
                case (int)Enums.ReferenceType.IdAction:
                {
                    UI.Business.Objects.CORE.OpAction act = obj as UI.Business.Objects.CORE.OpAction;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAction, act.IdAction);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, act.SerialNbr);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeter, act.IdMeter);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, act.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdActionStatus, act.IdActionStatus);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAction, act.IdActionParent);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDataArch, act.IdDataArch);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdModule, act.IdModule);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, act.IdOperator);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIMRServer, act.ID_SERVER);

                    break;
                }
                case (int)Enums.ReferenceType.IdActor:
                {
                    OpActor act = obj as OpActor;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdActor, act.IdActor);

                    break;
                }
                case (int)Enums.ReferenceType.IdAlarm:
                {
                    UI.Business.Objects.DW.OpAlarm alarm = obj as UI.Business.Objects.DW.OpAlarm;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarm, alarm.IdAlarm);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmEvent, alarm.IdAlarmEvent);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmDef, alarm.IdAlarmDef);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, alarm.SerialNbr);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeter, alarm.IdMeter);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, alarm.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmType, alarm.IdAlarmType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, alarm.IdOperator);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmStatus, alarm.IdAlarmStatus);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTransmissionType, alarm.IdTransmissionType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmGroup, alarm.IdAlarmGroup);

                    break;
                }
                case (int)Enums.ReferenceType.IdAlarmEvent:
                {
                    UI.Business.Objects.DW.OpAlarmEvent alarmEv = obj as UI.Business.Objects.DW.OpAlarmEvent;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmEvent, alarmEv.IdAlarmEvent);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmDef, alarmEv.IdAlarmDef);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, alarmEv.SerialNbr);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeter, alarmEv.IdMeter);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, alarmEv.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdAlarmType, alarmEv.IdAlarmType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIssue, alarmEv.IdIssue);

                    break;
                }
                case (int)Enums.ReferenceType.IdDeviceConnection:
                {
                    IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection devConn = obj as IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDeviceConnection, devConn.IdDeviceConnection);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, devConn.SerialNbr);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, devConn.SerialNbrParent);

                    break;
                }
                case (int)Enums.ReferenceType.IdDistributor:
                {
                    OpDistributor distr = obj as OpDistributor;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, distr.IdDistributor);

                    break;
                }
                case (int)Enums.ReferenceType.IdIssue:
                {
                    OpIssue issue = obj as OpIssue;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIssue, issue.IdIssue);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdActor, issue.IdActor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, issue.IdOperatorRegistering);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, issue.IdOperatorPerformer);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIssueType, issue.IdIssueType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIssueStatus, issue.IdIssueStatus);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, issue.IdDistributor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, issue.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdPriority, issue.IdPriority);

                    break;
                }
                case (int)Enums.ReferenceType.IdOperator:
                {
                    OpOperator oper = obj as OpOperator;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, oper.IdOperator);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdActor, oper.IdActor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, oper.IdDistributor);

                    break;
                }
                case (int)Enums.ReferenceType.IdReport:
                {
                    IMR.Suite.UI.Business.Objects.DW.OpReport report = obj as IMR.Suite.UI.Business.Objects.DW.OpReport;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdReport, report.IdReport);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdReportType, report.IdReportType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIMRServer, report.ID_SERVER);

                    break;
                }
                case (int)Enums.ReferenceType.IdRoute:
                {
                    OpRoute route = obj as OpRoute;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdRoute, route.IdRoute);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdRouteDef, route.IdRouteDef);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, route.IdOperatorExecutor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, route.IdOperatorApproved);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdRouteStatus, route.IdRouteStatus);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, route.IdDistributor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdRouteType, route.IdRouteType);

                    break;
                }
                case (int)Enums.ReferenceType.IdTariff:
                {
                    OpTariff tariff = obj as OpTariff;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTariff, tariff.IdTariff);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, tariff.IdDistributor);

                    break;
                }
                case (int)Enums.ReferenceType.IdTask:
                {
                    OpTask task = obj as OpTask;

                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTask, task.IdTask);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTaskType, task.IdTaskType);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdIssue, task.IdIssue);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTaskGroup, task.IdTaskGroup);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdRoute, task.IdPlannedRoute);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, task.IdOperatorRegistering);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, task.IdOperatorPerformer);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdTaskStatus, task.IdTaskStatus);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdOperator, task.IdOperatorAccepted);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, task.IdLocation);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdActor, task.IdActor);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdPriority, task.Priority);
                    AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdDistributor, task.IdDistributor);

                    break;
                }
            }
        }

        private void AddKeyToPrimarykeyCache(OpImrServer isItem, int idReferneceType, long? key)
        {
            EnsureKeyExists(PrimaryKeyCache, idReferneceType);
            EnsureKeyExists(PrimaryKeyCache[idReferneceType], isItem.IdImrServer);

            if (!key.HasValue)
            {
                return;
            }

            // Add dodaje element jesli nie istnieje. Jesli element juz istnieje w
            // hash setcie to nic sie nie dzieje.
            PrimaryKeyCache[idReferneceType][isItem.IdImrServer].Add(key.Value);
        }

        private long[] GetKeysFromCache(OpImrServer isItem, Enums.ReferenceType referneceType)
        {
            return GetKeysFromCache(isItem, (int)referneceType);
        }

        private int[] GetKeysFromCacheInt(OpImrServer isItem, Enums.ReferenceType referneceType)
        {
            long[] items = GetKeysFromCache(isItem, (int)referneceType);
            return items == null ? null : items.Select(x => Convert.ToInt32(x)).ToArray();
        }

        private long[] GetKeysFromCache(OpImrServer isItem, int idReferneceType)
        {
            return PrimaryKeyCache.ContainsKey(idReferneceType) && PrimaryKeyCache[idReferneceType].ContainsKey(isItem.IdImrServer)
                ? PrimaryKeyCache[idReferneceType][isItem.IdImrServer].ToArray()
                : null;
        }

        private TKey[] SelectKeys<TKey>(long[] keys, OpImrServer isItem, Enums.ReferenceType refType)
        {
            TKey[] result = null;

            if (keys == null)
            {
                result = null;
            }
            else if (typeof(TKey) == typeof(long))
            {
                result = keys.Cast<TKey>().ToArray();
            }
            else
            {
                result = keys.Select(x => Convert.ToInt32(x)).Cast<TKey>().ToArray();
            }

            if (result == null)
            {
                long[] keysFromCache = GetKeysFromCache(isItem, refType);

                if (keysFromCache != null)
                {
                    if (typeof(TKey) == typeof(long))
                    {
                        result = keysFromCache.Cast<TKey>().ToArray();
                    }
                    else
                    {
                        result = keysFromCache.Select(x => Convert.ToInt32(x)).Cast<TKey>().ToArray();
                    }
                }
            }

            return result;
        }

        private void CacheLocationEquipment(OpImrServer isItem, int idKeyReferenceType)
        {
            // IdLocation, SerialNbr (device), IdMeter traktujemy troche specjalnie
            // poniewaz laczy je tabelka LocationEquipment. Tak wiec jesli ktores z tych
            // bedzie chcialo klucze przy okazji dodajemy klucze dla pozostalej
            // dwojki.

            long[] keys = GetKeysFromCache(isItem, idKeyReferenceType);

            // Nie chcemy pobierac wszystki (null/Len = 0) LE.
            if (keys == null || keys.Length == 0)
            {
                return;
            }

            List<OpLocationEquipment> leList = new List<OpLocationEquipment>();

            if (idKeyReferenceType == (int)Enums.ReferenceType.IdLocation)
            {
                leList = DataProvider.GetLocationEquipmentFilter(IdLocation: keys, loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    customWhereClause: "[END_TIME] is null");
            }
            else if (idKeyReferenceType == (int)Enums.ReferenceType.IdMeter)
            {
                leList = DataProvider.GetLocationEquipmentFilter(IdMeter: keys, loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    customWhereClause: "[END_TIME] is null");
            }
            else if (idKeyReferenceType == (int)Enums.ReferenceType.SerialNbr)
            {
                leList = DataProvider.GetLocationEquipmentFilter(SerialNbr: keys, loadNavigationProperties: false, loadCustomData: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    customWhereClause: "[END_TIME] is null");
            }

            foreach (OpLocationEquipment le in leList)
            {
                AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdLocation, le.IdLocation);
                AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.SerialNbr, le.SerialNbr);
                AddKeyToPrimarykeyCache(isItem, (int)Enums.ReferenceType.IdMeter, le.IdMeter);
            }
        }

        #endregion

        #region SendEmail

        private void SendEmail(string message)
        {
            if (!EmailNotificationsEnabled)
            {
                return;
            }

            try
            {
                string subject = string.Format("ViewsManager notification from {0}", Environment.MachineName);

                message = ViewsManagerHostIdentification + Environment.NewLine + Environment.NewLine + message;

                foreach (string recipient in EmailRecipients)
                {
                    EmailComponent.SendEmail(recipient, subject, message);
                }
            }
            catch (Exception ex)
            {
                Log(EventID.ViewsManager.ExceptionOccured, ex.ToString());
            }
        }
        
        #endregion

        #endregion

        #region static Log
        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(Enums.Module.ViewsManager, EventData, Parameters);
        }
        #endregion

        #region class GenericKey

        [System.Diagnostics.DebuggerDisplay("Value = {KeyValue}")] 
        internal class GenericKey
            : IComparable, IEquatable<GenericKey>, IConvertible
        {
            public object KeyValue { get; private set; }

            public GenericKey(object keyValue)
            {
                KeyValue = keyValue;

                if (KeyValue == null || (KeyValue.GetType().IsValueType && Activator.CreateInstance(KeyValue.GetType()) == KeyValue))
                {
                    throw new ArgumentException("KeyValue must not be null or default");
                }

                if (KeyValue is GenericKey)
                {
                    KeyValue = (KeyValue as GenericKey).KeyValue;
                }
            }

            public override int GetHashCode()
            {
                return KeyValue.GetHashCode();
            }
            
            #region	IComparable Members

            public int CompareTo(object obj)
            {
                if (obj is GenericKey)
                {
                    return this == (obj as GenericKey) ? 0 : -1;
                }

                return this == new GenericKey(obj) ? 0 : -1;
            }

            #endregion

            #region IEquatable<Key> Members

            public bool Equals(GenericKey other)
            {
                if (other == null)
                    return false;

                return this == other;
            }

            #endregion

            #region override Equals

            public override bool Equals(object obj)
            {
                if (obj is GenericKey)
                {
                    return this == ((GenericKey)obj);
                }

                return this == obj;
            }
            
            #endregion

            #region IConvertible

            public TypeCode GetTypeCode()
            {
                return Type.GetTypeCode(KeyValue.GetType());
            }

            public bool ToBoolean(IFormatProvider provider)
            {
                return Convert.ToBoolean(KeyValue, provider);
            }

            public byte ToByte(IFormatProvider provider)
            {
                return Convert.ToByte(KeyValue, provider);
            }

            public char ToChar(IFormatProvider provider)
            {
                return Convert.ToChar(KeyValue, provider);
            }

            public DateTime ToDateTime(IFormatProvider provider)
            {
                return Convert.ToDateTime(KeyValue, provider);
            }

            public decimal ToDecimal(IFormatProvider provider)
            {
                return Convert.ToDecimal(KeyValue, provider);
            }

            public double ToDouble(IFormatProvider provider)
            {
                return Convert.ToDouble(KeyValue, provider);
            }

            public short ToInt16(IFormatProvider provider)
            {
                return Convert.ToInt16(KeyValue, provider);
            }

            public int ToInt32(IFormatProvider provider)
            {
                return Convert.ToInt32(KeyValue, provider);
            }

            public long ToInt64(IFormatProvider provider)
            {
                return Convert.ToInt64(KeyValue, provider);
            }

            public sbyte ToSByte(IFormatProvider provider)
            {
                return Convert.ToSByte(KeyValue, provider);
            }

            public float ToSingle(IFormatProvider provider)
            {
                return Convert.ToSingle(KeyValue, provider);
            }

            public string ToString(IFormatProvider provider)
            {
                return Convert.ToString(KeyValue, provider);
            }

            public object ToType(Type conversionType, IFormatProvider provider)
            {
                return Convert.ChangeType(KeyValue, conversionType, provider);
            }

            public ushort ToUInt16(IFormatProvider provider)
            {
                return Convert.ToUInt16(KeyValue, provider);
            }

            public uint ToUInt32(IFormatProvider provider)
            {
                return Convert.ToUInt32(KeyValue, provider);
            }

            public ulong ToUInt64(IFormatProvider provider)
            {
                return Convert.ToUInt64(KeyValue, provider);
            }

            #endregion

            public long AsLong()
            {
                return Convert.ToInt64(KeyValue);
            }

            public int AsInt()
            {
                return Convert.ToInt32(KeyValue);
            }

            public static bool operator ==(GenericKey left, object right)
            {
                if ((object)left == null)
                {
                    return right == null;
                }

                if (right == null)
                {
                    // W tym miejscu left nie jest nullem, wiec jak right jest to napewno mamy roznosc.
                    return false;
                }

                object leftValue = left;
                object rightValue = right;

                leftValue = (leftValue as GenericKey).KeyValue;
                if (rightValue is GenericKey) rightValue = (rightValue as GenericKey).KeyValue;

                // ==============================================================

                // Najczesciej wystapi sytuacja, ze jedna z wartosci
                // to long lub int wiec najpierw sprawdzamy bez kombinatoryki
                // kombinacje tych typow.
                if (leftValue is long && rightValue is long)
                {
                    return (long)leftValue == (long)rightValue;
                }
                else if (leftValue is long && rightValue is int)
                {
                    return (long)leftValue == (int)rightValue;
                }
                else if (leftValue is int && rightValue is long)
                {
                    return (int)leftValue == (long)rightValue;
                }
                else if (leftValue is int && rightValue is int)
                {
                    return (int)leftValue == (int)rightValue;
                }

                // No dobra cos nie wyszlo z wczesniejszymi porownaniami
                // wiec szukamy faktycznych typow obu wartosci.
                if (IsIntegerType(leftValue) && IsIntegerType(rightValue))
                {
                    return Convert.ToInt64(leftValue) == Convert.ToInt64(rightValue);
                }

                if (IsRealType(leftValue) && IsRealType(rightValue))
                {
                    return Convert.ToDouble(leftValue) == Convert.ToDouble(rightValue);
                }

                if ((IsIntegerType(leftValue) && IsRealType(rightValue))
                    || (IsRealType(leftValue) && IsIntegerType(rightValue)))
                {
                    return Convert.ToDouble(leftValue) == Convert.ToDouble(rightValue);
                }

                if (leftValue is string && rightValue is string)
                {
                    return String.Equals(((string)leftValue), ((string)rightValue));
                }

                if ((leftValue is string && (IsRealType(rightValue) || IsIntegerType(rightValue)))
                    || (rightValue is string && (IsRealType(leftValue) || IsIntegerType(leftValue))))
                {
                    return String.Equals(leftValue.ToString(), rightValue.ToString());
                }

                if (leftValue is bool && rightValue is bool)
                {
                    return (bool)leftValue == (bool)rightValue;
                }

                object leftBool = IsBooleanValue(leftValue);
                object rightBool = IsBooleanValue(rightValue);

                if (IsIntegerType(leftBool) && IsIntegerType(rightBool))
                {
                    return Convert.ToInt64(leftBool) == Convert.ToInt64(rightBool);
                }

                return left.Equals(right);
            }

            public static bool operator !=(GenericKey left, object right)
            {
                if (left is GenericKey && right is GenericKey)
                {
                    return !((GenericKey)left == (GenericKey)right);
                }

                if (left is GenericKey && !(right is GenericKey))
                {
                    return !((GenericKey)left == new GenericKey(right));
                }

                if (!(left is GenericKey) && right is GenericKey)
                {
                    return !(new GenericKey(left) == (GenericKey)left);
                }

                return !(new GenericKey(left) == new GenericKey(right));
            }

            public static bool operator >(GenericKey left, object right)
            {
                // Jesli left jest nullem, to niezaleznie od tego czy right jest nullem czy nie
                // i tak nie jest wiekszy.
                if ((object)left == null)
                {
                    return false;
                }

                // Tutaj left nie jest nullem, wiec jesli right jest nullem to left
                // jest wiekszy.
                if (right == null)
                {
                    return true;
                }

                object leftValue = left;
                object rightValue = right;

                leftValue = (leftValue as GenericKey).KeyValue;
                if (rightValue is GenericKey) rightValue = (rightValue as GenericKey).KeyValue;

                // ==============================================================

                // Najczesciej wystapi sytuacja, ze jedna z wartosci
                // to long lub int wiec najpierw sprawdzamy bez kombinatoryki
                // kombinacje tych typow.
                if (leftValue is long && rightValue is long)
                {
                    return (long)leftValue > (long)rightValue;
                }
                else if (leftValue is long && rightValue is int)
                {
                    return (long)leftValue > (int)rightValue;
                }
                else if (leftValue is int && rightValue is long)
                {
                    return (int)leftValue > (long)rightValue;
                }
                else if (leftValue is int && rightValue is int)
                {
                    return (int)leftValue > (int)rightValue;
                }

                // No dobra cos nie wyszlo z wczesniejszymi porownaniami
                // wiec szukamy faktycznych typow obu wartosci.
                if (IsIntegerType(leftValue) && IsIntegerType(rightValue))
                {
                    return Convert.ToInt64(leftValue) > Convert.ToInt64(rightValue);
                }

                if (IsRealType(leftValue) && IsRealType(rightValue))
                {
                    return Convert.ToDouble(leftValue) > Convert.ToDouble(rightValue);
                }

                if ((IsIntegerType(leftValue) && IsRealType(rightValue))
                    || (IsRealType(leftValue) && IsIntegerType(rightValue)))
                {
                    return Convert.ToDouble(leftValue) > Convert.ToDouble(rightValue);
                }

                // Porównanie stringów operatorem > nie jest do końca możliwe
                // więc używamy do takiego porównania GetHashCode.
                if (leftValue is string && rightValue is string)
                {
                    return ((string)leftValue).GetHashCode() > ((string)rightValue).GetHashCode();
                }

                if ((leftValue is string && (IsRealType(rightValue) || IsIntegerType(rightValue)))
                    || (rightValue is string && (IsRealType(leftValue) || IsIntegerType(leftValue))))
                {
                    return leftValue.ToString().GetHashCode() > rightValue.ToString().GetHashCode();
                }

                // W tym przypadku lewo jest wieksze tylko wtedy
                // kiedy jest true, a right jest false.
                if (leftValue is bool && rightValue is bool)
                {
                    return (bool)leftValue && !(bool)rightValue;
                }

                object leftBool = IsBooleanValue(leftValue);
                object rightBool = IsBooleanValue(rightValue);

                if (IsIntegerType(leftBool) && IsIntegerType(rightBool))
                {
                    return Convert.ToInt64(leftBool) > Convert.ToInt64(rightBool);
                }

                // TEORETYCZNIE, nigdy nie powinien tutaj dojsc.
                // Operator > jest uzywany glownie z PK ktory zazwyczaj jest
                // liczba calkowita.
                throw new ArgumentException(string.Format("Could not perform \"greater than\" operator comparison. Types not supported. Left: {0} {1}, Right: {2} {3}",
                    leftValue == null ? "<null>" : leftValue.GetType().ToString(),
                    leftValue == null ? "<null>" : leftValue.ToString(),
                    rightValue == null ? "<null>" : rightValue.GetType().ToString(),
                    rightValue == null ? "<null>" : rightValue.ToString()));
            }

            public static bool operator <(GenericKey left, object right)
            {
                // Jesli left jest nullem, to niezaleznie od tego czy right jest nullem czy nie
                // i tak nie jest wiekszy.
                if ((object)left == null)
                {
                    return false;
                }

                // Tutaj left nie jest nullem, wiec jesli right jest nullem to left
                // jest wiekszy.
                if (right == null)
                {
                    return true;
                }

                object leftValue = left;
                object rightValue = right;

                leftValue = (leftValue as GenericKey).KeyValue;
                if (rightValue is GenericKey) rightValue = (rightValue as GenericKey).KeyValue;

                // ==============================================================

                // Najczesciej wystapi sytuacja, ze jedna z wartosci
                // to long lub int wiec najpierw sprawdzamy bez kombinatoryki
                // kombinacje tych typow.
                if (leftValue is long && rightValue is long)
                {
                    return (long)leftValue < (long)rightValue;
                }
                else if (leftValue is long && rightValue is int)
                {
                    return (long)leftValue < (int)rightValue;
                }
                else if (leftValue is int && rightValue is long)
                {
                    return (int)leftValue < (long)rightValue;
                }
                else if (leftValue is int && rightValue is int)
                {
                    return (int)leftValue < (int)rightValue;
                }

                // No dobra cos nie wyszlo z wczesniejszymi porownaniami
                // wiec szukamy faktycznych typow obu wartosci.
                if (IsIntegerType(leftValue) && IsIntegerType(rightValue))
                {
                    return Convert.ToInt64(leftValue) < Convert.ToInt64(rightValue);
                }

                if (IsRealType(leftValue) && IsRealType(rightValue))
                {
                    return Convert.ToDouble(leftValue) < Convert.ToDouble(rightValue);
                }

                if ((IsIntegerType(leftValue) && IsRealType(rightValue))
                    || (IsRealType(leftValue) && IsIntegerType(rightValue)))
                {
                    return Convert.ToDouble(leftValue) < Convert.ToDouble(rightValue);
                }

                // Porównanie stringów operatorem < nie jest do końca możliwe
                // więc używamy do takiego porównania GetHashCode.
                if (leftValue is string && rightValue is string)
                {
                    return ((string)leftValue).GetHashCode() < ((string)rightValue).GetHashCode();
                }

                if ((leftValue is string && (IsRealType(rightValue) || IsIntegerType(rightValue)))
                    || (rightValue is string && (IsRealType(leftValue) || IsIntegerType(leftValue))))
                {
                    return leftValue.ToString().GetHashCode() < rightValue.ToString().GetHashCode();
                }

                // W tym przypadku prawo jest wieksze tylko wtedy
                // kiedy jest true, a lewo jest false.
                if (leftValue is bool && rightValue is bool)
                {
                    return !(bool)leftValue && (bool)rightValue;
                }

                object leftBool = IsBooleanValue(leftValue);
                object rightBool = IsBooleanValue(rightValue);

                if (IsIntegerType(leftBool) && IsIntegerType(rightBool))
                {
                    return Convert.ToInt64(leftBool) < Convert.ToInt64(rightBool);
                }

                // TEORETYCZNIE, nigdy nie powinien tutaj dojsc.
                // Operator < jest uzywany glownie z PK ktory zazwyczaj jest
                // liczba calkowita.
                throw new ArgumentException(string.Format("Could not perform \"lower than\" operator comparison. Types not supported. Left: {0} {1}, Right: {2} {3}",
                    leftValue == null ? "<null>" : leftValue.GetType().ToString(),
                    leftValue == null ? "<null>" : leftValue.ToString(),
                    rightValue == null ? "<null>" : rightValue.GetType().ToString(),
                    rightValue == null ? "<null>" : rightValue.ToString()));
            }

            private static bool IsIntegerType(object o)
            {
                switch (Type.GetTypeCode(o.GetType()))
                {
                    case TypeCode.Byte:
                    case TypeCode.SByte:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                        return true;
                    default:
                        return false;
                }
            }

            private static bool IsRealType(object o)
            {
                switch (Type.GetTypeCode(o.GetType()))
                {
                    case TypeCode.Decimal:
                    case TypeCode.Double:
                    case TypeCode.Single:
                        return true;
                    default:
                        return false;
                }
            }

            private static object IsBooleanValue(object value)
            {
                if (value != null && !String.IsNullOrEmpty(value.ToString()))
                {
                    if (String.Equals(value.ToString().Trim().ToUpper(), "CHECKED")
                        || String.Equals(value.ToString().Trim().ToUpper(), "TRUE")
                        || String.Equals(value.ToString().Trim().ToUpper(), "TAK")
                        || String.Equals(value.ToString().Trim().ToUpper(), "1"))
                        value = 1;

                    if (String.Equals(value.ToString().Trim().ToUpper(), "UNCHECKED")
                        || String.Equals(value.ToString().Trim().ToUpper(), "FALSE")
                        || String.Equals(value.ToString().Trim().ToUpper(), "NIE")
                        || String.Equals(value.ToString().Trim().ToUpper(), "0"))
                        value = 0;

                    int convTmp = 0;
                    if (!int.TryParse(value.ToString(), out convTmp))
                        value = 0;
                }

                return value;
            }
        }

        #endregion
    }
}
