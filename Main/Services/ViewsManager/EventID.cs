﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.ViewsManager
{
    public class EventID
    {
        #region ViewsManager [1000-1999]
        public class ViewsManager
        {
            #region Base logs [1000-1049]

            public static readonly LogData ModuleUp = new LogData(1000, LogLevel.Info, "Module {0} started at {1}.");
            public static readonly LogData ModuleUpError = new LogData(1001, LogLevel.Fatal, "Initialization failed. Exception: '{0}'.");
            public static readonly LogData ModuleDown = new LogData(1002, LogLevel.Info, "Module {0} stopped at {1}.");
            public static readonly LogData DBConnectionCreated = new LogData(1003, LogLevel.Info, "Successfully created connection to {0} server {1} database.");
            public static readonly LogData DataProviderCreated = new LogData(1004, LogLevel.Info, "Successfully created DataProvider.");
            public static readonly LogData DBCollectorConnectionOpenStart = new LogData(1005, LogLevel.Info, "DBCollector connection open start. IdImrServer: {0}, Description: {1}.");
            public static readonly LogData DBCollectorConnectionOpenEnd = new LogData(1006, LogLevel.Info, "DBCollector connection open end. IdImrServer: {0}, Description: {1}.");
            public static readonly LogData IMRServerMissingDBCollectorAddress = new LogData(1007, LogLevel.Warn, "Sever {0} (ID: {1}) has no DBCollector address defined.");
            public static readonly LogData DBCollectorConnectionUnexpectedError = new LogData(1008, LogLevel.Error, "DBCollector connection unexpected error occured. IdImrServer: {0}, Description: {1}.");
            public static readonly LogData DBCollectorConnectionFailed = new LogData(1009, LogLevel.Error, "DBCollector connection failed. IdImrServer: {0}, Description: {1}, SessionStatus: {2}, Details: {3}.");
            public static readonly LogData ReceivedMessage = new LogData(1010, LogLevel.Trace, "Received message. MessageCount: {0}, BytesCount: {1}, Time: {2}[ms], Server: {3}, Guid: {4}.");
            public static readonly LogData UnsupportedMessageType = new LogData(1011, LogLevel.Warn, "Unsupported message type {0}.");
            public static readonly LogData UnsupportedMessageContentType = new LogData(1012, LogLevel.Warn, "Unsupported message content type. MessageType {0}, ContentType: {1}.");
            public static readonly LogData MessageReceiveTime = new LogData(1013, LogLevel.Trace, "Message receive. ElementsCount: {0}, BytesCount: {1}, MessageType: {2}, ObjectType: {3}, Time: {4}[ms].");
            public static readonly LogData UnknownActionType = new LogData(1014, LogLevel.Warn, "Proceed Action - unknown type. Type: {0}.");
            public static readonly LogData UnsupportedTable = new LogData(1015, LogLevel.Warn, "Unsupported table. Table: {0}.");
            public static readonly LogData PreparedActionUpdateClause = new LogData(1016, LogLevel.Trace, "Prepared Action update clause: {0}.");
            public static readonly LogData UnsupportedDataTypeClass = new LogData(1017, LogLevel.Warn, "Unsupported DataTypeClass: {0}.");
            public static readonly LogData MissingTableConfiguration = new LogData(1018, LogLevel.Warn, "Missing table configuration. Table: {0}.");
            public static readonly LogData MissingKeyColumns = new LogData(1019, LogLevel.Warn, "Missing key columns. View: {0} in context of table: {1}.");
            public static readonly LogData UnknownHelperDataType = new LogData(1020, LogLevel.Warn, "Unknown helper data type for column: {0}, table: {1}.");
            public static readonly LogData PreparedChangeInTableClause = new LogData(1021, LogLevel.Trace, "Prepared ChangeInTable {0} clause: {1}.");
            public static readonly LogData ChangeInTableContent = new LogData(1022, LogLevel.Trace, "ChangeInTable content. Table: {0}, Content: {1}.");
            public static readonly LogData ProceedViewOperationException = new LogData(1023, LogLevel.Error, "Proceed view operation exception. Message: {0}, StackTrace: {1}.");
            public static readonly LogData ProceedActionMessageException = new LogData(1024, LogLevel.Error, "Proceed Action message exception. Message: {0}, StackTrace: {1}.");
            public static readonly LogData ProceedChangeInTableMessageException = new LogData(1025, LogLevel.Error, "Proceed ChangeInTable message exception. Message: {0}, StackTrace: {1}.");
            public static readonly LogData FailedViewOperation = new LogData(1026, LogLevel.Error, "Failed view operation: {0}.");
            public static readonly LogData ViewsConfigurationLoaded = new LogData(1027, LogLevel.Info, "Views configuration loaded. View count: {0}.");
            public static readonly LogData ReceivedMessageViewNotSynchronizable = new LogData(1028, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData ProceedMessageViewNotSynchronizable = new LogData(1029, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData ProceedMessageViewHasUnknownDatabaseType = new LogData(1030, LogLevel.Warn, "The view Id: {0}, Name: {1}, IdServer: {2}; Unknown database type. Messages for this view will be skipped.");
            public static readonly LogData ExceptionOccured = new LogData(1031, LogLevel.Error, "An exception has occured: {0}");

            #endregion

            #region InitializeView [1050-1099]

            public static readonly LogData InitializeViewUnknownTable = new LogData(1050, LogLevel.Warn, "InitializeView - Unknown table: {0}");
            public static readonly LogData InitializeViewMissingPrimaryKeyColumn = new LogData(1051, LogLevel.Error, "InitializeView - Missing primary key column for view: {0}");            
            public static readonly LogData InitializeViewException = new LogData(1052, LogLevel.Error, "Exception occured during ViewInitialize: {0}");
            public static readonly LogData InitializeViewStarted = new LogData(1053, LogLevel.Trace, "InitializeView - Started, View: {0}, Server: {1}");
            public static readonly LogData InitializeViewViewPrimaryKeyColumn = new LogData(1054, LogLevel.Trace, "InitializeView - Primary key column: {0}");
            public static readonly LogData InitializeViewPrimaryKeyTable = new LogData(1055, LogLevel.Trace, "InitializeView - Primary key table: {0}");
            public static readonly LogData InitializeViewProceedingDataColumnKeyTable = new LogData(1056, LogLevel.Trace, "Keyed table is: table: {0}, idDataType: {1}");            
            public static readonly LogData InitializeViewIsColumnImrServer = new LogData(1057, LogLevel.Trace, "InitializeView - Is column ImrServer: {0}");
            public static readonly LogData InitializeViewProceedColumnDataColumnIsKeyButAnotherTableIsAnalzyed = new LogData(1058, LogLevel.Trace, "InitializeView - Proceeding column: {0} ({1}) column is key but another table is analyzed ({2})");
            public static readonly LogData InitializeViewProceedColumnDataColumnIsImrServer = new LogData(1059, LogLevel.Trace, "InitializeView - Procceding column, column is imr server: {0} ({1})");
            public static readonly LogData InitializeViewProceedColumnDataEnd = new LogData(1060, LogLevel.Trace, "InitializeView - Procced column {0} ({1}) end");
            public static readonly LogData InitializeViewJoinTableValues = new LogData(1061, LogLevel.Trace, "InitializeView - Join tables - join values, source table: {0}, joined table: {1}, source keys: {2}, joined keys: {3}");
            public static readonly LogData InitializeViewJoinTablesAssociativeTable = new LogData(1062, LogLevel.Trace, "InitializeView - Join tables - associative table is: {0}");
            public static readonly LogData InitializeViewJoinTablesResult = new LogData(1063, LogLevel.Trace, "InitializeView - Join tables - result: {0}");
            public static readonly LogData InitializeViewMergeTableDictionaries = new LogData(1064, LogLevel.Trace, "InitializeView - Merge tables dictionaries: source: {0}, joined: {1}");
            public static readonly LogData InitializeViewMergeTableJoinedItems = new LogData(1065, LogLevel.Trace, "InitializeView - Merge tables joined items count: {0}");
            public static readonly LogData InitializeViewMergeTableDictionariesResult = new LogData(1066, LogLevel.Trace, "InitializeView - Merge table dictionaries result: {0}");
            public static readonly LogData InitializeViewProceedingDataColumn = new LogData(1067, LogLevel.Trace, "Proceeding DATA column, name: {0}, key: {1}, idx: {2}, table: {3}");
            public static readonly LogData InitializeViewProceedingDataColumnColumnIsKey = new LogData(1068, LogLevel.Trace, "Data column is key: {0}");
            public static readonly LogData InitializeViewProceedingDataColumnColumnIndex = new LogData(1069, LogLevel.Trace, "Column index: {0}");
            public static readonly LogData InitializeViewProceedingDataColumnFinished = new LogData(1070, LogLevel.Trace, "Proceeding DATA column finished, name: {0}, table: {1}");
            public static readonly LogData InitializeViewFinished = new LogData(1071, LogLevel.Trace, "InitializeView - Finished, View: {0}, Server: {1}, ViewOperation count: {2}, Time: {3,16:#####0.##########} [s]");
            public static readonly LogData InitializeViewViewNotSynchronizable = new LogData(1072, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");

            public static readonly LogData InitializeViewProceedColumnDataByReflection = new LogData(1073, LogLevel.Trace, "The column {0} (Table: {1}) in view {2} is initialized by reflection.");
            public static readonly LogData InitializeViewProceedColumnDataByReflectionMissingReflectedType = new LogData(1074, LogLevel.Warn, "Missing reflected type in dictionary. The type will be reflected now: {0}");
            public static readonly LogData InitializeViewProceedColumnDataByReflectionMissingReflectedFieldForType = new LogData(1075, LogLevel.Trace, "Missing reflected field {0} for type {1}. Tried to reflect missing field, result: {2}");
            public static readonly LogData InitializeViewProceedColumnDataByReflectionSettingFieldValueForItem = new LogData(1076, LogLevel.Trace, "Setting field value for column {0} (View: {1}, Type: {2}, Table: {3}, IdDataType: {4}) for object id: {5}. The value is: {6}");

            public static readonly LogData InitializeViewProceedColumnDataColumnIsExtendedHelper = new LogData(1077, LogLevel.Trace, "InitializeView - Proceeding column: {0} ({1}) column is key but another table is analyzed ({2})");
            public static readonly LogData InitializeViewProceedColumnDataGetDataException = new LogData(1078, LogLevel.Fatal, "Failed to get data for column: {0}, table: {1}, Exception: {2}");

            public static readonly LogData InitializeViewProceedColumnDataDataListCount = new LogData(1079, LogLevel.Trace, "ProceedColumnData dataList count: {0}");

            #endregion

            #region ProceedViewInitialization [1100-1119]

            public static readonly LogData ProceedViewInitializationUnknownTable = new LogData(1100, LogLevel.Warn, "InitializeView - Unknown table: {0}");
            public static readonly LogData ProceedViewInitializationBeginColumnAnalyze = new LogData(1101, LogLevel.Trace, "InitializeView - Begin column analyze, columns to proceed count: {0}");
            public static readonly LogData ProceedViewInitializationEndColumnAnalyze = new LogData(1102, LogLevel.Trace, "InitializeView - End column analyze, result: {0}");
            public static readonly LogData ProceedViewInitializationException = new LogData(1103, LogLevel.Error, "Exception occured during ViewInitialize: {0}");
            public static readonly LogData ProceedViewInitializationInitializeColumn = new LogData(1104, LogLevel.Trace, "InitializeView - Initialize column, view: {0}, column: {1}");
            public static readonly LogData ProceedViewInitializationMergingType = new LogData(1105, LogLevel.Trace, "InitializeView - Merging type: {0}");
            public static readonly LogData ProceedViewInitializationMissingParentViewColumnOrDependantColumns = new LogData(1106, LogLevel.Error, "Parent ViewColumn exists: {0}, Dependant ViewColumns dict exists: {1}");
            public static readonly LogData ProceedViewInitializationMissingReferenceTypeForParentColumn = new LogData(1107, LogLevel.Error, "Missing reference type for parent column: IdViewColumn: {0}, IdDataType: {1}");
            public static readonly LogData ProceedViewInitializationProceedingColumn = new LogData(1108, LogLevel.Trace, "InitializeView - Proceeding column, column: {0}, key: {1}, idx: {2}, table: {3}, duration: {4:0.00}[s]");
            public static readonly LogData ProceedViewInitializationProceedingDataColumnKeyTable = new LogData(1109, LogLevel.Trace, "Keyed table is: table: {0}, idDataType: {1}");
            public static readonly LogData ProceedViewInitializationProceedingKeyColumn = new LogData(1110, LogLevel.Trace, "InitializeView - Proceeding key column, name: {0}, table: {1}");
            public static readonly LogData ProceedViewInitializationViewNotSynchronizable = new LogData(1111, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");

            #endregion

            #region ViewOperation [1120-1129]

            public static readonly LogData ViewOperationQueueState = new LogData(1120, LogLevel.Trace, "ViewOperationQueue current length: {0}");
            public static readonly LogData ViewOperationsToProceedCount = new LogData(1121, LogLevel.Trace, "Operations to proceed count: {0}");
            public static readonly LogData ViewOperationToProceed = new LogData(1122, LogLevel.Trace, "Operation to proceed: [{0}]: {1}");

            #endregion

            #region DBCollector [1130-1149]

            public static readonly LogData DBCollectorKeepAliveException = new LogData(1130, LogLevel.Error, "DBCollector keep alive exception: {0}");
            public static readonly LogData DBCollectorIsConnectedException = new LogData(1131, LogLevel.Error, "DBCollector is connected exception: {0}");
            public static readonly LogData DBCollectorNotConnected = new LogData(1132, LogLevel.Trace, "DBCollector not connected: Client: {0}, Session: {1}, State: {2}, Exception occured: {3}");
            public static readonly LogData DBCollectorReconnectInfo = new LogData(1133, LogLevel.Trace, "DBCollector reconnect info: {0}");
            public static readonly LogData DBCollectorCloseError = new LogData(1134, LogLevel.Trace, "DBCollector close exception: {0}");
            public static readonly LogData DBCollectorClosed = new LogData(1135, LogLevel.Error, "DBCollector closed connection.");
            public static readonly LogData DBCollectorOnStopException = new LogData(1136, LogLevel.Warn, "DBCollector exception during OnStop: {0}");
            public static readonly LogData DBCollectorReconnectExcepion = new LogData(1137, LogLevel.Error, "DBCollector exception during reconnect: {0}");
            public static readonly LogData DBCollectorLoggedOut = new LogData(1138, LogLevel.Error, "DBCollector logged out, session guid: {0}, status: {1}");
            public static readonly LogData DBCollectorConnectionException = new LogData(1139, LogLevel.Error, "DBCollector connection exception: {0}");

            #endregion

            #region Verify views [1150-1179]

            public static readonly LogData VerifingViews = new LogData(1150, LogLevel.Info, "Verifing views, count: {0}");
            public static readonly LogData ViewsVerified = new LogData(1151, LogLevel.Info, "Views verified");
            public static readonly LogData VerifingView = new LogData(1152, LogLevel.Trace, "Verifing view: {0}, {1}");
            public static readonly LogData VerifyViewsException = new LogData(1153, LogLevel.Error, "Exception during view creation: {0}");
            public static readonly LogData GetViewsToBuildViewExists = new LogData(1154, LogLevel.Trace, "View exists: {0}, {1}");
            public static readonly LogData GetViewsToBuildExistsMissingColumns = new LogData(1155, LogLevel.Trace, "View's missing columns: {0}, Exists: {1}, count: {2}");
            public static readonly LogData BuildViewsCreatingView = new LogData(1156, LogLevel.Trace, "Creating view: {0}");
            public static readonly LogData VerifyViewInitializingView = new LogData(1157, LogLevel.Trace, "Initializing view: {0}");
            public static readonly LogData VerifyViewInitializingViewFinished = new LogData(1158, LogLevel.Trace, "Initializing view finished: {0}");
            public static readonly LogData BuildViewsInitializingViewColumns = new LogData(1159, LogLevel.Trace, "Initializing view's missing columns: {0}, columns: {1}");
            public static readonly LogData VerifyViewInitializingViewColumnsFinished = new LogData(1160, LogLevel.Trace, "Initializing view's missing columns finished for view: {0}");
            public static readonly LogData VerifyViewsViewNotSynchronizable = new LogData(1161, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData GetViewsToBuildDatabaseTypeNotSupported = new LogData(1162, LogLevel.Trace, "The view Id: {0}, Name: {1}, Database type: {2} is not supoorted.");
            public static readonly LogData GetViewsToBuildDatabaseTypeIsUnknownSettingDefault = new LogData(1163, LogLevel.Trace, "The view Id: {0}, Name: {1}, Database type: {2} has been set as default value because the database type was Unknown");

            #endregion

            #region ProceedViewChange [1180-1189]

            public static readonly LogData ProceedViewChangeException = new LogData(1180, LogLevel.Error, "Proceed view change exception. Exception: {0}.");
            public static readonly LogData ProceedViewChangeViewNotSynchronizable = new LogData(1181, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");

            #endregion
            
            #region ProccedViewCreation [1190-1199]

            public static readonly LogData ProceedViewCreationException = new LogData(1190, LogLevel.Error, "Exception during view creation: {0}, Exception: {1}");
            public static readonly LogData ProceedViewCreationViewsToCreate = new LogData(1191, LogLevel.Trace, "Views to create, Count: {0}, Ids: {1}");
            public static readonly LogData ProceedViewCreationInitializingView = new LogData(1192, LogLevel.Trace, "Initializing view: {0}");
            public static readonly LogData ProceedViewCreationInitializingViewFinished = new LogData(1193, LogLevel.Trace, "Initializing view finished: {0}");
            public static readonly LogData ProceedViewCreationCreatingView = new LogData(1194, LogLevel.Trace, "Creating view: {0}");
            public static readonly LogData ProceedViewCreationViewNotSynchronizable = new LogData(1195, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");

            #endregion

            #region ProceedExtendedHelper [1200-1249]

            public static readonly LogData ProceedExtendedHelperHelperTimeLocalTimeUsed = new LogData(1200, LogLevel.Trace, "ExtendedHelper: {0}. Could not recognize DataValue type: {1}, Column name: {2}, Object key: {3}, Used DataProvider.DateTimeUtcNow: {4}");

            #endregion

            #region ProceedMessage [1250-1299]

            public static readonly LogData ProceedMessageCommandNullOrWhitespace = new LogData(1250, LogLevel.Trace, "The command is null or empty: {0}");
            public static readonly LogData ProceedMessageViewOperation = new LogData(1251, LogLevel.Trace, "ViewOperation - ViewOperation: {0}, IdView: {1}, IdImrServer: {2}, KeyColumnName: {3}, ObjectId: {4}, CellValues: {5}");
            public static readonly LogData ProceedMessageViewOperationCellValue = new LogData(1252, LogLevel.Trace, "ViewOperation cell values - IdView: {0}, IdImrServer: {1}, ColumnName: {2}, IdDataType: {3}, Index: {4}, Value: {5}");
            public static readonly LogData ProceedMessageMissingViewConfiguration = new LogData(1253, LogLevel.Trace, "Missing view configuration - IdView: {0}");
            public static readonly LogData ProceedMessageNoViewsOrColumnsToRebuild = new LogData(1254, LogLevel.Trace, "No views or no columns to rebuild: IdView: {0}");
            public static readonly LogData ProceedMessageBuildingView = new LogData(1255, LogLevel.Trace, "Building view - IdView: {0}");
            public static readonly LogData ProceedMessageInitializeView = new LogData(1256, LogLevel.Trace, "Initialize view - IdView: {0}");
            public static readonly LogData ProceedMessageViewInitialized = new LogData(1257, LogLevel.Trace, "View initialized - IdView: {0}");
            public static readonly LogData ProceedMessageColumnsToProceed = new LogData(1258, LogLevel.Trace, "Columns to proceed - IdView: {0}, Count: {1}");
            public static readonly LogData ProceedMessageInvalidViewOperation = new LogData(1259, LogLevel.Trace, "Invalid view operation: {0}");
            public static readonly LogData ProceedMessageViewDoesNotExistsCantRebuildColumn = new LogData(1260, LogLevel.Trace, "Can't rebuild column - View {0} ({1}) does not exists");
            public static readonly LogData ProceedMessageNoColumnsToRebuildInMessage = new LogData(1261, LogLevel.Trace, "No columns to rebuild exists in message - View {0} ({1})");
            public static readonly LogData ProceedMessageCantRebuildColumnsMissingInViewColumn = new LogData(1262, LogLevel.Trace, "Cant rebuild column - missing columns in VIEW_COLUMN - View {0} ({1})");
            public static readonly LogData ProceedMessageFailedToDeserializeCommand = new LogData(1263, LogLevel.Error, "Failed to deserialize command: {0}");
            public static readonly LogData ProceedMessageCommandException = new LogData(1264, LogLevel.Error, "Proceed command message exception: {0}");

            #endregion

            #region DeleteNonExistingRows [1300-1349]

            public static LogData DeleteNonExistingRowsEnterFunction = new LogData(1300, LogLevel.Trace, "Enter function");
            public static LogData DeleteNonExistingRowsAnalyzingView = new LogData(1301, LogLevel.Trace, "Analyzing view: {0}");
            public static LogData DeleteNonExistingRowsAnalyzingViewMissingPKColumns = new LogData(1302, LogLevel.Trace, "Missing PK columns for view {0}, PK: {1}, ImrServer: {2}");
            public static LogData DeleteNonExistingRowsViewPrimaryKeys = new LogData(1303, LogLevel.Trace, "Primary keys from view count: {0}");
            public static LogData DeleteNonExistingKeysToDeleteFromView = new LogData(1304, LogLevel.Trace, "Keys to delete from view count: {0}");
            public static LogData DeleteNonExistingRowsTablePrimaryKeys = new LogData(1305, LogLevel.Trace, "Primary keys from table count: {0}");
            public static LogData DeleteNonExistingRowsExitFunction = new LogData(1306, LogLevel.Trace, "Exit function");
            public static LogData DeleteNonExistingRowsViewDoesNotExist = new LogData(1307, LogLevel.Trace, "View does not exist. IdView: {0}, Name: {1}");
            
            #endregion

            #region GetKeysFromView [1350-1369]

            public static LogData GetKeysFromViewCouldNotFindMethod = new LogData(1350, LogLevel.Trace, "Could not find method to get keys. Methods count: {0}, Method name: {1}");
            public static LogData GetKeysFromViewFoundMethod = new LogData(1351, LogLevel.Trace, "Get keys method found: {0}");
            public static LogData GetKeysFromViewPrimaryKeyField = new LogData(1351, LogLevel.Trace, "Field name for primary key: {0}, Reflected field: {1}");

            #endregion

            public static readonly LogData ProceedColumnDataViewNotSynchronizable = new LogData(1500, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData ProceedHierarchyColumnViewNotSynchronizable = new LogData(1501, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData GetViewsToBuildNotSynchronizable = new LogData(1502, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData GetViewsToBuildVeryfingView = new LogData(1503, LogLevel.Trace, "Verifing view: {0}, {1}");
            public static readonly LogData BuildViewsViewNotSynchronizable = new LogData(1504, LogLevel.Trace, "The view Id: {0}, Name: {1}, IdServer: {2} is not synchronizable.");
            public static readonly LogData PreparedDataCurrentValueUpdateClause = new LogData(1505, LogLevel.Trace, "Prepared DataCurrentValue update clause: {0}.");
            public static readonly LogData ProceedDataCurrentValueMessageException = new LogData(1506, LogLevel.Error, "Proceed view operation exception: {0}");

            public static readonly LogData FailedToGetDataFromCache = new LogData(1507, LogLevel.Error, "Failed to get data from cache. IdImrServer: {0}, IdReferenceType: {1}, Exception: {2}");

            public static readonly LogData ProceedViewOperationStatement = new LogData(1508, LogLevel.Trace, "ProceedViewOperation statement: {0}");

            public static readonly LogData ViewOperationProceedTaskFinished = new LogData(1509, LogLevel.Fatal, "ViewOperationProceedTask has ended! Status: {0}, Exception: {1}");
            public static readonly LogData ViewOperationProceedTaskFinishedStopEventIsSet = new LogData(1510, LogLevel.Info, "ViewOperationProceedTask has ended and StopEvent flag is set. Everything is fine.");
            public static readonly LogData ViewOperationProceedTaskStarted = new LogData(1511, LogLevel.Info, "ViewOperationProceedTask has started.");
            public static readonly LogData FailedViewOperationIsNull = new LogData(1512, LogLevel.Fatal, "ViewOperationProceedTask failed. The ViewOperation is null!");

            public static readonly LogData ProceedAssociativeTableMoreThanOneMatch = new LogData(1512, LogLevel.Fatal, "More than one match! Table: {0}, Columns: {1}");





            public static readonly LogData ViewsManagerStatus = new LogData(2000, LogLevel.Trace, "ViewsManager status. Queue size: {0}, RAM: {1} [KB], CPU: {2:0.00} [%] | DataProvider dictionary status: {3} | Caches status: {4}");
        }
        #endregion
    }
}
