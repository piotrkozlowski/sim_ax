﻿using IMR.Suite.Common;
using IMR.Suite.Common.MSMQ;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ViewsManager_T
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bCoreToEsDriverAction_Click(object sender, EventArgs e)
        {
            AddLog("CORE to ESBDriver [Action] clicked");
            IMR.Suite.Common.Action action = new IMR.Suite.Common.Action(Enums.ActionType.Unknown, null, null, null);
            //action.
            try
            {
                int actionCnt = Convert.ToInt32(tbCoreToEsbAction.Text);
                if (actionCnt > 1000) actionCnt = 1000;
                AddLog(String.Format("CORE to ESBDriver [Action] sending count: {0}", actionCnt));
                for (int i = 0; i < actionCnt; i++)
                {
                    MSMQ queue = new MSMQ("QueueActionsFromCORE");
                    queue.Send(action);
                }
            }
            catch (Exception ex)
            {
                AddLog(String.Format("CORE to ESBDriver [Action] exception: {0}", ex.Message));
            }
            finally
            {
                AddLog(String.Format("CORE to ESBDriver [Action] sending ended."));
            }
        }


        #region AddLog
        delegate void AddLogCallback(string text);
        private void AddLog(string text)
        {
            try
            {
                if (this.rtbLog.InvokeRequired)
                {
                    AddLogCallback d = new AddLogCallback(AddLog);
                    this.rtbLog.Invoke(d, new object[] { text });
                }
                else
                {
                    this.rtbLog.Text += String.Format("[{0}] ", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    this.rtbLog.Text += text;
                    this.rtbLog.Text += "\r\n";
                    this.rtbLog.SelectionStart = this.rtbLog.Text.Length;
                    this.rtbLog.ScrollToCaret();
                    this.rtbLog.Refresh();
                    //System.Threading.Thread.Sleep(100);
                }
            }
            catch (Exception) { }
        }
        #endregion

        private void bCoreToESBDriverCancheInTable_Click(object sender, EventArgs e)
        {

            AddLog("CORE to ESBDriver [ChangeInTable] clicked");
            ChangeInTable changeInTable = new ChangeInTable(ChangeType.ChangeInDictTable, "TEST");

            try
            {
                int actionCnt = Convert.ToInt32(tbCoreToEsbAction.Text);
                if (actionCnt > 1000) actionCnt = 1000;
                AddLog(String.Format("CORE to ESBDriver [ChangeInTable] sending count: {0}", actionCnt));
                for (int i = 0; i < actionCnt; i++)
                {
                    MSMQ queue = new MSMQ("QueueChangesInTablesFromCtrl");
                    queue.Send(changeInTable);
                }
            }
            catch (Exception ex)
            {
                AddLog(String.Format("CORE to ESBDriver [ChangeInTable] exception: {0}", ex.Message));
            }
            finally
            {
                AddLog(String.Format("CORE to ESBDriver [ChangeInTable] sending ended."));
            }
        }
    }
}
