﻿namespace ViewsManager_T
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bCoreToEsDriverAction = new System.Windows.Forms.Button();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.bCoreToESBDriverCancheInTable = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCoreToEsbAction = new System.Windows.Forms.TextBox();
            this.tbCoreToEsbChangeInTable = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bCoreToEsDriverAction
            // 
            this.bCoreToEsDriverAction.Location = new System.Drawing.Point(12, 12);
            this.bCoreToEsDriverAction.Name = "bCoreToEsDriverAction";
            this.bCoreToEsDriverAction.Size = new System.Drawing.Size(196, 23);
            this.bCoreToEsDriverAction.TabIndex = 0;
            this.bCoreToEsDriverAction.Text = "CORE to ESBDriver [Action]";
            this.bCoreToEsDriverAction.UseVisualStyleBackColor = true;
            this.bCoreToEsDriverAction.Click += new System.EventHandler(this.bCoreToEsDriverAction_Click);
            // 
            // rtbLog
            // 
            this.rtbLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbLog.Location = new System.Drawing.Point(0, 232);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(756, 173);
            this.rtbLog.TabIndex = 1;
            this.rtbLog.Text = "";
            // 
            // bCoreToESBDriverCancheInTable
            // 
            this.bCoreToESBDriverCancheInTable.Location = new System.Drawing.Point(13, 41);
            this.bCoreToESBDriverCancheInTable.Name = "bCoreToESBDriverCancheInTable";
            this.bCoreToESBDriverCancheInTable.Size = new System.Drawing.Size(195, 23);
            this.bCoreToESBDriverCancheInTable.TabIndex = 2;
            this.bCoreToESBDriverCancheInTable.Text = "CORE to ESBDriver [ChangeInTable]";
            this.bCoreToESBDriverCancheInTable.UseVisualStyleBackColor = true;
            this.bCoreToESBDriverCancheInTable.Click += new System.EventHandler(this.bCoreToESBDriverCancheInTable_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "x";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "x";
            // 
            // tbCoreToEsbAction
            // 
            this.tbCoreToEsbAction.Location = new System.Drawing.Point(232, 14);
            this.tbCoreToEsbAction.Name = "tbCoreToEsbAction";
            this.tbCoreToEsbAction.Size = new System.Drawing.Size(45, 20);
            this.tbCoreToEsbAction.TabIndex = 5;
            this.tbCoreToEsbAction.Text = "1";
            // 
            // tbCoreToEsbChangeInTable
            // 
            this.tbCoreToEsbChangeInTable.Location = new System.Drawing.Point(232, 43);
            this.tbCoreToEsbChangeInTable.Name = "tbCoreToEsbChangeInTable";
            this.tbCoreToEsbChangeInTable.Size = new System.Drawing.Size(45, 20);
            this.tbCoreToEsbChangeInTable.TabIndex = 6;
            this.tbCoreToEsbChangeInTable.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 405);
            this.Controls.Add(this.tbCoreToEsbChangeInTable);
            this.Controls.Add(this.tbCoreToEsbAction);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bCoreToESBDriverCancheInTable);
            this.Controls.Add(this.rtbLog);
            this.Controls.Add(this.bCoreToEsDriverAction);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCoreToEsDriverAction;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.Button bCoreToESBDriverCancheInTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCoreToEsbAction;
        private System.Windows.Forms.TextBox tbCoreToEsbChangeInTable;
    }
}

