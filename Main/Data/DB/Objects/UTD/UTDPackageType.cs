﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMR.Suite.Data.DB
{
    public class UTDPackageType
    {
        public int ID_PACKAGE_TYPE_DATA { get; set; }
        public int ID_PACKAGE_TYPE { get; set; }
        public int ID_DATA_TYPE { get; set; }
        public int INDEX_NBR { get; set; }
        public string VALUE { get; set; }
      /*  public string DESCRIPTION { get; set; }
        public string PHOTO_LINK { get; set; }
        public double PRICE { get; set; }
        public string CURRENCY { get; set; }
        public bool VISIBLE_FOR_CUSTOMER { get; set; }
        public bool VISIBLE_FOR_CONSULTANT { get; set; }*/
        
    }
}