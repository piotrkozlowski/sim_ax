﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    public class SLDevicesInSet
    {
        public int IdSet { get; set; }
        public string SerialNbr { get; set; }
    }
}
