﻿using System;

namespace IMR.Suite.Data.DB
{
    public class SLDeviceServiceHistory
    {
        public RowType ROW_TYPE { get; set; }
        public DateTime EVENT_DATE { get; set; }
        public string DESCR { get; set; }
        public string LONG_DESCR { get; set; }
        public DateTime? WARRANTY_DATE { get; set; }

        public enum RowType
        {
            ShippedToClient = 1,
            ApprovedForService = 2,
            ServicePerformed = 3,
            InstalledAtLocation = 4,
            RemoveFromLocation = 5,
            InPackage = 6,
            ReceivedByFitter = 7,
            DeinstalledByFitter = 8,
            RelatedWithTask = 9,
            SimCardAssigned = 10,
            SimCardRemoved = 11,
            SendToServer = 12
        }
    }
}