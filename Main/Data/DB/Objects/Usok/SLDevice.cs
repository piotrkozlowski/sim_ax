﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMR.Suite.Data.DB
{
    public class SLDevice
    {
        public int id_Shipping_list { get; set; }
        public string CONTRACT_NB { get; set; }
        public string SHIPPING_LIST_NBR { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public string SERIAL_NBR { get; set; }
        public string CODE_NAME { get; set; }
        public DateTime? WARANTY_DATE { get; set; }
    }
}