﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMR.Suite.Data.DB
{
    public class SLDeviceParameters
    {
        public long IMR_SERIAL_NBR { get; set; }
        public DateTime? WARRANTY_DATE { get; set; }
        public DateTime? SHIPPING_DATE { get; set; }
    }
}