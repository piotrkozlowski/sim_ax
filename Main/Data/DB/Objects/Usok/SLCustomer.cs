﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    public class SLCustomer
    {
        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StandardDescr { get; set; }
        public int IdDocumentPackage { get; set; }
    }
}
