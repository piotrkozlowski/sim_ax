using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_MODULE_DATA
    {
				public long ID_MODULE_DATA;
				public int ID_MODULE;
				public long ID_DATA_TYPE;
				public object VALUE;
		    
		partial void CloneUser(DB_MODULE_DATA clone);

		public DB_MODULE_DATA(DB_MODULE_DATA clone)
		{
						this.ID_MODULE_DATA = clone.ID_MODULE_DATA;
						this.ID_MODULE = clone.ID_MODULE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_MODULE_DATA()
		{
		}
	}
}