using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_UNIT
    {
        [DataMember]
		public int ID_UNIT;
        [DataMember]
		public int ID_UNIT_BASE;
        [DataMember]
		public Double SCALE;
        [DataMember]
		public Double BIAS;
        [DataMember]
		public string NAME;
        [DataMember]
		public long? ID_DESCR;
		    
		partial void CloneUser(DB_UNIT clone);

		public DB_UNIT(DB_UNIT clone)
		{
						this.ID_UNIT = clone.ID_UNIT;
						this.ID_UNIT_BASE = clone.ID_UNIT_BASE;
						this.SCALE = clone.SCALE;
						this.BIAS = clone.BIAS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_UNIT()
		{
		}
	}
}