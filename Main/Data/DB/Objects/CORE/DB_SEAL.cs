using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SEAL
    {
				public long ID_SEAL;
				public string SERIAL_NBR;
				public string DESCRIPTION;
		    
		partial void CloneUser(DB_SEAL clone);

		public DB_SEAL(DB_SEAL clone)
		{
						this.ID_SEAL = clone.ID_SEAL;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_SEAL()
		{
		}
	}
}