using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK_TYPE
    {
        public long ID_WORK_TYPE;
        public string NAME;
        public string PLUGIN_NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_WORK_TYPE clone);

        public DB_WORK_TYPE(DB_WORK_TYPE clone)
        {
            this.ID_WORK_TYPE = clone.ID_WORK_TYPE;
            this.NAME = clone.NAME;
            this.PLUGIN_NAME = clone.PLUGIN_NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_WORK_TYPE()
        {
        }
    }
}