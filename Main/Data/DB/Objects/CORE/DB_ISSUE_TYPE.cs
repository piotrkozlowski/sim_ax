using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ISSUE_TYPE
    {
				public int ID_ISSUE_TYPE;
				public long? ID_DESCR;
				public string DESCRIPTION;
		    
		partial void CloneUser(DB_ISSUE_TYPE clone);

		public DB_ISSUE_TYPE(DB_ISSUE_TYPE clone)
		{
						this.ID_ISSUE_TYPE = clone.ID_ISSUE_TYPE;
						this.ID_DESCR = clone.ID_DESCR;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_ISSUE_TYPE()
		{
		}
	}
}