using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_STATE_TYPE
    {
		public int ID_LOCATION_STATE_TYPE;
        public string NAME;
        public long? ID_DESCR;
        public string DESCRIPTION;
		    
		partial void CloneUser(DB_LOCATION_STATE_TYPE clone);

		public DB_LOCATION_STATE_TYPE(DB_LOCATION_STATE_TYPE clone)
		{
						this.ID_LOCATION_STATE_TYPE = clone.ID_LOCATION_STATE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_LOCATION_STATE_TYPE()
		{
		}
	}
}