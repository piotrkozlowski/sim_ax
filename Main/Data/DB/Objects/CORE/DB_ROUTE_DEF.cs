using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DEF
    {
				public int ID_ROUTE_DEF;
				public string NAME;
				public string DESCRIPTION;
				public DateTime CREATION_DATE;
				public DateTime EXPIRATION_DATE;
				public DateTime? DELETE_DATE;
				public int ID_ROUTE_TYPE;
				public int ID_ROUTE_STATUS;
				public int ID_OPERATOR;
                public bool AD_HOC;
		    
		partial void CloneUser(DB_ROUTE_DEF clone);

		public DB_ROUTE_DEF(DB_ROUTE_DEF clone)
		{
						this.ID_ROUTE_DEF = clone.ID_ROUTE_DEF;
						this.NAME = clone.NAME;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.EXPIRATION_DATE = clone.EXPIRATION_DATE;
						this.DELETE_DATE = clone.DELETE_DATE;
						this.ID_ROUTE_TYPE = clone.ID_ROUTE_TYPE;
						this.ID_ROUTE_STATUS = clone.ID_ROUTE_STATUS;
						this.ID_OPERATOR = clone.ID_OPERATOR;
                        this.AD_HOC = clone.AD_HOC;
						CloneUser(clone);
		}

		public DB_ROUTE_DEF()
		{
		}
	}
}