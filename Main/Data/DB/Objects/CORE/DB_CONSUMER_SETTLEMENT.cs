using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_SETTLEMENT
    {
				public long ID_CONSUMER_SETTLEMENT;
				public int ID_CONSUMER;
				public int ID_TARIFF_SETTLEMENT_PERIOD;
				public DateTime SETTLEMENT_TIME;
				public int ID_MODULE;
				public int? ID_OPERATOR;
				public int ID_CONSUMER_SETTLEMENT_REASON;
				public int ID_CONSUMER_SETTLEMENT_STATUS;
		    
		partial void CloneUser(DB_CONSUMER_SETTLEMENT clone);

		public DB_CONSUMER_SETTLEMENT(DB_CONSUMER_SETTLEMENT clone)
		{
						this.ID_CONSUMER_SETTLEMENT = clone.ID_CONSUMER_SETTLEMENT;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_TARIFF_SETTLEMENT_PERIOD = clone.ID_TARIFF_SETTLEMENT_PERIOD;
						this.SETTLEMENT_TIME = clone.SETTLEMENT_TIME;
						this.ID_MODULE = clone.ID_MODULE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_CONSUMER_SETTLEMENT_REASON = clone.ID_CONSUMER_SETTLEMENT_REASON;
						this.ID_CONSUMER_SETTLEMENT_STATUS = clone.ID_CONSUMER_SETTLEMENT_STATUS;
						CloneUser(clone);
		}

		public DB_CONSUMER_SETTLEMENT()
		{
		}
	}
}