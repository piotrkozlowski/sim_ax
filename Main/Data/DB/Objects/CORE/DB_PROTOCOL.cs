using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PROTOCOL
    {
				public int ID_PROTOCOL;
				public string NAME;
				public int? ID_PROTOCOL_DRIVER;
		    
		partial void CloneUser(DB_PROTOCOL clone);

		public DB_PROTOCOL(DB_PROTOCOL clone)
		{
						this.ID_PROTOCOL = clone.ID_PROTOCOL;
						this.NAME = clone.NAME;
						this.ID_PROTOCOL_DRIVER = clone.ID_PROTOCOL_DRIVER;
						CloneUser(clone);
		}

		public DB_PROTOCOL()
		{
		}
	}
}