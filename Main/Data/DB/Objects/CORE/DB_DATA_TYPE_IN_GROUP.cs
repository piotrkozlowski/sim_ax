using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DATA_TYPE_IN_GROUP
    {
				public int ID_DATA_TYPE_GROUP;
				public long ID_DATA_TYPE;
		    
		partial void CloneUser(DB_DATA_TYPE_IN_GROUP clone);

		public DB_DATA_TYPE_IN_GROUP(DB_DATA_TYPE_IN_GROUP clone)
		{
						this.ID_DATA_TYPE_GROUP = clone.ID_DATA_TYPE_GROUP;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						CloneUser(clone);
		}

		public DB_DATA_TYPE_IN_GROUP()
		{
		}
	}
}