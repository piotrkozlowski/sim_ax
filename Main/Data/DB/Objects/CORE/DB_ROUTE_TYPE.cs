using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_TYPE
    {
				public int ID_ROUTE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_ROUTE_TYPE clone);

		public DB_ROUTE_TYPE(DB_ROUTE_TYPE clone)
		{
						this.ID_ROUTE_TYPE = clone.ID_ROUTE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ROUTE_TYPE()
		{
		}
	}
}