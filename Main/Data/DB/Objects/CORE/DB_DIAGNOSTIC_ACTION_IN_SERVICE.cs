﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DIAGNOSTIC_ACTION_IN_SERVICE
    {
        public int ID_SERVICE;
        public int ID_DIAGNOSTIC_ACTION;
        public int INDEX_NBR;

        partial void CloneUser(DB_DIAGNOSTIC_ACTION_IN_SERVICE clone);

        public DB_DIAGNOSTIC_ACTION_IN_SERVICE(DB_DIAGNOSTIC_ACTION_IN_SERVICE clone)
        {
            this.ID_SERVICE = clone.ID_SERVICE;
            this.ID_DIAGNOSTIC_ACTION = clone.ID_DIAGNOSTIC_ACTION;
            this.INDEX_NBR = clone.INDEX_NBR;
            CloneUser(clone);
        }

        public DB_DIAGNOSTIC_ACTION_IN_SERVICE()
        {
        }
    }
}