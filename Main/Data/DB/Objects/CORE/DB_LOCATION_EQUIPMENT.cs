using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_LOCATION_EQUIPMENT
    {
        [DataMember]
        public long ID_LOCATION_EQUIPMENT;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public DateTime START_TIME;
        [DataMember]
		public DateTime? END_TIME;
		    
		partial void CloneUser(DB_LOCATION_EQUIPMENT clone);

		public DB_LOCATION_EQUIPMENT(DB_LOCATION_EQUIPMENT clone)
		{
						this.ID_LOCATION_EQUIPMENT = clone.ID_LOCATION_EQUIPMENT;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_METER = clone.ID_METER;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_LOCATION_EQUIPMENT()
		{
		}
	}
}