﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.Objects.CORE
{
    [Serializable]
    public partial class DB_PACKAGE_STATUS
    {
        public int ID_PACKAGE_STATUS;
        public string PACKAGE_STATUS;
        public long? ID_DESCR;

        partial void CloneUser(DB_PACKAGE_STATUS clone);

        public DB_PACKAGE_STATUS(DB_PACKAGE_STATUS clone)
        {
            this.ID_PACKAGE_STATUS = clone.ID_PACKAGE_STATUS;
            this.PACKAGE_STATUS = clone.PACKAGE_STATUS;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_PACKAGE_STATUS()
        {
        }
    }
}
