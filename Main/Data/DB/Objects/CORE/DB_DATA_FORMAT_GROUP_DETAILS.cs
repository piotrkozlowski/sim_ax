﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DATA_FORMAT_GROUP_DETAILS
    {
        public int ID_DATA_FORMAT_GROUP;
        public long ID_DATA_TYPE;
        public int? ID_DATA_TYPE_FORMAT_IN;
        public int? ID_DATA_TYPE_FORMAT_OUT;
        public int? ID_UNIT_IN;
        public int? ID_UNIT_OUT;

        partial void CloneUser(DB_DATA_FORMAT_GROUP_DETAILS clone);

        public DB_DATA_FORMAT_GROUP_DETAILS(DB_DATA_FORMAT_GROUP_DETAILS clone)
        {
            this.ID_DATA_FORMAT_GROUP = clone.ID_DATA_FORMAT_GROUP;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.ID_DATA_TYPE_FORMAT_IN = clone.ID_DATA_TYPE_FORMAT_IN;
            this.ID_DATA_TYPE_FORMAT_OUT = clone.ID_DATA_TYPE_FORMAT_OUT;
            this.ID_UNIT_IN = clone.ID_UNIT_IN;
            this.ID_UNIT_OUT = clone.ID_UNIT_OUT;
            CloneUser(clone);
        }

        public DB_DATA_FORMAT_GROUP_DETAILS()
        {
        }
    }
}