using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SESSION_STATUS
    {
				public int ID_SESSION_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_SESSION_STATUS clone);

		public DB_SESSION_STATUS(DB_SESSION_STATUS clone)
		{
						this.ID_SESSION_STATUS = clone.ID_SESSION_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_SESSION_STATUS()
		{
		}
	}
}