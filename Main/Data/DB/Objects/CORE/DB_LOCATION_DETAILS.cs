﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_LOCATION_DETAILS
    {
        public long ID_LOCATION_DETAILS;
        public long ID_LOCATION;
        public string CID;
        public string NAME;
        public string CITY;
        public string ADDRESS;
        public string ZIP;
        public string COUNTRY;
        public string LATITUDE;
        public string LONGITUDE;
        public int? IMR_SERVER;
        public DateTime? CREATION_DATE;
        public string LOCATION_MEMO;
        public long? LOCATION_SOURCE_SERVER_ID;
        public int ID_DISTRIBUTOR;
        public int ID_LOCATION_TYPE;
        public int ID_LOCATION_STATE_TYPE;
        public bool IN_KPI;
        public bool ALLOW_GROUPING;

        partial void CloneUser(DB_LOCATION_DETAILS clone);

        public DB_LOCATION_DETAILS(DB_LOCATION_DETAILS clone)
        {
            this.ID_LOCATION_DETAILS = clone.ID_LOCATION_DETAILS;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.CID = clone.CID;
            this.NAME = clone.NAME;
            this.CITY = clone.CITY;
            this.ADDRESS = clone.ADDRESS;
            this.ZIP = clone.ZIP;
            this.COUNTRY = clone.COUNTRY;
            this.LATITUDE = clone.LATITUDE;
            this.LONGITUDE = clone.LONGITUDE;
            this.IMR_SERVER = clone.IMR_SERVER;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.LOCATION_MEMO = clone.LOCATION_MEMO;
            this.LOCATION_SOURCE_SERVER_ID = clone.LOCATION_SOURCE_SERVER_ID;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_LOCATION_TYPE = clone.ID_LOCATION_TYPE;
            this.ID_LOCATION_STATE_TYPE = clone.ID_LOCATION_STATE_TYPE;
            this.IN_KPI = clone.IN_KPI;
            this.ALLOW_GROUPING = clone.ALLOW_GROUPING;
            CloneUser(clone);
        }

        public DB_LOCATION_DETAILS()
        {
        }
    }
}