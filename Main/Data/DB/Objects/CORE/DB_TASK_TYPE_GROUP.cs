using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_TASK_TYPE_GROUP
    {
        [DataMember]
				public int ID_TASK_TYPE_GROUP;
        [DataMember]
        public string NAME;
        [DataMember]
        public string DESCR;
        [DataMember]
        public int? ID_TASK_TYPE_GROUP_PARENT;
        [DataMember]
        public int I_LEFT;
        [DataMember]
        public int I_RIGHT;
        public object ROW_VERSION;
		    
		partial void CloneUser(DB_TASK_TYPE_GROUP clone);

		public DB_TASK_TYPE_GROUP(DB_TASK_TYPE_GROUP clone)
		{
						this.ID_TASK_TYPE_GROUP = clone.ID_TASK_TYPE_GROUP;
						this.NAME = clone.NAME;
						this.DESCR = clone.DESCR;
						this.ID_TASK_TYPE_GROUP_PARENT = clone.ID_TASK_TYPE_GROUP_PARENT;
						this.I_LEFT = clone.I_LEFT;
						this.I_RIGHT = clone.I_RIGHT;
						this.ROW_VERSION = clone.ROW_VERSION;
						CloneUser(clone);
		}

		public DB_TASK_TYPE_GROUP()
		{
		}
	}
}