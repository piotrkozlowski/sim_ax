using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTOR_GROUP
    {
				public int ID_ACTOR_GROUP;
				public string NAME;
				public bool BUILT_IN;
				public int ID_DISTRIBUTOR;
                public long? ID_DESCR;
		    
		partial void CloneUser(DB_ACTOR_GROUP clone);

		public DB_ACTOR_GROUP(DB_ACTOR_GROUP clone)
		{
						this.ID_ACTOR_GROUP = clone.ID_ACTOR_GROUP;
						this.NAME = clone.NAME;
						this.BUILT_IN = clone.BUILT_IN;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ACTOR_GROUP()
		{
		}
	}
}