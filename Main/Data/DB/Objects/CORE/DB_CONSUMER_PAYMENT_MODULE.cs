using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_PAYMENT_MODULE
    {
				public long ID_CONSUMER_PAYMENT_MODULE;
				public int ID_CONSUMER;
				public int ID_PAYMENT_MODULE;
				public string PAYMENT_SYSTEM_NUMBER;
		    
		partial void CloneUser(DB_CONSUMER_PAYMENT_MODULE clone);

		public DB_CONSUMER_PAYMENT_MODULE(DB_CONSUMER_PAYMENT_MODULE clone)
		{
						this.ID_CONSUMER_PAYMENT_MODULE = clone.ID_CONSUMER_PAYMENT_MODULE;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_PAYMENT_MODULE = clone.ID_PAYMENT_MODULE;
						this.PAYMENT_SYSTEM_NUMBER = clone.PAYMENT_SYSTEM_NUMBER;
						CloneUser(clone);
		}

		public DB_CONSUMER_PAYMENT_MODULE()
		{
		}
	}
}