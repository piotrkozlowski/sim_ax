using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_TEXT_DATA_TYPE
    {
				public long ID_ALARM_TEXT;
				public int ID_LANGUAGE;
				public int ARG_NBR;
				public long ID_DATA_TYPE;
				public int ID_UNIT;
		    
		partial void CloneUser(DB_ALARM_TEXT_DATA_TYPE clone);

		public DB_ALARM_TEXT_DATA_TYPE(DB_ALARM_TEXT_DATA_TYPE clone)
		{
						this.ID_ALARM_TEXT = clone.ID_ALARM_TEXT;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.ARG_NBR = clone.ARG_NBR;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.ID_UNIT = clone.ID_UNIT;
						CloneUser(clone);
		}

		public DB_ALARM_TEXT_DATA_TYPE()
		{
		}
	}
}