using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_FILE
    {
        [DataMember]
        public long ID_FILE;
        [DataMember]
        public string PHYSICAL_FILE_NAME;
        [DataMember]
        public object FILE_BYTES;
        [DataMember]
        public long? SIZE;
        [DataMember]
        public DateTime INSERT_DATE;
        [DataMember]
        public string DESCRIPTION;
        [DataMember]
        public string VERSION;
        [DataMember]
        public DateTime? LAST_DOWNLOADED;
        [DataMember]
        public bool IS_BLOCKED;
		    
		partial void CloneUser(DB_FILE clone);

		public DB_FILE(DB_FILE clone)
		{
						this.ID_FILE = clone.ID_FILE;
						this.PHYSICAL_FILE_NAME = clone.PHYSICAL_FILE_NAME;
						this.FILE_BYTES = clone.FILE_BYTES;
						this.SIZE = clone.SIZE;
						this.INSERT_DATE = clone.INSERT_DATE;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.VERSION = clone.VERSION;
						this.LAST_DOWNLOADED = clone.LAST_DOWNLOADED;
                        this.IS_BLOCKED= clone.IS_BLOCKED;
						CloneUser(clone);
		}

		public DB_FILE()
		{
		}
	}
}