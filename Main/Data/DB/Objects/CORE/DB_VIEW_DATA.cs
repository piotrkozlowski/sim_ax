﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_VIEW_DATA
    {
        public long ID_VIEW_DATA;
        public int ID_VIEW;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_VIEW_DATA clone);

        public DB_VIEW_DATA(DB_VIEW_DATA clone)
        {
            this.ID_VIEW_DATA = clone.ID_VIEW_DATA;
            this.ID_VIEW = clone.ID_VIEW;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_VIEW_DATA()
        {
        }
    }
}