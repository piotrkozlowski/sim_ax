﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_VIEW
    {
        [DataMember]
        public int ID_VIEW;
        [DataMember]
        public string NAME;
        [DataMember]
        public int ID_REFERENCE_TYPE;

        partial void CloneUser(DB_VIEW clone);

        public DB_VIEW(DB_VIEW clone)
        {
            this.ID_VIEW = clone.ID_VIEW;
            this.NAME = clone.NAME;
            this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
            CloneUser(clone);
        }

        public DB_VIEW()
        {
        }
    }
}