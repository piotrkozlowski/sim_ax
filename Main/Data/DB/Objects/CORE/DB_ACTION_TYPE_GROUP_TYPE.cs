using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_TYPE_GROUP_TYPE
    {
				public int ID_ACTION_TYPE_GROUP_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_ACTION_TYPE_GROUP_TYPE clone);

		public DB_ACTION_TYPE_GROUP_TYPE(DB_ACTION_TYPE_GROUP_TYPE clone)
		{
						this.ID_ACTION_TYPE_GROUP_TYPE = clone.ID_ACTION_TYPE_GROUP_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ACTION_TYPE_GROUP_TYPE()
		{
		}
	}
}