using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DEPOSITORY_ELEMENT
    {
        [DataMember]
        public int ID_DEPOSITORY_ELEMENT;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public int? ID_TASK;
        [DataMember]
        public int? ID_PACKAGE;
        [DataMember]
        public long ID_ARTICLE;
        [DataMember]
        public int ID_DEVICE_TYPE_CLASS;
        [DataMember]
        public int ID_DEVICE_STATE_TYPE;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public DateTime START_DATE;
        [DataMember]
        public DateTime? END_DATE;
        [DataMember]
        public bool REMOVED;
        [DataMember]
        public bool ORDERED;
        [DataMember]
        public bool? ACCEPTED;
        [DataMember]
        public string NOTES;
        [DataMember]
        public string EXTERNAL_SN;
        [DataMember]
		public int? ID_REFERENCE;
		    
		partial void CloneUser(DB_DEPOSITORY_ELEMENT clone);

		public DB_DEPOSITORY_ELEMENT(DB_DEPOSITORY_ELEMENT clone)
		{
						this.ID_DEPOSITORY_ELEMENT = clone.ID_DEPOSITORY_ELEMENT;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_TASK = clone.ID_TASK;
						this.ID_PACKAGE = clone.ID_PACKAGE;
						this.ID_ARTICLE = clone.ID_ARTICLE;
						this.ID_DEVICE_TYPE_CLASS = clone.ID_DEVICE_TYPE_CLASS;
						this.ID_DEVICE_STATE_TYPE = clone.ID_DEVICE_STATE_TYPE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.START_DATE = clone.START_DATE;
						this.END_DATE = clone.END_DATE;
						this.REMOVED = clone.REMOVED;
						this.ORDERED = clone.ORDERED;
						this.ACCEPTED = clone.ACCEPTED;
						this.NOTES = clone.NOTES;
						this.EXTERNAL_SN = clone.EXTERNAL_SN;
						this.ID_REFERENCE = clone.ID_REFERENCE;
						CloneUser(clone);
		}

		public DB_DEPOSITORY_ELEMENT()
		{
		}
	}
}