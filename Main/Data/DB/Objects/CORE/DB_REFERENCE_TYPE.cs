using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_REFERENCE_TYPE
    {
				public int ID_REFERENCE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_REFERENCE_TYPE clone);

		public DB_REFERENCE_TYPE(DB_REFERENCE_TYPE clone)
		{
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_REFERENCE_TYPE()
		{
		}
	}
}