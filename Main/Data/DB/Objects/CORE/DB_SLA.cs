using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SLA
    {
				public int ID_SLA;
				public int ID_DESCR;
				public string DESCRIPTION;
				public string ISSUE_RECEIVE_WD;
				public string ISSUE_RECEIVE_FD;
				public string TECHNICAL_SUPPORT_WD;
				public string TECHNICAL_SUPPORT_FD;
				public int? ISSUE_RESPONSE_WD;
				public int? ISSUE_RESPONSE_FD;
				public int? SYSTEM_FAILURE_REMOVE_WD;
				public int? SYSTEM_FAILURE_REMOVE_FD;
				public int? OBJECT_FAILURE_REMOVE_WD;
				public int? OBJECT_FAILURE_REMOVE_FD;
		    
		partial void CloneUser(DB_SLA clone);

		public DB_SLA(DB_SLA clone)
		{
						this.ID_SLA = clone.ID_SLA;
						this.ID_DESCR = clone.ID_DESCR;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.ISSUE_RECEIVE_WD = clone.ISSUE_RECEIVE_WD;
						this.ISSUE_RECEIVE_FD = clone.ISSUE_RECEIVE_FD;
						this.TECHNICAL_SUPPORT_WD = clone.TECHNICAL_SUPPORT_WD;
						this.TECHNICAL_SUPPORT_FD = clone.TECHNICAL_SUPPORT_FD;
						this.ISSUE_RESPONSE_WD = clone.ISSUE_RESPONSE_WD;
						this.ISSUE_RESPONSE_FD = clone.ISSUE_RESPONSE_FD;
						this.SYSTEM_FAILURE_REMOVE_WD = clone.SYSTEM_FAILURE_REMOVE_WD;
						this.SYSTEM_FAILURE_REMOVE_FD = clone.SYSTEM_FAILURE_REMOVE_FD;
						this.OBJECT_FAILURE_REMOVE_WD = clone.OBJECT_FAILURE_REMOVE_WD;
						this.OBJECT_FAILURE_REMOVE_FD = clone.OBJECT_FAILURE_REMOVE_FD;
						CloneUser(clone);
		}

		public DB_SLA()
		{
		}
	}
}