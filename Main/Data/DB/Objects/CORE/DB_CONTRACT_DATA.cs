﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_CONTRACT_DATA
    {
        public long ID_CONTRACT_DATA;
        public int ID_CONTRACT;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_CONTRACT_DATA clone);

        public DB_CONTRACT_DATA(DB_CONTRACT_DATA clone)
        {
            this.ID_CONTRACT_DATA = clone.ID_CONTRACT_DATA;
            this.ID_CONTRACT = clone.ID_CONTRACT;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_CONTRACT_DATA()
        {
        }
    }
}