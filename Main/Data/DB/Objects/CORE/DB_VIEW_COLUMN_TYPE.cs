﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_VIEW_COLUMN_TYPE
    {
        [DataMember]
        public int ID_VIEW_COLUMN_TYPE;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;

        partial void CloneUser(DB_VIEW_COLUMN_TYPE clone);

        public DB_VIEW_COLUMN_TYPE(DB_VIEW_COLUMN_TYPE clone)
        {
            this.ID_VIEW_COLUMN_TYPE = clone.ID_VIEW_COLUMN_TYPE;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_VIEW_COLUMN_TYPE()
        {
        }
    }
}