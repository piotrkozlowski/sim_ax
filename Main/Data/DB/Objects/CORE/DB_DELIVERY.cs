using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DELIVERY
    {
				public int ID_DELIVERY;
				public string NAME;
				public string DESCRIPTION;
				public string UPLOADED_FILE;
				public DateTime CREATION_DATE;
				public int ID_SHIPPING_LIST;
				public int ID_OPERATOR;
		    
		partial void CloneUser(DB_DELIVERY clone);

		public DB_DELIVERY(DB_DELIVERY clone)
		{
						this.ID_DELIVERY = clone.ID_DELIVERY;
						this.NAME = clone.NAME;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.UPLOADED_FILE = clone.UPLOADED_FILE;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						CloneUser(clone);
		}

		public DB_DELIVERY()
		{
		}
	}
}