using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_FAULT_CODE
    {
        public int ID_FAULT_CODE;
        public int ID_FAULT_CODE_TYPE;
        public int? ID_FAULT_CODE_PARENT;
        public string NAME;
        public long? ID_CLIENT_DESCR;
        public long? ID_COMPANY_DESCR;
        public int PRIORITY;
        public string FAULT_CODE_HIERARCHY;

        partial void CloneUser(DB_FAULT_CODE clone);

        public DB_FAULT_CODE(DB_FAULT_CODE clone)
        {
            this.ID_FAULT_CODE = clone.ID_FAULT_CODE;
            this.ID_FAULT_CODE_TYPE = clone.ID_FAULT_CODE_TYPE;
            this.ID_FAULT_CODE_PARENT = clone.ID_FAULT_CODE_PARENT;
            this.NAME = clone.NAME;
            this.ID_CLIENT_DESCR = clone.ID_CLIENT_DESCR;
            this.ID_COMPANY_DESCR = clone.ID_COMPANY_DESCR;
            this.PRIORITY = clone.PRIORITY;
            this.FAULT_CODE_HIERARCHY = clone.FAULT_CODE_HIERARCHY;
            CloneUser(clone);
        }

        public DB_FAULT_CODE()
        {
        }
    }
}