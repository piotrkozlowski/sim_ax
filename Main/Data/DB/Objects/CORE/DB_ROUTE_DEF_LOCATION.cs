using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DEF_LOCATION
    {
				public long ID_ROUTE_DEF_LOCATION;
				public int ID_ROUTE_DEF;
				public long ID_LOCATION;
				public int ORDER_NBR;
		    
		partial void CloneUser(DB_ROUTE_DEF_LOCATION clone);

		public DB_ROUTE_DEF_LOCATION(DB_ROUTE_DEF_LOCATION clone)
		{
						this.ID_ROUTE_DEF_LOCATION = clone.ID_ROUTE_DEF_LOCATION;
						this.ID_ROUTE_DEF = clone.ID_ROUTE_DEF;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ORDER_NBR = clone.ORDER_NBR;
						CloneUser(clone);
		}

		public DB_ROUTE_DEF_LOCATION()
		{
		}
	}
}