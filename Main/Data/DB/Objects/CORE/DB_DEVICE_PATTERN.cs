using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_PATTERN
    {
				public long ID_PATTERN;
				public long SERIAL_NBR;
				public int? ID_DISTRIBUTOR;
				public string NAME;
				public DateTime CREATION_DATE;
				public int CREATED_BY;
				public int ID_TEMPLATE;
                public int? ID_SERVER;
		    
		partial void CloneUser(DB_DEVICE_PATTERN clone);

		public DB_DEVICE_PATTERN(DB_DEVICE_PATTERN clone)
		{
						this.ID_PATTERN = clone.ID_PATTERN;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.NAME = clone.NAME;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.CREATED_BY = clone.CREATED_BY;
						this.ID_TEMPLATE = clone.ID_TEMPLATE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_DEVICE_PATTERN()
		{
		}
	}
}