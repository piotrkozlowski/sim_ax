using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    public class DB_PAYMENT
    {
				public int ID_PAYMENT;
				public int ID_CONSUMER;
				public int ID_OPERATOR;
				public int ID_DISTRIBUTOR;
				public int ID_DISTRIBUTOR_SUPPLIER;
				public DateTime DATE;
				public Double TOTAL_PAYMENT;
		    

		public DB_PAYMENT(DB_PAYMENT clone)
		{
						this.ID_PAYMENT = clone.ID_PAYMENT;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_DISTRIBUTOR_SUPPLIER = clone.ID_DISTRIBUTOR_SUPPLIER;
						this.DATE = clone.DATE;
						this.TOTAL_PAYMENT = clone.TOTAL_PAYMENT;
					}

		public DB_PAYMENT()
		{
		}
	}
}