using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_GROUP
    {
				public int ID_ALARM_GROUP;
				public string NAME;
				public string DESCRIPTION;
				public int ID_DISTRIBUTOR;
                public bool IS_ENABLED;
		    
		partial void CloneUser(DB_ALARM_GROUP clone);

		public DB_ALARM_GROUP(DB_ALARM_GROUP clone)
		{
						this.ID_ALARM_GROUP = clone.ID_ALARM_GROUP;
						this.NAME = clone.NAME;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
                        this.IS_ENABLED = clone.IS_ENABLED;
						CloneUser(clone);
		}

		public DB_ALARM_GROUP()
		{
		}
	}
}