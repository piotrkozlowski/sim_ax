using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DISTRIBUTOR_DATA
    {
        [DataMember]
		public long ID_DISTRIBUTOR_DATA;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
		    
		partial void CloneUser(DB_DISTRIBUTOR_DATA clone);

		public DB_DISTRIBUTOR_DATA(DB_DISTRIBUTOR_DATA clone)
		{
						this.ID_DISTRIBUTOR_DATA = clone.ID_DISTRIBUTOR_DATA;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_DISTRIBUTOR_DATA()
		{
		}
	}
}