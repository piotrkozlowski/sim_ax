using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ACTION_DEF
    {
        [DataMember]
        public long ID_ACTION_DEF;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long? ID_DATA_TYPE;
        [DataMember]
        public int ID_ACTION_TYPE;
        [DataMember]
        public long ID_ACTION_DATA;
        [DataMember]
        public object VALUE;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_ACTION_DEF clone);

		public DB_ACTION_DEF(DB_ACTION_DEF clone)
		{
						this.ID_ACTION_DEF = clone.ID_ACTION_DEF;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.ID_ACTION_TYPE = clone.ID_ACTION_TYPE;
						this.ID_ACTION_DATA = clone.ID_ACTION_DATA;
						this.VALUE = clone.VALUE;
						this.ID_DESCR = clone.ID_DESCR;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_ACTION_DEF()
		{
		}
	}
}