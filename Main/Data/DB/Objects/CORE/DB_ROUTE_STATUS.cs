using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_STATUS
    {
				public int ID_ROUTE_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_ROUTE_STATUS clone);

		public DB_ROUTE_STATUS(DB_ROUTE_STATUS clone)
		{
						this.ID_ROUTE_STATUS = clone.ID_ROUTE_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ROUTE_STATUS()
		{
		}
	}
}