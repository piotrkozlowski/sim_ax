using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER
    {
				public int ID_CONSUMER;
				public int? ID_CONSUMER_TYPE;
				public int ID_ACTOR;
				public int ID_DISTRIBUTOR;
				public string DESCRIPTION;
                public bool IS_BLOCKED;

        partial void CloneUser(DB_CONSUMER clone);

		public DB_CONSUMER(DB_CONSUMER clone)
		{
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_CONSUMER_TYPE = clone.ID_CONSUMER_TYPE;
						this.ID_ACTOR = clone.ID_ACTOR;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.DESCRIPTION = clone.DESCRIPTION;
                        this.IS_BLOCKED = clone.IS_BLOCKED;
						CloneUser(clone);
		}

		public DB_CONSUMER()
		{
		}
	}
}