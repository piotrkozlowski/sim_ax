using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_PROFILE_DATA
    {
        [DataMember]
        public long ID_PROFILE_DATA;
        [DataMember]
        public int ID_PROFILE;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_PROFILE_DATA clone);

		public DB_PROFILE_DATA(DB_PROFILE_DATA clone)
		{
						this.ID_PROFILE_DATA = clone.ID_PROFILE_DATA;
						this.ID_PROFILE = clone.ID_PROFILE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_PROFILE_DATA()
		{
		}
	}
}