using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DEF_HISTORY
    {
				public long ID_ROUTE_DEF_HISTORY;
				public int ID_ROUTE_DEF;
				public int ID_ROUTE_STATUS;
				public int ID_OPERATOR;
				public DateTime START_TIME;
				public DateTime? END_TIME;
		    
		partial void CloneUser(DB_ROUTE_DEF_HISTORY clone);

		public DB_ROUTE_DEF_HISTORY(DB_ROUTE_DEF_HISTORY clone)
		{
						this.ID_ROUTE_DEF_HISTORY = clone.ID_ROUTE_DEF_HISTORY;
						this.ID_ROUTE_DEF = clone.ID_ROUTE_DEF;
						this.ID_ROUTE_STATUS = clone.ID_ROUTE_STATUS;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_ROUTE_DEF_HISTORY()
		{
		}
	}
}