using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TASK_STATUS
    {
				public int ID_TASK_STATUS;
				public long ID_DESCR;
				public string DESCRIPTION;
				public bool IS_FINISHED;
		    
		partial void CloneUser(DB_TASK_STATUS clone);

		public DB_TASK_STATUS(DB_TASK_STATUS clone)
		{
						this.ID_TASK_STATUS = clone.ID_TASK_STATUS;
						this.ID_DESCR = clone.ID_DESCR;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.IS_FINISHED = clone.IS_FINISHED;
						CloneUser(clone);
		}

		public DB_TASK_STATUS()
		{
		}
	}
}