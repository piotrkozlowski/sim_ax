using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_OPERATOR
    {
        [DataMember]
        public int ID_OPERATOR;
        [DataMember]
        public int ID_ACTOR;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public string LOGIN;
        [DataMember]
        public string PASSWORD;
        [DataMember]
        public bool IS_BLOCKED;
        [DataMember]
        public string DESCRIPTION;
		    
		partial void CloneUser(DB_OPERATOR clone);

		public DB_OPERATOR(DB_OPERATOR clone)
		{
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_ACTOR = clone.ID_ACTOR;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.LOGIN = clone.LOGIN;
						this.PASSWORD = clone.PASSWORD;
						this.IS_BLOCKED = clone.IS_BLOCKED;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_OPERATOR()
		{
		}
	}
}