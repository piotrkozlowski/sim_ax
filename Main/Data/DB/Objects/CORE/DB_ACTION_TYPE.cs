using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_TYPE
    {
				public int ID_ACTION_TYPE;
				public int ID_ACTION_TYPE_CLASS;
				public string NAME;
				public string PLUGIN_NAME;
				public long? ID_DESCR;
				public bool IS_ACTION_DATA_FIXED;
                public int? ID_SERVER;
		    
		partial void CloneUser(DB_ACTION_TYPE clone);

		public DB_ACTION_TYPE(DB_ACTION_TYPE clone)
		{
						this.ID_ACTION_TYPE = clone.ID_ACTION_TYPE;
						this.ID_ACTION_TYPE_CLASS = clone.ID_ACTION_TYPE_CLASS;
						this.NAME = clone.NAME;
						this.PLUGIN_NAME = clone.PLUGIN_NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.IS_ACTION_DATA_FIXED = clone.IS_ACTION_DATA_FIXED;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_ACTION_TYPE()
		{
		}
	}
}