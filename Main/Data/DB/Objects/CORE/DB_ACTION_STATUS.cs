using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_STATUS
    {
				public int ID_ACTION_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_ACTION_STATUS clone);

		public DB_ACTION_STATUS(DB_ACTION_STATUS clone)
		{
						this.ID_ACTION_STATUS = clone.ID_ACTION_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ACTION_STATUS()
		{
		}
	}
}