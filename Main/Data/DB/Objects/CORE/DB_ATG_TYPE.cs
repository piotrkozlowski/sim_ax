using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ATG_TYPE
    {
				public int ID_ATG_TYPE;
				public string DESCR;
		    
		partial void CloneUser(DB_ATG_TYPE clone);

		public DB_ATG_TYPE(DB_ATG_TYPE clone)
		{
						this.ID_ATG_TYPE = clone.ID_ATG_TYPE;
						this.DESCR = clone.DESCR;
						CloneUser(clone);
		}

		public DB_ATG_TYPE()
		{
		}
	}
}