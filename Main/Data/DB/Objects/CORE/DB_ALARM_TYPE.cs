using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ALARM_TYPE
    {
        [DataMember]
		public int ID_ALARM_TYPE;
        [DataMember]
        public string SYMBOL;
        [DataMember]
        public long? ID_DESCR;
		    
		partial void CloneUser(DB_ALARM_TYPE clone);

		public DB_ALARM_TYPE(DB_ALARM_TYPE clone)
		{
						this.ID_ALARM_TYPE = clone.ID_ALARM_TYPE;
						this.SYMBOL = clone.SYMBOL;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ALARM_TYPE()
		{
		}
	}
}