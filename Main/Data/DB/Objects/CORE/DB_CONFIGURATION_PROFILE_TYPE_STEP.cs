using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONFIGURATION_PROFILE_TYPE_STEP
    {
				public int ID_CONFIGURATION_PROFILE_TYPE;
				public int ID_DEVICE_TYPE_PROFILE_STEP;
		    
		partial void CloneUser(DB_CONFIGURATION_PROFILE_TYPE_STEP clone);

		public DB_CONFIGURATION_PROFILE_TYPE_STEP(DB_CONFIGURATION_PROFILE_TYPE_STEP clone)
		{
						this.ID_CONFIGURATION_PROFILE_TYPE = clone.ID_CONFIGURATION_PROFILE_TYPE;
						this.ID_DEVICE_TYPE_PROFILE_STEP = clone.ID_DEVICE_TYPE_PROFILE_STEP;
						CloneUser(clone);
		}

		public DB_CONFIGURATION_PROFILE_TYPE_STEP()
		{
		}
	}
}