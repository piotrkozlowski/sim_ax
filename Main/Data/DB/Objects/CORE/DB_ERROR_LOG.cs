using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ERROR_LOG
    {
				public int ID_ERROR_LOG;
				public int APPLICATION_ID;
				public DateTime ERROR_DATE;
				public string CLASS_NAME;
				public string FUNCTION_NAME;
				public string SOURCE;
				public string MESSAGE;
				public string STACK_TRACE;
				public string DESCRIPTION;
				public int SEVERITY;
		    
		partial void CloneUser(DB_ERROR_LOG clone);

		public DB_ERROR_LOG(DB_ERROR_LOG clone)
		{
						this.ID_ERROR_LOG = clone.ID_ERROR_LOG;
						this.APPLICATION_ID = clone.APPLICATION_ID;
						this.ERROR_DATE = clone.ERROR_DATE;
						this.CLASS_NAME = clone.CLASS_NAME;
						this.FUNCTION_NAME = clone.FUNCTION_NAME;
						this.SOURCE = clone.SOURCE;
						this.MESSAGE = clone.MESSAGE;
						this.STACK_TRACE = clone.STACK_TRACE;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.SEVERITY = clone.SEVERITY;
						CloneUser(clone);
		}

		public DB_ERROR_LOG()
		{
		}
	}
}