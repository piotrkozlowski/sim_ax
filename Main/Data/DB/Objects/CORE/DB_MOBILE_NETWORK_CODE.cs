using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_MOBILE_NETWORK_CODE
    {
				public int CODE;
				public string MMC;
				public string MNC;
				public string BRAND;
				public string OPERATOR;
				public string BANDS;
				public string SMS_SERVICE_CENTER;
				public string APN_ADDRESS;
				public string APN_LOGIN;
				public string APN_PASSWORD;
		    
		partial void CloneUser(DB_MOBILE_NETWORK_CODE clone);

		public DB_MOBILE_NETWORK_CODE(DB_MOBILE_NETWORK_CODE clone)
		{
						this.CODE = clone.CODE;
						this.MMC = clone.MMC;
						this.MNC = clone.MNC;
						this.BRAND = clone.BRAND;
						this.OPERATOR = clone.OPERATOR;
						this.BANDS = clone.BANDS;
						this.SMS_SERVICE_CENTER = clone.SMS_SERVICE_CENTER;
						this.APN_ADDRESS = clone.APN_ADDRESS;
						this.APN_LOGIN = clone.APN_LOGIN;
						this.APN_PASSWORD = clone.APN_PASSWORD;
						CloneUser(clone);
		}

		public DB_MOBILE_NETWORK_CODE()
		{
		}
	}
}