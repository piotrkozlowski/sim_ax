using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_CONTENT
    {
        public int ID_CONTENT;
        public int ID_MODULE;
        public string NAME;
        public string DESCRIPTION;
        public string CONTENT;
        public int ID_LANGUAGE;

        partial void CloneUser(DB_CONTENT clone);

        public DB_CONTENT(DB_CONTENT clone)
        {
            this.ID_CONTENT = clone.ID_CONTENT;
            this.ID_MODULE = clone.ID_MODULE;
            this.NAME = clone.NAME;
            this.DESCRIPTION = clone.DESCRIPTION;
            this.CONTENT = clone.CONTENT;
            this.ID_LANGUAGE = clone.ID_LANGUAGE;
            CloneUser(clone);
        }

        public DB_CONTENT()
        {
        }
    }
}