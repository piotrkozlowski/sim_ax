﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ROUTE_TYPE_ACTION_TYPE
    {
        public int ID_ROUTE_TYPE_ACTION_TYPE;
        public int ID_ROUTE_TYPE;
        public int ID_ACTION_TYPE;
        public bool DEFAULT;

        partial void CloneUser(DB_ROUTE_TYPE_ACTION_TYPE clone);

        public DB_ROUTE_TYPE_ACTION_TYPE(DB_ROUTE_TYPE_ACTION_TYPE clone)
        {
            this.ID_ROUTE_TYPE_ACTION_TYPE = clone.ID_ROUTE_TYPE_ACTION_TYPE;
            this.ID_ROUTE_TYPE = clone.ID_ROUTE_TYPE;
            this.ID_ACTION_TYPE = clone.ID_ACTION_TYPE;
            this.DEFAULT = clone.DEFAULT;
            CloneUser(clone);
        }

        public DB_ROUTE_TYPE_ACTION_TYPE()
        {
        }
    }
}