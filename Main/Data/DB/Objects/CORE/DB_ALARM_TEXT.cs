using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_TEXT
    {
				public long ID_ALARM_TEXT;
				public int ID_LANGUAGE;
				public string NAME;
				public string ALARM_TEXT;
				public int? ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_ALARM_TEXT clone);

		public DB_ALARM_TEXT(DB_ALARM_TEXT clone)
		{
						this.ID_ALARM_TEXT = clone.ID_ALARM_TEXT;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.NAME = clone.NAME;
						this.ALARM_TEXT = clone.ALARM_TEXT;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_ALARM_TEXT()
		{
		}
	}
}