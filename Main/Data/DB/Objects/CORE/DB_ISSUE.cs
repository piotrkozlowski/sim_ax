using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ISSUE
    {
        [DataMember]
		public int ID_ISSUE;
        [DataMember]
		public int ID_ACTOR;
        [DataMember]
		public int ID_OPERATOR_REGISTERING;
        [DataMember]
		public int? ID_OPERATOR_PERFORMER;
        [DataMember]
		public int ID_ISSUE_TYPE;
        [DataMember]
		public int ID_ISSUE_STATUS;
        [DataMember]
		public int ID_DISTRIBUTOR;
        [DataMember]
		public DateTime? REALIZATION_DATE;
        [DataMember]
		public DateTime CREATION_DATE;
        [DataMember]
		public string SHORT_DESCR;
        [DataMember]
		public string LONG_DESCR;
        [DataMember]
		public long? ID_LOCATION;
        [DataMember]
		public DateTime? DEADLINE;
        [DataMember]
		public int ID_PRIORITY;
        [DataMember]
		public object ROW_VERSION;
		    
		partial void CloneUser(DB_ISSUE clone);

		public DB_ISSUE(DB_ISSUE clone)
		{
						this.ID_ISSUE = clone.ID_ISSUE;
						this.ID_ACTOR = clone.ID_ACTOR;
						this.ID_OPERATOR_REGISTERING = clone.ID_OPERATOR_REGISTERING;
						this.ID_OPERATOR_PERFORMER = clone.ID_OPERATOR_PERFORMER;
						this.ID_ISSUE_TYPE = clone.ID_ISSUE_TYPE;
						this.ID_ISSUE_STATUS = clone.ID_ISSUE_STATUS;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.REALIZATION_DATE = clone.REALIZATION_DATE;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.SHORT_DESCR = clone.SHORT_DESCR;
						this.LONG_DESCR = clone.LONG_DESCR;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.DEADLINE = clone.DEADLINE;
						this.ID_PRIORITY = clone.ID_PRIORITY;
						this.ROW_VERSION = clone.ROW_VERSION;
						CloneUser(clone);
		}

		public DB_ISSUE()
		{
		}
	}
}