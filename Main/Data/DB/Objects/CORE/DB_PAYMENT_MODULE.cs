using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PAYMENT_MODULE
    {
				public int ID_PAYMENT_MODULE;
				public string NAME;
				public int ID_PAYMENT_MODULE_TYPE;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_PAYMENT_MODULE clone);

		public DB_PAYMENT_MODULE(DB_PAYMENT_MODULE clone)
		{
						this.ID_PAYMENT_MODULE = clone.ID_PAYMENT_MODULE;
						this.NAME = clone.NAME;
						this.ID_PAYMENT_MODULE_TYPE = clone.ID_PAYMENT_MODULE_TYPE;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_PAYMENT_MODULE()
		{
		}
	}
}