using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SUPPLIER
    {
				public int ID_SUPPLIER;
				public string NAME;
		    
		partial void CloneUser(DB_SUPPLIER clone);

		public DB_SUPPLIER(DB_SUPPLIER clone)
		{
						this.ID_SUPPLIER = clone.ID_SUPPLIER;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_SUPPLIER()
		{
		}
	}
}