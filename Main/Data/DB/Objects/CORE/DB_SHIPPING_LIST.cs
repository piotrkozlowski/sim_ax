﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_SHIPPING_LIST
    {
        [DataMember]
        public int ID_SHIPPING_LIST;
        [DataMember]
        public string CONTRACT_NBR;
        [DataMember]
        public string SHIPPING_LIST_NBR;
        [DataMember]
        public int ID_OPERATOR_CREATOR;
        [DataMember]
        public int ID_OPERATOR_CONTACT;
        [DataMember]
        public int ID_OPERATOR_QUALITY;
        [DataMember]
        public int ID_OPERATOR_PROTOCOL;
        [DataMember]
        public string SHIPPING_LETTER_NBR;
        [DataMember]
        public long ID_LOCATION;
        [DataMember]
        public int ID_SHIPPING_LIST_TYPE;
        [DataMember]
        public DateTime CREATION_DATE;
        [DataMember]
        public DateTime? FINISH_DATE;
        [DataMember]
        public int ID_DISTRIBUTOR;

        partial void CloneUser(DB_SHIPPING_LIST clone);

        public DB_SHIPPING_LIST(DB_SHIPPING_LIST clone)
        {
            this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
            this.CONTRACT_NBR = clone.CONTRACT_NBR;
            this.SHIPPING_LIST_NBR = clone.SHIPPING_LIST_NBR;
            this.ID_OPERATOR_CREATOR = clone.ID_OPERATOR_CREATOR;
            this.ID_OPERATOR_CONTACT = clone.ID_OPERATOR_CONTACT;
            this.ID_OPERATOR_QUALITY = clone.ID_OPERATOR_QUALITY;
            this.ID_OPERATOR_PROTOCOL = clone.ID_OPERATOR_PROTOCOL;
            this.SHIPPING_LETTER_NBR = clone.SHIPPING_LETTER_NBR;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.ID_SHIPPING_LIST_TYPE = clone.ID_SHIPPING_LIST_TYPE;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.FINISH_DATE = clone.FINISH_DATE;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            CloneUser(clone);
        }

        public DB_SHIPPING_LIST()
        {
        }
    }
}