﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DATA_FORMAT_GROUP
    {
        public int ID_DATA_FORMAT_GROUP;
        public string NAME;
        public int? ID_MODULE;

        partial void CloneUser(DB_DATA_FORMAT_GROUP clone);

        public DB_DATA_FORMAT_GROUP(DB_DATA_FORMAT_GROUP clone)
        {
            this.ID_DATA_FORMAT_GROUP = clone.ID_DATA_FORMAT_GROUP;
            this.NAME = clone.NAME;
            this.ID_MODULE = clone.ID_MODULE;
            CloneUser(clone);
        }

        public DB_DATA_FORMAT_GROUP()
        {
        }
    }
}