using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ACTION_SMS_TEXT
    {
        [DataMember]
        public long ID_ACTION_SMS_TEXT;
        [DataMember]
        public int ID_LANGUAGE;
        [DataMember]
        public string SMS_TEXT;
        [DataMember]
        public long? ID_DATA_TYPE;
        [DataMember]
        public bool IS_INCOMING;
        [DataMember]
        public int? ID_TRANSMISSION_TYPE;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_ACTION_SMS_TEXT clone);

		public DB_ACTION_SMS_TEXT(DB_ACTION_SMS_TEXT clone)
		{
						this.ID_ACTION_SMS_TEXT = clone.ID_ACTION_SMS_TEXT;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.SMS_TEXT = clone.SMS_TEXT;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.IS_INCOMING = clone.IS_INCOMING;
						this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_ACTION_SMS_TEXT()
		{
		}
	}
}