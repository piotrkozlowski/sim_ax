using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTOR_IN_GROUP
    {
				public int ID_ACTOR;
				public int ID_ACTOR_GROUP;
		    
		partial void CloneUser(DB_ACTOR_IN_GROUP clone);

		public DB_ACTOR_IN_GROUP(DB_ACTOR_IN_GROUP clone)
		{
						this.ID_ACTOR = clone.ID_ACTOR;
						this.ID_ACTOR_GROUP = clone.ID_ACTOR_GROUP;
						CloneUser(clone);
		}

		public DB_ACTOR_IN_GROUP()
		{
		}
	}
}