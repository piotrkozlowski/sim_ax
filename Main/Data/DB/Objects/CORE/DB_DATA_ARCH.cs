using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DATA_ARCH
    {
				public long ID_DATA_ARCH;
				public long? SERIAL_NBR;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
				public DateTime TIME;
				public bool IS_ACTION;
				public bool IS_ALARM;
				public long? ID_PACKET;
                public long? ID_METER;
                public long? ID_LOCATION;
		    
		partial void CloneUser(DB_DATA_ARCH clone);

		public DB_DATA_ARCH(DB_DATA_ARCH clone)
		{
						this.ID_DATA_ARCH = clone.ID_DATA_ARCH;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						this.TIME = clone.TIME;
						this.IS_ACTION = clone.IS_ACTION;
						this.IS_ALARM = clone.IS_ALARM;
						this.ID_PACKET = clone.ID_PACKET;
                        this.ID_METER = clone.ID_METER;
                        this.ID_LOCATION = clone.ID_LOCATION;
						CloneUser(clone);
		}

		public DB_DATA_ARCH()
		{
		}
	}
}