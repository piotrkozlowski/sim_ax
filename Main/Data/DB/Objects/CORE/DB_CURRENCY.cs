using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CURRENCY
    {
				public int ID_CURRENCY;
				public string SYMBOL;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_CURRENCY clone);

		public DB_CURRENCY(DB_CURRENCY clone)
		{
						this.ID_CURRENCY = clone.ID_CURRENCY;
						this.SYMBOL = clone.SYMBOL;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_CURRENCY()
		{
		}
	}
}