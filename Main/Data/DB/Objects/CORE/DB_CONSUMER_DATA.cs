using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_DATA
    {
				public long ID_CONSUMER_DATA;
				public int ID_CONSUMER;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_CONSUMER_DATA clone);

		public DB_CONSUMER_DATA(DB_CONSUMER_DATA clone)
		{
						this.ID_CONSUMER_DATA = clone.ID_CONSUMER_DATA;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_CONSUMER_DATA()
		{
		}
	}
}