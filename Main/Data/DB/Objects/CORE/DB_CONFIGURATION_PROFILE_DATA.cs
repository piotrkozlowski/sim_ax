using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_CONFIGURATION_PROFILE_DATA
    {
        [DataMember]
				public long ID_CONFIGURATION_PROFILE_DATA;
        [DataMember]
        public long ID_CONFIGURATION_PROFILE;
        [DataMember]
        public int ID_DEVICE_TYPE;
        [DataMember]
        public int? ID_DEVICE_TYPE_PROFILE_STEP;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
		    
		partial void CloneUser(DB_CONFIGURATION_PROFILE_DATA clone);

		public DB_CONFIGURATION_PROFILE_DATA(DB_CONFIGURATION_PROFILE_DATA clone)
		{
						this.ID_CONFIGURATION_PROFILE_DATA = clone.ID_CONFIGURATION_PROFILE_DATA;
						this.ID_CONFIGURATION_PROFILE = clone.ID_CONFIGURATION_PROFILE;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
                        this.ID_DEVICE_TYPE_PROFILE_STEP = clone.ID_DEVICE_TYPE_PROFILE_STEP;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_CONFIGURATION_PROFILE_DATA()
		{
		}
	}
}