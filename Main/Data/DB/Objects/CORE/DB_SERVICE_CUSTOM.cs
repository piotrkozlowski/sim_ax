﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_CUSTOM
    {
        public long ID_SERVICE_CUSTOM;
        public int ID_SERVICE_LIST;
        public Double? EDITOR_COST;
        public Double? ENGINEER_COST;
        public Double? DIRECT_COST;
        public int? INDIRECT_COST;
        public Double? MATERIAL_COST;
        public Double? SUPPLY_COST;
        public Double? TRANSPORT_COST;
        public long? ID_FILE;
        public bool? CUSTOM_VALUE;
        public Double? EDITOR_TIME;
        public Double? ENGINEER_TIME;

        partial void CloneUser(DB_SERVICE_CUSTOM clone);

        public DB_SERVICE_CUSTOM(DB_SERVICE_CUSTOM clone)
        {
            this.ID_SERVICE_CUSTOM = clone.ID_SERVICE_CUSTOM;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.EDITOR_COST = clone.EDITOR_COST;
            this.ENGINEER_COST = clone.ENGINEER_COST;
            this.DIRECT_COST = clone.DIRECT_COST;
            this.INDIRECT_COST = clone.INDIRECT_COST;
            this.MATERIAL_COST = clone.MATERIAL_COST;
            this.SUPPLY_COST = clone.SUPPLY_COST;
            this.TRANSPORT_COST = clone.TRANSPORT_COST;
            this.ID_FILE = clone.ID_FILE;
            this.CUSTOM_VALUE = clone.CUSTOM_VALUE;
            this.EDITOR_TIME = clone.EDITOR_TIME;
            this.ENGINEER_TIME = clone.ENGINEER_TIME;
            CloneUser(clone);
        }

        public DB_SERVICE_CUSTOM()
        {
        }
    }
}