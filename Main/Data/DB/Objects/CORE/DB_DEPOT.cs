﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEPOT
    {
        public int ID_DEPOT;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_DEPOT clone);

        public DB_DEPOT(DB_DEPOT clone)
        {
            this.ID_DEPOT = clone.ID_DEPOT;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_DEPOT()
        {
        }
    }
}