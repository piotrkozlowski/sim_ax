﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_PACKAGE_STATUS_HISTORY
    {
        public int ID_PACKAGE_STATUS_HISTORY;
        public int ID_PACKAGE;
        public int ID_PACKAGE_STATUS;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_PACKAGE_STATUS_HISTORY clone);

        public DB_PACKAGE_STATUS_HISTORY(DB_PACKAGE_STATUS_HISTORY clone)
        {
            this.ID_PACKAGE_STATUS_HISTORY = clone.ID_PACKAGE_STATUS_HISTORY;
            this.ID_PACKAGE = clone.ID_PACKAGE;
            this.ID_PACKAGE_STATUS = clone.ID_PACKAGE_STATUS;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_PACKAGE_STATUS_HISTORY()
        {
        }
    }
}