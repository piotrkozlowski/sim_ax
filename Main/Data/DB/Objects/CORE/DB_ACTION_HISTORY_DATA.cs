using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_HISTORY_DATA
    {
				public long ID_ACTION_HISTORY_DATA;
				public int ARG_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_ACTION_HISTORY_DATA clone);

		public DB_ACTION_HISTORY_DATA(DB_ACTION_HISTORY_DATA clone)
		{
						this.ID_ACTION_HISTORY_DATA = clone.ID_ACTION_HISTORY_DATA;
						this.ARG_NBR = clone.ARG_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ACTION_HISTORY_DATA()
		{
		}
	}
}