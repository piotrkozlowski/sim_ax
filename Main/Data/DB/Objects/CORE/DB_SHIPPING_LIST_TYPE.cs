﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SHIPPING_LIST_TYPE
    {
        public int ID_SHIPPING_LIST_TYPE;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_SHIPPING_LIST_TYPE clone);

        public DB_SHIPPING_LIST_TYPE(DB_SHIPPING_LIST_TYPE clone)
        {
            this.ID_SHIPPING_LIST_TYPE = clone.ID_SHIPPING_LIST_TYPE;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_SHIPPING_LIST_TYPE()
        {
        }
    }
}