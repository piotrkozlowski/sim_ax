using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_FAULT_CODE_TYPE
    {
				public int ID_FAULT_CODE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_FAULT_CODE_TYPE clone);

		public DB_FAULT_CODE_TYPE(DB_FAULT_CODE_TYPE clone)
		{
						this.ID_FAULT_CODE_TYPE = clone.ID_FAULT_CODE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_FAULT_CODE_TYPE()
		{
		}
	}
}