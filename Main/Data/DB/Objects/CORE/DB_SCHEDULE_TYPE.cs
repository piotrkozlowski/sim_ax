using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SCHEDULE_TYPE
    {
				public int ID_SCHEDULE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_SCHEDULE_TYPE clone);

		public DB_SCHEDULE_TYPE(DB_SCHEDULE_TYPE clone)
		{
						this.ID_SCHEDULE_TYPE = clone.ID_SCHEDULE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_SCHEDULE_TYPE()
		{
		}
	}
}