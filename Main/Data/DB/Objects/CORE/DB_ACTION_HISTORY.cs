using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ACTION_HISTORY
    {
        [DataMember]
        public long ID_ACTION_HISTORY;
        [DataMember]
        public long ID_ACTION;
        [DataMember]
        public int ID_ACTION_STATUS;
        [DataMember]
        public DateTime? START_TIME;
        [DataMember]
        public DateTime? END_TIME;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public long? ID_ACTION_HISTORY_DATA;
        [DataMember]
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
        [DataMember]
        public int? ID_MODULE;
        [DataMember]
		public string ASSEMBLY;
		    
		partial void CloneUser(DB_ACTION_HISTORY clone);

		public DB_ACTION_HISTORY(DB_ACTION_HISTORY clone)
		{
						this.ID_ACTION_HISTORY = clone.ID_ACTION_HISTORY;
						this.ID_ACTION = clone.ID_ACTION;
						this.ID_ACTION_STATUS = clone.ID_ACTION_STATUS;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_ACTION_HISTORY_DATA = clone.ID_ACTION_HISTORY_DATA;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						this.ID_MODULE = clone.ID_MODULE;
						this.ASSEMBLY = clone.ASSEMBLY;
						CloneUser(clone);
		}

		public DB_ACTION_HISTORY()
		{
		}
	}
}