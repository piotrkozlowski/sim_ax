﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_SHIPPING_LIST_DATA
    {
        [DataMember]
        public long ID_SHIPPING_LIST_DATA;
        [DataMember]
        public int ID_SHIPPING_LIST;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;

        partial void CloneUser(DB_SHIPPING_LIST_DATA clone);

        public DB_SHIPPING_LIST_DATA(DB_SHIPPING_LIST_DATA clone)
        {
            this.ID_SHIPPING_LIST_DATA = clone.ID_SHIPPING_LIST_DATA;
            this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_SHIPPING_LIST_DATA()
        {
        }
    }
}