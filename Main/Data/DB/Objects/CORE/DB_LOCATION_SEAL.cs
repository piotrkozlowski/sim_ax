using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_SEAL
    {
				public long ID_LOCATION;
				public long ID_SEAL;
		    
		partial void CloneUser(DB_LOCATION_SEAL clone);

		public DB_LOCATION_SEAL(DB_LOCATION_SEAL clone)
		{
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_SEAL = clone.ID_SEAL;
						CloneUser(clone);
		}

		public DB_LOCATION_SEAL()
		{
		}
	}
}