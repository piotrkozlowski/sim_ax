using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_TYPE
    {
				public int ID_LOCATION_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_LOCATION_TYPE clone);

		public DB_LOCATION_TYPE(DB_LOCATION_TYPE clone)
		{
						this.ID_LOCATION_TYPE = clone.ID_LOCATION_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_LOCATION_TYPE()
		{
		}
	}
}