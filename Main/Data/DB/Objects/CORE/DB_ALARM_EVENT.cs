using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_EVENT
    {
				public long ID_ALARM_EVENT;
				public long ID_ALARM_DEF;
				public long? ID_DATA_ARCH;
				public bool IS_ACTIVE;
				public long? SERIAL_NBR;
				public long? ID_METER;
				public long? ID_LOCATION;
		    
		partial void CloneUser(DB_ALARM_EVENT clone);

		public DB_ALARM_EVENT(DB_ALARM_EVENT clone)
		{
						this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
						this.ID_ALARM_DEF = clone.ID_ALARM_DEF;
						this.ID_DATA_ARCH = clone.ID_DATA_ARCH;
						this.IS_ACTIVE = clone.IS_ACTIVE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						CloneUser(clone);
		}

		public DB_ALARM_EVENT()
		{
		}
	}
}