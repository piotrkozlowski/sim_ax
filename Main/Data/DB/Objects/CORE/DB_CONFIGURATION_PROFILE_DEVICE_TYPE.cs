using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONFIGURATION_PROFILE_DEVICE_TYPE
    {
				public long ID_CONFIGURATION_PROFILE;
				public int ID_DEVICE_TYPE;
		    
		partial void CloneUser(DB_CONFIGURATION_PROFILE_DEVICE_TYPE clone);

		public DB_CONFIGURATION_PROFILE_DEVICE_TYPE(DB_CONFIGURATION_PROFILE_DEVICE_TYPE clone)
		{
						this.ID_CONFIGURATION_PROFILE = clone.ID_CONFIGURATION_PROFILE;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						CloneUser(clone);
		}

		public DB_CONFIGURATION_PROFILE_DEVICE_TYPE()
		{
		}
	}
}