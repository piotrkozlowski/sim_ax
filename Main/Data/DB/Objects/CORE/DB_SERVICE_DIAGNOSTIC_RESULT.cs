﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_DIAGNOSTIC_RESULT
    {
        public int ID_SERVICE_DIAGNOSTIC_RESULT;
        public string NAME;
        public long? ID_DESCR;
        public bool? IS_SERVICE_RESULT;
        public bool? IS_DIAGNOSTIC_RESULT;

        partial void CloneUser(DB_SERVICE_DIAGNOSTIC_RESULT clone);

        public DB_SERVICE_DIAGNOSTIC_RESULT(DB_SERVICE_DIAGNOSTIC_RESULT clone)
        {
            this.ID_SERVICE_DIAGNOSTIC_RESULT = clone.ID_SERVICE_DIAGNOSTIC_RESULT;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            this.IS_SERVICE_RESULT = clone.IS_SERVICE_RESULT;
            this.IS_DIAGNOSTIC_RESULT = clone.IS_DIAGNOSTIC_RESULT;
            CloneUser(clone);
        }

        public DB_SERVICE_DIAGNOSTIC_RESULT()
        {
        }
    }
}