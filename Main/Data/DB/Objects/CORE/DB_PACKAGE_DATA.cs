using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PACKAGE_DATA
    {
				public long ID_PACKAGE_DATA;
				public int ID_PACKAGE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_PACKAGE_DATA clone);

		public DB_PACKAGE_DATA(DB_PACKAGE_DATA clone)
		{
						this.ID_PACKAGE_DATA = clone.ID_PACKAGE_DATA;
						this.ID_PACKAGE = clone.ID_PACKAGE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_PACKAGE_DATA()
		{
		}
	}
}