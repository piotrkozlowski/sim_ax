using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_STATE
    {
		public int ID_VERSION_STATE;
		public string NAME;
		public long? ID_DESCR;
		    
		partial void CloneUser(DB_VERSION_STATE clone);

		public DB_VERSION_STATE(DB_VERSION_STATE clone)
		{
			this.ID_VERSION_STATE = clone.ID_VERSION_STATE;
			this.NAME = clone.NAME;
			this.ID_DESCR = clone.ID_DESCR;
			CloneUser(clone);
		}

		public DB_VERSION_STATE()
		{
		}
	}
}