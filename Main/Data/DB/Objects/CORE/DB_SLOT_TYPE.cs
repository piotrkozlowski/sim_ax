using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SLOT_TYPE
    {
				public int ID_SLOT_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_SLOT_TYPE clone);

		public DB_SLOT_TYPE(DB_SLOT_TYPE clone)
		{
						this.ID_SLOT_TYPE = clone.ID_SLOT_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_SLOT_TYPE()
		{
		}
	}
}