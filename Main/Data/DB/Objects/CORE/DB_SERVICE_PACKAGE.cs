﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_PACKAGE
    {
        public long ID_SERVICE_PACKAGE;
        public int? ID_CONTRACT;
        public string NAME;
        public string DESCR;
        public bool IS_SINGLE;

        partial void CloneUser(DB_SERVICE_PACKAGE clone);

        public DB_SERVICE_PACKAGE(DB_SERVICE_PACKAGE clone)
        {
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.ID_CONTRACT = clone.ID_CONTRACT;
            this.NAME = clone.NAME;
            this.DESCR = clone.DESCR;
            this.IS_SINGLE = clone.IS_SINGLE;
            CloneUser(clone);
        }

        public DB_SERVICE_PACKAGE()
        {
        }
    }
}