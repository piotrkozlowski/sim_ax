using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_TASK
    {
        [DataMember]
        public int ID_TASK;
        [DataMember]
        public int ID_TASK_TYPE;
        [DataMember]
        public int? ID_ISSUE;
        [DataMember]
        public int? ID_TASK_GROUP;
        [DataMember]
        public long? ID_PLANNED_ROUTE;
        [DataMember]
        public int ID_OPERATOR_REGISTERING;
        [DataMember]
        public int? ID_OPERATOR_PERFORMER;
        [DataMember]
        public int ID_TASK_STATUS;
        [DataMember]
        public DateTime CREATION_DATE;
        [DataMember]
        public bool? ACCEPTED;
        [DataMember]
        public int? ID_OPERATOR_ACCEPTED;
        [DataMember]
        public DateTime? ACCEPTANCE_DATE;
        [DataMember]
        public string NOTES;
        [DataMember]
        public DateTime? DEADLINE;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public int? ID_ACTOR;
        [DataMember]
        public string TOPIC_NUMBER;
        [DataMember]
        public int PRIORITY;
        [DataMember]
        public long? OPERATION_CODE;
        [DataMember]
        public object ROW_VERSION;
        [DataMember]
		public int ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_TASK clone);

		public DB_TASK(DB_TASK clone)
		{
						this.ID_TASK = clone.ID_TASK;
						this.ID_TASK_TYPE = clone.ID_TASK_TYPE;
						this.ID_ISSUE = clone.ID_ISSUE;
						this.ID_TASK_GROUP = clone.ID_TASK_GROUP;
						this.ID_PLANNED_ROUTE = clone.ID_PLANNED_ROUTE;
						this.ID_OPERATOR_REGISTERING = clone.ID_OPERATOR_REGISTERING;
						this.ID_OPERATOR_PERFORMER = clone.ID_OPERATOR_PERFORMER;
						this.ID_TASK_STATUS = clone.ID_TASK_STATUS;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.ACCEPTED = clone.ACCEPTED;
						this.ID_OPERATOR_ACCEPTED = clone.ID_OPERATOR_ACCEPTED;
						this.ACCEPTANCE_DATE = clone.ACCEPTANCE_DATE;
						this.NOTES = clone.NOTES;
						this.DEADLINE = clone.DEADLINE;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_ACTOR = clone.ID_ACTOR;
						this.TOPIC_NUMBER = clone.TOPIC_NUMBER;
						this.PRIORITY = clone.PRIORITY;
						this.OPERATION_CODE = clone.OPERATION_CODE;
						this.ROW_VERSION = clone.ROW_VERSION;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_TASK()
		{
		}
	}
}