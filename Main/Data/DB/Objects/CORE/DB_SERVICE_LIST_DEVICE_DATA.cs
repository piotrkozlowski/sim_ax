﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST_DEVICE_DATA
    {
        public long ID_SERVICE_LIST_DEVICE_DATA;
        public int? ID_SERVICE_LIST;
        public long? SERIAL_NBR;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_SERVICE_LIST_DEVICE_DATA clone);

        public DB_SERVICE_LIST_DEVICE_DATA(DB_SERVICE_LIST_DEVICE_DATA clone)
        {
            this.ID_SERVICE_LIST_DEVICE_DATA = clone.ID_SERVICE_LIST_DEVICE_DATA;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST_DEVICE_DATA()
        {
        }
    }
}