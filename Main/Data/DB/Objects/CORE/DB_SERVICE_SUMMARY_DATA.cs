﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_SUMMARY_DATA
    {
        public long ID_SERVICE_SUMMARY_DATA;
        public int ID_SERVICE_LIST;
        public long ID_SERVICE_PACKAGE;
        public Double COST;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_SERVICE_SUMMARY_DATA clone);

        public DB_SERVICE_SUMMARY_DATA(DB_SERVICE_SUMMARY_DATA clone)
        {
            this.ID_SERVICE_SUMMARY_DATA = clone.ID_SERVICE_SUMMARY_DATA;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.COST = clone.COST;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_SERVICE_SUMMARY_DATA()
        {
        }
    }
}