using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_GROUP_OPERATOR
    {
				public int ID_ALARM_GROUP;
				public int? ID_OPERATOR;
				public long? ID_DATA_TYPE;
		    
		partial void CloneUser(DB_ALARM_GROUP_OPERATOR clone);

		public DB_ALARM_GROUP_OPERATOR(DB_ALARM_GROUP_OPERATOR clone)
		{
						this.ID_ALARM_GROUP = clone.ID_ALARM_GROUP;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						CloneUser(clone);
		}

		public DB_ALARM_GROUP_OPERATOR()
		{
		}
	}
}