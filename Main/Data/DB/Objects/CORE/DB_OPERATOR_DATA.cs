using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_OPERATOR_DATA
    {
        [DataMember]
        public long ID_OPERATOR_DATA;
        [DataMember]
        public int ID_OPERATOR;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
		public int? ID_DISTRIBUTOR;
        [DataMember]
        public int? ID_SERVER;

		partial void CloneUser(DB_OPERATOR_DATA clone);

		public DB_OPERATOR_DATA(DB_OPERATOR_DATA clone)
		{
						this.ID_OPERATOR_DATA = clone.ID_OPERATOR_DATA;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_OPERATOR_DATA()
		{
		}
	}
}