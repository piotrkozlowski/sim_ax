using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DAYLIGHT_SAVING_TIME
    {
				public int ID_DAYLIGHT_SAVING;
				public DateTime START_TIME;
				public DateTime END_TIME;
		    
		partial void CloneUser(DB_DAYLIGHT_SAVING_TIME clone);

		public DB_DAYLIGHT_SAVING_TIME(DB_DAYLIGHT_SAVING_TIME clone)
		{
						this.ID_DAYLIGHT_SAVING = clone.ID_DAYLIGHT_SAVING;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_DAYLIGHT_SAVING_TIME()
		{
		}
	}
}