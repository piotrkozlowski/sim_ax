using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DATA_TYPE
    {
        [DataMember]
		public long ID_DATA_TYPE;
        [DataMember]
		public string NAME;
        [DataMember]
		public long? ID_DESCR;
        [DataMember]
		public int ID_DATA_TYPE_CLASS;
        [DataMember]
		public int? ID_REFERENCE_TYPE;
        [DataMember]
		public int? ID_DATA_TYPE_FORMAT;
        [DataMember]
		public bool IS_ARCHIVE_ONLY;
        [DataMember]
		public bool IS_REMOTE_READ;
        [DataMember]
		public bool IS_REMOTE_WRITE;
        [DataMember]
		public bool IS_EDITABLE;
        [DataMember]
		public int ID_UNIT;
		    
		partial void CloneUser(DB_DATA_TYPE clone);

		public DB_DATA_TYPE(DB_DATA_TYPE clone)
		{
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_DATA_TYPE_CLASS = clone.ID_DATA_TYPE_CLASS;
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						this.ID_DATA_TYPE_FORMAT = clone.ID_DATA_TYPE_FORMAT;
						this.IS_ARCHIVE_ONLY = clone.IS_ARCHIVE_ONLY;
						this.IS_REMOTE_READ = clone.IS_REMOTE_READ;
						this.IS_REMOTE_WRITE = clone.IS_REMOTE_WRITE;
						this.IS_EDITABLE = clone.IS_EDITABLE;
						this.ID_UNIT = clone.ID_UNIT;
						CloneUser(clone);
		}

		public DB_DATA_TYPE()
		{
		}
	}
}