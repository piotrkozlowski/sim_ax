using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_LOCATION_STATE_HISTORY
    {
        [DataMember]
        public long ID_LOCATION_STATE;
        [DataMember]
        public long ID_LOCATION;
        [DataMember]
        public int ID_LOCATION_STATE_TYPE;
        [DataMember]
        public bool IN_KPI;
        [DataMember]
        public DateTime START_DATE;
        [DataMember]
        public DateTime? END_DATE;
        [DataMember]
		public string NOTES;
		    
		partial void CloneUser(DB_LOCATION_STATE_HISTORY clone);

		public DB_LOCATION_STATE_HISTORY(DB_LOCATION_STATE_HISTORY clone)
		{
						this.ID_LOCATION_STATE = clone.ID_LOCATION_STATE;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_LOCATION_STATE_TYPE = clone.ID_LOCATION_STATE_TYPE;
						this.IN_KPI = clone.IN_KPI;
						this.START_DATE = clone.START_DATE;
						this.END_DATE = clone.END_DATE;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_LOCATION_STATE_HISTORY()
		{
		}
	}
}