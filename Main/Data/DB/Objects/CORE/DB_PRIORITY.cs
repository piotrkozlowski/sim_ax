using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_PRIORITY
    {
        [DataMember]
		public int ID_PRIORITY;
        [DataMember]
        public long ID_DESCR;
        [DataMember]
        public int VALUE;
        [DataMember]
        public int? REALIZATION_TIME;
		    
		partial void CloneUser(DB_PRIORITY clone);

		public DB_PRIORITY(DB_PRIORITY clone)
		{
						this.ID_PRIORITY = clone.ID_PRIORITY;
						this.ID_DESCR = clone.ID_DESCR;
						this.VALUE = clone.VALUE;
						this.REALIZATION_TIME = clone.REALIZATION_TIME;
						CloneUser(clone);
		}

		public DB_PRIORITY()
		{
		}
	}
}