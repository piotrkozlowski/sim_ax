﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST_DEVICE_ACTION
    {
        public long SERIAL_NBR;
        public int ID_SERVICE_LIST;
        public long ID_SERVICE_PACKAGE;
        public long? ID_SERVICE_CUSTOM;

        partial void CloneUser(DB_SERVICE_LIST_DEVICE_ACTION clone);

        public DB_SERVICE_LIST_DEVICE_ACTION(DB_SERVICE_LIST_DEVICE_ACTION clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.ID_SERVICE_CUSTOM = clone.ID_SERVICE_CUSTOM;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST_DEVICE_ACTION()
        {
        }
    }
}