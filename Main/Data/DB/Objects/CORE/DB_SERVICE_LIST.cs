﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST
    {
        public int ID_SERVICE_LIST;
        public int ID_DISTRIBUTOR;
        public DateTime START_DATE;
        public DateTime? END_DATE;
        public int ID_OPERATOR_RECEIVER;
        public int ID_OPERATOR_SERVICER;
        public string COURIER_NAME;
        public int PRIORITY;
        public string SHIPPING_LETTER_NBR;
        public DateTime EXPECTED_FINISH_DATE;
        public string SERVICE_NAME;
        public int? ID_DOCUMENT_PACKAGE;
        public bool CONFIRMED;

        partial void CloneUser(DB_SERVICE_LIST clone);

        public DB_SERVICE_LIST(DB_SERVICE_LIST clone)
        {
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            this.ID_OPERATOR_RECEIVER = clone.ID_OPERATOR_RECEIVER;
            this.ID_OPERATOR_SERVICER = clone.ID_OPERATOR_SERVICER;
            this.COURIER_NAME = clone.COURIER_NAME;
            this.PRIORITY = clone.PRIORITY;
            this.SHIPPING_LETTER_NBR = clone.SHIPPING_LETTER_NBR;
            this.EXPECTED_FINISH_DATE = clone.EXPECTED_FINISH_DATE;
            this.SERVICE_NAME = clone.SERVICE_NAME;
            this.ID_DOCUMENT_PACKAGE = clone.ID_DOCUMENT_PACKAGE;
            this.CONFIRMED = clone.CONFIRMED;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST()
        {
        }
    }
}