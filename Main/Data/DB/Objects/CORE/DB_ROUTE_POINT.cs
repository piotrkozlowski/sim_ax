using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_POINT
    {
				public int ID_ROUTE_POINT;
				public long ID_ROUTE;
				public int TYPE;
				public int? ID_TASK;
				public DateTime? START_DATE_TIME;
				public DateTime? END_DATE_TIME;
				public Double LATITUDE;
				public Double LONGITUDE;
				public int? KM_COUNTER;
				public Double? QUOTE;
				public string INVOICE_NUMBER;
				public string NOTES;
				public byte[] SIGNATURE;
				public object ROW_VERSION;
				public int ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_ROUTE_POINT clone);

		public DB_ROUTE_POINT(DB_ROUTE_POINT clone)
		{
						this.ID_ROUTE_POINT = clone.ID_ROUTE_POINT;
						this.ID_ROUTE = clone.ID_ROUTE;
						this.TYPE = clone.TYPE;
						this.ID_TASK = clone.ID_TASK;
						this.START_DATE_TIME = clone.START_DATE_TIME;
						this.END_DATE_TIME = clone.END_DATE_TIME;
						this.LATITUDE = clone.LATITUDE;
						this.LONGITUDE = clone.LONGITUDE;
						this.KM_COUNTER = clone.KM_COUNTER;
						this.QUOTE = clone.QUOTE;
						this.INVOICE_NUMBER = clone.INVOICE_NUMBER;
						this.NOTES = clone.NOTES;
						this.SIGNATURE = clone.SIGNATURE;
						this.ROW_VERSION = clone.ROW_VERSION;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_ROUTE_POINT()
		{
		}
	}
}