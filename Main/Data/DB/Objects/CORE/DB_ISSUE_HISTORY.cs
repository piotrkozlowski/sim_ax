using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ISSUE_HISTORY
    {
				public int ID_ISSUE_HISTORY;
				public int ID_ISSUE;
				public int ID_ISSUE_STATUS;
				public int ID_OPERATOR;
				public DateTime START_DATE;
				public DateTime? END_DATE;
				public string NOTES;
				public string SHORT_DESCR;
				public DateTime? DEADLINE;
				public int? ID_PRIORITY;
				public object ROW_VERSION;
		    
		partial void CloneUser(DB_ISSUE_HISTORY clone);

		public DB_ISSUE_HISTORY(DB_ISSUE_HISTORY clone)
		{
						this.ID_ISSUE_HISTORY = clone.ID_ISSUE_HISTORY;
						this.ID_ISSUE = clone.ID_ISSUE;
						this.ID_ISSUE_STATUS = clone.ID_ISSUE_STATUS;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.START_DATE = clone.START_DATE;
						this.END_DATE = clone.END_DATE;
						this.NOTES = clone.NOTES;
						this.SHORT_DESCR = clone.SHORT_DESCR;
						this.DEADLINE = clone.DEADLINE;
						this.ID_PRIORITY = clone.ID_PRIORITY;
						this.ROW_VERSION = clone.ROW_VERSION;
						CloneUser(clone);
		}

		public DB_ISSUE_HISTORY()
		{
		}
	}
}