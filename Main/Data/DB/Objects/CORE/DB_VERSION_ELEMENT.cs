using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_ELEMENT
    {
		public int ID_VERSION_ELEMENT;
		public int ID_VERSION_ELEMENT_TYPE;
		public int? ID_VERSION_NBR_FORMAT;
		public string NAME;
		public long? ID_DESCR;
		public int? REFERENCE_TYPE;
		public object REFERENCE_VALUE;
		public long? ID_VERSION;
		    
		partial void CloneUser(DB_VERSION_ELEMENT clone);

		public DB_VERSION_ELEMENT(DB_VERSION_ELEMENT clone)
		{
			this.ID_VERSION_ELEMENT = clone.ID_VERSION_ELEMENT;
			this.ID_VERSION_ELEMENT_TYPE = clone.ID_VERSION_ELEMENT_TYPE;
			this.ID_VERSION_NBR_FORMAT = clone.ID_VERSION_NBR_FORMAT;
			this.NAME = clone.NAME;
			this.ID_DESCR = clone.ID_DESCR;
			this.REFERENCE_TYPE = clone.REFERENCE_TYPE;
			this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
			this.ID_VERSION = clone.ID_VERSION;
			CloneUser(clone);
		}

		public DB_VERSION_ELEMENT()
		{
		}
	}
}