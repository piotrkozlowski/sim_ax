﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_VIEW_COLUMN_DATA
    {
        [DataMember]
        public long ID_VIEW_COLUMN_DATA;
        [DataMember]
        public int ID_VIEW_COLUMN;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;

        partial void CloneUser(DB_VIEW_COLUMN_DATA clone);

        public DB_VIEW_COLUMN_DATA(DB_VIEW_COLUMN_DATA clone)
        {
            this.ID_VIEW_COLUMN_DATA = clone.ID_VIEW_COLUMN_DATA;
            this.ID_VIEW_COLUMN = clone.ID_VIEW_COLUMN;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_VIEW_COLUMN_DATA()
        {
        }
    }
}