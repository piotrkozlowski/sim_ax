using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ACTOR
    {
        [DataMember]
		public int ID_ACTOR;
        [DataMember]
        public string NAME;
        [DataMember]
        public string SURNAME;
        [DataMember]
        public string CITY;
        [DataMember]
        public string ADDRESS;
        [DataMember]
        public string POSTCODE;
        [DataMember]
        public string EMAIL;
        [DataMember]
        public string MOBILE;
        [DataMember]
        public string PHONE;
        [DataMember]
        public int ID_LANGUAGE;
        [DataMember]
        public string DESCRIPTION;
        [DataMember]
        public byte[] AVATAR;
		    
		partial void CloneUser(DB_ACTOR clone);

		public DB_ACTOR(DB_ACTOR clone)
		{
						this.ID_ACTOR = clone.ID_ACTOR;
						this.NAME = clone.NAME;
						this.SURNAME = clone.SURNAME;
						this.CITY = clone.CITY;
						this.ADDRESS = clone.ADDRESS;
						this.POSTCODE = clone.POSTCODE;
						this.EMAIL = clone.EMAIL;
						this.MOBILE = clone.MOBILE;
						this.PHONE = clone.PHONE;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.AVATAR = clone.AVATAR;
						CloneUser(clone);
		}

		public DB_ACTOR()
		{
		}
	}
}