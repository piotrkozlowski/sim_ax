using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TASK_HISTORY
    {
				public int ID_TASK_HISTORY;
				public int ID_TASK;
				public int ID_TASK_STATUS;
				public int ID_OPERATOR;
				public DateTime START_DATE;
				public DateTime? END_DATE;
				public string NOTES;
		    
		partial void CloneUser(DB_TASK_HISTORY clone);

		public DB_TASK_HISTORY(DB_TASK_HISTORY clone)
		{
						this.ID_TASK_HISTORY = clone.ID_TASK_HISTORY;
						this.ID_TASK = clone.ID_TASK;
						this.ID_TASK_STATUS = clone.ID_TASK_STATUS;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.START_DATE = clone.START_DATE;
						this.END_DATE = clone.END_DATE;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_TASK_HISTORY()
		{
		}
	}
}