using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ISSUE_STATUS
    {
				public int ID_ISSUE_STATUS;
				public long? ID_DESCR;
				public string DESCRIPTION;
		    
		partial void CloneUser(DB_ISSUE_STATUS clone);

		public DB_ISSUE_STATUS(DB_ISSUE_STATUS clone)
		{
						this.ID_ISSUE_STATUS = clone.ID_ISSUE_STATUS;
						this.ID_DESCR = clone.ID_DESCR;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_ISSUE_STATUS()
		{
		}
	}
}