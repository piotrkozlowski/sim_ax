using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LANGUAGE
    {
				public int ID_LANGUAGE;
				public string NAME;
				public string CULTURE_CODE;
				public int CULTURE_LANGID;
		    
		partial void CloneUser(DB_LANGUAGE clone);

		public DB_LANGUAGE(DB_LANGUAGE clone)
		{
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.NAME = clone.NAME;
						this.CULTURE_CODE = clone.CULTURE_CODE;
						this.CULTURE_LANGID = clone.CULTURE_LANGID;
						CloneUser(clone);
		}

		public DB_LANGUAGE()
		{
		}
	}
}