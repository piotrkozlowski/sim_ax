using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ROLE
    {
        public int ID_ROLE;
        public string NAME;
        public int? ID_MODULE;
        public long? ID_DESCR;

        partial void CloneUser(DB_ROLE clone);

        public DB_ROLE(DB_ROLE clone)
        {
            this.ID_ROLE = clone.ID_ROLE;
            this.NAME = clone.NAME;
            this.ID_MODULE = clone.ID_MODULE;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_ROLE()
        {
        }
    }
}