using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_PROFILE
    {
        [DataMember]
        public int ID_PROFILE;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public int? ID_MODULE;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_PROFILE clone);

		public DB_PROFILE(DB_PROFILE clone)
		{
						this.ID_PROFILE = clone.ID_PROFILE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_MODULE = clone.ID_MODULE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_PROFILE()
		{
		}
	}
}