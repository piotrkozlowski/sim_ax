using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_PROFILE_MAP
    {
				public int ID_DEVICE_TYPE_PROFILE_MAP;
				public int ID_DEVICE_TYPE;
				public int ID_OPERATOR_CREATED;
				public DateTime CREATED_TIME;
				public int? ID_OPERATOR_MODIFIED;
				public DateTime? MODIFIED_TIME;
		    
		partial void CloneUser(DB_DEVICE_TYPE_PROFILE_MAP clone);

		public DB_DEVICE_TYPE_PROFILE_MAP(DB_DEVICE_TYPE_PROFILE_MAP clone)
		{
						this.ID_DEVICE_TYPE_PROFILE_MAP = clone.ID_DEVICE_TYPE_PROFILE_MAP;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						this.ID_OPERATOR_CREATED = clone.ID_OPERATOR_CREATED;
						this.CREATED_TIME = clone.CREATED_TIME;
						this.ID_OPERATOR_MODIFIED = clone.ID_OPERATOR_MODIFIED;
						this.MODIFIED_TIME = clone.MODIFIED_TIME;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_PROFILE_MAP()
		{
		}
	}
}