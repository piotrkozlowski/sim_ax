using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_NOTIFICATION_TYPE
    {
				public int ID_NOTIFICATION_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_NOTIFICATION_TYPE clone);

		public DB_NOTIFICATION_TYPE(DB_NOTIFICATION_TYPE clone)
		{
						this.ID_NOTIFICATION_TYPE = clone.ID_NOTIFICATION_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_NOTIFICATION_TYPE()
		{
		}
	}
}