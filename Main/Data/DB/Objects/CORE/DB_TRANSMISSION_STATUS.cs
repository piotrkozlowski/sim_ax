using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TRANSMISSION_STATUS
    {
				public int ID_TRANSMISSION_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_TRANSMISSION_STATUS clone);

		public DB_TRANSMISSION_STATUS(DB_TRANSMISSION_STATUS clone)
		{
						this.ID_TRANSMISSION_STATUS = clone.ID_TRANSMISSION_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_TRANSMISSION_STATUS()
		{
		}
	}
}