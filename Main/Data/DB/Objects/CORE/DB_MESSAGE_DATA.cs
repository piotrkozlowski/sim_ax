﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_MESSAGE_DATA
    {
        public long ID_MESSAGE_DATA;
        public long ID_MESSAGE;
        public int INDEX_NBR;
        public long ID_DATA_TYPE;
        public object VALUE;

        partial void CloneUser(DB_MESSAGE_DATA clone);

        public DB_MESSAGE_DATA(DB_MESSAGE_DATA clone)
        {
            this.ID_MESSAGE_DATA = clone.ID_MESSAGE_DATA;
            this.ID_MESSAGE = clone.ID_MESSAGE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_MESSAGE_DATA()
        {
        }
    }
}