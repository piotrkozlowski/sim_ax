using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_HIERARCHY_PATTERN
    {
				public long ID_DEVICE_HIERARCHY_PATTERN;
				public int ID_DEVICE_TYPE_PARENT;
				public int ID_SLOT_TYPE;
				public int SLOT_NBR_MIN;
				public int SLOT_NBR_MAX;
				public int? ID_PROTOCOL_IN;
				public int? ID_PROTOCOL_OUT;
				public int? ID_DEVICE_TYPE;
				public int? ID_DEVICE_TYPE_GROUP;
                public int? ID_SERVER;
		    
		partial void CloneUser(DB_DEVICE_HIERARCHY_PATTERN clone);

		public DB_DEVICE_HIERARCHY_PATTERN(DB_DEVICE_HIERARCHY_PATTERN clone)
		{
						this.ID_DEVICE_HIERARCHY_PATTERN = clone.ID_DEVICE_HIERARCHY_PATTERN;
						this.ID_DEVICE_TYPE_PARENT = clone.ID_DEVICE_TYPE_PARENT;
						this.ID_SLOT_TYPE = clone.ID_SLOT_TYPE;
						this.SLOT_NBR_MIN = clone.SLOT_NBR_MIN;
						this.SLOT_NBR_MAX = clone.SLOT_NBR_MAX;
						this.ID_PROTOCOL_IN = clone.ID_PROTOCOL_IN;
						this.ID_PROTOCOL_OUT = clone.ID_PROTOCOL_OUT;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						this.ID_DEVICE_TYPE_GROUP = clone.ID_DEVICE_TYPE_GROUP;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_DEVICE_HIERARCHY_PATTERN()
		{
		}
	}
}