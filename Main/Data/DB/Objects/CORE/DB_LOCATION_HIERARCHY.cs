using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_LOCATION_HIERARCHY
    {
        [DataMember]
        public long ID_LOCATION_HIERARCHY;
        [DataMember]
        public long? ID_LOCATION_HIERARCHY_PARENT;
        [DataMember]
        public long ID_LOCATION;
        [DataMember]
		public string HIERARCHY;
		    
		partial void CloneUser(DB_LOCATION_HIERARCHY clone);

		public DB_LOCATION_HIERARCHY(DB_LOCATION_HIERARCHY clone)
		{
						this.ID_LOCATION_HIERARCHY = clone.ID_LOCATION_HIERARCHY;
						this.ID_LOCATION_HIERARCHY_PARENT = clone.ID_LOCATION_HIERARCHY_PARENT;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.HIERARCHY = clone.HIERARCHY;
						CloneUser(clone);
		}

		public DB_LOCATION_HIERARCHY()
		{
		}
	}
}