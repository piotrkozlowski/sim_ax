using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_OPERATOR_ACTIVITY
    {
				public long ID_OPERATOR_ACTIVITY;
				public int? ID_OPERATOR;
				public int ID_ACTIVITY;
				public object REFERENCE_VALUE;
				public bool DENY;
                public int? ID_MODULE;
		    
		partial void CloneUser(DB_OPERATOR_ACTIVITY clone);

		public DB_OPERATOR_ACTIVITY(DB_OPERATOR_ACTIVITY clone)
		{
						this.ID_OPERATOR_ACTIVITY = clone.ID_OPERATOR_ACTIVITY;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_ACTIVITY = clone.ID_ACTIVITY;
                        this.ID_MODULE = clone.ID_MODULE;
						this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
						this.DENY = clone.DENY;
						CloneUser(clone);
		}

		public DB_OPERATOR_ACTIVITY()
		{
		}
	}
}