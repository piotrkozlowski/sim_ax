using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DEF_DATA
    {
				public int ID_ROUTE_DEF;
				public long ID_DATA_TYPE;
				public object VALUE;
                public int INDEX_NBR;
		    
		partial void CloneUser(DB_ROUTE_DEF_DATA clone);

		public DB_ROUTE_DEF_DATA(DB_ROUTE_DEF_DATA clone)
		{
						this.ID_ROUTE_DEF = clone.ID_ROUTE_DEF;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
                        this.INDEX_NBR = clone.INDEX_NBR;
						CloneUser(clone);
		}

		public DB_ROUTE_DEF_DATA()
		{
		}
	}
}