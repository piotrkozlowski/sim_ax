using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ORDER
    {
				public int ID_ORDER;
				public int ID_ORDER_STATE_TYPE;
				public int ID_OPERATOR;
		    
		partial void CloneUser(DB_ORDER clone);

		public DB_ORDER(DB_ORDER clone)
		{
						this.ID_ORDER = clone.ID_ORDER;
						this.ID_ORDER_STATE_TYPE = clone.ID_ORDER_STATE_TYPE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						CloneUser(clone);
		}

		public DB_ORDER()
		{
		}
	}
}