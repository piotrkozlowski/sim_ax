using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ALARM_DATA
    {
        [DataMember]
        public long ID_ALARM_DATA;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
		    
		partial void CloneUser(DB_ALARM_DATA clone);

		public DB_ALARM_DATA(DB_ALARM_DATA clone)
		{
						this.ID_ALARM_DATA = clone.ID_ALARM_DATA;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ALARM_DATA()
		{
		}
	}
}