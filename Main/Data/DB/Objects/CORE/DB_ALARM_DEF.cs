using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_DEF
    {
				public long ID_ALARM_DEF;
				public string NAME;
				public long? SERIAL_NBR;
				public long? ID_METER;
				public long? ID_LOCATION;
				public int ID_ALARM_TYPE;
				public long ID_DATA_TYPE_ALARM;
				public long? ID_DATA_TYPE_CONFIG;
				public object SYSTEM_ALARM_VALUE;
				public bool IS_ENABLED;
				public long? ID_ALARM_TEXT;
				public long? ID_ALARM_DATA;
				public bool CHECK_LAST_ALARM;
				public int SUSPENSION_TIME;
				public bool IS_CONFIRMABLE;
                public int INDEX_NBR;
		    
		partial void CloneUser(DB_ALARM_DEF clone);

		public DB_ALARM_DEF(DB_ALARM_DEF clone)
		{
						this.ID_ALARM_DEF = clone.ID_ALARM_DEF;
						this.NAME = clone.NAME;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_ALARM_TYPE = clone.ID_ALARM_TYPE;
						this.ID_DATA_TYPE_ALARM = clone.ID_DATA_TYPE_ALARM;
						this.ID_DATA_TYPE_CONFIG = clone.ID_DATA_TYPE_CONFIG;
						this.SYSTEM_ALARM_VALUE = clone.SYSTEM_ALARM_VALUE;
						this.IS_ENABLED = clone.IS_ENABLED;
						this.ID_ALARM_TEXT = clone.ID_ALARM_TEXT;
						this.ID_ALARM_DATA = clone.ID_ALARM_DATA;
						this.CHECK_LAST_ALARM = clone.CHECK_LAST_ALARM;
						this.SUSPENSION_TIME = clone.SUSPENSION_TIME;
						this.IS_CONFIRMABLE = clone.IS_CONFIRMABLE;
                        this.INDEX_NBR = clone.INDEX_NBR;
						CloneUser(clone);
		}

		public DB_ALARM_DEF()
		{
		}
	}
}