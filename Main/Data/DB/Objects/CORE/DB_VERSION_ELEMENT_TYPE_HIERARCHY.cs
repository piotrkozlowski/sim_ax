using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_ELEMENT_TYPE_HIERARCHY
    {
		public int ID_VERSION_ELEMENT_TYPE_HIERARCHY;
		public int? ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT;
		public int ID_VERSION_ELEMENT_TYPE;
		public bool REQUIRED;
		public string HIERARCHY;
		    
		partial void CloneUser(DB_VERSION_ELEMENT_TYPE_HIERARCHY clone);

		public DB_VERSION_ELEMENT_TYPE_HIERARCHY(DB_VERSION_ELEMENT_TYPE_HIERARCHY clone)
		{
			this.ID_VERSION_ELEMENT_TYPE_HIERARCHY = clone.ID_VERSION_ELEMENT_TYPE_HIERARCHY;
			this.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT = clone.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT;
			this.ID_VERSION_ELEMENT_TYPE = clone.ID_VERSION_ELEMENT_TYPE;
			this.REQUIRED = clone.REQUIRED;
			this.HIERARCHY = clone.HIERARCHY;
			CloneUser(clone);
		}

		public DB_VERSION_ELEMENT_TYPE_HIERARCHY()
		{
		}
	}
}