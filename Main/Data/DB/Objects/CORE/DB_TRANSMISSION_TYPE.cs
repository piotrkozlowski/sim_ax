using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_TRANSMISSION_TYPE
    {
        [DataMember]
        public int ID_TRANSMISSION_TYPE;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;
		    
		partial void CloneUser(DB_TRANSMISSION_TYPE clone);

		public DB_TRANSMISSION_TYPE(DB_TRANSMISSION_TYPE clone)
		{
						this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_TRANSMISSION_TYPE()
		{
		}
	}
}