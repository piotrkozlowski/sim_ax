﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_AUDIT
    {
        public long ID_AUDIT;
        public long BATCH_ID;
        public int CHANGE_TYPE;
        public string TABLE_NAME;
        public long? KEY1;
        public long? KEY2;
        public long? KEY3;
        public long? KEY4;
        public long? KEY5;
        public long? KEY6;
        public string COLUMN_NAME;
        public object OLD_VALUE;
        public object NEW_VALUE;
        public DateTime TIME;
        public string USER;
        public string HOST;
        public string APPLICATION;
        public string CONTEXTINFO;

        partial void CloneUser(DB_AUDIT clone);

        public DB_AUDIT(DB_AUDIT clone)
        {
            this.ID_AUDIT = clone.ID_AUDIT;
            this.BATCH_ID = clone.BATCH_ID;
            this.CHANGE_TYPE = clone.CHANGE_TYPE;
            this.TABLE_NAME = clone.TABLE_NAME;
            this.KEY1 = clone.KEY1;
            this.KEY2 = clone.KEY2;
            this.KEY3 = clone.KEY3;
            this.KEY4 = clone.KEY4;
            this.KEY5 = clone.KEY5;
            this.KEY6 = clone.KEY6;
            this.COLUMN_NAME = clone.COLUMN_NAME;
            this.OLD_VALUE = clone.OLD_VALUE;
            this.NEW_VALUE = clone.NEW_VALUE;
            this.TIME = clone.TIME;
            this.USER = clone.USER;
            this.HOST = clone.HOST;
            this.APPLICATION = clone.APPLICATION;
            this.CONTEXTINFO = clone.CONTEXTINFO;
            CloneUser(clone);
        }

        public DB_AUDIT()
        {
        }
    }
}
