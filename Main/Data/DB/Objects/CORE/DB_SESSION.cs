using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SESSION
    {
				public long ID_SESSION;
				public Guid SESSION_GUID;
				public int ID_MODULE;
				public int? ID_OPERATOR;
				public int ID_SESSION_STATUS;
		    
		partial void CloneUser(DB_SESSION clone);

		public DB_SESSION(DB_SESSION clone)
		{
						this.ID_SESSION = clone.ID_SESSION;
						this.SESSION_GUID = clone.SESSION_GUID;
						this.ID_MODULE = clone.ID_MODULE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_SESSION_STATUS = clone.ID_SESSION_STATUS;
						CloneUser(clone);
		}

		public DB_SESSION()
		{
		}
	}
}