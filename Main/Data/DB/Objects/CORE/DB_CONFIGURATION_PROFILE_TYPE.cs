using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONFIGURATION_PROFILE_TYPE
    {
				public int ID_CONFIGURATION_PROFILE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_CONFIGURATION_PROFILE_TYPE clone);

		public DB_CONFIGURATION_PROFILE_TYPE(DB_CONFIGURATION_PROFILE_TYPE clone)
		{
						this.ID_CONFIGURATION_PROFILE_TYPE = clone.ID_CONFIGURATION_PROFILE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_CONFIGURATION_PROFILE_TYPE()
		{
		}
	}
}