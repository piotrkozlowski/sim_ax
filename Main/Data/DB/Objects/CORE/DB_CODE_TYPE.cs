using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CODE_TYPE
    {
				public int ID_CODE_TYPE;
				public string NAME;
				public long? ID_DESCR;
				public int? ID_DATA_TYPE_FORMAT;
		    
		partial void CloneUser(DB_CODE_TYPE clone);

		public DB_CODE_TYPE(DB_CODE_TYPE clone)
		{
						this.ID_CODE_TYPE = clone.ID_CODE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_DATA_TYPE_FORMAT = clone.ID_DATA_TYPE_FORMAT;
						CloneUser(clone);
		}

		public DB_CODE_TYPE()
		{
		}
	}
}