﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_IN_PACKAGE
    {
        public long ID_SERVICE_IN_PACKAGE;
        public long ID_SERVICE_PACKAGE;
        public int ID_SERVICE;

        partial void CloneUser(DB_SERVICE_IN_PACKAGE clone);

        public DB_SERVICE_IN_PACKAGE(DB_SERVICE_IN_PACKAGE clone)
        {
            this.ID_SERVICE_IN_PACKAGE = clone.ID_SERVICE_IN_PACKAGE;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.ID_SERVICE = clone.ID_SERVICE;
            CloneUser(clone);
        }

        public DB_SERVICE_IN_PACKAGE()
        {
        }
    }
}