using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ACTIVITY
    {
        [DataMember]
				public int ID_ACTIVITY;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public int? ID_REFERENCE_TYPE;
		    
		partial void CloneUser(DB_ACTIVITY clone);

		public DB_ACTIVITY(DB_ACTIVITY clone)
		{
						this.ID_ACTIVITY = clone.ID_ACTIVITY;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						CloneUser(clone);
		}

		public DB_ACTIVITY()
		{
		}
	}
}