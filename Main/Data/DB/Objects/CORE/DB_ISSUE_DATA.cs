using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ISSUE_DATA
    {
				public long ID_ISSUE_DATA;
				public int ID_ISSUE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_ISSUE_DATA clone);

		public DB_ISSUE_DATA(DB_ISSUE_DATA clone)
		{
						this.ID_ISSUE_DATA = clone.ID_ISSUE_DATA;
						this.ID_ISSUE = clone.ID_ISSUE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ISSUE_DATA()
		{
		}
	}
}