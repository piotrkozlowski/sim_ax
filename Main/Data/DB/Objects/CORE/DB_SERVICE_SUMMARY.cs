﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_SUMMARY
    {
        public int ID_SERVICE_LIST;
        public long ID_SERVICE_PACKAGE;
        public int AMOUNT;
        public Double COST;

        partial void CloneUser(DB_SERVICE_SUMMARY clone);

        public DB_SERVICE_SUMMARY(DB_SERVICE_SUMMARY clone)
        {
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.AMOUNT = clone.AMOUNT;
            this.COST = clone.COST;
            CloneUser(clone);
        }

        public DB_SERVICE_SUMMARY()
        {
        }
    }
}