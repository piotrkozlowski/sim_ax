using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_METER_TYPE_DEVICE_TYPE
    {
				public int ID_METER_TYPE;
				public int ID_DEVICE_TYPE;
		    
		partial void CloneUser(DB_METER_TYPE_DEVICE_TYPE clone);

		public DB_METER_TYPE_DEVICE_TYPE(DB_METER_TYPE_DEVICE_TYPE clone)
		{
						this.ID_METER_TYPE = clone.ID_METER_TYPE;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						CloneUser(clone);
		}

		public DB_METER_TYPE_DEVICE_TYPE()
		{
		}
	}
}