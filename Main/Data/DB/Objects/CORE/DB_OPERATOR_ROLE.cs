using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_OPERATOR_ROLE
    {
				public int ID_OPERATOR;
				public int ID_ROLE;
		    
		partial void CloneUser(DB_OPERATOR_ROLE clone);

		public DB_OPERATOR_ROLE(DB_OPERATOR_ROLE clone)
		{
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_ROLE = clone.ID_ROLE;
						CloneUser(clone);
		}

		public DB_OPERATOR_ROLE()
		{
		}
	}
}