using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ARTICLE
    {
        public long ID_ARTICLE;
        public long? EXTERNAL_ID;
        public string NAME;
        public string DESCRIPTION;
        public bool IS_UNIQUE;
        public bool? VISIBLE;
        public bool IS_AIUT;

        partial void CloneUser(DB_ARTICLE clone);

        public DB_ARTICLE(DB_ARTICLE clone)
        {
            this.ID_ARTICLE = clone.ID_ARTICLE;
            this.EXTERNAL_ID = clone.EXTERNAL_ID;
            this.NAME = clone.NAME;
            this.DESCRIPTION = clone.DESCRIPTION;
            this.IS_UNIQUE = clone.IS_UNIQUE;
            this.VISIBLE = clone.VISIBLE;
            this.IS_AIUT = clone.IS_AIUT;
            CloneUser(clone);
        }

        public DB_ARTICLE()
        {
        }
    }
}