using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_COMMAND_CODE_PARAM_MAP
    {
        public long ID_COMMAND_CODE_PARAM_MAP;
        public long ID_COMMAND_CODE;
        public long ARG_NBR;
        public string NAME;
        public int BYTE_OFFSET;
        public int BIT_OFFSET;
        public int LENGTH;
        public int ID_DATA_TYPE_CLASS;
        public int ID_UNIT;
        public long? ID_DESCR;
        public int? ID_PROTOCOL;
        public bool IS_REQUIRED;
        public int? ID_SERVER;

        partial void CloneUser(DB_COMMAND_CODE_PARAM_MAP clone);

        public DB_COMMAND_CODE_PARAM_MAP(DB_COMMAND_CODE_PARAM_MAP clone)
        {
            this.ID_COMMAND_CODE_PARAM_MAP = clone.ID_COMMAND_CODE_PARAM_MAP;
            this.ID_COMMAND_CODE = clone.ID_COMMAND_CODE;
            this.ARG_NBR = clone.ARG_NBR;
            this.NAME = clone.NAME;
            this.BYTE_OFFSET = clone.BYTE_OFFSET;
            this.BIT_OFFSET = clone.BIT_OFFSET;
            this.LENGTH = clone.LENGTH;
            this.ID_DATA_TYPE_CLASS = clone.ID_DATA_TYPE_CLASS;
            this.ID_UNIT = clone.ID_UNIT;
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_SERVER = clone.ID_SERVER;
            this.ID_PROTOCOL = clone.ID_PROTOCOL;
            this.IS_REQUIRED = clone.IS_REQUIRED;
            CloneUser(clone);
        }

        public DB_COMMAND_CODE_PARAM_MAP()
        {
        }
    }
}