using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_DATA
    {
				public long ID_ACTION_DATA;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_ACTION_DATA clone);

		public DB_ACTION_DATA(DB_ACTION_DATA clone)
		{
						this.ID_ACTION_DATA = clone.ID_ACTION_DATA;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_ACTION_DATA()
		{
		}
	}
}