using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_PROFILE_STEP
    {
				public int ID_DEVICE_TYPE_PROFILE_STEP;
				public string NAME;
		    
		partial void CloneUser(DB_DEVICE_TYPE_PROFILE_STEP clone);

		public DB_DEVICE_TYPE_PROFILE_STEP(DB_DEVICE_TYPE_PROFILE_STEP clone)
		{
						this.ID_DEVICE_TYPE_PROFILE_STEP = clone.ID_DEVICE_TYPE_PROFILE_STEP;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_PROFILE_STEP()
		{
		}
	}
}