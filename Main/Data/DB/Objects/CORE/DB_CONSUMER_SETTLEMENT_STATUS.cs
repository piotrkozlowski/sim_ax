using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_SETTLEMENT_STATUS
    {
				public int ID_CONSUMER_SETTLEMENT_STATUS;
				public string NAME;
				public bool IS_ERROR;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_CONSUMER_SETTLEMENT_STATUS clone);

		public DB_CONSUMER_SETTLEMENT_STATUS(DB_CONSUMER_SETTLEMENT_STATUS clone)
		{
						this.ID_CONSUMER_SETTLEMENT_STATUS = clone.ID_CONSUMER_SETTLEMENT_STATUS;
						this.NAME = clone.NAME;
						this.IS_ERROR = clone.IS_ERROR;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_CONSUMER_SETTLEMENT_STATUS()
		{
		}
	}
}