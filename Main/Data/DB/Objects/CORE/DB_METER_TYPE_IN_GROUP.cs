using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_METER_TYPE_IN_GROUP
    {
				public int ID_METER_TYPE_GROUP;
				public int ID_METER_TYPE;
		    
		partial void CloneUser(DB_METER_TYPE_IN_GROUP clone);

		public DB_METER_TYPE_IN_GROUP(DB_METER_TYPE_IN_GROUP clone)
		{
						this.ID_METER_TYPE_GROUP = clone.ID_METER_TYPE_GROUP;
						this.ID_METER_TYPE = clone.ID_METER_TYPE;
						CloneUser(clone);
		}

		public DB_METER_TYPE_IN_GROUP()
		{
		}
	}
}