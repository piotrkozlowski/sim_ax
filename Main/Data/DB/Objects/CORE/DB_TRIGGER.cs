﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_TRIGGER
    {
        public long ID_TRIGGER;
        public string TABLE_NAME;
        public string AUDIT;
        public string DATA_CHANGE;
        public string DICT_TABLE;
        public string OBJECT_TABLE;
        public string DATA_TABLE;

        partial void CloneUser(DB_TRIGGER clone);

        public DB_TRIGGER(DB_TRIGGER clone)
        {
            this.ID_TRIGGER = clone.ID_TRIGGER;
            this.TABLE_NAME = clone.TABLE_NAME;
            this.AUDIT = clone.AUDIT;
            this.DATA_CHANGE = clone.DATA_CHANGE;
            this.DICT_TABLE = clone.DICT_TABLE;
            this.OBJECT_TABLE = clone.OBJECT_TABLE;
            this.DATA_TABLE = clone.DATA_TABLE;
            CloneUser(clone);
        }

        public DB_TRIGGER()
        {
        }
    }
}
