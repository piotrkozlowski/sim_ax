using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_POINT_DATA
    {
				public long ID_ROUTE_POINT_DATA;
				public int ID_ROUTE_POINT;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_ROUTE_POINT_DATA clone);

		public DB_ROUTE_POINT_DATA(DB_ROUTE_POINT_DATA clone)
		{
						this.ID_ROUTE_POINT_DATA = clone.ID_ROUTE_POINT_DATA;
						this.ID_ROUTE_POINT = clone.ID_ROUTE_POINT;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ROUTE_POINT_DATA()
		{
		}
	}
}