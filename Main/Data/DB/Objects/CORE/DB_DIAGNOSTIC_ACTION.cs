﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DIAGNOSTIC_ACTION
    {
        public int ID_DIAGNOSTIC_ACTION;
        public int? ID_DEVICE_ORDER_NUMBER;
        public int? ID_DISTRIBUTOR;
        public string NAME;
        public DateTime START_DATE;
        public DateTime? END_DATE;
        public long? ID_DESCR;

        partial void CloneUser(DB_DIAGNOSTIC_ACTION clone);

        public DB_DIAGNOSTIC_ACTION(DB_DIAGNOSTIC_ACTION clone)
        {
            this.ID_DIAGNOSTIC_ACTION = clone.ID_DIAGNOSTIC_ACTION;
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.NAME = clone.NAME;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_DIAGNOSTIC_ACTION()
        {
        }
    }
}