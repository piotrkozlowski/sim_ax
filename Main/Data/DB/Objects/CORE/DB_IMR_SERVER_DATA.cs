﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_IMR_SERVER_DATA
    {
        [DataMember]
        public long ID_IMR_SERVER_DATA;
        [DataMember]
        public int ID_IMR_SERVER;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;

        partial void CloneUser(DB_IMR_SERVER_DATA clone);

        public DB_IMR_SERVER_DATA(DB_IMR_SERVER_DATA clone)
        {
            this.ID_IMR_SERVER_DATA = clone.ID_IMR_SERVER_DATA;
            this.ID_IMR_SERVER = clone.ID_IMR_SERVER;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_IMR_SERVER_DATA()
        {
        }
    }
}