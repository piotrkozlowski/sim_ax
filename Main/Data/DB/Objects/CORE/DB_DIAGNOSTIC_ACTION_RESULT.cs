﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DIAGNOSTIC_ACTION_RESULT
    {
        public int ID_DIAGNOSTIC_ACTION_RESULT;
        public int ID_DIAGNOSTIC_ACTION;
        public string NAME;
        public bool ALLOW_INPUT_TEXT;
        public int DAMAGE_LEVEL;
        public long? ID_DESCR;
        public int? ID_SERVICE_REFERENCE_TYPE;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_DIAGNOSTIC_ACTION_RESULT clone);

        public DB_DIAGNOSTIC_ACTION_RESULT(DB_DIAGNOSTIC_ACTION_RESULT clone)
        {
            this.ID_DIAGNOSTIC_ACTION_RESULT = clone.ID_DIAGNOSTIC_ACTION_RESULT;
            this.ID_DIAGNOSTIC_ACTION = clone.ID_DIAGNOSTIC_ACTION;
            this.NAME = clone.NAME;
            this.ALLOW_INPUT_TEXT = clone.ALLOW_INPUT_TEXT;
            this.DAMAGE_LEVEL = clone.DAMAGE_LEVEL;
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_SERVICE_REFERENCE_TYPE = clone.ID_SERVICE_REFERENCE_TYPE;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_DIAGNOSTIC_ACTION_RESULT()
        {
        }
    }
}