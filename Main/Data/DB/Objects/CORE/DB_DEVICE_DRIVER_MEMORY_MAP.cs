using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_DRIVER_MEMORY_MAP
    {
        public int ID_DEVICE_DRIVER_MEMORY_MAP;
        public int ID_DEVICE_DRIVER;
        public string FILE_NAME;
        public string FILE_VERSION;
        public int? ID_SERVER;

        partial void CloneUser(DB_DEVICE_DRIVER_MEMORY_MAP clone);

        public DB_DEVICE_DRIVER_MEMORY_MAP(DB_DEVICE_DRIVER_MEMORY_MAP clone)
        {
            this.ID_DEVICE_DRIVER_MEMORY_MAP = clone.ID_DEVICE_DRIVER_MEMORY_MAP;
            this.ID_DEVICE_DRIVER = clone.ID_DEVICE_DRIVER;
            this.FILE_NAME = clone.FILE_NAME;
            this.FILE_VERSION = clone.FILE_VERSION;
            this.ID_SERVER = clone.ID_SERVER;
            CloneUser(clone);
        }

        public DB_DEVICE_DRIVER_MEMORY_MAP()
        {
        }
    }
}