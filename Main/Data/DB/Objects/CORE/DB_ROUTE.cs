using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ROUTE
    {
                [DataMember]
				public long ID_ROUTE;
                [DataMember]
				public int? ID_ROUTE_DEF;
                [DataMember]
				public int YEAR;
                [DataMember]
				public int MONTH;
                [DataMember]
				public int WEEK;
                [DataMember]
				public int ID_OPERATOR_EXECUTOR;
                [DataMember]
				public int? ID_OPERATOR_APPROVED;
                [DataMember]
				public DateTime? DATE_UPLOADED;
                [DataMember]
				public DateTime? DATE_APPROVED;
                [DataMember]
				public DateTime? DATE_FINISHED;
                [DataMember]
				public int ID_ROUTE_STATUS;
                [DataMember]
				public object ROW_VERSION;
                [DataMember]
				public int ID_DISTRIBUTOR;
                [DataMember]
                public int ID_ROUTE_TYPE;
                [DataMember]
                public DateTime? CREATION_DATE;
                [DataMember]
                public DateTime? EXPIRATION_DATE;
                [DataMember]
                public string NAME;
		    
		partial void CloneUser(DB_ROUTE clone);

		public DB_ROUTE(DB_ROUTE clone)
		{
						this.ID_ROUTE = clone.ID_ROUTE;
						this.ID_ROUTE_DEF = clone.ID_ROUTE_DEF;
						this.YEAR = clone.YEAR;
						this.MONTH = clone.MONTH;
						this.WEEK = clone.WEEK;
						this.ID_OPERATOR_EXECUTOR = clone.ID_OPERATOR_EXECUTOR;
						this.ID_OPERATOR_APPROVED = clone.ID_OPERATOR_APPROVED;
						this.DATE_UPLOADED = clone.DATE_UPLOADED;
						this.DATE_APPROVED = clone.DATE_APPROVED;
						this.DATE_FINISHED = clone.DATE_FINISHED;
						this.ID_ROUTE_STATUS = clone.ID_ROUTE_STATUS;
						this.ROW_VERSION = clone.ROW_VERSION;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
                        this.CREATION_DATE = clone.CREATION_DATE;
                        this.EXPIRATION_DATE = clone.EXPIRATION_DATE;
                        this.ID_ROUTE_TYPE = clone.ID_ROUTE_TYPE;
            this.NAME = clone.NAME;
            CloneUser(clone);
		}

		public DB_ROUTE()
		{
		}
	}
}