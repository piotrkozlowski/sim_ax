﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ARTICLE_DATA
    {
        public long ID_ARTICLE_DATA;
        public long ID_ARTICLE;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_ARTICLE_DATA clone);

        public DB_ARTICLE_DATA(DB_ARTICLE_DATA clone)
        {
            this.ID_ARTICLE_DATA = clone.ID_ARTICLE_DATA;
            this.ID_ARTICLE = clone.ID_ARTICLE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_ARTICLE_DATA()
        {
        }
    }
}