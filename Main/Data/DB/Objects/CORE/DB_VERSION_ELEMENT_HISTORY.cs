using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_ELEMENT_HISTORY
    {
		public long ID_VERSION_ELEMENT_HISTORY;
		public int ID_VERSION_ELEMENT;
		public long ID_VERSION;
		public DateTime START_TIME;
		public DateTime? END_TIME;
		public int ID_OPERATOR;
		    
		partial void CloneUser(DB_VERSION_ELEMENT_HISTORY clone);

		public DB_VERSION_ELEMENT_HISTORY(DB_VERSION_ELEMENT_HISTORY clone)
		{
			this.ID_VERSION_ELEMENT_HISTORY = clone.ID_VERSION_ELEMENT_HISTORY;
			this.ID_VERSION_ELEMENT = clone.ID_VERSION_ELEMENT;
			this.ID_VERSION = clone.ID_VERSION;
			this.START_TIME = clone.START_TIME;
			this.END_TIME = clone.END_TIME;
			this.ID_OPERATOR = clone.ID_OPERATOR;
			CloneUser(clone);
		}

		public DB_VERSION_ELEMENT_HISTORY()
		{
		}
	}
}