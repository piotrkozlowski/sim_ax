using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_SCHEDULE_ACTION
    {
        [DataMember]
        public long ID_SCHEDULE_ACTION;
        [DataMember]
        public int ID_SCHEDULE;
        [DataMember]
        public long ID_ACTION_DEF;
        [DataMember]
        public bool IS_ENABLED;
        [DataMember]
        public int? ID_OPERATOR;
        [DataMember]
        public int? ID_PROFILE;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_SCHEDULE_ACTION clone);

		public DB_SCHEDULE_ACTION(DB_SCHEDULE_ACTION clone)
		{
                        this.ID_SCHEDULE_ACTION = clone.ID_SCHEDULE_ACTION;
						this.ID_SCHEDULE = clone.ID_SCHEDULE;
						this.ID_ACTION_DEF = clone.ID_ACTION_DEF;
						this.IS_ENABLED = clone.IS_ENABLED;
                        this.ID_OPERATOR = clone.ID_OPERATOR;
                        this.ID_PROFILE = clone.ID_PROFILE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_SCHEDULE_ACTION()
		{
		}
	}
}