using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_METER_TYPE
    {
				public int ID_METER_TYPE;
				public string NAME;
				public long? ID_DESCR;
				public int ID_METER_TYPE_CLASS;
		    
		partial void CloneUser(DB_METER_TYPE clone);

		public DB_METER_TYPE(DB_METER_TYPE clone)
		{
						this.ID_METER_TYPE = clone.ID_METER_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_METER_TYPE_CLASS = clone.ID_METER_TYPE_CLASS;
						CloneUser(clone);
		}

		public DB_METER_TYPE()
		{
		}
	}
}