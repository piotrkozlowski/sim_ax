using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_DRIVER
    {
        public int ID_DEVICE_DRIVER;
        public string NAME;
        public long? ID_DESCR;
        public string PLUGIN_NAME;
        public bool AUTO_UPDATE;

        partial void CloneUser(DB_DEVICE_DRIVER clone);

        public DB_DEVICE_DRIVER(DB_DEVICE_DRIVER clone)
        {
            this.ID_DEVICE_DRIVER = clone.ID_DEVICE_DRIVER;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            this.PLUGIN_NAME = clone.PLUGIN_NAME;
            this.AUTO_UPDATE = clone.AUTO_UPDATE;
            CloneUser(clone);
        }

        public DB_DEVICE_DRIVER()
        {
        }
    }
}