using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DISTRIBUTOR
    {
        [DataMember]
				public int ID_DISTRIBUTOR;
        [DataMember]
        public string NAME;
        [DataMember]
        public string CITY;
        [DataMember]
        public string ADDRESS;
        [DataMember]
        public long? ID_DESCR;
		    
		partial void CloneUser(DB_DISTRIBUTOR clone);

		public DB_DISTRIBUTOR(DB_DISTRIBUTOR clone)
		{
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.NAME = clone.NAME;
						this.CITY = clone.CITY;
						this.ADDRESS = clone.ADDRESS;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_DISTRIBUTOR()
		{
		}
	}
}