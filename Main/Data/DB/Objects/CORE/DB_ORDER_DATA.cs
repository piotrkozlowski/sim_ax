using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ORDER_DATA
    {
				public long ID_ORDER_DATA;
				public int ID_ORDER;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_ORDER_DATA clone);

		public DB_ORDER_DATA(DB_ORDER_DATA clone)
		{
						this.ID_ORDER_DATA = clone.ID_ORDER_DATA;
						this.ID_ORDER = clone.ID_ORDER;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ORDER_DATA()
		{
		}
	}
}