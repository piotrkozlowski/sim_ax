using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LANGUAGE_DATA
    {
				public long ID_LANGUAGE_DATA;
				public int ID_LANGUAGE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_LANGUAGE_DATA clone);

		public DB_LANGUAGE_DATA(DB_LANGUAGE_DATA clone)
		{
						this.ID_LANGUAGE_DATA = clone.ID_LANGUAGE_DATA;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_LANGUAGE_DATA()
		{
		}
	}
}