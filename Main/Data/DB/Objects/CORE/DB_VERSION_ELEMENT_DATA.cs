using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_ELEMENT_DATA
    {
		public long ID_VERSION_ELEMENT_DATA;
		public int ID_VERSION_ELEMENT;
		public long ID_DATA_TYPE;
		public int INDEX_NBR;
		public object VALUE;
		    
		partial void CloneUser(DB_VERSION_ELEMENT_DATA clone);

		public DB_VERSION_ELEMENT_DATA(DB_VERSION_ELEMENT_DATA clone)
		{
			this.ID_VERSION_ELEMENT_DATA = clone.ID_VERSION_ELEMENT_DATA;
			this.ID_VERSION_ELEMENT = clone.ID_VERSION_ELEMENT;
			this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
			this.INDEX_NBR = clone.INDEX_NBR;
			this.VALUE = clone.VALUE;
			CloneUser(clone);
		}

		public DB_VERSION_ELEMENT_DATA()
		{
		}
	}
}