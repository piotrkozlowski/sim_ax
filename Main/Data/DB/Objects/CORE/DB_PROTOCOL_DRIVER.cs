using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PROTOCOL_DRIVER
    {
				public int ID_PROTOCOL_DRIVER;
				public string NAME;
				public long? ID_DESCR;
				public string PLUGIN_NAME;
		    
		partial void CloneUser(DB_PROTOCOL_DRIVER clone);

		public DB_PROTOCOL_DRIVER(DB_PROTOCOL_DRIVER clone)
		{
						this.ID_PROTOCOL_DRIVER = clone.ID_PROTOCOL_DRIVER;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.PLUGIN_NAME = clone.PLUGIN_NAME;
						CloneUser(clone);
		}

		public DB_PROTOCOL_DRIVER()
		{
		}
	}
}