using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_FAULT_CODE_SUMMARY
    {
				public int ID_FAULT_CODE_SUMMARY;
				public int? ID_FAULT_CODE;
				public int ID_SERVICE_REFERENCE_TYPE;
				public object REFERENCE_VALUE;
				public int FAULTS_COUNT;
                public int TOTAL_COUNT;
                public int ID_FAULT_CODE_SUMMARY_TYPE;
		    
		partial void CloneUser(DB_FAULT_CODE_SUMMARY clone);

		public DB_FAULT_CODE_SUMMARY(DB_FAULT_CODE_SUMMARY clone)
		{
						this.ID_FAULT_CODE_SUMMARY = clone.ID_FAULT_CODE_SUMMARY;
						this.ID_FAULT_CODE = clone.ID_FAULT_CODE;
						this.ID_SERVICE_REFERENCE_TYPE = clone.ID_SERVICE_REFERENCE_TYPE;
						this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
						this.FAULTS_COUNT = clone.FAULTS_COUNT;
                        this.TOTAL_COUNT = clone.TOTAL_COUNT;
                        this.ID_FAULT_CODE_SUMMARY_TYPE = clone.ID_FAULT_CODE_SUMMARY_TYPE;
						CloneUser(clone);
		}

		public DB_FAULT_CODE_SUMMARY()
		{
		}
	}
}