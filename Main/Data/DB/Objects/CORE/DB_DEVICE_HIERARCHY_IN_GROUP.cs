﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_HIERARCHY_IN_GROUP
    {
        public long ID_DEVICE_HIERARCHY_GROUP;
        public long ID_DEVICE_HIERARCHY;

        partial void CloneUser(DB_DEVICE_HIERARCHY_IN_GROUP clone);

        public DB_DEVICE_HIERARCHY_IN_GROUP(DB_DEVICE_HIERARCHY_IN_GROUP clone)
        {
            this.ID_DEVICE_HIERARCHY_GROUP = clone.ID_DEVICE_HIERARCHY_GROUP;
            this.ID_DEVICE_HIERARCHY = clone.ID_DEVICE_HIERARCHY;
            CloneUser(clone);
        }

        public DB_DEVICE_HIERARCHY_IN_GROUP()
        {
        }
    }
}