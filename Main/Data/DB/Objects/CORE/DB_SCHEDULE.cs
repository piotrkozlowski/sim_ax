using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_SCHEDULE
    {
        [DataMember]
        public int ID_SCHEDULE;
        [DataMember]
        public string NAME;
        [DataMember]
        public string DESCRIPTION;
        [DataMember]
        public int ID_SCHEDULE_TYPE;
        [DataMember]
        public int? SCHEDULE_INTERVAL;
        [DataMember]
        public int SUBDAY_TYPE;
        [DataMember]
        public int? SUBDAY_INTERVAL;
        [DataMember]
        public int? RELATIVE_INTERVAL;
        [DataMember]
        public int? RECURRENCE_FACTOR;
        [DataMember]
        public DateTime START_DATE;
        [DataMember]
        public DateTime? END_DATE;
        [DataMember]
        public TimeSpan? START_TIME;
        [DataMember]
        public TimeSpan? END_TIME;
        [DataMember]
        public bool TIMES_IN_UTC;
        [DataMember]
        public DateTime? LAST_RUN_DATE;
        [DataMember]
        public bool IS_ACTIVE;
        [DataMember]
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector

        partial void CloneUser(DB_SCHEDULE clone);

        public DB_SCHEDULE(DB_SCHEDULE clone)
        {
            this.ID_SCHEDULE = clone.ID_SCHEDULE;
            this.NAME = clone.NAME;
            this.DESCRIPTION = clone.DESCRIPTION;
            this.ID_SCHEDULE_TYPE = clone.ID_SCHEDULE_TYPE;
            this.SCHEDULE_INTERVAL = clone.SCHEDULE_INTERVAL;
            this.SUBDAY_TYPE = clone.SUBDAY_TYPE;
            this.SUBDAY_INTERVAL = clone.SUBDAY_INTERVAL;
            this.RELATIVE_INTERVAL = clone.RELATIVE_INTERVAL;
            this.RECURRENCE_FACTOR = clone.RECURRENCE_FACTOR;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.TIMES_IN_UTC = clone.TIMES_IN_UTC;
            this.LAST_RUN_DATE = clone.LAST_RUN_DATE;
            this.IS_ACTIVE = clone.IS_ACTIVE;
            this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
            CloneUser(clone);
        }

        public DB_SCHEDULE()
        {
        }
    }
}