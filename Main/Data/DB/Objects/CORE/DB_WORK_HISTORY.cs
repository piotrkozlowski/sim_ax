using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK_HISTORY
    {
        public long ID_WORK_HISTORY;
        public long ID_WORK;
        public int ID_WORK_STATUS;
        public DateTime? START_TIME;
        public DateTime? END_TIME;
        public long? ID_DESCR;
        public int? ID_MODULE;
        public string ASSEMBLY;

        partial void CloneUser(DB_WORK_HISTORY clone);

        public DB_WORK_HISTORY(DB_WORK_HISTORY clone)
        {
            this.ID_WORK_HISTORY = clone.ID_WORK_HISTORY;
            this.ID_WORK = clone.ID_WORK;
            this.ID_WORK_STATUS = clone.ID_WORK_STATUS;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_MODULE = clone.ID_MODULE;
            this.ASSEMBLY = clone.ASSEMBLY;
            CloneUser(clone);
        }

        public DB_WORK_HISTORY()
        {
        }
    }
}