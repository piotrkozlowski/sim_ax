﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WARRANTY_CONTRACT
    {
        public int ID_DEVICE_ORDER_NUMBER;
        public int ID_COMPONENT;
        public int ID_CONTRACT;
        public int WARRANTY_TIME;
        public int ID_WARRANTY_START_DATE_CALCULATION_METHOD;

        partial void CloneUser(DB_WARRANTY_CONTRACT clone);

        public DB_WARRANTY_CONTRACT(DB_WARRANTY_CONTRACT clone)
        {
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.ID_COMPONENT = clone.ID_COMPONENT;
            this.ID_CONTRACT = clone.ID_CONTRACT;
            this.WARRANTY_TIME = clone.WARRANTY_TIME;
            this.ID_WARRANTY_START_DATE_CALCULATION_METHOD = clone.ID_WARRANTY_START_DATE_CALCULATION_METHOD;
            CloneUser(clone);
        }

        public DB_WARRANTY_CONTRACT()
        {
        }
    }
}