﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST_DEVICE_ACTION_DETAILS
    {
        public long ID_SERVICE_LIST_DEVICE_ACTION_DETAILS;
        public long SERIAL_NBR;
        public int ID_SERVICE_LIST;
        public long ID_SERVICE_PACKAGE;
        public int ID_SERVICE;
        public string INPUT_TEXT;
        public int? ID_SERVICE_ACTION_RESULT;

        partial void CloneUser(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS clone);

        public DB_SERVICE_LIST_DEVICE_ACTION_DETAILS(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS clone)
        {
            this.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS = clone.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.ID_SERVICE = clone.ID_SERVICE;
            this.INPUT_TEXT = clone.INPUT_TEXT;
            this.ID_SERVICE_ACTION_RESULT = clone.ID_SERVICE_ACTION_RESULT;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST_DEVICE_ACTION_DETAILS()
        {
        }
    }
}