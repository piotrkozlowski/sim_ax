using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_VERSION
    {
        public long ID_VERSION;
        public int ID_VERSION_STATE;
        public int? ID_VERSION_ELEMENT_TYPE;
        public int ID_VERSION_TYPE;
        public string VERSION_NBR;
        public long? VERSION_RAW;
        public DateTime? RELEASE_DATE;
        public int? ID_OPERATOR_PUBLISHER;
        public DateTime? NOTIFICATION_DATE;
        public int? ID_OPERATOR_NOTIFIER;
        public DateTime? CONFIRM_DATE;
        public int? ID_OPERATOR_COFIRMING;
        public string RELEASE_NOTES;
        public int ID_PRIORITY;
        public DateTime? DEADLINE;
        public DateTime START_TIME;
        public DateTime? END_TIME;

        partial void CloneUser(DB_VERSION clone);

        public DB_VERSION(DB_VERSION clone)
        {
            this.ID_VERSION = clone.ID_VERSION;
            this.ID_VERSION_STATE = clone.ID_VERSION_STATE;
            this.ID_VERSION_ELEMENT_TYPE = clone.ID_VERSION_ELEMENT_TYPE;
            this.ID_VERSION_TYPE = clone.ID_VERSION_TYPE;
            this.VERSION_NBR = clone.VERSION_NBR;
            this.VERSION_RAW = clone.VERSION_RAW;
            this.RELEASE_DATE = clone.RELEASE_DATE;
            this.ID_OPERATOR_PUBLISHER = clone.ID_OPERATOR_PUBLISHER;
            this.NOTIFICATION_DATE = clone.NOTIFICATION_DATE;
            this.ID_OPERATOR_NOTIFIER = clone.ID_OPERATOR_NOTIFIER;
            this.CONFIRM_DATE = clone.CONFIRM_DATE;
            this.ID_OPERATOR_COFIRMING = clone.ID_OPERATOR_COFIRMING;
            this.RELEASE_NOTES = clone.RELEASE_NOTES;
            this.ID_PRIORITY = clone.ID_PRIORITY;
            this.DEADLINE = clone.DEADLINE;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            CloneUser(clone);
        }

        public DB_VERSION()
        {
        }
    }
}