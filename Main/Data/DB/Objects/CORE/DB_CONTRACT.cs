﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_CONTRACT
    {
        public int ID_CONTRACT;
        public string NAME;
        public string DESCR;
        public int ID_DISTRIBUTOR;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_CONTRACT clone);

        public DB_CONTRACT(DB_CONTRACT clone)
        {
            this.ID_CONTRACT = clone.ID_CONTRACT;
            this.NAME = clone.NAME;
            this.DESCR = clone.DESCR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_CONTRACT()
        {
        }
    }
}