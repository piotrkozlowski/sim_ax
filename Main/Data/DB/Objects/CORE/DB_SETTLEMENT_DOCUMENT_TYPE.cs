using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SETTLEMENT_DOCUMENT_TYPE
    {
				public int ID_SETTLEMENT_DOCUMENT_TYPE;
				public string NAME;
				public string DESCRIPTION;
				public bool ACTIVE;
				public int? ID_DISTRIBUTOR;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_SETTLEMENT_DOCUMENT_TYPE clone);

		public DB_SETTLEMENT_DOCUMENT_TYPE(DB_SETTLEMENT_DOCUMENT_TYPE clone)
		{
						this.ID_SETTLEMENT_DOCUMENT_TYPE = clone.ID_SETTLEMENT_DOCUMENT_TYPE;
						this.NAME = clone.NAME;
						this.DESCRIPTION = clone.DESCRIPTION;
						this.ACTIVE = clone.ACTIVE;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_SETTLEMENT_DOCUMENT_TYPE()
		{
		}
	}
}