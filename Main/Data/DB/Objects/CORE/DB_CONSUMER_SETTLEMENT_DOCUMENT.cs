using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_SETTLEMENT_DOCUMENT
    {
				public long ID_CONSUMER_SETTLEMENT_DOCUMENT;
				public long ID_CONSUMER_SETTLEMENT;
				public int ID_SETTLEMENT_DOCUMENT_TYPE;
				public string METADATA;
				public byte[] DOCUMENT_LINK;
				public Guid? DOCUMENT_GUID;
		    
		partial void CloneUser(DB_CONSUMER_SETTLEMENT_DOCUMENT clone);

		public DB_CONSUMER_SETTLEMENT_DOCUMENT(DB_CONSUMER_SETTLEMENT_DOCUMENT clone)
		{
						this.ID_CONSUMER_SETTLEMENT_DOCUMENT = clone.ID_CONSUMER_SETTLEMENT_DOCUMENT;
						this.ID_CONSUMER_SETTLEMENT = clone.ID_CONSUMER_SETTLEMENT;
						this.ID_SETTLEMENT_DOCUMENT_TYPE = clone.ID_SETTLEMENT_DOCUMENT_TYPE;
						this.METADATA = clone.METADATA;
						this.DOCUMENT_LINK = clone.DOCUMENT_LINK;
						this.DOCUMENT_GUID = clone.DOCUMENT_GUID;
						CloneUser(clone);
		}

		public DB_CONSUMER_SETTLEMENT_DOCUMENT()
		{
		}
	}
}