using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_ELEMENT_HIERARCHY
    {
		public long ID_VERSION_ELEMENT_HIERARCHY;
		public long? ID_VERSION_ELEMENT_HIERARCHY_PARENT;
		public int ID_VERSION_ELEMENT;
		public string HIERARCHY;
		    
		partial void CloneUser(DB_VERSION_ELEMENT_HIERARCHY clone);

		public DB_VERSION_ELEMENT_HIERARCHY(DB_VERSION_ELEMENT_HIERARCHY clone)
		{
			this.ID_VERSION_ELEMENT_HIERARCHY = clone.ID_VERSION_ELEMENT_HIERARCHY;
			this.ID_VERSION_ELEMENT_HIERARCHY_PARENT = clone.ID_VERSION_ELEMENT_HIERARCHY_PARENT;
			this.ID_VERSION_ELEMENT = clone.ID_VERSION_ELEMENT;
			this.HIERARCHY = clone.HIERARCHY;
			CloneUser(clone);
		}

		public DB_VERSION_ELEMENT_HIERARCHY()
		{
		}
	}
}