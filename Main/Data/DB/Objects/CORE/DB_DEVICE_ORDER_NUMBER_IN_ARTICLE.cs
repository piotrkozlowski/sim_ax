﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_ORDER_NUMBER_IN_ARTICLE
    {
        public int ID_DEVICE_ORDER_NUMBER;
        public long ID_ARTICLE;

        partial void CloneUser(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE clone);

        public DB_DEVICE_ORDER_NUMBER_IN_ARTICLE(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE clone)
        {
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.ID_ARTICLE = clone.ID_ARTICLE;
            CloneUser(clone);
        }

        public DB_DEVICE_ORDER_NUMBER_IN_ARTICLE()
        {
        }
    }
}