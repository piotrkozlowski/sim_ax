using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_DRIVER_MEMORY_MAP_DATA
    {
        public long ID_DEVICE_DRIVER_MEMORY_MAP_DATA;
        public long? ID_DDMMD_PARENT;
        public int ID_DEVICE_DRIVER_MEMORY_MAP;
        public int? ID_OMB;
        public string NAME;
        public long? ID_DATA_TYPE;
        public int? INDEX_NBR;
        public bool IS_READ;
        public bool IS_WRITE;
        public bool IS_PUT_DATA_OMB;
        public long? ID_DDMMD_PUT_DATA;
        public long? ID_DESCR;
        public int? ID_SERVER;

        partial void CloneUser(DB_DEVICE_DRIVER_MEMORY_MAP_DATA clone);

        public DB_DEVICE_DRIVER_MEMORY_MAP_DATA(DB_DEVICE_DRIVER_MEMORY_MAP_DATA clone)
        {
            this.ID_DEVICE_DRIVER_MEMORY_MAP_DATA = clone.ID_DEVICE_DRIVER_MEMORY_MAP_DATA;
            this.ID_DDMMD_PARENT = clone.ID_DDMMD_PARENT;
            this.ID_DEVICE_DRIVER_MEMORY_MAP = clone.ID_DEVICE_DRIVER_MEMORY_MAP;
            this.ID_OMB = clone.ID_OMB;
            this.NAME = clone.NAME;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.IS_READ = clone.IS_READ;
            this.IS_WRITE = clone.IS_WRITE;
            this.IS_PUT_DATA_OMB = clone.IS_PUT_DATA_OMB;
            this.ID_DDMMD_PUT_DATA = clone.ID_DDMMD_PUT_DATA;
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_SERVER = clone.ID_SERVER;
            CloneUser(clone);
        }

        public DB_DEVICE_DRIVER_MEMORY_MAP_DATA()
        {
        }
    }
}