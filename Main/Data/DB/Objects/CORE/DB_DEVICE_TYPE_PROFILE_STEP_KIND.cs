using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_PROFILE_STEP_KIND
    {
				public int ID_DEVICE_TYPE_PROFILE_STEP_KIND;
				public string NAME;
		    
		partial void CloneUser(DB_DEVICE_TYPE_PROFILE_STEP_KIND clone);

		public DB_DEVICE_TYPE_PROFILE_STEP_KIND(DB_DEVICE_TYPE_PROFILE_STEP_KIND clone)
		{
						this.ID_DEVICE_TYPE_PROFILE_STEP_KIND = clone.ID_DEVICE_TYPE_PROFILE_STEP_KIND;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_PROFILE_STEP_KIND()
		{
		}
	}
}