using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_REQUIREMENT
    {
		public long ID_VERSION;
		public long ID_VERSION_REQUIRED;
		public int ID_OPERATOR;
		public long ID_VERSION_REQUIREMENT;
		    
		partial void CloneUser(DB_VERSION_REQUIREMENT clone);

		public DB_VERSION_REQUIREMENT(DB_VERSION_REQUIREMENT clone)
		{
			this.ID_VERSION = clone.ID_VERSION;
			this.ID_VERSION_REQUIRED = clone.ID_VERSION_REQUIRED;
			this.ID_OPERATOR = clone.ID_OPERATOR;
			this.ID_VERSION_REQUIREMENT = clone.ID_VERSION_REQUIREMENT;
			CloneUser(clone);
		}

		public DB_VERSION_REQUIREMENT()
		{
		}
	}
}