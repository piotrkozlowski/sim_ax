using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SESSION_EVENT_DATA
    {
				public long ID_SESSION_EVENT_DATA;
				public long ID_SESSION_EVENT;
				public long ID_DATA_TYPE;
				public object VALUE;
		    
		partial void CloneUser(DB_SESSION_EVENT_DATA clone);

		public DB_SESSION_EVENT_DATA(DB_SESSION_EVENT_DATA clone)
		{
						this.ID_SESSION_EVENT_DATA = clone.ID_SESSION_EVENT_DATA;
						this.ID_SESSION_EVENT = clone.ID_SESSION_EVENT;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_SESSION_EVENT_DATA()
		{
		}
	}
}