using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_TYPE
    {
				public int ID_CONSUMER_TYPE;
				public string NAME;
				public long? ID_DESCR;
                public int ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_CONSUMER_TYPE clone);

		public DB_CONSUMER_TYPE(DB_CONSUMER_TYPE clone)
		{
						this.ID_CONSUMER_TYPE = clone.ID_CONSUMER_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
                        this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_CONSUMER_TYPE()
		{
		}
	}
}