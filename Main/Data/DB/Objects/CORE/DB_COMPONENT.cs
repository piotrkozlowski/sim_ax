﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_COMPONENT
    {
        public int ID_COMPONENT;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_COMPONENT clone);

        public DB_COMPONENT(DB_COMPONENT clone)
        {
            this.ID_COMPONENT = clone.ID_COMPONENT;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_COMPONENT()
        {
        }
    }
}