using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DATA
    {
				public long ID_ROUTE_DATA;
				public long ID_ROUTE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_ROUTE_DATA clone);

		public DB_ROUTE_DATA(DB_ROUTE_DATA clone)
		{
						this.ID_ROUTE_DATA = clone.ID_ROUTE_DATA;
						this.ID_ROUTE = clone.ID_ROUTE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ROUTE_DATA()
		{
		}
	}
}