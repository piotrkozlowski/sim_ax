﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_CONVERSION
    {
        public long ID_CONVERSION;
        public int ID_REFERENCE_TYPE;
        public long REFERENCE_VALUE;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public Double SLOPE;
        public Double BIAS;

        partial void CloneUser(DB_CONVERSION clone);

        public DB_CONVERSION(DB_CONVERSION clone)
        {
            this.ID_CONVERSION = clone.ID_CONVERSION;
            this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
            this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.SLOPE = clone.SLOPE;
            this.BIAS = clone.BIAS;
            CloneUser(clone);
        }

        public DB_CONVERSION()
        {
        }
    }
}