using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PAYMENT_MODULE_TYPE
    {
				public int ID_PAYMENT_MODULE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_PAYMENT_MODULE_TYPE clone);

		public DB_PAYMENT_MODULE_TYPE(DB_PAYMENT_MODULE_TYPE clone)
		{
						this.ID_PAYMENT_MODULE_TYPE = clone.ID_PAYMENT_MODULE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_PAYMENT_MODULE_TYPE()
		{
		}
	}
}