﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_VIEW_COLUMN
    {
        [DataMember]
        public int ID_VIEW_COLUMN;
        [DataMember]
        public int ID_VIEW;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public string NAME;
        [DataMember]
        public string TABLE;
        [DataMember]
        public bool PRIMARY_KEY;
        [DataMember]
        public object DEFAULT_VALUE;
        [DataMember]
        public long? SIZE;
        [DataMember]
        public int ID_VIEW_COLUMN_SOURCE;
        [DataMember]
        public int ID_VIEW_COLUMN_TYPE;
        [DataMember]
        public bool KEY;
        [DataMember]
        public int? ID_KEY_VIEW_COLUMN;

        partial void CloneUser(DB_VIEW_COLUMN clone);

        public DB_VIEW_COLUMN(DB_VIEW_COLUMN clone)
        {
            this.ID_VIEW_COLUMN = clone.ID_VIEW_COLUMN;
            this.ID_VIEW = clone.ID_VIEW;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.NAME = clone.NAME;
            this.TABLE = clone.TABLE;
            this.PRIMARY_KEY = clone.PRIMARY_KEY;
            this.DEFAULT_VALUE = clone.DEFAULT_VALUE;
            this.SIZE = clone.SIZE;
            this.ID_VIEW_COLUMN_SOURCE = clone.ID_VIEW_COLUMN_SOURCE;
            this.ID_VIEW_COLUMN_TYPE = clone.ID_VIEW_COLUMN_TYPE;
            this.KEY = clone.KEY;
            this.ID_KEY_VIEW_COLUMN = clone.ID_KEY_VIEW_COLUMN;
            CloneUser(clone);
        }

        public DB_VIEW_COLUMN()
        {
        }
    }
}