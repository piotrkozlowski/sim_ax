using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_SIM_CARD_HISTORY
    {
				public long ID_DEVICE_SIM_CARD_HISTORY;
				public long SERIAL_NBR;
				public int? ID_SIM_CARD;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string NOTES;
		    
		partial void CloneUser(DB_DEVICE_SIM_CARD_HISTORY clone);

		public DB_DEVICE_SIM_CARD_HISTORY(DB_DEVICE_SIM_CARD_HISTORY clone)
		{
						this.ID_DEVICE_SIM_CARD_HISTORY = clone.ID_DEVICE_SIM_CARD_HISTORY;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_SIM_CARD = clone.ID_SIM_CARD;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_DEVICE_SIM_CARD_HISTORY()
		{
		}
	}
}