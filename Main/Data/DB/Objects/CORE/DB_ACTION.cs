using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION
    {
				public long ID_ACTION;
				public long? SERIAL_NBR;
				public long? ID_METER;
				public long? ID_LOCATION;
				public int ID_ACTION_TYPE;
				public int ID_ACTION_STATUS;
				public long ID_ACTION_DATA;
				public long? ID_ACTION_PARENT;
				public long? ID_DATA_ARCH;
				public int? ID_MODULE;
				public int? ID_OPERATOR;
                public DateTime CREATION_DATE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_ACTION clone);

		public DB_ACTION(DB_ACTION clone)
		{
						this.ID_ACTION = clone.ID_ACTION;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_ACTION_TYPE = clone.ID_ACTION_TYPE;
						this.ID_ACTION_STATUS = clone.ID_ACTION_STATUS;
						this.ID_ACTION_DATA = clone.ID_ACTION_DATA;
						this.ID_ACTION_PARENT = clone.ID_ACTION_PARENT;
						this.ID_DATA_ARCH = clone.ID_DATA_ARCH;
						this.ID_MODULE = clone.ID_MODULE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
                        this.CREATION_DATE = clone.CREATION_DATE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_ACTION()
		{
		}
	}
}