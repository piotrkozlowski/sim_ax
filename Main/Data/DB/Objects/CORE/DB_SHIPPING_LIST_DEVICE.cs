﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_SHIPPING_LIST_DEVICE
    {
        [DataMember]
        public int ID_SHIPPING_LIST_DEVICE;
        [DataMember]
        public int ID_SHIPPING_LIST;
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_ARTICLE;
        [DataMember]
        public DateTime INSERT_DATE;

        partial void CloneUser(DB_SHIPPING_LIST_DEVICE clone);

        public DB_SHIPPING_LIST_DEVICE(DB_SHIPPING_LIST_DEVICE clone)
        {
            this.ID_SHIPPING_LIST_DEVICE = clone.ID_SHIPPING_LIST_DEVICE;
            this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_METER = clone.ID_METER;
            this.ID_ARTICLE = clone.ID_ARTICLE;
            this.INSERT_DATE = clone.INSERT_DATE;
            CloneUser(clone);
        }

        public DB_SHIPPING_LIST_DEVICE()
        {
        }
    }
}