using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DATA_CHANGE
    {
        [DataMember]
        public long ID_DATA_CHANGE;
        [DataMember]
        public string TABLE_NAME;
        [DataMember]
        public long? CHANGED_ID;
        [DataMember]
        public long? CHANGED_ID2;
        [DataMember]
        public long? CHANGED_ID3;
        [DataMember]
        public long? CHANGED_ID4;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public int CHANGE_TYPE;
        [DataMember]
        public DateTime TIME;
        [DataMember]
		public string USER;
		    
		partial void CloneUser(DB_DATA_CHANGE clone);

		public DB_DATA_CHANGE(DB_DATA_CHANGE clone)
		{
						this.ID_DATA_CHANGE = clone.ID_DATA_CHANGE;
						this.TABLE_NAME = clone.TABLE_NAME;
						this.CHANGED_ID = clone.CHANGED_ID;
						this.CHANGED_ID2 = clone.CHANGED_ID2;
						this.CHANGED_ID3 = clone.CHANGED_ID3;
						this.CHANGED_ID4 = clone.CHANGED_ID4;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.CHANGE_TYPE = clone.CHANGE_TYPE;
						this.TIME = clone.TIME;
						this.USER = clone.USER;
						CloneUser(clone);
		}

		public DB_DATA_CHANGE()
		{
		}
	}
}