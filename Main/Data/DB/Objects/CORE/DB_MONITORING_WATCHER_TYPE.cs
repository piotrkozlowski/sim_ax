using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_MONITORING_WATCHER_TYPE
    {
				public int ID_MONITORING_WATCHER_TYPE;
				public string NAME;
				public string PLUGIN;
		    
		partial void CloneUser(DB_MONITORING_WATCHER_TYPE clone);

		public DB_MONITORING_WATCHER_TYPE(DB_MONITORING_WATCHER_TYPE clone)
		{
						this.ID_MONITORING_WATCHER_TYPE = clone.ID_MONITORING_WATCHER_TYPE;
						this.NAME = clone.NAME;
						this.PLUGIN = clone.PLUGIN;
						CloneUser(clone);
		}

		public DB_MONITORING_WATCHER_TYPE()
		{
		}
	}
}