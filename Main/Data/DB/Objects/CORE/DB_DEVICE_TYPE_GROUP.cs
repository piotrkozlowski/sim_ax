using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_GROUP
    {
				public int ID_DEVICE_TYPE_GROUP;
				public string NAME;
				public string DESCRIPTION;
		    
		partial void CloneUser(DB_DEVICE_TYPE_GROUP clone);

		public DB_DEVICE_TYPE_GROUP(DB_DEVICE_TYPE_GROUP clone)
		{
						this.ID_DEVICE_TYPE_GROUP = clone.ID_DEVICE_TYPE_GROUP;
						this.NAME = clone.NAME;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_GROUP()
		{
		}
	}
}