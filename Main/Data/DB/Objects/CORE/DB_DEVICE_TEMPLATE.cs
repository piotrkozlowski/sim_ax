using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TEMPLATE
    {
		public int ID_TEMPLATE;
		public int? ID_TEMPLATE_PATTERN;
		public int ID_DEVICE_TYPE;
		public string FIRMWARE_VERSION;
		public string NAME;
		public int CREATED_BY;
		public DateTime CREATION_DATE;
		public int? CONFIRMED_BY;
		public DateTime? CONFIRM_DATE;
        public int? ID_SERVER;        
		    
		partial void CloneUser(DB_DEVICE_TEMPLATE clone);

		public DB_DEVICE_TEMPLATE(DB_DEVICE_TEMPLATE clone)
		{
						this.ID_TEMPLATE = clone.ID_TEMPLATE;
						this.ID_TEMPLATE_PATTERN = clone.ID_TEMPLATE_PATTERN;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						this.FIRMWARE_VERSION = clone.FIRMWARE_VERSION;
						this.NAME = clone.NAME;
						this.CREATED_BY = clone.CREATED_BY;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.CONFIRMED_BY = clone.CONFIRMED_BY;
						this.CONFIRM_DATE = clone.CONFIRM_DATE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_DEVICE_TEMPLATE()
		{
		}
	}
}