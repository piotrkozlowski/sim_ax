using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_CLASS
    {
				public int ID_DEVICE_TYPE_CLASS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_DEVICE_TYPE_CLASS clone);

		public DB_DEVICE_TYPE_CLASS(DB_DEVICE_TYPE_CLASS clone)
		{
						this.ID_DEVICE_TYPE_CLASS = clone.ID_DEVICE_TYPE_CLASS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_CLASS()
		{
		}
	}
}