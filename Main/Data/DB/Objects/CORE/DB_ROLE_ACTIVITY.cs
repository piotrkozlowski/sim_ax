using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROLE_ACTIVITY
    {
				public long ID_ROLE_ACTIVITY;
				public int ID_ROLE;
				public int ID_ACTIVITY;
				public object REFERENCE_VALUE;
				public bool DENY;
		    
		partial void CloneUser(DB_ROLE_ACTIVITY clone);

		public DB_ROLE_ACTIVITY(DB_ROLE_ACTIVITY clone)
		{
						this.ID_ROLE_ACTIVITY = clone.ID_ROLE_ACTIVITY;
						this.ID_ROLE = clone.ID_ROLE;
						this.ID_ACTIVITY = clone.ID_ACTIVITY;
						this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
						this.DENY = clone.DENY;
						CloneUser(clone);
		}

		public DB_ROLE_ACTIVITY()
		{
		}
	}
}