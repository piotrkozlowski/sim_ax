﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST_DEVICE_DIAGNOSTIC
    {
        public long SERIAL_NBR;
        public int ID_SERVICE_LIST;
        public int ID_DIAGNOSTIC_ACTION;
        public int? ID_DIAGNOSTIC_ACTION_RESULT;
        public string INPUT_TEXT;
        public object DAMAGE_LEVEL;

        partial void CloneUser(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC clone);

        public DB_SERVICE_LIST_DEVICE_DIAGNOSTIC(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.ID_DIAGNOSTIC_ACTION = clone.ID_DIAGNOSTIC_ACTION;
            this.ID_DIAGNOSTIC_ACTION_RESULT = clone.ID_DIAGNOSTIC_ACTION_RESULT;
            this.INPUT_TEXT = clone.INPUT_TEXT;
            this.DAMAGE_LEVEL = clone.DAMAGE_LEVEL;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST_DEVICE_DIAGNOSTIC()
        {
        }
    }
}