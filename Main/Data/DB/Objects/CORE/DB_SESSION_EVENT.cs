using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SESSION_EVENT
    {
				public long ID_SESSION_EVENT;
				public long ID_SESSION;
				public int ID_EVENT;
				public DateTime TIME;
		    
		partial void CloneUser(DB_SESSION_EVENT clone);

		public DB_SESSION_EVENT(DB_SESSION_EVENT clone)
		{
						this.ID_SESSION_EVENT = clone.ID_SESSION_EVENT;
						this.ID_SESSION = clone.ID_SESSION;
						this.ID_EVENT = clone.ID_EVENT;
						this.TIME = clone.TIME;
						CloneUser(clone);
		}

		public DB_SESSION_EVENT()
		{
		}
	}
}