using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_ENTRANCE
    {
				public long ID_LOCATION;
				public int ENTRANCE_INDEX_NBR;
				public int SECTION_COUNT;
				public string NAME;
				public object FLOOR_PLAN;
		    
		partial void CloneUser(DB_LOCATION_ENTRANCE clone);

		public DB_LOCATION_ENTRANCE(DB_LOCATION_ENTRANCE clone)
		{
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ENTRANCE_INDEX_NBR = clone.ENTRANCE_INDEX_NBR;
						this.SECTION_COUNT = clone.SECTION_COUNT;
						this.NAME = clone.NAME;
						this.FLOOR_PLAN = clone.FLOOR_PLAN;
						CloneUser(clone);
		}

		public DB_LOCATION_ENTRANCE()
		{
		}
	}
}