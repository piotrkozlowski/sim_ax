using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_TYPE_GROUP
    {
				public int ID_ACTION_TYPE_GROUP;
				public int ID_ACTION_TYPE_GROUP_TYPE;
				public int? ID_REFERENCE_TYPE;
				public object REFERENCE_VALUE;
				public int? ID_PARENT_GROUP;

                public int? ID_SERVER;
		    
		partial void CloneUser(DB_ACTION_TYPE_GROUP clone);

		public DB_ACTION_TYPE_GROUP(DB_ACTION_TYPE_GROUP clone)
		{
						this.ID_ACTION_TYPE_GROUP = clone.ID_ACTION_TYPE_GROUP;
						this.ID_ACTION_TYPE_GROUP_TYPE = clone.ID_ACTION_TYPE_GROUP_TYPE;
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
						this.ID_PARENT_GROUP = clone.ID_PARENT_GROUP;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_ACTION_TYPE_GROUP()
		{
		}
	}
}