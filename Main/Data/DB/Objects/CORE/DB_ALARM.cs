using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ALARM
    {
        [DataMember]
		public long ID_ALARM;
        [DataMember]
        public long ID_ALARM_EVENT;
        [DataMember]
        public int ID_OPERATOR;
        [DataMember]
        public int ID_ALARM_GROUP;
        [DataMember]
        public int ID_ALARM_STATUS;
        [DataMember]
        public int ID_TRANSMISSION_TYPE;
		    
		partial void CloneUser(DB_ALARM clone);

		public DB_ALARM(DB_ALARM clone)
		{
						this.ID_ALARM = clone.ID_ALARM;
						this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_ALARM_GROUP = clone.ID_ALARM_GROUP;
						this.ID_ALARM_STATUS = clone.ID_ALARM_STATUS;
						this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
						CloneUser(clone);
		}

		public DB_ALARM()
		{
		}
	}
}