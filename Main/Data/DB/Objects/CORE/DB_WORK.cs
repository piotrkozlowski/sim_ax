using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK
    {
        public long ID_WORK;
        public long ID_WORK_TYPE;
        public int ID_WORK_STATUS;
        public long? ID_WORK_DATA;
        public long? ID_WORK_PARENT;
        public int? ID_MODULE;
        public int? ID_OPERATOR;
        public DateTime CREATION_DATE;
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector

        partial void CloneUser(DB_WORK clone);

        public DB_WORK(DB_WORK clone)
        {
            this.ID_WORK = clone.ID_WORK;
            this.ID_WORK_TYPE = clone.ID_WORK_TYPE;
            this.ID_WORK_STATUS = clone.ID_WORK_STATUS;
            this.ID_WORK_DATA = clone.ID_WORK_DATA;
            this.ID_WORK_PARENT = clone.ID_WORK_PARENT;
            this.ID_MODULE = clone.ID_MODULE;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
            CloneUser(clone);
        }

        public DB_WORK()
        {
        }
    }
}