using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_DEF_GROUP
    {
				public long ID_ALARM_DEF;
				public int ID_ALARM_GROUP;
				public int ID_TRANSMISSION_TYPE;
				public int CONFIRM_LEVEL;
				public int CONFIRM_TIMEOUT;
		    
		partial void CloneUser(DB_ALARM_DEF_GROUP clone);

		public DB_ALARM_DEF_GROUP(DB_ALARM_DEF_GROUP clone)
		{
						this.ID_ALARM_DEF = clone.ID_ALARM_DEF;
						this.ID_ALARM_GROUP = clone.ID_ALARM_GROUP;
						this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
						this.CONFIRM_LEVEL = clone.CONFIRM_LEVEL;
						this.CONFIRM_TIMEOUT = clone.CONFIRM_TIMEOUT;
						CloneUser(clone);
		}

		public DB_ALARM_DEF_GROUP()
		{
		}
	}
}