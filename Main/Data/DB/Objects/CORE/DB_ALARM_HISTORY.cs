using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ALARM_HISTORY
    {
				public long ID_ALARM_HISTORY;
				public long ID_ALARM;
				public int ID_ALARM_STATUS;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string START_RULE_HOST;
				public string START_RULE_USER;
				public string END_RULE_HOST;
				public string END_RULE_USER;
		    
		partial void CloneUser(DB_ALARM_HISTORY clone);

		public DB_ALARM_HISTORY(DB_ALARM_HISTORY clone)
		{
						this.ID_ALARM_HISTORY = clone.ID_ALARM_HISTORY;
						this.ID_ALARM = clone.ID_ALARM;
						this.ID_ALARM_STATUS = clone.ID_ALARM_STATUS;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.START_RULE_HOST = clone.START_RULE_HOST;
						this.START_RULE_USER = clone.START_RULE_USER;
						this.END_RULE_HOST = clone.END_RULE_HOST;
						this.END_RULE_USER = clone.END_RULE_USER;
						CloneUser(clone);
		}

		public DB_ALARM_HISTORY()
		{
		}
	}
}