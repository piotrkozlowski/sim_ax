﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_ACTION_RESULT
    {
        public int ID_SERVICE_ACTION_RESULT;
        public int ID_SERVICE;
        public string NAME;
        public bool ALLOW_INPUT_TEXT;
        public long? ID_DESCR;
        public int? ID_SERVICE_REFERENCE_TYPE;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_SERVICE_ACTION_RESULT clone);

        public DB_SERVICE_ACTION_RESULT(DB_SERVICE_ACTION_RESULT clone)
        {
            this.ID_SERVICE_ACTION_RESULT = clone.ID_SERVICE_ACTION_RESULT;
            this.ID_SERVICE = clone.ID_SERVICE;
            this.NAME = clone.NAME;
            this.ALLOW_INPUT_TEXT = clone.ALLOW_INPUT_TEXT;
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_SERVICE_REFERENCE_TYPE = clone.ID_SERVICE_REFERENCE_TYPE;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_SERVICE_ACTION_RESULT()
        {
        }
    }
}