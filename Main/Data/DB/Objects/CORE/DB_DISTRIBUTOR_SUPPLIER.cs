using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DISTRIBUTOR_SUPPLIER
    {
				public int ID_DISTRIBUTOR_SUPPLIER;
				public int ID_DISTRIBUTOR;
				public int? ID_SUPPLIER;
				public int? ID_PRODUCT_CODE;
				public int ID_ACTOR;
		    
		partial void CloneUser(DB_DISTRIBUTOR_SUPPLIER clone);

		public DB_DISTRIBUTOR_SUPPLIER(DB_DISTRIBUTOR_SUPPLIER clone)
		{
						this.ID_DISTRIBUTOR_SUPPLIER = clone.ID_DISTRIBUTOR_SUPPLIER;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_SUPPLIER = clone.ID_SUPPLIER;
						this.ID_PRODUCT_CODE = clone.ID_PRODUCT_CODE;
						this.ID_ACTOR = clone.ID_ACTOR;
						CloneUser(clone);
		}

		public DB_DISTRIBUTOR_SUPPLIER()
		{
		}
	}
}