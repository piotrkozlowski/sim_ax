using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK_HISTORY_DATA
    {
        public long ID_WORK_HISTORY_DATA;
        public long ID_WORK_HISTORY;
        public int ARG_NBR;
        public object VALUE;

        partial void CloneUser(DB_WORK_HISTORY_DATA clone);

        public DB_WORK_HISTORY_DATA(DB_WORK_HISTORY_DATA clone)
        {
            this.ID_WORK_HISTORY_DATA = clone.ID_WORK_HISTORY_DATA;
            this.ID_WORK_HISTORY = clone.ID_WORK_HISTORY;
            this.ARG_NBR = clone.ARG_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_WORK_HISTORY_DATA()
        {
        }
    }
}