using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PAYMENT_MODULE_DATA
    {
				public long ID_PAYMENT_MODULE_DATA;
				public int ID_PAYMENT_MODULE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_PAYMENT_MODULE_DATA clone);

		public DB_PAYMENT_MODULE_DATA(DB_PAYMENT_MODULE_DATA clone)
		{
						this.ID_PAYMENT_MODULE_DATA = clone.ID_PAYMENT_MODULE_DATA;
						this.ID_PAYMENT_MODULE = clone.ID_PAYMENT_MODULE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_PAYMENT_MODULE_DATA()
		{
		}
	}
}