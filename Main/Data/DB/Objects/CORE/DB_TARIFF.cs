using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TARIFF
    {
				public int ID_TARIFF;
				public int ID_DISTRIBUTOR;
				public string NAME;
		    
		partial void CloneUser(DB_TARIFF clone);

		public DB_TARIFF(DB_TARIFF clone)
		{
						this.ID_TARIFF = clone.ID_TARIFF;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_TARIFF()
		{
		}
	}
}