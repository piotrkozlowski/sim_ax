﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SHIPPING_LIST_DEVICES_WARRANTY_User
    {
        public int ID_SHIPPING_LIST;
        public string CONTRACT_NBR;
        public string SHIPPING_LIST_NBR;
        public DateTime CREATION_DATE;
        public long SERIAL_NBR;
        public string DEVICE_ORDER_NUMBER;
        public int ID_COMPONENT;
        public DateTime? WARRANTY_DATE;

        partial void CloneUser(DB_SHIPPING_LIST_DEVICES_WARRANTY_User clone);

		public DB_SHIPPING_LIST_DEVICES_WARRANTY_User(DB_SHIPPING_LIST_DEVICES_WARRANTY_User clone)
		{
            this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
			this.SERIAL_NBR = clone.SERIAL_NBR;
            this.CONTRACT_NBR = clone.CONTRACT_NBR;
            this.SHIPPING_LIST_NBR = clone.SHIPPING_LIST_NBR;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.DEVICE_ORDER_NUMBER = clone.DEVICE_ORDER_NUMBER;
            this.ID_COMPONENT = clone.ID_COMPONENT;
            this.WARRANTY_DATE = clone.WARRANTY_DATE;
			CloneUser(clone);
		}

        public DB_SHIPPING_LIST_DEVICES_WARRANTY_User()
		{
		}
    }
}
