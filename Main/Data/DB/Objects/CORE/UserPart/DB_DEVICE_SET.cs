﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_SET
    {
        public long SERIAL_NBR;
        public int ID_SET;
        public string SET_SERIAL_NBR;


        partial void CloneUser(DB_DEVICE_SET clone);

        public DB_DEVICE_SET(DB_DEVICE_SET clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_SET = clone.ID_SET;
            this.SET_SERIAL_NBR = clone.SET_SERIAL_NBR;
            CloneUser(clone);
        }

        public DB_DEVICE_SET()
        {
        }
    }
}
