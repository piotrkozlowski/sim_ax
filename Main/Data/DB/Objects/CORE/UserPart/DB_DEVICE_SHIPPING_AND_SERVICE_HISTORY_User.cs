﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User
    {
        public int ROW_TYPE;
        public DateTime EVENT_DATE;
        public string DESCR;
        public string LONG_DESCR;
        public DateTime? WARRANTY_DATE;



        partial void CloneUser(DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User clone);

		public DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User(DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User clone)
		{
            this.ROW_TYPE = clone.ROW_TYPE;
            this.EVENT_DATE = clone.EVENT_DATE;
            this.DESCR = clone.DESCR;
            this.LONG_DESCR = clone.LONG_DESCR;
            this.WARRANTY_DATE = clone.WARRANTY_DATE;
			CloneUser(clone);
		}

        public DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User()
		{
		}
    }
}
