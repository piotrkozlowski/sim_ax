﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_SIM_CARD
    {
        public int? ID_SIM_CARD;
        public long DEVICE_SERIAL_NBR;
        public int ID_DISTRIBUTOR;
        public string PHONE;
        public int ID_DEVICE_ORDER_NUMBER;
        public long? ID_LOCATION;

        public int? ID_TARIFF;
        public string TARIFF_NAME;
        public float? TARIFF_TAX;

        public DB_DEVICE_SIM_CARD()
        {
        }

        partial void CloneUser(DB_DEVICE_SIM_CARD clone);

        public DB_DEVICE_SIM_CARD(DB_DEVICE_SIM_CARD clone)
		{
            this.ID_SIM_CARD = clone.ID_SIM_CARD;
            this.DEVICE_SERIAL_NBR = clone.DEVICE_SERIAL_NBR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.PHONE = clone.PHONE;
            this.ID_TARIFF = clone.ID_TARIFF;
            this.TARIFF_NAME = clone.TARIFF_NAME;
            this.TARIFF_TAX = clone.TARIFF_TAX;
            this.ID_LOCATION = clone.ID_LOCATION;
			CloneUser(clone);
		}
    }
}
