﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_TASK_STATISTICS
    {
        public int ID_OPERATOR;
        public int TOTAL_NO;
        public int FINISHED_NO;
        public int PLANNED_NO;
        public int IN_PROGRESS_NO;
        public int WAITING_FOR_ACCEPTANCE_NO;
        public int NOT_PAID_NO;


        partial void CloneUser(DB_TASK_STATISTICS clone);

        public DB_TASK_STATISTICS(DB_TASK_STATISTICS clone)
        {
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.TOTAL_NO = clone.TOTAL_NO;
            this.FINISHED_NO = clone.FINISHED_NO;
            this.PLANNED_NO = clone.PLANNED_NO;
            this.IN_PROGRESS_NO = clone.IN_PROGRESS_NO;
            this.WAITING_FOR_ACCEPTANCE_NO = clone.WAITING_FOR_ACCEPTANCE_NO;
            this.NOT_PAID_NO = clone.NOT_PAID_NO;
            CloneUser(clone);
        }

        public DB_TASK_STATISTICS()
        {
        }
    }
}