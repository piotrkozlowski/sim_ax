﻿using System;
using System.Collections.Generic;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public class DB_BILLING_CONSUMER
    {
        #region Members

        public int ID_CONSUMER;
        public int ID_ACTOR;
        public int ID_DISTRIBUTOR;
        //public string MODULE_BILLING_NBR_DATA;
        //public string NOTIFICATION_DATA;
        public long SERIAL_NBR;
        public string DEVICE_PHONE_NBR;
        public int ID_DEVICE_TYPE;
        public int ID_TARIFF;
        public int ID_CONSUMER_TYPE;
        public long ID_LOCATION;
        //public List<string> CONSUMER_NOTIFICATION_GCM_ID;

        #endregion Members

        #region .Ctor

        public DB_BILLING_CONSUMER(DB_BILLING_CONSUMER clone)
        {
            this.ID_CONSUMER = clone.ID_CONSUMER;
            this.ID_ACTOR = clone.ID_ACTOR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_LOCATION = clone.ID_LOCATION;
            //this.MODULE_BILLING_NBR_DATA = clone.MODULE_BILLING_NBR_DATA;
            //this.NOTIFICATION_DATA = clone.NOTIFICATION_DATA;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.DEVICE_PHONE_NBR = clone.DEVICE_PHONE_NBR;
            this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
            this.ID_TARIFF = clone.ID_TARIFF;
            this.ID_CONSUMER_TYPE = clone.ID_CONSUMER_TYPE;
        }

        public DB_BILLING_CONSUMER()
        {
        }

        #endregion .Ctor
    }
}
