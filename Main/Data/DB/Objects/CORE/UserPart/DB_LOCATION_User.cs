using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    public partial class DB_LOCATION
    {
        public int U_STATUS;
		public string U_GROUP_NAME;
        public DB_DATA[] U_DATA_ARRAY;
        
        partial void CloneUser(DB_LOCATION clone)
        {
            this.U_STATUS = clone.U_STATUS;
			this.U_GROUP_NAME = clone.U_GROUP_NAME;
            this.U_DATA_ARRAY = clone.U_DATA_ARRAY;
        }

    }
}