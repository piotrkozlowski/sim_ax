﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DEVICE_SCHEDULE_User
    {
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public bool SCHEDULE_EXISTS;
        [DataMember]
        public long? ALEVEL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public int? ID_DEVICE_TYPE;
        [DataMember]
        public int? ID_METER_TYPE;
        [DataMember]
        public int? ID_METER_TYPE_CLASS;
        [DataMember]
        public long? ID_DATA_TYPE;
        [DataMember]
        public int DEVICE_ACCEPTABLE_SCHEDULE_READOUT_DELAY;
        [DataMember]
        public int DEVICE_OKO_ACCEPTABLE_SCHEDULE_READOUT_DELAY;
        [DataMember]
        public long? OKO_ID_DATA_TYPE;
        [DataMember]
        public List<DB_DEVICE_SCHEDULE> DB_DEVICE_SCHEDULE_LIST;

        public DateTime START_TIME;
        public DateTime END_TIME;
        public bool READOUT_FOR_SERIAL_NBR;//np OKO gazomierzowe nie mają wykonfigurowanych ALEVEL'i i to dla niech bezpośrednio sprawdzamy pomiar

        partial void CloneUser(DB_DEVICE_SCHEDULE_User clone);

        public DB_DEVICE_SCHEDULE_User(DB_DEVICE_SCHEDULE_User clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.SCHEDULE_EXISTS = clone.SCHEDULE_EXISTS;
            this.ALEVEL_NBR = clone.ALEVEL_NBR;
            this.ID_METER = clone.ID_METER;
            this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
            this.ID_METER_TYPE = clone.ID_METER_TYPE;
            this.ID_METER_TYPE_CLASS = clone.ID_METER_TYPE_CLASS;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.DEVICE_ACCEPTABLE_SCHEDULE_READOUT_DELAY = clone.DEVICE_ACCEPTABLE_SCHEDULE_READOUT_DELAY;
            this.DEVICE_OKO_ACCEPTABLE_SCHEDULE_READOUT_DELAY = clone.DEVICE_OKO_ACCEPTABLE_SCHEDULE_READOUT_DELAY;
            this.READOUT_FOR_SERIAL_NBR = clone.READOUT_FOR_SERIAL_NBR;
            this.OKO_ID_DATA_TYPE = clone.OKO_ID_DATA_TYPE;
            this.DB_DEVICE_SCHEDULE_LIST = clone.DB_DEVICE_SCHEDULE_LIST;
            CloneUser(clone);
        }

        public DB_DEVICE_SCHEDULE_User()
        {
        }
    }
}
