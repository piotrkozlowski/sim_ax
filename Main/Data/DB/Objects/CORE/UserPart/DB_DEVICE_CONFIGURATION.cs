﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_CONFIGURATION
    {
        public long SERIAL_NBR;
        public DataValue[] CONFIGURATION;

        public DB_DEVICE_CONFIGURATION(long SERIAL_NBR, DataValue[] CONFIGURATION)
        {
            this.SERIAL_NBR = SERIAL_NBR;
            this.CONFIGURATION = CONFIGURATION;
        }

    }
}
