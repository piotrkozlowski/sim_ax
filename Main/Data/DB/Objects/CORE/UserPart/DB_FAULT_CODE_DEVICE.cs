﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_FAULT_CODE_DEVICE
    {
        public long SERIAL_NBR;
        public int? ID_FAULT_CODE;
        public DateTime? FAULT_CODE_DATE;
        public int ID_DISTRIBUTOR;
        public int ID_DEVICE_TYPE;
        public int ID_DEVICE_ORDER_NUMBER;
        public int ID_SERVICE_LIST;
        public string BATCH_NUMBER;
        public string MANUFACTURING_BATCH_NUMBER;
        public bool? IS_BIG_FAULT;

        public int? ID_SHIPPING_LIST;
        public DateTime? SHIPPING_DATE;
    }
}
