using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_SETTLEMENT_ERROR
    {
				public int ID_CONSUMER_SETTLEMENT_ERROR;
				public long ID_CONSUMER_SETTLEMENT;
				public int ID_CONSUMER_SETTLEMENT_ERROR_TYPE;
				public string DESCRIPTION;
		    
		partial void CloneUser(DB_CONSUMER_SETTLEMENT_ERROR clone);

		public DB_CONSUMER_SETTLEMENT_ERROR(DB_CONSUMER_SETTLEMENT_ERROR clone)
		{
						this.ID_CONSUMER_SETTLEMENT_ERROR = clone.ID_CONSUMER_SETTLEMENT_ERROR;
						this.ID_CONSUMER_SETTLEMENT = clone.ID_CONSUMER_SETTLEMENT;
						this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = clone.ID_CONSUMER_SETTLEMENT_ERROR_TYPE;
						this.DESCRIPTION = clone.DESCRIPTION;
						CloneUser(clone);
		}

		public DB_CONSUMER_SETTLEMENT_ERROR()
		{
		}
	}
}