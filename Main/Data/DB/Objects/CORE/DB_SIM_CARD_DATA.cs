﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SIM_CARD_DATA
    {
        public long ID_SIM_CARD_DATA;
        public int ID_SIM_CARD;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_SIM_CARD_DATA clone);

        public DB_SIM_CARD_DATA(DB_SIM_CARD_DATA clone)
        {
            this.ID_SIM_CARD_DATA = clone.ID_SIM_CARD_DATA;
            this.ID_SIM_CARD = clone.ID_SIM_CARD;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_SIM_CARD_DATA()
        {
        }
    }
}