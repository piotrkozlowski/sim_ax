using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TARIFF_SETTLEMENT_PERIOD
    {
				public int ID_TARIFF_SETTLEMENT_PERIOD;
				public int ID_TARIFF;
				public DateTime START_TIME;
				public DateTime END_TIME;
		    
		partial void CloneUser(DB_TARIFF_SETTLEMENT_PERIOD clone);

		public DB_TARIFF_SETTLEMENT_PERIOD(DB_TARIFF_SETTLEMENT_PERIOD clone)
		{
						this.ID_TARIFF_SETTLEMENT_PERIOD = clone.ID_TARIFF_SETTLEMENT_PERIOD;
						this.ID_TARIFF = clone.ID_TARIFF;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_TARIFF_SETTLEMENT_PERIOD()
		{
		}
	}
}