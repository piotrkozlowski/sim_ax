using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_PROFILE_MAP_CONFIG
    {
				public int ID_DEVICE_TYPE_PROFILE_MAP;
				public int ID_DEVICE_TYPE_PROFILE_STEP;
				public int ID_DEVICE_TYPE_PROFILE_STEP_KIND;
		    
		partial void CloneUser(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG clone);

		public DB_DEVICE_TYPE_PROFILE_MAP_CONFIG(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG clone)
		{
						this.ID_DEVICE_TYPE_PROFILE_MAP = clone.ID_DEVICE_TYPE_PROFILE_MAP;
						this.ID_DEVICE_TYPE_PROFILE_STEP = clone.ID_DEVICE_TYPE_PROFILE_STEP;
						this.ID_DEVICE_TYPE_PROFILE_STEP_KIND = clone.ID_DEVICE_TYPE_PROFILE_STEP_KIND;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_PROFILE_MAP_CONFIG()
		{
		}
	}
}