using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_TYPE
    {
		public int ID_VERSION_TYPE;
		public string NAME;
		public long? ID_DESCR;
		    
		partial void CloneUser(DB_VERSION_TYPE clone);

		public DB_VERSION_TYPE(DB_VERSION_TYPE clone)
		{
			this.ID_VERSION_TYPE = clone.ID_VERSION_TYPE;
			this.NAME = clone.NAME;
			this.ID_DESCR = clone.ID_DESCR;
			CloneUser(clone);
		}

		public DB_VERSION_TYPE()
		{
		}
	}
}