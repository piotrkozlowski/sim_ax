using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DISTRIBUTOR_HIERARCHY
    {
				public int ID_DISTRIBUTOR_HIERARCHY;
				public int? ID_DISTRIBUTOR_HIERARCHY_PARENT;
				public int ID_DISTRIBUTOR;
				public string HIERARCHY;
		    
		partial void CloneUser(DB_DISTRIBUTOR_HIERARCHY clone);

		public DB_DISTRIBUTOR_HIERARCHY(DB_DISTRIBUTOR_HIERARCHY clone)
		{
						this.ID_DISTRIBUTOR_HIERARCHY = clone.ID_DISTRIBUTOR_HIERARCHY;
						this.ID_DISTRIBUTOR_HIERARCHY_PARENT = clone.ID_DISTRIBUTOR_HIERARCHY_PARENT;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.HIERARCHY = clone.HIERARCHY;
						CloneUser(clone);
		}

		public DB_DISTRIBUTOR_HIERARCHY()
		{
		}
	}
}