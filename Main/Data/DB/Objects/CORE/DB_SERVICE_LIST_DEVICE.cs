﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_LIST_DEVICE
    {
        public int ID_SERVICE_LIST;
        public long SERIAL_NBR;
        public string FAILURE_DESCRIPTION;
        public string FAILURE_SUGGESTION;
        public string NOTES;
        public long? PHOTO;
        public int? ID_OPERATOR_SERVICER;
        public string SERVICE_SHORT_DESCR;
        public string SERVICE_LONG_DESCR;
        public long? PHOTO_SERVICED;
        public bool? ATEX_OK;
        public bool? PAY_FOR_SERVICE;
        public int ID_SERVICE_STATUS;
        public int ID_DIAGNOSTIC_STATUS;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_SERVICE_LIST_DEVICE clone);

        public DB_SERVICE_LIST_DEVICE(DB_SERVICE_LIST_DEVICE clone)
        {
            this.ID_SERVICE_LIST = clone.ID_SERVICE_LIST;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.FAILURE_DESCRIPTION = clone.FAILURE_DESCRIPTION;
            this.FAILURE_SUGGESTION = clone.FAILURE_SUGGESTION;
            this.NOTES = clone.NOTES;
            this.PHOTO = clone.PHOTO;
            this.ID_OPERATOR_SERVICER = clone.ID_OPERATOR_SERVICER;
            this.SERVICE_SHORT_DESCR = clone.SERVICE_SHORT_DESCR;
            this.SERVICE_LONG_DESCR = clone.SERVICE_LONG_DESCR;
            this.PHOTO_SERVICED = clone.PHOTO_SERVICED;
            this.ATEX_OK = clone.ATEX_OK;
            this.PAY_FOR_SERVICE = clone.PAY_FOR_SERVICE;
            this.ID_SERVICE_STATUS = clone.ID_SERVICE_STATUS;
            this.ID_DIAGNOSTIC_STATUS = clone.ID_DIAGNOSTIC_STATUS;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_SERVICE_LIST_DEVICE()
        {
        }
    }
}