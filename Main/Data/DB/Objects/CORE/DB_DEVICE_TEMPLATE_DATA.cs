using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TEMPLATE_DATA
    {
		public int ID_TEMPLATE;
		public long ID_DATA_TYPE;
		public int INDEX_NBR;
		public object VALUE;
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_DEVICE_TEMPLATE_DATA clone);

		public DB_DEVICE_TEMPLATE_DATA(DB_DEVICE_TEMPLATE_DATA clone)
		{
						this.ID_TEMPLATE = clone.ID_TEMPLATE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_DEVICE_TEMPLATE_DATA()
		{
		}
	}
}