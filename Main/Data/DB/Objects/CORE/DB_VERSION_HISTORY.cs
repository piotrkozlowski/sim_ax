using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_HISTORY
    {
		public long ID_VERSION_HISTORY;
		public long ID_VERSION;
		public int? ID_VERSION_STATE;
		public DateTime START_TIME;
		public DateTime? END_TIME;
		public int ID_OPERATOR;
		public string NOTES;
		    
		partial void CloneUser(DB_VERSION_HISTORY clone);

		public DB_VERSION_HISTORY(DB_VERSION_HISTORY clone)
		{
			this.ID_VERSION_HISTORY = clone.ID_VERSION_HISTORY;
			this.ID_VERSION = clone.ID_VERSION;
			this.ID_VERSION_STATE = clone.ID_VERSION_STATE;
			this.START_TIME = clone.START_TIME;
			this.END_TIME = clone.END_TIME;
			this.ID_OPERATOR = clone.ID_OPERATOR;
			this.NOTES = clone.NOTES;
			CloneUser(clone);
		}

		public DB_VERSION_HISTORY()
		{
		}
	}
}