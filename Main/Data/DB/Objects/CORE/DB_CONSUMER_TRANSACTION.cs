using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.CORE
{
    [Serializable]
    public partial class DB_CONSUMER_TRANSACTION
    {
        public long ID_CONSUMER_TRANSACTION;
        public int ID_CONSUMER;
        public int? ID_OPERATOR;
        public int ID_DISTRIBUTOR;
        public int ID_CONSUMER_TRANSACTION_TYPE;
        public int? ID_TARIFF;
        public DateTime TIME;
        public Decimal CHARGE_PREPAID;
        public Decimal CHARGE_EC;
        public Decimal CHARGE_OWED;
        public Decimal BALANCE_PREPAID;
        public Decimal BALANCE_EC;
        public Decimal BALANCE_OWED;
        public Double? METER_INDEX;
        public long? ID_CONSUMER_SETTLEMENT;
        public int? ID_PAYMENT_MODULE;
        public string TRANSACTION_GUID;
        public long? ID_PACKET;
        public long? ID_ACTION;
        public long? ID_PREVIOUS_CONSUMER_TRANSACTION;

        partial void CloneUser(DB_CONSUMER_TRANSACTION clone);

        public DB_CONSUMER_TRANSACTION(DB_CONSUMER_TRANSACTION clone)
        {
            this.ID_CONSUMER_TRANSACTION = clone.ID_CONSUMER_TRANSACTION;
            this.ID_CONSUMER = clone.ID_CONSUMER;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_CONSUMER_TRANSACTION_TYPE = clone.ID_CONSUMER_TRANSACTION_TYPE;
            this.ID_TARIFF = clone.ID_TARIFF;
            this.TIME = clone.TIME;
            this.CHARGE_PREPAID = clone.CHARGE_PREPAID;
            this.CHARGE_EC = clone.CHARGE_EC;
            this.CHARGE_OWED = clone.CHARGE_OWED;
            this.BALANCE_PREPAID = clone.BALANCE_PREPAID;
            this.BALANCE_EC = clone.BALANCE_EC;
            this.BALANCE_OWED = clone.BALANCE_OWED;
            this.METER_INDEX = clone.METER_INDEX;
            this.ID_CONSUMER_SETTLEMENT = clone.ID_CONSUMER_SETTLEMENT;
            this.ID_PAYMENT_MODULE = clone.ID_PAYMENT_MODULE;
            this.TRANSACTION_GUID = clone.TRANSACTION_GUID;
            this.ID_PACKET = clone.ID_PACKET;
            this.ID_ACTION = clone.ID_ACTION;
            this.ID_PREVIOUS_CONSUMER_TRANSACTION = clone.ID_PREVIOUS_CONSUMER_TRANSACTION;
            CloneUser(clone);
        }

        public DB_CONSUMER_TRANSACTION()
        {
        }
    }
}