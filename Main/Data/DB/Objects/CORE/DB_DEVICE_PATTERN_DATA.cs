﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_PATTERN_DATA
    {
        public long ID_PATTERN;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;
        public int? ID_SERVER;

        partial void CloneUser(DB_DEVICE_PATTERN_DATA clone);

        public DB_DEVICE_PATTERN_DATA(DB_DEVICE_PATTERN_DATA clone)
        {
            this.ID_PATTERN = clone.ID_PATTERN;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            this.ID_SERVER = clone.ID_SERVER;
            CloneUser(clone);
        }

        public DB_DEVICE_PATTERN_DATA()
        {
        }
    }
}