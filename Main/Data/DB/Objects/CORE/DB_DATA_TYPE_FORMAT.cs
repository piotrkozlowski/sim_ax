using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DATA_TYPE_FORMAT
    {
				public int ID_DATA_TYPE_FORMAT;
				public long? ID_DESCR;
				public int? TEXT_MIN_LENGTH;
				public int? TEXT_MAX_LENGTH;
				public int? NUMBER_MIN_PRECISION;
				public int? NUMBER_MAX_PRECISION;
				public int? NUMBER_MIN_SCALE;
				public int? NUMBER_MAX_SCALE;
				public Double? NUMBER_MIN_VALUE;
				public Double? NUMBER_MAX_VALUE;
				public string DATETIME_FORMAT;
				public string REGULAR_EXPRESSION;
				public bool? IS_REQUIRED;
				public int? ID_UNIQUE_TYPE;
		    
		partial void CloneUser(DB_DATA_TYPE_FORMAT clone);

		public DB_DATA_TYPE_FORMAT(DB_DATA_TYPE_FORMAT clone)
		{
						this.ID_DATA_TYPE_FORMAT = clone.ID_DATA_TYPE_FORMAT;
						this.ID_DESCR = clone.ID_DESCR;
						this.TEXT_MIN_LENGTH = clone.TEXT_MIN_LENGTH;
						this.TEXT_MAX_LENGTH = clone.TEXT_MAX_LENGTH;
						this.NUMBER_MIN_PRECISION = clone.NUMBER_MIN_PRECISION;
						this.NUMBER_MAX_PRECISION = clone.NUMBER_MAX_PRECISION;
						this.NUMBER_MIN_SCALE = clone.NUMBER_MIN_SCALE;
						this.NUMBER_MAX_SCALE = clone.NUMBER_MAX_SCALE;
						this.NUMBER_MIN_VALUE = clone.NUMBER_MIN_VALUE;
						this.NUMBER_MAX_VALUE = clone.NUMBER_MAX_VALUE;
						this.DATETIME_FORMAT = clone.DATETIME_FORMAT;
						this.REGULAR_EXPRESSION = clone.REGULAR_EXPRESSION;
						this.IS_REQUIRED = clone.IS_REQUIRED;
						this.ID_UNIQUE_TYPE = clone.ID_UNIQUE_TYPE;
						CloneUser(clone);
		}

		public DB_DATA_TYPE_FORMAT()
		{
		}
	}
}