using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE_IN_GROUP
    {
				public int ID_DEVICE_TYPE_GROUP;
				public int ID_DEVICE_TYPE;
                public int? ID_SERVER;
		    
		partial void CloneUser(DB_DEVICE_TYPE_IN_GROUP clone);

		public DB_DEVICE_TYPE_IN_GROUP(DB_DEVICE_TYPE_IN_GROUP clone)
		{
						this.ID_DEVICE_TYPE_GROUP = clone.ID_DEVICE_TYPE_GROUP;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE_IN_GROUP()
		{
		}
	}
}