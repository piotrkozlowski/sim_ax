using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_COMMAND_CODE_TYPE
    {
		public long ID_COMMAND_CODE;
		public string NAME;
		public long? ID_DESCR;
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_COMMAND_CODE_TYPE clone);

		public DB_COMMAND_CODE_TYPE(DB_COMMAND_CODE_TYPE clone)
		{
						this.ID_COMMAND_CODE = clone.ID_COMMAND_CODE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_COMMAND_CODE_TYPE()
		{
		}
	}
}