using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_SUPPLIER
    {
				public int ID_LOCATION_SUPPLIER;
				public long ID_LOCATION;
				public int ID_DISTRIBUTOR_SUPPLIER;
				public int ACCOUNT_NO;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string NOTES;
		    
		partial void CloneUser(DB_LOCATION_SUPPLIER clone);

		public DB_LOCATION_SUPPLIER(DB_LOCATION_SUPPLIER clone)
		{
						this.ID_LOCATION_SUPPLIER = clone.ID_LOCATION_SUPPLIER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_DISTRIBUTOR_SUPPLIER = clone.ID_DISTRIBUTOR_SUPPLIER;
						this.ACCOUNT_NO = clone.ACCOUNT_NO;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_LOCATION_SUPPLIER()
		{
		}
	}
}