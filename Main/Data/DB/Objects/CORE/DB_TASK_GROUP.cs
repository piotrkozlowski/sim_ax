using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TASK_GROUP
    {
				public int ID_TASK_GROUP;
				public int? ID_PARRENT_GROUP;
				public string NAME;
				public DateTime DATE_CREATED;
				public int ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_TASK_GROUP clone);

		public DB_TASK_GROUP(DB_TASK_GROUP clone)
		{
						this.ID_TASK_GROUP = clone.ID_TASK_GROUP;
						this.ID_PARRENT_GROUP = clone.ID_PARRENT_GROUP;
						this.NAME = clone.NAME;
						this.DATE_CREATED = clone.DATE_CREATED;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_TASK_GROUP()
		{
		}
	}
}