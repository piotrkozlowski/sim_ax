using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_ORDER_NUMBER
    {
				public int ID_DEVICE_ORDER_NUMBER;
				public string NAME;
				public string FIRST_QUARTER;
				public string SECOND_QUARTER;
				public string THIRD_QUARTER;
		    
		partial void CloneUser(DB_DEVICE_ORDER_NUMBER clone);

		public DB_DEVICE_ORDER_NUMBER(DB_DEVICE_ORDER_NUMBER clone)
		{
						this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
						this.NAME = clone.NAME;
						this.FIRST_QUARTER = clone.FIRST_QUARTER;
						this.SECOND_QUARTER = clone.SECOND_QUARTER;
						this.THIRD_QUARTER = clone.THIRD_QUARTER;
						CloneUser(clone);
		}

		public DB_DEVICE_ORDER_NUMBER()
		{
		}
	}
}