﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_ORDER_NUMBER_DATA
    {
        public long ID_DEVICE_ORDER_NUMBER_DATA;
        public int ID_DEVICE_ORDER_NUMBER;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_DEVICE_ORDER_NUMBER_DATA clone);

        public DB_DEVICE_ORDER_NUMBER_DATA(DB_DEVICE_ORDER_NUMBER_DATA clone)
        {
            this.ID_DEVICE_ORDER_NUMBER_DATA = clone.ID_DEVICE_ORDER_NUMBER_DATA;
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_DEVICE_ORDER_NUMBER_DATA()
        {
        }
    }
}