﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_REFERENCE_TYPE
    {
        public int ID_SERVICE_REFERENCE_TYPE;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_SERVICE_REFERENCE_TYPE clone);

        public DB_SERVICE_REFERENCE_TYPE(DB_SERVICE_REFERENCE_TYPE clone)
        {
            this.ID_SERVICE_REFERENCE_TYPE = clone.ID_SERVICE_REFERENCE_TYPE;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_SERVICE_REFERENCE_TYPE()
        {
        }
    }
}