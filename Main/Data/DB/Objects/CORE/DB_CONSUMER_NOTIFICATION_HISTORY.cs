using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_NOTIFICATION_HISTORY
    {
				public long ID_CONSUMER_NOTIFICATION_HISTORY;
				public int ID_CONSUMER;
				public int ID_NOTIFICATION_DELIVERY_TYPE;
				public int ID_NOTIFICATION_TYPE;
				public DateTime TIME;
				public string BODY;
				public string DELIVERY_ADDRESS;
		    
		partial void CloneUser(DB_CONSUMER_NOTIFICATION_HISTORY clone);

		public DB_CONSUMER_NOTIFICATION_HISTORY(DB_CONSUMER_NOTIFICATION_HISTORY clone)
		{
						this.ID_CONSUMER_NOTIFICATION_HISTORY = clone.ID_CONSUMER_NOTIFICATION_HISTORY;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_NOTIFICATION_DELIVERY_TYPE = clone.ID_NOTIFICATION_DELIVERY_TYPE;
						this.ID_NOTIFICATION_TYPE = clone.ID_NOTIFICATION_TYPE;
						this.TIME = clone.TIME;
						this.BODY = clone.BODY;
						this.DELIVERY_ADDRESS = clone.DELIVERY_ADDRESS;
						CloneUser(clone);
		}

		public DB_CONSUMER_NOTIFICATION_HISTORY()
		{
		}
	}
}