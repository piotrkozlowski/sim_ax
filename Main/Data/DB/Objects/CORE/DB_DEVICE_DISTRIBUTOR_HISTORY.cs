﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_DISTRIBUTOR_HISTORY
    {
        public long ID_DEVICE_DISTRIBUTOR_HISTORY;
        public long SERIAL_NBR;
        public int ID_DISTRIBUTOR;
        public int? ID_DISTRIBUTOR_OWNER;
        public DateTime START_TIME;
        public DateTime? END_TIME;
        public string NOTES;
        public int ID_OPERATOR;
        public int? ID_SERVICE_REFERENCE_TYPE;
        public object REFERENCE_VALUE;

        partial void CloneUser(DB_DEVICE_DISTRIBUTOR_HISTORY clone);

        public DB_DEVICE_DISTRIBUTOR_HISTORY(DB_DEVICE_DISTRIBUTOR_HISTORY clone)
        {
            this.ID_DEVICE_DISTRIBUTOR_HISTORY = clone.ID_DEVICE_DISTRIBUTOR_HISTORY;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_DISTRIBUTOR_OWNER = clone.ID_DISTRIBUTOR_OWNER;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.NOTES = clone.NOTES;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.ID_SERVICE_REFERENCE_TYPE = clone.ID_SERVICE_REFERENCE_TYPE;
            this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
            CloneUser(clone);
        }

        public DB_DEVICE_DISTRIBUTOR_HISTORY()
        {
        }
    }
}