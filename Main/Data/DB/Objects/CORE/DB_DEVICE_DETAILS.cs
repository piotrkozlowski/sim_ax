﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_DETAILS
    {
        public long ID_DEVICE_DETAILS;
        public long? SERIAL_NBR;
        public long? ID_LOCATION;
        public string FACTORY_NBR;
        public DateTime? SHIPPING_DATE;
        public DateTime? WARRANTY_DATE;
        public string PHONE;
        public int ID_DEVICE_TYPE;
        public int ID_DEVICE_ORDER_NUMBER;
        public int ID_DISTRIBUTOR;
        public int ID_DEVICE_STATE_TYPE;
        public string PRODUCTION_DEVICE_ORDER_NUMBER;
        public string DEVICE_BATCH_NUMBER;
        public int? ID_DISTRIBUTOR_OWNER;
        public string MANUFACTURING_BATCH;

        partial void CloneUser(DB_DEVICE_DETAILS clone);

        public DB_DEVICE_DETAILS(DB_DEVICE_DETAILS clone)
        {
            this.ID_DEVICE_DETAILS = clone.ID_DEVICE_DETAILS;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.FACTORY_NBR = clone.FACTORY_NBR;
            this.SHIPPING_DATE = clone.SHIPPING_DATE;
            this.WARRANTY_DATE = clone.WARRANTY_DATE;
            this.PHONE = clone.PHONE;
            this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_DEVICE_STATE_TYPE = clone.ID_DEVICE_STATE_TYPE;
            this.PRODUCTION_DEVICE_ORDER_NUMBER = clone.PRODUCTION_DEVICE_ORDER_NUMBER;
            this.DEVICE_BATCH_NUMBER = clone.DEVICE_BATCH_NUMBER;
            this.ID_DISTRIBUTOR_OWNER = clone.ID_DISTRIBUTOR_OWNER;
            this.MANUFACTURING_BATCH = clone.MANUFACTURING_BATCH;
            CloneUser(clone);
        }

        public DB_DEVICE_DETAILS()
        {
        }
    }
}