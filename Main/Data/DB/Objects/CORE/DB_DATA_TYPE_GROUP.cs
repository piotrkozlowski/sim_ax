using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DATA_TYPE_GROUP
    {
				public int ID_DATA_TYPE_GROUP;
				public int ID_DATA_TYPE_GROUP_TYPE;
				public int? ID_REFERENCE_TYPE;
				public object REFERENCE_VALUE;
				public string NAME;
				public int? ID_PARENT_GROUP;
		    
		partial void CloneUser(DB_DATA_TYPE_GROUP clone);

		public DB_DATA_TYPE_GROUP(DB_DATA_TYPE_GROUP clone)
		{
						this.ID_DATA_TYPE_GROUP = clone.ID_DATA_TYPE_GROUP;
						this.ID_DATA_TYPE_GROUP_TYPE = clone.ID_DATA_TYPE_GROUP_TYPE;
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						this.REFERENCE_VALUE = clone.REFERENCE_VALUE;
						this.NAME = clone.NAME;
						this.ID_PARENT_GROUP = clone.ID_PARENT_GROUP;
						CloneUser(clone);
		}

		public DB_DATA_TYPE_GROUP()
		{
		}
	}
}