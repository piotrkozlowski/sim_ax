using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_WORK_ORDER_JCI
    {
				public int ID_WORK_ORDER_JCI;
				public string WONUM;
				public DateTime SENTDATETIME;
				public string WODESC;
				public string IVRCODE;
				public string SUBCASDESC;
				public string PROBLEMDESC;
				public int PRIORITY;
				public string TYPEDESC;
				public string STATUSDESC;
				public DateTime REPORTDATETIME;
				public DateTime RFDATETIME;
				public string PONUMBER;
				public string SUPPLIERID;
				public string SUPPLIERNAME;
				public string SITEID;
				public string SITENAME;
				public string SITEPHONE;
				public string EQUIPID;
				public string EQUIPDESC;
                public string DOWNTIME;
                public string LUMPSUM;
                public string DISCLAIMER_TEXT;
		    
		partial void CloneUser(DB_WORK_ORDER_JCI clone);

		public DB_WORK_ORDER_JCI(DB_WORK_ORDER_JCI clone)
		{
						this.ID_WORK_ORDER_JCI = clone.ID_WORK_ORDER_JCI;
						this.WONUM = clone.WONUM;
						this.SENTDATETIME = clone.SENTDATETIME;
						this.WODESC = clone.WODESC;
						this.IVRCODE = clone.IVRCODE;
						this.SUBCASDESC = clone.SUBCASDESC;
						this.PROBLEMDESC = clone.PROBLEMDESC;
						this.PRIORITY = clone.PRIORITY;
						this.TYPEDESC = clone.TYPEDESC;
						this.STATUSDESC = clone.STATUSDESC;
						this.REPORTDATETIME = clone.REPORTDATETIME;
						this.RFDATETIME = clone.RFDATETIME;
						this.PONUMBER = clone.PONUMBER;
						this.SUPPLIERID = clone.SUPPLIERID;
						this.SUPPLIERNAME = clone.SUPPLIERNAME;
						this.SITEID = clone.SITEID;
						this.SITENAME = clone.SITENAME;
						this.SITEPHONE = clone.SITEPHONE;
						this.EQUIPID = clone.EQUIPID;
						this.EQUIPDESC = clone.EQUIPDESC;
                        this.DOWNTIME = clone.DOWNTIME;
                        this.LUMPSUM = clone.LUMPSUM;
                        this.DISCLAIMER_TEXT = clone.DISCLAIMER_TEXT;
						CloneUser(clone);
		}

		public DB_WORK_ORDER_JCI()
		{
		}
	}
}