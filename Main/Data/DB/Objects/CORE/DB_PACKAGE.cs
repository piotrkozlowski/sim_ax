using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_PACKAGE
    {
        public int ID_PACKAGE;
        public string NAME;
        public DateTime CREATION_DATE;
        public string CODE;
        public DateTime? SEND_DATE;
        public int ID_OPERATOR_CREATOR;
        public int? ID_OPERATOR_RECEIVER;
        public bool RECEIVED;
        public DateTime? RECEIVE_DATE;
        public int ID_DISTRIBUTOR;
        public int ID_PACKAGE_STATUS;

        partial void CloneUser(DB_PACKAGE clone);

        public DB_PACKAGE(DB_PACKAGE clone)
        {
            this.ID_PACKAGE = clone.ID_PACKAGE;
            this.NAME = clone.NAME;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.CODE = clone.CODE;
            this.SEND_DATE = clone.SEND_DATE;
            this.ID_OPERATOR_CREATOR = clone.ID_OPERATOR_CREATOR;
            this.ID_OPERATOR_RECEIVER = clone.ID_OPERATOR_RECEIVER;
            this.RECEIVED = clone.RECEIVED;
            this.RECEIVE_DATE = clone.RECEIVE_DATE;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_PACKAGE_STATUS = clone.ID_PACKAGE_STATUS;
            CloneUser(clone);
        }

        public DB_PACKAGE()
        {
        }
    }
}