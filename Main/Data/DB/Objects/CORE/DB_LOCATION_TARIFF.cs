using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_TARIFF
    {
				public int ID_LOCATION_TARIFF;
				public long ID_LOCATION;
				public int ID_TARIFF;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string NOTES;
		    
		partial void CloneUser(DB_LOCATION_TARIFF clone);

		public DB_LOCATION_TARIFF(DB_LOCATION_TARIFF clone)
		{
						this.ID_LOCATION_TARIFF = clone.ID_LOCATION_TARIFF;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_TARIFF = clone.ID_TARIFF;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_LOCATION_TARIFF()
		{
		}
	}
}