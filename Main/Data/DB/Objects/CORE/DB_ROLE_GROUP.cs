using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROLE_GROUP
    {
				public int ID_ROLE_PARENT;
				public int ID_ROLE;
		    
		partial void CloneUser(DB_ROLE_GROUP clone);

		public DB_ROLE_GROUP(DB_ROLE_GROUP clone)
		{
						this.ID_ROLE_PARENT = clone.ID_ROLE_PARENT;
						this.ID_ROLE = clone.ID_ROLE;
						CloneUser(clone);
		}

		public DB_ROLE_GROUP()
		{
		}
	}
}