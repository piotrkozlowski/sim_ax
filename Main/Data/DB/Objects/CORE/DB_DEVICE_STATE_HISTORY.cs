using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_STATE_HISTORY
    {
				public long ID_DEVICE_STATE;
				public long SERIAL_NBR;
				public int ID_DEVICE_STATE_TYPE;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string NOTES;
		    
		partial void CloneUser(DB_DEVICE_STATE_HISTORY clone);

		public DB_DEVICE_STATE_HISTORY(DB_DEVICE_STATE_HISTORY clone)
		{
						this.ID_DEVICE_STATE = clone.ID_DEVICE_STATE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_DEVICE_STATE_TYPE = clone.ID_DEVICE_STATE_TYPE;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_DEVICE_STATE_HISTORY()
		{
		}
	}
}