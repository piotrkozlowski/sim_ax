using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_ALARM_STATUS
    {
        [DataMember]
        public int ID_ALARM_STATUS;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;
		    
		partial void CloneUser(DB_ALARM_STATUS clone);

		public DB_ALARM_STATUS(DB_ALARM_STATUS clone)
		{
						this.ID_ALARM_STATUS = clone.ID_ALARM_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_ALARM_STATUS()
		{
		}
	}
}