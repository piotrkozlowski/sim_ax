using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_MODULE
    {
				public int ID_MODULE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_MODULE clone);

		public DB_MODULE(DB_MODULE clone)
		{
						this.ID_MODULE = clone.ID_MODULE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_MODULE()
		{
		}
	}
}