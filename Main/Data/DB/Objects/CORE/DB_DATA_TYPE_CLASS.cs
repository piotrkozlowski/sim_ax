using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DATA_TYPE_CLASS
    {
        [DataMember]
		public int ID_DATA_TYPE_CLASS;
        [DataMember]
		public string NAME;
        [DataMember]
		public long? ID_DESCR;
		    
		partial void CloneUser(DB_DATA_TYPE_CLASS clone);

		public DB_DATA_TYPE_CLASS(DB_DATA_TYPE_CLASS clone)
		{
						this.ID_DATA_TYPE_CLASS = clone.ID_DATA_TYPE_CLASS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_DATA_TYPE_CLASS()
		{
		}
	}
}