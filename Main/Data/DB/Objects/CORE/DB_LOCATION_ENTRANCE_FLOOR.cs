using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_ENTRANCE_FLOOR
    {
				public long ID_LOCATION;
				public int ENTRANCE_INDEX_NBR;
				public int FLOOR_INDEX_NBR;
				public string NAME;
		    
		partial void CloneUser(DB_LOCATION_ENTRANCE_FLOOR clone);

		public DB_LOCATION_ENTRANCE_FLOOR(DB_LOCATION_ENTRANCE_FLOOR clone)
		{
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ENTRANCE_INDEX_NBR = clone.ENTRANCE_INDEX_NBR;
						this.FLOOR_INDEX_NBR = clone.FLOOR_INDEX_NBR;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_LOCATION_ENTRANCE_FLOOR()
		{
		}
	}
}