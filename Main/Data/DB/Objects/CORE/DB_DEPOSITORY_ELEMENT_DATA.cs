using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DEPOSITORY_ELEMENT_DATA
    {
        [DataMember]
        public long ID_DEPOSITORY_ELEMENT_DATA;
        [DataMember]
        public int ID_DEPOSITORY_ELEMENT;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
		public object VALUE;
		    
		partial void CloneUser(DB_DEPOSITORY_ELEMENT_DATA clone);

		public DB_DEPOSITORY_ELEMENT_DATA(DB_DEPOSITORY_ELEMENT_DATA clone)
		{
						this.ID_DEPOSITORY_ELEMENT_DATA = clone.ID_DEPOSITORY_ELEMENT_DATA;
						this.ID_DEPOSITORY_ELEMENT = clone.ID_DEPOSITORY_ELEMENT;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_DEPOSITORY_ELEMENT_DATA()
		{
		}
	}
}