using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DEVICE_TYPE
    {
				public int ID_DEVICE_TYPE;
				public string NAME;
				public int ID_DEVICE_TYPE_CLASS;
				public long? ID_DESCR;
				public bool TWO_WAY_TRANS_AVAILABLE;
				public int? ID_DEVICE_DRIVER;
				public int? ID_PROTOCOL;
				public int? DEFAULT_ID_DEVICE_ODER_NUMBER;
                //Projekt: DEVICE_TYPE bez FRAME_DEFINITION
			    //public object FRAME_DEFINITION;
		    
		partial void CloneUser(DB_DEVICE_TYPE clone);

		public DB_DEVICE_TYPE(DB_DEVICE_TYPE clone)
		{
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DEVICE_TYPE_CLASS = clone.ID_DEVICE_TYPE_CLASS;
						this.ID_DESCR = clone.ID_DESCR;
						this.TWO_WAY_TRANS_AVAILABLE = clone.TWO_WAY_TRANS_AVAILABLE;
						this.ID_DEVICE_DRIVER = clone.ID_DEVICE_DRIVER;
						this.ID_PROTOCOL = clone.ID_PROTOCOL;
						this.DEFAULT_ID_DEVICE_ODER_NUMBER = clone.DEFAULT_ID_DEVICE_ODER_NUMBER;
                        //Projekt: DEVICE_TYPE bez FRAME_DEFINITION
						//this.FRAME_DEFINITION = clone.FRAME_DEFINITION;
						CloneUser(clone);
		}

		public DB_DEVICE_TYPE()
		{
		}
	}
}