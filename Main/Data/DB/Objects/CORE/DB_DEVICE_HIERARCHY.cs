using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DEVICE_HIERARCHY
    {
        [DataMember]
        public long ID_DEVICE_HIERARCHY;
        [DataMember]
        public long SERIAL_NBR_PARENT;
        [DataMember]
        public int ID_SLOT_TYPE;
        [DataMember]
        public int SLOT_NBR;
        [DataMember]
        public int? ID_PROTOCOL_IN;
        [DataMember]
        public int? ID_PROTOCOL_OUT;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public bool IS_ACTIVE;
        [DataMember]
        public DateTime START_TIME;
        [DataMember]
		public DateTime? END_TIME;
		    
		partial void CloneUser(DB_DEVICE_HIERARCHY clone);

		public DB_DEVICE_HIERARCHY(DB_DEVICE_HIERARCHY clone)
		{
						this.ID_DEVICE_HIERARCHY = clone.ID_DEVICE_HIERARCHY;
						this.SERIAL_NBR_PARENT = clone.SERIAL_NBR_PARENT;
						this.ID_SLOT_TYPE = clone.ID_SLOT_TYPE;
						this.SLOT_NBR = clone.SLOT_NBR;
						this.ID_PROTOCOL_IN = clone.ID_PROTOCOL_IN;
						this.ID_PROTOCOL_OUT = clone.ID_PROTOCOL_OUT;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.IS_ACTIVE = clone.IS_ACTIVE;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_DEVICE_HIERARCHY()
		{
		}
	}
}