using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK_DATA
    {
        public long ID_WORK_DATA;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;

        partial void CloneUser(DB_WORK_DATA clone);

        public DB_WORK_DATA(DB_WORK_DATA clone)
        {
            this.ID_WORK_DATA = clone.ID_WORK_DATA;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_WORK_DATA()
        {
        }
    }
}