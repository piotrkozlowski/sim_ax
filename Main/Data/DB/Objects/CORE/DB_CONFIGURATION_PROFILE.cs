using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_CONFIGURATION_PROFILE
    {
        [DataMember]
				public long ID_CONFIGURATION_PROFILE;
        [DataMember]
        public int ID_CONFIGURATION_PROFILE_TYPE;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public string NAME;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public int ID_OPERATOR_CREATED;
        [DataMember]
        public DateTime CREATED_TIME;
		    
		partial void CloneUser(DB_CONFIGURATION_PROFILE clone);

		public DB_CONFIGURATION_PROFILE(DB_CONFIGURATION_PROFILE clone)
		{
						this.ID_CONFIGURATION_PROFILE = clone.ID_CONFIGURATION_PROFILE;
						this.ID_CONFIGURATION_PROFILE_TYPE = clone.ID_CONFIGURATION_PROFILE_TYPE;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_OPERATOR_CREATED = clone.ID_OPERATOR_CREATED;
						this.CREATED_TIME = clone.CREATED_TIME;
						CloneUser(clone);
		}

		public DB_CONFIGURATION_PROFILE()
		{
		}
	}
}