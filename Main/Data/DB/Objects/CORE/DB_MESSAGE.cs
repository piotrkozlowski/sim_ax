﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_MESSAGE
    {
        public long ID_MESSAGE;
        public int ID_MESSAGE_TYPE;
        public long? ID_MESSAGE_PARENT;
        public int ID_OPERATOR;
        public string MESSAGE_CONTENT;
        public bool HAS_ATTACHMENT;
        public int? POINTS;
        public string HIERARCHY;
        public DateTime INSERT_DATE;

        partial void CloneUser(DB_MESSAGE clone);

        public DB_MESSAGE(DB_MESSAGE clone)
        {
            this.ID_MESSAGE = clone.ID_MESSAGE;
            this.ID_MESSAGE_TYPE = clone.ID_MESSAGE_TYPE;
            this.ID_MESSAGE_PARENT = clone.ID_MESSAGE_PARENT;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.MESSAGE_CONTENT = clone.MESSAGE_CONTENT;
            this.HAS_ATTACHMENT = clone.HAS_ATTACHMENT;
            this.POINTS = clone.POINTS;
            this.HIERARCHY = clone.HIERARCHY;
            this.INSERT_DATE = clone.INSERT_DATE;
            CloneUser(clone);
        }

        public DB_MESSAGE()
        {
        }
    }
}