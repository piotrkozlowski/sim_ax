using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_TASK_DATA
    {
        [DataMember]
        public long ID_TASK_DATA;
        [DataMember]
        public int ID_TASK;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
		public object VALUE;
		    
		partial void CloneUser(DB_TASK_DATA clone);

		public DB_TASK_DATA(DB_TASK_DATA clone)
		{
						this.ID_TASK_DATA = clone.ID_TASK_DATA;
						this.ID_TASK = clone.ID_TASK;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_TASK_DATA()
		{
		}
	}
}