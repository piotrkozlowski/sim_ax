using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_DEVICE
    {
				public long ID_ROUTE_DEVICE;
				public long ID_ROUTE;
				public long SERIAL_NBR;
		    
		partial void CloneUser(DB_ROUTE_DEVICE clone);

		public DB_ROUTE_DEVICE(DB_ROUTE_DEVICE clone)
		{
						this.ID_ROUTE_DEVICE = clone.ID_ROUTE_DEVICE;
						this.ID_ROUTE = clone.ID_ROUTE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						CloneUser(clone);
		}

		public DB_ROUTE_DEVICE()
		{
		}
	}
}