using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_VERSION_NBR_FORMAT
    {
		public int ID_VERSION_NBR_FORMAT;
		public string NAME;
		public long? ID_DESCR;
		public int ID_DATA_TYPE_CLASS;
		public string REGULAR_EXPRESSION;
		    
		partial void CloneUser(DB_VERSION_NBR_FORMAT clone);

		public DB_VERSION_NBR_FORMAT(DB_VERSION_NBR_FORMAT clone)
		{
			this.ID_VERSION_NBR_FORMAT = clone.ID_VERSION_NBR_FORMAT;
			this.NAME = clone.NAME;
			this.ID_DESCR = clone.ID_DESCR;
			this.ID_DATA_TYPE_CLASS = clone.ID_DATA_TYPE_CLASS;
			this.REGULAR_EXPRESSION = clone.REGULAR_EXPRESSION;
			CloneUser(clone);
		}

		public DB_VERSION_NBR_FORMAT()
		{
		}
	}
}