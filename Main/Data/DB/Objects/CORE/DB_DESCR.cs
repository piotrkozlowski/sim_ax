using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [DataContract, Serializable]
    public partial class DB_DESCR
    {
        [DataMember]
        public long ID_DESCR;
        [DataMember]
        public int ID_LANGUAGE;
        [DataMember]
        public string DESCRIPTION;
        [DataMember]
        public string EXTENDED_DESCRIPTION;
        [DataMember]
        public int? ID_SERVER;

        partial void CloneUser(DB_DESCR clone);

        public DB_DESCR(DB_DESCR clone)
        {
            this.ID_DESCR = clone.ID_DESCR;
            this.ID_LANGUAGE = clone.ID_LANGUAGE;
            this.DESCRIPTION = clone.DESCRIPTION;
            this.EXTENDED_DESCRIPTION = clone.EXTENDED_DESCRIPTION;
            this.ID_SERVER = clone.ID_SERVER;
            CloneUser(clone);
        }

        public DB_DESCR()
        {
        }
    }
}