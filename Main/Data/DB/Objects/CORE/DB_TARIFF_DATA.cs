using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TARIFF_DATA
    {
				public long ID_TARIFF_DATA;
				public int ID_TARIFF;
				public long ID_DATA_TYPE;
				public object VALUE;
				public int INDEX_NBR;
		    
		partial void CloneUser(DB_TARIFF_DATA clone);

		public DB_TARIFF_DATA(DB_TARIFF_DATA clone)
		{
						this.ID_TARIFF_DATA = clone.ID_TARIFF_DATA;
						this.ID_TARIFF = clone.ID_TARIFF;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
						this.INDEX_NBR = clone.INDEX_NBR;
						CloneUser(clone);
		}

		public DB_TARIFF_DATA()
		{
		}
	}
}