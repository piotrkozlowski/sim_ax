﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DATA_TRANSFER
    {
        [DataMember]
        public long ID_DATA_TRANSFER;
        [DataMember]
        public long BATCH_ID;
        [DataMember]
        public int CHANGE_TYPE;
        [DataMember]
        public int CHANGE_DIRECTION;
        [DataMember]
        public int? ID_SOURCE_SERVER;
        [DataMember]
        public int? ID_DESTINATION_SERVER;
        [DataMember]
        public string TABLE_NAME;
        [DataMember]
        public long? KEY1;
        [DataMember]
        public long? KEY2;
        [DataMember]
        public long? KEY3;
        [DataMember]
        public long? KEY4;
        [DataMember]
        public long? KEY5;
        [DataMember]
        public long? KEY6;
        [DataMember]
        public string COLUMN_NAME;
        [DataMember]
        public object OLD_VALUE;
        [DataMember]
        public object NEW_VALUE;
        [DataMember]
        public DateTime INSERT_TIME;
        [DataMember]
        public DateTime? PROCEED_TIME;
        [DataMember]
        public string USER;
        [DataMember]
        public string HOST;
        [DataMember]
        public string APPLICATION;

        partial void CloneUser(DB_DATA_TRANSFER clone);

        public DB_DATA_TRANSFER(DB_DATA_TRANSFER clone)
        {
            this.ID_DATA_TRANSFER = clone.ID_DATA_TRANSFER;
            this.BATCH_ID = clone.BATCH_ID;
            this.CHANGE_TYPE = clone.CHANGE_TYPE;
            this.CHANGE_DIRECTION = clone.CHANGE_DIRECTION;
            this.ID_SOURCE_SERVER = clone.ID_SOURCE_SERVER;
            this.ID_DESTINATION_SERVER = clone.ID_DESTINATION_SERVER;
            this.TABLE_NAME = clone.TABLE_NAME;
            this.KEY1 = clone.KEY1;
            this.KEY2 = clone.KEY2;
            this.KEY3 = clone.KEY3;
            this.KEY4 = clone.KEY4;
            this.KEY5 = clone.KEY5;
            this.KEY6 = clone.KEY6;
            this.COLUMN_NAME = clone.COLUMN_NAME;
            this.OLD_VALUE = clone.OLD_VALUE;
            this.NEW_VALUE = clone.NEW_VALUE;
            this.INSERT_TIME = clone.INSERT_TIME;
            this.PROCEED_TIME = clone.PROCEED_TIME;
            this.USER = clone.USER;
            this.HOST = clone.HOST;
            this.APPLICATION = clone.APPLICATION;
            CloneUser(clone);
        }

        public DB_DATA_TRANSFER()
        {
        }
    }
}