using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ETL
    {
				public int ID_PARAM;
				public string CODE;
				public string DESCR;
				public object VALUE;
		    
		partial void CloneUser(DB_ETL clone);

		public DB_ETL(DB_ETL clone)
		{
						this.ID_PARAM = clone.ID_PARAM;
						this.CODE = clone.CODE;
						this.DESCR = clone.DESCR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_ETL()
		{
		}
	}
}