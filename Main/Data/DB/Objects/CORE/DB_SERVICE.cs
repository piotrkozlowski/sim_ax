﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE
    {
        public int ID_SERVICE;
        public string NAME;
        public int? ID_DEVICE_ORDER_NUMBER;
        public DateTime? DELETE_DATE;
        public long? ID_DESCR;

        partial void CloneUser(DB_SERVICE clone);

        public DB_SERVICE(DB_SERVICE clone)
        {
            this.ID_SERVICE = clone.ID_SERVICE;
            this.NAME = clone.NAME;
            this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
            this.DELETE_DATE = clone.DELETE_DATE;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_SERVICE()
        {
        }
    }
}