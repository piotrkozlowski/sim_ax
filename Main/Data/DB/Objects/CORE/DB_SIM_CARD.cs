using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SIM_CARD
    {
				public int ID_SIM_CARD;
				public int ID_DISTRIBUTOR;
				public string SERIAL_NBR;
				public string PHONE;
				public string PIN;
				public string PUK;
				public int? MOBILE_NETWORK_CODE;
				public string IP;
				public string APN_LOGIN;
				public string APN_PASSWORD;
		    
		partial void CloneUser(DB_SIM_CARD clone);

		public DB_SIM_CARD(DB_SIM_CARD clone)
		{
						this.ID_SIM_CARD = clone.ID_SIM_CARD;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.PHONE = clone.PHONE;
						this.PIN = clone.PIN;
						this.PUK = clone.PUK;
						this.MOBILE_NETWORK_CODE = clone.MOBILE_NETWORK_CODE;
						this.IP = clone.IP;
						this.APN_LOGIN = clone.APN_LOGIN;
						this.APN_PASSWORD = clone.APN_PASSWORD;
						CloneUser(clone);
		}

		public DB_SIM_CARD()
		{
		}
	}
}