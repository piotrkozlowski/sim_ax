using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_METER
    {
        [DataMember]
        public long ID_METER;
        [DataMember]
        public int ID_METER_TYPE;
        [DataMember]
        public long? ID_METER_PATTERN;
        [DataMember]
        public long? ID_DESCR_PATTERN;
        [DataMember]
		public int ID_DISTRIBUTOR;
		    
		partial void CloneUser(DB_METER clone);

		public DB_METER(DB_METER clone)
		{
						this.ID_METER = clone.ID_METER;
						this.ID_METER_TYPE = clone.ID_METER_TYPE;
						this.ID_METER_PATTERN = clone.ID_METER_PATTERN;
						this.ID_DESCR_PATTERN = clone.ID_DESCR_PATTERN;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						CloneUser(clone);
		}

		public DB_METER()
		{
		}
	}
}