using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_LOCATION
    {
        [DataMember]
        public long ID_LOCATION;
        [DataMember]
        public int ID_LOCATION_TYPE;
        [DataMember]
        public long? ID_LOCATION_PATTERN;
        [DataMember]
        public long? ID_DESCR_PATTERN;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public int? ID_CONSUMER;
        [DataMember]
        public int ID_LOCATION_STATE_TYPE;
        [DataMember]
        public bool IN_KPI;
        [DataMember]
        public bool ALLOW_GROUPING;
        [DataMember]
        public int? ID_SERVER;
		    
		partial void CloneUser(DB_LOCATION clone);

		public DB_LOCATION(DB_LOCATION clone)
		{
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_LOCATION_TYPE = clone.ID_LOCATION_TYPE;
						this.ID_LOCATION_PATTERN = clone.ID_LOCATION_PATTERN;
						this.ID_DESCR_PATTERN = clone.ID_DESCR_PATTERN;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_LOCATION_STATE_TYPE = clone.ID_LOCATION_STATE_TYPE;
						this.IN_KPI = clone.IN_KPI;
						this.ALLOW_GROUPING = clone.ALLOW_GROUPING;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_LOCATION()
		{
		}
	}
}