﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_HIERARCHY_GROUP
    {
        public long ID_DEVICE_HIERARCHY_GROUP;
        public string DEVICE_HIERARCHY_SERIAL_NBR;
        public int? ID_DISTRIBUTOR;
        public long? ID_DEPOSITORY_LOCATION;
        public DateTime CREATION_DATE;
        public DateTime? FINISH_DATE;

        partial void CloneUser(DB_DEVICE_HIERARCHY_GROUP clone);

        public DB_DEVICE_HIERARCHY_GROUP(DB_DEVICE_HIERARCHY_GROUP clone)
        {
            this.ID_DEVICE_HIERARCHY_GROUP = clone.ID_DEVICE_HIERARCHY_GROUP;
            this.DEVICE_HIERARCHY_SERIAL_NBR = clone.DEVICE_HIERARCHY_SERIAL_NBR;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_DEPOSITORY_LOCATION = clone.ID_DEPOSITORY_LOCATION;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.FINISH_DATE = clone.FINISH_DATE;
            CloneUser(clone);
        }

        public DB_DEVICE_HIERARCHY_GROUP()
        {
        }
    }
}