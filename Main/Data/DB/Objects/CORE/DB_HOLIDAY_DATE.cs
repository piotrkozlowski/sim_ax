﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_HOLIDAY_DATE
    {
        public DateTime DATE;
        public string COUNTRY_CODE;

        partial void CloneUser(DB_HOLIDAY_DATE clone);

        public DB_HOLIDAY_DATE(DB_HOLIDAY_DATE clone)
        {
            this.DATE = clone.DATE;
            this.COUNTRY_CODE = clone.COUNTRY_CODE;
            CloneUser(clone);
        }

        public DB_HOLIDAY_DATE()
        {
        }
    }
}