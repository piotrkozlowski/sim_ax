using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_FAULT_CODE_DATA
    {
				public long ID_FAULT_CODE_DATA;
				public int ID_FAULT_CODE;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_FAULT_CODE_DATA clone);

		public DB_FAULT_CODE_DATA(DB_FAULT_CODE_DATA clone)
		{
						this.ID_FAULT_CODE_DATA = clone.ID_FAULT_CODE_DATA;
						this.ID_FAULT_CODE = clone.ID_FAULT_CODE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_FAULT_CODE_DATA()
		{
		}
	}
}