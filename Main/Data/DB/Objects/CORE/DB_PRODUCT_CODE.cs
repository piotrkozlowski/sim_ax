using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_PRODUCT_CODE
    {
				public int ID_PRODUCT_CODE;
				public string CODE;
				public long? ID_DESCR;
				public int? DENSITY;
				public Double ALPHA;
				public Double CALORIFIC_VALUE;
		    
		partial void CloneUser(DB_PRODUCT_CODE clone);

		public DB_PRODUCT_CODE(DB_PRODUCT_CODE clone)
		{
						this.ID_PRODUCT_CODE = clone.ID_PRODUCT_CODE;
						this.CODE = clone.CODE;
						this.ID_DESCR = clone.ID_DESCR;
						this.DENSITY = clone.DENSITY;
						this.ALPHA = clone.ALPHA;
						this.CALORIFIC_VALUE = clone.CALORIFIC_VALUE;
						CloneUser(clone);
		}

		public DB_PRODUCT_CODE()
		{
		}
	}
}