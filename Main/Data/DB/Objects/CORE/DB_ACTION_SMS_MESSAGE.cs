using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_SMS_MESSAGE
    {
				public long ID_ACTION_SMS_MESSAGE;
				public long SERIAL_NBR;
				public DateTime TIME;
				public bool IS_INCOMING;
				public int ID_TRANSMISSION_STATUS;
				public long? ID_ACTION;
				public string MESSAGE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_ACTION_SMS_MESSAGE clone);

		public DB_ACTION_SMS_MESSAGE(DB_ACTION_SMS_MESSAGE clone)
		{
						this.ID_ACTION_SMS_MESSAGE = clone.ID_ACTION_SMS_MESSAGE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.TIME = clone.TIME;
						this.IS_INCOMING = clone.IS_INCOMING;
						this.ID_TRANSMISSION_STATUS = clone.ID_TRANSMISSION_STATUS;
						this.ID_ACTION = clone.ID_ACTION;
						this.MESSAGE = clone.MESSAGE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_ACTION_SMS_MESSAGE()
		{
		}
	}
}