using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_SETTLEMENT_ERROR_TYPE
    {
				public int ID_CONSUMER_SETTLEMENT_ERROR_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_CONSUMER_SETTLEMENT_ERROR_TYPE clone);

		public DB_CONSUMER_SETTLEMENT_ERROR_TYPE(DB_CONSUMER_SETTLEMENT_ERROR_TYPE clone)
		{
						this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = clone.ID_CONSUMER_SETTLEMENT_ERROR_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_CONSUMER_SETTLEMENT_ERROR_TYPE()
		{
		}
	}
}