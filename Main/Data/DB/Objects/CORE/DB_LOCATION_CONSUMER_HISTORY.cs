using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_LOCATION_CONSUMER_HISTORY
    {
				public long ID_LOCATION_CONSUMER_HISTORY;
				public long ID_LOCATION;
				public int ID_CONSUMER;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public string NOTES;
		    
		partial void CloneUser(DB_LOCATION_CONSUMER_HISTORY clone);

		public DB_LOCATION_CONSUMER_HISTORY(DB_LOCATION_CONSUMER_HISTORY clone)
		{
						this.ID_LOCATION_CONSUMER_HISTORY = clone.ID_LOCATION_CONSUMER_HISTORY;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_LOCATION_CONSUMER_HISTORY()
		{
		}
	}
}