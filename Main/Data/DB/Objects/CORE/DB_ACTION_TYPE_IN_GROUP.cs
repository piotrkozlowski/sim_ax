using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ACTION_TYPE_IN_GROUP
    {
				public int ID_ACTION_TYPE_GROUP;
				public string ACTION_TYPE_NAME;

                public int? ID_SERVER;
		    
		partial void CloneUser(DB_ACTION_TYPE_IN_GROUP clone);

		public DB_ACTION_TYPE_IN_GROUP(DB_ACTION_TYPE_IN_GROUP clone)
		{
						this.ID_ACTION_TYPE_GROUP = clone.ID_ACTION_TYPE_GROUP;
						this.ACTION_TYPE_NAME = clone.ACTION_TYPE_NAME;
                        this.ID_SERVER = clone.ID_SERVER;
						CloneUser(clone);
		}

		public DB_ACTION_TYPE_IN_GROUP()
		{
		}
	}
}