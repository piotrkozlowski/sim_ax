using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DEVICE
    {
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public int ID_DEVICE_TYPE;
        [DataMember]
        public long? SERIAL_NBR_PATTERN;
        [DataMember]
        public long? ID_DESCR_PATTERN;
        [DataMember]
        public int ID_DEVICE_ORDER_NUMBER;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public int ID_DEVICE_STATE_TYPE;
		    
		partial void CloneUser(DB_DEVICE clone);

		public DB_DEVICE(DB_DEVICE clone)
		{
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_DEVICE_TYPE = clone.ID_DEVICE_TYPE;
						this.SERIAL_NBR_PATTERN = clone.SERIAL_NBR_PATTERN;
						this.ID_DESCR_PATTERN = clone.ID_DESCR_PATTERN;
						this.ID_DEVICE_ORDER_NUMBER = clone.ID_DEVICE_ORDER_NUMBER;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_DEVICE_STATE_TYPE = clone.ID_DEVICE_STATE_TYPE;
						CloneUser(clone);
		}

		public DB_DEVICE()
		{
		}
	}
}