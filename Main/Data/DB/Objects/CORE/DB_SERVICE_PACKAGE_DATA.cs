﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_SERVICE_PACKAGE_DATA
    {
        public long ID_SERVICE_PACKAGE_DATA;
        public long ID_SERVICE_PACKAGE;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public int? MIN;
        public int? MAX;
        public object VALUE;

        partial void CloneUser(DB_SERVICE_PACKAGE_DATA clone);

        public DB_SERVICE_PACKAGE_DATA(DB_SERVICE_PACKAGE_DATA clone)
        {
            this.ID_SERVICE_PACKAGE_DATA = clone.ID_SERVICE_PACKAGE_DATA;
            this.ID_SERVICE_PACKAGE = clone.ID_SERVICE_PACKAGE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.MIN = clone.MIN;
            this.MAX = clone.MAX;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_SERVICE_PACKAGE_DATA()
        {
        }
    }
}