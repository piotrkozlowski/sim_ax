using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_UNIQUE_TYPE
    {
				public int ID_UNIQUE_TYPE;
				public long? ID_DESCR;
				public string PROC_NAME;
		    
		partial void CloneUser(DB_UNIQUE_TYPE clone);

		public DB_UNIQUE_TYPE(DB_UNIQUE_TYPE clone)
		{
						this.ID_UNIQUE_TYPE = clone.ID_UNIQUE_TYPE;
						this.ID_DESCR = clone.ID_DESCR;
						this.PROC_NAME = clone.PROC_NAME;
						CloneUser(clone);
		}

		public DB_UNIQUE_TYPE()
		{
		}
	}
}