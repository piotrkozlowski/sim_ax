using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CODE
    {
				public long ID_CODE;
				public long? SERIAL_NBR;
				public long? ID_METER;
				public long? ID_LOCATION;
				public int ID_CODE_TYPE;
				public int CODE_NBR;
				public string CODE;
				public bool IS_ACTIVE;
				public DateTime? ACTIVATION_TIME;
		    
		partial void CloneUser(DB_CODE clone);

		public DB_CODE(DB_CODE clone)
		{
						this.ID_CODE = clone.ID_CODE;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_CODE_TYPE = clone.ID_CODE_TYPE;
						this.CODE_NBR = clone.CODE_NBR;
						this.CODE = clone.CODE;
						this.IS_ACTIVE = clone.IS_ACTIVE;
						this.ACTIVATION_TIME = clone.ACTIVATION_TIME;
						CloneUser(clone);
		}

		public DB_CODE()
		{
		}
	}
}