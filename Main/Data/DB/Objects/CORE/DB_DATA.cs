using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_DATA
    {
        [DataMember]
        public long ID_DATA;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
	    public int STATUS;
		    
		partial void CloneUser(DB_DATA clone);

		public DB_DATA(DB_DATA clone)
		{
						this.ID_DATA = clone.ID_DATA;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						this.STATUS = clone.STATUS;
						CloneUser(clone);
		}

		public DB_DATA()
		{
		}
	}
}