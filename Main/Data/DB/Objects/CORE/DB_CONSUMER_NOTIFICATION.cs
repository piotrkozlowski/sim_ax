using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_CONSUMER_NOTIFICATION
    {
				public long ID_CONSUMER_NOTIFICATION;
				public int ID_CONSUMER;
				public int ID_NOTIFICATION_DELIVERY_TYPE;
				public int ID_OPERATOR;
				public DateTime START_TIME;
				public DateTime? END_TIME;
		    
		partial void CloneUser(DB_CONSUMER_NOTIFICATION clone);

		public DB_CONSUMER_NOTIFICATION(DB_CONSUMER_NOTIFICATION clone)
		{
						this.ID_CONSUMER_NOTIFICATION = clone.ID_CONSUMER_NOTIFICATION;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_NOTIFICATION_DELIVERY_TYPE = clone.ID_NOTIFICATION_DELIVERY_TYPE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_CONSUMER_NOTIFICATION()
		{
		}
	}
}