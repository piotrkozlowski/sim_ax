﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DEVICE_WARRANTY
    {
        public int ID_DEVICE_WARRANTY;
        public int? ID_SHIPPING_LIST;
        public long SERIAL_NBR;
        public int ID_COMPONENT;
        public int? ID_CONTRACT;
        public int WARRANTY_LENGTH;
        public DateTime? SHIPPING_DATE;
        public DateTime? INSTALLATION_DATE;
        public DateTime START_DATE;
        public DateTime? END_DATE;

        partial void CloneUser(DB_DEVICE_WARRANTY clone);

        public DB_DEVICE_WARRANTY(DB_DEVICE_WARRANTY clone)
        {
            this.ID_DEVICE_WARRANTY = clone.ID_DEVICE_WARRANTY;
            this.ID_SHIPPING_LIST = clone.ID_SHIPPING_LIST;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_COMPONENT = clone.ID_COMPONENT;
            this.ID_CONTRACT = clone.ID_CONTRACT;
            this.WARRANTY_LENGTH = clone.WARRANTY_LENGTH;
            this.SHIPPING_DATE = clone.SHIPPING_DATE;
            this.INSTALLATION_DATE = clone.INSTALLATION_DATE;
            this.START_DATE = clone.START_DATE;
            this.END_DATE = clone.END_DATE;
            CloneUser(clone);
        }

        public DB_DEVICE_WARRANTY()
        {
        }
    }
}