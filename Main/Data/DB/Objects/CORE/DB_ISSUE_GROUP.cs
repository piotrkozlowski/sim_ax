﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ISSUE_GROUP
    {
        public int ID_ISSUE_GROUP;
        public int? ID_PARENT_GROUP;
        public string NAME;
        public DateTime DATE_CREATED;
        public int ID_DISTRIBUTOR;

        partial void CloneUser(DB_ISSUE_GROUP clone);

        public DB_ISSUE_GROUP(DB_ISSUE_GROUP clone)
        {
            this.ID_ISSUE_GROUP = clone.ID_ISSUE_GROUP;
            this.ID_PARENT_GROUP = clone.ID_PARENT_GROUP;
            this.NAME = clone.NAME;
            this.DATE_CREATED = clone.DATE_CREATED;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            CloneUser(clone);
        }

        public DB_ISSUE_GROUP()
        {
        }
    }
}