using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_IMR_SERVER
    {
        [DataMember]
				public int ID_IMR_SERVER;
        [DataMember]
        public string IP_ADDRESS;
        [DataMember]
        public string DNS_NAME;
        [DataMember]
        public string WEBSERVICE_ADDRESS;
        [DataMember]
        public int? WEBSERVICE_TIMEOUT;
        [DataMember]
        public string LOGIN;
        [DataMember]
        public string PASSWORD;
        [DataMember]
        public string STANDARD_DESCRIPTION;
        [DataMember]
        public bool IS_GOOD;
        [DataMember]
        public DateTime CREATION_DATE;
        [DataMember]
        public string UTDC_SSEC_USER;
        [DataMember]
        public string UTDC_SSEC_PASS;
        [DataMember]
        public string COLLATION;
        [DataMember]
        public string DB_NAME;
        [DataMember]
        public int IMR_SERVER_VERSION;
        [DataMember]
        public bool IS_IMRLT;
		    
		partial void CloneUser(DB_IMR_SERVER clone);

		public DB_IMR_SERVER(DB_IMR_SERVER clone)
		{
						this.ID_IMR_SERVER = clone.ID_IMR_SERVER;
						this.IP_ADDRESS = clone.IP_ADDRESS;
						this.DNS_NAME = clone.DNS_NAME;
						this.WEBSERVICE_ADDRESS = clone.WEBSERVICE_ADDRESS;
						this.WEBSERVICE_TIMEOUT = clone.WEBSERVICE_TIMEOUT;
						this.LOGIN = clone.LOGIN;
						this.PASSWORD = clone.PASSWORD;
						this.STANDARD_DESCRIPTION = clone.STANDARD_DESCRIPTION;
						this.IS_GOOD = clone.IS_GOOD;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.UTDC_SSEC_USER = clone.UTDC_SSEC_USER;
						this.UTDC_SSEC_PASS = clone.UTDC_SSEC_PASS;
						this.COLLATION = clone.COLLATION;
						this.DB_NAME = clone.DB_NAME;
						this.IMR_SERVER_VERSION = clone.IMR_SERVER_VERSION;
						this.IS_IMRLT = clone.IS_IMRLT;
						CloneUser(clone);
		}

		public DB_IMR_SERVER()
		{
		}
	}
}