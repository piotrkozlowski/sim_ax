using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_SYSTEM_DATA
    {
				public long ID_SYSTEM_DATA;
				public long ID_DATA_TYPE;
                public int INDEX_NBR;
				public object VALUE;
		    
		partial void CloneUser(DB_SYSTEM_DATA clone);

		public DB_SYSTEM_DATA(DB_SYSTEM_DATA clone)
		{
						this.ID_SYSTEM_DATA = clone.ID_SYSTEM_DATA;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
                        this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_SYSTEM_DATA()
		{
		}
	}
}