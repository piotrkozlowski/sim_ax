using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
	public partial class DB_TASK_TYPE
    {
        [DataMember]
		public int ID_TASK_TYPE;
        [DataMember]
        public int ID_TASK_TYPE_GROUP;
        [DataMember]
        public string NAME;
        [DataMember]
        public string DESCR;
        [DataMember]
        public bool REMOTE;
        [DataMember]
        public int? DURATION_ESTIMATE;
        [DataMember]
        public int ID_OPERATOR_CREATOR;
        [DataMember]
        public DateTime CREATION_DATE;
        [DataMember]
        public bool AUTHORIZED;
        [DataMember]
        public bool IMR_OPERATOR;
		public object ROW_VERSION;
		    
		partial void CloneUser(DB_TASK_TYPE clone);

		public DB_TASK_TYPE(DB_TASK_TYPE clone)
		{
						this.ID_TASK_TYPE = clone.ID_TASK_TYPE;
						this.ID_TASK_TYPE_GROUP = clone.ID_TASK_TYPE_GROUP;
						this.NAME = clone.NAME;
						this.DESCR = clone.DESCR;
						this.REMOTE = clone.REMOTE;
						this.DURATION_ESTIMATE = clone.DURATION_ESTIMATE;
						this.ID_OPERATOR_CREATOR = clone.ID_OPERATOR_CREATOR;
						this.CREATION_DATE = clone.CREATION_DATE;
						this.AUTHORIZED = clone.AUTHORIZED;
						this.IMR_OPERATOR = clone.IMR_OPERATOR;
						this.ROW_VERSION = clone.ROW_VERSION;
						CloneUser(clone);
		}

		public DB_TASK_TYPE()
		{
		}
	}
}