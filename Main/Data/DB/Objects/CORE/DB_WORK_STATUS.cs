using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_WORK_STATUS
    {
        public int ID_WORK_STATUS;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_WORK_STATUS clone);

        public DB_WORK_STATUS(DB_WORK_STATUS clone)
        {
            this.ID_WORK_STATUS = clone.ID_WORK_STATUS;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_WORK_STATUS()
        {
        }
    }
}