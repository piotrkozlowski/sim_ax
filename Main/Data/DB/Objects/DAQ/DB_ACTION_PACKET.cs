using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
	public partial class DB_ACTION_PACKET
    {
				public long ID_ACTION;
				public long? ID_PACKET;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_ACTION_PACKET clone);

		public DB_ACTION_PACKET(DB_ACTION_PACKET clone)
		{
						this.ID_ACTION = clone.ID_ACTION;
						this.ID_PACKET = clone.ID_PACKET;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_ACTION_PACKET()
		{
		}
	}
}