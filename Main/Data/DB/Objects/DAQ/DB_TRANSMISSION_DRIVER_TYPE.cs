using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
	public partial class DB_TRANSMISSION_DRIVER_TYPE
    {
				public int ID_TRANSMISSION_DRIVER_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_TRANSMISSION_DRIVER_TYPE clone);

		public DB_TRANSMISSION_DRIVER_TYPE(DB_TRANSMISSION_DRIVER_TYPE clone)
		{
						this.ID_TRANSMISSION_DRIVER_TYPE = clone.ID_TRANSMISSION_DRIVER_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_TRANSMISSION_DRIVER_TYPE()
		{
		}
	}
}