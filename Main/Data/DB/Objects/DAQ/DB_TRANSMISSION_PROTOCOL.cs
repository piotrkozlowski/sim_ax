using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
    public partial class DB_TRANSMISSION_PROTOCOL
    {
        public int ID_TRANSMISSION_PROTOCOL;
        public int ID_TRANSMISSION_DRIVER;
        public int ORDER;
        public int ID_PROTOCOL_DRIVER;

        partial void CloneUser(DB_TRANSMISSION_PROTOCOL clone);

        public DB_TRANSMISSION_PROTOCOL(DB_TRANSMISSION_PROTOCOL clone)
        {
            this.ID_TRANSMISSION_PROTOCOL = clone.ID_TRANSMISSION_PROTOCOL;
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.ORDER = clone.ORDER;
            this.ID_PROTOCOL_DRIVER = clone.ID_PROTOCOL_DRIVER;
            CloneUser(clone);
        }

        public DB_TRANSMISSION_PROTOCOL()
        {
        }
    }
}