﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
    public partial class DB_PACKET_DATA_ARCH
    {

        public long SERIAL_NBR;
        public long ID_ACTION;
        public long ID_PACKET;
        public long ID_DATA_TYPE;
        public string NAME;
        public int? INDEX_NBR;
        public object VALUE;
        public string UNIT;
        public DateTime TIME;
        public string STATUS;
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector

        partial void CloneUser(DB_PACKET_DATA_ARCH clone);

        public DB_PACKET_DATA_ARCH(DB_PACKET_DATA_ARCH clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_ACTION = clone.ID_ACTION;
            this.ID_PACKET = clone.ID_PACKET;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.NAME = clone.NAME;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            this.UNIT = clone.UNIT;
            this.TIME = clone.TIME;
            this.STATUS = clone.STATUS;
            this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
            CloneUser(clone);
        }

        public DB_PACKET_DATA_ARCH()
        {
        }
    }
}