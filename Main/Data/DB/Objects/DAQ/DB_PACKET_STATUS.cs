using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
	public partial class DB_PACKET_STATUS
    {
				public int ID_PACKET_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_PACKET_STATUS clone);

		public DB_PACKET_STATUS(DB_PACKET_STATUS clone)
		{
						this.ID_PACKET_STATUS = clone.ID_PACKET_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_PACKET_STATUS()
		{
		}
	}
}