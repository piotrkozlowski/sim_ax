using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_CONNECTION
    {
        public long ID;
        public int ID_CONNECTION;
        public int ORDER;
        public int LINK_TYPE_OUT;
        public int LINK_TYPE_IN;
        public int RETRY_ATTEMPT;
        public int TIME_ATTEMPT;
        public string NAME;

        partial void CloneUser(DB_CONNECTION clone);

        public DB_CONNECTION(DB_CONNECTION clone)
        {
            this.ID = clone.ID;
            this.ID_CONNECTION = clone.ID_CONNECTION;
            this.ORDER = clone.ORDER;
            this.LINK_TYPE_OUT = clone.LINK_TYPE_OUT;
            this.LINK_TYPE_IN = clone.LINK_TYPE_IN;
            this.RETRY_ATTEMPT = clone.RETRY_ATTEMPT;
            this.TIME_ATTEMPT = clone.TIME_ATTEMPT;
            this.NAME = clone.NAME;
            CloneUser(clone);
        }

        public DB_CONNECTION()
        {
        }
    }
}