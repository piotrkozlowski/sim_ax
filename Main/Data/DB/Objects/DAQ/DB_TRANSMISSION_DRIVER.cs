using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
	public partial class DB_TRANSMISSION_DRIVER
    {
				public int ID_TRANSMISSION_DRIVER;
				public int ID_TRANSMISSION_DRIVER_TYPE;
				public string NAME;
				public string RUN_AT_HOST;
				public string RESPONSE_ADDRESS;
				public string PLUGIN;
				public bool IS_ACTIVE;
				public bool USE_CACHE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_TRANSMISSION_DRIVER clone);

		public DB_TRANSMISSION_DRIVER(DB_TRANSMISSION_DRIVER clone)
		{
						this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
						this.ID_TRANSMISSION_DRIVER_TYPE = clone.ID_TRANSMISSION_DRIVER_TYPE;
						this.NAME = clone.NAME;
						this.RUN_AT_HOST = clone.RUN_AT_HOST;
						this.RESPONSE_ADDRESS = clone.RESPONSE_ADDRESS;
						this.PLUGIN = clone.PLUGIN;
						this.IS_ACTIVE = clone.IS_ACTIVE;
						this.USE_CACHE = clone.USE_CACHE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_TRANSMISSION_DRIVER()
		{
		}
	}
}