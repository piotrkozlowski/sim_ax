using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
    public partial class DB_DATA_ARCH_TRASH_STATUS
    {
        public int ID_DATA_ARCH_TRASH_STATUS;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_DATA_ARCH_TRASH_STATUS clone);

        public DB_DATA_ARCH_TRASH_STATUS(DB_DATA_ARCH_TRASH_STATUS clone)
        {
            this.ID_DATA_ARCH_TRASH_STATUS = clone.ID_DATA_ARCH_TRASH_STATUS;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_DATA_ARCH_TRASH_STATUS()
        {
        }
    }
}