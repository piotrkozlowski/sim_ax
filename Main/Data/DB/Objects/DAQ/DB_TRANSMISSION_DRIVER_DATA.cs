using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_TRANSMISSION_DRIVER_DATA
    {
        public long ID_TRANSMISSION_DRIVER_DATA;
        public int ID_TRANSMISSION_DRIVER;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public object VALUE;
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector

        partial void CloneUser(DB_TRANSMISSION_DRIVER_DATA clone);

        public DB_TRANSMISSION_DRIVER_DATA(DB_TRANSMISSION_DRIVER_DATA clone)
        {
            this.ID_TRANSMISSION_DRIVER_DATA = clone.ID_TRANSMISSION_DRIVER_DATA;
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
            CloneUser(clone);
        }

        public DB_TRANSMISSION_DRIVER_DATA()
        {
        }
    }
}