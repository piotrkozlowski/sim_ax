﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ROUTE_TABLE_BACKUP
    {
        public int ID;
        public int ID_ROUTE;
        public int ID_ROUTE_BACKUP;
        public int ORDER;

        partial void CloneUser(DB_ROUTE_TABLE_BACKUP clone);

        public DB_ROUTE_TABLE_BACKUP(DB_ROUTE_TABLE_BACKUP clone)
        {
            this.ID = clone.ID;
            this.ID_ROUTE = clone.ID_ROUTE;
            this.ID_ROUTE_BACKUP = clone.ID_ROUTE_BACKUP;
            this.ORDER = clone.ORDER;
            CloneUser(clone);
        }

        public DB_ROUTE_TABLE_BACKUP()
        {
        }
    }
}