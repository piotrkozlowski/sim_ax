using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
    public partial class DB_PACKET
    {
        public long ID_PACKET;
        public long SERIAL_NBR;
        public string ADDRESS;
        public int ID_TRANSMISSION_DRIVER;
        public bool IS_INCOMING;
        public int ID_TRANSMISSION_TYPE;
        public int TRANSMITTED_PACKETS;
        public int BYTES;
        public int TRANSMITTED_BYTES;
        public byte[] BODY;
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector

        partial void CloneUser(DB_PACKET clone);

        public DB_PACKET(DB_PACKET clone)
        {
            this.ID_PACKET = clone.ID_PACKET;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ADDRESS = clone.ADDRESS;
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.IS_INCOMING = clone.IS_INCOMING;
            this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
            this.TRANSMITTED_PACKETS = clone.TRANSMITTED_PACKETS;
            this.BYTES = clone.BYTES;
            this.TRANSMITTED_BYTES = clone.TRANSMITTED_BYTES;
            this.BODY = clone.BODY;
            this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
            CloneUser(clone);
        }

        public DB_PACKET()
        {
        }
    }
}