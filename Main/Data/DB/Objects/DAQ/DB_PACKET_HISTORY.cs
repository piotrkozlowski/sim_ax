using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DAQ
{
    [Serializable]
	public partial class DB_PACKET_HISTORY
    {
				public long ID_PACKET_HISTORY;
				public long ID_PACKET;
				public DateTime TIME;
				public int ID_PACKET_STATUS;
				public DateTime? SYSTEM_TIME;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_PACKET_HISTORY clone);

		public DB_PACKET_HISTORY(DB_PACKET_HISTORY clone)
		{
						this.ID_PACKET_HISTORY = clone.ID_PACKET_HISTORY;
						this.ID_PACKET = clone.ID_PACKET;
						this.TIME = clone.TIME;
						this.ID_PACKET_STATUS = clone.ID_PACKET_STATUS;
						this.SYSTEM_TIME = clone.SYSTEM_TIME;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_PACKET_HISTORY()
		{
		}
	}
}