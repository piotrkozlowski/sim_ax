using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_ROUTE_TABLE
    {
				public int ID_ROUTE;
				public string ADDRESS;
				public int PREFIX;
				public string HOP;
				public int COST_OUT;
				public int COST_IN;
				public string NAME;
		    
		partial void CloneUser(DB_ROUTE_TABLE clone);

		public DB_ROUTE_TABLE(DB_ROUTE_TABLE clone)
		{
						this.ID_ROUTE = clone.ID_ROUTE;
						this.ADDRESS = clone.ADDRESS;
						this.PREFIX = clone.PREFIX;
						this.HOP = clone.HOP;
						this.COST_OUT = clone.COST_OUT;
						this.COST_IN = clone.COST_IN;
						this.NAME = clone.NAME;
						CloneUser(clone);
		}

		public DB_ROUTE_TABLE()
		{
		}
	}
}