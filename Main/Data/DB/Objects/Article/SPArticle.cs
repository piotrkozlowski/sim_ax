﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMR.Suite.Data.DB
{
    public class SPArticle
    {
        public int Twr_GIDNumer { get; set; }
        public string KodTowaru { get; set; }
        public string NazwaTowaru { get; set; }
    }
}