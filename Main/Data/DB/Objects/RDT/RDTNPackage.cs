﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNPackage
    {
        public int ID_PACKAGE { get; set; }
        public string SERIAL_NBR { get; set; }
        public int BOX { get; set; }
        public int SET { get; set; }
        public int MAX_DEVICES_IN_PACKAGE { get; set; }
        public bool FINISHED { get; set; }
        public int? IN_PRODUCTION_LINE_TYPE { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public int? ID_CONTRACT { get; set; }
        public int? ID_PACKAGE_TYPE { get; set; }
        public DateTime? VERIFICATION_DATE { get; set; }
        public int? ID_EMPLOYEE_VERIFICATOR { get; set; }
        public string VERIFICATION_NOTE { get; set; }
        public bool? VERIFICATION_OK { get; set; }
    }
}
