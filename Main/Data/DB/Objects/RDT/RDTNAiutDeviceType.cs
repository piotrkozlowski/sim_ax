﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNAiutDeviceType
    {
        public int IdDeviceType { get; set; }
        public string DeviceTypeName { get; set; }
        public bool IsConfigurable { get; set; }
        public bool IsChecked { get; set; }
    }
}
