﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNGsmOperator
    {
        public int ID_GSM_OPERATOR;
        public string NAME;
        public string SMS_CENTER_NUMBER;
        public int SIM_SERIAL_LENGTH;
        public int PHONE_NUMBER_LENGTH;
        public string OPERATOR_CODE;
    }
}
