﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceChange
    {
        public long SERIAL_NBR;
        public string FACTORY_NUMBER;
        public int ID_DEVICE_TYPE;
        public string NAME;
        public string FIRST_QUARTER;
        public string SECOND_QUARTER;
        public string THIRD_QUARTER;
    }
}
