﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceTypeCertyficate
    {
        public int ID_RADIO_DEVICE_TYPE_CERTYFICATE { get; set; }
        public int ID_RADIO_DEVICE_TYPE { get; set; }
        public string DEVICE_TYPE_NAME { get; set; }
        public string DOCUMENT_NAME { get; set; }
        public string DOCUMENT_LINK { get; set; }
        public string SYMBOL { get; set; }
        public bool? REQUIRED { get; set; }
        public int LANGUAGE { get; set; }
        public bool? ATEX { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}",ID_RADIO_DEVICE_TYPE, DOCUMENT_NAME);
        }
    }
}
