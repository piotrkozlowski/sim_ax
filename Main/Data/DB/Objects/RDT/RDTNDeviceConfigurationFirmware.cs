﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceConfigurationFirmware
    {
        public long SERIAL_NBR;
        public int ID_CONFIGURATION;
        public string CONFIGURATION_DESCR;
        public bool? CONFIGURATION_ALLOWED_TO_PRODUCTION;
        public bool CONFIGURATION_VERIFICATED;
        public string CONFIGURATION_VERIFICATED_BY;
        public int? CONFIGURATION_ID_DISTRIBUTOR;
        public int? ID_FIRMWARE;
        public int? ID_FIRMWARE_TYPE;
        public string FIRMWARE_INFORMATION;
        public string PROCESSOR_TYPE;
        public string FIRMWARE_VERSION;
        public string HARDWARE_VERSION;

        public RDTNDeviceConfigurationFirmware()
        {
        }
    }
}
