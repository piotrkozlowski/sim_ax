﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    #region SimaServerDevice

    public struct SimaServerDevice
    {
        public string DeviceSerialNbr;
        public uint RadioAddress;
        public string DeviceName;
        public uint IdAiutDBDeviceType;
        public uint IdSoftwareConfiguration;
        public DateTime WarrantyDate;

        public string EncryptedServicePin1;
        public string EncryptedServicePin2;
        public string EncryptedServicePin3;
        public string EncryptedServicePin4;

        public string EncryptedValvePin1;
        public string EncryptedValvePin2;
        public string EncryptedValvePin3;
        public string EncryptedValvePin4;
        public string EncryptedValvePin5;
        public string EncryptedValvePin6;
        public string EncryptedValvePin7;
        public string EncryptedValvePin8;
        public string EncryptedValvePin9;

        public byte[] CryptographyKey;
        public byte[] CryptographyMacKey;
        public byte[] CryptographyKey2;
        public byte[] CryptographyMacKey2;
        public byte[] CryptographyKey3;
        public byte[] CryptographyMacKey3;

        public string MeterSerialNbr;
        public string MeterType;

        public string PackageSerialNbr;
        public int PackageId;
        public string GsmOperatorCode;
        public string DeviceDriverFileName;


        public string impulseWeight;
        public string unit;

        //Odczytanie danych z tagu <GasmeterInstalation>
        public string employeeName;
        public string montageDate;

        public string meterVolume1;
        public string deviceVolume1;
        public string impulseCount1;
        public string meterVolume2;
        public string deviceVolume2;
        public string impulseCount2;

        public string patternSerialNbr;


        public int IdMeterType;
        public int? ITNumber;

        public long? deviceIvOut;
        public long? deviceIvOutChange;
        /// <summary>
        /// 0 po instalacji, 1 przed instalacja
        /// </summary>
        public uint Status;
    }

    #endregion
    #region DeviceInformation

    public struct DeviceInformation
    {
        public long AiutDBDeviceTypeID;
        public string DeviceSerialNbr;
        public string SimCardSerialNbr;

        public string CodeName;
        public string FirstQuarter;
        public string SecondQuarter;
        public string ThirdQuarter;

        public int? RadioFrequency0;
        public int? RadioFrequency1;
        public int? RadioFrequency2;
        public int? RadioFrequency3;
        public int? CounterQueryFrequency;

        public int? OlanMainAddress;
        public byte[] OlanMainCryptKey;
        public byte[] OlanMainMacKey;

        public int? OlanInterface1Address;
        public byte[] OlanInterface1CryptKey;
        public byte[] OlanInterface1MacKey;

        public int? OlanInterface2Address;
        public byte[] OlanInterface2CryptKey;
        public byte[] OlanInterface2MacKey;

        public int? OlanInterface3Address;

        public string ValveOpenCode1;
        public string ValveOpenCode2;
        public string ValveOpenCode3;
        public string ValveOpenCode4;
        public string ValveOpenCode5;
        public string ValveOpenCode6;
        public string ValveOpenCode7;
        public string ValveOpenCode8;
        public string ValveOpenCode9;

        public string ExtendedMenuCode1;
        public string ExtendedMenuCode2;
        public string ExtendedMenuCode3;
        public string ExtendedMenuCode4;

        public string SimPhone;
        public string SimPin;
        public string SimPuk;
        public int? SimSegregator;
        public int? SimPage;
        public int? SimSection;
        public bool? SimIsWorking;
        public DateTime? SimTimeCreated;
        public int? SimIdGsmOperator;
        
        public string GsmOperatorCode;
        public uint RadioAddress;
        public string SimIp;

        public byte[] CryptKey;
    }

    #endregion
    #region DeviceDetails

    public struct DeviceDetails
    {
        public string Phone;
    }

    #endregion
}
