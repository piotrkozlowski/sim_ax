﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceOrderNumber
    {
        public long SerialNbr { get; set; }
        public string FactoryNumber { get; set; }
        public string Name { get; set; }
        public string FirstQuarter { get; set; }
        public string SecondQuarter { get; set; }
        public string ThirdQuarter { get; set; }

        public string DeviceOrderNumber
        {
            get
            {
                return Name + " " + FirstQuarter + "-" + SecondQuarter + "-" + ThirdQuarter;
            }
        }
    }
}


