﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDistributor
    {
        public int ID_DISTRIBUTOR { get; set; }
        public string NAME { get; set; }

        public override string ToString()
        {
            return this.NAME;
        }
    }
}
