﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNSimCard
    {
        public int IdSimCard { get; set; }
        public string SerialNbr { get; set; }
        public string Phone { get; set; }
        public string Pin { get; set; }
        public string Puk { get; set; }
        public string IpNumber { get; set; }
        public string ApnLogin { get; set; }
        public string ApnPassword { get; set; }
        public bool IsWorking { get; set; }
        public int IdDistributor { get; set; }
        public int IdOwner { get; set; }
        public DateTime? TimeCreated { get; set; }
        public int IdGsmOperator { get; set; }
        public string OperatorCode { get; set; }
    }
}
