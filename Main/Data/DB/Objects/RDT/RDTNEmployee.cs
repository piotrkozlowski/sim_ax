﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNEmployee
    {
        public int ID_EMPLOYEE { get; set; }
        public string NAME { get; set; }
        public string LOGIN { get; set; }
        public string PASSWORD { get; set; }
        public int ID_EMPLOYEE_ROLE { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string MOBILE { get; set; }
        public string FAX { get; set; }
        public string MAIL { get; set; }
        public string DESCR { get; set; }

        public override string  ToString()
        {
 	         return this.LOGIN;
        }
    }
}
