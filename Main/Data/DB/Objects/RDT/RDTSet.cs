﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTSet
    {
        public int IdSet { get; set; }
        public int IdSetType { get; set; }
        public DateTime CreationDate { get; set; }
        public int IdEmployee { get; set; }
        public int IdSetState { get; set; }
        public int? IdImrServer { get; set; }
        public string SetSerialNbr { get; set; }
        public int? IdInstallation { get; set; }
        public int? IdArchEntry { get; set; }
        public int? IdDistributor { get; set; }
        public int? IdDepositoryLocation { get; set; }
    }
}
