﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceBatchNumber
    {
        public string SerialNbr { get; set; }
        public string BatchNumber { get; set; }
    }
}


