﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNDeviceInPackage
    {
        public int ID_PACKAGE { get; set; }
        public string PACKAGE_SERIAL_NBR { get; set; }
        public bool FINISHED { get; set; }
        public int MAX_DEVICES_IN_PACKAGE { get; set; }
        public string DEVICE_SERIAL_NBR { get; set; }
        public int ID_DEVICE { get; set; }
        public int? ORDER { get; set; }
    }
}
