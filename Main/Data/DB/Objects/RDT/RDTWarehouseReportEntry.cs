﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTWarehouseReportEntry
    {
        public string DeviceSerialNumber { get; set; }
        public string SetSerialNumber { get; set; }
        public string PackageSerialNumber { get; set; }
    }
}
