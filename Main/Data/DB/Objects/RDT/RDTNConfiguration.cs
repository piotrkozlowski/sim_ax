﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.RDT
{
    public class RDTNConfiguration
    {
        public int OpState;
        public int ID_CONFIGURATION;
        public string ORDER_NUMBER;
        public string DESCRIPTION;
        public string PATTERN_SN;
        public string PATTERN_DESCRIPTION;
        public bool IS_GOOD;
        public bool IS_VERYFICATED;
        public int IdConfiguration
        {
            get
            {
                return this.ID_CONFIGURATION;
            }
        }
        public string OrderNumber
        {
            get
            {
                return this.ORDER_NUMBER;
            }
        }
        public string Description
        {
            get
            {
                return this.DESCRIPTION;
            }
        }
        public string PatternSN
        {
            get
            {
                return this.PATTERN_SN;
            }
        }
        public string PatternDescription
        {
            get
            {
                return this.PATTERN_DESCRIPTION;
            }
        }
        public override string ToString()
        {
            return string.Format("{0}. {1} ({2})", this.ID_CONFIGURATION, this.DESCRIPTION, this.PATTERN_SN);
        }
    }
}
