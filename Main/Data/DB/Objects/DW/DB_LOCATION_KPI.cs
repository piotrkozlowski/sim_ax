﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
    public partial class DB_LOCATION_KPI
    {
        public long ID_LOCATION_KPI;
        public int ID_DISTRIBUTOR;
        public DateTime DATE;
        public double KPI;
        public int? NEW;
        public int? OPERATIONAL;
        public int? SUSPENDED;
        public int? PENDING;
        public int? KPI_RELEVANT;
        public int? KPI_IRRELEVANT;
        public int? KPI_CONFORMING;
        public int? KPI_NONCONFORMING;
        public int? SRT_CONFORMING;
        public bool IS_MAINTENANCE_ONLY;

        partial void CloneUser(DB_LOCATION_KPI clone);

        public DB_LOCATION_KPI(DB_LOCATION_KPI clone)
        {
            this.ID_LOCATION_KPI = clone.ID_LOCATION_KPI;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.DATE = clone.DATE;
            this.KPI = clone.KPI;
            this.NEW = clone.NEW;
            this.OPERATIONAL = clone.OPERATIONAL;
            this.SUSPENDED = clone.SUSPENDED;
            this.PENDING = clone.PENDING;
            this.KPI_RELEVANT = clone.KPI_RELEVANT;
            this.KPI_IRRELEVANT = clone.KPI_IRRELEVANT;
            this.KPI_CONFORMING = clone.KPI_CONFORMING;
            this.KPI_NONCONFORMING = clone.KPI_NONCONFORMING;
            this.SRT_CONFORMING = clone.SRT_CONFORMING;
            this.IS_MAINTENANCE_ONLY = clone.IS_MAINTENANCE_ONLY;
            CloneUser(clone);
        }

        public DB_LOCATION_KPI()
        {
        }
    }
}