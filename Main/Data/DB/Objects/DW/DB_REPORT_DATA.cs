using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public class DB_REPORT_DATA
    {
        [DataMember]
		public long ID_REPORT_DATA;
        [DataMember]
		public int ID_REPORT;
        [DataMember]
		public long ID_DATA_TYPE;
        [DataMember]
        public object VALUE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public int? ID_SERVER;

		public DB_REPORT_DATA(DB_REPORT_DATA clone)
		{
						this.ID_REPORT_DATA = clone.ID_REPORT_DATA;
						this.ID_REPORT = clone.ID_REPORT;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
                        this.INDEX_NBR = clone.INDEX_NBR;
                        this.ID_SERVER = clone.ID_SERVER;
					}

		public DB_REPORT_DATA()
		{
		}
	}
}