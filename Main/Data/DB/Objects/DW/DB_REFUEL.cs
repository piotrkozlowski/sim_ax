using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_REFUEL
    {
				public long ID_REFUEL;
				public long? ID_METER;
				public long? ID_LOCATION;
				public long ID_DATA_SOURCE;
				public DateTime? START_TIME;
                public DateTime? END_TIME;
		    
		partial void CloneUser(DB_REFUEL clone);

		public DB_REFUEL(DB_REFUEL clone)
		{
						this.ID_REFUEL = clone.ID_REFUEL;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_REFUEL()
		{
		}
	}
}