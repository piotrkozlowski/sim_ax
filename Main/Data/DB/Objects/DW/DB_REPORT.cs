using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public class DB_REPORT
    {
        [DataMember]
        public int ID_REPORT;
        [DataMember]
        public int ID_REPORT_TYPE;
        [DataMember]
        public object LAYOUT;
        [DataMember]
        public int? ID_SERVER;

        public DB_REPORT(DB_REPORT clone)
        {
            this.ID_REPORT = clone.ID_REPORT;
            this.ID_REPORT_TYPE = clone.ID_REPORT_TYPE;
            this.LAYOUT = clone.LAYOUT;
            this.ID_SERVER = clone.ID_SERVER;
        }

        public DB_REPORT()
        {
        }
    }
}