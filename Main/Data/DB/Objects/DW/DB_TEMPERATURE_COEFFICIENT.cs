using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_TEMPERATURE_COEFFICIENT
    {
		public long ID_TEMPERATURE_COEFFICIENT;
		public Double TEMPERATURE;
		public Double DENSITY;
		public Double VALUE;
		    
		partial void CloneUser(DB_TEMPERATURE_COEFFICIENT clone);

		public DB_TEMPERATURE_COEFFICIENT(DB_TEMPERATURE_COEFFICIENT clone)
		{
						this.ID_TEMPERATURE_COEFFICIENT = clone.ID_TEMPERATURE_COEFFICIENT;
						this.TEMPERATURE = clone.TEMPERATURE;
						this.DENSITY = clone.DENSITY;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_TEMPERATURE_COEFFICIENT()
		{
		}
	}
}