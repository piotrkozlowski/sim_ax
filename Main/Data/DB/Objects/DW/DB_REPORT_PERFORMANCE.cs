﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_REPORT_PERFORMANCE
    {
        [DataMember]
        public long ID_REPORT_PERFORMANCE;
        [DataMember]
        public int ID_REPORT;
        [DataMember]
        public int ID_AGGREGATION_TYPE;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public DateTime START_TIME;
        [DataMember]
        public DateTime END_TIME;
        [DataMember]
        public object VALUE;
        [DataMember]
        public int? ID_DATA_SOURCE_TYPE;

        partial void CloneUser(DB_REPORT_PERFORMANCE clone);

        public DB_REPORT_PERFORMANCE(DB_REPORT_PERFORMANCE clone)
        {
            this.ID_REPORT_PERFORMANCE = clone.ID_REPORT_PERFORMANCE;
            this.ID_REPORT = clone.ID_REPORT;
            this.ID_AGGREGATION_TYPE = clone.ID_AGGREGATION_TYPE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.VALUE = clone.VALUE;
            this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
            CloneUser(clone);
        }

        public DB_REPORT_PERFORMANCE()
        {
        }
    }
}