using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_MODULE_HISTORY
    {
				public int ID_MODULE_HISTORY;
				public int ID_MODULE;
				public string COMMON_NAME;
				public string VERSION;
				public DateTime START_TIME;
				public DateTime? END_TIME;
				public int ID_OPERATOR_UPGRADE;
				public int ID_OPERATOR_ACCEPT;
				public string UPGRADE_DESCRIPTION;
		    
		partial void CloneUser(DB_MODULE_HISTORY clone);

		public DB_MODULE_HISTORY(DB_MODULE_HISTORY clone)
		{
						this.ID_MODULE_HISTORY = clone.ID_MODULE_HISTORY;
						this.ID_MODULE = clone.ID_MODULE;
						this.COMMON_NAME = clone.COMMON_NAME;
						this.VERSION = clone.VERSION;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.ID_OPERATOR_UPGRADE = clone.ID_OPERATOR_UPGRADE;
						this.ID_OPERATOR_ACCEPT = clone.ID_OPERATOR_ACCEPT;
						this.UPGRADE_DESCRIPTION = clone.UPGRADE_DESCRIPTION;
						CloneUser(clone);
		}

		public DB_MODULE_HISTORY()
		{
		}
	}
}