using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_CALIBRATION_DATA
    {
        public long ID_CALIBRATION_DATA;
        public int ID_CALIBRATION_SESSION;
        public int? ID_CALIBRATION_MODULE;
        public long? ID_CALIBRATION_TEST;
        public int INDEX_NBR;
        public long ID_DATA_TYPE;
        public object VALUE;
        public DateTime START_TIME;
        public DateTime? END_TIME;
        public int STATUS;
		    
		partial void CloneUser(DB_CALIBRATION_DATA clone);

		public DB_CALIBRATION_DATA(DB_CALIBRATION_DATA clone)
		{
            this.ID_CALIBRATION_DATA = clone.ID_CALIBRATION_SESSION;
            this.ID_CALIBRATION_SESSION = clone.ID_CALIBRATION_SESSION;
            this.ID_CALIBRATION_MODULE = clone.ID_CALIBRATION_MODULE;
            this.ID_CALIBRATION_TEST = clone.ID_CALIBRATION_TEST;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.VALUE = clone.VALUE;
            this.START_TIME = new DateTime(clone.START_TIME.Ticks);
            if (clone.END_TIME.HasValue)
                this.END_TIME = new DateTime(clone.END_TIME.Value.Ticks);
            this.STATUS = clone.STATUS;
            CloneUser(clone);
		}

        public DB_CALIBRATION_DATA()
		{
		}
	}
}