using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_REPORT_DATA_TYPE
    {
				public long ID_REPORT_DATA_TYPE;
				public string SIGNATURE;
				public long ID_DATA_TYPE;
				public int ID_UNIT;
				public int? DIGITS_AFTER_COMMA;
				public int? SEQUENCE_NBR;
		    
		partial void CloneUser(DB_REPORT_DATA_TYPE clone);

		public DB_REPORT_DATA_TYPE(DB_REPORT_DATA_TYPE clone)
		{
						this.ID_REPORT_DATA_TYPE = clone.ID_REPORT_DATA_TYPE;
						this.SIGNATURE = clone.SIGNATURE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.ID_UNIT = clone.ID_UNIT;
						this.DIGITS_AFTER_COMMA = clone.DIGITS_AFTER_COMMA;
						this.SEQUENCE_NBR = clone.SEQUENCE_NBR;
						CloneUser(clone);
		}

		public DB_REPORT_DATA_TYPE()
		{
		}
	}
}