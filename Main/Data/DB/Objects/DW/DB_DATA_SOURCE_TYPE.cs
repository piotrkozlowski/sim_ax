using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
    public partial class DB_DATA_SOURCE_TYPE
    {
				public int ID_DATA_SOURCE_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_DATA_SOURCE_TYPE clone);

		public DB_DATA_SOURCE_TYPE(DB_DATA_SOURCE_TYPE clone)
		{
						this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_DATA_SOURCE_TYPE()
		{
		}
	}
}