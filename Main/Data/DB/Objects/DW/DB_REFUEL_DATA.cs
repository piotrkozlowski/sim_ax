using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_REFUEL_DATA
    {
				public long ID_REFUEL_DATA;
				public long ID_REFUEL;
				public long ID_DATA_TYPE;
				public int INDEX_NBR;
				public object VALUE;
				public DateTime? START_TIME;
				public DateTime? END_TIME;
				public int? STATUS;
		    
		partial void CloneUser(DB_REFUEL_DATA clone);

		public DB_REFUEL_DATA(DB_REFUEL_DATA clone)
		{
						this.ID_REFUEL_DATA = clone.ID_REFUEL_DATA;
						this.ID_REFUEL = clone.ID_REFUEL;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						this.STATUS = clone.STATUS;
						CloneUser(clone);
		}

		public DB_REFUEL_DATA()
		{
		}
	}
}