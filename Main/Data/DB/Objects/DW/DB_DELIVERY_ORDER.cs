using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DELIVERY_ORDER
    {
        public int ID_DELIVERY_ORDER;
        public DateTime CREATION_DATE;
        public int ID_DELIVERY_BATCH;
        public long ID_LOCATION;
        public long? ID_METER;
        public int ID_OPERATOR;
        public int ID_DELIVERY_ADVICE;
        public int? DELIVERY_VOLUME;
        public bool? IS_SUCCESFULL;
        public string ORDER_NO;

        partial void CloneUser(DB_DELIVERY_ORDER clone);

        public DB_DELIVERY_ORDER(DB_DELIVERY_ORDER clone)
        {
            this.ID_DELIVERY_ORDER = clone.ID_DELIVERY_ORDER;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.ID_DELIVERY_BATCH = clone.ID_DELIVERY_BATCH;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.ID_METER = clone.ID_METER;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.ID_DELIVERY_ADVICE = clone.ID_DELIVERY_ADVICE;
            this.DELIVERY_VOLUME = clone.DELIVERY_VOLUME;
            this.IS_SUCCESFULL = clone.IS_SUCCESFULL;
            this.ORDER_NO = clone.ORDER_NO;
            CloneUser(clone);
        }

        public DB_DELIVERY_ORDER()
        {
        }
    }
}