using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_ETL_PERFORMANCE
    {
        public long ID_ETL_PERFORMANCE;
        public int ID_ETL;
        public int ID_AGGREGATION_TYPE;
        public long ID_DATA_TYPE;
        public int INDEX_NBR;
        public DateTime START_TIME;
        public DateTime END_TIME;
        public object VALUE;
        public int? ID_DATA_SOURCE_TYPE;
        public string CODE;

        partial void CloneUser(DB_ETL_PERFORMANCE clone);

        public DB_ETL_PERFORMANCE(DB_ETL_PERFORMANCE clone)
        {
            this.ID_ETL_PERFORMANCE = clone.ID_ETL_PERFORMANCE;
            this.ID_ETL = clone.ID_ETL;
            this.ID_AGGREGATION_TYPE = clone.ID_AGGREGATION_TYPE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.VALUE = clone.VALUE;
            this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
            this.CODE = clone.CODE;
            CloneUser(clone);
        }

        public DB_ETL_PERFORMANCE()
        {
        }
    }
}