using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable, DataContract]
	public partial class DB_ALARM
    {
        [DataMember]
        public long ID_ALARM;
        [DataMember]
        public long ID_ALARM_EVENT;
        [DataMember]
        public int? ID_ALARM_DEF;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public int ID_ALARM_TYPE;
        [DataMember]
        public long ID_DATA_TYPE_ALARM;
        [DataMember]
        public object SYSTEM_ALARM_VALUE;
        [DataMember]
        public object ALARM_VALUE;
        [DataMember]
        public int ID_OPERATOR;
        [DataMember]
        public int ID_ALARM_STATUS;
        [DataMember]
        public string TRANSMISSION_TYPE;
        [DataMember]
        public int? ID_TRANSMISSION_TYPE;
        [DataMember]
        public int? ID_ALARM_GROUP;
		    
		partial void CloneUser(DB_ALARM clone);

		public DB_ALARM(DB_ALARM clone)
		{
						this.ID_ALARM = clone.ID_ALARM;
						this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
						this.ID_ALARM_DEF = clone.ID_ALARM_DEF;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_ALARM_TYPE = clone.ID_ALARM_TYPE;
						this.ID_DATA_TYPE_ALARM = clone.ID_DATA_TYPE_ALARM;
						this.SYSTEM_ALARM_VALUE = clone.SYSTEM_ALARM_VALUE;
						this.ALARM_VALUE = clone.ALARM_VALUE;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_ALARM_STATUS = clone.ID_ALARM_STATUS;
						this.TRANSMISSION_TYPE = clone.TRANSMISSION_TYPE;
						this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
						this.ID_ALARM_GROUP = clone.ID_ALARM_GROUP;
						CloneUser(clone);
		}

		public DB_ALARM()
		{
		}
	}
}