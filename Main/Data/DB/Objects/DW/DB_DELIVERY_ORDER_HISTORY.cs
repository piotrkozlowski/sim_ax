using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DELIVERY_ORDER_HISTORY
    {
				public long ID_DELIVERY_ORDER_HISTORY;
				public int ID_DELIVERY_ORDER;
				public int ID_OPERATOR;
				public bool IS_SUCCESFULL;
				public DateTime COMMIT_DATE;
				public string COMMIT_RESULT;
				public string NOTES;
		    
		partial void CloneUser(DB_DELIVERY_ORDER_HISTORY clone);

		public DB_DELIVERY_ORDER_HISTORY(DB_DELIVERY_ORDER_HISTORY clone)
		{
						this.ID_DELIVERY_ORDER_HISTORY = clone.ID_DELIVERY_ORDER_HISTORY;
						this.ID_DELIVERY_ORDER = clone.ID_DELIVERY_ORDER;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.IS_SUCCESFULL = clone.IS_SUCCESFULL;
						this.COMMIT_DATE = clone.COMMIT_DATE;
						this.COMMIT_RESULT = clone.COMMIT_RESULT;
						this.NOTES = clone.NOTES;
						CloneUser(clone);
		}

		public DB_DELIVERY_ORDER_HISTORY()
		{
		}
	}
}