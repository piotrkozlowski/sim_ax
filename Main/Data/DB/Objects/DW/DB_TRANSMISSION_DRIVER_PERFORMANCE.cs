﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_TRANSMISSION_DRIVER_PERFORMANCE
    {
        [DataMember]
        public long ID_TRANSMISSION_DRIVER_PERFORMANCE;
        [DataMember]
        public int ID_TRANSMISSION_DRIVER;
        [DataMember]
        public int ID_AGGREGATION_TYPE;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public DateTime START_TIME;
        [DataMember]
        public DateTime END_TIME;
        [DataMember]
        public object VALUE;
        [DataMember]
        public int? ID_DATA_SOURCE_TYPE;

        partial void CloneUser(DB_TRANSMISSION_DRIVER_PERFORMANCE clone);

        public DB_TRANSMISSION_DRIVER_PERFORMANCE(DB_TRANSMISSION_DRIVER_PERFORMANCE clone)
        {
            this.ID_TRANSMISSION_DRIVER_PERFORMANCE = clone.ID_TRANSMISSION_DRIVER_PERFORMANCE;
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.ID_AGGREGATION_TYPE = clone.ID_AGGREGATION_TYPE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.VALUE = clone.VALUE;
            this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
            CloneUser(clone);
        }

        public DB_TRANSMISSION_DRIVER_PERFORMANCE()
        {
        }
    }
}