using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_MONITORING_WATCHER_DATA
    {
				public int ID_MONITORING_WATCHER_DATA;
				public int ID_MONITORING_WATCHER;
				public long ID_DATA_TYPE;
				public int INDEX;
				public object VALUE;
		    
		partial void CloneUser(DB_MONITORING_WATCHER_DATA clone);

		public DB_MONITORING_WATCHER_DATA(DB_MONITORING_WATCHER_DATA clone)
		{
						this.ID_MONITORING_WATCHER_DATA = clone.ID_MONITORING_WATCHER_DATA;
						this.ID_MONITORING_WATCHER = clone.ID_MONITORING_WATCHER;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX = clone.INDEX;
						this.VALUE = clone.VALUE;
						CloneUser(clone);
		}

		public DB_MONITORING_WATCHER_DATA()
		{
		}
	}
}