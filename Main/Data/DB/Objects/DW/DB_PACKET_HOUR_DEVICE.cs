﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.Objects.DW
{
    [Serializable]
    public partial class DB_PACKET_HOUR_DEVICE
    {
        public long SERIAL_NBR;
        public int? ID_TRANSMISSION_DRIVER;
        public DateTime DATE;
        public bool IS_INCOMING;
        public int PACKETS;

        partial void CloneUser(DB_PACKET_HOUR_DEVICE clone);

        public DB_PACKET_HOUR_DEVICE(DB_PACKET_HOUR_DEVICE clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.DATE = clone.DATE;
            this.IS_INCOMING = clone.IS_INCOMING;
            this.PACKETS = clone.PACKETS;
            CloneUser(clone);
        }

        public DB_PACKET_HOUR_DEVICE()
        {
        }
    }
}