using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public class DB_REPORT_TYPE
    {
				public int ID_REPORT_TYPE;
				public string NAME;
				public long? ID_DESCR;
		    

		public DB_REPORT_TYPE(DB_REPORT_TYPE clone)
		{
						this.ID_REPORT_TYPE = clone.ID_REPORT_TYPE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
					}

		public DB_REPORT_TYPE()
		{
		}
	}
}