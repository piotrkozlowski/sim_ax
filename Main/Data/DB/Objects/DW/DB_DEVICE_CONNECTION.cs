using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable, DataContract]
    public partial class DB_DEVICE_CONNECTION
    {
        [DataMember]
        public long ID_DEVICE_CONNECTION;
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public long SERIAL_NBR_PARENT;
        [DataMember]
        public DateTime TIME;
		    
		partial void CloneUser(DB_DEVICE_CONNECTION clone);

		public DB_DEVICE_CONNECTION(DB_DEVICE_CONNECTION clone)
		{
						this.ID_DEVICE_CONNECTION = clone.ID_DEVICE_CONNECTION;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.SERIAL_NBR_PARENT = clone.SERIAL_NBR_PARENT;
						this.TIME = clone.TIME;
						CloneUser(clone);
		}

		public DB_DEVICE_CONNECTION()
		{
		}
	}
}