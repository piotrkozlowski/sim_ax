using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_LOCATION_DISTANCE
    {
				public long ID_LOCATION_ORIGIN;
				public long ID_LOCATION_DEST;
				public Double? DISTANCE_STRAIGHT;
				public Double? DISTANCE_FORMULA;
				public Double? DISTANCE_GOOGLE;
				public DateTime? LAST_GOOGLE_UPDATE;
		    
		partial void CloneUser(DB_LOCATION_DISTANCE clone);

		public DB_LOCATION_DISTANCE(DB_LOCATION_DISTANCE clone)
		{
						this.ID_LOCATION_ORIGIN = clone.ID_LOCATION_ORIGIN;
						this.ID_LOCATION_DEST = clone.ID_LOCATION_DEST;
						this.DISTANCE_STRAIGHT = clone.DISTANCE_STRAIGHT;
						this.DISTANCE_FORMULA = clone.DISTANCE_FORMULA;
						this.DISTANCE_GOOGLE = clone.DISTANCE_GOOGLE;
						this.LAST_GOOGLE_UPDATE = clone.LAST_GOOGLE_UPDATE;
						CloneUser(clone);
		}

		public DB_LOCATION_DISTANCE()
		{
		}
	}
}