using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable, DataContract]
    public partial class DB_ALARM_EVENT
    {
        [DataMember]
        public long ID_ALARM_EVENT;
        [DataMember]
        public long ID_ALARM_DEF;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public int ID_ALARM_TYPE;
        [DataMember]
        public int ID_DATA_TYPE_ALARM;
        [DataMember]
        public object SYSTEM_ALARM_VALUE;
        [DataMember]
        public object ALARM_VALUE;
        [DataMember]
        public DateTime TIME;
        [DataMember]
        public long? ID_ISSUE;
        [DataMember]
        public bool IS_CONFIRMED;
        [DataMember]
        public int? CONFIRMED_BY;
        [DataMember]
        public DateTime? CONFIRM_TIME;

        partial void CloneUser(DB_ALARM_EVENT clone);

        public DB_ALARM_EVENT(DB_ALARM_EVENT clone)
        {
            this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
            this.ID_ALARM_DEF = clone.ID_ALARM_DEF;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_METER = clone.ID_METER;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.ID_ALARM_TYPE = clone.ID_ALARM_TYPE;
            this.ID_DATA_TYPE_ALARM = clone.ID_DATA_TYPE_ALARM;
            this.SYSTEM_ALARM_VALUE = clone.SYSTEM_ALARM_VALUE;
            this.ALARM_VALUE = clone.ALARM_VALUE;
            this.TIME = clone.TIME;
            this.ID_ISSUE = clone.ID_ISSUE;
            this.IS_CONFIRMED = clone.IS_CONFIRMED;
            this.CONFIRMED_BY = clone.CONFIRMED_BY;
            this.CONFIRM_TIME = clone.CONFIRM_TIME;
            CloneUser(clone);
        }

        public DB_ALARM_EVENT()
        {
        }
    }
}