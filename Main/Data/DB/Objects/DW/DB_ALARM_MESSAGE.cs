using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_ALARM_MESSAGE
    {
				public long ID_ALARM_EVENT;
				public int ID_LANGUAGE;
				public string ALARM_MESSAGE;
		    
		partial void CloneUser(DB_ALARM_MESSAGE clone);

		public DB_ALARM_MESSAGE(DB_ALARM_MESSAGE clone)
		{
						this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
						this.ID_LANGUAGE = clone.ID_LANGUAGE;
						this.ALARM_MESSAGE = clone.ALARM_MESSAGE;
						CloneUser(clone);
		}

		public DB_ALARM_MESSAGE()
		{
		}
	}
}