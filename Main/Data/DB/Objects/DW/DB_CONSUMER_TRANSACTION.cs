using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_CONSUMER_TRANSACTION
    {
				public int ID_CONSUMER_TRANSACTION;
				public int ID_CONSUMER;
				public int? ID_OPERATOR;
				public int ID_DISTRIBUTOR;
				public int ID_TRANSACTION_TYPE;
				public int ID_TARIFF;
				public DateTime TIMESTAMP;
				public Double CHARGE_PREPAID;
				public Double CHARGE_EC;
				public Double CHARGE_OWED;
				public Double BALANCE_PREPAID;
				public Double BALANCE_EC;
				public Double BALANCE_OWED;
				public Double? METER_INDEX;
                public long? ID_CONSUMER_SETTLEMENT;
                public int? ID_MODULE;
                public string TRANSACTION_GUID;
		    
		partial void CloneUser(DB_CONSUMER_TRANSACTION clone);

		public DB_CONSUMER_TRANSACTION(DB_CONSUMER_TRANSACTION clone)
		{
						this.ID_CONSUMER_TRANSACTION = clone.ID_CONSUMER_TRANSACTION;
						this.ID_CONSUMER = clone.ID_CONSUMER;
						this.ID_OPERATOR = clone.ID_OPERATOR;
						this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
						this.ID_TRANSACTION_TYPE = clone.ID_TRANSACTION_TYPE;
						this.ID_TARIFF = clone.ID_TARIFF;
						this.TIMESTAMP = clone.TIMESTAMP;
						this.CHARGE_PREPAID = clone.CHARGE_PREPAID;
						this.CHARGE_EC = clone.CHARGE_EC;
						this.CHARGE_OWED = clone.CHARGE_OWED;
						this.BALANCE_PREPAID = clone.BALANCE_PREPAID;
						this.BALANCE_EC = clone.BALANCE_EC;
						this.BALANCE_OWED = clone.BALANCE_OWED;
						this.METER_INDEX = clone.METER_INDEX;
                        this.ID_CONSUMER_SETTLEMENT = clone.ID_CONSUMER_SETTLEMENT;
                        this.ID_MODULE = clone.ID_MODULE;
                        this.TRANSACTION_GUID = clone.TRANSACTION_GUID;
						CloneUser(clone);
		}

		public DB_CONSUMER_TRANSACTION()
		{
		}
	}
}