﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB.Objects.DW.UserPart
{
    // Ta klasa powstała na potrzeby DBCollector (ESB) ponieważ webservice (po stronie java przedewszystkim)
    // nie radził sobie z rozróznieniem tych samych nazw klas w dwóch róznych namespace (CORE.DB_DATA i DW.DB_DATA).
    // Stworzenie takiej klasy ma rozwiązać ten problem.
    [Serializable, DataContract]
    public partial class DB_DATA_DW
    {
        [DataMember]
        public long ID_DATA;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
        public DateTime TIME;
        [DataMember]
        public long? ID_DATA_SOURCE;
        [DataMember]
        public int? ID_DATA_SOURCE_TYPE;
        [DataMember]
        public int? STATUS;

        partial void CloneUser(DB_DATA_DW clone);

        public DB_DATA_DW(DB_DATA_DW clone)
        {
            this.ID_DATA = clone.ID_DATA;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_METER = clone.ID_METER;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            this.TIME = clone.TIME;
            this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
            this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
            this.STATUS = clone.STATUS;
            CloneUser(clone);
        }

        public DB_DATA_DW()
        {
        }
    }
}
