﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable, DataContract]
    public partial class DB_DEVICE_BATTERY_STATE
    {
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public bool? IS_ACTIVE;
        [DataMember]
        public DateTime? ACTIVATION_TIME;
        [DataMember]
        public int? CURRENT_BATTERY_STATE;
        [DataMember]
        public int? CURRENT_BATTERY_STATE_PERCENT;
        [DataMember]
        public DateTime? CURRENT_BATTERY_STATE_TIME;
        [DataMember]
        public int? LAST_MONTH_BATTERY_STATE;
        [DataMember]
        public int? LAST_MONTH_BATTERY_STATE_PERCENT;
        [DataMember]
        public DateTime? LAST_MONTH_BATTERY_STATE_TIME;

        partial void CloneUser(DB_DEVICE_BATTERY_STATE clone);

        public DB_DEVICE_BATTERY_STATE(DB_DEVICE_BATTERY_STATE clone)
        {
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.IS_ACTIVE = clone.IS_ACTIVE;
            this.ACTIVATION_TIME = clone.ACTIVATION_TIME;
            this.CURRENT_BATTERY_STATE = clone.CURRENT_BATTERY_STATE;
            this.CURRENT_BATTERY_STATE_PERCENT = clone.CURRENT_BATTERY_STATE_PERCENT;
            this.CURRENT_BATTERY_STATE_TIME = clone.CURRENT_BATTERY_STATE_TIME;
            this.LAST_MONTH_BATTERY_STATE = clone.LAST_MONTH_BATTERY_STATE;
            this.LAST_MONTH_BATTERY_STATE_PERCENT = clone.LAST_MONTH_BATTERY_STATE_PERCENT;
            this.LAST_MONTH_BATTERY_STATE_TIME = clone.LAST_MONTH_BATTERY_STATE_TIME;
            CloneUser(clone);
        }

        public DB_DEVICE_BATTERY_STATE()
        {
        }
    }
}
