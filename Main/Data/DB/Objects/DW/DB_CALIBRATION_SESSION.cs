using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_CALIBRATION_SESSION
    {
        public int ID_CALIBRATION_SESSION;
        public string NAME;
        public DateTime START_TIME;
        public DateTime? END_TIME;
        public string REPORTER_NAME;
        public int STATUS;
		    
		partial void CloneUser(DB_CALIBRATION_SESSION clone);

		public DB_CALIBRATION_SESSION(DB_CALIBRATION_SESSION clone)
		{
            this.ID_CALIBRATION_SESSION = clone.ID_CALIBRATION_SESSION;
            this.NAME = String.Copy(clone.NAME);
            this.START_TIME = new DateTime(clone.START_TIME.Ticks);
            if (clone.END_TIME.HasValue)
                this.END_TIME = new DateTime(clone.END_TIME.Value.Ticks);
            this.REPORTER_NAME = String.Copy(clone.REPORTER_NAME);
            this.STATUS = clone.STATUS;
            CloneUser(clone);
		}

        public DB_CALIBRATION_SESSION()
		{
		}
	}
}