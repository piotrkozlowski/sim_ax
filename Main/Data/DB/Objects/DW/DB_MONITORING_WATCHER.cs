using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_MONITORING_WATCHER
    {
				public int ID_MONITORING_WATCHER;
				public int ID_MONITORING_WATCHER_TYPE;
				public bool IS_ACTIVE;
		    
		partial void CloneUser(DB_MONITORING_WATCHER clone);

		public DB_MONITORING_WATCHER(DB_MONITORING_WATCHER clone)
		{
						this.ID_MONITORING_WATCHER = clone.ID_MONITORING_WATCHER;
						this.ID_MONITORING_WATCHER_TYPE = clone.ID_MONITORING_WATCHER_TYPE;
						this.IS_ACTIVE = clone.IS_ACTIVE;
						CloneUser(clone);
		}

		public DB_MONITORING_WATCHER()
		{
		}
	}
}