﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_PACKET_TRASH_HOUR_ADDRESS
    {
        public string ADDRESS;
        public DateTime DATE;
        public bool IS_INCOMING;
        public int PACKETS;

        partial void CloneUser(DB_PACKET_TRASH_HOUR_ADDRESS clone);

        public DB_PACKET_TRASH_HOUR_ADDRESS(DB_PACKET_TRASH_HOUR_ADDRESS clone)
        {
            this.ADDRESS = clone.ADDRESS;
            this.DATE = clone.DATE;
            this.IS_INCOMING = clone.IS_INCOMING;
            this.PACKETS = clone.PACKETS;
            CloneUser(clone);
        }

        public DB_PACKET_TRASH_HOUR_ADDRESS()
        {
        }
    }
}
