using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_MAPPING
    {
				public long ID_MAPPING;
				public int ID_REFERENCE_TYPE;
				public long ID_SOURCE;
				public long ID_DESTINATION;
				public int ID_IMR_SERVER;
				public DateTime TIMESTAMP;
		    
		partial void CloneUser(DB_MAPPING clone);

		public DB_MAPPING(DB_MAPPING clone)
		{
						this.ID_MAPPING = clone.ID_MAPPING;
						this.ID_REFERENCE_TYPE = clone.ID_REFERENCE_TYPE;
						this.ID_SOURCE = clone.ID_SOURCE;
						this.ID_DESTINATION = clone.ID_DESTINATION;
						this.ID_IMR_SERVER = clone.ID_IMR_SERVER;
						this.TIMESTAMP = clone.TIMESTAMP;
						CloneUser(clone);
		}

		public DB_MAPPING()
		{
		}
	}
}