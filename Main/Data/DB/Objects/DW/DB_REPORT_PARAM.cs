using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public class DB_REPORT_PARAM
    {
        [DataMember]
		public int ID_REPORT_PARAM;
        [DataMember]
        public int ID_REPORT;
        [DataMember]
        public string FIELD_NAME;
        [DataMember]
        public long? ID_DESCR;
        [DataMember]
        public int ID_UNIT;
        [DataMember]
        public string NAME;
        [DataMember]
        public int? ID_REFERENCE;
        [DataMember]
        public int ID_DATA_TYPE_CLASS;
        [DataMember]
        public bool IS_INPUT_PARAM;
        [DataMember]
        public bool IS_VISIBLE;
        [DataMember]
        public object DEFAULT_VALUE;
        [DataMember]
        public string COLOR_BG;
        [DataMember]
        public string FORMAT;
		    

		public DB_REPORT_PARAM(DB_REPORT_PARAM clone)
		{
						this.ID_REPORT_PARAM = clone.ID_REPORT_PARAM;
						this.ID_REPORT = clone.ID_REPORT;
						this.FIELD_NAME = clone.FIELD_NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.ID_UNIT = clone.ID_UNIT;
						this.NAME = clone.NAME;
						this.ID_REFERENCE = clone.ID_REFERENCE;
						this.ID_DATA_TYPE_CLASS = clone.ID_DATA_TYPE_CLASS;
						this.IS_INPUT_PARAM = clone.IS_INPUT_PARAM;
						this.IS_VISIBLE = clone.IS_VISIBLE;
						this.DEFAULT_VALUE = clone.DEFAULT_VALUE;
						this.COLOR_BG = clone.COLOR_BG;
						this.FORMAT = clone.FORMAT;
					}

		public DB_REPORT_PARAM()
		{
		}
	}
}