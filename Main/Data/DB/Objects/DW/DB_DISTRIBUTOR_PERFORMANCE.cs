﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DISTRIBUTOR_PERFORMANCE
    {
        [DataMember]
        public long ID_DISTRIBUTOR_PERFORMANCE;
        [DataMember]
        public int ID_DISTRIBUTOR;
        [DataMember]
        public int ID_AGGREGATION_TYPE;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public DateTime START_TIME;
        [DataMember]
        public DateTime? END_TIME;
        [DataMember]
        public object VALUE;

        partial void CloneUser(DB_DISTRIBUTOR_PERFORMANCE clone);

        public DB_DISTRIBUTOR_PERFORMANCE(DB_DISTRIBUTOR_PERFORMANCE clone)
        {
            this.ID_DISTRIBUTOR_PERFORMANCE = clone.ID_DISTRIBUTOR_PERFORMANCE;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.ID_AGGREGATION_TYPE = clone.ID_AGGREGATION_TYPE;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.VALUE = clone.VALUE;
            CloneUser(clone);
        }

        public DB_DISTRIBUTOR_PERFORMANCE()
        {
        }
    }
}