using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_ALARM_HISTORY
    {
				public long ID_ALARM_HISTORY;
				public long ID_ALARM;
				public int ID_ALARM_STATUS;
				public DateTime START_TIME;
				public DateTime? END_TIME;
		    
		partial void CloneUser(DB_ALARM_HISTORY clone);

		public DB_ALARM_HISTORY(DB_ALARM_HISTORY clone)
		{
						this.ID_ALARM_HISTORY = clone.ID_ALARM_HISTORY;
						this.ID_ALARM = clone.ID_ALARM;
						this.ID_ALARM_STATUS = clone.ID_ALARM_STATUS;
						this.START_TIME = clone.START_TIME;
						this.END_TIME = clone.END_TIME;
						CloneUser(clone);
		}

		public DB_ALARM_HISTORY()
		{
		}
	}
}