using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB.DW
{
    [DataContract]
    public partial class DB_DATA_ARCH
    {
        [DataMember]
        public long ID_DATA_ARCH;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
        public DateTime TIME;
        [DataMember]
        public long? ID_DATA_SOURCE;
        [DataMember]
        public int? ID_DATA_SOURCE_TYPE;
        [DataMember]
        public long? ID_ALARM_EVENT;
        [DataMember]
		public long STATUS;
        [DataMember]
        public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_DATA_ARCH clone);

		public DB_DATA_ARCH(DB_DATA_ARCH clone)
		{
						this.ID_DATA_ARCH = clone.ID_DATA_ARCH;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.ID_METER = clone.ID_METER;
						this.ID_LOCATION = clone.ID_LOCATION;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.INDEX_NBR = clone.INDEX_NBR;
						this.VALUE = clone.VALUE;
						this.TIME = clone.TIME;
						this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
						this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
						this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
						this.STATUS = clone.STATUS;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_DATA_ARCH()
		{
		}
	}
}