using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DELIVERY_BATCH_STATUS
    {
				public int ID_DELIVERY_BATCH_STATUS;
				public string NAME;
				public long? ID_DESCR;
		    
		partial void CloneUser(DB_DELIVERY_BATCH_STATUS clone);

		public DB_DELIVERY_BATCH_STATUS(DB_DELIVERY_BATCH_STATUS clone)
		{
						this.ID_DELIVERY_BATCH_STATUS = clone.ID_DELIVERY_BATCH_STATUS;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						CloneUser(clone);
		}

		public DB_DELIVERY_BATCH_STATUS()
		{
		}
	}
}