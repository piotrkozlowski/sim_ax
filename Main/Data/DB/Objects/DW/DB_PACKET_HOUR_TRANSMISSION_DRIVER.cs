﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_PACKET_HOUR_TRANSMISSION_DRIVER
    {
        public int? ID_TRANSMISSION_DRIVER;
        public DateTime DATE;
        public bool IS_INCOMING;
        public int PACKETS;
        public int? ID_TRANSMISSION_TYPE;

        partial void CloneUser(DB_PACKET_HOUR_TRANSMISSION_DRIVER clone);

        public DB_PACKET_HOUR_TRANSMISSION_DRIVER(DB_PACKET_HOUR_TRANSMISSION_DRIVER clone)
        {
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.DATE = clone.DATE;
            this.IS_INCOMING = clone.IS_INCOMING;
            this.PACKETS = clone.PACKETS;
            this.ID_TRANSMISSION_TYPE = clone.ID_TRANSMISSION_TYPE;
            CloneUser(clone);
        }

        public DB_PACKET_HOUR_TRANSMISSION_DRIVER()
        {
        }
    }
}