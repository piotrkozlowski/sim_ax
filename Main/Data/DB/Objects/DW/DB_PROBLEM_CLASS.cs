﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_PROBLEM_CLASS
    {
        public int ID_PROBLEM_CLASS;
        public string NAME;
        public long? ID_DESCR;

        partial void CloneUser(DB_PROBLEM_CLASS clone);

        public DB_PROBLEM_CLASS(DB_PROBLEM_CLASS clone)
        {
            this.ID_PROBLEM_CLASS = clone.ID_PROBLEM_CLASS;
            this.NAME = clone.NAME;
            this.ID_DESCR = clone.ID_DESCR;
            CloneUser(clone);
        }

        public DB_PROBLEM_CLASS()
        {
        }
    }
}