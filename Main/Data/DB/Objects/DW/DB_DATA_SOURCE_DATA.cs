using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    public partial class DB_DATA_SOURCE_DATA
    {
				public long ID_DATA_SOURCE;
				public long ID_DATA_TYPE;
				public object VALUE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_DATA_SOURCE_DATA clone);

		public DB_DATA_SOURCE_DATA(DB_DATA_SOURCE_DATA clone)
		{
						this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
						this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
						this.VALUE = clone.VALUE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_DATA_SOURCE_DATA()
		{
		}
	}
}