using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public partial class DB_DELIVERY_BATCH
    {
        public int ID_DELIVERY_BATCH;
        public DateTime CREATION_DATE;
        public string NAME;
        public int ID_DELIVERY_BATCH_STATUS;
        public int ID_OPERATOR;
        public DateTime? COMMIT_DATE;
        public int ID_DISTRIBUTOR;
        public int SUCCESS_TOTAL;
        public int FAILED_TOTAL;

        partial void CloneUser(DB_DELIVERY_BATCH clone);

        public DB_DELIVERY_BATCH(DB_DELIVERY_BATCH clone)
        {
            this.ID_DELIVERY_BATCH = clone.ID_DELIVERY_BATCH;
            this.CREATION_DATE = clone.CREATION_DATE;
            this.NAME = clone.NAME;
            this.ID_DELIVERY_BATCH_STATUS = clone.ID_DELIVERY_BATCH_STATUS;
            this.ID_OPERATOR = clone.ID_OPERATOR;
            this.COMMIT_DATE = clone.COMMIT_DATE;
            this.ID_DISTRIBUTOR = clone.ID_DISTRIBUTOR;
            this.SUCCESS_TOTAL = clone.SUCCESS_TOTAL;
            this.FAILED_TOTAL = clone.FAILED_TOTAL;
            CloneUser(clone);
        }

        public DB_DELIVERY_BATCH()
        {
        }
    }
}