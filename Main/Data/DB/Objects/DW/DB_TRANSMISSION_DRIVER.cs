﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB.Objects.DW
{
    [Serializable, DataContract]
    public partial class DB_TRANSMISSION_DRIVER
    {
        [DataMember]
        public int ID_TRANSMISSION_DRIVER;
        [DataMember]
        public int ID_TRANSMISSION_DRIVER_TYPE;
        [DataMember]
        public string TRANSMISSION_DRIVER_TYPE_NAME;
        [DataMember]
        public string NAME;
        [DataMember]
        public string RESPONSE_ADDRESS;
        [DataMember]
        public string PLUGIN;
        [DataMember]
        public bool? IS_ACTIVE;
        [DataMember]
        public int? ID_TRANSMISSION_DRIVER_DAQ;

        partial void CloneUser(DB_TRANSMISSION_DRIVER clone);

        public DB_TRANSMISSION_DRIVER(DB_TRANSMISSION_DRIVER clone)
        {
            this.ID_TRANSMISSION_DRIVER = clone.ID_TRANSMISSION_DRIVER;
            this.ID_TRANSMISSION_DRIVER_TYPE = clone.ID_TRANSMISSION_DRIVER_TYPE;
            this.TRANSMISSION_DRIVER_TYPE_NAME = clone.TRANSMISSION_DRIVER_TYPE_NAME;
            this.NAME = clone.NAME;
            this.RESPONSE_ADDRESS = clone.RESPONSE_ADDRESS;
            this.PLUGIN = clone.PLUGIN;
            this.IS_ACTIVE = clone.IS_ACTIVE;
            this.ID_TRANSMISSION_DRIVER_DAQ = clone.ID_TRANSMISSION_DRIVER_DAQ;
            CloneUser(clone);
        }

        public DB_TRANSMISSION_DRIVER()
        {
        }
    }
}