using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DEVICE_SCHEDULE
    {
        [DataMember]
        public int ID_DEVICE_SCHEDULE;
        [DataMember]
        public long SERIAL_NBR;
        [DataMember]
        public int DAY_OF_MONTH;
        [DataMember]
        public int DAY_OF_WEEK;
        [DataMember]
        public int HOUR_OF_DAY;
        [DataMember]
        public int MINUTE_OF_HOUR;
        [DataMember]
        public int COMMAND_CODE;

        partial void CloneUser(DB_DEVICE_SCHEDULE clone);

        public DB_DEVICE_SCHEDULE(DB_DEVICE_SCHEDULE clone)
        {
            this.ID_DEVICE_SCHEDULE = clone.ID_DEVICE_SCHEDULE;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.DAY_OF_MONTH = clone.DAY_OF_MONTH;
            this.DAY_OF_WEEK = clone.DAY_OF_WEEK;
            this.HOUR_OF_DAY = clone.HOUR_OF_DAY;
            this.MINUTE_OF_HOUR = clone.MINUTE_OF_HOUR;
            this.COMMAND_CODE = clone.COMMAND_CODE;
            CloneUser(clone);
        }

        public DB_DEVICE_SCHEDULE()
        {
        }
    }
}