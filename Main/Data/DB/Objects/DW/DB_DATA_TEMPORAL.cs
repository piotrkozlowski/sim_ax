using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IMR.Suite.Data.DB
{
    [Serializable, DataContract]
    public partial class DB_DATA_TEMPORAL
    {
        [DataMember]
        public long ID_DATA_TEMPORAL;
        [DataMember]
        public long? SERIAL_NBR;
        [DataMember]
        public long? ID_METER;
        [DataMember]
        public long? ID_LOCATION;
        [DataMember]
        public long ID_DATA_TYPE;
        [DataMember]
        public int INDEX_NBR;
        [DataMember]
        public object VALUE;
        [DataMember]
        public DateTime? START_TIME;
        [DataMember]
        public DateTime? END_TIME;
        [DataMember]
        public int? ID_AGGREGATION_TYPE;
        [DataMember]
        public long? ID_DATA_SOURCE;
        [DataMember]
        public int? ID_DATA_SOURCE_TYPE;
        [DataMember]
        public long? ID_ALARM_EVENT;
        [DataMember]
        public int STATUS;

        partial void CloneUser(DB_DATA_TEMPORAL clone);

        public DB_DATA_TEMPORAL(DB_DATA_TEMPORAL clone)
        {
            this.ID_DATA_TEMPORAL = clone.ID_DATA_TEMPORAL;
            this.SERIAL_NBR = clone.SERIAL_NBR;
            this.ID_METER = clone.ID_METER;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.ID_DATA_TYPE = clone.ID_DATA_TYPE;
            this.INDEX_NBR = clone.INDEX_NBR;
            this.VALUE = clone.VALUE;
            this.START_TIME = clone.START_TIME;
            this.END_TIME = clone.END_TIME;
            this.ID_AGGREGATION_TYPE = clone.ID_AGGREGATION_TYPE;
            this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
            this.ID_DATA_SOURCE_TYPE = clone.ID_DATA_SOURCE_TYPE;
            this.ID_ALARM_EVENT = clone.ID_ALARM_EVENT;
            this.STATUS = clone.STATUS;
            CloneUser(clone);
        }

        public DB_DATA_TEMPORAL()
        {
        }
    }
}