using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_CALIBRATION_MODULE
    {
        public int ID_CALIBRATION_MODULE;
        public string NAME;
		    
		partial void CloneUser(DB_CALIBRATION_MODULE clone);

		public DB_CALIBRATION_MODULE(DB_CALIBRATION_MODULE clone)
		{
            this.ID_CALIBRATION_MODULE = clone.ID_CALIBRATION_MODULE;
            this.NAME = String.Copy(clone.NAME);
            CloneUser(clone);
		}

        public DB_CALIBRATION_MODULE()
		{
		}
	}
}