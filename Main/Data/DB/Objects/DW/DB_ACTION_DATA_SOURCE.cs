using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_ACTION_DATA_SOURCE
    {
				public long ID_ACTION;
				public long ID_DATA_SOURCE;
                public int? ID_SERVER; // sdudzik - na potrzeby DBCollector
		    
		partial void CloneUser(DB_ACTION_DATA_SOURCE clone);

		public DB_ACTION_DATA_SOURCE(DB_ACTION_DATA_SOURCE clone)
		{
						this.ID_ACTION = clone.ID_ACTION;
						this.ID_DATA_SOURCE = clone.ID_DATA_SOURCE;
                        this.ID_SERVER = clone.ID_SERVER; // sdudzik - na potrzeby DBCollector
						CloneUser(clone);
		}

		public DB_ACTION_DATA_SOURCE()
		{
		}
	}
}