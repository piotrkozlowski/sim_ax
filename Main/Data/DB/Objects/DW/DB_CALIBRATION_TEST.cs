using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB.DW
{
    [Serializable]
	public partial class DB_CALIBRATION_TEST
    {
        public long ID_CALIBRATION_TEST;
        public int? ID_CALIBRATION_MODULE;
        public int ID_CALIBRATION_SESSION;
        public int ID_METER;
        public int ID_LOCATION;
        public string NAME;
		    
		partial void CloneUser(DB_CALIBRATION_TEST clone);

		public DB_CALIBRATION_TEST(DB_CALIBRATION_TEST clone)
		{
            this.ID_CALIBRATION_TEST = clone.ID_CALIBRATION_TEST;
            this.ID_CALIBRATION_MODULE = clone.ID_CALIBRATION_MODULE;
            this.ID_CALIBRATION_SESSION = clone.ID_CALIBRATION_SESSION;
            this.ID_METER = clone.ID_METER;
            this.ID_LOCATION = clone.ID_LOCATION;
            this.NAME = String.Copy(clone.NAME);
            
            CloneUser(clone);
		}

        public DB_CALIBRATION_TEST()
		{
		}
	}
}