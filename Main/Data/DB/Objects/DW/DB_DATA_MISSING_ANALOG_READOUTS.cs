using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DATA_MISSING_ANALOG_READOUTS
    {
				public long ID_DATA_MISSING_ANALOG_READOUTS;
				public long SERIAL_NBR;
				public long? SERIAL_NBR_PARENT;
				public DateTime MISSING_DATA_START_TIME;
				public int PACKETS_COUNT;
		    
		partial void CloneUser(DB_DATA_MISSING_ANALOG_READOUTS clone);

		public DB_DATA_MISSING_ANALOG_READOUTS(DB_DATA_MISSING_ANALOG_READOUTS clone)
		{
						this.ID_DATA_MISSING_ANALOG_READOUTS = clone.ID_DATA_MISSING_ANALOG_READOUTS;
						this.SERIAL_NBR = clone.SERIAL_NBR;
						this.SERIAL_NBR_PARENT = clone.SERIAL_NBR_PARENT;
						this.MISSING_DATA_START_TIME = clone.MISSING_DATA_START_TIME;
						this.PACKETS_COUNT = clone.PACKETS_COUNT;
						CloneUser(clone);
		}

		public DB_DATA_MISSING_ANALOG_READOUTS()
		{
		}
	}
}