using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Data.DB
{
    [Serializable]
	public partial class DB_DELIVERY_ADVICE
    {
				public int ID_DELIVERY_ADVICE;
				public string NAME;
				public long? ID_DESCR;
				public int PRIORITY;
		    
		partial void CloneUser(DB_DELIVERY_ADVICE clone);

		public DB_DELIVERY_ADVICE(DB_DELIVERY_ADVICE clone)
		{
						this.ID_DELIVERY_ADVICE = clone.ID_DELIVERY_ADVICE;
						this.NAME = clone.NAME;
						this.ID_DESCR = clone.ID_DESCR;
						this.PRIORITY = clone.PRIORITY;
						CloneUser(clone);
		}

		public DB_DELIVERY_ADVICE()
		{
		}
	}
}