using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using System.Collections.ObjectModel;

namespace IMR.Suite.Data.DB
{/*
    /// <summary>
    /// Represents a cache, synchronized collection of keys and values.
    /// </summary>
    /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
    [Serializable]
#if !WindowsCE
    public class CacheDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary, ICollection, IEnumerable, ISerializable, IDeserializationCallback
#else
    public class CacheDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary, ICollection, IEnumerable
#endif
    {
#if !WindowsCE
        private ReaderWriterLock rwLock;
#endif
        private Dictionary<TKey, TValue> dictionary;
        private Dictionary<TKey, DateTime> dictionaryRelease;

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the Dictionary class that is empty, has the default initial capacity, and uses the default equality comparer for the key type.
        /// </summary>
        public CacheDictionary()
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            dictionary = new Dictionary<TKey, TValue>();

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }

        /// <summary>
        /// Initializes a new instance of the Dictionary class that contains elements copied from the specified IDictionary and uses the default equality comparer for the key type.
        /// </summary>
        /// <param name="dictionary">The IDictionary whose elements are copied to the new Dictionary.</param>
        public CacheDictionary(IDictionary<TKey, TValue> dictionary)
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            this.dictionary = new Dictionary<TKey, TValue>(dictionary);

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }

        /// <summary>
        /// Initializes a new instance of the Dictionary class that is empty, has the default initial capacity, and uses the specified IEqualityComparer.
        /// </summary>
        /// <param name="comparer">The IEqualityComparer implementation to use when comparing keys, or a null reference (Nothing in Visual Basic) to use the default EqualityComparer for the type of the key.</param>
        public CacheDictionary(IEqualityComparer<TKey> comparer)
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            dictionary = new Dictionary<TKey, TValue>(comparer);

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }

        /// <summary>
        /// Initializes a new instance of the Dictionary class that is empty, has the specified initial capacity, and uses the default equality comparer for the key type.
        /// </summary>
        /// <param name="capacity">The initial number of elements that the Dictionary can contain.</param>
        public CacheDictionary(int capacity)
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            dictionary = new Dictionary<TKey, TValue>(capacity);

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }

        /// <summary>
        /// Initializes a new instance of the Dictionary class that contains elements copied from the specified IDictionary and uses the specified IEqualityComparer.
        /// </summary>
        /// <param name="dictionary">The IDictionary whose elements are copied to the new Dictionary.</param>
        /// <param name="comparer">The IEqualityComparer implementation to use when comparing keys, or a null reference (Nothing in Visual Basic) to use the default EqualityComparer for the type of the key.</param>
        public CacheDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            this.dictionary = new Dictionary<TKey, TValue>(dictionary, comparer);

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }

        /// <summary>
        /// Initializes a new instance of the Dictionary class that is empty, has the specified initial capacity, and uses the specified IEqualityComparer.
        /// </summary>
        /// <param name="capacity">The initial number of elements that the Dictionary can contain.</param>
        /// <param name="comparer">The IEqualityComparer implementation to use when comparing keys, or a null reference (Nothing in Visual Basic) to use the default EqualityComparer for the type of the key.</param>
        public CacheDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
#if !WindowsCE
            rwLock = new ReaderWriterLock();
#endif
            dictionary = new Dictionary<TKey, TValue>(capacity, comparer);

            dictionaryRelease = new Dictionary<TKey, DateTime>();
        }
        //protected CacheDictionary(SerializationInfo info, StreamingContext context)
        //{
        //	rwLock = new ReaderWriterLock();
        //	siInfo = info;
        //}
        #endregion

        #region Destructor
        ~CacheDictionary()
        {
            StopThread();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the IEqualityComparer that is used to determine equality of keys for the dictionary.
        /// </summary>
        public IEqualityComparer<TKey> Comparer
        {
            get { return dictionary.Comparer; }
        }

        /// <summary>
        /// Gets the number of key/value pairs contained in the Dictionary.
        /// </summary>
        public int Count
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return dictionary.Count;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get or set.</param>
        /// <returns>The value associated with the specified key. If the specified key is not found, a get operation throws a KeyNotFoundException, and a set operation creates a new element with the specified key.</returns>
        public TValue this[TKey key]
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return dictionary[key];
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
            set
            {
                try
                {
                    AcquireWriterLock();
                    dictionary[key] = value;
                }
                finally
                {
                    ReleaseWriterLock();
                }
            }
        }

        /// <summary>
        /// Gets a collection containing the keys in the Dictionary.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    Dictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                    return newDictionary.Keys;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Gets a collection containing the values in the Dictionary.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    Dictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                    return newDictionary.Values;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Adds the specified key and value to the dictionary.
        /// </summary>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
        public void Add(TKey key, TValue value)
        {
            try
            {
                AcquireWriterLock();
                dictionary.Add(key, value);
            }
            //catch (Exception ex)
            //{
            //    Console.Write(ex.StackTrace);
            //}
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Adds the specified key and value to the dictionary with release time.
        /// </summary>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
        /// <param name="releaseTime">The release timespan for added element.</param>
        public void Add(TKey key, TValue value, TimeSpan releaseTime)
        {
            try
            {
                AcquireWriterLock();
                StartThread();
                dictionary.Add(key, value);
                dictionaryRelease.Add(key, DateTime.UtcNow + releaseTime);
                ItemsChanged.Set();
            }
            finally
            {
                ReleaseWriterLock();
            }
        }
        /// <summary>
        /// Sets/Updates the specified key and value to the dictionary with release time.
        /// </summary>
        /// <param name="key">The key of the element to set/update.</param>
        /// <param name="value">The value of the element to set/update. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
        /// <param name="releaseTime">The release timespan for set/updated element.</param>
        public void Set(TKey key, TValue value, TimeSpan releaseTime)
        {
            try
            {
                AcquireWriterLock();
                StartThread();
                dictionary[key] = value;
                dictionaryRelease[key] = DateTime.UtcNow + releaseTime;
                ItemsChanged.Set();
            }
            finally
            {
                ReleaseWriterLock();
            }
        }
        /// <summary>
        /// Removes all keys and values from the Dictionary.
        /// </summary>
        public void Clear()
        {
            try
            {
                AcquireWriterLock();
                dictionary.Clear();
                dictionaryRelease.Clear();
                ItemsChanged.Set();
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Determines whether the Dictionary contains the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the Dictionary.</param>
        /// <returns><b>true</b> if the Dictionary contains an element with the specified key; otherwise, <b>false</b>.</returns>
        public bool ContainsKey(TKey key)
        {
            try
            {
                AcquireReaderLock();
                return dictionary.ContainsKey(key);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Determines whether the Dictionary contains the specified value.
        /// </summary>
        /// <param name="value">The value to locate in the Dictionary.</param>
        /// <returns><b>true</b> if the Dictionary contains an element with the specified value; otherwise, <b>false</b>.</returns>
        public bool ContainsValue(TValue value)
        {
            try
            {
                AcquireReaderLock();
                return dictionary.ContainsValue(value);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An IEnumerator that can be used to iterate through the collection.</returns>
        public IEnumerator GetEnumerator()
        {
            try
            {
                AcquireReaderLock();
                Dictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                return newDictionary.GetEnumerator();
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

#if !WindowsCE
        /// <summary>
        /// Implements the System.Runtime.Serialization.ISerializable interface and returns the data needed to serialize the Dictionary instance.
        /// </summary>
        /// <param name="info">A System.Runtime.Serialization.SerializationInfo object that contains the information required to serialize the Dictionary instance.</param>
        /// <param name="context">A System.Runtime.Serialization.StreamingContext structure that contains the source and destination of the serialized stream associated with the Dictionary instance.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //if (info == null)
            //{
            //	throw new ArgumentNullException("info");
            //}
            //info.AddValue("dictionary", dictionary);
            //info.AddValue("dictionaryRelease", dictionaryRelease);

            try
            {
                AcquireReaderLock();
                ((ISerializable)dictionary).GetObjectData(info, context);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }
        
        /// <summary>
        /// Implements the System.Runtime.Serialization.ISerializable interface and raises the deserialization event when the deserialization is complete.
        /// </summary>
        /// <param name="sender">The source of the deserialization event.</param>
        public void OnDeserialization(object sender)
        {
            //if (siInfo != null)
            //{
            //	dictionary = (Dictionary<TKey, TValue>)siInfo.GetValue("dictionary", typeof(Dictionary<TKey, TValue>));
            //	dictionaryRelease = (Dictionary<TKey, DateTime>)siInfo.GetValue("dictionaryRelease", typeof(Dictionary<TKey, DateTime>));
            //}

            try
            {
                AcquireWriterLock();
                ((IDeserializationCallback)dictionary).OnDeserialization(sender);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }
#endif


        /// <summary>
        /// Removes the value with the specified key from the Dictionary.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns><b>true</b> if the element is successfully found and removed; otherwise, <b>false</b>. This method returns <b>false</b> if key is not found in the Dictionary.</returns>
        public bool Remove(TKey key)
        {
            try
            {
                AcquireWriterLock();
                if (dictionaryRelease.ContainsKey(key))
                {
                    dictionaryRelease.Remove(key);
                    ItemsChanged.Set();
                }
                return dictionary.Remove(key);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get.</param>
        /// <param name="value">When this method returns, contains the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <i>value</i> parameter. This parameter is passed uninitialized.</param>
        /// <returns><b>true</b> if the Dictionary contains an element with the specified key; otherwise, <b>false</b>.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            try
            {
                AcquireReaderLock();
                return dictionary.TryGetValue(key, out value);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }
        #endregion

        #region Explicit Interface Implementations
        /// <summary>
        /// Adds the specified value to the ICollection with the specified key.
        /// </summary>
        /// <param name="item">The KeyValuePair structure representing the key and value to add to the Dictionary.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            try
            {
                AcquireWriterLock();
                ((ICollection<KeyValuePair<TKey, TValue>>)dictionary).Add(item);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Determines whether the ICollection contains a specific key and value.
        /// </summary>
        /// <param name="item">The KeyValuePair structure to locate in the ICollection.</param>
        /// <returns><b>true</b> if keyValuePair is found in the ICollection; otherwise, <b>false</b>.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            try
            {
                AcquireReaderLock();
                return ((ICollection<KeyValuePair<TKey, TValue>>)dictionary).Contains(item);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Copies the elements of the ICollection to an array of type KeyValuePair, starting at the specified array index.
        /// </summary>
        /// <param name="array">The one-dimensional array of type KeyValuePair that is the destination of the KeyValuePair elements copied from the ICollection. The array must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            try
            {
                AcquireReaderLock();
                ((ICollection<KeyValuePair<TKey, TValue>>)dictionary).CopyTo(array, arrayIndex);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the dictionary is read-only.
        /// </summary>
        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return ((ICollection<KeyValuePair<TKey, TValue>>)dictionary).IsReadOnly;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Removes a key and value from the dictionary.
        /// </summary>
        /// <param name="item">The KeyValuePair structure representing the key and value to remove from the Dictionary.</param>
        /// <returns><b>true</b> if the key and value represented by keyValuePair is successfully found and removed; otherwise, <b>false</b>. This method returns <b>false</b> if keyValuePair is not found in the ICollection.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            try
            {
                AcquireWriterLock();
                if (dictionaryRelease.ContainsKey(item.Key))
                {
                    dictionaryRelease.Remove(item.Key);
                    ItemsChanged.Set();
                }
                return ((ICollection<KeyValuePair<TKey, TValue>>)dictionary).Remove(item);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An IEnumerator that can be used to iterate through the collection.</returns>
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            try
            {
                AcquireReaderLock();
                Dictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                return newDictionary.GetEnumerator();
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Adds the specified key and value to the dictionary.
        /// </summary>
        /// <param name="key">The object to use as the key.</param>
        /// <param name="value">The object to use as the value.</param>
        void IDictionary.Add(object key, object value)
        {
            try
            {
                AcquireWriterLock();
                ((IDictionary)dictionary).Add(key, value);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Determines whether the IDictionary contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the IDictionary.</param>
        /// <returns><b>true</b> if the IDictionary contains an element with the specified key; otherwise, <b>false</b>.</returns>
        bool IDictionary.Contains(object key)
        {
            try
            {
                AcquireReaderLock();
                return ((IDictionary)dictionary).Contains(key);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Returns an IDictionaryEnumerator for the IDictionary.
        /// </summary>
        /// <returns>An IDictionaryEnumerator for the IDictionary.</returns>
        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            try
            {
                AcquireReaderLock();
                return ((IDictionary)dictionary).GetEnumerator();
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the IDictionary has a fixed size.
        /// </summary>
        bool IDictionary.IsFixedSize
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return ((IDictionary)dictionary).IsFixedSize;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the IDictionary is read-only.
        /// </summary>
        bool IDictionary.IsReadOnly
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return ((IDictionary)dictionary).IsReadOnly;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Gets an ICollection containing the keys of the IDictionary.
        /// </summary>
        ICollection IDictionary.Keys
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    IDictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                    return ((IDictionary)newDictionary).Keys;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Removes the element with the specified key from the IDictionary.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        void IDictionary.Remove(object key)
        {
            try
            {
                AcquireWriterLock();
                if (((IDictionary)dictionaryRelease).Contains(key))
                {
                    ((IDictionary)dictionaryRelease).Remove(key);
                    ItemsChanged.Set();
                }
                ((IDictionary)dictionary).Remove(key);
            }
            finally
            {
                ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Gets an ICollection containing the values in the IDictionary.
        /// </summary>
        ICollection IDictionary.Values
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    IDictionary<TKey, TValue> newDictionary = new Dictionary<TKey, TValue>(dictionary);
                    return ((IDictionary)newDictionary).Values;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }

        /// <summary>
        /// Gets or sets the value with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get.</param>
        /// <returns>The value associated with the specified key, or a null reference (Nothing in Visual Basic) if key is not in the dictionary or key is of a type that is not assignable to the key type TKey of the Dictionary.</returns>
        object IDictionary.this[object key]
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return ((IDictionary)dictionary)[key];
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
            set
            {
                try
                {
                    AcquireWriterLock();
                    ((IDictionary)dictionary)[key] = value;
                }
                finally
                {
                    ReleaseWriterLock();
                }
            }
        }

        /// <summary>
        /// Copies the elements of the ICollection to an array, starting at the specified array index.
        /// </summary>
        /// <param name="array">The one-dimensional array that is the destination of the elements copied from ICollection. The array must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            try
            {
                AcquireReaderLock();
                ((ICollection)dictionary).CopyTo(array, index);
            }
            finally
            {
                ReleaseReaderLock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the ICollection is synchronized (thread safe).
        /// </summary>
        bool ICollection.IsSynchronized
        {
            get { return true; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the ICollection.
        /// </summary>
        object ICollection.SyncRoot
        {
            get
            {
                try
                {
                    AcquireReaderLock();
                    return ((ICollection)dictionary).SyncRoot;
                }
                finally
                {
                    ReleaseReaderLock();
                }
            }
        }
        #endregion

        #region ReleaseEvent
        public event EventHandler<ReleaseEventArgs> ReleaseEvent;
        public class ReleaseEventArgs : EventArgs
        {
            public readonly TKey Key;
            public readonly TValue Value;

            public ReleaseEventArgs(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }
        }
        #endregion
        private Thread mThread = null;
        private ManualResetEvent StopEvent = new ManualResetEvent(false);
        private AutoResetEvent ItemsChanged = new AutoResetEvent(false);

        #region internal StartThread
        internal void StartThread()
        {
#if !WindowsCE
            if (mThread == null)
            {
                mThread = new Thread(new ThreadStart(ThreadProc));
                mThread.IsBackground = true;
                mThread.Start();
            }
            else
            {
                if (!mThread.IsAlive)
                {
                    mThread.Abort();
                    mThread = new Thread(new ThreadStart(ThreadProc));
                    mThread.IsBackground = true;
                    mThread.Start();
                }
            }
#endif
        }
        #endregion
        #region internal StopThread
        internal void StopThread()
        {
#if !WindowsCE
            if (mThread != null && mThread.IsAlive)
            {
                StopEvent.Set();

                if (!mThread.Join(1000))
                    mThread.Abort();//.Interrupt();
                mThread = null;
            }
#endif
        }
        #endregion
        #region internal ThreadProc
        internal void ThreadProc()
        {
            TimeSpan waitTime = TimeSpan.FromHours(24);
            WaitHandle[] waitHandle = new WaitHandle[] { StopEvent, ItemsChanged };

            while (true)
            {
                object ReleaseKey = null;
                TValue ReleaseValue = default(TValue);
                DateTime ReleaseTime = DateTime.MaxValue;

                try
                {
                    #region Find element with shortest release time
                    AcquireWriterLock();
                    foreach (KeyValuePair<TKey, DateTime> kvp in dictionaryRelease)
                    {
                        if (kvp.Value < ReleaseTime)
                        {
                            ReleaseKey = kvp.Key;
                            ReleaseTime = kvp.Value;
                        }
                    }
                    ReleaseWriterLock();
                    #endregion
                    #region If element finded calculate time span
                    if (ReleaseKey != null)
                    {
                        waitTime = ReleaseTime - DateTime.UtcNow;
                        if (waitTime.Milliseconds < 0)
                            waitTime = TimeSpan.FromMilliseconds(5);
						if (waitTime.TotalMilliseconds > Int32.MaxValue)
							waitTime = TimeSpan.FromDays(1);
                    }
                    #endregion
                    #region Wait for calculated time or event stop or event ItemsChaned
#if !WindowsCE
                    switch (WaitHandle.WaitAny(waitHandle, waitTime, true))
                    {
                        case 0: //StopEvent
                            return;
                        case 1: //ItemsChanged, maybe somebody remove this element, find again element with shortest time
                            continue;
                        default: //Release time elapsed
                            // Check (for sure) if we have any element or time elapsed
                            if (ReleaseKey == null || DateTime.UtcNow < ReleaseTime)
                                continue;

                            break;
                    }
#else
                    if (StopEvent.WaitOne((int)waitTime.TotalMilliseconds, false))
                    {
                        //StopEvent
                        return;
                    }
                    if (ItemsChanged.WaitOne((int)waitTime.TotalMilliseconds, false))
                    {    //ItemsChanged, maybe somebody remove this element, find again element with shortest time
                        continue;
                    }
                    else
                    {
                        //Release time elapsed
                        // Check (for sure) if we have any element or time elapsed
                        if (ReleaseKey == null || DateTime.UtcNow < ReleaseTime)
                            continue;
                    }
#endif
                    #endregion
                    #region Check again (for sure) if elements were changed
                    if (ItemsChanged.WaitOne(0, true))
                        continue;
                    #endregion
                    #region Get TKey and TValue for released element and remove one from our dictionaries
                    AcquireWriterLock();
                    ReleaseValue = dictionary[(TKey)ReleaseKey];
                    dictionary.Remove((TKey)ReleaseKey);
                    dictionaryRelease.Remove((TKey)ReleaseKey);
                    ReleaseWriterLock();
                    #endregion
                    #region Invoke client delegate if subscribed
                    EventHandler<ReleaseEventArgs> handler = ReleaseEvent;
                    if (handler != null)
                    {
                        //handler.Invoke(this, null);
                        handler(this, new ReleaseEventArgs((TKey)ReleaseKey, ReleaseValue));
                    }
                    #endregion
                    AcquireWriterLock();
                    if (dictionaryRelease.Count == 0)
                        return;
                }
                catch (Exception)
                {
                }
                finally
                {
#if !WindowsCE
                    if (rwLock.IsWriterLockHeld)
                        ReleaseWriterLock();
#endif
                }
            }
        }
        #endregion

        #region internal AcquireReaderLock
        internal void AcquireReaderLock()
        {
#if !WindowsCE
			rwLock.AcquireReaderLock(Timeout.Infinite);
#endif
        }
        #endregion
        #region internal ReleaseReaderLock
        internal void ReleaseReaderLock()
        {
#if !WindowsCE
			rwLock.ReleaseReaderLock();
#endif
        }
        #endregion
        #region internal AcquireWriterLock
        internal void AcquireWriterLock()
        {
#if !WindowsCE
			rwLock.AcquireWriterLock(Timeout.Infinite);
#endif
        }
        #endregion
        #region internal ReleaseWriterLock
        internal void ReleaseWriterLock()
        {
#if !WindowsCE
			rwLock.ReleaseWriterLock();
#endif
        }
        #endregion
    }
  */ 
}
