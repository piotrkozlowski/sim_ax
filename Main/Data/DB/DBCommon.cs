﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.Data;
using System.Text;
using System.Linq;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Reflection;
using System.Security.Cryptography;
using IMR.Suite.Common.Code;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Globalization;

namespace IMR.Suite.Data.DB
{
    #region Structures/Classes
    #region Language
    //[Serializable]
    //public class Language
    //{
    //    #region Members
    //    public int IdLanguage;
    //    public string Name;
    //    public string CultureCode;
    //    public int IdCultureLang;
    //    #endregion
    //    public int IconIndex;	// TW: Czy to jest tutaj potrzebne??

    //    #region Constructor
    //    public Language()
    //    {
    //    }
    //    public Language(int id, string code, string name)
    //    {
    //        this.IdLanguage = id;
    //        this.Name = name;
    //        this.CultureCode = code;
    //    }
    //    public Language(int id, string code, string name, int iconIndex)
    //        : this(id, code, name)
    //    {
    //        this.IconIndex = iconIndex;
    //    }
    //    #endregion

    //    #region override ToString
    //    public override string ToString()
    //    {
    //        return Name;
    //    }
    //    #endregion
    //    #region override Equals
    //    public override bool Equals(object obj)
    //    {
    //        if (obj == null || obj.GetType() != typeof(Language))
    //            return false;
    //        return IdLanguage.Equals((obj as Language).IdLanguage);
    //    }
    //    #endregion
    //    #region override GetHashCode
    //    public override int GetHashCode()
    //    {
    //        return IdLanguage;
    //    }
    //    #endregion
    //}
    #endregion
    #region TypeDateTimeCode
    public class TypeDateTimeCode
    {
        public enum ModeType : int
        {
            None = -1,
            Null = 0,
            Between = 1,
            Different = 2,
            Equal = 3,
            Lesser = 4,
            LesserEqual = 5,
            GreaterEqual = 6,
            Greater = 7,
        }

        public ModeType Mode { get; set; }
        public DateTime? DateTime1 { get; set; }
        public DateTime? DateTime2 { get; set; }

        public static TypeDateTimeCode None()
        {
            return new TypeDateTimeCode() { Mode = ModeType.None };
        }
        public static TypeDateTimeCode Null()
        {
            return new TypeDateTimeCode() { Mode = ModeType.Null };
        }
        public static TypeDateTimeCode Between(DateTime startDate, DateTime endDate)
        {
            return new TypeDateTimeCode() { Mode = ModeType.Between, DateTime1 = startDate, DateTime2 = endDate };
        }
        public static TypeDateTimeCode Different(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.Different, DateTime1 = dateTime };
        }
        public static TypeDateTimeCode Equal(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.Equal, DateTime1 = dateTime };
        }
        public static TypeDateTimeCode Lesser(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.Lesser, DateTime1 = dateTime };
        }
        public static TypeDateTimeCode LesserEqual(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.LesserEqual, DateTime1 = dateTime };
        }
        public static TypeDateTimeCode GreaterEqual(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.GreaterEqual, DateTime1 = dateTime };
        }
        public static TypeDateTimeCode Greater(DateTime dateTime)
        {
            return new TypeDateTimeCode() { Mode = ModeType.Greater, DateTime1 = dateTime };
        }
    }
    #endregion
    #endregion

    #region internal ODBC32
    internal static class ODBC32
    {
        #region Fields
        internal const int COLUMN_NAME = 4;
        internal const int COLUMN_SIZE = 8;
        internal const int COLUMN_TYPE = 5;
        internal const int DATA_TYPE = 6;
        internal const int DECIMAL_DIGITS = 10;
        internal const int MAX_CONNECTION_STRING_LENGTH = 0x400;
        internal const int NUM_PREC_RADIX = 11;
        private const int SIGNED_OFFSET = -20;
        internal const short SQL_ALL_TYPES = 0;
        internal static readonly IntPtr SQL_AUTOCOMMIT_OFF = IntPtr.Zero;
        internal static readonly IntPtr SQL_AUTOCOMMIT_ON = IntPtr.Zero;
        internal const int SQL_CD_FALSE = 0;
        internal const int SQL_CD_TRUE = 1;
        internal const short SQL_COMMIT = 0;
        internal static readonly IntPtr SQL_CP_OFF = IntPtr.Zero;
        internal static readonly IntPtr SQL_CP_ONE_PER_DRIVER = IntPtr.Zero;
        internal static readonly IntPtr SQL_CP_ONE_PER_HENV = IntPtr.Zero;
        internal const int SQL_DEFAULT_PARAM = -5;
        internal const short SQL_DIAG_SQLSTATE = 4;
        internal const int SQL_DTC_DONE = 0;
        internal static readonly IntPtr SQL_HANDLE_NULL = IntPtr.Zero;
        internal const int SQL_IS_POINTER = -4;
        internal const int SQL_IS_PTR = 1;
        internal const int SQL_NO_TOTAL = -4;
        internal const int SQL_NTS = -3;
        internal const int SQL_NULL_DATA = -1;
        internal static readonly IntPtr SQL_OV_ODBC3 = IntPtr.Zero;
        internal const short SQL_RESULT_COL = 3;
        internal const short SQL_ROLLBACK = 1;
        private const int UNSIGNED_OFFSET = -22;
        #endregion
        #region Nested Types
        #region HANDLER
        internal enum HANDLER
        {
            IGNORE,
            THROW
        }
        #endregion
        #region RetCode
        internal enum RetCode : short
        {
            ERROR = -1,
            INVALID_HANDLE = -2,
            NO_DATA = 100,
            SUCCESS = 0,
            SUCCESS_WITH_INFO = 1
        }
        #endregion
        #region RETCODE
        [Serializable]
        public enum RETCODE
        {
            ERROR = -1,
            INVALID_HANDLE = -2,
            NO_DATA = 100,
            SUCCESS = 0,
            SUCCESS_WITH_INFO = 1
        }
        #endregion
        #region SQL_API
        internal enum SQL_API : ushort
        {
            SQLCOLUMNS = 40,
            SQLEXECDIRECT = 11,
            SQLGETTYPEINFO = 0x2f,
            SQLPROCEDURECOLUMNS = 0x42,
            SQLPROCEDURES = 0x43,
            SQLSTATISTICS = 0x35,
            SQLTABLES = 0x36
        }
        #endregion
        #region SQL_ATTR
        internal enum SQL_ATTR
        {
            APP_PARAM_DESC = 0x271b,
            APP_ROW_DESC = 0x271a,
            AUTOCOMMIT = 0x66,
            CONNECTION_DEAD = 0x4b9,
            CONNECTION_POOLING = 0xc9,
            CURRENT_CATALOG = 0x6d,
            IMP_PARAM_DESC = 0x271d,
            IMP_ROW_DESC = 0x271c,
            LOGIN_TIMEOUT = 0x67,
            METADATA_ID = 0x271e,
            ODBC_VERSION = 200,
            QUERY_TIMEOUT = 0,
            SQL_COPT_SS_ENLIST_IN_DTC = 0x4b7,
            TXN_ISOLATION = 0x6c
        }
        #endregion
        #region SQL_C
        internal enum SQL_C : short
        {
            ARD_TYPE = -99,
            BINARY = -2,
            BIT = -7,
            CHAR = 1,
            DEFAULT = 0x63,
            DOUBLE = 8,
            GUID = -11,
            NUMERIC = 2,
            REAL = 7,
            SBIGINT = -25,
            SLONG = -16,
            SSHORT = -15,
            TIMESTAMP = 11,
            TYPE_DATE = 0x5b,
            TYPE_TIME = 0x5c,
            TYPE_TIMESTAMP = 0x5d,
            UBIGINT = -27,
            UTINYINT = -28,
            WCHAR = -8
        }
        #endregion
        #region SQL_COLUMN
        internal enum SQL_COLUMN
        {
            COUNT,
            NAME,
            TYPE,
            LENGTH,
            PRECISION,
            SCALE,
            DISPLAY_SIZE,
            NULLABLE,
            UNSIGNED,
            MONEY,
            UPDATABLE,
            AUTO_INCREMENT,
            CASE_SENSITIVE,
            SEARCHABLE,
            TYPE_NAME,
            TABLE_NAME,
            OWNER_NAME,
            QUALIFIER_NAME,
            LABEL
        }
        #endregion
        #region SQL_CONVERT
        internal enum SQL_CONVERT : ushort
        {
            BIGINT = 0x35,
            BINARY = 0x36,
            BIT = 0x37,
            CHAR = 0x38,
            DATE = 0x39,
            DECIMAL = 0x3a,
            DOUBLE = 0x3b,
            FLOAT = 60,
            INTEGER = 0x3d,
            LONGVARBINARY = 0x47,
            LONGVARCHAR = 0x3e,
            NUMERIC = 0x3f,
            REAL = 0x40,
            SMALLINT = 0x41,
            TIME = 0x42,
            TIMESTAMP = 0x43,
            TINYINT = 0x44,
            VARBINARY = 0x45,
            VARCHAR = 70
        }
        #endregion
        #region SQL_CVT
        [Flags]
        internal enum SQL_CVT
        {
            BIGINT = 0x4000,
            BINARY = 0x400,
            BIT = 0x1000,
            CHAR = 1,
            DATE = 0x8000,
            DECIMAL = 4,
            DOUBLE = 0x80,
            FLOAT = 0x20,
            GUID = 0x1000000,
            INTEGER = 8,
            INTERVAL_DAY_TIME = 0x100000,
            INTERVAL_YEAR_MONTH = 0x80000,
            LONGVARBINARY = 0x40000,
            LONGVARCHAR = 0x200,
            NUMERIC = 2,
            REAL = 0x40,
            SMALLINT = 0x10,
            TIME = 0x10000,
            TIMESTAMP = 0x20000,
            TINYINT = 0x2000,
            VARBINARY = 0x800,
            VARCHAR = 0x100,
            WCHAR = 0x200000,
            WLONGVARCHAR = 0x400000,
            WVARCHAR = 0x800000
        }
        #endregion
        #region SQL_DESC
        internal enum SQL_DESC : short
        {
            ALLOC_TYPE = 0x44b,
            AUTO_UNIQUE_VALUE = 11,
            BASE_COLUMN_NAME = 0x16,
            BASE_TABLE_NAME = 0x17,
            CATALOG_NAME = 0x11,
            CONCISE_TYPE = 2,
            COUNT = 0x3e9,
            DATA_PTR = 0x3f2,
            DATETIME_INTERVAL_CODE = 0x3ef,
            DISPLAY_SIZE = 6,
            INDICATOR_PTR = 0x3f1,
            LENGTH = 0x3eb,
            NAME = 0x3f3,
            NULLABLE = 0x3f0,
            OCTET_LENGTH = 0x3f5,
            OCTET_LENGTH_PTR = 0x3ec,
            PRECISION = 0x3ed,
            SCALE = 0x3ee,
            SCHEMA_NAME = 0x10,
            TABLE_NAME = 15,
            TYPE = 0x3ea,
            TYPE_NAME = 14,
            UNNAMED = 0x3f4,
            UNSIGNED = 8,
            UPDATABLE = 10
        }
        #endregion
        #region SQL_HANDLE
        internal enum SQL_HANDLE : short
        {
            DBC = 2,
            DESC = 4,
            ENV = 1,
            STMT = 3
        }
        #endregion
        #region SQL_INFO
        internal enum SQL_INFO : ushort
        {
            CATALOG_NAME_SEPARATOR = 0x29,
            DATA_SOURCE_NAME = 2,
            DBMS_NAME = 0x11,
            DBMS_VER = 0x12,
            DRIVER_NAME = 6,
            DRIVER_ODBC_VER = 0x4d,
            DRIVER_VER = 7,
            GROUP_BY = 0x58,
            IDENTIFIER_CASE = 0x1c,
            IDENTIFIER_QUOTE_CHAR = 0x1d,
            KEYWORDS = 0x59,
            ODBC_VER = 10,
            ORDER_BY_COLUMNS_IN_SELECT = 90,
            QUOTED_IDENTIFIER_CASE = 0x5d,
            SEARCH_PATTERN_ESCAPE = 14,
            SERVER_NAME = 13,
            SQL_OJ_CAPABILITIES_20 = 0xfdeb,
            SQL_OJ_CAPABILITIES_30 = 0x73,
            SQL_SQL92_RELATIONAL_JOIN_OPERATORS = 0xa1
        }
        #endregion
        #region SQL_IS
        internal enum SQL_IS
        {
            INTEGER = -6,
            POINTER = -4,
            SMALLINT = -8,
            UINTEGER = -5
        }
        #endregion
        #region SQL_NULLABILITY
        internal enum SQL_NULLABILITY : ushort
        {
            NO_NULLS = 0,
            NULLABLE = 1,
            UNKNOWN = 2
        }
        #endregion
        #region SQL_PARAM
        internal enum SQL_PARAM
        {
            INPUT = 1,
            INPUT_OUTPUT = 2,
            OUTPUT = 4,
            RETURN_VALUE = 5
        }
        #endregion
        #region SQL_SCOPE
        internal enum SQL_SCOPE : ushort
        {
            CURROW = 0,
            SESSION = 2,
            TRANSACTION = 1
        }
        #endregion
        #region SQL_SPECIALCOLS
        internal enum SQL_SPECIALCOLS : ushort
        {
            BEST_ROWID = 1,
            ROWVER = 2
        }
        #endregion
        #region SQL_TRANSACTION
        internal enum SQL_TRANSACTION
        {
            READ_COMMITTED = 2,
            READ_UNCOMMITTED = 1,
            REPEATABLE_READ = 4,
            SERIALIZABLE = 8,
            SNAPSHOT = 0x10
        }
        #endregion
        #region SQL_TYPE
        internal enum SQL_TYPE : short
        {
            BIGINT = -5,
            BINARY = -2,
            BIT = -7,
            CHAR = 1,
            DECIMAL = 3,
            DOUBLE = 8,
            FLOAT = 6,
            GUID = -11,
            INTEGER = 4,
            LONGVARBINARY = -4,
            LONGVARCHAR = -1,
            NUMERIC = 2,
            REAL = 7,
            SMALLINT = 5,
            SS_TIME_EX = -154,
            SS_UDT = -151,
            SS_UTCDATETIME = -153,
            SS_VARIANT = -150,
            SS_XML = -152,
            TIMESTAMP = 11,
            TINYINT = -6,
            TYPE_DATE = 0x5b,
            TYPE_TIME = 0x5c,
            TYPE_TIMESTAMP = 0x5d,
            VARBINARY = -3,
            VARCHAR = 12,
            WCHAR = -8,
            WLONGVARCHAR = -10,
            WVARCHAR = -9
        }
        #endregion
        #region STMT
        internal enum STMT : short
        {
            CLOSE = 0,
            DROP = 1,
            RESET_PARAMS = 3,
            UNBIND = 2
        }
        #endregion
        #endregion
    }
    #endregion
    #region internal SqlTypeMap
    internal sealed class SqlTypeMap
    {
        // Fields
        internal readonly DbType _dbType;
        internal readonly OdbcType _odbcType;
        internal readonly SqlDbType _sqlDbType;
        internal readonly Type _type;
        internal readonly ODBC32.SQL_TYPE _sql_type;
        internal readonly ODBC32.SQL_C _sql_c;
        internal readonly ODBC32.SQL_C _param_sql_c;
        internal readonly int _bufferSize;
        internal readonly int _columnSize;
        internal readonly bool _signType;

        private static readonly SqlTypeMap _BigInt = new SqlTypeMap(OdbcType.BigInt, DbType.Int64, SqlDbType.BigInt, typeof(long), ODBC32.SQL_TYPE.BIGINT, ODBC32.SQL_C.SBIGINT, ODBC32.SQL_C.SBIGINT, 8, 20, true);
        private static readonly SqlTypeMap _Binary = new SqlTypeMap(OdbcType.Binary, DbType.Binary, SqlDbType.Binary, typeof(byte[]), ODBC32.SQL_TYPE.BINARY, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _Bit = new SqlTypeMap(OdbcType.Bit, DbType.Boolean, SqlDbType.Bit, typeof(bool), ODBC32.SQL_TYPE.BIT, ODBC32.SQL_C.BIT, ODBC32.SQL_C.BIT, 1, 1, false);
        private static readonly SqlTypeMap _Char = new SqlTypeMap(OdbcType.Char, DbType.AnsiStringFixedLength, SqlDbType.Char, typeof(string), ODBC32.SQL_TYPE.CHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.CHAR, -1, -1, false);
        private static readonly SqlTypeMap _Date = new SqlTypeMap(OdbcType.Date, DbType.Date, SqlDbType.Date, typeof(DateTime), ODBC32.SQL_TYPE.TYPE_DATE, ODBC32.SQL_C.TYPE_DATE, ODBC32.SQL_C.TYPE_DATE, 6, 10, false);
        private static readonly SqlTypeMap _DateTime = new SqlTypeMap(OdbcType.DateTime, DbType.DateTime, SqlDbType.DateTime, typeof(DateTime), ODBC32.SQL_TYPE.TYPE_TIMESTAMP, ODBC32.SQL_C.TYPE_TIMESTAMP, ODBC32.SQL_C.TYPE_TIMESTAMP, 0x10, 0x17, false);
        private static readonly SqlTypeMap _Decimal = new SqlTypeMap(OdbcType.Decimal, DbType.Decimal, SqlDbType.Decimal, typeof(decimal), ODBC32.SQL_TYPE.DECIMAL, ODBC32.SQL_C.NUMERIC, ODBC32.SQL_C.NUMERIC, 0x13, 0x1c, false);
        private static readonly SqlTypeMap _Double = new SqlTypeMap(OdbcType.Double, DbType.Double, SqlDbType.Real, typeof(double), ODBC32.SQL_TYPE.DOUBLE, ODBC32.SQL_C.DOUBLE, ODBC32.SQL_C.DOUBLE, 8, 15, false);
        private static readonly SqlTypeMap _Image = new SqlTypeMap(OdbcType.Image, DbType.Binary, SqlDbType.Image, typeof(byte[]), ODBC32.SQL_TYPE.LONGVARBINARY, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _Int = new SqlTypeMap(OdbcType.Int, DbType.Int32, SqlDbType.Int, typeof(int), ODBC32.SQL_TYPE.INTEGER, ODBC32.SQL_C.SLONG, ODBC32.SQL_C.SLONG, 4, 10, true);
        private static readonly SqlTypeMap _NChar = new SqlTypeMap(OdbcType.NChar, DbType.StringFixedLength, SqlDbType.NChar, typeof(string), ODBC32.SQL_TYPE.WCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.WCHAR, -1, -1, false);
        private static readonly SqlTypeMap _NText = new SqlTypeMap(OdbcType.NText, DbType.String, SqlDbType.NText, typeof(string), ODBC32.SQL_TYPE.WLONGVARCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.WCHAR, -1, -1, false);
        private static readonly SqlTypeMap _Numeric = new SqlTypeMap(OdbcType.Numeric, DbType.Decimal, SqlDbType.Decimal, typeof(decimal), ODBC32.SQL_TYPE.NUMERIC, ODBC32.SQL_C.NUMERIC, ODBC32.SQL_C.NUMERIC, 0x13, 0x1c, false);
        private static readonly SqlTypeMap _NVarChar = new SqlTypeMap(OdbcType.NVarChar, DbType.String, SqlDbType.NVarChar, typeof(string), ODBC32.SQL_TYPE.WVARCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.WCHAR, -1, -1, false);
        private static readonly SqlTypeMap _Real = new SqlTypeMap(OdbcType.Real, DbType.Single, SqlDbType.Real, typeof(float), ODBC32.SQL_TYPE.REAL, ODBC32.SQL_C.REAL, ODBC32.SQL_C.REAL, 4, 7, false);
        private static readonly SqlTypeMap _SmallDT = new SqlTypeMap(OdbcType.SmallDateTime, DbType.DateTime, SqlDbType.SmallDateTime, typeof(DateTime), ODBC32.SQL_TYPE.TYPE_TIMESTAMP, ODBC32.SQL_C.TYPE_TIMESTAMP, ODBC32.SQL_C.TYPE_TIMESTAMP, 0x10, 0x17, false);
        private static readonly SqlTypeMap _SmallInt = new SqlTypeMap(OdbcType.SmallInt, DbType.Int16, SqlDbType.SmallInt, typeof(short), ODBC32.SQL_TYPE.SMALLINT, ODBC32.SQL_C.SSHORT, ODBC32.SQL_C.SSHORT, 2, 5, true);
        private static readonly SqlTypeMap _Text = new SqlTypeMap(OdbcType.Text, DbType.AnsiString, SqlDbType.Text, typeof(string), ODBC32.SQL_TYPE.LONGVARCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.CHAR, -1, -1, false);
        private static readonly SqlTypeMap _Time = new SqlTypeMap(OdbcType.Time, DbType.Time, SqlDbType.Time, typeof(TimeSpan), ODBC32.SQL_TYPE.TYPE_TIME, ODBC32.SQL_C.TYPE_TIME, ODBC32.SQL_C.TYPE_TIME, 6, 12, false);
        private static readonly SqlTypeMap _Timestamp = new SqlTypeMap(OdbcType.Timestamp, DbType.Binary, SqlDbType.Timestamp, typeof(byte[]), ODBC32.SQL_TYPE.BINARY, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _TinyInt = new SqlTypeMap(OdbcType.TinyInt, DbType.Byte, SqlDbType.TinyInt, typeof(byte), ODBC32.SQL_TYPE.TINYINT, ODBC32.SQL_C.UTINYINT, ODBC32.SQL_C.UTINYINT, 1, 3, true);
        private static readonly SqlTypeMap _UDT = new SqlTypeMap(OdbcType.Binary, DbType.Binary, SqlDbType.Udt, typeof(object), ODBC32.SQL_TYPE.SS_UDT, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _UniqueId = new SqlTypeMap(OdbcType.UniqueIdentifier, DbType.Guid, SqlDbType.UniqueIdentifier, typeof(Guid), ODBC32.SQL_TYPE.GUID, ODBC32.SQL_C.GUID, ODBC32.SQL_C.GUID, 0x10, 0x24, false);
        private static readonly SqlTypeMap _VarBinary = new SqlTypeMap(OdbcType.VarBinary, DbType.Binary, SqlDbType.VarBinary, typeof(byte[]), ODBC32.SQL_TYPE.VARBINARY, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _VarChar = new SqlTypeMap(OdbcType.VarChar, DbType.AnsiString, SqlDbType.VarChar, typeof(string), ODBC32.SQL_TYPE.VARCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.CHAR, -1, -1, false);
        private static readonly SqlTypeMap _Variant = new SqlTypeMap(OdbcType.Binary, DbType.Binary, SqlDbType.Variant, typeof(object), ODBC32.SQL_TYPE.SS_VARIANT, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);
        private static readonly SqlTypeMap _XML = new SqlTypeMap(OdbcType.Text, DbType.AnsiString, SqlDbType.Text, typeof(string), ODBC32.SQL_TYPE.LONGVARCHAR, ODBC32.SQL_C.WCHAR, ODBC32.SQL_C.CHAR, -1, -1, false);
        private static readonly SqlTypeMap _Structured = new SqlTypeMap(OdbcType.Binary, DbType.Binary, SqlDbType.Structured, typeof(object[]), ODBC32.SQL_TYPE.SS_UTCDATETIME, ODBC32.SQL_C.BINARY, ODBC32.SQL_C.BINARY, -1, -1, false);

        // Methods
        #region Constructor
        private SqlTypeMap(OdbcType odbcType, DbType dbType, SqlDbType sqlDbType, Type type, ODBC32.SQL_TYPE sql_type, ODBC32.SQL_C sql_c, ODBC32.SQL_C param_sql_c, int bsize, int csize, bool signType)
        {
            this._odbcType = odbcType;
            this._dbType = dbType;
            this._sqlDbType = sqlDbType;
            this._type = type;
            this._sql_type = sql_type;
            this._sql_c = sql_c;
            this._param_sql_c = param_sql_c;
            this._bufferSize = bsize;
            this._columnSize = csize;
            this._signType = signType;
        }
        #endregion
        #region FromDbType
        internal static SqlTypeMap FromDbType(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.AnsiString:
                    return _VarChar;

                case DbType.Binary:
                    return _VarBinary;

                case DbType.Byte:
                    return _TinyInt;

                case DbType.Boolean:
                    return _Bit;

                case DbType.Currency:
                    return _Decimal;

                case DbType.Date:
                    return _Date;

                case DbType.DateTime:
                    return _DateTime;

                case DbType.Decimal:
                    return _Decimal;

                case DbType.Double:
                    return _Double;

                case DbType.Guid:
                    return _UniqueId;

                case DbType.Int16:
                    return _SmallInt;

                case DbType.Int32:
                    return _Int;

                case DbType.Int64:
                    return _BigInt;

                case DbType.Single:
                    return _Real;

                case DbType.String:
                    return _NVarChar;

                case DbType.Time:
                    return _Time;

                case DbType.AnsiStringFixedLength:
                    return _Char;

                case DbType.StringFixedLength:
                    return _NChar;
            }
            throw new ArgumentException("ADP_DbTypeNotSupported", dbType.ToString());
        }
        #endregion
        #region FromOdbcType
        internal static SqlTypeMap FromOdbcType(OdbcType odbcType)
        {
            switch (odbcType)
            {
                case OdbcType.BigInt:
                    return _BigInt;

                case OdbcType.Binary:
                    return _Binary;

                case OdbcType.Bit:
                    return _Bit;

                case OdbcType.Char:
                    return _Char;

                case OdbcType.DateTime:
                    return _DateTime;

                case OdbcType.Decimal:
                    return _Decimal;

                case OdbcType.Numeric:
                    return _Numeric;

                case OdbcType.Double:
                    return _Double;

                case OdbcType.Image:
                    return _Image;

                case OdbcType.Int:
                    return _Int;

                case OdbcType.NChar:
                    return _NChar;

                case OdbcType.NText:
                    return _NText;

                case OdbcType.NVarChar:
                    return _NVarChar;

                case OdbcType.Real:
                    return _Real;

                case OdbcType.UniqueIdentifier:
                    return _UniqueId;

                case OdbcType.SmallDateTime:
                    return _SmallDT;

                case OdbcType.SmallInt:
                    return _SmallInt;

                case OdbcType.Text:
                    return _Text;

                case OdbcType.Timestamp:
                    return _Timestamp;

                case OdbcType.TinyInt:
                    return _TinyInt;

                case OdbcType.VarBinary:
                    return _VarBinary;

                case OdbcType.VarChar:
                    return _VarChar;

                case OdbcType.Date:
                    return _Date;

                case OdbcType.Time:
                    return _Time;
            }
            throw new ArgumentOutOfRangeException("odbcType", odbcType, "ADP_InvalidEnumerationValue");
        }
        #endregion
        #region FromSqlType
        internal static SqlTypeMap FromSqlType(ODBC32.SQL_TYPE sqltype)
        {
            switch (sqltype)
            {
                case ODBC32.SQL_TYPE.SS_TIME_EX:
                    throw new ArgumentException("Odbc_UnknownSQLType", "sqltype:ODBC32.SQL_TYPE.SS_TIME_EX");

                case ODBC32.SQL_TYPE.SS_UTCDATETIME:
                    return _Structured;

                case ODBC32.SQL_TYPE.SS_XML:
                    return _XML;

                case ODBC32.SQL_TYPE.SS_UDT:
                    return _UDT;

                case ODBC32.SQL_TYPE.SS_VARIANT:
                    return _Variant;

                case ODBC32.SQL_TYPE.GUID:
                    return _UniqueId;

                case ODBC32.SQL_TYPE.WLONGVARCHAR:
                    return _NText;

                case ODBC32.SQL_TYPE.WVARCHAR:
                    return _NVarChar;

                case ODBC32.SQL_TYPE.WCHAR:
                    return _NChar;

                case ODBC32.SQL_TYPE.BIT:
                    return _Bit;

                case ODBC32.SQL_TYPE.TINYINT:
                    return _TinyInt;

                case ODBC32.SQL_TYPE.BIGINT:
                    return _BigInt;

                case ODBC32.SQL_TYPE.LONGVARBINARY:
                    return _Image;

                case ODBC32.SQL_TYPE.VARBINARY:
                    return _VarBinary;

                case ODBC32.SQL_TYPE.BINARY:
                    return _Binary;

                case ODBC32.SQL_TYPE.LONGVARCHAR:
                    return _Text;

                case ODBC32.SQL_TYPE.CHAR:
                    return _Char;

                case ODBC32.SQL_TYPE.NUMERIC:
                    return _Numeric;

                case ODBC32.SQL_TYPE.DECIMAL:
                    return _Decimal;

                case ODBC32.SQL_TYPE.INTEGER:
                    return _Int;

                case ODBC32.SQL_TYPE.SMALLINT:
                    return _SmallInt;

                case ODBC32.SQL_TYPE.FLOAT:
                    return _Double;

                case ODBC32.SQL_TYPE.REAL:
                    return _Real;

                case ODBC32.SQL_TYPE.DOUBLE:
                    return _Double;

                case ODBC32.SQL_TYPE.TIMESTAMP:
                case ODBC32.SQL_TYPE.TYPE_TIMESTAMP:
                    return _DateTime;

                case ODBC32.SQL_TYPE.VARCHAR:
                    return _VarChar;

                case ODBC32.SQL_TYPE.TYPE_DATE:
                    return _Date;

                case ODBC32.SQL_TYPE.TYPE_TIME:
                    return _Time;
            }
            throw new ArgumentException("Odbc_UnknownSQLType", sqltype.ToString());

        }
        #endregion
        #region FromSystemType
        internal static SqlTypeMap FromSystemType(Type dataType)
        {
            switch (Type.GetTypeCode(dataType))
            {
                case TypeCode.Empty:
                    throw new ArgumentException("ADP_InvalidDataType", "TypeCode.Empty");

                case TypeCode.Object:
                    if (dataType == typeof(byte[]))
                    {
                        return _VarBinary;
                    }
                    if (dataType == typeof(Guid))
                    {
                        return _UniqueId;
                    }
                    if (dataType == typeof(TimeSpan))
                    {
                        return _Time;
                    }
                    if (dataType == typeof(object[]))
                    {
                        return _Structured;
                    }
                    if (dataType != typeof(char[]))
                    {
                        throw new ArgumentException("ADP_InvalidDataType", dataType.ToString());
                    }
                    return _NVarChar;

                case TypeCode.DBNull:
                    throw new ArgumentException("ADP_InvalidDataType", "TypeCode.DBNull");

                case TypeCode.Boolean:
                    return _Bit;

                case TypeCode.Char:
                case TypeCode.String:
                    return _NVarChar;

                case TypeCode.SByte:
                    return _SmallInt;

                case TypeCode.Byte:
                    return _TinyInt;

                case TypeCode.Int16:
                    return _SmallInt;

                case TypeCode.UInt16:
                    return _Int;

                case TypeCode.Int32:
                    return _Int;

                case TypeCode.UInt32:
                    return _BigInt;

                case TypeCode.Int64:
                    return _BigInt;

                case TypeCode.UInt64:
                    return _Numeric;

                case TypeCode.Single:
                    return _Real;

                case TypeCode.Double:
                    return _Double;

                case TypeCode.Decimal:
                    return _Numeric;

                case TypeCode.DateTime:
                    return _DateTime;
            }
            throw new ArgumentException("ADP_UnknownDataTypeCode", dataType.ToString());
        }
        #endregion

        #region UpgradeSignedType
        internal static SqlTypeMap UpgradeSignedType(SqlTypeMap typeMap, bool unsigned)
        {
            if (unsigned)
            {
                switch (typeMap._dbType)
                {
                    case DbType.Int16:
                        return _Int;

                    case DbType.Int32:
                        return _BigInt;

                    case DbType.Int64:
                        return _Decimal;
                }
                return typeMap;
            }
            if (typeMap._dbType == DbType.Byte)
            {
                return _SmallInt;
            }
            return typeMap;
        }
        #endregion
    }
    #endregion

    #region IMRDBException
    public class IMRDBException : Exception
    {
        public IMRDBException() { }

        public IMRDBException(string message)
            : base(message) { }

        public IMRDBException(string message, Exception innerException)
            : base(message, innerException) { }
    }
    #endregion

    public class DBCommon : MarshalByRefObjectDisposable, IDisposable
    {
        #region Members
        //const string privateKey = "<RSAKeyValue><Modulus>3rUCOagVKP7WcKMr1/DDWJsxJomWHOFoab0cxaMf0PmN6Ck+2/Flg5Fnf77ZZyM26tpR+JMsmgDlc91I3xyXeW1gwwGfhKT5KqvFI8HdnXFYcpcTPx/ind0Gv696Jm2CSgsbHjxCq4H3aH9W8q5cc7hOuHGzmcmwCKaH28jT1sU=</Modulus><Exponent>AQAB</Exponent><P>9Y1Fa/Y0jlT28vqbUvZzJl80W8lXxd7CC+QSXh1+D17eTU3CNyodRw4t839yrvnQLr2StrY9lIfHeJn0LXafaQ==</P><Q>6C7lSPKhOdm372ho6mUGeQ1NnfrppoFc8XORf/3/BFgVH4OQMDxFO4ZYTITJFIC4+93fo4CVztN3revAwZhs/Q==</Q><DP>UCl9d2BaCfk84Vfk6zGfp/A+tJRHfkZZjO44/Nad98CdzBjNhZrPEgpUAAEkXHEbgJbM1a8q7VliSkHgUBNAuQ==</DP><DQ>c93bPDEgth7pRIcFPFuYBFbYMgtiCF6sRC4ZIRde6QsP138vOHMLVa4waFcnhZzLM53AmfZ0TBeJtPheY/4t/Q==</DQ><InverseQ>DXSlKJfmjdBY/PEFrXdyapxhQZTts8+C3u7SKKNaNSAI+yv1clQASemLAuIWLVVJSR/M1I1woAK5eFLAE2v+bA==</InverseQ><D>MzlrE5vnkhBbfnjpgresFttLSNPopSfrfjazQXotvUFJNJcYEDrL4KE3LxjUpHZjEU26APTJE3sVS7sgIcFPzX5Km4U5l6rNH//IA/vUeyyzmhp0qsH5R/lnK3zSyx0OOhQaYTnKmTe0pNlvywbng62oCH0qg+/RnK9Gz2zTaME=</D></RSAKeyValue>";
        #region DB
        private DB _db = new DB();
        public DB DB
        {
            get { return _db; }
        }
        #endregion

        #region Connected
        public bool Connected
        {
            get { return DB.Connected; }
        }
        #endregion

        #region Language
        // domyslny jezyk: angielski
        private Enums.Language language = Enums.Language.English;
        public Enums.Language Language
        {
            get { return language; }
            set { language = value; }
        }
        #endregion
        #region TimeZoneInfo
        public TimeZoneInfo TimeZoneInfo
        {
            get { return DB.TimeZoneInfo; }
            set { DB.TimeZoneInfo = value; }
        }
        #endregion
        #region ShortTimeout
        // domyslny krotki timeout: 30 [s]
        private int shortTimeout = 30;
        public int ShortTimeout
        {
            get
            {
                return shortTimeout;
            }
            set
            {
                shortTimeout = value;
            }
        }
        #endregion
        #region LongTimeout
        // domyslny dlugi timeout: 1800 [s] (pol godziny)
        private int longTimeout = 1800;
        public int LongTimeout
        {
            get
            {
                return longTimeout;
            }
            set
            {
                longTimeout = value;
            }
        }
        #endregion
        #region Description
        public string Description
        {
            get
            {
                return DB.ConnectionDescription;
            }
            set
            {
                DB.ConnectionDescription = value;
            }
        }
        #endregion
        #region ConnectTime
        public TimeSpan ConnectTime
        {
            get
            {
                return DB.ConnectTime;
            }
            set
            {
                DB.ConnectTime = value;
            }
        }
        #endregion
        #region ConnectRetryCount
        public int ConnectRetryCount
        {
            get
            {
                return DB.ConnectRetryCount;
            }
            set
            {
                DB.ConnectRetryCount = value;
            }
        }
        #endregion
        #region ReconnectTime
        public TimeSpan ReconnectTime
        {
            get
            {
                return DB.ReconnectTime;
            }
            set
            {
                DB.ReconnectTime = value;
            }
        }
        #endregion
        #region IMRServerVersion | DBSchemaVersion
        public string IMRServerVersion = null;
        public string DBSchemaVersion = null;
        #endregion

        #region SqlServer
        public SqlServerElement SqlServer;
        #endregion

        #region Filter
        private int[] _DistributorFilter = new int[0];
        public int[] DistributorFilter
        {
            get { if (_DistributorFilter == null) _DistributorFilter = new int[0]; return _DistributorFilter; }
            set { if (value == null) _DistributorFilter = new int[0]; else _DistributorFilter = value; _DistributorFilterDict = _DistributorFilter.Distinct().ToDictionary(q => q); }
        }
        private Dictionary<int, int> _DistributorFilterDict = new Dictionary<int, int>();
        public Dictionary<int, int> DistributorFilterDict
        {
            get { if (_DistributorFilterDict == null) _DistributorFilterDict = new Dictionary<int, int>(); return _DistributorFilterDict; }
            set { if (value == null) _DistributorFilterDict = new Dictionary<int, int>(); else _DistributorFilterDict = value; _DistributorFilter = _DistributorFilterDict.Keys.ToArray(); }
        }
        private long[] _LocationFilter = new long[0];
        public long[] LocationFilter
        {
            get { if (_LocationFilter == null) _LocationFilter = new long[0]; return _LocationFilter; }
            set { if (value == null) _LocationFilter = new long[0]; else _LocationFilter = value; _LocationFilterDict = _LocationFilter.Distinct().ToDictionary(q => q); }
        }
        private Dictionary<long, long> _LocationFilterDict = new Dictionary<long, long>();
        public Dictionary<long, long> LocationFilterDict
        {
            get { if (_LocationFilterDict == null) _LocationFilterDict = new Dictionary<long, long>(); return _LocationFilterDict; }
            set { if (value == null) _LocationFilterDict = new Dictionary<long, long>(); else _LocationFilterDict = value; _LocationFilter = _LocationFilterDict.Keys.ToArray(); }
        }
        private long[] _DeviceFilter = new long[0];
        public long[] DeviceFilter
        {
            get { if (_DeviceFilter == null) _DeviceFilter = new long[0]; return _DeviceFilter; }
            set { if (value == null) _DeviceFilter = new long[0]; else _DeviceFilter = value; _DeviceFilterDict = _DeviceFilter.Distinct().ToDictionary(q => q); }
        }
        private Dictionary<long, long> _DeviceFilterDict = new Dictionary<long, long>();
        public Dictionary<long, long> DeviceFilterDict
        {
            get { if (_DeviceFilterDict == null) _DeviceFilterDict = new Dictionary<long, long>(); return _DeviceFilterDict; }
            set { if (value == null) _DeviceFilterDict = new Dictionary<long, long>(); else _DeviceFilterDict = value; _DeviceFilter = _DeviceFilterDict.Keys.ToArray(); }
        }
        private long[] _MeterFilter = new long[0];
        public long[] MeterFilter
        {
            get { if (_MeterFilter == null) _MeterFilter = new long[0]; return _MeterFilter; }
            set { if (value == null) _MeterFilter = new long[0]; else _MeterFilter = value; _MeterFilterDict = _MeterFilter.Distinct().ToDictionary(q => q); }
        }
        private Dictionary<long, long> _MeterFilterDict = new Dictionary<long, long>();
        public Dictionary<long, long> MeterFilterDict
        {
            get { if (_MeterFilterDict == null) _MeterFilterDict = new Dictionary<long, long>(); return _MeterFilterDict; }
            set { if (value == null) _MeterFilterDict = new Dictionary<long, long>(); else _MeterFilterDict = value; _MeterFilter = _MeterFilterDict.Keys.ToArray(); }
        }
        private int[] _TaskGroupFilter = new int[0];
        public int[] TaskGroupFilter
        {
            get { if (_TaskGroupFilter == null) _TaskGroupFilter = new int[0]; return _TaskGroupFilter; }
            set { if (value == null) _TaskGroupFilter = new int[0]; else _TaskGroupFilter = value; _TaskGroupFilterDict = _TaskGroupFilter.Distinct().ToDictionary(q => q); }
        }
        private Dictionary<int, int> _TaskGroupFilterDict = new Dictionary<int, int>();
        public Dictionary<int, int> TaskGroupFilterDict
        {
            get { if (_TaskGroupFilterDict == null) _TaskGroupFilterDict = new Dictionary<int, int>(); return _TaskGroupFilterDict; }
            set { if (value == null) _TaskGroupFilterDict = new Dictionary<int, int>(); else _TaskGroupFilterDict = value; _TaskGroupFilter = _TaskGroupFilterDict.Keys.ToArray(); }
        }
        #endregion

        /// <summary>
        /// Default Sql Server timeout, to be used for setting Command Timeout
        /// </summary>
        public const int SqlServerDefaultTimeout = int.MaxValue;

        protected readonly int MaxItemsPerSqlTableType = 1000;
        protected readonly bool UseBulkInsertSqlTableType = true;
        #endregion

        #region Constructor
        public DBCommon(string ServerName, Enums.Language Language, TimeSpan? TimeZoneOffset, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime, string OperatorLogin = null)
        {
            this.Language = Language;
            this.ShortTimeout = ShortTimeout;
            this.LongTimeout = LongTimeout;
            if (ConnectTime != TimeSpan.Zero)
                this.ConnectTime = ConnectTime;
            if (ConnectRetryCount != 0)
                this.ConnectRetryCount = ConnectRetryCount;
            if (ReconnectTime != TimeSpan.Zero)
                this.ReconnectTime = ReconnectTime;

            SqlServerElement SqlServer;

            try
            {
                SqlServerSection SqlServers = (SqlServerSection)ConfigurationManager.GetSection("SqlServersSection");
                SqlServer = SqlServers.SqlServers[ServerName];
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Server '{0}' entry error or entry missing in configuration file: {0}", ServerName, ex.Message));
            }

            if (string.IsNullOrEmpty(Description))
            {
                try
                {
                    Description = Assembly.GetEntryAssembly().EntryPoint.DeclaringType.Namespace;
                }
                catch
                {
                    Description = SqlServer.Description;
                }
            }

            DB.Start(SqlServer.Server, Description, SqlServer.DB, Decrypt(SqlServer.User), Decrypt(SqlServer.Pwd), SqlServer.UseTrustedConnection, OperatorLogin);
            if (TimeZoneOffset != null)
                this.TimeZoneInfo = TimeZoneInfo.CreateCustomTimeZone("IMR", TimeZoneOffset.Value, "IMR", "IMR");
        }
        public DBCommon(string ServerName)
            : this(ServerName, Enums.Language.English)
        { }
        public DBCommon(string ServerName, Enums.Language Language)
            : this(ServerName, Language, 30, 1800)
        { }
        public DBCommon(string ServerName, Enums.Language Language, TimeSpan ConnectTime)
            : this(ServerName, Language, null, 30, 1800, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string ServerName, Enums.Language Language, int ShortTimeout, int LongTimeout)
            : this(ServerName, Language, null, ShortTimeout, LongTimeout, TimeSpan.Zero, 0, TimeSpan.Zero)
        { }
        public DBCommon(string ServerName, Enums.Language Language, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime)
            : this(ServerName, Language, null, ShortTimeout, LongTimeout, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string ServerName, Enums.Language Language, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(ServerName, Language, null, 30, 1800, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        public DBCommon(string ServerName, int ShortTimeout, int LongTimeout)
            : this(ServerName, Enums.Language.English, ShortTimeout, LongTimeout)
        { }
        public DBCommon(string ServerName, TimeSpan ConnectTime)
            : this(ServerName, Enums.Language.English, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string ServerName, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(ServerName, Enums.Language.English, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        public DBCommon(string ServerName, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime)
            : this(ServerName, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string ServerName, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(ServerName, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }


        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language, TimeSpan? TimeZoneOffset, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime, bool UseTrustedConnection = false, string OperatorLogin = null, string TimeZoneId = null)
        {
            this.Language = Language;
            this.ShortTimeout = ShortTimeout;
            this.LongTimeout = LongTimeout;
            if (ConnectTime != TimeSpan.Zero)
                this.ConnectTime = ConnectTime;
            if (ConnectRetryCount != 0)
                this.ConnectRetryCount = ConnectRetryCount;
            if (ReconnectTime != TimeSpan.Zero)
                this.ReconnectTime = ReconnectTime;

            if (string.IsNullOrEmpty(Description))
            {
                try
                {
                    Description = Assembly.GetEntryAssembly().EntryPoint.DeclaringType.Namespace;
                }
                catch
                {
                }
            }

            DB.Start(Server, Description, DataBase, Decrypt(User), Decrypt(Password), UseTrustedConnection, OperatorLogin);
            if (TimeZoneOffset != null)
                this.TimeZoneInfo = TimeZoneInfo.CreateCustomTimeZone("IMR", TimeZoneOffset.Value, "IMR", "IMR");
            else if (TimeZoneId != null)
            {
                try
                {
                    this.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
                }
                catch
                {
                    // ignored
                }
            }
        }

        public DBCommon(string Server, string DataBase, string User, string Password)
            : this(Server, DataBase, User, Password, Enums.Language.English)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language)
            : this(Server, DataBase, User, Password, Language, 30, 1800)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language, int ShortTimeout, int LongTimeout)
            : this(Server, DataBase, User, Password, Language, null, ShortTimeout, LongTimeout, TimeSpan.Zero, 0, TimeSpan.Zero)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language, TimeSpan ConnectTime)
            : this(Server, DataBase, User, Password, Language, null, 30, 1800, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime)
            : this(Server, DataBase, User, Password, Language, null, ShortTimeout, LongTimeout, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, Enums.Language Language, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(Server, DataBase, User, Password, Language, null, 30, 1800, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, int ShortTimeout, int LongTimeout)
            : this(Server, DataBase, User, Password, Enums.Language.English, ShortTimeout, LongTimeout)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, TimeSpan ConnectTime)
            : this(Server, DataBase, User, Password, Enums.Language.English, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(Server, DataBase, User, Password, Enums.Language.English, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime)
            : this(Server, DataBase, User, Password, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, 1, TimeSpan.Zero)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(Server, DataBase, User, Password, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime, string OperatorLogin)
            : this(Server, DataBase, User, Password, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, ConnectRetryCount, ReconnectTime, OperatorLogin: OperatorLogin)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime, bool UseTrustedConnection, string OperatorLogin)
            : this(Server, DataBase, User, Password, Enums.Language.English, null, ShortTimeout, LongTimeout, ConnectTime, ConnectRetryCount, ReconnectTime, UseTrustedConnection: UseTrustedConnection, OperatorLogin: OperatorLogin)
        { }
        public DBCommon(string Server, string DataBase, string User, string Password, TimeSpan? TimeZoneOffset, int ShortTimeout, int LongTimeout, TimeSpan ConnectTime, int ConnectRetryCount, TimeSpan ReconnectTime)
            : this(Server, DataBase, User, Password, Enums.Language.English, TimeZoneOffset, ShortTimeout, LongTimeout, ConnectTime, ConnectRetryCount, ReconnectTime)
        { }
        #endregion
        #region Destrcutor
        ~DBCommon()
        {
            Cache.ClearAll();

            if (DB == null) Logger.Add(EventID.DB.DestructorDBCheck);

            DB.Stop();

            Logger.Add(EventID.DB.DestructorDBStopped);

            Dispose(false);
        }
        #endregion
        #region Decrypt
        internal string Decrypt(string strToDecript)
        {
            try
            {
                return IMR.Suite.Common.RSA.Decrypt(strToDecript); // sprobuj odszyfrowac
            }
            catch { } // nie udalo sie odszyfrowac - oznacza, ze bylo odszyfrowane
            return strToDecript;

            //using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
            //{
            //    RSACryptoServiceProvider.UseMachineKeyStore = true;
            //    provider.FromXmlString(privateKey);

            //    byte[] bytes = provider.Decrypt(Convert.FromBase64String(strToDecript), false);
            //    return Encoding.UTF8.GetString(bytes);
            //}
        }

        #endregion
        #region Encrypt
        internal string Encrypt(string strToEncrypt)
        {
            return IMR.Suite.Common.RSA.Encrypt(strToEncrypt);
            //using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
            //{
            //    RSACryptoServiceProvider.UseMachineKeyStore = true;
            //    provider.FromXmlString(privateKey);

            //    byte[] bytes = provider.Encrypt(Encoding.ASCII.GetBytes(strToEncrypt), false);
            //    return Convert.ToBase64String(bytes);
            //}
        }

        #endregion
        #region IDisposable Members
        public void Dispose()
        {
            Cache.ClearAll();
            DB.Stop();
            base.Dispose();
        }
        #endregion

        #region DateTimeNow
        public DateTime DateTimeNow
        {
            get
            {
                if (this.TimeZoneInfo != null)
                    return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.Utc, this.TimeZoneInfo);
                return DateTime.Now;
            }
        }
        #endregion
        #region DateTimeUtcNow
        public DateTime DateTimeUtcNow
        {
            get { return DateTime.UtcNow; }
        }
        #endregion
        #region ConvertUtcTimeToTimeZone
        public DateTime? ConvertUtcTimeToTimeZone(DateTime? dateTime)
        {
            if (dateTime == null)
                return null;

            return ConvertUtcTimeToTimeZone(dateTime.Value);
        }
        public DateTime ConvertUtcTimeToTimeZone(DateTime dateTime)
        {
            if (this.TimeZoneInfo != null)
            {
//                if (this.TimeZoneInfo.BaseUtcOffset.Ticks == 0)
//                    return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
//                else
                    return TimeZoneInfo.ConvertTime(new DateTime(dateTime.Ticks, DateTimeKind.Unspecified), TimeZoneInfo.Utc, this.TimeZoneInfo);
            }
            return dateTime.ToLocalTime();
        }
        #endregion
        #region ConvertTimeZoneToUtcTime
        public DateTime? ConvertTimeZoneToUtcTime(DateTime? dateTime)
        {
            if (dateTime == null)
                return null;

            return ConvertTimeZoneToUtcTime(dateTime.Value);
        }
        public DateTime ConvertTimeZoneToUtcTime(DateTime dateTime)
        {
            if (this.TimeZoneInfo != null)
            {
//                if (this.TimeZoneInfo.BaseUtcOffset.Ticks == 0)
//                    return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
//                else
                    return TimeZoneInfo.ConvertTime(new DateTime(dateTime.Ticks, DateTimeKind.Unspecified), this.TimeZoneInfo, TimeZoneInfo.Utc);
            }
            return dateTime.ToUniversalTime();
        }
        #endregion

        #region ConvertToSqlType
        public object ConvertToSqlType(object value)
        {
            if (value is ulong)
                return (SqlInt64)(long)(ulong)value;
            else if (value is uint)
                return (SqlInt32)(int)(uint)value;
            else if (value is ushort)
                return (SqlInt16)(short)(ushort)value;
            else if (value is sbyte)
                return (SqlByte)(byte)(sbyte)value;
            else if (value == null)
                return DBNull.Value;
            else
                return value;
        }
        #endregion
        #region Procedury GetPossiblyNullValue
        #region GetPossiblyNullValue(SqlDataReader, int, out bool)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out bool Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new bool();
                return false;
            }

            Result = QueryResult.GetBoolean(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out byte)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out byte Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new byte();
                return false;
            }

            Result = QueryResult.GetByte(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out int)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out int Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new int();
                return false;
            }

            Result = QueryResult.GetInt32(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out uint)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out uint Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new uint();
                return false;
            }

            Result = (uint)QueryResult.GetInt32(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out DateTime)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out DateTime Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new DateTime();
                return false;
            }

            Result = QueryResult.GetDateTime(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out string)
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out string Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = "";
                return false;
            }

            Result = QueryResult.GetString(ColumnOrdinal);
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(SqlDataReader, int, out byte[])
        protected static bool GetPossiblyNullValue(SqlDataReader QueryResult, int ColumnOrdinal, out byte[] Result)
        {
            if (QueryResult.IsDBNull(ColumnOrdinal))
            {
                Result = new byte[0];
                return false;
            }

            Result = QueryResult.GetSqlBinary(ColumnOrdinal).Value;
            return true;
        }
        #endregion

        #region GetPossiblyNullValue(DataRow, int, uint, out uint)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, uint ValueIfNull, out uint Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = new uint();
                return false;
            }

            Result = (uint)(int)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, uint, out uint)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, uint ValueIfNull, out uint Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = new uint();
                return false;
            }

            Result = (uint)(int)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out uint)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out uint Result)
        {
            return GetPossiblyNullValue(Row, ColumnOrdinal, new uint(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out uint)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out uint Result)
        {
            return GetPossiblyNullValue(Row, ColumnName, new uint(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, int, out int)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, int ValueIfNull, out int Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (int)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, int, out int)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, int ValueIfNull, out int Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (int)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out int)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out int Result)
        {
            return GetPossiblyNullValue(Row, ColumnOrdinal, new int(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out int)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out int Result)
        {
            return GetPossiblyNullValue(Row, ColumnName, new int(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, string, out string)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, string ValueIfNull, out string Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (string)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, string, out string)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, string ValueIfNull, out string Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (string)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out string)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out string Result)
        {
            return GetPossiblyNullValue(Row, ColumnOrdinal, "", out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out string)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out string Result)
        {
            return GetPossiblyNullValue(Row, ColumnName, "", out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, DateTime, out DateTime)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, DateTime ValueIfNull, out DateTime Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (DateTime)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, DateTime, out DateTime)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, DateTime ValueIfNull, out DateTime Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (DateTime)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out DateTime)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out DateTime Result)
        {
            return GetPossiblyNullValue(Row, ColumnOrdinal, new DateTime(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out DateTime)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out DateTime Result)
        {
            return GetPossiblyNullValue(Row, ColumnName, new DateTime(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, bool, out bool)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, bool ValueIfNull, out bool Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (bool)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, bool, out bool)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, bool ValueIfNull, out bool Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = ValueIfNull;
                return false;
            }

            Result = (bool)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out bool)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out bool Result)
        {
            return GetPossiblyNullValue(Row, ColumnOrdinal, new bool(), out Result);
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out bool)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out bool Result)
        {
            return GetPossiblyNullValue(Row, ColumnName, new bool(), out Result);
        }
        #endregion

        #region GetPossiblyNullValue(DataRow, int, out uint?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out uint? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (uint)(int)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out uint?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out uint? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (uint)(int)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out int?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out int? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (int)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out int?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out int? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (int)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out ulong?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out ulong? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (ulong)(long)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out ulong?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out ulong? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (ulong)(long)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out long?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out long? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (long)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out long?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out long? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (long)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out DateTime?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out DateTime? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (DateTime)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out DateTime?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out DateTime? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (DateTime)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out bool?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out bool? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (bool)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out bool?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out bool? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (bool)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out double?)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out double? Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = (double)Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out double?)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out double? Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = (double)Row[ColumnName];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, int, out object)
        protected static bool GetPossiblyNullValue(DataRow Row, int ColumnOrdinal, out object Result)
        {
            if (Row.IsNull(ColumnOrdinal))
            {
                Result = null;
                return false;
            }

            Result = Row[ColumnOrdinal];
            return true;
        }
        #endregion
        #region GetPossiblyNullValue(DataRow, string, out object)
        protected static bool GetPossiblyNullValue(DataRow Row, string ColumnName, out object Result)
        {
            if (Row.IsNull(ColumnName))
            {
                Result = null;
                return false;
            }

            Result = Row[ColumnName];
            return true;
        }
        #endregion
        #endregion

        #region ExecuteProcedure
        public DataSet ExecuteProcedure(string storedProcName, params object[] args)
        {
            return ExecuteProcedure(storedProcName, this.ShortTimeout, args);
        }
        public DataSet ExecuteProcedure(string storedProcName, int timeOut, params object[] args)
        {
            object[] argsArray = new object[args.Length]; args.CopyTo(argsArray, 0);
            return ExecuteProcedure(storedProcName, timeOut, ref argsArray);
        }
        public DataSet ExecuteProcedure(string storedProcName, ref object[] args)
        {
            return ExecuteProcedure(storedProcName, this.ShortTimeout, ref args);
        }
        public DataSet ExecuteProcedure(string storedProcName, int timeOut, ref object[] args)
        {
            DataSet result = new DataSet();

            DB.Parameter[] parameters;
            if (PrepareDBParameters(storedProcName, args, out parameters))
            {
                // sa parametry OUT w sp wiec musimy je pobrac i "oddac" do args
                object[] ds = (object[])DB.ExecuteProcedure(storedProcName, new DB.AnalyzeDataSetAndParameters(DefaultAnalyzeDataSetAndParameters), parameters, timeOut);
                result = (DataSet)ds[0];

                DB.Parameter[] retParameters = (DB.Parameter[])ds[1];
                for (int i = 0; i < retParameters.Length; i++)
                {
                    if (retParameters[i].Direction == ParameterDirection.InputOutput ||
                        retParameters[i].Direction == ParameterDirection.Output)
                    {
                        if (retParameters[i].IsNull)
                            args[i] = null;
                        else
                            args[i] = retParameters[i].Value;
                    }
                }
            }
            else
            {
                result = (DataSet)DB.ExecuteProcedure(storedProcName, new DB.AnalyzeDataSet(DefaultAnalyzeDataSet), parameters, timeOut);
            }
            return result;
        }
        #endregion
        #region ExecuteNonQueryProcedure
        public void ExecuteNonQueryProcedure(string storedProcName, params object[] args)
        {
            ExecuteNonQueryProcedure(storedProcName, this.ShortTimeout, args);
        }
        public void ExecuteNonQueryProcedure(string storedProcName, int timeOut, params object[] args)
        {
            object[] argsArray = new object[args.Length]; args.CopyTo(argsArray, 0);
            ExecuteNonQueryProcedure(storedProcName, timeOut, ref argsArray);
        }
        public void ExecuteNonQueryProcedure(string storedProcName, ref object[] args)
        {
            ExecuteNonQueryProcedure(storedProcName, this.ShortTimeout, ref args);
        }
        public void ExecuteNonQueryProcedure(string storedProcName, int timeOut, ref object[] args)
        {
            DB.Parameter[] parameters;
            if (PrepareDBParameters(storedProcName, args, out parameters))
            {
                // sa parametry OUT w sp wiec musimy je pobrac i "oddac" do args
                DB.Parameter[] retParameters = (DB.Parameter[])DB.ExecuteNonQueryProcedure(storedProcName, new DB.AnalyzeParameters(DefaultAnalyzeParameters), parameters, timeOut);
                for (int i = 0; i < retParameters.Length; i++)
                {
                    if (retParameters[i].Direction == ParameterDirection.InputOutput ||
                        retParameters[i].Direction == ParameterDirection.Output)
                    {
                        if (retParameters[i].IsNull)
                            args[i] = null;
                        else
                            args[i] = retParameters[i].Value;
                    }
                }
            }
            else
            {
                DB.ExecuteNonQueryProcedure(storedProcName, parameters, timeOut);
            }
        }
        #endregion
        #region ExecuteScalar
        public object ExecuteScalar(string storedProcName, params object[] args)
        {
            return ExecuteScalar(storedProcName, this.ShortTimeout, args);
        }
        public object ExecuteScalar(string storedProcName, int timeOut, params object[] args)
        {
            object[] argsArray = new object[args.Length]; args.CopyTo(argsArray, 0);
            return ExecuteScalar(storedProcName, timeOut, ref argsArray);
        }
        public object ExecuteScalar(string storedProcName, ref object[] args)
        {
            return ExecuteScalar(storedProcName, this.ShortTimeout, ref args);
        }
        public object ExecuteScalar(string storedProcName, int timeOut, ref object[] args)
        {
            object result = null;

            DB.Parameter[] parameters;
            if (PrepareDBParameters(storedProcName, args, out parameters))
            {
                // sa parametry OUT w sp wiec musimy je pobrac i "oddac" do args
                object[] ds = (object[])DB.ExecuteScalar(storedProcName, new DB.AnalyzeResultAndParameters(DefaultAnalyzeResultAndParameters), parameters, timeOut);
                result = (object)ds[0];

                DB.Parameter[] retParameters = (DB.Parameter[])ds[1];
                for (int i = 0; i < retParameters.Length; i++)
                {
                    if (retParameters[i].Direction == ParameterDirection.InputOutput ||
                        retParameters[i].Direction == ParameterDirection.Output)
                    {
                        if (retParameters[i].IsNull)
                            args[i] = null;
                        else
                            args[i] = retParameters[i].Value;
                    }
                }
            }
            else
            {
                result = DB.ExecuteScalar(storedProcName, parameters, timeOut);
            }
            return result;
        }
        #endregion


        #region private PrepareDBParameters
        private bool PrepareDBParameters(string storedProcName, object[] args, out DB.Parameter[] parameters)
        {

            DataTable dt = Cache.StoredProcSchema.Get(storedProcName, this);

            if (dt.Rows.Count == 0)
            {
                throw new ArgumentException("Invalid or empty stored procedure", storedProcName);
            }

            if (dt.Rows.Count - 1 != args.Length)
            {
                throw new ArgumentException("Incorrect argumnet numbers", storedProcName);
            }

            bool bAnalyzeOutParameters = false;
            parameters = new DB.Parameter[dt.Rows.Count - 1];
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                ODBC32.SQL_PARAM sqlParam = (ODBC32.SQL_PARAM)int.Parse(dt.Rows[i]["COLUMN_TYPE"].ToString());
                ODBC32.SQL_TYPE sqlType = (ODBC32.SQL_TYPE)int.Parse(dt.Rows[i]["DATA_TYPE"].ToString());
                SqlTypeMap sqlTypeMap = SqlTypeMap.FromSqlType(sqlType);

                string paramName = dt.Rows[i]["COLUMN_NAME"].ToString();

                switch (sqlParam)
                {
                    case ODBC32.SQL_PARAM.RETURN_VALUE:
                        break;
                    case ODBC32.SQL_PARAM.INPUT:
                        {
                            if (sqlTypeMap._sqlDbType != SqlDbType.Structured)
                            {
                                parameters[i - 1] = new DB.InParameter(paramName, sqlTypeMap._sqlDbType, args[i - 1], sqlTypeMap._bufferSize);
                                break;
                            }
                            //if (!(args[i - 1] is object[]))
                            if (args[i - 1].GetType().BaseType != typeof(Array))
                            {
                                parameters[i - 1] = new DB.InParameter(paramName, sqlTypeMap._sqlDbType, args[i - 1], sqlTypeMap._bufferSize);
                                break;
                            }

                            string userTableTypeName = dt.Rows[i]["TYPE_NAME"].ToString();
                            DataTable userTableType = Cache.UserTableTypeSchema.Get(userTableTypeName, this);

                            object[] argTableType = (object[])args[i - 1];
                            for (int rows = 0; rows < argTableType.Length; rows++)
                            {
                                DataRow row = userTableType.NewRow();
                                if (argTableType[rows] is object[])
                                {
                                    if (userTableType.Columns.Count != ((object[])argTableType[rows]).Length)
                                    {
                                        throw new ArgumentException("Incorrect count of columns", storedProcName);
                                    }
                                    for (int column = 0; column < userTableType.Columns.Count; column++)
                                    {
                                        row[column] = ((object[])argTableType[rows])[column] ?? DBNull.Value;
                                    }
                                }
                                else
                                {
                                    row[0] = argTableType[rows] ?? DBNull.Value;
                                }
                                userTableType.Rows.Add(row);
                            }
                            parameters[i - 1] = new DB.InParameter(paramName, sqlTypeMap._sqlDbType, userTableType, sqlTypeMap._bufferSize);
                        }
                        break;
                    case ODBC32.SQL_PARAM.INPUT_OUTPUT:
                        parameters[i - 1] = new DB.InOutParameter(paramName, sqlTypeMap._sqlDbType, args[i - 1]);
                        bAnalyzeOutParameters = true;
                        break;
                    case ODBC32.SQL_PARAM.OUTPUT:
                        parameters[i - 1] = new DB.OutParameter(paramName, sqlTypeMap._sqlDbType, sqlTypeMap._bufferSize);
                        bAnalyzeOutParameters = true;
                        break;
                }
            }
            return bAnalyzeOutParameters;
        }
        #endregion
        #region CreateTableParam
        public DB.InParameter CreateTableParam(string ParamName, int[] ParamValues)
        {
            if (ParamValues == null)
                ParamValues = new int[0];

            DataTable table = new DataTable();

            table.Columns.Add("VALUE", typeof(int));

            for (int i = 0; i < ParamValues.Length; i++)
            {
                DataRow row = table.NewRow();
                row["VALUE"] = ParamValues[i];

                table.Rows.Add(row);
            }

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        public DB.InParameter CreateTableParam(string ParamName, long[] ParamValues)
        {
            if (ParamValues == null)
                ParamValues = new long[0];

            DataTable table = new DataTable();

            table.Columns.Add("VALUE", typeof(long));

            for (int i = 0; i < ParamValues.Length; i++)
            {
                DataRow row = table.NewRow();
                row["VALUE"] = ParamValues[i];

                table.Rows.Add(row);
            }

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        public DB.InParameter CreateTableParam(string ParamName, double[] ParamValues)
        {
            if (ParamValues == null)
                ParamValues = new double[0];

            DataTable table = new DataTable();

            table.Columns.Add("VALUE", typeof(double));

            for (int i = 0; i < ParamValues.Length; i++)
            {
                DataRow row = table.NewRow();
                row["VALUE"] = ParamValues[i];

                table.Rows.Add(row);
            }

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        public DB.InParameter CreateTableParam(string ParamName, string[] ParamValues)
        {
            if (ParamValues == null)
                ParamValues = new string[0];

            DataTable table = new DataTable();

            table.Columns.Add("VALUE", typeof(string));

            for (int i = 0; i < ParamValues.Length; i++)
            {
                DataRow row = table.NewRow();
                row["VALUE"] = ParamValues[i];

                table.Rows.Add(row);
            }

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        public DB.InParameter CreateTableParam(string ParamName, TypeDateTimeCode ParamValues)
        {
            DataTable table = new DataTable();

            table.Columns.Add("MODE", typeof(int));
            table.Columns.Add("DATETIME_1", typeof(DateTime));
            table.Columns.Add("DATETIME_2", typeof(DateTime));

            DataRow row = table.NewRow();
            if (ParamValues != null)
            {
                row["MODE"] = (int)ParamValues.Mode;
                if (ParamValues.DateTime1.HasValue)
                    row["DATETIME_1"] = ConvertTimeZoneToUtcTime(ParamValues.DateTime1.Value);
                else
                    row["DATETIME_1"] = DBNull.Value;
                if (ParamValues.DateTime2.HasValue)
                    row["DATETIME_2"] = ConvertTimeZoneToUtcTime(ParamValues.DateTime2.Value);
                else
                    row["DATETIME_2"] = DBNull.Value;
            }
            else
                row["MODE"] = -1;

            table.Rows.Add(row);

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        public DB.InParameter CreateTableParam(string ParamName, object ParamValues)
        {
            if (ParamValues == null)
                return CreateTableParam(ParamName, null as int[]);

            if (ParamValues is int[])
                return CreateTableParam(ParamName, ParamValues as int[]);

            if (ParamValues is long[])
                return CreateTableParam(ParamName, ParamValues as long[]);

            if (ParamValues is double[])
                return CreateTableParam(ParamName, ParamValues as double[]);

            if (ParamValues is string[])
                return CreateTableParam(ParamName, ParamValues as string[]);

            throw new ApplicationException("Unknown object type for ParamValues parameter");
        }
        public DB.InParameter CreateTableParam(string ParamName, DATA_TYPE_UNIT[] ParamValues)
        {
            DataTable table = new DataTable();

            table.Columns.Add("ID_DATA_TYPE", typeof(long));
            table.Columns.Add("ID_UNIT", typeof(int));

            for (int i = 0; i < ParamValues.Length; i++)
            {
                DataRow row = table.NewRow();
                row["ID_DATA_TYPE"] = ParamValues[i].IdDataType;
                if (ParamValues[i].IdUnit.HasValue && ParamValues[i].IdUnit.Value > 0)
                    row["ID_UNIT"] = ParamValues[i].IdUnit.Value;
                else
                    row["ID_UNIT"] = DBNull.Value;

                table.Rows.Add(row);
            }

            return new DB.InParameter(ParamName, SqlDbType.Structured, table);
        }
        #endregion

        #region private DefaultAnalyzeDataSet
        private object DefaultAnalyzeDataSet(DataSet QueryResult)
        {
            return QueryResult;
        }
        #endregion
        #region private DefaultAnalyzeParameters
        private object DefaultAnalyzeParameters(DB.Parameter[] Parameters)
        {
            return Parameters;
        }
        #endregion
        #region private DefaultAnalyzeDataSetAndParameters
        private object DefaultAnalyzeDataSetAndParameters(DataSet QueryResult, DB.Parameter[] Parameters)
        {
            return new object[] { QueryResult, Parameters };
        }
        #endregion
        #region private DefaultAnalyzeResultAndParameters
        private object DefaultAnalyzeResultAndParameters(Object Result, DB.Parameter[] Parameters)
        {
            return new object[] { Result, Parameters };
        }
        #endregion

        // Procedury wspolne dla wszystkich
        #region InsertImrseProcArg
        protected void InsertImrseProcArg(long id, List<Tuple<int, object>> args)
        {
            bool bAllOk = false;
            DB.Advanced DBAdvanced = new DB.Advanced(this.DB);

            SqlCommand Command = null;
            SqlDataAdapter selectDataAdapter = null;

            int RetryNumForDeadLock = 0;
            while (!bAllOk)
            {
                #region inicjalizacja komunikacji z baza
                try
                {
                    lock (this.DB)
                    {
                        //nastepna linia automatycznie rozpoczyna transakcje !!!
                        Command = DBAdvanced.GetCommand(false);
                #endregion
                        #region Pobranie schematu Tabelki z cache lub z bazy
                        DataTable dataTable = Cache.TableSchema.Get("IMRSE_PROC_ARG", this);
                        #endregion
                        #region Przepisanie wszystkich wierszy z wartosciami z data do DataTable
                        foreach (Tuple<int, object> arg in args)
                        {
                            DataRow row = dataTable.NewRow();
                            row["ID"] = id;
                            row["TYPE"] = arg.Item1;
                            if (arg.Item2 == null)
                                row["VALUE"] = "NULL";
                            else
                                row["VALUE"] = ConvertToSqlType(arg.Item2);
                            dataTable.Rows.Add(row);
                        }
                        #endregion
                        #region BulkCopy - "masowe" wstawienie wierszy do DB
                        Command.CommandText = "SqlBulkCopy";
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(Command.Connection, SqlBulkCopyOptions.Default, Command.Transaction))
                        {
                            bulkCopy.BatchSize = 30000;
                            bulkCopy.BulkCopyTimeout = ShortTimeout;
                            bulkCopy.DestinationTableName = "IMRSE_PROC_ARG";
                            bulkCopy.WriteToServer(dataTable);
                        }
                        #endregion
                        #region zakonczenie komunikacji z baza
                        bAllOk = true;
                    }
                }
                catch (SqlException Exception)
                {
                    bool bThrow = true;
                    #region If Deadlocked
                    if (DBAdvanced.Deadlocked(Exception))
                    {
                        if ((++RetryNumForDeadLock > this.DB.RetryCountForDeadLock))
                            bThrow = true;
                        else
                            bThrow = false;
                    }
                    #endregion
                    if (bThrow)
                    {
                        string CommandText = "";
                        string ParametersText = "";

                        if (Command != null)
                            DBAdvanced.CreateCommandDump(Command, out CommandText, out ParametersText);

                        DBAdvanced.Rollback();

                        if (Command != null)
                            DBAdvanced.LogException(Command, Exception, CommandText, ParametersText);

                        throw;
                    }
                }
                finally
                {
                    if (selectDataAdapter != null)
                        selectDataAdapter.Dispose();

                    if (bAllOk)
                        DBAdvanced.Commit();
                    else
                        DBAdvanced.Rollback();
                }
                        #endregion
            }
        }
        #endregion
        #region DeleteImrseProcArg
        protected void DeleteImrseProcArg(long id)
        {
            DB.ExecuteNonQuerySql(string.Format(@"DELETE FROM IMRSE_PROC_ARG WHERE ID={0}", id), false);
        }
        #endregion

        #region BulkInsertUpdate

        #region GetTableRowType

        protected Type GetTableRowType(object tableRow)
        {
            Type argType = tableRow.GetType();
            if (argType.Name.StartsWith("Op"))
                argType = argType.BaseType;
            return argType;
        }

        #endregion
        #region GetTableSchema

        protected DataTable GetTableSchema(object tableRow)
        {
            string tableName = "";
            Type argType = GetTableRowType(tableRow);
            tableName = argType.Name.Substring(3);
            return GetTableSchema(tableName);
        }

        protected DataTable GetTableSchema(string tableName)
        {
            DataTable table = Cache.TableSchema.Get(tableName, this);
            table.TableName = tableName;
            return table;
        }

        #endregion

        #region FillDataTable

        protected void FillDataTable(object[] args, ref DataTable dataTable, Enums.DataChange changeType)
        {
            if (args != null && args.Count() > 0 && (changeType == Enums.DataChange.Insert || changeType == Enums.DataChange.Update))
            {
                Type argType = GetTableRowType(args[0]);
                System.Reflection.FieldInfo[] fiList = argType.GetFields();
                int iterator = 0;
                foreach (object arg in args)
                {
                    DataRow row = dataTable.NewRow();
                    foreach (System.Reflection.FieldInfo fiItem in fiList)
                    {
                        object value = fiItem.GetValue(arg);
                        if (value == null)
                            row[fiItem.Name] = DBNull.Value;
                        else
                            row[fiItem.Name] = value;
                    }
                    dataTable.Rows.Add(row);
                    if (changeType == Enums.DataChange.Update)
                    {
                        dataTable.Rows[iterator].AcceptChanges();
                        dataTable.Rows[iterator].SetModified();
                    }
                    iterator++;
                }
            }
        }

        #endregion

        #region BulkInsert
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="args"></param>
        /// <param name="primaryKeysFields">Jeśli przekazujemy ten parametr to dane w DataTable powinny być posortowane wg.
        /// kluczy, ponieważ baza danych przy insert sortowała wg. kluczy, co powodowało,
        /// że mogło to być w innej kolejności niż w przekazanym DataTabe i następnie podczas
        /// przypisywania ID były przypisywane ID do złych obiektów.</param>
        /// <param name="fillDataTableMethod"></param>
        /// <param name="getMaxIdFromDB">true, pobierze Max ID z DB dla klucza głównego (tylko gdy jest jeden klucz główny)</param>
        /// <param name="commandTimeout"></param>
        protected void BulkInsert(string tableName, object[] args,
            List<string> primaryKeysFields = null,
            Func<object[], DataTable, Enums.DataChange, DataTable> fillDataTableMethod = null,
            bool getMaxIdFromDB = false,
            int commandTimeout = 0)
        {
            if (args == null || args.Length == 0)
                return;

            BulkInsert(tableName, new object[][] { args }, primaryKeysFields: primaryKeysFields, fillDataTableMethod: fillDataTableMethod, getMaxIdFromDB: getMaxIdFromDB, commandTimeout: commandTimeout);
        }


        // Dlaczego przekazywana jest tablica tablic obiektów do wstawienia, patrz komentarz niżej w kodzie: Małe wyjaśnienie "object[][] args"
        protected void BulkInsert(string tableName, object[][] args,
            List<string> primaryKeysFields = null,
            Func<object[], DataTable, Enums.DataChange, DataTable> fillDataTableMethod = null,
            bool getMaxIdFromDB = false,
            int commandTimeout = 0)
        {
            if (args == null || args.Length == 0)
                return;
            bool bAllOk = false;
            DB.Advanced DBAdvanced = new DB.Advanced(this.DB);

            System.Data.SqlClient.SqlCommand Command = null;
            System.Data.SqlClient.SqlDataAdapter selectDataAdapter = null;

            int RetryNumForDeadLock = 0;
            while (!bAllOk)
            {
                #region inicjalizacja komunikacji z baza
                try
                {
                    lock (this.DB)
                    {
                        //nastepna linia automatycznie rozpoczyna transakcje !!!
                        Command = DBAdvanced.GetCommand(false);
                        if (commandTimeout > 0)
                            Command.CommandTimeout = commandTimeout;
                #endregion
                        #region Pobranie schematu Tabelki z cache lub z bazy
                        DataTable dataTable = GetTableSchema(tableName);//Cache.TableSchema.Get(destinationTableName, this);

                        #endregion
                        #region Przepisanie wszystkich wierszy z wartosciami z data do DataTable

                        List<string> primaryKeysColumnsList = new List<string>();
                        if (primaryKeysFields != null && primaryKeysFields.Count > 0)
                        {
                            primaryKeysColumnsList = primaryKeysFields;
                        }
                        else
                        {
                            foreach (DataColumn dcItem in dataTable.Columns)
                            {
                                if (Convert.ToBoolean(dcItem.ExtendedProperties["IsKey"]))
                                    primaryKeysColumnsList.Add(dcItem.ColumnName);// = dataTable.PrimaryKey.Select(p => p.ColumnName).ToList();
                            }
                        }

                        // Małe wyjaśnienie "object[][] args"
                        // W naszych rozwiązaniach bazodanowych mamy czasem specyficzne rozwiąznia (zgodne lub niezabraniające z projektowaniem DB)
                        // Chodzi np o tabelki ACTION i ACTION_DATA gdzie mamy (nazwijmy to) na odwórt wskazanie ID. 
                        // I tak w ACTION mamy ID_ACTION_DATA wskazujący na zestaw ACTION_DATA czyli wiele wierszy ACTION_DATA z tym samym ID, bez klucza głównego i identity w ACTION_DATA
                        // W związku z powyższym potrzebujemy przy masowym wstawianiu:
                        // 1. pobrać ID_ACTION_DATA (następny/max id) i z takim ID wstawić zestaw wierszy do ACTION_DATA
                        // 2. robiąc masowy insert chcemy wstawić jak najwięcej wierszy za jednym razem a potrzebujemy przy tym wyznaczyć kolejne ID_ACTION_DATA dla każdego wstawianego zestawu ACTION_DATA
                        long idMax = 0;
                        if (getMaxIdFromDB && primaryKeysColumnsList.Count == 1)
                        {
                            #region Pobranie Max ID_[KEY] dla ktorego beda wstawiane dane
                            Command.CommandText = "SELECT MAX(tab.Id) FROM (" +
                                                  " SELECT MAX(" + primaryKeysColumnsList[0] + ") as 'Id' FROM " + tableName + " WITH (TABLOCKX HOLDLOCK)" +
                                                  ") as tab";
                            Command.CommandType = CommandType.Text;
                            Command.CommandTimeout = ShortTimeout;

                            object selectedMax = Command.ExecuteScalar();
                            if (selectedMax == DBNull.Value)
                            {
                                idMax = 1;
                            }
                            else
                            {
                                if (!long.TryParse(selectedMax.ToString(), out idMax))
                                {
                                    throw new Exception("Could not get Max ID from table " + tableName);
                                }
                                idMax++;
                            }
                            #endregion
                        }

                        for (int i = 0; i < args.Length; i++)
                        {
                            DataTable tempDataTable = dataTable.Clone();
                            if (fillDataTableMethod == null)
                                FillDataTable(args[i], ref tempDataTable, Enums.DataChange.Insert);
                            else
                                tempDataTable = fillDataTableMethod(args[i], tempDataTable, Enums.DataChange.Insert);

                            foreach (DataRow row in tempDataTable.Rows)
                            {
                                if (getMaxIdFromDB && primaryKeysColumnsList.Count == 1)
                                    row[primaryKeysColumnsList[0]] = idMax;
                                dataTable.Rows.Add(row.ItemArray);
                            }

                            if (getMaxIdFromDB && primaryKeysColumnsList.Count == 1)
                            {
                                Type argType = GetTableRowType(args[i][0]);
                                System.Reflection.FieldInfo fiItem = argType.GetField(primaryKeysColumnsList[0]);

                                for (long j = 0; j < args[i].LongLength; j++)
                                    fiItem.SetValue(args[i][j], (long)(idMax - args[i].LongLength + j + 1));

                                idMax++;
                            }
                        }
                        #endregion
                        #region BulkCopy - "masowe" wstawienie wierszy do DB
                        Command.CommandText = "SqlBulkCopy";
                        using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(Command.Connection, System.Data.SqlClient.SqlBulkCopyOptions.Default, Command.Transaction))
                        {
                            bulkCopy.BatchSize = 30000;
                            bulkCopy.BulkCopyTimeout = ShortTimeout;
                            bulkCopy.DestinationTableName = dataTable.TableName;
                            bulkCopy.WriteToServer(dataTable);
                        }
                        #endregion
                        if (primaryKeysColumnsList.Count == 1 && !getMaxIdFromDB)
                        {
                            // Dane w DataTable powinny być posrotowane wg. kluczy (np. w DB_SIM_CARD_DATA po IdSimCard, IdDataType, Index)
                            // ponieważ baza w ten w podobny sposób sortuje i klucze przypisywane w tym miejscu mogły zostać przypisane w złej kolejności
                            // niż w bazie a to następnie powodowało różne błędy, np. podczas UPDATE:
                            // Violation of PRIMARY KEY constraint 'xxxx'.
                            // Cannot insert duplicate key in object 'yyyy'. The duplicate key value is zzzz.

                            #region Pobranie ostatniego wstawionego IDENTITY
                            Command.CommandText = string.Format("SELECT ISNULL(MAX({0}),0) FROM [{1}]", primaryKeysColumnsList[0], dataTable.TableName);
                            Command.CommandType = CommandType.Text;
                            long identity = long.Parse(Command.ExecuteScalar().ToString());

                            #endregion
                            #region Aktualizacja ID(identity) dla kazdego wiersza w data
                            for (int i = 0; i < args.Length; i++)
                            {
                                Type argType = GetTableRowType(args[i][0]);
                                System.Reflection.FieldInfo fiItem = argType.GetField(primaryKeysColumnsList[0]);

                                for (long j = 0; j < args[i].LongLength; j++)
                                {
                                    fiItem.SetValue(args[i][j], (long)(identity - args[i].LongLength + j + 1));
                                }
                            }
                            #endregion
                        }
                        #region zakonczenie komunikacji z baza
                        bAllOk = true;
                    }
                }
                catch (System.Data.SqlClient.SqlException Exception)
                {
                    bool bThrow = true;
                    #region If Deadlocked
                    if (DBAdvanced.Deadlocked(Exception))
                    {
                        if ((++RetryNumForDeadLock > this.DB.RetryCountForDeadLock))
                            bThrow = true;
                        else
                            bThrow = false;
                    }
                    #endregion
                    if (bThrow)
                    {
                        string CommandText = "";
                        string ParametersText = "";

                        if (Command != null)
                            DBAdvanced.CreateCommandDump(Command, out CommandText, out ParametersText);

                        DBAdvanced.Rollback();

                        if (Command != null)
                            DBAdvanced.LogException(Command, Exception, CommandText, ParametersText);

                        throw;
                    }
                }
                finally
                {
                    if (selectDataAdapter != null)
                        selectDataAdapter.Dispose();

                    if (bAllOk)
                        DBAdvanced.Commit();
                    else
                        DBAdvanced.Rollback();
                }
                        #endregion
            }
        }
        #endregion

        #region BulkUpdate
        protected void BulkUpdate(string tableName, object[] args, List<string> primaryKeysFields = null,
            Func<object[], DataTable, Enums.DataChange, DataTable> FillDataTableMethod = null,
            List<string> updateSetSkipColumns = null,
            int commandTimeout = 0)
        {
            if (args == null || args.Count() == 0)
                return;

            bool bAllOk = false;
            DB.Advanced DBAdvanced = new DB.Advanced(this.DB);

            System.Data.SqlClient.SqlCommand Command = null;
            System.Data.SqlClient.SqlDataAdapter updateDataAdapter = null;

            int RetryNumForDeadLock = 0;
            while (!bAllOk)
            {
                #region inicjalizacja komunikacji z baza
                try
                {
                    lock (this.DB)
                    {
                        //nastepna linia automatycznie rozpoczyna transakcje !!!
                        Command = DBAdvanced.GetCommand();
                        if (commandTimeout > 0)
                            Command.CommandTimeout = commandTimeout;
                #endregion
                        #region Pobranie schematu Tabelki z cache lub z bazy
                        DataTable dataTable = GetTableSchema(tableName);

                        List<string> primaryKeysColumnsList = null;

                        if (primaryKeysFields != null && primaryKeysFields.Count > 0)
                            primaryKeysColumnsList = primaryKeysFields;
                        else if (dataTable.PrimaryKey == null || dataTable.PrimaryKey.Count() == 0)
                        {
                            primaryKeysColumnsList = new List<string>();
                            foreach (DataColumn dcItem in dataTable.Columns)
                            {
                                if (Convert.ToBoolean(dcItem.ExtendedProperties["IsKey"]))
                                    primaryKeysColumnsList.Add(dcItem.ColumnName);// = dataTable.PrimaryKey.Select(p => p.ColumnName).ToList();
                            }
                        }

                        if (primaryKeysColumnsList == null || primaryKeysColumnsList.Count() == 0)
                            throw new Exception(String.Format("Table {0} without primary keys", dataTable.TableName));
                        #endregion
                        if (primaryKeysColumnsList.Count < dataTable.Columns.Count)
                        {
                            #region Przepisanie kolumn ktore beda aktualizowanez data do DataTable

                            List<string> otherColumsList = new List<string>();
                            foreach (DataColumn dcItem in dataTable.Columns)
                            {
                                if (!primaryKeysColumnsList.Contains(dcItem.ColumnName) && (updateSetSkipColumns == null || !updateSetSkipColumns.Contains(dcItem.ColumnName)))
                                    otherColumsList.Add(dcItem.ColumnName);
                            }
                            string updateSet = String.Join(", ", otherColumsList.Select(c => String.Format("{0} = @{1}", c, c)));
                            string updateWhere = String.Join(" and ", primaryKeysColumnsList.Select(c => String.Format("{0} = @{1}", c, c)));

                            string updateText = String.Format("UPDATE {0} set {1} where {2}", dataTable.TableName, updateSet, updateWhere);

                            if (FillDataTableMethod == null)
                                FillDataTable(args, ref dataTable, Enums.DataChange.Update);
                            else
                                dataTable = FillDataTableMethod(args, dataTable, Enums.DataChange.Update);
                            #endregion
                            #region "Masowy" update wartosci dla poszczegolnych kolumn
                            Command.CommandText = updateText;
                            updateDataAdapter = new System.Data.SqlClient.SqlDataAdapter();
                            updateDataAdapter.UpdateCommand = new System.Data.SqlClient.SqlCommand(Command.CommandText, Command.Connection, Command.Transaction);

                            foreach (DataColumn dcItem in dataTable.Columns)
                            {
                                //if (!otherColumsList.Contains(dcItem.ColumnName))
                                //    continue;
                                SqlDbType sqlDbType = (SqlDbType)Convert.ToInt32(dcItem.ExtendedProperties["ProviderType"]);
                                int columnSize = Convert.ToInt32(dcItem.ExtendedProperties["ColumnSize"]);
                                updateDataAdapter.UpdateCommand.Parameters.Add("@" + dcItem.ColumnName, sqlDbType, columnSize, dcItem.ColumnName);
                            }
                            updateDataAdapter.Update(dataTable);
                            #endregion
                        }
                        #region zakonczenie komunikacji z baza
                        bAllOk = true;
                    }
                }
                catch (System.Data.SqlClient.SqlException Exception)
                {
                    bool bThrow = true;
                    #region If Deadlocked
                    if (DBAdvanced.Deadlocked(Exception))
                    {
                        if ((++RetryNumForDeadLock > this.DB.RetryCountForDeadLock))
                            bThrow = true;
                        else
                            bThrow = false;
                    }
                    #endregion
                    if (bThrow)
                    {
                        string CommandText = "";
                        string ParametersText = "";

                        if (Command != null)
                            DBAdvanced.CreateCommandDump(Command, out CommandText, out ParametersText);

                        DBAdvanced.Rollback();

                        if (Command != null)
                            DBAdvanced.LogException(Command, Exception, CommandText, ParametersText);

                        throw;
                    }
                    System.Threading.Thread.Sleep(this.DB.RetrySleepTimeForDeadlock(RetryNumForDeadLock));
                }
                finally
                {
                    if (updateDataAdapter != null)
                        updateDataAdapter.Dispose();

                    if (bAllOk)
                        DBAdvanced.Commit();
                    else
                        DBAdvanced.Rollback();
                }
                        #endregion
            }
        }
        #endregion

        #endregion

        #region SendMail

        public void SendMail(string Recipients, string Sender, string Subject, string TextBody, Boolean IsAttachment,
            string FileURL, string FileName, string ProfileName)
        {
            DB.InParameter paramRecipients = new DB.InParameter("@RECIPIENTS", Recipients);
            DB.InParameter paramSender = new DB.InParameter("@SENDER", Sender);
            DB.InParameter paramSubject = new DB.InParameter("@SUBJECT", Subject);
            DB.InParameter paramTextBody = new DB.InParameter("@TEXT_BODY", TextBody);
            DB.InParameter paramIsAttachment = new DB.InParameter("@IS_ATTACHMENT", IsAttachment);
            DB.InParameter paramFileURL = new DB.InParameter("@FILE_URL", FileURL);
            DB.InParameter paramFileName = new DB.InParameter("@FILE_NAME", FileName);
            DB.InParameter paramProfileName = new DB.InParameter("@PROFILE_NAME", ProfileName);

            DB.ExecuteNonQueryProcedure(
                    "sp_SendMail",
                    new DB.Parameter[]
				{
				    paramRecipients,
                    paramSender,
                    paramSubject,
                    paramTextBody,
                    paramIsAttachment,
                    paramFileURL,
                    paramFileName,
                    paramProfileName
                },
                this.ShortTimeout
            );
        }


        /// <summary>
        /// Próbuje wysłać email z załącznikami (jeśli takie dodano do FILE_IDS).
        /// W przypadku braku uprawnień wysyłany jest email z dopisanym powiadomieniem o sytuacji.
        /// </summary>
        /// <param name="Recipients"></param>
        /// <param name="Sender"></param>
        /// <param name="ProfileName"></param>
        /// <param name="Subject"></param>
        /// <param name="TextBody"></param>
        /// <param name="IsAttachment"></param>
        /// <param name="IdFiles">Id plików w bazie które będą załącznikami</param>
        public void SendMail(string Recipients, string Sender, string ProfileName, string Subject, string TextBody, Boolean IsAttachment,
            long[] IdFiles)
        {
            DB.InParameter paramRecipients = new DB.InParameter("@RECIPIENTS", Recipients);
            DB.InParameter paramSender = new DB.InParameter("@SENDER", Sender);
            DB.InParameter paramSubject = new DB.InParameter("@SUBJECT", Subject);
            DB.InParameter paramTextBody = new DB.InParameter("@TEXT_BODY", TextBody);
            DB.InParameter paramIsAttachment = new DB.InParameter("@IS_ATTACHMENT", IsAttachment);
            DB.InParameter paramIdFiles = CreateTableParam("@FILE_IDS", IdFiles);
            DB.InParameter paramProfileName = new DB.InParameter("@PROFILE_NAME", ProfileName);

            DB.ExecuteNonQueryProcedure(
                    "sp_SendEmailExtended",
                    new DB.Parameter[]
				{
				    paramRecipients,
                    paramSender,
                    paramSubject,
                    paramTextBody,
                    paramIsAttachment,
                    paramIdFiles,
                    paramProfileName
                },
                this.ShortTimeout
            );
        }

        #endregion

        #region GetValue
        public object GetValue(object objectValue)
        {
            if (objectValue is DBNull)
                return null;
            else
                return objectValue;
        }

        public T GetValue<T>(object objectValue) where T : IConvertible
        {
            if (objectValue == null || objectValue == DBNull.Value)
                return default(T);
            else
                return GenericConverter.Parse<T>(objectValue);
        }

        public Nullable<T> GetNullableValue<T>(object objectValue) where T : struct, IConvertible
        {
            if (objectValue == null || objectValue == DBNull.Value)
                return null;
            else
                return GenericConverter.Parse<T>(objectValue);
        }
        #endregion

        #region ParamObject
        public DB.InParameter ParamObject(string name, object ToBeConverted, long? IdDataType, int IdDataTypeClass)
        {
            if (ToBeConverted == null)
                return new DB.InNullParameter(name);

            try
            {
                switch ((Enums.DataTypeClass)IdDataTypeClass)
                {
                    case Enums.DataTypeClass.Integer:
                        try
                        {
                            if (ToBeConverted is long || ToBeConverted is ulong)
                                return new DB.InParameter(name, SqlDbType.BigInt, ToBeConverted);
                            else
                                return new DB.InParameter(name, SqlDbType.Int, Convert.ToInt32(ToBeConverted));
                        }
                        catch (OverflowException)
                        {
                            return new DB.InParameter(name, SqlDbType.BigInt, Convert.ToInt64(ToBeConverted));
                        }
                    case Enums.DataTypeClass.Real:
                        try
                        {
                            return new DB.InParameter(name, SqlDbType.Float, Convert.ToDouble(ToBeConverted));
                        }
                        catch (FormatException)
                        {
                            return new DB.InParameter(name, SqlDbType.Float, Convert.ToDouble(ToBeConverted, CultureInfo.InvariantCulture));
                        }
                    case Enums.DataTypeClass.Decimal:
                        try
                        {
                            return new DB.InParameter(name, SqlDbType.Decimal, Convert.ToDecimal(ToBeConverted));
                        }
                        catch (FormatException)
                        {
                            return new DB.InParameter(name, SqlDbType.Decimal, Convert.ToDecimal(ToBeConverted, CultureInfo.InvariantCulture));
                        }
                    case Enums.DataTypeClass.Text:
                        return new DB.InParameter(name, SqlDbType.NVarChar, ToBeConverted.ToString());
                    case Enums.DataTypeClass.Date:
                        try
                        {
                            return new DB.InParameter(name, SqlDbType.Date, Convert.ToDateTime(ToBeConverted).Date);
                        }
                        catch (FormatException)
                        {
                            return new DB.InParameter(name, SqlDbType.Date, Convert.ToDateTime(ToBeConverted, CultureInfo.InvariantCulture).Date);
                        }
                    case Enums.DataTypeClass.Time:
                        if (ToBeConverted is TimeSpan || ToBeConverted is TimeSpan?)
                            return new DB.InParameter(name, SqlDbType.Time, ToBeConverted);
                        else
                            try
                            {
                                return new DB.InParameter(name, SqlDbType.Time, TimeSpan.Parse(ToBeConverted.ToString()));
                            }
                            catch (FormatException)
                            {
                                return new DB.InParameter(name, SqlDbType.Time, TimeSpan.Parse(ToBeConverted.ToString(), CultureInfo.InvariantCulture));
                            }
                    case Enums.DataTypeClass.Datetime:
                        try
                        {
                            return new DB.InParameter(name, SqlDbType.DateTime, Convert.ToDateTime(ToBeConverted));
                        }
                        catch (FormatException)
                        {
                            return new DB.InParameter(name, SqlDbType.DateTime, Convert.ToDateTime(ToBeConverted, CultureInfo.InvariantCulture));
                        }
                    case Enums.DataTypeClass.Boolean:
                        return new DB.InParameter(name, SqlDbType.Bit, Convert.ToBoolean(ToBeConverted));
                    case Enums.DataTypeClass.Object:     //fall to: ParamObject(name, ToBeConverted);
                        break;//return new DB.InParameter(name, SqlDbType. , ToBeConverted);
                    case Enums.DataTypeClass.Xml:   //Xml type not compatible with sql_variant
                        return new DB.InParameter(name, SqlDbType.NVarChar, ToBeConverted.ToString());
                    case Enums.DataTypeClass.Color:    //fall to: ParamObject(name, ToBeConverted);
                        break;//return new DB.InParameter(name, SqlDbType. , ToBeConverted);
                    case Enums.DataTypeClass.Binary:
                        return new DB.InParameter(name, SqlDbType.Binary, Convert.ChangeType(ToBeConverted, typeof(byte[])));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DB.ErrorCastingData, IdDataType, IdDataTypeClass, ToBeConverted.ToString(), ex.Message);
                throw new Exception(ex.ToString() + Environment.NewLine + Environment.NewLine + "IdDataType: " + IdDataType.ToString() +
                              Environment.NewLine + "IdDataTypeClass: " + IdDataTypeClass.ToString() +
                              Environment.NewLine + "Value: " + (ToBeConverted == null ? "[NULL]" : ToBeConverted.ToString()));
                throw ex;
            }

            //if unable to convert based on DataTypeClass
            return ParamObject(name, ToBeConverted);
        }

        public DB.InParameter ParamObject(string name, object ToBeConverted)
        {
            if (ToBeConverted == null)
            {
                return new DB.InNullParameter(name);
            }
            if (ToBeConverted is int || ToBeConverted is int? || ToBeConverted is uint || ToBeConverted is uint? ||
                ToBeConverted is short || ToBeConverted is short? || ToBeConverted is ushort || ToBeConverted is ushort? ||
                ToBeConverted is byte || ToBeConverted is byte? || ToBeConverted is sbyte || ToBeConverted is sbyte?)
            {
                return new DB.InParameter(name, SqlDbType.Int, ToBeConverted);
            }
            if (ToBeConverted is long || ToBeConverted is long? || ToBeConverted is ulong || ToBeConverted is ulong?)
            {
                return new DB.InParameter(name, SqlDbType.BigInt, ToBeConverted);
            }
            if (ToBeConverted is DateTime || ToBeConverted is DateTime?)
            {
                return new DB.InParameter(name, SqlDbType.DateTime, ToBeConverted);
            }
            if (ToBeConverted is string)
            {
                return new DB.InParameter(name, SqlDbType.NVarChar, ToBeConverted);
            }

            //Added by AF 25-07-2011:
            if (ToBeConverted is bool || ToBeConverted is bool?)
            {
                return new DB.InParameter(name, SqlDbType.Bit, ToBeConverted);
            }
            if (ToBeConverted is Double || ToBeConverted is Double?)
            {
                return new DB.InParameter(name, SqlDbType.Float, ToBeConverted);
            }
            if (ToBeConverted is byte[])
            {
                return new DB.InParameter(name, SqlDbType.VarBinary, ToBeConverted);    //zmiana z SqlDbType.Image 02-03-2012
            }

            //Added by SW 24-11-2011
            if (ToBeConverted is decimal || ToBeConverted is decimal? || ToBeConverted is float || ToBeConverted is float?)
            {
                return new DB.InParameter(name, SqlDbType.Float, ToBeConverted);
            }
            if (ToBeConverted is char || ToBeConverted is char?)
            {
                return new DB.InParameter(name, SqlDbType.Char, ToBeConverted);
            }

            return new DB.InParameter(name, ToBeConverted.ToString());
        }

        public object ParamObject(object ToBeConverted, long? IdDataType, int IdDataTypeClass)
        {
            if (ToBeConverted == null)
                return null;

            try
            {
                switch ((Enums.DataTypeClass)IdDataTypeClass)
                {
                    case Enums.DataTypeClass.Integer:
                        try
                        {
                            if (ToBeConverted is long || ToBeConverted is ulong)
                                return ToBeConverted;
                            else
                                return Convert.ToInt32(ToBeConverted);
                        }
                        catch (OverflowException)
                        {
                            return Convert.ToInt64(ToBeConverted);
                        }
                    case Enums.DataTypeClass.Real:
                        try
                        {
                            return Convert.ToDouble(ToBeConverted);
                        }
                        catch (FormatException)
                        {
                            return Convert.ToDouble(ToBeConverted, System.Globalization.CultureInfo.InvariantCulture);
                        }
                    case Enums.DataTypeClass.Decimal:
                        try
                        {
                            return Convert.ToDecimal(ToBeConverted);
                        }
                        catch (FormatException)
                        {
                            return Convert.ToDecimal(ToBeConverted, System.Globalization.CultureInfo.InvariantCulture);
                        }
                    case Enums.DataTypeClass.Text:
                        return ToBeConverted.ToString();
                    case Enums.DataTypeClass.Date:
                        try
                        {
                            return Convert.ToDateTime(ToBeConverted).Date;
                        }
                        catch (FormatException)
                        {
                            return Convert.ToDateTime(ToBeConverted, System.Globalization.CultureInfo.InvariantCulture).Date;
                        }
                    case Enums.DataTypeClass.Time:
                        if (ToBeConverted is TimeSpan || ToBeConverted is TimeSpan?)
                            return ToBeConverted;
                        else
                            try
                            {
                                return TimeSpan.Parse(ToBeConverted.ToString());
                            }
                            catch (FormatException)
                            {
                                return TimeSpan.Parse(ToBeConverted.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                            }
                    case Enums.DataTypeClass.Datetime:
                        try
                        {
                            return Convert.ToDateTime(ToBeConverted);
                        }
                        catch (FormatException)
                        {
                            return Convert.ToDateTime(ToBeConverted, System.Globalization.CultureInfo.InvariantCulture);
                        }
                    case Enums.DataTypeClass.Boolean:
                        return Convert.ToBoolean(ToBeConverted);
                    case Enums.DataTypeClass.Object:     //fall to: ParamObject(name, ToBeConverted);
                        break;//return new DB.InParameter(name, SqlDbType. , ToBeConverted);
                    case Enums.DataTypeClass.Xml:   //Xml type not compatible with sql_variant
                        return ToBeConverted.ToString();
                    case Enums.DataTypeClass.Color:    //fall to: ParamObject(name, ToBeConverted);
                        break;//return new DB.InParameter(name, SqlDbType. , ToBeConverted);
                    case Enums.DataTypeClass.Binary:
                        return Convert.ChangeType(ToBeConverted, typeof(byte[]));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DB.ErrorCastingData, IdDataType, IdDataTypeClass, ToBeConverted.ToString(), ex.Message);
                throw new Exception(ex.ToString() + Environment.NewLine + Environment.NewLine + "IdDataType: " + IdDataType.ToString() +
                              Environment.NewLine + "IdDataTypeClass: " + IdDataTypeClass.ToString());
                throw ex;
            }

            //if unable to convert based on DataTypeClass
            return ToBeConverted;
        }
        #endregion

        #region IsParameterDefined
        public bool IsParameterDefined(string storedProcName, string paramName, DBCommon db)
        {
            if (String.IsNullOrWhiteSpace(storedProcName) || String.IsNullOrWhiteSpace(paramName) || db == null) return false;

            return Cache.StoredProcSchema.GetParameterDict(storedProcName, db).ContainsKey(paramName);
        }
        #endregion
        #region StoredProcedureExists
        public bool StoredProcedureExists(string storedProcName)
        {
            if (String.IsNullOrWhiteSpace(storedProcName)) return false;

            DataTable dataTable = Cache.StoredProcSchema.Get(storedProcName, this);
            return dataTable != null && dataTable.Rows.Count > 0;
        }
        #endregion

        #region PrepareSqlDataValueClause
        public string PrepareDataValue(object value, DB_DATA_TYPE dataType)
        {
            string retVal = "null";
            if (value == null)
                return retVal;
            try
            {
                switch (dataType.ID_DATA_TYPE_CLASS)
                {
                    case (int)Enums.DataTypeClass.Integer:
                    //Do obsłużenia ,. w zależności od kultury serwera
                    case (int)Enums.DataTypeClass.Real:
                    case (int)Enums.DataTypeClass.Decimal:
                        retVal = value.ToString().Trim().Replace(",", ".");
                        break;
                    case (int)Enums.DataTypeClass.Datetime:
                        retVal = "'" + Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss.fff").Replace("'", "''") + "'"; // Żeby uniknać niepoprawnych SQL zamieniamy ' na '' w wartości
                        break;
                    case (int)Enums.DataTypeClass.Date:
                    case (int)Enums.DataTypeClass.Time:
                        retVal = "'" + value.ToString().Replace("'", "''") + "'"; // Żeby uniknać niepoprawnych SQL zamieniamy ' na '' w wartości
                        break;
                    case (int)Enums.DataTypeClass.Text:
                        retVal = "N'" + value.ToString().Replace("'", "''") + "'"; // Żeby uniknać niepoprawnych SQL zamieniamy ' na '' w wartości
                        break;
                    case (int)Enums.DataTypeClass.Boolean:
                        retVal = "cast(" + Convert.ToInt32(value) + " as bit)";
                        break;
                    case (int)Enums.DataTypeClass.Binary:
                        retVal = String.Format("0x{0}", BitConverter.ToString(value as byte[]).Replace("-", "").ToLower());
                        break;
                    case (int)Enums.DataTypeClass.Object:
                        {
                            if (value is sbyte || value is byte || value is short || value is ushort || value is int || value is uint || value is long || value is ulong)
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Integer });
                            }
                            else if (value is float || value is double || value is decimal)
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Decimal });
                            }
                            else if (value is DateTime || value is TimeSpan)
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Datetime });
                            }
                            else if (value is string)
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Text });
                            }
                            else if (value is bool)
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Boolean });
                            }
                            else if (value is byte[])
                            {
                                retVal = PrepareDataValue(value, new DB_DATA_TYPE() { ID_DATA_TYPE_CLASS = (int)Enums.DataTypeClass.Binary });
                            }
                            else
                            {
                                retVal = "null";
                                //throw new Exception("Unsupported object type for Data Type class Object: " + value == null ? "<null>" : value.GetType().Name);
                            }

                            break;
                        }
                    default:
                        throw new Exception("Unsupported Data Type Class");
                        break;
                }
            }catch(Exception ex)
            {
                throw new Exception(String.Format("Exception occured during preparing data value. Value: {0}, IdDataType: {1}, IdDataTypeClass: {2}. See inner exception for more details",
                    value, dataType == null ? "<null>" : dataType.ID_DATA_TYPE.ToString(), dataType == null ? "<null>" : dataType.ID_DATA_TYPE_CLASS.ToString()), ex);
            }
            return retVal;
        }

        #endregion
    }
}
