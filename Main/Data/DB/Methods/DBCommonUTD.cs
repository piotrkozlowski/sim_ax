﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    public class DBCommonUTD : DBCommon
    {
        #region Ctor
        public DBCommonUTD(string ServerName)
            : base(ServerName)
        {
        }

        public DBCommonUTD(string ServerName, int ShortTimeout, int LongTimeout)
            : base(ServerName, ShortTimeout, LongTimeout)
        {
        }

        public DBCommonUTD(string server, string db, string userEncrypted, string passwordEncrypted, int ShortTimeout, int LongTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, ShortTimeout, LongTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        #endregion

        #region GetPackageTypes
        public UTDPackageType[] GetPackageTypes(int LanguageID, bool visibleForCustomer, bool visibleForConsultant)
        {
            return (UTDPackageType[])DB.ExecuteProcedure(
                "tm_GetPackageTypesForOrders",
                new DB.AnalyzeDataSet(GetPackageTypes),
                new DB.Parameter[] { 
                    new DB.InParameter("@LanguageID", LanguageID),
                    new DB.InParameter("@VisibleForCustomer", visibleForCustomer),
                    new DB.InParameter("@VisibleForConsultant", visibleForConsultant)
                }
            );
        }

        private object GetPackageTypes(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            UTDPackageType[] list = new UTDPackageType[count];
            for (int i = 0; i < count; i++)
            {
                UTDPackageType insert = new UTDPackageType();
                insert.ID_PACKAGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_TYPE"]);
            //    insert.STANDARD_DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["STANDARD_DESCRIPTION"]);
             //   insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
             //   insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
            //    insert.PHOTO_LINK = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHOTO_LINK"]);
            //    insert.PRICE = GetValue<double>(QueryResult.Tables[0].Rows[i]["PRICE"]);
            //    insert.VISIBLE_FOR_CUSTOMER = GetValue<bool>(QueryResult.Tables[0].Rows[i]["VISIBLE_FOR_CUSTOMER"]);
            //    insert.VISIBLE_FOR_CONSULTANT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["VISIBLE_FOR_CONSULTANT"]);
            //    insert.CURRENCY = GetValue<string>(QueryResult.Tables[0].Rows[i]["CURRENCY"]);
                
                list[i] = insert;
            }
            return list;
        }
        #endregion      

        #region GetPackageDataTypes

        public UTDPackageType[] GetPackageDataTypes(int LanguageID, int[] idPackageList)
        {
            try
            {
                DataTable idPackageListTable = new DataTable();
                idPackageListTable.Columns.Add("idPackage", System.Type.GetType("System.Int32"));

                if (idPackageList != null)
                {
                    for (int i = 0; i < idPackageList.Length; i++)
                    {
                        idPackageListTable.Rows.Add(idPackageList[i]);
                    }
                }



                return (UTDPackageType[])DB.ExecuteProcedure(
                    "tm_GetPackageDataTypes",
                    new DB.AnalyzeDataSet(GetPackageDataTypes),
                    new DB.Parameter[] { 
                    new DB.InParameter("@IdLanguage", LanguageID),
                    new DB.InParameter("@IdPackageList",SqlDbType.Structured, idPackageListTable)
                }
                );
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private object GetPackageDataTypes(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            UTDPackageType[] list = new UTDPackageType[count];
            for (int i = 0; i < count; i++)
            {
                UTDPackageType insert = new UTDPackageType();
                insert.ID_PACKAGE_TYPE_DATA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_TYPE_DATA"]);
                insert.ID_PACKAGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue<string>(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region GetIdPackageTypeByIdPackageTypeData

        public UTDPackageType[] GetIdPackageTypeByIdPackageTypeData(int LanguageID, int idPackageTypeData)
        {

            return (UTDPackageType[])DB.ExecuteProcedure(
                    "tm_GetIdPackageTypeByIdPackageTypeData",
                    new DB.AnalyzeDataSet(GetIdPackageTypeByIdPackageTypeData),
                    new DB.Parameter[] { 
                    new DB.InParameter("@IdLanguage", LanguageID),
					new DB.InParameter("@IdPackageTypeData", idPackageTypeData)
                }
                );
        }

        private object GetIdPackageTypeByIdPackageTypeData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            UTDPackageType[] list = new UTDPackageType[count];
            for (int i = 0; i < count; i++)
            {
                UTDPackageType insert = new UTDPackageType();
                insert.ID_PACKAGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_TYPE"]);
                insert.VALUE = GetValue<string>(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion
    }
}
