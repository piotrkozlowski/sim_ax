﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    public class DBCommonArticle : DBCommon
    {
        #region Ctor
        public DBCommonArticle(string ServerName)
            : base(ServerName)
        {
        }

        public DBCommonArticle(string ServerName, int ShortTimeout, int LongTimeout)
            : base(ServerName, ShortTimeout, LongTimeout)
        {
        }

        public DBCommonArticle(string server, string db, string userEncrypted, string passwordEncrypted, int ShortTimeout, int LongTimeout)
			: base(server, db, userEncrypted, passwordEncrypted, ShortTimeout, LongTimeout, TimeSpan.FromSeconds(5), 2, TimeSpan.FromSeconds(5))
        {
        }
		#endregion

        #region GetArticles
        public SPArticle[] GetArticles()
        {
            //DATEDIFF(day, '12/28/1800',GETDATE())
            //set @CDN_MagSesja = 105481
            //set @CDN_MagWatek = 4
            //set @CDN_Filtr
            //procedura przygotowana przez A.Tramś, nie pytajcie dlaczego te parametry maja takie wartosci :)

            return (SPArticle[])DB.ExecuteProcedure(
                "[CDN].[at_StanMagazynuNaDzien]",
                GetArticles,
                new DB.Parameter[] { 
                    new DB.InParameter("@Data", (int)(DateTime.Now - new DateTime(1800, 12,28)).TotalDays),
                    new DB.InParameter("@CDN_MagSesja", 105481),
                    new DB.InParameter("@CDN_MagWatek", 4),
                    new DB.InParameter("@CDN_Filtr", (string)null) 
                }
            );
        }

        private object GetArticles(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SPArticle[] list = new SPArticle[count];
            for (int i = 0; i < count; i++)
            {
                SPArticle insert = new SPArticle();
                insert.Twr_GIDNumer = GetValue<int>(QueryResult.Tables[0].Rows[i]["Twr_GIDNumer"]);
                insert.KodTowaru = GetValue<string>(QueryResult.Tables[0].Rows[i]["KodTowaru"]);
                insert.NazwaTowaru = GetValue<string>(QueryResult.Tables[0].Rows[i]["NazwaTowaru"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion
    }
}
