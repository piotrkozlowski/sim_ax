﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.Data.DB.Objects.DW.UserPart;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDW : DBCommon
    {
        #region Constructor
        public DBCommonDW(string serverName)
            : base(serverName)
        {
        }
        public DBCommonDW(string serverName, int shortTimeout, int longTimeout)
            : base(serverName, shortTimeout, longTimeout)
        {
        }
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, TimeSpan? timeZoneOffset, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, timeZoneOffset, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime)
        {
        }
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, null)
        {
        }
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, operatorLogin)
        {
        }
        public DBCommonDW(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonDW(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5), operatorLogin)
        {
        }
        public DBCommonDW(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5), useTrustedConnection, operatorLogin)
        {
        }

        //TS: specjalny jałowy konstruktor aby można przekazać informacje o trusted connection w projektach gdzie są wykorzystywane Data/DB oraz Common/DB jednocześnie. Do wywalenia jak będzie zrobiony merge tych projektów
        public DBCommonDW(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, Enums.Language.English, null, shortTimeout, longTimeout, TimeSpan.Zero, 0, TimeSpan.Zero, useTrustedConnection, TimeZoneId: "UTC")
        {
        }
        #endregion

        #region Prepaid functions

        public DW.DB_DATA_ARCH[] GetPrepaidData(long SerialNbr)
        {
            return (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrpp_GetPrepaidData",
                new DB.AnalyzeDataSet(GetPrepaidData),
                new DB.Parameter[] 
                { 
                    new DB.InParameter("@SERIAL_NBR", SerialNbr)
                }
            );
        }

        public object GetPrepaidData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_ARCH[] list = new DB_DATA_ARCH[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_ARCH insert = new DB_DATA_ARCH();
                insert.ID_DATA_ARCH = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REPORT_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);

                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);

                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);

                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table ACTION_DATA_SOURCE

        private object GetActionDataSource(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_DATA_SOURCE[] list = new DB_ACTION_DATA_SOURCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_DATA_SOURCE insert = new DB_ACTION_DATA_SOURCE();
                insert.ID_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.ID_DATA_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table CONSUMER_TRANSATION
        public int SaveConsumerTransaction(DB_CONSUMER_TRANSACTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TRANSACTION", ToBeSaved.ID_CONSUMER_TRANSACTION);
            DB.InOutParameter BALANCE_PREPAID = new DB.InOutParameter("@BALANCE_PREPAID", ToBeSaved.BALANCE_PREPAID);
            DB.InOutParameter BALANCE_EC = new DB.InOutParameter("@BALANCE_EC", ToBeSaved.BALANCE_EC);
            DB.InOutParameter BALANCE_OWED = new DB.InOutParameter("@BALANCE_OWED", ToBeSaved.BALANCE_OWED);

            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveConsumerTransaction",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
					,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
					,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
					,new DB.InParameter("@ID_TRANSACTION_TYPE", ToBeSaved.ID_TRANSACTION_TYPE)
					,new DB.InParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF)
					,new DB.InParameter("@TIMESTAMP", ConvertTimeZoneToUtcTime(ToBeSaved.TIMESTAMP))
					,new DB.InParameter("@CHARGE_PREPAID", ToBeSaved.CHARGE_PREPAID)
                    ,new DB.InParameter("@CHARGE_EC", ToBeSaved.CHARGE_EC)
                    ,new DB.InParameter("@CHARGE_OWED", ToBeSaved.CHARGE_OWED)
					,BALANCE_PREPAID
					,BALANCE_EC
                    ,BALANCE_OWED
                    ,new DB.InParameter("@METER_INDEX", ToBeSaved.METER_INDEX)
                    ,new DB.InParameter("@ID_CONSUMER_SETTLEMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT)
                    ,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
                    ,new DB.InParameter("@TRANSACTION_GUID", ToBeSaved.TRANSACTION_GUID)
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TRANSACTION = (int)InsertId.Value;

            if (!BALANCE_PREPAID.IsNull)
                ToBeSaved.BALANCE_PREPAID = (double)BALANCE_PREPAID.Value;

            if (!BALANCE_EC.IsNull)
                ToBeSaved.BALANCE_EC = (double)BALANCE_EC.Value;

            if (!BALANCE_OWED.IsNull)
                ToBeSaved.BALANCE_OWED = (double)BALANCE_OWED.Value;

            return (int)InsertId.Value;
        }

        public DB_CONSUMER_TRANSACTION[] GetLastConsumerTransaction(int ID_CONSUMER, int? ID_TRANSACTION_TYPE)
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_u_GetLastConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_CONSUMER", ID_CONSUMER),
                    new DB.InParameter("@ID_TRANSACTION_TYPE", ID_TRANSACTION_TYPE)
				}
            );
        }

        public DB_CONSUMER_TRANSACTION[] GetFirstConsumerTransaction(int ID_CONSUMER, int? ID_TRANSACTION_TYPE, DateTime? Timestamp)
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_u_GetFirstConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_CONSUMER", ID_CONSUMER),
                    new DB.InParameter("@ID_TRANSACTION_TYPE", ID_TRANSACTION_TYPE),
                    new DB.InParameter("@TIMESTAMP", ConvertTimeZoneToUtcTime(Timestamp))
				}

            );
        }
        #endregion

        #region Table DATA
        private object GetData(DataSet QueryResult)
        {
            DW.DB_DATA[] list = QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_DATA()
            {
                ID_DATA = GetValue<long>(row["ID_DATA"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                ID_DATA_SOURCE = GetNullableValue<long>(row["ID_DATA_SOURCE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                STATUS = GetNullableValue<int>(row["STATUS"]),
            }).ToArray();

            return list;
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_DATA[] list = new DW.DB_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_DATA insert = new DW.DB_DATA();
                insert.ID_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.ID_DATA_SOURCE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                insert.STATUS = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        private object GetDataDw(DataSet QueryResult)
        {
            DB_DATA_DW[] list = QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_DW()
            {
                ID_DATA = GetValue<long>(row["ID_DATA"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                ID_DATA_SOURCE = GetNullableValue<long>(row["ID_DATA_SOURCE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                STATUS = GetNullableValue<int>(row["STATUS"]),
            }).ToArray();

            return list;
        }

        public long SaveData(DW.DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA", ToBeSaved.ID_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE)
														,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
														,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteData(DW.DB_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelData",
                new DB.Parameter[] 
                {
                    new DB.InParameter("@ID_DATA", toBeDeleted.ID_DATA)
                }
            );

        }

        public DW.DB_DATA[] GetCurrentDataForDistributor(int[] IdDistributor, int[] IdDeviceType, int[] IdDeviceStateType, long[] IdDataType, int[] IndexNbr)
        {
            return (DW.DB_DATA[])DB.ExecuteProcedure(
                "imrse_u_GetCurrentDataForDistributor",
                new DB.AnalyzeDataSet(GetData),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
			        CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
			        CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
			        CreateTableParam("@ID_DATA_TYPE", IdDataType),
			        CreateTableParam("@INDEX_NBR", IndexNbr)});
        }

        #region GetDataFilter
        public DW.DB_DATA[] GetDataFilter(long[] IdData, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                            TypeDateTimeCode Time, long[] IdDataSource, int[] IdDataSourceType, int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            DW.DB_DATA[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdData != null && IdData.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdData != null) args.AddRange(IdData.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(10, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DW.DB_DATA[])DB.ExecuteProcedure(
                        "imrse_GetDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetData),
                        new DB.Parameter[] { 
			                new DB.InParameter("@ID", SqlDbType.BigInt, id),
			                CreateTableParam("@TIME_CODE", Time),
			                new DB.InParameter("@TOP_COUNT", topCount),
			                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DW.DB_DATA[])DB.ExecuteProcedure(
                    "imrse_GetDataFilter",
                    new DB.AnalyzeDataSet(GetData),
                    new DB.Parameter[] { 
			            CreateTableParam("@ID_DATA", IdData),
			            CreateTableParam("@SERIAL_NBR", SerialNbr),
			            CreateTableParam("@ID_METER", IdMeter),
			            CreateTableParam("@ID_LOCATION", IdLocation),
			            CreateTableParam("@ID_DATA_TYPE", IdDataType),
			            CreateTableParam("@INDEX_NBR", IndexNbr),
			            CreateTableParam("@TIME_CODE", Time),
			            CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
			            CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
			            CreateTableParam("@STATUS", Status),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DW.DB_DATA[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            //if (UseBulkInsertSqlTableType && (
            //    (IdData != null && IdData.Length > MaxItemsPerSqlTableType) ||
            //    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
            //    (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
            //    (Status != null && Status.Length > MaxItemsPerSqlTableType)
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdData != null) args.AddRange(IdData.Select(s => new Tuple<int, object>(1, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
            //    if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
            //    if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
            //    if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
            //    if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
            //    if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(10, s)));

            //    DW.DB_DATA[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DW.DB_DATA[])DB.ExecuteProcedure(
            //            "imrse_GetDataFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetData),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                CreateTableParam("@TIME_CODE", Time),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DW.DB_DATA[])DB.ExecuteProcedure(
            //        "imrse_GetDataFilter",
            //        new DB.AnalyzeDataSet(GetData),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_DATA", IdData),
            //            CreateTableParam("@SERIAL_NBR", SerialNbr),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_DATA_TYPE", IdDataType),
            //            CreateTableParam("@INDEX_NBR", IndexNbr),
            //            CreateTableParam("@TIME_CODE", Time),
            //            CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
            //            CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
            //            CreateTableParam("@STATUS", Status),
            //            new DB.InParameter("@TOP_COUNT", topCount),
            //            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}
            #endregion
        }
        #endregion


        #region GetDataDwFilter
        public DB_DATA_DW[] GetDataDwFilter(long[] IdData, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                            TypeDateTimeCode Time, long[] IdDataSource, int[] IdDataSourceType, int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataDwFilter"));

            DB_DATA_DW[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdData != null && IdData.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdData != null) args.AddRange(IdData.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(10, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_DATA_DW[])DB.ExecuteProcedure(
                        "imrse_GetDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataDw),
                        new DB.Parameter[] { 
			                new DB.InParameter("@ID", SqlDbType.BigInt, id),
			                CreateTableParam("@TIME_CODE", Time),
			                new DB.InParameter("@TOP_COUNT", topCount),
			                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_DATA_DW[])DB.ExecuteProcedure(
                    "imrse_GetDataFilter",
                    new DB.AnalyzeDataSet(GetDataDw),
                    new DB.Parameter[] { 
			            CreateTableParam("@ID_DATA", IdData),
			            CreateTableParam("@SERIAL_NBR", SerialNbr),
			            CreateTableParam("@ID_METER", IdMeter),
			            CreateTableParam("@ID_LOCATION", IdLocation),
			            CreateTableParam("@ID_DATA_TYPE", IdDataType),
			            CreateTableParam("@INDEX_NBR", IndexNbr),
			            CreateTableParam("@TIME_CODE", Time),
			            CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
			            CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
			            CreateTableParam("@STATUS", Status),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_DATA_DW[0];
        }
        #endregion
        #endregion

        #region Table DATA_ARCH
        public DW.DB_DATA_ARCH[] GetDataArchFilterAggregated(long[] IdDataArch, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                            TypeDateTimeCode Time, long[] IdDataSource, int[] IdDataSourceType, long[] IdAlarmEvent, int[] Status, int AggreationType,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));

                DW.DB_DATA_ARCH[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                        "imrse_u_GetDataArchFilterAggregatedTableArgs",
                        new DB.AnalyzeDataSet(GetDataArch),
                        new DB.Parameter[] { 
			                new DB.InParameter("@ID", SqlDbType.BigInt, id),
			                CreateTableParam("@TIME_CODE", Time),
                            new DB.InParameter("@AGGREGATION_TYPE", AggreationType),
			                new DB.InParameter("@TOP_COUNT", topCount),
			                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                    "imrse_u_GetDataArchFilterAggregated",
                    new DB.AnalyzeDataSet(GetDataArch),
                    new DB.Parameter[] { 
					    CreateTableParam("@ID_DATA_ARCH", IdDataArch),
					    CreateTableParam("@SERIAL_NBR", SerialNbr),
					    CreateTableParam("@ID_METER", IdMeter),
					    CreateTableParam("@ID_LOCATION", IdLocation),
					    CreateTableParam("@ID_DATA_TYPE", IdDataType),
					    CreateTableParam("@INDEX_NBR", IndexNbr),
					    CreateTableParam("@TIME_CODE", Time),
					    CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
					    CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
					    CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
					    CreateTableParam("@STATUS", Status),
                        new DB.InParameter("@AGGREGATION_TYPE", AggreationType),
					    new DB.InParameter("@TOP_COUNT", topCount),
					    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        public void SaveDataArch(DW.DB_DATA_ARCH[] ToBeSaved, DB_DATA_TYPE[] ToBeSavedType, int commandTimeout = 0, List<string> updateSetSkipColumns = null)
        {
            //upewniamy się, że typ zapisywany do sql_variant odpowiada typowi zadeklarowanemu dla data typa
            Dictionary<long, DB_DATA_TYPE> toBeSavedTypeDict = ToBeSavedType.ToDictionary(d => d.ID_DATA_TYPE);
            foreach (DW.DB_DATA_ARCH dtItem in ToBeSaved)
                dtItem.VALUE = ParamObject(dtItem.VALUE, dtItem.ID_DATA_TYPE, toBeSavedTypeDict[dtItem.ID_DATA_TYPE].ID_DATA_TYPE_CLASS);

            if (ToBeSaved.Count(d => d.ID_DATA_ARCH != 0) > 0)
                BulkUpdate("DATA_ARCH", ToBeSaved.Where(d => d.ID_DATA_ARCH != 0).ToArray(), null, FillDataArch, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_DATA_ARCH == 0) > 0)
                BulkInsert("DATA_ARCH", ToBeSaved.Where(d => d.ID_DATA_ARCH == 0).ToArray(), fillDataTableMethod: FillDataArch, commandTimeout: commandTimeout);
        }

        #region GetDataArchFilter
        public DW.DB_DATA_ARCH[] GetDataArchFilter(long[] IdDataArch, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                             TypeDateTimeCode Time, long[] IdDataSource, int[] IdDataSourceType, long[] IdAlarmEvent, int[] Status,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));

            DW.DB_DATA_ARCH[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                        "imrse_GetDataArchFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataArch),
                        new DB.Parameter[] { 
				        new DB.InParameter("@ID", SqlDbType.BigInt, id),
				            CreateTableParam("@TIME_CODE", Time),
				            new DB.InParameter("@TOP_COUNT", topCount),
				            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    //DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                    "imrse_GetDataArchFilter",
                    new DB.AnalyzeDataSet(GetDataArch),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_DATA_ARCH", IdDataArch),
				        CreateTableParam("@SERIAL_NBR", SerialNbr),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_DATA_TYPE", IdDataType),
				        CreateTableParam("@INDEX_NBR", IndexNbr),
				        CreateTableParam("@TIME_CODE", Time),
				        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
				        CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
				        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
				        CreateTableParam("@STATUS", Status),
    					new DB.InParameter("@TOP_COUNT", topCount),
	    				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DW.DB_DATA_ARCH[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));

            //if (UseBulkInsertSqlTableType && (
            //    (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
            //    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
            //    (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
            //    (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
            //    (Status != null && Status.Length > MaxItemsPerSqlTableType)
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(1, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
            //    if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
            //    if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
            //    if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
            //    if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
            //    if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
            //    if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));

            //    DW.DB_DATA_ARCH[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
            //            "imrse_GetDataArchFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetDataArch),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                    CreateTableParam("@TIME_CODE", Time),
            //                    new DB.InParameter("@TOP_COUNT", topCount),
            //                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        //DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
            //        "imrse_GetDataArchFilter",
            //        new DB.AnalyzeDataSet(GetDataArch),
            //        new DB.Parameter[] { 
            //                CreateTableParam("@ID_DATA_ARCH", IdDataArch),
            //                CreateTableParam("@SERIAL_NBR", SerialNbr),
            //                CreateTableParam("@ID_METER", IdMeter),
            //                CreateTableParam("@ID_LOCATION", IdLocation),
            //                CreateTableParam("@ID_DATA_TYPE", IdDataType),
            //                CreateTableParam("@INDEX_NBR", IndexNbr),
            //                CreateTableParam("@TIME_CODE", Time),
            //                CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
            //                CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
            //                CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
            //                CreateTableParam("@STATUS", Status),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}
            #endregion
        }
        #endregion
        #region DeleteDataArchFilter
        public void DeleteDataArchFilter(long[] IdDataArch, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                             TypeDateTimeCode Time, long[] IdDataSource, int[] IdDataSourceType, long[] IdAlarmEvent, int[] Status,
                             string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataArchFilter"));

            //zabezpieczenie przed głupim wywołaniem!
            if (IdDataArch == null && SerialNbr == null && IdMeter == null && IdLocation == null && IdDataType == null && IndexNbr == null && Time == null && IdDataSource == null && 
                IdDataSourceType == null && IdAlarmEvent == null && Status == null && customWhereClause == null)
                return;

            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteDataArchFilter",
                new DB.Parameter[] { 
				        CreateTableParam("@ID_DATA_ARCH", IdDataArch),
				        CreateTableParam("@SERIAL_NBR", SerialNbr),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_DATA_TYPE", IdDataType),
				        CreateTableParam("@INDEX_NBR", IndexNbr),
				        CreateTableParam("@TIME_CODE", Time),
				        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
				        CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
				        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
				        CreateTableParam("@STATUS", Status),
	    				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #endregion

        #region Table DATA_TEMPORAL

        public DB_DATA_TEMPORAL[] GetDataTemporal(long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IdAggregationType,
                                                  DateTime? StartTimeStart, DateTime? StartTimeEnd,
                                                  DateTime? EndTimeStart, DateTime? EndTimeEnd)
        {
            if (StartTimeStart.HasValue)
                StartTimeStart = ConvertTimeZoneToUtcTime(StartTimeStart);
            if (StartTimeEnd.HasValue)
                StartTimeEnd = ConvertTimeZoneToUtcTime(StartTimeEnd);
            if (EndTimeStart.HasValue)
                EndTimeStart = ConvertTimeZoneToUtcTime(EndTimeStart);
            if (EndTimeEnd.HasValue)
                EndTimeEnd = ConvertTimeZoneToUtcTime(EndTimeEnd);
            return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                "imrse_u_GetDataTemporal",
                new DB.AnalyzeDataSet(GetDataTemporal),
                new DB.Parameter[] { 
			            CreateTableParam("@SERIAL_NBR", SerialNbr),
			            CreateTableParam("@ID_METER", IdMeter),
			            CreateTableParam("@ID_LOCATION", IdLocation),
			            CreateTableParam("@ID_DATA_TYPE", IdDataType),
			            CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
			            new DB.InParameter("@START_TIME_START", SqlDbType.DateTime, StartTimeStart),
			            new DB.InParameter("@START_TIME_END", SqlDbType.DateTime, StartTimeEnd),
			            new DB.InParameter("@END_TIME_START", SqlDbType.DateTime, EndTimeStart),
			            new DB.InParameter("@END_TIME_END", SqlDbType.DateTime, EndTimeEnd)
					}, 1800
            );
        }

        public void DeleteDataTemporal(long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause) && (SerialNbr == null || SerialNbr.Count() == 0)
                && (IdMeter == null || IdMeter.Count() == 0)
                && (IdLocation == null || IdLocation.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelDataTemporal",
                    new DB.Parameter[] {
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_METER", IdMeter ),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        public void SaveDataTemporal(DB_DATA_TEMPORAL[] ToBeSaved, DB_DATA_TYPE[] ToBeSavedType, int commandTimeout = 0, List<string> updateSetSkipColumns = null)
        {
            //upewniamy się, że typ zapisywany do sql_variant odpowiada typowi zadeklarowanemu dla data typa
            Dictionary<long, DB_DATA_TYPE> toBeSavedTypeDict = ToBeSavedType.ToDictionary(d => d.ID_DATA_TYPE);
            foreach (DB_DATA_TEMPORAL dtItem in ToBeSaved)
                dtItem.VALUE = ParamObject(dtItem.VALUE, dtItem.ID_DATA_TYPE, toBeSavedTypeDict[dtItem.ID_DATA_TYPE].ID_DATA_TYPE_CLASS);

            if (ToBeSaved.Count(d => d.ID_DATA_TEMPORAL != 0) > 0)
                BulkUpdate("DATA_TEMPORAL", ToBeSaved.Where(d => d.ID_DATA_TEMPORAL != 0).ToArray(), null, FillDataTemporal, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_DATA_TEMPORAL == 0) > 0)
                BulkInsert("DATA_TEMPORAL", ToBeSaved.Where(d => d.ID_DATA_TEMPORAL == 0).ToArray(), fillDataTableMethod: FillDataTemporal, commandTimeout: commandTimeout);
        }


        #region GetDataTemporalDevicePerformanceFilter
        public DB_DATA_TEMPORAL[] GetDataTemporalDevicePerformanceFilter(long[] IdDataTemporal, long[] SerialNbr, long[] IdDataType, int[] IndexNbr,
                            int[] IdAggregationType, int[] IdDataSourceType,
                            int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            long[] IdMeter = null;
            long[] IdLocation = null;
            long[] IdAlarmEvent = null;
            long[] IdDataSource = null;
            TypeDateTimeCode StartTime = null;
            TypeDateTimeCode EndTime = null;
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTemporalFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataTemporal != null && IdDataTemporal.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdAggregationType != null && IdAggregationType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataTemporal != null) args.AddRange(IdDataTemporal.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(12, s)));
                if (IdAggregationType != null) args.AddRange(IdAggregationType.Select(s => new Tuple<int, object>(13, s)));

                DB_DATA_TEMPORAL[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                        "imrse_GetDataTemporalFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataTemporalDevicePerformance),
                        new DB.Parameter[] { 
				            new DB.InParameter("@ID", SqlDbType.BigInt, id),
				            CreateTableParam("@START_TIME_CODE", StartTime),
				            CreateTableParam("@END_TIME_CODE", EndTime),
				            new DB.InParameter("@TOP_COUNT", topCount),
				            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                    "imrse_u_GetDataTemporalDevicePerformanceFilter",
                    new DB.AnalyzeDataSet(GetDataTemporalDevicePerformance),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_DATA_TEMPORAL", IdDataTemporal),
				        CreateTableParam("@SERIAL_NBR", SerialNbr),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_DATA_TYPE", IdDataType),
				        CreateTableParam("@START_TIME_CODE", StartTime),
				        CreateTableParam("@END_TIME_CODE", EndTime),
				        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
				        CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
				        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
				        CreateTableParam("@STATUS", Status),
				        CreateTableParam("@INDEX_NBR", IndexNbr),
				        CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }



        private object GetDataTemporalDevicePerformance(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_TEMPORAL()
            {
                ID_DATA_TEMPORAL = GetValue<long>(row["ID_DATA_TEMPORAL"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                VALUE = GetValue(row["VALUE"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                STATUS = GetValue<int>(row["STATUS"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                ID_AGGREGATION_TYPE = GetNullableValue<int>(row["ID_AGGREGATION_TYPE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TEMPORAL[] list = new DB_DATA_TEMPORAL[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TEMPORAL insert = new DB_DATA_TEMPORAL();
                insert.ID_DATA_TEMPORAL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TEMPORAL"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.ID_AGGREGATION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                insert.ID_DATA_SOURCE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                insert.ID_ALARM_EVENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        #endregion

        #region GetDataTemporalByDistributor
        public DB_DATA_TEMPORAL[] GetDataTemporalByDistributor(Enums.ReferenceType FilterOption, int[] IdDistributor, long[] IdDataType, int[] IdAggregationType, string customWhereClause,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                "imrse_u_GetDataTemporalByDistributor",
                new DB.AnalyzeDataSet(GetDataTemporal),
                new DB.Parameter[] { 
                    new DB.InParameter("@FILTER_OPTION", (int)FilterOption),
				    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),
				    CreateTableParam("@ID_DATA_TYPE", IdDataType),
				    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion

        #region GetDataTemporalFilter
        public DB_DATA_TEMPORAL[] GetDataTemporalFilter(long[] IdDataTemporal, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                            TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, int[] IdAggregationType, long[] IdDataSource, int[] IdDataSourceType,
                            long[] IdAlarmEvent, int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTemporalFilter"));

            DB_DATA_TEMPORAL[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdDataTemporal != null && IdDataTemporal.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdAggregationType != null && IdAggregationType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataTemporal != null) args.AddRange(IdDataTemporal.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(12, s)));
                if (IdAggregationType != null) args.AddRange(IdAggregationType.Select(s => new Tuple<int, object>(13, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                        "imrse_GetDataTemporalFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataTemporal),
                        new DB.Parameter[] { 
				            new DB.InParameter("@ID", SqlDbType.BigInt, id),
				            CreateTableParam("@START_TIME_CODE", StartTime),
				            CreateTableParam("@END_TIME_CODE", EndTime),
				            new DB.InParameter("@TOP_COUNT", topCount),
				            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                    "imrse_GetDataTemporalFilter",
                    new DB.AnalyzeDataSet(GetDataTemporal),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_DATA_TEMPORAL", IdDataTemporal),
				        CreateTableParam("@SERIAL_NBR", SerialNbr),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_DATA_TYPE", IdDataType),
				        CreateTableParam("@START_TIME_CODE", StartTime),
				        CreateTableParam("@END_TIME_CODE", EndTime),
				        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
				        CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
				        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
				        CreateTableParam("@STATUS", Status),
				        CreateTableParam("@INDEX_NBR", IndexNbr),
				        CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_DATA_TEMPORAL[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTemporalFilter"));

            //if (UseBulkInsertSqlTableType && (
            //    (IdDataTemporal != null && IdDataTemporal.Length > MaxItemsPerSqlTableType) ||
            //    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType) ||
            //    (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
            //    (Status != null && Status.Length > MaxItemsPerSqlTableType) ||
            //    (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdAggregationType != null && IdAggregationType.Length > MaxItemsPerSqlTableType)
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdDataTemporal != null) args.AddRange(IdDataTemporal.Select(s => new Tuple<int, object>(1, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
            //    if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
            //    if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(8, s)));
            //    if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(9, s)));
            //    if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(10, s)));
            //    if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(11, s)));
            //    if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(12, s)));
            //    if (IdAggregationType != null) args.AddRange(IdAggregationType.Select(s => new Tuple<int, object>(13, s)));

            //    DB_DATA_TEMPORAL[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
            //            "imrse_GetDataTemporalFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetDataTemporal),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                CreateTableParam("@START_TIME_CODE", StartTime),
            //                CreateTableParam("@END_TIME_CODE", EndTime),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
            //        "imrse_GetDataTemporalFilter",
            //        new DB.AnalyzeDataSet(GetDataTemporal),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_DATA_TEMPORAL", IdDataTemporal),
            //            CreateTableParam("@SERIAL_NBR", SerialNbr),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_DATA_TYPE", IdDataType),
            //            CreateTableParam("@START_TIME_CODE", StartTime),
            //            CreateTableParam("@END_TIME_CODE", EndTime),
            //            CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
            //            CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
            //            CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
            //            CreateTableParam("@STATUS", Status),
            //            CreateTableParam("@INDEX_NBR", IndexNbr),
            //            CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
            //            new DB.InParameter("@TOP_COUNT", topCount),
            //            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}
            #endregion
        }
        #endregion
        #endregion

        #region Table DELIVERY_ORDER
        #region GetDeliveryOrderFilter
        public DB_DELIVERY_ORDER[] GetDeliveryOrderFilter(int[] IdDeliveryOrder, TypeDateTimeCode CreationDate, int[] IdDeliveryBatch, long[] IdLocation, long[] IdMeter, int[] IdOperator,
                            int[] IdDeliveryAdvice, int[] DeliveryVolume, bool? IsSuccesfull, string OrderNo, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryOrderFilter"));

            DB_DELIVERY_ORDER[] tmpResult = (DB_DELIVERY_ORDER[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrderFilter",
                new DB.AnalyzeDataSet(GetDeliveryOrder),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DELIVERY_ORDER", IdDeliveryOrder),
					CreateTableParam("@CREATION_DATE_CODE", CreationDate),
					CreateTableParam("@ID_DELIVERY_BATCH", IdDeliveryBatch),
					CreateTableParam("@ID_LOCATION", IdLocation),
					CreateTableParam("@ID_METER", IdMeter),
					CreateTableParam("@ID_OPERATOR", IdOperator),
					CreateTableParam("@ID_DELIVERY_ADVICE", IdDeliveryAdvice),
					CreateTableParam("@DELIVERY_VOLUME", DeliveryVolume),
					new DB.InParameter("@IS_SUCCESFULL", IsSuccesfull),
					new DB.InParameter("@ORDER_NO", OrderNo),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                ).ToArray() : new DB_DELIVERY_ORDER[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryOrderFilter"));

            //return (DB_DELIVERY_ORDER[])DB.ExecuteProcedure(
            //    "imrse_GetDeliveryOrderFilter",
            //    new DB.AnalyzeDataSet(GetDeliveryOrder),
            //    new DB.Parameter[] { 
            //        CreateTableParam("@ID_DELIVERY_ORDER", IdDeliveryOrder),
            //        CreateTableParam("@CREATION_DATE_CODE", CreationDate),
            //        CreateTableParam("@ID_DELIVERY_BATCH", IdDeliveryBatch),
            //        CreateTableParam("@ID_LOCATION", IdLocation),
            //        CreateTableParam("@ID_METER", IdMeter),
            //        CreateTableParam("@ID_OPERATOR", IdOperator),
            //        CreateTableParam("@ID_DELIVERY_ADVICE", IdDeliveryAdvice),
            //        CreateTableParam("@DELIVERY_VOLUME", DeliveryVolume),
            //        new DB.InParameter("@IS_SUCCESFULL", IsSuccesfull),
            //        new DB.InParameter("@ORDER_NO", OrderNo),
            //        new DB.InParameter("@TOP_COUNT", topCount),
            //        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //    autoTransaction, transactionLevel, commandTimeout
            //);
            #endregion
        }
        #endregion
        #endregion

        #region Table DISTRIBUTOR_PERFORMANCE

        public void DeleteDistributorPerformance(int[] IdDistributor, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdDistributor == null || IdDistributor.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelDistributorPerformance",
                    new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region DEVICE

        #region GetDeviceInvoice

        public DataTableCollection GetDeviceInvoice(int IdDistributor, DateTime StartDate, DateTime EndDate)
        {
            return (DataTableCollection)DB.ExecuteProcedure(
                "imrse_u_GetDeviceInvoice",
                new DB.AnalyzeDataSet(GetDeviceInvoice),
                new DB.Parameter[] { 
			        new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, IdDistributor),                    
                    new DB.InParameter("@START_DATE", SqlDbType.DateTime, StartDate),
                    new DB.InParameter("@END_DATE", SqlDbType.DateTime, EndDate)    
				}, AutoTransaction: false, IsolationLevel: IsolationLevel.ReadUncommitted, CommandTimeout: 720000
            );
        }

        private object GetDeviceInvoice(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
                return QueryResult.Tables;
            return null;
        }

        #endregion

        #region CheckBatteryState

        public DB_DEVICE_BATTERY_STATE[] CheckBatteryState(long[] SerialNbr, int commandTimeout = 0)
        {
            return (DB_DEVICE_BATTERY_STATE[])DB.ExecuteProcedure(
                "imrse_u_DeviceBatteryStateCheck",
                new DB.AnalyzeDataSet(CheckBatteryState),
                new DB.Parameter[] { 
                    CreateTableParam("@SERIAL_NBR", SerialNbr)}, commandTimeout
            );
        }

        private object CheckBatteryState(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                List<DB_DEVICE_BATTERY_STATE> retList = new List<DB_DEVICE_BATTERY_STATE>();
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    DB_DEVICE_BATTERY_STATE dbsItem = new DB_DEVICE_BATTERY_STATE();
                    dbsItem.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                    dbsItem.IS_ACTIVE = GetNullableValue<bool>(drItem["IS_ACTIVE"]);
                    dbsItem.ACTIVATION_TIME = GetNullableValue<DateTime>(drItem["ACTIVATION_TIME"]);
                    dbsItem.CURRENT_BATTERY_STATE = GetNullableValue<int>(drItem["CURRENT_BATTERY_STATE"]);
                    dbsItem.CURRENT_BATTERY_STATE_PERCENT = GetNullableValue<int>(drItem["CURRENT_BATTERY_STATE_PERCENT"]);
                    dbsItem.CURRENT_BATTERY_STATE_TIME = GetNullableValue<DateTime>(drItem["CURRENT_BATTERY_STATE_TIME"]);
                    dbsItem.LAST_MONTH_BATTERY_STATE = GetNullableValue<int>(drItem["LAST_MONTH_BATTERY_STATE"]);
                    dbsItem.LAST_MONTH_BATTERY_STATE_PERCENT = GetNullableValue<int>(drItem["LAST_MONTH_BATTERY_STATE_PERCENT"]);
                    dbsItem.LAST_MONTH_BATTERY_STATE_TIME = GetNullableValue<DateTime>(drItem["LAST_MONTH_BATTERY_STATE_TIME"]);
                    retList.Add(dbsItem);
                }
                return retList.ToArray();
            }
            return new List<DB_DEVICE_BATTERY_STATE>().ToArray();
        }

        #endregion

        #endregion

        #region Table MODULE
        public int SaveModule(DB_MODULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE", ToBeSaved.ID_MODULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveModule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region DEVICE_SCHEDULE

        #region CheckDeviceSchedule
        public long[] CheckDeviceSchedule(long[] serialNbr, DateTime startTime, DateTime endTime, Enums.CommandCode commandCode)
        {
            return (long[])DB.ExecuteProcedure(
                "sp_CheckDeviceSchedule",
                new DB.AnalyzeDataSet(CheckDeviceSchedule),
                new DB.Parameter[]
				{
					CreateTableParam("@SERIAL_NBR", serialNbr),
					new DB.InParameter("@START_TIME", startTime),
					new DB.InParameter("@END_TIME", endTime),
					new DB.InParameter("@COMMAND_CODE", (int)commandCode)
				}, false, IsolationLevel.ReadUncommitted,
                this.LongTimeout
            );
        }

        private object CheckDeviceSchedule(DataSet ds)
        {
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<long> retList = new List<long>();

                foreach (DataRow drItem in ds.Tables[0].Rows)
                {
                    retList.Add(GetValue<long>(drItem["SERIAL_NBR"]));
                }

                return retList.ToArray();
            }
            else
                return null;
        }
        #endregion

        public void SaveDeviceSchedule(long[] serialNbr, int commandTimeout = 0)
        {
            DB.ExecuteNonQueryProcedure(
                    "sp_SaveDeviceSchedule",
                    new DB.Parameter[] {
                    CreateTableParam("@SERIAL_NBR", serialNbr)
                }, false, IsolationLevel.ReadUncommitted, commandTimeout
            );
        }

        #endregion

        #region CalibrationSession

        public DB_CALIBRATION_SESSION[] GetAllCalibrationSessions()
        {
            return (DB_CALIBRATION_SESSION[])DB.ExecuteProcedure("awsr_GetAllCalibrationSession", GetAllCalibrationSessions, 1800);
        }

        private object GetAllCalibrationSessions(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CALIBRATION_SESSION()
            {
                ID_CALIBRATION_SESSION = GetNullableValue<int>(row["ID_CALIBRATION_SESSION"]).Value,
                NAME = row["NAME"].ToString(),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = GetNullableValue<DateTime>(row["END_TIME"]),
                REPORTER_NAME = row["REPORTER_NAME"].ToString(),
                STATUS = GetNullableValue<int>(row["STATUS"]).Value

            }).ToArray();
        }

        public void DeleteCalibrationSession(DB_CALIBRATION_SESSION ToBeDeleted)
        {
            DB.InParameter DeletedID = new DB.InParameter("@ID_CALIBRATION_SESSION", SqlDbType.Int, ToBeDeleted.ID_CALIBRATION_SESSION);

            DB.ExecuteNonQueryProcedure("awsr_DeleteCalibrationSession", new DB.Parameter[] { DeletedID });
        }

        public DB_CALIBRATION_SESSION[] GetCalibrationSessions(int[] IdSessiosn)
        {
            return (DB_CALIBRATION_SESSION[])DB.ExecuteProcedure(
                "awsr_GetCalibrationSessions",
                GetAllCalibrationSessions,
                new DB.Parameter[] { CreateTableParam("@ID_CALIBRATION_SESSION", IdSessiosn) }
            );

        }

        public int SaveCalibrationSession(DB_CALIBRATION_SESSION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CALIBRATION_SESSION", ToBeSaved.ID_CALIBRATION_SESSION);
            DB.InParameter Name = new DB.InParameter("@NAME", SqlDbType.NVarChar, ToBeSaved.NAME);
            DB.InParameter StartTime = new DB.InParameter("@START_TIME", SqlDbType.DateTime, ToBeSaved.START_TIME);
            DB.InParameter EndTime = new DB.InParameter("@END_TIME", SqlDbType.DateTime, ToBeSaved.END_TIME);
            DB.InParameter ReporterName = new DB.InParameter("@REPORTER_NAME", SqlDbType.NVarChar, ToBeSaved.REPORTER_NAME);
            DB.InParameter Status = new DB.InParameter("@STATUS", SqlDbType.Int, ToBeSaved.STATUS);

            DB.ExecuteNonQueryProcedure(
                "awsr_SaveCalibrationSession",
                new DB.Parameter[] {
			InsertId,
            Name,
            StartTime,
            EndTime,
            ReporterName,
            Status
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CALIBRATION_SESSION = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        #endregion

        #region CalibrationModule

        public DB_CALIBRATION_MODULE[] GetAllCalibrationModules()
        {
            return (DB_CALIBRATION_MODULE[])DB.ExecuteProcedure("awsr_GetAllCalibrationModules", GetAllCalibrationModules, 1800);
        }

        private object GetAllCalibrationModules(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CALIBRATION_MODULE()
            {
                ID_CALIBRATION_MODULE = GetNullableValue<int>(row["ID_CALIBRATION_MODULE"]).Value,
                NAME = row["NAME"].ToString()
            }).ToArray();
        }

        public DB_CALIBRATION_MODULE[] GetCalibrationModules(int[] IdModules)
        {
            return (DB_CALIBRATION_MODULE[])DB.ExecuteProcedure(
                "awsr_GetCalibrationModules",
                GetAllCalibrationModules,
                new DB.Parameter[] { CreateTableParam("@ID_CALIBRATION_MODULE", IdModules) }
            );

        }

        #endregion

        #region CALIBRATION_TEST

        public DB_CALIBRATION_TEST[] GetCalibrationTests(long[] IdTests)
        {
            return (DB_CALIBRATION_TEST[])DB.ExecuteProcedure(
                "awsr_GetCalibrationTests",
                GetAllCalibrationTests,
                new DB.Parameter[] { CreateTableParam("@ID_CALIBRATION_TEST", IdTests) }
            );

        }

        private object GetAllCalibrationTests(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CALIBRATION_TEST()
            {
                ID_CALIBRATION_TEST = GetNullableValue<int>(row["ID_CALIBRATION_TEST"]).Value,
                ID_CALIBRATION_MODULE = GetNullableValue<int>(row["ID_CALIBRATION_MODULE"]),
                ID_CALIBRATION_SESSION = GetNullableValue<int>(row["ID_CALIBRATION_SESSION"]).Value,
                ID_METER = GetNullableValue<int>(row["ID_METER"]).Value,
                ID_LOCATION = GetNullableValue<int>(row["ID_LOCATION"]).Value,
                NAME = row["NAME"].ToString()
            }).ToArray();
        }

        public long? SaveCalibrationTest(DB_CALIBRATION_TEST ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CALIBRATION_TEST", ToBeSaved.ID_CALIBRATION_TEST);
            DB.InParameter Name = new DB.InParameter("@NAME", SqlDbType.NVarChar, ToBeSaved.NAME);
            DB.InParameter idCalibrationModule = new DB.InParameter("@ID_CALIBRATION_MODULE", ToBeSaved.ID_CALIBRATION_MODULE);
            DB.InParameter idCalibrationSession = new DB.InParameter("@ID_CALIBRATION_SESSION", ToBeSaved.ID_CALIBRATION_SESSION);
            DB.InParameter idMeter = new DB.InParameter("@ID_METER", ToBeSaved.ID_METER);
            DB.InParameter idLocation = new DB.InParameter("@IDLocation", ToBeSaved.ID_LOCATION);

            DB.ExecuteNonQueryProcedure(
                "awsr_SaveCalibrationTest",
                new DB.Parameter[] {
            InsertId,
            Name,
            idCalibrationSession,
            idCalibrationModule,
            idMeter,
            idLocation
                                    }
            );
            if (!InsertId.IsNull)
            {
                ToBeSaved.ID_CALIBRATION_TEST = (long)InsertId.Value;
                return ToBeSaved.ID_CALIBRATION_TEST;
            }
            else
                return null;
        }

        #endregion

        #region CALIBRATION_DATA
        public void SaveCalibrationData(DB_CALIBRATION_DATA[] ToBeSaved, int commandTimeout = 0, List<string> updateSetSkipColumns = null)
        {
            if (ToBeSaved.Count(d => d.ID_CALIBRATION_DATA != 0) > 0)
                BulkUpdate("CALIBRATION_DATA", ToBeSaved.Where(d => d.ID_CALIBRATION_DATA != 0).ToArray(), null, FillCalibrationData, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_CALIBRATION_DATA == 0) > 0)
                BulkInsert("CALIBRATION_DATA", ToBeSaved.Where(d => d.ID_CALIBRATION_DATA == 0).ToArray(), fillDataTableMethod: FillCalibrationData, commandTimeout: commandTimeout);
        }

        public DB_CALIBRATION_DATA[] GetCalibrationData(int ID_CALIBRATION_SESSION)
        {
            return (DB_CALIBRATION_DATA[])DB.ExecuteProcedure(
                   "awsr_GetCalibrationData",
                   new DB.AnalyzeDataSet(GetCalibrationData),
                   new DB.Parameter[] 
                   { 
			            new DB.InParameter("@ID_CALIBRATION_SESSION", ID_CALIBRATION_SESSION) 
                   }, 1800
               );
        }

        private object GetCalibrationData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CALIBRATION_DATA()
            {
                ID_CALIBRATION_DATA = GetNullableValue<long>(row["ID_CALIBRATION_DATA"]).Value,
                ID_CALIBRATION_SESSION = GetNullableValue<int>(row["ID_CALIBRATION_SESSION"]).Value,
                ID_CALIBRATION_MODULE = GetNullableValue<int>(row["ID_CALIBRATION_MODULE"]),
                ID_CALIBRATION_TEST = GetNullableValue<long>(row["ID_CALIBRATION_TEST"]),
                INDEX_NBR = GetNullableValue<int>(row["INDEX_NBR"]).Value,
                ID_DATA_TYPE = GetNullableValue<long>(row["ID_DATA_TYPE"]).Value,
                VALUE = row["VALUE"],
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = GetNullableValue<DateTime>(row["END_TIME"]),
                STATUS = GetNullableValue<int>(row["STATUS"]).Value,
            }).ToArray();
        }

        public void DeleteCalibrationDataSession(int ToBeDeleted)
        {
            DB.InParameter DeletedID = new DB.InParameter("@ID_CALIBRATION_SESSION", SqlDbType.Int, ToBeDeleted);

            DB.ExecuteNonQueryProcedure("awsr_DeleteCalibrationDataSession", new DB.Parameter[] { DeletedID });
        }
        #endregion

        #region Table REFUEL
        #region GetRefuelFilter
        public DB_REFUEL[] GetRefuelFilter(long[] IdRefuel, long[] IdMeter, long[] IdLocation, long[] IdDataSource, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                                           long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRefuelFilter"));

            DB_REFUEL[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdRefuel != null && IdRefuel.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdRefuel != null) args.AddRange(IdRefuel.Select(s => new Tuple<int, object>(1, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(2, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(4, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_REFUEL[])DB.ExecuteProcedure(
                        "imrse_GetRefuelFilterTableArgs",
                        new DB.AnalyzeDataSet(GetRefuel),
                        new DB.Parameter[] { 
				            new DB.InParameter("@ID", SqlDbType.BigInt, id),
				            CreateTableParam("@START_TIME_CODE", StartTime),
				            CreateTableParam("@END_TIME_CODE", EndTime),
				            new DB.InParameter("@TOP_COUNT", topCount),
				            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_REFUEL[])DB.ExecuteProcedure(
                    "imrse_GetRefuelFilter",
                    new DB.AnalyzeDataSet(GetRefuel),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_REFUEL", IdRefuel),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
				        CreateTableParam("@START_TIME_CODE", StartTime),
				        CreateTableParam("@END_TIME_CODE", EndTime),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                ).ToArray() : new DB_REFUEL[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRefuelFilter"));

            //if (UseBulkInsertSqlTableType && (
            //    (IdRefuel != null && IdRefuel.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataSource != null && IdDataSource.Length > MaxItemsPerSqlTableType)
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdRefuel != null) args.AddRange(IdRefuel.Select(s => new Tuple<int, object>(1, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdDataSource != null) args.AddRange(IdDataSource.Select(s => new Tuple<int, object>(4, s)));

            //    DB_REFUEL[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DB_REFUEL[])DB.ExecuteProcedure(
            //            "imrse_GetRefuelFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetRefuel),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                CreateTableParam("@START_TIME_CODE", StartTime),
            //                CreateTableParam("@END_TIME_CODE", EndTime),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DB_REFUEL[])DB.ExecuteProcedure(
            //        "imrse_GetRefuelFilter",
            //        new DB.AnalyzeDataSet(GetRefuel),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_REFUEL", IdRefuel),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
            //            CreateTableParam("@START_TIME_CODE", StartTime),
            //            CreateTableParam("@END_TIME_CODE", EndTime),
            //            new DB.InParameter("@TOP_COUNT", topCount),
            //            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}

            //return (DB_REFUEL[])DB.ExecuteProcedure(
            //    "imrse_GetRefuelFilter",
            //    new DB.AnalyzeDataSet(GetRefuel),
            //    new DB.Parameter[] { 
            //        CreateTableParam("@ID_REFUEL", IdRefuel),
            //        CreateTableParam("@ID_METER", IdMeter),
            //        CreateTableParam("@ID_LOCATION", IdLocation),
            //        CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
            //        new DB.InParameter("@TOP_COUNT", topCount),
            //        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //    autoTransaction, transactionLevel, commandTimeout
            //);
            #endregion
        }
        #endregion
        #endregion

        #region Table REPORT

        public DB_REPORT_MAPPING[] GetReportIdMappings(int[] IdReport, int IdImrServer, bool IsIdReportFromImrServer)
        {
            return (DB_REPORT_MAPPING[])DB.ExecuteProcedure(
                   "imrse_u_GetReportIdMappings",
                   new DB.AnalyzeDataSet(GetReportIdMappings),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_REPORT", IdReport),
			            new DB.InParameter("@ID_IMR_SERVER", IdImrServer), 
			            new DB.InParameter("@IS_ID_REPORT_FROM_IMR_SERVER", IsIdReportFromImrServer) 
                   }, 1800
               );
        }

        private object GetReportIdMappings(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_REPORT_MAPPING()
            {
                ID_REPORT_IMR_SERVER = GetNullableValue<int>(row["ID_REPORT_IMR_SERVER"]),
                ID_REPORT_IMRSC = GetNullableValue<int>(row["ID_REPORT_IMRSC"])
            }).ToArray();
        }

        public int SaveReport(DB_REPORT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REPORT", ToBeSaved.ID_REPORT);
            DB.InParameter layoutParam = new DB.InParameter("@LAYOUT", SqlDbType.Image, ToBeSaved.LAYOUT);

            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveReport",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_REPORT_TYPE", ToBeSaved.ID_REPORT_TYPE)
														//,ParamObject("@LAYOUT", ToBeSaved.LAYOUT)
														,layoutParam
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REPORT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table REPORT_DATA_TYPE

        public DB_REPORT_DATA_TYPE[] GetReportDataType()
        {
            return (DB_REPORT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportDataType",
                new DB.AnalyzeDataSet(GetReportDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_REPORT_DATA_TYPE", new long[] {})
					}
            );
        }

        public DB_REPORT_DATA_TYPE[] GetReportDataType(long[] Ids)
        {
            return (DB_REPORT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportDataType",
                new DB.AnalyzeDataSet(GetReportDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_REPORT_DATA_TYPE", Ids)
					}
            );
        }

        private object GetReportDataType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT_DATA_TYPE[] list = new DB_REPORT_DATA_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT_DATA_TYPE insert = new DB_REPORT_DATA_TYPE();
                insert.ID_REPORT_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REPORT_DATA_TYPE"]);
                insert.SIGNATURE = GetValue<string>(QueryResult.Tables[0].Rows[i]["SIGNATURE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                insert.DIGITS_AFTER_COMMA = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["DIGITS_AFTER_COMMA"]);
                insert.SEQUENCE_NBR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SEQUENCE_NBR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveReportDataType(DB_REPORT_DATA_TYPE ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveReportDataType",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_REPORT_DATA_TYPE", ToBeSaved.ID_REPORT_DATA_TYPE)
										,new DB.InParameter("@SIGNATURE", ToBeSaved.SIGNATURE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT)
														,new DB.InParameter("@DIGITS_AFTER_COMMA", ToBeSaved.DIGITS_AFTER_COMMA)
														,new DB.InParameter("@SEQUENCE_NBR", ToBeSaved.SEQUENCE_NBR)
									}
            );


            return ToBeSaved.ID_REPORT_DATA_TYPE;
        }

        public void DeleteReportDataType(DB_REPORT_DATA_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelReportDataType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT_DATA_TYPE", toBeDeleted.ID_REPORT_DATA_TYPE)			,
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)			
		}
            );
        }

        #endregion

        #region Table REPORT_PERFORMANCE

        public void DeleteReportPerformance(int[] IdReport, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdReport == null || IdReport.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelReportPerformance",
                    new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT", IdReport),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region Table DATA_SOURCE_DATA (save and delete)
        public long SaveDataSourceData(DB_DATA_SOURCE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDataSourceData",
                new DB.Parameter[] {
			                            InsertId
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
										,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_SOURCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataSourceData(DB_DATA_SOURCE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDataSourceData",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_DATA_SOURCE", toBeDeleted.ID_DATA_SOURCE),			
                    new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)
		        });
        }
        #endregion

        #region Table ETL_PERFORMANCE

        public void DeleteEtlPerformance(int[] IdEtl, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteEtlPerformance"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdEtl == null || IdEtl.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelEtlPerformance",
                    new DB.Parameter[] {
                    CreateTableParam("@ID_ETL", IdEtl),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region Table IMR_SERVER_PERFORMANCE

        public void DeleteImrServerPerformance(int[] IdImrServer, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdImrServer == null || IdImrServer.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelImrServerPerformance",
                    new DB.Parameter[] {
                    CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region Table TRANSMISSION_DRIVER

        public DB_TRANSMISSION_DRIVER_MAPPING[] GetTransmissionDriverIdMappings(int[] IdTransmissionDriver, int IdImrServer, bool IsIdTransmissionDriverFromImrServer)
        {
            return (DB_TRANSMISSION_DRIVER_MAPPING[])DB.ExecuteProcedure(
                   "imrse_u_GetTransmissionDriverIdMappings",
                   new DB.AnalyzeDataSet(GetLocationIdMappings),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
			            new DB.InParameter("@ID_IMR_SERVER", IdImrServer), 
			            new DB.InParameter("@IS_ID_TRANSMISSION_DRIVER_FROM_IMR_SERVER", IsIdTransmissionDriverFromImrServer) 
                   }, 1800
               );
        }

        private object GetLocationIdMappings(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_TRANSMISSION_DRIVER_MAPPING()
            {
                ID_TRANSMISSION_DRIVER_IMR_SERVER = GetNullableValue<int>(row["ID_TRANSMISSION_DRIVER_IMR_SERVER"]),
                ID_TRANSMISSION_DRIVER_IMRSC = GetNullableValue<int>(row["ID_TRANSMISSION_DRIVER_IMRSC"])
            }).ToArray();
        }

        #endregion

        #region Table TRANSMISSION_DRIVER_PERFORMANCE

        public void DeleteTransmissionDriverPerformance(int[] IdTransmissionDriver, long[] IdDataType, int[] IdAggregationType, string customWhereClause, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdTransmissionDriver == null || IdTransmissionDriver.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelTransmissionDriverPerformance",
                    new DB.Parameter[] {
                    CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region Custom
        public void InvalidateDataFromDataArch(long[] idDataArch)
        {
            DB.ExecuteNonQueryProcedure(
                    "sp_InvalidateDataFromDataArch",
                    new DB.Parameter[] {
                    CreateTableParam("@IDS",idDataArch)
                }
            );
        }

        public long SaveDataSource(int idOperator)
        {
            DB.InOutParameter idDataSource = new DB.InOutParameter("@ID_DATA_SOURCE", (long?)null);
            DB.ExecuteNonQueryProcedure(
                    "imrse_u_SaveDataSource",
                    new DB.Parameter[] {
                    new DB.InParameter("@ID_OPERATOR", idOperator),
                    idDataSource
                }
            );

            if (!idDataSource.IsNull)
                return Convert.ToInt64(idDataSource.Value);

            throw new ApplicationException("idDataSource is null");
        }
        #region AddDeliveryToLomosoft
        public void AddDeliveryToLomosoft(int idOperator, long idLocation, long? idMeter, int? ullage, int idProductCode, int idDeliveryAdvice,
            DateTime commitDate, int commandTimeout, out bool isSuccessful, out string order_no, out string result, out int errorCode)
        {
            isSuccessful = false;
            order_no = result = null;
            errorCode = 0;
            DB.InParameter paramIdOperator = new DB.InParameter("@ID_OPERATOR", SqlDbType.Int, idOperator);
            DB.InParameter paramIdMeter = new DB.InParameter("@ID_METER", SqlDbType.BigInt, idMeter);
            DB.InParameter paramIdLocation = new DB.InParameter("@ID_LOCATION", SqlDbType.BigInt, idLocation);
            DB.InParameter paramUllage = new DB.InParameter("@ULLAGE", SqlDbType.Int, ullage);
            DB.InParameter paramIdProductCode = new DB.InParameter("@ID_PRODUCT_CODE", SqlDbType.Int, idProductCode);
            DB.InParameter paramIdDeliveryAdvice = new DB.InParameter("@ID_DELIVERY_ADVICE", SqlDbType.Int, idDeliveryAdvice);
            DB.InParameter paramCommitDate = new DB.InParameter("@COMMIT_DATE", SqlDbType.DateTime, commitDate);
            DB.OutParameter paramIsSuccessful = new DB.OutParameter("@IS_SUCCESSFUL", SqlDbType.Bit);
            DB.OutParameter paramOrderNo = new DB.OutParameter("@ORDER_NO", SqlDbType.NVarChar, 10);
            DB.OutParameter paramResult = new DB.OutParameter("@RESULT", SqlDbType.NVarChar, 50);
            DB.OutParameter paramErrorCode = new DB.OutParameter("@ERROR_CODE", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "sp_AddDeliveryToLomosoft",
                new DB.Parameter[]
                    { 
                        paramIdOperator,
                        paramIdMeter,
                        paramIdLocation,
                        paramUllage,
                        paramIdProductCode,
                        paramIdDeliveryAdvice,
                        paramCommitDate,
                        paramIsSuccessful,
                        paramOrderNo,
                        paramResult,
                        paramErrorCode
                    }, commandTimeout);

            if (!paramIsSuccessful.IsNull)
                isSuccessful = (bool)paramIsSuccessful.Value;
            if (!paramOrderNo.IsNull)
                order_no = (string)paramOrderNo.Value;
            if (!paramResult.IsNull)
                result = (string)paramResult.Value;
            if (!paramErrorCode.IsNull)
                errorCode = (int)paramErrorCode.Value;
        }
        #endregion

        #region GetFuelNozzlesForMeter

        public List<long> GetFuelNozzlesForMeter(long IdMeter)
        {
            return (List<long>)DB.ExecuteProcedure(
                "awsr_GetFuelNozzlesForMeter",
                new DB.AnalyzeDataSet(GetFuelNozzlesForMeter),
                new DB.Parameter[] { 
			        new DB.InParameter("@ID_METER", SqlDbType.BigInt, IdMeter)  
				}, 720000
            );
        }

        private object GetFuelNozzlesForMeter(DataSet QueryResult)
        {
            List<long> retList = new List<long>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    retList.Add(GetValue<long>(drItem["ID_METER"]));
                }
            }
            return retList;
        }

        #endregion

        #region GetFirstPacketTime

        public List<Tuple<long, DateTime>> GetFirstPacketTime(long[] SerialNbr, DateTime ReferenceDate, int TopCount = 1,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (List<Tuple<long, DateTime>>)DB.ExecuteProcedure(
                "imrse_u_GetFirstPacketTime",
                new DB.AnalyzeDataSet(GetFirstPacketTime),
                new DB.Parameter[] { 
			        CreateTableParam("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@REFERENCE_DATE", SqlDbType.DateTime, ReferenceDate),
                    new DB.InParameter("@TOP_COUNT", SqlDbType.Int, TopCount)
				}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetFirstPacketTime(DataSet QueryResult)
        {
            List<Tuple<long, DateTime>> retList = new List<Tuple<long, DateTime>>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    retList.Add(new Tuple<long, DateTime>(GetValue<long>(drItem["SERIAL_NBR"]), GetValue<DateTime>(drItem["PACKET_TIME"])));
                }
            }
            return retList;
        }

        #endregion

        #region ProceedViewOperationRow

        public void ProceedViewOperationRow(string[] ViewOperation)
        {
            if (ViewOperation != null && ViewOperation.Length > 0)
            {
                DB.ExecuteScalarSql(String.Join(";", ViewOperation));
            }
        }
        
        #endregion
        #region ProceedViewCreation

        public void ProceedViewCreation(string[] ViewCreation)
        {
            if (ViewCreation != null && ViewCreation.Length > 0)
            {
                DB.ExecuteScalarSql(String.Join(";", ViewCreation));
            }
        }
        
        #endregion
        #region CheckIfTableExists
        public bool CheckIfTableExists(string name)
        {
            bool ret = false;
            DB.InOutParameter retValue = new DB.InOutParameter("@RESULT", ret);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_CheckIfTableExists",
                new DB.Parameter[] {
			                             retValue
										,new DB.InParameter("@TABLE_NAME", name)
									}
            );
            if (!retValue.IsNull)
                return ((int)retValue.Value == 1 ? true : false);

            return false;
        }
        #endregion

        #endregion

        #region IMR-SCADA interface
        public long[] IMRScadaGetDataArchIDsToRemove()
        {
            return (long[])DB.ExecuteProcedure(
                "dataservice_IMRScadaGetDataArchIDsToRemove",
                new DB.AnalyzeDataSet(IMRScadaGetDataArchIDsToRemove),
                new DB.Parameter[] { }
            );
        }

        private object IMRScadaGetDataArchIDsToRemove(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<long> resultList = new List<long>();

            for (int i = 0; i < count; i++)
            {
                long dataArchID = (long)QueryResult.Tables[0].Rows[i][0];
                resultList.Add(dataArchID);
            }
            return resultList.ToArray();
        }

        public int IMRScadaCommitDataArchIDsRemoval(long dataArchID)
        {
            return (int)DB.ExecuteScalar(
                "dataservice_IMRScadaCommitDataArchIDsRemoval",
                new DB.Parameter[] { new DB.InParameter("@ID_DATA_ARCH", dataArchID) });
        }

        public DataArchValue[] GetArchiveDataForLocationsFromLastIDDataArch(long[] Locations, DATA_TYPE_UNIT[] TypesWithUnits, long LastIDDataArch, long? StatusMaskAny, long? StatusMaskAll)
        {
            if (Locations == null || Locations.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataArchValue[0];
            }

            DB.InParameter paramLocations = CreateTableParam("@LOCATION_IDS", Locations);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataArchValue[] result = (DataArchValue[])DB.ExecuteProcedure(
                    "sp_GetArchiveDataForLocationsFromLastIDDataArch",
                    new DB.AnalyzeDataSet(GetDataArchValues),
                    new DB.Parameter[]
				{
                    paramLocations,
                    paramTypes,
                    new DB.InParameter ("@LAST_ID_DATA_ARCH", LastIDDataArch),
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
				},
                false,
                this.LongTimeout
            );

            return result;
        }

        public void IMRScadaCreateLastUploadedIDDataArch(long dataArchID, DateTime uploadDate)
        {
            DB.ExecuteNonQueryProcedure(
                "dataservice_CreateLastUploadedIDDataArch",
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DATA_ARCH", dataArchID),
                    new DB.InParameter("@UPLOAD_DATE", uploadDate) });
        }

        public long IMRScadaGetLastUploadedIDDataArch()
        {
            string tmp = DB.ExecuteScalar("dataservice_GetLastUploadedIDDataArch").ToString();
            return long.Parse(tmp);
        }
        #endregion

        #region Metody rozliczeniowe SGM
        public object StroyService_Payment_Accounting(long idLocation, DateTime startTime, DateTime endtime)
        {
            return DB.ExecuteScalar(
                "rpt_StroyService_Payment_Accounting",
                new DB.Parameter[] { 
                    new DB.InParameter("@id_location", idLocation),
                    new DB.InParameter("@start_time", startTime),
                    new DB.InParameter("@end_time", endtime)	
                });
        }
        #endregion

        #region IMR-SOCAR interface
        public List<DataTable> IMRSocarGetLocationsAreaReadouts(DateTime StartDateTimeUTC, DateTime EndDateTimeUTC)
        {
            return (List<DataTable>)DB.ExecuteProcedure(
                "dataservice_SocarGeorgiaUploadDailyDataOnFTP",
                new DB.AnalyzeDataSet(IMRSocarGetLocationsAreaReadouts),
                new DB.Parameter[] {
                new DB.InParameter("@StartDateTimeUTC", StartDateTimeUTC),
                new DB.InParameter("@EndDateTimeUTC", EndDateTimeUTC),
            }, false, this.LongTimeout);
        }

        private object IMRSocarGetLocationsAreaReadouts(DataSet QueryResult)
        {
            List<DataTable> resultTables = new List<DataTable>();   //3 tablice na wyniki procedury: dataservice_SocarGeorgiaUploadDailyDataOnFTP
            int count = 0;
            int currentTable = 0;

            #region 1. Areas table
            resultTables.Add(new DataTable());
            resultTables[currentTable].Columns.Add("ID_LOCATION_AREA");
            resultTables[currentTable].Columns.Add("NAME");

            count = QueryResult.Tables[currentTable].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long areaLocationID = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                string areLocationName = GetValue<string>(QueryResult.Tables[currentTable].Rows[i][col++]);


                resultTables[currentTable].Rows.Add(areaLocationID, areLocationName);
            }
            #endregion

            #region 2. Locations table
            currentTable = 1;
            resultTables.Add(new DataTable());
            resultTables[currentTable].Columns.Add("ID_LOCATION");
            resultTables[currentTable].Columns.Add("CID");
            resultTables[currentTable].Columns.Add("ID_LOCATION_AREA");

            count = QueryResult.Tables[currentTable].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long locationID = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                string cid = GetValue<string>(QueryResult.Tables[currentTable].Rows[i][col++]);
                long areaLocationID = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);

                resultTables[currentTable].Rows.Add(locationID, cid, areaLocationID);
            }
            #endregion

            #region 3. Readouts table
            currentTable = 2;
            resultTables.Add(new DataTable());
            resultTables[currentTable].Columns.Add("ID_DATA_ARCH");
            resultTables[currentTable].Columns.Add("ID_LOCATION");
            resultTables[currentTable].Columns.Add("CID");
            resultTables[currentTable].Columns.Add("DEVICE_SERIAL_NBR");
            resultTables[currentTable].Columns.Add("TIMESTAMP_UTC");
            resultTables[currentTable].Columns.Add("READOUT_TYPE");
            resultTables[currentTable].Columns.Add("VALUE");
            resultTables[currentTable].Columns.Add("UNIT");

            count = QueryResult.Tables[currentTable].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long dataArchID = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                long locationID = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                string cid = GetValue<string>(QueryResult.Tables[currentTable].Rows[i][col++]);
                long serialNbr = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                DateTime timeStampUtc = GetValue<DateTime>(QueryResult.Tables[currentTable].Rows[i][col++]);
                long readoutType = GetValue<long>(QueryResult.Tables[currentTable].Rows[i][col++]);
                object readoutValue = QueryResult.Tables[currentTable].Rows[i][col++];
                //double readoutValue = GetValue<double>(QueryResult.Tables[currentTable].Rows[i][col++]);
                string unit = GetValue<string>(QueryResult.Tables[currentTable].Rows[i][col++]);

                resultTables[currentTable].Rows.Add(dataArchID, locationID, cid, serialNbr, timeStampUtc, readoutType, readoutValue, unit);
            }
            #endregion

            return resultTables;
        }
        #endregion

        #region sgm data initialization
        public DW.DB_DATA[] SgmGetDataForLocationFromGivenDistributor(int[] idDistributorArray, long[] idDataType)
        {
            return (DW.DB_DATA[])DB.ExecuteProcedure("sgm_GetDataForLocationFromGivenDistributor"
                , new DB.AnalyzeDataSet(GetData)
                , new DB.Parameter[] { CreateTableParam("@IdDistributorTable", idDistributorArray), CreateTableParam("@IdDataTypesTable", idDataType) });
        }
        #endregion

        #region IMR-WSG interface
        public DataTable IMRWsgGetReadoutsForCids(string[] LocationCIDs)
        {
            DataTable LocationCIDsDT = new DataTable();
            LocationCIDsDT.Columns.Add("VALUE", typeof(string));
            if (LocationCIDs != null)
            {
                foreach (string cid in LocationCIDs)
                {
                    LocationCIDsDT.Rows.Add(cid);
                }
            }

            return (DataTable)DB.ExecuteProcedure(
                "dataservice_IMRWsgGetReadoutsForCids",
                new DB.AnalyzeDataSet(IMRWsgGetReadoutsForCids),
                new DB.Parameter[] { new DB.InParameter("@LocationCIDs", SqlDbType.Structured, LocationCIDsDT), });

        }

        private object IMRWsgGetReadoutsForCids(DataSet QueryResult)
        {
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("ID_LOCATION");
            resultTable.Columns.Add("CID");
            resultTable.Columns.Add("VALUE");
            resultTable.Columns.Add("TIMESTAMP");


            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long locationID = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string cid = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                float value = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                DateTime timestamp = GetValue<DateTime>(QueryResult.Tables[0].Rows[i][col++]);

                resultTable.Rows.Add(locationID, cid, value, timestamp);
            }
            return resultTable;
        }


        #endregion

        #region BulkInsertUpdate

        #region FillDataTable
        protected DataTable FillDataArch(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DW.DB_DATA_ARCH[] dataList = data as DW.DB_DATA_ARCH[];
            int iterator = 0;

            foreach (DW.DB_DATA_ARCH dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();
                row["ID_DATA_ARCH"] = dtItem.ID_DATA_ARCH;
                if (dtItem.SERIAL_NBR.HasValue)
                    row["SERIAL_NBR"] = dtItem.SERIAL_NBR.Value;
                else
                    row["SERIAL_NBR"] = DBNull.Value;
                if (dtItem.ID_METER.HasValue)
                    row["ID_METER"] = dtItem.ID_METER.Value;
                else
                    row["ID_METER"] = DBNull.Value;
                if (dtItem.ID_LOCATION.HasValue)
                    row["ID_LOCATION"] = dtItem.ID_LOCATION.Value;
                else
                    row["ID_LOCATION"] = DBNull.Value;
                row["ID_DATA_TYPE"] = dtItem.ID_DATA_TYPE;
                row["INDEX_NBR"] = dtItem.INDEX_NBR;
                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;
                row["TIME"] = ConvertTimeZoneToUtcTime(dtItem.TIME);
                if (dtItem.ID_DATA_SOURCE.HasValue)
                    row["ID_DATA_SOURCE"] = dtItem.ID_DATA_SOURCE.Value;
                else
                    row["ID_DATA_SOURCE"] = DBNull.Value;
                if (dtItem.ID_DATA_SOURCE_TYPE.HasValue)
                    row["ID_DATA_SOURCE_TYPE"] = dtItem.ID_DATA_SOURCE_TYPE.Value;
                else
                    row["ID_DATA_SOURCE_TYPE"] = DBNull.Value;
                if (dtItem.ID_ALARM_EVENT.HasValue)
                    row["ID_ALARM_EVENT"] = dtItem.ID_ALARM_EVENT.Value;
                else
                    row["ID_ALARM_EVENT"] = DBNull.Value;
                row["STATUS"] = dtItem.STATUS;
                dataTable.Rows.Add(row);
                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }
                iterator++;
            }

            return dataTable;
        }

        protected DataTable FillCalibrationData(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_CALIBRATION_DATA[] dataList = data as DB_CALIBRATION_DATA[];

            int iterator = 0;

            foreach (DB_CALIBRATION_DATA dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_CALIBRATION_DATA"] = dtItem.ID_CALIBRATION_DATA;

                row["ID_CALIBRATION_SESSION"] = dtItem.ID_CALIBRATION_SESSION;

                if (dtItem.ID_CALIBRATION_MODULE.HasValue)
                    row["ID_CALIBRATION_MODULE"] = dtItem.ID_CALIBRATION_MODULE.Value;
                else
                    row["ID_CALIBRATION_MODULE"] = DBNull.Value;

                if (dtItem.ID_CALIBRATION_TEST.HasValue)
                    row["ID_CALIBRATION_TEST"] = dtItem.ID_CALIBRATION_TEST.Value;
                else
                    row["ID_CALIBRATION_TEST"] = DBNull.Value;

                row["INDEX_NBR"] = dtItem.INDEX_NBR;
                row["ID_DATA_TYPE"] = dtItem.ID_DATA_TYPE;

                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;

                row["START_TIME"] = ConvertTimeZoneToUtcTime(dtItem.START_TIME);

                if (dtItem.END_TIME.HasValue)
                    row["END_TIME"] = ConvertTimeZoneToUtcTime(dtItem.END_TIME.Value);
                else
                    row["END_TIME"] = DBNull.Value;

                row["STATUS"] = dtItem.STATUS;

                dataTable.Rows.Add(row);
                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }

                iterator++;
            }

            return dataTable;
        }

        protected DataTable FillDataTemporal(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_DATA_TEMPORAL[] dataList = data as DB_DATA_TEMPORAL[];
            int iterator = 0;

            foreach (DB_DATA_TEMPORAL dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();
                row["ID_DATA_TEMPORAL"] = dtItem.ID_DATA_TEMPORAL;
                if (dtItem.SERIAL_NBR.HasValue)
                    row["SERIAL_NBR"] = dtItem.SERIAL_NBR.Value;
                else
                    row["SERIAL_NBR"] = DBNull.Value;
                if (dtItem.ID_METER.HasValue)
                    row["ID_METER"] = dtItem.ID_METER.Value;
                else
                    row["ID_METER"] = DBNull.Value;
                if (dtItem.ID_LOCATION.HasValue)
                    row["ID_LOCATION"] = dtItem.ID_LOCATION.Value;
                else
                    row["ID_LOCATION"] = DBNull.Value;
                row["ID_DATA_TYPE"] = dtItem.ID_DATA_TYPE;
                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;
                row["START_TIME"] = ConvertTimeZoneToUtcTime(dtItem.START_TIME);
                row["END_TIME"] = ConvertTimeZoneToUtcTime(dtItem.END_TIME);
                if (dtItem.ID_DATA_SOURCE.HasValue)
                    row["ID_DATA_SOURCE"] = dtItem.ID_DATA_SOURCE.Value;
                else
                    row["ID_DATA_SOURCE"] = DBNull.Value;
                if (dtItem.ID_DATA_SOURCE_TYPE.HasValue)
                    row["ID_DATA_SOURCE_TYPE"] = dtItem.ID_DATA_SOURCE_TYPE.Value;
                else
                    row["ID_DATA_SOURCE_TYPE"] = DBNull.Value;
                if (dtItem.ID_ALARM_EVENT.HasValue)
                    row["ID_ALARM_EVENT"] = dtItem.ID_ALARM_EVENT.Value;
                else
                    row["ID_ALARM_EVENT"] = DBNull.Value;
                row["STATUS"] = dtItem.STATUS;
                row["INDEX_NBR"] = dtItem.INDEX_NBR;
                if (dtItem.ID_AGGREGATION_TYPE.HasValue)
                    row["ID_AGGREGATION_TYPE"] = dtItem.ID_AGGREGATION_TYPE.Value;
                else
                    row["ID_AGGREGATION_TYPE"] = DBNull.Value;
                dataTable.Rows.Add(row);
                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }
                iterator++;
            }

            return dataTable;
        }

        #endregion

        #endregion
    }
}
