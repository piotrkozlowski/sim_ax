﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IMR.Suite.Data.DB.DAQ;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDAQ : DBCommon
    {
        #region Table ACTION_PACKET

        private object GetActionPacket(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_PACKET[] list = new DB_ACTION_PACKET[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_PACKET insert = new DB_ACTION_PACKET();
                insert.ID_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.ID_PACKET = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table MODULE
        public int SaveModule(DB_MODULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE", ToBeSaved.ID_MODULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveModule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region SaveRouteTable
        public int SaveRouteTable(DB_ROUTE_TABLE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveRouteTable",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
														,new DB.InParameter("@PREFIX", ToBeSaved.PREFIX)
														,new DB.InParameter("@HOP", ToBeSaved.HOP)
														,new DB.InParameter("@COST_OUT", ToBeSaved.COST_OUT)
														,new DB.InParameter("@COST_IN", ToBeSaved.COST_IN)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region GetClusterVeryfication
        public DataTable GetClusterVeryfication(long[] oko, DateTime startTime, DateTime endTime)
        {
            return (DataTable)DB.ExecuteProcedure("dataservice_GetClusteredVeryfication",
                new DB.AnalyzeDataSet(GetClusterVeryficationElements),
                new DB.Parameter[] { 
                        CreateTableParam("@oko_serial_nbrs", oko),
                        new DB.InParameter("@StartDate", startTime),
                        new DB.InParameter("@EndDate", endTime)}, 0);
        }
        private object GetClusterVeryficationElements(DataSet ds)
        {
            return ds.Tables[0];
        }
        #endregion

        #region GetClusterVeryficationWAN3
        public DataTable GetClusterVeryficationWAN3(long[] oko, DateTime startTime, DateTime endTime)
        {
            return (DataTable)DB.ExecuteProcedure("dataservice_GetClusteredVeryficationWAN3",
                new DB.AnalyzeDataSet(GetClusterVeryficationWAN3Elements),
                new DB.Parameter[] { 
                        CreateTableParam("@oko_serial_nbrs", oko),
                        new DB.InParameter("@StartDate", startTime),
                        new DB.InParameter("@EndDate", endTime)}, 0);
        }
        private object GetClusterVeryficationWAN3Elements(DataSet ds)
        {
            return ds.Tables[0];
        }
        #endregion

    }
}
