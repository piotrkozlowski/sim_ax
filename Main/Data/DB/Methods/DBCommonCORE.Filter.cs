using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.CORE;
using System.Data.SqlClient;
using IMR.Suite.Data.DB.CORE;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonCORE
    {
        #region GetActionDataFilter

        public DB_ACTION_DATA[] GetActionDataFilter(long[] IdActionData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDataFilter"));

            return (DB_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionDataFilter",
                new DB.AnalyzeDataSet(GetActionData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_DATA", IdActionData),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionDefFilter

        public DB_ACTION_DEF[] GetActionDefFilter(long[] IdActionDef, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IdActionType,
                            long[] IdActionData, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDefFilter"));

            return (DB_ACTION_DEF[])DB.ExecuteProcedure(
                "imrse_GetActionDefFilter",
                new DB.AnalyzeDataSet(GetActionDef),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_DEF", IdActionDef),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_METER", IdMeter),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_ACTION_TYPE", IdActionType),
                    CreateTableParam("@ID_ACTION_DATA", IdActionData),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionHistoryFilter
        public DB_ACTION_HISTORY[] GetActionHistoryFilter(long[] IdActionHistory, long[] IdAction, int[] IdActionStatus, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long[] IdDescr,
                            long[] IdActionHistoryData, int[] IdModule, string Assembly, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionHistoryFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();

            if (UseBulkInsertSqlTableType && (
                (IdActionHistory != null && IdActionHistory.Length > MaxItemsPerSqlTableType) ||
                (IdAction != null && IdAction.Length > MaxItemsPerSqlTableType) ||
                (IdActionStatus != null && IdActionStatus.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType) ||
                (IdActionHistoryData != null && IdActionHistoryData.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdActionHistory != null) args.AddRange(IdActionHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdAction != null) args.AddRange(IdAction.Select(s => new Tuple<int, object>(2, s)));
                if (IdActionStatus != null) args.AddRange(IdActionStatus.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(6, s)));
                if (IdActionHistoryData != null) args.AddRange(IdActionHistoryData.Select(s => new Tuple<int, object>(7, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(8, s)));

                DB_ACTION_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    parameters.Add(new DB.InParameter("@ID", SqlDbType.BigInt, id));
                    parameters.Add(CreateTableParam("@START_TIME_CODE", StartTime));
                    parameters.Add(CreateTableParam("@END_TIME_CODE", EndTime));
                    if (IsParameterDefined("imrse_GetActionHistoryFilterTableArgs", "@ASSEMBLY", this))
                        parameters.Add(new DB.InParameter("@ASSEMBLY", Assembly));
                    parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                    parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
                    result = (DB_ACTION_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetActionHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetActionHistory),
                        parameters.ToArray(),
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                parameters.Add(CreateTableParam("@ID_ACTION_HISTORY", IdActionHistory));
                parameters.Add(CreateTableParam("@ID_ACTION", IdAction));
                parameters.Add(CreateTableParam("@ID_ACTION_STATUS", IdActionStatus));
                parameters.Add(CreateTableParam("@START_TIME_CODE", StartTime));
                parameters.Add(CreateTableParam("@END_TIME_CODE", EndTime));
                parameters.Add(CreateTableParam("@ID_DESCR", IdDescr));
                parameters.Add(CreateTableParam("@ID_ACTION_HISTORY_DATA", IdActionHistoryData));
                if (IsParameterDefined("imrse_GetActionHistoryFilter", "@ID_MODULE", this))
                    parameters.Add(CreateTableParam("@ID_MODULE", IdModule));
                if (IsParameterDefined("imrse_GetActionHistoryFilter", "@ASSEMBLY", this))
                    parameters.Add(new DB.InParameter("@ASSEMBLY", Assembly));
                parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));

                return (DB_ACTION_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetActionHistoryFilter",
                    new DB.AnalyzeDataSet(GetActionHistory),
                    parameters.ToArray(),
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetActionHistoryDataFilter

        public DB_ACTION_HISTORY_DATA[] GetActionHistoryDataFilter(long[] IdActionHistoryData, int[] ArgNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionHistoryDataFilter"));

            return (DB_ACTION_HISTORY_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionHistoryDataFilter",
                new DB.AnalyzeDataSet(GetActionHistoryData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_HISTORY_DATA", IdActionHistoryData),
                    CreateTableParam("@ARG_NBR", ArgNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionSmsTextFilter

        public DB_ACTION_SMS_TEXT[] GetActionSmsTextFilter(long[] IdActionSmsText, int[] IdLanguage, string SmsText, long[] IdDataType, bool? IsIncoming, int[] IdTransmissionType,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsTextFilter"));

            return (DB_ACTION_SMS_TEXT[])DB.ExecuteProcedure(
                "imrse_GetActionSmsTextFilter",
                new DB.AnalyzeDataSet(GetActionSmsText),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_SMS_TEXT", IdActionSmsText),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@SMS_TEXT", SmsText),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionSmsTextDataFilter

        public DB_ACTION_SMS_TEXT_DATA[] GetActionSmsTextDataFilter(long[] IdActionSmsText, int[] IdLanguage, int[] ArgNbr, long[] IdDataType, int[] IdUnit, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsTextDataFilter"));

            return (DB_ACTION_SMS_TEXT_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionSmsTextDataFilter",
                new DB.AnalyzeDataSet(GetActionSmsTextData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_SMS_TEXT", IdActionSmsText),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    CreateTableParam("@ARG_NBR", ArgNbr),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_UNIT", IdUnit),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionStatusFilter

        public DB_ACTION_STATUS[] GetActionStatusFilter(int[] IdActionStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionStatusFilter"));

            return (DB_ACTION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetActionStatusFilter",
                new DB.AnalyzeDataSet(GetActionStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_STATUS", IdActionStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionTypeFilter

        public DB_ACTION_TYPE[] GetActionTypeFilter(int[] IdActionType, int[] IdActionTypeClass, string Name, string PluginName, long[] IdDescr, bool? IsActionDataFixed,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeFilter"));

            return (DB_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionTypeFilter",
                new DB.AnalyzeDataSet(GetActionType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_TYPE", IdActionType),
                    CreateTableParam("@ID_ACTION_TYPE_CLASS", IdActionTypeClass),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@PLUGIN_NAME", PluginName),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@IS_ACTION_DATA_FIXED", IsActionDataFixed),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionTypeClassFilter

        public DB_ACTION_TYPE_CLASS[] GetActionTypeClassFilter(int[] IdActionTypeClass, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeClassFilter"));

            return (DB_ACTION_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetActionTypeClassFilter",
                new DB.AnalyzeDataSet(GetActionTypeClass),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_TYPE_CLASS", IdActionTypeClass),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionTypeGroupFilter

        public DB_ACTION_TYPE_GROUP[] GetActionTypeGroupFilter(int[] IdActionTypeGroup, int[] IdActionTypeGroupType, int[] IdReferenceType, int[] IdParentGroup, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeGroupFilter"));

            return (DB_ACTION_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroupFilter",
                new DB.AnalyzeDataSet(GetActionTypeGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_TYPE_GROUP", IdActionTypeGroup),
                    CreateTableParam("@ID_ACTION_TYPE_GROUP_TYPE", IdActionTypeGroupType),
                    CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                    CreateTableParam("@ID_PARENT_GROUP", IdParentGroup),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionTypeGroupTypeFilter

        public DB_ACTION_TYPE_GROUP_TYPE[] GetActionTypeGroupTypeFilter(int[] IdActionTypeGroupType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeGroupTypeFilter"));

            return (DB_ACTION_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroupTypeFilter",
                new DB.AnalyzeDataSet(GetActionTypeGroupType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_TYPE_GROUP_TYPE", IdActionTypeGroupType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionTypeInGroupFilter

        public DB_ACTION_TYPE_IN_GROUP[] GetActionTypeInGroupFilter(int[] IdActionTypeGroup, string ActionTypeName, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeInGroupFilter"));

            return (DB_ACTION_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeInGroupFilter",
                new DB.AnalyzeDataSet(GetActionTypeInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION_TYPE_GROUP", IdActionTypeGroup),
                    new DB.InParameter("@ACTION_TYPE_NAME", ActionTypeName),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActivityFilter

        public DB_ACTIVITY[] GetActivityFilter(int[] IdActivity, string Name, long[] IdDescr, int[] IdReferenceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActivityFilter"));

            return (DB_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetActivityFilter",
                new DB.AnalyzeDataSet(GetActivity),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTIVITY", IdActivity),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActorFilter

        public DB_ACTOR[] GetActorFilter(int[] IdActor, string Name, string Surname, string City, string Address, string Postcode,
                            string Email, string Mobile, string Phone, int[] IdLanguage, string Description,
                            long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorFilter"));

            return (DB_ACTOR[])DB.ExecuteProcedure(
                "imrse_GetActorFilter",
                new DB.AnalyzeDataSet(GetActor),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTOR", IdActor),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@SURNAME", Surname),
                    new DB.InParameter("@CITY", City),
                    new DB.InParameter("@ADDRESS", Address),
                    new DB.InParameter("@POSTCODE", Postcode),
                    new DB.InParameter("@EMAIL", Email),
                    new DB.InParameter("@MOBILE", Mobile),
                    new DB.InParameter("@PHONE", Phone),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActorGroupFilter

        public DB_ACTOR_GROUP[] GetActorGroupFilter(int[] IdActorGroup, string Name, bool? BuiltIn, int[] IdDistributor, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorGroupFilter"));

            return (DB_ACTOR_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorGroupFilter",
                new DB.AnalyzeDataSet(GetActorGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTOR_GROUP", IdActorGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@BUILT_IN", BuiltIn),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActorInGroupFilter

        public DB_ACTOR_IN_GROUP[] GetActorInGroupFilter(int[] IdActor, int[] IdActorGroup, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorInGroupFilter"));

            return (DB_ACTOR_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorInGroupFilter",
                new DB.AnalyzeDataSet(GetActorInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTOR", IdActor),
                    CreateTableParam("@ID_ACTOR_GROUP", IdActorGroup),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmFilter

        public DB_ALARM[] GetAlarmFilter(long[] IdAlarm, long[] IdAlarmEvent, int[] IdOperator, int[] IdAlarmGroup, int[] IdAlarmStatus, int[] IdTransmissionType,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmFilter"));

            return (DB_ALARM[])DB.ExecuteProcedure(
                "imrse_GetAlarmFilter",
                new DB.AnalyzeDataSet(GetAlarm),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM", IdAlarm),
                    CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_ALARM_GROUP", IdAlarmGroup),
                    CreateTableParam("@ID_ALARM_STATUS", IdAlarmStatus),
                    CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmDataFilter

        public DB_ALARM_DATA[] GetAlarmDataFilter(long[] IdAlarmData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDataFilter"));

            return (DB_ALARM_DATA[])DB.ExecuteProcedure(
                "imrse_GetAlarmDataFilter",
                new DB.AnalyzeDataSet(GetAlarmData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_DATA", IdAlarmData),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmDefFilter

        public DB_ALARM_DEF[] GetAlarmDefFilter(long[] IdAlarmDef, string Name, long[] SerialNbr, long[] IdMeter, long[] IdLocation, int[] IdAlarmType,
                            long[] IdDataTypeAlarm, int[] IndexNbr, long[] IdDataTypeConfig, bool? IsEnabled, long[] IdAlarmText, long[] IdAlarmData,
                            bool? CheckLastAlarm, int[] SuspensionTime, bool? IsConfirmable, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDefFilter"));
            List<DB.Parameter> parameters = new List<DB.Parameter>();

            parameters.Add(CreateTableParam("@ID_ALARM_DEF", IdAlarmDef));
            parameters.Add(new DB.InParameter("@NAME", Name));
            parameters.Add(CreateTableParam("@SERIAL_NBR", SerialNbr));
            parameters.Add(CreateTableParam("@ID_METER", IdMeter));
            parameters.Add(CreateTableParam("@ID_LOCATION", IdLocation));
            parameters.Add(CreateTableParam("@ID_ALARM_TYPE", IdAlarmType));
            parameters.Add(CreateTableParam("@ID_DATA_TYPE_ALARM", IdDataTypeAlarm));
            if (IsParameterDefined("imrse_GetAlarmDefFilter", "@INDEX_NBR", this))
                parameters.Add(CreateTableParam("@INDEX_NBR", IndexNbr));
            parameters.Add(CreateTableParam("@ID_DATA_TYPE_CONFIG", IdDataTypeConfig));
            parameters.Add(new DB.InParameter("@IS_ENABLED", IsEnabled));
            parameters.Add(CreateTableParam("@ID_ALARM_TEXT", IdAlarmText));
            parameters.Add(CreateTableParam("@ID_ALARM_DATA", IdAlarmData));
            parameters.Add(new DB.InParameter("@CHECK_LAST_ALARM", CheckLastAlarm));
            parameters.Add(CreateTableParam("@SUSPENSION_TIME", SuspensionTime));
            parameters.Add(new DB.InParameter("@IS_CONFIRMABLE", IsConfirmable));
            parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));


            return (DB_ALARM_DEF[])DB.ExecuteProcedure(
                "imrse_GetAlarmDefFilter",
                new DB.AnalyzeDataSet(GetAlarmDef),
                parameters.ToArray(),
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmDefGroupFilter

        public DB_ALARM_DEF_GROUP[] GetAlarmDefGroupFilter(long[] IdAlarmDef, int[] IdAlarmGroup, int[] IdTransmissionType, int[] ConfirmLevel, int[] ConfirmTimeout, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDefGroupFilter"));

            return (DB_ALARM_DEF_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmDefGroupFilter",
                new DB.AnalyzeDataSet(GetAlarmDefGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_DEF", IdAlarmDef),
                    CreateTableParam("@ID_ALARM_GROUP", IdAlarmGroup),
                    CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                    CreateTableParam("@CONFIRM_LEVEL", ConfirmLevel),
                    CreateTableParam("@CONFIRM_TIMEOUT", ConfirmTimeout),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmGroupFilter

        public DB_ALARM_GROUP[] GetAlarmGroupFilter(int[] IdAlarmGroup, string Name, string Description, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        , bool? IsEnabled = null)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmGroupFilter"));

            return (DB_ALARM_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroupFilter",
                new DB.AnalyzeDataSet(GetAlarmGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_GROUP", IdAlarmGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@IS_ENABLED", IsEnabled),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmGroupOperatorFilter

        public DB_ALARM_GROUP_OPERATOR[] GetAlarmGroupOperatorFilter(int[] IdAlarmGroup, int[] IdOperator, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmGroupOperatorFilter"));

            return (DB_ALARM_GROUP_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroupOperatorFilter",
                new DB.AnalyzeDataSet(GetAlarmGroupOperator),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_GROUP", IdAlarmGroup),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmHistoryFilter

        public DB_ALARM_HISTORY[] GetAlarmHistoryFilter(long[] IdAlarmHistory, long[] IdAlarm, int[] IdAlarmStatus, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, string StartRuleHost,
                            string StartRuleUser, string EndRuleHost, string EndRuleUser, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmHistoryFilter"));

            return (DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetAlarmHistoryFilter",
                new DB.AnalyzeDataSet(GetAlarmHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_HISTORY", IdAlarmHistory),
                    CreateTableParam("@ID_ALARM", IdAlarm),
                    CreateTableParam("@ID_ALARM_STATUS", IdAlarmStatus),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@START_RULE_HOST", StartRuleHost),
                    new DB.InParameter("@START_RULE_USER", StartRuleUser),
                    new DB.InParameter("@END_RULE_HOST", EndRuleHost),
                    new DB.InParameter("@END_RULE_USER", EndRuleUser),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmMessageFilter

        public DB_ALARM_MESSAGE[] GetAlarmMessageFilter(long[] IdAlarmEvent, int[] IdLanguage, string AlarmMessage, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmMessageFilter"));

            return (DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetAlarmMessageFilter",
                new DB.AnalyzeDataSet(GetAlarmMessage),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@ALARM_MESSAGE", AlarmMessage),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmStatusFilter

        public DB_ALARM_STATUS[] GetAlarmStatusFilter(int[] IdAlarmStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmStatusFilter"));

            return (DB_ALARM_STATUS[])DB.ExecuteProcedure(
                "imrse_GetAlarmStatusFilter",
                new DB.AnalyzeDataSet(GetAlarmStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_STATUS", IdAlarmStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmTextFilter

        public DB_ALARM_TEXT[] GetAlarmTextFilter(long[] IdAlarmText, int[] IdLanguage, string Name, string AlarmText, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTextFilter"));

            return (DB_ALARM_TEXT[])DB.ExecuteProcedure(
                "imrse_GetAlarmTextFilter",
                new DB.AnalyzeDataSet(GetAlarmText),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_TEXT", IdAlarmText),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@ALARM_TEXT", AlarmText),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmTextDataTypeFilter

        public DB_ALARM_TEXT_DATA_TYPE[] GetAlarmTextDataTypeFilter(long[] IdAlarmText, int[] IdLanguage, int[] ArgNbr, long[] IdDataType, int[] IdUnit, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTextDataTypeFilter"));

            return (DB_ALARM_TEXT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmTextDataTypeFilter",
                new DB.AnalyzeDataSet(GetAlarmTextDataType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_TEXT", IdAlarmText),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    CreateTableParam("@ARG_NBR", ArgNbr),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_UNIT", IdUnit),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAlarmTypeFilter

        public DB_ALARM_TYPE[] GetAlarmTypeFilter(int[] IdAlarmType, string Symbol, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTypeFilter"));

            return (DB_ALARM_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmTypeFilter",
                new DB.AnalyzeDataSet(GetAlarmType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ALARM_TYPE", IdAlarmType),
                    new DB.InParameter("@SYMBOL", Symbol),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetArticleFilter

        public DB_ARTICLE[] GetArticleFilter(long[] IdArticle, long[] ExternalId, string Name, string Description, bool? IsUnique, bool? Visible,
                            bool? IsAiut, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetArticleFilter"));

            return (DB_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetArticleFilter",
                new DB.AnalyzeDataSet(GetArticle),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ARTICLE", IdArticle),
                    CreateTableParam("@EXTERNAL_ID", ExternalId),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@IS_UNIQUE", IsUnique),
                    new DB.InParameter("@VISIBLE", Visible),
                    new DB.InParameter("@IS_AIUT", IsAiut),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetArticleDataFilter

        public DB_ARTICLE_DATA[] GetArticleDataFilter(long[] IdArticleData, long[] IdArticle, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetArticleDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdArticleData != null && IdArticleData.Length > MaxItemsPerSqlTableType) ||
                (IdArticle != null && IdArticle.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdArticleData != null) args.AddRange(IdArticleData.Select(s => new Tuple<int, object>(1, s)));
                if (IdArticle != null) args.AddRange(IdArticle.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_ARTICLE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ARTICLE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetArticleDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetArticleData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_ARTICLE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetArticleDataFilter",
                    new DB.AnalyzeDataSet(GetArticleData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_ARTICLE_DATA", IdArticleData),
                CreateTableParam("@ID_ARTICLE", IdArticle),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetAtgTypeFilter

        public DB_ATG_TYPE[] GetAtgTypeFilter(int[] IdAtgType, string Descr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAtgTypeFilter"));

            return (DB_ATG_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAtgTypeFilter",
                new DB.AnalyzeDataSet(GetAtgType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ATG_TYPE", IdAtgType),
                    new DB.InParameter("@DESCR", Descr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetAuditFilter

        public DB_AUDIT[] GetAuditFilter(long[] IdAudit, long[] BatchId, int[] ChangeType, string TableName, long[] Key1, long[] Key2,
                            long[] Key3, long[] Key4, long[] Key5, long[] Key6, string ColumnName,
                            TypeDateTimeCode Time, string User, string Host, string Application, string Contextinfo,
                            long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();

            if (UseBulkInsertSqlTableType && (
                (IdAudit != null && IdAudit.Length > MaxItemsPerSqlTableType) ||
                (BatchId != null && BatchId.Length > MaxItemsPerSqlTableType) ||
                (ChangeType != null && ChangeType.Length > MaxItemsPerSqlTableType) ||
                (Key1 != null && Key1.Length > MaxItemsPerSqlTableType) ||
                (Key2 != null && Key2.Length > MaxItemsPerSqlTableType) ||
                (Key3 != null && Key3.Length > MaxItemsPerSqlTableType) ||
                (Key4 != null && Key4.Length > MaxItemsPerSqlTableType) ||
                (Key5 != null && Key5.Length > MaxItemsPerSqlTableType) ||
                (Key6 != null && Key6.Length > MaxItemsPerSqlTableType)
                        ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAudit != null) args.AddRange(IdAudit.Select(s => new Tuple<int, object>(1, s)));
                if (BatchId != null) args.AddRange(BatchId.Select(s => new Tuple<int, object>(2, s)));
                if (ChangeType != null) args.AddRange(ChangeType.Select(s => new Tuple<int, object>(3, s)));
                if (Key1 != null) args.AddRange(Key1.Select(s => new Tuple<int, object>(5, s)));
                if (Key2 != null) args.AddRange(Key2.Select(s => new Tuple<int, object>(6, s)));
                if (Key3 != null) args.AddRange(Key3.Select(s => new Tuple<int, object>(7, s)));
                if (Key4 != null) args.AddRange(Key4.Select(s => new Tuple<int, object>(8, s)));
                if (Key5 != null) args.AddRange(Key5.Select(s => new Tuple<int, object>(9, s)));
                if (Key6 != null) args.AddRange(Key6.Select(s => new Tuple<int, object>(10, s)));

                DB_AUDIT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    parameters.Add(new DB.InParameter("@ID", SqlDbType.BigInt, id));
                    parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
                    parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
                    parameters.Add(CreateTableParam("@TIME_CODE", Time));
                    parameters.Add(new DB.InParameter("@USER", User));
                    parameters.Add(new DB.InParameter("@HOST", Host));
                    parameters.Add(new DB.InParameter("@APPLICATION", Application));
                    if (IsParameterDefined("imrse_GetAuditFilterTableArgs", "@CONTEXTINFO", this))
                        parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
                    parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                    parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
                    result = (DB_AUDIT[])DB.ExecuteProcedure(
                        "imrse_GetAuditFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAudit),
                        parameters.ToArray(),
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                //    DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                parameters.Add(CreateTableParam("@ID_AUDIT", IdAudit));
                parameters.Add(CreateTableParam("@BATCH_ID", BatchId));
                parameters.Add(CreateTableParam("@CHANGE_TYPE", ChangeType));
                parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
                parameters.Add(CreateTableParam("@KEY1", Key1));
                parameters.Add(CreateTableParam("@KEY2", Key2));
                parameters.Add(CreateTableParam("@KEY3", Key3));
                parameters.Add(CreateTableParam("@KEY4", Key4));
                parameters.Add(CreateTableParam("@KEY5", Key5));
                parameters.Add(CreateTableParam("@KEY6", Key6));
                parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
                parameters.Add(CreateTableParam("@TIME_CODE", Time));
                parameters.Add(new DB.InParameter("@USER", User));
                parameters.Add(new DB.InParameter("@HOST", Host));
                parameters.Add(new DB.InParameter("@APPLICATION", Application));
                if (IsParameterDefined("imrse_GetAuditFilter", "@CONTEXTINFO", this))
                    parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
                parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
                return (DB_AUDIT[])DB.ExecuteProcedure(
                    "imrse_GetAuditFilter",
                    new DB.AnalyzeDataSet(GetAudit),
                    parameters.ToArray(),
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetCodeFilter

        public DB_CODE[] GetCodeFilter(long[] IdCode, long[] SerialNbr, long[] IdMeter, long[] IdLocation, int[] IdCodeType, int[] CodeNbr,
                            string Code, bool? IsActive, TypeDateTimeCode ActivationTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCodeFilter"));

            return (DB_CODE[])DB.ExecuteProcedure(
                "imrse_GetCodeFilter",
                new DB.AnalyzeDataSet(GetCode),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CODE", IdCode),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_METER", IdMeter),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_CODE_TYPE", IdCodeType),
                    CreateTableParam("@CODE_NBR", CodeNbr),
                    new DB.InParameter("@CODE", Code),
                    new DB.InParameter("@IS_ACTIVE", IsActive),
                    CreateTableParam("@ACTIVATION_TIME_CODE", ActivationTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetCodeTypeFilter

        public DB_CODE_TYPE[] GetCodeTypeFilter(int[] IdCodeType, string Name, long[] IdDescr, int[] IdDataTypeFormat, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCodeTypeFilter"));

            return (DB_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCodeTypeFilter",
                new DB.AnalyzeDataSet(GetCodeType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CODE_TYPE", IdCodeType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_DATA_TYPE_FORMAT", IdDataTypeFormat),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetCommandCodeParamMapFilter

        public DB_COMMAND_CODE_PARAM_MAP[] GetCommandCodeParamMapFilter(long[] IdCommandCodeParamMap, long[] IdCommandCode, long[] ArgNbr, string Name, int[] ByteOffset, int[] BitOffset,
                            int[] Length, int[] IdDataTypeClass, int[] IdUnit, long[] IdDescr, int[] IdProtocol, bool? IsRequired, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCommandCodeParamMapFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(CreateTableParam("@ID_COMMAND_CODE_PARAM_MAP", IdCommandCodeParamMap));
            parameters.Add(CreateTableParam("@ID_COMMAND_CODE", IdCommandCode));
            parameters.Add(CreateTableParam("@ARG_NBR", ArgNbr));
            parameters.Add(new DB.InParameter("@NAME", Name));
            parameters.Add(CreateTableParam("@BYTE_OFFSET", ByteOffset));
            parameters.Add(CreateTableParam("@BIT_OFFSET", BitOffset));
            parameters.Add(CreateTableParam("@LENGTH", Length));
            parameters.Add(CreateTableParam("@ID_DATA_TYPE_CLASS", IdDataTypeClass));
            parameters.Add(CreateTableParam("@ID_UNIT", IdUnit));
            parameters.Add(CreateTableParam("@ID_DESCR", IdDescr));

            if (IsParameterDefined("imrse_GetCommandCodeParamMapFilter", "@ID_PROTOCOL", this))
                parameters.Add(CreateTableParam("@ID_PROTOCOL", IdProtocol));
            if (IsParameterDefined("imrse_GetCommandCodeParamMapFilter", "@IS_REQUIRED", this))
                parameters.Add(new DB.InParameter("@IS_REQUIRED", IsRequired));

            parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));

            return (DB_COMMAND_CODE_PARAM_MAP[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeParamMapFilter",
                new DB.AnalyzeDataSet(GetCommandCodeParamMap),
                parameters.ToArray(),
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetCommandCodeTypeFilter

        public DB_COMMAND_CODE_TYPE[] GetCommandCodeTypeFilter(long[] IdCommandCode, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCommandCodeTypeFilter"));

            return (DB_COMMAND_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeTypeFilter",
                new DB.AnalyzeDataSet(GetCommandCodeType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_COMMAND_CODE", IdCommandCode),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetComponentFilter

        public DB_COMPONENT[] GetComponentFilter(int[] IdComponent, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetComponentFilter"));

            return (DB_COMPONENT[])DB.ExecuteProcedure(
                "imrse_GetComponentFilter",
                new DB.AnalyzeDataSet(GetComponent),
                new DB.Parameter[] {
                    CreateTableParam("@ID_COMPONENT", IdComponent),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConfigurationProfileFilter

        public DB_CONFIGURATION_PROFILE[] GetConfigurationProfileFilter(long[] IdConfigurationProfile, int[] IdConfigurationProfileType, int[] IdDistributor, string Name, long[] IdDescr, int[] IdOperatorCreated,
                    TypeDateTimeCode CreatedTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileFilter"));

            return (DB_CONFIGURATION_PROFILE[])DB.ExecuteProcedure(
            "imrse_GetConfigurationProfileFilter",
            new DB.AnalyzeDataSet(GetConfigurationProfile),
            new DB.Parameter[] {
            CreateTableParam("@ID_CONFIGURATION_PROFILE", IdConfigurationProfile),
            CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", IdConfigurationProfileType),
            CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
            new DB.InParameter("@NAME", Name),
            CreateTableParam("@ID_DESCR", IdDescr),
            CreateTableParam("@ID_OPERATOR_CREATED", IdOperatorCreated),
            CreateTableParam("@CREATED_TIME_CODE", CreatedTime),
            new DB.InParameter("@TOP_COUNT", topCount),
            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConfigurationProfileDataFilter

        public DB_CONFIGURATION_PROFILE_DATA[] GetConfigurationProfileDataFilter(long[] IdConfigurationProfileData, long[] IdConfigurationProfile, int[] IdDeviceType, int[] IdDeviceTypeProfileStep, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileDataFilter"));

            return (DB_CONFIGURATION_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileDataFilter",
                new DB.AnalyzeDataSet(GetConfigurationProfileData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONFIGURATION_PROFILE_DATA", IdConfigurationProfileData),
                    CreateTableParam("@ID_CONFIGURATION_PROFILE", IdConfigurationProfile),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", IdDeviceTypeProfileStep),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConfigurationProfileDeviceTypeFilter

        public DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] GetConfigurationProfileDeviceTypeFilter(long[] IdConfigurationProfile, int[] IdDeviceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileDeviceTypeFilter"));

            return (DB_CONFIGURATION_PROFILE_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileDeviceTypeFilter",
                new DB.AnalyzeDataSet(GetConfigurationProfileDeviceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONFIGURATION_PROFILE", IdConfigurationProfile),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConfigurationProfileTypeFilter

        public DB_CONFIGURATION_PROFILE_TYPE[] GetConfigurationProfileTypeFilter(int[] IdConfigurationProfileType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileTypeFilter"));

            return (DB_CONFIGURATION_PROFILE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileTypeFilter",
                new DB.AnalyzeDataSet(GetConfigurationProfileType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", IdConfigurationProfileType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConfigurationProfileTypeStepFilter

        public DB_CONFIGURATION_PROFILE_TYPE_STEP[] GetConfigurationProfileTypeStepFilter(int[] IdConfigurationProfileType, int[] IdDeviceTypeProfileStep, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileTypeStepFilter"));

            return (DB_CONFIGURATION_PROFILE_TYPE_STEP[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileTypeStepFilter",
                new DB.AnalyzeDataSet(GetConfigurationProfileTypeStep),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", IdConfigurationProfileType),
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", IdDeviceTypeProfileStep),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConsumerFilter

        public DB_CONSUMER[] GetConsumerFilter(int[] IdConsumer, int[] IdConsumerType, int[] IdActor, int[] IdDistributor, string Description, bool IsBlocked, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerFilter"));

            return (DB_CONSUMER[])DB.ExecuteProcedure(
                "imrse_GetConsumerFilter",
                new DB.AnalyzeDataSet(GetConsumer),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER", IdConsumer),
                    CreateTableParam("@ID_CONSUMER_TYPE", IdConsumerType),
                    CreateTableParam("@ID_ACTOR", IdActor),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@IS_BLOCKED",IsBlocked),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConsumerDataFilter

        public DB_CONSUMER_DATA[] GetConsumerDataFilter(long[] IdConsumerData, int[] IdConsumer, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerDataFilter"));

            return (DB_CONSUMER_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerDataFilter",
                new DB.AnalyzeDataSet(GetConsumerData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER_DATA", IdConsumerData),
                    CreateTableParam("@ID_CONSUMER", IdConsumer),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConsumerNotificationFilter

        public DB_CONSUMER_NOTIFICATION[] GetConsumerNotificationFilter(long[] IdConsumerNotification, int[] IdConsumer, int[] IdNotificationDeliveryType, int[] IdOperator, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerNotification != null && IdConsumerNotification.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdNotificationDeliveryType != null && IdNotificationDeliveryType.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerNotification != null) args.AddRange(IdConsumerNotification.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdNotificationDeliveryType != null) args.AddRange(IdNotificationDeliveryType.Select(s => new Tuple<int, object>(3, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(4, s)));

                DB_CONSUMER_NOTIFICATION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_NOTIFICATION[])DB.ExecuteProcedure(
                        "imrse_GetConsumerNotificationFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerNotification),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_NOTIFICATION[])DB.ExecuteProcedure(
                    "imrse_GetConsumerNotificationFilter",
                    new DB.AnalyzeDataSet(GetConsumerNotification),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_NOTIFICATION", IdConsumerNotification),
                CreateTableParam("@ID_CONSUMER", IdConsumer),
                CreateTableParam("@ID_NOTIFICATION_DELIVERY_TYPE", IdNotificationDeliveryType),
                CreateTableParam("@ID_OPERATOR", IdOperator),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerNotificationHistoryFilter

        public DB_CONSUMER_NOTIFICATION_HISTORY[] GetConsumerNotificationHistoryFilter(long[] IdConsumerNotificationHistory, int[] IdConsumer, int[] IdNotificationDeliveryType, int[] IdNotificationType, TypeDateTimeCode Time, string Body,
                            string DeliveryAddress, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationHistoryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerNotificationHistory != null && IdConsumerNotificationHistory.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdNotificationDeliveryType != null && IdNotificationDeliveryType.Length > MaxItemsPerSqlTableType) ||
                (IdNotificationType != null && IdNotificationType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerNotificationHistory != null) args.AddRange(IdConsumerNotificationHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdNotificationDeliveryType != null) args.AddRange(IdNotificationDeliveryType.Select(s => new Tuple<int, object>(3, s)));
                if (IdNotificationType != null) args.AddRange(IdNotificationType.Select(s => new Tuple<int, object>(4, s)));

                DB_CONSUMER_NOTIFICATION_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_NOTIFICATION_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetConsumerNotificationHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerNotificationHistory),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@BODY", Body),
                    new DB.InParameter("@DELIVERY_ADDRESS", DeliveryAddress),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_NOTIFICATION_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetConsumerNotificationHistoryFilter",
                    new DB.AnalyzeDataSet(GetConsumerNotificationHistory),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_NOTIFICATION_HISTORY", IdConsumerNotificationHistory),
                CreateTableParam("@ID_CONSUMER", IdConsumer),
                CreateTableParam("@ID_NOTIFICATION_DELIVERY_TYPE", IdNotificationDeliveryType),
                CreateTableParam("@ID_NOTIFICATION_TYPE", IdNotificationType),
                CreateTableParam("@TIME_CODE", Time),
                new DB.InParameter("@BODY", Body),
                new DB.InParameter("@DELIVERY_ADDRESS", DeliveryAddress),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerNotificationTypeDataFilter

        public DB_CONSUMER_NOTIFICATION_TYPE_DATA[] GetConsumerNotificationTypeDataFilter(long[] IdConsumerNotificationTypeData, int[] IdConsumer, int[] IdNotificationType, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationTypeDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerNotificationTypeData != null && IdConsumerNotificationTypeData.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdNotificationType != null && IdNotificationType.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerNotificationTypeData != null) args.AddRange(IdConsumerNotificationTypeData.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdNotificationType != null) args.AddRange(IdNotificationType.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(4, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(5, s)));

                DB_CONSUMER_NOTIFICATION_TYPE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_NOTIFICATION_TYPE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetConsumerNotificationTypeDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerNotificationTypeData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_NOTIFICATION_TYPE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetConsumerNotificationTypeDataFilter",
                    new DB.AnalyzeDataSet(GetConsumerNotificationTypeData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_NOTIFICATION_TYPE_DATA", IdConsumerNotificationTypeData),
                CreateTableParam("@ID_CONSUMER", IdConsumer),
                CreateTableParam("@ID_NOTIFICATION_TYPE", IdNotificationType),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerPaymentModuleFilter

        public DB_CONSUMER_PAYMENT_MODULE[] GetConsumerPaymentModuleFilter(long[] IdConsumerPaymentModule, int[] IdConsumer, int[] IdPaymentModule, string PaymentSystemNumber, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerPaymentModuleFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerPaymentModule != null && IdConsumerPaymentModule.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdPaymentModule != null && IdPaymentModule.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerPaymentModule != null) args.AddRange(IdConsumerPaymentModule.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdPaymentModule != null) args.AddRange(IdPaymentModule.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_PAYMENT_MODULE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_PAYMENT_MODULE[])DB.ExecuteProcedure(
                        "imrse_GetConsumerPaymentModuleFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerPaymentModule),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@PAYMENT_SYSTEM_NUMBER", PaymentSystemNumber),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_PAYMENT_MODULE[])DB.ExecuteProcedure(
                    "imrse_GetConsumerPaymentModuleFilter",
                    new DB.AnalyzeDataSet(GetConsumerPaymentModule),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_CONSUMER_PAYMENT_MODULE", IdConsumerPaymentModule),
                        CreateTableParam("@ID_CONSUMER", IdConsumer),
                        CreateTableParam("@ID_PAYMENT_MODULE", IdPaymentModule),
                        new DB.InParameter("@PAYMENT_SYSTEM_NUMBER", PaymentSystemNumber),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerTransactionFilter

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransactionFilter(long[] IdConsumerTransaction, int[] IdConsumer, int[] IdOperator, int[] IdDistributor, int[] IdConsumerTransactionType, int[] IdTariff,
                            TypeDateTimeCode Time, long[] IdConsumerSettlement, int[] IdPaymentModule, string TransactionGuid, long[] IdPacket,
                            long[] IdAction, long[] IdPreviousConsumerTransaction, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerTransaction != null && IdConsumerTransaction.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdConsumerTransactionType != null && IdConsumerTransactionType.Length > MaxItemsPerSqlTableType) ||
                (IdTariff != null && IdTariff.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlement != null && IdConsumerSettlement.Length > MaxItemsPerSqlTableType) ||
                (IdPaymentModule != null && IdPaymentModule.Length > MaxItemsPerSqlTableType) ||
                (IdPacket != null && IdPacket.Length > MaxItemsPerSqlTableType) ||
                (IdAction != null && IdAction.Length > MaxItemsPerSqlTableType) ||
                (IdPreviousConsumerTransaction != null && IdPreviousConsumerTransaction.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerTransaction != null) args.AddRange(IdConsumerTransaction.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(3, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(4, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(4, s)));
                if (IdConsumerTransactionType != null) args.AddRange(IdConsumerTransactionType.Select(s => new Tuple<int, object>(5, s)));
                if (IdTariff != null) args.AddRange(IdTariff.Select(s => new Tuple<int, object>(6, s)));
                if (IdConsumerSettlement != null) args.AddRange(IdConsumerSettlement.Select(s => new Tuple<int, object>(8, s)));
                if (IdPaymentModule != null) args.AddRange(IdPaymentModule.Select(s => new Tuple<int, object>(9, s)));
                if (IdPacket != null) args.AddRange(IdPacket.Select(s => new Tuple<int, object>(11, s)));
                if (IdAction != null) args.AddRange(IdAction.Select(s => new Tuple<int, object>(12, s)));
                if (IdPreviousConsumerTransaction != null) args.AddRange(IdPreviousConsumerTransaction.Select(s => new Tuple<int, object>(13, s)));

                DB_CONSUMER_TRANSACTION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                        "imrse_GetConsumerTransactionFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerTransaction),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@TIME_CODE", Time),
                            new DB.InParameter("@TRANSACTION_GUID", TransactionGuid),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                    "imrse_GetConsumerTransactionFilter",
                    new DB.AnalyzeDataSet(GetConsumerTransaction),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_CONSUMER_TRANSACTION", IdConsumerTransaction),
                        CreateTableParam("@ID_CONSUMER", IdConsumer),
                        CreateTableParam("@ID_OPERATOR", IdOperator),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                        CreateTableParam("@ID_CONSUMER_TRANSACTION_TYPE", IdConsumerTransactionType),
                        CreateTableParam("@ID_TARIFF", IdTariff),
                        CreateTableParam("@TIME_CODE", Time),
                        CreateTableParam("@ID_CONSUMER_SETTLEMENT", IdConsumerSettlement),
                        CreateTableParam("@ID_PAYMENT_MODULE", IdPaymentModule),
                        new DB.InParameter("@TRANSACTION_GUID", TransactionGuid),
                        CreateTableParam("@ID_PACKET", IdPacket),
                        CreateTableParam("@ID_ACTION", IdAction),
                        CreateTableParam("@ID_PREVIOUS_CONSUMER_TRANSACTION", IdPreviousConsumerTransaction),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerTransactionDataFilter

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionDataFilter(long[] IdConsumerTransactionData, long[] IdConsumerTransaction, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerTransactionData != null && IdConsumerTransactionData.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerTransaction != null && IdConsumerTransaction.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerTransactionData != null) args.AddRange(IdConsumerTransactionData.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumerTransaction != null) args.AddRange(IdConsumerTransaction.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_CONSUMER_TRANSACTION_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                        "imrse_GetConsumerTransactionDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerTransactionData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                    "imrse_GetConsumerTransactionDataFilter",
                    new DB.AnalyzeDataSet(GetConsumerTransactionData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", IdConsumerTransactionData),
                CreateTableParam("@ID_CONSUMER_TRANSACTION", IdConsumerTransaction),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerTransactionTypeFilter

        public DB_CONSUMER_TRANSACTION_TYPE[] GetConsumerTransactionTypeFilter(int[] IdConsumerTransactionType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerTransactionType != null && IdConsumerTransactionType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerTransactionType != null) args.AddRange(IdConsumerTransactionType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_TRANSACTION_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_TRANSACTION_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetConsumerTransactionTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerTransactionType),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONSUMER_TRANSACTION_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetConsumerTransactionTypeFilter",
                    new DB.AnalyzeDataSet(GetConsumerTransactionType),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_CONSUMER_TRANSACTION_TYPE", IdConsumerTransactionType),
                        new DB.InParameter("@NAME", Name),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerTypeFilter

        public DB_CONSUMER_TYPE[] GetConsumerTypeFilter(int[] IdConsumerType, string Name, long[] IdDescr, long? topCount, int[] IdDistributor
            , string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTypeFilter"));

            return (DB_CONSUMER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerTypeFilter",
                new DB.AnalyzeDataSet(GetConsumerType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER_TYPE", IdConsumerType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConsumerTypeDataFilter

        public DB_CONSUMER_TYPE_DATA[] GetConsumerTypeDataFilter(long[] IdConsumerTypeData, int[] IdConsumerType, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTypeDataFilter"));

            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTypeDataFilter",
                new DB.AnalyzeDataSet(GetConsumerTypeData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER_TYPE_DATA", IdConsumerTypeData),
                    CreateTableParam("@ID_CONSUMER_TYPE", IdConsumerType),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion

        #region GetConsumerSettlementFilter

        public DB_CONSUMER_SETTLEMENT[] GetConsumerSettlementFilter(long[] IdConsumerSettlement, int[] IdConsumer, int[] IdTariffSettlementPeriod, TypeDateTimeCode SettlementTime, int[] IdModule, int[] IdOperator,
                            int[] IdConsumerSettlementReason, int[] IdConsumerSettlementStatus, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlement != null && IdConsumerSettlement.Length > MaxItemsPerSqlTableType) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdTariffSettlementPeriod != null && IdTariffSettlementPeriod.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlementReason != null && IdConsumerSettlementReason.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlementStatus != null && IdConsumerSettlementStatus.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlement != null) args.AddRange(IdConsumerSettlement.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(2, s)));
                if (IdTariffSettlementPeriod != null) args.AddRange(IdTariffSettlementPeriod.Select(s => new Tuple<int, object>(3, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(5, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(6, s)));
                if (IdConsumerSettlementReason != null) args.AddRange(IdConsumerSettlementReason.Select(s => new Tuple<int, object>(7, s)));
                if (IdConsumerSettlementStatus != null) args.AddRange(IdConsumerSettlementStatus.Select(s => new Tuple<int, object>(8, s)));

                DB_CONSUMER_SETTLEMENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlement),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@SETTLEMENT_TIME_CODE", SettlementTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlement),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_SETTLEMENT", IdConsumerSettlement),
                CreateTableParam("@ID_CONSUMER", IdConsumer),
                CreateTableParam("@ID_TARIFF_SETTLEMENT_PERIOD", IdTariffSettlementPeriod),
                CreateTableParam("@SETTLEMENT_TIME_CODE", SettlementTime),
                CreateTableParam("@ID_MODULE", IdModule),
                CreateTableParam("@ID_OPERATOR", IdOperator),
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_REASON", IdConsumerSettlementReason),
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_STATUS", IdConsumerSettlementStatus),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerSettlementDocumentFilter

        public DB_CONSUMER_SETTLEMENT_DOCUMENT[] GetConsumerSettlementDocumentFilter(long[] IdConsumerSettlementDocument, long[] IdConsumerSettlement, int[] IdSettlementDocumentType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementDocumentFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlementDocument != null && IdConsumerSettlementDocument.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlement != null && IdConsumerSettlement.Length > MaxItemsPerSqlTableType) ||
                (IdSettlementDocumentType != null && IdSettlementDocumentType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlementDocument != null) args.AddRange(IdConsumerSettlementDocument.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumerSettlement != null) args.AddRange(IdConsumerSettlement.Select(s => new Tuple<int, object>(2, s)));
                if (IdSettlementDocumentType != null) args.AddRange(IdSettlementDocumentType.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_SETTLEMENT_DOCUMENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT_DOCUMENT[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementDocumentFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlementDocument),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT_DOCUMENT[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementDocumentFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlementDocument),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_DOCUMENT", IdConsumerSettlementDocument),
                CreateTableParam("@ID_CONSUMER_SETTLEMENT", IdConsumerSettlement),
                CreateTableParam("@ID_SETTLEMENT_DOCUMENT_TYPE", IdSettlementDocumentType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerSettlementErrorFilter

        public DB_CONSUMER_SETTLEMENT_ERROR[] GetConsumerSettlementErrorFilter(int[] IdConsumerSettlementError, long[] IdConsumerSettlement, int[] IdConsumerSettlementErrorType, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementErrorFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlementError != null && IdConsumerSettlementError.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlement != null && IdConsumerSettlement.Length > MaxItemsPerSqlTableType) ||
                (IdConsumerSettlementErrorType != null && IdConsumerSettlementErrorType.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlementError != null) args.AddRange(IdConsumerSettlementError.Select(s => new Tuple<int, object>(1, s)));
                if (IdConsumerSettlement != null) args.AddRange(IdConsumerSettlement.Select(s => new Tuple<int, object>(2, s)));
                if (IdConsumerSettlementErrorType != null) args.AddRange(IdConsumerSettlementErrorType.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_SETTLEMENT_ERROR[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT_ERROR[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementErrorFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlementError),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@DESCRIPTION", Description),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT_ERROR[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementErrorFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlementError),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR", IdConsumerSettlementError),
                        CreateTableParam("@ID_CONSUMER_SETTLEMENT", IdConsumerSettlement),
                        CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", IdConsumerSettlementErrorType),
                        new DB.InParameter("@DESCRIPTION", Description),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerSettlementErrorTypeFilter

        public DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] GetConsumerSettlementErrorTypeFilter(int[] IdConsumerSettlementErrorType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementErrorTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlementErrorType != null && IdConsumerSettlementErrorType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlementErrorType != null) args.AddRange(IdConsumerSettlementErrorType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT_ERROR_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementErrorTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlementErrorType),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT_ERROR_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementErrorTypeFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlementErrorType),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", IdConsumerSettlementErrorType),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerSettlementReasonFilter

        public DB_CONSUMER_SETTLEMENT_REASON[] GetConsumerSettlementReasonFilter(int[] IdConsumerSettlementReason, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementReasonFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlementReason != null && IdConsumerSettlementReason.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlementReason != null) args.AddRange(IdConsumerSettlementReason.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_CONSUMER_SETTLEMENT_REASON[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT_REASON[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementReasonFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlementReason),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT_REASON[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementReasonFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlementReason),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_REASON", IdConsumerSettlementReason),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerSettlementStatusFilter

        public DB_CONSUMER_SETTLEMENT_STATUS[] GetConsumerSettlementStatusFilter(int[] IdConsumerSettlementStatus, string Name, bool? IsError, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementStatusFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConsumerSettlementStatus != null && IdConsumerSettlementStatus.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConsumerSettlementStatus != null) args.AddRange(IdConsumerSettlementStatus.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(4, s)));

                DB_CONSUMER_SETTLEMENT_STATUS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONSUMER_SETTLEMENT_STATUS[])DB.ExecuteProcedure(
                        "imrse_GetConsumerSettlementStatusFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConsumerSettlementStatus),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@IS_ERROR", IsError),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_CONSUMER_SETTLEMENT_STATUS[])DB.ExecuteProcedure(
                    "imrse_GetConsumerSettlementStatusFilter",
                    new DB.AnalyzeDataSet(GetConsumerSettlementStatus),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONSUMER_SETTLEMENT_STATUS", IdConsumerSettlementStatus),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@IS_ERROR", IsError),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        #region GetContentFilter

        public DB_CONTENT[] GetContentFilter(int[] IdContent, int[] IdModule, string Name, string Description, int[] IdLanguage, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContentFilter"));

            return (DB_CONTENT[])DB.ExecuteProcedure(
                "imrse_GetContentFilter",
                new DB.AnalyzeDataSet(GetContent),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONTENT", IdContent),
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetContractFilter

        public DB_CONTRACT[] GetContractFilter(int[] IdContract, string Name, string Descr, int[] IdDistributor, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContractFilter"));

            return (DB_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetContractFilter",
                new DB.AnalyzeDataSet(GetContract),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONTRACT", IdContract),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCR", Descr),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetContractDataFilter

        public DB_CONTRACT_DATA[] GetContractDataFilter(long[] IdContractData, int[] IdContract, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContractDataFilter"));

            return (DB_CONTRACT_DATA[])DB.ExecuteProcedure(
                "imrse_GetContractDataFilter",
                new DB.AnalyzeDataSet(GetContractData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONTRACT_DATA", IdContractData),
                    CreateTableParam("@ID_CONTRACT", IdContract),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConversionFilter

        public DB_CONVERSION[] GetConversionFilter(long[] IdConversion, int[] IdReferenceType, long[] ReferenceValue, long[] IdDataType, int[] IndexNbr, Double[] Slope,
                            Double[] Bias, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConversionFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdConversion != null && IdConversion.Length > MaxItemsPerSqlTableType) ||
                (IdReferenceType != null && IdReferenceType.Length > MaxItemsPerSqlTableType) ||
                (ReferenceValue != null && ReferenceValue.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (Slope != null && Slope.Length > MaxItemsPerSqlTableType) ||
                (Bias != null && Bias.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdConversion != null) args.AddRange(IdConversion.Select(s => new Tuple<int, object>(1, s)));
                if (IdReferenceType != null) args.AddRange(IdReferenceType.Select(s => new Tuple<int, object>(2, s)));
                if (ReferenceValue != null) args.AddRange(ReferenceValue.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(4, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(5, s)));
                if (Slope != null) args.AddRange(Slope.Select(s => new Tuple<int, object>(6, s)));
                if (Bias != null) args.AddRange(Bias.Select(s => new Tuple<int, object>(7, s)));

                DB_CONVERSION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_CONVERSION[])DB.ExecuteProcedure(
                        "imrse_GetConversionFilterTableArgs",
                        new DB.AnalyzeDataSet(GetConversion),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_CONVERSION[])DB.ExecuteProcedure(
                    "imrse_GetConversionFilter",
                    new DB.AnalyzeDataSet(GetConversion),
                    new DB.Parameter[] {
                CreateTableParam("@ID_CONVERSION", IdConversion),
                CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                CreateTableParam("@REFERENCE_VALUE", ReferenceValue),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                CreateTableParam("@SLOPE", Slope),
                CreateTableParam("@BIAS", Bias),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetCurrencyFilter

        public DB_CURRENCY[] GetCurrencyFilter(int[] IdCurrency, string Symbol, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCurrencyFilter"));

            return (DB_CURRENCY[])DB.ExecuteProcedure(
                "imrse_GetCurrencyFilter",
                new DB.AnalyzeDataSet(GetCurrency),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CURRENCY", IdCurrency),
                    new DB.InParameter("@SYMBOL", Symbol),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataArchFilter
        public DB_DATA_ARCH[] GetDataArchFilter(long[] IdDataArch, long[] SerialNbr, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode Time, bool? IsAction,
                            bool? IsAlarm, long[] IdPacket, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdPacket != null && IdPacket.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));
                if (IdPacket != null) args.AddRange(IdPacket.Select(s => new Tuple<int, object>(8, s)));

                DB_DATA_ARCH[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DATA_ARCH[])DB.ExecuteProcedure(
                        "imrse_GetDataArchFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataArch),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@IS_ACTION", IsAction),
                    new DB.InParameter("@IS_ALARM", IsAlarm),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DATA_ARCH[])DB.ExecuteProcedure(
                    "imrse_GetDataArchFilter",
                    new DB.AnalyzeDataSet(GetDataArch),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DATA_ARCH", IdDataArch),
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                CreateTableParam("@TIME_CODE", Time),
                new DB.InParameter("@IS_ACTION", IsAction),
                new DB.InParameter("@IS_ALARM", IsAlarm),
                CreateTableParam("@ID_PACKET", IdPacket),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDataChangeFilter

        public DB_DATA_CHANGE[] GetDataChangeFilter(long[] IdDataChange, string TableName, long[] ChangedId, long[] ChangedId2, long[] ChangedId3, long[] ChangedId4,
                            int[] IndexNbr, int[] ChangeType, TypeDateTimeCode Time, string User, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataChangeFilter"));

            return (DB_DATA_CHANGE[])DB.ExecuteProcedure(
                "imrse_GetDataChangeFilter",
                new DB.AnalyzeDataSet(GetDataChange),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_CHANGE", IdDataChange),
                    new DB.InParameter("@TABLE_NAME", TableName),
                    CreateTableParam("@CHANGED_ID", ChangedId),
                    CreateTableParam("@CHANGED_ID2", ChangedId2),
                    CreateTableParam("@CHANGED_ID3", ChangedId3),
                    CreateTableParam("@CHANGED_ID4", ChangedId4),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    CreateTableParam("@CHANGE_TYPE", ChangeType),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@USER", User),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataFormatGroupFilter

        public DB_DATA_FORMAT_GROUP[] GetDataFormatGroupFilter(int[] IdDataFormatGroup, string Name, int[] IdModule, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFormatGroupFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataFormatGroup != null && IdDataFormatGroup.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataFormatGroup != null) args.AddRange(IdDataFormatGroup.Select(s => new Tuple<int, object>(1, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(3, s)));

                DB_DATA_FORMAT_GROUP[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DATA_FORMAT_GROUP[])DB.ExecuteProcedure(
                        "imrse_GetDataFormatGroupFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataFormatGroup),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DATA_FORMAT_GROUP[])DB.ExecuteProcedure(
                    "imrse_GetDataFormatGroupFilter",
                    new DB.AnalyzeDataSet(GetDataFormatGroup),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DATA_FORMAT_GROUP", IdDataFormatGroup),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_MODULE", IdModule),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDataFormatGroupDetailsFilter

        public DB_DATA_FORMAT_GROUP_DETAILS[] GetDataFormatGroupDetailsFilter(int[] IdDataFormatGroup, long[] IdDataType, int[] IdDataTypeFormatIn, int[] IdDataTypeFormatOut, int[] IdUnitIn, int[] IdUnitOut,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFormatGroupDetailsFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataFormatGroup != null && IdDataFormatGroup.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IdDataTypeFormatIn != null && IdDataTypeFormatIn.Length > MaxItemsPerSqlTableType) ||
                (IdDataTypeFormatOut != null && IdDataTypeFormatOut.Length > MaxItemsPerSqlTableType) ||
                (IdUnitIn != null && IdUnitIn.Length > MaxItemsPerSqlTableType) ||
                (IdUnitOut != null && IdUnitOut.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataFormatGroup != null) args.AddRange(IdDataFormatGroup.Select(s => new Tuple<int, object>(1, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataTypeFormatIn != null) args.AddRange(IdDataTypeFormatIn.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataTypeFormatOut != null) args.AddRange(IdDataTypeFormatOut.Select(s => new Tuple<int, object>(4, s)));
                if (IdUnitIn != null) args.AddRange(IdUnitIn.Select(s => new Tuple<int, object>(5, s)));
                if (IdUnitOut != null) args.AddRange(IdUnitOut.Select(s => new Tuple<int, object>(6, s)));

                DB_DATA_FORMAT_GROUP_DETAILS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DATA_FORMAT_GROUP_DETAILS[])DB.ExecuteProcedure(
                        "imrse_GetDataFormatGroupDetailsFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataFormatGroupDetails),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DATA_FORMAT_GROUP_DETAILS[])DB.ExecuteProcedure(
                    "imrse_GetDataFormatGroupDetailsFilter",
                    new DB.AnalyzeDataSet(GetDataFormatGroupDetails),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DATA_FORMAT_GROUP", IdDataFormatGroup),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@ID_DATA_TYPE_FORMAT_IN", IdDataTypeFormatIn),
                CreateTableParam("@ID_DATA_TYPE_FORMAT_OUT", IdDataTypeFormatOut),
                CreateTableParam("@ID_UNIT_IN", IdUnitIn),
                CreateTableParam("@ID_UNIT_OUT", IdUnitOut),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDataTypeFilter

        public DB_DATA_TYPE[] GetDataTypeFilter(long[] IdDataType, string Name, long[] IdDescr, int[] IdDataTypeClass, int[] IdReferenceType, int[] IdDataTypeFormat,
                            bool? IsArchiveOnly, bool? IsRemoteRead, bool? IsRemoteWrite, bool? IsEditable, int[] IdUnit,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeFilter"));

            return (DB_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataTypeFilter",
                new DB.AnalyzeDataSet(GetDataType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_DATA_TYPE_CLASS", IdDataTypeClass),
                    CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                    CreateTableParam("@ID_DATA_TYPE_FORMAT", IdDataTypeFormat),
                    new DB.InParameter("@IS_ARCHIVE_ONLY", IsArchiveOnly),
                    new DB.InParameter("@IS_REMOTE_READ", IsRemoteRead),
                    new DB.InParameter("@IS_REMOTE_WRITE", IsRemoteWrite),
                    new DB.InParameter("@IS_EDITABLE", IsEditable),
                    CreateTableParam("@ID_UNIT", IdUnit),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTypeClassFilter

        public DB_DATA_TYPE_CLASS[] GetDataTypeClassFilter(int[] IdDataTypeClass, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeClassFilter"));

            return (DB_DATA_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDataTypeClassFilter",
                new DB.AnalyzeDataSet(GetDataTypeClass),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE_CLASS", IdDataTypeClass),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTypeFormatFilter

        public DB_DATA_TYPE_FORMAT[] GetDataTypeFormatFilter(int[] IdDataTypeFormat, long[] IdDescr, int[] TextMinLength, int[] TextMaxLength, int[] NumberMinPrecision, int[] NumberMaxPrecision,
                            int[] NumberMinScale, int[] NumberMaxScale, Double[] NumberMinValue, Double[] NumberMaxValue, string DatetimeFormat,
                            string RegularExpression, bool? IsRequired, int[] IdUniqueType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeFormatFilter"));

            return (DB_DATA_TYPE_FORMAT[])DB.ExecuteProcedure(
                "imrse_GetDataTypeFormatFilter",
                new DB.AnalyzeDataSet(GetDataTypeFormat),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE_FORMAT", IdDataTypeFormat),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@TEXT_MIN_LENGTH", TextMinLength),
                    CreateTableParam("@TEXT_MAX_LENGTH", TextMaxLength),
                    CreateTableParam("@NUMBER_MIN_PRECISION", NumberMinPrecision),
                    CreateTableParam("@NUMBER_MAX_PRECISION", NumberMaxPrecision),
                    CreateTableParam("@NUMBER_MIN_SCALE", NumberMinScale),
                    CreateTableParam("@NUMBER_MAX_SCALE", NumberMaxScale),
                    CreateTableParam("@NUMBER_MIN_VALUE", NumberMinValue),
                    CreateTableParam("@NUMBER_MAX_VALUE", NumberMaxValue),
                    new DB.InParameter("@DATETIME_FORMAT", DatetimeFormat),
                    new DB.InParameter("@REGULAR_EXPRESSION", RegularExpression),
                    new DB.InParameter("@IS_REQUIRED", IsRequired),
                    CreateTableParam("@ID_UNIQUE_TYPE", IdUniqueType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTypeGroupFilter

        public DB_DATA_TYPE_GROUP[] GetDataTypeGroupFilter(int[] IdDataTypeGroup, int[] IdDataTypeGroupType, int[] IdReferenceType, string Name, int[] IdParentGroup, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeGroupFilter"));

            return (DB_DATA_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroupFilter",
                new DB.AnalyzeDataSet(GetDataTypeGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE_GROUP", IdDataTypeGroup),
                    CreateTableParam("@ID_DATA_TYPE_GROUP_TYPE", IdDataTypeGroupType),
                    CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_PARENT_GROUP", IdParentGroup),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTypeGroupTypeFilter

        public DB_DATA_TYPE_GROUP_TYPE[] GetDataTypeGroupTypeFilter(int[] IdDataTypeGroupType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeGroupTypeFilter"));

            return (DB_DATA_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroupTypeFilter",
                new DB.AnalyzeDataSet(GetDataTypeGroupType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE_GROUP_TYPE", IdDataTypeGroupType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTypeInGroupFilter

        public DB_DATA_TYPE_IN_GROUP[] GetDataTypeInGroupFilter(int[] IdDataTypeGroup, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeInGroupFilter"));

            return (DB_DATA_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeInGroupFilter",
                new DB.AnalyzeDataSet(GetDataTypeInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_TYPE_GROUP", IdDataTypeGroup),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataTransferFilter

        public DB_DATA_TRANSFER[] GetDataTransferFilter(long[] IdDataTransfer, long[] BatchId, int[] ChangeType, int[] ChangeDirection, int[] IdSourceServer, int[] IdDestinationServer,
                            string TableName, long[] Key1, long[] Key2, long[] Key3, long[] Key4,
                            long[] Key5, long[] Key6, string ColumnName, TypeDateTimeCode InsertTime, TypeDateTimeCode ProceedTime,
                            string User, string Host, string Application, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTransferFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDataTransfer != null && IdDataTransfer.Length > MaxItemsPerSqlTableType) ||
                (BatchId != null && BatchId.Length > MaxItemsPerSqlTableType) ||
                (ChangeType != null && ChangeType.Length > MaxItemsPerSqlTableType) ||
                (ChangeDirection != null && ChangeDirection.Length > MaxItemsPerSqlTableType) ||
                (IdSourceServer != null && IdSourceServer.Length > MaxItemsPerSqlTableType) ||
                (IdDestinationServer != null && IdDestinationServer.Length > MaxItemsPerSqlTableType) ||
                (Key1 != null && Key1.Length > MaxItemsPerSqlTableType) ||
                (Key2 != null && Key2.Length > MaxItemsPerSqlTableType) ||
                (Key3 != null && Key3.Length > MaxItemsPerSqlTableType) ||
                (Key4 != null && Key4.Length > MaxItemsPerSqlTableType) ||
                (Key5 != null && Key5.Length > MaxItemsPerSqlTableType) ||
                (Key6 != null && Key6.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataTransfer != null) args.AddRange(IdDataTransfer.Select(s => new Tuple<int, object>(1, s)));
                if (BatchId != null) args.AddRange(BatchId.Select(s => new Tuple<int, object>(2, s)));
                if (ChangeType != null) args.AddRange(ChangeType.Select(s => new Tuple<int, object>(3, s)));
                if (ChangeDirection != null) args.AddRange(ChangeDirection.Select(s => new Tuple<int, object>(4, s)));
                if (IdSourceServer != null) args.AddRange(IdSourceServer.Select(s => new Tuple<int, object>(5, s)));
                if (IdDestinationServer != null) args.AddRange(IdDestinationServer.Select(s => new Tuple<int, object>(6, s)));
                if (Key1 != null) args.AddRange(Key1.Select(s => new Tuple<int, object>(8, s)));
                if (Key2 != null) args.AddRange(Key2.Select(s => new Tuple<int, object>(9, s)));
                if (Key3 != null) args.AddRange(Key3.Select(s => new Tuple<int, object>(10, s)));
                if (Key4 != null) args.AddRange(Key4.Select(s => new Tuple<int, object>(11, s)));
                if (Key5 != null) args.AddRange(Key5.Select(s => new Tuple<int, object>(12, s)));
                if (Key6 != null) args.AddRange(Key6.Select(s => new Tuple<int, object>(13, s)));

                DB_DATA_TRANSFER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DATA_TRANSFER[])DB.ExecuteProcedure(
                        "imrse_GetDataTransferFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDataTransfer),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TABLE_NAME", TableName),
                    new DB.InParameter("@COLUMN_NAME", ColumnName),
                    CreateTableParam("@INSERT_TIME_CODE", InsertTime),
                    CreateTableParam("@PROCEED_TIME_CODE", ProceedTime),
                    new DB.InParameter("@USER", User),
                    new DB.InParameter("@HOST", Host),
                    new DB.InParameter("@APPLICATION", Application),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_DATA_TRANSFER[])DB.ExecuteProcedure(
                    "imrse_GetDataTransferFilter",
                    new DB.AnalyzeDataSet(GetDataTransfer),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DATA_TRANSFER", IdDataTransfer),
                CreateTableParam("@BATCH_ID", BatchId),
                CreateTableParam("@CHANGE_TYPE", ChangeType),
                CreateTableParam("@CHANGE_DIRECTION", ChangeDirection),
                CreateTableParam("@ID_SOURCE_SERVER", IdSourceServer),
                CreateTableParam("@ID_DESTINATION_SERVER", IdDestinationServer),
                new DB.InParameter("@TABLE_NAME", TableName),
                CreateTableParam("@KEY1", Key1),
                CreateTableParam("@KEY2", Key2),
                CreateTableParam("@KEY3", Key3),
                CreateTableParam("@KEY4", Key4),
                CreateTableParam("@KEY5", Key5),
                CreateTableParam("@KEY6", Key6),
                new DB.InParameter("@COLUMN_NAME", ColumnName),
                CreateTableParam("@INSERT_TIME_CODE", InsertTime),
                CreateTableParam("@PROCEED_TIME_CODE", ProceedTime),
                new DB.InParameter("@USER", User),
                new DB.InParameter("@HOST", Host),
                new DB.InParameter("@APPLICATION", Application),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDaylightSavingTimeFilter

        public DB_DAYLIGHT_SAVING_TIME[] GetDaylightSavingTimeFilter(int[] IdDaylightSaving, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDaylightSavingTimeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDaylightSaving != null && IdDaylightSaving.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDaylightSaving != null) args.AddRange(IdDaylightSaving.Select(s => new Tuple<int, object>(1, s)));

                DB_DAYLIGHT_SAVING_TIME[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DAYLIGHT_SAVING_TIME[])DB.ExecuteProcedure(
                        "imrse_GetDaylightSavingTimeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDaylightSavingTime),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_DAYLIGHT_SAVING_TIME[])DB.ExecuteProcedure(
                    "imrse_GetDaylightSavingTimeFilter",
                    new DB.AnalyzeDataSet(GetDaylightSavingTime),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DAYLIGHT_SAVING", IdDaylightSaving),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDeliveryFilter

        public DB_DELIVERY[] GetDeliveryFilter(int[] IdDelivery, string Name, string Description, string UploadedFile, TypeDateTimeCode CreationDate, int[] IdShippingList,
                            int[] IdOperator, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryFilter"));

            return (DB_DELIVERY[])DB.ExecuteProcedure(
                "imrse_GetDeliveryFilter",
                new DB.AnalyzeDataSet(GetDelivery),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DELIVERY", IdDelivery),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@UPLOADED_FILE", UploadedFile),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDepositoryElementFilter

        public DB_DEPOSITORY_ELEMENT[] GetDepositoryElementFilter(int[] IdDepositoryElement, long[] IdLocation, int[] IdTask, int[] IdPackage, long[] IdArticle, int[] IdDeviceTypeClass,
                            int[] IdDeviceStateType, long[] SerialNbr, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, bool? Removed,
                            bool? Ordered, bool? Accepted, string Notes, string ExternalSn, int[] IdReference,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepositoryElementFilter"));

            return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElementFilter",
                new DB.AnalyzeDataSet(GetDepositoryElement),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEPOSITORY_ELEMENT", IdDepositoryElement),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_TASK", IdTask),
                    CreateTableParam("@ID_PACKAGE", IdPackage),
                    CreateTableParam("@ID_ARTICLE", IdArticle),
                    CreateTableParam("@ID_DEVICE_TYPE_CLASS", IdDeviceTypeClass),
                    CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@REMOVED", Removed),
                    new DB.InParameter("@ORDERED", Ordered),
                    new DB.InParameter("@ACCEPTED", Accepted),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@EXTERNAL_SN", ExternalSn),
                    CreateTableParam("@ID_REFERENCE", IdReference),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDepositoryElementDataFilter

        public DB_DEPOSITORY_ELEMENT_DATA[] GetDepositoryElementDataFilter(long[] IdDepositoryElementData, int[] IdDepositoryElement, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepositoryElementDataFilter"));

            return (DB_DEPOSITORY_ELEMENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElementDataFilter",
                new DB.AnalyzeDataSet(GetDepositoryElementData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEPOSITORY_ELEMENT_DATA", IdDepositoryElementData),
                    CreateTableParam("@ID_DEPOSITORY_ELEMENT", IdDepositoryElement),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDepotFilter

        public DB_DEPOT[] GetDepotFilter(int[] IdDepot, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepotFilter"));

            return (DB_DEPOT[])DB.ExecuteProcedure(
                "imrse_GetDepotFilter",
                new DB.AnalyzeDataSet(GetDepot),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEPOT", IdDepot),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDescrFilter

        public DB_DESCR[] GetDescrFilter(long[] IdDescr, int[] IdLanguage, string Description, string ExtendedDescription, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDescrFilter"));

            return (DB_DESCR[])DB.ExecuteProcedure(
                "imrse_GetDescrFilter",
                new DB.AnalyzeDataSet(GetDescr),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_LANGUAGE", IdLanguage),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@EXTENDED_DESCRIPTION", ExtendedDescription),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceFilter
        public DB_DEVICE[] GetDeviceFilter(long[] SerialNbr, int[] IdDeviceType, long[] SerialNbrPattern, long[] IdDescrPattern, int[] IdDeviceOrderNumber, int[] IdDistributor,
                            int[] IdDeviceStateType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, SqlCommand sqlCommand = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceFilter"));

            if (UseBulkInsertSqlTableType && (
                ((SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) || (SerialNbr == null && DeviceFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdDeviceType != null && IdDeviceType.Length > MaxItemsPerSqlTableType) ||
                (SerialNbrPattern != null && SerialNbrPattern.Length > MaxItemsPerSqlTableType) ||
                (IdDescrPattern != null && IdDescrPattern.Length > MaxItemsPerSqlTableType) ||
                (IdDeviceOrderNumber != null && IdDeviceOrderNumber.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdDeviceStateType != null && IdDeviceStateType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(1, s)));
                else if (DeviceFilter.Length > 0) args.AddRange(DeviceFilter.Select(s => new Tuple<int, object>(1, s)));
                if (IdDeviceType != null) args.AddRange(IdDeviceType.Select(s => new Tuple<int, object>(2, s)));
                if (SerialNbrPattern != null) args.AddRange(SerialNbrPattern.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescrPattern != null) args.AddRange(IdDescrPattern.Select(s => new Tuple<int, object>(4, s)));
                if (IdDeviceOrderNumber != null) args.AddRange(IdDeviceOrderNumber.Select(s => new Tuple<int, object>(5, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(6, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(6, s)));
                if (IdDeviceStateType != null) args.AddRange(IdDeviceStateType.Select(s => new Tuple<int, object>(7, s)));

                DB_DEVICE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE[])DB.ExecuteProcedure(
                        "imrse_GetDeviceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDevice),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout, sqlCommand
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DEVICE[])DB.ExecuteProcedure(
                    "imrse_GetDeviceFilter",
                    new DB.AnalyzeDataSet(GetDevice),
                    new DB.Parameter[] {
                        CreateTableParam("@SERIAL_NBR", SerialNbr != null ? SerialNbr : DeviceFilter),
                        CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                        CreateTableParam("@SERIAL_NBR_PATTERN", SerialNbrPattern),
                        CreateTableParam("@ID_DESCR_PATTERN", IdDescrPattern),
                        CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                        CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout, sqlCommand
                );
            }
        }
        #endregion
        #region GetDeviceDetailsFilter

        public DB_DEVICE_DETAILS[] GetDeviceDetailsFilter(long[] IdDeviceDetails, long[] SerialNbr, long[] IdLocation, string FactoryNbr, TypeDateTimeCode ShippingDate, TypeDateTimeCode WarrantyDate,
                    string Phone, int[] IdDeviceType, int[] IdDeviceOrderNumber, int[] IdDistributor, int[] IdDeviceStateType, string ProductionDeviceOrderNumber, string DeviceBatchNumber,
                    int[] IdDistributorOwner,
                    long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDetailsFilter"));

            return (DB_DEVICE_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetDeviceDetailsFilter",
                new DB.AnalyzeDataSet(GetDeviceDetails),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DETAILS", IdDeviceDetails),
                    CreateTableParam("@SERIAL_NBR", SerialNbr != null ? SerialNbr : DeviceFilter),
                    CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                    new DB.InParameter("@FACTORY_NBR", FactoryNbr),
                    CreateTableParam("@SHIPPING_DATE_CODE", ShippingDate),
                    CreateTableParam("@WARRANTY_DATE_CODE", WarrantyDate),
                    new DB.InParameter("@PHONE", Phone),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
					//new DB.InParameter("@PRODUCTION_DEVICE_ORDER_NUMBER", SqlDbType.NVarChar, ProductionDeviceOrderNumber, 200),
					//new DB.InParameter("@DEVICE_BATCH_NUMBER", SqlDbType.NVarChar, DeviceBatchNumber, 200),
					//CreateTableParam("@ID_DISTRIBUTOR_OWNER", IdDistributorOwner),
					new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceDistributorHistoryFilter

        public DB_DEVICE_DISTRIBUTOR_HISTORY[] GetDeviceDistributorHistoryFilter(long[] IdDeviceDistributorHistory, long[] SerialNbr, int[] IdDistributor, int[] IdDistributorOwner, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                            string Notes, int[] IdOperator, int[] IdServiceReferenceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDistributorHistoryFilter"));

            return (DB_DEVICE_DISTRIBUTOR_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceDistributorHistoryFilter",
                new DB.AnalyzeDataSet(GetDeviceDistributorHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DISTRIBUTOR_HISTORY", IdDeviceDistributorHistory),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_DISTRIBUTOR_OWNER", IdDistributorOwner),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", IdServiceReferenceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceDriverFilter

        public DB_DEVICE_DRIVER[] GetDeviceDriverFilter(int[] IdDeviceDriver, string Name, long[] IdDescr, string PluginName,
                            bool? AutoUpdate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverFilter"));

            return (DB_DEVICE_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverFilter",
                new DB.AnalyzeDataSet(GetDeviceDriver),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DRIVER", IdDeviceDriver),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@PLUGIN_NAME", PluginName),
                    new DB.InParameter("@AUTO_UPDATE", AutoUpdate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceDriverDataFilter

        public DB_DEVICE_DRIVER_DATA[] GetDeviceDriverDataFilter(long[] IdDeviceDriverData, int[] IdDeviceDriver, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverDataFilter"));

            return (DB_DEVICE_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverDataFilter",
                new DB.AnalyzeDataSet(GetDeviceDriverData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DRIVER_DATA", IdDeviceDriverData),
                    CreateTableParam("@ID_DEVICE_DRIVER", IdDeviceDriver),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceDriverMemoryMapFilter

        public DB_DEVICE_DRIVER_MEMORY_MAP[] GetDeviceDriverMemoryMapFilter(int[] IdDeviceDriverMemoryMap, int[] IdDeviceDriver, string FileName, string FileVersion, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverMemoryMapFilter"));

            return (DB_DEVICE_DRIVER_MEMORY_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMapFilter",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMap),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP", IdDeviceDriverMemoryMap),
                    CreateTableParam("@ID_DEVICE_DRIVER", IdDeviceDriver),
                    new DB.InParameter("@FILE_NAME", FileName),
                    new DB.InParameter("@FILE_VERSION", FileVersion),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceDriverMemoryMapDataFilter

        public DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] GetDeviceDriverMemoryMapDataFilter(long[] IdDeviceDriverMemoryMapData, long[] IdDdmmdParent, int[] IdDeviceDriverMemoryMap, int[] IdOmb, string Name, long[] IdDataType,
                            int[] IndexNbr, bool? IsRead, bool? IsWrite, bool? IsPutDataOmb, long[] IdDdmmdPutData,
                    long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverMemoryMapDataFilter"));

            return (DB_DEVICE_DRIVER_MEMORY_MAP_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMapDataFilter",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMapData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP_DATA", IdDeviceDriverMemoryMapData),
                    CreateTableParam("@ID_DDMMD_PARENT", IdDdmmdParent),
                    CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP", IdDeviceDriverMemoryMap),
                    CreateTableParam("@ID_OMB", IdOmb),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@IS_READ", IsRead),
                    new DB.InParameter("@IS_WRITE", IsWrite),
                    new DB.InParameter("@IS_PUT_DATA_OMB", IsPutDataOmb),
                    CreateTableParam("@ID_DDMMD_PUT_DATA", IdDdmmdPutData),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceHierarchyGroupFilter

        public DB_DEVICE_HIERARCHY_GROUP[] GetDeviceHierarchyGroupFilter(long[] IdDeviceHierarchyGroup, string DeviceHierarchySerialNbr, int[] IdDistributor, long[] IdDepositoryLocation, TypeDateTimeCode CreationDate, TypeDateTimeCode FinishDate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyGroupFilter"));

            return (DB_DEVICE_HIERARCHY_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyGroupFilter",
                new DB.AnalyzeDataSet(GetDeviceHierarchyGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", IdDeviceHierarchyGroup),
                    new DB.InParameter("@DEVICE_HIERARCHY_SERIAL_NBR", DeviceHierarchySerialNbr),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_DEPOSITORY_LOCATION", IdDepositoryLocation),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@FINISH_DATE_CODE", FinishDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceHierarchyInGroupFilter

        public DB_DEVICE_HIERARCHY_IN_GROUP[] GetDeviceHierarchyInGroupFilter(long[] IdDeviceHierarchyGroup, long[] IdDeviceHierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyInGroupFilter"));

            return (DB_DEVICE_HIERARCHY_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyInGroupFilter",
                new DB.AnalyzeDataSet(GetDeviceHierarchyInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", IdDeviceHierarchyGroup),
                    CreateTableParam("@ID_DEVICE_HIERARCHY", IdDeviceHierarchy),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceHierarchyPatternFilter

        public DB_DEVICE_HIERARCHY_PATTERN[] GetDeviceHierarchyPatternFilter(long[] IdDeviceHierarchyPattern, int[] IdDeviceTypeParent, int[] IdSlotType, int[] SlotNbrMin, int[] SlotNbrMax, int[] IdProtocolIn,
                            int[] IdProtocolOut, int[] IdDeviceType, int[] IdDeviceTypeGroup, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyPatternFilter"));

            return (DB_DEVICE_HIERARCHY_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyPatternFilter",
                new DB.AnalyzeDataSet(GetDeviceHierarchyPattern),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_HIERARCHY_PATTERN", IdDeviceHierarchyPattern),
                    CreateTableParam("@ID_DEVICE_TYPE_PARENT", IdDeviceTypeParent),
                    CreateTableParam("@ID_SLOT_TYPE", IdSlotType),
                    CreateTableParam("@SLOT_NBR_MIN", SlotNbrMin),
                    CreateTableParam("@SLOT_NBR_MAX", SlotNbrMax),
                    CreateTableParam("@ID_PROTOCOL_IN", IdProtocolIn),
                    CreateTableParam("@ID_PROTOCOL_OUT", IdProtocolOut),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    CreateTableParam("@ID_DEVICE_TYPE_GROUP", IdDeviceTypeGroup),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceOrderNumberFilter

        public DB_DEVICE_ORDER_NUMBER[] GetDeviceOrderNumberFilter(int[] IdDeviceOrderNumber, string Name, string FirstQuarter, string SecondQuarter, string ThirdQuarter, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberFilter"));

            return (DB_DEVICE_ORDER_NUMBER[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberFilter",
                new DB.AnalyzeDataSet(GetDeviceOrderNumber),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@FIRST_QUARTER", FirstQuarter),
                    new DB.InParameter("@SECOND_QUARTER", SecondQuarter),
                    new DB.InParameter("@THIRD_QUARTER", ThirdQuarter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceOrderNumberDataFilter

        public DB_DEVICE_ORDER_NUMBER_DATA[] GetDeviceOrderNumberDataFilter(long[] IdDeviceOrderNumberData, int[] IdDeviceOrderNumber, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberDataFilter"));

            return (DB_DEVICE_ORDER_NUMBER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberDataFilter",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER_DATA", IdDeviceOrderNumberData),
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceOrderNumberInArticleFilter

        public DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] GetDeviceOrderNumberInArticleFilter(int[] IdDeviceOrderNumber, long[] IdArticle, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberInArticleFilter"));

            return (DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberInArticleFilter",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberInArticle),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@ID_ARTICLE", IdArticle),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDevicePatternFilter

        public DB_DEVICE_PATTERN[] GetDevicePatternFilter(long[] IdPattern, long[] SerialNbr, int[] IdDistributor, string Name, TypeDateTimeCode CreationDate, int[] CreatedBy,
                            int[] IdTemplate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDevicePatternFilter"));

            return (DB_DEVICE_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDevicePatternFilter",
                new DB.AnalyzeDataSet(GetDevicePattern),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PATTERN", IdPattern),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@CREATED_BY", CreatedBy),
                    CreateTableParam("@ID_TEMPLATE", IdTemplate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDevicePatternDataFilter

        public DB_DEVICE_PATTERN_DATA[] GetDevicePatternDataFilter(long[] IdPattern, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDevicePatternDataFilter"));

            return (DB_DEVICE_PATTERN_DATA[])DB.ExecuteProcedure(
                "imrse_GetDevicePatternDataFilter",
                new DB.AnalyzeDataSet(GetDevicePatternData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PATTERN", IdPattern),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceSimCardHistoryFilter

        public DB_DEVICE_SIM_CARD_HISTORY[] GetDeviceSimCardHistoryFilter(long[] IdDeviceSimCardHistory, long[] SerialNbr, int[] IdSimCard, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, string Notes,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceSimCardHistoryFilter"));

            return (DB_DEVICE_SIM_CARD_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceSimCardHistoryFilter",
                new DB.AnalyzeDataSet(GetDeviceSimCardHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_SIM_CARD_HISTORY", IdDeviceSimCardHistory),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_SIM_CARD", IdSimCard),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceStateHistoryFilter

        public DB_DEVICE_STATE_HISTORY[] GetDeviceStateHistoryFilter(long[] IdDeviceState, long[] SerialNbr, int[] IdDeviceStateType, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, string Notes,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceStateHistoryFilter"));
            if (UseBulkInsertSqlTableType && (
                (IdDeviceState != null && IdDeviceState.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDeviceStateType != null && IdDeviceStateType.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDeviceState != null) args.AddRange(IdDeviceState.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdDeviceStateType != null) args.AddRange(IdDeviceStateType.Select(s => new Tuple<int, object>(3, s)));

                DB_DEVICE_STATE_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetDeviceStateHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDeviceStateHistory),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_TIME_CODE", StartTime),
                            CreateTableParam("@END_TIME_CODE", EndTime),
                            new DB.InParameter("@NOTES", Notes),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetDeviceStateHistoryFilter",
                    new DB.AnalyzeDataSet(GetDeviceStateHistory),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_STATE", IdDeviceState),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetDeviceStateTypeFilter

        public DB_DEVICE_STATE_TYPE[] GetDeviceStateTypeFilter(int[] IdDeviceStateType, string Name, long[] IdDescr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceStateTypeFilter"));

            return (DB_DEVICE_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceStateTypeFilter",
                new DB.AnalyzeDataSet(GetDeviceStateType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTemplateFilter

        public DB_DEVICE_TEMPLATE[] GetDeviceTemplateFilter(int[] IdTemplate, int[] IdTemplatePattern, int[] IdDeviceType, string FirmwareVersion, string Name, int[] CreatedBy,
                            TypeDateTimeCode CreationDate, int[] ConfirmedBy, TypeDateTimeCode ConfirmDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTemplateFilter"));

            return (DB_DEVICE_TEMPLATE[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplateFilter",
                new DB.AnalyzeDataSet(GetDeviceTemplate),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TEMPLATE", IdTemplate),
                    CreateTableParam("@ID_TEMPLATE_PATTERN", IdTemplatePattern),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    new DB.InParameter("@FIRMWARE_VERSION", FirmwareVersion),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@CREATED_BY", CreatedBy),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@CONFIRMED_BY", ConfirmedBy),
                    CreateTableParam("@CONFIRM_DATE_CODE", ConfirmDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTemplateDataFilter

        public DB_DEVICE_TEMPLATE_DATA[] GetDeviceTemplateDataFilter(int[] IdTemplate, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTemplateDataFilter"));

            return (DB_DEVICE_TEMPLATE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplateDataFilter",
                new DB.AnalyzeDataSet(GetDeviceTemplateData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TEMPLATE", IdTemplate),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeFilter

        public DB_DEVICE_TYPE[] GetDeviceTypeFilter(int[] IdDeviceType, string Name, int[] IdDeviceTypeClass, long[] IdDescr, bool? TwoWayTransAvailable, int[] IdDeviceDriver,
                            int[] IdProtocol, int[] DefaultIdDeviceOderNumber, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeFilter"));

            return (DB_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeFilter",
                new DB.AnalyzeDataSet(GetDeviceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DEVICE_TYPE_CLASS", IdDeviceTypeClass),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TWO_WAY_TRANS_AVAILABLE", TwoWayTransAvailable),
                    CreateTableParam("@ID_DEVICE_DRIVER", IdDeviceDriver),
                    CreateTableParam("@ID_PROTOCOL", IdProtocol),
                    CreateTableParam("@DEFAULT_ID_DEVICE_ODER_NUMBER", DefaultIdDeviceOderNumber),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeClassFilter

        public DB_DEVICE_TYPE_CLASS[] GetDeviceTypeClassFilter(int[] IdDeviceTypeClass, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeClassFilter"));

            return (DB_DEVICE_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeClassFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeClass),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_CLASS", IdDeviceTypeClass),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeDataFilter

        public DB_DEVICE_TYPE_DATA[] GetDeviceTypeDataFilter(long[] IdDeviceTypeData, int[] IdDeviceType, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeDataFilter"));

            return (DB_DEVICE_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeDataFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_DATA", IdDeviceTypeData),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeGroupFilter

        public DB_DEVICE_TYPE_GROUP[] GetDeviceTypeGroupFilter(int[] IdDeviceTypeGroup, string Name, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeGroupFilter"));

            return (DB_DEVICE_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeGroupFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_GROUP", IdDeviceTypeGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeInGroupFilter

        public DB_DEVICE_TYPE_IN_GROUP[] GetDeviceTypeInGroupFilter(int[] IdDeviceTypeGroup, int[] IdDeviceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeInGroupFilter"));

            return (DB_DEVICE_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeInGroupFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_GROUP", IdDeviceTypeGroup),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeProfileMapFilter

        public DB_DEVICE_TYPE_PROFILE_MAP[] GetDeviceTypeProfileMapFilter(int[] IdDeviceTypeProfileMap, int[] IdDeviceType, int[] IdOperatorCreated, TypeDateTimeCode CreatedTime, int[] IdOperatorModified, TypeDateTimeCode ModifiedTime,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileMapFilter"));

            return (DB_DEVICE_TYPE_PROFILE_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMapFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMap),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", IdDeviceTypeProfileMap),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    CreateTableParam("@ID_OPERATOR_CREATED", IdOperatorCreated),
                    CreateTableParam("@CREATED_TIME_CODE", CreatedTime),
                    CreateTableParam("@ID_OPERATOR_MODIFIED", IdOperatorModified),
                    CreateTableParam("@MODIFIED_TIME_CODE", ModifiedTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeProfileMapConfigFilter

        public DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] GetDeviceTypeProfileMapConfigFilter(int[] IdDeviceTypeProfileMap, int[] IdDeviceTypeProfileStep, int[] IdDeviceTypeProfileStepKind, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileMapConfigFilter"));

            return (DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMapConfigFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMapConfig),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", IdDeviceTypeProfileMap),
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", IdDeviceTypeProfileStep),
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", IdDeviceTypeProfileStepKind),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeProfileStepFilter

        public DB_DEVICE_TYPE_PROFILE_STEP[] GetDeviceTypeProfileStepFilter(int[] IdDeviceTypeProfileStep, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileStepFilter"));

            return (DB_DEVICE_TYPE_PROFILE_STEP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStepFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStep),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", IdDeviceTypeProfileStep),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceTypeProfileStepKindFilter

        public DB_DEVICE_TYPE_PROFILE_STEP_KIND[] GetDeviceTypeProfileStepKindFilter(int[] IdDeviceTypeProfileStepKind, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileStepKindFilter"));

            return (DB_DEVICE_TYPE_PROFILE_STEP_KIND[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStepKindFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStepKind),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", IdDeviceTypeProfileStepKind),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceWarrantyFilter

        public DB_DEVICE_WARRANTY[] GetDeviceWarrantyFilter(int[] IdDeviceWarranty, int[] IdShippingList, long[] SerialNbr, int[] IdComponent, int[] IdContract, int[] WarrantyLength,
                            TypeDateTimeCode ShippingDate, TypeDateTimeCode InstallationDate, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceWarrantyFilter"));

            return (DB_DEVICE_WARRANTY[])DB.ExecuteProcedure(
                "imrse_GetDeviceWarrantyFilter",
                new DB.AnalyzeDataSet(GetDeviceWarranty),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_WARRANTY", IdDeviceWarranty),
                    CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_COMPONENT", IdComponent),
                    CreateTableParam("@ID_CONTRACT", IdContract),
                    CreateTableParam("@WARRANTY_LENGTH", WarrantyLength),
                    CreateTableParam("@SHIPPING_DATE_CODE", ShippingDate),
                    CreateTableParam("@INSTALLATION_DATE_CODE", InstallationDate),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDiagnosticActionFilter

        public DB_DIAGNOSTIC_ACTION[] GetDiagnosticActionFilter(int[] IdDiagnosticAction, int[] IdDeviceOrderNumber, int[] IdDistributor, string Name, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                            long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionFilter"));

            return (DB_DIAGNOSTIC_ACTION[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionFilter",
                new DB.AnalyzeDataSet(GetDiagnosticAction),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION", IdDiagnosticAction),
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),// != null ? IdDistributor : DistributorFilter),
					new DB.InParameter("@NAME", Name),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDiagnosticActionDataFilter

        public DB_DIAGNOSTIC_ACTION_DATA[] GetDiagnosticActionDataFilter(long[] IdDiagnosticActionData, int[] IdDiagnosticAction, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionDataFilter"));

            return (DB_DIAGNOSTIC_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionDataFilter",
                new DB.AnalyzeDataSet(GetDiagnosticActionData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION_DATA", IdDiagnosticActionData),
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION", IdDiagnosticAction),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDiagnosticActionInServiceFilter

        public DB_DIAGNOSTIC_ACTION_IN_SERVICE[] GetDiagnosticActionInServiceFilter(int[] IdService, int[] IdDiagnosticAction, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionInServiceFilter"));

            return (DB_DIAGNOSTIC_ACTION_IN_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionInServiceFilter",
                new DB.AnalyzeDataSet(GetDiagnosticActionInService),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE", IdService),
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION", IdDiagnosticAction),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDiagnosticActionResultFilter

        public DB_DIAGNOSTIC_ACTION_RESULT[] GetDiagnosticActionResultFilter(int[] IdDiagnosticActionResult, int[] IdDiagnosticAction, string Name, bool? AllowInputText, int[] DamageLevel, long[] IdDescr,
                            int[] IdServiceReferenceType, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionResultFilter"));

            return (DB_DIAGNOSTIC_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionResultFilter",
                new DB.AnalyzeDataSet(GetDiagnosticActionResult),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION_RESULT", IdDiagnosticActionResult),
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION", IdDiagnosticAction),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@ALLOW_INPUT_TEXT", AllowInputText),
                    CreateTableParam("@DAMAGE_LEVEL", DamageLevel),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", IdServiceReferenceType),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDistributorFilter

        public DB_DISTRIBUTOR[] GetDistributorFilter(int[] IdDistributor, string Name, string City, string Address, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorFilter"));

            return (DB_DISTRIBUTOR[])DB.ExecuteProcedure(
                "imrse_GetDistributorFilter",
                new DB.AnalyzeDataSet(GetDistributor),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@CITY", City),
                    new DB.InParameter("@ADDRESS", Address),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDistributorDataFilter

        public DB_DISTRIBUTOR_DATA[] GetDistributorDataFilter(long[] IdDistributorData, int[] IdDistributor, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorDataFilter"));

            return (DB_DISTRIBUTOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetDistributorDataFilter",
                new DB.AnalyzeDataSet(GetDistributorData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR_DATA", IdDistributorData),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDistributorHierarchyFilter

        public DB_DISTRIBUTOR_HIERARCHY[] GetDistributorHierarchyFilter(int[] IdDistributorHierarchy, int[] IdDistributorHierarchyParent, int[] IdDistributor, string Hierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorHierarchyFilter"));

            return (DB_DISTRIBUTOR_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetDistributorHierarchyFilter",
                new DB.AnalyzeDataSet(GetDistributorHierarchy),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR_HIERARCHY", IdDistributorHierarchy),
                    CreateTableParam("@ID_DISTRIBUTOR_HIERARCHY_PARENT", IdDistributorHierarchyParent),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@HIERARCHY", Hierarchy),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDistributorSupplierFilter

        public DB_DISTRIBUTOR_SUPPLIER[] GetDistributorSupplierFilter(int[] IdDistributorSupplier, int[] IdDistributor, int[] IdSupplier, int[] IdProductCode, int[] IdActor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorSupplierFilter"));

            return (DB_DISTRIBUTOR_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetDistributorSupplierFilter",
                new DB.AnalyzeDataSet(GetDistributorSupplier),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR_SUPPLIER", IdDistributorSupplier),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_SUPPLIER", IdSupplier),
                    CreateTableParam("@ID_PRODUCT_CODE", IdProductCode),
                    CreateTableParam("@ID_ACTOR", IdActor),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetErrorLogFilter

        public DB_ERROR_LOG[] GetErrorLogFilter(int[] IdErrorLog, int[] ApplicationId, TypeDateTimeCode ErrorDate, string ClassName, string FunctionName, string Source,
                            string Message, string StackTrace, string Description, int[] Severity, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetErrorLogFilter"));

            return (DB_ERROR_LOG[])DB.ExecuteProcedure(
                "imrse_GetErrorLogFilter",
                new DB.AnalyzeDataSet(GetErrorLog),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ERROR_LOG", IdErrorLog),
                    CreateTableParam("@APPLICATION_ID", ApplicationId),
                    CreateTableParam("@ERROR_DATE_CODE", ErrorDate),
                    new DB.InParameter("@CLASS_NAME", ClassName),
                    new DB.InParameter("@FUNCTION_NAME", FunctionName),
                    new DB.InParameter("@SOURCE", Source),
                    new DB.InParameter("@MESSAGE", Message),
                    new DB.InParameter("@STACK_TRACE", StackTrace),
                    new DB.InParameter("@DESCRIPTION", Description),
                    CreateTableParam("@SEVERITY", Severity),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetEtlFilter

        public DB_ETL[] GetEtlFilter(int[] IdParam, string Code, string Descr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdParam != null && IdParam.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdParam != null) args.AddRange(IdParam.Select(s => new Tuple<int, object>(1, s)));

                DB_ETL[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ETL[])DB.ExecuteProcedure(
                        "imrse_GetEtlFilterTableArgs",
                        new DB.AnalyzeDataSet(GetEtl),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@CODE", Code),
                    new DB.InParameter("@DESCR", Descr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_ETL[])DB.ExecuteProcedure(
                    "imrse_GetEtlFilter",
                    new DB.AnalyzeDataSet(GetEtl),
                    new DB.Parameter[] {
                CreateTableParam("@ID_PARAM", IdParam),
                new DB.InParameter("@CODE", Code),
                new DB.InParameter("@DESCR", Descr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetFaultCodeFilter

        public DB_FAULT_CODE[] GetFaultCodeFilter(int[] IdFaultCode, int[] IdFaultCodeType, int[] IdFaultCodeParent, string Name, long[] IdClientDescr, long[] IdCompanyDescr,
                            int[] Priority, string FaultCodeHierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeFilter"));

            return (DB_FAULT_CODE[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeFilter",
                new DB.AnalyzeDataSet(GetFaultCode),
                new DB.Parameter[] {
                    CreateTableParam("@ID_FAULT_CODE", IdFaultCode),
                    CreateTableParam("@ID_FAULT_CODE_TYPE", IdFaultCodeType),
                    CreateTableParam("@ID_FAULT_CODE_PARENT", IdFaultCodeParent),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_CLIENT_DESCR", IdClientDescr),
                    CreateTableParam("@ID_COMPANY_DESCR", IdCompanyDescr),
                    CreateTableParam("@PRIORITY", Priority),
                    new DB.InParameter("@FAULT_CODE_HIERARCHY", FaultCodeHierarchy),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetFaultCodeFilter

        public DB_FAULT_CODE[] GetFaultCodeFilter(int[] IdFaultCode, int[] IdFaultCodeType, string Name, long[] IdClientDescr, long[] IdCompanyDescr, int[] Priority,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdFaultCode != null && IdFaultCode.Length > MaxItemsPerSqlTableType) ||
                (IdFaultCodeType != null && IdFaultCodeType.Length > MaxItemsPerSqlTableType) ||
                (IdClientDescr != null && IdClientDescr.Length > MaxItemsPerSqlTableType) ||
                (IdCompanyDescr != null && IdCompanyDescr.Length > MaxItemsPerSqlTableType) ||
                (Priority != null && Priority.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdFaultCode != null) args.AddRange(IdFaultCode.Select(s => new Tuple<int, object>(1, s)));
                if (IdFaultCodeType != null) args.AddRange(IdFaultCodeType.Select(s => new Tuple<int, object>(2, s)));
                if (IdClientDescr != null) args.AddRange(IdClientDescr.Select(s => new Tuple<int, object>(4, s)));
                if (IdCompanyDescr != null) args.AddRange(IdCompanyDescr.Select(s => new Tuple<int, object>(5, s)));
                if (Priority != null) args.AddRange(Priority.Select(s => new Tuple<int, object>(6, s)));

                DB_FAULT_CODE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_FAULT_CODE[])DB.ExecuteProcedure(
                        "imrse_GetFaultCodeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetFaultCode),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_FAULT_CODE[])DB.ExecuteProcedure(
                    "imrse_GetFaultCodeFilter",
                    new DB.AnalyzeDataSet(GetFaultCode),
                    new DB.Parameter[] {
                CreateTableParam("@ID_FAULT_CODE", IdFaultCode),
                CreateTableParam("@ID_FAULT_CODE_TYPE", IdFaultCodeType),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_CLIENT_DESCR", IdClientDescr),
                CreateTableParam("@ID_COMPANY_DESCR", IdCompanyDescr),
                CreateTableParam("@PRIORITY", Priority),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetFaultCodeDataFilter

        public DB_FAULT_CODE_DATA[] GetFaultCodeDataFilter(long[] IdFaultCodeData, int[] IdFaultCode, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdFaultCodeData != null && IdFaultCodeData.Length > MaxItemsPerSqlTableType) ||
                (IdFaultCode != null && IdFaultCode.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdFaultCodeData != null) args.AddRange(IdFaultCodeData.Select(s => new Tuple<int, object>(1, s)));
                if (IdFaultCode != null) args.AddRange(IdFaultCode.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_FAULT_CODE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_FAULT_CODE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetFaultCodeDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetFaultCodeData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_FAULT_CODE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetFaultCodeDataFilter",
                    new DB.AnalyzeDataSet(GetFaultCodeData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_FAULT_CODE_DATA", IdFaultCodeData),
                CreateTableParam("@ID_FAULT_CODE", IdFaultCode),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetFaultCodeSummaryFilter

        public DB_FAULT_CODE_SUMMARY[] GetFaultCodeSummaryFilter(int[] IdFaultCodeSummary, int[] IdFaultCode, int[] IdServiceReferenceType, int[] FaultsCount, int[] TotalCount, int[] IdFaultCodeSummaryType,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeSummaryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdFaultCodeSummary != null && IdFaultCodeSummary.Length > MaxItemsPerSqlTableType) ||
                (IdFaultCode != null && IdFaultCode.Length > MaxItemsPerSqlTableType) ||
                (IdServiceReferenceType != null && IdServiceReferenceType.Length > MaxItemsPerSqlTableType) ||
                (FaultsCount != null && FaultsCount.Length > MaxItemsPerSqlTableType) ||
                (TotalCount != null && TotalCount.Length > MaxItemsPerSqlTableType) ||
                (IdFaultCodeSummaryType != null && IdFaultCodeSummaryType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdFaultCodeSummary != null) args.AddRange(IdFaultCodeSummary.Select(s => new Tuple<int, object>(1, s)));
                if (IdFaultCode != null) args.AddRange(IdFaultCode.Select(s => new Tuple<int, object>(2, s)));
                if (IdServiceReferenceType != null) args.AddRange(IdServiceReferenceType.Select(s => new Tuple<int, object>(3, s)));
                if (FaultsCount != null) args.AddRange(FaultsCount.Select(s => new Tuple<int, object>(4, s)));
                if (TotalCount != null) args.AddRange(TotalCount.Select(s => new Tuple<int, object>(5, s)));
                if (IdFaultCodeSummaryType != null) args.AddRange(IdFaultCodeSummaryType.Select(s => new Tuple<int, object>(6, s)));

                DB_FAULT_CODE_SUMMARY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_FAULT_CODE_SUMMARY[])DB.ExecuteProcedure(
                        "imrse_GetFaultCodeSummaryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetFaultCodeSummary),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_FAULT_CODE_SUMMARY[])DB.ExecuteProcedure(
                    "imrse_GetFaultCodeSummaryFilter",
                    new DB.AnalyzeDataSet(GetFaultCodeSummary),
                    new DB.Parameter[] {
                CreateTableParam("@ID_FAULT_CODE_SUMMARY", IdFaultCodeSummary),
                CreateTableParam("@ID_FAULT_CODE", IdFaultCode),
                CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", IdServiceReferenceType),
                CreateTableParam("@FAULTS_COUNT", FaultsCount),
                CreateTableParam("@TOTAL_COUNT", TotalCount),
                CreateTableParam("@ID_FAULT_CODE_SUMMARY_TYPE", IdFaultCodeSummaryType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetFaultCodeTypeFilter

        public DB_FAULT_CODE_TYPE[] GetFaultCodeTypeFilter(int[] IdFaultCodeType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdFaultCodeType != null && IdFaultCodeType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdFaultCodeType != null) args.AddRange(IdFaultCodeType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_FAULT_CODE_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_FAULT_CODE_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetFaultCodeTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetFaultCodeType),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_FAULT_CODE_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetFaultCodeTypeFilter",
                    new DB.AnalyzeDataSet(GetFaultCodeType),
                    new DB.Parameter[] {
                CreateTableParam("@ID_FAULT_CODE_TYPE", IdFaultCodeType),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetHolidayDateFilter

        public DB_HOLIDAY_DATE[] GetHolidayDateFilter(TypeDateTimeCode Date, string CountryCode, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetHolidayDateFilter"));

            return (DB_HOLIDAY_DATE[])DB.ExecuteProcedure(
                "imrse_GetHolidayDateFilter",
                new DB.AnalyzeDataSet(GetHolidayDate),
                new DB.Parameter[] {
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@COUNTRY_CODE", CountryCode),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetImrServerFilter

        public DB_IMR_SERVER[] GetImrServerFilter(int[] IdImrServer, string IpAddress, string DnsName, string WebserviceAddress, int[] WebserviceTimeout, string Login,
                            string Password, string StandardDescription, bool? IsGood, TypeDateTimeCode CreationDate, string UtdcSsecUser,
                            string UtdcSsecPass, string Collation, string DbName, int[] ImrServerVersion, bool? IsImrlt,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerFilter"));

            return (DB_IMR_SERVER[])DB.ExecuteProcedure(
                "imrse_GetImrServerFilter",
                new DB.AnalyzeDataSet(GetImrServer),
                new DB.Parameter[] {
                    CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                    new DB.InParameter("@IP_ADDRESS", IpAddress),
                    new DB.InParameter("@DNS_NAME", DnsName),
                    new DB.InParameter("@WEBSERVICE_ADDRESS", WebserviceAddress),
                    CreateTableParam("@WEBSERVICE_TIMEOUT", WebserviceTimeout),
                    new DB.InParameter("@LOGIN", Login),
                    new DB.InParameter("@PASSWORD", Password),
                    new DB.InParameter("@STANDARD_DESCRIPTION", StandardDescription),
                    new DB.InParameter("@IS_GOOD", IsGood),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@UTDC_SSEC_USER", UtdcSsecUser),
                    new DB.InParameter("@UTDC_SSEC_PASS", UtdcSsecPass),
                    new DB.InParameter("@COLLATION", Collation),
                    new DB.InParameter("@DB_NAME", DbName),
                    CreateTableParam("@IMR_SERVER_VERSION", ImrServerVersion),
                    new DB.InParameter("@IS_IMRLT", IsImrlt),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetImrServerDataFilter

        public DB_IMR_SERVER_DATA[] GetImrServerDataFilter(long[] IdImrServerData, int[] IdImrServer, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerDataFilter"));

            return (DB_IMR_SERVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetImrServerDataFilter",
                new DB.AnalyzeDataSet(GetImrServerData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_IMR_SERVER_DATA", IdImrServerData),
                    CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetIssueFilter
        public DB_ISSUE[] GetIssueFilter(int[] IdIssue, int[] IdActor, int[] IdOperatorRegistering, int[] IdOperatorPerformer, int[] IdIssueType, int[] IdIssueStatus,
                            int[] IdDistributor, TypeDateTimeCode RealizationDate, TypeDateTimeCode CreationDate, string ShortDescr, string LongDescr,
                            long[] IdLocation, TypeDateTimeCode Deadline, int[] IdPriority, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueFilter"));

            return (DB_ISSUE[])DB.ExecuteProcedure(
                "imrse_GetIssueFilter",
                new DB.AnalyzeDataSet(GetIssue),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ISSUE", IdIssue),
                    CreateTableParam("@ID_ACTOR", IdActor),
                    CreateTableParam("@ID_OPERATOR_REGISTERING", IdOperatorRegistering),
                    CreateTableParam("@ID_OPERATOR_PERFORMER", IdOperatorPerformer),
                    CreateTableParam("@ID_ISSUE_TYPE", IdIssueType),
                    CreateTableParam("@ID_ISSUE_STATUS", IdIssueStatus),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@REALIZATION_DATE_CODE", RealizationDate),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@SHORT_DESCR", ShortDescr),
                    new DB.InParameter("@LONG_DESCR", LongDescr),
                    CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                    CreateTableParam("@DEADLINE_CODE", Deadline),
                    CreateTableParam("@ID_PRIORITY", IdPriority),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetIssueDataFilter

        public DB_ISSUE_DATA[] GetIssueDataFilter(long[] IdIssueData, int[] IdIssue, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdIssueData != null && IdIssueData.Length > MaxItemsPerSqlTableType) ||
                (IdIssue != null && IdIssue.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdIssueData != null) args.AddRange(IdIssueData.Select(s => new Tuple<int, object>(1, s)));
                if (IdIssue != null) args.AddRange(IdIssue.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_ISSUE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ISSUE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetIssueDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetIssueData),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_ISSUE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetIssueDataFilter",
                    new DB.AnalyzeDataSet(GetIssueData),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_ISSUE_DATA", IdIssueData),
                    CreateTableParam("@ID_ISSUE", IdIssue),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetIssueGroupFilter

        public DB_ISSUE_GROUP[] GetIssueGroupFilter(int[] IdIssueGroup, int[] IdParentGroup, string Name, TypeDateTimeCode DateCreated, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueGroupFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdIssueGroup != null && IdIssueGroup.Length > MaxItemsPerSqlTableType) ||
                (IdParentGroup != null && IdParentGroup.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdIssueGroup != null) args.AddRange(IdIssueGroup.Select(s => new Tuple<int, object>(1, s)));
                if (IdParentGroup != null) args.AddRange(IdParentGroup.Select(s => new Tuple<int, object>(2, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(5, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(5, s)));

                DB_ISSUE_GROUP[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ISSUE_GROUP[])DB.ExecuteProcedure(
                        "imrse_GetIssueGroupFilterTableArgs",
                        new DB.AnalyzeDataSet(GetIssueGroup),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@DATE_CREATED_CODE", DateCreated),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_ISSUE_GROUP[])DB.ExecuteProcedure(
                    "imrse_GetIssueGroupFilter",
                    new DB.AnalyzeDataSet(GetIssueGroup),
                    new DB.Parameter[] {
                CreateTableParam("@ID_ISSUE_GROUP", IdIssueGroup),
                CreateTableParam("@ID_PARENT_GROUP", IdParentGroup),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@DATE_CREATED_CODE", DateCreated),
                CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetIssueHistoryFilter

        public DB_ISSUE_HISTORY[] GetIssueHistoryFilter(int[] IdIssueHistory, int[] IdIssue, int[] IdIssueStatus, int[] IdOperator, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                            string Notes, string ShortDescr, TypeDateTimeCode Deadline, int[] IdPriority, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueHistoryFilter"));

            return (DB_ISSUE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetIssueHistoryFilter",
                new DB.AnalyzeDataSet(GetIssueHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ISSUE_HISTORY", IdIssueHistory),
                    CreateTableParam("@ID_ISSUE", IdIssue),
                    CreateTableParam("@ID_ISSUE_STATUS", IdIssueStatus),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@SHORT_DESCR", ShortDescr),
                    CreateTableParam("@DEADLINE_CODE", Deadline),
                    CreateTableParam("@ID_PRIORITY", IdPriority),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetIssueStatusFilter

        public DB_ISSUE_STATUS[] GetIssueStatusFilter(int[] IdIssueStatus, long[] IdDescr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueStatusFilter"));

            return (DB_ISSUE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetIssueStatusFilter",
                new DB.AnalyzeDataSet(GetIssueStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ISSUE_STATUS", IdIssueStatus),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetIssueTypeFilter

        public DB_ISSUE_TYPE[] GetIssueTypeFilter(int[] IdIssueType, long[] IdDescr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueTypeFilter"));

            return (DB_ISSUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetIssueTypeFilter",
                new DB.AnalyzeDataSet(GetIssueType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ISSUE_TYPE", IdIssueType),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLanguageDataFilter

        public DB_LANGUAGE_DATA[] GetLanguageDataFilter(long[] IdLanguageData, int[] IdLanguage, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLanguageDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLanguageData != null && IdLanguageData.Length > MaxItemsPerSqlTableType) ||
                (IdLanguage != null && IdLanguage.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLanguageData != null) args.AddRange(IdLanguageData.Select(s => new Tuple<int, object>(1, s)));
                if (IdLanguage != null) args.AddRange(IdLanguage.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_LANGUAGE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LANGUAGE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetLanguageDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLanguageData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_LANGUAGE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetLanguageDataFilter",
                    new DB.AnalyzeDataSet(GetLanguageData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_LANGUAGE_DATA", IdLanguageData),
                CreateTableParam("@ID_LANGUAGE", IdLanguage),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLanguageFilter

        public DB_LANGUAGE[] GetLanguageFilter(int[] IdLanguage, string Name, string CultureCode, int[] CultureLangid, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLanguageFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLanguage != null && IdLanguage.Length > MaxItemsPerSqlTableType) ||
                (CultureLangid != null && CultureLangid.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLanguage != null) args.AddRange(IdLanguage.Select(s => new Tuple<int, object>(1, s)));
                if (CultureLangid != null) args.AddRange(CultureLangid.Select(s => new Tuple<int, object>(4, s)));

                DB_LANGUAGE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LANGUAGE[])DB.ExecuteProcedure(
                        "imrse_GetLanguageFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLanguage),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@CULTURE_CODE", CultureCode),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_LANGUAGE[])DB.ExecuteProcedure(
                    "imrse_GetLanguageFilter",
                    new DB.AnalyzeDataSet(GetLanguage),
                    new DB.Parameter[] {
                CreateTableParam("@ID_LANGUAGE", IdLanguage),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@CULTURE_CODE", CultureCode),
                CreateTableParam("@CULTURE_LANGID", CultureLangid),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLocationFilter
        public DB_LOCATION[] GetLocationFilter(long[] IdLocation, int[] IdLocationType, long[] IdLocationPattern, long[] IdDescrPattern, int[] IdDistributor, int[] IdConsumer,
                            int[] IdLocationStateType, bool? InKpi, bool? AllowGrouping, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationFilter"));

            if (UseBulkInsertSqlTableType && (
                ((IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) || (IdLocation == null && LocationFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdLocationType != null && IdLocationType.Length > MaxItemsPerSqlTableType) ||
                (IdLocationPattern != null && IdLocationPattern.Length > MaxItemsPerSqlTableType) ||
                (IdDescrPattern != null && IdDescrPattern.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdConsumer != null && IdConsumer.Length > MaxItemsPerSqlTableType) ||
                (IdLocationStateType != null && IdLocationStateType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(1, s)));
                else if (LocationFilter.Length > 0) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(1, s)));
                if (IdLocationType != null) args.AddRange(IdLocationType.Select(s => new Tuple<int, object>(2, s)));
                if (IdLocationPattern != null) args.AddRange(IdLocationPattern.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescrPattern != null) args.AddRange(IdDescrPattern.Select(s => new Tuple<int, object>(4, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(5, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(5, s)));
                if (IdConsumer != null) args.AddRange(IdConsumer.Select(s => new Tuple<int, object>(6, s)));
                if (IdLocationStateType != null) args.AddRange(IdLocationStateType.Select(s => new Tuple<int, object>(7, s)));

                DB_LOCATION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION[])DB.ExecuteProcedure(
                        "imrse_GetLocationFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocation),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@IN_KPI", InKpi),
                            new DB.InParameter("@ALLOW_GROUPING", AllowGrouping),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_LOCATION[])DB.ExecuteProcedure(
                    "imrse_GetLocationFilter",
                    new DB.AnalyzeDataSet(GetLocation),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                        CreateTableParam("@ID_LOCATION_TYPE", IdLocationType),
                        CreateTableParam("@ID_LOCATION_PATTERN", IdLocationPattern),
                        CreateTableParam("@ID_DESCR_PATTERN", IdDescrPattern),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                        CreateTableParam("@ID_CONSUMER", IdConsumer),
                        CreateTableParam("@ID_LOCATION_STATE_TYPE", IdLocationStateType),
                        new DB.InParameter("@IN_KPI", InKpi),
                        new DB.InParameter("@ALLOW_GROUPING", AllowGrouping),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLocationConsumerHistoryFilter

        public DB_LOCATION_CONSUMER_HISTORY[] GetLocationConsumerHistoryFilter(long[] IdLocationConsumerHistory, long[] IdLocation, int[] IdConsumer, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, string Notes,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationConsumerHistoryFilter"));

            return (DB_LOCATION_CONSUMER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationConsumerHistoryFilter",
                new DB.AnalyzeDataSet(GetLocationConsumerHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_CONSUMER_HISTORY", IdLocationConsumerHistory),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_CONSUMER", IdConsumer),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationDetailsFilter

        public DB_LOCATION_DETAILS[] GetLocationDetailsFilter(long[] IdLocationDetails, long[] IdLocation, string Cid, string Name, string City, string Address,
                            string Zip, string Country, string Latitude, string Longitude, int[] ImrServer,
                            TypeDateTimeCode CreationDate, string LocationMemo, long[] LocationSourceServerId, int[] IdDistributor, int[] IdLocationType,
                            int[] IdLocationStateType, bool? InKpi, bool? AllowGrouping, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationDetailsFilter"));
            if (UseBulkInsertSqlTableType && (
        (IdLocationDetails != null && IdLocationDetails.Length > MaxItemsPerSqlTableType) ||
        ((IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) || (IdLocation == null && LocationFilter.Length > MaxItemsPerSqlTableType)) ||
        (ImrServer != null && ImrServer.Length > MaxItemsPerSqlTableType) ||
        (LocationSourceServerId != null && LocationSourceServerId.Length > MaxItemsPerSqlTableType) ||
        ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
        (IdLocationType != null && IdLocationType.Length > MaxItemsPerSqlTableType) ||
        (IdLocationStateType != null && IdLocationStateType.Length > MaxItemsPerSqlTableType)
        ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocationDetails != null) args.AddRange(IdLocationDetails.Select(s => new Tuple<int, object>(1, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(2, s)));
                else if (LocationFilter.Length > 0) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(2, s)));
                if (ImrServer != null) args.AddRange(ImrServer.Select(s => new Tuple<int, object>(11, s)));
                if (LocationSourceServerId != null) args.AddRange(LocationSourceServerId.Select(s => new Tuple<int, object>(14, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(15, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(15, s)));
                if (IdLocationType != null) args.AddRange(IdLocationType.Select(s => new Tuple<int, object>(16, s)));
                if (IdLocationStateType != null) args.AddRange(IdLocationStateType.Select(s => new Tuple<int, object>(17, s)));

                DB_LOCATION_DETAILS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION_DETAILS[])DB.ExecuteProcedure(
                        "imrse_GetLocationDetailsFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationDetails),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@CID", Cid),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@CITY", City),
                    new DB.InParameter("@ADDRESS", Address),
                    new DB.InParameter("@ZIP", Zip),
                    new DB.InParameter("@COUNTRY", Country),
                    new DB.InParameter("@LATITUDE", Latitude),
                    new DB.InParameter("@LONGITUDE", Longitude),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@LOCATION_MEMO", LocationMemo),
                    new DB.InParameter("@IN_KPI", InKpi),
                    new DB.InParameter("@ALLOW_GROUPING", AllowGrouping),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_LOCATION_DETAILS[])DB.ExecuteProcedure(
                    "imrse_GetLocationDetailsFilter",
                    new DB.AnalyzeDataSet(GetLocationDetails),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_DETAILS", IdLocationDetails),
                    CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                    new DB.InParameter("@CID", Cid),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@CITY", City),
                    new DB.InParameter("@ADDRESS", Address),
                    new DB.InParameter("@ZIP", Zip),
                    new DB.InParameter("@COUNTRY", Country),
                    new DB.InParameter("@LATITUDE", Latitude),
                    new DB.InParameter("@LONGITUDE", Longitude),
                    CreateTableParam("@IMR_SERVER", ImrServer),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@LOCATION_MEMO", LocationMemo),
                    CreateTableParam("@LOCATION_SOURCE_SERVER_ID", LocationSourceServerId),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_LOCATION_TYPE", IdLocationType),
                    CreateTableParam("@ID_LOCATION_STATE_TYPE", IdLocationStateType),
                    new DB.InParameter("@IN_KPI", InKpi),
                    new DB.InParameter("@ALLOW_GROUPING", AllowGrouping),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetLocationHierarchyFilter
        public DB_LOCATION_HIERARCHY[] GetLocationHierarchyFilter(long[] IdLocationHierarchy, long[] IdLocationHierarchyParent, long[] IdLocation, string Hierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationHierarchyFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLocationHierarchy != null && IdLocationHierarchy.Length > MaxItemsPerSqlTableType) ||
                (IdLocationHierarchyParent != null && IdLocationHierarchyParent.Length > MaxItemsPerSqlTableType) ||
                ((IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) || (IdLocation == null && LocationFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocationHierarchy != null) args.AddRange(IdLocationHierarchy.Select(s => new Tuple<int, object>(1, s)));
                if (IdLocationHierarchyParent != null) args.AddRange(IdLocationHierarchyParent.Select(s => new Tuple<int, object>(2, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(3, s)));
                else if (LocationFilter.Length > 0) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(3, s)));

                DB_LOCATION_HIERARCHY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION_HIERARCHY[])DB.ExecuteProcedure(
                        "imrse_GetLocationHierarchyFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationHierarchy),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@HIERARCHY", Hierarchy),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_LOCATION_HIERARCHY[])DB.ExecuteProcedure(
                    "imrse_GetLocationHierarchyFilter",
                    new DB.AnalyzeDataSet(GetLocationHierarchy),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION_HIERARCHY", IdLocationHierarchy),
                        CreateTableParam("@ID_LOCATION_HIERARCHY_PARENT", IdLocationHierarchyParent),
                        CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                        new DB.InParameter("@HIERARCHY", Hierarchy),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLocationSealFilter

        public DB_LOCATION_SEAL[] GetLocationSealFilter(long[] IdLocation, long[] IdSeal, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationSealFilter"));

            return (DB_LOCATION_SEAL[])DB.ExecuteProcedure(
                "imrse_GetLocationSealFilter",
                new DB.AnalyzeDataSet(GetLocationSeal),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_SEAL", IdSeal),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationStateHistoryFilter

        public DB_LOCATION_STATE_HISTORY[] GetLocationStateHistoryFilter(long[] IdLocationState, long[] IdLocation, int[] IdLocationStateType, bool? InKpi, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                            string Notes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, SqlCommand sqlCommand = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationStateHistoryFilter"));

            return (DB_LOCATION_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationStateHistoryFilter",
                new DB.AnalyzeDataSet(GetLocationStateHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_STATE", IdLocationState),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_LOCATION_STATE_TYPE", IdLocationStateType),
                    new DB.InParameter("@IN_KPI", InKpi),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout, sqlCommand
            );
        }

        #endregion
        #region GetLocationStateTypeFilter

        public DB_LOCATION_STATE_TYPE[] GetLocationStateTypeFilter(int[] IdLocationStateType, string Name, long[] IdDescr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationStateTypeFilter"));

            return (DB_LOCATION_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationStateTypeFilter",
                new DB.AnalyzeDataSet(GetLocationStateType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_STATE_TYPE", IdLocationStateType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationSupplierFilter

        public DB_LOCATION_SUPPLIER[] GetLocationSupplierFilter(int[] IdLocationSupplier, long[] IdLocation, int[] IdDistributorSupplier, int[] AccountNo, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                            string Notes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationSupplierFilter"));

            return (DB_LOCATION_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetLocationSupplierFilter",
                new DB.AnalyzeDataSet(GetLocationSupplier),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_SUPPLIER", IdLocationSupplier),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_DISTRIBUTOR_SUPPLIER", IdDistributorSupplier),
                    CreateTableParam("@ACCOUNT_NO", AccountNo),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationTariffFilter

        public DB_LOCATION_TARIFF[] GetLocationTariffFilter(int[] IdLocationTariff, long[] IdLocation, int[] IdTariff, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, string Notes,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationTariffFilter"));

            return (DB_LOCATION_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetLocationTariffFilter",
                new DB.AnalyzeDataSet(GetLocationTariff),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_TARIFF", IdLocationTariff),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_TARIFF", IdTariff),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationTypeFilter

        public DB_LOCATION_TYPE[] GetLocationTypeFilter(int[] IdLocationType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationTypeFilter"));

            return (DB_LOCATION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationTypeFilter",
                new DB.AnalyzeDataSet(GetLocationType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_LOCATION_TYPE", IdLocationType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationEntranceFilter
        public DB_LOCATION_ENTRANCE[] GetLocationEntranceFilter(long[] IdLocation, int[] EntranceIndexNbr, int[] SectionCount, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEntranceFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (EntranceIndexNbr != null && EntranceIndexNbr.Length > MaxItemsPerSqlTableType) ||
                (SectionCount != null && SectionCount.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(1, s)));
                if (EntranceIndexNbr != null) args.AddRange(EntranceIndexNbr.Select(s => new Tuple<int, object>(2, s)));
                if (SectionCount != null) args.AddRange(SectionCount.Select(s => new Tuple<int, object>(3, s)));

                DB_LOCATION_ENTRANCE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION_ENTRANCE[])DB.ExecuteProcedure(
                        "imrse_GetLocationEntranceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationEntrance),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_LOCATION_ENTRANCE[])DB.ExecuteProcedure(
                    "imrse_GetLocationEntranceFilter",
                    new DB.AnalyzeDataSet(GetLocationEntrance),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION", IdLocation),
                        CreateTableParam("@ENTRANCE_INDEX_NBR", EntranceIndexNbr),
                        CreateTableParam("@SECTION_COUNT", SectionCount),
                        new DB.InParameter("@NAME", Name),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLocationEntranceFloorFilter

        public DB_LOCATION_ENTRANCE_FLOOR[] GetLocationEntranceFloorFilter(long[] IdLocation, int[] EntranceIndexNbr, int[] FloorIndexNbr, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEntranceFloorFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (EntranceIndexNbr != null && EntranceIndexNbr.Length > MaxItemsPerSqlTableType) ||
                (FloorIndexNbr != null && FloorIndexNbr.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(1, s)));
                if (EntranceIndexNbr != null) args.AddRange(EntranceIndexNbr.Select(s => new Tuple<int, object>(2, s)));
                if (FloorIndexNbr != null) args.AddRange(FloorIndexNbr.Select(s => new Tuple<int, object>(3, s)));

                DB_LOCATION_ENTRANCE_FLOOR[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION_ENTRANCE_FLOOR[])DB.ExecuteProcedure(
                        "imrse_GetLocationEntranceFloorFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationEntranceFloor),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_LOCATION_ENTRANCE_FLOOR[])DB.ExecuteProcedure(
                    "imrse_GetLocationEntranceFloorFilter",
                    new DB.AnalyzeDataSet(GetLocationEntranceFloor),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION", IdLocation),
                        CreateTableParam("@ENTRANCE_INDEX_NBR", EntranceIndexNbr),
                        CreateTableParam("@FLOOR_INDEX_NBR", FloorIndexNbr),
                        new DB.InParameter("@NAME", Name),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMappingFilter

        public DB_MAPPING[] GetMappingFilter(long[] IdMapping, int[] IdReferenceType, long[] IdSource, long[] IdDestination, int[] IdImrServer, TypeDateTimeCode Timestamp,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMappingFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMapping != null && IdMapping.Length > MaxItemsPerSqlTableType) ||
                (IdReferenceType != null && IdReferenceType.Length > MaxItemsPerSqlTableType) ||
                (IdSource != null && IdSource.Length > MaxItemsPerSqlTableType) ||
                (IdDestination != null && IdDestination.Length > MaxItemsPerSqlTableType) ||
                (IdImrServer != null && IdImrServer.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMapping != null) args.AddRange(IdMapping.Select(s => new Tuple<int, object>(1, s)));
                if (IdReferenceType != null) args.AddRange(IdReferenceType.Select(s => new Tuple<int, object>(2, s)));
                if (IdSource != null) args.AddRange(IdSource.Select(s => new Tuple<int, object>(3, s)));
                if (IdDestination != null) args.AddRange(IdDestination.Select(s => new Tuple<int, object>(4, s)));
                if (IdImrServer != null) args.AddRange(IdImrServer.Select(s => new Tuple<int, object>(5, s)));

                DB_MAPPING[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_MAPPING[])DB.ExecuteProcedure(
                        "imrse_GetMappingFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMapping),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIMESTAMP_CODE", Timestamp),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_MAPPING[])DB.ExecuteProcedure(
                    "imrse_GetMappingFilter",
                    new DB.AnalyzeDataSet(GetMapping),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MAPPING", IdMapping),
                CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                CreateTableParam("@ID_SOURCE", IdSource),
                CreateTableParam("@ID_DESTINATION", IdDestination),
                CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                CreateTableParam("@TIMESTAMP_CODE", Timestamp),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMessageFilter

        public DB_MESSAGE[] GetMessageFilter(long[] IdMessage, int[] IdMessageType, long[] IdMessageParent, int[] IdOperator, string MessageContent, bool? HasAttachment,
                            int[] Points, string Hierarchy, TypeDateTimeCode InsertDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMessageFilter"));

            return (DB_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetMessageFilter",
                new DB.AnalyzeDataSet(GetMessage),
                new DB.Parameter[] {
                    CreateTableParam("@ID_MESSAGE", IdMessage),
                    CreateTableParam("@ID_MESSAGE_TYPE", IdMessageType),
                    CreateTableParam("@ID_MESSAGE_PARENT", IdMessageParent),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    new DB.InParameter("@MESSAGE_CONTENT", MessageContent),
                    new DB.InParameter("@HAS_ATTACHMENT", HasAttachment),
                    CreateTableParam("@POINTS", Points),
                    new DB.InParameter("@HIERARCHY", Hierarchy),
                    CreateTableParam("@INSERT_DATE_CODE", InsertDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMessageDataFilter

        public DB_MESSAGE_DATA[] GetMessageDataFilter(long[] IdMessageData, long[] IdMessage, int[] IndexNbr, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMessageDataFilter"));

            return (DB_MESSAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetMessageDataFilter",
                new DB.AnalyzeDataSet(GetMessageData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_MESSAGE_DATA", IdMessageData),
                    CreateTableParam("@ID_MESSAGE", IdMessage),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMeterFilter
        public DB_METER[] GetMeterFilter(long[] IdMeter, int[] IdMeterType, long[] IdMeterPattern, long[] IdDescrPattern, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, SqlCommand sqlCommand = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterFilter"));

            if (UseBulkInsertSqlTableType && (
                ((IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) || (IdMeter == null && MeterFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdMeterType != null && IdMeterType.Length > MaxItemsPerSqlTableType) ||
                (IdMeterPattern != null && IdMeterPattern.Length > MaxItemsPerSqlTableType) ||
                (IdDescrPattern != null && IdDescrPattern.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(1, s)));
                else if (MeterFilter.Length > 0) args.AddRange(MeterFilter.Select(s => new Tuple<int, object>(1, s)));
                if (IdMeterType != null) args.AddRange(IdMeterType.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeterPattern != null) args.AddRange(IdMeterPattern.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescrPattern != null) args.AddRange(IdDescrPattern.Select(s => new Tuple<int, object>(4, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(5, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(5, s)));

                DB_METER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_METER[])DB.ExecuteProcedure(
                        "imrse_GetMeterFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMeter),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout, sqlCommand
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_METER[])DB.ExecuteProcedure(
                    "imrse_GetMeterFilter",
                    new DB.AnalyzeDataSet(GetMeter),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_METER", IdMeter != null ? IdMeter : MeterFilter),
                        CreateTableParam("@ID_METER_TYPE", IdMeterType),
                        CreateTableParam("@ID_METER_PATTERN", IdMeterPattern),
                        CreateTableParam("@ID_DESCR_PATTERN", IdDescrPattern),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout, sqlCommand
                );
            }
        }
        #endregion
        #region GetMeterTypeFilter

        public DB_METER_TYPE[] GetMeterTypeFilter(int[] IdMeterType, string Name, long[] IdDescr, int[] IdMeterTypeClass, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeFilter"));

            return (DB_METER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeFilter",
                new DB.AnalyzeDataSet(GetMeterType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_METER_TYPE", IdMeterType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_METER_TYPE_CLASS", IdMeterTypeClass),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMeterTypeClassFilter

        public DB_METER_TYPE_CLASS[] GetMeterTypeClassFilter(int[] IdMeterTypeClass, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeClassFilter"));

            return (DB_METER_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeClassFilter",
                new DB.AnalyzeDataSet(GetMeterTypeClass),
                new DB.Parameter[] {
                    CreateTableParam("@ID_METER_TYPE_CLASS", IdMeterTypeClass),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMeterTypeDataFilter

        public DB_METER_TYPE_DATA[] GetMeterTypeDataFilter(long[] IdMeterTypeData, int[] IdMeterType, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMeterTypeData != null && IdMeterTypeData.Length > MaxItemsPerSqlTableType) ||
                (IdMeterType != null && IdMeterType.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMeterTypeData != null) args.AddRange(IdMeterTypeData.Select(s => new Tuple<int, object>(1, s)));
                if (IdMeterType != null) args.AddRange(IdMeterType.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_METER_TYPE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_METER_TYPE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetMeterTypeDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMeterTypeData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_METER_TYPE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetMeterTypeDataFilter",
                    new DB.AnalyzeDataSet(GetMeterTypeData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_METER_TYPE_DATA", IdMeterTypeData),
                CreateTableParam("@ID_METER_TYPE", IdMeterType),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMeterTypeDeviceTypeFilter

        public DB_METER_TYPE_DEVICE_TYPE[] GetMeterTypeDeviceTypeFilter(int[] IdMeterType, int[] IdDeviceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeDeviceTypeFilter"));

            return (DB_METER_TYPE_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeDeviceTypeFilter",
                new DB.AnalyzeDataSet(GetMeterTypeDeviceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_METER_TYPE", IdMeterType),
                    CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMeterTypeGroupFilter

        public DB_METER_TYPE_GROUP[] GetMeterTypeGroupFilter(int[] IdMeterTypeGroup, string Name, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeGroupFilter"));

            return (DB_METER_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeGroupFilter",
                new DB.AnalyzeDataSet(GetMeterTypeGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_METER_TYPE_GROUP", IdMeterTypeGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMeterTypeInGroupFilter

        public DB_METER_TYPE_IN_GROUP[] GetMeterTypeInGroupFilter(int[] IdMeterTypeGroup, int[] IdMeterType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeInGroupFilter"));

            return (DB_METER_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeInGroupFilter",
                new DB.AnalyzeDataSet(GetMeterTypeInGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_METER_TYPE_GROUP", IdMeterTypeGroup),
                    CreateTableParam("@ID_METER_TYPE", IdMeterType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMobileNetworkCodeFilter

        public DB_MOBILE_NETWORK_CODE[] GetMobileNetworkCodeFilter(int[] Code, string Mmc, string Mnc, string Brand, string Operator, string Bands,
                            string SmsServiceCenter, string ApnAddress, string ApnLogin, string ApnPassword, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMobileNetworkCodeFilter"));

            return (DB_MOBILE_NETWORK_CODE[])DB.ExecuteProcedure(
                "imrse_GetMobileNetworkCodeFilter",
                new DB.AnalyzeDataSet(GetMobileNetworkCode),
                new DB.Parameter[] {
                    CreateTableParam("@CODE", Code),
                    new DB.InParameter("@MMC", Mmc),
                    new DB.InParameter("@MNC", Mnc),
                    new DB.InParameter("@BRAND", Brand),
                    new DB.InParameter("@OPERATOR", Operator),
                    new DB.InParameter("@BANDS", Bands),
                    new DB.InParameter("@SMS_SERVICE_CENTER", SmsServiceCenter),
                    new DB.InParameter("@APN_ADDRESS", ApnAddress),
                    new DB.InParameter("@APN_LOGIN", ApnLogin),
                    new DB.InParameter("@APN_PASSWORD", ApnPassword),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetModuleFilter

        public DB_MODULE[] GetModuleFilter(int[] IdModule, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleFilter"));

            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModuleFilter",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] {
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetModuleDataFilter

        public DB_MODULE_DATA[] GetModuleDataFilter(long[] IdModuleData, int[] IdModule, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleDataFilter"));

            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleDataFilter",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_MODULE_DATA", IdModuleData),
                    CreateTableParam("@ID_MODULE", IdModule),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMonitoringWatcherFilter

        public DB_MONITORING_WATCHER[] GetMonitoringWatcherFilter(int[] IdMonitoringWatcher, int[] IdMonitoringWatcherType, bool? IsActive, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcher != null && IdMonitoringWatcher.Length > MaxItemsPerSqlTableType) ||
                (IdMonitoringWatcherType != null && IdMonitoringWatcherType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcher != null) args.AddRange(IdMonitoringWatcher.Select(s => new Tuple<int, object>(1, s)));
                if (IdMonitoringWatcherType != null) args.AddRange(IdMonitoringWatcherType.Select(s => new Tuple<int, object>(2, s)));

                DB_MONITORING_WATCHER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcher),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@IS_ACTIVE", IsActive),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcher),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER", IdMonitoringWatcher),
                CreateTableParam("@ID_MONITORING_WATCHER_TYPE", IdMonitoringWatcherType),
                new DB.InParameter("@IS_ACTIVE", IsActive),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMonitoringWatcherDataFilter

        public DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherDataFilter(int[] IdMonitoringWatcherData, int[] IdMonitoringWatcher, long[] IdDataType, int[] Index, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcherData != null && IdMonitoringWatcherData.Length > MaxItemsPerSqlTableType) ||
                (IdMonitoringWatcher != null && IdMonitoringWatcher.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (Index != null && Index.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcherData != null) args.AddRange(IdMonitoringWatcherData.Select(s => new Tuple<int, object>(1, s)));
                if (IdMonitoringWatcher != null) args.AddRange(IdMonitoringWatcher.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (Index != null) args.AddRange(Index.Select(s => new Tuple<int, object>(4, s)));

                DB_MONITORING_WATCHER_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcherData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherDataFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcherData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER_DATA", IdMonitoringWatcherData),
                CreateTableParam("@ID_MONITORING_WATCHER", IdMonitoringWatcher),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX", Index),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMonitoringWatcherTypeFilter

        public DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherTypeFilter(int[] IdMonitoringWatcherType, string Name, string Plugin, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcherType != null && IdMonitoringWatcherType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcherType != null) args.AddRange(IdMonitoringWatcherType.Select(s => new Tuple<int, object>(1, s)));

                DB_MONITORING_WATCHER_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcherType),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@PLUGIN", Plugin),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherTypeFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcherType),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER_TYPE", IdMonitoringWatcherType),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@PLUGIN", Plugin),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetNotificationDeliveryTypeFilter

        public DB_NOTIFICATION_DELIVERY_TYPE[] GetNotificationDeliveryTypeFilter(int[] IdNotificationDeliveryType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetNotificationDeliveryTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdNotificationDeliveryType != null && IdNotificationDeliveryType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdNotificationDeliveryType != null) args.AddRange(IdNotificationDeliveryType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_NOTIFICATION_DELIVERY_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_NOTIFICATION_DELIVERY_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetNotificationDeliveryTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetNotificationDeliveryType),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_NOTIFICATION_DELIVERY_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetNotificationDeliveryTypeFilter",
                    new DB.AnalyzeDataSet(GetNotificationDeliveryType),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_NOTIFICATION_DELIVERY_TYPE", IdNotificationDeliveryType),
                        new DB.InParameter("@NAME", Name),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetNotificationTypeFilter

        public DB_NOTIFICATION_TYPE[] GetNotificationTypeFilter(int[] IdNotificationType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetNotificationTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdNotificationType != null && IdNotificationType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdNotificationType != null) args.AddRange(IdNotificationType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_NOTIFICATION_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_NOTIFICATION_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetNotificationTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetNotificationType),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_NOTIFICATION_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetNotificationTypeFilter",
                    new DB.AnalyzeDataSet(GetNotificationType),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_NOTIFICATION_TYPE", IdNotificationType),
                        new DB.InParameter("@NAME", Name),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetOperatorFilter

        public DB_OPERATOR[] GetOperatorFilter(int[] IdOperator, int[] IdActor, int[] IdDistributor, long[] SerialNbr, string Login, string Password,
                            bool? IsBlocked, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorFilter"));

            return (DB_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetOperatorFilter",
                new DB.AnalyzeDataSet(GetOperator),
                new DB.Parameter[] {
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_ACTOR", IdActor),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@LOGIN", Login),
                    new DB.InParameter("@PASSWORD", Password),
                    new DB.InParameter("@IS_BLOCKED", IsBlocked),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOperatorActivityFilter

        public DB_OPERATOR_ACTIVITY[] GetOperatorActivityFilter(long[] IdOperatorActivity, int[] IdOperator, int[] IdActivity, int[] IdModule, bool? Deny, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorActivityFilter"));

            return (DB_OPERATOR_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetOperatorActivityFilter",
                new DB.AnalyzeDataSet(GetOperatorActivity),
                new DB.Parameter[] {
                    CreateTableParam("@ID_OPERATOR_ACTIVITY", IdOperatorActivity),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_ACTIVITY", IdActivity),
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@DENY", Deny),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOperatorDataFilter

        public DB_OPERATOR_DATA[] GetOperatorDataFilter(long[] IdOperatorData, int[] IdOperator, long[] IdDataType, int[] IndexNbr, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorDataFilter"));

            List<DB.Parameter> parameterList = new List<DB.Parameter>();
            if (IdOperatorData != null) parameterList.Add(CreateTableParam("@ID_OPERATOR_DATA", IdOperatorData));
            if (IdOperator != null) parameterList.Add(CreateTableParam("@ID_OPERATOR", IdOperator));
            if (IdDataType != null) parameterList.Add(CreateTableParam("@ID_DATA_TYPE", IdDataType));
            if (IndexNbr != null) parameterList.Add(CreateTableParam("@INDEX_NBR", IndexNbr));
            if (IdDistributor != null) parameterList.Add(CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter));
            parameterList.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameterList.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
            return (DB_OPERATOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetOperatorDataFilter",
                new DB.AnalyzeDataSet(GetOperatorData),
                parameterList.ToArray(),
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOperatorRoleFilter

        public DB_OPERATOR_ROLE[] GetOperatorRoleFilter(int[] IdOperator, int[] IdRole, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorRoleFilter"));

            return (DB_OPERATOR_ROLE[])DB.ExecuteProcedure(
                "imrse_GetOperatorRoleFilter",
                new DB.AnalyzeDataSet(GetOperatorRole),
                new DB.Parameter[] {
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_ROLE", IdRole),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOrderFilter

        public DB_ORDER[] GetOrderFilter(int[] IdOrder, int[] IdOrderStateType, int[] IdOperator, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderFilter"));

            return (DB_ORDER[])DB.ExecuteProcedure(
                "imrse_GetOrderFilter",
                new DB.AnalyzeDataSet(GetOrder),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ORDER", IdOrder),
                    CreateTableParam("@ID_ORDER_STATE_TYPE", IdOrderStateType),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOrderDataFilter

        public DB_ORDER_DATA[] GetOrderDataFilter(long[] IdOrderData, int[] IdOrder, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderDataFilter"));

            return (DB_ORDER_DATA[])DB.ExecuteProcedure(
                "imrse_GetOrderDataFilter",
                new DB.AnalyzeDataSet(GetOrderData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ORDER_DATA", IdOrderData),
                    CreateTableParam("@ID_ORDER", IdOrder),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetOrderStateTypeFilter

        public DB_ORDER_STATE_TYPE[] GetOrderStateTypeFilter(int[] IdOrderStateType, string Name, long[] IdDescr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderStateTypeFilter"));

            return (DB_ORDER_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetOrderStateTypeFilter",
                new DB.AnalyzeDataSet(GetOrderStateType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ORDER_STATE_TYPE", IdOrderStateType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPackageFilter

        public DB_PACKAGE[] GetPackageFilter(int[] IdPackage, string Name, TypeDateTimeCode CreationDate, string Code, TypeDateTimeCode SendDate, int[] IdOperatorCreator,
                            int[] IdOperatorReceiver, bool? Received, TypeDateTimeCode ReceiveDate, int[] IdDistributor, int[] IdPackageStatus,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageFilter"));

            return (DB_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetPackageFilter",
                new DB.AnalyzeDataSet(GetPackage),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PACKAGE", IdPackage),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@CODE", Code),
                    CreateTableParam("@SEND_DATE_CODE", SendDate),
                    CreateTableParam("@ID_OPERATOR_CREATOR", IdOperatorCreator),
                    CreateTableParam("@ID_OPERATOR_RECEIVER", IdOperatorReceiver),
                    new DB.InParameter("@RECEIVED", Received),
                    CreateTableParam("@RECEIVE_DATE_CODE", ReceiveDate),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_PACKAGE_STATUS", IdPackageStatus),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPackageStatusFilter

        public DB_PACKAGE_STATUS[] GetPackageStatusFilter(int[] IdPackageStatus, string PackageStatus, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageStatusFilter"));

            return (DB_PACKAGE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPackageStatusFilter",
                new DB.AnalyzeDataSet(GetPackageStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PACKAGE_STATUS", IdPackageStatus),
                    new DB.InParameter("@PACKAGE_STATUS", PackageStatus),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPackageStatusHistoryFilter

        public DB_PACKAGE_STATUS_HISTORY[] GetPackageStatusHistoryFilter(int[] IdPackageStatusHistory, int[] IdPackage, int[] IdPackageStatus, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageStatusHistoryFilter"));

            return (DB_PACKAGE_STATUS_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPackageStatusHistoryFilter",
                new DB.AnalyzeDataSet(GetPackageStatusHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PACKAGE_STATUS_HISTORY", IdPackageStatusHistory),
                    CreateTableParam("@ID_PACKAGE", IdPackage),
                    CreateTableParam("@ID_PACKAGE_STATUS", IdPackageStatus),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPackageDataFilter

        public DB_PACKAGE_DATA[] GetPackageDataFilter(long[] IdPackageData, int[] IdPackage, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageDataFilter"));

            return (DB_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetPackageDataFilter",
                new DB.AnalyzeDataSet(GetPackageData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PACKAGE_DATA", IdPackageData),
                    CreateTableParam("@ID_PACKAGE", IdPackage),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPaymentModuleFilter

        public DB_PAYMENT_MODULE[] GetPaymentModuleFilter(int[] IdPaymentModule, string Name, int[] IdPaymentModuleType, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdPaymentModule != null && IdPaymentModule.Length > MaxItemsPerSqlTableType) ||
                (IdPaymentModuleType != null && IdPaymentModuleType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdPaymentModule != null) args.AddRange(IdPaymentModule.Select(s => new Tuple<int, object>(1, s)));
                if (IdPaymentModuleType != null) args.AddRange(IdPaymentModuleType.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(4, s)));

                DB_PAYMENT_MODULE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PAYMENT_MODULE[])DB.ExecuteProcedure(
                        "imrse_GetPaymentModuleFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPaymentModule),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PAYMENT_MODULE[])DB.ExecuteProcedure(
                    "imrse_GetPaymentModuleFilter",
                    new DB.AnalyzeDataSet(GetPaymentModule),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_PAYMENT_MODULE", IdPaymentModule),
                        new DB.InParameter("@NAME", Name),
                        CreateTableParam("@ID_PAYMENT_MODULE_TYPE", IdPaymentModuleType),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPaymentModuleDataFilter

        public DB_PAYMENT_MODULE_DATA[] GetPaymentModuleDataFilter(long[] IdPaymentModuleData, int[] IdPaymentModule, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdPaymentModuleData != null && IdPaymentModuleData.Length > MaxItemsPerSqlTableType) ||
                (IdPaymentModule != null && IdPaymentModule.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdPaymentModuleData != null) args.AddRange(IdPaymentModuleData.Select(s => new Tuple<int, object>(1, s)));
                if (IdPaymentModule != null) args.AddRange(IdPaymentModule.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_PAYMENT_MODULE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PAYMENT_MODULE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetPaymentModuleDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPaymentModuleData),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PAYMENT_MODULE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetPaymentModuleDataFilter",
                    new DB.AnalyzeDataSet(GetPaymentModuleData),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_PAYMENT_MODULE_DATA", IdPaymentModuleData),
                        CreateTableParam("@ID_PAYMENT_MODULE", IdPaymentModule),
                        CreateTableParam("@ID_DATA_TYPE", IdDataType),
                        CreateTableParam("@INDEX_NBR", IndexNbr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPaymentModuleTypeFilter

        public DB_PAYMENT_MODULE_TYPE[] GetPaymentModuleTypeFilter(int[] IdPaymentModuleType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdPaymentModuleType != null && IdPaymentModuleType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdPaymentModuleType != null) args.AddRange(IdPaymentModuleType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_PAYMENT_MODULE_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PAYMENT_MODULE_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetPaymentModuleTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPaymentModuleType),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@NAME", Name),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PAYMENT_MODULE_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetPaymentModuleTypeFilter",
                    new DB.AnalyzeDataSet(GetPaymentModuleType),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_PAYMENT_MODULE_TYPE", IdPaymentModuleType),
                        new DB.InParameter("@NAME", Name),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPriorityFilter

        public DB_PRIORITY[] GetPriorityFilter(int[] IdPriority, long[] IdDescr, int[] Value, int[] RealizationTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPriorityFilter"));

            return (DB_PRIORITY[])DB.ExecuteProcedure(
                "imrse_GetPriorityFilter",
                new DB.AnalyzeDataSet(GetPriority),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PRIORITY", IdPriority),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@VALUE", Value),
                    CreateTableParam("@REALIZATION_TIME", RealizationTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetProductCodeFilter

        public DB_PRODUCT_CODE[] GetProductCodeFilter(int[] IdProductCode, string Code, long[] IdDescr, int[] Density, Double[] Alpha, Double[] CalorificValue,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProductCodeFilter"));

            return (DB_PRODUCT_CODE[])DB.ExecuteProcedure(
                "imrse_GetProductCodeFilter",
                new DB.AnalyzeDataSet(GetProductCode),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PRODUCT_CODE", IdProductCode),
                    new DB.InParameter("@CODE", Code),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@DENSITY", Density),
                    CreateTableParam("@ALPHA", Alpha),
                    CreateTableParam("@CALORIFIC_VALUE", CalorificValue),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetProfileFilter

        public DB_PROFILE[] GetProfileFilter(int[] IdProfile, string Name, long[] IdDescr, int[] IdModule, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProfileFilter"));

            return (DB_PROFILE[])DB.ExecuteProcedure(
                "imrse_GetProfileFilter",
                new DB.AnalyzeDataSet(GetProfile),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PROFILE", IdProfile),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetProfileDataFilter

        public DB_PROFILE_DATA[] GetProfileDataFilter(long[] IdProfileData, int[] IdProfile, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProfileDataFilter"));

            return (DB_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetProfileDataFilter",
                new DB.AnalyzeDataSet(GetProfileData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PROFILE_DATA", IdProfileData),
                    CreateTableParam("@ID_PROFILE", IdProfile),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetProtocolFilter

        public DB_PROTOCOL[] GetProtocolFilter(int[] IdProtocol, string Name, int[] IdProtocolDriver, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProtocolFilter"));

            return (DB_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetProtocolFilter",
                new DB.AnalyzeDataSet(GetProtocol),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PROTOCOL", IdProtocol),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_PROTOCOL_DRIVER", IdProtocolDriver),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetProtocolDriverFilter

        public DB_PROTOCOL_DRIVER[] GetProtocolDriverFilter(int[] IdProtocolDriver, string Name, long[] IdDescr, string PluginName, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProtocolDriverFilter"));

            return (DB_PROTOCOL_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetProtocolDriverFilter",
                new DB.AnalyzeDataSet(GetProtocolDriver),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PROTOCOL_DRIVER", IdProtocolDriver),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@PLUGIN_NAME", PluginName),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReferenceTypeFilter

        public DB_REFERENCE_TYPE[] GetReferenceTypeFilter(int[] IdReferenceType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReferenceTypeFilter"));

            return (DB_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReferenceTypeFilter",
                new DB.AnalyzeDataSet(GetReferenceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRoleFilter

        public DB_ROLE[] GetRoleFilter(int[] IdRole, string Name, int[] IdModule, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleFilter"));

            return (DB_ROLE[])DB.ExecuteProcedure(
                "imrse_GetRoleFilter",
                new DB.AnalyzeDataSet(GetRole),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROLE", IdRole),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_MODULE", IdModule),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRoleActivityFilter

        public DB_ROLE_ACTIVITY[] GetRoleActivityFilter(long[] IdRoleActivity, int[] IdRole, int[] IdActivity, bool? Deny, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleActivityFilter"));

            return (DB_ROLE_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetRoleActivityFilter",
                new DB.AnalyzeDataSet(GetRoleActivity),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROLE_ACTIVITY", IdRoleActivity),
                    CreateTableParam("@ID_ROLE", IdRole),
                    CreateTableParam("@ID_ACTIVITY", IdActivity),
                    new DB.InParameter("@DENY", Deny),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRoleGroupFilter

        public DB_ROLE_GROUP[] GetRoleGroupFilter(int[] IdRoleParent, int[] IdRole, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleGroupFilter"));

            return (DB_ROLE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetRoleGroupFilter",
                new DB.AnalyzeDataSet(GetRoleGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROLE_PARENT", IdRoleParent),
                    CreateTableParam("@ID_ROLE", IdRole),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteFilter

        public DB_ROUTE[] GetRouteFilter(long[] IdRoute, int[] IdRouteDef, int[] Year, int[] Month, int[] Week, int[] IdOperatorExecutor,
                            int[] IdOperatorApproved, TypeDateTimeCode DateUploaded, TypeDateTimeCode DateApproved, TypeDateTimeCode DateFinished, int[] IdRouteStatus,
                            int[] IdDistributor, int[] IdRouteType, TypeDateTimeCode CreationDate, TypeDateTimeCode ExpirationDate, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.AddRange(new DB.Parameter[]
            {
                CreateTableParam("@ID_ROUTE", IdRoute),
                CreateTableParam("@ID_ROUTE_DEF", IdRouteDef),
                CreateTableParam("@YEAR", Year),
                CreateTableParam("@MONTH", Month),
                CreateTableParam("@WEEK", Week),
                CreateTableParam("@ID_OPERATOR_EXECUTOR", IdOperatorExecutor),
                CreateTableParam("@ID_OPERATOR_APPROVED", IdOperatorApproved),
                CreateTableParam("@DATE_UPLOADED_CODE", DateUploaded),
                CreateTableParam("@DATE_APPROVED_CODE", DateApproved),
                CreateTableParam("@DATE_FINISHED_CODE", DateFinished),
                CreateTableParam("@ID_ROUTE_STATUS", IdRouteStatus),
                CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter)
            });

            if (IsParameterDefined("imrse_GetRouteFilter", "@ID_ROUTE_TYPE", this))
            {
                parameters.Add(CreateTableParam("@ID_ROUTE_TYPE", IdRouteType));
            }
            if (IsParameterDefined("imrse_GetRouteFilter", "@CREATION_DATE", this))
            {
                parameters.Add(CreateTableParam("@CREATION_DATE", CreationDate));
            }
            if (IsParameterDefined("imrse_GetRouteFilter", "@EXPIRATION_DATE", this))
            {
                parameters.Add(CreateTableParam("@EXPIRATION_DATE", ExpirationDate));
            }
            if (IsParameterDefined("imrse_GetRouteFilter", "@NAME", this))
            {
                parameters.Add(new DB.InParameter("@NAME", Name));
            }

            parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));

            return (DB_ROUTE[])DB.ExecuteProcedure(
                "imrse_GetRouteFilter",
                new DB.AnalyzeDataSet(GetRoute),
                parameters.ToArray(),
                autoTransaction,
                transactionLevel,
                commandTimeout
            );
        }

        #endregion
        #region GetRouteDataFilter

        public DB_ROUTE_DATA[] GetRouteDataFilter(long[] IdRouteData, long[] IdRoute, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDataFilter"));

            return (DB_ROUTE_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDataFilter",
                new DB.AnalyzeDataSet(GetRouteData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DATA", IdRouteData),
                    CreateTableParam("@ID_ROUTE", IdRoute),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteDefFilter

        public DB_ROUTE_DEF[] GetRouteDefFilter(int[] IdRouteDef, string Name, string Description, TypeDateTimeCode CreationDate, TypeDateTimeCode ExpirationDate, TypeDateTimeCode DeleteDate,
                    int[] IdRouteType, int[] IdRouteStatus, int[] IdOperator, bool? AdHoc, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefFilter"));

            return (DB_ROUTE_DEF[])DB.ExecuteProcedure(
                "imrse_GetRouteDefFilter",
                new DB.AnalyzeDataSet(GetRouteDef),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEF", IdRouteDef),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@EXPIRATION_DATE_CODE", ExpirationDate),
                    CreateTableParam("@DELETE_DATE_CODE", DeleteDate),
                    CreateTableParam("@ID_ROUTE_TYPE", IdRouteType),
                    CreateTableParam("@ID_ROUTE_STATUS", IdRouteStatus),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    new DB.InParameter("@AD_HOC", AdHoc),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetRouteDefDataFilter

        public DB_ROUTE_DEF_DATA[] GetRouteDefDataFilter(int[] IdRouteDef, long[] IdDataType, int[] IndexNbr,
            long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefDataFilter"));

            return (DB_ROUTE_DEF_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefDataFilter",
                new DB.AnalyzeDataSet(GetRouteDefData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEF", IdRouteDef),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteDefHistoryFilter

        public DB_ROUTE_DEF_HISTORY[] GetRouteDefHistoryFilter(long[] IdRouteDefHistory, int[] IdRouteDef, int[] IdRouteStatus, int[] IdOperator, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefHistoryFilter"));

            return (DB_ROUTE_DEF_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetRouteDefHistoryFilter",
                new DB.AnalyzeDataSet(GetRouteDefHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEF_HISTORY", IdRouteDefHistory),
                    CreateTableParam("@ID_ROUTE_DEF", IdRouteDef),
                    CreateTableParam("@ID_ROUTE_STATUS", IdRouteStatus),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteDefLocationFilter

        public DB_ROUTE_DEF_LOCATION[] GetRouteDefLocationFilter(long[] IdRouteDefLocation, int[] IdRouteDef, long[] IdLocation, int[] OrderNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefLocationFilter"));

            return (DB_ROUTE_DEF_LOCATION[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocationFilter",
                new DB.AnalyzeDataSet(GetRouteDefLocation),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEF_LOCATION", IdRouteDefLocation),
                    CreateTableParam("@ID_ROUTE_DEF", IdRouteDef),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ORDER_NBR", OrderNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteDefLocationDataFilter

        public DB_ROUTE_DEF_LOCATION_DATA[] GetRouteDefLocationDataFilter(long[] IdRouteDefLocation, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefLocationDataFilter"));

            return (DB_ROUTE_DEF_LOCATION_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocationDataFilter",
                new DB.AnalyzeDataSet(GetRouteDefLocationData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEF_LOCATION", IdRouteDefLocation),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteDeviceFilter

        public DB_ROUTE_DEVICE[] GetRouteDeviceFilter(long[] IdRouteDevice, long[] IdRoute, long[] SerialNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDeviceFilter"));

            return (DB_ROUTE_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetRouteDeviceFilter",
                new DB.AnalyzeDataSet(GetRouteDevice),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_DEVICE", IdRouteDevice),
                    CreateTableParam("@ID_ROUTE", IdRoute),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteElementStatusFilter

        public DB_ROUTE_ELEMENT_STATUS[] GetRouteElementStatusFilter(int[] IdRouteElementStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteElementStatusFilter"));

            return (DB_ROUTE_ELEMENT_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteElementStatusFilter",
                new DB.AnalyzeDataSet(GetRouteElementStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_ELEMENT_STATUS", IdRouteElementStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRoutePointFilter

        public DB_ROUTE_POINT[] GetRoutePointFilter(int[] IdRoutePoint, long[] IdRoute, int[] Type, int[] IdTask, TypeDateTimeCode StartDateTime, TypeDateTimeCode EndDateTime,
                            Double[] Latitude, Double[] Longitude, int[] KmCounter, string InvoiceNumber, string Notes,
                            int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoutePointFilter"));

            return (DB_ROUTE_POINT[])DB.ExecuteProcedure(
                "imrse_GetRoutePointFilter",
                new DB.AnalyzeDataSet(GetRoutePoint),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_POINT", IdRoutePoint),
                    CreateTableParam("@ID_ROUTE", IdRoute),
                    CreateTableParam("@TYPE", Type),
                    CreateTableParam("@ID_TASK", IdTask),
                    CreateTableParam("@START_DATE_TIME_CODE", StartDateTime),
                    CreateTableParam("@END_DATE_TIME_CODE", EndDateTime),
                    CreateTableParam("@LATITUDE", Latitude),
                    CreateTableParam("@LONGITUDE", Longitude),
                    CreateTableParam("@KM_COUNTER", KmCounter),
                    new DB.InParameter("@INVOICE_NUMBER", InvoiceNumber),
                    new DB.InParameter("@NOTES", Notes),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRoutePointDataFilter

        public DB_ROUTE_POINT_DATA[] GetRoutePointDataFilter(long[] IdRoutePointData, int[] IdRoutePoint, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoutePointDataFilter"));

            return (DB_ROUTE_POINT_DATA[])DB.ExecuteProcedure(
                "imrse_GetRoutePointDataFilter",
                new DB.AnalyzeDataSet(GetRoutePointData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_POINT_DATA", IdRoutePointData),
                    CreateTableParam("@ID_ROUTE_POINT", IdRoutePoint),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteStatusFilter

        public DB_ROUTE_STATUS[] GetRouteStatusFilter(int[] IdRouteStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteStatusFilter"));

            return (DB_ROUTE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteStatusFilter",
                new DB.AnalyzeDataSet(GetRouteStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_STATUS", IdRouteStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteTypeFilter

        public DB_ROUTE_TYPE[] GetRouteTypeFilter(int[] IdRouteType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTypeFilter"));

            return (DB_ROUTE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteTypeFilter",
                new DB.AnalyzeDataSet(GetRouteType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_TYPE", IdRouteType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteTypeActionTypeFilter

        public DB_ROUTE_TYPE_ACTION_TYPE[] GetRouteTypeActionTypeFilter(int[] IdRouteTypeActionType, int[] IdRouteType, int[] IdActionType, bool? Default, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTypeActionTypeFilter"));

            return (DB_ROUTE_TYPE_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteTypeActionTypeFilter",
                new DB.AnalyzeDataSet(GetRouteTypeActionType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ROUTE_TYPE_ACTION_TYPE", IdRouteTypeActionType),
                    CreateTableParam("@ID_ROUTE_TYPE", IdRouteType),
                    CreateTableParam("@ID_ACTION_TYPE", IdActionType),
                    new DB.InParameter("@DEFAULT", Default),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetScheduleFilter

        public DB_SCHEDULE[] GetScheduleFilter(int[] IdSchedule, string Name, string Description, int[] IdScheduleType, int[] ScheduleInterval, int[] SubdayType,
                            int[] SubdayInterval, int[] RelativeInterval, int[] RecurrenceFactor, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                            TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, bool? TimesInUtc, TypeDateTimeCode LastRunDate, bool? IsActive,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleFilter"));

            return (DB_SCHEDULE[])DB.ExecuteProcedure(
                "imrse_GetScheduleFilter",
                new DB.AnalyzeDataSet(GetSchedule),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SCHEDULE", IdSchedule),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    CreateTableParam("@ID_SCHEDULE_TYPE", IdScheduleType),
                    CreateTableParam("@SCHEDULE_INTERVAL", ScheduleInterval),
                    CreateTableParam("@SUBDAY_TYPE", SubdayType),
                    CreateTableParam("@SUBDAY_INTERVAL", SubdayInterval),
                    CreateTableParam("@RELATIVE_INTERVAL", RelativeInterval),
                    CreateTableParam("@RECURRENCE_FACTOR", RecurrenceFactor),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TIMES_IN_UTC", TimesInUtc),
                    CreateTableParam("@LAST_RUN_DATE_CODE", LastRunDate),
                    new DB.InParameter("@IS_ACTIVE", IsActive),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetScheduleActionFilter

        public DB_SCHEDULE_ACTION[] GetScheduleActionFilter(long[] IdScheduleAction, int[] IdSchedule, long[] IdActionDef, bool? IsEnabled, int[] IdOperator, int[] IdProfile, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleActionFilter"));

            return (DB_SCHEDULE_ACTION[])DB.ExecuteProcedure(
                "imrse_GetScheduleActionFilter",
                new DB.AnalyzeDataSet(GetScheduleAction),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SCHEDULE", IdSchedule),
                    CreateTableParam("@ID_ACTION_DEF", IdActionDef),
                    new DB.InParameter("@IS_ENABLED", IsEnabled),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_PROFILE", IdProfile),
                    CreateTableParam("@ID_SCHEDULE_ACTION", IdScheduleAction),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetScheduleTypeFilter

        public DB_SCHEDULE_TYPE[] GetScheduleTypeFilter(int[] IdScheduleType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleTypeFilter"));

            return (DB_SCHEDULE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetScheduleTypeFilter",
                new DB.AnalyzeDataSet(GetScheduleType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SCHEDULE_TYPE", IdScheduleType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSealFilter

        public DB_SEAL[] GetSealFilter(long[] IdSeal, string SerialNbr, string Description, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSealFilter"));

            return (DB_SEAL[])DB.ExecuteProcedure(
                "imrse_GetSealFilter",
                new DB.AnalyzeDataSet(GetSeal),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SEAL", IdSeal),
                    new DB.InParameter("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSettlementDocumentTypeFilter

        public DB_SETTLEMENT_DOCUMENT_TYPE[] GetSettlementDocumentTypeFilter(int[] IdSettlementDocumentType, string Name, string Description, bool? Active, int[] IdDistributor, long[] IdDescr,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSettlementDocumentTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdSettlementDocumentType != null && IdSettlementDocumentType.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdSettlementDocumentType != null) args.AddRange(IdSettlementDocumentType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(5, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(5, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(6, s)));

                DB_SETTLEMENT_DOCUMENT_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_SETTLEMENT_DOCUMENT_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetSettlementDocumentTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetSettlementDocumentType),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@ACTIVE", Active),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_SETTLEMENT_DOCUMENT_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetSettlementDocumentTypeFilter",
                    new DB.AnalyzeDataSet(GetSettlementDocumentType),
                    new DB.Parameter[] {
                CreateTableParam("@ID_SETTLEMENT_DOCUMENT_TYPE", IdSettlementDocumentType),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@DESCRIPTION", Description),
                new DB.InParameter("@ACTIVE", Active),
                CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetServiceFilter

        public DB_SERVICE[] GetServiceFilter(int[] IdService, string Name, int[] IdDeviceOrderNumber, TypeDateTimeCode DeleteDate, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceFilter"));

            return (DB_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceFilter",
                new DB.AnalyzeDataSet(GetService),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE", IdService),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@DELETE_DATE_CODE", DeleteDate),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceActionResultFilter

        public DB_SERVICE_ACTION_RESULT[] GetServiceActionResultFilter(int[] IdServiceActionResult, int[] IdService, string Name, bool? AllowInputText, long[] IdDescr, int[] IdServiceReferenceType,
                            TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceActionResultFilter"));

            return (DB_SERVICE_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceActionResultFilter",
                new DB.AnalyzeDataSet(GetServiceActionResult),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_ACTION_RESULT", IdServiceActionResult),
                    CreateTableParam("@ID_SERVICE", IdService),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@ALLOW_INPUT_TEXT", AllowInputText),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", IdServiceReferenceType),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceDataFilter

        public DB_SERVICE_DATA[] GetServiceDataFilter(long[] IdServiceData, int[] IdService, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceDataFilter"));

            return (DB_SERVICE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceDataFilter",
                new DB.AnalyzeDataSet(GetServiceData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_DATA", IdServiceData),
                    CreateTableParam("@ID_SERVICE", IdService),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceCustomFilter

        public DB_SERVICE_CUSTOM[] GetServiceCustomFilter(long[] IdServiceCustom, int[] IdServiceList, int[] IndirectCost, long[] IdFile, bool? CustomValue, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceCustomFilter"));

            return (DB_SERVICE_CUSTOM[])DB.ExecuteProcedure(
                "imrse_GetServiceCustomFilter",
                new DB.AnalyzeDataSet(GetServiceCustom),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_CUSTOM", IdServiceCustom),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@INDIRECT_COST", IndirectCost),
                    CreateTableParam("@ID_FILE", IdFile),
                    new DB.InParameter("@CUSTOM_VALUE", CustomValue),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceCustomDataFilter

        public DB_SERVICE_CUSTOM_DATA[] GetServiceCustomDataFilter(long[] IdServiceCustomData, long[] IdServiceCustom, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceCustomDataFilter"));

            return (DB_SERVICE_CUSTOM_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceCustomDataFilter",
                new DB.AnalyzeDataSet(GetServiceCustomData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_CUSTOM_DATA", IdServiceCustomData),
                    CreateTableParam("@ID_SERVICE_CUSTOM", IdServiceCustom),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceDiagnosticResultFilter

        public DB_SERVICE_DIAGNOSTIC_RESULT[] GetServiceDiagnosticResultFilter(int[] IdServiceDiagnosticResult, string Name, long[] IdDescr, bool? IsServiceResult, bool? IsDiagnosticResult, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceDiagnosticResultFilter"));

            return (DB_SERVICE_DIAGNOSTIC_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceDiagnosticResultFilter",
                new DB.AnalyzeDataSet(GetServiceDiagnosticResult),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_DIAGNOSTIC_RESULT", IdServiceDiagnosticResult),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@IS_SERVICE_RESULT", IsServiceResult),
                    new DB.InParameter("@IS_DIAGNOSTIC_RESULT", IsDiagnosticResult),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceInPackageFilter

        public DB_SERVICE_IN_PACKAGE[] GetServiceInPackageFilter(long[] IdServiceInPackage, long[] IdServicePackage, int[] IdService, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceInPackageFilter"));

            return (DB_SERVICE_IN_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServiceInPackageFilter",
                new DB.AnalyzeDataSet(GetServiceInPackage),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_IN_PACKAGE", IdServiceInPackage),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_SERVICE", IdService),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListFilter

        public DB_SERVICE_LIST[] GetServiceListFilter(int[] IdServiceList, int[] IdDistributor, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate, int[] IdOperatorReceiver, int[] IdOperatorServicer,
                            string CourierName, int[] Priority, string ShippingLetterNbr, TypeDateTimeCode ExpectedFinishDate, string ServiceName,
                            int[] IdDocumentPackage, bool? Confirmed, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListFilter"));

            return (DB_SERVICE_LIST[])DB.ExecuteProcedure(
                "imrse_GetServiceListFilter",
                new DB.AnalyzeDataSet(GetServiceList),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    CreateTableParam("@ID_OPERATOR_RECEIVER", IdOperatorReceiver),
                    CreateTableParam("@ID_OPERATOR_SERVICER", IdOperatorServicer),
                    new DB.InParameter("@COURIER_NAME", CourierName),
                    CreateTableParam("@PRIORITY", Priority),
                    new DB.InParameter("@SHIPPING_LETTER_NBR", ShippingLetterNbr),
                    CreateTableParam("@EXPECTED_FINISH_DATE_CODE", ExpectedFinishDate),
                    new DB.InParameter("@SERVICE_NAME", ServiceName),
                    CreateTableParam("@ID_DOCUMENT_PACKAGE", IdDocumentPackage),
                    new DB.InParameter("@CONFIRMED", Confirmed),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListDataFilter

        public DB_SERVICE_LIST_DATA[] GetServiceListDataFilter(long[] IdServiceListData, int[] IdServiceList, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDataFilter"));

            return (DB_SERVICE_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceListDataFilter",
                new DB.AnalyzeDataSet(GetServiceListData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST_DATA", IdServiceListData),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListDeviceFilter

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDeviceFilter(int[] IdServiceList, long[] SerialNbr, string FailureDescription, string FailureSuggestion, string Notes, long[] Photo,
                            int[] IdOperatorServicer, string ServiceShortDescr, string ServiceLongDescr, long[] PhotoServiced, bool? AtexOk,
                            bool? PayForService, int[] IdServiceStatus, int[] IdDiagnosticStatus, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceFilter"));

            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceFilter",
                new DB.AnalyzeDataSet(GetServiceListDevice),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@FAILURE_DESCRIPTION", FailureDescription),
                    new DB.InParameter("@FAILURE_SUGGESTION", FailureSuggestion),
                    new DB.InParameter("@NOTES", Notes),
                    CreateTableParam("@PHOTO", Photo),
                    CreateTableParam("@ID_OPERATOR_SERVICER", IdOperatorServicer),
                    new DB.InParameter("@SERVICE_SHORT_DESCR", ServiceShortDescr),
                    new DB.InParameter("@SERVICE_LONG_DESCR", ServiceLongDescr),
                    CreateTableParam("@PHOTO_SERVICED", PhotoServiced),
                    new DB.InParameter("@ATEX_OK", AtexOk),
                    new DB.InParameter("@PAY_FOR_SERVICE", PayForService),
                    CreateTableParam("@ID_SERVICE_STATUS", IdServiceStatus),
                    CreateTableParam("@ID_DIAGNOSTIC_STATUS", IdDiagnosticStatus),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListDeviceDataFilter

        public DB_SERVICE_LIST_DEVICE_DATA[] GetServiceListDeviceDataFilter(long[] IdServiceListDeviceData, int[] IdServiceList, long[] SerialNbr, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceDataFilter"));
            if (UseBulkInsertSqlTableType && (
                    (IdServiceListDeviceData != null && IdServiceListDeviceData.Length > MaxItemsPerSqlTableType) ||
                    (IdServiceList != null && IdServiceList.Length > MaxItemsPerSqlTableType) ||
                    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                    (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                    (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                    ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdServiceListDeviceData != null) args.AddRange(IdServiceListDeviceData.Select(s => new Tuple<int, object>(1, s)));
                if (IdServiceList != null) args.AddRange(IdServiceList.Select(s => new Tuple<int, object>(2, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(4, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(5, s)));

                DB_SERVICE_LIST_DEVICE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_SERVICE_LIST_DEVICE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetServiceListDeviceDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetServiceListDeviceData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_SERVICE_LIST_DEVICE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetServiceListDeviceDataFilter",
                    new DB.AnalyzeDataSet(GetServiceListDeviceData),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST_DEVICE_DATA", IdServiceListDeviceData),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetServiceListDeviceActionFilter

        public DB_SERVICE_LIST_DEVICE_ACTION[] GetServiceListDeviceActionFilter(long[] SerialNbr, int[] IdServiceList, long[] IdServicePackage, long[] IdServiceCustom, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceActionFilter"));

            return (DB_SERVICE_LIST_DEVICE_ACTION[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceActionFilter",
                new DB.AnalyzeDataSet(GetServiceListDeviceAction),
                new DB.Parameter[] {
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_SERVICE_CUSTOM", IdServiceCustom),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListDeviceActionDetailsFilter

        public DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] GetServiceListDeviceActionDetailsFilter(long[] IdServiceListDeviceActionDetails, long[] SerialNbr, int[] IdServiceList, long[] IdServicePackage, int[] IdService, string InputText,
                            int[] IdServiceActionResult, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceActionDetailsFilter"));

            return (DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceActionDetailsFilter",
                new DB.AnalyzeDataSet(GetServiceListDeviceActionDetails),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST_DEVICE_ACTION_DETAILS", IdServiceListDeviceActionDetails),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_SERVICE", IdService),
                    new DB.InParameter("@INPUT_TEXT", InputText),
                    CreateTableParam("@ID_SERVICE_ACTION_RESULT", IdServiceActionResult),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceListDeviceDiagnosticFilter

        public DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] GetServiceListDeviceDiagnosticFilter(long[] SerialNbr, int[] IdServiceList, int[] IdDiagnosticAction, int[] IdDiagnosticActionResult, string InputText, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceDiagnosticFilter"));

            return (DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceDiagnosticFilter",
                new DB.AnalyzeDataSet(GetServiceListDeviceDiagnostic),
                new DB.Parameter[] {
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION", IdDiagnosticAction),
                    CreateTableParam("@ID_DIAGNOSTIC_ACTION_RESULT", IdDiagnosticActionResult),
                    new DB.InParameter("@INPUT_TEXT", InputText),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServicePackageFilter

        public DB_SERVICE_PACKAGE[] GetServicePackageFilter(long[] IdServicePackage, int[] IdContract, string Name, string Descr, bool? IsSingle, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServicePackageFilter"));

            return (DB_SERVICE_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServicePackageFilter",
                new DB.AnalyzeDataSet(GetServicePackage),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_CONTRACT", IdContract),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCR", Descr),
                    new DB.InParameter("@IS_SINGLE", IsSingle),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServicePackageDataFilter

        public DB_SERVICE_PACKAGE_DATA[] GetServicePackageDataFilter(long[] IdServicePackageData, long[] IdServicePackage, long[] IdDataType, int[] IndexNbr, int[] Min, int[] Max,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServicePackageDataFilter"));

            return (DB_SERVICE_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServicePackageDataFilter",
                new DB.AnalyzeDataSet(GetServicePackageData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_PACKAGE_DATA", IdServicePackageData),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    CreateTableParam("@MIN", Min),
                    CreateTableParam("@MAX", Max),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceReferenceTypeFilter

        public DB_SERVICE_REFERENCE_TYPE[] GetServiceReferenceTypeFilter(int[] IdServiceReferenceType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceReferenceTypeFilter"));

            return (DB_SERVICE_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetServiceReferenceTypeFilter",
                new DB.AnalyzeDataSet(GetServiceReferenceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", IdServiceReferenceType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceSummaryFilter

        public DB_SERVICE_SUMMARY[] GetServiceSummaryFilter(int[] IdServiceList, long[] IdServicePackage, int[] Amount, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceSummaryFilter"));

            return (DB_SERVICE_SUMMARY[])DB.ExecuteProcedure(
                "imrse_GetServiceSummaryFilter",
                new DB.AnalyzeDataSet(GetServiceSummary),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@AMOUNT", Amount),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetServiceSummaryDataFilter

        public DB_SERVICE_SUMMARY_DATA[] GetServiceSummaryDataFilter(long[] IdServiceSummaryData, int[] IdServiceList, long[] IdServicePackage, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceSummaryDataFilter"));

            return (DB_SERVICE_SUMMARY_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceSummaryDataFilter",
                new DB.AnalyzeDataSet(GetServiceSummaryData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SERVICE_SUMMARY_DATA", IdServiceSummaryData),
                    CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
                    CreateTableParam("@ID_SERVICE_PACKAGE", IdServicePackage),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSessionFilter

        public DB_SESSION[] GetSessionFilter(long[] IdSession, int[] IdModule, int[] IdOperator, int[] IdSessionStatus, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionFilter"));

            return (DB_SESSION[])DB.ExecuteProcedure(
                "imrse_GetSessionFilter",
                new DB.AnalyzeDataSet(GetSession),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SESSION", IdSession),
                    CreateTableParam("@ID_MODULE", IdModule),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_SESSION_STATUS", IdSessionStatus),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSessionDataFilter

        public DB_SESSION_DATA[] GetSessionDataFilter(long[] IdSessionData, long[] IdSession, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionDataFilter"));

            return (DB_SESSION_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionDataFilter",
                new DB.AnalyzeDataSet(GetSessionData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SESSION_DATA", IdSessionData),
                    CreateTableParam("@ID_SESSION", IdSession),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSessionEventFilter

        public DB_SESSION_EVENT[] GetSessionEventFilter(long[] IdSessionEvent, long[] IdSession, int[] IdEvent, TypeDateTimeCode Time, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionEventFilter"));

            return (DB_SESSION_EVENT[])DB.ExecuteProcedure(
                "imrse_GetSessionEventFilter",
                new DB.AnalyzeDataSet(GetSessionEvent),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SESSION_EVENT", IdSessionEvent),
                    CreateTableParam("@ID_SESSION", IdSession),
                    CreateTableParam("@ID_EVENT", IdEvent),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSessionEventDataFilter

        public DB_SESSION_EVENT_DATA[] GetSessionEventDataFilter(long[] IdSessionEventData, long[] IdSessionEvent, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionEventDataFilter"));

            return (DB_SESSION_EVENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionEventDataFilter",
                new DB.AnalyzeDataSet(GetSessionEventData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SESSION_EVENT_DATA", IdSessionEventData),
                    CreateTableParam("@ID_SESSION_EVENT", IdSessionEvent),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSessionStatusFilter

        public DB_SESSION_STATUS[] GetSessionStatusFilter(int[] IdSessionStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionStatusFilter"));

            return (DB_SESSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetSessionStatusFilter",
                new DB.AnalyzeDataSet(GetSessionStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SESSION_STATUS", IdSessionStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetShippingListFilter

        public DB_SHIPPING_LIST[] GetShippingListFilter(int[] IdShippingList, string ContractNbr, string ShippingListNbr, int[] IdOperatorCreator, int[] IdOperatorContact, int[] IdOperatorQuality,
                    int[] IdOperatorProtocol, string ShippingLetterNbr, long[] IdLocation, int[] IdShippingListType, TypeDateTimeCode CreationDate,
                    TypeDateTimeCode FinishDate, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListFilter"));
            if (UseBulkInsertSqlTableType && (
                (IdShippingList != null && IdShippingList.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorCreator != null && IdOperatorCreator.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorContact != null && IdOperatorContact.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorQuality != null && IdOperatorQuality.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorProtocol != null && IdOperatorProtocol.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdShippingListType != null && IdShippingListType.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdShippingList != null) args.AddRange(IdShippingList.Select(s => new Tuple<int, object>(1, s)));
                if (IdOperatorCreator != null) args.AddRange(IdOperatorCreator.Select(s => new Tuple<int, object>(4, s)));
                if (IdOperatorContact != null) args.AddRange(IdOperatorContact.Select(s => new Tuple<int, object>(5, s)));
                if (IdOperatorQuality != null) args.AddRange(IdOperatorQuality.Select(s => new Tuple<int, object>(6, s)));
                if (IdOperatorProtocol != null) args.AddRange(IdOperatorProtocol.Select(s => new Tuple<int, object>(7, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(9, s)));
                if (IdShippingListType != null) args.AddRange(IdShippingListType.Select(s => new Tuple<int, object>(10, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(13, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(13, s)));

                DB_SHIPPING_LIST[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_SHIPPING_LIST[])DB.ExecuteProcedure(
                        "imrse_GetShippingListFilterTableArgs",
                        new DB.AnalyzeDataSet(GetShippingList),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@CONTRACT_NBR", ContractNbr),
                    new DB.InParameter("@SHIPPING_LIST_NBR", ShippingListNbr),
                    new DB.InParameter("@SHIPPING_LETTER_NBR", ShippingLetterNbr),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@FINISH_DATE_CODE", FinishDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_SHIPPING_LIST[])DB.ExecuteProcedure(
                    "imrse_GetShippingListFilter",
                    new DB.AnalyzeDataSet(GetShippingList),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
                    new DB.InParameter("@CONTRACT_NBR", ContractNbr),
                    new DB.InParameter("@SHIPPING_LIST_NBR", ShippingListNbr),
                    CreateTableParam("@ID_OPERATOR_CREATOR", IdOperatorCreator),
                    CreateTableParam("@ID_OPERATOR_CONTACT", IdOperatorContact),
                    CreateTableParam("@ID_OPERATOR_QUALITY", IdOperatorQuality),
                    CreateTableParam("@ID_OPERATOR_PROTOCOL", IdOperatorProtocol),
                    new DB.InParameter("@SHIPPING_LETTER_NBR", ShippingLetterNbr),
                    CreateTableParam("@ID_LOCATION", IdLocation),
                    CreateTableParam("@ID_SHIPPING_LIST_TYPE", IdShippingListType),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    CreateTableParam("@FINISH_DATE_CODE", FinishDate),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetShippingListDataFilter

        public DB_SHIPPING_LIST_DATA[] GetShippingListDataFilter(long[] IdShippingListData, int[] IdShippingList, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListDataFilter"));

            return (DB_SHIPPING_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetShippingListDataFilter",
                new DB.AnalyzeDataSet(GetShippingListData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SHIPPING_LIST_DATA", IdShippingListData),
                    CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetShippingListDeviceFilter

        public DB_SHIPPING_LIST_DEVICE[] GetShippingListDeviceFilter(int[] IdShippingListDevice, int[] IdShippingList, long[] SerialNbr, long[] IdMeter, long[] IdArticle, TypeDateTimeCode InsertDate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListDeviceFilter"));
            if (UseBulkInsertSqlTableType && (
                (IdShippingListDevice != null && IdShippingListDevice.Length > MaxItemsPerSqlTableType) ||
                (IdShippingList != null && IdShippingList.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdArticle != null && IdArticle.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdShippingListDevice != null) args.AddRange(IdShippingListDevice.Select(s => new Tuple<int, object>(1, s)));
                if (IdShippingList != null) args.AddRange(IdShippingList.Select(s => new Tuple<int, object>(2, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(3, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(4, s)));
                if (IdArticle != null) args.AddRange(IdArticle.Select(s => new Tuple<int, object>(5, s)));

                DB_SHIPPING_LIST_DEVICE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_SHIPPING_LIST_DEVICE[])DB.ExecuteProcedure(
                        "imrse_GetShippingListDeviceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetShippingListDevice),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@INSERT_DATE_CODE", InsertDate),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                                autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_SHIPPING_LIST_DEVICE[])DB.ExecuteProcedure(
                    "imrse_GetShippingListDeviceFilter",
                    new DB.AnalyzeDataSet(GetShippingListDevice),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_SHIPPING_LIST_DEVICE", IdShippingListDevice),
                    CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@ID_METER", IdMeter),
                    CreateTableParam("@ID_ARTICLE", IdArticle),
                    CreateTableParam("@INSERT_DATE_CODE", InsertDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetShippingListTypeFilter

        public DB_SHIPPING_LIST_TYPE[] GetShippingListTypeFilter(int[] IdShippingListType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListTypeFilter"));

            return (DB_SHIPPING_LIST_TYPE[])DB.ExecuteProcedure(
                "imrse_GetShippingListTypeFilter",
                new DB.AnalyzeDataSet(GetShippingListType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SHIPPING_LIST_TYPE", IdShippingListType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSimCardFilter

        public DB_SIM_CARD[] GetSimCardFilter(int[] IdSimCard, int[] IdDistributor, string SerialNbr, string Phone, string Pin, string Puk,
                            int[] MobileNetworkCode, string Ip, string ApnLogin, string ApnPassword, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSimCardFilter"));

            return (DB_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_GetSimCardFilter",
                new DB.AnalyzeDataSet(GetSimCard),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SIM_CARD", IdSimCard),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@PHONE", Phone),
                    new DB.InParameter("@PIN", Pin),
                    new DB.InParameter("@PUK", Puk),
                    CreateTableParam("@MOBILE_NETWORK_CODE", MobileNetworkCode),
                    new DB.InParameter("@IP", Ip),
                    new DB.InParameter("@APN_LOGIN", ApnLogin),
                    new DB.InParameter("@APN_PASSWORD", ApnPassword),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSimCardDataFilter

        public DB_SIM_CARD_DATA[] GetSimCardDataFilter(long[] IdSimCardData, int[] IdSimCard, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSimCardDataFilter"));

            return (DB_SIM_CARD_DATA[])DB.ExecuteProcedure(
                "imrse_GetSimCardDataFilter",
                new DB.AnalyzeDataSet(GetSimCardData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SIM_CARD_DATA", IdSimCardData),
                    CreateTableParam("@ID_SIM_CARD", IdSimCard),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSlaFilter

        public DB_SLA[] GetSlaFilter(int[] IdSla, int[] IdDescr, string Description, string IssueReceiveWd, string IssueReceiveFd, string TechnicalSupportWd,
                            string TechnicalSupportFd, int[] IssueResponseWd, int[] IssueResponseFd, int[] SystemFailureRemoveWd, int[] SystemFailureRemoveFd,
                            int[] ObjectFailureRemoveWd, int[] ObjectFailureRemoveFd, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSlaFilter"));

            return (DB_SLA[])DB.ExecuteProcedure(
                "imrse_GetSlaFilter",
                new DB.AnalyzeDataSet(GetSla),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SLA", IdSla),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@ISSUE_RECEIVE_WD", IssueReceiveWd),
                    new DB.InParameter("@ISSUE_RECEIVE_FD", IssueReceiveFd),
                    new DB.InParameter("@TECHNICAL_SUPPORT_WD", TechnicalSupportWd),
                    new DB.InParameter("@TECHNICAL_SUPPORT_FD", TechnicalSupportFd),
                    CreateTableParam("@ISSUE_RESPONSE_WD", IssueResponseWd),
                    CreateTableParam("@ISSUE_RESPONSE_FD", IssueResponseFd),
                    CreateTableParam("@SYSTEM_FAILURE_REMOVE_WD", SystemFailureRemoveWd),
                    CreateTableParam("@SYSTEM_FAILURE_REMOVE_FD", SystemFailureRemoveFd),
                    CreateTableParam("@OBJECT_FAILURE_REMOVE_WD", ObjectFailureRemoveWd),
                    CreateTableParam("@OBJECT_FAILURE_REMOVE_FD", ObjectFailureRemoveFd),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSlotTypeFilter

        public DB_SLOT_TYPE[] GetSlotTypeFilter(int[] IdSlotType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSlotTypeFilter"));

            return (DB_SLOT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetSlotTypeFilter",
                new DB.AnalyzeDataSet(GetSlotType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SLOT_TYPE", IdSlotType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSupplierFilter

        public DB_SUPPLIER[] GetSupplierFilter(int[] IdSupplier, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSupplierFilter"));

            return (DB_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetSupplierFilter",
                new DB.AnalyzeDataSet(GetSupplier),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SUPPLIER", IdSupplier),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSystemDataFilter

        public DB_SYSTEM_DATA[] GetSystemDataFilter(long[] IdSystemData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));

            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemDataFilter",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SYSTEM_DATA", IdSystemData),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTariffFilter

        public DB_TARIFF[] GetTariffFilter(int[] IdTariff, int[] IdDistributor, string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffFilter"));

            return (DB_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetTariffFilter",
                new DB.AnalyzeDataSet(GetTariff),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TARIFF", IdTariff),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTariffDataFilter

        public DB_TARIFF_DATA[] GetTariffDataFilter(long[] IdTariffData, int[] IdTariff, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffDataFilter"));

            return (DB_TARIFF_DATA[])DB.ExecuteProcedure(
                "imrse_GetTariffDataFilter",
                new DB.AnalyzeDataSet(GetTariffData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TARIFF_DATA", IdTariffData),
                    CreateTableParam("@ID_TARIFF", IdTariff),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTariffSettlementPeriodFilter
        public DB_TARIFF_SETTLEMENT_PERIOD[] GetTariffSettlementPeriodFilter(int[] IdTariffSettlementPeriod, int[] IdTariff, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffSettlementPeriodFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTariffSettlementPeriod != null && IdTariffSettlementPeriod.Length > MaxItemsPerSqlTableType) ||
                (IdTariff != null && IdTariff.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTariffSettlementPeriod != null) args.AddRange(IdTariffSettlementPeriod.Select(s => new Tuple<int, object>(1, s)));
                if (IdTariff != null) args.AddRange(IdTariff.Select(s => new Tuple<int, object>(2, s)));

                DB_TARIFF_SETTLEMENT_PERIOD[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TARIFF_SETTLEMENT_PERIOD[])DB.ExecuteProcedure(
                        "imrse_GetTariffSettlementPeriodFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTariffSettlementPeriod),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_TARIFF_SETTLEMENT_PERIOD[])DB.ExecuteProcedure(
                    "imrse_GetTariffSettlementPeriodFilter",
                    new DB.AnalyzeDataSet(GetTariffSettlementPeriod),
                    new DB.Parameter[] {
                CreateTableParam("@ID_TARIFF_SETTLEMENT_PERIOD", IdTariffSettlementPeriod),
                CreateTableParam("@ID_TARIFF", IdTariff),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion


        #region GetTaskFilter

        public DB_TASK[] GetTaskFilter(int[] IdTask, int[] IdTaskType, int[] IdIssue, int[] IdTaskGroup, long[] IdPlannedRoute, int[] IdOperatorRegistering,
                            int[] IdOperatorPerformer, int[] IdTaskStatus, TypeDateTimeCode CreationDate, bool? Accepted, int[] IdOperatorAccepted,
                            TypeDateTimeCode AcceptanceDate, string Notes, TypeDateTimeCode Deadline, long[] IdLocation, int[] IdActor,
                            string TopicNumber, int[] Priority, long[] OperationCode, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskFilter"));
            if (UseBulkInsertSqlTableType && (
                (IdTask != null && IdTask.Length > MaxItemsPerSqlTableType) ||
                (IdTaskType != null && IdTaskType.Length > MaxItemsPerSqlTableType) ||
                (IdIssue != null && IdIssue.Length > MaxItemsPerSqlTableType) ||
                ((IdTaskGroup != null && IdTaskGroup.Length > MaxItemsPerSqlTableType) || (IdTaskGroup == null && TaskGroupFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdPlannedRoute != null && IdPlannedRoute.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorRegistering != null && IdOperatorRegistering.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorPerformer != null && IdOperatorPerformer.Length > MaxItemsPerSqlTableType) ||
                (IdTaskStatus != null && IdTaskStatus.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorAccepted != null && IdOperatorAccepted.Length > MaxItemsPerSqlTableType) ||
                ((IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) || (IdLocation == null && LocationFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdActor != null && IdActor.Length > MaxItemsPerSqlTableType) ||
                (Priority != null && Priority.Length > MaxItemsPerSqlTableType) ||
                (OperationCode != null && OperationCode.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTask != null) args.AddRange(IdTask.Select(s => new Tuple<int, object>(1, s)));
                if (IdTaskType != null) args.AddRange(IdTaskType.Select(s => new Tuple<int, object>(2, s)));
                if (IdIssue != null) args.AddRange(IdIssue.Select(s => new Tuple<int, object>(3, s)));
                if (IdTaskGroup != null) args.AddRange(IdTaskGroup.Select(s => new Tuple<int, object>(4, s)));
                else if (TaskGroupFilter.Length > 0) args.AddRange(TaskGroupFilter.Select(s => new Tuple<int, object>(4, s)));
                if (IdPlannedRoute != null) args.AddRange(IdPlannedRoute.Select(s => new Tuple<int, object>(5, s)));
                if (IdOperatorRegistering != null) args.AddRange(IdOperatorRegistering.Select(s => new Tuple<int, object>(6, s)));
                if (IdOperatorPerformer != null) args.AddRange(IdOperatorPerformer.Select(s => new Tuple<int, object>(7, s)));
                if (IdTaskStatus != null) args.AddRange(IdTaskStatus.Select(s => new Tuple<int, object>(8, s)));
                if (IdOperatorAccepted != null) args.AddRange(IdOperatorAccepted.Select(s => new Tuple<int, object>(11, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(15, s)));
                else if (LocationFilter != null) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(15, s)));
                if (IdActor != null) args.AddRange(IdActor.Select(s => new Tuple<int, object>(16, s)));
                if (Priority != null) args.AddRange(Priority.Select(s => new Tuple<int, object>(18, s)));
                if (OperationCode != null) args.AddRange(OperationCode.Select(s => new Tuple<int, object>(19, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(20, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(20, s)));

                DB_TASK[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TASK[])DB.ExecuteProcedure(
                        "imrse_GetTaskFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTask),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@ACCEPTED", Accepted),
                    CreateTableParam("@ACCEPTANCE_DATE_CODE", AcceptanceDate),
                    new DB.InParameter("@NOTES", Notes),
                    CreateTableParam("@DEADLINE_CODE", Deadline),
                    new DB.InParameter("@TOPIC_NUMBER", TopicNumber),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_TASK[])DB.ExecuteProcedure(
                    "imrse_GetTaskFilter",
                    new DB.AnalyzeDataSet(GetTask),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_TASK", IdTask),
                    CreateTableParam("@ID_TASK_TYPE", IdTaskType),
                    CreateTableParam("@ID_ISSUE", IdIssue),
                    CreateTableParam("@ID_TASK_GROUP", IdTaskGroup != null ? IdTaskGroup : TaskGroupFilter),
                    CreateTableParam("@ID_PLANNED_ROUTE", IdPlannedRoute),
                    CreateTableParam("@ID_OPERATOR_REGISTERING", IdOperatorRegistering),
                    CreateTableParam("@ID_OPERATOR_PERFORMER", IdOperatorPerformer),
                    CreateTableParam("@ID_TASK_STATUS", IdTaskStatus),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@ACCEPTED", Accepted),
                    CreateTableParam("@ID_OPERATOR_ACCEPTED", IdOperatorAccepted),
                    CreateTableParam("@ACCEPTANCE_DATE_CODE", AcceptanceDate),
                    new DB.InParameter("@NOTES", Notes),
                    CreateTableParam("@DEADLINE_CODE", Deadline),
                    CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
                    CreateTableParam("@ID_ACTOR", IdActor),
                    new DB.InParameter("@TOPIC_NUMBER", TopicNumber),
                    CreateTableParam("@PRIORITY", Priority),
                    CreateTableParam("@OPERATION_CODE", OperationCode),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetTaskDataFilter

        public DB_TASK_DATA[] GetTaskDataFilter(long[] IdTaskData, int[] IdTask, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskDataFilter"));
            if (UseBulkInsertSqlTableType && (
                (IdTaskData != null && IdTaskData.Length > MaxItemsPerSqlTableType) ||
                (IdTask != null && IdTask.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTaskData != null) args.AddRange(IdTaskData.Select(s => new Tuple<int, object>(1, s)));
                if (IdTask != null) args.AddRange(IdTask.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_TASK_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TASK_DATA[])DB.ExecuteProcedure(
                        "imrse_GetTaskDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTaskData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun�c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_TASK_DATA[])DB.ExecuteProcedure(
                    "imrse_GetTaskDataFilter",
                    new DB.AnalyzeDataSet(GetTaskData),
                    new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_DATA", IdTaskData),
                    CreateTableParam("@ID_TASK", IdTask),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetTaskGroupFilter

        public DB_TASK_GROUP[] GetTaskGroupFilter(int[] IdTaskGroup, int[] IdParrentGroup, string Name, TypeDateTimeCode DateCreated, int[] IdDistributor, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskGroupFilter"));

            return (DB_TASK_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskGroupFilter",
                new DB.AnalyzeDataSet(GetTaskGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_GROUP", IdTaskGroup != null ? IdTaskGroup : TaskGroupFilter),
                    CreateTableParam("@ID_PARRENT_GROUP", IdParrentGroup),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@DATE_CREATED_CODE", DateCreated),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTaskHistoryFilter

        public DB_TASK_HISTORY[] GetTaskHistoryFilter(int[] IdTaskHistory, int[] IdTask, int[] IdTaskStatus, int[] IdOperator, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                            string Notes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskHistoryFilter"));

            return (DB_TASK_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetTaskHistoryFilter",
                new DB.AnalyzeDataSet(GetTaskHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_HISTORY", IdTaskHistory),
                    CreateTableParam("@ID_TASK", IdTask),
                    CreateTableParam("@ID_TASK_STATUS", IdTaskStatus),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@START_DATE_CODE", StartDate),
                    CreateTableParam("@END_DATE_CODE", EndDate),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTaskStatusFilter

        public DB_TASK_STATUS[] GetTaskStatusFilter(int[] IdTaskStatus, long[] IdDescr, string Description, bool? IsFinished, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskStatusFilter"));

            return (DB_TASK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTaskStatusFilter",
                new DB.AnalyzeDataSet(GetTaskStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_STATUS", IdTaskStatus),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@DESCRIPTION", Description),
                    new DB.InParameter("@IS_FINISHED", IsFinished),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTaskTypeFilter

        public DB_TASK_TYPE[] GetTaskTypeFilter(int[] IdTaskType, int[] IdTaskTypeGroup, string Name, string Descr, bool? Remote, int[] DurationEstimate,
                            int[] IdOperatorCreator, TypeDateTimeCode CreationDate, bool? Authorized, bool? ImrOperator, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskTypeFilter"));

            return (DB_TASK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTaskTypeFilter",
                new DB.AnalyzeDataSet(GetTaskType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_TYPE", IdTaskType),
                    CreateTableParam("@ID_TASK_TYPE_GROUP", IdTaskTypeGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCR", Descr),
                    new DB.InParameter("@REMOTE", Remote),
                    CreateTableParam("@DURATION_ESTIMATE", DurationEstimate),
                    CreateTableParam("@ID_OPERATOR_CREATOR", IdOperatorCreator),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@AUTHORIZED", Authorized),
                    new DB.InParameter("@IMR_OPERATOR", ImrOperator),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTaskTypeGroupFilter

        public DB_TASK_TYPE_GROUP[] GetTaskTypeGroupFilter(int[] IdTaskTypeGroup, string Name, string Descr, int[] IdTaskTypeGroupParent, int[] ILeft, int[] IRight,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskTypeGroupFilter"));

            return (DB_TASK_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskTypeGroupFilter",
                new DB.AnalyzeDataSet(GetTaskTypeGroup),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TASK_TYPE_GROUP", IdTaskTypeGroup),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@DESCR", Descr),
                    CreateTableParam("@ID_TASK_TYPE_GROUP_PARENT", IdTaskTypeGroupParent),
                    CreateTableParam("@I_LEFT", ILeft),
                    CreateTableParam("@I_RIGHT", IRight),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTransmissionStatusFilter

        public DB_TRANSMISSION_STATUS[] GetTransmissionStatusFilter(int[] IdTransmissionStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionStatusFilter"));

            return (DB_TRANSMISSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTransmissionStatusFilter",
                new DB.AnalyzeDataSet(GetTransmissionStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TRANSMISSION_STATUS", IdTransmissionStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTransmissionTypeFilter

        public DB_TRANSMISSION_TYPE[] GetTransmissionTypeFilter(int[] IdTransmissionType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionTypeFilter"));

            return (DB_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionTypeFilter",
                new DB.AnalyzeDataSet(GetTransmissionType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetUniqueTypeFilter

        public DB_UNIQUE_TYPE[] GetUniqueTypeFilter(int[] IdUniqueType, long[] IdDescr, string ProcName, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetUniqueTypeFilter"));

            return (DB_UNIQUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetUniqueTypeFilter",
                new DB.AnalyzeDataSet(GetUniqueType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_UNIQUE_TYPE", IdUniqueType),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@PROC_NAME", ProcName),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetUnitFilter

        public DB_UNIT[] GetUnitFilter(int[] IdUnit, int[] IdUnitBase, Double[] Scale, Double[] Bias, string Name, long[] IdDescr,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetUnitFilter"));

            return (DB_UNIT[])DB.ExecuteProcedure(
                "imrse_GetUnitFilter",
                new DB.AnalyzeDataSet(GetUnit),
                new DB.Parameter[] {
                    CreateTableParam("@ID_UNIT", IdUnit),
                    CreateTableParam("@ID_UNIT_BASE", IdUnitBase),
                    CreateTableParam("@SCALE", Scale),
                    CreateTableParam("@BIAS", Bias),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetVersionFilter

        public DB_VERSION[] GetVersionFilter(long[] IdVersion, int[] IdVersionState, int[] IdVersionElementType, int[] IdVersionType, string VersionNbr, long[] VersionRaw,
                            TypeDateTimeCode ReleaseDate, int[] IdOperatorPublisher, TypeDateTimeCode NotificationDate, int[] IdOperatorNotifier, TypeDateTimeCode ConfirmDate,
                            int[] IdOperatorCofirming, string ReleaseNotes, int[] IdPriority, TypeDateTimeCode Deadline, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType) ||
                (IdVersionState != null && IdVersionState.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementType != null && IdVersionElementType.Length > MaxItemsPerSqlTableType) ||
                (IdVersionType != null && IdVersionType.Length > MaxItemsPerSqlTableType) ||
                (VersionRaw != null && VersionRaw.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorPublisher != null && IdOperatorPublisher.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorNotifier != null && IdOperatorNotifier.Length > MaxItemsPerSqlTableType) ||
                (IdOperatorCofirming != null && IdOperatorCofirming.Length > MaxItemsPerSqlTableType) ||
                (IdPriority != null && IdPriority.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionState != null) args.AddRange(IdVersionState.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionElementType != null) args.AddRange(IdVersionElementType.Select(s => new Tuple<int, object>(3, s)));
                if (IdVersionType != null) args.AddRange(IdVersionType.Select(s => new Tuple<int, object>(4, s)));
                if (VersionRaw != null) args.AddRange(VersionRaw.Select(s => new Tuple<int, object>(6, s)));
                if (IdOperatorPublisher != null) args.AddRange(IdOperatorPublisher.Select(s => new Tuple<int, object>(8, s)));
                if (IdOperatorNotifier != null) args.AddRange(IdOperatorNotifier.Select(s => new Tuple<int, object>(10, s)));
                if (IdOperatorCofirming != null) args.AddRange(IdOperatorCofirming.Select(s => new Tuple<int, object>(12, s)));
                if (IdPriority != null) args.AddRange(IdPriority.Select(s => new Tuple<int, object>(14, s)));

                DB_VERSION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION[])DB.ExecuteProcedure(
                        "imrse_GetVersionFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersion),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@VERSION_NBR", VersionNbr),
                    CreateTableParam("@RELEASE_DATE_CODE", ReleaseDate),
                    CreateTableParam("@NOTIFICATION_DATE_CODE", NotificationDate),
                    CreateTableParam("@CONFIRM_DATE_CODE", ConfirmDate),
                    new DB.InParameter("@RELEASE_NOTES", ReleaseNotes),
                    CreateTableParam("@DEADLINE_CODE", Deadline),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION[])DB.ExecuteProcedure(
                    "imrse_GetVersionFilter",
                    new DB.AnalyzeDataSet(GetVersion),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VERSION", IdVersion),
                CreateTableParam("@ID_VERSION_STATE", IdVersionState),
                CreateTableParam("@ID_VERSION_ELEMENT_TYPE", IdVersionElementType),
                CreateTableParam("@ID_VERSION_TYPE", IdVersionType),
                new DB.InParameter("@VERSION_NBR", VersionNbr),
                CreateTableParam("@VERSION_RAW", VersionRaw),
                CreateTableParam("@RELEASE_DATE_CODE", ReleaseDate),
                CreateTableParam("@ID_OPERATOR_PUBLISHER", IdOperatorPublisher),
                CreateTableParam("@NOTIFICATION_DATE_CODE", NotificationDate),
                CreateTableParam("@ID_OPERATOR_NOTIFIER", IdOperatorNotifier),
                CreateTableParam("@CONFIRM_DATE_CODE", ConfirmDate),
                CreateTableParam("@ID_OPERATOR_COFIRMING", IdOperatorCofirming),
                new DB.InParameter("@RELEASE_NOTES", ReleaseNotes),
                CreateTableParam("@ID_PRIORITY", IdPriority),
                CreateTableParam("@DEADLINE_CODE", Deadline),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        #endregion
        #region GetViewFilter

        public DB_VIEW[] GetViewFilter(int[] IdView, string Name, int[] IdReferenceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdView != null && IdView.Length > MaxItemsPerSqlTableType) ||
                (IdReferenceType != null && IdReferenceType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdView != null) args.AddRange(IdView.Select(s => new Tuple<int, object>(1, s)));
                if (IdReferenceType != null) args.AddRange(IdReferenceType.Select(s => new Tuple<int, object>(3, s)));

                DB_VIEW[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW[])DB.ExecuteProcedure(
                        "imrse_GetViewFilterTableArgs",
                        new DB.AnalyzeDataSet(GetView),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW[])DB.ExecuteProcedure(
                    "imrse_GetViewFilter",
                    new DB.AnalyzeDataSet(GetView),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW", IdView),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetViewColumnFilter

        public DB_VIEW_COLUMN[] GetViewColumnFilter(int[] IdViewColumn, int[] IdView, long[] IdDataType, string Name, string Table, bool? PrimaryKey,
                            long[] Size, int[] IdViewColumnSource, int[] IdViewColumnType, bool? Key, int[] IdKeyViewColumn,
                            long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdViewColumn != null && IdViewColumn.Length > MaxItemsPerSqlTableType) ||
                (IdView != null && IdView.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (Size != null && Size.Length > MaxItemsPerSqlTableType) ||
                (IdViewColumnSource != null && IdViewColumnSource.Length > MaxItemsPerSqlTableType) ||
                (IdViewColumnType != null && IdViewColumnType.Length > MaxItemsPerSqlTableType) ||
                (IdKeyViewColumn != null && IdKeyViewColumn.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdViewColumn != null) args.AddRange(IdViewColumn.Select(s => new Tuple<int, object>(1, s)));
                if (IdView != null) args.AddRange(IdView.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (Size != null) args.AddRange(Size.Select(s => new Tuple<int, object>(7, s)));
                if (IdViewColumnSource != null) args.AddRange(IdViewColumnSource.Select(s => new Tuple<int, object>(8, s)));
                if (IdViewColumnType != null) args.AddRange(IdViewColumnType.Select(s => new Tuple<int, object>(9, s)));
                if (IdKeyViewColumn != null) args.AddRange(IdKeyViewColumn.Select(s => new Tuple<int, object>(10, s)));

                DB_VIEW_COLUMN[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW_COLUMN[])DB.ExecuteProcedure(
                        "imrse_GetViewColumnFilterTableArgs",
                        new DB.AnalyzeDataSet(GetViewColumn),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TABLE", Table),
                    new DB.InParameter("@PRIMARY_KEY", PrimaryKey),
                    new DB.InParameter("@KEY", Key),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW_COLUMN[])DB.ExecuteProcedure(
                    "imrse_GetViewColumnFilter",
                    new DB.AnalyzeDataSet(GetViewColumn),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW_COLUMN", IdViewColumn),
                CreateTableParam("@ID_VIEW", IdView),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@TABLE", Table),
                new DB.InParameter("@PRIMARY_KEY", PrimaryKey),
                CreateTableParam("@SIZE", Size),
                CreateTableParam("@ID_VIEW_COLUMN_SOURCE", IdViewColumnSource),
                CreateTableParam("@ID_VIEW_COLUMN_TYPE", IdViewColumnType),
                new DB.InParameter("@KEY", Key),
                CreateTableParam("@ID_KEY_VIEW_COLUMN", IdKeyViewColumn),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetViewColumnDataFilter

        public DB_VIEW_COLUMN_DATA[] GetViewColumnDataFilter(long[] IdViewColumnData, int[] IdViewColumn, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdViewColumnData != null && IdViewColumnData.Length > MaxItemsPerSqlTableType) ||
                (IdViewColumn != null && IdViewColumn.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdViewColumnData != null) args.AddRange(IdViewColumnData.Select(s => new Tuple<int, object>(1, s)));
                if (IdViewColumn != null) args.AddRange(IdViewColumn.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_VIEW_COLUMN_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW_COLUMN_DATA[])DB.ExecuteProcedure(
                        "imrse_GetViewColumnDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetViewColumnData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW_COLUMN_DATA[])DB.ExecuteProcedure(
                    "imrse_GetViewColumnDataFilter",
                    new DB.AnalyzeDataSet(GetViewColumnData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW_COLUMN_DATA", IdViewColumnData),
                CreateTableParam("@ID_VIEW_COLUMN", IdViewColumn),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetViewColumnSourceFilter

        public DB_VIEW_COLUMN_SOURCE[] GetViewColumnSourceFilter(int[] IdViewColumnSource, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnSourceFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdViewColumnSource != null && IdViewColumnSource.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdViewColumnSource != null) args.AddRange(IdViewColumnSource.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_VIEW_COLUMN_SOURCE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW_COLUMN_SOURCE[])DB.ExecuteProcedure(
                        "imrse_GetViewColumnSourceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetViewColumnSource),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW_COLUMN_SOURCE[])DB.ExecuteProcedure(
                    "imrse_GetViewColumnSourceFilter",
                    new DB.AnalyzeDataSet(GetViewColumnSource),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW_COLUMN_SOURCE", IdViewColumnSource),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetViewColumnTypeFilter

        public DB_VIEW_COLUMN_TYPE[] GetViewColumnTypeFilter(int[] IdViewColumnType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdViewColumnType != null && IdViewColumnType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdViewColumnType != null) args.AddRange(IdViewColumnType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_VIEW_COLUMN_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW_COLUMN_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetViewColumnTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetViewColumnType),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW_COLUMN_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetViewColumnTypeFilter",
                    new DB.AnalyzeDataSet(GetViewColumnType),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW_COLUMN_TYPE", IdViewColumnType),
                new DB.InParameter("@NAME", Name),
                CreateTableParam("@ID_DESCR", IdDescr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetViewDataFilter

        public DB_VIEW_DATA[] GetViewDataFilter(long[] IdViewData, int[] IdView, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdViewData != null && IdViewData.Length > MaxItemsPerSqlTableType) ||
                (IdView != null && IdView.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdViewData != null) args.AddRange(IdViewData.Select(s => new Tuple<int, object>(1, s)));
                if (IdView != null) args.AddRange(IdView.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_VIEW_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VIEW_DATA[])DB.ExecuteProcedure(
                        "imrse_GetViewDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetViewData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VIEW_DATA[])DB.ExecuteProcedure(
                    "imrse_GetViewDataFilter",
                    new DB.AnalyzeDataSet(GetViewData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_VIEW_DATA", IdViewData),
                CreateTableParam("@ID_VIEW", IdView),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetWarrantyContractFilter

        public DB_WARRANTY_CONTRACT[] GetWarrantyContractFilter(int[] IdDeviceOrderNumber, int[] IdComponent, int[] IdContract, int[] WarrantyTime, int[] IdWarrantyStartDateCalculationMethod, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWarrantyContractFilter"));

            return (DB_WARRANTY_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetWarrantyContractFilter",
                new DB.AnalyzeDataSet(GetWarrantyContract),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
                    CreateTableParam("@ID_COMPONENT", IdComponent),
                    CreateTableParam("@ID_CONTRACT", IdContract),
                    CreateTableParam("@WARRANTY_TIME", WarrantyTime),
                    CreateTableParam("@ID_WARRANTY_START_DATE_CALCULATION_METHOD", IdWarrantyStartDateCalculationMethod),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetWmbusTransmissionTypeFilter

        public DB_WMBUS_TRANSMISSION_TYPE[] GetWmbusTransmissionTypeFilter(long[] IdWmbusTransmissionType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWmbusTransmissionTypeFilter"));

            return (DB_WMBUS_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWmbusTransmissionTypeFilter",
                new DB.AnalyzeDataSet(GetWmbusTransmissionType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WMBUS_TRANSMISSION_TYPE", IdWmbusTransmissionType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetWorkDataFilter
        public DB_WORK_DATA[] GetWorkDataFilter(long[] IdWorkData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkDataFilter"));

            return (DB_WORK_DATA[])DB.ExecuteProcedure(
                "imrse_GetWorkDataFilter",
                new DB.AnalyzeDataSet(GetWorkData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_DATA", IdWorkData),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetWorkHistoryFilter
        public DB_WORK_HISTORY[] GetWorkHistoryFilter(long[] IdWorkHistory, long[] IdWork, int[] IdWorkStatus, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long[] IdDescr,
                                                      int[] IdModule, string Assembly, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkHistoryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdWorkHistory != null && IdWorkHistory.Length > MaxItemsPerSqlTableType) ||
                (IdWork != null && IdWork.Length > MaxItemsPerSqlTableType) ||
                (IdWorkStatus != null && IdWorkStatus.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdWorkHistory != null) args.AddRange(IdWorkHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdWork != null) args.AddRange(IdWork.Select(s => new Tuple<int, object>(2, s)));
                if (IdWorkStatus != null) args.AddRange(IdWorkStatus.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(6, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(7, s)));

                DB_WORK_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_WORK_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetWorkHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetWorkHistory),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_TIME_CODE", StartTime),
                            CreateTableParam("@END_TIME_CODE", EndTime),
                            new DB.InParameter("@ASSEMBLY", Assembly),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_WORK_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetWorkHistoryFilter",
                    new DB.AnalyzeDataSet(GetWorkHistory),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_WORK_HISTORY", IdWorkHistory),
                        CreateTableParam("@ID_WORK", IdWork),
                        CreateTableParam("@ID_WORK_STATUS", IdWorkStatus),
                        CreateTableParam("@START_TIME_CODE", StartTime),
                        CreateTableParam("@END_TIME_CODE", EndTime),
                        CreateTableParam("@ID_DESCR", IdDescr),
                        CreateTableParam("@ID_MODULE", IdModule),
                        new DB.InParameter("@ASSEMBLY", Assembly),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetWorkHistoryDataFilter
        public DB_WORK_HISTORY_DATA[] GetWorkHistoryDataFilter(long[] IdWorkHistoryData, long[] IdWorkHistory, int[] ArgNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkHistoryDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdWorkHistoryData != null && IdWorkHistoryData.Length > MaxItemsPerSqlTableType) ||
                (IdWorkHistory != null && IdWorkHistory.Length > MaxItemsPerSqlTableType) ||
                (ArgNbr != null && ArgNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdWorkHistoryData != null) args.AddRange(IdWorkHistoryData.Select(s => new Tuple<int, object>(1, s)));
                if (IdWorkHistory != null) args.AddRange(IdWorkHistory.Select(s => new Tuple<int, object>(2, s)));
                if (ArgNbr != null) args.AddRange(ArgNbr.Select(s => new Tuple<int, object>(3, s)));

                DB_WORK_HISTORY_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_WORK_HISTORY_DATA[])DB.ExecuteProcedure(
                        "imrse_GetWorkHistoryDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetWorkHistoryData),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_WORK_HISTORY_DATA[])DB.ExecuteProcedure(
                    "imrse_GetWorkHistoryDataFilter",
                    new DB.AnalyzeDataSet(GetWorkHistoryData),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_WORK_HISTORY_DATA", IdWorkHistoryData),
                        CreateTableParam("@ID_WORK_HISTORY", IdWorkHistory),
                        CreateTableParam("@ARG_NBR", ArgNbr),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetWorkStatusFilter
        public DB_WORK_STATUS[] GetWorkStatusFilter(int[] IdWorkStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkStatusFilter"));

            return (DB_WORK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetWorkStatusFilter",
                new DB.AnalyzeDataSet(GetWorkStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_STATUS", IdWorkStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetActionTypeFilter
        public DB_WORK_TYPE[] GetWorkTypeFilter(int[] IdWorkType, string Name, string PluginName, long[] IdDescr,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkTypeFilter"));

            return (DB_WORK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWorkTypeFilter",
                new DB.AnalyzeDataSet(GetWorkType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_TYPE", IdWorkType),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@PLUGIN_NAME", PluginName),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion

        #region GetWorkOrderJciFilter

        public DB_WORK_ORDER_JCI[] GetWorkOrderJciFilter(int[] IdWorkOrderJci, string Wonum, TypeDateTimeCode Sentdatetime, string Wodesc, string Ivrcode, string Subcasdesc,
                            string Problemdesc, int[] Priority, string Typedesc, string Statusdesc, TypeDateTimeCode Reportdatetime,
                            TypeDateTimeCode Rfdatetime, string Ponumber, string Supplierid, string Suppliername, string Siteid,
                            string Sitename, string Sitephone, string Equipid, string Equipdesc, string Downtime,
                            string Lumpsum, string DisclaimerText, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkOrderJciFilter"));

            return (DB_WORK_ORDER_JCI[])DB.ExecuteProcedure(
                "imrse_GetWorkOrderJciFilter",
                new DB.AnalyzeDataSet(GetWorkOrderJci),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_ORDER_JCI", IdWorkOrderJci),
                    new DB.InParameter("@WONUM", Wonum),
                    CreateTableParam("@SENTDATETIME_CODE", Sentdatetime),
                    new DB.InParameter("@WODESC", Wodesc),
                    new DB.InParameter("@IVRCODE", Ivrcode),
                    new DB.InParameter("@SUBCASDESC", Subcasdesc),
                    new DB.InParameter("@PROBLEMDESC", Problemdesc),
                    CreateTableParam("@PRIORITY", Priority),
                    new DB.InParameter("@TYPEDESC", Typedesc),
                    new DB.InParameter("@STATUSDESC", Statusdesc),
                    CreateTableParam("@REPORTDATETIME_CODE", Reportdatetime),
                    CreateTableParam("@RFDATETIME_CODE", Rfdatetime),
                    new DB.InParameter("@PONUMBER", Ponumber),
                    new DB.InParameter("@SUPPLIERID", Supplierid),
                    new DB.InParameter("@SUPPLIERNAME", Suppliername),
                    new DB.InParameter("@SITEID", Siteid),
                    new DB.InParameter("@SITENAME", Sitename),
                    new DB.InParameter("@SITEPHONE", Sitephone),
                    new DB.InParameter("@EQUIPID", Equipid),
                    new DB.InParameter("@EQUIPDESC", Equipdesc),
                    new DB.InParameter("@DOWNTIME", Downtime),
                    new DB.InParameter("@LUMPSUM", Lumpsum),
                    new DB.InParameter("@DISCLAIMER_TEXT", DisclaimerText),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
    }
}