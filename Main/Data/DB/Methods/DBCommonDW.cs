﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Collections.Concurrent;

using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.Data.DB.Objects.DW;


namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDW : DBCommon
    {
        #region Table ALARM
        public DW.DB_ALARM[] GetAlarm()
        {
            return (DW.DB_ALARM[])DB.ExecuteProcedure(
                "imrse_GetAlarm",
                new DB.AnalyzeDataSet(GetAlarm),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM", new long[] {})
                    }
            );
        }
        public DW.DB_ALARM[] GetAlarm(long[] Ids)
        {
            return (DW.DB_ALARM[])DB.ExecuteProcedure(
                "imrse_GetAlarm",
                new DB.AnalyzeDataSet(GetAlarm),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM", Ids)
                    }
            );
        }

        public long SaveAlarm(DW.DB_ALARM ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM", ToBeSaved.ID_ALARM);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarm",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT)
                                        ,new DB.InParameter("@ID_ALARM_DEF", ToBeSaved.ID_ALARM_DEF)
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                        ,new DB.InParameter("@ID_ALARM_TYPE", ToBeSaved.ID_ALARM_TYPE)
                                        ,new DB.InParameter("@ID_DATA_TYPE_ALARM", ToBeSaved.ID_DATA_TYPE_ALARM)
                                        ,new DB.InParameter("@SYSTEM_ALARM_VALUE", SqlDbType.Variant, ToBeSaved.SYSTEM_ALARM_VALUE)
                                        ,new DB.InParameter("@SYSTEM_ALARM_VALUE", SqlDbType.Variant, ToBeSaved.SYSTEM_ALARM_VALUE)
                                                        ,new DB.InParameter("@ALARM_VALUE", SqlDbType.Variant, ToBeSaved.ALARM_VALUE)
                                                        ,new DB.InParameter("@ID_ALARM_GROUP", ToBeSaved.ID_ALARM_GROUP)
                                                        ,new DB.InParameter("@ID_ALARM_STATUS", ToBeSaved.ID_ALARM_STATUS)
                                                        ,new DB.InParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE)
                                                        ,new DB.InParameter("@TRANSMISSION_TYPE", ToBeSaved.TRANSMISSION_TYPE)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarm(DW.DB_ALARM toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarm",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM", toBeDeleted.ID_ALARM)
        }
            );
        }
        private object GetAlarm(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_ALARM()
            {
                ID_ALARM = GetValue<long>(row["ID_ALARM"]),
                ID_ALARM_EVENT = GetValue<long>(row["ID_ALARM_EVENT"]),
                ID_ALARM_DEF = GetNullableValue<int>(row["ID_ALARM_DEF"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_ALARM_TYPE = GetValue<int>(row["ID_ALARM_TYPE"]),
                ID_DATA_TYPE_ALARM = GetValue<long>(row["ID_DATA_TYPE_ALARM"]),
                SYSTEM_ALARM_VALUE = GetValue(row["SYSTEM_ALARM_VALUE"]),
                ALARM_VALUE = GetValue(row["ALARM_VALUE"]),
                ID_OPERATOR = GetValue<int>(row["ID_OPERATOR"]),
                ID_ALARM_STATUS = GetValue<int>(row["ID_ALARM_STATUS"]),
                TRANSMISSION_TYPE = GetValue<string>(row["TRANSMISSION_TYPE"]),
                ID_TRANSMISSION_TYPE = GetNullableValue<int>(row["ID_TRANSMISSION_TYPE"]),
                ID_ALARM_GROUP = GetNullableValue<int>(row["ID_ALARM_GROUP"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_ALARM[] list = new DW.DB_ALARM[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_ALARM insert = new DW.DB_ALARM();
                insert.ID_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM"]);
                insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.ID_ALARM_DEF = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DEF"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_ALARM_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TYPE"]);
                insert.ID_DATA_TYPE_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_ALARM"]);
                insert.SYSTEM_ALARM_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["SYSTEM_ALARM_VALUE"]);
                insert.ALARM_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["ALARM_VALUE"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_ALARM_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_STATUS"]);
                insert.TRANSMISSION_TYPE = GetValue<string>(QueryResult.Tables[0].Rows[i]["TRANSMISSION_TYPE"]);
                insert.ID_TRANSMISSION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                insert.ID_ALARM_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_GROUP"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }
        #endregion
        #region Table ALARM_EVENT

        public DW.DB_ALARM_EVENT[] GetAlarmEventDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_ALARM_EVENT[])DB.ExecuteProcedure(
                "imrse_GetAlarmEvent",
                new DB.AnalyzeDataSet(GetAlarmEventDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_EVENT", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DW.DB_ALARM_EVENT[] GetAlarmEventDW(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_ALARM_EVENT[])DB.ExecuteProcedure(
                "imrse_GetAlarmEvent",
                new DB.AnalyzeDataSet(GetAlarmEventDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_EVENT", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetAlarmEventDW(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_ALARM_EVENT()
            {
                ID_ALARM_EVENT = GetValue<long>(row["ID_ALARM_EVENT"]),
                ID_ALARM_DEF = GetValue<long>(row["ID_ALARM_DEF"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_ALARM_TYPE = GetValue<int>(row["ID_ALARM_TYPE"]),
                ID_DATA_TYPE_ALARM = GetValue<int>(row["ID_DATA_TYPE_ALARM"]),
                SYSTEM_ALARM_VALUE = GetValue(row["SYSTEM_ALARM_VALUE"]),
                ALARM_VALUE = GetValue(row["ALARM_VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                ID_ISSUE = GetNullableValue<long>(row["ID_ISSUE"]),
                IS_CONFIRMED = GetValue<bool>(row["IS_CONFIRMED"]),
                CONFIRMED_BY = GetNullableValue<int>(row["CONFIRMED_BY"]),
                CONFIRM_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["CONFIRM_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_ALARM_EVENT[] list = new DW.DB_ALARM_EVENT[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_ALARM_EVENT insert = new DW.DB_ALARM_EVENT();
                                            insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                                                        insert.ID_ALARM_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DEF"]);
                                                        insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                                                        insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                                                        insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                                                        insert.ID_ALARM_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TYPE"]);
                                                        insert.ID_DATA_TYPE_ALARM = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_ALARM"]);
                                                        insert.SYSTEM_ALARM_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["SYSTEM_ALARM_VALUE"]);
                                                        insert.ALARM_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["ALARM_VALUE"]);
                                                        insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                                                        insert.ID_ISSUE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
                                                        insert.IS_CONFIRMED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_CONFIRMED"]);
                                                        insert.CONFIRMED_BY = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["CONFIRMED_BY"]);
                                                        insert.CONFIRM_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["CONFIRM_TIME"]));
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveAlarmEventDW(DW.DB_ALARM_EVENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmEvent",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_ALARM_DEF", ToBeSaved.ID_ALARM_DEF)
                                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                                        ,new DB.InParameter("@ID_ALARM_TYPE", ToBeSaved.ID_ALARM_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE_ALARM", ToBeSaved.ID_DATA_TYPE_ALARM)
                                                        ,ParamObject("@SYSTEM_ALARM_VALUE", ToBeSaved.SYSTEM_ALARM_VALUE, 0, 0)
                                                    ,ParamObject("@ALARM_VALUE", ToBeSaved.ALARM_VALUE, 0, 0)
                                                    ,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
                                                    ,new DB.InParameter("@ID_ISSUE", ToBeSaved.ID_ISSUE)
                                                        ,new DB.InParameter("@IS_CONFIRMED", ToBeSaved.IS_CONFIRMED)
                                                        ,new DB.InParameter("@CONFIRMED_BY", ToBeSaved.CONFIRMED_BY)
                                                        ,new DB.InParameter("@CONFIRM_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.CONFIRM_TIME))
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_EVENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmEventDW(DW.DB_ALARM_EVENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmEvent",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_EVENT", toBeDeleted.ID_ALARM_EVENT)
                }
            );
        }

        #endregion
        #region Table ALARM_HISTORY
        public DW.DB_ALARM_HISTORY[] GetAlarmHistory()
        {
            return (DW.DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetAlarmHistory",
                new DB.AnalyzeDataSet(GetAlarmHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_HISTORY", new long[] {})
                    }
            );
        }
        public DW.DB_ALARM_HISTORY[] GetAlarmHistory(long[] Ids)
        {
            return (DW.DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetAlarmHistory",
                new DB.AnalyzeDataSet(GetAlarmHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_HISTORY", Ids)
                    }
            );
        }
        private object GetAlarmHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_ALARM_HISTORY()
            {
                ID_ALARM_HISTORY = GetValue<long>(row["ID_ALARM_HISTORY"]),
                ID_ALARM = GetValue<long>(row["ID_ALARM"]),
                ID_ALARM_STATUS = GetValue<int>(row["ID_ALARM_STATUS"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_ALARM_HISTORY[] list = new DW.DB_ALARM_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_ALARM_HISTORY insert = new DW.DB_ALARM_HISTORY();
                insert.ID_ALARM_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_HISTORY"]);
                insert.ID_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM"]);
                insert.ID_ALARM_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_STATUS"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }
        #endregion
        #region Table ALARM_MESSAGE
        public DW.DB_ALARM_MESSAGE[] GetAlarmMessage()
        {
            return (DW.DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetAlarmMessage",
                new DB.AnalyzeDataSet(GetAlarmMessage),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_EVENT", new long[] {})
                    }
            );
        }
        public DW.DB_ALARM_MESSAGE[] GetAlarmMessage(long[] Ids)
        {
            return (DW.DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetAlarmMessage",
                new DB.AnalyzeDataSet(GetAlarmMessage),
                new DB.Parameter[] {
            CreateTableParam("@ID_ALARM_EVENT", Ids)
                    }
            );
        }
        private object GetAlarmMessage(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_ALARM_MESSAGE()
            {
                ID_ALARM_EVENT = GetValue<long>(row["ID_ALARM_EVENT"]),
                ID_LANGUAGE = GetValue<int>(row["ID_LANGUAGE"]),
                ALARM_MESSAGE = GetValue<string>(row["ALARM_MESSAGE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_ALARM_MESSAGE[] list = new DW.DB_ALARM_MESSAGE[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_ALARM_MESSAGE insert = new DW.DB_ALARM_MESSAGE();
                insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.ALARM_MESSAGE = GetValue<string>(QueryResult.Tables[0].Rows[i]["ALARM_MESSAGE"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }
        #endregion

        #region Table AUDIT

        public DB_AUDIT[] GetAudit()
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] {
            CreateTableParam("@ID_AUDIT", new long[] {})
                    }
            );
        }

        public DB_AUDIT[] GetAudit(long[] Ids)
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] {
            CreateTableParam("@ID_AUDIT", Ids)
                    }
            );
        }



        private object GetAudit(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_AUDIT()
            {
                ID_AUDIT = GetValue<long>(row["ID_AUDIT"]),
                BATCH_ID = GetValue<long>(row["BATCH_ID"]),
                CHANGE_TYPE = GetValue<int>(row["CHANGE_TYPE"]),
                TABLE_NAME = GetValue<string>(row["TABLE_NAME"]),
                KEY1 = GetNullableValue<long>(row["KEY1"]),
                KEY2 = GetNullableValue<long>(row["KEY2"]),
                KEY3 = GetNullableValue<long>(row["KEY3"]),
                KEY4 = GetNullableValue<long>(row["KEY4"]),
                KEY5 = GetNullableValue<long>(row["KEY5"]),
                KEY6 = GetNullableValue<long>(row["KEY6"]),
                COLUMN_NAME = GetValue<string>(row["COLUMN_NAME"]),
                OLD_VALUE = GetValue(row["OLD_VALUE"]),
                NEW_VALUE = GetValue(row["NEW_VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                USER = GetValue<string>(row["USER"]),
                HOST = GetValue<string>(row["HOST"]),
                APPLICATION = GetValue<string>(row["APPLICATION"]),
                CONTEXTINFO = QueryResult.Tables[0].Columns.Contains("CONTEXTINFO") ? GetValue<string>(row["CONTEXTINFO"]) : null,
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_AUDIT[] list = new DB_AUDIT[count];
    for (int i = 0; i < count; i++)
    {
        DB_AUDIT insert = new DB_AUDIT();
									insert.ID_AUDIT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_AUDIT"]);
												insert.BATCH_ID = GetValue<long>(QueryResult.Tables[0].Rows[i]["BATCH_ID"]);
												insert.CHANGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_TYPE"]);
												insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
												insert.KEY1 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY1"]);
												insert.KEY2 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY2"]);
												insert.KEY3 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY3"]);
												insert.KEY4 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY4"]);
												insert.KEY5 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY5"]);
												insert.KEY6 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY6"]);
												insert.COLUMN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLUMN_NAME"]);
												insert.OLD_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["OLD_VALUE"]);
												insert.NEW_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["NEW_VALUE"]);
												insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
												insert.USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["USER"]);
												insert.HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["HOST"]);
												insert.APPLICATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["APPLICATION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveAudit(DB_AUDIT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_AUDIT", ToBeSaved.ID_AUDIT);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@BATCH_ID", ToBeSaved.BATCH_ID));
            parameters.Add(new DB.InParameter("@CHANGE_TYPE", ToBeSaved.CHANGE_TYPE));
            parameters.Add(new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME));
            parameters.Add(new DB.InParameter("@KEY1", ToBeSaved.KEY1));
            parameters.Add(new DB.InParameter("@KEY2", ToBeSaved.KEY2));
            parameters.Add(new DB.InParameter("@KEY3", ToBeSaved.KEY3));
            parameters.Add(new DB.InParameter("@KEY4", ToBeSaved.KEY4));
            parameters.Add(new DB.InParameter("@KEY5", ToBeSaved.KEY5));
            parameters.Add(new DB.InParameter("@KEY6", ToBeSaved.KEY6));
            parameters.Add(new DB.InParameter("@COLUMN_NAME", ToBeSaved.COLUMN_NAME));
            parameters.Add(ParamObject("@OLD_VALUE", ToBeSaved.OLD_VALUE, 0, 0));
            parameters.Add(ParamObject("@NEW_VALUE", ToBeSaved.NEW_VALUE, 0, 0));
            parameters.Add(new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME)));
            parameters.Add(new DB.InParameter("@USER", ToBeSaved.USER));
            parameters.Add(new DB.InParameter("@HOST", ToBeSaved.HOST));
            parameters.Add(new DB.InParameter("@APPLICATION", ToBeSaved.APPLICATION));
            if (IsParameterDefined("imrse_SaveAudit", "@CONTEXTINFO", this))
                parameters.Add(new DB.InParameter("@CONTEXTINFO", ToBeSaved.CONTEXTINFO));
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAudit",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_AUDIT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAudit(DB_AUDIT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAudit",
                new DB.Parameter[] {
            new DB.InParameter("@ID_AUDIT", toBeDeleted.ID_AUDIT)
        }
            );
        }

        #endregion

        #region Table CONSUMER_TRANSACTION

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransaction()
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] {
            CreateTableParam("@ID_CONSUMER_TRANSACTION", new int[] {})
                            ,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
                    }
            );
        }

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransaction(int[] Ids)
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] {
            CreateTableParam("@ID_CONSUMER_TRANSACTION", Ids)
                            ,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
                    }
            );
        }



        private object GetConsumerTransaction(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER_TRANSACTION[] list = new DB_CONSUMER_TRANSACTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_TRANSACTION insert = new DB_CONSUMER_TRANSACTION();
                insert.ID_CONSUMER_TRANSACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION"]);
                insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_TRANSACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSACTION_TYPE"]);
                insert.ID_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
                insert.TIMESTAMP = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIMESTAMP"]));
                insert.CHARGE_PREPAID = GetValue<Double>(QueryResult.Tables[0].Rows[i]["CHARGE_PREPAID"]);
                insert.CHARGE_EC = GetValue<Double>(QueryResult.Tables[0].Rows[i]["CHARGE_EC"]);
                insert.CHARGE_OWED = GetValue<Double>(QueryResult.Tables[0].Rows[i]["CHARGE_OWED"]);
                insert.BALANCE_PREPAID = GetValue<Double>(QueryResult.Tables[0].Rows[i]["BALANCE_PREPAID"]);
                insert.BALANCE_EC = GetValue<Double>(QueryResult.Tables[0].Rows[i]["BALANCE_EC"]);
                insert.BALANCE_OWED = GetValue<Double>(QueryResult.Tables[0].Rows[i]["BALANCE_OWED"]);
                insert.METER_INDEX = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["METER_INDEX"]);
                if (QueryResult.Tables[0].Columns.Contains("ID_CONSUMER_SETTLEMENT"))
                {
                    insert.ID_CONSUMER_SETTLEMENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT"]);
                }
                if (QueryResult.Tables[0].Columns.Contains("[ID_MODULE]"))
                {
                    insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                }
                if (QueryResult.Tables[0].Columns.Contains("[TRANSACTION_GUID]"))
                {
                    insert.TRANSACTION_GUID = GetValue<string>(QueryResult.Tables[0].Rows[i]["TRANSACTION_GUID"]);
                }
                list[i] = insert;
            }
            return list;
        }

        public void DeleteConsumerTransaction(DB_CONSUMER_TRANSACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTransaction",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TRANSACTION", toBeDeleted.ID_CONSUMER_TRANSACTION)
        }
            );
        }

        #endregion

        #region Table CONSUMER_TRANSACTION_DATA

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionData()
        {
            return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionData",
                new DB.AnalyzeDataSet(GetConsumerTransactionData),
                new DB.Parameter[] {
            CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", new long[] {})
                    }
            );
        }

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionData(long[] Ids)
        {
            return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionData",
                new DB.AnalyzeDataSet(GetConsumerTransactionData),
                new DB.Parameter[] {
            CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", Ids)
                    }
            );
        }



        private object GetConsumerTransactionData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER_TRANSACTION_DATA[] list = new DB_CONSUMER_TRANSACTION_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_TRANSACTION_DATA insert = new DB_CONSUMER_TRANSACTION_DATA();
                insert.ID_CONSUMER_TRANSACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION_DATA"]);
                insert.ID_CONSUMER_TRANSACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConsumerTransactionData(DB_CONSUMER_TRANSACTION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TRANSACTION_DATA", ToBeSaved.ID_CONSUMER_TRANSACTION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerTransactionData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_CONSUMER_TRANSACTION", ToBeSaved.ID_CONSUMER_TRANSACTION)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TRANSACTION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerTransactionData(DB_CONSUMER_TRANSACTION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTransactionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TRANSACTION_DATA", toBeDeleted.ID_CONSUMER_TRANSACTION_DATA)
        }
            );
        }

        #endregion

        #region Table DATA_ARCH

        public DW.DB_DATA_ARCH[] GetDataArch()
        {
            return (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_ARCH", new long[] {})
                    }
            );
        }

        public DW.DB_DATA_ARCH[] GetDataArch(long[] Ids)
        {
            return (DW.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_ARCH", Ids)
                    }
            );
        }

        private object GetDataArch(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_DATA_ARCH()
            {
                ID_DATA_ARCH = GetValue<long>(row["ID_DATA_ARCH"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                ID_DATA_SOURCE = GetNullableValue<long>(row["ID_DATA_SOURCE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                ID_ALARM_EVENT = GetNullableValue<long>(row["ID_ALARM_EVENT"]),
                STATUS = GetValue<int>(row["STATUS"]),
            }).ToArray();
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_DATA_ARCH[] list = new DW.DB_DATA_ARCH[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_DATA_ARCH insert = new DW.DB_DATA_ARCH();
                insert.ID_DATA_ARCH = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.ID_DATA_SOURCE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                insert.ID_ALARM_EVENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
        }

        public long SaveDataArch(DW.DB_DATA_ARCH ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_ARCH", ToBeSaved.ID_DATA_ARCH);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataArch",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                        ,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
                                                        ,new DB.InParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE)
                                                        ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                                        ,new DB.InParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT)
                                                        ,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_ARCH = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataArch(DW.DB_DATA_ARCH toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataArch",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_ARCH", toBeDeleted.ID_DATA_ARCH)
        }
            );
        }

        public DataCurrentValue[] GetLatestDataForLocations(long[] Locations, DATA_TYPE_UNIT[] TypesWithUnits, DateTime? From, long? StatusMaskAny, long? StatusMaskAll)
        {
            //if (Locations == null || Locations.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            if (TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataCurrentValue[0];
            }

            DB.InParameter paramLocations = CreateTableParam("@LOCATION_IDS", Locations);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataCurrentValue[] result = (DataCurrentValue[])DB.ExecuteProcedure(
                    "sp_GetLatestDataForLocations",
                    new DB.AnalyzeDataSet(GetDataCurrentValues),
                    new DB.Parameter[]
                {
                    paramLocations,
                    paramTypes,
                    new DB.InParameter ("@FROM", From),
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
                },
                false,
                this.LongTimeout
            );

            return result;

        }

        public DataCurrentValue[] GetLatestDataForDevices(long[] Devices, DATA_TYPE_UNIT[] TypesWithUnits, DateTime? From, long? StatusMaskAny, long? StatusMaskAll)
        {
            if (Devices == null || Devices.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataCurrentValue[0];
            }

            DB.InParameter paramLocations = CreateTableParam("@DEVICE_SN", Devices);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataCurrentValue[] result = (DataCurrentValue[])DB.ExecuteProcedure(
                    "sp_GetLatestDataForDevices",
                    new DB.AnalyzeDataSet(GetDataCurrentValues),
                    new DB.Parameter[]
                {
                    paramLocations,
                    paramTypes,
                    new DB.InParameter ("@FROM", From),
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
                },
                false,
                this.LongTimeout
            );
            return result;
        }

        public DataArchValue[] GetArchiveDataForLocations(long[] Locations, DATA_TYPE_UNIT[] TypesWithUnits, DateTime From, DateTime To, long? StatusMaskAny, long? StatusMaskAll)
        {
            if (Locations == null || Locations.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataArchValue[0];
            }

            DB.InParameter paramLocations = CreateTableParam("@LOCATION_IDS", Locations);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataArchValue[] result = (DataArchValue[])DB.ExecuteProcedure(
                    "sp_GetArchiveDataForLocations",
                    new DB.AnalyzeDataSet(GetDataArchValues),
                    new DB.Parameter[]
                {
                    paramLocations,
                    paramTypes,
                    new DB.InParameter ("@FROM", From),
                    new DB.InParameter ("@TO", To),
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
                },
                false,
                this.LongTimeout
            );

            return result;

        }

        public DataArchValue[] GetArchiveDataForDevices(long[] Devices, DATA_TYPE_UNIT[] TypesWithUnits, DateTime From, DateTime To, long? StatusMaskAny, long? StatusMaskAll)
        {
            if (Devices == null || Devices.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataArchValue[0];
            }

            DB.InParameter paramDevices = CreateTableParam("@DEVICE_SN", Devices);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataArchValue[] result = (DataArchValue[])DB.ExecuteProcedure(
                    "sp_GetArchiveDataForDevices",
                    new DB.AnalyzeDataSet(GetDataArchValues),
                    new DB.Parameter[]
                {
                    paramDevices,
                    paramTypes,
                    new DB.InParameter ("@FROM", From),
                    new DB.InParameter ("@TO", To),
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
                },
                false,
                this.LongTimeout
            );

            return result;
        }

        private object GetDataCurrentValues(DataSet QueryResult)
        {
            DataRowCollection rows = QueryResult.Tables[0].Rows;
            bool bIndexColumn = false;
            if (QueryResult.Tables[0].Columns.Contains("INDEX_NBR"))
                bIndexColumn = true;

            DataCurrentValue[] result = new DataCurrentValue[rows.Count];

            DataCurrentValue dataCurrentValue;
            long idData;
            ulong? serialNbr;
            long? idMeter;
            long? idLocation;
            long idDataType;
            object value;
            int index = 0;
            DateTime time;
            string unit;
            long? idDataSource;
            int? idDataSourceType;
            long status;

            for (int i = 0; i < rows.Count; i++)
            {
                idData = Convert.ToInt64(rows[i]["ID_DATA"]);
                GetPossiblyNullValue(rows[i], "SERIAL_NBR", out serialNbr);
                GetPossiblyNullValue(rows[i], "ID_METER", out idMeter);
                GetPossiblyNullValue(rows[i], "ID_LOCATION", out idLocation);
                idDataType = Convert.ToInt64(rows[i]["ID_DATA_TYPE"]);
                if (bIndexColumn)
                    index = Convert.ToInt32(rows[i]["INDEX_NBR"]);
                GetPossiblyNullValue(rows[i], "VALUE", out value);
                time = Convert.ToDateTime(rows[i]["TIME"]);
                unit = rows[i]["SYMBOL"].ToString();
                status = Convert.ToInt64(rows[i]["STATUS"]);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE", out idDataSource);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE_TYPE", out idDataSourceType);

                dataCurrentValue = new DataCurrentValue(idData, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, index, value, time, unit, status, idDataSource, idDataSourceType);

                result[i] = dataCurrentValue;
            }

            return result;
        }

        private object GetDataArchValues(DataSet QueryResult)
        {
            DataRowCollection rows = QueryResult.Tables[0].Rows;
            bool bIndexColumn = false;
            if (QueryResult.Tables[0].Columns.Contains("INDEX_NBR"))
                bIndexColumn = true;

            DataArchValue[] result = new DataArchValue[rows.Count];

            DataArchValue dataArchValue;
            long idDataArch;
            ulong? serialNbr;
            long? idMeter;
            long? idLocation;
            long idDataType;
            object value;
            int index = 0;
            DateTime time;
            string unit;
            long? idDataSource;
            int? idDataSourceType;
            long? idAlarmEvent;
            long status;

            for (int i = 0; i < rows.Count; i++)
            {
                idDataArch = Convert.ToInt64(rows[i]["ID_DATA_ARCH"]);
                GetPossiblyNullValue(rows[i], "SERIAL_NBR", out serialNbr);
                GetPossiblyNullValue(rows[i], "ID_METER", out idMeter);
                GetPossiblyNullValue(rows[i], "ID_LOCATION", out idLocation);
                idDataType = Convert.ToInt64(rows[i]["ID_DATA_TYPE"]);
                if (bIndexColumn)
                    index = Convert.ToInt32(rows[i]["INDEX_NBR"]);
                GetPossiblyNullValue(rows[i], "VALUE", out value);
                time = Convert.ToDateTime(rows[i]["TIME"]);
                unit = rows[i]["SYMBOL"].ToString();
                status = Convert.ToInt64(rows[i]["STATUS"]);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE", out idDataSource);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE_TYPE", out idDataSourceType);
                GetPossiblyNullValue(rows[i], "ID_ALARM_EVENT", out idAlarmEvent);
                dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, index, value, time, unit, status, idDataSource, idDataSourceType, idAlarmEvent);

                //if (bIndexColumn)
                //    dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, index, value, time, unit, status, idDataSource, idDataSourceType, idAlarmEvent);
                //else
                //    dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, value, time, unit, status, idDataSource, idDataSourceType, idAlarmEvent);

                result[i] = dataArchValue;
            }

            return result;
        }

        #endregion

        #region Table DATA_SOURCE

        public DB_DATA_SOURCE[] GetDataSource()
        {
            return (DB_DATA_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetDataSource",
                new DB.AnalyzeDataSet(GetDataSource),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE", new long[] {})
                    }
            );
        }

        public DB_DATA_SOURCE[] GetDataSource(long[] Ids)
        {
            return (DB_DATA_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetDataSource",
                new DB.AnalyzeDataSet(GetDataSource),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE", Ids)
                    }
            );
        }



        private object GetDataSource(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_SOURCE[] list = new DB_DATA_SOURCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_SOURCE insert = new DB_DATA_SOURCE();
                insert.ID_DATA_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_SOURCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataSource(DB_DATA_SOURCE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataSource",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_SOURCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataSource(DB_DATA_SOURCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataSource",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_SOURCE", toBeDeleted.ID_DATA_SOURCE)
        }
            );
        }

        #endregion

        #region Table DATA_SOURCE_DATA

        public DB_DATA_SOURCE_DATA[] GetDataSourceData()
        {
            return (DB_DATA_SOURCE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDataSourceData",
                new DB.AnalyzeDataSet(GetDataSourceData),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE", new long[] {})
                    }
            );
        }

        public DB_DATA_SOURCE_DATA[] GetDataSourceData(long[] Ids)
        {
            return (DB_DATA_SOURCE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDataSourceData",
                new DB.AnalyzeDataSet(GetDataSourceData),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE", Ids)
                    }
            );
        }



        private object GetDataSourceData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_SOURCE_DATA[] list = new DB_DATA_SOURCE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_SOURCE_DATA insert = new DB_DATA_SOURCE_DATA();
                insert.ID_DATA_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table DATA_SOURCE_TYPE

        public DB_DATA_SOURCE_TYPE[] GetDataSourceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_SOURCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataSourceType",
                new DB.AnalyzeDataSet(GetDataSourceType),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE_TYPE", new int[] {})
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DATA_SOURCE_TYPE[] GetDataSourceType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_SOURCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataSourceType",
                new DB.AnalyzeDataSet(GetDataSourceType),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_SOURCE_TYPE", Ids)
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDataSourceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_SOURCE_TYPE[] list = new DB_DATA_SOURCE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_SOURCE_TYPE insert = new DB_DATA_SOURCE_TYPE();
                insert.ID_DATA_SOURCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataSourceType(DB_DATA_SOURCE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataSourceType",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_SOURCE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataSourceType(DB_DATA_SOURCE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataSourceType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_SOURCE_TYPE", toBeDeleted.ID_DATA_SOURCE_TYPE)
        }
            );
        }

        #endregion

        #region Table DATA_TEMPORAL

        public DB_DATA_TEMPORAL[] GetDataTemporal()
        {
            return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                "imrse_GetDataTemporal",
                new DB.AnalyzeDataSet(GetDataTemporal),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_TEMPORAL", new long[] {})
                    }
            );
        }

        public DB_DATA_TEMPORAL[] GetDataTemporal(long[] Ids)
        {
            return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                "imrse_GetDataTemporal",
                new DB.AnalyzeDataSet(GetDataTemporal),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_TEMPORAL", Ids)
                    }
            );
        }

        public DB_DATA_TEMPORAL[] GetGLTCDataForAllMeters()
        {
            return (DB_DATA_TEMPORAL[])DB.ExecuteProcedure(
                "awsr_GetGLTCDataForAllMeters",
                new DB.AnalyzeDataSet(GetDataTemporal),
                1800
            );
        }

        private object GetDataTemporal(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_TEMPORAL()
            {
                ID_DATA_TEMPORAL = GetValue<long>(row["ID_DATA_TEMPORAL"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                VALUE = GetValue(row["VALUE"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                ID_DATA_SOURCE = GetNullableValue<long>(row["ID_DATA_SOURCE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                ID_ALARM_EVENT = GetNullableValue<long>(row["ID_ALARM_EVENT"]),
                STATUS = GetValue<int>(row["STATUS"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                ID_AGGREGATION_TYPE = GetNullableValue<int>(row["ID_AGGREGATION_TYPE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TEMPORAL[] list = new DB_DATA_TEMPORAL[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TEMPORAL insert = new DB_DATA_TEMPORAL();
                insert.ID_DATA_TEMPORAL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TEMPORAL"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.ID_AGGREGATION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                insert.ID_DATA_SOURCE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                insert.ID_ALARM_EVENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveDataTemporal(DB_DATA_TEMPORAL ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TEMPORAL", ToBeSaved.ID_DATA_TEMPORAL);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataTemporal",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                        ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE)
                                                        ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                                        ,new DB.InParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT)
                                                        ,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TEMPORAL = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataTemporal(DB_DATA_TEMPORAL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTemporal",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TEMPORAL", toBeDeleted.ID_DATA_TEMPORAL)
        }
            );
        }

        #endregion

        #region Table DELIVERY_ADVICE

        public DB_DELIVERY_ADVICE[] GetDeliveryAdvice()
        {
            return (DB_DELIVERY_ADVICE[])DB.ExecuteProcedure(
                "imrse_GetDeliveryAdvice",
                new DB.AnalyzeDataSet(GetDeliveryAdvice),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ADVICE", new int[] {})
                    }
            );
        }

        public DB_DELIVERY_ADVICE[] GetDeliveryAdvice(int[] Ids)
        {
            return (DB_DELIVERY_ADVICE[])DB.ExecuteProcedure(
                "imrse_GetDeliveryAdvice",
                new DB.AnalyzeDataSet(GetDeliveryAdvice),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ADVICE", Ids)
                    }
            );
        }



        private object GetDeliveryAdvice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY_ADVICE[] list = new DB_DELIVERY_ADVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY_ADVICE insert = new DB_DELIVERY_ADVICE();
                insert.ID_DELIVERY_ADVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_ADVICE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["PRIORITY"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeliveryAdvice(DB_DELIVERY_ADVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY_ADVICE", ToBeSaved.ID_DELIVERY_ADVICE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeliveryAdvice",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                                        ,new DB.InParameter("@PRIORITY", ToBeSaved.PRIORITY)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY_ADVICE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeliveryAdvice(DB_DELIVERY_ADVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeliveryAdvice",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY_ADVICE", toBeDeleted.ID_DELIVERY_ADVICE)
        }
            );
        }

        #endregion

        #region Table DELIVERY_BATCH

        public DB_DELIVERY_BATCH[] GetDeliveryBatch()
        {
            return (DB_DELIVERY_BATCH[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatch",
                new DB.AnalyzeDataSet(GetDeliveryBatch),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_BATCH", new int[] {})
                            ,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
                    }
            );
        }

        public DB_DELIVERY_BATCH[] GetDeliveryBatch(int[] Ids)
        {
            return (DB_DELIVERY_BATCH[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatch",
                new DB.AnalyzeDataSet(GetDeliveryBatch),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_BATCH", Ids)
                            ,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
                    }
            );
        }



        private object GetDeliveryBatch(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY_BATCH[] list = new DB_DELIVERY_BATCH[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY_BATCH insert = new DB_DELIVERY_BATCH();
                insert.ID_DELIVERY_BATCH = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_BATCH"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DELIVERY_BATCH_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_BATCH_STATUS"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.COMMIT_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["COMMIT_DATE"]));
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.SUCCESS_TOTAL = GetValue<int>(QueryResult.Tables[0].Rows[i]["SUCCESS_TOTAL"]);
                insert.FAILED_TOTAL = GetValue<int>(QueryResult.Tables[0].Rows[i]["FAILED_TOTAL"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeliveryBatch(DB_DELIVERY_BATCH ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY_BATCH", ToBeSaved.ID_DELIVERY_BATCH);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeliveryBatch",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
                                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DELIVERY_BATCH_STATUS", ToBeSaved.ID_DELIVERY_BATCH_STATUS)
                                                        ,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
                                                        ,new DB.InParameter("@COMMIT_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.COMMIT_DATE))
                                                        ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
                                                        ,new DB.InParameter("@SUCCESS_TOTAL", ToBeSaved.SUCCESS_TOTAL)
                                                        ,new DB.InParameter("@FAILED_TOTAL", ToBeSaved.FAILED_TOTAL)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY_BATCH = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeliveryBatch(DB_DELIVERY_BATCH toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeliveryBatch",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY_BATCH", toBeDeleted.ID_DELIVERY_BATCH)
        }
            );
        }

        #endregion

        #region Table DELIVERY_BATCH_STATUS

        public DB_DELIVERY_BATCH_STATUS[] GetDeliveryBatchStatus()
        {
            return (DB_DELIVERY_BATCH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatchStatus",
                new DB.AnalyzeDataSet(GetDeliveryBatchStatus),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_BATCH_STATUS", new int[] {})
                    }
            );
        }

        public DB_DELIVERY_BATCH_STATUS[] GetDeliveryBatchStatus(int[] Ids)
        {
            return (DB_DELIVERY_BATCH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatchStatus",
                new DB.AnalyzeDataSet(GetDeliveryBatchStatus),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_BATCH_STATUS", Ids)
                    }
            );
        }



        private object GetDeliveryBatchStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY_BATCH_STATUS[] list = new DB_DELIVERY_BATCH_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY_BATCH_STATUS insert = new DB_DELIVERY_BATCH_STATUS();
                insert.ID_DELIVERY_BATCH_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_BATCH_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeliveryBatchStatus(DB_DELIVERY_BATCH_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY_BATCH_STATUS", ToBeSaved.ID_DELIVERY_BATCH_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeliveryBatchStatus",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY_BATCH_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeliveryBatchStatus(DB_DELIVERY_BATCH_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeliveryBatchStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY_BATCH_STATUS", toBeDeleted.ID_DELIVERY_BATCH_STATUS)
        }
            );
        }

        #endregion

        #region Table DELIVERY_ORDER

        public DB_DELIVERY_ORDER[] GetDeliveryOrder()
        {
            return (DB_DELIVERY_ORDER[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrder",
                new DB.AnalyzeDataSet(GetDeliveryOrder),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ORDER", new int[] {})
                    }
            );
        }

        public DB_DELIVERY_ORDER[] GetDeliveryOrder(int[] Ids)
        {
            return (DB_DELIVERY_ORDER[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrder",
                new DB.AnalyzeDataSet(GetDeliveryOrder),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ORDER", Ids)
                    }
            );
        }



        private object GetDeliveryOrder(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY_ORDER[] list = new DB_DELIVERY_ORDER[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY_ORDER insert = new DB_DELIVERY_ORDER();
                insert.ID_DELIVERY_ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_ORDER"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.ID_DELIVERY_BATCH = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_BATCH"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_DELIVERY_ADVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_ADVICE"]);
                insert.DELIVERY_VOLUME = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["DELIVERY_VOLUME"]);
                insert.IS_SUCCESFULL = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_SUCCESFULL"]);
                insert.ORDER_NO = GetValue<string>(QueryResult.Tables[0].Rows[i]["ORDER_NO"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeliveryOrder(DB_DELIVERY_ORDER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY_ORDER", ToBeSaved.ID_DELIVERY_ORDER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeliveryOrder",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
                                                        ,new DB.InParameter("@ID_DELIVERY_BATCH", ToBeSaved.ID_DELIVERY_BATCH)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
                                                        ,new DB.InParameter("@ID_DELIVERY_ADVICE", ToBeSaved.ID_DELIVERY_ADVICE)
                                                        ,new DB.InParameter("@DELIVERY_VOLUME", ToBeSaved.DELIVERY_VOLUME)
                                                        ,new DB.InParameter("@IS_SUCCESFULL", ToBeSaved.IS_SUCCESFULL)
                                                        ,new DB.InParameter("@ORDER_NO", ToBeSaved.ORDER_NO)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY_ORDER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeliveryOrder(DB_DELIVERY_ORDER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeliveryOrder",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY_ORDER", toBeDeleted.ID_DELIVERY_ORDER)
        }
            );
        }

        #endregion

        #region Table DELIVERY_ORDER_HISTORY

        public DB_DELIVERY_ORDER_HISTORY[] GetDeliveryOrderHistory()
        {
            return (DB_DELIVERY_ORDER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrderHistory",
                new DB.AnalyzeDataSet(GetDeliveryOrderHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ORDER_HISTORY", new long[] {})
                    }
            );
        }

        public DB_DELIVERY_ORDER_HISTORY[] GetDeliveryOrderHistory(long[] Ids)
        {
            return (DB_DELIVERY_ORDER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrderHistory",
                new DB.AnalyzeDataSet(GetDeliveryOrderHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_DELIVERY_ORDER_HISTORY", Ids)
                    }
            );
        }



        private object GetDeliveryOrderHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY_ORDER_HISTORY[] list = new DB_DELIVERY_ORDER_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY_ORDER_HISTORY insert = new DB_DELIVERY_ORDER_HISTORY();
                insert.ID_DELIVERY_ORDER_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_ORDER_HISTORY"]);
                insert.ID_DELIVERY_ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY_ORDER"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.IS_SUCCESFULL = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_SUCCESFULL"]);
                insert.COMMIT_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["COMMIT_DATE"]));
                insert.COMMIT_RESULT = GetValue<string>(QueryResult.Tables[0].Rows[i]["COMMIT_RESULT"]);
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeliveryOrderHistory(DB_DELIVERY_ORDER_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY_ORDER_HISTORY", ToBeSaved.ID_DELIVERY_ORDER_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeliveryOrderHistory",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_DELIVERY_ORDER", ToBeSaved.ID_DELIVERY_ORDER)
                                                        ,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
                                                        ,new DB.InParameter("@IS_SUCCESFULL", ToBeSaved.IS_SUCCESFULL)
                                                        ,new DB.InParameter("@COMMIT_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.COMMIT_DATE))
                                                        ,new DB.InParameter("@COMMIT_RESULT", ToBeSaved.COMMIT_RESULT)
                                                        ,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY_ORDER_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeliveryOrderHistory(DB_DELIVERY_ORDER_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeliveryOrderHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY_ORDER_HISTORY", toBeDeleted.ID_DELIVERY_ORDER_HISTORY)
        }
            );
        }

        #endregion

        #region Table DEVICE_CONNECTION

        public DB_DEVICE_CONNECTION[] GetDeviceConnection(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_CONNECTION[])DB.ExecuteProcedure(
                "imrse_GetDeviceConnection",
                new DB.AnalyzeDataSet(GetDeviceConnection),
                new DB.Parameter[] {
            CreateTableParam("@ID_DEVICE_CONNECTION", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_CONNECTION[] GetDeviceConnection(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_CONNECTION[])DB.ExecuteProcedure(
                "imrse_GetDeviceConnection",
                new DB.AnalyzeDataSet(GetDeviceConnection),
                new DB.Parameter[] {
            CreateTableParam("@ID_DEVICE_CONNECTION", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDeviceConnection(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DEVICE_CONNECTION()
            {
                ID_DEVICE_CONNECTION = GetValue<long>(row["ID_DEVICE_CONNECTION"]),
                SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                SERIAL_NBR_PARENT = GetValue<long>(row["SERIAL_NBR_PARENT"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_CONNECTION[] list = new DB_DEVICE_CONNECTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_CONNECTION insert = new DB_DEVICE_CONNECTION();
                                            insert.ID_DEVICE_CONNECTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_CONNECTION"]);
                                                        insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                                                        insert.SERIAL_NBR_PARENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PARENT"]);
                                                        insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveDeviceConnection(DB_DEVICE_CONNECTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_CONNECTION", ToBeSaved.ID_DEVICE_CONNECTION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceConnection",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@SERIAL_NBR_PARENT", ToBeSaved.SERIAL_NBR_PARENT)
                                                        ,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_CONNECTION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceConnection(DB_DEVICE_CONNECTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceConnection",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_CONNECTION", toBeDeleted.ID_DEVICE_CONNECTION)
                }
            );
        }

        #endregion

        #region Table DEVICE_SCHEDULE

        public DB_DEVICE_SCHEDULE[] GetDeviceSchedule()
        {
            return (DB_DEVICE_SCHEDULE[])DB.ExecuteProcedure(
                "imrse_GetDeviceSchedule",
                new DB.AnalyzeDataSet(GetDeviceSchedule),
                new DB.Parameter[] {
            CreateTableParam("@ID_DEVICE_SCHEDULE", new int[] {})
                    }
            );
        }

        public DB_DEVICE_SCHEDULE[] GetDeviceSchedule(int[] Ids)
        {
            return (DB_DEVICE_SCHEDULE[])DB.ExecuteProcedure(
                "imrse_GetDeviceSchedule",
                new DB.AnalyzeDataSet(GetDeviceSchedule),
                new DB.Parameter[] {
            CreateTableParam("@ID_DEVICE_SCHEDULE", Ids)
                    }
            );
        }



        private object GetDeviceSchedule(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DEVICE_SCHEDULE()
            {
                ID_DEVICE_SCHEDULE = GetValue<int>(row["ID_DEVICE_SCHEDULE"]),
                SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                DAY_OF_MONTH = GetValue<int>(row["DAY_OF_MONTH"]),
                DAY_OF_WEEK = GetValue<int>(row["DAY_OF_WEEK"]),
                HOUR_OF_DAY = GetValue<int>(row["HOUR_OF_DAY"]),
                MINUTE_OF_HOUR = GetValue<int>(row["MINUTE_OF_HOUR"]),
                COMMAND_CODE = GetValue<int>(row["COMMAND_CODE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_DEVICE_SCHEDULE[] list = new DB_DEVICE_SCHEDULE[count];
    for (int i = 0; i < count; i++)
    {
        DB_DEVICE_SCHEDULE insert = new DB_DEVICE_SCHEDULE();
									insert.ID_DEVICE_SCHEDULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_SCHEDULE"]);
												insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
												insert.DAY_OF_MONTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["DAY_OF_MONTH"]);
												insert.DAY_OF_WEEK = GetValue<int>(QueryResult.Tables[0].Rows[i]["DAY_OF_WEEK"]);
												insert.HOUR_OF_DAY = GetValue<int>(QueryResult.Tables[0].Rows[i]["HOUR_OF_DAY"]);
												insert.MINUTE_OF_HOUR = GetValue<int>(QueryResult.Tables[0].Rows[i]["MINUTE_OF_HOUR"]);
												insert.COMMAND_CODE = GetValue<int>(QueryResult.Tables[0].Rows[i]["COMMAND_CODE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveDeviceSchedule(DB_DEVICE_SCHEDULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_SCHEDULE", ToBeSaved.ID_DEVICE_SCHEDULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceSchedule",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@DAY_OF_MONTH", ToBeSaved.DAY_OF_MONTH)
                                                        ,new DB.InParameter("@DAY_OF_WEEK", ToBeSaved.DAY_OF_WEEK)
                                                        ,new DB.InParameter("@HOUR_OF_DAY", ToBeSaved.HOUR_OF_DAY)
                                                        ,new DB.InParameter("@MINUTE_OF_HOUR", ToBeSaved.MINUTE_OF_HOUR)
                                                        ,new DB.InParameter("@COMMAND_CODE", ToBeSaved.COMMAND_CODE)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_SCHEDULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceSchedule(DB_DEVICE_SCHEDULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceSchedule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_SCHEDULE", toBeDeleted.ID_DEVICE_SCHEDULE)
        }
            );
        }

        #endregion

        #region Table DISTRIBUTOR_PERFORMANCE

        public DB_DISTRIBUTOR_PERFORMANCE[] GetDistributorPerformance()
        {
            return (DB_DISTRIBUTOR_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetDistributorPerformance",
                new DB.AnalyzeDataSet(GetDistributorPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_DISTRIBUTOR_PERFORMANCE", new long[] {})
                            ,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
                    }
            );
        }

        public DB_DISTRIBUTOR_PERFORMANCE[] GetDistributorPerformance(long[] Ids)
        {
            return (DB_DISTRIBUTOR_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetDistributorPerformance",
                new DB.AnalyzeDataSet(GetDistributorPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_DISTRIBUTOR_PERFORMANCE", Ids)
                            ,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
                    }
            );
        }



        private object GetDistributorPerformance(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DISTRIBUTOR_PERFORMANCE[] list = new DB_DISTRIBUTOR_PERFORMANCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DISTRIBUTOR_PERFORMANCE insert = new DB_DISTRIBUTOR_PERFORMANCE();
                insert.ID_DISTRIBUTOR_PERFORMANCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_PERFORMANCE"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_AGGREGATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDistributorPerformance(DB_DISTRIBUTOR_PERFORMANCE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR_PERFORMANCE", ToBeSaved.ID_DISTRIBUTOR_PERFORMANCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDistributorPerformance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                    ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR_PERFORMANCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDistributorPerformance(DB_DISTRIBUTOR_PERFORMANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDistributorPerformance",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DISTRIBUTOR_PERFORMANCE", toBeDeleted.ID_DISTRIBUTOR_PERFORMANCE)
        }
            );
        }

        #endregion

        #region Table ETL

        //public DW.DB_ETL[] GetEtlDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        public DB_ETL[] GetEtlDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            //return (DW.DB_ETL[])DB.ExecuteProcedure(
            return (DB_ETL[])DB.ExecuteProcedure(
                "imrse_GetEtl",
                new DB.AnalyzeDataSet(GetEtlDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_PARAM", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ETL[] GetEtlDW(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //public DW.DB_ETL[] GetEtlDW(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            //return (DW.DB_ETL[])DB.ExecuteProcedure(
            return (DB_ETL[])DB.ExecuteProcedure(
                "imrse_GetEtl",
                new DB.AnalyzeDataSet(GetEtlDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_PARAM", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetEtlDW(DataSet QueryResult)
        {
            //return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_ETL()
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ETL()
            {
                ID_PARAM = GetValue<int>(row["ID_PARAM"]),
                CODE = GetValue<string>(row["CODE"]),
                DESCR = GetValue<string>(row["DESCR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ETL[] list = new DB_ETL[count];
            for (int i = 0; i < count; i++)
            {
                DB_ETL insert = new DB_ETL();
                                            insert.ID_PARAM = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARAM"]);
                                                        insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                                                        insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        //public int SaveEtlDW(DW.DB_ETL ToBeSaved)
        public int SaveEtlDW(DB_ETL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PARAM", ToBeSaved.ID_PARAM);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveEtl",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@CODE", ToBeSaved.CODE)
                                                        ,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, 0, 0)
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PARAM = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        //public void DeleteEtlDW(DW.DB_ETL toBeDeleted)
        public void DeleteEtlDW(DB_ETL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelEtl",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PARAM", toBeDeleted.ID_PARAM)
                }
            );
        }

        #endregion

        #region Table ETL_PERFORMANCE

        public DB_ETL_PERFORMANCE[] GetEtlPerformance(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ETL_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetEtlPerformance",
                new DB.AnalyzeDataSet(GetEtlPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_ETL_PERFORMANCE", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ETL_PERFORMANCE[] GetEtlPerformance(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ETL_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetEtlPerformance",
                new DB.AnalyzeDataSet(GetEtlPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_ETL_PERFORMANCE", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetEtlPerformance(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ETL_PERFORMANCE()
            {
                ID_ETL_PERFORMANCE = GetValue<long>(row["ID_ETL_PERFORMANCE"]),
                ID_ETL = GetValue<int>(row["ID_ETL"]),
                ID_AGGREGATION_TYPE = GetValue<int>(row["ID_AGGREGATION_TYPE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["END_TIME"])),
                VALUE = GetValue(row["VALUE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
                CODE = GetValue<string>(row["CODE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ETL_PERFORMANCE[] list = new DB_ETL_PERFORMANCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ETL_PERFORMANCE insert = new DB_ETL_PERFORMANCE();
                                            insert.ID_ETL_PERFORMANCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ETL_PERFORMANCE"]);
                                                        insert.ID_ETL = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ETL"]);
                                                        insert.ID_AGGREGATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                                                        insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                                                        insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                                        insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                                                        insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveEtlPerformance(DB_ETL_PERFORMANCE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ETL_PERFORMANCE", ToBeSaved.ID_ETL_PERFORMANCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveEtlPerformance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_ETL", ToBeSaved.ID_ETL)
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                    ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                    ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                                        ,new DB.InParameter("@CODE", ToBeSaved.CODE)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ETL_PERFORMANCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteEtlPerformance(DB_ETL_PERFORMANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelEtlPerformance",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ETL_PERFORMANCE", toBeDeleted.ID_ETL_PERFORMANCE)
                }
            );
        }

        #endregion

        #region Table IMR_SERVER_PERFORMANCE

        public DB_IMR_SERVER_PERFORMANCE[] GetImrServerPerformance()
        {
            return (DB_IMR_SERVER_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetImrServerPerformance",
                new DB.AnalyzeDataSet(GetImrServerPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_IMR_SERVER_PERFORMANCE", new long[] {})
                    }
            );
        }

        public DB_IMR_SERVER_PERFORMANCE[] GetImrServerPerformance(long[] Ids)
        {
            return (DB_IMR_SERVER_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetImrServerPerformance",
                new DB.AnalyzeDataSet(GetImrServerPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_IMR_SERVER_PERFORMANCE", Ids)
                    }
            );
        }



        private object GetImrServerPerformance(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_IMR_SERVER_PERFORMANCE[] list = new DB_IMR_SERVER_PERFORMANCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_IMR_SERVER_PERFORMANCE insert = new DB_IMR_SERVER_PERFORMANCE();
                insert.ID_IMR_SERVER_PERFORMANCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER_PERFORMANCE"]);
                insert.ID_IMR_SERVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER"]);
                insert.ID_AGGREGATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveImrServerPerformance(DB_IMR_SERVER_PERFORMANCE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_IMR_SERVER_PERFORMANCE", ToBeSaved.ID_IMR_SERVER_PERFORMANCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveImrServerPerformance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_IMR_SERVER", ToBeSaved.ID_IMR_SERVER)
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                    ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_IMR_SERVER_PERFORMANCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteImrServerPerformance(DB_IMR_SERVER_PERFORMANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelImrServerPerformance",
                new DB.Parameter[] {
            new DB.InParameter("@ID_IMR_SERVER_PERFORMANCE", toBeDeleted.ID_IMR_SERVER_PERFORMANCE)
        }
            );
        }

        #endregion

        #region Table LOCATION_KPI

        public DB_LOCATION_KPI[] GetLocationKpi()
        {
            return (DB_LOCATION_KPI[])DB.ExecuteProcedure(
                "imrse_GetLocationKpi",
                new DB.AnalyzeDataSet(GetLocationKpi),
                new DB.Parameter[] {
            CreateTableParam("@ID_LOCATION_KPI", this.DistributorFilter)
                    }
            );
        }

        public DB_LOCATION_KPI[] GetLocationKpi(long[] Ids)
        {
            return (DB_LOCATION_KPI[])DB.ExecuteProcedure(
                "imrse_GetLocationKpi",
                new DB.AnalyzeDataSet(GetLocationKpi),
                new DB.Parameter[] {
            CreateTableParam("@ID_LOCATION_KPI", Ids)
                    }
            );
        }



        private object GetLocationKpi(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_KPI()
            {
                ID_LOCATION_KPI = GetValue<long>(row["ID_LOCATION_KPI"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["DATE"])),
                KPI = GetValue<Double>(row["KPI"]),
                KPI_RELEVANT = GetValue<int>(row["KPI_RELEVANT"]),
                KPI_CONFORMING = GetValue<int>(row["KPI_CONFORMING"]),
                IS_MAINTENANCE_ONLY = GetValue<bool>(row["IS_MAINTENANCE_ONLY"]),
                SRT_CONFORMING = GetNullableValue<int>(row["SRT_CONFORMING"]),
                KPI_NONCONFORMING = GetNullableValue<int>(row["KPI_NONCONFORMING"]),
                KPI_IRRELEVANT = GetNullableValue<int>(row["KPI_IRRELEVANT"]),
                NEW = GetNullableValue<int>(row["NEW"]),
                PENDING = GetNullableValue<int>(row["PENDING"]),
                OPERATIONAL = GetNullableValue<int>(row["OPERATIONAL"]),
                SUSPENDED = GetNullableValue<int>(row["SUSPENDED"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
             int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_KPI[] list = new DB_LOCATION_KPI[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_KPI insert = new DB_LOCATION_KPI();
                insert.ID_LOCATION_KPI = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_KPI"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]));
                insert.KPI = GetValue<double>(QueryResult.Tables[0].Rows[i]["KPI"]);
                insert.NEW = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["NEW"]);
                insert.OPERATIONAL = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["OPERATIONAL"]);
                insert.SUSPENDED = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SUSPENDED"]);
                insert.PENDING = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["PENDING"]);
                insert.KPI_RELEVANT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["KPI_RELEVANT"]);
                insert.KPI_IRRELEVANT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["KPI_IRRELEVANT"]);
                insert.KPI_CONFORMING = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["KPI_CONFORMING"]);
                insert.KPI_NONCONFORMING = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["KPI_NONCONFORMING"]);
                insert.SRT_CONFORMING = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SRT_CONFORMING"]);
                insert.IS_MAINTENANCE_ONLY = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_MAINTENANCE_ONLY"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveLocationKpi(DB_LOCATION_KPI ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_KPI", ToBeSaved.ID_LOCATION_KPI);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationKpi",
                new DB.Parameter[] {
            InsertId
                ,new DB.InParameter("@ID_LOCATION_KPI", ToBeSaved.ID_LOCATION_KPI)
                ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
                ,new DB.InParameter("@DATE", ConvertTimeZoneToUtcTime(ToBeSaved.DATE))
                ,new DB.InParameter("@KPI", ToBeSaved.KPI)
                ,new DB.InParameter("@NEW", ToBeSaved.NEW)
                ,new DB.InParameter("@OPERATIONAL", ToBeSaved.OPERATIONAL)
                ,new DB.InParameter("@SUSPENDED", ToBeSaved.SUSPENDED)
                ,new DB.InParameter("@PENDING", ToBeSaved.PENDING)
                ,new DB.InParameter("@KPI_RELEVANT", ToBeSaved.KPI_RELEVANT)
                ,new DB.InParameter("@KPI_IRRELEVANT", ToBeSaved.KPI_IRRELEVANT)
                ,new DB.InParameter("@KPI_CONFORMING", ToBeSaved.KPI_CONFORMING)
                ,new DB.InParameter("@KPI_NONCONFORMING", ToBeSaved.KPI_NONCONFORMING)
                ,new DB.InParameter("@SRT_CONFORMING", ToBeSaved.SRT_CONFORMING)
                ,new DB.InParameter("@IS_MAINTENANCE_ONLY", ToBeSaved.IS_MAINTENANCE_ONLY)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_KPI = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationKpi(DB_LOCATION_KPI toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationKpi",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_KPI", toBeDeleted.ID_LOCATION_KPI)
        }
            );
        }

        #endregion

        #region Table LOCATION_DISTANCE

        public DB_LOCATION_DISTANCE[] GetLocationDistance()
        {
            return (DB_LOCATION_DISTANCE[])DB.ExecuteProcedure(
                "imrse_GetLocationDistance",
                new DB.AnalyzeDataSet(GetLocationDistance),
                new DB.Parameter[] {
            CreateTableParam("@ID_LOCATION_ORIGIN", new long[] {})
                    }
            );
        }

        public DB_LOCATION_DISTANCE[] GetLocationDistance(long[] Ids)
        {
            return (DB_LOCATION_DISTANCE[])DB.ExecuteProcedure(
                "imrse_GetLocationDistance",
                new DB.AnalyzeDataSet(GetLocationDistance),
                new DB.Parameter[] {
            CreateTableParam("@ID_LOCATION_ORIGIN", Ids)
                    }
            );
        }

        private object GetLocationDistance(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_DISTANCE()
            {
                ID_LOCATION_ORIGIN = GetValue<long>(row["ID_LOCATION_ORIGIN"]),
                ID_LOCATION_DEST = GetValue<long>(row["ID_LOCATION_DEST"]),
                DISTANCE_STRAIGHT = GetNullableValue<Double>(row["DISTANCE_STRAIGHT"]),
                DISTANCE_FORMULA = GetNullableValue<Double>(row["DISTANCE_FORMULA"]),
                DISTANCE_GOOGLE = GetNullableValue<Double>(row["DISTANCE_GOOGLE"]),
                LAST_GOOGLE_UPDATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["LAST_GOOGLE_UPDATE"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_LOCATION_DISTANCE[] list = new DB_LOCATION_DISTANCE[count];
    for (int i = 0; i < count; i++)
    {
        DB_LOCATION_DISTANCE insert = new DB_LOCATION_DISTANCE();
									insert.ID_LOCATION_ORIGIN = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_ORIGIN"]);
												insert.ID_LOCATION_DEST = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_DEST"]);
												insert.DISTANCE_STRAIGHT = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["DISTANCE_STRAIGHT"]);
												insert.DISTANCE_FORMULA = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["DISTANCE_FORMULA"]);
												insert.DISTANCE_GOOGLE = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["DISTANCE_GOOGLE"]);
												insert.LAST_GOOGLE_UPDATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["LAST_GOOGLE_UPDATE"]));
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveLocationDistance(DB_LOCATION_DISTANCE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_ORIGIN", ToBeSaved.ID_LOCATION_ORIGIN);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationDistance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_LOCATION_DEST", ToBeSaved.ID_LOCATION_DEST)
                                                        ,new DB.InParameter("@DISTANCE_STRAIGHT", ToBeSaved.DISTANCE_STRAIGHT)
                                                        ,new DB.InParameter("@DISTANCE_FORMULA", ToBeSaved.DISTANCE_FORMULA)
                                                        ,new DB.InParameter("@DISTANCE_GOOGLE", ToBeSaved.DISTANCE_GOOGLE)
                                                        ,new DB.InParameter("@LAST_GOOGLE_UPDATE", ConvertTimeZoneToUtcTime(ToBeSaved.LAST_GOOGLE_UPDATE))
                                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_ORIGIN = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationDistance(DB_LOCATION_DISTANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationDistance",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_LOCATION_ORIGIN", toBeDeleted.ID_LOCATION_ORIGIN)
                }
            );
        }

        #endregion

        #region Table MAPPING

        public DW.DB_MAPPING[] GetMappingDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MAPPING[])DB.ExecuteProcedure(
                "imrse_GetMapping",
                new DB.AnalyzeDataSet(GetMappingDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MAPPING", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DW.DB_MAPPING[] GetMappingDW(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MAPPING[])DB.ExecuteProcedure(
                "imrse_GetMapping",
                new DB.AnalyzeDataSet(GetMappingDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MAPPING", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMappingDW(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_MAPPING()
            {
                ID_MAPPING = GetValue<long>(row["ID_MAPPING"]),
                ID_REFERENCE_TYPE = GetValue<int>(row["ID_REFERENCE_TYPE"]),
                ID_SOURCE = GetValue<long>(row["ID_SOURCE"]),
                ID_DESTINATION = GetValue<long>(row["ID_DESTINATION"]),
                ID_IMR_SERVER = GetValue<int>(row["ID_IMR_SERVER"]),
                TIMESTAMP = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIMESTAMP"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MAPPING[] list = new DB_MAPPING[count];
            for (int i = 0; i < count; i++)
            {
                DB_MAPPING insert = new DB_MAPPING();
                                            insert.ID_MAPPING = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MAPPING"]);
                                                        insert.ID_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                                                        insert.ID_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SOURCE"]);
                                                        insert.ID_DESTINATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESTINATION"]);
                                                        insert.ID_IMR_SERVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER"]);
                                                        insert.TIMESTAMP = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIMESTAMP"]));
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveMappingDW(DW.DB_MAPPING ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MAPPING", ToBeSaved.ID_MAPPING);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMapping",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
                                                        ,new DB.InParameter("@ID_SOURCE", ToBeSaved.ID_SOURCE)
                                                        ,new DB.InParameter("@ID_DESTINATION", ToBeSaved.ID_DESTINATION)
                                                        ,new DB.InParameter("@ID_IMR_SERVER", ToBeSaved.ID_IMR_SERVER)
                                                        ,new DB.InParameter("@TIMESTAMP", ConvertTimeZoneToUtcTime(ToBeSaved.TIMESTAMP))
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MAPPING = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMappingDW(DW.DB_MAPPING toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMapping",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MAPPING", toBeDeleted.ID_MAPPING)
                }
            );
        }

        #endregion

        #region Table MODULE

        public DB_MODULE[] GetModule()
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE", new int[] {})
                    }
            );
        }

        public DB_MODULE[] GetModule(int[] Ids)
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE", Ids)
                    }
            );
        }

        private object GetModule(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE[] list = new DB_MODULE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE insert = new DB_MODULE();
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteModule(DB_MODULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE", toBeDeleted.ID_MODULE)
        }
            );
        }

        #endregion

        #region Table MODULE_DATA

        public DB_MODULE_DATA[] GetModuleData()
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE_DATA", new long[] {})
                    }
            );
        }

        public DB_MODULE_DATA[] GetModuleData(long[] Ids)
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE_DATA", Ids)
                    }
            );
        }



        private object GetModuleData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE_DATA[] list = new DB_MODULE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE_DATA insert = new DB_MODULE_DATA();
                insert.ID_MODULE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MODULE_DATA"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveModuleData(DB_MODULE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE_DATA", ToBeSaved.ID_MODULE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveModuleData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteModuleData(DB_MODULE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModuleData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE_DATA", toBeDeleted.ID_MODULE_DATA)
        }
            );
        }

        #endregion

        #region Table MODULE_HISTORY

        public DB_MODULE_HISTORY[] GetModuleHistory()
        {
            return (DB_MODULE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetModuleHistory",
                new DB.AnalyzeDataSet(GetModuleHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE_HISTORY", new int[] {})
                    }
            );
        }

        public DB_MODULE_HISTORY[] GetModuleHistory(int[] Ids)
        {
            return (DB_MODULE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetModuleHistory",
                new DB.AnalyzeDataSet(GetModuleHistory),
                new DB.Parameter[] {
            CreateTableParam("@ID_MODULE_HISTORY", Ids)
                    }
            );
        }



        private object GetModuleHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE_HISTORY[] list = new DB_MODULE_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE_HISTORY insert = new DB_MODULE_HISTORY();
                insert.ID_MODULE_HISTORY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE_HISTORY"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.COMMON_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COMMON_NAME"]);
                insert.VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["VERSION"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.ID_OPERATOR_UPGRADE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_UPGRADE"]);
                insert.ID_OPERATOR_ACCEPT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_ACCEPT"]);
                insert.UPGRADE_DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["UPGRADE_DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveModuleHistory(DB_MODULE_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE_HISTORY", ToBeSaved.ID_MODULE_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveModuleHistory",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
                                                        ,new DB.InParameter("@COMMON_NAME", ToBeSaved.COMMON_NAME)
                                                        ,new DB.InParameter("@VERSION", ToBeSaved.VERSION)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                        ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                        ,new DB.InParameter("@ID_OPERATOR_UPGRADE", ToBeSaved.ID_OPERATOR_UPGRADE)
                                                        ,new DB.InParameter("@ID_OPERATOR_ACCEPT", ToBeSaved.ID_OPERATOR_ACCEPT)
                                                        ,new DB.InParameter("@UPGRADE_DESCRIPTION", ToBeSaved.UPGRADE_DESCRIPTION)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE_HISTORY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteModuleHistory(DB_MODULE_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModuleHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE_HISTORY", toBeDeleted.ID_MODULE_HISTORY)
        }
            );
        }

        #endregion

        #region Table MONITORING_WATCHER

        public DW.DB_MONITORING_WATCHER[] GetMonitoringWatcherDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcher",
                new DB.AnalyzeDataSet(GetMonitoringWatcherDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DW.DB_MONITORING_WATCHER[] GetMonitoringWatcherDW(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcher",
                new DB.AnalyzeDataSet(GetMonitoringWatcherDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcherDW(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_MONITORING_WATCHER()
            {
                ID_MONITORING_WATCHER = GetValue<int>(row["ID_MONITORING_WATCHER"]),
                ID_MONITORING_WATCHER_TYPE = GetValue<int>(row["ID_MONITORING_WATCHER_TYPE"]),
                IS_ACTIVE = GetValue<bool>(row["IS_ACTIVE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_MONITORING_WATCHER[] list = new DW.DB_MONITORING_WATCHER[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_MONITORING_WATCHER insert = new DW.DB_MONITORING_WATCHER();
                                            insert.ID_MONITORING_WATCHER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER"]);
                                                        insert.ID_MONITORING_WATCHER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_TYPE"]);
                                                        insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcherDW(DW.DB_MONITORING_WATCHER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER", ToBeSaved.ID_MONITORING_WATCHER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcher",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MONITORING_WATCHER_TYPE", ToBeSaved.ID_MONITORING_WATCHER_TYPE)
                                                        ,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcherDW(DW.DB_MONITORING_WATCHER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcher",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER", toBeDeleted.ID_MONITORING_WATCHER)
                }
            );
        }

        #endregion

        #region Table MONITORING_WATCHER_DATA

        public DW.DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherDataDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherData",
                new DB.AnalyzeDataSet(GetMonitoringWatcherDataDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_DATA", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DW.DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherDataDW(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherData",
                new DB.AnalyzeDataSet(GetMonitoringWatcherDataDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_DATA", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcherDataDW(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_MONITORING_WATCHER_DATA()
            {
                ID_MONITORING_WATCHER_DATA = GetValue<int>(row["ID_MONITORING_WATCHER_DATA"]),
                ID_MONITORING_WATCHER = GetValue<int>(row["ID_MONITORING_WATCHER"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX = GetValue<int>(row["INDEX"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_MONITORING_WATCHER_DATA[] list = new DW.DB_MONITORING_WATCHER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_MONITORING_WATCHER_DATA insert = new DW.DB_MONITORING_WATCHER_DATA();
                                            insert.ID_MONITORING_WATCHER_DATA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_DATA"]);
                                                        insert.ID_MONITORING_WATCHER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.INDEX = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX"]);
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcherDataDW(DW.DB_MONITORING_WATCHER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER_DATA", ToBeSaved.ID_MONITORING_WATCHER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcherData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MONITORING_WATCHER", ToBeSaved.ID_MONITORING_WATCHER)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX", ToBeSaved.INDEX)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER_DATA = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcherDataDW(DW.DB_MONITORING_WATCHER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcherData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER_DATA", toBeDeleted.ID_MONITORING_WATCHER_DATA)
                }
            );
        }

        #endregion

        #region Table MONITORING_WATCHER_TYPE

        public DW.DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherTypeDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherType",
                new DB.AnalyzeDataSet(GetMonitoringWatcherTypeDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_TYPE", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DW.DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherTypeDW(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DW.DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherType",
                new DB.AnalyzeDataSet(GetMonitoringWatcherTypeDW),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_TYPE", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcherTypeDW(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DW.DB_MONITORING_WATCHER_TYPE()
            {
                ID_MONITORING_WATCHER_TYPE = GetValue<int>(row["ID_MONITORING_WATCHER_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                PLUGIN = GetValue<string>(row["PLUGIN"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DW.DB_MONITORING_WATCHER_TYPE[] list = new DW.DB_MONITORING_WATCHER_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DW.DB_MONITORING_WATCHER_TYPE insert = new DW.DB_MONITORING_WATCHER_TYPE();
                                            insert.ID_MONITORING_WATCHER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_TYPE"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.PLUGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcherTypeDW(DW.DB_MONITORING_WATCHER_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER_TYPE", ToBeSaved.ID_MONITORING_WATCHER_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcherType",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@PLUGIN", ToBeSaved.PLUGIN)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcherTypeDW(DW.DB_MONITORING_WATCHER_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcherType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER_TYPE", toBeDeleted.ID_MONITORING_WATCHER_TYPE)
                }
            );
        }

        #endregion

        #region Table PROBLEM_CLASS

        public DB_PROBLEM_CLASS[] GetProblemClass()
        {
            return (DB_PROBLEM_CLASS[])DB.ExecuteProcedure(
                "imrse_GetProblemClass",
                new DB.AnalyzeDataSet(GetProblemClass),
                new DB.Parameter[] {
            CreateTableParam("@ID_PROBLEM_CLASS", new int[] {})
                    }
            );
        }

        public DB_PROBLEM_CLASS[] GetProblemClass(int[] Ids)
        {
            return (DB_PROBLEM_CLASS[])DB.ExecuteProcedure(
                "imrse_GetProblemClass",
                new DB.AnalyzeDataSet(GetProblemClass),
                new DB.Parameter[] {
            CreateTableParam("@ID_PROBLEM_CLASS", Ids)
                    }
            );
        }



        private object GetProblemClass(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PROBLEM_CLASS[] list = new DB_PROBLEM_CLASS[count];
            for (int i = 0; i < count; i++)
            {
                DB_PROBLEM_CLASS insert = new DB_PROBLEM_CLASS();
                insert.ID_PROBLEM_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROBLEM_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveProblemClass(DB_PROBLEM_CLASS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PROBLEM_CLASS", ToBeSaved.ID_PROBLEM_CLASS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveProblemClass",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PROBLEM_CLASS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteProblemClass(DB_PROBLEM_CLASS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProblemClass",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PROBLEM_CLASS", toBeDeleted.ID_PROBLEM_CLASS)
        }
            );
        }

        #endregion

        #region Table REFUEL

        public DB_REFUEL[] GetRefuel()
        {
            return (DB_REFUEL[])DB.ExecuteProcedure(
                "imrse_GetRefuel",
                new DB.AnalyzeDataSet(GetRefuel),
                new DB.Parameter[] {
            CreateTableParam("@ID_REFUEL", new long[] {})
                    }
            );
        }

        public DB_REFUEL[] GetRefuel(long[] Ids)
        {
            return (DB_REFUEL[])DB.ExecuteProcedure(
                "imrse_GetRefuel",
                new DB.AnalyzeDataSet(GetRefuel),
                new DB.Parameter[] {
            CreateTableParam("@ID_REFUEL", Ids)
                    }
            );
        }



        private object GetRefuel(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_REFUEL()
            {
                ID_REFUEL = GetValue<long>(row["ID_REFUEL"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_SOURCE = GetValue<long>(row["ID_DATA_SOURCE"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REFUEL[] list = new DB_REFUEL[count];
            for (int i = 0; i < count; i++)
            {
                DB_REFUEL insert = new DB_REFUEL();
                insert.ID_REFUEL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REFUEL"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveRefuel(DB_REFUEL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REFUEL", ToBeSaved.ID_REFUEL);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRefuel",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
                                                        ,new DB.InParameter("@ID_DATA_SOURCE", ToBeSaved.ID_DATA_SOURCE)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                        ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REFUEL = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRefuel(DB_REFUEL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRefuel",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REFUEL", toBeDeleted.ID_REFUEL)
        }
            );
        }

        #endregion

        #region Table REFUEL_DATA

        /// <summary>
        /// Execute fp_GetDeliveryVolumeByReconciliation
        /// </summary>
        /// <param name="idMeters">array of idMeters</param>
        /// <param name="startDate">Date to begin with</param>
        /// <param name="endDate">Date to end with</param>
        /// <param name="declaredSt">Refuel SourceTypes: Estimation, ManualDataEntry, Interface</param>
        /// <param name="idAggregation">id of aggregation type</param>
        public List<DataTemporalValue> GetRefuelData(long[] idMeters, DateTime startDate, DateTime endDate, int[] declaredSt, int idAggregation, bool TimeCorrection)
        {
            List<DataTemporalValue> RetVal = new List<DataTemporalValue>();

            var table = this.ExecuteProcedure("fp_GetDeliveryVolumeByReconciliation", new object[] {
                            idMeters.Select(s => s as object).ToArray(),
                            startDate as object,
                            endDate as object,
                            declaredSt.Select(s => s as object).ToArray(),
                            idAggregation as object
                        });

            Dictionary<Tuple<long, DateTime>, List<DataTemporalValue>> TmpList = null;

            foreach (System.Data.DataRow row in table.Tables[0].Rows)
            {
                var x = row;
                int id_meter = Convert.ToInt32(x.ItemArray[0]);
                long id_location = Convert.ToInt64(x.ItemArray[1]);
                long id_data_type = Convert.ToInt64(x.ItemArray[2]);
                DateTime day = Convert.ToDateTime(x.ItemArray[3]);
                DateTime start_time = Convert.ToDateTime(x.ItemArray[4]);
                DateTime end_time = Convert.ToDateTime(x.ItemArray[5]);
                double value = Convert.ToDouble(x.ItemArray[6]);
                int id_data_source_type = Convert.ToInt32(x.ItemArray[7]);
                int id_aggregation_type = Convert.ToInt32(x.ItemArray[8]);

                if (TimeCorrection)
                {
                    if (TmpList == null)
                    {
                        TmpList = new Dictionary<Tuple<long, DateTime>, List<DataTemporalValue>>();
                    }

                    Tuple<long, DateTime> key = new Tuple<long, DateTime>(id_meter, day);

                    if (!TmpList.ContainsKey(key))
                    {
                        TmpList[key] = new List<DataTemporalValue>();
                    }

                    TmpList[key].Add(new DataTemporalValue(null, null, id_meter, id_location, id_data_type, 0, value, start_time, end_time, "m3", null, null, id_data_source_type, null, (Enums.AggregationType)id_aggregation_type));
                }
                else
                {
                    RetVal.Add(new DataTemporalValue(null, null, id_meter, id_location, id_data_type, 0, value, start_time, end_time, "m3", null, null, id_data_source_type, null, (Enums.AggregationType)id_aggregation_type));
                }
            }

            if (TimeCorrection && TmpList != null && TmpList.Count > 0)
            {
                foreach (var item in TmpList)
                {
                    List<DataTemporalValue> TestData = item.Value.FindAll(q => q.SourceType == Enums.DataSourceType.OKO);

                    if (TestData != null && TestData.Count > 0)
                    {
                        DateTime StartTime = TestData[0].StartTime;
                        DateTime EndTime = TestData[0].EndTime;

                        foreach (var TempValue in item.Value)
                        {
                            RetVal.Add(new DataTemporalValue(null, null, TempValue.IdMeter, TempValue.IdLocation, TempValue.Type.IdDataType, 0, TempValue.Value, StartTime, EndTime, "m3", null, null, (int)TempValue.SourceType, null, TempValue.AggregationType));
                        }
                    }
                    else
                    {
                        foreach (var TempValue in item.Value)
                        {
                            RetVal.Add(new DataTemporalValue(null, null, TempValue.IdMeter, TempValue.IdLocation, TempValue.Type.IdDataType, 0, TempValue.Value, TempValue.StartTime, TempValue.EndTime, "m3", null, null, (int)TempValue.SourceType, null, TempValue.AggregationType));
                        }
                    }
                }
            }

            return RetVal;

        }


        public DB_REFUEL_DATA[] GetRefuelData()
        {
            return (DB_REFUEL_DATA[])DB.ExecuteProcedure(
                "imrse_GetRefuelData",
                new DB.AnalyzeDataSet(GetRefuelData),
                new DB.Parameter[] {
            CreateTableParam("@ID_REFUEL_DATA", new long[] {})
                    }
            );
        }

        public DB_REFUEL_DATA[] GetRefuelData(long[] Ids)
        {
            return (DB_REFUEL_DATA[])DB.ExecuteProcedure(
                "imrse_GetRefuelData",
                new DB.AnalyzeDataSet(GetRefuelData),
                new DB.Parameter[] {
            CreateTableParam("@ID_REFUEL_DATA", Ids)
                    }
            );
        }

        private object GetRefuelData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_REFUEL_DATA()
            {
                ID_REFUEL_DATA = GetValue<long>(row["ID_REFUEL_DATA"]),
                ID_REFUEL = GetValue<long>(row["ID_REFUEL"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                STATUS = GetNullableValue<int>(row["STATUS"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REFUEL_DATA[] list = new DB_REFUEL_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_REFUEL_DATA insert = new DB_REFUEL_DATA();
                insert.ID_REFUEL_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REFUEL_DATA"]);
                insert.ID_REFUEL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REFUEL"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.STATUS = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveRefuelData(DB_REFUEL_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REFUEL_DATA", ToBeSaved.ID_REFUEL_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRefuelData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_REFUEL", ToBeSaved.ID_REFUEL)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                        ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                        ,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REFUEL_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRefuelData(DB_REFUEL_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRefuelData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REFUEL_DATA", toBeDeleted.ID_REFUEL_DATA)
        }
            );
        }

        #endregion

        #region Table REPORT

        public DB_REPORT[] GetReport(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REPORT[])DB.ExecuteProcedure(
                "imrse_GetReport",
                new DB.AnalyzeDataSet(GetReport),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT", new int[] {})
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_REPORT[] GetReport(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REPORT[])DB.ExecuteProcedure(
                "imrse_GetReport",
                new DB.AnalyzeDataSet(GetReport),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT", Ids)
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetReport(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT[] list = new DB_REPORT[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT insert = new DB_REPORT();
                insert.ID_REPORT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT"]);
                insert.ID_REPORT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT_TYPE"]);
                insert.LAYOUT = GetValue(QueryResult.Tables[0].Rows[i]["LAYOUT"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteReport(DB_REPORT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReport",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT", toBeDeleted.ID_REPORT)
        }
            );
        }

        #endregion

        #region Table REPORT_DATA

        public DB_REPORT_DATA[] GetReportData()
        {
            return (DB_REPORT_DATA[])DB.ExecuteProcedure(
                "imrse_GetReportData",
                new DB.AnalyzeDataSet(GetReportData),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_DATA", new long[] {})
                    }
            );
        }

        public DB_REPORT_DATA[] GetReportData(long[] Ids)
        {
            return (DB_REPORT_DATA[])DB.ExecuteProcedure(
                "imrse_GetReportData",
                new DB.AnalyzeDataSet(GetReportData),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_DATA", Ids)
                    }
            );
        }



        private object GetReportData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT_DATA[] list = new DB_REPORT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT_DATA insert = new DB_REPORT_DATA();
                insert.ID_REPORT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REPORT_DATA"]);
                insert.ID_REPORT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveReportData(DB_REPORT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REPORT_DATA", ToBeSaved.ID_REPORT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveReportData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_REPORT", ToBeSaved.ID_REPORT)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REPORT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteReportData(DB_REPORT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReportData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT_DATA", toBeDeleted.ID_REPORT_DATA)
        }
            );
        }

        #endregion

        #region Table REPORT_PARAM

        public DB_REPORT_PARAM[] GetReportParam()
        {
            return (DB_REPORT_PARAM[])DB.ExecuteProcedure(
                "imrse_GetReportParam",
                new DB.AnalyzeDataSet(GetReportParam),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_PARAM", new int[] {})
                    }
            );
        }

        public DB_REPORT_PARAM[] GetReportParam(int[] Ids)
        {
            return (DB_REPORT_PARAM[])DB.ExecuteProcedure(
                "imrse_GetReportParam",
                new DB.AnalyzeDataSet(GetReportParam),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_PARAM", Ids)
                    }
            );
        }



        private object GetReportParam(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT_PARAM[] list = new DB_REPORT_PARAM[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT_PARAM insert = new DB_REPORT_PARAM();
                insert.ID_REPORT_PARAM = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT_PARAM"]);
                insert.ID_REPORT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT"]);
                insert.FIELD_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIELD_NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_REFERENCE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE"]);
                insert.ID_DATA_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CLASS"]);
                insert.IS_INPUT_PARAM = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INPUT_PARAM"]);
                insert.IS_VISIBLE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_VISIBLE"]);
                insert.DEFAULT_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["DEFAULT_VALUE"]);
                insert.COLOR_BG = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLOR_BG"]);
                insert.FORMAT = GetValue<string>(QueryResult.Tables[0].Rows[i]["FORMAT"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveReportParam(DB_REPORT_PARAM ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REPORT_PARAM", ToBeSaved.ID_REPORT_PARAM);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveReportParam",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_REPORT", ToBeSaved.ID_REPORT)
                                                        ,new DB.InParameter("@FIELD_NAME", ToBeSaved.FIELD_NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                                        ,new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT)
                                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_REFERENCE", ToBeSaved.ID_REFERENCE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE_CLASS", ToBeSaved.ID_DATA_TYPE_CLASS)
                                                        ,new DB.InParameter("@IS_INPUT_PARAM", ToBeSaved.IS_INPUT_PARAM)
                                                        ,new DB.InParameter("@IS_VISIBLE", ToBeSaved.IS_VISIBLE)
                                                        ,ParamObject("@DEFAULT_VALUE", ToBeSaved.DEFAULT_VALUE)
                                                        ,new DB.InParameter("@COLOR_BG", ToBeSaved.COLOR_BG)
                                                        ,new DB.InParameter("@FORMAT", ToBeSaved.FORMAT)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REPORT_PARAM = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteReportParam(DB_REPORT_PARAM toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReportParam",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT_PARAM", toBeDeleted.ID_REPORT_PARAM)
        }
            );
        }

        #endregion

        #region Table REPORT_PERFORMANCE

        public DB_REPORT_PERFORMANCE[] GetReportPerformance()
        {
            return (DB_REPORT_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetReportPerformance",
                new DB.AnalyzeDataSet(GetReportPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_PERFORMANCE", new long[] {})
                    }
            );
        }

        public DB_REPORT_PERFORMANCE[] GetReportPerformance(long[] Ids)
        {
            return (DB_REPORT_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetReportPerformance",
                new DB.AnalyzeDataSet(GetReportPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_PERFORMANCE", Ids)
                    }
            );
        }



        private object GetReportPerformance(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT_PERFORMANCE[] list = new DB_REPORT_PERFORMANCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT_PERFORMANCE insert = new DB_REPORT_PERFORMANCE();
                insert.ID_REPORT_PERFORMANCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_REPORT_PERFORMANCE"]);
                insert.ID_REPORT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT"]);
                insert.ID_AGGREGATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveReportPerformance(DB_REPORT_PERFORMANCE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REPORT_PERFORMANCE", ToBeSaved.ID_REPORT_PERFORMANCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveReportPerformance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_REPORT", ToBeSaved.ID_REPORT)
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                        ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                        ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REPORT_PERFORMANCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteReportPerformance(DB_REPORT_PERFORMANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReportPerformance",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT_PERFORMANCE", toBeDeleted.ID_REPORT_PERFORMANCE)
        }
            );
        }

        #endregion

        #region Table REPORT_TYPE

        public DB_REPORT_TYPE[] GetReportType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REPORT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportType",
                new DB.AnalyzeDataSet(GetReportType),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_TYPE", new int[] {})
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_REPORT_TYPE[] GetReportType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REPORT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportType",
                new DB.AnalyzeDataSet(GetReportType),
                new DB.Parameter[] {
            CreateTableParam("@ID_REPORT_TYPE", Ids)
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetReportType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REPORT_TYPE[] list = new DB_REPORT_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_REPORT_TYPE insert = new DB_REPORT_TYPE();
                insert.ID_REPORT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REPORT_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveReportType(DB_REPORT_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REPORT_TYPE", ToBeSaved.ID_REPORT_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveReportType",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REPORT_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteReportType(DB_REPORT_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReportType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REPORT_TYPE", toBeDeleted.ID_REPORT_TYPE)
        }
            );
        }

        #endregion

        #region Table SYSTEM_DATA

        public DB_SYSTEM_DATA[] GetSystemData()
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] {
            CreateTableParam("@ID_SYSTEM_DATA", new long[] {})
                    }
            );
        }

        public DB_SYSTEM_DATA[] GetSystemData(long[] Ids)
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] {
            CreateTableParam("@ID_SYSTEM_DATA", Ids)
                    }
            );
        }



        private object GetSystemData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SYSTEM_DATA[] list = new DB_SYSTEM_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SYSTEM_DATA insert = new DB_SYSTEM_DATA();
                insert.ID_SYSTEM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SYSTEM_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSystemData(DB_SYSTEM_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SYSTEM_DATA", ToBeSaved.ID_SYSTEM_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSystemData",
                new DB.Parameter[] {
                    InsertId
                    ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                    ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SYSTEM_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSystemData(DB_SYSTEM_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSystemData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SYSTEM_DATA", toBeDeleted.ID_SYSTEM_DATA)
        }
            );
        }

        #endregion

        #region Table TRIGGERS

        public DB_TRIGGER[] GetTriggers()
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRIGGER", new long[] {})
                    }
            );
        }

        public DB_TRIGGER[] GetTriggers(long[] Ids)
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRIGGER", Ids)
                    }
            );
        }

        private object GetTriggers(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRIGGER[] list = new DB_TRIGGER[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRIGGER insert = new DB_TRIGGER();
                insert.ID_TRIGGER = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRIGGER"]);
                insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
                insert.AUDIT = GetValue<string>(QueryResult.Tables[0].Rows[i]["AUDIT"]);
                insert.DICT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DICT_TABLE"]);
                insert.OBJECT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["OBJECT_TABLE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTriggers(DB_TRIGGER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRIGGER", ToBeSaved.ID_TRIGGER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTrigger",
                new DB.Parameter[] {
                    InsertId
                    ,new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME)
                    ,new DB.InParameter("@AUDIT", ToBeSaved.AUDIT)
                    ,new DB.InParameter("@DICT_TABLE", ToBeSaved.DICT_TABLE)
                    ,new DB.InParameter("@OBJECT_TABLE", ToBeSaved.OBJECT_TABLE)

                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRIGGER = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTriggers(DB_TRIGGER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTrigger",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRIGGER", toBeDeleted.ID_TRIGGER)
        }
            );
        }

        #endregion

        #region Table PACKET_HOUR_ADDRESS

        public DB_PACKET_HOUR_ADDRESS[] GetPacketHourAddress()
        {
            return (DB_PACKET_HOUR_ADDRESS[])DB.ExecuteProcedure(
                "imrse_GetPacketHourAddress",
                new DB.AnalyzeDataSet(GetPacketHourAddress),
                new DB.Parameter[] {
            CreateTableParam("@ADDRESS", new string[] {})
                    }
            );
        }

        public DB_PACKET_HOUR_ADDRESS[] GetPacketHourAddress(string[] Ids)
        {
            return (DB_PACKET_HOUR_ADDRESS[])DB.ExecuteProcedure(
                "imrse_GetPacketHourAddress",
                new DB.AnalyzeDataSet(GetPacketHourAddress),
                new DB.Parameter[] {
            CreateTableParam("@ADDRESS", Ids)
                    }
            );
        }



        private object GetPacketHourAddress(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_PACKET_HOUR_ADDRESS()
            {
                ADDRESS = GetValue<string>(row["ADDRESS"]),
                DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["DATE"])),
                IS_INCOMING = GetValue<bool>(row["IS_INCOMING"]),
                PACKETS = GetValue<int>(row["PACKETS"]),
            }).ToArray();
        }
        #endregion

        #region Table PACKET_HOUR_DEVICE

        public DB_PACKET_HOUR_DEVICE[] GetPacketHourDevice()
        {
            return (DB_PACKET_HOUR_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetPacketHourDevice",
                new DB.AnalyzeDataSet(GetPacketHourDevice),
                new DB.Parameter[] {
            CreateTableParam("@SERIAL_NBR", new long[] {})
                    }
            );
        }

        public DB_PACKET_HOUR_DEVICE[] GetPacketHourDevice(long[] SerialNbrs)
        {
            return (DB_PACKET_HOUR_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetPacketHourDevice",
                new DB.AnalyzeDataSet(GetPacketHourDevice),
                new DB.Parameter[] {
            CreateTableParam("@SERIAL_NBR", SerialNbrs)
                    }
            );
        }

        private object GetPacketHourDevice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_HOUR_DEVICE[] list = new DB_PACKET_HOUR_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_HOUR_DEVICE insert = new DB_PACKET_HOUR_DEVICE();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]);
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region Table PACKET_HOUR_TRANSMISSION_DRIVER

        public DB_PACKET_HOUR_TRANSMISSION_DRIVER[] GetPacketHourTransmissionDriver()
        {
            return (DB_PACKET_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetPacketHourTransmissionDriver",
                new DB.AnalyzeDataSet(GetPacketHourTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", new int[] {})
                    }
            );
        }

        public DB_PACKET_HOUR_TRANSMISSION_DRIVER[] GetPacketHourTransmissionDriver(int[] TransmisionDriverIds)
        {
            return (DB_PACKET_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetPacketHourTransmissionDriver",
                new DB.AnalyzeDataSet(GetPacketHourTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", TransmisionDriverIds)
                    }
            );
        }

        private object GetPacketHourTransmissionDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_HOUR_TRANSMISSION_DRIVER[] list = new DB_PACKET_HOUR_TRANSMISSION_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_HOUR_TRANSMISSION_DRIVER insert = new DB_PACKET_HOUR_TRANSMISSION_DRIVER();
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]);
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                insert.ID_TRANSMISSION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region Table TEMPERATURE_COEFFICIENT
        public DB_TEMPERATURE_COEFFICIENT[] GetTemperatureCoefficient(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TEMPERATURE_COEFFICIENT[])DB.ExecuteProcedure(
                "imrse_GetTemperatureCoefficient",
                new DB.AnalyzeDataSet(GetTemperatureCoefficient),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_TEMPERATURE_COEFFICIENT", new long[] {})
				},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TEMPERATURE_COEFFICIENT[] GetTemperatureCoefficient(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TEMPERATURE_COEFFICIENT[])DB.ExecuteProcedure(
                "imrse_GetTemperatureCoefficient",
                new DB.AnalyzeDataSet(GetTemperatureCoefficient),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_TEMPERATURE_COEFFICIENT", Ids)
				},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTemperatureCoefficient(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_TEMPERATURE_COEFFICIENT()
            {
                ID_TEMPERATURE_COEFFICIENT = GetValue<long>(row["ID_TEMPERATURE_COEFFICIENT"]),
                TEMPERATURE = GetValue<Double>(row["TEMPERATURE"]),
                DENSITY = GetValue<Double>(row["DENSITY"]),
                VALUE = GetValue<Double>(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TEMPERATURE_COEFFICIENT[] list = new DB_TEMPERATURE_COEFFICIENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_TEMPERATURE_COEFFICIENT insert = new DB_TEMPERATURE_COEFFICIENT();
									        insert.ID_TEMPERATURE_COEFFICIENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TEMPERATURE_COEFFICIENT"]);
												        insert.TEMPERATURE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["TEMPERATURE"]);
												        insert.DENSITY = GetValue<Double>(QueryResult.Tables[0].Rows[i]["DENSITY"]);
												        insert.VALUE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["VALUE"]);
							        list[i] = insert;
            }
            return list;
	        */
            #endregion
        }

        public long SaveTemperatureCoefficient(DB_TEMPERATURE_COEFFICIENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TEMPERATURE_COEFFICIENT", ToBeSaved.ID_TEMPERATURE_COEFFICIENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTemperatureCoefficient",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@TEMPERATURE", ToBeSaved.TEMPERATURE)
					,new DB.InParameter("@DENSITY", ToBeSaved.DENSITY)
					,new DB.InParameter("@VALUE", ToBeSaved.VALUE)
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TEMPERATURE_COEFFICIENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTemperatureCoefficient(DB_TEMPERATURE_COEFFICIENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTemperatureCoefficient",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_TEMPERATURE_COEFFICIENT", toBeDeleted.ID_TEMPERATURE_COEFFICIENT)			
		        }
            );
        }
        #endregion

        #region Table TRANSMISSION_DRIVER

        public DB_TRANSMISSION_DRIVER[] GetTransmissionDriver(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriver",
                new DB.AnalyzeDataSet(GetTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", new int[] {})
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TRANSMISSION_DRIVER[] GetTransmissionDriver(int[] TransmisionDriverIds, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriver",
                new DB.AnalyzeDataSet(GetTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", TransmisionDriverIds)
                    }, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTransmissionDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_DRIVER[] list = new DB_TRANSMISSION_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_DRIVER insert = new DB_TRANSMISSION_DRIVER();
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.ID_TRANSMISSION_DRIVER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_TYPE"]);
                insert.TRANSMISSION_DRIVER_TYPE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TRANSMISSION_DRIVER_TYPE_NAME"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.RESPONSE_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["RESPONSE_ADDRESS"]);
                insert.PLUGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN"]);
                              
                if (QueryResult.Tables[0].Columns.Contains("ID_TRANSMISSION_DRIVER_DAQ"))
                {
                    insert.ID_TRANSMISSION_DRIVER_DAQ = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_DAQ"]);
                }
                if (QueryResult.Tables[0].Columns.Contains("IS_ACTIVE"))
                {
                    insert.IS_ACTIVE = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                }

                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionDriver(DB_TRANSMISSION_DRIVER ToBeSaved)
        {
            List<DB.Parameter> parameters = new List<Data.DB.DB.Parameter>();
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER);
            parameters.AddRange(new DB.Parameter[] {
                InsertId
                                        ,new DB.InParameter("@ID_TRANSMISSION_DRIVER_TYPE", ToBeSaved.ID_TRANSMISSION_DRIVER_TYPE)
                                                        ,new DB.InParameter("@TRANSMISSION_DRIVER_TYPE_NAME", ToBeSaved.TRANSMISSION_DRIVER_TYPE_NAME)
                                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@RESPONSE_ADDRESS", ToBeSaved.RESPONSE_ADDRESS)
                                                        ,new DB.InParameter("@PLUGIN", ToBeSaved.PLUGIN)                                                        
                                    }
            );
            if (IsParameterDefined("imrse_SaveTransmissionDriver", "@ID_TRANSMISSION_DRIVER_DAQ", this))
                parameters.Add(new DB.InParameter("@ID_TRANSMISSION_DRIVER_DAQ", ToBeSaved.ID_TRANSMISSION_DRIVER_DAQ));
            if (IsParameterDefined("imrse_SaveTransmissionDriver", "@IS_ACTIVE", this))
                parameters.Add(new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE));

            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriver",
                parameters.ToArray()
            );

            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public int SaveTransmissionDriverOneToOne(DB_TRANSMISSION_DRIVER ToBeSaved)
        {
            List<DB.Parameter> parameters = new List<Data.DB.DB.Parameter>();
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER);
            parameters.AddRange(new DB.Parameter[] {
                InsertId
                                        ,new DB.InParameter("@ID_TRANSMISSION_DRIVER_TYPE", ToBeSaved.ID_TRANSMISSION_DRIVER_TYPE)
                                                        ,new DB.InParameter("@TRANSMISSION_DRIVER_TYPE_NAME", ToBeSaved.TRANSMISSION_DRIVER_TYPE_NAME)
                                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@RESPONSE_ADDRESS", ToBeSaved.RESPONSE_ADDRESS)
                                                        ,new DB.InParameter("@PLUGIN", ToBeSaved.PLUGIN)
                                    }
            );
            if (IsParameterDefined("imrse_u_SaveTransmissionDriver", "@ID_TRANSMISSION_DRIVER_DAQ", this))
                parameters.Add(new DB.InParameter("@ID_TRANSMISSION_DRIVER_DAQ", ToBeSaved.ID_TRANSMISSION_DRIVER_DAQ));
            if (IsParameterDefined("imrse_u_SaveTransmissionDriver", "@IS_ACTIVE", this))
                parameters.Add(new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE));

            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveTransmissionDriver",
                parameters.ToArray()
            );

            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        #endregion

        #region TRANSMISSION_DRIVER_DATA


        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverData",
                new DB.AnalyzeDataSet(GetTransmissionDriverData),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", new long[] {})
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverData",
                new DB.AnalyzeDataSet(GetTransmissionDriverData),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", Ids)
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetTransmissionDriverData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_TRANSMISSION_DRIVER_DATA()
            {
                ID_TRANSMISSION_DRIVER_DATA = GetValue<long>(row["ID_TRANSMISSION_DRIVER_DATA"]),
                ID_TRANSMISSION_DRIVER = GetValue<int>(row["ID_TRANSMISSION_DRIVER"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_TRANSMISSION_DRIVER_DATA[] list = new DB_TRANSMISSION_DRIVER_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_TRANSMISSION_DRIVER_DATA insert = new DB_TRANSMISSION_DRIVER_DATA();
									insert.ID_TRANSMISSION_DRIVER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_DATA"]);
												insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveTransmissionDriverData(DB_TRANSMISSION_DRIVER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER_DATA", ToBeSaved.ID_TRANSMISSION_DRIVER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriverData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        #endregion

        #region Table TRANSMISSION_DRIVER_PERFORMANCE

        public DB_TRANSMISSION_DRIVER_PERFORMANCE[] GetTransmissionDriverPerformance(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverPerformance",
                new DB.AnalyzeDataSet(GetTransmissionDriverPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER_PERFORMANCE", new long[] {})
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TRANSMISSION_DRIVER_PERFORMANCE[] GetTransmissionDriverPerformance(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TRANSMISSION_DRIVER_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverPerformance",
                new DB.AnalyzeDataSet(GetTransmissionDriverPerformance),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER_PERFORMANCE", Ids)
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetTransmissionDriverPerformance(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_TRANSMISSION_DRIVER_PERFORMANCE()
            {
                ID_TRANSMISSION_DRIVER_PERFORMANCE = GetValue<long>(row["ID_TRANSMISSION_DRIVER_PERFORMANCE"]),
                ID_TRANSMISSION_DRIVER = GetValue<int>(row["ID_TRANSMISSION_DRIVER"]),
                ID_AGGREGATION_TYPE = GetValue<int>(row["ID_AGGREGATION_TYPE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["END_TIME"])),
                VALUE = GetValue(row["VALUE"]),
                ID_DATA_SOURCE_TYPE = GetNullableValue<int>(row["ID_DATA_SOURCE_TYPE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_TRANSMISSION_DRIVER_PERFORMANCE[] list = new DB_TRANSMISSION_DRIVER_PERFORMANCE[count];
    for (int i = 0; i < count; i++)
    {
        DB_TRANSMISSION_DRIVER_PERFORMANCE insert = new DB_TRANSMISSION_DRIVER_PERFORMANCE();
									insert.ID_TRANSMISSION_DRIVER_PERFORMANCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_PERFORMANCE"]);
												insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
												insert.ID_AGGREGATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_AGGREGATION_TYPE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
												insert.ID_DATA_SOURCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_SOURCE_TYPE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveTransmissionDriverPerformance(DB_TRANSMISSION_DRIVER_PERFORMANCE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER_PERFORMANCE", ToBeSaved.ID_TRANSMISSION_DRIVER_PERFORMANCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriverPerformance",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER)
                                                        ,new DB.InParameter("@ID_AGGREGATION_TYPE", ToBeSaved.ID_AGGREGATION_TYPE)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
                                                    ,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
                                                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                    ,new DB.InParameter("@ID_DATA_SOURCE_TYPE", ToBeSaved.ID_DATA_SOURCE_TYPE)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER_PERFORMANCE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTransmissionDriverPerformance(DB_TRANSMISSION_DRIVER_PERFORMANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionDriverPerformance",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_DRIVER_PERFORMANCE", toBeDeleted.ID_TRANSMISSION_DRIVER_PERFORMANCE)
        }
            );
        }

        #endregion

        #region Table PACKET_TRASH_HOUR_ADDRESS

        public DB_PACKET_TRASH_HOUR_ADDRESS[] GetPacketTrashHourAddress()
        {
            return (DB_PACKET_TRASH_HOUR_ADDRESS[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourAddress",
                new DB.AnalyzeDataSet(GetPacketTrashHourAddress),
                new DB.Parameter[] {
            CreateTableParam("@ADDRESS", new string[] {})
                    }
            );
        }

        public DB_PACKET_TRASH_HOUR_ADDRESS[] GetPacketTrashHourAddress(string[] Ids)
        {
            return (DB_PACKET_TRASH_HOUR_ADDRESS[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourAddress",
                new DB.AnalyzeDataSet(GetPacketTrashHourAddress),
                new DB.Parameter[] {
            CreateTableParam("@ADDRESS", Ids)
                    }
            );
        }



        private object GetPacketTrashHourAddress(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_TRASH_HOUR_ADDRESS[] list = new DB_PACKET_TRASH_HOUR_ADDRESS[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_TRASH_HOUR_ADDRESS insert = new DB_PACKET_TRASH_HOUR_ADDRESS();
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]));
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region Table PACKET_TRASH_HOUR_DEVICE

        public DB_PACKET_TRASH_HOUR_DEVICE[] GetPacketTrashHourDevice()
        {
            return (DB_PACKET_TRASH_HOUR_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourDevice",
                new DB.AnalyzeDataSet(GetPacketTrashHourDevice),
                new DB.Parameter[] {
            CreateTableParam("@SERIAL_NBR", new long?[] {})
                    }
            );
        }

        public DB_PACKET_TRASH_HOUR_DEVICE[] GetPacketTrashHourDevice(long?[] Ids)
        {
            return (DB_PACKET_TRASH_HOUR_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourDevice",
                new DB.AnalyzeDataSet(GetPacketTrashHourDevice),
                new DB.Parameter[] {
            CreateTableParam("@SERIAL_NBR", Ids)
                    }
            );
        }



        private object GetPacketTrashHourDevice(DataSet QueryResult)
        {

            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_TRASH_HOUR_DEVICE[] list = new DB_PACKET_TRASH_HOUR_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_TRASH_HOUR_DEVICE insert = new DB_PACKET_TRASH_HOUR_DEVICE();
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]));
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region Table PACKET_TRASH_HOUR_TRANSMISSION_DRIVER

        public DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] GetPacketTrashHourTransmissionDriver()
        {
            return (DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourTransmissionDriver",
                new DB.AnalyzeDataSet(GetPacketTrashHourTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", new int[] {})
                    }
            );
        }

        public DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] GetPacketTrashHourTransmissionDriver(int[] Ids)
        {
            return (DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetPacketTrashHourTransmissionDriver",
                new DB.AnalyzeDataSet(GetPacketTrashHourTransmissionDriver),
                new DB.Parameter[] {
            CreateTableParam("@ID_TRANSMISSION_DRIVER", Ids)
                    }
            );
        }



        private object GetPacketTrashHourTransmissionDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] list = new DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER insert = new DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER();
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]));
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                insert.ID_TRANSMISSION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region Table DATA_MISSING_ANALOG_READOUTS

        public DB_DATA_MISSING_ANALOG_READOUTS[] GetDataMissingAnalogReadouts()
        {
            return (DB_DATA_MISSING_ANALOG_READOUTS[])DB.ExecuteProcedure(
                "imrse_GetDataMissingAnalogReadouts",
                new DB.AnalyzeDataSet(GetDataMissingAnalogReadouts),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_MISSING_ANALOG_READOUTS", new long[] {})
                    }
            );
        }

        public DB_DATA_MISSING_ANALOG_READOUTS[] GetDataMissingAnalogReadouts(long[] Ids)
        {
            return (DB_DATA_MISSING_ANALOG_READOUTS[])DB.ExecuteProcedure(
                "imrse_GetDataMissingAnalogReadouts",
                new DB.AnalyzeDataSet(GetDataMissingAnalogReadouts),
                new DB.Parameter[] {
            CreateTableParam("@ID_DATA_MISSING_ANALOG_READOUTS", Ids)
                    }
            );
        }



        private object GetDataMissingAnalogReadouts(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_MISSING_ANALOG_READOUTS[] list = new DB_DATA_MISSING_ANALOG_READOUTS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_MISSING_ANALOG_READOUTS insert = new DB_DATA_MISSING_ANALOG_READOUTS();
                insert.ID_DATA_MISSING_ANALOG_READOUTS = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_MISSING_ANALOG_READOUTS"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.SERIAL_NBR_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PARENT"]);
                insert.MISSING_DATA_START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["MISSING_DATA_START_TIME"]));
                insert.PACKETS_COUNT = GetValue<int>(QueryResult.Tables[0].Rows[i]["PACKETS_COUNT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataMissingAnalogReadouts(DB_DATA_MISSING_ANALOG_READOUTS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_MISSING_ANALOG_READOUTS", ToBeSaved.ID_DATA_MISSING_ANALOG_READOUTS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataMissingAnalogReadouts",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
                                                        ,new DB.InParameter("@SERIAL_NBR_PARENT", ToBeSaved.SERIAL_NBR_PARENT)
                                                        ,new DB.InParameter("@MISSING_DATA_START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.MISSING_DATA_START_TIME))
                                                        ,new DB.InParameter("@PACKETS_COUNT", ToBeSaved.PACKETS_COUNT)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_MISSING_ANALOG_READOUTS = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataMissingAnalogReadouts(DB_DATA_MISSING_ANALOG_READOUTS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataMissingAnalogReadouts",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_MISSING_ANALOG_READOUTS", toBeDeleted.ID_DATA_MISSING_ANALOG_READOUTS)
        }
            );
        }

        #endregion
        #region DATA
        public DataArchValue[] GetDataForLocations(long[] Locations, DATA_TYPE_UNIT[] TypesWithUnits, long? StatusMaskAny, long? StatusMaskAll)
        {
            if (Locations == null || Locations.Length == 0 || TypesWithUnits == null || TypesWithUnits.Length == 0)
            {
                return new DataArchValue[0];
            }

            DB.InParameter paramLocations = CreateTableParam("@LOCATION_IDS", Locations);
            DB.InParameter paramTypes = CreateTableParam("@TYPES_WITH_UNITS", TypesWithUnits);

            DataArchValue[] result = (DataArchValue[])DB.ExecuteProcedure(
                    "sp_GetLatestDataForLocations",
                    new DB.AnalyzeDataSet(GetDataValues),
                    new DB.Parameter[]
    {
                    paramLocations,
                    paramTypes,
                    new DB.InParameter ("@ID_LANGUAGE", (int)this.Language),
                    new DB.InParameter ("@STATUS_MASK_ANY", StatusMaskAny),
                    new DB.InParameter ("@STATUS_MASK_ALL", StatusMaskAll),
    },
                false,
                this.LongTimeout
            );

            return result;

        }
        private object GetDataValues(DataSet QueryResult)
        {
            DataRowCollection rows = QueryResult.Tables[0].Rows;
            bool bIndexColumn = false;
            if (QueryResult.Tables[0].Columns.Contains("INDEX_NBR"))
                bIndexColumn = true;

            DataArchValue[] result = new DataArchValue[rows.Count];

            DataArchValue dataArchValue;
            long idDataArch;
            ulong? serialNbr;
            long? idMeter;
            long? idLocation;
            long idDataType;
            object value;
            int index = 0;
            DateTime time;
            string unit;
            long? idDataSource;
            int? idDataSourceType;
            long status;

            for (int i = 0; i < rows.Count; i++)
            {
                idDataArch = Convert.ToInt64(rows[i]["ID_DATA"]);
                GetPossiblyNullValue(rows[i], "SERIAL_NBR", out serialNbr);
                GetPossiblyNullValue(rows[i], "ID_METER", out idMeter);
                GetPossiblyNullValue(rows[i], "ID_LOCATION", out idLocation);
                idDataType = Convert.ToInt64(rows[i]["ID_DATA_TYPE"]);
                if (bIndexColumn)
                    index = Convert.ToInt32(rows[i]["INDEX_NBR"]);
                GetPossiblyNullValue(rows[i], "VALUE", out value);
                time = Convert.ToDateTime(rows[i]["TIME"]);
                unit = rows[i]["SYMBOL"].ToString();
                status = Convert.ToInt64(rows[i]["STATUS"]);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE", out idDataSource);
                GetPossiblyNullValue(rows[i], "ID_DATA_SOURCE_TYPE", out idDataSourceType);

                dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, index, value, time, unit, status, idDataSource, idDataSourceType, null);

                //if (bIndexColumn)
                //    dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, index, value, time, unit, status, idDataSource, idDataSourceType, idAlarmEvent);
                //else
                //    dataArchValue = new DataArchValue(idDataArch, SN.FromDBValue(serialNbr), idMeter, idLocation, idDataType, value, time, unit, status, idDataSource, idDataSourceType, idAlarmEvent);

                result[i] = dataArchValue;
            }

            return result;
        }
        #endregion

    }
}
