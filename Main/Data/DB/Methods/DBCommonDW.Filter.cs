﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.DW;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDW
    {
        #region GetAlarmFilter
        public DW.DB_ALARM[] GetAlarmFilter(long[] IdAlarm, long[] IdAlarmEvent, int[] IdAlarmDef, long[] SerialNbr, long[] IdMeter, long[] IdLocation,
                            int[] IdAlarmType, long[] IdDataTypeAlarm, int[] IdOperator, int[] IdAlarmStatus, string TransmissionType,
                            int[] IdTransmissionType, int[] IdAlarmGroup, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdAlarm != null && IdAlarm.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmDef != null && IdAlarmDef.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmType != null && IdAlarmType.Length > MaxItemsPerSqlTableType) ||
                (IdDataTypeAlarm != null && IdDataTypeAlarm.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmStatus != null && IdAlarmStatus.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionType != null && IdTransmissionType.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmGroup != null && IdAlarmGroup.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAlarm != null) args.AddRange(IdAlarm.Select(s => new Tuple<int, object>(1, s)));
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(2, s)));
                if (IdAlarmDef != null) args.AddRange(IdAlarmDef.Select(s => new Tuple<int, object>(3, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(4, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(5, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(6, s)));
                if (IdAlarmType != null) args.AddRange(IdAlarmType.Select(s => new Tuple<int, object>(7, s)));
                if (IdDataTypeAlarm != null) args.AddRange(IdDataTypeAlarm.Select(s => new Tuple<int, object>(8, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(9, s)));
                if (IdAlarmStatus != null) args.AddRange(IdAlarmStatus.Select(s => new Tuple<int, object>(10, s)));
                if (IdTransmissionType != null) args.AddRange(IdTransmissionType.Select(s => new Tuple<int, object>(12, s)));
                if (IdAlarmGroup != null) args.AddRange(IdAlarmGroup.Select(s => new Tuple<int, object>(13, s)));

                DW.DB_ALARM[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_ALARM[])DB.ExecuteProcedure(
                        "imrse_GetAlarmFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAlarm),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@TRANSMISSION_TYPE", TransmissionType),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DW.DB_ALARM[])DB.ExecuteProcedure(
                    "imrse_GetAlarmFilter",
                    new DB.AnalyzeDataSet(GetAlarm),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_ALARM", IdAlarm),
                        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
                        CreateTableParam("@ID_ALARM_DEF", IdAlarmDef),
                        CreateTableParam("@SERIAL_NBR", SerialNbr),
                        CreateTableParam("@ID_METER", IdMeter),
                        CreateTableParam("@ID_LOCATION", IdLocation),
                        CreateTableParam("@ID_ALARM_TYPE", IdAlarmType),
                        CreateTableParam("@ID_DATA_TYPE_ALARM", IdDataTypeAlarm),
                        CreateTableParam("@ID_OPERATOR", IdOperator),
                        CreateTableParam("@ID_ALARM_STATUS", IdAlarmStatus),
                        new DB.InParameter("@TRANSMISSION_TYPE", TransmissionType),
                        CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                        CreateTableParam("@ID_ALARM_GROUP", IdAlarmGroup),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetAlarmEventFilter

        public DW.DB_ALARM_EVENT[] GetAlarmEventFilterDW(long[] IdAlarmEvent, long[] IdAlarmDef, long[] SerialNbr, long[] IdMeter, long[] IdLocation, int[] IdAlarmType,
                            int[] IdDataTypeAlarm, TypeDateTimeCode Time, long[] IdIssue, bool? IsConfirmed, int[] ConfirmedBy,
                            TypeDateTimeCode ConfirmTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmEventFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmDef != null && IdAlarmDef.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmType != null && IdAlarmType.Length > MaxItemsPerSqlTableType) ||
                (IdDataTypeAlarm != null && IdDataTypeAlarm.Length > MaxItemsPerSqlTableType) ||
                (IdIssue != null && IdIssue.Length > MaxItemsPerSqlTableType) ||
                (ConfirmedBy != null && ConfirmedBy.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(1, s)));
                if (IdAlarmDef != null) args.AddRange(IdAlarmDef.Select(s => new Tuple<int, object>(2, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(3, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(4, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(5, s)));
                if (IdAlarmType != null) args.AddRange(IdAlarmType.Select(s => new Tuple<int, object>(6, s)));
                if (IdDataTypeAlarm != null) args.AddRange(IdDataTypeAlarm.Select(s => new Tuple<int, object>(7, s)));
                if (IdIssue != null) args.AddRange(IdIssue.Select(s => new Tuple<int, object>(9, s)));
                if (ConfirmedBy != null) args.AddRange(ConfirmedBy.Select(s => new Tuple<int, object>(11, s)));

                DW.DB_ALARM_EVENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_ALARM_EVENT[])DB.ExecuteProcedure(
                        "imrse_GetAlarmEventFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAlarmEventDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@IS_CONFIRMED", IsConfirmed),
                    CreateTableParam("@CONFIRM_TIME_CODE", ConfirmTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DW.DB_ALARM_EVENT[])DB.ExecuteProcedure(
                    "imrse_GetAlarmEventFilter",
                    new DB.AnalyzeDataSet(GetAlarmEventDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
                CreateTableParam("@ID_ALARM_DEF", IdAlarmDef),
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@ID_METER", IdMeter),
                CreateTableParam("@ID_LOCATION", IdLocation),
                CreateTableParam("@ID_ALARM_TYPE", IdAlarmType),
                CreateTableParam("@ID_DATA_TYPE_ALARM", IdDataTypeAlarm),
                CreateTableParam("@TIME_CODE", Time),
                CreateTableParam("@ID_ISSUE", IdIssue),
                new DB.InParameter("@IS_CONFIRMED", IsConfirmed),
                CreateTableParam("@CONFIRMED_BY", ConfirmedBy),
                CreateTableParam("@CONFIRM_TIME_CODE", ConfirmTime),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetAlarmHistoryFilter
        public DW.DB_ALARM_HISTORY[] GetAlarmHistoryFilter(long[] IdAlarmHistory, long[] IdAlarm, int[] IdAlarmStatus, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmHistoryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdAlarmHistory != null && IdAlarmHistory.Length > MaxItemsPerSqlTableType) ||
                (IdAlarm != null && IdAlarm.Length > MaxItemsPerSqlTableType) ||
                (IdAlarmStatus != null && IdAlarmStatus.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAlarmHistory != null) args.AddRange(IdAlarmHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdAlarm != null) args.AddRange(IdAlarm.Select(s => new Tuple<int, object>(2, s)));
                if (IdAlarmStatus != null) args.AddRange(IdAlarmStatus.Select(s => new Tuple<int, object>(3, s)));

                DW.DB_ALARM_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetAlarmHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAlarmHistory),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_TIME_CODE", StartTime),
                            CreateTableParam("@END_TIME_CODE", EndTime),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DW.DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetAlarmHistoryFilter",
                    new DB.AnalyzeDataSet(GetAlarmHistory),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_ALARM_HISTORY", IdAlarmHistory),
                        CreateTableParam("@ID_ALARM", IdAlarm),
                        CreateTableParam("@ID_ALARM_STATUS", IdAlarmStatus),
                        CreateTableParam("@START_TIME_CODE", StartTime),
                        CreateTableParam("@END_TIME_CODE", EndTime),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetAlarmMessageFilter
        public DW.DB_ALARM_MESSAGE[] GetAlarmMessageFilter(long[] IdAlarmEvent, int[] IdLanguage, string AlarmMessage, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmMessageFilter"));


            if (UseBulkInsertSqlTableType && (
                (IdAlarmEvent != null && IdAlarmEvent.Length > MaxItemsPerSqlTableType) ||
                (IdLanguage != null && IdLanguage.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAlarmEvent != null) args.AddRange(IdAlarmEvent.Select(s => new Tuple<int, object>(1, s)));
                if (IdLanguage != null) args.AddRange(IdLanguage.Select(s => new Tuple<int, object>(2, s)));

                DW.DB_ALARM_MESSAGE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                        "imrse_GetAlarmMessageFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAlarmMessage),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            new DB.InParameter("@ALARM_MESSAGE", AlarmMessage),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DW.DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                    "imrse_GetAlarmMessageFilter",
                    new DB.AnalyzeDataSet(GetAlarmMessage),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
                        CreateTableParam("@ID_LANGUAGE", IdLanguage),
                        new DB.InParameter("@ALARM_MESSAGE", AlarmMessage),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetAuditFilter

        public DB_AUDIT[] GetAuditFilter(long[] IdAudit, long[] BatchId, int[] ChangeType, string TableName, long[] Key1, long[] Key2,
                            long[] Key3, long[] Key4, long[] Key5, long[] Key6, string ColumnName,
                            TypeDateTimeCode Time, string User, string Host, string Application, string Contextinfo, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();

            if (UseBulkInsertSqlTableType && (
                (IdAudit != null && IdAudit.Length > MaxItemsPerSqlTableType) ||
                (BatchId != null && BatchId.Length > MaxItemsPerSqlTableType) ||
                (ChangeType != null && ChangeType.Length > MaxItemsPerSqlTableType) ||
                (Key1 != null && Key1.Length > MaxItemsPerSqlTableType) ||
                (Key2 != null && Key2.Length > MaxItemsPerSqlTableType) ||
                (Key3 != null && Key3.Length > MaxItemsPerSqlTableType) ||
                (Key4 != null && Key4.Length > MaxItemsPerSqlTableType) ||
                (Key5 != null && Key5.Length > MaxItemsPerSqlTableType) ||
                (Key6 != null && Key6.Length > MaxItemsPerSqlTableType)
                        ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAudit != null) args.AddRange(IdAudit.Select(s => new Tuple<int, object>(1, s)));
                if (BatchId != null) args.AddRange(BatchId.Select(s => new Tuple<int, object>(2, s)));
                if (ChangeType != null) args.AddRange(ChangeType.Select(s => new Tuple<int, object>(3, s)));
                if (Key1 != null) args.AddRange(Key1.Select(s => new Tuple<int, object>(5, s)));
                if (Key2 != null) args.AddRange(Key2.Select(s => new Tuple<int, object>(6, s)));
                if (Key3 != null) args.AddRange(Key3.Select(s => new Tuple<int, object>(7, s)));
                if (Key4 != null) args.AddRange(Key4.Select(s => new Tuple<int, object>(8, s)));
                if (Key5 != null) args.AddRange(Key5.Select(s => new Tuple<int, object>(9, s)));
                if (Key6 != null) args.AddRange(Key6.Select(s => new Tuple<int, object>(10, s)));

                DB_AUDIT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    parameters.Add(new DB.InParameter("@ID", SqlDbType.BigInt, id));
                    parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
                    parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
                    parameters.Add(CreateTableParam("@TIME_CODE", Time));
                    parameters.Add(new DB.InParameter("@USER", User));
                    parameters.Add(new DB.InParameter("@HOST", Host));
                    parameters.Add(new DB.InParameter("@APPLICATION", Application));
                    if (IsParameterDefined("imrse_GetAuditFilterTableArgs", "@CONTEXTINFO", this))
                        parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
                    parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                    parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
                    result = (DB_AUDIT[])DB.ExecuteProcedure(
                        "imrse_GetAuditFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAudit),
                        parameters.ToArray(),
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                //    DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                parameters.Add(CreateTableParam("@ID_AUDIT", IdAudit));
                parameters.Add(CreateTableParam("@BATCH_ID", BatchId));
                parameters.Add(CreateTableParam("@CHANGE_TYPE", ChangeType));
                parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
                parameters.Add(CreateTableParam("@KEY1", Key1));
                parameters.Add(CreateTableParam("@KEY2", Key2));
                parameters.Add(CreateTableParam("@KEY3", Key3));
                parameters.Add(CreateTableParam("@KEY4", Key4));
                parameters.Add(CreateTableParam("@KEY5", Key5));
                parameters.Add(CreateTableParam("@KEY6", Key6));
                parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
                parameters.Add(CreateTableParam("@TIME_CODE", Time));
                parameters.Add(new DB.InParameter("@USER", User));
                parameters.Add(new DB.InParameter("@HOST", Host));
                parameters.Add(new DB.InParameter("@APPLICATION", Application));
                if (IsParameterDefined("imrse_GetAuditFilter", "@CONTEXTINFO", this))
                    parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
                parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
                parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
                return (DB_AUDIT[])DB.ExecuteProcedure(
                    "imrse_GetAuditFilter",
                    new DB.AnalyzeDataSet(GetAudit),
                    parameters.ToArray(),
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetConsumerTransactionFilter

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransactionFilter(int[] IdConsumerTransaction, int[] IdConsumer, int[] IdOperator, int[] IdDistributor, int[] IdTransactionType, int[] IdTariff,
                            TypeDateTimeCode Timestamp, Double[] ChargePrepaid, Double[] ChargeEc, Double[] ChargeOwed, Double[] BalancePrepaid,
                            Double[] BalanceEc, Double[] BalanceOwed, Double[] MeterIndex, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                            long[] IdConsumerSettlement = null, int[] IdModule = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionFilter"));

            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionFilter",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER_TRANSACTION", IdConsumerTransaction),
                    CreateTableParam("@ID_CONSUMER", IdConsumer),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_TRANSACTION_TYPE", IdTransactionType),
                    CreateTableParam("@ID_TARIFF", IdTariff),
                    CreateTableParam("@TIMESTAMP_CODE", Timestamp),
                    CreateTableParam("@CHARGE_PREPAID", ChargePrepaid),
                    CreateTableParam("@CHARGE_EC", ChargeEc),
                    CreateTableParam("@CHARGE_OWED", ChargeOwed),
                    CreateTableParam("@BALANCE_PREPAID", BalancePrepaid),
                    CreateTableParam("@BALANCE_EC", BalanceEc),
                    CreateTableParam("@BALANCE_OWED", BalanceOwed),
                    CreateTableParam("@METER_INDEX", MeterIndex),
                    CreateTableParam("@ID_CONSUMER_SETTLEMENT", IdConsumerSettlement),
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetConsumerTransactionDataFilter

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionDataFilter(long[] IdConsumerTransactionData, int[] IdConsumerTransaction, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionDataFilter"));

            return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionDataFilter",
                new DB.AnalyzeDataSet(GetConsumerTransactionData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", IdConsumerTransactionData),
                    CreateTableParam("@ID_CONSUMER_TRANSACTION", IdConsumerTransaction),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetActionDataSourceFilter

        public DB_ACTION_DATA_SOURCE[] GetActionDataSourceFilter(long[] IdAction, long[] IdDataSource, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDataSourceFilter"));

            return (DB_ACTION_DATA_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetActionDataSourceFilter",
                new DB.AnalyzeDataSet(GetActionDataSource),
                new DB.Parameter[] {
                    CreateTableParam("@ID_ACTION", IdAction),
                    CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataSourceFilter

        public DB_DATA_SOURCE[] GetDataSourceFilter(long[] IdDataSource, int[] IdDataSourceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceFilter"));

            return (DB_DATA_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetDataSourceFilter",
                new DB.AnalyzeDataSet(GetDataSource),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
                    CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataSourceDataFilter

        public DB_DATA_SOURCE_DATA[] GetDataSourceDataFilter(long[] IdDataSource, long[] IdDataType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceDataFilter"));

            return (DB_DATA_SOURCE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDataSourceDataFilter",
                new DB.AnalyzeDataSet(GetDataSourceData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_SOURCE", IdDataSource),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataSourceTypeFilter

        public DB_DATA_SOURCE_TYPE[] GetDataSourceTypeFilter(int[] IdDataSourceType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceTypeFilter"));

            return (DB_DATA_SOURCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataSourceTypeFilter",
                new DB.AnalyzeDataSet(GetDataSourceType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeliveryAdviceFilter
        public DB_DELIVERY_ADVICE[] GetDeliveryAdviceFilter(int[] IdDeliveryAdvice, string Name, long[] IdDescr, int[] Priority, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryAdviceFilter"));

            return (DB_DELIVERY_ADVICE[])DB.ExecuteProcedure(
                "imrse_GetDeliveryAdviceFilter",
                new DB.AnalyzeDataSet(GetDeliveryAdvice),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DELIVERY_ADVICE", IdDeliveryAdvice),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@PRIORITY", Priority),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetDeliveryBatchFilter
        public DB_DELIVERY_BATCH[] GetDeliveryBatchFilter(int[] IdDeliveryBatch, TypeDateTimeCode CreationDate, string Name, int[] IdDeliveryBatchStatus, int[] IdOperator, TypeDateTimeCode CommitDate,
                            int[] IdDistributor, int[] SuccessTotal, int[] FailedTotal, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryBatchFilter"));

            return (DB_DELIVERY_BATCH[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatchFilter",
                new DB.AnalyzeDataSet(GetDeliveryBatch),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DELIVERY_BATCH", IdDeliveryBatch),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DELIVERY_BATCH_STATUS", IdDeliveryBatchStatus),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    CreateTableParam("@COMMIT_DATE_CODE", CommitDate),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@SUCCESS_TOTAL", SuccessTotal),
                    CreateTableParam("@FAILED_TOTAL", FailedTotal),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetDeliveryBatchStatusFilter

        public DB_DELIVERY_BATCH_STATUS[] GetDeliveryBatchStatusFilter(int[] IdDeliveryBatchStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryBatchStatusFilter"));

            return (DB_DELIVERY_BATCH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDeliveryBatchStatusFilter",
                new DB.AnalyzeDataSet(GetDeliveryBatchStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DELIVERY_BATCH_STATUS", IdDeliveryBatchStatus),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeliveryOrderHistoryFilter

        public DB_DELIVERY_ORDER_HISTORY[] GetDeliveryOrderHistoryFilter(long[] IdDeliveryOrderHistory, int[] IdDeliveryOrder, int[] IdOperator, bool? IsSuccesfull, TypeDateTimeCode CommitDate, string CommitResult,
                            string Notes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryOrderHistoryFilter"));

            return (DB_DELIVERY_ORDER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeliveryOrderHistoryFilter",
                new DB.AnalyzeDataSet(GetDeliveryOrderHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DELIVERY_ORDER_HISTORY", IdDeliveryOrderHistory),
                    CreateTableParam("@ID_DELIVERY_ORDER", IdDeliveryOrder),
                    CreateTableParam("@ID_OPERATOR", IdOperator),
                    new DB.InParameter("@IS_SUCCESFULL", IsSuccesfull),
                    CreateTableParam("@COMMIT_DATE_CODE", CommitDate),
                    new DB.InParameter("@COMMIT_RESULT", CommitResult),
                    new DB.InParameter("@NOTES", Notes),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceConnectionFilter

        public DB_DEVICE_CONNECTION[] GetDeviceConnectionFilter(long[] IdDeviceConnection, long[] SerialNbr, long[] SerialNbrParent, TypeDateTimeCode Time, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceConnectionFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDeviceConnection != null && IdDeviceConnection.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (SerialNbrParent != null && SerialNbrParent.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDeviceConnection != null) args.AddRange(IdDeviceConnection.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (SerialNbrParent != null) args.AddRange(SerialNbrParent.Select(s => new Tuple<int, object>(3, s)));

                DB_DEVICE_CONNECTION[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE_CONNECTION[])DB.ExecuteProcedure(
                        "imrse_GetDeviceConnectionFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDeviceConnection),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIME_CODE", Time),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_DEVICE_CONNECTION[])DB.ExecuteProcedure(
                    "imrse_GetDeviceConnectionFilter",
                    new DB.AnalyzeDataSet(GetDeviceConnection),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DEVICE_CONNECTION", IdDeviceConnection),
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@SERIAL_NBR_PARENT", SerialNbrParent),
                CreateTableParam("@TIME_CODE", Time),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDeviceConnectionHourlyFilter
        private object GetTupleOflonglonglong(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            Tuple<long, long, long>[] list = new Tuple<long, long, long>[count];
            for (int i = 0; i < count; i++)
            {
                long Item1 = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                long Item2 = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PARENT"]);
                long Item3 = GetValue<long>(QueryResult.Tables[0].Rows[i]["PACKETS"]);
                Tuple<long, long, long> insert = new Tuple<long, long, long>(Item1, Item2, Item3);
                list[i] = insert;
            }
            return list;
        }

        public Tuple<long, long, long>[] GetDeviceConnectionHourlyFilter(long[] SerialNbr = null, long[] SerialNbrParent = null, DateTime? StartTime = null, DateTime? EndTime = null,
                                                         bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {

            //long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);
            //List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(1, s)));
            //if (SerialNbrParent != null) args.AddRange(SerialNbrParent.Select(s => new Tuple<int, object>(1, s)));
            //InsertImrseProcArg(id, args);

            return (Tuple<long, long, long>[])DB.ExecuteProcedure(
                "imrse_u_GetDeviceConnectionHourlyFilter",
                new DB.AnalyzeDataSet(GetTupleOflonglonglong),
                new DB.Parameter[] {
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@SERIAL_NBR_PARENT", SerialNbrParent),
                    new DB.InParameter("@START_TIME", StartTime ),
                    new DB.InParameter("@END_TIME", EndTime )
                },
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDeviceScheduleFilter

        public DB_DEVICE_SCHEDULE[] GetDeviceScheduleFilter(int[] IdDeviceSchedule, long[] SerialNbr, int[] DayOfMonth, int[] DayOfWeek, int[] HourOfDay, int[] MinuteOfHour,
                            int[] CommandCode, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceScheduleFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDeviceSchedule != null && IdDeviceSchedule.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (DayOfMonth != null && DayOfMonth.Length > MaxItemsPerSqlTableType) ||
                (DayOfWeek != null && DayOfWeek.Length > MaxItemsPerSqlTableType) ||
                (HourOfDay != null && HourOfDay.Length > MaxItemsPerSqlTableType) ||
                (MinuteOfHour != null && MinuteOfHour.Length > MaxItemsPerSqlTableType) ||
                (CommandCode != null && CommandCode.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDeviceSchedule != null) args.AddRange(IdDeviceSchedule.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (DayOfMonth != null) args.AddRange(DayOfMonth.Select(s => new Tuple<int, object>(3, s)));
                if (DayOfWeek != null) args.AddRange(DayOfWeek.Select(s => new Tuple<int, object>(4, s)));
                if (HourOfDay != null) args.AddRange(HourOfDay.Select(s => new Tuple<int, object>(5, s)));
                if (MinuteOfHour != null) args.AddRange(MinuteOfHour.Select(s => new Tuple<int, object>(6, s)));
                if (CommandCode != null) args.AddRange(CommandCode.Select(s => new Tuple<int, object>(7, s)));

                DB_DEVICE_SCHEDULE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE_SCHEDULE[])DB.ExecuteProcedure(
                        "imrse_GetDeviceScheduleFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDeviceSchedule),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DEVICE_SCHEDULE[])DB.ExecuteProcedure(
                    "imrse_GetDeviceScheduleFilter",
                    new DB.AnalyzeDataSet(GetDeviceSchedule),
                    new DB.Parameter[] {
                CreateTableParam("@ID_DEVICE_SCHEDULE", IdDeviceSchedule),
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@DAY_OF_MONTH", DayOfMonth),
                CreateTableParam("@DAY_OF_WEEK", DayOfWeek),
                CreateTableParam("@HOUR_OF_DAY", HourOfDay),
                CreateTableParam("@MINUTE_OF_HOUR", MinuteOfHour),
                CreateTableParam("@COMMAND_CODE", CommandCode),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetDistributorPerformanceFilter

        public DB_DISTRIBUTOR_PERFORMANCE[] GetDistributorPerformanceFilter(long[] IdDistributorPerformance, int[] IdDistributor, int[] IdAggregationType, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorPerformanceFilter"));

            return (DB_DISTRIBUTOR_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetDistributorPerformanceFilter",
                new DB.AnalyzeDataSet(GetDistributorPerformance),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DISTRIBUTOR_PERFORMANCE", IdDistributorPerformance),
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetEtlFilterDW

        //public DW.DB_ETL[] GetEtlFilterDW(int[] IdParam, string Code, string Descr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        public DB_ETL[] GetEtlFilterDW(int[] IdParam, string Code, string Descr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdParam != null && IdParam.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdParam != null) args.AddRange(IdParam.Select(s => new Tuple<int, object>(1, s)));

                //DW.DB_ETL[] result = null;
                DB_ETL[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    //result = (DW.DB_ETL[])DB.ExecuteProcedure(
                    result = (DB_ETL[])DB.ExecuteProcedure(
                        "imrse_GetEtlFilterTableArgs",
                        new DB.AnalyzeDataSet(GetEtlDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@CODE", Code),
                    new DB.InParameter("@DESCR", Descr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                //return (DW.DB_ETL[])DB.ExecuteProcedure(
                return (DB_ETL[])DB.ExecuteProcedure(
                    "imrse_GetEtlFilter",
                    new DB.AnalyzeDataSet(GetEtlDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_PARAM", IdParam),
                new DB.InParameter("@CODE", Code),
                new DB.InParameter("@DESCR", Descr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetEtlPerformanceFilter

        public DB_ETL_PERFORMANCE[] GetEtlPerformanceFilter(long[] IdEtlPerformance, int[] IdEtl, int[] IdAggregationType, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, int[] IdDataSourceType, string Code, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlPerformanceFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdEtlPerformance != null && IdEtlPerformance.Length > MaxItemsPerSqlTableType) ||
                (IdEtl != null && IdEtl.Length > MaxItemsPerSqlTableType) ||
                (IdAggregationType != null && IdAggregationType.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdEtlPerformance != null) args.AddRange(IdEtlPerformance.Select(s => new Tuple<int, object>(1, s)));
                if (IdEtl != null) args.AddRange(IdEtl.Select(s => new Tuple<int, object>(2, s)));
                if (IdAggregationType != null) args.AddRange(IdAggregationType.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(4, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(5, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(8, s)));

                DB_ETL_PERFORMANCE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ETL_PERFORMANCE[])DB.ExecuteProcedure(
                        "imrse_GetEtlPerformanceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetEtlPerformance),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@CODE", Code),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_ETL_PERFORMANCE[])DB.ExecuteProcedure(
                    "imrse_GetEtlPerformanceFilter",
                    new DB.AnalyzeDataSet(GetEtlPerformance),
                    new DB.Parameter[] {
                CreateTableParam("@ID_ETL_PERFORMANCE", IdEtlPerformance),
                CreateTableParam("@ID_ETL", IdEtl),
                CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
                new DB.InParameter("@CODE", Code),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetImrServerPerformanceFilter

        public DB_IMR_SERVER_PERFORMANCE[] GetImrServerPerformanceFilter(long[] IdImrServerPerformance, int[] IdImrServer, int[] IdAggregationType, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerPerformanceFilter"));

            return (DB_IMR_SERVER_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetImrServerPerformanceFilter",
                new DB.AnalyzeDataSet(GetImrServerPerformance),
                new DB.Parameter[] {
                            CreateTableParam("@ID_IMR_SERVER_PERFORMANCE", IdImrServerPerformance),
                            CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                            CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                            CreateTableParam("@ID_DATA_TYPE", IdDataType),
                            CreateTableParam("@INDEX_NBR", IndexNbr),
                            CreateTableParam("@START_TIME_CODE", StartTime),
                            CreateTableParam("@END_TIME_CODE", EndTime),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetLocationKpiFilter
        public DW.DB_LOCATION_KPI[] GetLocationKpiFilter(long[] IdLocationKpi, int[] IdDistributor, TypeDateTimeCode Date,
                            int[] New, int[] Operational, int[] Suspended, int[] Pending,
                            int[] KpiRelevant, int[] KpiIrrelevant, long[] KpiConforming, long[] KpiNonConforming, int[] SrtConforming, bool? IsMaintenanceOnly,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationKpiFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLocationKpi != null && IdLocationKpi.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (KpiRelevant != null && KpiRelevant.Length > MaxItemsPerSqlTableType) ||
                (KpiConforming != null && KpiConforming.Length > MaxItemsPerSqlTableType) ||
                (SrtConforming != null && SrtConforming.Length > MaxItemsPerSqlTableType) ||
                (KpiNonConforming != null && KpiNonConforming.Length > MaxItemsPerSqlTableType) ||
                (KpiIrrelevant != null && KpiIrrelevant.Length > MaxItemsPerSqlTableType) ||
                (New != null && New.Length > MaxItemsPerSqlTableType) ||
                (Pending != null && Pending.Length > MaxItemsPerSqlTableType) ||
                (Operational != null && Operational.Length > MaxItemsPerSqlTableType) ||
                (Suspended != null && Suspended.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocationKpi != null) args.AddRange(IdLocationKpi.Select(s => new Tuple<int, object>(1, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(2, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(2, s)));
                if (KpiRelevant != null) args.AddRange(KpiRelevant.Select(s => new Tuple<int, object>(5, s)));
                if (KpiConforming != null) args.AddRange(KpiConforming.Select(s => new Tuple<int, object>(6, s)));
                if (SrtConforming != null) args.AddRange(SrtConforming.Select(s => new Tuple<int, object>(8, s)));
                if (KpiNonConforming != null) args.AddRange(KpiNonConforming.Select(s => new Tuple<int, object>(9, s)));
                if (KpiIrrelevant != null) args.AddRange(KpiIrrelevant.Select(s => new Tuple<int, object>(10, s)));
                if (New != null) args.AddRange(New.Select(s => new Tuple<int, object>(11, s)));
                if (Pending != null) args.AddRange(Pending.Select(s => new Tuple<int, object>(12, s)));
                if (Operational != null) args.AddRange(Operational.Select(s => new Tuple<int, object>(13, s)));
                if (Suspended != null) args.AddRange(Suspended.Select(s => new Tuple<int, object>(14, s)));

                DW.DB_LOCATION_KPI[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_LOCATION_KPI[])DB.ExecuteProcedure(
                        "imrse_GetLocationKpiFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationKpi),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@DATE_CODE", Date),
                            new DB.InParameter("@IS_MAINTENANCE_ONLY", IsMaintenanceOnly),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DW.DB_LOCATION_KPI[])DB.ExecuteProcedure(
                    "imrse_GetLocationKpiFilter",
                    new DB.AnalyzeDataSet(GetLocationKpi),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION_KPI", IdLocationKpi),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter),
                        CreateTableParam("@DATE_CODE", Date),
                        CreateTableParam("@KPI_RELEVANT", KpiRelevant),
                        CreateTableParam("@KPI_CONFORMING", KpiConforming),
                        new DB.InParameter("@IS_MAINTENANCE_ONLY", IsMaintenanceOnly),
                        CreateTableParam("@SRT_CONFORMING", SrtConforming),
                        CreateTableParam("@KPI_NONCONFORMING", KpiNonConforming),
                        CreateTableParam("@KPI_IRRELEVANT", KpiIrrelevant),
                        CreateTableParam("@NEW", New),
                        CreateTableParam("@PENDING", Pending),
                        CreateTableParam("@OPERATIONAL", Operational),
                        CreateTableParam("@SUSPENDED", Suspended),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetLocationDistanceFilter
        public DB_LOCATION_DISTANCE[] GetLocationDistanceFilter(long[] IdLocationOrigin, long[] IdLocationDest, Double[] DistanceStraight, Double[] DistanceFormula, Double[] DistanceGoogle, TypeDateTimeCode LastGoogleUpdate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationDistanceFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdLocationOrigin != null && IdLocationOrigin.Length > MaxItemsPerSqlTableType) ||
                (IdLocationDest != null && IdLocationDest.Length > MaxItemsPerSqlTableType) ||
                (DistanceStraight != null && DistanceStraight.Length > MaxItemsPerSqlTableType) ||
                (DistanceFormula != null && DistanceFormula.Length > MaxItemsPerSqlTableType) ||
                (DistanceGoogle != null && DistanceGoogle.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocationOrigin != null) args.AddRange(IdLocationOrigin.Select(s => new Tuple<int, object>(1, s)));
                if (IdLocationDest != null) args.AddRange(IdLocationDest.Select(s => new Tuple<int, object>(2, s)));
                if (DistanceStraight != null) args.AddRange(DistanceStraight.Select(s => new Tuple<int, object>(3, s)));
                if (DistanceFormula != null) args.AddRange(DistanceFormula.Select(s => new Tuple<int, object>(4, s)));
                if (DistanceGoogle != null) args.AddRange(DistanceGoogle.Select(s => new Tuple<int, object>(5, s)));

                DB_LOCATION_DISTANCE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_LOCATION_DISTANCE[])DB.ExecuteProcedure(
                        "imrse_GetLocationDistanceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationDistance),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@LAST_GOOGLE_UPDATE_CODE", LastGoogleUpdate),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_LOCATION_DISTANCE[])DB.ExecuteProcedure(
                    "imrse_GetLocationDistanceFilter",
                    new DB.AnalyzeDataSet(GetLocationDistance),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_LOCATION_ORIGIN", IdLocationOrigin),
                        CreateTableParam("@ID_LOCATION_DEST", IdLocationDest),
                        CreateTableParam("@DISTANCE_STRAIGHT", DistanceStraight),
                        CreateTableParam("@DISTANCE_FORMULA", DistanceFormula),
                        CreateTableParam("@DISTANCE_GOOGLE", DistanceGoogle),
                        CreateTableParam("@LAST_GOOGLE_UPDATE_CODE", LastGoogleUpdate),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMappingFilter

        public DW.DB_MAPPING[] GetMappingFilterDW(long[] IdMapping, int[] IdReferenceType, long[] IdSource, long[] IdDestination, int[] IdImrServer, TypeDateTimeCode Timestamp,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMappingFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMapping != null && IdMapping.Length > MaxItemsPerSqlTableType) ||
                (IdReferenceType != null && IdReferenceType.Length > MaxItemsPerSqlTableType) ||
                (IdSource != null && IdSource.Length > MaxItemsPerSqlTableType) ||
                (IdDestination != null && IdDestination.Length > MaxItemsPerSqlTableType) ||
                (IdImrServer != null && IdImrServer.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMapping != null) args.AddRange(IdMapping.Select(s => new Tuple<int, object>(1, s)));
                if (IdReferenceType != null) args.AddRange(IdReferenceType.Select(s => new Tuple<int, object>(2, s)));
                if (IdSource != null) args.AddRange(IdSource.Select(s => new Tuple<int, object>(3, s)));
                if (IdDestination != null) args.AddRange(IdDestination.Select(s => new Tuple<int, object>(4, s)));
                if (IdImrServer != null) args.AddRange(IdImrServer.Select(s => new Tuple<int, object>(5, s)));

                DW.DB_MAPPING[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_MAPPING[])DB.ExecuteProcedure(
                        "imrse_GetMappingFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMappingDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@TIMESTAMP_CODE", Timestamp),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DW.DB_MAPPING[])DB.ExecuteProcedure(
                    "imrse_GetMappingFilter",
                    new DB.AnalyzeDataSet(GetMappingDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MAPPING", IdMapping),
                CreateTableParam("@ID_REFERENCE_TYPE", IdReferenceType),
                CreateTableParam("@ID_SOURCE", IdSource),
                CreateTableParam("@ID_DESTINATION", IdDestination),
                CreateTableParam("@ID_IMR_SERVER", IdImrServer),
                CreateTableParam("@TIMESTAMP_CODE", Timestamp),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetModuleHistoryFilter

        public DB_MODULE_HISTORY[] GetModuleHistoryFilter(int[] IdModuleHistory, int[] IdModule, string CommonName, string Version, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                            int[] IdOperatorUpgrade, int[] IdOperatorAccept, string UpgradeDescription, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleHistoryFilter"));

            return (DB_MODULE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetModuleHistoryFilter",
                new DB.AnalyzeDataSet(GetModuleHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_MODULE_HISTORY", IdModuleHistory),
                    CreateTableParam("@ID_MODULE", IdModule),
                    new DB.InParameter("@COMMON_NAME", CommonName),
                    new DB.InParameter("@VERSION", Version),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    CreateTableParam("@ID_OPERATOR_UPGRADE", IdOperatorUpgrade),
                    CreateTableParam("@ID_OPERATOR_ACCEPT", IdOperatorAccept),
                    new DB.InParameter("@UPGRADE_DESCRIPTION", UpgradeDescription),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetMonitoringWatcherFilterDW

        public DW.DB_MONITORING_WATCHER[] GetMonitoringWatcherFilterDW(int[] IdMonitoringWatcher, int[] IdMonitoringWatcherType, bool? IsActive, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcher != null && IdMonitoringWatcher.Length > MaxItemsPerSqlTableType) ||
                (IdMonitoringWatcherType != null && IdMonitoringWatcherType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcher != null) args.AddRange(IdMonitoringWatcher.Select(s => new Tuple<int, object>(1, s)));
                if (IdMonitoringWatcherType != null) args.AddRange(IdMonitoringWatcherType.Select(s => new Tuple<int, object>(2, s)));

                DW.DB_MONITORING_WATCHER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcherDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@IS_ACTIVE", IsActive),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DW.DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcherDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER", IdMonitoringWatcher),
                CreateTableParam("@ID_MONITORING_WATCHER_TYPE", IdMonitoringWatcherType),
                new DB.InParameter("@IS_ACTIVE", IsActive),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMonitoringWatcherDataFilterDW

        public DW.DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherDataFilterDW(int[] IdMonitoringWatcherData, int[] IdMonitoringWatcher, long[] IdDataType, int[] Index, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcherData != null && IdMonitoringWatcherData.Length > MaxItemsPerSqlTableType) ||
                (IdMonitoringWatcher != null && IdMonitoringWatcher.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (Index != null && Index.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcherData != null) args.AddRange(IdMonitoringWatcherData.Select(s => new Tuple<int, object>(1, s)));
                if (IdMonitoringWatcher != null) args.AddRange(IdMonitoringWatcher.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (Index != null) args.AddRange(Index.Select(s => new Tuple<int, object>(4, s)));

                DW.DB_MONITORING_WATCHER_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcherDataDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DW.DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherDataFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcherDataDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER_DATA", IdMonitoringWatcherData),
                CreateTableParam("@ID_MONITORING_WATCHER", IdMonitoringWatcher),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX", Index),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetMonitoringWatcherTypeFilterDW

        public DW.DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherTypeFilterDW(int[] IdMonitoringWatcherType, string Name, string Plugin, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdMonitoringWatcherType != null && IdMonitoringWatcherType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdMonitoringWatcherType != null) args.AddRange(IdMonitoringWatcherType.Select(s => new Tuple<int, object>(1, s)));

                DW.DB_MONITORING_WATCHER_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DW.DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetMonitoringWatcherTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetMonitoringWatcherTypeDW),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@NAME", Name),
                    new DB.InParameter("@PLUGIN", Plugin),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DW.DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetMonitoringWatcherTypeFilter",
                    new DB.AnalyzeDataSet(GetMonitoringWatcherTypeDW),
                    new DB.Parameter[] {
                CreateTableParam("@ID_MONITORING_WATCHER_TYPE", IdMonitoringWatcherType),
                new DB.InParameter("@NAME", Name),
                new DB.InParameter("@PLUGIN", Plugin),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetProblemClassFilter

        public DB_PROBLEM_CLASS[] GetProblemClassFilter(int[] IdProblemClass, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProblemClassFilter"));

            return (DB_PROBLEM_CLASS[])DB.ExecuteProcedure(
                "imrse_GetProblemClassFilter",
                new DB.AnalyzeDataSet(GetProblemClass),
                new DB.Parameter[] {
                    CreateTableParam("@ID_PROBLEM_CLASS", IdProblemClass),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRefuelDataFilter
        public DB_REFUEL_DATA[] GetRefuelDataFilter(long[] IdRefuelData, long[] IdRefuel, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                            int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRefuelDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdRefuelData != null && IdRefuelData.Length > MaxItemsPerSqlTableType) ||
                (IdRefuel != null && IdRefuel.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (Status != null && Status.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdRefuelData != null) args.AddRange(IdRefuelData.Select(s => new Tuple<int, object>(1, s)));
                if (IdRefuel != null) args.AddRange(IdRefuel.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(7, s)));

                DB_REFUEL_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_REFUEL_DATA[])DB.ExecuteProcedure(
                        "imrse_GetRefuelDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetRefuelData),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_TIME_CODE", StartTime),
                            CreateTableParam("@END_TIME_CODE", EndTime),
                            new DB.InParameter("@TOP_COUNT", topCount),
                            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_REFUEL_DATA[])DB.ExecuteProcedure(
                    "imrse_GetRefuelDataFilter",
                    new DB.AnalyzeDataSet(GetRefuelData),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_REFUEL_DATA", IdRefuelData),
                        CreateTableParam("@ID_REFUEL", IdRefuel),
                        CreateTableParam("@ID_DATA_TYPE", IdDataType),
                        CreateTableParam("@INDEX_NBR", IndexNbr),
                        CreateTableParam("@START_TIME_CODE", StartTime),
                        CreateTableParam("@END_TIME_CODE", EndTime),
                        CreateTableParam("@STATUS", Status),
                        new DB.InParameter("@TOP_COUNT", topCount),
                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetReportFilter

        public DB_REPORT[] GetReportFilter(int[] IdReport, int[] IdReportType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportFilter"));

            return (DB_REPORT[])DB.ExecuteProcedure(
                "imrse_GetReportFilter",
                new DB.AnalyzeDataSet(GetReport),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT", IdReport),
                    CreateTableParam("@ID_REPORT_TYPE", IdReportType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReportDataFilter

        public DB_REPORT_DATA[] GetReportDataFilter(long[] IdReportData, int[] IdReport, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportDataFilter"));

            return (DB_REPORT_DATA[])DB.ExecuteProcedure(
                "imrse_GetReportDataFilter",
                new DB.AnalyzeDataSet(GetReportData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT_DATA", IdReportData),
                    CreateTableParam("@ID_REPORT", IdReport),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReportDataTypeFilter

        public DB_REPORT_DATA_TYPE[] GetReportDataTypeFilter(long[] IdReportDataType, string Signature, long[] IdDataType, int[] IdUnit, int[] DigitsAfterComma, int[] SequenceNbr,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportDataTypeFilter"));

            return (DB_REPORT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportDataTypeFilter",
                new DB.AnalyzeDataSet(GetReportDataType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT_DATA_TYPE", IdReportDataType),
                    new DB.InParameter("@SIGNATURE", Signature),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@ID_UNIT", IdUnit),
                    CreateTableParam("@DIGITS_AFTER_COMMA", DigitsAfterComma),
                    CreateTableParam("@SEQUENCE_NBR", SequenceNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReportParamFilter

        public DB_REPORT_PARAM[] GetReportParamFilter(int[] IdReportParam, int[] IdReport, string FieldName, long[] IdDescr, int[] IdUnit, string Name,
                            int[] IdReference, int[] IdDataTypeClass, bool? IsInputParam, bool? IsVisible, string ColorBg,
                            string Format, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportParamFilter"));

            return (DB_REPORT_PARAM[])DB.ExecuteProcedure(
                "imrse_GetReportParamFilter",
                new DB.AnalyzeDataSet(GetReportParam),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT_PARAM", IdReportParam),
                    CreateTableParam("@ID_REPORT", IdReport),
                    new DB.InParameter("@FIELD_NAME", FieldName),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    CreateTableParam("@ID_UNIT", IdUnit),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_REFERENCE", IdReference),
                    CreateTableParam("@ID_DATA_TYPE_CLASS", IdDataTypeClass),
                    new DB.InParameter("@IS_INPUT_PARAM", IsInputParam),
                    new DB.InParameter("@IS_VISIBLE", IsVisible),
                    new DB.InParameter("@COLOR_BG", ColorBg),
                    new DB.InParameter("@FORMAT", Format),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReportPerformanceFilter

        public DB_REPORT_PERFORMANCE[] GetReportPerformanceFilter(long[] IdReportPerformance, int[] IdReport, int[] IdAggregationType, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, int[] IdDataSourceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportPerformanceFilter"));

            return (DB_REPORT_PERFORMANCE[])DB.ExecuteProcedure(
                "imrse_GetReportPerformanceFilter",
                new DB.AnalyzeDataSet(GetReportPerformance),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT_PERFORMANCE", IdReportPerformance),
                    CreateTableParam("@ID_REPORT", IdReport),
                    CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetReportTypeFilter

        public DB_REPORT_TYPE[] GetReportTypeFilter(int[] IdReportType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportTypeFilter"));

            return (DB_REPORT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReportTypeFilter",
                new DB.AnalyzeDataSet(GetReportType),
                new DB.Parameter[] {
                    CreateTableParam("@ID_REPORT_TYPE", IdReportType),
                    new DB.InParameter("@NAME", Name),
                    CreateTableParam("@ID_DESCR", IdDescr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetSystemDataFilter

        public DB_SYSTEM_DATA[] GetSystemDataFilter(long[] IdSystemData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));

            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemDataFilter",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_SYSTEM_DATA", IdSystemData),
                    CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataMissingAnalogReadoutsFilter

        public DB_DATA_MISSING_ANALOG_READOUTS[] GetDataMissingAnalogReadoutsFilter(long[] IdDataMissingAnalogReadouts, long[] SerialNbr, long[] SerialNbrParent, TypeDateTimeCode MissingDataStartTime, int[] PacketsCount, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataMissingAnalogReadoutsFilter"));

            return (DB_DATA_MISSING_ANALOG_READOUTS[])DB.ExecuteProcedure(
                "imrse_GetDataMissingAnalogReadoutsFilter",
                new DB.AnalyzeDataSet(GetDataMissingAnalogReadouts),
                new DB.Parameter[] {
                    CreateTableParam("@ID_DATA_MISSING_ANALOG_READOUTS", IdDataMissingAnalogReadouts),
                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                    CreateTableParam("@SERIAL_NBR_PARENT", SerialNbrParent),
                    CreateTableParam("@MISSING_DATA_START_TIME_CODE", MissingDataStartTime),
                    CreateTableParam("@PACKETS_COUNT", PacketsCount),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPacketHourDeviceFilter

        public DB_PACKET_HOUR_DEVICE[] GetPacketHourDeviceFilter(long[] SerialNbr, int[] IdTransmissionDriver, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourDeviceFilter"));

            if (UseBulkInsertSqlTableType && (
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(1, s)));
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(2, s)));
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(5, s)));

                DB_PACKET_HOUR_DEVICE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_HOUR_DEVICE[])DB.ExecuteProcedure(
                        "imrse_GetPacketHourDeviceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketHourDevice),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_PACKET_HOUR_DEVICE[])DB.ExecuteProcedure(
                    "imrse_GetPacketHourDeviceFilter",
                    new DB.AnalyzeDataSet(GetPacketHourDevice),
                    new DB.Parameter[] {
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPacketHourAddressFilter

        public DB_PACKET_HOUR_ADDRESS[] GetPacketHourAddressFilter(string Address, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourAddressFilter"));

            if (UseBulkInsertSqlTableType && (
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(4, s)));

                DB_PACKET_HOUR_ADDRESS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_HOUR_ADDRESS[])DB.ExecuteProcedure(
                        "imrse_GetPacketHourAddressFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketHourAddress),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@ADDRESS", Address),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_PACKET_HOUR_ADDRESS[])DB.ExecuteProcedure(
                    "imrse_GetPacketHourAddressFilter",
                    new DB.AnalyzeDataSet(GetPacketHourAddress),
                    new DB.Parameter[] {
                new DB.InParameter("@ADDRESS", Address),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPacketHourTransmissionDriverFilter

        public DB_PACKET_HOUR_TRANSMISSION_DRIVER[] GetPacketHourTransmissionDriverFilter(int[] IdTransmissionDriver, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, int[] IdTransmissionType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourTransmissionDriverFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionType != null && IdTransmissionType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(1, s)));
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(4, s)));
                if (IdTransmissionType != null) args.AddRange(IdTransmissionType.Select(s => new Tuple<int, object>(5, s)));

                DB_PACKET_HOUR_TRANSMISSION_DRIVER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                        "imrse_GetPacketHourTransmissionDriverFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketHourTransmissionDriver),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_PACKET_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                    "imrse_GetPacketHourTransmissionDriverFilter",
                    new DB.AnalyzeDataSet(GetPacketHourTransmissionDriver),
                    new DB.Parameter[] {
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPacketTrashHourAddressFilter

        public DB_PACKET_TRASH_HOUR_ADDRESS[] GetPacketTrashHourAddressFilter(string Address, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourAddressFilter"));

            if (UseBulkInsertSqlTableType && (
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(4, s)));

                DB_PACKET_TRASH_HOUR_ADDRESS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_TRASH_HOUR_ADDRESS[])DB.ExecuteProcedure(
                        "imrse_GetPacketTrashHourAddressFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketTrashHourAddress),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@ADDRESS", Address),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PACKET_TRASH_HOUR_ADDRESS[])DB.ExecuteProcedure(
                    "imrse_GetPacketTrashHourAddressFilter",
                    new DB.AnalyzeDataSet(GetPacketTrashHourAddress),
                    new DB.Parameter[] {
                new DB.InParameter("@ADDRESS", Address),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPacketTrashHourDeviceFilter

        public DB_PACKET_TRASH_HOUR_DEVICE[] GetPacketTrashHourDeviceFilter(long[] SerialNbr, int[] IdTransmissionDriver, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourDeviceFilter"));

            if (UseBulkInsertSqlTableType && (
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(1, s)));
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(2, s)));
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(5, s)));

                DB_PACKET_TRASH_HOUR_DEVICE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_TRASH_HOUR_DEVICE[])DB.ExecuteProcedure(
                        "imrse_GetPacketTrashHourDeviceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketTrashHourDevice),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PACKET_TRASH_HOUR_DEVICE[])DB.ExecuteProcedure(
                    "imrse_GetPacketTrashHourDeviceFilter",
                    new DB.AnalyzeDataSet(GetPacketTrashHourDevice),
                    new DB.Parameter[] {
                CreateTableParam("@SERIAL_NBR", SerialNbr),
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetPacketTrashHourTransmissionDriverFilter

        public DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] GetPacketTrashHourTransmissionDriverFilter(int[] IdTransmissionDriver, TypeDateTimeCode Date, bool? IsIncoming, int[] Packets, int[] IdTransmissionType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourTransmissionDriverFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (Packets != null && Packets.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionType != null && IdTransmissionType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(1, s)));
                if (Packets != null) args.AddRange(Packets.Select(s => new Tuple<int, object>(4, s)));
                if (IdTransmissionType != null) args.AddRange(IdTransmissionType.Select(s => new Tuple<int, object>(5, s)));

                DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                        "imrse_GetPacketTrashHourTransmissionDriverFilterTableArgs",
                        new DB.AnalyzeDataSet(GetPacketTrashHourTransmissionDriver),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@DATE_CODE", Date),
                    new DB.InParameter("@IS_INCOMING", IsIncoming),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                    "imrse_GetPacketTrashHourTransmissionDriverFilter",
                    new DB.AnalyzeDataSet(GetPacketTrashHourTransmissionDriver),
                    new DB.Parameter[] {
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@DATE_CODE", Date),
                new DB.InParameter("@IS_INCOMING", IsIncoming),
                CreateTableParam("@PACKETS", Packets),
                CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetTemperatureCoefficientFilter
        public DB_TEMPERATURE_COEFFICIENT[] GetTemperatureCoefficientFilter(long[] IdTemperatureCoefficient, Double[] Temperature, Double[] Density, Double[] Value, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTemperatureCoefficientFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTemperatureCoefficient != null && IdTemperatureCoefficient.Length > MaxItemsPerSqlTableType) ||
                (Temperature != null && Temperature.Length > MaxItemsPerSqlTableType) ||
                (Density != null && Density.Length > MaxItemsPerSqlTableType) ||
                (Value != null && Value.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTemperatureCoefficient != null) args.AddRange(IdTemperatureCoefficient.Select(s => new Tuple<int, object>(1, s)));
                if (Temperature != null) args.AddRange(Temperature.Select(s => new Tuple<int, object>(2, s)));
                if (Density != null) args.AddRange(Density.Select(s => new Tuple<int, object>(3, s)));
                if (Value != null) args.AddRange(Value.Select(s => new Tuple<int, object>(4, s)));

                DB_TEMPERATURE_COEFFICIENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TEMPERATURE_COEFFICIENT[])DB.ExecuteProcedure(
                        "imrse_GetTemperatureCoefficientFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTemperatureCoefficient),
                        new DB.Parameter[] { 
					        new DB.InParameter("@ID", SqlDbType.BigInt, id),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_TEMPERATURE_COEFFICIENT[])DB.ExecuteProcedure(
                    "imrse_GetTemperatureCoefficientFilter",
                    new DB.AnalyzeDataSet(GetTemperatureCoefficient),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_TEMPERATURE_COEFFICIENT", IdTemperatureCoefficient),
				        CreateTableParam("@TEMPERATURE", Temperature),
				        CreateTableParam("@DENSITY", Density),
				        CreateTableParam("@VALUE", Value),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetTransmissionDriverDataFilter

        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverDataFilter(long[] IdTransmissionDriverData, int[] IdTransmissionDriver, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTransmissionDriverData != null && IdTransmissionDriverData.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTransmissionDriverData != null) args.AddRange(IdTransmissionDriverData.Select(s => new Tuple<int, object>(1, s)));
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_TRANSMISSION_DRIVER_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                        "imrse_GetTransmissionDriverDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTransmissionDriverData),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                    "imrse_GetTransmissionDriverDataFilter",
                    new DB.AnalyzeDataSet(GetTransmissionDriverData),
                    new DB.Parameter[] {
                CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", IdTransmissionDriverData),
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetTransmissionDriverPerformanceFilter

        public DB_TRANSMISSION_DRIVER_PERFORMANCE[] GetTransmissionDriverPerformanceFilter(long[] IdTransmissionDriverPerformance, int[] IdTransmissionDriver, int[] IdAggregationType, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode StartTime,
                            TypeDateTimeCode EndTime, int[] IdDataSourceType, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverPerformanceFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdTransmissionDriverPerformance != null && IdTransmissionDriverPerformance.Length > MaxItemsPerSqlTableType) ||
                (IdTransmissionDriver != null && IdTransmissionDriver.Length > MaxItemsPerSqlTableType) ||
                (IdAggregationType != null && IdAggregationType.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) ||
                (IdDataSourceType != null && IdDataSourceType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdTransmissionDriverPerformance != null) args.AddRange(IdTransmissionDriverPerformance.Select(s => new Tuple<int, object>(1, s)));
                if (IdTransmissionDriver != null) args.AddRange(IdTransmissionDriver.Select(s => new Tuple<int, object>(2, s)));
                if (IdAggregationType != null) args.AddRange(IdAggregationType.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(4, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(5, s)));
                if (IdDataSourceType != null) args.AddRange(IdDataSourceType.Select(s => new Tuple<int, object>(8, s)));

                DB_TRANSMISSION_DRIVER_PERFORMANCE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_TRANSMISSION_DRIVER_PERFORMANCE[])DB.ExecuteProcedure(
                        "imrse_GetTransmissionDriverPerformanceFilterTableArgs",
                        new DB.AnalyzeDataSet(GetTransmissionDriverPerformance),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@START_TIME_CODE", StartTime),
                    CreateTableParam("@END_TIME_CODE", EndTime),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_TRANSMISSION_DRIVER_PERFORMANCE[])DB.ExecuteProcedure(
                    "imrse_GetTransmissionDriverPerformanceFilter",
                    new DB.AnalyzeDataSet(GetTransmissionDriverPerformance),
                    new DB.Parameter[] {
                CreateTableParam("@ID_TRANSMISSION_DRIVER_PERFORMANCE", IdTransmissionDriverPerformance),
                CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
                CreateTableParam("@ID_AGGREGATION_TYPE", IdAggregationType),
                CreateTableParam("@ID_DATA_TYPE", IdDataType),
                CreateTableParam("@INDEX_NBR", IndexNbr),
                CreateTableParam("@START_TIME_CODE", StartTime),
                CreateTableParam("@END_TIME_CODE", EndTime),
                CreateTableParam("@ID_DATA_SOURCE_TYPE", IdDataSourceType),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
    }
}
