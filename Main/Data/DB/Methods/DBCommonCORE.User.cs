﻿//#define TRIADA_DATA_CONCURRENT_BAG // wylaczone!
using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections.Concurrent;
using IMR.Suite.Common;
using System.Globalization;
using System.Text;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonCORE : DBCommon
    {
        #region Constructor
        public DBCommonCORE(string serverName)
            : base(serverName)
        {
        }
        public DBCommonCORE(string serverName, int shortTimeout, int longTimeout)
            : base(serverName, shortTimeout, longTimeout)
        {
        }
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(10), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, TimeSpan? timeZoneOffset, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, timeZoneOffset, shortTimeout, longTimeout, TimeSpan.FromSeconds(10), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime)
        {
        }
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, null)
        {
        }
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, operatorLogin)
        {
        }
        public DBCommonCORE(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(10), 2, TimeSpan.FromSeconds(5))
        {
        }
        public DBCommonCORE(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(10), 2, TimeSpan.FromSeconds(5), operatorLogin)
        {
        }

        public DBCommonCORE(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(10), 2, TimeSpan.FromSeconds(5), useTrustedConnection, operatorLogin)
        {
        }

        //TS: specjalny jałowy konstruktor aby można przekazać informacje o trusted connection w projektach gdzie są wykorzystywane Data/DB oraz Common/DB jednocześnie. Do wywalenia jak będzie zrobiony merge tych projektów
        public DBCommonCORE(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, Enums.Language.English, null, shortTimeout, longTimeout, TimeSpan.Zero, 0, TimeSpan.Zero, useTrustedConnection, TimeZoneId: "UTC")
        {
        }
        #endregion

        #region Shipping_list_devices
        public int CheckSimForDevicesForShippingList(int id_shipping_list)
        {
            return (int)DB.ExecuteScalar("imrse_u_CheckSimForDevicesForShipping", new DB.Parameter[]
            {
                new DB.InParameter("@idShippingList", id_shipping_list)
            });
        }
        #endregion

        #region Table ACTION
        public DB_ACTION[] GetAction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION[])DB.ExecuteProcedure(
                "imrse_u_GetAction",
                new DB.AnalyzeDataSet(GetAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION", new long[] {}),
            CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ACTION[] GetAction(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION[])DB.ExecuteProcedure(
                "imrse_u_GetAction",
                new DB.AnalyzeDataSet(GetAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION", Ids),
            CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ACTION[] GetActionFilter(long[] IdAction, long[] SerialNbr, long[] IdMeter, long[] IdLocation, int[] IdActionType, int[] IdActionStatus,
                            long[] IdActionData, long[] IdActionParent, long[] IdDataArch, int[] IdModule, int[] IdOperator,
                            TypeDateTimeCode CreationDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionFilter"));

            DB_ACTION[] tmpResult = (DB_ACTION[])DB.ExecuteProcedure(
                    "imrse_GetActionFilter",
                    new DB.AnalyzeDataSet(GetAction),
                    new DB.Parameter[] { 
                                    CreateTableParam("@ID_ACTION", IdAction),
                                    CreateTableParam("@SERIAL_NBR", SerialNbr),
                                    CreateTableParam("@ID_METER", IdMeter),
                                    CreateTableParam("@ID_LOCATION", IdLocation),
                                    CreateTableParam("@ID_ACTION_TYPE", IdActionType),
                                    CreateTableParam("@ID_ACTION_STATUS", IdActionStatus),
                                    CreateTableParam("@ID_ACTION_DATA", IdActionData),
                                    CreateTableParam("@ID_ACTION_PARENT", IdActionParent),
                                    CreateTableParam("@ID_DATA_ARCH", IdDataArch),
                                    CreateTableParam("@ID_MODULE", IdModule),
                                    CreateTableParam("@ID_OPERATOR", IdOperator),
                                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                                    new DB.InParameter("@TOP_COUNT", topCount),
                                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );

            return tmpResult != null ? tmpResult.Where(q => 
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_ACTION[0];

            #region // old version
            //return (DB_ACTION[])DB.ExecuteProcedure(
            //    "imrse_GetActionFilter",
            //    new DB.AnalyzeDataSet(GetAction),
            //    new DB.Parameter[] { 
            //        CreateTableParam("@ID_ACTION", IdAction),
            //        CreateTableParam("@SERIAL_NBR", SerialNbr),
            //        CreateTableParam("@ID_METER", IdMeter),
            //        CreateTableParam("@ID_LOCATION", IdLocation),
            //        CreateTableParam("@ID_ACTION_TYPE", IdActionType),
            //        CreateTableParam("@ID_ACTION_STATUS", IdActionStatus),
            //        CreateTableParam("@ID_ACTION_DATA", IdActionData),
            //        CreateTableParam("@ID_ACTION_PARENT", IdActionParent),
            //        CreateTableParam("@ID_DATA_ARCH", IdDataArch),
            //        CreateTableParam("@ID_MODULE", IdModule),
            //        CreateTableParam("@ID_OPERATOR", IdOperator),
            //        CreateTableParam("@CREATION_DATE_CODE", CreationDate),
            //        new DB.InParameter("@TOP_COUNT", topCount),
            //        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //    autoTransaction, transactionLevel, commandTimeout
            //);
            #endregion
        }

        public DB_ACTION[] GetActionFilter(long[] IdAction, long[] SerialNbr, long[] IdMeter, long[] IdLocation, int[] IdActionType, int[] IdActionStatus,
                            long[] IdActionData, long[] IdActionParent, long[] IdDataArch, int[] IdModule, int[] IdOperator, int[] IdDistributor,
                            Data.DB.TypeDateTimeCode StartDateFrom, Data.DB.TypeDateTimeCode StartDateTo,
                            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            DB_ACTION[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdAction != null && IdAction.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdActionType != null && IdActionType.Length > MaxItemsPerSqlTableType) ||
                (IdActionStatus != null && IdActionStatus.Length > MaxItemsPerSqlTableType) ||
                (IdActionData != null && IdActionData.Length > MaxItemsPerSqlTableType) ||
                (IdActionParent != null && IdActionParent.Length > MaxItemsPerSqlTableType) ||
                (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdAction != null) args.AddRange(IdAction.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdActionType != null) args.AddRange(IdActionType.Select(s => new Tuple<int, object>(5, s)));
                if (IdActionStatus != null) args.AddRange(IdActionStatus.Select(s => new Tuple<int, object>(6, s)));
                if (IdActionData != null) args.AddRange(IdActionData.Select(s => new Tuple<int, object>(7, s)));
                if (IdActionParent != null) args.AddRange(IdActionParent.Select(s => new Tuple<int, object>(8, s)));
                if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(9, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(10, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(11, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(12, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(12, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_ACTION[])DB.ExecuteProcedure(
                        "imrse_u_GetActionFilterTableArgs",
                        new DB.AnalyzeDataSet(GetAction),
                        new DB.Parameter[] { 
					        new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_DATE_CODE", StartDateFrom),
                            CreateTableParam("@END_DATE_CODE", StartDateTo)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_ACTION[])DB.ExecuteProcedure(
                    "imrse_u_GetActionFilter",
                    new DB.AnalyzeDataSet(GetAction),
                    new DB.Parameter[] { 
					    CreateTableParam("@ID_ACTION", IdAction),
					    CreateTableParam("@SERIAL_NBR", SerialNbr),
					    CreateTableParam("@ID_METER", IdMeter),
					    CreateTableParam("@ID_LOCATION", IdLocation),
					    CreateTableParam("@ID_ACTION_TYPE", IdActionType),
					    CreateTableParam("@ID_ACTION_STATUS", IdActionStatus),
					    CreateTableParam("@ID_ACTION_DATA", IdActionData),
					    CreateTableParam("@ID_ACTION_PARENT", IdActionParent),
					    CreateTableParam("@ID_DATA_ARCH", IdDataArch),
					    CreateTableParam("@ID_MODULE", IdModule),
					    CreateTableParam("@ID_OPERATOR", IdOperator),
                        CreateTableParam("@START_DATE_CODE", StartDateFrom),
                        CreateTableParam("@END_DATE_CODE", StartDateTo),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor ?? DistributorFilter)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_ACTION[0];

            #region // old version
            //if (UseBulkInsertSqlTableType && (
            //    (IdAction != null && IdAction.Length > MaxItemsPerSqlTableType) ||
            //    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdActionType != null && IdActionType.Length > MaxItemsPerSqlTableType) ||
            //    (IdActionStatus != null && IdActionStatus.Length > MaxItemsPerSqlTableType) ||
            //    (IdActionData != null && IdActionData.Length > MaxItemsPerSqlTableType) ||
            //    (IdActionParent != null && IdActionParent.Length > MaxItemsPerSqlTableType) ||
            //    (IdDataArch != null && IdDataArch.Length > MaxItemsPerSqlTableType) ||
            //    (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType) ||
            //    (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
            //    ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdAction != null) args.AddRange(IdAction.Select(s => new Tuple<int, object>(1, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
            //    if (IdActionType != null) args.AddRange(IdActionType.Select(s => new Tuple<int, object>(5, s)));
            //    if (IdActionStatus != null) args.AddRange(IdActionStatus.Select(s => new Tuple<int, object>(6, s)));
            //    if (IdActionData != null) args.AddRange(IdActionData.Select(s => new Tuple<int, object>(7, s)));
            //    if (IdActionParent != null) args.AddRange(IdActionParent.Select(s => new Tuple<int, object>(8, s)));
            //    if (IdDataArch != null) args.AddRange(IdDataArch.Select(s => new Tuple<int, object>(9, s)));
            //    if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(10, s)));
            //    if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(11, s)));
            //    if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(12, s)));
            //    else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(12, s)));

            //    DB_ACTION[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DB_ACTION[])DB.ExecuteProcedure(
            //            "imrse_u_GetActionFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetAction),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                CreateTableParam("@START_DATE_CODE", StartDateFrom),
            //                CreateTableParam("@END_DATE_CODE", StartDateTo)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DB_ACTION[])DB.ExecuteProcedure(
            //        "imrse_u_GetActionFilter",
            //        new DB.AnalyzeDataSet(GetAction),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_ACTION", IdAction),
            //            CreateTableParam("@SERIAL_NBR", SerialNbr),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_ACTION_TYPE", IdActionType),
            //            CreateTableParam("@ID_ACTION_STATUS", IdActionStatus),
            //            CreateTableParam("@ID_ACTION_DATA", IdActionData),
            //            CreateTableParam("@ID_ACTION_PARENT", IdActionParent),
            //            CreateTableParam("@ID_DATA_ARCH", IdDataArch),
            //            CreateTableParam("@ID_MODULE", IdModule),
            //            CreateTableParam("@ID_OPERATOR", IdOperator),
            //            CreateTableParam("@START_DATE_CODE", StartDateFrom),
            //            CreateTableParam("@END_DATE_CODE", StartDateTo),
            //            CreateTableParam("@ID_DISTRIBUTOR", IdDistributor ?? DistributorFilter)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}
            #endregion
        }

        public long SaveAction(DB_ACTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION", ToBeSaved.ID_ACTION);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveAction",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_ACTION_TYPE", ToBeSaved.ID_ACTION_TYPE)
														,new DB.InParameter("@ID_ACTION_STATUS", ToBeSaved.ID_ACTION_STATUS)
														,new DB.InParameter("@ID_ACTION_DATA", ToBeSaved.ID_ACTION_DATA)
														,new DB.InParameter("@ID_ACTION_PARENT", ToBeSaved.ID_ACTION_PARENT)
														,new DB.InParameter("@ID_DATA_ARCH", ToBeSaved.ID_DATA_ARCH)
														,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
                                                        ,ToBeSaved.CREATION_DATE == DateTime.MinValue ? new DB.InNullParameter("@CREATION_DATE") : new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }
        #endregion

        #region Table ACTION_SMS_MESSAGE
        public DB_ACTION_SMS_MESSAGE[] GetActionSmsMessageFilter(long[] IdActionSmsMessage, long[] SerialNbr, TypeDateTimeCode Time, bool? IsIncoming, int[] IdTransmissionStatus, long[] IdAction,
                            string Message, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsMessageFilter"));

            return (DB_ACTION_SMS_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetActionSmsMessageFilter",
                new DB.AnalyzeDataSet(GetActionSmsMessage),
                new DB.Parameter[] { 
					CreateTableParam("@ID_ACTION_SMS_MESSAGE", IdActionSmsMessage),
					CreateTableParam("@SERIAL_NBR", SerialNbr ?? DeviceFilter),
					CreateTableParam("@TIME_CODE", Time),
					new DB.InParameter("@IS_INCOMING", IsIncoming),
					CreateTableParam("@ID_TRANSMISSION_STATUS", IdTransmissionStatus),
					CreateTableParam("@ID_ACTION", IdAction),
					new DB.InParameter("@MESSAGE", Message),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion

        #region Table ACTION_SMS_TEXT

        public DB_ACTION_SMS_TEXT[] GetActionSmsText(Enums.Language language)
        {
            return (DB_ACTION_SMS_TEXT[])DB.ExecuteProcedure(
                "imrse_GetActionSmsText",
                new DB.AnalyzeDataSet(GetActionSmsText),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_TEXT", new long[] {})
            ,new DB.InParameter("@ID_LANGUAGE", (int)language)
					}
            );
        }

        public DB_ACTION_SMS_TEXT[] GetActionSmsText(long[] Ids, Enums.Language language)
        {
            return (DB_ACTION_SMS_TEXT[])DB.ExecuteProcedure(
                "imrse_GetActionSmsText",
                new DB.AnalyzeDataSet(GetActionSmsText),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_TEXT", Ids)
            ,new DB.InParameter("@ID_LANGUAGE", (int)language)
					}
            );
        }



        private object GetActionSmsText(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_SMS_TEXT[] list = new DB_ACTION_SMS_TEXT[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_SMS_TEXT insert = new DB_ACTION_SMS_TEXT();
                insert.ID_ACTION_SMS_TEXT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_SMS_TEXT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.SMS_TEXT = GetValue<string>(QueryResult.Tables[0].Rows[i]["SMS_TEXT"]);
                insert.ID_DATA_TYPE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.ID_TRANSMISSION_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionSmsText(DB_ACTION_SMS_TEXT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_SMS_TEXT", ToBeSaved.ID_ACTION_SMS_TEXT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionSmsText",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@SMS_TEXT", SqlDbType.NVarChar, ToBeSaved.SMS_TEXT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@IS_INCOMING", ToBeSaved.IS_INCOMING)
														,new DB.InParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_SMS_TEXT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionSmsText(DB_ACTION_SMS_TEXT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionSmsText",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_SMS_TEXT", toBeDeleted.ID_ACTION_SMS_TEXT)			
		}
            );
        }

        #endregion

        #region Table ACTOR_IN_GROUP

        public DB_ACTOR_IN_GROUP[] GetActorInGroup()
        {
            return (DB_ACTOR_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorInGroup",
                new DB.AnalyzeDataSet(GetActorInGroup),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_ACTOR", new int[] {}),
                        CreateTableParam("@ID_ACTOR_GROUP", new int[] {})
					}
            );
        }

        public DB_ACTOR_IN_GROUP[] GetActorInGroup(int[] IdActorGroup)
        {
            return (DB_ACTOR_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorInGroup",
                new DB.AnalyzeDataSet(GetActorInGroup),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_ACTOR", new int[] {}),
                        CreateTableParam("@ID_ACTOR_GROUP", IdActorGroup)
					}
            );
        }

        public DB_ACTOR_IN_GROUP[] GetActorInGroup(int[] IdActor, int[] IdActorGroup)
        {
            return (DB_ACTOR_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_u_GetActorInGroup",
                new DB.AnalyzeDataSet(GetActorInGroup),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_ACTOR", IdActor),
                        CreateTableParam("@ID_ACTOR_GROUP", IdActorGroup)
					}
            );
        }

        private object GetActorInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTOR_IN_GROUP[] list = new DB_ACTOR_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTOR_IN_GROUP insert = new DB_ACTOR_IN_GROUP();
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.ID_ACTOR_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR_GROUP"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActorInGroup(DB_ACTOR_IN_GROUP ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveActorInGroup",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
										,new DB.InParameter("@ID_ACTOR_GROUP", ToBeSaved.ID_ACTOR_GROUP)
									}
            );


            return ToBeSaved.ID_ACTOR_GROUP;
        }

        public void DeleteActorInGroup(DB_ACTOR_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelActorInGroup",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_ACTOR", toBeDeleted.ID_ACTOR)		
	        	    ,new DB.InParameter("@ID_ACTOR_GROUP", toBeDeleted.ID_ACTOR_GROUP)
		        }
            );
        }

        #endregion

        #region Table ALARM_EVENT
        public DB_ALARM_EVENT[] GetAlarmEventFilter(long[] IdAlarmEvent, long[] IdAlarmDef, long[] IdDataArch, bool? IsActive, long[] SerialNbr, long[] IdMeter,
                            long[] IdLocation, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmEventFilter"));

            DB_ALARM_EVENT[] tmpResult = (DB_ALARM_EVENT[])DB.ExecuteProcedure(
                "imrse_GetAlarmEventFilter",
                new DB.AnalyzeDataSet(GetAlarmEvent),
                new DB.Parameter[] { 
					CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
					CreateTableParam("@ID_ALARM_DEF", IdAlarmDef),
					CreateTableParam("@ID_DATA_ARCH", IdDataArch),
					new DB.InParameter("@IS_ACTIVE", IsActive),
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					CreateTableParam("@ID_METER", IdMeter),
					CreateTableParam("@ID_LOCATION", IdLocation),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_ALARM_EVENT[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmEventFilter"));

            //return (DB_ALARM_EVENT[])DB.ExecuteProcedure(
            //    "imrse_GetAlarmEventFilter",
            //    new DB.AnalyzeDataSet(GetAlarmEvent),
            //    new DB.Parameter[] { 
            //        CreateTableParam("@ID_ALARM_EVENT", IdAlarmEvent),
            //        CreateTableParam("@ID_ALARM_DEF", IdAlarmDef),
            //        CreateTableParam("@ID_DATA_ARCH", IdDataArch),
            //        new DB.InParameter("@IS_ACTIVE", IsActive),
            //        CreateTableParam("@SERIAL_NBR", SerialNbr),
            //        CreateTableParam("@ID_METER", IdMeter),
            //        CreateTableParam("@ID_LOCATION", IdLocation),
            //        new DB.InParameter("@TOP_COUNT", topCount),
            //        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //    autoTransaction, transactionLevel, commandTimeout
            //);
            #endregion
        }
        #endregion

        #region Table ALARM_TEXT

        public DB_ALARM_TEXT[] GetAlarmText(Enums.Language language)
        {
            return (DB_ALARM_TEXT[])DB.ExecuteProcedure(
                "imrse_u_GetAlarmText",
                new DB.AnalyzeDataSet(GetAlarmText),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TEXT", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
                            ,new DB.InParameter("@ID_LANGUAGE", (int)language)
					}
            );
        }

        public DB_ALARM_TEXT[] GetAlarmText(long[] Ids, Enums.Language language)
        {
            return (DB_ALARM_TEXT[])DB.ExecuteProcedure(
                "imrse_u_GetAlarmText",
                new DB.AnalyzeDataSet(GetAlarmText),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TEXT", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
                            ,new DB.InParameter("@ID_LANGUAGE", (int)language)
					}
            );
        }



        private object GetAlarmText(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_TEXT[] list = new DB_ALARM_TEXT[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_TEXT insert = new DB_ALARM_TEXT();
                insert.ID_ALARM_TEXT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TEXT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ALARM_TEXT = GetValue<string>(QueryResult.Tables[0].Rows[i]["ALARM_TEXT"]);
                insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmText(DB_ALARM_TEXT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_TEXT", ToBeSaved.ID_ALARM_TEXT);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveAlarmText",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ALARM_TEXT", ToBeSaved.ALARM_TEXT)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_TEXT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmText(DB_ALARM_TEXT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelAlarmText",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_TEXT", toBeDeleted.ID_ALARM_TEXT)			
		}
            );
        }

        #endregion

        #region Table ACTIVITY
        public int SaveActivity(DB_ACTIVITY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTIVITY", ToBeSaved.ID_ACTIVITY);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveActivity",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTIVITY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table AUDIT

        public List<long> GetAuditBatchFromTimePeriod(DateTime StartTime, DateTime EndTime, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            TypeDateTimeCode dtcItem = new TypeDateTimeCode();
            dtcItem.DateTime1 = StartTime;
            dtcItem.DateTime2 = EndTime;
            dtcItem.Mode = TypeDateTimeCode.ModeType.Between;

            return (List<long>)DB.ExecuteProcedure(
                    "imrse_u_GetAuditBatchFromTimePeriod",
                    new DB.AnalyzeDataSet(GetAuditBatchFromTimePeriod),
                    new DB.Parameter[] { 
					    CreateTableParam("@TIME_CODE", dtcItem)},
                    autoTransaction, transactionLevel, commandTimeout
                );
        }


        private object GetAuditBatchFromTimePeriod(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => GetValue<long>(row["BATCH_ID"])).ToList();
        }

        #endregion

        #region Table CONVERSION
        public void UpdateConversionFactor(long SerialNbr, float Slope, float Bias)
        {
            DB.ExecuteNonQueryProcedure(
                "dataservice_UpdateConversionFactor",
                new DB.Parameter[] {
                new DB.InParameter("@SerialNbr", SerialNbr), 
                new DB.InParameter("@Slope", Slope),
                new DB.InParameter("@Bias", Bias),});
        }
        #endregion

        #region Table DATA
        #region GetDataFilter
        public DB_DATA[] GetDataFilter(long[] IdData, long[] SerialNbr, long[] IdMeter, long[] IdLocation, long[] IdDataType, int[] IndexNbr,
                            int[] Status, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            DB_DATA[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) || (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) || (Status != null && Status.Length > MaxItemsPerSqlTableType)))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdData != null) args.AddRange(IdData.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
                if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(7, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_DATA[])DB.ExecuteProcedure(
                        "imrse_GetDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetData),
                        new DB.Parameter[] { 
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun¹c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_DATA[])DB.ExecuteProcedure(
                    "imrse_GetDataFilter",
                    new DB.AnalyzeDataSet(GetData),
                    new DB.Parameter[] { 
					    CreateTableParam("@ID_DATA", IdData),
					    CreateTableParam("@SERIAL_NBR", SerialNbr),
					    CreateTableParam("@ID_METER", IdMeter),
					    CreateTableParam("@ID_LOCATION", IdLocation),
					    CreateTableParam("@ID_DATA_TYPE", IdDataType),
					    CreateTableParam("@INDEX_NBR", IndexNbr),
					    CreateTableParam("@STATUS", Status),
					    new DB.InParameter("@TOP_COUNT", topCount),
					    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_DATA[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            //if (UseBulkInsertSqlTableType &&
            //    ((SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) || (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) || (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //     (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) || (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType) || (Status != null && Status.Length > MaxItemsPerSqlTableType)))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdData != null) args.AddRange(IdData.Select(s => new Tuple<int, object>(1, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(4, s)));
            //    if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(5, s)));
            //    if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(6, s)));
            //    if (Status != null) args.AddRange(Status.Select(s => new Tuple<int, object>(7, s)));

            //    DB_DATA[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DB_DATA[])DB.ExecuteProcedure(
            //            "imrse_GetDataFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetData),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usun¹c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DB_DATA[])DB.ExecuteProcedure(
            //        "imrse_GetDataFilter",
            //        new DB.AnalyzeDataSet(GetData),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_DATA", IdData),
            //            CreateTableParam("@SERIAL_NBR", SerialNbr),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_DATA_TYPE", IdDataType),
            //            CreateTableParam("@INDEX_NBR", IndexNbr),
            //            CreateTableParam("@STATUS", Status),
            //            new DB.InParameter("@TOP_COUNT", topCount),
            //            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout
            //    );
            //}
            #endregion
        }
        #endregion
        #endregion

        #region Table DATA_FORMAT_GROUP_DETAILS
        public int SaveDataFormatGroupDetails(DB_DATA_FORMAT_GROUP_DETAILS ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDataFormatGroupDetails",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_DATA_FORMAT_GROUP", ToBeSaved.ID_DATA_FORMAT_GROUP)
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
										,new DB.InParameter("@ID_DATA_TYPE_FORMAT_IN", ToBeSaved.ID_DATA_TYPE_FORMAT_IN)
										,new DB.InParameter("@ID_DATA_TYPE_FORMAT_OUT", ToBeSaved.ID_DATA_TYPE_FORMAT_OUT)
										,new DB.InParameter("@ID_UNIT_IN", ToBeSaved.ID_UNIT_IN)
										,new DB.InParameter("@ID_UNIT_OUT", ToBeSaved.ID_UNIT_OUT)
									}
            );

            return ToBeSaved.ID_DATA_FORMAT_GROUP;
        }

        public void DeleteDataFormatGroupDetails(DB_DATA_FORMAT_GROUP_DETAILS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDataFormatGroupDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_DATA_FORMAT_GROUP", toBeDeleted.ID_DATA_FORMAT_GROUP)
										,new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)			
		}
            );
        }

        public DB_DATA_FORMAT_GROUP_DETAILS[] GetHierarchicalDataFormatGroupDetails(int IdOperator, int IdDistributor, int IdModule)
        {
            return (DB_DATA_FORMAT_GROUP_DETAILS[])DB.ExecuteProcedure(
                "imrse_u_GetHierarchicalDataFormatGroupDetails",
                new DB.AnalyzeDataSet(GetDataFormatGroupDetails),
                new DB.Parameter[] { 
			                new DB.InParameter("@ID_OPERATOR", IdOperator)
							,new DB.InParameter("@ID_DISTRIBUTOR", IdDistributor)
                            ,new DB.InParameter("@ID_MODULE", IdModule)
					}
            );
        }
        #endregion

        #region Table DATA_TYPE_IN_GROUP

        public DB_DATA_TYPE_IN_GROUP[] GetDataTypeInGroup()
        {
            return (DB_DATA_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeInGroup",
                new DB.AnalyzeDataSet(GetDataTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_DATA_TYPE_IN_GROUP[] GetDataTypeInGroup(int[] Ids)
        {
            return (DB_DATA_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeInGroup",
                new DB.AnalyzeDataSet(GetDataTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP", Ids)
					}
            );
        }

        private object GetDataTypeInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE_IN_GROUP[] list = new DB_DATA_TYPE_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE_IN_GROUP insert = new DB_DATA_TYPE_IN_GROUP();
                insert.ID_DATA_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_GROUP"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataTypeInGroup(DB_DATA_TYPE_IN_GROUP ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDataTypeInGroup",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_DATA_TYPE_GROUP", ToBeSaved.ID_DATA_TYPE_GROUP)
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
									}
            );

            return ToBeSaved.ID_DATA_TYPE_GROUP;
        }

        public void DeleteDataTypeInGroup(DB_DATA_TYPE_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDataTypeInGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE_GROUP", toBeDeleted.ID_DATA_TYPE_GROUP)	,		
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)		
		}
            );
        }

        #endregion

        #region Table DATA_TRANSFER

        public long SaveDataTransfer(DB_DATA_TRANSFER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TRANSFER", ToBeSaved.ID_DATA_TRANSFER);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDataTransfer",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@BATCH_ID", ToBeSaved.BATCH_ID)
														,new DB.InParameter("@CHANGE_TYPE", ToBeSaved.CHANGE_TYPE)
														,new DB.InParameter("@CHANGE_DIRECTION", ToBeSaved.CHANGE_DIRECTION)
														,new DB.InParameter("@ID_SOURCE_SERVER", ToBeSaved.ID_SOURCE_SERVER)
														,new DB.InParameter("@ID_DESTINATION_SERVER", ToBeSaved.ID_DESTINATION_SERVER)
														,new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME)
														,new DB.InParameter("@KEY1", ToBeSaved.KEY1)
														,new DB.InParameter("@KEY2", ToBeSaved.KEY2)
														,new DB.InParameter("@KEY3", ToBeSaved.KEY3)
														,new DB.InParameter("@KEY4", ToBeSaved.KEY4)
														,new DB.InParameter("@KEY5", ToBeSaved.KEY5)
														,new DB.InParameter("@KEY6", ToBeSaved.KEY6)
														,new DB.InParameter("@COLUMN_NAME", ToBeSaved.COLUMN_NAME)
														,ParamObject("@OLD_VALUE", ToBeSaved.OLD_VALUE, 0, 0)
													,ParamObject("@NEW_VALUE", ToBeSaved.NEW_VALUE, 0, 0)
													,new DB.InParameter("@INSERT_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.INSERT_TIME))
													,new DB.InParameter("@PROCEED_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.PROCEED_TIME))
													,new DB.InParameter("@USER", ToBeSaved.USER)
														,new DB.InParameter("@HOST", ToBeSaved.HOST)
														,new DB.InParameter("@APPLICATION", ToBeSaved.APPLICATION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TRANSFER = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void SaveDataTransfer(DB_DATA_TRANSFER[] ToBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            if (ToBeSaved.Count(d => d.ID_DATA_TRANSFER != 0) > 0)
                BulkUpdate("DATA_TRANSFER", ToBeSaved.Where(d => d.ID_DATA_TRANSFER != 0).ToArray(),
                            primaryKeysFields: null,
                            FillDataTableMethod: FillDataTransfer,
                            updateSetSkipColumns: (updateSetSkipColumns == null ? new List<string>() { "USER", "HOST", "APPLICATION", "INSERT_TIME" } : updateSetSkipColumns),
                            commandTimeout: commandTimeout);
            if (ToBeSaved.Count(d => d.ID_DATA_TRANSFER == 0) > 0)
                BulkInsert("DATA_TRANSFER", ToBeSaved.Where(d => d.ID_DATA_TRANSFER == 0).ToArray(), fillDataTableMethod: FillDataTransfer, commandTimeout: commandTimeout);
        }

        protected DataTable FillDataTransfer(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_DATA_TRANSFER[] dataList = data as DB_DATA_TRANSFER[];
            int iterator = 0;

            foreach (DB_DATA_TRANSFER dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();
                row["ID_DATA_TRANSFER"] = dtItem.ID_DATA_TRANSFER;
                row["BATCH_ID"] = dtItem.BATCH_ID;
                row["CHANGE_TYPE"] = dtItem.CHANGE_TYPE;
                row["CHANGE_DIRECTION"] = dtItem.CHANGE_DIRECTION;
                if (dtItem.ID_SOURCE_SERVER.HasValue)
                    row["ID_SOURCE_SERVER"] = dtItem.ID_SOURCE_SERVER.Value;
                else
                    row["ID_SOURCE_SERVER"] = DBNull.Value;
                if (dtItem.ID_DESTINATION_SERVER.HasValue)
                    row["ID_DESTINATION_SERVER"] = dtItem.ID_DESTINATION_SERVER.Value;
                else
                    row["ID_DESTINATION_SERVER"] = DBNull.Value;
                row["TABLE_NAME"] = dtItem.TABLE_NAME;

                if (dtItem.KEY1.HasValue)
                    row["KEY1"] = dtItem.KEY1.Value;
                else
                    row["KEY1"] = DBNull.Value;
                if (dtItem.KEY2.HasValue)
                    row["KEY2"] = dtItem.KEY2.Value;
                else
                    row["KEY2"] = DBNull.Value;
                if (dtItem.KEY3.HasValue)
                    row["KEY3"] = dtItem.KEY3.Value;
                else
                    row["KEY3"] = DBNull.Value;
                if (dtItem.KEY4.HasValue)
                    row["KEY4"] = dtItem.KEY4.Value;
                else
                    row["KEY4"] = DBNull.Value;
                if (dtItem.KEY5.HasValue)
                    row["KEY5"] = dtItem.KEY5.Value;
                else
                    row["KEY5"] = DBNull.Value;
                if (dtItem.KEY6.HasValue)
                    row["KEY6"] = dtItem.KEY6.Value;
                else
                    row["KEY6"] = DBNull.Value;

                row["COLUMN_NAME"] = dtItem.COLUMN_NAME;

                if (dtItem.OLD_VALUE != null)
                    row["OLD_VALUE"] = dtItem.OLD_VALUE;
                else
                    row["OLD_VALUE"] = DBNull.Value;
                if (dtItem.NEW_VALUE != null)
                    row["NEW_VALUE"] = dtItem.NEW_VALUE;
                else
                    row["NEW_VALUE"] = DBNull.Value;
                row["INSERT_TIME"] = dtItem.INSERT_TIME;
                if (dtItem.PROCEED_TIME.HasValue)
                    row["PROCEED_TIME"] = dtItem.PROCEED_TIME.Value;
                else
                    row["PROCEED_TIME"] = DBNull.Value;
                if (dtItem.USER != null)
                    row["USER"] = dtItem.USER;
                else
                    row["USER"] = DBNull.Value;
                if (dtItem.HOST != null)
                    row["HOST"] = dtItem.HOST;
                else
                    row["HOST"] = DBNull.Value;
                if (dtItem.APPLICATION != null)
                    row["APPLICATION"] = dtItem.APPLICATION;
                else
                    row["APPLICATION"] = DBNull.Value;
                dataTable.Rows.Add(row);
                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }
                iterator++;
            }

            return dataTable;
        }

        public void ProceedDataTransfer(long[] IdDataTransfer, DateTime? ProceedTime, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (UseBulkInsertSqlTableType && (
                (IdDataTransfer != null && IdDataTransfer.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDataTransfer != null) args.AddRange(IdDataTransfer.Select(s => new Tuple<int, object>(1, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    DB.ExecuteNonQueryProcedure(
                        "imrse_u_ProceedDataTransferTableArgs",
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@PROCEED_TIME", ConvertTimeZoneToUtcTime(ProceedTime))},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
            }
            else
            {
                DB.ExecuteNonQueryProcedure(
                    "imrse_u_ProceedDataTransfer",
                    new DB.Parameter[] { 
				CreateTableParam("@ID_DATA_TRANSFER", IdDataTransfer),
				new DB.InParameter("@PROCEED_TIME", ConvertTimeZoneToUtcTime(ProceedTime))},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        public void DeleteDataTransfer(long[] BatchId = null,
                                                         string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (BatchId == null || BatchId.Count() == 0))
                throw new ApplicationException("Not specified any parameter");

            DB.ExecuteNonQueryProcedure(
                    "imrse_u_DelDataTransfer",
                    new DB.Parameter[] {
                    CreateTableParam("@BATCH_ID", BatchId),
				    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)
                }, commandTimeout
            );
        }

        #endregion

        #region Table DESCR

        public DB_DESCR[] GetDescr(Enums.Language language, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DESCR[])DB.ExecuteProcedure(
                "imrse_u_GetDescr",
                new DB.AnalyzeDataSet(GetDescr),
                new DB.Parameter[] { 
                    CreateTableParam("@ID_DESCR", new long[]{}), 
                    new DB.InParameter("@ID_LANGUAGE", (int)language) 
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_DESCR[] GetDescr(long[] Ids, Enums.Language language, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DESCR[])DB.ExecuteProcedure(
                "imrse_u_GetDescr",
                new DB.AnalyzeDataSet(GetDescr),
                new DB.Parameter[] { 
                    CreateTableParam("@ID_DESCR", Ids),
                    new DB.InParameter("@ID_LANGUAGE", (int)language)
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetDescr(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DESCR[] list = new DB_DESCR[count];
            for (int i = 0; i < count; i++)
            {
                DB_DESCR insert = new DB_DESCR();
                insert.ID_DESCR = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.EXTENDED_DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["EXTENDED_DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDescr(DB_DESCR ToBeSaved, long IdDescrRangeStart, long IdDescrRangeStop)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DESCR", ToBeSaved.ID_DESCR);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDescr",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
					,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
					,new DB.InParameter("@EXTENDED_DESCRIPTION", ToBeSaved.EXTENDED_DESCRIPTION)
                    ,new DB.InParameter("@ID_DESCR_RANGE_START", IdDescrRangeStart)
                    ,new DB.InParameter("@ID_DESCR_RANGE_STOP", IdDescrRangeStop)
			}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DESCR = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDescr(DB_DESCR ToBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteDescr",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_DESCR", ToBeDeleted.ID_DESCR)
					,new DB.InParameter("@ID_LANGUAGE", ToBeDeleted.ID_LANGUAGE)
			}
            );
        }

        #endregion

        #region Table DEVICE
        public void AddDevice(DB_DEVICE ToBeSaved)
        {
            DB.InParameter InsertId = new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_AddDevice",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@SERIAL_NBR_PATTERN", ToBeSaved.SERIAL_NBR_PATTERN)
														,new DB.InParameter("@ID_DESCR_PATTERN", ToBeSaved.ID_DESCR_PATTERN)
														,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE)
									}
            );
        }

        public int CheckDeviceState(string serialNbr, int statusValidateOption = 0, object statusValidateParam = null)
        {
            DB.OutParameter pStatus = new DB.OutParameter("@RESULT", SqlDbType.Int);
            DB.InParameter pSerialNbr = new DB.InParameter("@SERIAL_NBR", SqlDbType.NVarChar, serialNbr, 100);
            DB.InParameter pStatusValidateOption = new DB.InParameter("@STATUS_VALIDATE_OPTION", SqlDbType.Int, statusValidateOption);

            SqlDbType paramDBType = SqlDbType.Int;
            if (statusValidateParam != null)
            {
                if (statusValidateParam.GetType() == typeof(int))
                    paramDBType = SqlDbType.Int;
                else if (statusValidateParam.GetType() == typeof(long))
                    paramDBType = SqlDbType.BigInt;
                else if (statusValidateOption.GetType() == typeof(DateTime))
                    paramDBType = SqlDbType.DateTime;
                else if (statusValidateOption.GetType() == typeof(string))
                    paramDBType = SqlDbType.NVarChar;
            }

            DB.InParameter pStatusValidateParam = new DB.InParameter("@STATUS_VALIDATE_PARAM", paramDBType, statusValidateParam);

            DB.ExecuteNonQueryProcedure(
                        "imrse_u_CheckDeviceState",
                        new DB.Parameter[] { 
                            pStatus,
                            pSerialNbr,
                            pStatusValidateOption,
                            pStatusValidateParam
                }
                    );

            if (pStatus.Value != null)
                return (int)pStatus.Value;
            else
                return -1;
        }

        #region FindDevice
        /// <summary>
        /// FindDevice check if device exists in table RADIO_DEVICE in database RadioDeviceTester
        /// </summary>
        /// <param name="serialNbr">Nvarchar(16)</param>
        /// <returns></returns>
        public bool FindDevice(long serialNbr)
        {
            DB.InParameter pSerialNbr = new DB.InParameter("@DEVICE_SERIAL_NBR", SqlDbType.NVarChar, serialNbr, 16);
            DB.OutParameter pStatus = new DB.OutParameter("@RESULT", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                        "imrse_u_FindDevice",
                        new DB.Parameter[] { 
                            pSerialNbr,
                            pStatus
                }
            );
            if (pStatus.Value != null)
                return (bool)pStatus.Value;
            else
                return false;
        }
        #endregion

        #region CheckIfDeviceHasShippingList
        public bool CheckIfDeviceHasShippingList(long serialNbr)
        {
            DB.InParameter pSerialNbr = new DB.InParameter("@SerialNbr", SqlDbType.BigInt, serialNbr);
            DB.OutParameter pHasShippingList = new DB.OutParameter("@HasShippingList", SqlDbType.Bit);

            bool hasShippingList = false;

            DB.ExecuteNonQueryProcedure(
                        "imrsc_MW_CheckIfDeviceHasShippingList",
                        new DB.Parameter[] { pSerialNbr, pHasShippingList }
                    );

            if (!pHasShippingList.IsNull)
                hasShippingList = (bool)pHasShippingList.Value;
            return hasShippingList;
        }
        #endregion

        #region GetMirroringDepositoryElements

        public DataTable GetMirroringDepositoryElements()//int idDistributor)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_MirroringDepositoryElements",
                new DB.AnalyzeDataSet(GetMirroringDepositoryElements),
                new DB.Parameter[] { 
			               //new DB.InParameter("@ID_DISTRIBUTOR", idDistributor)
					}, 300
            );
        }

        private object GetMirroringDepositoryElements(DataSet ds)
        {
            return ds.Tables[0];
        }

        #endregion

        #region CheckDataStatusForDevice
        public void CheckDataStatusForDevice(ulong? serialNbr)
        {
            if (serialNbr == null)
                return;

            DB.ExecuteNonQueryProcedure(
                "sp_CheckDataStatusForDevice",
                new DB.Parameter[]
				{
					new DB.InParameter("@SERIAL_NBR", serialNbr.Value),
				},
                true, this.LongTimeout
            );
        }
        #endregion

        #region GetDeviceSet

        public DB_DEVICE_SET[] GetDeviceSet(int[] idDistributor, int[] idDeviceStateType)
        {
            return (DB_DEVICE_SET[])DB.ExecuteProcedure(
                "imrse_u_GetDeviceSet",
                new DB.AnalyzeDataSet(GetDeviceSet),
                new DB.Parameter[] { 
                            CreateTableParam("@ID_DISTRIBUTOR", idDistributor),
                            CreateTableParam("@ID_DEVICE_STATE_TYPE", idDeviceStateType)
					}, 300
            );
        }

        private object GetDeviceSet(DataSet ds)
        {
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<DB_DEVICE_SET> retList = new List<DB_DEVICE_SET>();

                foreach (DataRow drItem in ds.Tables[0].Rows)
                {
                    DB_DEVICE_SET dsItem = new DB_DEVICE_SET();
                    dsItem.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                    dsItem.ID_SET = GetValue<int>(drItem["ID_SET"]);
                    dsItem.SET_SERIAL_NBR = GetValue<string>(drItem["SET_SERIAL_NBR"]);
                    retList.Add(dsItem);
                }

                return retList.ToArray();
            }
            else
                return null;
        }

        #endregion

        #region CheckDeviceSchedule
        public DB_DEVICE_SCHEDULE_User[] CheckDeviceSchedule(long[] serialNbr, DateTime startTime, DateTime endTime)
        {
            return (DB_DEVICE_SCHEDULE_User[])DB.ExecuteProcedure(
                "sp_CheckDeviceSchedule",
                new DB.AnalyzeDataSet(CheckDeviceScheduleUser),
                new DB.Parameter[]
				{
					CreateTableParam("@SERIAL_NBR", serialNbr),
					new DB.InParameter("@START_TIME", startTime),
					new DB.InParameter("@END_TIME", endTime)
				}, false, IsolationLevel.ReadUncommitted,
                this.LongTimeout
            );
        }

        public DB_DEVICE_SCHEDULE_User[] GetReadoutDeviceRelationship(long[] serialNbr)
        {
            return (DB_DEVICE_SCHEDULE_User[])DB.ExecuteProcedure(
                "sp_GetReadoutDeviceRelationship",
                new DB.AnalyzeDataSet(CheckDeviceScheduleUser),
                new DB.Parameter[]
				{
					CreateTableParam("@SERIAL_NBR", serialNbr)
				}, false, IsolationLevel.ReadUncommitted,
                this.LongTimeout
            );
        }

        private object CheckDeviceScheduleUser(DataSet ds)
        {
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<DB_DEVICE_SCHEDULE_User> retList = new List<DB_DEVICE_SCHEDULE_User>();
                bool existsOkoIdDataType = ds.Tables[0].Columns.Cast<DataColumn>().ToList().Exists(c => String.Equals(c.ColumnName, "OKO_ID_DATA_TYPE"));

                foreach (DataRow drItem in ds.Tables[0].Rows)
                {
                    DB_DEVICE_SCHEDULE_User dsItem = new DB_DEVICE_SCHEDULE_User();
                    dsItem.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                    dsItem.SCHEDULE_EXISTS = GetValue<bool>(drItem["SCHEDULE_EXISTS"]);
                    dsItem.ID_DEVICE_TYPE = GetNullableValue<int>(drItem["ID_DEVICE_TYPE"]);
                    dsItem.ID_METER = GetNullableValue<long>(drItem["ID_METER"]);
                    dsItem.ID_METER_TYPE = GetNullableValue<int>(drItem["ID_METER_TYPE"]);
                    dsItem.ID_METER_TYPE_CLASS = GetNullableValue<int>(drItem["ID_METER_TYPE_CLASS"]);
                    dsItem.ID_DATA_TYPE = GetNullableValue<long>(drItem["ID_DATA_TYPE"]);
                    dsItem.ALEVEL_NBR = GetNullableValue<long>(drItem["ALEVEL_NBR"]);
                    if (existsOkoIdDataType)
                        dsItem.OKO_ID_DATA_TYPE = GetNullableValue<long>(drItem["OKO_ID_DATA_TYPE"]);
                    retList.Add(dsItem);
                }

                return retList.ToArray();
            }
            else
                return null;
        }
        #endregion

        #region GetDeviceBatchNumbers

        public List<string> GetDeviceBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (List<string>)DB.ExecuteProcedure(
                "imrse_u_GetDeviceBatchNumbers",
                new DB.AnalyzeDataSet(GetDeviceBatchNumbers),
                new DB.Parameter[] { 
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }
        public List<string> GetDeviceManufacturingBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (List<string>)DB.ExecuteProcedure(
                "imrse_u_GetDeviceManufacturingBatchNumbers",
                new DB.AnalyzeDataSet(GetDeviceBatchNumbers),
                new DB.Parameter[] { 
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }
        private object GetDeviceBatchNumbers(DataSet ds)
        {
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<string> retList = new List<string>();

                foreach (DataRow drItem in ds.Tables[0].Rows)
                {
                    retList.Add(GetValue<string>(drItem["DEVICE_BATCH_NUMBER"]));
                }

                return retList.ToList();
            }
            else
                return null;
        }

        #endregion

        #region CreateNewDeviceSet

        public int CreateNewDeviceSet(int idPackageType, string[] radioAddresses, long idLocation, string packageNbr, out string errorMsg)
        {
            //[tmq_CreateNewDeviceSet]
            //@ID_PACKAGE_TYPE int,
            //@DEVICES TYPE_BIGINT_INT readonly,
            //@ID_LOCATION bigint,
            //@PACKAGE_NBR nvarchar(10),
            //@RESULT int out
            DataTable dtDevices = new DataTable();
            dtDevices.Columns.Add("SERIAL_NBR", System.Type.GetType("System.Int64"));
            //dtDevices.Columns.Add("ID_DEVICE_TYPE", System.Type.GetType("System.Int32"));

            if (radioAddresses == null || radioAddresses.Length == 0)
            {
                errorMsg = "No devices has been specified!";
                return -1;
            }

            foreach (string radioAddress in radioAddresses)
            {
                dtDevices.Rows.Add(long.Parse(GetRadioAddress(radioAddress)));
            }

            DB.OutParameter result = new DB.OutParameter("@RESULT", SqlDbType.Int);
            try
            {


                DB.ExecuteScalar(
                    "tmq_CreateNewDeviceSet",
                    new DB.Parameter[]
					{
						new DB.InParameter ("@ID_PACKAGE_TYPE", SqlDbType.Int, idPackageType),
						new DB.InParameter ("@DEVICES", SqlDbType.Structured, dtDevices),
						new DB.InParameter ("@ID_LOCATION", SqlDbType.BigInt, idLocation),
                        new DB.InParameter ("@PACKAGE_NBR", SqlDbType.NVarChar, packageNbr, 10),
						result
					}
                    );

                errorMsg = "";
                return result.Value == DBNull.Value ? -1 : (int)result.Value;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result.Value == DBNull.Value ? -1 : (int)result.Value;
            }
        }

        string GetRadioAddress(string serial)
        {
            if (serial == null)
                return null;

            if (serial.Trim().Length == 16)
                return "" + serial[0] + serial[1] + serial[8] + serial[10] + serial[11] + serial[12] + serial[13] + serial[14];

            return serial.Trim();
        }

        #endregion

        #endregion

        #region Table DEVICE_DETAILS
        #region GetDeviceDetailsFilterUser

        public DB_DEVICE_DETAILS[] GetDeviceDetailsFilterUser(long[] IdDeviceDetails, long[] SerialNbr, long[] IdLocation, string FactoryNbr, TypeDateTimeCode ShippingDate, TypeDateTimeCode WarrantyDate,
                    string Phone, int[] IdDeviceType, int[] IdDeviceOrderNumber, int[] IdDistributor, int[] IdDeviceStateType,
                    string ProductionDeviceOrderNumber, string DeviceBatchNumber, int[] IdDistributorOwner, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDetailsFilterUser"));
            if (UseBulkInsertSqlTableType && (
                (IdDeviceDetails != null && IdDeviceDetails.Length > MaxItemsPerSqlTableType) ||
                ((SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) || (SerialNbr == null && DeviceFilter.Length > MaxItemsPerSqlTableType)) ||
                ((IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) || (IdLocation == null && LocationFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdDeviceType != null && IdDeviceType.Length > MaxItemsPerSqlTableType) ||
                (IdDeviceOrderNumber != null && IdDeviceOrderNumber.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType)) ||
                (IdDeviceStateType != null && IdDeviceStateType.Length > MaxItemsPerSqlTableType) ||
                (IdDistributorOwner != null && IdDistributorOwner.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDeviceDetails != null) args.AddRange(IdDeviceDetails.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(2, s)));
                else if (DeviceFilter.Length > 0) args.AddRange(DeviceFilter.Select(s => new Tuple<int, object>(2, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(3, s)));
                else if (LocationFilter.Length > 0) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(3, s)));
                if (IdDeviceType != null) args.AddRange(IdDeviceType.Select(s => new Tuple<int, object>(8, s)));
                if (IdDeviceOrderNumber != null) args.AddRange(IdDeviceOrderNumber.Select(s => new Tuple<int, object>(9, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(10, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(10, s)));
                if (IdDeviceStateType != null) args.AddRange(IdDeviceStateType.Select(s => new Tuple<int, object>(11, s)));
                if (IdDistributorOwner != null) args.AddRange(IdDistributorOwner.Select(s => new Tuple<int, object>(14, s)));

                DB_DEVICE_DETAILS[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE_DETAILS[])DB.ExecuteProcedure(
                        "imrse_GetDeviceDetailsFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDeviceDetailsUser),
                        new DB.Parameter[] { 
					        new DB.InParameter("@ID", SqlDbType.BigInt, id),
					        new DB.InParameter("@FACTORY_NBR", FactoryNbr),
					        CreateTableParam("@SHIPPING_DATE_CODE", ShippingDate),
					        CreateTableParam("@WARRANTY_DATE_CODE", WarrantyDate),
					        new DB.InParameter("@PHONE", Phone),
					        new DB.InParameter("@PRODUCTION_DEVICE_ORDER_NUMBER", ProductionDeviceOrderNumber),
                            new DB.InParameter("@DEVICE_BATCH_NUMBER", DeviceBatchNumber),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DEVICE_DETAILS[])DB.ExecuteProcedure(
                    "imrse_u_GetDeviceDetailsFilter",
                    new DB.AnalyzeDataSet(GetDeviceDetailsUser),
                    new DB.Parameter[] { 
					CreateTableParam("@ID_DEVICE_DETAILS", IdDeviceDetails),
					CreateTableParam("@SERIAL_NBR", SerialNbr != null ? SerialNbr : DeviceFilter),
					CreateTableParam("@ID_LOCATION", IdLocation != null ? IdLocation : LocationFilter),
					new DB.InParameter("@FACTORY_NBR", FactoryNbr),
					CreateTableParam("@SHIPPING_DATE_CODE", ShippingDate),
					CreateTableParam("@WARRANTY_DATE_CODE", WarrantyDate),
					new DB.InParameter("@PHONE", Phone),
					CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
					CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
					CreateTableParam("@ID_DISTRIBUTOR", (IdDistributor != null && IdDistributor.Count() > 0 ? IdDistributor : DistributorFilter)),
					CreateTableParam("@ID_DEVICE_STATE_TYPE", IdDeviceStateType),
					new DB.InParameter("@PRODUCTION_DEVICE_ORDER_NUMBER", ProductionDeviceOrderNumber),
                    new DB.InParameter("@DEVICE_BATCH_NUMBER", DeviceBatchNumber),
                    CreateTableParam("@ID_DISTRIBUTOR_OWNER", IdDistributorOwner),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }


        private object GetDeviceDetailsUser(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DETAILS[] list = new DB_DEVICE_DETAILS[count];
            bool manufacturingBatchColumnExists = QueryResult.Tables[0].Columns.Contains("MANUFACTURING_BATCH");
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DETAILS insert = new DB_DEVICE_DETAILS();
                insert.ID_DEVICE_DETAILS = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DETAILS"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.FACTORY_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["FACTORY_NBR"]);
                insert.SHIPPING_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SHIPPING_DATE"]));
                insert.WARRANTY_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]));
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                insert.PRODUCTION_DEVICE_ORDER_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["PRODUCTION_DEVICE_ORDER_NUMBER"]);
                insert.DEVICE_BATCH_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_BATCH_NUMBER"]);
                insert.ID_DISTRIBUTOR_OWNER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_OWNER"]);
                if (manufacturingBatchColumnExists)
                    insert.MANUFACTURING_BATCH = GetValue<string>(QueryResult.Tables[0].Rows[i]["MANUFACTURING_BATCH"]); ;
                list[i] = insert;
            }
            return list;
        }
        #endregion
        #region DevicesInvoice

        public DataTable DevicesInvoice(int IdDistributor, DateTime? ReferenceDate, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (DataTable)DB.ExecuteProcedure(
                "sp_DevicesInvoice",
                new DB.AnalyzeDataSet(DevicesInvoice),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DISTRIBUTOR_OWNER", SqlDbType.Int, IdDistributor),
                    new DB.InParameter("@REFERENCE_DATE", SqlDbType.DateTime, ReferenceDate),
                },
                    autoTransaction, transactionLevel, commandTimeout
            );
        }


        private object DevicesInvoice(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
                return QueryResult.Tables[0];
            return null;
        }
        #endregion
        #region GetDistributorAllowedDeviceAttributes

        public Tuple<List<DB_DEVICE_TYPE>, List<DB_DEVICE_ORDER_NUMBER>, List<string>, List<DB_FAULT_CODE>, List<DB_SHIPPING_LIST>, List<string>> GetDistributorAllowedDeviceAttributes(int[] IdDistributor, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
        {
            return (Tuple<List<DB_DEVICE_TYPE>, List<DB_DEVICE_ORDER_NUMBER>, List<string>, List<DB_FAULT_CODE>, List<DB_SHIPPING_LIST>, List<string>>)DB.ExecuteProcedure(
                "imrse_u_GetDistributorAllowedDeviceAttributes",
                new DB.AnalyzeDataSet(GetDistributorAllowedDeviceAttributes),
                new DB.Parameter[] { 
				CreateTableParam("@ID_DISTRIBUTOR", (IdDistributor != null && IdDistributor.Count() > 0 ? IdDistributor : DistributorFilter))
                },
                autoTransaction, transactionLevel, commandTimeout
            );

        }

        private object GetDistributorAllowedDeviceAttributes(DataSet QueryResult)
        {
            Tuple<List<DB_DEVICE_TYPE>, List<DB_DEVICE_ORDER_NUMBER>, List<string>, List<DB_FAULT_CODE>, List<DB_SHIPPING_LIST>, List<string>> ret = new Tuple<List<DB_DEVICE_TYPE>, List<DB_DEVICE_ORDER_NUMBER>, List<string>, List<DB_FAULT_CODE>, List<DB_SHIPPING_LIST>, List<string>>(new List<DB_DEVICE_TYPE>(), new List<DB_DEVICE_ORDER_NUMBER>(), new List<string>(), new List<DB_FAULT_CODE>(), new List<DB_SHIPPING_LIST>(), new List<string>());
            if (QueryResult == null || QueryResult.Tables == null || QueryResult.Tables.Count != 6)
                return ret;
            DataSet ds = new DataSet();
            ds.Tables.Add(QueryResult.Tables[0].Copy());
            ret.Item1.AddRange((GetDeviceType(ds) as DB_DEVICE_TYPE[]).ToList());
            ds = new DataSet();
            ds.Tables.Add(QueryResult.Tables[1].Copy());
            ret.Item2.AddRange((GetDeviceOrderNumber(ds) as DB_DEVICE_ORDER_NUMBER[]).ToList());
            foreach (DataRow drItem in QueryResult.Tables[2].Rows)
            {
                ret.Item3.Add(GetValue<string>(drItem["DEVICE_BATCH_NUMBER"]));
            }
            ds = new DataSet();
            ds.Tables.Add(QueryResult.Tables[3].Copy());
            ret.Item4.AddRange((GetFaultCode(ds) as DB_FAULT_CODE[]).ToList());
            ds = new DataSet();
            ds.Tables.Add(QueryResult.Tables[4].Copy());
            ret.Item5.AddRange((GetShippingList(ds) as DB_SHIPPING_LIST[]).ToList());
            foreach (DataRow drItem in QueryResult.Tables[5].Rows)
            {
                ret.Item6.Add(GetValue<string>(drItem["DEVICE_MANUFACTURING_BATCH_NUMBER"]));
            }

            return ret;
        }
        #endregion

        #endregion

        #region Table DEVICE_DRIVER

        public int SaveDeviceDriver(DB_DEVICE_DRIVER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DRIVER", ToBeSaved.ID_DEVICE_DRIVER);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceDriver",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@PLUGIN_NAME", ToBeSaved.PLUGIN_NAME)
														,new DB.InParameter("@AUTO_UPDATE", ToBeSaved.AUTO_UPDATE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DRIVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        #endregion

        #region Table DEVICE_HIERARCHY
        #region GetDeviceHierarchyFilter
        public DB_DEVICE_HIERARCHY[] GetDeviceHierarchyFilter(long[] IdDeviceHierarchy, long[] SerialNbrParent, int[] IdSlotType, int[] SlotNbr, int[] IdProtocolIn, int[] IdProtocolOut,
                            long[] SerialNbr, bool? IsActive, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, SqlCommand sqlCommand = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdDeviceHierarchy != null && IdDeviceHierarchy.Length > MaxItemsPerSqlTableType) ||
                (SerialNbrParent != null && SerialNbrParent.Length > MaxItemsPerSqlTableType) ||
                (IdSlotType != null && IdSlotType.Length > MaxItemsPerSqlTableType) ||
                (SlotNbr != null && SlotNbr.Length > MaxItemsPerSqlTableType) ||
                (IdProtocolIn != null && IdProtocolIn.Length > MaxItemsPerSqlTableType) ||
                (IdProtocolOut != null && IdProtocolOut.Length > MaxItemsPerSqlTableType) ||
                ((SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType) || (SerialNbr == null && DeviceFilter.Length > MaxItemsPerSqlTableType))))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdDeviceHierarchy != null) args.AddRange(IdDeviceHierarchy.Select(s => new Tuple<int, object>(1, s)));
                if (SerialNbrParent != null) args.AddRange(SerialNbrParent.Select(s => new Tuple<int, object>(2, s)));
                if (IdSlotType != null) args.AddRange(IdSlotType.Select(s => new Tuple<int, object>(3, s)));
                if (SlotNbr != null) args.AddRange(SlotNbr.Select(s => new Tuple<int, object>(4, s)));
                if (IdProtocolIn != null) args.AddRange(IdProtocolIn.Select(s => new Tuple<int, object>(5, s)));
                if (IdProtocolOut != null) args.AddRange(IdProtocolOut.Select(s => new Tuple<int, object>(6, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(7, s)));
                else if (DeviceFilter.Length > 0) args.AddRange(DeviceFilter.Select(s => new Tuple<int, object>(7, s)));

                DB_DEVICE_HIERARCHY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_DEVICE_HIERARCHY[])DB.ExecuteProcedure(
                        "imrse_GetDeviceHierarchyFilterTableArgs",
                        new DB.AnalyzeDataSet(GetDeviceHierarchy),
                        new DB.Parameter[] { 
					        new DB.InParameter("@ID", SqlDbType.BigInt, id),
					        new DB.InParameter("@IS_ACTIVE", IsActive),
					        CreateTableParam("@START_TIME_CODE", StartTime),
					        CreateTableParam("@END_TIME_CODE", EndTime),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout, sqlCommand
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun¹c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_DEVICE_HIERARCHY[])DB.ExecuteProcedure(
                    "imrse_GetDeviceHierarchyFilter",
                    new DB.AnalyzeDataSet(GetDeviceHierarchy),
                    new DB.Parameter[] { 
					    CreateTableParam("@ID_DEVICE_HIERARCHY", IdDeviceHierarchy),
					    CreateTableParam("@SERIAL_NBR_PARENT", SerialNbrParent),
					    CreateTableParam("@ID_SLOT_TYPE", IdSlotType),
					    CreateTableParam("@SLOT_NBR", SlotNbr),
					    CreateTableParam("@ID_PROTOCOL_IN", IdProtocolIn),
					    CreateTableParam("@ID_PROTOCOL_OUT", IdProtocolOut),
					    CreateTableParam("@SERIAL_NBR", SerialNbr ?? DeviceFilter),
					    new DB.InParameter("@IS_ACTIVE", IsActive),
					    CreateTableParam("@START_TIME_CODE", StartTime),
					    CreateTableParam("@END_TIME_CODE", EndTime),
					    new DB.InParameter("@TOP_COUNT", topCount),
					    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout, sqlCommand
                );
            }
        }
        #endregion

        #region GetDeviceHierarchyDataChange

        public DB_DEVICE_HIERARCHY[] GetDeviceHierarchyDataChange(DateTime Time, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (DB_DEVICE_HIERARCHY[])DB.ExecuteProcedure(
                "etl_GetDeviceHierarchyDataChange",
                new DB.AnalyzeDataSet(GetDeviceHierarchyDataChange),
                new DB.Parameter[] { 
					new DB.InParameter("@TIME", SqlDbType.DateTime, ConvertTimeZoneToUtcTime(Time))},
                    AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDeviceHierarchyDataChange(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_HIERARCHY[] list = new DB_DEVICE_HIERARCHY[count];
            string ID_DEVICE_HIERARCHY = "ID_DEVICE_HIERARCHY";
            if (!QueryResult.Tables[0].Columns.Contains("ID_DEVICE_HIERARCHY"))
                ID_DEVICE_HIERARCHY = "changed_id";
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_HIERARCHY insert = new DB_DEVICE_HIERARCHY();
                insert.ID_DEVICE_HIERARCHY = GetValue<long>(QueryResult.Tables[0].Rows[i][ID_DEVICE_HIERARCHY]);
                insert.SERIAL_NBR_PARENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PARENT"]);
                insert.ID_SLOT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SLOT_TYPE"]);
                insert.SLOT_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["SLOT_NBR"]);
                insert.ID_PROTOCOL_IN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_IN"]);
                insert.ID_PROTOCOL_OUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_OUT"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                if (QueryResult.Tables[0].Columns.Contains("START_TIME"))
                {
                    insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                }
                if (QueryResult.Tables[0].Columns.Contains("END_TIME"))
                {
                    insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                }
                list[i] = insert;
            }
            return list;
        }

        #endregion
        #endregion

        #region Table DEVICE_PATTERN

        #region GetDevicesPattern

        public Dictionary<long, long> GetDevicesPattern()
        {
            return (Dictionary<long, long>)DB.ExecuteProcedure(
                "imrse_u_GetDevicePattern",
                new DB.AnalyzeDataSet(GetDevicesPattern),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)}
            );
        }


        private object GetDevicesPattern(DataSet QueryResult)
        {
            Dictionary<long, long> result = new Dictionary<long, long>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    long serialNbr = GetValue<long>(drItem["SERIAL_NBR"]);
                    long serialNbrPattern = GetValue<long>(drItem["SERIAL_NBR_PATTERN"]);
                    result.Add(serialNbr, serialNbrPattern);
                }
            }
            return result;
        }

        #endregion

        #region GetPatternDevices

        public DataTable GetPatternDevices(int IdDeviceType, int? IdDistributor)
        {
            DataTable Result = (DataTable)DB.ExecuteProcedure("tmq_GetPatternDevices", new DB.AnalyzeDataSet(GetPatternDevices),
                new DB.Parameter[]{
                    new DB.InParameter("@ID_DEVICE_TYPE", IdDeviceType),
					new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, IdDistributor)
                }, AutoTransaction: false, IsolationLevel: IsolationLevel.ReadUncommitted, CommandTimeout: 0);

            return Result;
        }

        private object GetPatternDevices(DataSet QueryResult)
        {
            return QueryResult.Tables[0];
        }

        #endregion

        #endregion

        #region Table DEPOSITORY_ELEMENT

        public void SaveMultipleDepositoryElement(List<DB_DEPOSITORY_ELEMENT> ToBeSaved)
        {
            DataTable toIntert = new DataTable();

            #region PrepareData
            toIntert.Columns.Add(new DataColumn("ID_DEPOSITORY_ELEMENT", System.Type.GetType("System.Int64")));
            toIntert.Columns.Add(new DataColumn("ID_LOCATION", System.Type.GetType("System.Int64")));
            toIntert.Columns.Add(new DataColumn("ID_TASK", System.Type.GetType("System.Int32")));
            toIntert.Columns.Add(new DataColumn("ID_PACKAGE", System.Type.GetType("System.Int32")));
            toIntert.Columns.Add(new DataColumn("ID_ARTICLE", System.Type.GetType("System.Int64")));
            toIntert.Columns.Add(new DataColumn("ID_DEVICE_TYPE_CLASS", System.Type.GetType("System.Int32")));
            toIntert.Columns.Add(new DataColumn("ID_DEVICE_STATE_TYPE", System.Type.GetType("System.Int32")));
            toIntert.Columns.Add(new DataColumn("SERIAL_NBR", System.Type.GetType("System.Int64")));
            toIntert.Columns.Add(new DataColumn("START_DATE", typeof(DateTime)));
            toIntert.Columns.Add(new DataColumn("END_DATE", typeof(DateTime)));
            toIntert.Columns.Add(new DataColumn("REMOVED", System.Type.GetType("System.Boolean")));
            toIntert.Columns.Add(new DataColumn("ORDERED", System.Type.GetType("System.Boolean")));
            toIntert.Columns.Add(new DataColumn("ACCEPTED", System.Type.GetType("System.Boolean")));
            toIntert.Columns.Add(new DataColumn("NOTES", System.Type.GetType("System.String")));
            toIntert.Columns.Add(new DataColumn("EXTERNAL_SN", System.Type.GetType("System.String")));
            toIntert.Columns.Add(new DataColumn("ID_REFERENCE", System.Type.GetType("System.Int64")));

            foreach (DB_DEPOSITORY_ELEMENT deItem in ToBeSaved)
            {
                toIntert.Rows.Add(deItem.ID_DEPOSITORY_ELEMENT,
                                  deItem.ID_LOCATION,
                                  deItem.ID_TASK,
                                  deItem.ID_PACKAGE,
                                  deItem.ID_ARTICLE,
                                  deItem.ID_DEVICE_TYPE_CLASS,
                                  deItem.ID_DEVICE_STATE_TYPE,
                                  deItem.SERIAL_NBR,
                                  Convert.ToDateTime(deItem.START_DATE.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                                  deItem.END_DATE.HasValue ? (DateTime?)Convert.ToDateTime(deItem.END_DATE.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")) : null,
                                  deItem.REMOVED,
                                  deItem.ORDERED,
                                  deItem.ACCEPTED,
                                  deItem.NOTES,
                                  deItem.EXTERNAL_SN,
                                  deItem.ID_REFERENCE);
            }
            #endregion

            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveMultipleDepositoryElement",
                new DB.Parameter[] {
										new DB.InParameter("@DEPOSITORY_ELEMENT_TBL", SqlDbType.Structured, toIntert)
									}
            );

        }

        public DB_DEPOSITORY_ELEMENT[] GetDepositoryMagazineContentForOperator(int[] IdOperator)
        {
            return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
                "imrse_u_GetDepositoryMagazineContentForOperator",
                new DB.AnalyzeDataSet(GetDepositoryElement),
                new DB.Parameter[] { 
                    CreateTableParam("@ID_OPERATOR", IdOperator)
                }
            );
        }

        public DB_DEPOSITORY_ELEMENT[] GetTaskPackageDataForDistributor(int[] IdDistributor, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
                "imrse_u_GetTaskPackageDataForDistributor",
                new DB.AnalyzeDataSet(GetDepositoryElement),
                new DB.Parameter[] { 
                    CreateTableParam("@ID_DISTRIBUTOR", IdDistributor)
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DataTable GetMountedDevicesFromServicePool(int IdDistributor, long SerialNbrService)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetMountedDevicesFromServicePool",
                new DB.AnalyzeDataSet(GetMountedDevicesFromServicePool),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, IdDistributor),
                    new DB.InParameter("@SERIAL_NBR_SERVICE", SqlDbType.BigInt, SerialNbrService)
                }
            );
        }

        private object GetMountedDevicesFromServicePool(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
                return QueryResult.Tables[0];
            return null;
        }

        #endregion

        #region Table DEVICE_STATE_HISTORY

        public DB_DEVICE_STATE_HISTORY[] GetDeviceStateHistoryByDistributor(int[] IdDistributor, bool IncludeShipping, bool IncludeService)
        {
            return (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_u_GetDeviceStateHistoryByDistributor",
                new DB.AnalyzeDataSet(GetDeviceStateHistory),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_DISTRIBUTOR", IdDistributor)
                        ,new DB.InParameter("@INCLUDE_SHIPPING", IncludeShipping)
                        ,new DB.InParameter("@INCLUDE_SERVICE", IncludeService)
					}
            );
        }

        #endregion

        #region Table DEVICE_STATE_TYPE
        public int SaveDeviceStateType(DB_DEVICE_STATE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceStateType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_STATE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table DEVICE_TYPE
        public void AddDeviceType(DB_DEVICE_TYPE ToBeSaved)
        {
            DB.InParameter InsertId = new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_AddDeviceType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DEVICE_TYPE_CLASS", ToBeSaved.ID_DEVICE_TYPE_CLASS)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@TWO_WAY_TRANS_AVAILABLE", ToBeSaved.TWO_WAY_TRANS_AVAILABLE)
														,new DB.InParameter("@ID_DEVICE_DRIVER", ToBeSaved.ID_DEVICE_DRIVER)
														,new DB.InParameter("@ID_PROTOCOL", ToBeSaved.ID_PROTOCOL)
														,new DB.InParameter("@DEFAULT_ID_DEVICE_ODER_NUMBER", ToBeSaved.DEFAULT_ID_DEVICE_ODER_NUMBER)
									}
            );
        }

        public int SaveDeviceType(DB_DEVICE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DEVICE_TYPE_CLASS", ToBeSaved.ID_DEVICE_TYPE_CLASS)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@TWO_WAY_TRANS_AVAILABLE", ToBeSaved.TWO_WAY_TRANS_AVAILABLE)
														,new DB.InParameter("@ID_DEVICE_DRIVER", ToBeSaved.ID_DEVICE_DRIVER)
														,new DB.InParameter("@ID_PROTOCOL", ToBeSaved.ID_PROTOCOL)
														,new DB.InParameter("@DEFAULT_ID_DEVICE_ODER_NUMBER", ToBeSaved.DEFAULT_ID_DEVICE_ODER_NUMBER)
                                                        //Projekt: DEVICE_TYPE bez FRAME_DEFINITION
														//,new DB.InParameter("@FRAME_DEFINITION", SqlDbType.Image, ToBeSaved.FRAME_DEFINITION)//ParamObject("@FRAME_DEFINITION", ToBeSaved.FRAME_DEFINITION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table DEVICE_TYPE_CLASS
        public int SaveDeviceTypeClass(DB_DEVICE_TYPE_CLASS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_CLASS", ToBeSaved.ID_DEVICE_TYPE_CLASS);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceTypeClass",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_CLASS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table DISTRIBUTOR
        public void AddDistributor(DB_DISTRIBUTOR ToBeSaved)
        {
            DB.InParameter InsertId = new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_AddDistributor",
                new DB.Parameter[] {
			        InsertId
				    ,new DB.InParameter("@NAME", ToBeSaved.NAME)
					,new DB.InParameter("@CITY", ToBeSaved.CITY)
					,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
			}
            );
        }

        public DB_DISTRIBUTOR[] GetDistributor(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DISTRIBUTOR[])DB.ExecuteProcedure(
                "imrse_u_GetDistributor",
                new DB.AnalyzeDataSet(GetDistributor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter != null ? DistributorFilter : new int[] {})
							
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_DISTRIBUTOR[] GetDistributor(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DISTRIBUTOR[])DB.ExecuteProcedure(
                "imrse_u_GetDistributor",
                new DB.AnalyzeDataSet(GetDistributor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR",  Ids != null ? Ids : DistributorFilter)
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetDistributor(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DISTRIBUTOR[] list = new DB_DISTRIBUTOR[count];
            for (int i = 0; i < count; i++)
            {
                DB_DISTRIBUTOR insert = new DB_DISTRIBUTOR();
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.CITY = GetValue<string>(QueryResult.Tables[0].Rows[i]["CITY"]);
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDistributor(DB_DISTRIBUTOR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDistributor",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@CITY", ToBeSaved.CITY)
                                                        ,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDistributor(DB_DISTRIBUTOR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDistributor",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DISTRIBUTOR", toBeDeleted.ID_DISTRIBUTOR)			
		}
            );
        }

        public DataTable GetDistributorImrServer(int[] IdDistributor)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetDistributorImrServer",
                new DB.AnalyzeDataSet(GetDistributorImrServer),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor)
							
                }
            );
        }

        private object GetDistributorImrServer(DataSet QuerryResult)
        {
            if (QuerryResult != null && QuerryResult.Tables != null && QuerryResult.Tables.Count > 0)
                return QuerryResult.Tables[0];
            return null;
        }

        #endregion

        #region Table FAULT_CODE

        public DB_FAULT_CODE_DEVICE[] GetFaultCodeDevice(int[] IdDistributor, int[] IdDeviceType, int[] IdDeviceOrderNumber, int[] IdFaultCode,
            string BatchNumber, string ManufacturingBatchNumber,
            bool ShowServiceFaultCodeView,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (DB_FAULT_CODE_DEVICE[])DB.ExecuteProcedure(
                "imrse_u_GetFaultCodeDevicesFilter",
                new DB.AnalyzeDataSet(GetFaultCodeDevice),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_DISTRIBUTOR", (IdDistributor == null || IdDistributor.Count() > 0 ? IdDistributor : DistributorFilter)),
			        CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
			        CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
			        CreateTableParam("@ID_FAULT_CODE", IdFaultCode),
                    new DB.InParameter("@BATCH_NUMBER", SqlDbType.NVarChar, BatchNumber),
                    new DB.InParameter("@MANUFACTURING_BATCH_NUMBER", SqlDbType.NVarChar, ManufacturingBatchNumber),
                    new DB.InParameter("@SHOW_SERVICE_FAULT_CODE_VIEW", SqlDbType.Bit, ShowServiceFaultCodeView)
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_FAULT_CODE_DEVICE[] GetFaultCodeDeviceForShippingList(int[] IdShippingList, int[] IdDistributor, int[] IdDeviceType, int[] IdDeviceOrderNumber, int[] IdFaultCode)
        {
            return (DB_FAULT_CODE_DEVICE[])DB.ExecuteProcedure(
                "imrse_u_GetFaultCodeDevicesForShippingList",
                new DB.AnalyzeDataSet(GetFaultCodeDevice),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_SHIPPING_LIST", IdShippingList),
			        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor),
			        CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
			        CreateTableParam("@ID_DEVICE_ORDER_NUMBER", IdDeviceOrderNumber),
			        CreateTableParam("@ID_FAULT_CODE", IdFaultCode)
                }
            );
        }

        private object GetFaultCodeDevice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<DB_FAULT_CODE_DEVICE> retList = new List<DB_FAULT_CODE_DEVICE>();
            for (int i = 0; i < count; i++)
            {
                DB_FAULT_CODE_DEVICE insert = new DB_FAULT_CODE_DEVICE();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_FAULT_CODE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE"]);
                insert.FAULT_CODE_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["FAULT_CODE_DATE"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.BATCH_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["BATCH_NUMBER"]);
                insert.MANUFACTURING_BATCH_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["MANUFACTURING_BATCH_NUMBER"]);
                insert.IS_BIG_FAULT = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_BIG_FAULT"]);
                retList.Add(insert);
            }
            return retList.ToArray();
        }

        #endregion

        #region Table HOLIDAY_DATE
        #region GetWorkingDate

        public DateTime GetWorkingDate(DateTime startDate, int days, string countryCode)
        {
            return (DateTime)DB.ExecuteProcedure(
                "imrse_u_GetWorkingDate",
                new DB.AnalyzeDataSet(GetWorkingDate),
                new DB.Parameter[] { 
					new DB.InParameter("@START_DATE", startDate),
                    new DB.InParameter("@DAYS", days),
					new DB.InParameter("@COUNTRY_CODE", countryCode)
                });
        }

        private object GetWorkingDate(DataSet QueryResult)
        {
            DateTime insert = GetValue<DateTime>(QueryResult.Tables[0].Rows[0]["DATE"]);
            return insert;
        }

        #endregion
        #endregion

        #region Table IMR_SERVER
        public int SaveImrServer(DB_IMR_SERVER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_IMR_SERVER", ToBeSaved.ID_IMR_SERVER);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveImrServer",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@IP_ADDRESS", ToBeSaved.IP_ADDRESS)
														,new DB.InParameter("@DNS_NAME", ToBeSaved.DNS_NAME)
														,new DB.InParameter("@WEBSERVICE_ADDRESS", ToBeSaved.WEBSERVICE_ADDRESS)
														,new DB.InParameter("@WEBSERVICE_TIMEOUT", ToBeSaved.WEBSERVICE_TIMEOUT)
														,new DB.InParameter("@LOGIN", ToBeSaved.LOGIN)
														,new DB.InParameter("@PASSWORD", ToBeSaved.PASSWORD)
														,new DB.InParameter("@STANDARD_DESCRIPTION", ToBeSaved.STANDARD_DESCRIPTION)
														,new DB.InParameter("@IS_GOOD", ToBeSaved.IS_GOOD)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@UTDC_SSEC_USER", ToBeSaved.UTDC_SSEC_USER)
														,new DB.InParameter("@UTDC_SSEC_PASS", ToBeSaved.UTDC_SSEC_PASS)
														,new DB.InParameter("@COLLATION", ToBeSaved.COLLATION)
														,new DB.InParameter("@DB_NAME", ToBeSaved.DB_NAME)
														,new DB.InParameter("@IMR_SERVER_VERSION", ToBeSaved.IMR_SERVER_VERSION)
														,new DB.InParameter("@IS_IMRLT", ToBeSaved.IS_IMRLT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_IMR_SERVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table FILE

        public DB_FILE[] GetFile(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_FILE[])DB.ExecuteProcedure(
                "imrse_u_GetFile",
                new DB.AnalyzeDataSet(GetFile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FILE", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_FILE[] GetFile(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_FILE[])DB.ExecuteProcedure(
                "imrse_u_GetFile",
                new DB.AnalyzeDataSet(GetFile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FILE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetFile(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_FILE[] list = new DB_FILE[count];

            for (int i = 0; i < count; i++)
            {
                DB_FILE insert = new DB_FILE();
                insert.ID_FILE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_FILE"]);
                insert.PHYSICAL_FILE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHYSICAL_FILE_NAME"]);
                //insert.FILE_BYTES = GetValue(QueryResult.Tables[0].Rows[i]["FILE_BYTES"]);
                insert.SIZE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SIZE"]);
                insert.INSERT_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["INSERT_DATE"]));
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["VERSION"]);
                insert.LAST_DOWNLOADED = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["LAST_DOWNLOADED"]));
                if (QueryResult.Tables[0].Columns.Contains("IS_BLOCKED"))
                    insert.IS_BLOCKED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_BLOCKED"]);
                list[i] = insert;
            }
            return list;
        }

        public DB_FILE[] GetFileFilter(long[] IdFile, string PhysicalFileName, long[] Size, TypeDateTimeCode InsertDate, string Description, string Version,
                            TypeDateTimeCode LastDownloaded, bool? IsBlocked, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFileFilter"));

            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(CreateTableParam("@ID_FILE", IdFile));
            parameters.Add(new DB.InParameter("@PHYSICAL_FILE_NAME", PhysicalFileName));
            parameters.Add(CreateTableParam("@SIZE", Size));
            parameters.Add(CreateTableParam("@INSERT_DATE_CODE", InsertDate));
            parameters.Add(new DB.InParameter("@DESCRIPTION", Description));
            parameters.Add(new DB.InParameter("@VERSION", Version));
            parameters.Add(CreateTableParam("@LAST_DOWNLOADED_CODE", LastDownloaded));
            if (IsParameterDefined("imrse_u_GetFileFilter", "@IS_BLOCKED", this))
                parameters.Add(new DB.InParameter("@IS_BLOCKED", IsBlocked));
            parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));

            return (DB_FILE[])DB.ExecuteProcedure(
                "imrse_u_GetFileFilter",
                new DB.AnalyzeDataSet(GetFile),
                parameters.ToArray(),
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        public long SaveFile(DB_FILE ToBeSaved)
        {
            List<DB.Parameter> parameters = new List<Data.DB.DB.Parameter>();
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_FILE", ToBeSaved.ID_FILE);

            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@PHYSICAL_FILE_NAME", ToBeSaved.PHYSICAL_FILE_NAME));
            parameters.Add(new DB.InParameter("@SIZE", ToBeSaved.SIZE));
            parameters.Add(new DB.InParameter("@INSERT_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.INSERT_DATE)));
            parameters.Add(new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION));
            parameters.Add(new DB.InParameter("@VERSION", ToBeSaved.VERSION));
            parameters.Add(new DB.InParameter("@LAST_DOWNLOADED", ConvertTimeZoneToUtcTime(ToBeSaved.LAST_DOWNLOADED)));
            if (IsParameterDefined("imrse_u_SaveFile", "@IS_BLOCKED", this))
                parameters.Add(new DB.InParameter("@IS_BLOCKED", ToBeSaved.IS_BLOCKED));

            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveFile",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_FILE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteFile(DB_FILE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelFile",
                new DB.Parameter[] {
            new DB.InParameter("@ID_FILE", toBeDeleted.ID_FILE)			
		}
            );
        }

        public void SaveFileContent(DB_FILE ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveFileContent",
                new DB.Parameter[]
                    {
                         ParamObject("@ID_FILE", ToBeSaved.ID_FILE)
                        , ParamObject("@FILE_BYTES", ToBeSaved.FILE_BYTES)
                    }
                );
        }

        public void GetFileContent(DB_FILE ToBeSaved)
        {
            DB_FILE[] Files = (DB_FILE[])DB.ExecuteProcedure(
                "imrse_u_GetFileContent",
                new DB.AnalyzeDataSet(GetFileContent),
                new DB.Parameter[] { 
			        new DB.InParameter("@ID_FILE", ToBeSaved.ID_FILE)
			    }
            );
            if (Files.Length > 0)
                ToBeSaved.FILE_BYTES = Files[0].FILE_BYTES;
        }

        private object GetFileContent(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_FILE[] list = new DB_FILE[count];
            for (int i = 0; i < count; i++)
            {
                DB_FILE insert = new DB_FILE();
                insert.FILE_BYTES = GetValue(QueryResult.Tables[0].Rows[i]["FILE_BYTES"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table LANGUAGE
        public int SaveLanguage(DB_LANGUAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveLanguage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@CULTURE_CODE", ToBeSaved.CULTURE_CODE)
														,new DB.InParameter("@CULTURE_LANGID", ToBeSaved.CULTURE_LANGID)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LANGUAGE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table LOCATION_ENTRANCE

        public long SaveLocationEntrance(DB_LOCATION_ENTRANCE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveLocationEntrance",
                new DB.Parameter[] {
			        InsertId,
					new DB.InParameter("@ENTRANCE_INDEX_NBR", ToBeSaved.ENTRANCE_INDEX_NBR),
					new DB.InParameter("@SECTION_COUNT", ToBeSaved.SECTION_COUNT),
				    new DB.InParameter("@NAME", ToBeSaved.NAME),
                    new DB.InParameter("@FLOOR_PLAN", SqlDbType.Image, ToBeSaved.FLOOR_PLAN)
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationEntrance(DB_LOCATION_ENTRANCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelLocationEntrance",
                new DB.Parameter[] {
                        new DB.InParameter("@ID_LOCATION", toBeDeleted.ID_LOCATION),
		                new DB.InParameter("@ENTRANCE_INDEX_NBR", toBeDeleted.ENTRANCE_INDEX_NBR)
		        }
            );
        }

        #endregion

        #region Table LOCATION_ENTRANCE_FLOOR

        public long SaveLocationEntranceFloor(DB_LOCATION_ENTRANCE_FLOOR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveLocationEntranceFloor",
                new DB.Parameter[] {
			            InsertId,
						new DB.InParameter("@ENTRANCE_INDEX_NBR", ToBeSaved.ENTRANCE_INDEX_NBR),
						new DB.InParameter("@FLOOR_INDEX_NBR", ToBeSaved.FLOOR_INDEX_NBR),
						new DB.InParameter("@NAME", ToBeSaved.NAME)
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationEntranceFloor(DB_LOCATION_ENTRANCE_FLOOR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelLocationEntranceFloor",
                new DB.Parameter[] {
                        new DB.InParameter("@ID_LOCATION", toBeDeleted.ID_LOCATION),
			            new DB.InParameter("@ENTRANCE_INDEX_NBR", toBeDeleted.ENTRANCE_INDEX_NBR),
                        new DB.InParameter("@FLOOR_INDEX_NBR", toBeDeleted.FLOOR_INDEX_NBR)
		        }
            );
        }
        #endregion

        #region Table LOCATION

        public DB_DATA[] GetLocationsIncorrectAssignment()
        {
            return (DB_DATA[])DB.ExecuteProcedure(
                   "imrse_u_GetLocationsIncorrectAssignment",
                   new DB.AnalyzeDataSet(GetData),
                   new DB.Parameter[] { }
               );
        }

        public DB_LOCATION_MAPPING[] GetLocationIdMappings(long[] IdLocation, int IdImrServer, bool IsIdLocationFromImrServer)
        {
            return (DB_LOCATION_MAPPING[])DB.ExecuteProcedure(
                   "imrse_u_GetLocationIdMappings",
                   new DB.AnalyzeDataSet(GetLocationIdMappings),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_LOCATION", IdLocation),
			            new DB.InParameter("@ID_IMR_SERVER", IdImrServer), 
			            new DB.InParameter("@IS_ID_LOCATION_FROM_IMR_SERVER", IsIdLocationFromImrServer) 
                   }, AutoTransaction: false, CommandTimeout: 1800
               );
        }

        private object GetLocationIdMappings(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_MAPPING()
            {
                ID_LOCATION_IMR_SERVER = GetNullableValue<long>(row["ID_LOCATION_IMR_SERVER"]),
                ID_LOCATION_IMRSC = GetNullableValue<long>(row["ID_LOCATION_IMRSC"])
            }).ToArray();
        }



        public DB_LOCATION[] GetLocationMagazineDistinct(int[] IdLocationType, int[] IdDistributor, bool AllowBlankCid,
            bool autoTransaction, IsolationLevel transactionLevel, int commandTimeout)
        {
            return (DB_LOCATION[])DB.ExecuteProcedure(
                   "imrse_u_GetLocationMagazineDistinct",
                   new DB.AnalyzeDataSet(GetLocation),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_LOCATION_TYPE", IdLocationType),
			            CreateTableParam("@ID_DISTRIBUTOR", IdDistributor), 
			            new DB.InParameter("@ALLOW_BLANK_CID", AllowBlankCid) 
                   }, autoTransaction, transactionLevel, commandTimeout
               );
        }

        #endregion

        #region Table LOCATION_EQUIPMENT

        public DB_LOCATION_EQUIPMENT[] GetLocationEquipment()
        {
            return (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
                "imrse_u_GetLocationEquipment",
                new DB.AnalyzeDataSet(GetLocationEquipment),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_LOCATION_EQUIPMENT", new long[] {}),
                    CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
				}
            );
        }

        public DB_LOCATION_EQUIPMENT[] GetLocationEquipment(long[] Ids)
        {
            return (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
                "imrse_u_GetLocationEquipment",
                new DB.AnalyzeDataSet(GetLocationEquipment),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_LOCATION_EQUIPMENT", Ids),
                    CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
				}
            );
        }
        #region GetLocationEquipmentFilter
        public DB_LOCATION_EQUIPMENT[] GetLocationEquipmentFilter(long[] IdLocationEquipment, long[] IdLocation, long[] IdMeter, long[] SerialNbr, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, SqlCommand sqlCommand = null
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEquipmentFilter"));

            DB_LOCATION_EQUIPMENT[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdLocationEquipment != null && IdLocationEquipment.Length > MaxItemsPerSqlTableType) ||
                (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
                (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
                (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdLocationEquipment != null) args.AddRange(IdLocationEquipment.Select(s => new Tuple<int, object>(1, s)));
                if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(2, s)));
                if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
                if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(4, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
                        "imrse_GetLocationEquipmentFilterTableArgs",
                        new DB.AnalyzeDataSet(GetLocationEquipment),
                        new DB.Parameter[] { 
					        new DB.InParameter("@ID", SqlDbType.BigInt, id),
					        CreateTableParam("@START_TIME_CODE", StartTime),
					        CreateTableParam("@END_TIME_CODE", EndTime),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout, sqlCommand
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usun¹c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
                    "imrse_GetLocationEquipmentFilter",
                    new DB.AnalyzeDataSet(GetLocationEquipment),
                    new DB.Parameter[] { 
				        CreateTableParam("@ID_LOCATION_EQUIPMENT", IdLocationEquipment),
				        CreateTableParam("@ID_LOCATION", IdLocation),
				        CreateTableParam("@ID_METER", IdMeter),
				        CreateTableParam("@SERIAL_NBR", SerialNbr),
				        CreateTableParam("@START_TIME_CODE", StartTime),
				        CreateTableParam("@END_TIME_CODE", EndTime),
				        new DB.InParameter("@TOP_COUNT", topCount),
				        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout, sqlCommand
                );
            }

            return tmpResult != null ? tmpResult.Where(q =>
                (IdLocation != null || !q.ID_LOCATION.HasValue || LocationFilterDict.Count == 0 || LocationFilterDict.ContainsKey(q.ID_LOCATION.Value))
                && (IdMeter != null || !q.ID_METER.HasValue || MeterFilterDict.Count == 0 || MeterFilterDict.ContainsKey(q.ID_METER.Value))
                && (SerialNbr != null || !q.SERIAL_NBR.HasValue || DeviceFilterDict.Count == 0 || DeviceFilterDict.ContainsKey(q.SERIAL_NBR.Value))
                ).ToArray() : new DB_LOCATION_EQUIPMENT[0];

            #region // old version
            //if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            //    throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEquipmentFilter"));

            //if (UseBulkInsertSqlTableType && (
            //    (IdLocationEquipment != null && IdLocationEquipment.Length > MaxItemsPerSqlTableType) ||
            //    (IdLocation != null && IdLocation.Length > MaxItemsPerSqlTableType) ||
            //    (IdMeter != null && IdMeter.Length > MaxItemsPerSqlTableType) ||
            //    (SerialNbr != null && SerialNbr.Length > MaxItemsPerSqlTableType)
            //    ))
            //{
            //    long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            //    List<Tuple<int, object>> args = new List<Tuple<int, object>>();
            //    if (IdLocationEquipment != null) args.AddRange(IdLocationEquipment.Select(s => new Tuple<int, object>(1, s)));
            //    if (IdLocation != null) args.AddRange(IdLocation.Select(s => new Tuple<int, object>(2, s)));
            //    if (IdMeter != null) args.AddRange(IdMeter.Select(s => new Tuple<int, object>(3, s)));
            //    if (SerialNbr != null) args.AddRange(SerialNbr.Select(s => new Tuple<int, object>(4, s)));

            //    DB_LOCATION_EQUIPMENT[] result = null;
            //    try
            //    {
            //        InsertImrseProcArg(id, args);
            //        result = (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
            //            "imrse_GetLocationEquipmentFilterTableArgs",
            //            new DB.AnalyzeDataSet(GetLocationEquipment),
            //            new DB.Parameter[] { 
            //                new DB.InParameter("@ID", SqlDbType.BigInt, id),
            //                CreateTableParam("@START_TIME_CODE", StartTime),
            //                CreateTableParam("@END_TIME_CODE", EndTime),
            //                new DB.InParameter("@TOP_COUNT", topCount),
            //                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //            autoTransaction, transactionLevel, commandTimeout, sqlCommand
            //        );
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        // Usun¹c po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
            //        DeleteImrseProcArg(id);
            //    }
            //    return result;
            //}
            //else
            //{
            //    return (DB_LOCATION_EQUIPMENT[])DB.ExecuteProcedure(
            //        "imrse_GetLocationEquipmentFilter",
            //        new DB.AnalyzeDataSet(GetLocationEquipment),
            //        new DB.Parameter[] { 
            //            CreateTableParam("@ID_LOCATION_EQUIPMENT", IdLocationEquipment),
            //            CreateTableParam("@ID_LOCATION", IdLocation),
            //            CreateTableParam("@ID_METER", IdMeter),
            //            CreateTableParam("@SERIAL_NBR", SerialNbr),
            //            CreateTableParam("@START_TIME_CODE", StartTime),
            //            CreateTableParam("@END_TIME_CODE", EndTime),
            //            new DB.InParameter("@TOP_COUNT", topCount),
            //            new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
            //        autoTransaction, transactionLevel, commandTimeout, sqlCommand
            //    );
            //}
            #endregion
        }
        #endregion
        #endregion

        #region Table LOCATION_STATE_TYPE
        public int SaveLocationStateType(DB_LOCATION_STATE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_STATE_TYPE", ToBeSaved.ID_LOCATION_STATE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveLocationStateType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_STATE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table LOCATION_TYPE
        public int SaveLocationType(DB_LOCATION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_TYPE", ToBeSaved.ID_LOCATION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveLocationType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table METER

        public DB_DATA[] GetMetersIncorrectAssignment()
        {
            return (DB_DATA[])DB.ExecuteProcedure(
                   "imrse_u_GetMetersIncorrectAssignment",
                   new DB.AnalyzeDataSet(GetData),
                   new DB.Parameter[] { }
               );
        }



        public DB_METER_MAPPING[] GetMeterIdMappings(long[] IdMeter, int IdImrServer, bool IsIdMeterFromImrServer)
        {
            return (DB_METER_MAPPING[])DB.ExecuteProcedure(
                   "imrse_u_GetMeterIdMappings",
                   new DB.AnalyzeDataSet(GetMeterIdMappings),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_METER", IdMeter),
			            new DB.InParameter("@ID_IMR_SERVER", IdImrServer), 
			            new DB.InParameter("@IS_ID_METER_FROM_IMR_SERVER", IsIdMeterFromImrServer) 
                   }, 1800
               );
        }

        private object GetMeterIdMappings(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_METER_MAPPING()
            {
                ID_METER_IMR_SERVER = GetNullableValue<long>(row["ID_METER_IMR_SERVER"]),
                ID_METER_IMRSC = GetNullableValue<long>(row["ID_METER_IMRSC"])
            }).ToArray();
        }

        #endregion

        #region Table METER_TYPE
        public int SaveMeterType(DB_METER_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER_TYPE", ToBeSaved.ID_METER_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveMeterType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_METER_TYPE_CLASS", ToBeSaved.ID_METER_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table METER_TYPE_CLASS
        public int SaveMeterTypeClass(DB_METER_TYPE_CLASS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER_TYPE_CLASS", ToBeSaved.ID_METER_TYPE_CLASS);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveMeterTypeClass",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER_TYPE_CLASS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table METER_TYPE_DEVICE_TYPE
        public DB_METER_TYPE_DEVICE_TYPE[] GetMeterTypeDeviceType()
        {
            return GetMeterTypeDeviceType(null, null);
        }

        public DB_METER_TYPE_DEVICE_TYPE[] GetMeterTypeDeviceType(int? idMeterType, int? idDeviceType)
        {
            DB.InParameter idMeterTypeParam;
            if (idMeterType != null)
                idMeterTypeParam = new DB.InParameter("@ID_METER_TYPE", idMeterType.Value);
            else
                idMeterTypeParam = new DB.InNullParameter("@ID_METER_TYPE");

            DB.InParameter idDeviceTypeParam;
            if (idDeviceType != null)
                idDeviceTypeParam = new DB.InParameter("@ID_DEVICE_TYPE", idDeviceType.Value);
            else
                idDeviceTypeParam = new DB.InNullParameter("@ID_DEVICE_TYPE");

            return (DB_METER_TYPE_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_u_GetMeterTypeDeviceType",
                new DB.AnalyzeDataSet(GetMeterTypeDeviceType),
                new DB.Parameter[] { 
					idMeterTypeParam,
					idDeviceTypeParam
				}
            );
        }

        private object GetMeterTypeDeviceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER_TYPE_DEVICE_TYPE[] list = new DB_METER_TYPE_DEVICE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER_TYPE_DEVICE_TYPE insert = new DB_METER_TYPE_DEVICE_TYPE();
                insert.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public void SaveMeterTypeDeviceType(DB_METER_TYPE_DEVICE_TYPE ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMeterTypeDeviceType",
                new DB.Parameter[] {
					new DB.InParameter("@ID_METER_TYPE", ToBeSaved.ID_METER_TYPE),
					new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
				}
            );
        }

        public void DeleteMeterTypeDeviceType(DB_METER_TYPE_DEVICE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterTypeDeviceType",
                new DB.Parameter[] {
					new DB.InParameter("@ID_METER_TYPE", toBeDeleted.ID_METER_TYPE),
					new DB.InParameter("@ID_DEVICE_TYPE", toBeDeleted.ID_DEVICE_TYPE)
				}
            );
        }
        #endregion

        #region Table MODULE
        public int SaveModule(DB_MODULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE", ToBeSaved.ID_MODULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveModule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table OPERATOR
        public User[] GetSitaOperators(int DistributorID)
        {
            return (User[])DB.ExecuteProcedure(
                "dataservice_GetSitaOperatorsWithActivities",
                new DB.AnalyzeDataSet(GetSitaOperators),
                new DB.Parameter[] { 
			        new DB.InParameter("@DistributorID", DistributorID),                    
				}
            );
        }

        public DB_OPERATOR_ROLE[] GetSitaOperatorRole(int idOperator)
        {
            DB.InParameter idOperatorParam;
            idOperatorParam = new DB.InParameter("@ID_OPERATOR", idOperator);
            return (DB_OPERATOR_ROLE[])DB.ExecuteProcedure(
                "operator_GetOperatorRole",
                new DB.AnalyzeDataSet(GetOperatorRole),
                new DB.Parameter[] { idOperatorParam });
        }

        public DB_ROLE_ACTIVITY[] GetSitaOperatorsAllowedDistributor(int[] IdRole)
        {
            return (DB_ROLE_ACTIVITY[])DB.ExecuteProcedure(
                "dataservice_GetSitaOperatorsAllowedDistributor",
                new DB.AnalyzeDataSet(GetRoleActivity),
                new DB.Parameter[] { CreateTableParam("@ID_ROLE", IdRole) });
        }

        private object GetSitaOperators(DataSet QueryResult)
        {
            Dictionary<int, User> usersDict = new Dictionary<int, User>();
            List<User> operatorList = new List<User>();
            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                User user = new User();
                user.IdOperator = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                user.IdDistributor = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                user.UserName = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.Password = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.FirstName = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.Surname = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.City = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.Address = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.PostalCode = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.Email = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.PhoneMobile = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                user.Phone = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                uint ActivityID = GetValue<uint>(QueryResult.Tables[0].Rows[i][col++]);
                user.Activities = new List<Activity>();
                user.Activities.Add(new Activity(ActivityID, false));

                if (!usersDict.ContainsKey(user.IdOperator.Value))
                {
                    //Użytkownik jeszcze nie istnieje w słowniku
                    usersDict.Add(user.IdOperator.Value, user);
                }
                else
                {
                    //Użytkownik już istnieje w słowniku. Dodaj tylko uprawnienia
                    usersDict[user.IdOperator.Value].Activities.Add(new Activity(ActivityID, false));
                }
            }
            foreach (KeyValuePair<int, User> u in usersDict)
            {
                operatorList.Add(u.Value);
            }

            return operatorList.ToArray();
        }
        //public int SaveOperator(DB_OPERATOR ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_u_SaveOperator",
        //        new DB.Parameter[] {
        //    InsertId
        //                                ,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
        //                                                ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
        //                                                ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
        //                                                ,new DB.InParameter("@LOGIN", ToBeSaved.LOGIN)
        //                                                ,new DB.InParameter("@PASSWORD", ToBeSaved.PASSWORD)
        //                                                ,new DB.InParameter("@IS_BLOCKED", ToBeSaved.IS_BLOCKED)
        //                                                ,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
        //                            }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_OPERATOR = (int)InsertId.Value;

        //    return (int)InsertId.Value;
        //}

        public List<int> GetOperatorShippingListMailingList(int IdDistributor, int OperationType)
        {
            return (List<int>)DB.ExecuteProcedure(
                "imrse_u_GetOperatorShippingListMailingList",
                new DB.AnalyzeDataSet(GetOperatorShippingListMailingList),
                new DB.Parameter[] { 
			        new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, IdDistributor),                    
                    new DB.InParameter("@OPERATION_TYPE", SqlDbType.Int, OperationType)    
				}
            );
        }

        private object GetOperatorShippingListMailingList(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<int> list = new List<int>();
            for (int i = 0; i < count; i++)
            {
                list.Add(GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]));
            }
            return list;
        }


        public DB_OPERATOR_MAPPING[] GetOperatorIdMappings(int[] IdOperator, int IdImrServer, bool IsIdOperatorFromImrServer)
        {
            return (DB_OPERATOR_MAPPING[])DB.ExecuteProcedure(
                   "imrse_u_GetOperatorIdMappings",
                   new DB.AnalyzeDataSet(GetOperatorIdMappings),
                   new DB.Parameter[] 
                   { 
			            CreateTableParam("@ID_OPERATOR", IdOperator),
			            new DB.InParameter("@ID_IMR_SERVER", IdImrServer), 
			            new DB.InParameter("@IS_ID_OPERATOR_FROM_IMR_SERVER", IsIdOperatorFromImrServer) 
                   }, 1800
               );
        }

        private object GetOperatorIdMappings(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_OPERATOR_MAPPING()
            {
                ID_OPERATOR_IMR_SERVER = GetNullableValue<int>(row["ID_OPERATOR_IMR_SERVER"]),
                ID_OPERATOR_IMRSC = GetNullableValue<int>(row["ID_OPERATOR_IMRSC"])
            }).ToArray();
        }



        public List<Tuple<int, int>> GetOperatorRealizedOrders(int? IdOperator)
        {
            return (List<Tuple<int, int>>)DB.ExecuteProcedure(
                   "imrse_u_GetOperatorRealizedOrders",
                   new DB.AnalyzeDataSet(GetOperatorRealizedOrders),
                   new DB.Parameter[] 
                   { 
			            new DB.InParameter("@ID_OPERATOR", IdOperator)
                   }, 1800
               );
        }

        private object GetOperatorRealizedOrders(DataSet QueryResult)
        {
            List<Tuple<int, int>> retList = new List<Tuple<int, int>>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    Tuple<int, int> item = new Tuple<int, int>(GetValue<int>(drItem["ID_ORDER_PACKAGE"]),
                                                               GetValue<int>(drItem["ID_REALIZED_PACKAGE"]));
                    retList.Add(item);
                }
            }
            return retList;
        }

        #endregion

        #region Table OPERATOR_ROLE
        public DB_OPERATOR_ROLE[] GetOperatorRole()
        {
            return GetOperatorRole(null, null);
        }

        public DB_OPERATOR_ROLE[] GetOperatorRole(int? idOperator, int? idRole)
        {
            DB.InParameter idOperatorParam;
            if (idOperator != null)
                idOperatorParam = new DB.InParameter("@ID_OPERATOR", idOperator.Value);
            else
                idOperatorParam = new DB.InNullParameter("@ID_OPERATOR");

            DB.InParameter idRoleParam;
            if (idRole != null)
                idRoleParam = new DB.InParameter("@ID_ROLE", idRole.Value);
            else
                idRoleParam = new DB.InNullParameter("@ID_ROLE");

            return (DB_OPERATOR_ROLE[])DB.ExecuteProcedure(
                "imrse_u_GetOperatorRole",
                new DB.AnalyzeDataSet(GetOperatorRole),
                new DB.Parameter[] { 
					idOperatorParam,
					idRoleParam
				}
            );
        }

        private object GetOperatorRole(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_OPERATOR_ROLE[] list = new DB_OPERATOR_ROLE[count];
            for (int i = 0; i < count; i++)
            {
                DB_OPERATOR_ROLE insert = new DB_OPERATOR_ROLE();
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE"]);
                list[i] = insert;
            }
            return list;
        }

        public void SaveOperatorRole(DB_OPERATOR_ROLE ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveOperatorRole",
                new DB.Parameter[] {
			        new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
					,new DB.InParameter("@ID_ROLE", ToBeSaved.ID_ROLE)
			    }
            );
        }

        public void DeleteOperatorRole(DB_OPERATOR_ROLE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelOperatorRole",
                new DB.Parameter[] {
            new DB.InParameter("@ID_OPERATOR", toBeDeleted.ID_OPERATOR)			,
            new DB.InParameter("@ID_ROLE", toBeDeleted.ID_ROLE)
		}
            );
        }

        public void DeleteOperatorRole(int idOperator, int idRole)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelOperatorRoleByOperatorAndRole",
                new DB.Parameter[] {
            new DB.InParameter("@ID_OPERATOR", idOperator)			,
            new DB.InParameter("@ID_ROLE", idRole)
		}
            );
        }

        //public DB_OPERATOR_ACTIVITY[] GetOperatorActivities(int idOperator)
        //{
        //    return (DB_OPERATOR_ACTIVITY[])DB.ExecuteProcedure(
        //        "imrse_u_GetOperatorActivities",
        //        new DB.AnalyzeDataSet(GetActivity),
        //        new DB.Parameter[] { new DB.InNullParameter("@ID_OPERATOR") }
        //    );
        //}

        #endregion

        #region Table OPERATIOR_ACTIVITY

        public DB_OPERATOR_ACTIVITY[] GetOperatorActivity(int IdOperator)
        {
            return (DB_OPERATOR_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_u_GetOperatorActivity",
                new DB.AnalyzeDataSet(GetOperatorActivity),
                new DB.Parameter[] { 
			        new DB.InParameter("@ID_OPERATOR", IdOperator)
		        }
            );
        }

        public DB_OPERATOR_ACTIVITY[] GetOperatorActivity()
        {
            return (DB_OPERATOR_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetOperatorActivity",
                new DB.AnalyzeDataSet(GetOperatorActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR_ACTIVITY", new long[] {})
					}
            );
        }

        public DB_OPERATOR_ACTIVITY[] GetOperatorActivity(long[] Ids)
        {
            return (DB_OPERATOR_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetOperatorActivity",
                new DB.AnalyzeDataSet(GetOperatorActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR_ACTIVITY", Ids)
					}
            );
        }


        private object GetOperatorActivity(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_OPERATOR_ACTIVITY[] list = new DB_OPERATOR_ACTIVITY[count];
            for (int i = 0; i < count; i++)
            {
                DB_OPERATOR_ACTIVITY insert = new DB_OPERATOR_ACTIVITY();
                insert.ID_OPERATOR_ACTIVITY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_ACTIVITY"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_ACTIVITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTIVITY"]);
                insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
                insert.DENY = GetValue<bool>(QueryResult.Tables[0].Rows[i]["DENY"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveOperatorActivity(DB_OPERATOR_ACTIVITY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_OPERATOR_ACTIVITY", ToBeSaved.ID_OPERATOR_ACTIVITY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOperatorActivity",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_ACTIVITY", ToBeSaved.ID_ACTIVITY)
                                                        ,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE)
														,new DB.InParameter("@DENY", ToBeSaved.DENY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_OPERATOR_ACTIVITY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteOperatorActivity(DB_OPERATOR_ACTIVITY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOperatorActivity",
                new DB.Parameter[] {
            new DB.InParameter("@ID_OPERATOR_ACTIVITY", toBeDeleted.ID_OPERATOR_ACTIVITY)			
		}
            );
        }

        #endregion

        #region Table PROTOCOL
        public int SaveProtocol(DB_PROTOCOL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PROTOCOL", ToBeSaved.ID_PROTOCOL);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveProtocol",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_PROTOCOL_DRIVER", ToBeSaved.ID_PROTOCOL_DRIVER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PROTOCOL = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region Table ROLE_GROUP
        public DB_ROLE_GROUP[] GetRoleGroup()
        {
            return GetRoleGroup(null, null);
        }

        public DB_ROLE_GROUP[] GetRoleGroup(int? idParentRole, int? idRole)
        {
            DB.InParameter idRoleParent;
            if (idParentRole != null)
                idRoleParent = new DB.InParameter("@ID_ROLE_PARENT", idParentRole.Value);
            else
                idRoleParent = new DB.InNullParameter("@ID_ROLE_PARENT");

            DB.InParameter idRoleParam;
            if (idRole != null)
                idRoleParam = new DB.InParameter("@ID_ROLE", idRole.Value);
            else
                idRoleParam = new DB.InNullParameter("@ID_ROLE");

            return (DB_ROLE_GROUP[])DB.ExecuteProcedure(
                "imrse_u_GetRoleGroup",
                new DB.AnalyzeDataSet(GetRoleGroup),
                new DB.Parameter[] { 
					idRoleParent,
					idRoleParam
				}
            );
        }

        private object GetRoleGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROLE_GROUP[] list = new DB_ROLE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROLE_GROUP insert = new DB_ROLE_GROUP();
                insert.ID_ROLE_PARENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE_PARENT"]);
                insert.ID_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE"]);
                list[i] = insert;
            }
            return list;
        }

        public void SaveRoleGroup(DB_ROLE_GROUP ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveRoleGroup",
                new DB.Parameter[] {
			        new DB.InParameter("@ID_ROLE_PARENT", ToBeSaved.ID_ROLE_PARENT)
					,new DB.InParameter("@ID_ROLE", ToBeSaved.ID_ROLE)
			    }
            );
        }

        public void DeleteRoleGroup(DB_ROLE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelRoleGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROLE_PARENT", toBeDeleted.ID_ROLE_PARENT)			,
            new DB.InParameter("@ID_ROLE", toBeDeleted.ID_ROLE)
		}
            );
        }

        #endregion

        #region Table ROUTE

        #region SaveRouteDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveRouteDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateRouteDetails",
                new DB.Parameter[] {
                      new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
                    , CreateTableParam("@PARTICULAR_UNITS", particularUnits)
                    , CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #endregion

        #region Table CONSUMER

        #endregion

        #region Table CONSUMER_TYPE

        public DB_CONSUMER_TYPE[] GetConsumerType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerType",
                new DB.AnalyzeDataSet(GetConsumerType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TYPE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_CONSUMER_TYPE[] GetConsumerType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerType",
                new DB.AnalyzeDataSet(GetConsumerType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TYPE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetConsumerType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            bool useIdDistributorTable = QueryResult.Tables[0].Columns.Contains("ID_DISTRIBUTOR");

            DB_CONSUMER_TYPE[] list = new DB_CONSUMER_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_TYPE insert = new DB_CONSUMER_TYPE();
                insert.ID_CONSUMER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                if (useIdDistributorTable)
                    insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveConsumerType(DB_CONSUMER_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TYPE", ToBeSaved.ID_CONSUMER_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                                        ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteConsumerType(DB_CONSUMER_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelConsumerType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TYPE", toBeDeleted.ID_CONSUMER_TYPE)			
		}
            );
        }

        #endregion

        #region Table TARIFF

        public DB_TARIFF[] GetTariff(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetTariff",
                new DB.AnalyzeDataSet(GetTariff),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TARIFF[] GetTariff(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetTariff",
                new DB.AnalyzeDataSet(GetTariff),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTariff(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TARIFF[] list = new DB_TARIFF[count];
            for (int i = 0; i < count; i++)
            {
                DB_TARIFF insert = new DB_TARIFF();
                insert.ID_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTariff(DB_TARIFF ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTariff",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TARIFF = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTariff(DB_TARIFF toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelTariff",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TARIFF", toBeDeleted.ID_TARIFF)			
		}
            );
        }

        #endregion

        #region Table TASK

        public DB_TASK_STATISTICS[] GetTaskStatistics(int[] IdOperator)
        {
            return (DB_TASK_STATISTICS[])DB.ExecuteProcedure(
                "imrse_u_GetTasksStatistics",
                new DB.AnalyzeDataSet(GetTaskStatistics),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_OPERATOR", IdOperator)
					}
            );
        }

        private object GetTaskStatistics(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<DB_TASK_STATISTICS> list = new List<DB_TASK_STATISTICS>();
            for (int i = 0; i < count; i++)
            {
                DB_TASK_STATISTICS insertTaskStatistics = new DB_TASK_STATISTICS();
                insertTaskStatistics.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insertTaskStatistics.TOTAL_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["TOTAL_NO"]);
                insertTaskStatistics.FINISHED_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["FINISHED_NO"]);
                insertTaskStatistics.PLANNED_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["PLANNED_NO"]);
                insertTaskStatistics.IN_PROGRESS_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["IN_PROGRESS_NO"]);
                insertTaskStatistics.WAITING_FOR_ACCEPTANCE_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["WAITING_FOR_ACCEPTANCE_NO"]);
                insertTaskStatistics.NOT_PAID_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["NOT_PAID_NO"]);

                list.Add(insertTaskStatistics);
            }
            return list.ToArray();
        }

        #endregion

        #region Table TASK_DATA

        public Tuple<DB_TASK_DATA, string>[] GetTaskAssignedOperations(int[] IdTask, int IdLanguage)
        {
            return (Tuple<DB_TASK_DATA, string>[])DB.ExecuteProcedure(
                "imrse_u_GetTaskAssignedOperations",
                new DB.AnalyzeDataSet(GetTaskAssignedOperations),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_TASK", IdTask),
                        new DB.InParameter("@ID_LANGUAGE", SqlDbType.Int, IdLanguage)
					}
            );
        }

        private object GetTaskAssignedOperations(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<Tuple<DB_TASK_DATA, string>> list = new List<Tuple<DB_TASK_DATA, string>>();
            for (int i = 0; i < count; i++)
            {
                DB_TASK_DATA insertTaskData = new DB_TASK_DATA();
                insertTaskData.ID_TASK_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TASK_DATA"]);
                insertTaskData.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insertTaskData.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insertTaskData.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insertTaskData.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);

                Tuple<DB_TASK_DATA, string> insert = new Tuple<DB_TASK_DATA, string>(insertTaskData, GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]));
                list.Add(insert);
            }
            return list.ToArray();
        }

        public Tuple<int, string>[] GetTaskAssignedOperations2(int[] IdTask, int IdLanguage, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (Tuple<int, string>[])DB.ExecuteProcedure(
                "imrse_u_GetTaskAssignedOperations",
                new DB.AnalyzeDataSet(GetTaskAssignedOperations2),
                new DB.Parameter[] { 
			            CreateTableParam("@ID_TASK", IdTask),
                        new DB.InParameter("@ID_LANGUAGE", SqlDbType.Int, IdLanguage)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetTaskAssignedOperations2(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<Tuple<int, string>> list = new List<Tuple<int, string>>();
            for (int i = 0; i < count; i++)
            {
                DB_TASK_DATA insertTaskData = new DB_TASK_DATA();
                insertTaskData.ID_TASK_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TASK_DATA"]);
                insertTaskData.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insertTaskData.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insertTaskData.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insertTaskData.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);

                Tuple<int, string> insert = new Tuple<int, string>(GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]),
                                                                   GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]));
                list.Add(insert);
            }
            return list.ToArray();
        }

        #endregion

        #region Table ISSUE
        public DataTable GetIssuesForInterface(DateTime StartDate, DateTime EndDate)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetIssuesForInterface",
                new DB.AnalyzeDataSet(GetIssuesForInterface),
                new DB.Parameter[] { 
			        new DB.InParameter("@StartDate", StartDate),
                    new DB.InParameter("@EndDate", EndDate)
				}
            );
        }

        private object GetIssuesForInterface(DataSet QueryResult)
        {
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("IssueID");
            resultTable.Columns.Add("IssueLocationCID");
            resultTable.Columns.Add("IssueLocationName");
            resultTable.Columns.Add("IssueCity");
            resultTable.Columns.Add("IssueAddress");
            resultTable.Columns.Add("IssueRegisteredByName");
            resultTable.Columns.Add("IssueRegisteredBySurname");
            resultTable.Columns.Add("IssueCreationDate");
            resultTable.Columns.Add("IssueShortDescription");
            resultTable.Columns.Add("IssueLongDescription");
            resultTable.Columns.Add("IssueStatus");
            resultTable.Columns.Add("TaskPerfomedByName");
            resultTable.Columns.Add("TaskPerfomedBySurname");
            resultTable.Columns.Add("TaskRealisationDate");
            resultTable.Columns.Add("TaskRealisationNotes");

            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                int IssueID = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                if (IssueID == 6130)
                {
                }
                string IssueLocationCID = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueLocationName = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueCity = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueAddress = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                string IssueRegisteredByName = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueRegisteredBySurname = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                DateTime IssueCreationDate = GetValue<DateTime>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueShortDescription = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string IssueLongDescription = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                int IssueStatus = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                int TaskStatus = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                bool IsTaskFinished = GetValue<bool>(QueryResult.Tables[0].Rows[i][col++]);
                int IssueStatusProcessed = 0;
                if (IsTaskFinished)
                {
                    switch (TaskStatus)
                    {
                        case 4:	/*Task status Finished succesfully*/ IssueStatusProcessed = 2; break;
                        case 5:	/*Task status Finished with error*/ IssueStatusProcessed = 3; break;
                        case 7:	/*Task status Canceled*/ IssueStatusProcessed = 4; break;
                        default: IssueStatusProcessed = 0; break;
                    }
                }
                else
                {
                    switch (TaskStatus)
                    {
                        case 3: /*Task status Paused*/ IssueStatusProcessed = 1; break;
                        default: IssueStatusProcessed = 0; break;
                    }
                }

                string TaskPerfomedByName = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string TaskPerfomedBySurname = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                DateTime? TaskRealisationDate = GetValue<DateTime>(QueryResult.Tables[0].Rows[i][col++]);
                string TaskRealisationNotes = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                resultTable.Rows.Add(IssueID, IssueLocationCID, IssueLocationName, IssueCity, IssueAddress,
                    IssueRegisteredByName, IssueRegisteredBySurname,
                    IssueCreationDate, IssueShortDescription, IssueLongDescription,
                    IssueStatusProcessed, TaskPerfomedByName, TaskPerfomedBySurname, TaskRealisationDate, TaskRealisationNotes);

            }
            return resultTable;
        }
        #endregion

        #region TABLE SCHEDULE_ACTION
        public DB_SCHEDULE_ACTION[] GetScheduleAction()
        {
            return (DB_SCHEDULE_ACTION[])DB.ExecuteProcedure(
                "imrse_u_GetScheduleAction",
                new DB.AnalyzeDataSet(GetScheduleAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE_ACTION", new long[] {})
					}
            );
        }

        public DB_SCHEDULE_ACTION[] GetScheduleAction(long[] Ids)
        {
            return (DB_SCHEDULE_ACTION[])DB.ExecuteProcedure(
                "imrse_u_GetScheduleAction",
                new DB.AnalyzeDataSet(GetScheduleAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE_ACTION", Ids)
					}
            );
        }

        private object GetScheduleAction(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SCHEDULE_ACTION[] list = new DB_SCHEDULE_ACTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_SCHEDULE_ACTION insert = new DB_SCHEDULE_ACTION();
                insert.ID_SCHEDULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SCHEDULE"]);
                insert.ID_ACTION_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_DEF"]);
                insert.IS_ENABLED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ENABLED"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_PROFILE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROFILE"]);
                insert.ID_SCHEDULE_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SCHEDULE_ACTION"]);
                list[i] = insert;
            }
            return list;
        }


        public void SaveScheduleAction(DB_SCHEDULE_ACTION ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveScheduleAction",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_SCHEDULE_ACTION", ToBeSaved.ID_SCHEDULE_ACTION)
			                            ,new DB.InParameter("@ID_SCHEDULE", ToBeSaved.ID_SCHEDULE)
										,new DB.InParameter("@ID_ACTION_DEF", ToBeSaved.ID_ACTION_DEF)
										,new DB.InParameter("@IS_ENABLED", ToBeSaved.IS_ENABLED)
										,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
										,new DB.InParameter("@ID_PROFILE", ToBeSaved.ID_PROFILE)
									}
            );
        }

        public void DeleteScheduleAction(DB_SCHEDULE_ACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelScheduleAction",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SCHEDULE_ACTION", toBeDeleted.ID_SCHEDULE_ACTION)	
		}
            );
        }
        #endregion

        #region Table SHIPPING_LIST_DEVICE_WARRANTY

        public void SaveShippingListDeviceWarranty(int idShippingList, long serialNbr, int idComponent, int? idContract, DateTime? warrantyDate)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveShippingListDeviceWarranty",
                new DB.Parameter[] {
			                                             new DB.InParameter("@ID_SHIPPING_LIST", idShippingList)
										                ,new DB.InParameter("@SERIAL_NBR", serialNbr)
														,new DB.InParameter("@ID_COMPONENT", idComponent)
														,new DB.InParameter("@ID_CONTRACT", idContract)
														,new DB.InParameter("@WARRANTY_DATE", warrantyDate)
									}
            );
        }

        public void DeleteShippingListDeviceWarranty(int idShippingList, long serialNbr, int idComponent)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelShippingListDeviceWarranty",
                new DB.Parameter[] {
                             new DB.InParameter("@ID_SHIPPING_LIST", idShippingList)
							,new DB.InParameter("@SERIAL_NBR", serialNbr)
							,new DB.InParameter("@ID_COMPONENT", idComponent)		
		}
            );
        }

        public DB_SHIPPING_LIST_DEVICES_WARRANTY_User[] GetShippingListDeviceWarrantyByDistributor(int idDistributor)
        {
            return (DB_SHIPPING_LIST_DEVICES_WARRANTY_User[])DB.ExecuteProcedure(
                "imrse_u_GetShippingListDeviceWarrantyByDistributor",
                new DB.AnalyzeDataSet(GetShippingListDeviceWarrantyByDistributor),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DISTRIBUTOR", idDistributor) 
                }
            );
        }
        private object GetShippingListDeviceWarrantyByDistributor(DataSet QueryResult)
        {
            if (QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                int count = QueryResult.Tables[0].Rows.Count;
                DB_SHIPPING_LIST_DEVICES_WARRANTY_User[] list = new DB_SHIPPING_LIST_DEVICES_WARRANTY_User[count];
                for (int i = 0; i < count; i++)
                {
                    DB_SHIPPING_LIST_DEVICES_WARRANTY_User insert = new DB_SHIPPING_LIST_DEVICES_WARRANTY_User();
                    insert.ID_SHIPPING_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                    insert.CONTRACT_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONTRACT_NBR"]);
                    insert.SHIPPING_LIST_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHIPPING_LIST_NBR"]);
                    insert.CREATION_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]);
                    insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                    insert.DEVICE_ORDER_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_ORDER_NUMBER"]);
                    insert.ID_COMPONENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_COMPONENT"]);
                    insert.WARRANTY_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]);
                    list[i] = insert;
                }
                return list;
            }
            else
                return null;
        }

        public DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[] GetDeviceShippingAndServiceHistory(long serialNbr)
        {
            return (DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[])DB.ExecuteProcedure(
                "imrsc_MW_GetDeviceShippingAndServiceHistory",
                new DB.AnalyzeDataSet(GetDeviceShippingAndServiceHistory),
                new DB.Parameter[] { 
                    new DB.InParameter("@SerialNbr", serialNbr) 
                }
            );
        }
        private object GetDeviceShippingAndServiceHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[] list = new DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User insert = new DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User();
                insert.ROW_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ROW_TYPE"]);
                insert.EVENT_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["EVENT_DATE"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.LONG_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["LONG_DESCR"]);
                insert.WARRANTY_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region Table SIM_CARD

        public DB_DEVICE_SIM_CARD[] GetSimCardDataBySerialNbr(long[] SerialNbr, int[] IdDistributor)
        {
            if (SerialNbr == null)
                SerialNbr = new List<long>().ToArray();
            return (DB_DEVICE_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_u_GetSimCardDataBySerialNbr",
                new DB.AnalyzeDataSet(GetSimCardDataBySerialNbr),
                new DB.Parameter[] { 
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter)
                }
            );
        }

        public DB_DEVICE_SIM_CARD[] GetSimCardBySerialNbr(long[] SerialNbr, int[] IdDistributor)
        {
            if (SerialNbr == null)
                SerialNbr = new List<long>().ToArray();
            return (DB_DEVICE_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_u_GetSimCardBySerialNbr",
                new DB.AnalyzeDataSet(GetSimCardBySerialNbr),
                new DB.Parameter[] { 
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					CreateTableParam("@ID_DISTRIBUTOR", IdDistributor != null ? IdDistributor : DistributorFilter)
                }
            );
        }

        private object GetSimCardDataBySerialNbr(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_SIM_CARD[] list = new DB_DEVICE_SIM_CARD[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_SIM_CARD insert = new DB_DEVICE_SIM_CARD();
                insert.DEVICE_SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SIM_CARD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.ID_TARIFF = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
                insert.TARIFF_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TARIFF_NAME"]);
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.TARIFF_TAX = GetNullableValue<float>(QueryResult.Tables[0].Rows[i]["TARIFF_TAX"]);
                list[i] = insert;
            }
            return list;
        }

        private object GetSimCardBySerialNbr(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_SIM_CARD[] list = new DB_DEVICE_SIM_CARD[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_SIM_CARD insert = new DB_DEVICE_SIM_CARD();
                insert.DEVICE_SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SIM_CARD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                list[i] = insert;
            }
            return list;
        }

        public DB_DEVICE_SIM_CARD[] GetSimCardDeviceByDistributor(int[] idSimCardDistributor, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 120)
        {
            return (DB_DEVICE_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_u_GetSimCardDeviceByDistributor",
                new DB.AnalyzeDataSet(GetSimCardDeviceByDistributor),
                new DB.Parameter[] { 
					CreateTableParam("@ID_SIM_CARD_DISTRIBUTOR", idSimCardDistributor != null ? idSimCardDistributor : DistributorFilter)
                }, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetSimCardDeviceByDistributor(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_SIM_CARD[] list = new DB_DEVICE_SIM_CARD[count];
            bool idLocationColumnExistst = QueryResult.Tables[0].Columns.Contains("ID_LOCATION");
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_SIM_CARD insert = new DB_DEVICE_SIM_CARD();
                insert.ID_SIM_CARD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.DEVICE_SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["DEVICE_SERIAL_NBR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                if (idLocationColumnExistst)
                    insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                list[i] = insert;
            }
            return list;
        }

        public int AddSimCardTmq(int idDistributor, string serialNbr, string phone, string pin, string puk, string ip, int mobileNetworkCode, out string errorMsg)
        {
            try
            {
                DB.ExecuteScalar(
                    "tmq_AddSimCard",
                    new DB.Parameter[]
					{
						new DB.InParameter ("@ID_DISTRIBUTOR", idDistributor),
                        new DB.InParameter ("@SERIAL_NBR", serialNbr),
                        new DB.InParameter ("@PHONE", phone),
                        new DB.InParameter ("@PIN", pin),
                        new DB.InParameter ("@PUK", puk),
                        new DB.InParameter ("@IP", ip),
                        new DB.InParameter ("@MOBILE_NETWORK_CODE", mobileNetworkCode)
					}
                    );
                errorMsg = "";
                return 0;
            }
            catch (SqlException sex)
            {
                errorMsg = sex.Message;
                return -1;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return -1;
            }
        }

        public void SaveSimCard(DB_SIM_CARD[] ToBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            if (ToBeSaved.Count(d => d.ID_SIM_CARD != 0) > 0)
                BulkUpdate("SIM_CARD", ToBeSaved.Where(d => d.ID_SIM_CARD != 0).ToArray(), null, FillSimCard, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_SIM_CARD == 0) > 0)
                BulkInsert("SIM_CARD", ToBeSaved.Where(d => d.ID_SIM_CARD == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_SIM_CARD" }, fillDataTableMethod: FillSimCard, commandTimeout: commandTimeout);
        }

        #endregion

        #region SIM_CARD_DATA

        public void SaveSimCardData(DB_SIM_CARD_DATA[] ToBeSaved, DB_DATA_TYPE[] ToBeSavedType, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            //upewniamy się, że typ zapisywany do sql_variant odpowiada typowi zadeklarowanemu dla data typa
            Dictionary<long, DB_DATA_TYPE> toBeSavedTypeDict = ToBeSavedType.ToDictionary(d => d.ID_DATA_TYPE);
            foreach (DB_SIM_CARD_DATA dtItem in ToBeSaved)
                dtItem.VALUE = ParamObject(dtItem.VALUE, dtItem.ID_DATA_TYPE, toBeSavedTypeDict[dtItem.ID_DATA_TYPE].ID_DATA_TYPE_CLASS);

            if (ToBeSaved.Count(d => d.ID_SIM_CARD_DATA != 0) > 0)
                BulkUpdate("SIM_CARD_DATA", ToBeSaved.Where(d => d.ID_SIM_CARD_DATA != 0).ToArray(), new List<string>() { "ID_SIM_CARD_DATA" }, FillSimCardData, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_SIM_CARD_DATA == 0) > 0)
                BulkInsert("SIM_CARD_DATA", ToBeSaved.Where(d => d.ID_SIM_CARD_DATA == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_SIM_CARD_DATA" }, fillDataTableMethod: FillSimCardData, commandTimeout: commandTimeout);
        }

        #endregion

        #region TABLE SERVICE_LIST_DEVICE

        #region SaveServiceListDevice

        public void SaveServiceListDevice(int idServiceList, DB_SERVICE_LIST_DEVICE ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveServiceListDevice",
                new DB.Parameter[] {
			                             new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@FAILURE_DESCRIPTION", ToBeSaved.FAILURE_DESCRIPTION)
														,new DB.InParameter("@FAILURE_SUGGESTION", ToBeSaved.FAILURE_SUGGESTION)
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@PHOTO", ToBeSaved.PHOTO)
														,new DB.InParameter("@ID_OPERATOR_SERVICER", ToBeSaved.ID_OPERATOR_SERVICER)
														,new DB.InParameter("@SERVICE_SHORT_DESCR", ToBeSaved.SERVICE_SHORT_DESCR)
														,new DB.InParameter("@SERVICE_LONG_DESCR", ToBeSaved.SERVICE_LONG_DESCR)
														,new DB.InParameter("@PHOTO_SERVICED", ToBeSaved.PHOTO_SERVICED)
														,new DB.InParameter("@ATEX_OK", ToBeSaved.ATEX_OK)
														,new DB.InParameter("@PAY_FOR_SERVICE", ToBeSaved.PAY_FOR_SERVICE)
														,new DB.InParameter("@ID_SERVICE_STATUS", ToBeSaved.ID_SERVICE_STATUS)
														,new DB.InParameter("@ID_DIAGNOSTIC_STATUS", ToBeSaved.ID_DIAGNOSTIC_STATUS)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
        }

        #endregion

        #region GetServiceListDeviceFilterCustom

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDeviceFilterCustom(int[] IdServiceList, long[] SerialNbr, string FailureDescription, string FailureSuggestion, string Notes, long[] Photo,
                            int[] IdOperatorServicer, string ServiceShortDescr, string ServiceLongDescr, long[] PhotoServiced, bool? AtexOk,
                            bool? PayForService, int[] IdServiceStatus, int[] IdDiagnosticStatus, TypeDateTimeCode StartDate, TypeDateTimeCode EndDate,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceFilter"));

            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceFilter",
                new DB.AnalyzeDataSet(GetServiceListDeviceCustom),
                new DB.Parameter[] { 
					CreateTableParam("@ID_SERVICE_LIST", IdServiceList),
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					new DB.InParameter("@FAILURE_DESCRIPTION", FailureDescription),
					new DB.InParameter("@FAILURE_SUGGESTION", FailureSuggestion),
					new DB.InParameter("@NOTES", Notes),
					CreateTableParam("@PHOTO", Photo),
					CreateTableParam("@ID_OPERATOR_SERVICER", IdOperatorServicer),
					new DB.InParameter("@SERVICE_SHORT_DESCR", ServiceShortDescr),
					new DB.InParameter("@SERVICE_LONG_DESCR", ServiceLongDescr),
					CreateTableParam("@PHOTO_SERVICED", PhotoServiced),
					new DB.InParameter("@ATEX_OK", AtexOk),
					new DB.InParameter("@PAY_FOR_SERVICE", PayForService),
					CreateTableParam("@ID_SERVICE_STATUS", IdServiceStatus),
					CreateTableParam("@ID_DIAGNOSTIC_STATUS", IdDiagnosticStatus),
					CreateTableParam("@START_DATE_CODE", StartDate),
					CreateTableParam("@END_DATE_CODE", EndDate),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetServiceListDeviceCustom(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE[] list = new DB_SERVICE_LIST_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE insert = new DB_SERVICE_LIST_DEVICE();
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_OPERATOR_SERVICER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_SERVICER"]);
                insert.ATEX_OK = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["ATEX_OK"]);
                insert.PAY_FOR_SERVICE = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["PAY_FOR_SERVICE"]);
                insert.ID_SERVICE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_STATUS"]);
                insert.ID_DIAGNOSTIC_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_STATUS"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region GetServiceListDeviceCustom

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDeviceCustom(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDevice",
                new DB.AnalyzeDataSet(GetServiceListDeviceCustom),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDeviceCustom(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDevice",
                new DB.AnalyzeDataSet(GetServiceListDeviceCustom),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        #endregion

        #region GetServicesForDevices

        public DataTable GetServicesForDevices(long[] SerialNbr, int IdServiceList)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetServicesForDevices",
                new DB.AnalyzeDataSet(GetServicesForDevices),
                new DB.Parameter[] { 
                        CreateTableParam("@SERIAL_NBR", SerialNbr),
                        new DB.InParameter("@ID_SERVICE_LIST", IdServiceList)
					}
            );
        }

        private object GetServicesForDevices(DataSet QueryResult)
        {
            return QueryResult.Tables[0];
        }

        #endregion

        #region GetServiceDeviceDiagnosticActions

        public DataTable GetServiceDeviceDiagnosticActions(long[] SerialNbr, int IdLanguage)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetServiceDeviceDiagnosticActions",
                new DB.AnalyzeDataSet(GetServiceDeviceDiagnosticActions),
                new DB.Parameter[] { 
			            CreateTableParam("@SERIAL_NBR", SerialNbr),
                        new DB.InParameter("@ID_LANGUAGE", IdLanguage)
					}
            );
        }

        private object GetServiceDeviceDiagnosticActions(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
                return QueryResult.Tables[0];
            return null;
        }

        #endregion

        #region GetServiceDeviceServiceActions

        public DataTable GetServiceDeviceServiceActions(long[] SerialNbr, int IdLanguage)
        {
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetServiceDeviceServiceActions",
                new DB.AnalyzeDataSet(GetServiceDeviceServiceActions),
                new DB.Parameter[] { 
			            CreateTableParam("@SERIAL_NBR", SerialNbr),
                        new DB.InParameter("@ID_LANGUAGE", IdLanguage)
					}
            );
        }

        private object GetServiceDeviceServiceActions(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
                return QueryResult.Tables[0];
            return null;
        }

        #endregion

        #region GetServiceListCloseInfo

        public Dictionary<int, bool> GetServiceListCloseInfo()
        {
            return (Dictionary<int, bool>)DB.ExecuteProcedure(
                "imrse_u_GetServiceListCloseInfo",
                new DB.AnalyzeDataSet(GetServiceListCloseInfo),
                new DB.Parameter[] { 
					}
            );
        }

        private object GetServiceListCloseInfo(DataSet QueryResult)
        {
            Dictionary<int, bool> retDict = new Dictionary<int, bool>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    retDict.Add(GetValue<int>(drItem["ID_SERVICE_LIST"]), GetValue<bool>(drItem["IS_READY_TO_CLOSE"]));
                }
            }
            return retDict;
        }

        #endregion

        #endregion

        #region Table WARRANTY_CONTRACT

        public void SaveWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent, int months, int idWarrantyStartDateCalcMethod)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveWarrantyContract",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", idDeviceOrderNumber)
									   ,new DB.InParameter("@ID_COMPONENT", idComponent)
									   ,new DB.InParameter("@ID_CONTRACT", idContract)
									   ,new DB.InParameter("@WARRANTY_TIME", months)
                                       ,new DB.InParameter("@ID_WARRANTY_START_DATE_CALCULATION_METHOD", idWarrantyStartDateCalcMethod)
									}
            );
        }

        public void DeleteWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelWarrantyContract",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", idDeviceOrderNumber),
            new DB.InParameter("@ID_COMPONENT", idComponent),	
            new DB.InParameter("@ID_CONTRACT", idContract)	
		}
            );
        }

        #endregion

        #region Table WORK
        public DB_WORK[] GetWork(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK[])DB.ExecuteProcedure(
                "imrse_GetWork",
                new DB.AnalyzeDataSet(GetWork),
                new DB.Parameter[] {
            CreateTableParam("@ID_WORK", new long[] {}),
            CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK[] GetWork(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK[])DB.ExecuteProcedure(
                "imrse_GetWork",
                new DB.AnalyzeDataSet(GetWork),
                new DB.Parameter[] {
            CreateTableParam("@ID_WORK", Ids),
            CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter)
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK[] GetWorkFilter(long[] IdWork, long[] IdWorkType, int[] IdWorkStatus, long[] IdWorkData, long[] IdWorkParent, int[] IdModule,
                            int[] IdOperator, TypeDateTimeCode CreationDate, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdWork != null && IdWork.Length > MaxItemsPerSqlTableType) ||
                (IdWorkType != null && IdWorkType.Length > MaxItemsPerSqlTableType) ||
                (IdWorkStatus != null && IdWorkStatus.Length > MaxItemsPerSqlTableType) ||
                (IdWorkData != null && IdWorkData.Length > MaxItemsPerSqlTableType) ||
                (IdWorkParent != null && IdWorkParent.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdWork != null) args.AddRange(IdWork.Select(s => new Tuple<int, object>(1, s)));
                if (IdWorkType != null) args.AddRange(IdWorkType.Select(s => new Tuple<int, object>(2, s)));
                if (IdWorkStatus != null) args.AddRange(IdWorkStatus.Select(s => new Tuple<int, object>(3, s)));
                if (IdWorkData != null) args.AddRange(IdWorkData.Select(s => new Tuple<int, object>(4, s)));
                if (IdWorkParent != null) args.AddRange(IdWorkParent.Select(s => new Tuple<int, object>(5, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(6, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(7, s)));

                DB_WORK[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_WORK[])DB.ExecuteProcedure(
                        "imrse_GetWorkFilterTableArgs",
                        new DB.AnalyzeDataSet(GetWork),
                        new DB.Parameter[] {
                    new DB.InParameter("@ID", SqlDbType.BigInt, id),
                    CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                    new DB.InParameter("@TOP_COUNT", topCount),
                    new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_WORK[])DB.ExecuteProcedure(
                    "imrse_GetWorkFilter",
                    new DB.AnalyzeDataSet(GetWork),
                    new DB.Parameter[] {
                CreateTableParam("@ID_WORK", IdWork),
                CreateTableParam("@ID_WORK_TYPE", IdWorkType),
                CreateTableParam("@ID_WORK_STATUS", IdWorkStatus),
                CreateTableParam("@ID_WORK_DATA", IdWorkData),
                CreateTableParam("@ID_WORK_PARENT", IdWorkParent),
                CreateTableParam("@ID_MODULE", IdModule),
                CreateTableParam("@ID_OPERATOR", IdOperator),
                CreateTableParam("@CREATION_DATE_CODE", CreationDate),
                new DB.InParameter("@TOP_COUNT", topCount),
                new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }

        public DB_WORK[] GetWorkFilter(long[] IdWork, long[] IdWorkType, int[] IdWorkStatus,
                    long[] IdWorkData, long[] IdWorkParent, int[] IdModule, int[] IdOperator, int[] IdDistributor,
                    Data.DB.TypeDateTimeCode StartDateFrom, Data.DB.TypeDateTimeCode StartDateTo,
                    bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            DB_WORK[] tmpResult = null;

            if (UseBulkInsertSqlTableType && (
                (IdWork != null && IdWork.Length > MaxItemsPerSqlTableType) ||
                (IdWorkType != null && IdWorkType.Length > MaxItemsPerSqlTableType) ||
                (IdWorkStatus != null && IdWorkStatus.Length > MaxItemsPerSqlTableType) ||
                (IdWorkData != null && IdWorkData.Length > MaxItemsPerSqlTableType) ||
                (IdWorkParent != null && IdWorkParent.Length > MaxItemsPerSqlTableType) ||
                (IdModule != null && IdModule.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType) ||
                ((IdDistributor != null && IdDistributor.Length > MaxItemsPerSqlTableType) || (IdDistributor == null && DistributorFilter.Length > MaxItemsPerSqlTableType))
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdWork != null) args.AddRange(IdWork.Select(s => new Tuple<int, object>(1, s)));
                if (IdWorkType != null) args.AddRange(IdWorkType.Select(s => new Tuple<int, object>(2, s)));
                if (IdWorkStatus != null) args.AddRange(IdWorkStatus.Select(s => new Tuple<int, object>(3, s)));
                if (IdWorkData != null) args.AddRange(IdWorkData.Select(s => new Tuple<int, object>(4, s)));
                if (IdWorkParent != null) args.AddRange(IdWorkParent.Select(s => new Tuple<int, object>(5, s)));
                if (IdModule != null) args.AddRange(IdModule.Select(s => new Tuple<int, object>(6, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(7, s)));
                if (IdDistributor != null) args.AddRange(IdDistributor.Select(s => new Tuple<int, object>(8, s)));
                else if (DistributorFilter.Length > 0) args.AddRange(DistributorFilter.Select(s => new Tuple<int, object>(12, s)));

                try
                {
                    InsertImrseProcArg(id, args);
                    tmpResult = (DB_WORK[])DB.ExecuteProcedure(
                        "imrse_u_GetWorkFilterTableArgs",
                        new DB.AnalyzeDataSet(GetWork),
                        new DB.Parameter[] {
                            new DB.InParameter("@ID", SqlDbType.BigInt, id),
                            CreateTableParam("@START_DATE_CODE", StartDateFrom),
                            CreateTableParam("@END_DATE_CODE", StartDateTo)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
            }
            else
            {
                tmpResult = (DB_WORK[])DB.ExecuteProcedure(
                    "imrse_u_GetWorkFilter",
                    new DB.AnalyzeDataSet(GetWork),
                    new DB.Parameter[] {
                        CreateTableParam("@ID_WORK", IdWork),
                        CreateTableParam("@ID_WORK_TYPE", IdWorkType),
                        CreateTableParam("@ID_WORK_STATUS", IdWorkStatus),
                        CreateTableParam("@ID_WORK_DATA", IdWorkData),
                        CreateTableParam("@ID_WORK_PARENT", IdWorkParent),
                        CreateTableParam("@ID_MODULE", IdModule),
                        CreateTableParam("@ID_OPERATOR", IdOperator),
                        CreateTableParam("@START_DATE_CODE", StartDateFrom),
                        CreateTableParam("@END_DATE_CODE", StartDateTo),
                        CreateTableParam("@ID_DISTRIBUTOR", IdDistributor ?? DistributorFilter)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }

            return tmpResult;
        }

        public long SaveWork(DB_WORK ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK", ToBeSaved.ID_WORK);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWork",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@ID_WORK_TYPE", ToBeSaved.ID_WORK_TYPE),
                    new DB.InParameter("@ID_WORK_STATUS", ToBeSaved.ID_WORK_STATUS),
                    new DB.InParameter("@ID_WORK_DATA", ToBeSaved.ID_WORK_DATA),
                    new DB.InParameter("@ID_WORK_PARENT", ToBeSaved.ID_WORK_PARENT),
                    new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE),
                    new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR),
                    ToBeSaved.CREATION_DATE == DateTime.MinValue ? new DB.InNullParameter("@CREATION_DATE") : new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void SaveWork(DB_WORK[] ToBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            if (ToBeSaved.Count(d => d.ID_WORK != 0) > 0)
                BulkUpdate("WORK", ToBeSaved.Where(d => d.ID_WORK != 0).ToArray(), null, FillWork, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_WORK == 0) > 0)
                BulkInsert("WORK", ToBeSaved.Where(d => d.ID_WORK == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_WORK" }, fillDataTableMethod: FillWork, commandTimeout: commandTimeout);
        }
        public void ChangeWorkStatus(DB_WORK[] ToBeChanged, int commandTimeout = 0)
        {
            if (ToBeChanged.Count(d => d.ID_WORK != 0) > 0)
                BulkUpdate("WORK", ToBeChanged.Where(d => d.ID_WORK != 0).ToArray(), null, FillWork, new string[] { "ID_WORK_TYPE", "ID_WORK_PARENT", "ID_MODULE", "ID_OPERATOR", "CREATION_DATE" }.ToList(), commandTimeout);
        }

        #region FillWork
        protected DataTable FillWork(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_WORK[] dataList = data as DB_WORK[];
            int iterator = 0;

            foreach (DB_WORK dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_WORK"] = dtItem.ID_WORK;
                row["ID_WORK_TYPE"] = dtItem.ID_WORK_TYPE;
                row["ID_WORK_STATUS"] = dtItem.ID_WORK_STATUS;

                if (dtItem.ID_WORK_DATA.HasValue)
                    row["ID_WORK_DATA"] = dtItem.ID_WORK_DATA.Value;
                else
                    row["ID_WORK_DATA"] = DBNull.Value;

                if (dtItem.ID_WORK_PARENT.HasValue)
                    row["ID_WORK_PARENT"] = dtItem.ID_WORK_PARENT.Value;
                else
                    row["ID_WORK_PARENT"] = DBNull.Value;

                if (dtItem.ID_MODULE.HasValue)
                    row["ID_MODULE"] = dtItem.ID_MODULE.Value;
                else
                    row["ID_MODULE"] = DBNull.Value;

                if (dtItem.ID_OPERATOR.HasValue)
                    row["ID_OPERATOR"] = dtItem.ID_OPERATOR.Value;
                else
                    row["ID_OPERATOR"] = DBNull.Value;

                dataTable.Rows.Add(row);

                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }

                iterator++;
            }
            return dataTable;
        }
        #endregion
        #endregion
        #region Table WORK_DATA
        public void SaveWorkData(DB_WORK_DATA[] ToBeSaved, DB_DATA_TYPE[] ToBeSavedType, int commandTimeout = 0, List<string> updateSetSkipColumns = null)
        {
            //upewniamy się, że typ zapisywany do sql_variant odpowiada typowi zadeklarowanemu dla data typa
            Dictionary<long, DB_DATA_TYPE> toBeSavedTypeDict = ToBeSavedType.ToDictionary(d => d.ID_DATA_TYPE);
            foreach (DB_WORK_DATA dtItem in ToBeSaved)
                dtItem.VALUE = ParamObject(dtItem.VALUE, dtItem.ID_DATA_TYPE, toBeSavedTypeDict[dtItem.ID_DATA_TYPE].ID_DATA_TYPE_CLASS);

            if (ToBeSaved.Count(d => d.ID_WORK_DATA != 0) > 0)
                BulkUpdate("WORK_DATA", ToBeSaved.Where(d => d.ID_WORK_DATA != 0).ToArray(), null, FillWorkData, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_WORK_DATA == 0) > 0)
                BulkInsert("WORK_DATA", ToBeSaved.Where(d => d.ID_WORK_DATA == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_WORK_DATA" }, fillDataTableMethod: FillWorkData, getMaxIdFromDB: true, commandTimeout: commandTimeout);
        }
        #region FillWorkData
        protected DataTable FillWorkData(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_WORK_DATA[] dataList = data as DB_WORK_DATA[];
            int iterator = 0;

            foreach (DB_WORK_DATA dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();
                row["ID_WORK_DATA"] = dtItem.ID_WORK_DATA;
                row["ID_DATA_TYPE"] = dtItem.ID_DATA_TYPE;
                row["INDEX_NBR"] = dtItem.INDEX_NBR;
                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;

                dataTable.Rows.Add(row);
                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }
                iterator++;
            }
            return dataTable;
        }
        #endregion
        #endregion
        #region Table WORK_HISTORY
        public void SaveWorkHistory(DB_WORK_HISTORY[] ToBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            if (ToBeSaved.Count(d => d.ID_WORK_HISTORY != 0) > 0)
                BulkUpdate("WORK_HISTORY", ToBeSaved.Where(d => d.ID_WORK_HISTORY != 0).ToArray(), null, FillWorkHistory, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_WORK_HISTORY == 0) > 0)
                BulkInsert("WORK_HISTORY", ToBeSaved.Where(d => d.ID_WORK_HISTORY == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_WORK_HISTORY" }, fillDataTableMethod: FillWorkHistory, commandTimeout: commandTimeout);
        }
        #region FillWorkHistory
        protected DataTable FillWorkHistory(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_WORK_HISTORY[] dataList = data as DB_WORK_HISTORY[];
            int iterator = 0;

            foreach (DB_WORK_HISTORY dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_WORK_HISTORY"] = dtItem.ID_WORK_HISTORY;
                row["ID_WORK"] = dtItem.ID_WORK;
                row["ID_WORK_STATUS"] = dtItem.ID_WORK_STATUS;

                if (dtItem.ID_MODULE.HasValue)
                    row["ID_MODULE"] = dtItem.ID_MODULE.Value;
                else
                    row["ID_MODULE"] = DBNull.Value;

                if (dtItem.ID_DESCR.HasValue)
                    row["ID_DESCR"] = dtItem.ID_DESCR.Value;
                else
                    row["ID_DESCR"] = DBNull.Value;

                if (dtItem.START_TIME.HasValue)
                    row["START_TIME"] = dtItem.START_TIME.Value;
                else
                    row["START_TIME"] = DBNull.Value;

                dataTable.Rows.Add(row);

                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }
                iterator++;
            }
            return dataTable;
        }
        #endregion
        #endregion
        #region Table WORK_HISTORY_DATA
        public void SaveWorkHistoryData(DB_WORK_HISTORY_DATA[] ToBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0)
        {
            if (ToBeSaved.Count(d => d.ID_WORK_HISTORY_DATA != 0) > 0)
                BulkUpdate("WORK_HISTORY_DATA", ToBeSaved.Where(d => d.ID_WORK_HISTORY_DATA != 0).ToArray(), null, FillWorkHistoryData, updateSetSkipColumns, commandTimeout);
            if (ToBeSaved.Count(d => d.ID_WORK_HISTORY_DATA == 0) > 0)
                BulkInsert("WORK_HISTORY_DATA", ToBeSaved.Where(d => d.ID_WORK_HISTORY_DATA == 0).ToArray(), primaryKeysFields: new List<string>() { "ID_WORK_HISTORY_DATA" }, fillDataTableMethod: FillWorkHistoryData, commandTimeout: commandTimeout);
        }
        #region FillWorkHistoryData
        protected DataTable FillWorkHistoryData(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_WORK_HISTORY_DATA[] dataList = data as DB_WORK_HISTORY_DATA[];
            int iterator = 0;

            foreach (DB_WORK_HISTORY_DATA dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_WORK_HISTORY_DATA"] = dtItem.ID_WORK_HISTORY_DATA;
                row["ID_WORK_HISTORY"] = dtItem.ID_WORK_HISTORY;
                row["ARG_NBR"] = dtItem.ARG_NBR;
                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;

                dataTable.Rows.Add(row);

                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }

                iterator++;
            }
            return dataTable;
        }
        #endregion
        #endregion

        #region Table VIEW

        public void ProceedViewOperationRow(string[] ViewOperation)
        {
            if(ViewOperation != null && ViewOperation.Length > 0)
            {
                DB.ExecuteScalarSql(String.Join(";", ViewOperation));
            }
        }

        public void ProceedViewCreation(string[] ViewCreation)
        {
            if (ViewCreation != null && ViewCreation.Length > 0)
            {
                DB.ExecuteScalarSql(String.Join(";", ViewCreation));
            }
        }

        #region InsertViewRow
        //public void InsertViewRow(DB_VIEW view, Dictionary<long, Dictionary<string, Tuple<object, DB_DATA_TYPE, int>>> toInsert)
        //{
        //    // Dictionary<ObjectId, Dictionary<ColumnName,Tuple<CellValue, DataType IndexNbr>>>
        //    List<string> insertClauses = new List<string>();

        //    string insertFormat = "INSERT INTO [{0}] ({1}) VALUES ({2})";
        //    List<string> updateClauses = new List<string>();
        //    foreach (KeyValuePair<long, Dictionary<string, Tuple<object, DB_DATA_TYPE, int>>> kvDevItem in toInsert)
        //    {
        //        string columnClause = String.Format("[{0}]", String.Join("],[", kvDevItem.Value.Keys));
        //        string valuesClause = String.Empty;
        //        foreach (Tuple<object, DB_DATA_TYPE, int> tItem in kvDevItem.Value.Values)
        //            valuesClause += PrepareDataValue(tItem.Item1, tItem.Item2) + ",";
        //        if (valuesClause.Length > 0)
        //            valuesClause = valuesClause.Substring(0, valuesClause.Length - 1);

        //        string insertClause = String.Format(insertFormat, view.NAME, columnClause, valuesClause);
        //        insertClauses.Add(insertClause);

        //    }
        //    if (insertClauses.Count > 0)
        //    {
        //        try
        //        {
        //            DB.ExecuteScalarSql(String.Join(";", insertClauses));
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        #endregion
        #region UpdateViewRow
        //public void UpdateViewRow(DB_VIEW view, DB_VIEW_COLUMN imrServerColumn, Dictionary<Tuple<long, int>, Dictionary<string, Dictionary<string, Tuple<object, DB_DATA_TYPE, int>>>> toUpdate)
        //{
        //    //tu nie mozę byc w paramtrze serwera, to musi być już w strukturze
        //    // Dictionary<Tuple<ObjectId, ServerId>, Dictionary<KeyColumnName, Dictionary<SetColumnName, Tuple<CellValue, DataType, IndexNbr>>>>
        //    string updateFormat = "UPDATE [{0}] SET {1} WHERE [" + imrServerColumn.NAME + "] = {2} AND {3}";
        //    List<string> updateClauses = new List<string>();
        //    foreach (KeyValuePair<Tuple<long, int>, Dictionary<string, Dictionary<string, Tuple<object, DB_DATA_TYPE, int>>>> kvDevItem in toUpdate)
        //    {
        //        foreach (KeyValuePair<string, Dictionary<string, Tuple<object, DB_DATA_TYPE, int>>> kvUpdItem in kvDevItem.Value)
        //        {
        //            List<string> setClause = new List<string>();
        //            foreach (KeyValuePair<string, Tuple<object, DB_DATA_TYPE, int>> kvSetClauseItem in kvUpdItem.Value)
        //                setClause.Add(String.Format("[{0}] = {1}", kvSetClauseItem.Key, PrepareDataValue(kvSetClauseItem.Value.Item1, kvSetClauseItem.Value.Item2)));
                    
        //            string updateClause = String.Format(updateFormat,
        //                view.NAME, String.Join(",", setClause), kvDevItem.Key.Item2, String.Format("[{0}] = {1}", kvUpdItem.Key, kvDevItem.Key.Item1));
        //            updateClauses.Add(updateClause);
        //        }
        //    }
        //    if (updateClauses.Count > 0)
        //    {
        //        try
        //        {
        //            DB.ExecuteScalarSql(String.Join(";", updateClauses));
        //        }
        //        catch(Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        #endregion
        #region DeleteViewRow
        //public void DeleteViewRow(DB_VIEW view, DB_VIEW_COLUMN imrServerColumn, DB_VIEW_COLUMN primaryKeyColumn, List<Tuple<long, int>> toDelete)
        //{
        //    // List<ObjectId, ServerId>
        //    List<string> deleteClauses = new List<string>();

        //    string deleteFormat = "DELETE FROM [{0}] WHERE [{1}] = {2} AND [{3}] = {4}";
        //    List<string> updateClauses = new List<string>();
        //    foreach (Tuple<long, int> tItem in toDelete)
        //    {
        //        string deleteClause = String.Format(deleteFormat, view.NAME, primaryKeyColumn.NAME, tItem.Item1, imrServerColumn.NAME, tItem.Item2);
        //        deleteClauses.Add(deleteClause);

        //    }
        //    if (deleteClauses.Count > 0)
        //    {
        //        try
        //        {
        //            DB.ExecuteScalarSql(String.Join(";", deleteClauses));
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        #endregion

        #endregion

        #region AddActionWithActionDataListToQueue
        public virtual void AddActionWithActionDataListToQueue(long? IdAction, long IdActionType, int IdActionStatus, long? IdActionData,
            long? SerialNbr, long? IdMeter, long? IdLocation, int IdModule, long? IdOperator, string XmlActionData, string QueuePath,
            bool AutoTransaction = true, IsolationLevel TransactionLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            List<DB.InParameter> parameters = new List<Data.DB.DB.InParameter>();
            parameters.Add(new DB.InParameter("@ID_ACTION", IdAction));
            parameters.Add(new DB.InParameter("@ID_ACTION_TYPE", IdActionType));
            parameters.Add(new DB.InParameter("@ID_ACTION_STATUS", IdActionStatus));
            parameters.Add(new DB.InParameter("@ID_ACTION_DATA", IdActionData));
            parameters.Add(new DB.InParameter("@SERIAL_NBR", SerialNbr));
            parameters.Add(new DB.InParameter("@ID_METER", IdMeter));
            parameters.Add(new DB.InParameter("@ID_LOCATION", IdLocation));
            parameters.Add(new DB.InParameter("@ID_MODULE", IdModule));
            parameters.Add(new DB.InParameter("@ID_OPERATOR", IdOperator));
            parameters.Add(new DB.InParameter("@ACTION_DATA", XmlActionData));
            if (IsParameterDefined("sp_AddActionWithActionDataListToQueue", "@QUEUE_PATH", this))
                parameters.Add(new DB.InParameter("@QUEUE_PATH", QueuePath));

            DB.ExecuteNonQueryProcedure("sp_AddActionWithActionDataListToQueue",
                parameters.ToArray(),
                AutoTransaction: AutoTransaction, IsolationLevel: TransactionLevel, CommandTimeout: CommandTimeout);
        }
        #endregion

        #region LogInUser
        public int? LogInUser(string UserName, string PasswordHash)
        {
            DB_OPERATOR[] oprs = (DB_OPERATOR[])DB.ExecuteProcedure(
                "imrse_u_LogInUser",
                new DB.AnalyzeDataSet(GetOperator),
                new DB.Parameter[]
                    { 
                        new DB.InParameter("@LOGIN", UserName), 
                        new DB.InParameter("@PASSWORD", PasswordHash)
                    });

            if (oprs.Length == 0)
                return null;
            else
                return oprs[0].ID_OPERATOR;
        }
        #endregion
        #region CheckIfLoginExists
        public bool CheckIfLoginExists(string login, int? idDistributor = null)
        {
            DB.InOutParameter exists = new DB.InOutParameter("@EXISTS", SqlDbType.Bit, false);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_CheckIfLoginExists",
                new DB.Parameter[]
                    { 
                        new DB.InParameter("@LOGIN", login), 
                        new DB.InParameter("@ID_DISTRIBUTOR", idDistributor),
                        exists
                    });

            return (bool)exists.Value;
        }
        #endregion
        #region ChangePassword
        public bool ChangePassword(string login, bool sendMail, out int result)
        {
            DB.InParameter paramLogin = new DB.InParameter("@LOGIN", login);
            DB.InParameter paramSendMail = new DB.InParameter("@SEND_MAIL", sendMail);
            DB.OutParameter paramResult = new DB.OutParameter("@RESULT", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "imrse_u_ChangePassword",
                new DB.Parameter[]
                    { 
                        paramLogin,
                        paramSendMail,
                        paramResult
                    });

            result = Convert.ToInt32(paramResult.Value);
            return result == 0;
        }
        #endregion

        #region CheckIfDataBaseExists
        public bool? CheckIfDataBaseExists(string name)
        {
            bool ret = false;
            DB.InOutParameter retValue = new DB.InOutParameter("@EXISTS", ret);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_CheckIfDatabaseExists",
                new DB.Parameter[] {
			                             retValue
										,new DB.InParameter("@NAME", name)
									}
            );
            if (!retValue.IsNull)
                return ((int)retValue.Value == 1 ? true : false);

            return null;
        }
        #endregion
        #region CheckIfTableExists
        public bool CheckIfTableExists(string name)
        {
            bool ret = false;
            DB.InOutParameter retValue = new DB.InOutParameter("@RESULT", ret);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_CheckIfTableExists",
                new DB.Parameter[] {
			                             retValue
										,new DB.InParameter("@TABLE_NAME", name)
									}
            );
            if (!retValue.IsNull)
                return ((int)retValue.Value == 1 ? true : false);

            return false;
        }
        #endregion
        #region CheckIfLinkedServerExists
        public bool CheckIfLinkedServerExists(string name)
        {
            bool ret = false;
            DB.InOutParameter retValue = new DB.InOutParameter("@RESULT", ret);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_CheckIfLinkedServerExists",
                new DB.Parameter[] {
                    new DB.InParameter("@LINKED_SERVER_NAME", name),
			        retValue
				}
            );
            if (!retValue.IsNull)
                return ((int)retValue.Value == 1 ? true : false);

            return false;
        }
        #endregion

        #region ManageDistributorCORE
        public void ManageDistributorCORE(DB_DISTRIBUTOR ToBeSaved)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_ManageDistributorsCORE",
                new DB.Parameter[] {
			                             new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                        ,new DB.InParameter("@CITY", ToBeSaved.CITY)
                                        ,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
        }
        #endregion

        #region GetTriadaData
        public System.Tuple<DB_LOCATION[], DB_METER[], DB_DEVICE[]> GetTriadaData(List<long> locations, List<long> locationDataTypes, List<long> meters, List<long> meterDataTypes, List<long> serialNbrs, List<long> serialNbrDataTypes, int? commandTimeout = null)//, System.Diagnostics.Stopwatch sw = null, System.Text.StringBuilder txt = null)
        {
            if (!commandTimeout.HasValue)
                commandTimeout = this.ShortTimeout;
            DataSet QueryResult = new DataSet();
            //if (sw != null) txt.AppendFormat("a: {0},", sw.ElapsedMilliseconds);

            if (UseBulkInsertSqlTableType &&
                (((locations != null && locations.Count > MaxItemsPerSqlTableType) || (locations == null && LocationFilter.Length > MaxItemsPerSqlTableType)) ||
                (locationDataTypes != null && locationDataTypes.Count > MaxItemsPerSqlTableType) ||
                ((meters != null && meters.Count > MaxItemsPerSqlTableType) || (meters == null && MeterFilter.Length > MaxItemsPerSqlTableType)) ||
                (meterDataTypes != null && meterDataTypes.Count > MaxItemsPerSqlTableType) ||
                ((serialNbrs != null && serialNbrs.Count > MaxItemsPerSqlTableType) || (serialNbrs == null && DeviceFilter.Length > MaxItemsPerSqlTableType)) ||
                (serialNbrDataTypes != null && serialNbrDataTypes.Count > MaxItemsPerSqlTableType)))
            {
                //if (sw != null) txt.AppendFormat("b: {0},", sw.ElapsedMilliseconds);
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (locations != null) args.AddRange(locations.Select(s => new Tuple<int, object>(1, s)));
                else if (LocationFilter.Length > 0) args.AddRange(LocationFilter.Select(s => new Tuple<int, object>(1, s)));
                if (locationDataTypes != null) args.AddRange(locationDataTypes.Select(s => new Tuple<int, object>(2, s)));
                if (meters != null) args.AddRange(meters.Select(s => new Tuple<int, object>(3, s)));
                else if (MeterFilter.Length > 0) args.AddRange(MeterFilter.Select(s => new Tuple<int, object>(3, s)));
                if (meterDataTypes != null) args.AddRange(meterDataTypes.Select(s => new Tuple<int, object>(4, s)));
                if (serialNbrs != null) args.AddRange(serialNbrs.Select(s => new Tuple<int, object>(5, s)));
                else if (DeviceFilter.Length > 0) args.AddRange(DeviceFilter.Select(s => new Tuple<int, object>(5, s)));
                if (serialNbrDataTypes != null) args.AddRange(serialNbrDataTypes.Select(s => new Tuple<int, object>(6, s)));

                //if (sw != null) txt.AppendFormat("c: {0},", sw.ElapsedMilliseconds);

                try
                {
                    InsertImrseProcArg(id, args);
                    //if (sw != null) txt.AppendFormat("d: {0},", sw.ElapsedMilliseconds);
                    QueryResult = ExecuteProcedure("imrse_u_GetTriadaDataFilterTableArgs", timeOut: commandTimeout.Value, args: new object[] { id });
                    //if (sw != null) txt.AppendFormat("e: {0},", sw.ElapsedMilliseconds);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                //if (sw != null) txt.AppendFormat("f: {0},", sw.ElapsedMilliseconds);
            }
            else
            {
                QueryResult = ExecuteProcedure("imrse_u_GetTriadaDataFilter",
                  timeOut: commandTimeout.Value,
                  args: new object[]
                  {
                    serialNbrs != null ? serialNbrs.Select(s => s as object).ToArray() : DeviceFilter.Select(s => s as object).ToArray(),
                    serialNbrDataTypes != null ? serialNbrDataTypes.Select(s => s as object).ToArray() : new object[0],
                    meters != null ? meters.Select(s => s as object).ToArray() : MeterFilter.Select(s => s as object).ToArray(),
                    meterDataTypes != null ? meterDataTypes.Select(s => s as object).ToArray() : new object[0],
                    locations != null ? locations.Select(s => s as object).ToArray() : LocationFilter.Select(s => s as object).ToArray(),
                    locationDataTypes != null ? locationDataTypes.Select(s => s as object).ToArray() : new object[0]
                  });
            }
            //Dictionary<long, List<DB_DATA>> locactionsDict = new Dictionary<long, List<DB_DATA>>();
            //Dictionary<long, List<DB_DATA>> metersDict = new Dictionary<long, List<DB_DATA>>();
            //Dictionary<long, List<DB_DATA>> devicesDict = new Dictionary<long, List<DB_DATA>>();
            ConcurrentDictionary<long, List<DB_DATA>> locationsDict = new ConcurrentDictionary<long, List<DB_DATA>>();
            ConcurrentDictionary<long, List<DB_DATA>> metersDict = new ConcurrentDictionary<long, List<DB_DATA>>();
            ConcurrentDictionary<long, List<DB_DATA>> devicesDict = new ConcurrentDictionary<long, List<DB_DATA>>();
            #region DB_DATA
            //if (sw != null) txt.AppendFormat("g: {0},", sw.ElapsedMilliseconds);
            //for (int i = 0; i < QueryResult.Tables[3].Rows.Count; i++)
            //{
            //    DB_DATA dbData = new DB_DATA();
            //    dbData.ID_DATA = GetValue<long>(QueryResult.Tables[3].Rows[i]["ID_DATA"]);
            //    dbData.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[3].Rows[i]["SERIAL_NBR"]);
            //    dbData.ID_METER = GetNullableValue<long>(QueryResult.Tables[3].Rows[i]["ID_METER"]);
            //    dbData.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[3].Rows[i]["ID_LOCATION"]);
            //    dbData.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[3].Rows[i]["ID_DATA_TYPE"]);
            //    dbData.INDEX_NBR = GetValue<int>(QueryResult.Tables[3].Rows[i]["INDEX_NBR"]);
            //    dbData.VALUE = GetValue(QueryResult.Tables[3].Rows[i]["VALUE"]);
            //    dbData.STATUS = GetValue<int>(QueryResult.Tables[3].Rows[i]["STATUS"]);

            //    if (dbData.ID_LOCATION.HasValue)
            //    {
            //        if (!locactionsDict.ContainsKey(dbData.ID_LOCATION.Value))
            //            locactionsDict.Add(dbData.ID_LOCATION.Value, new List<DB_DATA>());
            //        locactionsDict[dbData.ID_LOCATION.Value].Add(dbData);
            //    }
            //    if (dbData.ID_METER.HasValue)
            //    {
            //        if (!metersDict.ContainsKey(dbData.ID_METER.Value))
            //            metersDict.Add(dbData.ID_METER.Value, new List<DB_DATA>());
            //        metersDict[dbData.ID_METER.Value].Add(dbData);
            //    }
            //    if (dbData.SERIAL_NBR.HasValue)
            //    {
            //        if (!devicesDict.ContainsKey(dbData.SERIAL_NBR.Value))
            //            devicesDict.Add(dbData.SERIAL_NBR.Value, new List<DB_DATA>());
            //        devicesDict[dbData.SERIAL_NBR.Value].Add(dbData);
            //    }
            //}

            DB_DATA[] dataList = QueryResult.Tables[3].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA()
            {
                ID_DATA = GetValue<long>(row["ID_DATA"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                STATUS = GetValue<int>(row["STATUS"]),
            }).ToArray();
            dataList.Where(w => w.ID_LOCATION.HasValue).AsParallel().ForAll(item =>
            {
                lock (locationsDict)
                {
                    locationsDict.TryAdd(item.ID_LOCATION.Value, new List<DB_DATA>());
                    locationsDict[item.ID_LOCATION.Value].Add(item);
                }
            });
            dataList.Where(w => w.ID_METER.HasValue).AsParallel().ForAll(item =>
            {
                lock (metersDict)
                {
                    metersDict.TryAdd(item.ID_METER.Value, new List<DB_DATA>());
                    metersDict[item.ID_METER.Value].Add(item);
                }
            });
            dataList.Where(w => w.SERIAL_NBR.HasValue).AsParallel().ForAll(item =>
            {
                lock (devicesDict)
                {
                    devicesDict.TryAdd(item.SERIAL_NBR.Value, new List<DB_DATA>());
                    devicesDict[item.SERIAL_NBR.Value].Add(item);
                }
            });

            #endregion
            #region DB_LOCATION[]
            //if (sw != null) txt.AppendFormat("h: {0},", sw.ElapsedMilliseconds);
            //int count = QueryResult.Tables[0].Rows.Count;
            //DB_LOCATION[] locationList = new DB_LOCATION[count];
            //for (int i = 0; i < count; i++)
            //{
            //    DB_LOCATION dbLocation = new DB_LOCATION();
            //    dbLocation.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
            //    dbLocation.ID_LOCATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_TYPE"]);
            //    dbLocation.ID_LOCATION_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_PATTERN"]);
            //    dbLocation.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR_PATTERN"]);
            //    dbLocation.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
            //    dbLocation.ID_CONSUMER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
            //    dbLocation.ID_LOCATION_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE_TYPE"]);
            //    dbLocation.IN_KPI = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IN_KPI"]);
            //    dbLocation.ALLOW_GROUPING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ALLOW_GROUPING"]);
            //    if (locactionsDict.ContainsKey(dbLocation.ID_LOCATION))
            //        dbLocation.U_DATA_ARRAY = locactionsDict[dbLocation.ID_LOCATION].ToArray();
            //    locationList[i] = dbLocation;
            //}

#if TRIADA_DATA_CONCURRENT_BAG
            ConcurrentBag<DB_LOCATION> locationList = new ConcurrentBag<DB_LOCATION>(QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION()
            {
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                ID_LOCATION_TYPE = GetValue<int>(row["ID_LOCATION_TYPE"]),
                ID_LOCATION_PATTERN = GetNullableValue<long>(row["ID_LOCATION_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                ID_CONSUMER = GetNullableValue<int>(row["ID_CONSUMER"]),
                ID_LOCATION_STATE_TYPE = GetValue<int>(row["ID_LOCATION_STATE_TYPE"]),
                IN_KPI = GetValue<bool>(row["IN_KPI"]),
                ALLOW_GROUPING = GetValue<bool>(row["ALLOW_GROUPING"]),
            }));
            locationList.AsParallel().ForAll(item => item.U_DATA_ARRAY = locationsDict.ContainsKey(item.ID_LOCATION) ? locationsDict[item.ID_LOCATION].ToArray() : null);
#else
            DB_LOCATION[] locationArray = QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row =>
                new DB_LOCATION()
                {
                    ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                    ID_LOCATION_TYPE = GetValue<int>(row["ID_LOCATION_TYPE"]),
                    ID_LOCATION_PATTERN = GetNullableValue<long>(row["ID_LOCATION_PATTERN"]),
                    ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                    ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                    ID_CONSUMER = GetNullableValue<int>(row["ID_CONSUMER"]),
                    ID_LOCATION_STATE_TYPE = GetValue<int>(row["ID_LOCATION_STATE_TYPE"]),
                    IN_KPI = GetValue<bool>(row["IN_KPI"]),
                    ALLOW_GROUPING = GetValue<bool>(row["ALLOW_GROUPING"])
                }).ToArray();
            locationArray.AsParallel().ForAll(item => item.U_DATA_ARRAY = locationsDict.ContainsKey(item.ID_LOCATION) ? locationsDict[item.ID_LOCATION].ToArray() : null);
#endif
            #endregion
            #region DB_METER[]
            //if (sw != null) txt.AppendFormat("i: {0},", sw.ElapsedMilliseconds);

            //count = QueryResult.Tables[1].Rows.Count;
            //DB_METER[] meterList = new DB_METER[count];
            //for (int i = 0; i < count; i++)
            //{
            //    DB_METER dbMeter = new DB_METER();
            //    dbMeter.ID_METER = GetValue<long>(QueryResult.Tables[1].Rows[i]["ID_METER"]);
            //    dbMeter.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[1].Rows[i]["ID_METER_TYPE"]);
            //    dbMeter.ID_METER_PATTERN = GetNullableValue<long>(QueryResult.Tables[1].Rows[i]["ID_METER_PATTERN"]);
            //    dbMeter.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[1].Rows[i]["ID_DESCR_PATTERN"]);
            //    dbMeter.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[1].Rows[i]["ID_DISTRIBUTOR"]);
            //    if (metersDict.ContainsKey(dbMeter.ID_METER))
            //        dbMeter.U_DATA_ARRAY = metersDict[dbMeter.ID_METER].ToArray();
            //    meterList[i] = dbMeter;
            //}
#if TRIADA_DATA_CONCURRENT_BAG
            ConcurrentBag<DB_METER> meterList = new ConcurrentBag<DB_METER>(QueryResult.Tables[1].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_METER()
            {
                ID_METER = GetValue<long>(row["ID_METER"]),
                ID_METER_TYPE = GetValue<int>(row["ID_METER_TYPE"]),
                ID_METER_PATTERN = GetNullableValue<long>(row["ID_METER_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
            }));
            meterList.AsParallel().ForAll(item => item.U_DATA_ARRAY = metersDict.ContainsKey(item.ID_METER) ? metersDict[item.ID_METER].ToArray() : null);
#else
            DB_METER[] meterArray = QueryResult.Tables[1].Rows.OfType<DataRow>().AsEnumerable().Select(row =>
                new DB_METER()
                {
                    ID_METER = GetValue<long>(row["ID_METER"]),
                    ID_METER_TYPE = GetValue<int>(row["ID_METER_TYPE"]),
                    ID_METER_PATTERN = GetNullableValue<long>(row["ID_METER_PATTERN"]),
                    ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                    ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                }).ToArray();
            meterArray.AsParallel().ForAll(item => item.U_DATA_ARRAY = metersDict.ContainsKey(item.ID_METER) ? metersDict[item.ID_METER].ToArray() : null);
#endif
            #endregion
            #region DB_DEVICE[]
            //if (sw != null) txt.AppendFormat("j: {0},", sw.ElapsedMilliseconds);
            //count = QueryResult.Tables[2].Rows.Count;
            //DB_DEVICE[] deviceList = new DB_DEVICE[count];
            //for (int i = 0; i < count; i++)
            //{
            //    DB_DEVICE dbDevice = new DB_DEVICE();
            //    dbDevice.SERIAL_NBR = GetValue<long>(QueryResult.Tables[2].Rows[i]["SERIAL_NBR"]);
            //    dbDevice.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[2].Rows[i]["ID_DEVICE_TYPE"]);
            //    dbDevice.SERIAL_NBR_PATTERN = GetNullableValue<long>(QueryResult.Tables[2].Rows[i]["SERIAL_NBR_PATTERN"]);
            //    dbDevice.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[2].Rows[i]["ID_DESCR_PATTERN"]);
            //    dbDevice.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[2].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
            //    dbDevice.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[2].Rows[i]["ID_DISTRIBUTOR"]);
            //    dbDevice.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[2].Rows[i]["ID_DEVICE_STATE_TYPE"]);
            //    if (devicesDict.ContainsKey(dbDevice.SERIAL_NBR))
            //        dbDevice.U_DATA_ARRAY = devicesDict[dbDevice.SERIAL_NBR].ToArray();
            //    deviceList[i] = dbDevice;
            //}
#if TRIADA_DATA_CONCURRENT_BAG
            ConcurrentBag<DB_DEVICE> deviceList = new ConcurrentBag<DB_DEVICE>(QueryResult.Tables[2].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DEVICE()
            {
                SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                ID_DEVICE_TYPE = GetValue<int>(row["ID_DEVICE_TYPE"]),
                SERIAL_NBR_PATTERN = GetNullableValue<long>(row["SERIAL_NBR_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DEVICE_ORDER_NUMBER = GetValue<int>(row["ID_DEVICE_ORDER_NUMBER"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                ID_DEVICE_STATE_TYPE = GetValue<int>(row["ID_DEVICE_STATE_TYPE"]),
            }));
            deviceList.AsParallel().ForAll(item => item.U_DATA_ARRAY = devicesDict.ContainsKey(item.SERIAL_NBR) ? devicesDict[item.SERIAL_NBR].ToArray() : null);
#else
            DB_DEVICE[] deviceArray = QueryResult.Tables[2].Rows.OfType<DataRow>().AsEnumerable().Select(row =>
                new DB_DEVICE()
                {
                    SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                    ID_DEVICE_TYPE = GetValue<int>(row["ID_DEVICE_TYPE"]),
                    SERIAL_NBR_PATTERN = GetNullableValue<long>(row["SERIAL_NBR_PATTERN"]),
                    ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                    ID_DEVICE_ORDER_NUMBER = GetValue<int>(row["ID_DEVICE_ORDER_NUMBER"]),
                    ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                    ID_DEVICE_STATE_TYPE = GetValue<int>(row["ID_DEVICE_STATE_TYPE"]),
                }).ToArray();
            deviceArray.AsParallel().ForAll(item => item.U_DATA_ARRAY = devicesDict.ContainsKey(item.SERIAL_NBR) ? devicesDict[item.SERIAL_NBR].ToArray() : null);
#endif
            #endregion

            //if (sw != null) txt.AppendFormat("k: {0},", sw.ElapsedMilliseconds);
#if TRIADA_DATA_CONCURRENT_BAG
            return System.Tuple.Create<DB_LOCATION[], DB_METER[], DB_DEVICE[]>(locationList.ToArray(), meterList.ToArray(), deviceList.ToArray());
#else
            return System.Tuple.Create<DB_LOCATION[], DB_METER[], DB_DEVICE[]>(locationArray, meterArray, deviceArray);
#endif
        }
        #endregion

        #region GetIMRSCAlarms

        public DataTable GetIMRSCAlarms(int IdLanguage, string AlarmModule)
        {
            DB.InParameter idLanguage = new DB.InParameter("@ID_LAGUAGE", SqlDbType.Int, IdLanguage);
            DB.InParameter alarmModule = new DB.InParameter("@ALARM_MODULE", SqlDbType.NVarChar, AlarmModule, 100);
            return (DataTable)DB.ExecuteProcedure(
                "imrse_u_GetIMRSCAlarms",
                new DB.AnalyzeDataSet(GetIMRSCAlarms),
                new DB.Parameter[] { 
			               idLanguage,
                           alarmModule
					}
            );
        }

        private object GetIMRSCAlarms(DataSet ds)
        {
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;
        }

        #endregion

        #region GetAdditionalInfoForLocations
        public void GetAdditionalInfoForLocations(List<DB_LOCATION> locations)
        {
            Dictionary<long, DB_LOCATION> dictLocations = locations.ToDictionary(l => l.ID_LOCATION);

            DataSet ds = ExecuteProcedure("imrse_u_GetLocationAdditionalInfo",
                            new object[]
							{
								locations.Select(w => w.ID_LOCATION as object).ToArray(),
								DistributorFilter.Select(w => w as object).ToArray()
							});

            long idLocation;
            int status;
            string groupName;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                idLocation = GetValue<long>(ds.Tables[0].Rows[i]["ID_LOCATION"]);
                status = GetValue<int>(ds.Tables[0].Rows[i]["STATUS"]);
                groupName = GetValue<string>(ds.Tables[0].Rows[i]["GROUP_NAME"]);

                if (dictLocations.ContainsKey(idLocation))
                {
                    dictLocations[idLocation].U_STATUS = status;
                    dictLocations[idLocation].U_GROUP_NAME = groupName;
                }
            }
        }
        #endregion

        #region SaveProtocolDriver
        public int SaveProtocolDriver(DB_PROTOCOL_DRIVER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PROTOCOL_DRIVER", ToBeSaved.ID_PROTOCOL_DRIVER);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveProtocolDriver",
                new DB.Parameter[] {
			                            InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
										,new DB.InParameter("@PLUGIN_NAME", ToBeSaved.PLUGIN_NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PROTOCOL_DRIVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region SaveDeviceTypeGroup
        public int SaveDeviceTypeGroup(DB_DEVICE_TYPE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_GROUP", ToBeSaved.ID_DEVICE_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceTypeGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region SaveDeviceTypeInGroup
        public int SaveDeviceTypeInGroup(DB_DEVICE_TYPE_IN_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_GROUP", ToBeSaved.ID_DEVICE_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceTypeInGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region DeleteDeviceTypeInGroup
        public void DeleteDeviceTypeInGroup(DB_DEVICE_TYPE_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDeviceTypeInGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_GROUP", toBeDeleted.ID_DEVICE_TYPE_GROUP),
            new DB.InParameter("@ID_DEVICE_TYPE", toBeDeleted.ID_DEVICE_TYPE)			
		}
            );
        }
        #endregion

        #region DeleteRouteDetails

        public void DeleteRouteDetails(long idRoute)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteRouteDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_ROUTE", idRoute)	
		                           }
            );
        }

        #endregion

        #region GetRouteDef

        public DB_ROUTE_DEF[] GetRouteDefByDistributor(int[] IdsDistributor)
        {
            return (DB_ROUTE_DEF[])DB.ExecuteProcedure(
                "imrse_u_GetRouteDefByDistributor",
                new DB.AnalyzeDataSet(GetRouteDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR", IdsDistributor)
					}
            );
        }

        #endregion

        #region Save/DeleteDevicePatternConfig
        public void SaveDevicePatternConfig(DB_DEVICE_PATTERN_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDevicePatternConfig",
                new DB.Parameter[] {
					new DB.InParameter("@ID_PATTERN", ToBeSaved.ID_PATTERN)
					,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
					,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                    ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
				}
            );
        }

        public void DeleteDevicePatternConfig(DB_DEVICE_PATTERN_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDevicePatternConfig",
                new DB.Parameter[] {
                        new DB.InParameter("@ID_PATTERN", toBeDeleted.ID_PATTERN)
                        ,new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)
                        ,new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)			
		        }
            );
        }
        #endregion

        #region Save/DeleteRouteDefLocationData
        public void SaveRouteDefLocationData(DB_ROUTE_DEF_LOCATION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            //DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEF_LOCATION", ToBeSaved.ID_ROUTE_DEF_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveRouteDefLocationData",
                new DB.Parameter[] {
										 new DB.InParameter("@ID_ROUTE_DEF_LOCATION", ToBeSaved.ID_ROUTE_DEF_LOCATION)
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
										,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
										,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            //if (!InsertId.IsNull)
            //    ToBeSaved.ID_ROUTE_DEF_LOCATION = (long)InsertId.Value;

            //return (long)InsertId.Value;
        }

        public void DeleteRouteDefLocationData(DB_ROUTE_DEF_LOCATION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelRouteDefLocationData",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_ROUTE_DEF_LOCATION", toBeDeleted.ID_ROUTE_DEF_LOCATION)	
                                       ,new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)	
                                       ,new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)			
		                           }
            );
        }
        #endregion

        #region Save/DeleteRouteDefData
        public void SaveRouteDefData(DB_ROUTE_DEF_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            //DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveRouteDefData",
                new DB.Parameter[] {
			                            new DB.InParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF)
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
										,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                        , new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
									}
            );
            //if (!InsertId.IsNull)
            //    ToBeSaved.ID_ROUTE_DEF = (int)InsertId.Value;

            //return (int)InsertId.Value;
        }

        public void DeleteRouteDefData(DB_ROUTE_DEF_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelRouteDefData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DEF", toBeDeleted.ID_ROUTE_DEF)	
		    ,new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)
            , new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)
		}
            );
        }
        #endregion

        #region GetSchedule (method for DB.AnalyzeDataSet)
        private object GetSchedule(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SCHEDULE[] list = new DB_SCHEDULE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SCHEDULE insert = new DB_SCHEDULE();
                insert.ID_SCHEDULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SCHEDULE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_SCHEDULE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SCHEDULE_TYPE"]);
                insert.SCHEDULE_INTERVAL = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SCHEDULE_INTERVAL"]);
                insert.SUBDAY_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["SUBDAY_TYPE"]);
                insert.SUBDAY_INTERVAL = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SUBDAY_INTERVAL"]);
                insert.RELATIVE_INTERVAL = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["RELATIVE_INTERVAL"]);
                insert.RECURRENCE_FACTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["RECURRENCE_FACTOR"]);
                insert.START_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]);
                insert.END_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]);
                insert.START_TIME = QueryResult.Tables[0].Rows[i]["START_TIME"] as TimeSpan?;
                insert.END_TIME = QueryResult.Tables[0].Rows[i]["END_TIME"] as TimeSpan?;
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.TIMES_IN_UTC = GetValue<bool>(QueryResult.Tables[0].Rows[i]["TIMES_IN_UTC"]);
                insert.LAST_RUN_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["LAST_RUN_DATE"]));
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion
        #region SaveSchedule
        public int SaveSchedule(DB_SCHEDULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SCHEDULE", ToBeSaved.ID_SCHEDULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSchedule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_SCHEDULE_TYPE", ToBeSaved.ID_SCHEDULE_TYPE)
														,new DB.InParameter("@SCHEDULE_INTERVAL", ToBeSaved.SCHEDULE_INTERVAL)
														,new DB.InParameter("@SUBDAY_TYPE", ToBeSaved.SUBDAY_TYPE)
														,new DB.InParameter("@SUBDAY_INTERVAL", ToBeSaved.SUBDAY_INTERVAL)
														,new DB.InParameter("@RELATIVE_INTERVAL", ToBeSaved.RELATIVE_INTERVAL)
														,new DB.InParameter("@RECURRENCE_FACTOR", ToBeSaved.RECURRENCE_FACTOR)
														,new DB.InParameter("@START_DATE", ToBeSaved.START_DATE)
														,new DB.InParameter("@END_DATE", ToBeSaved.END_DATE)
														,new DB.InParameter("@START_TIME", SqlDbType.Time, ToBeSaved.START_TIME)
														,new DB.InParameter("@END_TIME", SqlDbType.Time, ToBeSaved.END_TIME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
                                                        ,new DB.InParameter("@TIMES_IN_UTC", ToBeSaved.TIMES_IN_UTC)
														,new DB.InParameter("@LAST_RUN_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.LAST_RUN_DATE))
														,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SCHEDULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        #endregion

        #region GetHierarchicalProfiles
        public Dictionary<int, List<long[]>> GetHierarchicalProfiles(long[] serialNbr = null, long[] idMeter = null, long[] idLocation = null, int[] idDistributor = null, int[] idProfile = null, long idDataType = 10020)
        {
            bool filterDevice = serialNbr == null;
            bool filterMeter = idMeter == null;
            bool filterLocation = idLocation == null;
            bool filterDistributor = idDistributor == null;
            Dictionary<int, List<long[]>> tmpResult = (Dictionary<int, List<long[]>>)DB.ExecuteProcedure("imrse_GetHierarchicalProfiles",
                new DB.AnalyzeDataSet(GetHierarchicalProfiles),
                new DB.Parameter[] 
                {
                    CreateTableParam("@SERIAL_NBR", serialNbr),
                    CreateTableParam("@ID_METER", idMeter),
                    CreateTableParam("@ID_LOCATION", idLocation),
                    CreateTableParam("@ID_DISTRIBUTOR", idDistributor != null ? idDistributor : DistributorFilter),
                    CreateTableParam("@ID_PROFILE", idProfile),
                    new DB.InParameter("@ID_DATA_TYPE", idDataType)
                });

            Dictionary<int, List<long[]>> result = new Dictionary<int, List<long[]>>();
            foreach (var pairValue in tmpResult)
            {
                if (!result.ContainsKey(pairValue.Key))
                    result.Add(pairValue.Key, new List<long[]>());
                foreach (long[] entry in pairValue.Value)
                {
                    if ((!filterDevice || entry[0] == 0 || DeviceFilterDict.Keys.Count == 0 || DeviceFilterDict.ContainsKey(entry[0]))
                        && (!filterMeter || entry[1] == 0 || MeterFilterDict.Keys.Count == 0 || MeterFilterDict.ContainsKey(entry[1]))
                        && (!filterLocation || entry[2] == 0 || LocationFilterDict.Keys.Count == 0 || LocationFilterDict.ContainsKey(entry[2]))
                        && (!filterDistributor || entry[3] == 0 || DistributorFilterDict.Keys.Count == 0 || DistributorFilterDict.ContainsKey(Convert.ToInt32(entry[3]))))
                    {
                        if (result.ContainsKey(pairValue.Key))
                            result[pairValue.Key].Add(entry);
                        else
                            result.Add(pairValue.Key, new List<long[]>(new long[][] { entry }));
                    }
                }
            }
            return result;
        }
        private object GetHierarchicalProfiles(DataSet QueryResult)
        {
            Dictionary<int, List<long[]>> result = new Dictionary<int, List<long[]>>();
            if (QueryResult.Tables.Count > 0)
            {
                foreach (DataRow row in QueryResult.Tables[0].Rows)
                {
                    int idProfile;
                    if (!GetPossiblyNullValue(row, "ID_PROFILE", out idProfile))
                        continue;

                    long? serialNbr, idMeter, idLocation;
                    int? idDistributor;
                    GetPossiblyNullValue(row, "SERIAL_NBR", out serialNbr);
                    GetPossiblyNullValue(row, "ID_METER", out idMeter);
                    GetPossiblyNullValue(row, "ID_LOCATION", out idLocation);
                    GetPossiblyNullValue(row, "ID_DISTRIBUTOR", out idDistributor);
                    //long? srcSerialNbr, srcIdMeter, srcIdLocation;
                    //GetPossiblyNullValue(row, "SOURCE_SERIAL_NBR", out srcSerialNbr);
                    //GetPossiblyNullValue(row, "SOURCE_ID_METER", out srcIdMeter);
                    //GetPossiblyNullValue(row, "SOURCE_ID_LOCATION", out srcIdLocation);

                    long[] entry = new long[] { serialNbr ?? 0, idMeter ?? 0, idLocation ?? 0, idDistributor ?? 0 };

                    //srcSerialNbr ?? 0, srcIdMeter ?? 0, srcIdLocation ?? 0 };
                    if (result.ContainsKey(idProfile))
                        result[idProfile].Add(entry);
                    else
                        result.Add(idProfile, new List<long[]>(new long[][] { entry }));
                }
            }
            return result;
        }
        #endregion

        #region UpdateMeterData
        public void UpdateGasMeterDetails(ulong IdMeter, uint IdMeterType, string MeterSerialNbr, float MeterM3PerPulse, uint DeviceCounterLiterMaxValue,
            string MeterSeries, float MeterQmin, float MeterQmax, int MeterSize, out bool MeterFound)
        {
            DB.OutParameter paramMeterFound = new DB.OutParameter("@METER_FOUND", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                "dataservice_UpdateGasMeterDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@ID_METER", IdMeter)
					, new DB.InParameter("@ID_METER_TYPE", IdMeterType)
					, new DB.InParameter("@METER_SERIAL_NBR", MeterSerialNbr)
					, new DB.InParameter("@METER_M3_PER_IMPULSE", MeterM3PerPulse)
					, new DB.InParameter("@DEVICE_COUNTER_LITER_MAX_VALUE", DeviceCounterLiterMaxValue)
                    //Nowe parametry gazomierza
                    , new DB.InParameter("@METER_SERIES", MeterSeries)
                    , new DB.InParameter("@METER_Q_MIN", MeterQmin)
                    , new DB.InParameter("@METER_Q_MAX", MeterQmax)
	                , new DB.InParameter("@METER_SIZE", MeterSize)	
					, paramMeterFound
                });

            MeterFound = (bool)paramMeterFound.Value;
        }
        #endregion
        
        #region UpdateLocationDetails
        public void UpdateLocationDetails(ulong LocationID, double Latitude, double Longitude, out bool LocationFound)
        {
            DB.OutParameter paramLocationFound = new DB.OutParameter("@LOCATION_FOUND", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                "dataservice_UpdateLocationDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@ID_LOCATION", LocationID)
					, new DB.InParameter("@LATITUDE", Latitude)
					, new DB.InParameter("@LONGITUDE", Longitude)				
					, paramLocationFound
                });

            LocationFound = (bool)paramLocationFound.Value;
        }
        #endregion

        #region SaveLocationDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveLocationDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateLocationDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
					, CreateTableParam("@PARTICULAR_UNITS", particularUnits)
					, CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region SaveOperatorDetails

        public void SaveOperatorDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateOperatorDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
					, CreateTableParam("@PARTICULAR_UNITS", particularUnits)
					, CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }
        
        #endregion

        #region DeleteOperatorDetails

        public void DeleteOperatorDetails(int idOperator)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteOperatorDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_OPERATOR", idOperator)	
		                           }
            );
        }

        #endregion

        #region SaveActorDetails

        public void SaveActorDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateActorDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
					, CreateTableParam("@PARTICULAR_UNITS", particularUnits)
					, CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region DeleteActorDetails

        public void DeleteActorDetails(int idActor)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteActorDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_ACTOR", idActor)	
		                           }
            );
        }

        #endregion

        #region SaveSimCardDetails

        public void SaveSimCardDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateSimCardDetails",
                new DB.Parameter[] {
					  new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
					, CreateTableParam("@PARTICULAR_UNITS", particularUnits)
					, CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region DeleteSimCardDetails

        public void DeleteSimCardDetails(int idActor)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteSimCardDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_SIM_CARD", idActor)	
		                           }
            );
        }

        #endregion

        #region SaveMeterDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveMeterDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateMeterDetails",
                new DB.Parameter[] {
                      new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
                    , CreateTableParam("@PARTICULAR_UNITS", particularUnits)
                    , CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region DeleteMeterDetails

        public void DeleteMeterDetails(long idMeter)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DeleteMeterDetails",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_METER", idMeter)	
		                           }
            );
        }

        #endregion

        #region SaveDeviceDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveDeviceDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateDeviceDetails",
                new DB.Parameter[] {
                      new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
                    , CreateTableParam("@PARTICULAR_UNITS", particularUnits)
                    , CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region LocationDeviceChange
        public bool LocationDeviceChange(ulong? InstalledIdLocation, ulong? InstalledIdMeter, ulong? InstalledDeviceSerialNbr, out int ErrorCode)
        {
            DB.OutParameter paramErrorCode = new DB.OutParameter("@ERROR_CODE", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "dataservice_LocationDeviceChange",
                new DB.Parameter[] {
					  InstalledIdLocation.HasValue ? new DB.InParameter("@INSTALLED_ID_LOCATION", InstalledIdLocation)
                        : new DB.InNullParameter("@INSTALLED_ID_LOCATION")
					, InstalledIdMeter.HasValue ? new DB.InParameter("@INSTALLED_ID_METER", InstalledIdMeter) 
                        : new DB.InNullParameter("@INSTALLED_ID_METER")
					, InstalledDeviceSerialNbr.HasValue ? new DB.InParameter("@INSTALLED_SERIAL_NBR", InstalledDeviceSerialNbr) 
                        : new DB.InNullParameter("@INSTALLED_SERIAL_NBR")
					, paramErrorCode
									}
            );

            ErrorCode = (int)paramErrorCode.Value;
            return ErrorCode == 0;
        }
        #endregion

        #region SaveActionData
        public void SaveActionData(ref long? IdActionData, long IdDataType, int IndexNbr, object Value)
        {
            DB.InOutParameter paramIdActionData;

            paramIdActionData = new DB.InOutParameter("@ID_ACTION_DATA", IdActionData);

            DB.ExecuteNonQueryProcedure(
                "dataservice_SaveActionData",
                new DB.Parameter[] {
                      paramIdActionData
					, new DB.InParameter("@ID_DATA_TYPE", IdDataType)
					, new DB.InParameter("@INDEX_NBR", IndexNbr)
					, ParamObject("@VALUE", Value)
									}
            );

            IdActionData = paramIdActionData.IsNull ? null : (long?)(long)paramIdActionData.Value;
        }
        #endregion

        #region Testowe
        public void GetStatusForLocations(List<DB_LOCATION> locations)
        {
            Dictionary<long, DB_LOCATION> dictLocations = locations.ToDictionary(l => l.ID_LOCATION);
            DataSet ds = ExecuteProcedure("sima_GetLocations", new object[] { locations[0].ID_DISTRIBUTOR, new object[] { 1 } });
            long idLocation;
            bool isNotSync;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                idLocation = GetValue<long>(ds.Tables[0].Rows[i]["ID_LOCATION"]);
                isNotSync = GetValue<bool>(ds.Tables[0].Rows[i]["IS_NOT_SYNC"]);
                if (dictLocations.ContainsKey(idLocation))
                {
                    dictLocations[idLocation].U_STATUS = isNotSync ? 1 : 0;
                }
            }
        }
        #endregion

        #region GetShippingListDevice
        public DataTable GetShippingListDevices(int idShippingList)
        {
            return (DataTable)DB.ExecuteProcedure(
                "rpt_ShippingListDevices",
                new DB.AnalyzeDataSet(GetServicesForDevices),
                new DB.Parameter[] { 
                        new DB.InParameter("@ID_SHIPPING_LIST", idShippingList)
					}, 3600
            );
        }
        #endregion

        #region GetDashboardData
        public DataSet GetDashboardData(string Procedure, Enums.DashboardItemClass DashboardItemClass, long[] IdType, int IdOperator, 
            Enums.Language Language = Enums.Language.Default,
            int[] IdDistributor = null, long[] IdLocation = null, long[] IdMeter = null, long[] SerialNbr = null,
            int CommandTimeout = 0)
        {
            return (DataSet)DB.ExecuteProcedure(Procedure,
                            new DB.AnalyzeDataSet(GetDashboardData),
                            new DB.Parameter[] {
                                CreateTableParam("@ID_TYPE", IdType),
                                CreateTableParam("@ALLOWED_DISTIBUTOR", IdDistributor == null ? DistributorFilter : IdDistributor),
                                CreateTableParam("@ALLOWED_LOCATION", IdLocation == null ? LocationFilter : IdLocation),
                                CreateTableParam("@ALLOWED_METER", IdMeter == null ? MeterFilter : IdMeter),
                                CreateTableParam("@ALLOWED_DEVICE", SerialNbr == null ? DeviceFilter : SerialNbr),
                                new DB.InParameter("@ID_CLASS", (int)DashboardItemClass),
                                new DB.InParameter("@ID_LANGUAGE", (int)Language),
                                new DB.InParameter("@ID_OPERATOR", IdOperator)},
                                AutoTransaction: true, IsolationLevel: IsolationLevel.ReadUncommitted, CommandTimeout: CommandTimeout);
        }
        private object GetDashboardData(DataSet QueryResult)
        {
            return QueryResult;
        }

        #endregion

        #region GetDeadlineDate

        public DateTime? GetDeadlineDate(DateTime? startDate = null, bool? calendarDays = null, int? days = null, string countryCode = null, int? idIssue = null, int? idTask = null)
        {
            DateTime? retDate = null;
            DB.InParameter calendarDaysParam = new DB.InParameter("@CALENDAR_DAYS", SqlDbType.Bit, calendarDays);
            DB.InParameter idIssueParam = new DB.InParameter("@ID_ISSUE", SqlDbType.Int, idIssue);
            DB.InParameter idTaskParam = new DB.InParameter("@ID_TASK", SqlDbType.Int, idTask);
            DB.InParameter daysParam = new DB.InParameter("@DAYS", SqlDbType.Int, days);
            DB.InParameter startDateParam = new DB.InParameter("@START_DATE", SqlDbType.DateTime, startDate);
            DB.InParameter countryCodeParam = new DB.InParameter("@COUNTRY_CODE", SqlDbType.NVarChar, countryCode, 10);
            DB.OutParameter deadlineParams = new DB.OutParameter("@DEADLINE_DATE", SqlDbType.DateTime);

            DB.ExecuteNonQueryProcedure(
                        "imrse_u_GetDeadlineDate",
                        new DB.Parameter[] { 
                            calendarDaysParam,
                            idIssueParam,
                            idTaskParam,
                            startDateParam,
                            daysParam,
                            countryCodeParam,
                            deadlineParams
                }
            );
            if (deadlineParams.Value != null)
                retDate = (DateTime)deadlineParams.Value;

            return retDate;
        }

        #endregion

        #region SaveServiceSummary

        public void SaveServiceSummary(int idServiceList, long idServicePackage, int amount, double cost)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveServiceSummary",
                new DB.Parameter[] {
			                             new DB.InParameter("@ID_SERVICE_LIST", idServiceList)
										,new DB.InParameter("@ID_SERVICE_PACKAGE", idServicePackage)
														,new DB.InParameter("@AMOUNT", amount)
														,new DB.InParameter("@COST", cost)
									}
            );
        }

        #endregion

        #region DeleteServiceSummary

        public void DeleteServiceSummary(int idServiceList, long idServiCePackage, double? cost)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelServiceSummary",
                new DB.Parameter[] {
                        new DB.InParameter("@ID_SERVICE_LIST", idServiceList),
                        new DB.InParameter("@ID_SERVICE_PACKAGE", idServiCePackage)	,
                        new DB.InParameter("@COST", cost)	
		}
            );
        }

        #endregion

        #region DeleteServiceListDeviceDiagnostic

        public void DeleteServiceListDeviceDiagnostic(long serialNbr, int idServiceList)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelServiceListDeviceDiagnostic",
                new DB.Parameter[] {
            new DB.InParameter("@SERIAL_NBR", serialNbr),
            new DB.InParameter("@ID_SERVICE_LIST", idServiceList)
		}
            );
        }

        #endregion

        #region SaveServiceListDeviceDiagnostic

        public void SaveServiceListDeviceDiagnostic(long serialNbr, int idServiceList, int idDiagnosticAction, string inputText, object damageLevel, int? idDiagnosticActionResult = null)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveServiceListDeviceDiagnostic",
                new DB.Parameter[] {
			                            new DB.InParameter("@SERIAL_NBR", serialNbr)
										,new DB.InParameter("@ID_SERVICE_LIST", idServiceList)
														,new DB.InParameter("@ID_DIAGNOSTIC_ACTION", idDiagnosticAction)
                                                        ,new DB.InParameter("@ID_DIAGNOSTIC_ACTION_RESULT", idDiagnosticActionResult)
														,new DB.InParameter("@INPUT_TEXT", inputText)
														,new DB.InParameter("@DAMAGE_LEVEL", SqlDbType.Variant, damageLevel)
									}
            );
        }

        #endregion

        #region DeleteServiceListDeviceAction

        public void DeleteServiceListDeviceAction(long serialNbr, int idServiceList, long idServicePackage, long? idServiceCustom)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelServiceListDeviceAction",
                new DB.Parameter[] {
            new DB.InParameter("@SERIAL_NBR", serialNbr),			
            new DB.InParameter("@ID_SERVICE_LIST", idServiceList),	
            new DB.InParameter("@ID_SERVICE_PACKAGE", idServicePackage),	
            new DB.InParameter("@ID_SERVICE_CUSTOM", idServiceCustom)	
		}
            );
        }

        #endregion

        #region SaveServiceListDeviceAction

        public void SaveServiceListDeviceAction(long serialNbr, int idServiceList, long idServciePackage, long? idServiceCustom)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveServiceListDeviceAction",
                new DB.Parameter[] {
			                             new DB.InParameter("@SERIAL_NBR", serialNbr)
										,new DB.InParameter("@ID_SERVICE_LIST", idServiceList)
										,new DB.InParameter("@ID_SERVICE_PACKAGE", idServciePackage)
										,new DB.InParameter("@ID_SERVICE_CUSTOM", idServiceCustom)
									}
            );
        }

        #endregion

        #region SaveDistributorDataByIdDistributorAndDataType

        public long SaveDistributorDataByIdDistributorAndDataType(DB_DISTRIBUTOR_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR_DATA", ToBeSaved.ID_DISTRIBUTOR_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDistributorData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        #endregion

        #region DeleteDiagnosticActionInService

        public void DeleteDiagnosticActionInService(int? idDiagnosticAction, int? idService)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDiagnosticActionInService",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE", idService),
            new DB.InParameter("@ID_DIAGNOSTIC_ACTION", idDiagnosticAction)
		}
            );
        }

        #endregion

        #region SaveDiagnosticActionInService

        public void SaveDiagnosticActionInService(int idService, int idDiagnosticAction, int indexNbr)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDiagnosticActionInService",
                new DB.Parameter[] {
                                        new DB.InParameter("@ID_SERVICE", idService),
										new DB.InParameter("@ID_DIAGNOSTIC_ACTION", idDiagnosticAction),
										new DB.InParameter("@INDEX_NBR", indexNbr)
									}
            );

        }

        #endregion

        #region DeleteDeviceOrderNumberInArticleByArticle

        public void DeleteDeviceOrderNumberInArticleByArticle(long idArticle)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelDeviceOrderNumberInArticle",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ARTICLE", idArticle)			
		}
            );
        }

        #endregion

        #region SaveDeviceOrderNumberInArticle

        public void SaveDeviceOrderNumberInArticle(long idArticle, int idDeviceOrderNumber)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDeviceOrderNumberInArticle",
                new DB.Parameter[] {
		                                 new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", idDeviceOrderNumber),
										 new DB.InParameter("@ID_ARTICLE", idArticle)
									}
            );
        }

        #endregion

        #region ServiceListDeviceDiagnostic
        public DataSet ServiceListDeviceDiagnostic(int idShippingList)
        {
            return ExecuteProcedure("rpt_ServiceListDeviceDiagnostic", 5000, new object[] { idShippingList });
        }
        #endregion

        #region SmartGasMeteringInterface
        public DB_DATA[] GetDataFilterForLocations(long[] IdLocation, long[] IdDataType, string customWhereClause)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            List<string> whereClauses = new List<string>();
            string whereClause = "";
            foreach (var id in IdLocation)
            {
                if (whereClause.Length + string.Format("{0},", id).Length > 3900)
                {
                    if (whereClause[whereClause.Length - 1] == ',')
                        whereClause = whereClause.Remove(whereClause.Length - 1);
                    whereClauses.Add(whereClause);
                    whereClause = string.Empty;
                }
                whereClause += id.ToString() + ",";
            }
            if (whereClause[whereClause.Length - 1] == ',')
                whereClause = whereClause.Remove(whereClause.Length - 1);
            whereClauses.Add(whereClause);

            List<DB_DATA> result = new List<DB_DATA>();
            foreach (var loop in whereClauses)
            {
                result.AddRange((DB_DATA[])DB.ExecuteProcedure(
                    "dataservice_GetDataFilterForLocations",
                    new DB.AnalyzeDataSet(GetData),
                    new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					new DB.InParameter("@LOCATIONS_FOR_IN_CLAUSE", loop),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)}, 60000));
            }
            return result.ToArray();
        }

        public DB_DATA[] GetDataFilterForDevices(long[] SerialNbr, long[] IdDataType, string customWhereClause)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            List<string> whereClauses = new List<string>();
            string whereClause = "";
            foreach (var id in SerialNbr)
            {
                if (whereClause.Length + string.Format("{0},", id).Length > 3900)
                {
                    if (whereClause[whereClause.Length - 1] == ',')
                        whereClause = whereClause.Remove(whereClause.Length - 1);
                    whereClauses.Add(whereClause);
                    whereClause = string.Empty;
                }
                whereClause += id.ToString() + ",";
            }
            if (whereClause[whereClause.Length - 1] == ',')
                whereClause = whereClause.Remove(whereClause.Length - 1, 1);
            whereClauses.Add(whereClause);

            List<DB_DATA> result = new List<DB_DATA>();
            foreach (var loop in whereClauses)
            {
                result.AddRange((DB_DATA[])DB.ExecuteProcedure(
                    "dataservice_GetDataFilterForDevices",
                    new DB.AnalyzeDataSet(GetData),
                    new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					new DB.InParameter("@DEVICES_FOR_IN_CLAUSE", loop),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)}));
            }
            return result.ToArray();
        }

        public DB_DATA[] GetDataFilterForMeters(long[] IdMeter, long[] IdDataType, string customWhereClause)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));

            List<string> whereClauses = new List<string>();
            string whereClause = "";
            if (IdMeter != null && IdMeter.Length > 0)
            {
                foreach (var id in IdMeter)
                {
                    if (whereClause.Length + string.Format("{0},", id).Length > 3900)
                    {
                        if (whereClause[whereClause.Length - 1] == ',')
                            whereClause = whereClause.Remove(whereClause.Length - 1);
                        whereClauses.Add(whereClause);
                        whereClause = string.Empty;
                    }
                    whereClause += id.ToString() + ",";
                }
            }
            if (whereClause[whereClause.Length - 1] == ',')
                whereClause = whereClause.Remove(whereClause.Length - 1, 1);
            whereClauses.Add(whereClause);

            List<DB_DATA> result = new List<DB_DATA>();
            foreach (var loop in whereClauses)
            {
                result.AddRange((DB_DATA[])DB.ExecuteProcedure(
                    "dataservice_GetDataFilterForMeters",
                    new DB.AnalyzeDataSet(GetData),
                    new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					new DB.InParameter("@METERS_FOR_IN_CLAUSE", loop),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)}));
            }
            return result.ToArray();
        }

        public void UpdateLocationEntranceFloorSectionDetails(ulong idLocation, int EntranceIndexNbr, int FloorIndexNbr, int SectionNbr, out bool LocationFound)
        {
            DB.OutParameter paramLocationFound = new DB.OutParameter("@LOCATION_FOUND", SqlDbType.Bit);
            DB.ExecuteNonQueryProcedure(
               "dataservice_UpdateLocationEntranceFloorSectionDetails",
               new DB.Parameter[]
               {
		            new DB.InParameter("@ID_LOCATION", idLocation),
			        new DB.InParameter("@ENTRANCE_INDEX_NBR", EntranceIndexNbr),
                    new DB.InParameter("@FLOOR_INDEX_NBR", FloorIndexNbr),
                    new DB.InParameter("@SECTION_NBR", SectionNbr),
                    paramLocationFound
			    });
            LocationFound = (bool)paramLocationFound.Value;
        }

        public void CreateMultihop(long apulseSN, long[] retransmittedApulseSN)
        {
            DB.ExecuteNonQueryProcedure(
               "dataservice_CreateMultihop",
               new DB.Parameter[]
               {
                    new DB.InParameter("@apulse_serial_nbr", apulseSN),
                    CreateTableParam("@retransmitted_apulses_serial_nbr", retransmittedApulseSN)
			    });
        }

        public void FindMeterID(int DistributorID, long DeviceSerialNbr, string MeterSerialNumber, out long MeterID)
        {
            MeterID = 0;
            DB.OutParameter paramFoundMeterID = new DB.OutParameter("@FOUND_METER_ID", SqlDbType.BigInt);
            DB.ExecuteNonQueryProcedure(
               "dataservice_FindMeterID",
               new DB.Parameter[]
               {
		            new DB.InParameter("@ID_DISTRIBUTOR", DistributorID),
			        new DB.InParameter("@SERIAL_NBR", DeviceSerialNbr),
                    new DB.InParameter("@METER_SERIAL_NUMBER", MeterSerialNumber),
                    paramFoundMeterID
			    });
            MeterID = (long)paramFoundMeterID.Value;
        }

        public void FindMeterIDByDeviceSerial(int DistributorID, long DeviceSerialNbr, out long MeterID)
        {
            MeterID = 0;
            DB.OutParameter paramFoundMeterID = new DB.OutParameter("@FOUND_METER_ID", SqlDbType.BigInt);
            DB.ExecuteNonQueryProcedure(
               "dataservice_FindMeterIDByDeviceSerial",
               new DB.Parameter[]
               {
		            new DB.InParameter("@ID_DISTRIBUTOR", DistributorID),
			        new DB.InParameter("@SERIAL_NBR", DeviceSerialNbr),
                    paramFoundMeterID
			    });
            MeterID = (long)paramFoundMeterID.Value;
        }

        public void CreateLocationDuringInstallation(string LocationName, string LocationCity, string LocationAddress, string LocationCountry, string locationCID, long groupingLocationID, int distributorID, int operatorID,
           out long CreatedLocationID, out int CreatedTaskID, out int ErrorCode)
        {
            DB.OutParameter paramCreatedLocationID = new DB.OutParameter("@CREATED_LOCATION_ID", SqlDbType.BigInt);
            DB.OutParameter paramCreatedTaskID = new DB.OutParameter("@CREATED_TASK_ID", SqlDbType.Int);
            DB.OutParameter paramErrorCode = new DB.OutParameter("@ERROR_CODE", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "dataservice_CreateLocationDuringInstallation",
                new DB.Parameter[] {
					  new DB.InParameter("@LOCATION_NAME", LocationName)
					, new DB.InParameter("@LOCATION_CITY", LocationCity)
					, new DB.InParameter("@LOCATION_ADDRESS", LocationAddress)
					, new DB.InParameter("@LOCATION_COUNTRY", LocationCountry)
					, new DB.InParameter("@LOCATION_CID", locationCID)
                    , new DB.InParameter("@GROUPING_LOCATION_ID", groupingLocationID)
                    , new DB.InParameter("@ID_DISTRIBUTOR", distributorID)
                    , new DB.InParameter("@ID_OPERATOR ", operatorID)
					, paramCreatedLocationID
                    , paramCreatedTaskID
                    , paramErrorCode
                });

            CreatedLocationID = (long)paramCreatedLocationID.Value;
            CreatedTaskID = (int)paramCreatedTaskID.Value;
            ErrorCode = (int)paramErrorCode.Value;
        }

        public void GetStatusForAction(long ActionID, out int ActionStatusID)
        {
            ActionStatusID = 0;
            DB.OutParameter paramActionStatusID = new DB.OutParameter("@ID_ACTION_STATUS", SqlDbType.Int);
            DB.ExecuteNonQueryProcedure(
               "dataservice_CheckActionStatus",
               new DB.Parameter[]
               {
		            new DB.InParameter("@ID_ACTION", ActionID),
                    paramActionStatusID
			    }, false, IsolationLevel.ReadUncommitted);
            ActionStatusID = (int)paramActionStatusID.Value;
        }

        public void CreateMobileAction(long? SerialNbr, long? MeterID, long? LocationID, string MobileActionType, int? OperatorID, DateTime CreationDate,
            DataTable DataTypeValues, out long ActionID, out int Result)
        {
            ActionID = 0;
            Result = 0;
            DB.OutParameter paramActionID = new DB.OutParameter("@ActionID", SqlDbType.BigInt);
            DB.OutParameter paramResult = new DB.OutParameter("@Result", SqlDbType.Int);
            DB.ExecuteNonQueryProcedure(
               "dataservice_CreateMobileAction",
               new DB.Parameter[]
               {
                   new DB.InParameter("@SerialNbr", SerialNbr),
                   new DB.InParameter("@MeterID", MeterID),
                   new DB.InParameter("@LocationID", LocationID),
                   new DB.InParameter("@MobileActionType", MobileActionType),
                   new DB.InParameter("@OperatorID", OperatorID),
		           new DB.InParameter("@CreationDate", CreationDate),
                   new DB.InParameter("@DataTypeValues", SqlDbType.Structured, DataTypeValues),
                   paramActionID,
                   paramResult
			    });
            ActionID = (long)paramActionID.Value;
            Result = (int)paramResult.Value;
        }

        public void CreateOnlyLocationDuringInstallation(string LocationName, string LocationCity, string LocationAddress, string LocationCountry, string locationCID, long groupingLocationID, int distributorID, int operatorID,
           out long CreatedLocationID, out int ErrorCode)
        {
            DB.OutParameter paramCreatedLocationID = new DB.OutParameter("@CREATED_LOCATION_ID", SqlDbType.BigInt);
            DB.OutParameter paramErrorCode = new DB.OutParameter("@ERROR_CODE", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "dataservice_CreateOnlyLocationDuringInstallation",
                new DB.Parameter[] {
					  new DB.InParameter("@LOCATION_NAME", LocationName)
					, new DB.InParameter("@LOCATION_CITY", LocationCity)
					, new DB.InParameter("@LOCATION_ADDRESS", LocationAddress)
					, new DB.InParameter("@LOCATION_COUNTRY", LocationCountry)
					, new DB.InParameter("@LOCATION_CID", locationCID)
                    , new DB.InParameter("@GROUPING_LOCATION_ID", groupingLocationID)
                    , new DB.InParameter("@ID_DISTRIBUTOR", distributorID)
                    , new DB.InParameter("@ID_OPERATOR ", operatorID)
					, paramCreatedLocationID
                    , paramErrorCode
                });

            CreatedLocationID = (long)paramCreatedLocationID.Value;
            ErrorCode = (int)paramErrorCode.Value;
        }

        public void CreateTaskDuringInstallation(long locationID, int distributorID, int operatorID, out int CreatedTaskID)
        {
            DB.OutParameter paramCreatedTaskID = new DB.OutParameter("@CREATED_TASK_ID", SqlDbType.Int);

            DB.ExecuteNonQueryProcedure(
                "dataservice_CreateTaskDuringInstallation",
                new DB.Parameter[] {
					  new DB.InParameter("@ID_LOCATION", locationID)
                    , new DB.InParameter("@ID_DISTRIBUTOR", distributorID)
                    , new DB.InParameter("@ID_OPERATOR ", operatorID)
                    , paramCreatedTaskID
                });

            CreatedTaskID = (int)paramCreatedTaskID.Value;
        }

        public long GetFreeSerialNbrForI2CProbe(SqlCommand sqlCommand = null)
        {
            object res = DB.ExecuteScalar("dataservice_GetFreeSerialNbrForI2CProbe", sqlCommand);

            long result = Convert.ToInt64(res);
            return result;
        }

        #region UpdateInsertDataForLocation
        public int UpdateInsertLocationData(long? IdLocation, DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType, SqlCommand sqlCommand = null)
        {
            int result = UpdateInsertData(null, null, IdLocation, ToBeSaved, ToBeSavedType, sqlCommand);
            return result;
        }
        #endregion
        #region UpdateInsertDataForMeter
        public int UpdateInsertMeterData(long? IdMeter, DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType, SqlCommand sqlCommand = null)
        {
            int result = UpdateInsertData(null, IdMeter, null, ToBeSaved, ToBeSavedType, sqlCommand);
            return result;
        }
        #endregion
        #region UpdateInsertDataForDevice
        public int UpdateInsertDeviceData(long? SerialNbr, DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType, SqlCommand sqlCommand = null)
        {
            int result = UpdateInsertData(SerialNbr, null, null, ToBeSaved, ToBeSavedType, sqlCommand);
            return result;
        }
        #endregion
        #region UpdateInsertData
        public int UpdateInsertData(long? SerialNbr, long? IdMeter, long? IdLocation, DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType, SqlCommand sqlCommand = null)
        {
            DB.InParameter paramSerialNbr;
            DB.InParameter paramIdMeter;
            DB.InParameter paramIdLocation;
            if (SerialNbr != null)
                paramSerialNbr = new DB.InParameter("@SERIAL_NBR", SerialNbr);
            else
                paramSerialNbr = new DB.InNullParameter("@SERIAL_NBR");
            if (IdMeter != null)
                paramIdMeter = new DB.InParameter("@ID_METER", IdMeter);
            else
                paramIdMeter = new DB.InNullParameter("@ID_METER");
            if (IdLocation != null)
                paramIdLocation = new DB.InParameter("@ID_LOCATION", IdLocation);
            else
                paramIdLocation = new DB.InNullParameter("@ID_LOCATION");

            int result = sqlCommand == null
                ? (int)DB.ExecuteScalar(
                "sp_InsertUpdateDataValue",
                new DB.Parameter[]
				{
                    paramSerialNbr,
                    paramIdMeter,
                    paramIdLocation,
                    new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE),
                    new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR),
                    ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS),                 
				},
                true,
                this.ShortTimeout)
                : (int)DB.ExecuteScalar(
                "sp_InsertUpdateDataValue",
                new DB.Parameter[]
				{
                    paramSerialNbr,
                    paramIdMeter,
                    paramIdLocation,
                    new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE),
                    new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR),
                    ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS),
				},
                sqlCommand);

            return result;
        }
        #endregion


        #endregion

        #region IMR-SCADA interface
        public int IMRScadaManageLocation(int manageAction, string name, string address, string city, string rdg, string cid, float? latitude, float? longitude,
            string meterSerie, float? meterQmin, float? meterQmax, int? meterSize, float? meterMaxIndexValue, string meterSerialNbr, float? meterImpulseWeight)
        {
            List<DB.Parameter> paramsList = new List<Data.DB.DB.Parameter>();
            //Parametry lokalizacji
            paramsList.Add(new DB.InParameter("@MANAGE_ACTION", manageAction));
            paramsList.Add(new DB.InParameter("@NAME", name));
            paramsList.Add(new DB.InParameter("@ADDRESS", address));
            paramsList.Add(new DB.InParameter("@CITY", city));
            paramsList.Add(new DB.InParameter("@RDG", rdg));
            paramsList.Add(new DB.InParameter("@CID", cid));
            if (latitude.HasValue)
                paramsList.Add(new DB.InParameter("@LATITUDE", latitude));
            if (longitude.HasValue)
                paramsList.Add(new DB.InParameter("@LONGITUDE", longitude));
            //Parametry metera
            if (meterSerie != null)
                paramsList.Add(new DB.InParameter("@METER_SERIE", meterSerie));
            if (meterQmin.HasValue)
                paramsList.Add(new DB.InParameter("@METER_Q_MIN", meterQmin));
            if (meterQmax.HasValue)
                paramsList.Add(new DB.InParameter("@METER_Q_MAX", meterQmax));
            if (meterSize.HasValue)
                paramsList.Add(new DB.InParameter("@METER_SIZE", meterSize));
            if (meterMaxIndexValue.HasValue)
                paramsList.Add(new DB.InParameter("@METER_MAX_INDEX_VALUE", meterMaxIndexValue));
            if (meterSerialNbr != null)
                paramsList.Add(new DB.InParameter("@METER_SERIAL_NBR", meterSerialNbr));
            if (meterImpulseWeight.HasValue)
                paramsList.Add(new DB.InParameter("@METER_IMPULSE_WEIGHT", meterImpulseWeight));


            int result = (int)DB.ExecuteScalar("dataservice_IMRScadaManageLocation", paramsList.ToArray());

            return result;
        }

        public DataTable IMRScadaGetAllLocationDetails(string[] LocationCIDs)
        {
            DataTable LocationCIDsDT = new DataTable();
            LocationCIDsDT.Columns.Add("VALUE", typeof(string));
            if (LocationCIDs != null)
            {
                foreach (string cid in LocationCIDs)
                {
                    LocationCIDsDT.Rows.Add(cid);
                }
            }

            return (DataTable)DB.ExecuteProcedure(
                "dataservice_IMRScadaGetAllLocationDetails",
                new DB.AnalyzeDataSet(IMRScadaGetAllLocationDetails),
                new DB.Parameter[] { new DB.InParameter("@LocationCIDs", SqlDbType.Structured, LocationCIDsDT), });

        }

        private object IMRScadaGetAllLocationDetails(DataSet QueryResult)
        {
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("ID");
            resultTable.Columns.Add("Name");
            resultTable.Columns.Add("City");
            resultTable.Columns.Add("Address");
            resultTable.Columns.Add("CID");
            resultTable.Columns.Add("Latitude");
            resultTable.Columns.Add("Longitude");
            resultTable.Columns.Add("RDG");
            resultTable.Columns.Add("MeterType");
            resultTable.Columns.Add("MeterSerialNbr");
            resultTable.Columns.Add("MeterImpWeight");
            resultTable.Columns.Add("MeterSeries");
            resultTable.Columns.Add("MeterQmin");
            resultTable.Columns.Add("MeterQmax");
            resultTable.Columns.Add("MeterSize");
            resultTable.Columns.Add("MeterCounterFormat");
            resultTable.Columns.Add("DeviceInstallationDate");
            resultTable.Columns.Add("DeviceSerialNbr");
            resultTable.Columns.Add("DeviceOrderNbr");

            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long locationID = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string name = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string city = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string address = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string cid = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                float? latitude = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                float? longitude = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                string rdg = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                string meterType = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string meterSerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                float? meterImpWeight = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                string meterSeries = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                float? meterQmin = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                float? meterQmax = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);
                int? meterSize = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                float? meterCounterFormat = GetValue<float>(QueryResult.Tables[0].Rows[i][col++]);

                DateTime? deviceInstallationDate = GetValue<DateTime>(QueryResult.Tables[0].Rows[i][col++]);
                long? deviceSerialNbr = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string deviceOrderNbr = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                if (deviceSerialNbr == 0)
                    deviceInstallationDate = null;

                resultTable.Rows.Add(locationID, name, city, address, cid, latitude, longitude, rdg, meterType, meterSerialNbr, meterImpWeight, meterSeries, meterQmin, meterQmax, meterSize, meterCounterFormat,
                    deviceInstallationDate, deviceSerialNbr, deviceOrderNbr);
            }
            return resultTable;
        }

        public DataTable IMRScadaGetPointIDAndRDGForLocations(int? DeviceTypeID, int? DistributorID)
        {
            List<DB.Parameter> paramsList = new List<Data.DB.DB.Parameter>();
            paramsList.Add(new DB.InParameter("@DeviceTypeID", DeviceTypeID));
            paramsList.Add(new DB.InParameter("@DistributorID", DistributorID));

            return (DataTable)DB.ExecuteProcedure(
                "dataservice_IMRScadaGetPointIDAndRDGForLocations",
                new DB.AnalyzeDataSet(IMRScadaGetPointIDAndRDGForLocations),
                paramsList.ToArray());
        }

        private object IMRScadaGetPointIDAndRDGForLocations(DataSet QueryResult)
        {
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("ID_LOCATION");
            resultTable.Columns.Add("POINT_CODE");
            resultTable.Columns.Add("RDG");

            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long locationID = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string pointCode = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string rdg = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                resultTable.Rows.Add(locationID, pointCode, rdg);
            }
            return resultTable;
        }

        #endregion

        #region SGM initializing data procedures
        public object SgmInitializeDataGetLocations(int idDistributor, int idOperator, long[] locationIdDataTypes, DB.AnalyzeDataSetAndParameters analyzeDataDelegate)
        {
            List<DB.Parameter> parameterList = new List<Data.DB.DB.Parameter>(){new DB.InParameter("@IdDistributor", idDistributor), 
                CreateTableParam("@IdDataTypesLocation", locationIdDataTypes), new DB.InParameter("@IdOperator", idOperator)};//
            var result = DB.ExecuteProcedure("sgm_LoadLocationData", analyzeDataDelegate, parameterList.ToArray(), AutoTransaction: false, IsolationLevel: IsolationLevel.ReadUncommitted);

            return result;
        }

        public object SgmInitializeDataGetDeviceMeter(int idDistributor, int idOperator, long[] deviceIdDataTypes, long[] meterIdDataTypes, DB.AnalyzeDataSetAndParameters analyzeDataDelegate)
        {
            List<DB.Parameter> parameterList = new List<DB.Parameter>(){new DB.InParameter("@IdDistributor", idDistributor)
            , CreateTableParam("@IdDataTypesDevice", deviceIdDataTypes), CreateTableParam("@IdDataTypesMeter", meterIdDataTypes)
            , new DB.InParameter("@IdOperator", idOperator)};

            var result = DB.ExecuteProcedure("sgm_LoadDeviceMeterData", analyzeDataDelegate, parameterList.ToArray(), AutoTransaction: false, IsolationLevel: IsolationLevel.ReadUncommitted);
            return result;
        }

        //sgm_LoadConsumerData
        public object SgmInitializeDataGetConsumer(int idDistributor, int idOperator, long[] consumerDataTypes, DB.AnalyzeDataSetAndParameters analyzeDataDelegate)
        {
            List<DB.Parameter> parameterList = new List<DB.Parameter>(){new DB.InParameter("@IdDistributor", idDistributor)
            , CreateTableParam("@IdDataTypesConsumer", consumerDataTypes), new DB.InParameter("@IdOperator", idOperator)};

            var result = DB.ExecuteProcedure("sgm_LoadConsumerData", analyzeDataDelegate, parameterList.ToArray(), AutoTransaction: false, IsolationLevel: IsolationLevel.ReadUncommitted);
            return result;
        }
        #endregion

        #region SGM device data loader
        public long CreateNewDeviceFromProductionXML(long serialNumber, int idDeviceType, string codeName, string firstQuarter, string secondQuarter, string thirdQuarter,
            long? serialNbrPattern, string phone, string factoryNumber,
            int MainDeviceRadioAddress,
            int OlanRadioAddress,
            int MeterRadioAddress,
            int OkoRadioAddress,

            string ValveOpenCode1, string ValveOpenCode2, string ValveOpenCode3,
            string ValveOpenCode4, string ValveOpenCode5, string ValveOpenCode6,
            string ValveOpenCode7, string ValveOpenCode8, string ValveOpenCode9,

            string ExtendedMenuCode1, string ExtendedMenuCode2, string ExtendedMenuCode3, string ExtendedMenuCode4,

            int idDistributor,
            DateTime? WarrantyDate,
            long IdLocation,
            bool deviceHasSimCard,
            out string errorMsg)
        {
            try
            {
                #region radio addresses
                DataTable dtRadioAdresses = new DataTable();
                dtRadioAdresses.Columns.Add("RADIO_ADDRESS", System.Type.GetType("System.Int64"));
                dtRadioAdresses.Columns.Add("RADIO_ADDRESS_TYPE", System.Type.GetType("System.Int32"));

                //RADIO_ADDRESS <radio_address, radio_address_type>:
                //1 - main device radio address - radio_address
                //2 - olan radio address - radio_address
                //3 - meter radio address - radio_address_2
                //4 - oko radio address - radio_address_4
                long res = 0;
                if (MainDeviceRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", MainDeviceRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)1);
                }
                if (OlanRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", OlanRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)2);
                }
                if (MeterRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", MeterRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)3);
                }
                if (OkoRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", OkoRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)4);
                }
                #endregion
                #region menu codes
                //CODE <code_type, code_nbr, code>
                //1 - extended menu code
                //2 - valve pin code
                DataTable dtCodes = new DataTable();
                dtCodes.Columns.Add("ID_CODE_TYPE", System.Type.GetType("System.Int32"));
                dtCodes.Columns.Add("CODE_NBR", System.Type.GetType("System.Int32"));
                dtCodes.Columns.Add("CODE", System.Type.GetType("System.String"));

                //extended menu codes
                if (ExtendedMenuCode1 != null && ExtendedMenuCode1.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)1, (string)ExtendedMenuCode1);
                if (ExtendedMenuCode2 != null && ExtendedMenuCode2.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)2, (string)ExtendedMenuCode2);
                if (ExtendedMenuCode3 != null && ExtendedMenuCode3.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)3, (string)ExtendedMenuCode3);
                if (ExtendedMenuCode4 != null && ExtendedMenuCode4.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)4, (string)ExtendedMenuCode4);
                //valve open codes
                if (ValveOpenCode1 != null && ValveOpenCode1.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)1, (string)ValveOpenCode1);
                if (ValveOpenCode2 != null && ValveOpenCode2.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)2, (string)ValveOpenCode2);
                if (ValveOpenCode3 != null && ValveOpenCode3.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)3, (string)ValveOpenCode3);
                if (ValveOpenCode4 != null && ValveOpenCode4.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)4, (string)ValveOpenCode4);
                if (ValveOpenCode5 != null && ValveOpenCode5.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)5, (string)ValveOpenCode5);
                if (ValveOpenCode6 != null && ValveOpenCode6.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)6, (string)ValveOpenCode6);
                if (ValveOpenCode7 != null && ValveOpenCode7.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)7, (string)ValveOpenCode7);
                if (ValveOpenCode8 != null && ValveOpenCode8.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)8, (string)ValveOpenCode8);
                if (ValveOpenCode9 != null && ValveOpenCode9.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)9, (string)ValveOpenCode9);
                #endregion

                errorMsg = String.Empty;
                int errorCode = 0;
                errorCode = (int)DB.ExecuteScalar(
                    "tmq_CreateNewDeviceFromProductionXML",
                    new DB.Parameter[]
                    {
                        new DB.InParameter ("@SERIAL_NBR", SqlDbType.BigInt, serialNumber),
                        new DB.InParameter ("@ID_DEVICE_TYPE", idDeviceType),
                        new DB.InParameter ("@CODE_NAME", SqlDbType.NVarChar, codeName, 20),
                        new DB.InParameter ("@FIRST_QUARTER", SqlDbType.NVarChar, firstQuarter, 4),
                        new DB.InParameter ("@SECOND_QUARTER", SqlDbType.NVarChar, secondQuarter, 4),
                        new DB.InParameter ("@THIRD_QUARTER", SqlDbType.NVarChar, thirdQuarter, 4),
                        new DB.InParameter ("@SERIAL_NBR_PATTERN", SqlDbType.BigInt, serialNbrPattern.HasValue ? (object)serialNbrPattern.Value : (object)DBNull.Value),
                        new DB.InParameter ("@PHONE", SqlDbType.NVarChar, phone, 30),
                        new DB.InParameter ("@FACTORY_NUMBER", SqlDbType.NVarChar, factoryNumber, 20),
                        new DB.InParameter ("@RADIO_ADDRESS", SqlDbType.Structured, dtRadioAdresses),
                        new DB.InParameter ("@CODE", SqlDbType.Structured, dtCodes),
                        new DB.InParameter ("@ID_DISTRIBUTOR", SqlDbType.Int, idDistributor),
                        new DB.InParameter ("@DEVICE_HAS_SIM_CARD_INSTALLED", SqlDbType.Bit, deviceHasSimCard)//,
                        //new DB.InParameter ("@WARRANTY_DATE", SqlDbType.DateTime, WarrantyDate),
                        //new DB.InParameter ("@ID_LOCATION", SqlDbType.BigInt, IdLocation)
                    }
                    );
                return errorCode;
                //0 - OK
                //1 - Nieznany typ urzadzenia
                //2 - Brak konfiguracji wzorca na serwerze
                //3 - OK - Urzadzenie o tym numerze seryjnym juz istnieje na serwerze
                //4 - Podano telefon karty sim, ktora nie jest zarejestrowana na serwerze
                //5 - Istnieje juz urzadzenie korzystajace z karty SIM o podanym numerze telefonu
                //6 - Chcesz utworzyc OKO a nie podajesz numeru telefonu karty SIM która jest w nim umieszczona
            }

            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return -1;
            }
        }

        public long CreateNewDeviceFromProductionXML2(long serialNumber, int idDeviceType, string codeName, string firstQuarter, string secondQuarter, string thirdQuarter,
            long? serialNbrPattern, string phone, string factoryNumber,
            int MainDeviceRadioAddress,
            int OlanRadioAddress,
            int MeterRadioAddress,
            int OkoRadioAddress,

            string ValveOpenCode1, string ValveOpenCode2, string ValveOpenCode3,
            string ValveOpenCode4, string ValveOpenCode5, string ValveOpenCode6,
            string ValveOpenCode7, string ValveOpenCode8, string ValveOpenCode9,

            string ExtendedMenuCode1, string ExtendedMenuCode2, string ExtendedMenuCode3, string ExtendedMenuCode4,

            bool deviceHasSimCard,
            int idDistributor,
            DateTime? WarrantyDate,
            long idLocation,
            int idMeterType,
            string meterSerialNbr, string meterM3PerImpulse, string meterSeries,
            float? meterQMin, float? meterQMax, int? meterSize, float? meterCounterFormat, out string errorMsg,
            byte[] cryptographyKey = null, byte[] macKey = null, byte[] privateKey = null,
            byte[] cryptographyKeyEncrypted = null, byte[] macKeyEncrypted = null, byte[] privateKeyEncrypted = null)
        {
            try
            {
                //DataTable meterData = new DataTable();

                //meterData.Columns.Add("BIGINT_VALUE", typeof(long));
                //meterData.Columns.Add("SQLVARIANT_VALUE", typeof(object));
                //meterData.Columns.Add("INT_VALUE", typeof(int));
                ////meterData.Columns.Add("VALUE", typeof(object));
                //foreach (DB_DATA el in meterDataList)
                //{
                //    meterData.Rows.Add(el.ID_DATA_TYPE, (object)el.VALUE, el.INDEX_NBR); //(object)(el.VALUE.ToString().ToCharArray())
                //}

                #region radio adresses
                DataTable dtRadioAdresses = new DataTable();
                dtRadioAdresses.Columns.Add("RADIO_ADDRESS", System.Type.GetType("System.Int64"));
                dtRadioAdresses.Columns.Add("RADIO_ADDRESS_TYPE", System.Type.GetType("System.Int32"));

                //RADIO_ADDRESS <radio_address, radio_address_type>:
                //1 - main device radio address - radio_address
                //2 - olan radio address - radio_address
                //3 - meter radio address - radio_address_2
                //4 - oko radio address - radio_address_4
                long res = 0;
                if (MainDeviceRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", MainDeviceRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)1);
                }
                if (OlanRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", OlanRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)2);
                }
                if (MeterRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", MeterRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)3);
                }
                if (OkoRadioAddress != -1)
                {
                    if (long.TryParse(String.Format("{0:X}", OkoRadioAddress), out res))
                        dtRadioAdresses.Rows.Add(res, (int)4);
                }
                #endregion

                #region menu codes
                //CODE <code_type, code_nbr, code>
                //1 - extended menu code
                //2 - valve pin code
                DataTable dtCodes = new DataTable();
                dtCodes.Columns.Add("ID_CODE_TYPE", System.Type.GetType("System.Int32"));
                dtCodes.Columns.Add("CODE_NBR", System.Type.GetType("System.Int32"));
                dtCodes.Columns.Add("CODE", System.Type.GetType("System.String"));

                //extended menu codes
                if (ExtendedMenuCode1 != null && ExtendedMenuCode1.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)1, (string)ExtendedMenuCode1);
                if (ExtendedMenuCode2 != null && ExtendedMenuCode2.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)2, (string)ExtendedMenuCode2);
                if (ExtendedMenuCode3 != null && ExtendedMenuCode3.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)3, (string)ExtendedMenuCode3);
                if (ExtendedMenuCode4 != null && ExtendedMenuCode4.Trim().Length > 0)
                    dtCodes.Rows.Add((int)1, (int)4, (string)ExtendedMenuCode4);
                //valve open codes
                if (ValveOpenCode1 != null && ValveOpenCode1.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)1, (string)ValveOpenCode1);
                if (ValveOpenCode2 != null && ValveOpenCode2.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)2, (string)ValveOpenCode2);
                if (ValveOpenCode3 != null && ValveOpenCode3.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)3, (string)ValveOpenCode3);
                if (ValveOpenCode4 != null && ValveOpenCode4.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)4, (string)ValveOpenCode4);
                if (ValveOpenCode5 != null && ValveOpenCode5.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)5, (string)ValveOpenCode5);
                if (ValveOpenCode6 != null && ValveOpenCode6.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)6, (string)ValveOpenCode6);
                if (ValveOpenCode7 != null && ValveOpenCode7.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)7, (string)ValveOpenCode7);
                if (ValveOpenCode8 != null && ValveOpenCode8.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)8, (string)ValveOpenCode8);
                if (ValveOpenCode9 != null && ValveOpenCode9.Trim().Length > 0)
                    dtCodes.Rows.Add((int)2, (int)9, (string)ValveOpenCode9);
                #endregion

                List<DB.Parameter> parameters = new List<DB.Parameter>();

                parameters.Add(new DB.InParameter("@SERIAL_NBR", SqlDbType.BigInt, serialNumber));
                parameters.Add(new DB.InParameter("@ID_DEVICE_TYPE", SqlDbType.Int, idDeviceType));
                parameters.Add(new DB.InParameter("@CODE_NAME", SqlDbType.NVarChar, codeName, 20));
                parameters.Add(new DB.InParameter("@FIRST_QUARTER", SqlDbType.NVarChar, firstQuarter, 4));
                parameters.Add(new DB.InParameter("@SECOND_QUARTER", SqlDbType.NVarChar, secondQuarter, 4));
                parameters.Add(new DB.InParameter("@THIRD_QUARTER", SqlDbType.NVarChar, thirdQuarter, 4));
                parameters.Add(new DB.InParameter("@SERIAL_NBR_PATTERN", SqlDbType.BigInt, serialNbrPattern.HasValue ? (object)serialNbrPattern.Value : (object)DBNull.Value));
                parameters.Add(new DB.InParameter("@PHONE", SqlDbType.NVarChar, phone, 30));
                parameters.Add(new DB.InParameter("@FACTORY_NUMBER", SqlDbType.NVarChar, factoryNumber, 20));
                parameters.Add(new DB.InParameter("@RADIO_ADDRESS", SqlDbType.Structured, dtRadioAdresses));
                parameters.Add(new DB.InParameter("@CODE", SqlDbType.Structured, dtCodes));
                parameters.Add(new DB.InParameter("@DEVICE_HAS_SIM_CARD_INSTALLED", SqlDbType.Bit, deviceHasSimCard));
                parameters.Add(new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, idDistributor));
                parameters.Add(new DB.InParameter("@ID_LOCATION_DEPOSITORY", SqlDbType.BigInt, idLocation));
                parameters.Add(new DB.InParameter("@ID_METER_TYPE", SqlDbType.BigInt, idMeterType != 0 ? (long)idMeterType : (long?)null));

                parameters.Add(new DB.InParameter("@METER_SERIAL_NBR", SqlDbType.NVarChar, meterSerialNbr, 4000)); //can be (meterSerialNbr != String.Empty ? (object)meterSerialNbr : (object)DBNull.Value)
                parameters.Add(new DB.InParameter("@METER_M3_PER_IMPULSE", SqlDbType.NVarChar, meterM3PerImpulse, 20));
                parameters.Add(new DB.InParameter("@METER_SERIES", SqlDbType.NVarChar, (meterSeries != String.Empty ? (object)meterSeries : (object)DBNull.Value), 4000));
                parameters.Add(new DB.InParameter("@METER_Q_MIN", SqlDbType.Real, meterQMin));
                parameters.Add(new DB.InParameter("@METER_Q_MAX", SqlDbType.Real, meterQMax));
                parameters.Add(new DB.InParameter("@METER_SIZE", SqlDbType.Int, meterSize));
                parameters.Add(new DB.InParameter("@METER_COUNTER_FORMAT", SqlDbType.Real, meterCounterFormat));

                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_CRYPTOGRAPHY_KEY", this))
                    parameters.Add(new DB.InParameter("@DEVICE_CRYPTOGRAPHY_KEY", SqlDbType.Binary, cryptographyKey, 4000));
                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_MAC_KEY", this))
                    parameters.Add(new DB.InParameter("@DEVICE_MAC_KEY", SqlDbType.Binary, macKey, 4000));
                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_PRIVATE_KEY", this))
                    parameters.Add(new DB.InParameter("@DEVICE_PRIVATE_KEY", SqlDbType.Binary, privateKey, 4000));
                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_CRYPTOGRAPHY_KEY_ENCRYPTED", this))
                    parameters.Add(new DB.InParameter("@DEVICE_CRYPTOGRAPHY_KEY_ENCRYPTED", SqlDbType.Binary, cryptographyKeyEncrypted, 4000));
                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_MAC_KEY_ENCRYPTED", this))
                    parameters.Add(new DB.InParameter("@DEVICE_MAC_KEY_ENCRYPTED", SqlDbType.Binary, macKeyEncrypted, 4000));
                if (IsParameterDefined("tmq_CreateNewDeviceFromProductionXML2", "@DEVICE_PRIVATE_KEY_ENCRYPTED", this))
                    parameters.Add(new DB.InParameter("@DEVICE_PRIVATE_KEY_ENCRYPTED", SqlDbType.Binary, privateKeyEncrypted, 4000));

                //parameters.Add(new DB.InParameter("@METER_DATA", SqlDbType.Structured, meterData));

                errorMsg = String.Empty;
                int errorCode = 0;
                errorCode = (int)DB.ExecuteScalar(
                    "tmq_CreateNewDeviceFromProductionXML2",
                    parameters.ToArray(), 500
                    );
                return errorCode;
                //0 - OK
                //1 - Nieznany typ urzadzenia
                //2 - Brak konfiguracji wzorca na serwerze
                //3 - OK - Urzadzenie o tym numerze seryjnym juz istnieje na serwerze
                //4 - Podano telefon karty sim, ktora nie jest zarejestrowana na serwerze
                //5 - Istnieje juz urzadzenie korzystajace z karty SIM o podanym numerze telefonu
                //6 - Chcesz utworzyc OKO a nie podajesz numeru telefonu karty SIM która jest w nim umieszczona
            }

            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return -1;
            }
        }

        public bool AddSimCard(int idDistributor, string serialNumber, string phone, string pin, string puk, string ip, int mobileNetworkCode,
            out string errorMsg)
        {
            try
            {
                errorMsg = String.Empty;
                DB.ExecuteNonQueryProcedure(
                    "tmq_AddSimCardXML",
                    new DB.Parameter[]
                    {
                        new DB.InParameter ("@ID_DISTRIBUTOR", SqlDbType.Int, idDistributor),
                        new DB.InParameter ("@SERIAL_NBR", SqlDbType.NVarChar, serialNumber, 200),
                        new DB.InParameter ("@PHONE", SqlDbType.NVarChar, phone, 30),
                        new DB.InParameter ("@PIN", SqlDbType.NVarChar, pin, 4),
                        new DB.InParameter ("@PUK", SqlDbType.NVarChar, puk, 8),
                        new DB.InParameter ("@IP", SqlDbType.NVarChar, ip != null ? (object)ip : (object)DBNull.Value, 15),
                        new DB.InParameter ("@MOBILE_NETWORK_CODE", SqlDbType.Int, mobileNetworkCode)
                    }
                    );
                return true;
            }

            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
        }

        public bool UpdatePhoneForDevice(long serialNumber, string phone,
            out string errorMsg)
        {
            try
            {
                errorMsg = String.Empty;
                DB.ExecuteNonQueryProcedure(
                    "tmq_UpdatePhoneForDeviceXML",
                    new DB.Parameter[]
                    {
                        new DB.InParameter ("@SERIAL_NBR", SqlDbType.BigInt, serialNumber),
                        new DB.InParameter ("@PHONE", SqlDbType.VarChar, phone, 15)
                    }
                    );
                return true;
            }

            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
        }

        public bool RemoveDeviceDuringFileUploading(long serialNbr, long? idMeter)
        {
            try
            {
                DB.ExecuteNonQueryProcedure(
                    "tmq_RemoveDeviceDuringFileUploadingXML",
                    new DB.Parameter[]
                    {
                        new DB.InParameter ("@SERIAL_NBR", SqlDbType.BigInt, serialNbr),
                        new DB.InParameter ("@ID_METER", SqlDbType.NVarChar, idMeter != null ? (object)idMeter : (object)DBNull.Value, 15)
                    }
                    );
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Object SgmCreateLocationFromXLS
        (
            long AreaLocationID, int DistributorID,

            Data.DB.DB.AnalyzeDataSet DataSetAnalyzer,

            string LOCATION_NAME, string LOCATION_CITY, string LOCATION_ADDRESS, string LOCATION_CID, int idLanguage,

            string LOCATION_ZIP = null, string LOCATION_COUNTRY = null,
            double? LOCATION_LATITUDE = null, double? LOCATION_LONGITUDE = null, int? LOCATION_INSTALLATION_TYPE = null,

            int? idMeterType = null, double? METER_M3_PER_IMPULSE = null, string METER_SERIAL_NUMBER = null
            , string consumerName = null, string consumerSurname = null
         )
        {
            return DB.ExecuteProcedure(
                "sgm_CreateLocationFromXLS", DataSetAnalyzer,
                new DB.Parameter[] {
                      new DB.InParameter("@AreaLocationID", AreaLocationID)
                    , new DB.InParameter("@DistributorID", DistributorID)
                    , new DB.InParameter("@LOCATION_NAME", LOCATION_NAME)
                    , new DB.InParameter("@LOCATION_CITY", LOCATION_CITY)
                    , new DB.InParameter("@LOCATION_ADDRESS", LOCATION_ADDRESS)
                    , new DB.InParameter("@LOCATION_CID", LOCATION_CID)
                    , new DB.InParameter("@LOCATION_ZIP", LOCATION_ZIP)
                    , new DB.InParameter("@LOCATION_COUNTRY", LOCATION_COUNTRY)
                    , new DB.InParameter("@LOCATION_LATITUDE", LOCATION_LATITUDE)
                    , new DB.InParameter("@LOCATION_LONGITUDE", LOCATION_LONGITUDE)
                    , new DB.InParameter("@LOCATION_INSTALLATION_TYPE", LOCATION_INSTALLATION_TYPE)
                    , new DB.InParameter("@idMeterType", idMeterType)
                    , new DB.InParameter("@METER_M3_PER_IMPULSE", METER_M3_PER_IMPULSE)
                    , new DB.InParameter("@consumerName", consumerName)
                    , new DB.InParameter("@consumerSurname", consumerSurname)
                    , new DB.InParameter("@METER_SERIAL_NUMBER", METER_SERIAL_NUMBER) 
                    , new DB.InParameter("@idLanguage", idLanguage) 
                    });
        }

        public Object SgmCreateLocationFromXLSWithUpdate
        (
            long AreaLocationID, int DistributorID,
            Data.DB.DB.AnalyzeDataSet DataSetAnalyzer,
            string LOCATION_NAME, string LOCATION_CITY, string LOCATION_ADDRESS, string LOCATION_CID, int idLanguage,
            bool isExists,
            string LOCATION_ZIP = null, string LOCATION_COUNTRY = null,
            double? LOCATION_LATITUDE = null, double? LOCATION_LONGITUDE = null,
            int? LOCATION_INSTALLATION_TYPE = null,
            int? idMeterType = null, 
            double? METER_M3_PER_IMPULSE = null, 
            string METER_SERIAL_NUMBER = null, 
            string consumerName = null, string consumerSurname = null
         )
        {
            return DB.ExecuteProcedure(
                "sgm_CreateLocationFromXLSWithUpdateOption", DataSetAnalyzer,
                new DB.Parameter[] {
                      new DB.InParameter("@AreaLocationID", AreaLocationID)
                    , new DB.InParameter("@DistributorID", DistributorID)
                    , new DB.InParameter("@LOCATION_NAME", LOCATION_NAME)
                    , new DB.InParameter("@LOCATION_CITY", LOCATION_CITY)
                    , new DB.InParameter("@LOCATION_ADDRESS", LOCATION_ADDRESS)
                    , new DB.InParameter("@LOCATION_CID", LOCATION_CID)
                    , new DB.InParameter("@LOCATION_ZIP", LOCATION_ZIP)
                    , new DB.InParameter("@LOCATION_COUNTRY", LOCATION_COUNTRY)
                    , new DB.InParameter("@LOCATION_LATITUDE", LOCATION_LATITUDE)
                    , new DB.InParameter("@LOCATION_LONGITUDE", LOCATION_LONGITUDE)
                    , new DB.InParameter("@LOCATION_INSTALLATION_TYPE", LOCATION_INSTALLATION_TYPE)
                    , new DB.InParameter("@idMeterType", idMeterType)
                    , new DB.InParameter("@METER_M3_PER_IMPULSE", METER_M3_PER_IMPULSE)
                    , new DB.InParameter("@consumerName", consumerName)
                    , new DB.InParameter("@consumerSurname", consumerSurname)
                    , new DB.InParameter("@METER_SERIAL_NUMBER", METER_SERIAL_NUMBER) 
                    , new DB.InParameter("@idLanguage", idLanguage) 
                    , new DB.InParameter("@isExists", isExists)
                    });
        }

        #endregion

        #region Billing

        public DB_BILLING_CONSUMER[] GetBillingConsumer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_BILLING_CONSUMER[])DB.ExecuteProcedure(
                "billing_GetBillingConsumer",
                new DB.AnalyzeDataSet(GetBillingConsumer),
                AutoTransaction: autoTransaction,
                IsolationLevel: transactionLevel,
                CommandTimeout: commandTimeout
            );
        }

        public DB_BILLING_CONSUMER[] GetBillingConsumer(int[] ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_BILLING_CONSUMER[])DB.ExecuteProcedure(
                "billing_GetBillingConsumer",
                new DB.AnalyzeDataSet(GetBillingConsumer),
                new DB.Parameter[] 
                { 
			        CreateTableParam("@ID_CONSUMER_INPUT", ids)	
                },
                AutoTransaction: autoTransaction,
                IsolationLevel: transactionLevel,
                CommandTimeout: commandTimeout
            );
        }

        private object GetBillingConsumer(DataSet QueryResult)
        {
            Dictionary<int, DB_BILLING_CONSUMER> resDict = new Dictionary<int, DB_BILLING_CONSUMER>();
            DB_BILLING_CONSUMER[] resTable = QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_BILLING_CONSUMER()
            {
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_ACTOR = GetValue<int>(row["ID_ACTOR"]),
                ID_DEVICE_TYPE = GetValue<int>(row["ID_DEVICE_TYPE"]),
                ID_TARIFF = GetValue<int>(row["ID_TARIFF"]),
                ID_CONSUMER_TYPE = GetValue<int>(row["ID_CONSUMER_TYPE"]),
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                //NOTIFICATION_DATA = GetValue<string>(row["NOTIFICATION_DATA"]),
                SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                DEVICE_PHONE_NBR = GetValue<string>(row["DEVICE_PHONE_NBR"]),
            }).ToArray();

            foreach (DB_BILLING_CONSUMER item in resTable)
            {
                if (!resDict.ContainsKey(item.ID_CONSUMER))
                    resDict.Add(item.ID_CONSUMER, item);
            }

            List<DB_BILLING_CONSUMER> result = new List<DB_BILLING_CONSUMER>();

            foreach (KeyValuePair<int, DB_BILLING_CONSUMER> entry in resDict)
            {
                result.Add(entry.Value);
            }

            return result.ToArray();

            //return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_BILLING_CONSUMER()
            //{
            //    ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
            //    ID_ACTOR = GetValue<int>(row["ID_ACTOR"]),
            //    ID_DEVICE_TYPE = GetValue<int>(row["ID_DEVICE_TYPE"]),
            //    ID_TARIFF = GetValue<int>(row["ID_TARIFF"]),
            //    ID_CONSUMER_TYPE = GetValue<int>(row["ID_CONSUMER_TYPE"]),
            //    //NOTIFICATION_DATA = GetValue<string>(row["NOTIFICATION_DATA"]),
            //    SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
            //    ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
            //    DEVICE_PHONE_NBR = GetValue<string>(row["DEVICE_PHONE_NBR"]),
            //    //MODULE_BILLING_NBR_DATA = GetValue<string>(row["MODULE_BILLING_NBR_DATA"])
            //}).ToArray();
        }

        public bool SaveConsumerPeriodCreditStatusNotification(int consumerId, int timePeriodType, DateTime startTime, DateTime executeTime, int? customInterval)
        {
            DB.OutParameter resultParam = new DB.OutParameter("@RESULT", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                "billing_SaveConsumerPeriodCreditStatusNotification",
                new DB.Parameter[] {
                      new DB.InParameter("@ID_CONSUMER", consumerId)
                    , new DB.InParameter("@TIME_PERIOD_TYPE", timePeriodType)
                    , new DB.InParameter("@START_TIME", startTime)
                    , new DB.InParameter("@EXECUTE_TIME", executeTime)
                    , new DB.InParameter("@CUSTOM_INTERVAL", customInterval)
                    , resultParam
                });

            return (bool)resultParam.Value;
        }

        public bool DeleteConsumerPeriodCreditStatusNotification(int consumerId)
        {
            DB.OutParameter resultParam = new DB.OutParameter("@RESULT", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                "billing_DeleteConsumerPeriodCreditStatusNotification",
                new DB.Parameter[] {
                      new DB.InParameter("@ID_CONSUMER", consumerId)
                    , resultParam
                });

            return (bool)resultParam.Value;
        }

        #endregion

        #region Intergaz Latvia interface
        public int IntergazLatviaInterfaceManageLocation(int manageAction, int distributorID, string locationID, string locationName, string locationAddress, string locationCity, DataTable tanksToCreate, DataTable gasmeterToCreate)
        {
            List<DB.Parameter> paramsList = new List<Data.DB.DB.Parameter>();
            //Parametry lokalizacji
            paramsList.Add(new DB.InParameter("@MANAGE_ACTION", manageAction));
            paramsList.Add(new DB.InParameter("@ID_DISTRIBUTOR", distributorID));
            paramsList.Add(new DB.InParameter("@LocationID", locationID));
            paramsList.Add(new DB.InParameter("@LocationName", locationName));
            paramsList.Add(new DB.InParameter("@LocationAddress", locationAddress));
            paramsList.Add(new DB.InParameter("@LocationCity", locationCity));
            if (tanksToCreate != null)
                paramsList.Add(new DB.InParameter("@LocationTanks", SqlDbType.Structured, tanksToCreate));
            if (gasmeterToCreate != null)
                paramsList.Add(new DB.InParameter("@LocationGasmeters", SqlDbType.Structured, gasmeterToCreate));

            int result = (int)DB.ExecuteScalar("dataservice_IntergazLatviaInterfaceManageLocation", paramsList.ToArray());

            return result;
        }

        public DataTable IntergazLatviaInterfaceGetLocationsEquipment(int distributorID)
        {
            return (DataTable)DB.ExecuteProcedure(
                "dataservice_IntergazLatviaInterfaceGetLocationsEquipment",
                new DB.AnalyzeDataSet(IntergazLatviaInterfaceGetLocationsEquipment),
                new DB.Parameter[] { new DB.InParameter("@ID_DISTRIBUTOR", distributorID), });
        }

        private object IntergazLatviaInterfaceGetLocationsEquipment(DataSet QueryResult)
        {
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("ID", typeof(long));
            resultTable.Columns.Add("Name");
            resultTable.Columns.Add("City");
            resultTable.Columns.Add("Address");
            resultTable.Columns.Add("CID");

            resultTable.Columns.Add("MeterTypeClass", typeof(int));
            resultTable.Columns.Add("MeterType");
            resultTable.Columns.Add("MeterSerialNbr");
            resultTable.Columns.Add("TankCapacity", typeof(int));

            resultTable.Columns.Add("DeviceTypeClass", typeof(int));
            resultTable.Columns.Add("DeviceSerialNbr", typeof(long));
            resultTable.Columns.Add("DeviceOrderNbr");

            int count = QueryResult.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                int col = 0;
                long locationID = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string name = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string city = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string address = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string cid = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                int meterTypeClass = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                string meterType = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                string meterSerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);
                int tankCapacity = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);

                int deviceTypeClass = GetValue<int>(QueryResult.Tables[0].Rows[i][col++]);
                long? deviceSerialNbr = GetValue<long>(QueryResult.Tables[0].Rows[i][col++]);
                string deviceOrderNbr = GetValue<string>(QueryResult.Tables[0].Rows[i][col++]);

                resultTable.Rows.Add(locationID, name, city, address, cid, meterTypeClass, meterType, meterSerialNbr, tankCapacity,
                    deviceTypeClass, deviceSerialNbr, deviceOrderNbr);
            }
            return resultTable;
        }

        #endregion

        #region GetShippingListDevice
        public DataTable GetShippingListDeviceRadioDeviceTesterData(int idShippingList)
        {
            return (DataTable)DB.ExecuteProcedure(
                "rpt_ShippingListDeviceRadioDeviceTesterData",
                new DB.AnalyzeDataSet(GetServicesForDevices),
                new DB.Parameter[] { 
                        new DB.InParameter("@IdShippingList", idShippingList)
					}, 3600
            );
        }
        #endregion

        #region FillDataTable for Bulk Insert/Update
        #region FillSimCard

        protected DataTable FillSimCard(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            DB_SIM_CARD[] dataList = data as DB_SIM_CARD[];
            int iterator = 0;

            foreach (DB_SIM_CARD dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_SIM_CARD"] = dtItem.ID_SIM_CARD;
                row["ID_DISTRIBUTOR"] = dtItem.ID_DISTRIBUTOR;
                row["SERIAL_NBR"] = dtItem.SERIAL_NBR;
                row["PHONE"] = dtItem.PHONE;
                row["PIN"] = dtItem.PIN;
                row["PUK"] = dtItem.PUK;

                if (dtItem.MOBILE_NETWORK_CODE.HasValue)
                    row["MOBILE_NETWORK_CODE"] = dtItem.MOBILE_NETWORK_CODE.Value;
                else
                    row["MOBILE_NETWORK_CODE"] = DBNull.Value;

                if (dtItem.IP != null)
                    row["IP"] = dtItem.IP;
                else
                    row["IP"] = DBNull.Value;

                if (dtItem.APN_LOGIN != null)
                    row["APN_LOGIN"] = dtItem.APN_LOGIN;
                else
                    row["APN_LOGIN"] = DBNull.Value;

                if (dtItem.APN_PASSWORD != null)
                    row["APN_PASSWORD"] = dtItem.APN_PASSWORD;
                else
                    row["APN_PASSWORD"] = DBNull.Value;

                dataTable.Rows.Add(row);

                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }

                iterator++;
            }

            return dataTable;
        }

        #endregion
        #region FillSimCardData

        protected DataTable FillSimCardData(object[] data, DataTable dataTable, Enums.DataChange changeType)
        {
            // Sortowanie zbioru wynika z tego, że baza danych przy insert sortowała wg. kluczy, co powodowało, że mogło
            // to być w innej kolejności niż zbiór. Następnie podczas przypisywania ID_SIM_CARD_DATA i rózniącej się kolejności
            // złe ID były przypisywane do złych obiektów.
            DB_SIM_CARD_DATA[] dataList = (data as DB_SIM_CARD_DATA[])
                .OrderBy(x => x.ID_SIM_CARD)
                .ThenBy(x => x.ID_DATA_TYPE)
                .ThenBy(x => x.INDEX_NBR)
                .ToArray();

            for (int i = 0; i < dataList.Length; ++i)
            {
                data[i] = dataList[i];
            }

            int iterator = 0;

            foreach (DB_SIM_CARD_DATA dtItem in dataList)
            {
                DataRow row = dataTable.NewRow();

                row["ID_SIM_CARD_DATA"] = dtItem.ID_SIM_CARD_DATA;
                row["ID_SIM_CARD"] = dtItem.ID_SIM_CARD;
                row["ID_DATA_TYPE"] = dtItem.ID_DATA_TYPE;
                row["INDEX_NBR"] = dtItem.INDEX_NBR;

                if (dtItem.VALUE != null)
                    row["VALUE"] = dtItem.VALUE;
                else
                    row["VALUE"] = DBNull.Value;

                dataTable.Rows.Add(row);

                if (changeType == Enums.DataChange.Update)
                {
                    dataTable.Rows[iterator].AcceptChanges();
                    dataTable.Rows[iterator].SetModified();
                }

                iterator++;
            }

            return dataTable;
        }

        #endregion
        #endregion

        #region Old SIMA import

        #region GetRouteData
        public void GetRouteData(int RouteID, out string Name, out string Description, out string RouteStatus, out DateTime CreationDate)
        {
            Name = Description = RouteStatus = "";
            CreationDate = DateTime.Now;

            DB.OutParameter pName = new DB.OutParameter("@NAME", SqlDbType.NVarChar, 100);
            DB.OutParameter pDescription = new DB.OutParameter("@DESCRIPTION", SqlDbType.NVarChar, 300);
            DB.OutParameter pRouteStatus = new DB.OutParameter("@ROUTESTATUS", SqlDbType.NVarChar, 100);
            DB.OutParameter pCreationDate = new DB.OutParameter("@CREATIONDATE", SqlDbType.DateTime);

            DB.ExecuteNonQueryProcedure(
            "sgm_oldsima_GetRouteData",
                new DB.Parameter[]
                    {                          
                         new DB.InParameter("@ROUTEID", SqlDbType.Int, RouteID),
                         pName, pDescription, pRouteStatus, pCreationDate
                    }
            );
            Name = (string)pName.Value;
            Description = (string)pDescription.Value;
            RouteStatus = (string)pRouteStatus.Value;
            CreationDate = (DateTime)pCreationDate.Value;
        }
        #endregion

        #region MeasureType
        public object GetMeasureType(DB.AnalyzeDataSet GetMeasureType_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetMeasureTypes",
                GetMeasureType_Analyzer,
                new DB.Parameter[] { });
        }
        #endregion

        #region GetFrameDefinitions
        public object GetFrameDefinitions(DB.AnalyzeDataSet GetFrameDefinitions_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetImrModuleTypes",
                GetFrameDefinitions_Analyzer,
                new DB.Parameter[] { });
        }
        #endregion

        #region GetImrModuleTypes
        public object GetImrModuleTypes(DB.AnalyzeDataSet GetImrModuleTypes_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetImrModuleTypes",
                GetImrModuleTypes_Analyzer,
                new DB.Parameter[] { });
        }
        #endregion

        #region GetMeterTypes
        public object GetMeterTypes(DB.AnalyzeDataSet GetMeterTypes_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetMeterTypes",
                GetMeterTypes_Analyzer,
                new DB.Parameter[]
                {                    
                }
                );
        }
        #endregion

        #region GetReadoutRoute
        public object GetReadoutRoute(uint RouteID, DB.AnalyzeDataSet GetReadoutRoute_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetReadoutRouteData",
                GetReadoutRoute_Analyzer,
                new DB.Parameter[] 
                {
                    new DB.InParameter("@RouteID", RouteID),
                }
                );
        }
        #endregion

        #region GetDataFile
        public bool GetDataFile(out byte[] DataFile)
        {
            DataFile = null;

            DB.OutParameter pResult = new DB.OutParameter("@Result", SqlDbType.Bit);
            DB.OutParameter pDataFile = new DB.OutParameter("@DataFile", SqlDbType.VarBinary, int.MaxValue);

            DB.ExecuteNonQueryProcedure(
            "sgm_oldsima_GetDataFile",
                new DB.Parameter[]
                    {                                                   
                         pResult, pDataFile
                    });

            int result = pResult.Value == DBNull.Value ? 0 : (int)pResult.Value;
            DataFile = pDataFile.Value == DBNull.Value ? null : (byte[])pDataFile.Value;

            return result == 0 ? true : false;
        }
        #endregion

        #region UpdateMeter
        /*public void UpdateMeter(uint MeterID, string MeterSerialNbr)
        {
            DB.ExecuteNonQueryProcedure(
                "sgm_oldsima_UpdateMeter",
                new DB.Parameter[]
                {  	
                    new DB.InParameter("@MeterID", MeterID),
                    new DB.InParameter("@SerialNbr", MeterSerialNbr)
                });
        }

        public void UpdateMeter(uint MeterID, string MeterSerialNbr, uint MeterTypeID)
        {
            DB.ExecuteNonQueryProcedure(
                "sgm_oldsima_UpdateMeter",
                new DB.Parameter[]
                {  	
                    new DB.InParameter("@MeterID", MeterID),
                    new DB.InParameter("@SerialNbr", MeterSerialNbr),
                    new DB.InParameter("@MeterTypeID", MeterTypeID),
                });
        }*/
        #endregion

        #region GetUnit
        public object GetUnit_sima(DB.AnalyzeDataSet GetUnit_sima_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetUnit",
                GetUnit_sima_Analyzer,
                new DB.Parameter[] { });
        }
        #endregion

        #region CheckIfRouteExists
        public bool CheckIfRouteIDExists(uint RouteID)
        {
            int res = (int)DB.ExecuteScalar(
                "sgm_oldsima_CheckIfRouteIDExists",
                new DB.Parameter[]
                {                    
                    new DB.InParameter("@RouteID", RouteID),                    
                });
            return res == 0 ? false : true;
        }
        #endregion

        #region GetUsersForPsion
        public object GetUsersForPsion(DB.AnalyzeDataSet GetUsersForPsion_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetUsersForPsion",
                    GetUsersForPsion_Analyzer,
                    new DB.Parameter[]
                    {
                    }
                );
        }
        #endregion

        #region GetRoutes
        public object GetRoutes(int routeTypeId, bool top100, DB.AnalyzeDataSet GetRoutes_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetRoutes",
                    GetRoutes_Analyzer,
                    new DB.Parameter[]
                    {
                        new DB.InParameter("@ID_ROUTE_TYPE", routeTypeId ),
                        new DB.InParameter("@TOP_100", SqlDbType.Int, top100)
                    }
                );
        }
        #endregion

        #region GetGlobalParamValues
        public object GetGlobalParamValues(DB.AnalyzeDataSet GetGlobalParamValues_Analyzer)
        {
            return DB.ExecuteProcedure(
                "sgm_oldsima_GetGlobalParams",
                GetGlobalParamValues_Analyzer,
                new DB.Parameter[] { });
        }
        #endregion

        #region GetLocationsForArchives
        public object GetLocationsForArchives(DB.AnalyzeDataSet GetLocationsForArchives_Analyzer)
        {
            return DB.ExecuteProcedure(
                   "sgm_oldsima_GetLocationsForArchiveReport",
                   GetLocationsForArchives_Analyzer,
                   new DB.Parameter[] { });
        }
        #endregion

        #region GetDeviceIDForLocation
        public uint GetDeviceIDForLocation(uint LocationID)
        {
            return (uint)(int)DB.ExecuteScalar(
                "sgm_oldsima_GetDeviceIDForLocation",
                new DB.Parameter[]
                {
                    new DB.InParameter("@LocationID", LocationID)
                });
        }
        #endregion

        #region ChangeRouteStatus
        public void ChangeRouteStatus(int IdRoute, int Status)
        {
            DB.ExecuteNonQueryProcedure(
            "sgm_oldsima_ChangeRouteStatus",
                new DB.Parameter[]
                    {
                          new DB.InParameter("@ID_ROUTE", IdRoute),
                          new DB.InParameter("@ID_ROUTE_STATUS", Status)
                    }
            );
        }
        #endregion

        #region InsertMeasure
        public void InsertMeasure(uint? PsionSessionID, uint? DeviceID, uint? LocationID,
            uint MeasureTypeID, DateTime TimeStamp, object Value, uint IdDataSource)
        {
            List<DB.Parameter> paramList = new List<DB.Parameter>();
            paramList.Add(new DB.OutParameter("@ID_MEASURE", SqlDbType.Int));
            if (PsionSessionID.HasValue)
                paramList.Add(new DB.InParameter("@ID_PSION_SESSION", SqlDbType.Int, PsionSessionID.Value));
            if (DeviceID.HasValue)
                paramList.Add(new DB.InParameter("@ID_DEVICE", SqlDbType.Int, DeviceID.Value));
            if (LocationID.HasValue)
                paramList.Add(new DB.InParameter("@ID_LOCATION", SqlDbType.Int, LocationID.Value));

            paramList.Add(new DB.InParameter("@ID_MEASURE_TYPE", SqlDbType.Int, MeasureTypeID));
            if (TimeStamp != DateTime.MinValue)
                paramList.Add(new DB.InParameter("@TIME", SqlDbType.DateTime, TimeStamp));

            if (MeasureTypeID == 82 || MeasureTypeID == 83 || MeasureTypeID == 87 || MeasureTypeID == 88)
            {
                paramList.Add(new DB.InParameter("@VALUE", SqlDbType.NVarChar, Value));
                DB.ExecuteNonQueryProcedure(
                "sgm_oldsima_InsertSerializedArchive",
                paramList.ToArray()
                );
            }
            else
            {
                if (MeasureTypeID == 1 || MeasureTypeID == 24)
                    paramList.Add(new DB.InParameter("@VALUE", SqlDbType.Float, Value));
                else
                    paramList.Add(new DB.InParameter("@VALUE", SqlDbType.Variant, Value));

                paramList.Add(new DB.InParameter("@ID_DATA_SOURCE", SqlDbType.BigInt, IdDataSource));
                DB.ExecuteNonQueryProcedure(
                "sgm_oldsima_InsertMeasure",
                 paramList.ToArray()
                );
            }
        }
        #endregion

        #region CreateDataSourceForPsion
        public uint CreateDataSourceForPsion()
        {
            DB.OutParameter procedureResult = new DB.OutParameter("@ID_DATA_SOURCE", SqlDbType.BigInt);
            uint result = 0;
            DB.ExecuteNonQueryProcedure(
                "sgm_oldsima_CreateDataSourceForPsion",
                new DB.OutParameter[]
                    {                                              
                         procedureResult
                    }
                );

            if (procedureResult.Value != DBNull.Value)
                result = Convert.ToUInt32(procedureResult.Value);

            return result;
        }
        #endregion

        #region GetMeasureByTypeForLocation
        public object[] GetMeasureByTypeForLocation(uint id_location, uint id_data_type, DB.AnalyzeDataSet GetMeasureByTypeForLocation_Analyzer)
        {
            return (object[])DB.ExecuteProcedure(
                    "sgm_oldsima_GetMeasureByTypeForLocation",
                        GetMeasureByTypeForLocation_Analyzer,
                        new DB.Parameter[] { 
                            new DB.InParameter("@ID_LOCATION", id_location),
                            new DB.InParameter("@ID_DATA_TYPE", id_data_type),
                        });
        }
        #endregion

        #region Get data for SIME Polska reports

        public DataTable GetSIMEPolskaDataCurrentStates(DateTime StartDate, DateTime EndDate, DB.AnalyzeDataSet GetSIMEPolskaDataCurrentStates_Analyzer)
        {
            return (DataTable)DB.ExecuteProcedure(
                    "sgm_oldsima_SIMEReport_CurrentCounterState",
                        GetSIMEPolskaDataCurrentStates_Analyzer,
                        new DB.Parameter[] { 
                            new DB.InParameter("@StartDate", StartDate),
                            new DB.InParameter("@EndDate", EndDate),
                        }, false, IsolationLevel.ReadUncommitted, 2147483647);
        }

        public DataTable GetSIMEPolskaDataLachedCounterStateAndMaxHourFlow(DateTime StartDate, DateTime EndDate, DB.AnalyzeDataSet GetSIMEPolskaDataLachedCounterStateAndMaxHourFlow_Analyzer)
        {
            return (DataTable)DB.ExecuteProcedure(
                    "sgm_oldsima_SIMEReport_LachedCounterStateAndMaxHourFlow",
                        GetSIMEPolskaDataLachedCounterStateAndMaxHourFlow_Analyzer,
                        new DB.Parameter[] { 
                            new DB.InParameter("@StartDate", StartDate),
                            new DB.InParameter("@EndDate", EndDate),
                        }, false, IsolationLevel.ReadUncommitted, 2147483647);
        }

        public DataTable GetSIMEPolskaCurrentAndLachedCounterStateAndMaxHourFlow(DateTime StartDate, DateTime EndDate, DB.AnalyzeDataSet GetSIMEPolskaCurrentAndLachedCounterStateAndMaxHourFlow_Analyzer)
        {
            return (DataTable)DB.ExecuteProcedure(
                    "sgm_oldsima_SIMEReport_GetCurrentMeasures",
                        GetSIMEPolskaCurrentAndLachedCounterStateAndMaxHourFlow_Analyzer,
                        new DB.Parameter[] { 
                            new DB.InParameter("@StartDate", StartDate),
                            new DB.InParameter("@EndDate", EndDate),
                        }, false, IsolationLevel.ReadUncommitted, 2147483647);
        }

        #endregion

        #endregion

        #region !!!Deleted
        #region Table DEVICE_STATE_HISTORY delete
        //public DB_DEVICE_STATE_HISTORY[] GetDeviceStateHistoryForDevice(long SerialNbr)
        //{
        //    return (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
        //        "imrse_u_GetDeviceStateHistory",
        //        new DB.AnalyzeDataSet(GetDeviceStateHistory),
        //        new DB.Parameter[] { new DB.InParameter("@SERIAL_NBR", SerialNbr) }
        //    );
        //}
        #endregion
        #region Table DEPOSITORY_ELEMENT delete
        //public DB_DEPOSITORY_ELEMENT[] GetDepositoryElementsForTask(int idTask, bool? ordered, bool? removed, bool? accepted)
        //{
        //    return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
        //        "imrse_u_GetDepositoryElementsForTask",
        //        new DB.AnalyzeDataSet(GetDepositoryElement),
        //        new DB.Parameter[] {
        //            new DB.InParameter("@ID_TASK", idTask),
        //            new DB.InParameter("@ORDERED", ordered),
        //            new DB.InParameter("@REMOVED", removed),
        //            new DB.InParameter("@ACCEPTED", accepted)
        //        }
        //    );
        //}

        //public DB_DEPOSITORY_ELEMENT[] GetDepositoryElementsForLocation(long idLocation, bool? removed)
        //{
        //    return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
        //        "imrse_u_GetDepositoryElementsForLocation",
        //        new DB.AnalyzeDataSet(GetDepositoryElement),
        //        new DB.Parameter[] {
        //            new DB.InParameter("@ID_LOCATION", idLocation),
        //            new DB.InParameter("@REMOVED", removed)
        //        }
        //    );
        //}

        //public DB_DEPOSITORY_ELEMENT[] GetDepositoryElementsForPackage(int idPackage)
        //{
        //    return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
        //        "imrse_u_GetDepositoryElementsForPackage",
        //        new DB.AnalyzeDataSet(GetDepositoryElement),
        //        new DB.Parameter[] {
        //            new DB.InParameter("@ID_PACKAGE", idPackage)
        //        }
        //    );
        //}
        #endregion
        #region Table DATA delete

        //public DB_DATA[] GetDataForLocations(long[] idLocation, long[] idDataType)
        //{
        //    return (DB_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForLocations",
        //        new DB.AnalyzeDataSet(GetData),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_LOCATION", idLocation),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //public DB_DATA[] GetDataForDevices(long[] snList, long[] idDataType)
        //{
        //    return (DB_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForDevices",
        //        new DB.AnalyzeDataSet(GetData),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@SERIAL_NBR", snList),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //public DB_DATA[] GetDataForMeters(long[] idMeter, long[] idDataType)
        //{
        //    return (DB_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForMeters",
        //        new DB.AnalyzeDataSet(GetData),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_METER", idMeter),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetData(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_DATA[] list = new DB_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_DATA insert = new DB_DATA();
        //        insert.ID_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA"]);
        //        insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
        //        insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
        //        insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        insert.STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        //public long SaveData(DB_DATA ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA", ToBeSaved.ID_DATA);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_SaveData",
        //        new DB.Parameter[] {
        //    InsertId
        //                                        ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
        //                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
        //                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
        //                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
        //                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
        //                                        ,ParamObject("@VALUE", ToBeSaved.VALUE)
        //                                        ,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
        //                        }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_DATA = (long)InsertId.Value;

        //    return (long)InsertId.Value;
        //}

        //public void DeleteData(DB_DATA toBeDeleted)
        //{
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_DelData",
        //        new DB.Parameter[] {
        //    new DB.InParameter("@ID_DATA", toBeDeleted.ID_DATA)			
        //        }
        //    );
        //}

        #endregion
        #region Table SYSTEM_DATA delete

        //public DB_SYSTEM_DATA[] GetSystemData(long[] idDataType)
        //{
        //    return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetSystemData",
        //        new DB.AnalyzeDataSet(GetSystemData),
        //        new DB.Parameter[] { 
        //            CreateTableParam("@ID_DATA_TYPE", idDataType) }
        //    );
        //}

        //private object GetSystemData(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_SYSTEM_DATA[] list = new DB_SYSTEM_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_SYSTEM_DATA insert = new DB_SYSTEM_DATA();
        //        insert.ID_SYSTEM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SYSTEM_DATA"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        //public long SaveSystemData(DB_SYSTEM_DATA ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SYSTEM_DATA", ToBeSaved.ID_SYSTEM_DATA);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_SaveSystemData",
        //        new DB.Parameter[] {
        //    InsertId
        //                                ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
        //                                                ,ParamObject("@VALUE", ToBeSaved.VALUE)
        //                            }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_SYSTEM_DATA = (long)InsertId.Value;

        //    return (long)InsertId.Value;
        //}

        //public void DeleteSystemData(DB_SYSTEM_DATA toBeDeleted)
        //{
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_DelSystemData",
        //        new DB.Parameter[] {
        //    new DB.InParameter("@ID_SYSTEM_DATA", toBeDeleted.ID_SYSTEM_DATA)			
        //}
        //    );
        //}

        #endregion
        #region Table ISSUE delete
        //public DB_ISSUE[] GetIssuesRecentlyAdded(int topCount)
        //{
        //    return (DB_ISSUE[])DB.ExecuteProcedure(
        //        "imrse_u_GetIssuesRecentlyAdded",
        //        new DB.AnalyzeDataSet(GetIssue),
        //        new DB.Parameter[] {
        //            new DB.InParameter("@TOP_COUNT", topCount)                    
        //        }
        //    );
        //}

        //public DB_ISSUE[] GetIssueHistoryForLocation(long Id)
        //{
        //    return (DB_ISSUE[])DB.ExecuteProcedure(
        //        "imrse_u_GetIssueHistoryForLocation",
        //        new DB.AnalyzeDataSet(GetIssue),
        //        new DB.Parameter[] { 
        //            new DB.InParameter("@ID_LOCATION", Id), 
        //            new DB.InNullParameter("@ID_DISTRIBUTOR")
        //        }
        //    );
        //}
        #endregion
        #region Table MODULE_DATA delete

        //public DB_MODULE_DATA[] GetModuleData(long[] idDataType, int idModule)
        //{
        //    return (DB_MODULE_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetModuleData",
        //        new DB.AnalyzeDataSet(GetModuleData),
        //        new DB.Parameter[] { 
        //            CreateTableParam("@ID_DATA_TYPE", idDataType),
        //            new DB.InParameter("@ID_MODULE", idModule)}
        //    );
        //}

        //private object GetModuleData(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_MODULE_DATA[] list = new DB_MODULE_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_MODULE_DATA insert = new DB_MODULE_DATA();
        //        insert.ID_MODULE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MODULE_DATA"]);
        //        insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        //public long SaveModuleData(DB_MODULE_DATA ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE_DATA", ToBeSaved.ID_MODULE_DATA);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_SaveModuleData",
        //        new DB.Parameter[] {
        //    InsertId
        //                                ,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
        //                                                ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
        //                                                ,ParamObject("@VALUE", ToBeSaved.VALUE)
        //                            }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_MODULE_DATA = (long)InsertId.Value;

        //    return (long)InsertId.Value;
        //}

        //public void DeleteModuleData(DB_MODULE_DATA toBeDeleted)
        //{
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_DelModuleData",
        //        new DB.Parameter[] {
        //    new DB.InParameter("@ID_MODULE_DATA", toBeDeleted.ID_MODULE_DATA)			
        //}
        //    );
        //}

        #endregion
        #region Table ROUTE delete
        //public DB_ROUTE[] GetWorkOrdersRecentlyAdded(int topCount)
        //{
        //    return (DB_ROUTE[])DB.ExecuteProcedure(
        //        "imrse_u_GetWorkOrdersRecentlyAdded",
        //        new DB.AnalyzeDataSet(GetRoute),
        //        new DB.Parameter[] { new DB.InParameter("@TOP_COUNT", topCount) }
        //    );
        //}
        #endregion
        #region Table TASK delete
        //public DB_TASK[] GetTasksRecentlyAdded(int topCount)
        //{
        //    return (DB_TASK[])DB.ExecuteProcedure(
        //        "imrse_u_GetTasksRecentlyAdded",
        //        new DB.AnalyzeDataSet(GetTask),
        //        new DB.Parameter[] { new DB.InParameter("@TOP_COUNT", topCount) }
        //    );
        //}

        //public DB_TASK[] GetTaskHistoryForLocation(long Id)
        //{
        //    return (DB_TASK[])DB.ExecuteProcedure(
        //        "imrse_u_GetTaskHistoryForLocation",
        //        new DB.AnalyzeDataSet(GetTask),
        //        new DB.Parameter[] { new DB.InParameter("@ID_LOCATION", Id),
        //        new DB.InNullParameter("@ID_DISTRIBUTOR")}
        //    );
        //}

        #endregion
        #region Table LOCATION delete
        //public DB_LOCATION[] GetLocationsRecentlyAdded(int topCount)
        //{
        //    return (DB_LOCATION[])DB.ExecuteProcedure(
        //        "imrse_u_GetLocationsRecentlyAdded",
        //        new DB.AnalyzeDataSet(GetLocation),
        //        new DB.Parameter[] { new DB.InParameter("@TOP_COUNT", topCount) }
        //    );
        //}
        #endregion
        #region Table LOCATION_CONSUMER_HISTORY delete
        //public DB_LOCATION_CONSUMER_HISTORY[] GetLocationsStateHistory(long? idLocation, int? IdConsumer)
        //{
        //    return (DB_LOCATION_CONSUMER_HISTORY[])DB.ExecuteProcedure(
        //        "imrse_u_GetLocationConsumerHistory",
        //        new DB.AnalyzeDataSet(GetLocationConsumerHistory),
        //        new DB.Parameter[] { 
        //            new DB.InParameter("@ID_LOCATION", idLocation),
        //            new DB.InParameter("@ID_CONSUMER", IdConsumer)
        //        }
        //    );
        //}
        #endregion
        #region Table LOCATION_STATE_HISTORY delete

        //public DB_LOCATION_STATE_HISTORY[] GetLocationStateHistoryForLocation(long IdLocation)
        //{
        //    return (DB_LOCATION_STATE_HISTORY[])DB.ExecuteProcedure(
        //        "imrse_u_GetLocationStateHistory",
        //        new DB.AnalyzeDataSet(GetLocationStateHistory),
        //        new DB.Parameter[] { new DB.InParameter("@ID_LOCATION", IdLocation) }
        //    );
        //}

        #endregion
        #region Table ISSUE_DATA delete

        //public DB_ISSUE_DATA[] GetDataForIssues(int[] idIssue, long[] idDataType)
        //{
        //    return (DB_ISSUE_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForIssues",
        //        new DB.AnalyzeDataSet(GetDataForIssues),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_ISSUE", idIssue),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForIssues(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_ISSUE_DATA[] list = new DB_ISSUE_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_ISSUE_DATA insert = new DB_ISSUE_DATA();
        //        insert.ID_ISSUE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_DATA"]);
        //        insert.ID_ISSUE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table TASK_DATA delete

        //public DB_TASK_DATA[] GetDataForTasks(int[] idTask, long[] idDataType)
        //{
        //    return (DB_TASK_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForTasks",
        //        new DB.AnalyzeDataSet(GetDataForTasks),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_TASK", idTask),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForTasks(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_TASK_DATA[] list = new DB_TASK_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_TASK_DATA insert = new DB_TASK_DATA();
        //        insert.ID_TASK_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TASK_DATA"]);
        //        insert.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table ROUTE_DATA delete

        //public DB_ROUTE_DATA[] GetDataForRoutes(long[] idRoute, long[] idDataType)
        //{
        //    return (DB_ROUTE_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForRoutes",
        //        new DB.AnalyzeDataSet(GetDataForRoutes),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_ROUTE", idRoute),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForRoutes(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_ROUTE_DATA[] list = new DB_ROUTE_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_ROUTE_DATA insert = new DB_ROUTE_DATA();
        //        insert.ID_ROUTE_DATA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DATA"]);
        //        insert.ID_ROUTE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table ROUTE_POINT_DATA delete

        //public DB_ROUTE_POINT_DATA[] GetDataForRoutePoints(int[] idRoutePoint, long[] idDataType)
        //{
        //    return (DB_ROUTE_POINT_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForRoutePoints",
        //        new DB.AnalyzeDataSet(GetDataForRoutePoints),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_ROUTE_POINT", idRoutePoint),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForRoutePoints(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_ROUTE_POINT_DATA[] list = new DB_ROUTE_POINT_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_ROUTE_POINT_DATA insert = new DB_ROUTE_POINT_DATA();
        //        insert.ID_ROUTE_POINT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_POINT_DATA"]);
        //        insert.ID_ROUTE_POINT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_POINT"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table ROLE delete

        //public DB_ROLE[] GetRole()
        //{
        //    return (DB_ROLE[])DB.ExecuteProcedure(
        //        "imrse_GetRole",
        //        new DB.AnalyzeDataSet(GetRole),
        //        new DB.Parameter[] { new DB.InNullParameter("@ID_ROLE") }
        //    );
        //}

        //public DB_ROLE[] GetRole(int Id)
        //{
        //    return (DB_ROLE[])DB.ExecuteProcedure(
        //        "imrse_GetRole",
        //        new DB.AnalyzeDataSet(GetRole),
        //        new DB.Parameter[] { new DB.InParameter("@ID_ROLE", Id) }
        //    );
        //}

        //private object GetRole(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_ROLE[] list = new DB_ROLE[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_ROLE insert = new DB_ROLE();
        //        insert.ID_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE"]);
        //        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
        //        insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        //public int SaveRole(DB_ROLE ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROLE", ToBeSaved.ID_ROLE);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_SaveRole",
        //        new DB.Parameter[] {
        //            InsertId
        //            ,new DB.InParameter("@NAME", ToBeSaved.NAME)
        //            ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
        //        }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_ROLE = (int)InsertId.Value;

        //    return (int)InsertId.Value;
        //}

        //public void DeleteRole(DB_ROLE toBeDeleted)
        //{
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_DelRole",
        //        new DB.Parameter[] {
        //    new DB.InParameter("@ID_ROLE", toBeDeleted.ID_ROLE)			
        //}
        //    );
        //}

        #endregion
        #region Table OPERATOR delete

        //public DB_OPERATOR[] GetOperator()
        //{
        //    return (DB_OPERATOR[])DB.ExecuteProcedure(
        //        "imrse_GetOperator",
        //        new DB.AnalyzeDataSet(GetOperator),
        //        new DB.Parameter[] { 
        //            new DB.InNullParameter("@ID_OPERATOR"),
        //            CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
        //        }
        //    );
        //}

        //public DB_OPERATOR[] GetOperator(int Id)
        //{
        //    return (DB_OPERATOR[])DB.ExecuteProcedure(
        //        "imrse_GetOperator",
        //        new DB.AnalyzeDataSet(GetOperator),
        //        new DB.Parameter[] { 
        //            new DB.InParameter("@ID_OPERATOR", Id),
        //            CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
        //        }
        //    );
        //}

        //private object GetOperator(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_OPERATOR[] list = new DB_OPERATOR[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_OPERATOR insert = new DB_OPERATOR();
        //        insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
        //        insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
        //        insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
        //        insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
        //        insert.LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["LOGIN"]);
        //        insert.PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["PASSWORD"]);
        //        insert.IS_BLOCKED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_BLOCKED"]);
        //        insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        //public int SaveOperator(DB_OPERATOR ToBeSaved)
        //{
        //    DB.InOutParameter InsertId = new DB.InOutParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR);
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_SaveOperator",
        //        new DB.Parameter[] {
        //    InsertId
        //                                ,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
        //                                                ,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
        //                                                ,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
        //                                                ,new DB.InParameter("@LOGIN", ToBeSaved.LOGIN)
        //                                                ,new DB.InParameter("@PASSWORD", ToBeSaved.PASSWORD)
        //                                                ,new DB.InParameter("@IS_BLOCKED", ToBeSaved.IS_BLOCKED)
        //                                                ,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
        //                            }
        //    );
        //    if (!InsertId.IsNull)
        //        ToBeSaved.ID_OPERATOR = (int)InsertId.Value;

        //    return (int)InsertId.Value;
        //}


        //public void DeleteOperator(DB_OPERATOR toBeDeleted)
        //{
        //    DB.ExecuteNonQueryProcedure(
        //        "imrse_DelOperator",
        //        new DB.Parameter[] {
        //    new DB.InParameter("@ID_OPERATOR", toBeDeleted.ID_OPERATOR)			
        //}
        //    );
        //}

        #endregion
        #region Table OPERATOR_DATA delete

        //public DB_OPERATOR_DATA[] GetDataForOperators(int[] idOperator, long[] idDataType)
        //{
        //    return (DB_OPERATOR_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForOperators",
        //        new DB.AnalyzeDataSet(GetDataForOperators),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_OPERATOR", idOperator),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForOperators(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_OPERATOR_DATA[] list = new DB_OPERATOR_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_OPERATOR_DATA insert = new DB_OPERATOR_DATA();
        //        insert.ID_OPERATOR_DATA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_DATA"]);
        //        insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table CONSUMER_DATA delete

        //public DB_CONSUMER_DATA[] GetDataForConsumers(int[] idOperator, long[] idDataType)
        //{
        //    return (DB_CONSUMER_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForConsumers",
        //        new DB.AnalyzeDataSet(GetConsumerData),
        //        new DB.Parameter[] {
        //            CreateTableParam("@ID_CONSUMER", idOperator),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        #endregion
        #region Table CONSUMER_TYPE_DATA delete

        //        public DB_CONSUMER_TYPE_DATA[] GetDataForConsumerTypes(int[] idOperator, long[] idDataType)
        //        {
        //            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
        //                "imrse_u_GetDataForConsumerTypes",
        //                new DB.AnalyzeDataSet(GetConsumerTypeData),
        //                new DB.Parameter[] {
        //                    CreateTableParam("@ID_CONSUMER_TYPE", idOperator),
        //                    CreateTableParam("@ID_DATA_TYPE", idDataType)
        //                }
        //            );
        //        }

        //        public DB_CONSUMER_TYPE_DATA[] GetConsumerTypeData()
        //        {
        //            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
        //                "imrse_GetConsumerTypeData",
        //                new DB.AnalyzeDataSet(GetConsumerTypeData),
        //                new DB.Parameter[] { 
        //            CreateTableParam("@ID_CONSUMER_TYPE_DATA", new long[] {})
        //                    }
        //            );
        //        }

        //        public DB_CONSUMER_TYPE_DATA[] GetConsumerTypeData(long[] Ids)
        //        {
        //            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
        //                "imrse_GetConsumerTypeData",
        //                new DB.AnalyzeDataSet(GetConsumerTypeData),
        //                new DB.Parameter[] { 
        //            CreateTableParam("@ID_CONSUMER_TYPE_DATA", Ids)
        //                    }
        //            );
        //        }

        //        private object GetConsumerTypeData(DataSet QueryResult)
        //        {
        //            int count = QueryResult.Tables[0].Rows.Count;
        //            DB_CONSUMER_TYPE_DATA[] list = new DB_CONSUMER_TYPE_DATA[count];
        //            for (int i = 0; i < count; i++)
        //            {
        //                DB_CONSUMER_TYPE_DATA insert = new DB_CONSUMER_TYPE_DATA();
        //                insert.ID_CONSUMER_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE_DATA"]);
        //                insert.ID_CONSUMER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE"]);
        //                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //                list[i] = insert;
        //            }
        //            return list;
        //        }

        //        public long SaveConsumerTypeData(DB_CONSUMER_TYPE_DATA ToBeSaved)
        //        {
        //            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TYPE_DATA", ToBeSaved.ID_CONSUMER_TYPE_DATA);
        //            DB.ExecuteNonQueryProcedure(
        //                "imrse_SaveConsumerTypeData",
        //                new DB.Parameter[] {
        //                    InsertId
        //                    ,new DB.InParameter("@ID_CONSUMER_TYPE", ToBeSaved.ID_CONSUMER_TYPE)
        //                    ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
        //                    ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
        //                    ,ParamObject("@VALUE", ToBeSaved.VALUE)
        //}
        //            );
        //            if (!InsertId.IsNull)
        //                ToBeSaved.ID_CONSUMER_TYPE_DATA = (long)InsertId.Value;

        //            return (long)InsertId.Value;
        //        }

        //        public void DeleteConsumerTypeData(DB_CONSUMER_TYPE_DATA toBeDeleted)
        //        {
        //            DB.ExecuteNonQueryProcedure(
        //                "imrse_DelConsumerTypeData",
        //                new DB.Parameter[] {
        //                    new DB.InParameter("@ID_CONSUMER_TYPE_DATA", toBeDeleted.ID_CONSUMER_TYPE_DATA)			
        //                }
        //            );
        //        }

        #endregion
        #region Table TARIFF_DATA delete


        //public DB_TARIFF_DATA[] GetDataForTariffs(int[] idOperator, long[] idDataType)
        //{
        //    return (DB_TARIFF_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForTariffs",
        //        new DB.AnalyzeDataSet(GetTariffData),
        //        new DB.Parameter[] {
        //            CreateTableParam("@ID_TARIFF", idOperator),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        #endregion
        #region Table PACKAGE delete

        //public DB_PACKAGE[] GetPackagesForOperator(long idOperator)
        //{
        //    return (DB_PACKAGE[])DB.ExecuteProcedure(
        //        "imrse_u_GetPackagesForOperator",
        //        new DB.AnalyzeDataSet(GetPackagesForOperator),
        //        new DB.Parameter[] 
        //        { 
        //            new DB.InParameter("@id_operator", idOperator) 
        //        }
        //    );
        //}

        //private object GetPackagesForOperator(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_PACKAGE[] list = new DB_PACKAGE[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_PACKAGE insert = new DB_PACKAGE();
        //        insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
        //        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
        //        insert.CREATION_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]);
        //        insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
        //        insert.SEND_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SEND_DATE"]);
        //        insert.ID_OPERATOR_CREATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATOR"]);
        //        insert.ID_OPERATOR_RECEIVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_RECEIVER"]);
        //        insert.RECEIVED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["RECEIVED"]);
        //        insert.RECEIVE_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["RECEIVE_DATE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table PACKAGE_DATA delete

        //public DB_PACKAGE_DATA[] GetDataForPackages(int[] idPackage, long[] idDataType)
        //{
        //    return (DB_PACKAGE_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForPackages",
        //        new DB.AnalyzeDataSet(GetDataForPackages),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_PACKAGE", idPackage),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForPackages(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_PACKAGE_DATA[] list = new DB_PACKAGE_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_PACKAGE_DATA insert = new DB_PACKAGE_DATA();
        //        insert.ID_PACKAGE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_DATA"]);
        //        insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table DISTRIBUTOR_DATA delete

        //public DB_DISTRIBUTOR_DATA[] GetDataForDistributors(int[] idDistributor, long[] idDataType)
        //{
        //    return (DB_DISTRIBUTOR_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForDistributors",
        //        new DB.AnalyzeDataSet(GetDataForDistributors),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_DISTRIBUTOR", idDistributor),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForDistributors(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_DISTRIBUTOR_DATA[] list = new DB_DISTRIBUTOR_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_DISTRIBUTOR_DATA insert = new DB_DISTRIBUTOR_DATA();
        //        //insert.ID_DISTRIBUTOR_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_DATA"]);
        //        insert.ID_DISTRIBUTOR_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_DATA"]);
        //        insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}

        #endregion
        #region Table DEPOSITORY_ELEMENT_DATA delete
        //public DB_DEPOSITORY_ELEMENT_DATA[] GetDataForDepositoryElement(int[] idDepositoryElement, long[] idDataType)
        //{
        //    return (DB_DEPOSITORY_ELEMENT_DATA[])DB.ExecuteProcedure(
        //        "imrse_u_GetDataForDepositoryElement",
        //        new DB.AnalyzeDataSet(GetDataForDepositoryElement),
        //        new DB.Parameter[] 
        //        { 
        //            CreateTableParam("@ID_DEPOSITORY_ELEMENT", idDepositoryElement),
        //            CreateTableParam("@ID_DATA_TYPE", idDataType)
        //        }
        //    );
        //}

        //private object GetDataForDepositoryElement(DataSet QueryResult)
        //{
        //    int count = QueryResult.Tables[0].Rows.Count;
        //    DB_DEPOSITORY_ELEMENT_DATA[] list = new DB_DEPOSITORY_ELEMENT_DATA[count];
        //    for (int i = 0; i < count; i++)
        //    {
        //        DB_DEPOSITORY_ELEMENT_DATA insert = new DB_DEPOSITORY_ELEMENT_DATA();
        //        insert.ID_DEPOSITORY_ELEMENT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_ELEMENT_DATA"]);
        //        insert.ID_DEPOSITORY_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_ELEMENT"]);
        //        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
        //        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
        //        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
        //        list[i] = insert;
        //    }
        //    return list;
        //}
        #endregion
        #endregion

        #region SaveIssueDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveIssueDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateIssueDetails",
                new DB.Parameter[] {
                      new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
                    , CreateTableParam("@PARTICULAR_UNITS", particularUnits)
                    , CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion

        #region SaveTaskDetails

        /// <summary>
        /// WebSIMAX
        /// </summary>
        /// <param name="changeFromLastMinutes"></param>
        /// <param name="particularUnits"></param>
        /// <param name="idDataType"></param>
        public void SaveTaskDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_UpdateTaskDetails",
                new DB.Parameter[] {
                      new DB.InParameter("@CHANGE_FROM_LAST_MINUTES", changeFromLastMinutes)
                    , CreateTableParam("@PARTICULAR_UNITS", particularUnits)
                    , CreateTableParam("@ID_DATA_TYPE", idDataType)
                });
        }

        #endregion
    }
}
