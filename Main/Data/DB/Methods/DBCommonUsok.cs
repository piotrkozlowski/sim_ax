﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    public class DBCommonUsok : DBCommon
    {
        #region Ctor
        public DBCommonUsok(string ServerName)
            : base(ServerName)
        {
        }

        public DBCommonUsok(string ServerName, int ShortTimeout, int LongTimeout)
            : base(ServerName, ShortTimeout, LongTimeout)
        {
        }

        public DBCommonUsok(string server, string db, string userEncrypted, string passwordEncrypted, int ShortTimeout, int LongTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, ShortTimeout, LongTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        #endregion

        #region GetDevices//
        /*public SLDevice[] GetDevices(int ID_DISTRIBUTOR)
        {
            return (SLDevice[])DB.ExecuteProcedure(
                "msc_u_GetDevices",
                new DB.AnalyzeDataSet(GetSLDevices),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DISTRIBUTOR", ID_DISTRIBUTOR) 
                }
            );
        }

        private object GetSLDevices(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLDevice[] list = new SLDevice[count];
            for (int i = 0; i < count; i++)
            {
                SLDevice insert = new SLDevice();
                insert.CODE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE_NAME"]);
                insert.CONTRACT_NB = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONTRACT_NB"]);
                insert.CREATION_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]);
                insert.id_Shipping_list = GetValue<int>(QueryResult.Tables[0].Rows[i]["id_Shipping_list"]);
                insert.SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.SHIPPING_LIST_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHIPPING_LIST_NBR"]);
                insert.WARANTY_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARANTY_DATE"]);
                list[i] = insert;
            }
            return list;
        }*/
        #endregion
        #region GetCustomers//
        /*public SLCustomer[] GetCustomers()
        {
            return (SLCustomer[])DB.ExecuteProcedure(
                "usok_GetCustomers3",
                new DB.AnalyzeDataSet(GetSLCustomers),
                new DB.Parameter[] {  
                }
            );
        }

        private object GetSLCustomers(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLCustomer[] list = new SLCustomer[count];
            for (int i = 0; i < count; i++)
            {
                SLCustomer insert = new SLCustomer();
                insert.IdCustomer = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CUSTOMER"]);
                insert.Name = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.Address = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.City = GetValue<string>(QueryResult.Tables[0].Rows[i]["CITY"]);
                insert.StandardDescr = GetValue<string>(QueryResult.Tables[0].Rows[i]["STANDARD_DESCRIPTION"]);
                insert.IdDocumentPackage = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DOCUMENT_PACKAGE"]);
                list[i] = insert;
            }
            return list;
        }*/
        #endregion
        #region GetCustomersFromKomBitUpdateEngine//
        /*public SLCustomer[] GetCustomersFromKomBitUpdateEngine()
        {
            return (SLCustomer[])DB.ExecuteProcedure(
                "usok_GetCustomersFromKomBitUpdateEngine",
                new DB.AnalyzeDataSet(GetSLCustomersFromKomBitUpdateEngine),
                new DB.Parameter[] {  
                }
            );
        }

        private object GetSLCustomersFromKomBitUpdateEngine(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLCustomer[] list = new SLCustomer[count];
            for (int i = 0; i < count; i++)
            {
                SLCustomer insert = new SLCustomer();
                insert.IdCustomer = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CUSTOMER"]);
                insert.Name = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.Address = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.City = GetValue<string>(QueryResult.Tables[0].Rows[i]["CITY"]);
                insert.StandardDescr = GetValue<string>(QueryResult.Tables[0].Rows[i]["STANDARD_DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }*/
        #endregion
        #region GetCustomer//
        /*public bool GetCustomer(int IdCustomer, out string name, out string address, out string city)
        {
            name = address = city = null;
            DB.OutParameter pName = new DB.OutParameter("@NAME", SqlDbType.NVarChar, 100);
            DB.OutParameter pAddress = new DB.OutParameter("@ADDRESS", SqlDbType.NVarChar, 300);
            DB.OutParameter pCity = new DB.OutParameter("@CITY", SqlDbType.NVarChar, 100);


            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_GetCustomer",
                    //new DB.AnalyzeDataSet(GetCustomerDS),
                            new DB.Parameter[] { 
                        new DB.InParameter("@ID_CUSTOMER", IdCustomer),
                        pName,
                        pAddress,
                        pCity
                    }
                        );
            }
            catch (Exception)
            {
                return false;
            }

            if (!pName.IsNull)
                name = (string)pName.Value;
            if (!pAddress.IsNull)
                address = (string)pAddress.Value;
            if (!pCity.IsNull)
                city = (string)pCity.Value;

            return true;
        }

        private object GetCustomerDS(DataSet QueryResult)
        {
            return null;
        }*/
        #endregion
        #region SaveCustomer//
        /*
        public int SaveCustomer(int? IdCustomer, string Name, string Address, string City, string Description = null, string IdDocumentPackage = null)
        {

            DB.InParameter InsertId = null;
            if (IdCustomer.HasValue)
                InsertId = new DB.InParameter("@CustomerId", IdCustomer);
            else
                InsertId = new DB.InParameter("@CustomerId", 0);
            DB.ExecuteNonQueryProcedure("usok_SaveCustomer",
                                new DB.Parameter[]{
                                    InsertId,
                                    new DB.InParameter("@Name", Name),
                                    new DB.InParameter("@Address", Address),
                                    new DB.InParameter("@City", City),
                                    new DB.InParameter("@Description", Description),
                                    new DB.InParameter("@idDocumentPackage", IdDocumentPackage)
                    
                         });
            return (int)InsertId.Value;
        }*/
        #endregion
        #region ManageCustomers//
        /*
        public int ManageCustomers(int ManageActionId, int? IdCustomer, string Name, string Address, string City, string Description = null, string IdDocumentPackage = null)
        {

            DB.InParameter InsertId = null; 
            if(IdCustomer.HasValue)
                InsertId = new DB.InParameter("@CustomerId", IdCustomer);
            else
                InsertId = new DB.InParameter("@CustomerId", 0);
            DB.ExecuteNonQueryProcedure("usok_ManageCustomers",
                                new DB.Parameter[]{
                                    new DB.InParameter("@ManageActionId", ManageActionId),
                                    InsertId,
                                    new DB.InParameter("@Name", Name),
                                    new DB.InParameter("@Address", Address),
                                    new DB.InParameter("@City", City),
                                    new DB.InParameter("@Description", Description),
                                    new DB.InParameter("@idDocumentPackage", IdDocumentPackage)
                    
                         });
            return (int)InsertId.Value;
        }*/
        #endregion
        #region CreateCustomer// uzywane w WebService
        /*
        public int CreateCustomer(string name, string adress, string city, string description = null, string idDocumentPackage = null)
        {

            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_Customer", 0);
            DB.ExecuteNonQueryProcedure("usok_CreateCustomer",
                                new DB.Parameter[]{
                                    InsertId,
                                    new DB.InParameter("@Name", name),
                                    new DB.InParameter("@Address", adress),
                                    new DB.InParameter("@City", city),
                                    new DB.InParameter("@Description", description),
                                    new DB.InParameter("@idDocumentPackage", idDocumentPackage)
                    
                         });
            return (int)InsertId.Value;
        }*/
        #endregion
        #region FindDevice//
        /*
        /// <summary>
        /// FindDevice check if device exists in table RADIO_DEVICE in database RadioDeviceTester
        /// </summary>
        /// <param name="serialNbr">Nvarchar(16)</param>
        /// <returns></returns>
        public bool FindDevice(string serialNbr)
        {
            DB.InParameter pSerialNbr = new DB.InParameter("@DEVICE_SERIAL_NBR", SqlDbType.NVarChar, serialNbr, 16);
            DB.OutParameter pStatus = new DB.OutParameter("@RESULT", SqlDbType.Bit);

            DB.ExecuteNonQueryProcedure(
                        "imrse_u_FindDevice",
                        new DB.Parameter[] { 
                            pSerialNbr,
                            pStatus
                }
            );
            if (pStatus.Value != null)
                return (bool)pStatus.Value;
            else
                return false;
        }*/
        #endregion
        #region CheckIfDeviceExists//
        /*public bool CheckIfDeviceExists(string serialNbr, out long radioAddress, out string name, out string firstQuarter,
            out string secondQuarter, out string thirdQuarter, out int idDeviceState, out int idCustomer, out int idDeviceType, out string deviceTypeName)
        {
            name = firstQuarter = secondQuarter = thirdQuarter = deviceTypeName = null;
            radioAddress = idDeviceState = idCustomer = idDeviceType = 0;
            DB.OutParameter pDeviceExists = new DB.OutParameter("@DeviceExists", SqlDbType.Bit);
            DB.OutParameter pRadioAddress = new DB.OutParameter("@RadioAddress", SqlDbType.BigInt);
            DB.OutParameter pName = new DB.OutParameter("@Name", SqlDbType.NVarChar, 100);
            DB.OutParameter pFirstQuarter = new DB.OutParameter("@FirstQuarter", SqlDbType.NVarChar, 10);
            DB.OutParameter pSecondQuarter = new DB.OutParameter("@SecondQuarter", SqlDbType.NVarChar, 10);
            DB.OutParameter pThirdQuarter = new DB.OutParameter("@ThirdQuarter", SqlDbType.NVarChar, 10);
            DB.OutParameter pDeviceStateID = new DB.OutParameter("@DeviceStateID", SqlDbType.Int);
            DB.OutParameter pCustomerID = new DB.OutParameter("@CustomerID", SqlDbType.Int);
            DB.OutParameter pDeviceTypeID = new DB.OutParameter("@DeviceTypeID", SqlDbType.Int);
            DB.OutParameter pDeviceTypeName = new DB.OutParameter("@DeviceTypeName", SqlDbType.NVarChar, 50);
            DB.OutParameter pWarrantyDate = new DB.OutParameter("@WarrantyDate", SqlDbType.NVarChar, 50);

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_MW_CheckIfDeviceExists",
                            new DB.Parameter[] { 
                        new DB.InParameter("@DeviceSerialNbr", SqlDbType.Char, serialNbr, 16),
                        pDeviceExists,
                        pRadioAddress,
                        pName,
                        pFirstQuarter,
                        pSecondQuarter,
                        pThirdQuarter,
                        pDeviceStateID,
                        pCustomerID,
                        pDeviceTypeID,
                        pDeviceTypeName
                    }
                        );
            }
            catch (Exception)
            {
                return false;
            }

            bool retValue = false;

            if (!pDeviceExists.IsNull)
                retValue = (bool)pDeviceExists.Value;

            if (retValue) //znalezione urzadzenie
            {
                radioAddress = (long)pRadioAddress.Value;
                if (!pName.IsNull)
                    name = (string)pName.Value;
                if (!pFirstQuarter.IsNull)
                    firstQuarter = (string)pFirstQuarter.Value;
                if (!pSecondQuarter.IsNull)
                    secondQuarter = (string)pSecondQuarter.Value;
                if (!pThirdQuarter.IsNull)
                    thirdQuarter = (string)pThirdQuarter.Value;
                idDeviceState = (int)pDeviceStateID.Value;
                if (!pCustomerID.IsNull)
                    idCustomer = (int)pCustomerID.Value;
                idDeviceType = (int)pDeviceTypeID.Value;
                if (!pDeviceTypeName.IsNull)
                    deviceTypeName = (string)pDeviceTypeName.Value;
            }

            return retValue;
        }*/
        #endregion
        #region CheckDeviceState//
        /*
        /// <summary>
        /// CheckDeviceState cheks if device has correct ID_DEVICE_STATE on Usok DB and
        /// if has TESTED_OK on RadioDeviceTester
        /// </summary>
        /// <param name="serialNbr">Device serial number</param>
        /// <returns>1- device has correct params, 2 - device doesn't exists on USOK, 3 - wrong state type on USOK, 
        /// 4 - device doesn't exists on RadioDeviceTester, 5 - device has TESTED_OK = 0
        /// </returns>
        public int CheckDeviceState(string serialNbr)
        {
            DB.OutParameter pStatus = new DB.OutParameter("@RESULT", SqlDbType.Int);
            DB.InParameter pSerialNbr = new DB.InParameter("@SERIAL_NBR", SqlDbType.NVarChar, serialNbr, 100);

            DB.ExecuteNonQueryProcedure(
                        "usok_rdt_CheckDeviceState",
                        new DB.Parameter[] { 
                            pStatus,
                            pSerialNbr
                }
                    );

            if (pStatus.Value != null)
                return (int)pStatus.Value;
            else
                return -1;
        }
        */
        #endregion
        #region CreateServiceList//+c do zmiany kiedy wszystko zostanie wdrozone
        /*public bool CreateServiceList(int idCustomer, string courierName, int priority, string shippingLetterNbr,
             DateTime expectedFinishDate, string serviceName, out int idServiceList)
        {
            idServiceList = 0;

            DB.InParameter pCustomerID = new DB.InParameter("@CustomerID", SqlDbType.Int, idCustomer);
            DB.InParameter pCourierName = new DB.InParameter("@CourierName", SqlDbType.NVarChar, courierName, 100);
            DB.InParameter pPriority = new DB.InParameter("@Priority", SqlDbType.Int, priority);
            DB.InParameter pShippingLetterNbr = new DB.InParameter("@ShippingLetterNbr", SqlDbType.NVarChar, shippingLetterNbr, 100);
            DB.InParameter pExpectedFinishDate = new DB.InParameter("@ExpectedFinishDate", SqlDbType.DateTime, expectedFinishDate);
            DB.InParameter pServiceName = new DB.InParameter("@ServiceName", SqlDbType.NVarChar, serviceName, 100);
            DB.OutParameter pServiceListID = new DB.OutParameter("@ServiceListID", SqlDbType.Int);

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_MW_CreateServiceList",
                            new DB.Parameter[] { 
                        pCustomerID,
                        pCourierName,
                        pPriority,
                        pShippingLetterNbr,
                        pExpectedFinishDate,
                        pServiceName,
                        pServiceListID,
                    }
                        );
            }
            catch (Exception)
            {
                return false;
            }

            idServiceList = (int)pServiceListID.Value;

            return true;
        }*/
        #endregion
        #region AddDeviceToServiceList//+c do zmiany kiedy wszystko zostanie wdrozone
        /*public bool AddDeviceToServiceList(int idServiceList, long radioAddress)
        {
            DB.InParameter pServiceListID = new DB.InParameter("@ServiceListID", SqlDbType.Int, idServiceList);
            DB.InParameter pRadioAddress = new DB.InParameter("@RadioAddress", SqlDbType.BigInt, radioAddress);

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_MW_AddDeviceToServiceList",
                            new DB.Parameter[] { 
                        pServiceListID,
                        pRadioAddress,
                    }
                        );
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }*/
        #endregion
        #region SetDeviceState//+c do zmiany kiedy wszystko zostanie wdrozone
        /*public bool SetDeviceState(long radioAddress, int idDeviceState)
        {
            DB.InParameter pRadioAddress = new DB.InParameter("@RadioAddress", SqlDbType.BigInt, radioAddress);
            DB.InParameter pDeviceStateID = new DB.InParameter("@DeviceStateID", SqlDbType.Int, idDeviceState);

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_MW_SetDeviceState",
                            new DB.Parameter[] { 
                        pRadioAddress,
                        pDeviceStateID,
                    }
                        );
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }*/
        #endregion
        #region CheckIfDeviceHasShippingList//+c do zmiany kiedy wszystko zostanie wdrozone
        /*
        public void CheckIfDeviceHasShippingList(long serialNbr, out bool hasShippingList)
        {
            DB.InParameter pSerialNbr = new DB.InParameter("@SerialNbr", SqlDbType.BigInt, serialNbr);
            DB.OutParameter pHasShippingList = new DB.OutParameter("@HasShippingList", SqlDbType.Bit);

            hasShippingList = false;

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_MW_CheckIfDeviceHasShippingList",
                            new DB.Parameter[] { pSerialNbr, pHasShippingList }
                        );

                if (!pHasShippingList.IsNull)
                    hasShippingList = (bool)pHasShippingList.Value;
            }
            catch (Exception)
            {
            }
        }
        public void CheckIfDeviceHasAnyShippingList(long serialNbr, out bool hasShippingList)
        {
            DB.InParameter pSerialNbr = new DB.InParameter("@SerialNbr", SqlDbType.BigInt, serialNbr);
            DB.OutParameter pHasShippingList = new DB.OutParameter("@HasShippingList", SqlDbType.Bit);

            hasShippingList = false;

            try
            {
                DB.ExecuteNonQueryProcedure(
                            "imrsc_CheckIfDeviceHasAnyShippingList",
                            new DB.Parameter[] { pSerialNbr, pHasShippingList }
                        );

                if (!pHasShippingList.IsNull)
                    hasShippingList = (bool)pHasShippingList.Value;
            }
            catch (Exception ex)
            {
            }
        }
        */
        #endregion
        #region GetRestDevicesFromSet//+c do zmiany kiedy wszystko zostanie wdrozone
        /*
        public SLDevicesInSet[] GetRestDevicesFromSet(string SerialNbr)
        {
            return (SLDevicesInSet[])DB.ExecuteProcedure(
                "usok_GetRestDevicesFromSet",
                new DB.AnalyzeDataSet(GetRestDevicesFromSet),
                new DB.Parameter[] { 
                    new DB.InParameter("@SERIAL_NBR", SerialNbr) 
                }
            );
        }

        private object GetRestDevicesFromSet(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLDevicesInSet[] list = new SLDevicesInSet[count];
            for (int i = 0; i < count; i++)
            {
                SLDevicesInSet insert = new SLDevicesInSet();
                insert.IdSet = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SET"]);
                insert.SerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                list[i] = insert;
            }
            return list;
        }
        */
        #endregion
        #region GetDeviceParameters//collector
        /*public SLDeviceParameters[] GetDeviceParameters()
        {
            try
            {
                string query = "SELECT IMR_SERIAL_NBR, WARRANTY_DATE, SHIPPING_DATE FROM WarrantyDateForIMRSerialNbr WHERE IMR_SERIAL_NBR is not null";
                return (SLDeviceParameters[])DB.ExecuteQuerySql(
                                    query,
                                    new DB.AnalyzeDataSet(GetDeviceParametersDS)
                );
            }
            catch
            {
                return null;
            }
        }

        private object GetDeviceParametersDS(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLDeviceParameters[] list = new SLDeviceParameters[count];
            for (int i = 0; i < count; i++)
            {
                SLDeviceParameters insert = new SLDeviceParameters();
                insert.IMR_SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["IMR_SERIAL_NBR"]);
                insert.WARRANTY_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]);
                insert.SHIPPING_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SHIPPING_DATE"]);
                list[i] = insert;
            }
            return list;
        }*/
        #endregion



        #region GetDevices+c do zmiany kiedy wszystko zostanie wdrozone
        public SLDeviceServiceHistory[] GetDeviceShippingAndServiceHistory(long serialNbr)
        {
            return (SLDeviceServiceHistory[])DB.ExecuteProcedure(
                "imrsc_MW_GetDeviceShippingAndServiceHistory",
                new DB.AnalyzeDataSet(GetDeviceShippingAndServiceHistoryDS),
                new DB.Parameter[] { 
                    new DB.InParameter("@SerialNbr", serialNbr) 
                }
            );
        }

        private object GetDeviceShippingAndServiceHistoryDS(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            SLDeviceServiceHistory[] list = new SLDeviceServiceHistory[count];
            for (int i = 0; i < count; i++)
            {
                SLDeviceServiceHistory insert = new SLDeviceServiceHistory();
                insert.ROW_TYPE = (SLDeviceServiceHistory.RowType)GetValue<int>(QueryResult.Tables[0].Rows[i]["ROW_TYPE"]);
                insert.EVENT_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["EVENT_DATE"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.LONG_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["LONG_DESCR"]);
                insert.WARRANTY_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion
        #region Tymczasowo do zarzadzadzania customer storage 

        public DataTable GetCustomerStorage(int idDistributor, string name, string address, string city)
        {
            DataTable ret = (DataTable)DB.ExecuteProcedure(
                "usok_GetCustomersStorage",
                new DB.AnalyzeDataSet(GetCustomerStorage),
                new DB.Parameter[] {  
                    new DB.InParameter("@customerId", idDistributor) 
                }
            );
            for (int i = 0; i < ret.Rows.Count; i++)
            {
                if (!String.Equals(ret.Rows[i]["ADDRESS"].ToString(), address) ||
                   !String.Equals(ret.Rows[i]["CITY"].ToString(), city) ||
                   !String.Equals(ret.Rows[i]["STORAGE_NAME"].ToString(), name))
                {
                    ret.Rows.RemoveAt(i);
                    if (i >= 0)
                        i--;
                }
            }
            return ret;
        }
        private object GetCustomerStorage(DataSet QueryResult)
        {
            if (QueryResult.Tables.Count > 0)
                return QueryResult.Tables[1];
            return null;
        }

        public void DeleteCustomerStorage(int idDistributor, string name, string address, string city)
        {
            DataTable toDelete = GetCustomerStorage(idDistributor, name, address, city);
            foreach (DataRow dr in toDelete.Rows)
            {
                int idStorage = Convert.ToInt32(dr["ID_CUSTOMER_STORAGE"]);
                DB.ExecuteNonQueryProcedure("usok_DeleteCustomerStorage",
                                new DB.Parameter[]{
                                    new DB.InParameter("@storageId", Convert.ToInt32(dr["ID_CUSTOMER_STORAGE"]))
                         });
            }
        }

        public bool UpdateCustomerStorage(int idDistributor, string name, string address, string city, string country, string nameUpdate, string addressUpdate, string cityUpdate, string countryUpdate)
        {
            int storageId = 0;
            DataTable ret = GetCustomerStorage(idDistributor, name, address, city);
            if (ret.Rows.Count > 0)
            {
                storageId = Convert.ToInt32(ret.Rows[0]["ID_CUSTOMER_STORAGE"]);
                DB.ExecuteNonQueryProcedure("usok_ManageCustomerStorage",
                                    new DB.Parameter[]{
                                    new DB.InParameter("@ManageActionId", 2),
                                    new DB.InParameter("@storageAddress", addressUpdate),
                                    new DB.InParameter("@storageCity", cityUpdate),
                                    new DB.InParameter("@storageCountry", countryUpdate),
                                    new DB.InParameter("@CustomerId", idDistributor),
                                    new DB.InParameter("@storageDescription", ""),
                                    new DB.InParameter("@storageName", nameUpdate),
                                    new DB.InParameter("@storageId", storageId)
                         });
                return true;
            }
            return false;
        }

        public void AddCustomerStorage(int idDistributor, string name, string address, string city, string country)
        {
            int storageId = 0;
            DB.ExecuteNonQueryProcedure("usok_ManageCustomerStorage",
                                new DB.Parameter[]{
                                    new DB.InParameter("@ManageActionId", 1),
                                    new DB.InParameter("@storageAddress", address),
                                    new DB.InParameter("@storageCity", city),
                                    new DB.InParameter("@storageCountry", country),
                                    new DB.InParameter("@CustomerId", idDistributor),
                                    new DB.InParameter("@storageDescription", ""),
                                    new DB.InParameter("@storageName", name),
                                    new DB.InParameter("@storageId", storageId)
                         });
        }

        #endregion

    }
}
