﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Concurrent;
using IMR.Suite.Data.DB.DAQ;
using System.Collections.Generic;

using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDAQ : DBCommon
    {
        #region Constructor
		public DBCommonDAQ(string serverName)
			: base(serverName)
		{
		}
		public DBCommonDAQ(string serverName, int shortTimeout, int longTimeout)
			: base(serverName, shortTimeout, longTimeout)
		{
		}
		public DBCommonDAQ(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
			: base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
		{
		}
		public DBCommonDAQ(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime)
			: base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime)
		{
		}
        public DBCommonDAQ(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, null)
        {
        }
        public DBCommonDAQ(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, TimeSpan connectTime, int connectRetryCount, TimeSpan reconnectTime, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, connectTime, connectRetryCount, reconnectTime, useTrustedConnection, operatorLogin)
        {
        }
		public DBCommonDAQ(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout)
			: base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
		{
		}
        public DBCommonDAQ(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5), operatorLogin)
        {
        }
        public DBCommonDAQ(string server, string description, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection, string operatorLogin)
            : base(server, db, userEncrypted, passwordEncrypted, shortTimeout, longTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5), useTrustedConnection, operatorLogin)
        {
        }

        //TS: specjalny jałowy konstruktor aby można przekazać informacje o trusted connection w projektach gdzie są wykorzystywane Data/DB oraz Common/DB jednocześnie. Do wywalenia jak będzie zrobiony merge tych projektów
        public DBCommonDAQ(string server, string db, string userEncrypted, string passwordEncrypted, int shortTimeout, int longTimeout, bool useTrustedConnection)
            : base(server, db, userEncrypted, passwordEncrypted, Enums.Language.English, null, shortTimeout, longTimeout, TimeSpan.Zero, 0, TimeSpan.Zero, useTrustedConnection, TimeZoneId: "UTC")
        {
        }
		#endregion

        #region Table AUDIT

        public DB_AUDIT[] GetAudit()
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_AUDIT", new long[] {})
					}
            );
        }

        public DB_AUDIT[] GetAudit(long[] Ids)
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_AUDIT", Ids)
					}
            );
        }



        private object GetAudit(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_AUDIT()
            {
                ID_AUDIT = GetValue<long>(row["ID_AUDIT"]),
                BATCH_ID = GetValue<long>(row["BATCH_ID"]),
                CHANGE_TYPE = GetValue<int>(row["CHANGE_TYPE"]),
                TABLE_NAME = GetValue<string>(row["TABLE_NAME"]),
                KEY1 = GetNullableValue<long>(row["KEY1"]),
                KEY2 = GetNullableValue<long>(row["KEY2"]),
                KEY3 = GetNullableValue<long>(row["KEY3"]),
                KEY4 = GetNullableValue<long>(row["KEY4"]),
                KEY5 = GetNullableValue<long>(row["KEY5"]),
                KEY6 = GetNullableValue<long>(row["KEY6"]),
                COLUMN_NAME = GetValue<string>(row["COLUMN_NAME"]),
                OLD_VALUE = GetValue(row["OLD_VALUE"]),
                NEW_VALUE = GetValue(row["NEW_VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                USER = GetValue<string>(row["USER"]),
                HOST = GetValue<string>(row["HOST"]),
                APPLICATION = GetValue<string>(row["APPLICATION"]),
                CONTEXTINFO = QueryResult.Tables[0].Columns.Contains("CONTEXTINFO") ? GetValue<string>(row["CONTEXTINFO"]) : null, 
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_AUDIT[] list = new DB_AUDIT[count];
    for (int i = 0; i < count; i++)
    {
        DB_AUDIT insert = new DB_AUDIT();
									insert.ID_AUDIT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_AUDIT"]);
												insert.BATCH_ID = GetValue<long>(QueryResult.Tables[0].Rows[i]["BATCH_ID"]);
												insert.CHANGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_TYPE"]);
												insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
												insert.KEY1 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY1"]);
												insert.KEY2 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY2"]);
												insert.KEY3 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY3"]);
												insert.KEY4 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY4"]);
												insert.KEY5 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY5"]);
												insert.KEY6 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY6"]);
												insert.COLUMN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLUMN_NAME"]);
												insert.OLD_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["OLD_VALUE"]);
												insert.NEW_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["NEW_VALUE"]);
												insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
												insert.USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["USER"]);
												insert.HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["HOST"]);
												insert.APPLICATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["APPLICATION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveAudit(DB_AUDIT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_AUDIT", ToBeSaved.ID_AUDIT);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@BATCH_ID", ToBeSaved.BATCH_ID));
            parameters.Add(new DB.InParameter("@CHANGE_TYPE", ToBeSaved.CHANGE_TYPE));
            parameters.Add(new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME));
            parameters.Add(new DB.InParameter("@KEY1", ToBeSaved.KEY1));
            parameters.Add(new DB.InParameter("@KEY2", ToBeSaved.KEY2));
            parameters.Add(new DB.InParameter("@KEY3", ToBeSaved.KEY3));
            parameters.Add(new DB.InParameter("@KEY4", ToBeSaved.KEY4));
            parameters.Add(new DB.InParameter("@KEY5", ToBeSaved.KEY5));
            parameters.Add(new DB.InParameter("@KEY6", ToBeSaved.KEY6));
            parameters.Add(new DB.InParameter("@COLUMN_NAME", ToBeSaved.COLUMN_NAME));
            parameters.Add(ParamObject("@OLD_VALUE", ToBeSaved.OLD_VALUE, 0, 0));
            parameters.Add(ParamObject("@NEW_VALUE", ToBeSaved.NEW_VALUE, 0, 0));
            parameters.Add(new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME)));
            parameters.Add(new DB.InParameter("@USER", ToBeSaved.USER));
            parameters.Add(new DB.InParameter("@HOST", ToBeSaved.HOST));
            parameters.Add(new DB.InParameter("@APPLICATION", ToBeSaved.APPLICATION));
            if (IsParameterDefined("imrse_SaveAudit", "@CONTEXTINFO", this))
                parameters.Add(new DB.InParameter("@CONTEXTINFO", ToBeSaved.CONTEXTINFO));
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAudit",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_AUDIT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAudit(DB_AUDIT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAudit",
                new DB.Parameter[] {
            new DB.InParameter("@ID_AUDIT", toBeDeleted.ID_AUDIT)			
		}
            );
        }

        #endregion

        #region Table CONNECTION

        public DB_CONNECTION[] GetConnection()
        {
            return (DB_CONNECTION[])DB.ExecuteProcedure(
                "imrse_GetConnection",
                new DB.AnalyzeDataSet(GetConnection),
                new DB.Parameter[] { 
			CreateTableParam("@ID", new long[] {})
					}
            );
        }

        public DB_CONNECTION[] GetConnection(long[] Ids)
        {
            return (DB_CONNECTION[])DB.ExecuteProcedure(
                "imrse_GetConnection",
                new DB.AnalyzeDataSet(GetConnection),
                new DB.Parameter[] { 
			CreateTableParam("@ID", Ids)
					}
            );
        }



        private object GetConnection(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONNECTION[] list = new DB_CONNECTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONNECTION insert = new DB_CONNECTION();
                insert.ID = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID"]);
                insert.ID_CONNECTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONNECTION"]);
                insert.ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER"]);
                insert.LINK_TYPE_OUT = GetValue<int>(QueryResult.Tables[0].Rows[i]["LINK_TYPE_OUT"]);
                insert.LINK_TYPE_IN = GetValue<int>(QueryResult.Tables[0].Rows[i]["LINK_TYPE_IN"]);
                insert.RETRY_ATTEMPT = GetValue<int>(QueryResult.Tables[0].Rows[i]["RETRY_ATTEMPT"]);
                insert.TIME_ATTEMPT = GetValue<int>(QueryResult.Tables[0].Rows[i]["TIME_ATTEMPT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConnection(DB_CONNECTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID", ToBeSaved.ID);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConnection",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONNECTION", ToBeSaved.ID_CONNECTION)
														,new DB.InParameter("@ORDER", ToBeSaved.ORDER)
														,new DB.InParameter("@LINK_TYPE_OUT", ToBeSaved.LINK_TYPE_OUT)
														,new DB.InParameter("@LINK_TYPE_IN", ToBeSaved.LINK_TYPE_IN)
														,new DB.InParameter("@RETRY_ATTEMPT", ToBeSaved.RETRY_ATTEMPT)
														,new DB.InParameter("@TIME_ATTEMPT", ToBeSaved.TIME_ATTEMPT)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConnection(DB_CONNECTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConnection",
                new DB.Parameter[] {
            new DB.InParameter("@ID", toBeDeleted.ID)			
		}
            );
        }

        #endregion

        #region Table DATA_ARCH

        public DAQ.DB_DATA_ARCH[] GetDataArch()
        {
            return (DAQ.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH", new long[] {})
					}
            );
        }

        public DAQ.DB_DATA_ARCH[] GetDataArch(long[] Ids)
        {
            return (DAQ.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH", Ids)
					}
            );
        }



        private object GetDataArch(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DAQ.DB_DATA_ARCH[] list = new DAQ.DB_DATA_ARCH[count];
            for (int i = 0; i < count; i++)
            {
                DAQ.DB_DATA_ARCH insert = new DAQ.DB_DATA_ARCH();
                insert.ID_DATA_ARCH = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.ID_PACKET = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                insert.IS_ACTION = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTION"]);
                insert.IS_ALARM = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ALARM"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataArch(DAQ.DB_DATA_ARCH ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_ARCH", ToBeSaved.ID_DATA_ARCH);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataArch",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@ID_PACKET", ToBeSaved.ID_PACKET)
														,new DB.InParameter("@IS_ACTION", ToBeSaved.IS_ACTION)
														,new DB.InParameter("@IS_ALARM", ToBeSaved.IS_ALARM)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_ARCH = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataArch(DAQ.DB_DATA_ARCH toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataArch",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_ARCH", toBeDeleted.ID_DATA_ARCH)			
		}
            );
        }

        #endregion

        #region Table DATA_ARCH_TRASH

        public DB_DATA_ARCH_TRASH[] GetDataArchTrash()
        {
            return (DB_DATA_ARCH_TRASH[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrash",
                new DB.AnalyzeDataSet(GetDataArchTrash),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH_TRASH", new long[] {})
					}
            );
        }

        public DB_DATA_ARCH_TRASH[] GetDataArchTrash(long[] Ids)
        {
            return (DB_DATA_ARCH_TRASH[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrash",
                new DB.AnalyzeDataSet(GetDataArchTrash),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH_TRASH", Ids)
					}
            );
        }



        private object GetDataArchTrash(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_ARCH_TRASH[] list = new DB_DATA_ARCH_TRASH[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_ARCH_TRASH insert = new DB_DATA_ARCH_TRASH();
                insert.ID_DATA_ARCH_TRASH = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH_TRASH"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.ID_PACKET = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                insert.IS_ACTION = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTION"]);
                insert.IS_ALARM = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ALARM"]);
                insert.ID_DATA_ARCH_TRASH_STATUS = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH_TRASH_STATUS"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataArchTrash(DB_DATA_ARCH_TRASH ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_ARCH_TRASH", ToBeSaved.ID_DATA_ARCH_TRASH);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataArchTrash",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@ID_PACKET", ToBeSaved.ID_PACKET)
														,new DB.InParameter("@IS_ACTION", ToBeSaved.IS_ACTION)
														,new DB.InParameter("@IS_ALARM", ToBeSaved.IS_ALARM)
														,new DB.InParameter("@ID_DATA_ARCH_TRASH_STATUS", ToBeSaved.ID_DATA_ARCH_TRASH_STATUS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_ARCH_TRASH = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataArchTrash(DB_DATA_ARCH_TRASH toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataArchTrash",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_ARCH_TRASH", toBeDeleted.ID_DATA_ARCH_TRASH)			
		}
            );
        }

        #endregion

        #region Table DATA_ARCH_TRASH_STATUS

        public DB_DATA_ARCH_TRASH_STATUS[] GetDataArchTrashStatus()
        {
            return (DB_DATA_ARCH_TRASH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrashStatus",
                new DB.AnalyzeDataSet(GetDataArchTrashStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH_TRASH_STATUS", new int[] {})
					}
            );
        }

        public DB_DATA_ARCH_TRASH_STATUS[] GetDataArchTrashStatus(int[] Ids)
        {
            return (DB_DATA_ARCH_TRASH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrashStatus",
                new DB.AnalyzeDataSet(GetDataArchTrashStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH_TRASH_STATUS", Ids)
					}
            );
        }



        private object GetDataArchTrashStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_ARCH_TRASH_STATUS[] list = new DB_DATA_ARCH_TRASH_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_ARCH_TRASH_STATUS insert = new DB_DATA_ARCH_TRASH_STATUS();
                insert.ID_DATA_ARCH_TRASH_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH_TRASH_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataArchTrashStatus(DB_DATA_ARCH_TRASH_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_ARCH_TRASH_STATUS", ToBeSaved.ID_DATA_ARCH_TRASH_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataArchTrashStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_ARCH_TRASH_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataArchTrashStatus(DB_DATA_ARCH_TRASH_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataArchTrashStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_ARCH_TRASH_STATUS", toBeDeleted.ID_DATA_ARCH_TRASH_STATUS)			
		}
            );
        }

        #endregion

        #region Table MODULE

        public DB_MODULE[] GetModule()
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE", new int[] {})
					}
            );
        }

        public DB_MODULE[] GetModule(int[] Ids)
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE", Ids)
					}
            );
        }

        private object GetModule(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE[] list = new DB_MODULE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE insert = new DB_MODULE();
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteModule(DB_MODULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE", toBeDeleted.ID_MODULE)			
		}
            );
        }

        #endregion

        #region Table MODULE_DATA

        public DB_MODULE_DATA[] GetModuleData()
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE_DATA", new long[] {})
					}
            );
        }

        public DB_MODULE_DATA[] GetModuleData(long[] Ids)
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE_DATA", Ids)
					}
            );
        }



        private object GetModuleData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE_DATA[] list = new DB_MODULE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE_DATA insert = new DB_MODULE_DATA();
                insert.ID_MODULE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MODULE_DATA"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveModuleData(DB_MODULE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE_DATA", ToBeSaved.ID_MODULE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveModuleData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteModuleData(DB_MODULE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModuleData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE_DATA", toBeDeleted.ID_MODULE_DATA)			
		}
            );
        }

        #endregion

        #region Table PACKET

        public DB_PACKET[] GetPacket()
        {
            return (DB_PACKET[])DB.ExecuteProcedure(
                "imrse_GetPacket",
                new DB.AnalyzeDataSet(GetPacket),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET", new long[] {})
					}
            );
        }

        public DB_PACKET[] GetPacket(long[] Ids)
        {
            return (DB_PACKET[])DB.ExecuteProcedure(
                "imrse_GetPacket",
                new DB.AnalyzeDataSet(GetPacket),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET", Ids)
					}
            );
        }

        private object GetPacket(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET[] list = new DB_PACKET[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET insert = new DB_PACKET();
                insert.ID_PACKET = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.ID_TRANSMISSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                insert.TRANSMITTED_PACKETS = GetValue<int>(QueryResult.Tables[0].Rows[i]["TRANSMITTED_PACKETS"]);
                insert.BYTES = GetValue<int>(QueryResult.Tables[0].Rows[i]["BYTES"]);
                insert.TRANSMITTED_BYTES = GetValue<int>(QueryResult.Tables[0].Rows[i]["TRANSMITTED_BYTES"]);
                insert.BODY = (byte[])GetValue(QueryResult.Tables[0].Rows[i]["BODY"]);
                list[i] = insert;
            }
            return list;
        }

        public long SavePacket(DB_PACKET ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKET", ToBeSaved.ID_PACKET);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePacket",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
														,new DB.InParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER)
														,new DB.InParameter("@IS_INCOMING", ToBeSaved.IS_INCOMING)
														,new DB.InParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE)
														,new DB.InParameter("@TRANSMITTED_PACKETS", ToBeSaved.TRANSMITTED_PACKETS)
														,new DB.InParameter("@BYTES", ToBeSaved.BYTES)
														,new DB.InParameter("@TRANSMITTED_BYTES", ToBeSaved.TRANSMITTED_BYTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKET = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeletePacket(DB_PACKET toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPacket",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKET", toBeDeleted.ID_PACKET)			
		}
            );
        }

        #endregion

        #region Table PACKET_HISTORY

        public DB_PACKET_HISTORY[] GetPacketHistory()
        {
            return (DB_PACKET_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPacketHistory",
                new DB.AnalyzeDataSet(GetPacketHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET_HISTORY", new long[] {})
					}
            );
        }

        public DB_PACKET_HISTORY[] GetPacketHistory(long[] Ids)
        {
            return (DB_PACKET_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPacketHistory",
                new DB.AnalyzeDataSet(GetPacketHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET_HISTORY", Ids)
					}
            );
        }



        private object GetPacketHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_HISTORY[] list = new DB_PACKET_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_HISTORY insert = new DB_PACKET_HISTORY();
                insert.ID_PACKET_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET_HISTORY"]);
                insert.ID_PACKET = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.ID_PACKET_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKET_STATUS"]);
                insert.SYSTEM_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SYSTEM_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SavePacketHistory(DB_PACKET_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKET_HISTORY", ToBeSaved.ID_PACKET_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePacketHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PACKET", ToBeSaved.ID_PACKET)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@ID_PACKET_STATUS", ToBeSaved.ID_PACKET_STATUS)
														,new DB.InParameter("@SYSTEM_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.SYSTEM_TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKET_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeletePacketHistory(DB_PACKET_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPacketHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKET_HISTORY", toBeDeleted.ID_PACKET_HISTORY)			
		}
            );
        }

        #endregion

        #region Table PACKET_STATUS

        public DB_PACKET_STATUS[] GetPacketStatus()
        {
            return (DB_PACKET_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPacketStatus",
                new DB.AnalyzeDataSet(GetPacketStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET_STATUS", new int[] {})
					}
            );
        }

        public DB_PACKET_STATUS[] GetPacketStatus(int[] Ids)
        {
            return (DB_PACKET_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPacketStatus",
                new DB.AnalyzeDataSet(GetPacketStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKET_STATUS", Ids)
					}
            );
        }



        private object GetPacketStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_STATUS[] list = new DB_PACKET_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_STATUS insert = new DB_PACKET_STATUS();
                insert.ID_PACKET_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKET_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SavePacketStatus(DB_PACKET_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKET_STATUS", ToBeSaved.ID_PACKET_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePacketStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKET_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePacketStatus(DB_PACKET_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPacketStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKET_STATUS", toBeDeleted.ID_PACKET_STATUS)			
		}
            );
        }

        #endregion

        #region Table ROUTE_TABLE

        public DB_ROUTE_TABLE[] GetRouteTable()
        {
            return (DB_ROUTE_TABLE[])DB.ExecuteProcedure(
                "imrse_GetRouteTable",
                new DB.AnalyzeDataSet(GetRouteTable),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE", new int[] {})
					}
            );
        }

        public DB_ROUTE_TABLE[] GetRouteTable(int[] Ids)
        {
            return (DB_ROUTE_TABLE[])DB.ExecuteProcedure(
                "imrse_GetRouteTable",
                new DB.AnalyzeDataSet(GetRouteTable),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE", Ids)
					}
            );
        }



        private object GetRouteTable(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_TABLE[] list = new DB_ROUTE_TABLE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_TABLE insert = new DB_ROUTE_TABLE();
                insert.ID_ROUTE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.PREFIX = GetValue<int>(QueryResult.Tables[0].Rows[i]["PREFIX"]);
                insert.HOP = GetValue<string>(QueryResult.Tables[0].Rows[i]["HOP"]);
                insert.COST_OUT = GetValue<int>(QueryResult.Tables[0].Rows[i]["COST_OUT"]);
                insert.COST_IN = GetValue<int>(QueryResult.Tables[0].Rows[i]["COST_IN"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteRouteTable(DB_ROUTE_TABLE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteTable",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE", toBeDeleted.ID_ROUTE)			
		}
            );
        }

        #endregion

        #region Table ROUTE_TABLE_BACKUP

        public DB_ROUTE_TABLE_BACKUP[] GetRouteTableBackup()
        {
            return (DB_ROUTE_TABLE_BACKUP[])DB.ExecuteProcedure(
                "imrse_GetRouteTableBackup",
                new DB.AnalyzeDataSet(GetRouteTableBackup),
                new DB.Parameter[] { 
			CreateTableParam("@ID", new int[] {})
					}
            );
        }

        public DB_ROUTE_TABLE_BACKUP[] GetRouteTableBackup(int[] Ids)
        {
            return (DB_ROUTE_TABLE_BACKUP[])DB.ExecuteProcedure(
                "imrse_GetRouteTableBackup",
                new DB.AnalyzeDataSet(GetRouteTableBackup),
                new DB.Parameter[] { 
			CreateTableParam("@ID", Ids)
					}
            );
        }



        private object GetRouteTableBackup(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ROUTE_TABLE_BACKUP()
            {
                ID = GetValue<int>(row["ID"]),
                ID_ROUTE = GetValue<int>(row["ID_ROUTE"]),
                ID_ROUTE_BACKUP = GetValue<int>(row["ID_ROUTE_BACKUP"]),
                ORDER = GetValue<int>(row["ORDER"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_ROUTE_TABLE_BACKUP[] list = new DB_ROUTE_TABLE_BACKUP[count];
    for (int i = 0; i < count; i++)
    {
        DB_ROUTE_TABLE_BACKUP insert = new DB_ROUTE_TABLE_BACKUP();
									insert.ID = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID"]);
												insert.ID_ROUTE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
												insert.ID_ROUTE_BACKUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_BACKUP"]);
												insert.ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveRouteTableBackup(DB_ROUTE_TABLE_BACKUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID", ToBeSaved.ID);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteTableBackup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE)
														,new DB.InParameter("@ID_ROUTE_BACKUP", ToBeSaved.ID_ROUTE_BACKUP)
														,new DB.InParameter("@ORDER", ToBeSaved.ORDER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteTableBackup(DB_ROUTE_TABLE_BACKUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteTableBackup",
                new DB.Parameter[] {
            new DB.InParameter("@ID", toBeDeleted.ID)			
		}
            );
        }

        #endregion

        #region Table SYSTEM_DATA

        public DB_SYSTEM_DATA[] GetSystemData()
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SYSTEM_DATA", new long[] {})
					}
            );
        }

        public DB_SYSTEM_DATA[] GetSystemData(long[] Ids)
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SYSTEM_DATA", Ids)
					}
            );
        }



        private object GetSystemData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SYSTEM_DATA[] list = new DB_SYSTEM_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SYSTEM_DATA insert = new DB_SYSTEM_DATA();
                insert.ID_SYSTEM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SYSTEM_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSystemData(DB_SYSTEM_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SYSTEM_DATA", ToBeSaved.ID_SYSTEM_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSystemData",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                    ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
					,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
			    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SYSTEM_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSystemData(DB_SYSTEM_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSystemData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SYSTEM_DATA", toBeDeleted.ID_SYSTEM_DATA)			
		}
            );
        }

        #endregion

        #region Table TRANSMISSION_DRIVER

        public DB_TRANSMISSION_DRIVER[] GetTransmissionDriver()
        {
            return (DB_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriver",
                new DB.AnalyzeDataSet(GetTransmissionDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER", new int[] {})
					}
            );
        }

        public DB_TRANSMISSION_DRIVER[] GetTransmissionDriver(int[] Ids)
        {
            return (DB_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriver",
                new DB.AnalyzeDataSet(GetTransmissionDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER", Ids)
					}
            );
        }

        private object GetTransmissionDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_DRIVER[] list = new DB_TRANSMISSION_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_DRIVER insert = new DB_TRANSMISSION_DRIVER();
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.ID_TRANSMISSION_DRIVER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.RUN_AT_HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["RUN_AT_HOST"]);
                insert.RESPONSE_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["RESPONSE_ADDRESS"]);
                insert.PLUGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN"]);
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                insert.USE_CACHE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["USE_CACHE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionDriver(DB_TRANSMISSION_DRIVER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriver",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TRANSMISSION_DRIVER_TYPE", ToBeSaved.ID_TRANSMISSION_DRIVER_TYPE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@RUN_AT_HOST", ToBeSaved.RUN_AT_HOST)
														,new DB.InParameter("@RESPONSE_ADDRESS", ToBeSaved.RESPONSE_ADDRESS)
														,new DB.InParameter("@PLUGIN", ToBeSaved.PLUGIN)
														,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
														,new DB.InParameter("@USE_CACHE", ToBeSaved.USE_CACHE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        
        public void DeleteTransmissionDriver(DB_TRANSMISSION_DRIVER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionDriver",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_DRIVER", toBeDeleted.ID_TRANSMISSION_DRIVER)			
		}
            );
        }

        #endregion

        #region Table TRANSMISSION_DRIVER_DATA

        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverData()
        {
            return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverData",
                new DB.AnalyzeDataSet(GetTransmissionDriverData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", new long[] {})
					}
            );
        }

        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverData(long[] Ids)
        {
            return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverData",
                new DB.AnalyzeDataSet(GetTransmissionDriverData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", Ids)
					}
            );
        }



        private object GetTransmissionDriverData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_DRIVER_DATA[] list = new DB_TRANSMISSION_DRIVER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_DRIVER_DATA insert = new DB_TRANSMISSION_DRIVER_DATA();
                insert.ID_TRANSMISSION_DRIVER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_DATA"]);
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTransmissionDriverData(DB_TRANSMISSION_DRIVER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER_DATA", ToBeSaved.ID_TRANSMISSION_DRIVER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriverData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTransmissionDriverData(DB_TRANSMISSION_DRIVER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionDriverData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_DRIVER_DATA", toBeDeleted.ID_TRANSMISSION_DRIVER_DATA)			
		}
            );
        }

        #endregion
	
        #region Table TRANSMISSION_DRIVER_TYPE

        public DB_TRANSMISSION_DRIVER_TYPE[] GetTransmissionDriverType()
        {
            return (DB_TRANSMISSION_DRIVER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverType",
                new DB.AnalyzeDataSet(GetTransmissionDriverType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER_TYPE", new int[] {})
					}
            );
        }

        public DB_TRANSMISSION_DRIVER_TYPE[] GetTransmissionDriverType(int[] Ids)
        {
            return (DB_TRANSMISSION_DRIVER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverType",
                new DB.AnalyzeDataSet(GetTransmissionDriverType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_DRIVER_TYPE", Ids)
					}
            );
        }



        private object GetTransmissionDriverType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_DRIVER_TYPE[] list = new DB_TRANSMISSION_DRIVER_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_DRIVER_TYPE insert = new DB_TRANSMISSION_DRIVER_TYPE();
                insert.ID_TRANSMISSION_DRIVER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionDriverType(DB_TRANSMISSION_DRIVER_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_DRIVER_TYPE", ToBeSaved.ID_TRANSMISSION_DRIVER_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionDriverType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_DRIVER_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTransmissionDriverType(DB_TRANSMISSION_DRIVER_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionDriverType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_DRIVER_TYPE", toBeDeleted.ID_TRANSMISSION_DRIVER_TYPE)			
		}
            );
        }

        #endregion

        #region Table TRANSMISSION_PROTOCOL

        public DB_TRANSMISSION_PROTOCOL[] GetTransmissionProtocol()
        {
            return (DB_TRANSMISSION_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetTransmissionProtocol",
                new DB.AnalyzeDataSet(GetTransmissionProtocol),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_PROTOCOL", new int[] {})
					}
            );
        }

        public DB_TRANSMISSION_PROTOCOL[] GetTransmissionProtocol(int[] Ids)
        {
            return (DB_TRANSMISSION_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetTransmissionProtocol",
                new DB.AnalyzeDataSet(GetTransmissionProtocol),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_PROTOCOL", Ids)
					}
            );
        }



        private object GetTransmissionProtocol(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_PROTOCOL[] list = new DB_TRANSMISSION_PROTOCOL[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_PROTOCOL insert = new DB_TRANSMISSION_PROTOCOL();
                insert.ID_TRANSMISSION_PROTOCOL = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_PROTOCOL"]);
                insert.ID_TRANSMISSION_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_DRIVER"]);
                insert.ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER"]);
                insert.ID_PROTOCOL_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_DRIVER"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionProtocol(DB_TRANSMISSION_PROTOCOL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_PROTOCOL", ToBeSaved.ID_TRANSMISSION_PROTOCOL);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionProtocol",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TRANSMISSION_DRIVER", ToBeSaved.ID_TRANSMISSION_DRIVER)
														,new DB.InParameter("@ORDER", ToBeSaved.ORDER)
														,new DB.InParameter("@ID_PROTOCOL_DRIVER", ToBeSaved.ID_PROTOCOL_DRIVER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_PROTOCOL = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTransmissionProtocol(DB_TRANSMISSION_PROTOCOL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionProtocol",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_TRANSMISSION_PROTOCOL", toBeDeleted.ID_TRANSMISSION_PROTOCOL)			
		        });
        }

        #endregion\

        #region Table TRIGGERS

        public DB_TRIGGER[] GetTriggers()
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRIGGER", new long[] {})
					}
            );
        }

        public DB_TRIGGER[] GetTriggers(long[] Ids)
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRIGGER", Ids)
					}
            );
        }

        private object GetTriggers(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRIGGER[] list = new DB_TRIGGER[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRIGGER insert = new DB_TRIGGER();
                insert.ID_TRIGGER = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRIGGER"]);
                insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
                insert.AUDIT = GetValue<string>(QueryResult.Tables[0].Rows[i]["AUDIT"]);
                insert.DICT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DICT_TABLE"]);
                insert.OBJECT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["OBJECT_TABLE"]);
                insert.DATA_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DATA_TABLE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTriggers(DB_TRIGGER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRIGGER", ToBeSaved.ID_TRIGGER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTrigger",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME)
                    ,new DB.InParameter("@AUDIT", ToBeSaved.AUDIT)
                    ,new DB.InParameter("@DICT_TABLE", ToBeSaved.DICT_TABLE)
                    ,new DB.InParameter("@OBJECT_TABLE", ToBeSaved.OBJECT_TABLE)
                    ,new DB.InParameter("@DATA_TABLE", ToBeSaved.DATA_TABLE)
					
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRIGGER = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTriggers(DB_TRIGGER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTrigger",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRIGGER", toBeDeleted.ID_TRIGGER)			
		}
            );
        }

        #endregion

        #region  Table PACKET_DATA_ARCH
        private object GetPacketDataArch(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKET_DATA_ARCH[] list = new DB_PACKET_DATA_ARCH[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKET_DATA_ARCH insert = new DB_PACKET_DATA_ARCH();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.ID_PACKET = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.UNIT = GetValue<string>(QueryResult.Tables[0].Rows[i]["UNIT"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.STATUS = GetValue<string>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
        }
        public DB_PACKET_DATA_ARCH[] GetPacketDataArch(long serialNbr, long idPacket)
        {
            DB_PACKET_DATA_ARCH[] systemData = (DB_PACKET_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_u_GetPacketData",
                new DB.AnalyzeDataSet(GetPacketDataArch),
                new DB.Parameter[]
                    { 
                        new DB.InParameter("@SERIAL_NBR", serialNbr),new DB.InParameter("@ID_PACKET", idPacket)
                    });

            if (systemData.Length == 0)
                return null;
            return systemData;
        }
        #endregion
       

    }
}
