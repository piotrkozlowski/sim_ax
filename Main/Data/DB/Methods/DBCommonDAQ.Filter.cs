﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using IMR.Suite.Data.DB.DAQ;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonDAQ : DBCommon
    {
        #region GetActionPacketFilter

        public DB_ACTION_PACKET[] GetActionPacketFilter(long[] IdAction, long[] IdPacket, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionPacketFilter"));

            return (DB_ACTION_PACKET[])DB.ExecuteProcedure(
                "imrse_GetActionPacketFilter",
                new DB.AnalyzeDataSet(GetActionPacket),
                new DB.Parameter[] { 
					CreateTableParam("@ID_ACTION", IdAction),
					CreateTableParam("@ID_PACKET", IdPacket),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion

        #region GetAuditFilter

public DB_AUDIT[] GetAuditFilter(long[] IdAudit ,long[] BatchId ,int[] ChangeType ,string TableName,long[] Key1 ,long[] Key2 ,
					long[] Key3 ,long[] Key4 ,long[] Key5 ,long[] Key6 ,string ColumnName,
                    TypeDateTimeCode Time, string User, string Host, string Application, string Contextinfo, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
)
{
    if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));

    List<DB.Parameter> parameters = new List<DB.Parameter>();

	if (UseBulkInsertSqlTableType && (
		(IdAudit != null && IdAudit.Length > MaxItemsPerSqlTableType) ||
		(BatchId != null && BatchId.Length > MaxItemsPerSqlTableType) ||
		(ChangeType != null && ChangeType.Length > MaxItemsPerSqlTableType) ||
		(Key1 != null && Key1.Length > MaxItemsPerSqlTableType) ||
		(Key2 != null && Key2.Length > MaxItemsPerSqlTableType) ||
		(Key3 != null && Key3.Length > MaxItemsPerSqlTableType) ||
		(Key4 != null && Key4.Length > MaxItemsPerSqlTableType) ||
		(Key5 != null && Key5.Length > MaxItemsPerSqlTableType) ||
		(Key6 != null && Key6.Length > MaxItemsPerSqlTableType)
				))
	{
        long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

		List<Tuple<int, object>> args = new List<Tuple<int, object>>();
		if (IdAudit != null) args.AddRange(IdAudit.Select(s => new Tuple<int, object>(1, s)));
		if (BatchId != null) args.AddRange(BatchId.Select(s => new Tuple<int, object>(2, s)));
		if (ChangeType != null) args.AddRange(ChangeType.Select(s => new Tuple<int, object>(3, s)));
		if (Key1 != null) args.AddRange(Key1.Select(s => new Tuple<int, object>(5, s)));
		if (Key2 != null) args.AddRange(Key2.Select(s => new Tuple<int, object>(6, s)));
		if (Key3 != null) args.AddRange(Key3.Select(s => new Tuple<int, object>(7, s)));
		if (Key4 != null) args.AddRange(Key4.Select(s => new Tuple<int, object>(8, s)));
		if (Key5 != null) args.AddRange(Key5.Select(s => new Tuple<int, object>(9, s)));
		if (Key6 != null) args.AddRange(Key6.Select(s => new Tuple<int, object>(10, s)));
		
		DB_AUDIT[] result = null;
		try
		{
			InsertImrseProcArg(id, args);
            parameters.Add(new DB.InParameter("@ID", SqlDbType.BigInt, id));
            parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
            parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
            parameters.Add(CreateTableParam("@TIME_CODE", Time));
            parameters.Add(new DB.InParameter("@USER", User));
            parameters.Add(new DB.InParameter("@HOST", Host));
            parameters.Add(new DB.InParameter("@APPLICATION", Application));
            if (IsParameterDefined("imrse_GetAuditFilterTableArgs", "@CONTEXTINFO", this))
                parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
            parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
            parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
			result = (DB_AUDIT[])DB.ExecuteProcedure(
				"imrse_GetAuditFilterTableArgs",
				new DB.AnalyzeDataSet(GetAudit),
				parameters.ToArray(),
				autoTransaction, transactionLevel, commandTimeout
			);
		}
		catch (Exception ex)
		{
            DeleteImrseProcArg(id);
			throw ex;
		}
        // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
        //finally
        //{
        //    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
        //    DeleteImrseProcArg(id);
        //}
		return result;
	}
	else
	{
        parameters.Add(CreateTableParam("@ID_AUDIT", IdAudit));
        parameters.Add(CreateTableParam("@BATCH_ID", BatchId));
        parameters.Add(CreateTableParam("@CHANGE_TYPE", ChangeType));
        parameters.Add(new DB.InParameter("@TABLE_NAME", TableName));
        parameters.Add(CreateTableParam("@KEY1", Key1));
        parameters.Add(CreateTableParam("@KEY2", Key2));
        parameters.Add(CreateTableParam("@KEY3", Key3));
        parameters.Add(CreateTableParam("@KEY4", Key4));
        parameters.Add(CreateTableParam("@KEY5", Key5));
        parameters.Add(CreateTableParam("@KEY6", Key6));
        parameters.Add(new DB.InParameter("@COLUMN_NAME", ColumnName));
        parameters.Add(CreateTableParam("@TIME_CODE", Time));
        parameters.Add(new DB.InParameter("@USER", User));
        parameters.Add(new DB.InParameter("@HOST", Host));
        parameters.Add(new DB.InParameter("@APPLICATION", Application));
        if (IsParameterDefined("imrse_GetAuditFilter", "@CONTEXTINFO", this))
            parameters.Add(new DB.InParameter("@CONTEXTINFO", Contextinfo));
        parameters.Add(new DB.InParameter("@TOP_COUNT", topCount));
        parameters.Add(new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000));
		return (DB_AUDIT[])DB.ExecuteProcedure(
		    "imrse_GetAuditFilter",
			new DB.AnalyzeDataSet(GetAudit),
			parameters.ToArray(),
			autoTransaction, transactionLevel, commandTimeout
		);
	}
}
#endregion

		#region GetConnectionFilter
        public DB_CONNECTION[] GetConnectionFilter(long[] Id ,int[] IdConnection ,int[] Order ,int[] LinkTypeOut ,int[] LinkTypeIn ,int[] RetryAttempt ,
					        int[] TimeAttempt ,string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                        throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConnectionFilter"));
	
	        return (DB_CONNECTION[])DB.ExecuteProcedure(
                "imrse_GetConnectionFilter",
		        new DB.AnalyzeDataSet(GetConnection),
		        new DB.Parameter[] { 
					        CreateTableParam("@ID", Id),
					        CreateTableParam("@ID_CONNECTION", IdConnection),
					        CreateTableParam("@ORDER", Order),
					        CreateTableParam("@LINK_TYPE_OUT", LinkTypeOut),
					        CreateTableParam("@LINK_TYPE_IN", LinkTypeIn),
					        CreateTableParam("@RETRY_ATTEMPT", RetryAttempt),
					        CreateTableParam("@TIME_ATTEMPT", TimeAttempt),
					        new DB.InParameter("@NAME", Name),
					        new DB.InParameter("@TOP_COUNT", topCount),
					        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
		        autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion

        #region GetDataArchFilter

        public DAQ.DB_DATA_ARCH[] GetDataArchFilter(long[] IdDataArch, long[] SerialNbr, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode Time, long[] IdPacket,
                            bool? IsAction, bool? IsAlarm, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));

            return (DAQ.DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArchFilter",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_ARCH", IdDataArch),
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					CreateTableParam("@INDEX_NBR", IndexNbr),
					CreateTableParam("@TIME_CODE", Time),
					CreateTableParam("@ID_PACKET", IdPacket),
					new DB.InParameter("@IS_ACTION", IsAction),
					new DB.InParameter("@IS_ALARM", IsAlarm),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataArchTrashFilter

        public DB_DATA_ARCH_TRASH[] GetDataArchTrashFilter(long[] IdDataArchTrash, long[] SerialNbr, long[] IdDataType, int[] IndexNbr, TypeDateTimeCode Time, long[] IdPacket,
                            bool? IsAction, bool? IsAlarm, int[] IdDataArchTrashStatus, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchTrashFilter"));

            return (DB_DATA_ARCH_TRASH[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrashFilter",
                new DB.AnalyzeDataSet(GetDataArchTrash),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_ARCH_TRASH", IdDataArchTrash),
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					CreateTableParam("@INDEX_NBR", IndexNbr),
					CreateTableParam("@TIME_CODE", Time),
					CreateTableParam("@ID_PACKET", IdPacket),
					new DB.InParameter("@IS_ACTION", IsAction),
					new DB.InParameter("@IS_ALARM", IsAlarm),
					CreateTableParam("@ID_DATA_ARCH_TRASH_STATUS", IdDataArchTrashStatus),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetDataArchTrashStatusFilter

        public DB_DATA_ARCH_TRASH_STATUS[] GetDataArchTrashStatusFilter(int[] IdDataArchTrashStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchTrashStatusFilter"));

            return (DB_DATA_ARCH_TRASH_STATUS[])DB.ExecuteProcedure(
                "imrse_GetDataArchTrashStatusFilter",
                new DB.AnalyzeDataSet(GetDataArchTrashStatus),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DATA_ARCH_TRASH_STATUS", IdDataArchTrashStatus),
					new DB.InParameter("@NAME", Name),
					CreateTableParam("@ID_DESCR", IdDescr),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPacketFilter

        public DB_PACKET[] GetPacketFilter(long[] IdPacket, long[] SerialNbr, string Address, int[] IdTransmissionDriver, bool? IsIncoming, int[] IdTransmissionType,
                            int[] TransmittedPackets, int[] Bytes, int[] TransmittedBytes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketFilter"));

            return (DB_PACKET[])DB.ExecuteProcedure(
                "imrse_GetPacketFilter",
                new DB.AnalyzeDataSet(GetPacket),
                new DB.Parameter[] { 
					CreateTableParam("@ID_PACKET", IdPacket),
					CreateTableParam("@SERIAL_NBR", SerialNbr),
					new DB.InParameter("@ADDRESS", Address),
					CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
					new DB.InParameter("@IS_INCOMING", IsIncoming),
					CreateTableParam("@ID_TRANSMISSION_TYPE", IdTransmissionType),
					CreateTableParam("@TRANSMITTED_PACKETS", TransmittedPackets),
					CreateTableParam("@BYTES", Bytes),
					CreateTableParam("@TRANSMITTED_BYTES", TransmittedBytes),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPacketHistoryFilter

        public DB_PACKET_HISTORY[] GetPacketHistoryFilter(long[] IdPacketHistory, long[] IdPacket, TypeDateTimeCode Time, int[] IdPacketStatus, TypeDateTimeCode SystemTime, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHistoryFilter"));

            return (DB_PACKET_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPacketHistoryFilter",
                new DB.AnalyzeDataSet(GetPacketHistory),
                new DB.Parameter[] { 
					CreateTableParam("@ID_PACKET_HISTORY", IdPacketHistory),
					CreateTableParam("@ID_PACKET", IdPacket),
					CreateTableParam("@TIME_CODE", Time),
					CreateTableParam("@ID_PACKET_STATUS", IdPacketStatus),
					CreateTableParam("@SYSTEM_TIME_CODE", SystemTime),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetPacketStatusFilter

        public DB_PACKET_STATUS[] GetPacketStatusFilter(int[] IdPacketStatus, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketStatusFilter"));

            return (DB_PACKET_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPacketStatusFilter",
                new DB.AnalyzeDataSet(GetPacketStatus),
                new DB.Parameter[] { 
					CreateTableParam("@ID_PACKET_STATUS", IdPacketStatus),
					new DB.InParameter("@NAME", Name),
					CreateTableParam("@ID_DESCR", IdDescr),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetRouteTableFilter
        public DB_ROUTE_TABLE[] GetRouteTableFilter(int[] IdRoute, string Address, int[] Prefix, string Hop, int[] CostOut, int[] CostIn,
                            string Name, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTableFilter"));

            return (DB_ROUTE_TABLE[])DB.ExecuteProcedure(
                "imrse_GetRouteTableFilter",
                new DB.AnalyzeDataSet(GetRouteTable),
                new DB.Parameter[] { 
					CreateTableParam("@ID_ROUTE", IdRoute),
					new DB.InParameter("@ADDRESS", Address),
					CreateTableParam("@PREFIX", Prefix),
					new DB.InParameter("@HOP", Hop),
					CreateTableParam("@COST_OUT", CostOut),
					CreateTableParam("@COST_IN", CostIn),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetRouteTableBackupFilter

        public DB_ROUTE_TABLE_BACKUP[] GetRouteTableBackupFilter(int[] Id, int[] IdRoute, int[] IdRouteBackup, int[] Order, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTableBackupFilter"));

            if (UseBulkInsertSqlTableType && (
                (Id != null && Id.Length > MaxItemsPerSqlTableType) ||
                (IdRoute != null && IdRoute.Length > MaxItemsPerSqlTableType) ||
                (IdRouteBackup != null && IdRouteBackup.Length > MaxItemsPerSqlTableType) ||
                (Order != null && Order.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (Id != null) args.AddRange(Id.Select(s => new Tuple<int, object>(1, s)));
                if (IdRoute != null) args.AddRange(IdRoute.Select(s => new Tuple<int, object>(2, s)));
                if (IdRouteBackup != null) args.AddRange(IdRouteBackup.Select(s => new Tuple<int, object>(3, s)));
                if (Order != null) args.AddRange(Order.Select(s => new Tuple<int, object>(4, s)));

                DB_ROUTE_TABLE_BACKUP[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_ROUTE_TABLE_BACKUP[])DB.ExecuteProcedure(
                        "imrse_GetRouteTableBackupFilterTableArgs",
                        new DB.AnalyzeDataSet(GetRouteTableBackup),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Usunąc po zaaplikowanie poprwaek do procedur imrse_XXXFilterTableArgs
                    DeleteImrseProcArg(id);
                }
                return result;
            }
            else
            {
                return (DB_ROUTE_TABLE_BACKUP[])DB.ExecuteProcedure(
                    "imrse_GetRouteTableBackupFilter",
                    new DB.AnalyzeDataSet(GetRouteTableBackup),
                    new DB.Parameter[] { 
				CreateTableParam("@ID", Id),
				CreateTableParam("@ID_ROUTE", IdRoute),
				CreateTableParam("@ID_ROUTE_BACKUP", IdRouteBackup),
				CreateTableParam("@ORDER", Order),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #region GetSystemDataFilter

        public DB_SYSTEM_DATA[] GetSystemDataFilter(long[] IdSystemData, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));

            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemDataFilter",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] { 
					CreateTableParam("@ID_SYSTEM_DATA", IdSystemData),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
                    CreateTableParam("@INDEX_NBR", IndexNbr),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTransmissionDriverFilter
        public DB_TRANSMISSION_DRIVER[] GetTransmissionDriverFilter(int[] IdTransmissionDriver, int[] IdTransmissionDriverType, string Name, string RunAtHost, string ResponseAddress, string Plugin,
                            bool? IsActive, bool? UseCache, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverFilter"));

            return (DB_TRANSMISSION_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverFilter",
                new DB.AnalyzeDataSet(GetTransmissionDriver),
                new DB.Parameter[] { 
					CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
					CreateTableParam("@ID_TRANSMISSION_DRIVER_TYPE", IdTransmissionDriverType),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@RUN_AT_HOST", RunAtHost),
					new DB.InParameter("@RESPONSE_ADDRESS", ResponseAddress),
					new DB.InParameter("@PLUGIN", Plugin),
					new DB.InParameter("@IS_ACTIVE", IsActive),
					new DB.InParameter("@USE_CACHE", UseCache),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetTransmissionDriverDataFilter
        public DB_TRANSMISSION_DRIVER_DATA[] GetTransmissionDriverDataFilter(long[] IdTransmissionDriverData, int[] IdTransmissionDriver, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverDataFilter"));

            return (DB_TRANSMISSION_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverDataFilter",
                new DB.AnalyzeDataSet(GetTransmissionDriverData),
                new DB.Parameter[] { 
					CreateTableParam("@ID_TRANSMISSION_DRIVER_DATA", IdTransmissionDriverData),
					CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					CreateTableParam("@INDEX_NBR", IndexNbr),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
        #region GetTransmissionDriverTypeFilter

        public DB_TRANSMISSION_DRIVER_TYPE[] GetTransmissionDriverTypeFilter(int[] IdTransmissionDriverType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverTypeFilter"));

            return (DB_TRANSMISSION_DRIVER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionDriverTypeFilter",
                new DB.AnalyzeDataSet(GetTransmissionDriverType),
                new DB.Parameter[] { 
					CreateTableParam("@ID_TRANSMISSION_DRIVER_TYPE", IdTransmissionDriverType),
					new DB.InParameter("@NAME", Name),
					CreateTableParam("@ID_DESCR", IdDescr),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }

        #endregion
        #region GetTransmissionProtocolFilter
        public DB_TRANSMISSION_PROTOCOL[] GetTransmissionProtocolFilter(int[] IdTransmissionProtocol, int[] IdTransmissionDriver, int[] Order, int[] IdProtocolDriver, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionProtocolFilter"));

            return (DB_TRANSMISSION_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetTransmissionProtocolFilter",
                new DB.AnalyzeDataSet(GetTransmissionProtocol),
                new DB.Parameter[] { 
					CreateTableParam("@ID_TRANSMISSION_PROTOCOL", IdTransmissionProtocol),
					CreateTableParam("@ID_TRANSMISSION_DRIVER", IdTransmissionDriver),
					CreateTableParam("@ORDER", Order),
					CreateTableParam("@ID_PROTOCOL_DRIVER", IdProtocolDriver),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                autoTransaction, transactionLevel, commandTimeout
            );
        }
        #endregion
    }
}
