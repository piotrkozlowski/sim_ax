﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections.Concurrent;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.CORE;
using System.Data.SqlClient;
using IMR.Suite.Data.DB.CORE;

namespace IMR.Suite.Data.DB
{
    public partial class DBCommonCORE : DBCommon
    {
        #region Table ACTION

        private object GetAction(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ACTION()
            {
                ID_ACTION = GetValue<long>(row["ID_ACTION"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_ACTION_TYPE = GetValue<int>(row["ID_ACTION_TYPE"]),
                ID_ACTION_STATUS = GetValue<int>(row["ID_ACTION_STATUS"]),
                ID_ACTION_DATA = GetValue<long>(row["ID_ACTION_DATA"]),
                ID_ACTION_PARENT = GetNullableValue<long>(row["ID_ACTION_PARENT"]),
                ID_DATA_ARCH = GetNullableValue<long>(row["ID_DATA_ARCH"]),
                ID_MODULE = GetNullableValue<int>(row["ID_MODULE"]),
                ID_OPERATOR = GetNullableValue<int>(row["ID_OPERATOR"]),
                CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["CREATION_DATE"])),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION[] list = new DB_ACTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION insert = new DB_ACTION();
                insert.ID_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_ACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE"]);
                insert.ID_ACTION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_STATUS"]);
                insert.ID_ACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_DATA"]);
                insert.ID_ACTION_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_PARENT"]);
                insert.ID_DATA_ARCH = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH"]);
                insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public void DeleteAction(DB_ACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAction",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION", toBeDeleted.ID_ACTION)			
		}
            );
        }

        #endregion
        #region Table ACTION_DATA

        public DB_ACTION_DATA[] GetActionData()
        {
            return (DB_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionData",
                new DB.AnalyzeDataSet(GetActionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_DATA", new long[] {})
					}
            );
        }

        public DB_ACTION_DATA[] GetActionData(long[] Ids)
        {
            return (DB_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionData",
                new DB.AnalyzeDataSet(GetActionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_DATA", Ids)
					}
            );
        }

        private object GetActionData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_DATA[] list = new DB_ACTION_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_DATA insert = new DB_ACTION_DATA();
                insert.ID_ACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionData(DB_ACTION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_DATA", ToBeSaved.ID_ACTION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionData(DB_ACTION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_DATA", toBeDeleted.ID_ACTION_DATA),
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE), 
            new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)
		}
            );
        }

        #endregion
        #region Table ACTION_DEF

        public DB_ACTION_DEF[] GetActionDef()
        {
            return (DB_ACTION_DEF[])DB.ExecuteProcedure(
                "imrse_GetActionDef",
                new DB.AnalyzeDataSet(GetActionDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_DEF", new long[] {})
					}
            );
        }

        public DB_ACTION_DEF[] GetActionDef(long[] Ids)
        {
            return (DB_ACTION_DEF[])DB.ExecuteProcedure(
                "imrse_GetActionDef",
                new DB.AnalyzeDataSet(GetActionDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_DEF", Ids)
					}
            );
        }



        private object GetActionDef(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_DEF[] list = new DB_ACTION_DEF[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_DEF insert = new DB_ACTION_DEF();
                insert.ID_ACTION_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_DEF"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.ID_ACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE"]);
                insert.ID_ACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_DATA"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionDef(DB_ACTION_DEF ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_DEF", ToBeSaved.ID_ACTION_DEF);
            DB.InOutParameter InsertIdActionData = new DB.InOutParameter("@ID_ACTION_DATA", ToBeSaved.ID_ACTION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionDef",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@ID_ACTION_TYPE", ToBeSaved.ID_ACTION_TYPE)
														,InsertIdActionData//,new DB.InParameter("@ID_ACTION_DATA", ToBeSaved.ID_ACTION_DATA)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_DEF = (long)InsertId.Value;
            if (!InsertIdActionData.IsNull)
                ToBeSaved.ID_ACTION_DATA = (long)InsertIdActionData.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionDef(DB_ACTION_DEF toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionDef",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_DEF", toBeDeleted.ID_ACTION_DEF)			
		}
            );
        }

        #endregion
        #region Table ACTION_HISTORY
        public DB_ACTION_HISTORY[] GetActionHistory()
        {
            return (DB_ACTION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetActionHistory",
                new DB.AnalyzeDataSet(GetActionHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_HISTORY", new long[] {})
					}
            );
        }

        public DB_ACTION_HISTORY[] GetActionHistory(long[] Ids)
        {
            return (DB_ACTION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetActionHistory",
                new DB.AnalyzeDataSet(GetActionHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_HISTORY", Ids)
					}
            );
        }



        private object GetActionHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ACTION_HISTORY()
            {
                ID_ACTION_HISTORY = GetValue<long>(row["ID_ACTION_HISTORY"]),
                ID_ACTION = GetValue<long>(row["ID_ACTION"]),
                ID_ACTION_STATUS = GetValue<int>(row["ID_ACTION_STATUS"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
                ID_ACTION_HISTORY_DATA = GetNullableValue<long>(row["ID_ACTION_HISTORY_DATA"]),
                ID_MODULE = QueryResult.Tables[0].Columns.Contains("ID_MODULE") ? GetNullableValue<int>(row["ID_MODULE"]) : null,
                ASSEMBLY = QueryResult.Tables[0].Columns.Contains("ASSEMBLY") ? GetValue<string>(row["ASSEMBLY"]) : null,
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_HISTORY[] list = new DB_ACTION_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_HISTORY insert = new DB_ACTION_HISTORY();
                insert.ID_ACTION_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_HISTORY"]);
                insert.ID_ACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.ID_ACTION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_STATUS"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_ACTION_HISTORY_DATA = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_HISTORY_DATA"]);
                if (QueryResult.Tables[0].Columns.Contains("ID_MODULE"))
                    insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                if (QueryResult.Tables[0].Columns.Contains("ASSEMBLY"))
                    insert.ASSEMBLY = GetValue<string>(QueryResult.Tables[0].Rows[i]["ASSEMBLY"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveActionHistory(DB_ACTION_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_HISTORY", ToBeSaved.ID_ACTION_HISTORY);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
			parameters.Add(new DB.InParameter("@ID_ACTION", ToBeSaved.ID_ACTION));
			parameters.Add(new DB.InParameter("@ID_ACTION_STATUS", ToBeSaved.ID_ACTION_STATUS));
			parameters.Add(new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME)));
			parameters.Add(new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME)));
			parameters.Add(new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR));
			parameters.Add(new DB.InParameter("@ID_ACTION_HISTORY_DATA", ToBeSaved.ID_ACTION_HISTORY_DATA));
            if (IsParameterDefined("imrse_SaveActionHistory", "@ID_MODULE", this))
                parameters.Add(new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE));
            if (IsParameterDefined("imrse_SaveActionHistory", "@ASSEMBLY", this))
                parameters.Add(new DB.InParameter("@ASSEMBLY", ToBeSaved.ASSEMBLY));
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionHistory",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionHistory(DB_ACTION_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_HISTORY", toBeDeleted.ID_ACTION_HISTORY)			
		}
            );
        }

        #endregion
        #region Table ACTION_HISTORY_DATA

        public DB_ACTION_HISTORY_DATA[] GetActionHistoryData()
        {
            return (DB_ACTION_HISTORY_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionHistoryData",
                new DB.AnalyzeDataSet(GetActionHistoryData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_HISTORY_DATA", new long[] {})
					}
            );
        }

        public DB_ACTION_HISTORY_DATA[] GetActionHistoryData(long[] Ids)
        {
            return (DB_ACTION_HISTORY_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionHistoryData",
                new DB.AnalyzeDataSet(GetActionHistoryData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_HISTORY_DATA", Ids)
					}
            );
        }



        private object GetActionHistoryData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_HISTORY_DATA[] list = new DB_ACTION_HISTORY_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_HISTORY_DATA insert = new DB_ACTION_HISTORY_DATA();
                insert.ID_ACTION_HISTORY_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_HISTORY_DATA"]);
                insert.ARG_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ARG_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionHistoryData(DB_ACTION_HISTORY_DATA ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_HISTORY_DATA", ToBeSaved.ID_ACTION_HISTORY_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionHistoryData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ARG_NBR", ToBeSaved.ARG_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_HISTORY_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionHistoryData(DB_ACTION_HISTORY_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionHistoryData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_HISTORY_DATA", toBeDeleted.ID_ACTION_HISTORY_DATA)			
		}
            );
        }

        #endregion
        #region Table ACTION_SMS_MESSAGE

        public DB_ACTION_SMS_MESSAGE[] GetActionSmsMessage()
        {
            return (DB_ACTION_SMS_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetActionSmsMessage",
                new DB.AnalyzeDataSet(GetActionSmsMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_MESSAGE", new long[] {})
					}
            );
        }

        public DB_ACTION_SMS_MESSAGE[] GetActionSmsMessage(long[] Ids)
        {
            return (DB_ACTION_SMS_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetActionSmsMessage",
                new DB.AnalyzeDataSet(GetActionSmsMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_MESSAGE", Ids)
					}
            );
        }



        private object GetActionSmsMessage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_SMS_MESSAGE[] list = new DB_ACTION_SMS_MESSAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_SMS_MESSAGE insert = new DB_ACTION_SMS_MESSAGE();
                insert.ID_ACTION_SMS_MESSAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_SMS_MESSAGE"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.IS_INCOMING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_INCOMING"]);
                insert.ID_TRANSMISSION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_STATUS"]);
                insert.ID_ACTION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
                insert.MESSAGE = GetValue<string>(QueryResult.Tables[0].Rows[i]["MESSAGE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionSmsMessage(DB_ACTION_SMS_MESSAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_SMS_MESSAGE", ToBeSaved.ID_ACTION_SMS_MESSAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionSmsMessage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@IS_INCOMING", ToBeSaved.IS_INCOMING)
														,new DB.InParameter("@ID_TRANSMISSION_STATUS", ToBeSaved.ID_TRANSMISSION_STATUS)
														,new DB.InParameter("@ID_ACTION", ToBeSaved.ID_ACTION)
														,new DB.InParameter("@MESSAGE", ToBeSaved.MESSAGE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_SMS_MESSAGE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionSmsMessage(DB_ACTION_SMS_MESSAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionSmsMessage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_SMS_MESSAGE", toBeDeleted.ID_ACTION_SMS_MESSAGE)			
		}
            );
        }

        #endregion
        #region Table ACTION_SMS_TEXT_DATA

        public DB_ACTION_SMS_TEXT_DATA[] GetActionSmsTextData()
        {
            return (DB_ACTION_SMS_TEXT_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionSmsTextData",
                new DB.AnalyzeDataSet(GetActionSmsTextData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_TEXT", new long[] {})
					}
            );
        }

        public DB_ACTION_SMS_TEXT_DATA[] GetActionSmsTextData(long[] Ids)
        {
            return (DB_ACTION_SMS_TEXT_DATA[])DB.ExecuteProcedure(
                "imrse_GetActionSmsTextData",
                new DB.AnalyzeDataSet(GetActionSmsTextData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_SMS_TEXT", Ids)
					}
            );
        }



        private object GetActionSmsTextData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_SMS_TEXT_DATA[] list = new DB_ACTION_SMS_TEXT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_SMS_TEXT_DATA insert = new DB_ACTION_SMS_TEXT_DATA();
                insert.ID_ACTION_SMS_TEXT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION_SMS_TEXT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.ARG_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ARG_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.ID_UNIT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveActionSmsTextData(DB_ACTION_SMS_TEXT_DATA ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_SMS_TEXT", ToBeSaved.ID_ACTION_SMS_TEXT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionSmsTextData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@ARG_NBR", ToBeSaved.ARG_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_SMS_TEXT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteActionSmsTextData(DB_ACTION_SMS_TEXT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionSmsTextData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_SMS_TEXT", toBeDeleted.ID_ACTION_SMS_TEXT)
		}
            );
        }

        #endregion
        #region Table ACTION_STATUS
        public DB_ACTION_STATUS[] GetActionStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetActionStatus",
                new DB.AnalyzeDataSet(GetActionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_STATUS", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ACTION_STATUS[] GetActionStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetActionStatus",
                new DB.AnalyzeDataSet(GetActionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_STATUS", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetActionStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_STATUS[] list = new DB_ACTION_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_STATUS insert = new DB_ACTION_STATUS();
                insert.ID_ACTION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionStatus(DB_ACTION_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_STATUS", ToBeSaved.ID_ACTION_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionStatus(DB_ACTION_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_STATUS", toBeDeleted.ID_ACTION_STATUS)			
		}
            );
        }

        #endregion
        #region Table ACTION_TYPE
        public DB_ACTION_TYPE[] GetActionType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionType",
                new DB.AnalyzeDataSet(GetActionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ACTION_TYPE[] GetActionType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionType",
                new DB.AnalyzeDataSet(GetActionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetActionType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_TYPE[] list = new DB_ACTION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_TYPE insert = new DB_ACTION_TYPE();
                insert.ID_ACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE"]);
                insert.ID_ACTION_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.PLUGIN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN_NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.IS_ACTION_DATA_FIXED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTION_DATA_FIXED"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionType(DB_ACTION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_TYPE", ToBeSaved.ID_ACTION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ACTION_TYPE_CLASS", ToBeSaved.ID_ACTION_TYPE_CLASS)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@PLUGIN_NAME", ToBeSaved.PLUGIN_NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@IS_ACTION_DATA_FIXED", ToBeSaved.IS_ACTION_DATA_FIXED)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionType(DB_ACTION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_TYPE", toBeDeleted.ID_ACTION_TYPE)			
		}
            );
        }

        #endregion
        #region Table ACTION_TYPE_CLASS
        public DB_ACTION_TYPE_CLASS[] GetActionTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetActionTypeClass",
                new DB.AnalyzeDataSet(GetActionTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_CLASS", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ACTION_TYPE_CLASS[] GetActionTypeClass(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTION_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetActionTypeClass",
                new DB.AnalyzeDataSet(GetActionTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_CLASS", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetActionTypeClass(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_TYPE_CLASS[] list = new DB_ACTION_TYPE_CLASS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_TYPE_CLASS insert = new DB_ACTION_TYPE_CLASS();
                insert.ID_ACTION_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionTypeClass(DB_ACTION_TYPE_CLASS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_TYPE_CLASS", ToBeSaved.ID_ACTION_TYPE_CLASS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionTypeClass",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_TYPE_CLASS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionTypeClass(DB_ACTION_TYPE_CLASS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionTypeClass",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_TYPE_CLASS", toBeDeleted.ID_ACTION_TYPE_CLASS)			
		}
            );
        }

        #endregion
        #region Table ACTION_TYPE_GROUP

        public DB_ACTION_TYPE_GROUP[] GetActionTypeGroup()
        {
            return (DB_ACTION_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroup",
                new DB.AnalyzeDataSet(GetActionTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_ACTION_TYPE_GROUP[] GetActionTypeGroup(int[] Ids)
        {
            return (DB_ACTION_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroup",
                new DB.AnalyzeDataSet(GetActionTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP", Ids)
					}
            );
        }



        private object GetActionTypeGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_TYPE_GROUP[] list = new DB_ACTION_TYPE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_TYPE_GROUP insert = new DB_ACTION_TYPE_GROUP();
                insert.ID_ACTION_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_GROUP"]);
                insert.ID_ACTION_TYPE_GROUP_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_GROUP_TYPE"]);
                insert.ID_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
                insert.ID_PARENT_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARENT_GROUP"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionTypeGroup(DB_ACTION_TYPE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_TYPE_GROUP", ToBeSaved.ID_ACTION_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionTypeGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ACTION_TYPE_GROUP_TYPE", ToBeSaved.ID_ACTION_TYPE_GROUP_TYPE)
														,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE, 0, 0)
														,new DB.InParameter("@ID_PARENT_GROUP", ToBeSaved.ID_PARENT_GROUP)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionTypeGroup(DB_ACTION_TYPE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionTypeGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_TYPE_GROUP", toBeDeleted.ID_ACTION_TYPE_GROUP)			
		}
            );
        }

        #endregion
        #region Table ACTION_TYPE_GROUP_TYPE

        public DB_ACTION_TYPE_GROUP_TYPE[] GetActionTypeGroupType()
        {
            return (DB_ACTION_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroupType",
                new DB.AnalyzeDataSet(GetActionTypeGroupType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP_TYPE", new int[] {})
					}
            );
        }

        public DB_ACTION_TYPE_GROUP_TYPE[] GetActionTypeGroupType(int[] Ids)
        {
            return (DB_ACTION_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetActionTypeGroupType",
                new DB.AnalyzeDataSet(GetActionTypeGroupType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP_TYPE", Ids)
					}
            );
        }



        private object GetActionTypeGroupType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_TYPE_GROUP_TYPE[] list = new DB_ACTION_TYPE_GROUP_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_TYPE_GROUP_TYPE insert = new DB_ACTION_TYPE_GROUP_TYPE();
                insert.ID_ACTION_TYPE_GROUP_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_GROUP_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionTypeGroupType(DB_ACTION_TYPE_GROUP_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_TYPE_GROUP_TYPE", ToBeSaved.ID_ACTION_TYPE_GROUP_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionTypeGroupType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_TYPE_GROUP_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionTypeGroupType(DB_ACTION_TYPE_GROUP_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionTypeGroupType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_TYPE_GROUP_TYPE", toBeDeleted.ID_ACTION_TYPE_GROUP_TYPE)			
		}
            );
        }

        #endregion
        #region Table ACTION_TYPE_IN_GROUP

        public DB_ACTION_TYPE_IN_GROUP[] GetActionTypeInGroup()
        {
            return (DB_ACTION_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeInGroup",
                new DB.AnalyzeDataSet(GetActionTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_ACTION_TYPE_IN_GROUP[] GetActionTypeInGroup(int[] Ids)
        {
            return (DB_ACTION_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActionTypeInGroup",
                new DB.AnalyzeDataSet(GetActionTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTION_TYPE_GROUP", Ids)
					}
            );
        }



        private object GetActionTypeInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTION_TYPE_IN_GROUP[] list = new DB_ACTION_TYPE_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTION_TYPE_IN_GROUP insert = new DB_ACTION_TYPE_IN_GROUP();
                insert.ID_ACTION_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE_GROUP"]);
                insert.ACTION_TYPE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["ACTION_TYPE_NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActionTypeInGroup(DB_ACTION_TYPE_IN_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTION_TYPE_GROUP", ToBeSaved.ID_ACTION_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActionTypeInGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ACTION_TYPE_NAME", ToBeSaved.ACTION_TYPE_NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTION_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActionTypeInGroup(DB_ACTION_TYPE_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActionTypeInGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTION_TYPE_GROUP", toBeDeleted.ID_ACTION_TYPE_GROUP),
			new DB.InParameter("@ACTION_TYPE_NAME", toBeDeleted.ACTION_TYPE_NAME)	
		}
            );
        }

        #endregion

        #region Table ACTOR

        public DB_ACTOR[] GetActor(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTOR[])DB.ExecuteProcedure(
                "imrse_GetActor",
                new DB.AnalyzeDataSet(GetActor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTOR", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_ACTOR[] GetActor(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ACTOR[])DB.ExecuteProcedure(
                "imrse_GetActor",
                new DB.AnalyzeDataSet(GetActor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTOR", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }



        private object GetActor(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTOR[] list = new DB_ACTOR[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTOR insert = new DB_ACTOR();
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.SURNAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["SURNAME"]);
                insert.CITY = GetValue<string>(QueryResult.Tables[0].Rows[i]["CITY"]);
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.POSTCODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["POSTCODE"]);
                insert.EMAIL = GetValue<string>(QueryResult.Tables[0].Rows[i]["EMAIL"]);
                insert.MOBILE = GetValue<string>(QueryResult.Tables[0].Rows[i]["MOBILE"]);
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.AVATAR = (byte[])GetValue(QueryResult.Tables[0].Rows[i]["AVATAR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActor(DB_ACTOR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActor",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@SURNAME", ToBeSaved.SURNAME)
														,new DB.InParameter("@CITY", ToBeSaved.CITY)
														,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
														,new DB.InParameter("@POSTCODE", ToBeSaved.POSTCODE)
														,new DB.InParameter("@EMAIL", ToBeSaved.EMAIL)
														,new DB.InParameter("@MOBILE", ToBeSaved.MOBILE)
														,new DB.InParameter("@PHONE", ToBeSaved.PHONE)
														,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@AVATAR", ToBeSaved.AVATAR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTOR = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActor(DB_ACTOR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActor",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTOR", toBeDeleted.ID_ACTOR)			
		}
            );
        }

        #endregion
        #region Table ACTOR_GROUP

        public DB_ACTOR_GROUP[] GetActorGroup()
        {
            return (DB_ACTOR_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorGroup",
                new DB.AnalyzeDataSet(GetActorGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTOR_GROUP", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_ACTOR_GROUP[] GetActorGroup(int[] Ids)
        {
            return (DB_ACTOR_GROUP[])DB.ExecuteProcedure(
                "imrse_GetActorGroup",
                new DB.AnalyzeDataSet(GetActorGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTOR_GROUP", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetActorGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTOR_GROUP[] list = new DB_ACTOR_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTOR_GROUP insert = new DB_ACTOR_GROUP();
                insert.ID_ACTOR_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.BUILT_IN = GetValue<bool>(QueryResult.Tables[0].Rows[i]["BUILT_IN"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveActorGroup(DB_ACTOR_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ACTOR_GROUP", ToBeSaved.ID_ACTOR_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveActorGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@BUILT_IN", ToBeSaved.BUILT_IN)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ACTOR_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteActorGroup(DB_ACTOR_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActorGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTOR_GROUP", toBeDeleted.ID_ACTOR_GROUP)			
		}
            );
        }

        #endregion

        #region Table ACTIVITY

        public DB_ACTIVITY[] GetActivity()
        {
            return (DB_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetActivity",
                new DB.AnalyzeDataSet(GetActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTIVITY", new int[] {})
					}
            );
        }

        public DB_ACTIVITY[] GetActivity(int[] Ids)
        {
            return (DB_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetActivity",
                new DB.AnalyzeDataSet(GetActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ACTIVITY", Ids)
					}
            );
        }

        private object GetActivity(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ACTIVITY[] list = new DB_ACTIVITY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ACTIVITY insert = new DB_ACTIVITY();
                insert.ID_ACTIVITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTIVITY"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteActivity(DB_ACTIVITY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelActivity",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ACTIVITY", toBeDeleted.ID_ACTIVITY)			
		}
            );
        }

        #endregion

        #region Table ALARM

        public DB_ALARM[] GetAlarm()
        {
            return (DB_ALARM[])DB.ExecuteProcedure(
                "imrse_GetAlarm",
                new DB.AnalyzeDataSet(GetAlarm),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM", new long[] {})
					}
            );
        }

        public DB_ALARM[] GetAlarm(long[] Ids)
        {
            return (DB_ALARM[])DB.ExecuteProcedure(
                "imrse_GetAlarm",
                new DB.AnalyzeDataSet(GetAlarm),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM", Ids)
					}
            );
        }



        private object GetAlarm(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM[] list = new DB_ALARM[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM insert = new DB_ALARM();
                insert.ID_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM"]);
                insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_ALARM_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_GROUP"]);
                insert.ID_ALARM_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_STATUS"]);
                insert.ID_TRANSMISSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarm(DB_ALARM ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM", ToBeSaved.ID_ALARM);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarm",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_ALARM_GROUP", ToBeSaved.ID_ALARM_GROUP)
														,new DB.InParameter("@ID_ALARM_STATUS", ToBeSaved.ID_ALARM_STATUS)
														,new DB.InParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarm(DB_ALARM toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarm",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM", toBeDeleted.ID_ALARM)			
		}
            );
        }

        #endregion
        #region Table ALARM_DATA

        public DB_ALARM_DATA[] GetAlarmData()
        {
            return (DB_ALARM_DATA[])DB.ExecuteProcedure(
                "imrse_GetAlarmData",
                new DB.AnalyzeDataSet(GetAlarmData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DATA", new long[] {})
					}
            );
        }

        public DB_ALARM_DATA[] GetAlarmData(long[] Ids)
        {
            return (DB_ALARM_DATA[])DB.ExecuteProcedure(
                "imrse_GetAlarmData",
                new DB.AnalyzeDataSet(GetAlarmData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DATA", Ids)
					}
            );
        }



        private object GetAlarmData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_DATA[] list = new DB_ALARM_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_DATA insert = new DB_ALARM_DATA();
                insert.ID_ALARM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmData(DB_ALARM_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_DATA", ToBeSaved.ID_ALARM_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmData(DB_ALARM_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_DATA", toBeDeleted.ID_ALARM_DATA),
			new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE),
            new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)
		}
            );
        }

        #endregion
        #region Table ALARM_DEF

        public DB_ALARM_DEF[] GetAlarmDef()
        {
            return (DB_ALARM_DEF[])DB.ExecuteProcedure(
                "imrse_GetAlarmDef",
                new DB.AnalyzeDataSet(GetAlarmDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DEF", new long[] {})
					}
            );
        }

        public DB_ALARM_DEF[] GetAlarmDef(long[] Ids)
        {
            return (DB_ALARM_DEF[])DB.ExecuteProcedure(
                "imrse_GetAlarmDef",
                new DB.AnalyzeDataSet(GetAlarmDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DEF", Ids)
					}
            );
        }



        private object GetAlarmDef(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_DEF[] list = new DB_ALARM_DEF[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_DEF insert = new DB_ALARM_DEF();
                insert.ID_ALARM_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DEF"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_ALARM_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TYPE"]);
                insert.ID_DATA_TYPE_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_ALARM"]);
                insert.INDEX_NBR = QueryResult.Tables[0].Columns.Contains("INDEX_NBR") ? GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]) : 0;
                insert.ID_DATA_TYPE_CONFIG = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CONFIG"]);
                insert.SYSTEM_ALARM_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["SYSTEM_ALARM_VALUE"]);
                insert.IS_ENABLED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ENABLED"]);
                insert.ID_ALARM_TEXT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TEXT"]);
                insert.ID_ALARM_DATA = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DATA"]);
                insert.CHECK_LAST_ALARM = GetValue<bool>(QueryResult.Tables[0].Rows[i]["CHECK_LAST_ALARM"]);
                insert.SUSPENSION_TIME = GetValue<int>(QueryResult.Tables[0].Rows[i]["SUSPENSION_TIME"]);
                insert.IS_CONFIRMABLE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_CONFIRMABLE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmDef(DB_ALARM_DEF ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_DEF", ToBeSaved.ID_ALARM_DEF);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@NAME", ToBeSaved.NAME));
            parameters.Add(new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR));
            parameters.Add(new DB.InParameter("@ID_METER", ToBeSaved.ID_METER));
            parameters.Add(new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION));
            parameters.Add(new DB.InParameter("@ID_ALARM_TYPE", ToBeSaved.ID_ALARM_TYPE));
            parameters.Add(new DB.InParameter("@ID_DATA_TYPE_ALARM", ToBeSaved.ID_DATA_TYPE_ALARM));
            if (IsParameterDefined("imrse_SaveAlarmDef", "@INDEX_NBR", this))
                parameters.Add(new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR));
            parameters.Add(new DB.InParameter("@ID_DATA_TYPE_CONFIG", ToBeSaved.ID_DATA_TYPE_CONFIG));
            parameters.Add(ParamObject("@SYSTEM_ALARM_VALUE", ToBeSaved.SYSTEM_ALARM_VALUE, 0, 0));
            parameters.Add(new DB.InParameter("@IS_ENABLED", ToBeSaved.IS_ENABLED));
            parameters.Add(new DB.InParameter("@ID_ALARM_TEXT", ToBeSaved.ID_ALARM_TEXT));
            parameters.Add(new DB.InParameter("@ID_ALARM_DATA", ToBeSaved.ID_ALARM_DATA));
            parameters.Add(new DB.InParameter("@CHECK_LAST_ALARM", ToBeSaved.CHECK_LAST_ALARM));
            parameters.Add(new DB.InParameter("@SUSPENSION_TIME", ToBeSaved.SUSPENSION_TIME));
            parameters.Add(new DB.InParameter("@IS_CONFIRMABLE", ToBeSaved.IS_CONFIRMABLE));

            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmDef",
              parameters.ToArray()
            );

            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_DEF = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmDef(DB_ALARM_DEF toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmDef",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_DEF", toBeDeleted.ID_ALARM_DEF)			
		}
            );
        }

        #endregion
        #region Table ALARM_DEF_GROUP

        public DB_ALARM_DEF_GROUP[] GetAlarmDefGroup()
        {
            return (DB_ALARM_DEF_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmDefGroup",
                new DB.AnalyzeDataSet(GetAlarmDefGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DEF", new long[] {})
					}
            );
        }

        public DB_ALARM_DEF_GROUP[] GetAlarmDefGroup(long[] Ids)
        {
            return (DB_ALARM_DEF_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmDefGroup",
                new DB.AnalyzeDataSet(GetAlarmDefGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_DEF", Ids)
					}
            );
        }



        private object GetAlarmDefGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_DEF_GROUP[] list = new DB_ALARM_DEF_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_DEF_GROUP insert = new DB_ALARM_DEF_GROUP();
                insert.ID_ALARM_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DEF"]);
                insert.ID_ALARM_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_GROUP"]);
                insert.ID_TRANSMISSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                insert.CONFIRM_LEVEL = GetValue<int>(QueryResult.Tables[0].Rows[i]["CONFIRM_LEVEL"]);
                insert.CONFIRM_TIMEOUT = GetValue<int>(QueryResult.Tables[0].Rows[i]["CONFIRM_TIMEOUT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmDefGroup(DB_ALARM_DEF_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_DEF", ToBeSaved.ID_ALARM_DEF);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveAlarmDefGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ALARM_GROUP", ToBeSaved.ID_ALARM_GROUP)
														,new DB.InParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE)
														,new DB.InParameter("@CONFIRM_LEVEL", ToBeSaved.CONFIRM_LEVEL)
														,new DB.InParameter("@CONFIRM_TIMEOUT", ToBeSaved.CONFIRM_TIMEOUT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_DEF = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmDefGroup(DB_ALARM_DEF_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelAlarmDefGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_DEF", toBeDeleted.ID_ALARM_DEF),
            new DB.InParameter("@ID_ALARM_GROUP", toBeDeleted.ID_ALARM_GROUP),
            new DB.InParameter("@ID_TRANSMISSION_TYPE", toBeDeleted.ID_TRANSMISSION_TYPE)
		}
            );
        }

        #endregion
        #region Table ALARM_EVENT

        public DB_ALARM_EVENT[] GetAlarmEvent()
        {
            return (DB_ALARM_EVENT[])DB.ExecuteProcedure(
                "imrse_GetAlarmEvent",
                new DB.AnalyzeDataSet(GetAlarmEvent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_EVENT", new long[] {})
					}
            );
        }

        public DB_ALARM_EVENT[] GetAlarmEvent(long[] Ids)
        {
            return (DB_ALARM_EVENT[])DB.ExecuteProcedure(
                "imrse_GetAlarmEvent",
                new DB.AnalyzeDataSet(GetAlarmEvent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_EVENT", Ids)
					}
            );
        }



        private object GetAlarmEvent(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_EVENT[] list = new DB_ALARM_EVENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_EVENT insert = new DB_ALARM_EVENT();
                insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.ID_ALARM_DEF = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_DEF"]);
                insert.ID_DATA_ARCH = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH"]);
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmEvent(DB_ALARM_EVENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmEvent",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ALARM_DEF", ToBeSaved.ID_ALARM_DEF)
														,new DB.InParameter("@ID_DATA_ARCH", ToBeSaved.ID_DATA_ARCH)
														,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_EVENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmEvent(DB_ALARM_EVENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmEvent",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_EVENT", toBeDeleted.ID_ALARM_EVENT)			
		}
            );
        }

        #endregion
        #region Table ALARM_GROUP

        public DB_ALARM_GROUP[] GetAlarmGroup()
        {
            return (DB_ALARM_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroup",
                new DB.AnalyzeDataSet(GetAlarmGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_GROUP", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_ALARM_GROUP[] GetAlarmGroup(int[] Ids)
        {
            return (DB_ALARM_GROUP[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroup",
                new DB.AnalyzeDataSet(GetAlarmGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_GROUP", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetAlarmGroup(DataSet QueryResult)
        {
            bool bUseIsEnabledColumn = QueryResult.Tables[0].Columns.Contains("IS_ENABLED");

            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_GROUP[] list = new DB_ALARM_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_GROUP insert = new DB_ALARM_GROUP();
                insert.ID_ALARM_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                if (bUseIsEnabledColumn)
                    insert.IS_ENABLED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ENABLED"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveAlarmGroup(DB_ALARM_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_GROUP", ToBeSaved.ID_ALARM_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
                                                        ,new DB.InParameter("@IS_ENABLED", ToBeSaved.IS_ENABLED)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteAlarmGroup(DB_ALARM_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_GROUP", toBeDeleted.ID_ALARM_GROUP)			
		}
            );
        }

        #endregion
        #region Table ALARM_GROUP_OPERATOR

        public DB_ALARM_GROUP_OPERATOR[] GetAlarmGroupOperator()
        {
            return (DB_ALARM_GROUP_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroupOperator",
                new DB.AnalyzeDataSet(GetAlarmGroupOperator),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_GROUP", new int[] {})
					}
            );
        }

        public DB_ALARM_GROUP_OPERATOR[] GetAlarmGroupOperator(int[] Ids)
        {
            return (DB_ALARM_GROUP_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetAlarmGroupOperator",
                new DB.AnalyzeDataSet(GetAlarmGroupOperator),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_GROUP", Ids)
					}
            );
        }



        private object GetAlarmGroupOperator(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_GROUP_OPERATOR[] list = new DB_ALARM_GROUP_OPERATOR[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_GROUP_OPERATOR insert = new DB_ALARM_GROUP_OPERATOR();
                insert.ID_ALARM_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_GROUP"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_DATA_TYPE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveAlarmGroupOperator(DB_ALARM_GROUP_OPERATOR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_GROUP", ToBeSaved.ID_ALARM_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmGroupOperator",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
					,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteAlarmGroupOperator(DB_ALARM_GROUP_OPERATOR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmGroupOperator",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_GROUP", toBeDeleted.ID_ALARM_GROUP),			
            new DB.InParameter("@ID_OPERATOR", toBeDeleted.ID_OPERATOR),			
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)			
		    }
            );
        }

        #endregion
        #region Table ALARM_HISTORY

        public DB_ALARM_HISTORY[] GetAlarmHistory()
        {
            return (DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetAlarmHistory",
                new DB.AnalyzeDataSet(GetAlarmHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_HISTORY", new long[] {})
					}
            );
        }

        public DB_ALARM_HISTORY[] GetAlarmHistory(long[] Ids)
        {
            return (DB_ALARM_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetAlarmHistory",
                new DB.AnalyzeDataSet(GetAlarmHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_HISTORY", Ids)
					}
            );
        }



        private object GetAlarmHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_HISTORY[] list = new DB_ALARM_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_HISTORY insert = new DB_ALARM_HISTORY();
                insert.ID_ALARM_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_HISTORY"]);
                insert.ID_ALARM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM"]);
                insert.ID_ALARM_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_STATUS"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.START_RULE_HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["START_RULE_HOST"]);
                insert.START_RULE_USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["START_RULE_USER"]);
                insert.END_RULE_HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["END_RULE_HOST"]);
                insert.END_RULE_USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["END_RULE_USER"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmHistory(DB_ALARM_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_HISTORY", ToBeSaved.ID_ALARM_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ALARM", ToBeSaved.ID_ALARM)
														,new DB.InParameter("@ID_ALARM_STATUS", ToBeSaved.ID_ALARM_STATUS)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@START_RULE_HOST", ToBeSaved.START_RULE_HOST)
														,new DB.InParameter("@START_RULE_USER", ToBeSaved.START_RULE_USER)
														,new DB.InParameter("@END_RULE_HOST", ToBeSaved.END_RULE_HOST)
														,new DB.InParameter("@END_RULE_USER", ToBeSaved.END_RULE_USER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmHistory(DB_ALARM_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_HISTORY", toBeDeleted.ID_ALARM_HISTORY)			
		}
            );
        }

        #endregion
        #region Table ALARM_MESSAGE

        public DB_ALARM_MESSAGE[] GetAlarmMessage()
        {
            return (DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetAlarmMessage",
                new DB.AnalyzeDataSet(GetAlarmMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_EVENT", new long[] {})
					}
            );
        }

        public DB_ALARM_MESSAGE[] GetAlarmMessage(long[] Ids)
        {
            return (DB_ALARM_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetAlarmMessage",
                new DB.AnalyzeDataSet(GetAlarmMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_EVENT", Ids)
					}
            );
        }



        private object GetAlarmMessage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_MESSAGE[] list = new DB_ALARM_MESSAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_MESSAGE insert = new DB_ALARM_MESSAGE();
                insert.ID_ALARM_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_EVENT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.ALARM_MESSAGE = GetValue<string>(QueryResult.Tables[0].Rows[i]["ALARM_MESSAGE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmMessage(DB_ALARM_MESSAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_EVENT", ToBeSaved.ID_ALARM_EVENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmMessage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@ALARM_MESSAGE", ToBeSaved.ALARM_MESSAGE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_EVENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmMessage(DB_ALARM_MESSAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmMessage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_EVENT", toBeDeleted.ID_ALARM_EVENT)			
		}
            );
        }

        #endregion
        #region Table ALARM_STATUS

        public DB_ALARM_STATUS[] GetAlarmStatus()
        {
            return (DB_ALARM_STATUS[])DB.ExecuteProcedure(
                "imrse_GetAlarmStatus",
                new DB.AnalyzeDataSet(GetAlarmStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_STATUS", new int[] {})
					}
            );
        }

        public DB_ALARM_STATUS[] GetAlarmStatus(int[] Ids)
        {
            return (DB_ALARM_STATUS[])DB.ExecuteProcedure(
                "imrse_GetAlarmStatus",
                new DB.AnalyzeDataSet(GetAlarmStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_STATUS", Ids)
					}
            );
        }



        private object GetAlarmStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_STATUS[] list = new DB_ALARM_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_STATUS insert = new DB_ALARM_STATUS();
                insert.ID_ALARM_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveAlarmStatus(DB_ALARM_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_STATUS", ToBeSaved.ID_ALARM_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteAlarmStatus(DB_ALARM_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_STATUS", toBeDeleted.ID_ALARM_STATUS)			
		}
            );
        }

        #endregion
        #region Table ALARM_TEXT_DATA_TYPE

        public DB_ALARM_TEXT_DATA_TYPE[] GetAlarmTextDataType()
        {
            return (DB_ALARM_TEXT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmTextDataType",
                new DB.AnalyzeDataSet(GetAlarmTextDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TEXT", new long[] {})
					}
            );
        }

        public DB_ALARM_TEXT_DATA_TYPE[] GetAlarmTextDataType(long[] Ids)
        {
            return (DB_ALARM_TEXT_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmTextDataType",
                new DB.AnalyzeDataSet(GetAlarmTextDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TEXT", Ids)
					}
            );
        }



        private object GetAlarmTextDataType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_TEXT_DATA_TYPE[] list = new DB_ALARM_TEXT_DATA_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_TEXT_DATA_TYPE insert = new DB_ALARM_TEXT_DATA_TYPE();
                insert.ID_ALARM_TEXT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TEXT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                insert.ARG_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ARG_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveAlarmTextDataType(DB_ALARM_TEXT_DATA_TYPE ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_TEXT", ToBeSaved.ID_ALARM_TEXT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmTextDataType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@ARG_NBR", ToBeSaved.ARG_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_TEXT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAlarmTextDataType(DB_ALARM_TEXT_DATA_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmTextDataType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_TEXT", toBeDeleted.ID_ALARM_TEXT)			
		}
            );
        }

        #endregion
        #region Table ALARM_TYPE

        public DB_ALARM_TYPE[] GetAlarmType()
        {
            return (DB_ALARM_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmType",
                new DB.AnalyzeDataSet(GetAlarmType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TYPE", new int[] {})
					}
            );
        }

        public DB_ALARM_TYPE[] GetAlarmType(int[] Ids)
        {
            return (DB_ALARM_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAlarmType",
                new DB.AnalyzeDataSet(GetAlarmType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ALARM_TYPE", Ids)
					}
            );
        }



        private object GetAlarmType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ALARM_TYPE[] list = new DB_ALARM_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ALARM_TYPE insert = new DB_ALARM_TYPE();
                insert.ID_ALARM_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ALARM_TYPE"]);
                insert.SYMBOL = GetValue<string>(QueryResult.Tables[0].Rows[i]["SYMBOL"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveAlarmType(DB_ALARM_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ALARM_TYPE", ToBeSaved.ID_ALARM_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAlarmType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SYMBOL", ToBeSaved.SYMBOL)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ALARM_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteAlarmType(DB_ALARM_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAlarmType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ALARM_TYPE", toBeDeleted.ID_ALARM_TYPE)			
		}
            );
        }

        #endregion

        #region Table ARTICLE

        public DB_ARTICLE[] GetArticle(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetArticle",
                new DB.AnalyzeDataSet(GetArticle),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ARTICLE", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ARTICLE[] GetArticle(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetArticle",
                new DB.AnalyzeDataSet(GetArticle),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ARTICLE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public long GetIdArticleForDepositoryElement(string SerialNumber, string Name, string FirstQuarter, string SecondQuarter, string ThirdQuarter, bool AIUTDevice, int IdDistributor)
        {

            long IdArticle = 0;
            DB.InOutParameter ArticleId = new DB.InOutParameter("@ID_ARTICLE", IdArticle);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_GetIdArticleForDepositoryElement",
                new DB.Parameter[] { 
			            new DB.InParameter("@SERIAL_NBR", SerialNumber)
					,new DB.InParameter("@NAME", Name)
					,new DB.InParameter("@FIRST_QUARTER", FirstQuarter)
					,new DB.InParameter("@SECOND_QUARTER", SecondQuarter)
					,new DB.InParameter("@THIRD_QUARTER", ThirdQuarter)
                    ,new DB.InParameter("@ID_DISTRIBUTOR", IdDistributor)
                    ,new DB.InParameter("@AIUT_DEVICE", AIUTDevice)
                    ,ArticleId
				}
            );

            return (long)ArticleId.Value;

        }



        private object GetArticle(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ARTICLE[] list = new DB_ARTICLE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ARTICLE insert = new DB_ARTICLE();
                insert.ID_ARTICLE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE"]);
                insert.EXTERNAL_ID = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["EXTERNAL_ID"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.IS_UNIQUE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_UNIQUE"]);
                insert.VISIBLE = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["VISIBLE"]);
                insert.IS_AIUT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_AIUT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveArticle(DB_ARTICLE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ARTICLE", ToBeSaved.ID_ARTICLE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveArticle",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@EXTERNAL_ID", ToBeSaved.EXTERNAL_ID)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@IS_UNIQUE", ToBeSaved.IS_UNIQUE)
														,new DB.InParameter("@VISIBLE", ToBeSaved.VISIBLE)
														,new DB.InParameter("@IS_AIUT", ToBeSaved.IS_AIUT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ARTICLE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteArticle(DB_ARTICLE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelArticle",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ARTICLE", toBeDeleted.ID_ARTICLE)			
		}
            );
        }

        #endregion
        #region Table ARTICLE_DATA

        public DB_ARTICLE_DATA[] GetArticleData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ARTICLE_DATA[])DB.ExecuteProcedure(
                "imrse_GetArticleData",
                new DB.AnalyzeDataSet(GetArticleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ARTICLE_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ARTICLE_DATA[] GetArticleData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ARTICLE_DATA[])DB.ExecuteProcedure(
                "imrse_GetArticleData",
                new DB.AnalyzeDataSet(GetArticleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ARTICLE_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetArticleData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ARTICLE_DATA()
            {
                ID_ARTICLE_DATA = GetValue<long>(row["ID_ARTICLE_DATA"]),
                ID_ARTICLE = GetValue<long>(row["ID_ARTICLE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_ARTICLE_DATA[] list = new DB_ARTICLE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_ARTICLE_DATA insert = new DB_ARTICLE_DATA();
									insert.ID_ARTICLE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE_DATA"]);
												insert.ID_ARTICLE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveArticleData(DB_ARTICLE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ARTICLE_DATA", ToBeSaved.ID_ARTICLE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveArticleData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ARTICLE", ToBeSaved.ID_ARTICLE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ARTICLE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteArticleData(DB_ARTICLE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelArticleData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ARTICLE_DATA", toBeDeleted.ID_ARTICLE_DATA)			
		}
            );
        }

        #endregion

        #region Table ATG_TYPE

        public DB_ATG_TYPE[] GetAtgType()
        {
            return (DB_ATG_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAtgType",
                new DB.AnalyzeDataSet(GetAtgType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ATG_TYPE", new int[] {})
					}
            );
        }

        public DB_ATG_TYPE[] GetAtgType(int[] Ids)
        {
            return (DB_ATG_TYPE[])DB.ExecuteProcedure(
                "imrse_GetAtgType",
                new DB.AnalyzeDataSet(GetAtgType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ATG_TYPE", Ids)
					}
            );
        }

        private object GetAtgType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ATG_TYPE[] list = new DB_ATG_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ATG_TYPE insert = new DB_ATG_TYPE();
                insert.ID_ATG_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ATG_TYPE"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveAtgType(DB_ATG_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ATG_TYPE", ToBeSaved.ID_ATG_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAtgType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ATG_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteAtgType(DB_ATG_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAtgType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ATG_TYPE", toBeDeleted.ID_ATG_TYPE)			
		}
            );
        }

        #endregion

        #region Table AUDIT

        public DB_AUDIT[] GetAudit()
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_AUDIT", new long[] {})
					}
            );
        }

        public DB_AUDIT[] GetAudit(long[] Ids)
        {
            return (DB_AUDIT[])DB.ExecuteProcedure(
                "imrse_GetAudit",
                new DB.AnalyzeDataSet(GetAudit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_AUDIT", Ids)
					}
            );
        }



        private object GetAudit(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_AUDIT()
            {
                ID_AUDIT = GetValue<long>(row["ID_AUDIT"]),
                BATCH_ID = GetValue<long>(row["BATCH_ID"]),
                CHANGE_TYPE = GetValue<int>(row["CHANGE_TYPE"]),
                TABLE_NAME = GetValue<string>(row["TABLE_NAME"]),
                KEY1 = GetNullableValue<long>(row["KEY1"]),
                KEY2 = GetNullableValue<long>(row["KEY2"]),
                KEY3 = GetNullableValue<long>(row["KEY3"]),
                KEY4 = GetNullableValue<long>(row["KEY4"]),
                KEY5 = GetNullableValue<long>(row["KEY5"]),
                KEY6 = GetNullableValue<long>(row["KEY6"]),
                COLUMN_NAME = GetValue<string>(row["COLUMN_NAME"]),
                OLD_VALUE = GetValue(row["OLD_VALUE"]),
                NEW_VALUE = GetValue(row["NEW_VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                USER = GetValue<string>(row["USER"]),
                HOST = GetValue<string>(row["HOST"]),
                APPLICATION = GetValue<string>(row["APPLICATION"]),
                CONTEXTINFO = QueryResult.Tables[0].Columns.Contains("CONTEXTINFO") ? GetValue<string>(row["CONTEXTINFO"]) : null, 
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_AUDIT[] list = new DB_AUDIT[count];
    for (int i = 0; i < count; i++)
    {
        DB_AUDIT insert = new DB_AUDIT();
									insert.ID_AUDIT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_AUDIT"]);
												insert.BATCH_ID = GetValue<long>(QueryResult.Tables[0].Rows[i]["BATCH_ID"]);
												insert.CHANGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_TYPE"]);
												insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
												insert.KEY1 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY1"]);
												insert.KEY2 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY2"]);
												insert.KEY3 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY3"]);
												insert.KEY4 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY4"]);
												insert.KEY5 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY5"]);
												insert.KEY6 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY6"]);
												insert.COLUMN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLUMN_NAME"]);
												insert.OLD_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["OLD_VALUE"]);
												insert.NEW_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["NEW_VALUE"]);
												insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
												insert.USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["USER"]);
												insert.HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["HOST"]);
												insert.APPLICATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["APPLICATION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveAudit(DB_AUDIT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_AUDIT", ToBeSaved.ID_AUDIT);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@BATCH_ID", ToBeSaved.BATCH_ID));
            parameters.Add(new DB.InParameter("@CHANGE_TYPE", ToBeSaved.CHANGE_TYPE));
            parameters.Add(new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME));
            parameters.Add(new DB.InParameter("@KEY1", ToBeSaved.KEY1));
            parameters.Add(new DB.InParameter("@KEY2", ToBeSaved.KEY2));
            parameters.Add(new DB.InParameter("@KEY3", ToBeSaved.KEY3));
            parameters.Add(new DB.InParameter("@KEY4", ToBeSaved.KEY4));
            parameters.Add(new DB.InParameter("@KEY5", ToBeSaved.KEY5));
            parameters.Add(new DB.InParameter("@KEY6", ToBeSaved.KEY6));
            parameters.Add(new DB.InParameter("@COLUMN_NAME", ToBeSaved.COLUMN_NAME));
            parameters.Add(ParamObject("@OLD_VALUE", ToBeSaved.OLD_VALUE, 0, 0));
            parameters.Add(ParamObject("@NEW_VALUE", ToBeSaved.NEW_VALUE, 0, 0));
            parameters.Add(new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME)));
            parameters.Add(new DB.InParameter("@USER", ToBeSaved.USER));
            parameters.Add(new DB.InParameter("@HOST", ToBeSaved.HOST));
            parameters.Add(new DB.InParameter("@APPLICATION", ToBeSaved.APPLICATION));
            if (IsParameterDefined("imrse_SaveAudit", "@CONTEXTINFO", this))
                parameters.Add(new DB.InParameter("@CONTEXTINFO", ToBeSaved.CONTEXTINFO));
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveAudit",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_AUDIT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteAudit(DB_AUDIT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelAudit",
                new DB.Parameter[] {
            new DB.InParameter("@ID_AUDIT", toBeDeleted.ID_AUDIT)			
		}
            );
        }

        #endregion

        #region Table CODE

        public DB_CODE[] GetCode()
        {
            return (DB_CODE[])DB.ExecuteProcedure(
                "imrse_GetCode",
                new DB.AnalyzeDataSet(GetCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CODE", new long[] {})
					}
            );
        }

        public DB_CODE[] GetCode(long[] Ids)
        {
            return (DB_CODE[])DB.ExecuteProcedure(
                "imrse_GetCode",
                new DB.AnalyzeDataSet(GetCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CODE", Ids)
					}
            );
        }

        private object GetCode(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CODE[] list = new DB_CODE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CODE insert = new DB_CODE();
                insert.ID_CODE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CODE"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_CODE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CODE_TYPE"]);
                insert.CODE_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["CODE_NBR"]);
                insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                insert.ACTIVATION_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["ACTIVATION_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveCode(DB_CODE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CODE", ToBeSaved.ID_CODE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveCode",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_CODE_TYPE", ToBeSaved.ID_CODE_TYPE)
														,new DB.InParameter("@CODE_NBR", ToBeSaved.CODE_NBR)
														,new DB.InParameter("@CODE", ToBeSaved.CODE)
														,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
														,new DB.InParameter("@ACTIVATION_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.ACTIVATION_TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CODE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteCode(DB_CODE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelCode",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CODE", toBeDeleted.ID_CODE)			
		}
            );
        }

        #endregion
        #region Table CODE_TYPE

        public DB_CODE_TYPE[] GetCodeType()
        {
            return (DB_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCodeType",
                new DB.AnalyzeDataSet(GetCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CODE_TYPE", new int[] {})
					}
            );
        }

        public DB_CODE_TYPE[] GetCodeType(int[] Ids)
        {
            return (DB_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCodeType",
                new DB.AnalyzeDataSet(GetCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CODE_TYPE", Ids)
					}
            );
        }

        private object GetCodeType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CODE_TYPE[] list = new DB_CODE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CODE_TYPE insert = new DB_CODE_TYPE();
                insert.ID_CODE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CODE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_DATA_TYPE_FORMAT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_FORMAT"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveCodeType(DB_CODE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CODE_TYPE", ToBeSaved.ID_CODE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveCodeType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_DATA_TYPE_FORMAT", ToBeSaved.ID_DATA_TYPE_FORMAT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CODE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteCodeType(DB_CODE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelCodeType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CODE_TYPE", toBeDeleted.ID_CODE_TYPE)			
		}
            );
        }

        #endregion

        #region Table COMMAND_CODE_PARAM_MAP

        public DB_COMMAND_CODE_PARAM_MAP[] GetCommandCodeParamMap()
        {
            return (DB_COMMAND_CODE_PARAM_MAP[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeParamMap",
                new DB.AnalyzeDataSet(GetCommandCodeParamMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMMAND_CODE_PARAM_MAP", new long[] {})
					}
            );
        }

        public DB_COMMAND_CODE_PARAM_MAP[] GetCommandCodeParamMap(long[] Ids)
        {
            return (DB_COMMAND_CODE_PARAM_MAP[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeParamMap",
                new DB.AnalyzeDataSet(GetCommandCodeParamMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMMAND_CODE_PARAM_MAP", Ids)
					}
            );
        }



        private object GetCommandCodeParamMap(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_COMMAND_CODE_PARAM_MAP[] list = new DB_COMMAND_CODE_PARAM_MAP[count];
            for (int i = 0; i < count; i++)
            {
                DB_COMMAND_CODE_PARAM_MAP insert = new DB_COMMAND_CODE_PARAM_MAP();
                insert.ID_COMMAND_CODE_PARAM_MAP = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_COMMAND_CODE_PARAM_MAP"]);
                insert.ID_COMMAND_CODE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_COMMAND_CODE"]);
                insert.ARG_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["ARG_NBR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.BYTE_OFFSET = GetValue<int>(QueryResult.Tables[0].Rows[i]["BYTE_OFFSET"]);
                insert.BIT_OFFSET = GetValue<int>(QueryResult.Tables[0].Rows[i]["BIT_OFFSET"]);
                insert.LENGTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["LENGTH"]);
                insert.ID_DATA_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CLASS"]);
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);

                insert.ID_PROTOCOL = QueryResult.Tables[0].Columns.Contains("ID_PROTOCOL") ? GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL"]) : null;
                insert.IS_REQUIRED = QueryResult.Tables[0].Columns.Contains("IS_REQUIRED") ? GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_REQUIRED"]) : true;

                list[i] = insert;
            }
            return list;
        }

        public long SaveCommandCodeParamMap(DB_COMMAND_CODE_PARAM_MAP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_COMMAND_CODE_PARAM_MAP", ToBeSaved.ID_COMMAND_CODE_PARAM_MAP);
            List<DB.Parameter> parameters = new List<DB.Parameter>();
            parameters.Add(InsertId);
            parameters.Add(new DB.InParameter("@ID_COMMAND_CODE", ToBeSaved.ID_COMMAND_CODE));
            parameters.Add(new DB.InParameter("@ARG_NBR", ToBeSaved.ARG_NBR));
            parameters.Add(new DB.InParameter("@NAME", ToBeSaved.NAME));
            parameters.Add(new DB.InParameter("@BYTE_OFFSET", ToBeSaved.BYTE_OFFSET));
            parameters.Add(new DB.InParameter("@BIT_OFFSET", ToBeSaved.BIT_OFFSET));
            parameters.Add(new DB.InParameter("@LENGTH", ToBeSaved.LENGTH));
            parameters.Add(new DB.InParameter("@ID_DATA_TYPE_CLASS", ToBeSaved.ID_DATA_TYPE_CLASS));
            parameters.Add(new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT));
            parameters.Add(new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR));
            if (IsParameterDefined("imrse_SaveCommandCodeParamMap", "@ID_PROTOCOL", this))
                parameters.Add(new DB.InParameter("@ID_PROTOCOL", ToBeSaved.ID_PROTOCOL));
            if (IsParameterDefined("imrse_SaveCommandCodeParamMap", "@IS_REQUIRED", this))
                parameters.Add(new DB.InParameter("@IS_REQUIRED", ToBeSaved.IS_REQUIRED));

            DB.ExecuteNonQueryProcedure(
                "imrse_SaveCommandCodeParamMap",
                parameters.ToArray()
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_COMMAND_CODE_PARAM_MAP = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteCommandCodeParamMap(DB_COMMAND_CODE_PARAM_MAP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelCommandCodeParamMap",
                new DB.Parameter[] {
            new DB.InParameter("@ID_COMMAND_CODE_PARAM_MAP", toBeDeleted.ID_COMMAND_CODE_PARAM_MAP)			
		}
            );
        }

        #endregion
        #region Table COMMAND_CODE_TYPE

        public DB_COMMAND_CODE_TYPE[] GetCommandCodeType()
        {
            return (DB_COMMAND_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeType",
                new DB.AnalyzeDataSet(GetCommandCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMMAND_CODE", new long[] {})
					}
            );
        }

        public DB_COMMAND_CODE_TYPE[] GetCommandCodeType(long[] Ids)
        {
            return (DB_COMMAND_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetCommandCodeType",
                new DB.AnalyzeDataSet(GetCommandCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMMAND_CODE", Ids)
					}
            );
        }



        private object GetCommandCodeType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_COMMAND_CODE_TYPE[] list = new DB_COMMAND_CODE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_COMMAND_CODE_TYPE insert = new DB_COMMAND_CODE_TYPE();
                insert.ID_COMMAND_CODE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_COMMAND_CODE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveCommandCodeType(DB_COMMAND_CODE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_COMMAND_CODE", ToBeSaved.ID_COMMAND_CODE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveCommandCodeType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_COMMAND_CODE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteCommandCodeType(DB_COMMAND_CODE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelCommandCodeType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_COMMAND_CODE", toBeDeleted.ID_COMMAND_CODE)			
		}
            );
        }

        #endregion

        #region Table COMPONENT

        public DB_COMPONENT[] GetComponent(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_COMPONENT[])DB.ExecuteProcedure(
                "imrse_GetComponent",
                new DB.AnalyzeDataSet(GetComponent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMPONENT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_COMPONENT[] GetComponent(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_COMPONENT[])DB.ExecuteProcedure(
                "imrse_GetComponent",
                new DB.AnalyzeDataSet(GetComponent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_COMPONENT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetComponent(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_COMPONENT[] list = new DB_COMPONENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_COMPONENT insert = new DB_COMPONENT();
                insert.ID_COMPONENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_COMPONENT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveComponent(DB_COMPONENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_COMPONENT", ToBeSaved.ID_COMPONENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveComponent",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_COMPONENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteComponent(DB_COMPONENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelComponent",
                new DB.Parameter[] {
            new DB.InParameter("@ID_COMPONENT", toBeDeleted.ID_COMPONENT)			
		}
            );
        }

        #endregion

        #region Table CONFIGURATION_PROFILE

        public DB_CONFIGURATION_PROFILE[] GetConfigurationProfile()
        {
            return (DB_CONFIGURATION_PROFILE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfile",
                new DB.AnalyzeDataSet(GetConfigurationProfile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_CONFIGURATION_PROFILE[] GetConfigurationProfile(long[] Ids)
        {
            return (DB_CONFIGURATION_PROFILE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfile",
                new DB.AnalyzeDataSet(GetConfigurationProfile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetConfigurationProfile(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONFIGURATION_PROFILE[] list = new DB_CONFIGURATION_PROFILE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONFIGURATION_PROFILE insert = new DB_CONFIGURATION_PROFILE();
                insert.ID_CONFIGURATION_PROFILE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE"]);
                insert.ID_CONFIGURATION_PROFILE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE_TYPE"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_OPERATOR_CREATED = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATED"]);
                insert.CREATED_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATED_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveConfigurationProfile(DB_CONFIGURATION_PROFILE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONFIGURATION_PROFILE", ToBeSaved.ID_CONFIGURATION_PROFILE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConfigurationProfile",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONFIGURATION_PROFILE_TYPE", ToBeSaved.ID_CONFIGURATION_PROFILE_TYPE)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_OPERATOR_CREATED", ToBeSaved.ID_OPERATOR_CREATED)
														,new DB.InParameter("@CREATED_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.CREATED_TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONFIGURATION_PROFILE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConfigurationProfile(DB_CONFIGURATION_PROFILE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConfigurationProfile",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONFIGURATION_PROFILE", toBeDeleted.ID_CONFIGURATION_PROFILE)			
		}
            );
        }

        #endregion
        #region Table CONFIGURATION_PROFILE_DATA

        public DB_CONFIGURATION_PROFILE_DATA[] GetConfigurationProfileData()
        {
            return (DB_CONFIGURATION_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileData",
                new DB.AnalyzeDataSet(GetConfigurationProfileData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_DATA", new long[] {})
					}
            );
        }

        public DB_CONFIGURATION_PROFILE_DATA[] GetConfigurationProfileData(long[] Ids)
        {
            return (DB_CONFIGURATION_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileData",
                new DB.AnalyzeDataSet(GetConfigurationProfileData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_DATA", Ids)
					}
            );
        }



        private object GetConfigurationProfileData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONFIGURATION_PROFILE_DATA[] list = new DB_CONFIGURATION_PROFILE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONFIGURATION_PROFILE_DATA insert = new DB_CONFIGURATION_PROFILE_DATA();
                insert.ID_CONFIGURATION_PROFILE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE_DATA"]);
                insert.ID_CONFIGURATION_PROFILE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DEVICE_TYPE_PROFILE_STEP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConfigurationProfileData(DB_CONFIGURATION_PROFILE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONFIGURATION_PROFILE_DATA", ToBeSaved.ID_CONFIGURATION_PROFILE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConfigurationProfileData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONFIGURATION_PROFILE", ToBeSaved.ID_CONFIGURATION_PROFILE)
														,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
                                                        ,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONFIGURATION_PROFILE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConfigurationProfileData(DB_CONFIGURATION_PROFILE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConfigurationProfileData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONFIGURATION_PROFILE_DATA", toBeDeleted.ID_CONFIGURATION_PROFILE_DATA)			
		}
            );
        }

        #endregion
        #region Table CONFIGURATION_PROFILE_DEVICE_TYPE

        public DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] GetConfigurationProfileDeviceType()
        {
            return (DB_CONFIGURATION_PROFILE_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileDeviceType",
                new DB.AnalyzeDataSet(GetConfigurationProfileDeviceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE", new long[] {})
					}
            );
        }

        public DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] GetConfigurationProfileDeviceType(long[] Ids)
        {
            return (DB_CONFIGURATION_PROFILE_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileDeviceType",
                new DB.AnalyzeDataSet(GetConfigurationProfileDeviceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE", Ids)
					}
            );
        }



        private object GetConfigurationProfileDeviceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] list = new DB_CONFIGURATION_PROFILE_DEVICE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONFIGURATION_PROFILE_DEVICE_TYPE insert = new DB_CONFIGURATION_PROFILE_DEVICE_TYPE();
                insert.ID_CONFIGURATION_PROFILE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConfigurationProfileDeviceType(DB_CONFIGURATION_PROFILE_DEVICE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONFIGURATION_PROFILE", ToBeSaved.ID_CONFIGURATION_PROFILE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConfigurationProfileDeviceType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONFIGURATION_PROFILE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConfigurationProfileDeviceType(DB_CONFIGURATION_PROFILE_DEVICE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConfigurationProfileDeviceType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONFIGURATION_PROFILE", toBeDeleted.ID_CONFIGURATION_PROFILE)			
            ,new DB.InParameter("@ID_DEVICE_TYPE", toBeDeleted.ID_DEVICE_TYPE)			
		}
            );
        }

        #endregion
        #region Table CONFIGURATION_PROFILE_TYPE

        public DB_CONFIGURATION_PROFILE_TYPE[] GetConfigurationProfileType()
        {
            return (DB_CONFIGURATION_PROFILE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileType",
                new DB.AnalyzeDataSet(GetConfigurationProfileType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", new int[] {})
					}
            );
        }

        public DB_CONFIGURATION_PROFILE_TYPE[] GetConfigurationProfileType(int[] Ids)
        {
            return (DB_CONFIGURATION_PROFILE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileType",
                new DB.AnalyzeDataSet(GetConfigurationProfileType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", Ids)
					}
            );
        }



        private object GetConfigurationProfileType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONFIGURATION_PROFILE_TYPE[] list = new DB_CONFIGURATION_PROFILE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONFIGURATION_PROFILE_TYPE insert = new DB_CONFIGURATION_PROFILE_TYPE();
                insert.ID_CONFIGURATION_PROFILE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveConfigurationProfileType(DB_CONFIGURATION_PROFILE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONFIGURATION_PROFILE_TYPE", ToBeSaved.ID_CONFIGURATION_PROFILE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConfigurationProfileType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONFIGURATION_PROFILE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConfigurationProfileType(DB_CONFIGURATION_PROFILE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConfigurationProfileType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONFIGURATION_PROFILE_TYPE", toBeDeleted.ID_CONFIGURATION_PROFILE_TYPE)			
		}
            );
        }

        #endregion
        #region Table CONFIGURATION_PROFILE_TYPE_STEP

        public DB_CONFIGURATION_PROFILE_TYPE_STEP[] GetConfigurationProfileTypeStep()
        {
            return (DB_CONFIGURATION_PROFILE_TYPE_STEP[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileTypeStep",
                new DB.AnalyzeDataSet(GetConfigurationProfileTypeStep),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", new int[] {})
					}
            );
        }

        public DB_CONFIGURATION_PROFILE_TYPE_STEP[] GetConfigurationProfileTypeStep(int[] Ids)
        {
            return (DB_CONFIGURATION_PROFILE_TYPE_STEP[])DB.ExecuteProcedure(
                "imrse_GetConfigurationProfileTypeStep",
                new DB.AnalyzeDataSet(GetConfigurationProfileTypeStep),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONFIGURATION_PROFILE_TYPE", Ids)
					}
            );
        }



        private object GetConfigurationProfileTypeStep(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONFIGURATION_PROFILE_TYPE_STEP[] list = new DB_CONFIGURATION_PROFILE_TYPE_STEP[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONFIGURATION_PROFILE_TYPE_STEP insert = new DB_CONFIGURATION_PROFILE_TYPE_STEP();
                insert.ID_CONFIGURATION_PROFILE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION_PROFILE_TYPE"]);
                insert.ID_DEVICE_TYPE_PROFILE_STEP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveConfigurationProfileTypeStep(DB_CONFIGURATION_PROFILE_TYPE_STEP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONFIGURATION_PROFILE_TYPE", ToBeSaved.ID_CONFIGURATION_PROFILE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConfigurationProfileTypeStep",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONFIGURATION_PROFILE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConfigurationProfileTypeStep(DB_CONFIGURATION_PROFILE_TYPE_STEP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConfigurationProfileTypeStep",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONFIGURATION_PROFILE_TYPE", toBeDeleted.ID_CONFIGURATION_PROFILE_TYPE)			
            ,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", toBeDeleted.ID_DEVICE_TYPE_PROFILE_STEP)			
		}
            );
        }

        #endregion

        #region Table CONSUMER

        public DB_CONSUMER[] GetConsumer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER[])DB.ExecuteProcedure(
                "imrse_GetConsumer",
                new DB.AnalyzeDataSet(GetConsumer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_CONSUMER[] GetConsumer(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER[])DB.ExecuteProcedure(
                "imrse_GetConsumer",
                new DB.AnalyzeDataSet(GetConsumer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetConsumer(DataSet QueryResult)
        {
            bool isBlockedColumn = QueryResult.Tables[0].Columns.Contains("IS_BLOCKED");
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER[] list = new DB_CONSUMER[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER insert = new DB_CONSUMER();
                insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
                insert.ID_CONSUMER_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE"]);
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                if (isBlockedColumn)
                    insert.IS_BLOCKED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_BLOCKED"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveConsumer(DB_CONSUMER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER);
            
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumer",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER_TYPE", ToBeSaved.ID_CONSUMER_TYPE)
														,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
                                                        ,new DB.InParameter("@IS_BLOCKED", ToBeSaved.IS_BLOCKED)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConsumer(DB_CONSUMER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumer",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER", toBeDeleted.ID_CONSUMER)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_DATA

        public DB_CONSUMER_DATA[] GetConsumerData()
        {
            return (DB_CONSUMER_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerData",
                new DB.AnalyzeDataSet(GetConsumerData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_DATA", new long[] {})
					}
            );
        }

        public DB_CONSUMER_DATA[] GetConsumerData(long[] Ids)
        {
            return (DB_CONSUMER_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerData",
                new DB.AnalyzeDataSet(GetConsumerData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_DATA", Ids)
					}
            );
        }

        private object GetConsumerData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER_DATA[] list = new DB_CONSUMER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_DATA insert = new DB_CONSUMER_DATA();
                insert.ID_CONSUMER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_DATA"]);
                insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConsumerData(DB_CONSUMER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_DATA", ToBeSaved.ID_CONSUMER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerData(DB_CONSUMER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_DATA", toBeDeleted.ID_CONSUMER_DATA)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_NOTIFICATION

        public DB_CONSUMER_NOTIFICATION[] GetConsumerNotification(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotification",
                new DB.AnalyzeDataSet(GetConsumerNotification),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_NOTIFICATION[] GetConsumerNotification(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotification",
                new DB.AnalyzeDataSet(GetConsumerNotification),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerNotification(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_NOTIFICATION()
            {
                ID_CONSUMER_NOTIFICATION = GetValue<long>(row["ID_CONSUMER_NOTIFICATION"]),
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(row["ID_NOTIFICATION_DELIVERY_TYPE"]),
                ID_OPERATOR = GetValue<int>(row["ID_OPERATOR"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_NOTIFICATION[] list = new DB_CONSUMER_NOTIFICATION[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_NOTIFICATION insert = new DB_CONSUMER_NOTIFICATION();
									insert.ID_CONSUMER_NOTIFICATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_NOTIFICATION"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_DELIVERY_TYPE"]);
												insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerNotification(DB_CONSUMER_NOTIFICATION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_NOTIFICATION", ToBeSaved.ID_CONSUMER_NOTIFICATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerNotification",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_NOTIFICATION_DELIVERY_TYPE", ToBeSaved.ID_NOTIFICATION_DELIVERY_TYPE)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_NOTIFICATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerNotification(DB_CONSUMER_NOTIFICATION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerNotification",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_NOTIFICATION", toBeDeleted.ID_CONSUMER_NOTIFICATION)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_NOTIFICATION_HISTORY

        public DB_CONSUMER_NOTIFICATION_HISTORY[] GetConsumerNotificationHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotificationHistory",
                new DB.AnalyzeDataSet(GetConsumerNotificationHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION_HISTORY", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_NOTIFICATION_HISTORY[] GetConsumerNotificationHistory(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotificationHistory",
                new DB.AnalyzeDataSet(GetConsumerNotificationHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION_HISTORY", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerNotificationHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_NOTIFICATION_HISTORY()
            {
                ID_CONSUMER_NOTIFICATION_HISTORY = GetValue<long>(row["ID_CONSUMER_NOTIFICATION_HISTORY"]),
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(row["ID_NOTIFICATION_DELIVERY_TYPE"]),
                ID_NOTIFICATION_TYPE = GetValue<int>(row["ID_NOTIFICATION_TYPE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                BODY = GetValue<string>(row["BODY"]),
                DELIVERY_ADDRESS = GetValue<string>(row["DELIVERY_ADDRESS"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_NOTIFICATION_HISTORY[] list = new DB_CONSUMER_NOTIFICATION_HISTORY[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_NOTIFICATION_HISTORY insert = new DB_CONSUMER_NOTIFICATION_HISTORY();
									insert.ID_CONSUMER_NOTIFICATION_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_NOTIFICATION_HISTORY"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_DELIVERY_TYPE"]);
												insert.ID_NOTIFICATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_TYPE"]);
												insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
												insert.BODY = GetValue<string>(QueryResult.Tables[0].Rows[i]["BODY"]);
												insert.DELIVERY_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["DELIVERY_ADDRESS"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerNotificationHistory(DB_CONSUMER_NOTIFICATION_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_NOTIFICATION_HISTORY", ToBeSaved.ID_CONSUMER_NOTIFICATION_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerNotificationHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_NOTIFICATION_DELIVERY_TYPE", ToBeSaved.ID_NOTIFICATION_DELIVERY_TYPE)
														,new DB.InParameter("@ID_NOTIFICATION_TYPE", ToBeSaved.ID_NOTIFICATION_TYPE)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
													,new DB.InParameter("@BODY", ToBeSaved.BODY)
														,new DB.InParameter("@DELIVERY_ADDRESS", ToBeSaved.DELIVERY_ADDRESS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_NOTIFICATION_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerNotificationHistory(DB_CONSUMER_NOTIFICATION_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerNotificationHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_NOTIFICATION_HISTORY", toBeDeleted.ID_CONSUMER_NOTIFICATION_HISTORY)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_NOTIFICATION_TYPE_DATA

        public DB_CONSUMER_NOTIFICATION_TYPE_DATA[] GetConsumerNotificationTypeData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotificationTypeData",
                new DB.AnalyzeDataSet(GetConsumerNotificationTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION_TYPE_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_NOTIFICATION_TYPE_DATA[] GetConsumerNotificationTypeData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_NOTIFICATION_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerNotificationTypeData",
                new DB.AnalyzeDataSet(GetConsumerNotificationTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_NOTIFICATION_TYPE_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerNotificationTypeData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_NOTIFICATION_TYPE_DATA()
            {
                ID_CONSUMER_NOTIFICATION_TYPE_DATA = GetValue<long>(row["ID_CONSUMER_NOTIFICATION_TYPE_DATA"]),
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_NOTIFICATION_TYPE = GetValue<int>(row["ID_NOTIFICATION_TYPE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_NOTIFICATION_TYPE_DATA[] list = new DB_CONSUMER_NOTIFICATION_TYPE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_NOTIFICATION_TYPE_DATA insert = new DB_CONSUMER_NOTIFICATION_TYPE_DATA();
									insert.ID_CONSUMER_NOTIFICATION_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_NOTIFICATION_TYPE_DATA"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_NOTIFICATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_TYPE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerNotificationTypeData(DB_CONSUMER_NOTIFICATION_TYPE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_NOTIFICATION_TYPE_DATA", ToBeSaved.ID_CONSUMER_NOTIFICATION_TYPE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerNotificationTypeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_NOTIFICATION_TYPE", ToBeSaved.ID_NOTIFICATION_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_NOTIFICATION_TYPE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerNotificationTypeData(DB_CONSUMER_NOTIFICATION_TYPE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerNotificationTypeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_NOTIFICATION_TYPE_DATA", toBeDeleted.ID_CONSUMER_NOTIFICATION_TYPE_DATA)			
		}
            );
        }

        #endregion

        #region Table CONSUMER_PAYMENT_MODULE

        public DB_CONSUMER_PAYMENT_MODULE[] GetConsumerPaymentModule(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_PAYMENT_MODULE[])DB.ExecuteProcedure(
                "imrse_GetConsumerPaymentModule",
                new DB.AnalyzeDataSet(GetConsumerPaymentModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_PAYMENT_MODULE", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_PAYMENT_MODULE[] GetConsumerPaymentModule(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_PAYMENT_MODULE[])DB.ExecuteProcedure(
                "imrse_GetConsumerPaymentModule",
                new DB.AnalyzeDataSet(GetConsumerPaymentModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_PAYMENT_MODULE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerPaymentModule(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_PAYMENT_MODULE()
            {
                ID_CONSUMER_PAYMENT_MODULE = GetValue<long>(row["ID_CONSUMER_PAYMENT_MODULE"]),
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_PAYMENT_MODULE = GetValue<int>(row["ID_PAYMENT_MODULE"]),
                PAYMENT_SYSTEM_NUMBER = GetValue<string>(row["PAYMENT_SYSTEM_NUMBER"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_PAYMENT_MODULE[] list = new DB_CONSUMER_PAYMENT_MODULE[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_PAYMENT_MODULE insert = new DB_CONSUMER_PAYMENT_MODULE();
									insert.ID_CONSUMER_PAYMENT_MODULE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_PAYMENT_MODULE"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_PAYMENT_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE"]);
												insert.PAYMENT_SYSTEM_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["PAYMENT_SYSTEM_NUMBER"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerPaymentModule(DB_CONSUMER_PAYMENT_MODULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_PAYMENT_MODULE", ToBeSaved.ID_CONSUMER_PAYMENT_MODULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerPaymentModule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_PAYMENT_MODULE", ToBeSaved.ID_PAYMENT_MODULE)
														,new DB.InParameter("@PAYMENT_SYSTEM_NUMBER", ToBeSaved.PAYMENT_SYSTEM_NUMBER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_PAYMENT_MODULE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerPaymentModule(DB_CONSUMER_PAYMENT_MODULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerPaymentModule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_PAYMENT_MODULE", toBeDeleted.ID_CONSUMER_PAYMENT_MODULE)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_TYPE_DATA

        public DB_CONSUMER_TYPE_DATA[] GetConsumerTypeData()
        {
            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTypeData",
                new DB.AnalyzeDataSet(GetConsumerTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TYPE_DATA", new long[] {})
					}
            );
        }

        public DB_CONSUMER_TYPE_DATA[] GetConsumerTypeData(long[] Ids)
        {
            return (DB_CONSUMER_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTypeData",
                new DB.AnalyzeDataSet(GetConsumerTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TYPE_DATA", Ids)
					}
            );
        }



        private object GetConsumerTypeData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER_TYPE_DATA[] list = new DB_CONSUMER_TYPE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_TYPE_DATA insert = new DB_CONSUMER_TYPE_DATA();
                insert.ID_CONSUMER_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE_DATA"]);
                insert.ID_CONSUMER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveConsumerTypeData(DB_CONSUMER_TYPE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TYPE_DATA", ToBeSaved.ID_CONSUMER_TYPE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerTypeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER_TYPE", ToBeSaved.ID_CONSUMER_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TYPE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerTypeData(DB_CONSUMER_TYPE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTypeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TYPE_DATA", toBeDeleted.ID_CONSUMER_TYPE_DATA)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_TRANSACTION

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransaction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_TRANSACTION[] GetConsumerTransaction(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransaction",
                new DB.AnalyzeDataSet(GetConsumerTransaction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerTransaction(DataSet QueryResult)
{
	return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_TRANSACTION()
	{
		ID_CONSUMER_TRANSACTION = GetValue<long>(row["ID_CONSUMER_TRANSACTION"]),
		ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
		ID_OPERATOR = GetNullableValue<int>(row["ID_OPERATOR"]),
		ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
		ID_CONSUMER_TRANSACTION_TYPE = GetValue<int>(row["ID_CONSUMER_TRANSACTION_TYPE"]),
		ID_TARIFF = GetNullableValue<int>(row["ID_TARIFF"]),
		TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
		CHARGE_PREPAID = GetValue<Decimal>(row["CHARGE_PREPAID"]),
        CHARGE_EC = GetValue<Decimal>(row["CHARGE_EC"]),
        CHARGE_OWED = GetValue<Decimal>(row["CHARGE_OWED"]),
        BALANCE_PREPAID = GetValue<Decimal>(row["BALANCE_PREPAID"]),
        BALANCE_EC = GetValue<Decimal>(row["BALANCE_EC"]),
        BALANCE_OWED = GetValue<Decimal>(row["BALANCE_OWED"]),
                METER_INDEX = GetNullableValue<Double>(row["METER_INDEX"]),
		ID_CONSUMER_SETTLEMENT = GetNullableValue<long>(row["ID_CONSUMER_SETTLEMENT"]),
		ID_PAYMENT_MODULE = GetNullableValue<int>(row["ID_PAYMENT_MODULE"]),
		TRANSACTION_GUID = GetValue<string>(row["TRANSACTION_GUID"]), 
		ID_PACKET = GetNullableValue<long>(row["ID_PACKET"]),
		ID_ACTION = GetNullableValue<long>(row["ID_ACTION"]),
		ID_PREVIOUS_CONSUMER_TRANSACTION = GetNullableValue<long>(row["ID_PREVIOUS_CONSUMER_TRANSACTION"]),
	}).ToArray();
	#region Wersja z pętlą
	/*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_TRANSACTION[] list = new DB_CONSUMER_TRANSACTION[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_TRANSACTION insert = new DB_CONSUMER_TRANSACTION();
									insert.ID_CONSUMER_TRANSACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
												insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
												insert.ID_CONSUMER_TRANSACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION_TYPE"]);
												insert.ID_TARIFF = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
												insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
												        insert.CHARGE_PREPAID = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["CHARGE_PREPAID"]);
												        insert.CHARGE_EC = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["CHARGE_EC"]);
												        insert.CHARGE_OWED = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["CHARGE_OWED"]);
												        insert.BALANCE_PREPAID = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["BALANCE_PREPAID"]);
												        insert.BALANCE_EC = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["BALANCE_EC"]);
												        insert.BALANCE_OWED = GetValue<Decimal>(QueryResult.Tables[0].Rows[i]["BALANCE_OWED"]);
												        insert.METER_INDEX = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["METER_INDEX"]);
												insert.ID_CONSUMER_SETTLEMENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT"]);
												insert.ID_PAYMENT_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE"]);
												insert.TRANSACTION_GUID = GetValue<string>(QueryResult.Tables[0].Rows[i]["TRANSACTION_GUID"]);
												insert.ID_PACKET = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
												insert.ID_ACTION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ACTION"]);
												insert.ID_PREVIOUS_ CONSUMER_TRANSACTION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_PREVIOUS_ CONSUMER_TRANSACTION"]);
							list[i] = insert;
    }
    return list;
	*/
	#endregion
}

        public long SaveConsumerTransaction(DB_CONSUMER_TRANSACTION ToBeSaved)
{
	DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TRANSACTION", ToBeSaved.ID_CONSUMER_TRANSACTION);
    DB.ExecuteNonQueryProcedure(
        "imrse_SaveConsumerTransaction",
                new DB.Parameter[] 
                {
			InsertId
			,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
			,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
			,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
			,new DB.InParameter("@ID_CONSUMER_TRANSACTION_TYPE", ToBeSaved.ID_CONSUMER_TRANSACTION_TYPE)
			,new DB.InParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF)
			,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
			,new DB.InParameter("@CHARGE_PREPAID", SqlDbType.Decimal, ToBeSaved.CHARGE_PREPAID)
			,new DB.InParameter("@CHARGE_EC", SqlDbType.Decimal, ToBeSaved.CHARGE_EC)
			,new DB.InParameter("@CHARGE_OWED", SqlDbType.Decimal, ToBeSaved.CHARGE_OWED)
			,new DB.InParameter("@BALANCE_PREPAID", SqlDbType.Decimal, ToBeSaved.BALANCE_PREPAID)
			,new DB.InParameter("@BALANCE_EC", SqlDbType.Decimal, ToBeSaved.BALANCE_EC)
			,new DB.InParameter("@BALANCE_OWED", SqlDbType.Decimal, ToBeSaved.BALANCE_OWED)
					,new DB.InParameter("@METER_INDEX", ToBeSaved.METER_INDEX)
			,new DB.InParameter("@ID_CONSUMER_SETTLEMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT)
			,new DB.InParameter("@ID_PAYMENT_MODULE", ToBeSaved.ID_PAYMENT_MODULE)
			,new DB.InParameter("@TRANSACTION_GUID", ToBeSaved.TRANSACTION_GUID)
			,new DB.InParameter("@ID_PACKET", ToBeSaved.ID_PACKET)
			,new DB.InParameter("@ID_ACTION", ToBeSaved.ID_ACTION)
			,new DB.InParameter("@ID_PREVIOUS_CONSUMER_TRANSACTION", ToBeSaved.ID_PREVIOUS_CONSUMER_TRANSACTION)
		}
    );
	if (!InsertId.IsNull)
		ToBeSaved.ID_CONSUMER_TRANSACTION = (long)InsertId.Value;
	
	return (long)InsertId.Value;
}

        public void DeleteConsumerTransaction(DB_CONSUMER_TRANSACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTransaction",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TRANSACTION", toBeDeleted.ID_CONSUMER_TRANSACTION)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_TRANSACTION_DATA

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionData",
                new DB.AnalyzeDataSet(GetConsumerTransactionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_TRANSACTION_DATA[] GetConsumerTransactionData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionData",
                new DB.AnalyzeDataSet(GetConsumerTransactionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetConsumerTransactionData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_TRANSACTION_DATA()
            {
                ID_CONSUMER_TRANSACTION_DATA = GetValue<long>(row["ID_CONSUMER_TRANSACTION_DATA"]),
                ID_CONSUMER_TRANSACTION = GetValue<long>(row["ID_CONSUMER_TRANSACTION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_TRANSACTION_DATA[] list = new DB_CONSUMER_TRANSACTION_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_TRANSACTION_DATA insert = new DB_CONSUMER_TRANSACTION_DATA();
									insert.ID_CONSUMER_TRANSACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION_DATA"]);
												insert.ID_CONSUMER_TRANSACTION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerTransactionData(DB_CONSUMER_TRANSACTION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TRANSACTION_DATA", ToBeSaved.ID_CONSUMER_TRANSACTION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerTransactionData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER_TRANSACTION", ToBeSaved.ID_CONSUMER_TRANSACTION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TRANSACTION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerTransactionData(DB_CONSUMER_TRANSACTION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTransactionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TRANSACTION_DATA", toBeDeleted.ID_CONSUMER_TRANSACTION_DATA)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_TRANSACTION_TYPE

        public DB_CONSUMER_TRANSACTION_TYPE[] GetConsumerTransactionType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionType",
                new DB.AnalyzeDataSet(GetConsumerTransactionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_TRANSACTION_TYPE[] GetConsumerTransactionType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_TRANSACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerTransactionType",
                new DB.AnalyzeDataSet(GetConsumerTransactionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_TRANSACTION_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetConsumerTransactionType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONSUMER_TRANSACTION_TYPE[] list = new DB_CONSUMER_TRANSACTION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONSUMER_TRANSACTION_TYPE insert = new DB_CONSUMER_TRANSACTION_TYPE();
                insert.ID_CONSUMER_TRANSACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_TRANSACTION_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int? SaveConsumerTransactionType(DB_CONSUMER_TRANSACTION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_TRANSACTION_TYPE", ToBeSaved.ID_CONSUMER_TRANSACTION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerTransactionType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_TRANSACTION_TYPE = (int)InsertId.Value;

            return (int?)InsertId.Value;
        }

        public void DeleteConsumerTransactionType(DB_CONSUMER_TRANSACTION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerTransactionType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_TRANSACTION_TYPE", toBeDeleted.ID_CONSUMER_TRANSACTION_TYPE)			
		}
            );
        }

        #endregion

        #region Table CONSUMER_SETTLEMENT

        public DB_CONSUMER_SETTLEMENT[] GetConsumerSettlement(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlement",
                new DB.AnalyzeDataSet(GetConsumerSettlement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_SETTLEMENT[] GetConsumerSettlement(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlement",
                new DB.AnalyzeDataSet(GetConsumerSettlement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetConsumerSettlement(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT()
            {
                ID_CONSUMER_SETTLEMENT = GetValue<long>(row["ID_CONSUMER_SETTLEMENT"]),
                ID_CONSUMER = GetValue<int>(row["ID_CONSUMER"]),
                ID_TARIFF_SETTLEMENT_PERIOD = GetValue<int>(row["ID_TARIFF_SETTLEMENT_PERIOD"]),
                SETTLEMENT_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["SETTLEMENT_TIME"])),
                ID_MODULE = GetValue<int>(row["ID_MODULE"]),
                ID_OPERATOR = GetNullableValue<int>(row["ID_OPERATOR"]),
                ID_CONSUMER_SETTLEMENT_REASON = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_REASON"]),
                ID_CONSUMER_SETTLEMENT_STATUS = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_STATUS"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT[] list = new DB_CONSUMER_SETTLEMENT[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT insert = new DB_CONSUMER_SETTLEMENT();
									insert.ID_CONSUMER_SETTLEMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT"]);
												insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
												insert.ID_TARIFF_SETTLEMENT_PERIOD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF_SETTLEMENT_PERIOD"]);
												insert.SETTLEMENT_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["SETTLEMENT_TIME"]));
												insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
												insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
												insert.ID_CONSUMER_SETTLEMENT_REASON = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_REASON"]);
												insert.ID_CONSUMER_SETTLEMENT_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_STATUS"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerSettlement(DB_CONSUMER_SETTLEMENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlement",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_TARIFF_SETTLEMENT_PERIOD", ToBeSaved.ID_TARIFF_SETTLEMENT_PERIOD)
														,new DB.InParameter("@SETTLEMENT_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.SETTLEMENT_TIME))
													,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_CONSUMER_SETTLEMENT_REASON", ToBeSaved.ID_CONSUMER_SETTLEMENT_REASON)
														,new DB.InParameter("@ID_CONSUMER_SETTLEMENT_STATUS", ToBeSaved.ID_CONSUMER_SETTLEMENT_STATUS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerSettlement(DB_CONSUMER_SETTLEMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlement",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT", toBeDeleted.ID_CONSUMER_SETTLEMENT)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_SETTLEMENT_DOCUMENT

        public DB_CONSUMER_SETTLEMENT_DOCUMENT[] GetConsumerSettlementDocument()
        {
            return (DB_CONSUMER_SETTLEMENT_DOCUMENT[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementDocument",
                new DB.AnalyzeDataSet(GetConsumerSettlementDocument),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_DOCUMENT", new long[] {})
					}
            );
        }

        public DB_CONSUMER_SETTLEMENT_DOCUMENT[] GetConsumerSettlementDocument(long[] Ids)
        {
            return (DB_CONSUMER_SETTLEMENT_DOCUMENT[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementDocument",
                new DB.AnalyzeDataSet(GetConsumerSettlementDocument),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_DOCUMENT", Ids)
					}
            );
        }



        private object GetConsumerSettlementDocument(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT_DOCUMENT()
            {
                ID_CONSUMER_SETTLEMENT_DOCUMENT = GetValue<long>(row["ID_CONSUMER_SETTLEMENT_DOCUMENT"]),
                ID_CONSUMER_SETTLEMENT = GetValue<long>(row["ID_CONSUMER_SETTLEMENT"]),
                ID_SETTLEMENT_DOCUMENT_TYPE = GetValue<int>(row["ID_SETTLEMENT_DOCUMENT_TYPE"]),
                METADATA = GetValue<string>(row["METADATA"]),
                DOCUMENT_LINK = (byte[])GetValue(row["DOCUMENT_LINK"]),
                DOCUMENT_GUID = ConvertToNullableGuid(row["DOCUMENT_GUID"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT_DOCUMENT[] list = new DB_CONSUMER_SETTLEMENT_DOCUMENT[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT_DOCUMENT insert = new DB_CONSUMER_SETTLEMENT_DOCUMENT();
									insert.ID_CONSUMER_SETTLEMENT_DOCUMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_DOCUMENT"]);
												insert.ID_CONSUMER_SETTLEMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT"]);
												insert.ID_SETTLEMENT_DOCUMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SETTLEMENT_DOCUMENT_TYPE"]);
												insert.METADATA = GetValue<undefined>(QueryResult.Tables[0].Rows[i]["METADATA"]);
												insert.DOCUMENT_LINK = (byte[])GetValue(QueryResult.Tables[0].Rows[i]["DOCUMENT_LINK"]);
												insert.DOCUMENT_GUID = ConvertToNullableGuid(QueryResult.Tables[0].Rows[i]["DOCUMENT_GUID"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveConsumerSettlementDocument(DB_CONSUMER_SETTLEMENT_DOCUMENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT_DOCUMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT_DOCUMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlementDocument",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER_SETTLEMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT)
														,new DB.InParameter("@ID_SETTLEMENT_DOCUMENT_TYPE", ToBeSaved.ID_SETTLEMENT_DOCUMENT_TYPE)
														,new DB.InParameter("@METADATA", ToBeSaved.METADATA)
														,new DB.InParameter("@DOCUMENT_LINK", ToBeSaved.DOCUMENT_LINK)
														,new DB.InParameter("@DOCUMENT_GUID", ToBeSaved.DOCUMENT_GUID.Value)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT_DOCUMENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConsumerSettlementDocument(DB_CONSUMER_SETTLEMENT_DOCUMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlementDocument",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT_DOCUMENT", toBeDeleted.ID_CONSUMER_SETTLEMENT_DOCUMENT)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_SETTLEMENT_ERROR

        public DB_CONSUMER_SETTLEMENT_ERROR[] GetConsumerSettlementError()
        {
            return (DB_CONSUMER_SETTLEMENT_ERROR[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementError",
                new DB.AnalyzeDataSet(GetConsumerSettlementError),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR", new int[] {})
					}
            );
        }

        public DB_CONSUMER_SETTLEMENT_ERROR[] GetConsumerSettlementError(int[] Ids)
        {
            return (DB_CONSUMER_SETTLEMENT_ERROR[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementError",
                new DB.AnalyzeDataSet(GetConsumerSettlementError),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR", Ids)
					}
            );
        }



        private object GetConsumerSettlementError(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT_ERROR()
            {
                ID_CONSUMER_SETTLEMENT_ERROR = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_ERROR"]),
                ID_CONSUMER_SETTLEMENT = GetValue<long>(row["ID_CONSUMER_SETTLEMENT"]),
                ID_CONSUMER_SETTLEMENT_ERROR_TYPE = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_ERROR_TYPE"]),
                DESCRIPTION = GetValue<string>(row["DESCRIPTION"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT_ERROR[] list = new DB_CONSUMER_SETTLEMENT_ERROR[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT_ERROR insert = new DB_CONSUMER_SETTLEMENT_ERROR();
									insert.ID_CONSUMER_SETTLEMENT_ERROR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_ERROR"]);
												insert.ID_CONSUMER_SETTLEMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT"]);
												insert.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_ERROR_TYPE"]);
												insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveConsumerSettlementError(DB_CONSUMER_SETTLEMENT_ERROR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT_ERROR", ToBeSaved.ID_CONSUMER_SETTLEMENT_ERROR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlementError",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONSUMER_SETTLEMENT", ToBeSaved.ID_CONSUMER_SETTLEMENT)
														,new DB.InParameter("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", ToBeSaved.ID_CONSUMER_SETTLEMENT_ERROR_TYPE)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT_ERROR = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConsumerSettlementError(DB_CONSUMER_SETTLEMENT_ERROR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlementError",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT_ERROR", toBeDeleted.ID_CONSUMER_SETTLEMENT_ERROR)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_SETTLEMENT_ERROR_TYPE

        public DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] GetConsumerSettlementErrorType()
        {
            return (DB_CONSUMER_SETTLEMENT_ERROR_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementErrorType",
                new DB.AnalyzeDataSet(GetConsumerSettlementErrorType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", new int[] {})
					}
            );
        }

        public DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] GetConsumerSettlementErrorType(int[] Ids)
        {
            return (DB_CONSUMER_SETTLEMENT_ERROR_TYPE[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementErrorType",
                new DB.AnalyzeDataSet(GetConsumerSettlementErrorType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", Ids)
					}
            );
        }



        private object GetConsumerSettlementErrorType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT_ERROR_TYPE()
            {
                ID_CONSUMER_SETTLEMENT_ERROR_TYPE = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_ERROR_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] list = new DB_CONSUMER_SETTLEMENT_ERROR_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT_ERROR_TYPE insert = new DB_CONSUMER_SETTLEMENT_ERROR_TYPE();
									insert.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_ERROR_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveConsumerSettlementErrorType(DB_CONSUMER_SETTLEMENT_ERROR_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", ToBeSaved.ID_CONSUMER_SETTLEMENT_ERROR_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlementErrorType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConsumerSettlementErrorType(DB_CONSUMER_SETTLEMENT_ERROR_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlementErrorType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT_ERROR_TYPE", toBeDeleted.ID_CONSUMER_SETTLEMENT_ERROR_TYPE)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_SETTLEMENT_REASON
        public DB_CONSUMER_SETTLEMENT_REASON[] GetConsumerSettlementReason(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT_REASON[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementReason",
                new DB.AnalyzeDataSet(GetConsumerSettlementReason),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_REASON", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_SETTLEMENT_REASON[] GetConsumerSettlementReason(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT_REASON[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementReason",
                new DB.AnalyzeDataSet(GetConsumerSettlementReason),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_REASON", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetConsumerSettlementReason(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT_REASON()
            {
                ID_CONSUMER_SETTLEMENT_REASON = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_REASON"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT_REASON[] list = new DB_CONSUMER_SETTLEMENT_REASON[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT_REASON insert = new DB_CONSUMER_SETTLEMENT_REASON();
									insert.ID_CONSUMER_SETTLEMENT_REASON = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_REASON"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveConsumerSettlementReason(DB_CONSUMER_SETTLEMENT_REASON ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT_REASON", ToBeSaved.ID_CONSUMER_SETTLEMENT_REASON);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlementReason",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT_REASON = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConsumerSettlementReason(DB_CONSUMER_SETTLEMENT_REASON toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlementReason",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT_REASON", toBeDeleted.ID_CONSUMER_SETTLEMENT_REASON)			
		}
            );
        }

        #endregion
        #region Table CONSUMER_SETTLEMENT_STATUS
        public DB_CONSUMER_SETTLEMENT_STATUS[] GetConsumerSettlementStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT_STATUS[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementStatus",
                new DB.AnalyzeDataSet(GetConsumerSettlementStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_STATUS", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONSUMER_SETTLEMENT_STATUS[] GetConsumerSettlementStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONSUMER_SETTLEMENT_STATUS[])DB.ExecuteProcedure(
                "imrse_GetConsumerSettlementStatus",
                new DB.AnalyzeDataSet(GetConsumerSettlementStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONSUMER_SETTLEMENT_STATUS", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetConsumerSettlementStatus(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_CONSUMER_SETTLEMENT_STATUS()
            {
                ID_CONSUMER_SETTLEMENT_STATUS = GetValue<int>(row["ID_CONSUMER_SETTLEMENT_STATUS"]),
                NAME = GetValue<string>(row["NAME"]),
                IS_ERROR = GetValue<bool>(row["IS_ERROR"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_CONSUMER_SETTLEMENT_STATUS[] list = new DB_CONSUMER_SETTLEMENT_STATUS[count];
    for (int i = 0; i < count; i++)
    {
        DB_CONSUMER_SETTLEMENT_STATUS insert = new DB_CONSUMER_SETTLEMENT_STATUS();
									insert.ID_CONSUMER_SETTLEMENT_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER_SETTLEMENT_STATUS"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.IS_ERROR = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ERROR"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveConsumerSettlementStatus(DB_CONSUMER_SETTLEMENT_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONSUMER_SETTLEMENT_STATUS", ToBeSaved.ID_CONSUMER_SETTLEMENT_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConsumerSettlementStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@IS_ERROR", ToBeSaved.IS_ERROR)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONSUMER_SETTLEMENT_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteConsumerSettlementStatus(DB_CONSUMER_SETTLEMENT_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConsumerSettlementStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONSUMER_SETTLEMENT_STATUS", toBeDeleted.ID_CONSUMER_SETTLEMENT_STATUS)			
		}
            );
        }

        #endregion

        #region Table CONTENT

        public DB_CONTENT[] GetContent()
        {
            return (DB_CONTENT[])DB.ExecuteProcedure(
                "imrse_GetContent",
                new DB.AnalyzeDataSet(GetContent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTENT", new int[] {})
					}
            );
        }

        public DB_CONTENT[] GetContent(int[] Ids)
        {
            return (DB_CONTENT[])DB.ExecuteProcedure(
                "imrse_GetContent",
                new DB.AnalyzeDataSet(GetContent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTENT", Ids)
					}
            );
        }



        private object GetContent(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONTENT[] list = new DB_CONTENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONTENT insert = new DB_CONTENT();
                insert.ID_CONTENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTENT"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.CONTENT = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONTENT"]);
                insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveContent(DB_CONTENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONTENT", ToBeSaved.ID_CONTENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveContent",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@CONTENT", ToBeSaved.CONTENT)
														,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONTENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public int SaveContent2(DB_CONTENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONTENT", ToBeSaved.ID_CONTENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveContent2",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@CONTENT", ToBeSaved.CONTENT)
														,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONTENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public int SaveContentByModule(DB_CONTENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONTENT", ToBeSaved.ID_CONTENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveContent",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@CONTENT", ToBeSaved.CONTENT)
														,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONTENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteContent(DB_CONTENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelContent",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONTENT", toBeDeleted.ID_CONTENT)			
		}
            );
        }

        #endregion

        #region Table CONTRACT

        public DB_CONTRACT[] GetContract(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetContract",
                new DB.AnalyzeDataSet(GetContract),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTRACT", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONTRACT[] GetContract(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetContract",
                new DB.AnalyzeDataSet(GetContract),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTRACT", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetContract(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONTRACT[] list = new DB_CONTRACT[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONTRACT insert = new DB_CONTRACT();
                insert.ID_CONTRACT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        public int SaveContract(DB_CONTRACT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONTRACT", ToBeSaved.ID_CONTRACT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveContract",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONTRACT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteContract(DB_CONTRACT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelContract",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONTRACT", toBeDeleted.ID_CONTRACT)			
		}
            );
        }

        #endregion
        #region Table CONTRACT_DATA

        public DB_CONTRACT_DATA[] GetContractData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONTRACT_DATA[])DB.ExecuteProcedure(
                "imrse_GetContractData",
                new DB.AnalyzeDataSet(GetContractData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTRACT_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_CONTRACT_DATA[] GetContractData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_CONTRACT_DATA[])DB.ExecuteProcedure(
                "imrse_GetContractData",
                new DB.AnalyzeDataSet(GetContractData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONTRACT_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetContractData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONTRACT_DATA[] list = new DB_CONTRACT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONTRACT_DATA insert = new DB_CONTRACT_DATA();
                insert.ID_CONTRACT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT_DATA"]);
                insert.ID_CONTRACT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveContractData(DB_CONTRACT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONTRACT_DATA", ToBeSaved.ID_CONTRACT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveContractData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONTRACT", ToBeSaved.ID_CONTRACT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONTRACT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteContractData(DB_CONTRACT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelContractData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CONTRACT_DATA", toBeDeleted.ID_CONTRACT_DATA)			
		}
            );
        }

        #endregion

        #region Table CURRENCY

        public DB_CURRENCY[] GetCurrency()
        {
            return (DB_CURRENCY[])DB.ExecuteProcedure(
                "imrse_GetCurrency",
                new DB.AnalyzeDataSet(GetCurrency),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CURRENCY", new int[] {})
					}
            );
        }

        public DB_CURRENCY[] GetCurrency(int[] Ids)
        {
            return (DB_CURRENCY[])DB.ExecuteProcedure(
                "imrse_GetCurrency",
                new DB.AnalyzeDataSet(GetCurrency),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CURRENCY", Ids)
					}
            );
        }

        private object GetCurrency(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CURRENCY[] list = new DB_CURRENCY[count];
            for (int i = 0; i < count; i++)
            {
                DB_CURRENCY insert = new DB_CURRENCY();
                insert.ID_CURRENCY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CURRENCY"]);
                insert.SYMBOL = GetValue<string>(QueryResult.Tables[0].Rows[i]["SYMBOL"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveCurrency(DB_CURRENCY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CURRENCY", ToBeSaved.ID_CURRENCY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveCurrency",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SYMBOL", ToBeSaved.SYMBOL)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CURRENCY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteCurrency(DB_CURRENCY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelCurrency",
                new DB.Parameter[] {
            new DB.InParameter("@ID_CURRENCY", toBeDeleted.ID_CURRENCY)			
		}
            );
        }

        #endregion

        #region Table DATA

        public DB_DATA[] GetData()
        {
            return (DB_DATA[])DB.ExecuteProcedure(
                "imrse_GetData",
                new DB.AnalyzeDataSet(GetData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA", new long[] {})
					}
            );
        }

        public DB_DATA[] GetData(long[] Ids)
        {
            return (DB_DATA[])DB.ExecuteProcedure(
                "imrse_GetData",
                new DB.AnalyzeDataSet(GetData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA", Ids)
					}
            );
        }



        private object GetData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA()
            {
                ID_DATA = GetValue<long>(row["ID_DATA"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                STATUS = GetValue<int>(row["STATUS"]),
            }).ToArray();
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA[] list = new DB_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA insert = new DB_DATA();
                insert.ID_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["STATUS"]);
                list[i] = insert;
            }
            return list;
            */
        }

        public long SaveData(DB_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA", ToBeSaved.ID_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@STATUS", ToBeSaved.STATUS)
									},
                                    sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteData(DB_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA", toBeDeleted.ID_DATA)			
		}
            );
        }

        #endregion
        #region Table DATA_ARCH
        public DB_DATA_ARCH[] GetDataArch()
        {
            return (DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH", new long[] {})
					}
            );
        }

        public DB_DATA_ARCH[] GetDataArch(long[] Ids)
        {
            return (DB_DATA_ARCH[])DB.ExecuteProcedure(
                "imrse_GetDataArch",
                new DB.AnalyzeDataSet(GetDataArch),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_ARCH", Ids)
					}
            );
        }
        
        private object GetDataArch(DataSet QueryResult)
        {
            bool containsIdMeterColumn = QueryResult.Tables[0].Columns.Contains("ID_METER");
            bool containsIdLocationColumn = QueryResult.Tables[0].Columns.Contains("ID_LOCATION");


            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_ARCH()
            {
                ID_DATA_ARCH = GetValue<long>(row["ID_DATA_ARCH"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
                TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIME"])),
                IS_ACTION = GetValue<bool>(row["IS_ACTION"]),
                IS_ALARM = GetValue<bool>(row["IS_ALARM"]),
                ID_PACKET = GetNullableValue<long>(row["ID_PACKET"]),
                ID_METER = containsIdMeterColumn ? GetNullableValue<long>(row["ID_METER"]) : null,
                ID_LOCATION = containsIdLocationColumn ?  GetNullableValue<long>(row["ID_LOCATION"]) : null,
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_ARCH[] list = new DB_DATA_ARCH[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_ARCH insert = new DB_DATA_ARCH();
                insert.ID_DATA_ARCH = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_ARCH"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.IS_ACTION = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTION"]);
                insert.IS_ALARM = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ALARM"]);
                insert.ID_PACKET = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKET"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveDataArch(DB_DATA_ARCH ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_ARCH", ToBeSaved.ID_DATA_ARCH);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataArch",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@IS_ACTION", ToBeSaved.IS_ACTION)
														,new DB.InParameter("@IS_ALARM", ToBeSaved.IS_ALARM)
														,new DB.InParameter("@ID_PACKET", ToBeSaved.ID_PACKET)
                                                        ,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
                                                        ,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_ARCH = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataArch(DB_DATA_ARCH toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataArch",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_ARCH", toBeDeleted.ID_DATA_ARCH)			
		}
            );
        }
        #endregion
        #region Table DATA_CHANGE

        public DB_DATA_CHANGE[] GetDataChange()
        {
            return (DB_DATA_CHANGE[])DB.ExecuteProcedure(
                "imrse_GetDataChange",
                new DB.AnalyzeDataSet(GetDataChange),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_CHANGE", new long[] {})
					}
            );
        }

        public DB_DATA_CHANGE[] GetDataChange(long[] Ids)
        {
            return (DB_DATA_CHANGE[])DB.ExecuteProcedure(
                "imrse_GetDataChange",
                new DB.AnalyzeDataSet(GetDataChange),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_CHANGE", Ids)
					}
            );
        }

        private object GetDataChange(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_CHANGE[] list = new DB_DATA_CHANGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_CHANGE insert = new DB_DATA_CHANGE();
                insert.ID_DATA_CHANGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_CHANGE"]);
                insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
                insert.CHANGED_ID = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["CHANGED_ID"]);
                insert.CHANGED_ID2 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["CHANGED_ID2"]);
                insert.CHANGED_ID3 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["CHANGED_ID3"]);
                insert.CHANGED_ID4 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["CHANGED_ID4"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.CHANGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_TYPE"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                insert.USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["USER"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataChange(DB_DATA_CHANGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_CHANGE", ToBeSaved.ID_DATA_CHANGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataChange",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME)
														,new DB.InParameter("@CHANGED_ID", ToBeSaved.CHANGED_ID)
														,new DB.InParameter("@CHANGED_ID2", ToBeSaved.CHANGED_ID2)
														,new DB.InParameter("@CHANGED_ID3", ToBeSaved.CHANGED_ID3)
														,new DB.InParameter("@CHANGED_ID4", ToBeSaved.CHANGED_ID4)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,new DB.InParameter("@CHANGE_TYPE", ToBeSaved.CHANGE_TYPE)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
														,new DB.InParameter("@USER", ToBeSaved.USER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_CHANGE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataChange(DB_DATA_CHANGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataChange",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_CHANGE", toBeDeleted.ID_DATA_CHANGE)			
		}
            );
        }

        #endregion

        #region Table DATA_FORMAT_GROUP

        public DB_DATA_FORMAT_GROUP[] GetDataFormatGroup()
        {
            return (DB_DATA_FORMAT_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataFormatGroup",
                new DB.AnalyzeDataSet(GetDataFormatGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_FORMAT_GROUP", new int[] {})
					}
            );
        }

        public DB_DATA_FORMAT_GROUP[] GetDataFormatGroup(int[] Ids)
        {
            return (DB_DATA_FORMAT_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataFormatGroup",
                new DB.AnalyzeDataSet(GetDataFormatGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_FORMAT_GROUP", Ids)
					}
            );
        }



        private object GetDataFormatGroup(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_FORMAT_GROUP()
            {
                ID_DATA_FORMAT_GROUP = GetValue<int>(row["ID_DATA_FORMAT_GROUP"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_MODULE = GetNullableValue<int>(row["ID_MODULE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_DATA_FORMAT_GROUP[] list = new DB_DATA_FORMAT_GROUP[count];
    for (int i = 0; i < count; i++)
    {
        DB_DATA_FORMAT_GROUP insert = new DB_DATA_FORMAT_GROUP();
									insert.ID_DATA_FORMAT_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_FORMAT_GROUP"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveDataFormatGroup(DB_DATA_FORMAT_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_FORMAT_GROUP", ToBeSaved.ID_DATA_FORMAT_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataFormatGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_FORMAT_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataFormatGroup(DB_DATA_FORMAT_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataFormatGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_FORMAT_GROUP", toBeDeleted.ID_DATA_FORMAT_GROUP)			
		}
            );
        }

        #endregion
        #region Table DATA_FORMAT_GROUP_DETAILS

        public DB_DATA_FORMAT_GROUP_DETAILS[] GetDataFormatGroupDetails()
        {
            return (DB_DATA_FORMAT_GROUP_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetDataFormatGroupDetails",
                new DB.AnalyzeDataSet(GetDataFormatGroupDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_FORMAT_GROUP", new int[] {})
					}
            );
        }

        public DB_DATA_FORMAT_GROUP_DETAILS[] GetDataFormatGroupDetails(int[] Ids)
        {
            return (DB_DATA_FORMAT_GROUP_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetDataFormatGroupDetails",
                new DB.AnalyzeDataSet(GetDataFormatGroupDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_FORMAT_GROUP", Ids)
					}
            );
        }



        private object GetDataFormatGroupDetails(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_FORMAT_GROUP_DETAILS()
            {
                ID_DATA_FORMAT_GROUP = GetValue<int>(row["ID_DATA_FORMAT_GROUP"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                ID_DATA_TYPE_FORMAT_IN = GetNullableValue<int>(row["ID_DATA_TYPE_FORMAT_IN"]),
                ID_DATA_TYPE_FORMAT_OUT = GetNullableValue<int>(row["ID_DATA_TYPE_FORMAT_OUT"]),
                ID_UNIT_IN = GetNullableValue<int>(row["ID_UNIT_IN"]),
                ID_UNIT_OUT = GetNullableValue<int>(row["ID_UNIT_OUT"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_DATA_FORMAT_GROUP_DETAILS[] list = new DB_DATA_FORMAT_GROUP_DETAILS[count];
    for (int i = 0; i < count; i++)
    {
        DB_DATA_FORMAT_GROUP_DETAILS insert = new DB_DATA_FORMAT_GROUP_DETAILS();
									insert.ID_DATA_FORMAT_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_FORMAT_GROUP"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.ID_DATA_TYPE_FORMAT_IN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_FORMAT_IN"]);
												insert.ID_DATA_TYPE_FORMAT_OUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_FORMAT_OUT"]);
												insert.ID_UNIT_IN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT_IN"]);
												insert.ID_UNIT_OUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT_OUT"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        #endregion

        #region Table DATA_TYPE

        public DB_DATA_TYPE[] GetDataType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataType",
                new DB.AnalyzeDataSet(GetDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DATA_TYPE[] GetDataType(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataType",
                new DB.AnalyzeDataSet(GetDataType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDataType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE[] list = new DB_DATA_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE insert = new DB_DATA_TYPE();
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_DATA_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CLASS"]);
                insert.ID_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                insert.ID_DATA_TYPE_FORMAT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_FORMAT"]);
                insert.IS_ARCHIVE_ONLY = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ARCHIVE_ONLY"]);
                insert.IS_REMOTE_READ = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_REMOTE_READ"]);
                insert.IS_REMOTE_WRITE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_REMOTE_WRITE"]);
                insert.IS_EDITABLE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_EDITABLE"]);
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDataType(DB_DATA_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveDataType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_DATA_TYPE_CLASS", ToBeSaved.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE_FORMAT", ToBeSaved.ID_DATA_TYPE_FORMAT)
														,new DB.InParameter("@IS_ARCHIVE_ONLY", ToBeSaved.IS_ARCHIVE_ONLY)
														,new DB.InParameter("@IS_REMOTE_READ", ToBeSaved.IS_REMOTE_READ)
														,new DB.InParameter("@IS_REMOTE_WRITE", ToBeSaved.IS_REMOTE_WRITE)
														,new DB.InParameter("@IS_EDITABLE", ToBeSaved.IS_EDITABLE)
														,new DB.InParameter("@ID_UNIT", ToBeSaved.ID_UNIT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TYPE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDataType(DB_DATA_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE)			
		}
            );
        }

        #endregion
        #region Table DATA_TYPE_CLASS

        public DB_DATA_TYPE_CLASS[] GetDataTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDataTypeClass",
                new DB.AnalyzeDataSet(GetDataTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_CLASS", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DATA_TYPE_CLASS[] GetDataTypeClass(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDataTypeClass",
                new DB.AnalyzeDataSet(GetDataTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_CLASS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDataTypeClass(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE_CLASS[] list = new DB_DATA_TYPE_CLASS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE_CLASS insert = new DB_DATA_TYPE_CLASS();
                insert.ID_DATA_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataTypeClass(DB_DATA_TYPE_CLASS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TYPE_CLASS", ToBeSaved.ID_DATA_TYPE_CLASS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataTypeClass",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TYPE_CLASS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataTypeClass(DB_DATA_TYPE_CLASS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTypeClass",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE_CLASS", toBeDeleted.ID_DATA_TYPE_CLASS)			
		}
            );
        }

        #endregion
        #region Table DATA_TYPE_FORMAT

        public DB_DATA_TYPE_FORMAT[] GetDataTypeFormat(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE_FORMAT[])DB.ExecuteProcedure(
                "imrse_GetDataTypeFormat",
                new DB.AnalyzeDataSet(GetDataTypeFormat),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_FORMAT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DATA_TYPE_FORMAT[] GetDataTypeFormat(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TYPE_FORMAT[])DB.ExecuteProcedure(
                "imrse_GetDataTypeFormat",
                new DB.AnalyzeDataSet(GetDataTypeFormat),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_FORMAT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDataTypeFormat(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE_FORMAT[] list = new DB_DATA_TYPE_FORMAT[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE_FORMAT insert = new DB_DATA_TYPE_FORMAT();
                insert.ID_DATA_TYPE_FORMAT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_FORMAT"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.TEXT_MIN_LENGTH = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["TEXT_MIN_LENGTH"]);
                insert.TEXT_MAX_LENGTH = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["TEXT_MAX_LENGTH"]);
                insert.NUMBER_MIN_PRECISION = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["NUMBER_MIN_PRECISION"]);
                insert.NUMBER_MAX_PRECISION = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["NUMBER_MAX_PRECISION"]);
                insert.NUMBER_MIN_SCALE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["NUMBER_MIN_SCALE"]);
                insert.NUMBER_MAX_SCALE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["NUMBER_MAX_SCALE"]);
                insert.NUMBER_MIN_VALUE = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["NUMBER_MIN_VALUE"]);
                insert.NUMBER_MAX_VALUE = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["NUMBER_MAX_VALUE"]);
                insert.DATETIME_FORMAT = GetValue<string>(QueryResult.Tables[0].Rows[i]["DATETIME_FORMAT"]);
                insert.REGULAR_EXPRESSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["REGULAR_EXPRESSION"]);
                insert.IS_REQUIRED = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_REQUIRED"]);
                insert.ID_UNIQUE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIQUE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataTypeFormat(DB_DATA_TYPE_FORMAT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TYPE_FORMAT", ToBeSaved.ID_DATA_TYPE_FORMAT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataTypeFormat",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@TEXT_MIN_LENGTH", ToBeSaved.TEXT_MIN_LENGTH)
														,new DB.InParameter("@TEXT_MAX_LENGTH", ToBeSaved.TEXT_MAX_LENGTH)
														,new DB.InParameter("@NUMBER_MIN_PRECISION", ToBeSaved.NUMBER_MIN_PRECISION)
														,new DB.InParameter("@NUMBER_MAX_PRECISION", ToBeSaved.NUMBER_MAX_PRECISION)
														,new DB.InParameter("@NUMBER_MIN_SCALE", ToBeSaved.NUMBER_MIN_SCALE)
														,new DB.InParameter("@NUMBER_MAX_SCALE", ToBeSaved.NUMBER_MAX_SCALE)
														,new DB.InParameter("@NUMBER_MIN_VALUE", ToBeSaved.NUMBER_MIN_VALUE)
														,new DB.InParameter("@NUMBER_MAX_VALUE", ToBeSaved.NUMBER_MAX_VALUE)
														,new DB.InParameter("@DATETIME_FORMAT", ToBeSaved.DATETIME_FORMAT)
														,new DB.InParameter("@REGULAR_EXPRESSION", ToBeSaved.REGULAR_EXPRESSION)
														,new DB.InParameter("@IS_REQUIRED", ToBeSaved.IS_REQUIRED)
														,new DB.InParameter("@ID_UNIQUE_TYPE", ToBeSaved.ID_UNIQUE_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TYPE_FORMAT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataTypeFormat(DB_DATA_TYPE_FORMAT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTypeFormat",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE_FORMAT", toBeDeleted.ID_DATA_TYPE_FORMAT)			
		}
            );
        }

        #endregion
        #region Table DATA_TYPE_GROUP

        public DB_DATA_TYPE_GROUP[] GetDataTypeGroup()
        {
            return (DB_DATA_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroup",
                new DB.AnalyzeDataSet(GetDataTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_DATA_TYPE_GROUP[] GetDataTypeGroup(int[] Ids)
        {
            return (DB_DATA_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroup",
                new DB.AnalyzeDataSet(GetDataTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP", Ids)
					}
            );
        }

        private object GetDataTypeGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE_GROUP[] list = new DB_DATA_TYPE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE_GROUP insert = new DB_DATA_TYPE_GROUP();
                insert.ID_DATA_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_GROUP"]);
                insert.ID_DATA_TYPE_GROUP_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_GROUP_TYPE"]);
                insert.ID_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_PARENT_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARENT_GROUP"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataTypeGroup(DB_DATA_TYPE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TYPE_GROUP", ToBeSaved.ID_DATA_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataTypeGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DATA_TYPE_GROUP_TYPE", ToBeSaved.ID_DATA_TYPE_GROUP_TYPE)
														,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_PARENT_GROUP", ToBeSaved.ID_PARENT_GROUP)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataTypeGroup(DB_DATA_TYPE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTypeGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE_GROUP", toBeDeleted.ID_DATA_TYPE_GROUP)			
		}
            );
        }

        #endregion
        #region Table DATA_TYPE_GROUP_TYPE

        public DB_DATA_TYPE_GROUP_TYPE[] GetDataTypeGroupType()
        {
            return (DB_DATA_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroupType",
                new DB.AnalyzeDataSet(GetDataTypeGroupType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP_TYPE", new int[] {})
					}
            );
        }

        public DB_DATA_TYPE_GROUP_TYPE[] GetDataTypeGroupType(int[] Ids)
        {
            return (DB_DATA_TYPE_GROUP_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDataTypeGroupType",
                new DB.AnalyzeDataSet(GetDataTypeGroupType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TYPE_GROUP_TYPE", Ids)
					}
            );
        }

        private object GetDataTypeGroupType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DATA_TYPE_GROUP_TYPE[] list = new DB_DATA_TYPE_GROUP_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DATA_TYPE_GROUP_TYPE insert = new DB_DATA_TYPE_GROUP_TYPE();
                insert.ID_DATA_TYPE_GROUP_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_GROUP_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDataTypeGroupType(DB_DATA_TYPE_GROUP_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DATA_TYPE_GROUP_TYPE", ToBeSaved.ID_DATA_TYPE_GROUP_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDataTypeGroupType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DATA_TYPE_GROUP_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDataTypeGroupType(DB_DATA_TYPE_GROUP_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTypeGroupType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TYPE_GROUP_TYPE", toBeDeleted.ID_DATA_TYPE_GROUP_TYPE)			
		}
            );
        }

        #endregion

        #region Table DATA_TRANSFER

        public DB_DATA_TRANSFER[] GetDataTransfer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TRANSFER[])DB.ExecuteProcedure(
                "imrse_GetDataTransfer",
                new DB.AnalyzeDataSet(GetDataTransfer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TRANSFER", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DATA_TRANSFER[] GetDataTransfer(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DATA_TRANSFER[])DB.ExecuteProcedure(
                "imrse_GetDataTransfer",
                new DB.AnalyzeDataSet(GetDataTransfer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DATA_TRANSFER", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDataTransfer(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DATA_TRANSFER()
            {
                ID_DATA_TRANSFER = GetValue<long>(row["ID_DATA_TRANSFER"]),
                BATCH_ID = GetValue<long>(row["BATCH_ID"]),
                CHANGE_TYPE = GetValue<int>(row["CHANGE_TYPE"]),
                CHANGE_DIRECTION = GetValue<int>(row["CHANGE_DIRECTION"]),
                ID_SOURCE_SERVER = GetNullableValue<int>(row["ID_SOURCE_SERVER"]),
                ID_DESTINATION_SERVER = GetNullableValue<int>(row["ID_DESTINATION_SERVER"]),
                TABLE_NAME = GetValue<string>(row["TABLE_NAME"]),
                KEY1 = GetNullableValue<long>(row["KEY1"]),
                KEY2 = GetNullableValue<long>(row["KEY2"]),
                KEY3 = GetNullableValue<long>(row["KEY3"]),
                KEY4 = GetNullableValue<long>(row["KEY4"]),
                KEY5 = GetNullableValue<long>(row["KEY5"]),
                KEY6 = GetNullableValue<long>(row["KEY6"]),
                COLUMN_NAME = GetValue<string>(row["COLUMN_NAME"]),
                OLD_VALUE = GetValue(row["OLD_VALUE"]),
                NEW_VALUE = GetValue(row["NEW_VALUE"]),
                INSERT_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["INSERT_TIME"])),
                PROCEED_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["PROCEED_TIME"])),
                USER = GetValue<string>(row["USER"]),
                HOST = GetValue<string>(row["HOST"]),
                APPLICATION = GetValue<string>(row["APPLICATION"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_DATA_TRANSFER[] list = new DB_DATA_TRANSFER[count];
    for (int i = 0; i < count; i++)
    {
        DB_DATA_TRANSFER insert = new DB_DATA_TRANSFER();
									insert.ID_DATA_TRANSFER = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TRANSFER"]);
												insert.BATCH_ID = GetValue<long>(QueryResult.Tables[0].Rows[i]["BATCH_ID"]);
												insert.CHANGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_TYPE"]);
												insert.CHANGE_DIRECTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["CHANGE_DIRECTION"]);
												insert.ID_SOURCE_SERVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SOURCE_SERVER"]);
												insert.ID_DESTINATION_SERVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DESTINATION_SERVER"]);
												insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
												insert.KEY1 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY1"]);
												insert.KEY2 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY2"]);
												insert.KEY3 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY3"]);
												insert.KEY4 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY4"]);
												insert.KEY5 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY5"]);
												insert.KEY6 = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["KEY6"]);
												insert.COLUMN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLUMN_NAME"]);
												insert.OLD_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["OLD_VALUE"]);
												insert.NEW_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["NEW_VALUE"]);
												insert.INSERT_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["INSERT_TIME"]));
												insert.PROCEED_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["PROCEED_TIME"]));
												insert.USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["USER"]);
												insert.HOST = GetValue<string>(QueryResult.Tables[0].Rows[i]["HOST"]);
												insert.APPLICATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["APPLICATION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        public void DeleteDataTransfer(DB_DATA_TRANSFER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDataTransfer",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DATA_TRANSFER", toBeDeleted.ID_DATA_TRANSFER)			
		}
            );
        }

        #endregion

        #region Table DAYLIGHT_SAVING_TIME

        public DB_DAYLIGHT_SAVING_TIME[] GetDaylightSavingTime(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DAYLIGHT_SAVING_TIME[])DB.ExecuteProcedure(
                "imrse_GetDaylightSavingTime",
                new DB.AnalyzeDataSet(GetDaylightSavingTime),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DAYLIGHT_SAVING", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DAYLIGHT_SAVING_TIME[] GetDaylightSavingTime(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DAYLIGHT_SAVING_TIME[])DB.ExecuteProcedure(
                "imrse_GetDaylightSavingTime",
                new DB.AnalyzeDataSet(GetDaylightSavingTime),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DAYLIGHT_SAVING", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDaylightSavingTime(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DAYLIGHT_SAVING_TIME()
            {
                ID_DAYLIGHT_SAVING = GetValue<int>(row["ID_DAYLIGHT_SAVING"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_DAYLIGHT_SAVING_TIME[] list = new DB_DAYLIGHT_SAVING_TIME[count];
    for (int i = 0; i < count; i++)
    {
        DB_DAYLIGHT_SAVING_TIME insert = new DB_DAYLIGHT_SAVING_TIME();
									insert.ID_DAYLIGHT_SAVING = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DAYLIGHT_SAVING"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveDaylightSavingTime(DB_DAYLIGHT_SAVING_TIME ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DAYLIGHT_SAVING", ToBeSaved.ID_DAYLIGHT_SAVING);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDaylightSavingTime",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DAYLIGHT_SAVING = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDaylightSavingTime(DB_DAYLIGHT_SAVING_TIME toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDaylightSavingTime",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DAYLIGHT_SAVING", toBeDeleted.ID_DAYLIGHT_SAVING)			
		}
            );
        }

        #endregion

        #region Table DELIVERY

        public DB_DELIVERY[] GetDelivery()
        {
            return (DB_DELIVERY[])DB.ExecuteProcedure(
                "imrse_GetDelivery",
                new DB.AnalyzeDataSet(GetDelivery),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DELIVERY", new int[] {})
					}
            );
        }

        public DB_DELIVERY[] GetDelivery(int[] Ids)
        {
            return (DB_DELIVERY[])DB.ExecuteProcedure(
                "imrse_GetDelivery",
                new DB.AnalyzeDataSet(GetDelivery),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DELIVERY", Ids)
					}
            );
        }

        private object GetDelivery(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DELIVERY[] list = new DB_DELIVERY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DELIVERY insert = new DB_DELIVERY();
                insert.ID_DELIVERY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DELIVERY"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.UPLOADED_FILE = GetValue<string>(QueryResult.Tables[0].Rows[i]["UPLOADED_FILE"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.ID_SHIPPING_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDelivery(DB_DELIVERY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DELIVERY", ToBeSaved.ID_DELIVERY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDelivery",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@UPLOADED_FILE", ToBeSaved.UPLOADED_FILE)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@ID_SHIPPING_LIST", ToBeSaved.ID_SHIPPING_LIST)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DELIVERY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDelivery(DB_DELIVERY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDelivery",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DELIVERY", toBeDeleted.ID_DELIVERY)			
		}
            );
        }

        #endregion

        #region Table DEPOSITORY_ELEMENT

        public DB_DEPOSITORY_ELEMENT[] GetDepositoryElement()
        {
            return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElement",
                new DB.AnalyzeDataSet(GetDepositoryElement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOSITORY_ELEMENT", new int[] {})
					}
            );
        }

        public DB_DEPOSITORY_ELEMENT[] GetDepositoryElement(int[] Ids)
        {
            return (DB_DEPOSITORY_ELEMENT[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElement",
                new DB.AnalyzeDataSet(GetDepositoryElement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOSITORY_ELEMENT", Ids)
					}
            );
        }



        private object GetDepositoryElement(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEPOSITORY_ELEMENT[] list = new DB_DEPOSITORY_ELEMENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEPOSITORY_ELEMENT insert = new DB_DEPOSITORY_ELEMENT();
                insert.ID_DEPOSITORY_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_ELEMENT"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_TASK = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insert.ID_PACKAGE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.ID_ARTICLE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE"]);
                insert.ID_DEVICE_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_CLASS"]);
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.REMOVED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["REMOVED"]);
                insert.ORDERED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ORDERED"]);
                insert.ACCEPTED = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["ACCEPTED"]);
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.EXTERNAL_SN = GetValue<string>(QueryResult.Tables[0].Rows[i]["EXTERNAL_SN"]);
                insert.ID_REFERENCE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveDepositoryElement(DB_DEPOSITORY_ELEMENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEPOSITORY_ELEMENT", ToBeSaved.ID_DEPOSITORY_ELEMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDepositoryElement",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_TASK", ToBeSaved.ID_TASK)
														,new DB.InParameter("@ID_PACKAGE", ToBeSaved.ID_PACKAGE)
														,new DB.InParameter("@ID_ARTICLE", ToBeSaved.ID_ARTICLE)
														,new DB.InParameter("@ID_DEVICE_TYPE_CLASS", ToBeSaved.ID_DEVICE_TYPE_CLASS)
														,new DB.InParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@REMOVED", ToBeSaved.REMOVED)
														,new DB.InParameter("@ORDERED", ToBeSaved.ORDERED)
														,new DB.InParameter("@ACCEPTED", ToBeSaved.ACCEPTED)
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@EXTERNAL_SN", ToBeSaved.EXTERNAL_SN)
														,new DB.InParameter("@ID_REFERENCE", ToBeSaved.ID_REFERENCE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEPOSITORY_ELEMENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }



        public void DeleteDepositoryElement(DB_DEPOSITORY_ELEMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDepositoryElement",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEPOSITORY_ELEMENT", toBeDeleted.ID_DEPOSITORY_ELEMENT)			
		}
            );
        }

        #endregion
        #region Table DEPOSITORY_ELEMENT_DATA

        public DB_DEPOSITORY_ELEMENT_DATA[] GetDepositoryElementData()
        {
            return (DB_DEPOSITORY_ELEMENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElementData",
                new DB.AnalyzeDataSet(GetDepositoryElementData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOSITORY_ELEMENT_DATA", new long[] {})
					}
            );
        }

        public DB_DEPOSITORY_ELEMENT_DATA[] GetDepositoryElementData(long[] Ids)
        {
            return (DB_DEPOSITORY_ELEMENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetDepositoryElementData",
                new DB.AnalyzeDataSet(GetDepositoryElementData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOSITORY_ELEMENT_DATA", Ids)
					}
            );
        }

        private object GetDepositoryElementData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEPOSITORY_ELEMENT_DATA[] list = new DB_DEPOSITORY_ELEMENT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEPOSITORY_ELEMENT_DATA insert = new DB_DEPOSITORY_ELEMENT_DATA();
                insert.ID_DEPOSITORY_ELEMENT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_ELEMENT_DATA"]);
                insert.ID_DEPOSITORY_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_ELEMENT"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDepositoryElementData(DB_DEPOSITORY_ELEMENT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEPOSITORY_ELEMENT_DATA", ToBeSaved.ID_DEPOSITORY_ELEMENT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDepositoryElementData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEPOSITORY_ELEMENT", ToBeSaved.ID_DEPOSITORY_ELEMENT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEPOSITORY_ELEMENT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDepositoryElementData(DB_DEPOSITORY_ELEMENT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDepositoryElementData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEPOSITORY_ELEMENT_DATA", toBeDeleted.ID_DEPOSITORY_ELEMENT_DATA)			
		}
            );
        }

        #endregion

        #region Table DEPOT

        public DB_DEPOT[] GetDepot()
        {
            return (DB_DEPOT[])DB.ExecuteProcedure(
                "imrse_GetDepot",
                new DB.AnalyzeDataSet(GetDepot),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOT", new int[] {})
					}
            );
        }

        public DB_DEPOT[] GetDepot(int[] Ids)
        {
            return (DB_DEPOT[])DB.ExecuteProcedure(
                "imrse_GetDepot",
                new DB.AnalyzeDataSet(GetDepot),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEPOT", Ids)
					}
            );
        }



        private object GetDepot(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEPOT[] list = new DB_DEPOT[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEPOT insert = new DB_DEPOT();
                insert.ID_DEPOT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEPOT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDepot(DB_DEPOT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEPOT", ToBeSaved.ID_DEPOT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDepot",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEPOT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDepot(DB_DEPOT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDepot",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEPOT", toBeDeleted.ID_DEPOT)			
		}
            );
        }

        #endregion

        #region Table DEVICE
        public DB_DEVICE[] GetDevice(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetDevice",
                new DB.AnalyzeDataSet(GetDevice),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", this.DeviceFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE[] GetDevice(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetDevice",
                new DB.AnalyzeDataSet(GetDevice),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", Ids != null ? Ids : DeviceFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter != null ? DistributorFilter : new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDevice(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_DEVICE()
            {
                SERIAL_NBR = GetValue<long>(row["SERIAL_NBR"]),
                ID_DEVICE_TYPE = GetValue<int>(row["ID_DEVICE_TYPE"]),
                SERIAL_NBR_PATTERN = GetNullableValue<long>(row["SERIAL_NBR_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DEVICE_ORDER_NUMBER = GetValue<int>(row["ID_DEVICE_ORDER_NUMBER"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                ID_DEVICE_STATE_TYPE = GetValue<int>(row["ID_DEVICE_STATE_TYPE"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE[] list = new DB_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE insert = new DB_DEVICE();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.SERIAL_NBR_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PATTERN"]);
                insert.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR_PATTERN"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveDevice(DB_DEVICE ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDevice",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@SERIAL_NBR_PATTERN", ToBeSaved.SERIAL_NBR_PATTERN)
														,new DB.InParameter("@ID_DESCR_PATTERN", ToBeSaved.ID_DESCR_PATTERN)
														,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE)
									}
                                    , sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.SERIAL_NBR = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDevice(DB_DEVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDevice",
                new DB.Parameter[] {
            new DB.InParameter("@SERIAL_NBR", toBeDeleted.SERIAL_NBR)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DETAILS

        public DB_DEVICE_DETAILS[] GetDeviceDetails(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetDeviceDetails",
                new DB.AnalyzeDataSet(GetDeviceDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DETAILS", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_DETAILS[] GetDeviceDetails(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetDeviceDetails",
                new DB.AnalyzeDataSet(GetDeviceDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DETAILS", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDeviceDetails(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DETAILS[] list = new DB_DEVICE_DETAILS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DETAILS insert = new DB_DEVICE_DETAILS();
                insert.ID_DEVICE_DETAILS = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DETAILS"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.FACTORY_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["FACTORY_NBR"]);
                insert.SHIPPING_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SHIPPING_DATE"]));
                insert.WARRANTY_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["WARRANTY_DATE"]));
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }


        public long SaveDeviceDetails(DB_DEVICE_DETAILS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DETAILS", ToBeSaved.ID_DEVICE_DETAILS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceDetails",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@FACTORY_NBR", ToBeSaved.FACTORY_NBR)
														,new DB.InParameter("@SHIPPING_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.SHIPPING_DATE))
														,new DB.InParameter("@WARRANTY_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.WARRANTY_DATE))
														,new DB.InParameter("@PHONE", ToBeSaved.PHONE)
														,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE)
                                                        ,new DB.InParameter("@PRODUCTION_DEVICE_ORDER_NUMBER", ToBeSaved.PRODUCTION_DEVICE_ORDER_NUMBER)
                                                        ,new DB.InParameter("@DEVICE_BATCH_NUMBER", ToBeSaved.DEVICE_BATCH_NUMBER)
														,new DB.InParameter("@ID_DISTRIBUTOR_OWNER", ToBeSaved.ID_DISTRIBUTOR_OWNER)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DETAILS = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteDeviceDetails(DB_DEVICE_DETAILS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDetails",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DETAILS", toBeDeleted.ID_DEVICE_DETAILS)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DISTRIBUTOR_HISTORY

        public DB_DEVICE_DISTRIBUTOR_HISTORY[] GetDeviceDistributorHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DISTRIBUTOR_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceDistributorHistory",
                new DB.AnalyzeDataSet(GetDeviceDistributorHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DISTRIBUTOR_HISTORY", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_DISTRIBUTOR_HISTORY[] GetDeviceDistributorHistory(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DISTRIBUTOR_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceDistributorHistory",
                new DB.AnalyzeDataSet(GetDeviceDistributorHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DISTRIBUTOR_HISTORY", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDeviceDistributorHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DISTRIBUTOR_HISTORY[] list = new DB_DEVICE_DISTRIBUTOR_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DISTRIBUTOR_HISTORY insert = new DB_DEVICE_DISTRIBUTOR_HISTORY();
                insert.ID_DEVICE_DISTRIBUTOR_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DISTRIBUTOR_HISTORY"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DISTRIBUTOR_OWNER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_OWNER"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_SERVICE_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_REFERENCE_TYPE"]);
                insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceDistributorHistory(DB_DEVICE_DISTRIBUTOR_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DISTRIBUTOR_HISTORY", ToBeSaved.ID_DEVICE_DISTRIBUTOR_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceDistributorHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DISTRIBUTOR_OWNER", ToBeSaved.ID_DISTRIBUTOR_OWNER)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_SERVICE_REFERENCE_TYPE", ToBeSaved.ID_SERVICE_REFERENCE_TYPE)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE, 0, 0)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DISTRIBUTOR_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceDistributorHistory(DB_DEVICE_DISTRIBUTOR_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDistributorHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DISTRIBUTOR_HISTORY", toBeDeleted.ID_DEVICE_DISTRIBUTOR_HISTORY)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DRIVER

        public DB_DEVICE_DRIVER[] GetDeviceDriver(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriver",
                new DB.AnalyzeDataSet(GetDeviceDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_DRIVER[] GetDeviceDriver(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriver",
                new DB.AnalyzeDataSet(GetDeviceDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDeviceDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DRIVER[] list = new DB_DEVICE_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DRIVER insert = new DB_DEVICE_DRIVER();
                insert.ID_DEVICE_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.PLUGIN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN_NAME"]);
                insert.AUTO_UPDATE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["AUTO_UPDATE"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteDeviceDriver(DB_DEVICE_DRIVER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDriver",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DRIVER", toBeDeleted.ID_DEVICE_DRIVER)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DRIVER_DATA

        public DB_DEVICE_DRIVER_DATA[] GetDeviceDriverData()
        {
            return (DB_DEVICE_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverData",
                new DB.AnalyzeDataSet(GetDeviceDriverData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_DATA", new long[] {})
					}
            );
        }

        public DB_DEVICE_DRIVER_DATA[] GetDeviceDriverData(long[] Ids)
        {
            return (DB_DEVICE_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverData",
                new DB.AnalyzeDataSet(GetDeviceDriverData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_DATA", Ids)
					}
            );
        }



        private object GetDeviceDriverData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DRIVER_DATA[] list = new DB_DEVICE_DRIVER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DRIVER_DATA insert = new DB_DEVICE_DRIVER_DATA();
                insert.ID_DEVICE_DRIVER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER_DATA"]);
                insert.ID_DEVICE_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public DB_DEVICE_DRIVER_DATA[] GetDeviceDriverDataFilter(long[] IdDeviceDriverData, int[] IdDeviceDriver, long[] IdDataType, int[] IndexNbr)
        {
            return (DB_DEVICE_DRIVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverDataFilter",
                new DB.AnalyzeDataSet(GetDeviceDriverData),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DEVICE_DRIVER_DATA", IdDeviceDriverData),
					CreateTableParam("@ID_DEVICE_DRIVER", IdDeviceDriver),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					CreateTableParam("@INDEX_NBR", IndexNbr)		}
            );
        }

        public long SaveDeviceDriverData(DB_DEVICE_DRIVER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DRIVER_DATA", ToBeSaved.ID_DEVICE_DRIVER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceDriverData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_DRIVER", ToBeSaved.ID_DEVICE_DRIVER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DRIVER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceDriverData(DB_DEVICE_DRIVER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDriverData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DRIVER_DATA", toBeDeleted.ID_DEVICE_DRIVER_DATA)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DRIVER_MEMORY_MAP

        public DB_DEVICE_DRIVER_MEMORY_MAP[] GetDeviceDriverMemoryMap()
        {
            return (DB_DEVICE_DRIVER_MEMORY_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMap",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP", new int[] {})
					}
            );
        }

        public DB_DEVICE_DRIVER_MEMORY_MAP[] GetDeviceDriverMemoryMap(int[] Ids)
        {
            return (DB_DEVICE_DRIVER_MEMORY_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMap",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP", Ids)
					}
            );
        }



        private object GetDeviceDriverMemoryMap(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DRIVER_MEMORY_MAP[] list = new DB_DEVICE_DRIVER_MEMORY_MAP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DRIVER_MEMORY_MAP insert = new DB_DEVICE_DRIVER_MEMORY_MAP();
                insert.ID_DEVICE_DRIVER_MEMORY_MAP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER_MEMORY_MAP"]);
                insert.ID_DEVICE_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER"]);
                insert.FILE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["FILE_NAME"]);
                insert.FILE_VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FILE_VERSION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceDriverMemoryMap(DB_DEVICE_DRIVER_MEMORY_MAP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DRIVER_MEMORY_MAP", ToBeSaved.ID_DEVICE_DRIVER_MEMORY_MAP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceDriverMemoryMap",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_DRIVER", ToBeSaved.ID_DEVICE_DRIVER)
														,new DB.InParameter("@FILE_NAME", ToBeSaved.FILE_NAME)
														,new DB.InParameter("@FILE_VERSION", ToBeSaved.FILE_VERSION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DRIVER_MEMORY_MAP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceDriverMemoryMap(DB_DEVICE_DRIVER_MEMORY_MAP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDriverMemoryMap",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DRIVER_MEMORY_MAP", toBeDeleted.ID_DEVICE_DRIVER_MEMORY_MAP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_DRIVER_MEMORY_MAP_DATA

        public DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] GetDeviceDriverMemoryMapData()
        {
            return (DB_DEVICE_DRIVER_MEMORY_MAP_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMapData",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMapData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP_DATA", new long[] {})
					}
            );
        }

        public DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] GetDeviceDriverMemoryMapData(long[] Ids)
        {
            return (DB_DEVICE_DRIVER_MEMORY_MAP_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceDriverMemoryMapData",
                new DB.AnalyzeDataSet(GetDeviceDriverMemoryMapData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_DRIVER_MEMORY_MAP_DATA", Ids)
					}
            );
        }



        private object GetDeviceDriverMemoryMapData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] list = new DB_DEVICE_DRIVER_MEMORY_MAP_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_DRIVER_MEMORY_MAP_DATA insert = new DB_DEVICE_DRIVER_MEMORY_MAP_DATA();
                insert.ID_DEVICE_DRIVER_MEMORY_MAP_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER_MEMORY_MAP_DATA"]);
                insert.ID_DDMMD_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DDMMD_PARENT"]);
                insert.ID_DEVICE_DRIVER_MEMORY_MAP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER_MEMORY_MAP"]);
                insert.ID_OMB = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OMB"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DATA_TYPE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.IS_READ = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_READ"]);
                insert.IS_WRITE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_WRITE"]);
                insert.IS_PUT_DATA_OMB = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_PUT_DATA_OMB"]);
                insert.ID_DDMMD_PUT_DATA = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DDMMD_PUT_DATA"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceDriverMemoryMapData(DB_DEVICE_DRIVER_MEMORY_MAP_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_DRIVER_MEMORY_MAP_DATA", ToBeSaved.ID_DEVICE_DRIVER_MEMORY_MAP_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceDriverMemoryMapData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DDMMD_PARENT", ToBeSaved.ID_DDMMD_PARENT)
														,new DB.InParameter("@ID_DEVICE_DRIVER_MEMORY_MAP", ToBeSaved.ID_DEVICE_DRIVER_MEMORY_MAP)
														,new DB.InParameter("@ID_OMB", ToBeSaved.ID_OMB)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,new DB.InParameter("@IS_READ", ToBeSaved.IS_READ)
														,new DB.InParameter("@IS_WRITE", ToBeSaved.IS_WRITE)
														,new DB.InParameter("@IS_PUT_DATA_OMB", ToBeSaved.IS_PUT_DATA_OMB)
														,new DB.InParameter("@ID_DDMMD_PUT_DATA", ToBeSaved.ID_DDMMD_PUT_DATA)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_DRIVER_MEMORY_MAP_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceDriverMemoryMapData(DB_DEVICE_DRIVER_MEMORY_MAP_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceDriverMemoryMapData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_DRIVER_MEMORY_MAP_DATA", toBeDeleted.ID_DEVICE_DRIVER_MEMORY_MAP_DATA)			
		}
            );
        }

        #endregion
        #region Table DEVICE_HIERARCHY

        public DB_DEVICE_HIERARCHY[] GetDeviceHierarchy()
        {
            return (DB_DEVICE_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchy",
                new DB.AnalyzeDataSet(GetDeviceHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY", new long[] {})
					}
            );
        }

        public DB_DEVICE_HIERARCHY[] GetDeviceHierarchy(long[] Ids)
        {
            return (DB_DEVICE_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchy",
                new DB.AnalyzeDataSet(GetDeviceHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY", Ids)
					}
            );
        }

        private object GetDeviceHierarchy(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_HIERARCHY[] list = new DB_DEVICE_HIERARCHY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_HIERARCHY insert = new DB_DEVICE_HIERARCHY();
                insert.ID_DEVICE_HIERARCHY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_HIERARCHY"]);
                insert.SERIAL_NBR_PARENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR_PARENT"]);
                insert.ID_SLOT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SLOT_TYPE"]);
                insert.SLOT_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["SLOT_NBR"]);
                insert.ID_PROTOCOL_IN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_IN"]);
                insert.ID_PROTOCOL_OUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_OUT"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceHierarchy(DB_DEVICE_HIERARCHY ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_HIERARCHY", ToBeSaved.ID_DEVICE_HIERARCHY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceHierarchy",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR_PARENT", ToBeSaved.SERIAL_NBR_PARENT)
														,new DB.InParameter("@ID_SLOT_TYPE", ToBeSaved.ID_SLOT_TYPE)
														,new DB.InParameter("@SLOT_NBR", ToBeSaved.SLOT_NBR)
														,new DB.InParameter("@ID_PROTOCOL_IN", ToBeSaved.ID_PROTOCOL_IN)
														,new DB.InParameter("@ID_PROTOCOL_OUT", ToBeSaved.ID_PROTOCOL_OUT)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
									},
                                    sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_HIERARCHY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceHierarchy(DB_DEVICE_HIERARCHY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceHierarchy",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_HIERARCHY", toBeDeleted.ID_DEVICE_HIERARCHY)			
		}
            );
        }

        #endregion
        #region Table DEVICE_HIERARCHY_GROUP

        public DB_DEVICE_HIERARCHY_GROUP[] GetDeviceHierarchyGroup()
        {
            return (DB_DEVICE_HIERARCHY_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyGroup",
                new DB.AnalyzeDataSet(GetDeviceHierarchyGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_DEVICE_HIERARCHY_GROUP[] GetDeviceHierarchyGroup(long[] Ids)
        {
            return (DB_DEVICE_HIERARCHY_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyGroup",
                new DB.AnalyzeDataSet(GetDeviceHierarchyGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetDeviceHierarchyGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_HIERARCHY_GROUP[] list = new DB_DEVICE_HIERARCHY_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_HIERARCHY_GROUP insert = new DB_DEVICE_HIERARCHY_GROUP();
                insert.ID_DEVICE_HIERARCHY_GROUP = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_HIERARCHY_GROUP"]);
                insert.DEVICE_HIERARCHY_SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_HIERARCHY_SERIAL_NBR"]);
                insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DEPOSITORY_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEPOSITORY_LOCATION"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.FINISH_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["FINISH_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceHierarchyGroup(DB_DEVICE_HIERARCHY_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_HIERARCHY_GROUP", ToBeSaved.ID_DEVICE_HIERARCHY_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceHierarchyGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@DEVICE_HIERARCHY_SERIAL_NBR", ToBeSaved.DEVICE_HIERARCHY_SERIAL_NBR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DEPOSITORY_LOCATION", ToBeSaved.ID_DEPOSITORY_LOCATION)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@FINISH_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.FINISH_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_HIERARCHY_GROUP = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteDeviceHierarchyGroup(DB_DEVICE_HIERARCHY_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceHierarchyGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_HIERARCHY_GROUP", toBeDeleted.ID_DEVICE_HIERARCHY_GROUP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_HIERARCHY_IN_GROUP

        public DB_DEVICE_HIERARCHY_IN_GROUP[] GetDeviceHierarchyInGroup()
        {
            return (DB_DEVICE_HIERARCHY_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyInGroup",
                new DB.AnalyzeDataSet(GetDeviceHierarchyInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", new long[] {})
					}
            );
        }

        public DB_DEVICE_HIERARCHY_IN_GROUP[] GetDeviceHierarchyInGroup(long[] Ids)
        {
            return (DB_DEVICE_HIERARCHY_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyInGroup",
                new DB.AnalyzeDataSet(GetDeviceHierarchyInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_GROUP", Ids)
					}
            );
        }


        public long SaveDeviceHierarchyInGroup(DB_DEVICE_HIERARCHY_IN_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_HIERARCHY_GROUP", ToBeSaved.ID_DEVICE_HIERARCHY_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceHierarchyInGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_HIERARCHY", ToBeSaved.ID_DEVICE_HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_HIERARCHY_GROUP = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        private object GetDeviceHierarchyInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_HIERARCHY_IN_GROUP[] list = new DB_DEVICE_HIERARCHY_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_HIERARCHY_IN_GROUP insert = new DB_DEVICE_HIERARCHY_IN_GROUP();
                insert.ID_DEVICE_HIERARCHY_GROUP = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_HIERARCHY_GROUP"]);
                insert.ID_DEVICE_HIERARCHY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_HIERARCHY"]);
                list[i] = insert;
            }
            return list;
        }


        public void DeleteDeviceHierarchyInGroup(DB_DEVICE_HIERARCHY_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceHierarchyInGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_HIERARCHY_GROUP", toBeDeleted.ID_DEVICE_HIERARCHY_GROUP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_HIERARCHY_PATTERN

        public DB_DEVICE_HIERARCHY_PATTERN[] GetDeviceHierarchyPattern()
        {
            return (DB_DEVICE_HIERARCHY_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyPattern",
                new DB.AnalyzeDataSet(GetDeviceHierarchyPattern),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_PATTERN", new long[] {})
					}
            );
        }

        public DB_DEVICE_HIERARCHY_PATTERN[] GetDeviceHierarchyPattern(long[] Ids)
        {
            return (DB_DEVICE_HIERARCHY_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDeviceHierarchyPattern",
                new DB.AnalyzeDataSet(GetDeviceHierarchyPattern),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_HIERARCHY_PATTERN", Ids)
					}
            );
        }

        private object GetDeviceHierarchyPattern(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_HIERARCHY_PATTERN[] list = new DB_DEVICE_HIERARCHY_PATTERN[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_HIERARCHY_PATTERN insert = new DB_DEVICE_HIERARCHY_PATTERN();
                insert.ID_DEVICE_HIERARCHY_PATTERN = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_HIERARCHY_PATTERN"]);
                insert.ID_DEVICE_TYPE_PARENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PARENT"]);
                insert.ID_SLOT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SLOT_TYPE"]);
                insert.SLOT_NBR_MIN = GetValue<int>(QueryResult.Tables[0].Rows[i]["SLOT_NBR_MIN"]);
                insert.SLOT_NBR_MAX = GetValue<int>(QueryResult.Tables[0].Rows[i]["SLOT_NBR_MAX"]);
                insert.ID_PROTOCOL_IN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_IN"]);
                insert.ID_PROTOCOL_OUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_OUT"]);
                insert.ID_DEVICE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DEVICE_TYPE_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_GROUP"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceHierarchyPattern(DB_DEVICE_HIERARCHY_PATTERN ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_HIERARCHY_PATTERN", ToBeSaved.ID_DEVICE_HIERARCHY_PATTERN);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceHierarchyPattern",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE_PARENT", ToBeSaved.ID_DEVICE_TYPE_PARENT)
														,new DB.InParameter("@ID_SLOT_TYPE", ToBeSaved.ID_SLOT_TYPE)
														,new DB.InParameter("@SLOT_NBR_MIN", ToBeSaved.SLOT_NBR_MIN)
														,new DB.InParameter("@SLOT_NBR_MAX", ToBeSaved.SLOT_NBR_MAX)
														,new DB.InParameter("@ID_PROTOCOL_IN", ToBeSaved.ID_PROTOCOL_IN)
														,new DB.InParameter("@ID_PROTOCOL_OUT", ToBeSaved.ID_PROTOCOL_OUT)
														,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@ID_DEVICE_TYPE_GROUP", ToBeSaved.ID_DEVICE_TYPE_GROUP)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_HIERARCHY_PATTERN = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceHierarchyPattern(DB_DEVICE_HIERARCHY_PATTERN toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceHierarchyPattern",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_HIERARCHY_PATTERN", toBeDeleted.ID_DEVICE_HIERARCHY_PATTERN)			
		}
            );
        }

        #endregion
        #region Table DEVICE_ORDER_NUMBER

        public DB_DEVICE_ORDER_NUMBER[] GetDeviceOrderNumber(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_ORDER_NUMBER[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumber",
                new DB.AnalyzeDataSet(GetDeviceOrderNumber),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_ORDER_NUMBER[] GetDeviceOrderNumber(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_ORDER_NUMBER[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumber",
                new DB.AnalyzeDataSet(GetDeviceOrderNumber),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDeviceOrderNumber(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_ORDER_NUMBER[] list = new DB_DEVICE_ORDER_NUMBER[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_ORDER_NUMBER insert = new DB_DEVICE_ORDER_NUMBER();
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.FIRST_QUARTER = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIRST_QUARTER"]);
                insert.SECOND_QUARTER = GetValue<string>(QueryResult.Tables[0].Rows[i]["SECOND_QUARTER"]);
                insert.THIRD_QUARTER = GetValue<string>(QueryResult.Tables[0].Rows[i]["THIRD_QUARTER"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceOrderNumber(DB_DEVICE_ORDER_NUMBER ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceOrderNumber",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@FIRST_QUARTER", ToBeSaved.FIRST_QUARTER)
														,new DB.InParameter("@SECOND_QUARTER", ToBeSaved.SECOND_QUARTER)
														,new DB.InParameter("@THIRD_QUARTER", ToBeSaved.THIRD_QUARTER)
									}
                                    , sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_ORDER_NUMBER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceOrderNumber(DB_DEVICE_ORDER_NUMBER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceOrderNumber",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", toBeDeleted.ID_DEVICE_ORDER_NUMBER)			
		}
            );
        }

        #endregion
        #region Table DEVICE_ORDER_NUMBER_DATA

        public DB_DEVICE_ORDER_NUMBER_DATA[] GetDeviceOrderNumberData()
        {
            return (DB_DEVICE_ORDER_NUMBER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberData",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER_DATA", new long[] {})
					}
            );
        }

        public DB_DEVICE_ORDER_NUMBER_DATA[] GetDeviceOrderNumberData(long[] Ids)
        {
            return (DB_DEVICE_ORDER_NUMBER_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberData",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER_DATA", Ids)
					}
            );
        }



        private object GetDeviceOrderNumberData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_ORDER_NUMBER_DATA[] list = new DB_DEVICE_ORDER_NUMBER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_ORDER_NUMBER_DATA insert = new DB_DEVICE_ORDER_NUMBER_DATA();
                insert.ID_DEVICE_ORDER_NUMBER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER_DATA"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }


        public long SaveDeviceOrderNumberData(DB_DEVICE_ORDER_NUMBER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_ORDER_NUMBER_DATA", ToBeSaved.ID_DEVICE_ORDER_NUMBER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceOrderNumberData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_ORDER_NUMBER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteDeviceOrderNumberData(DB_DEVICE_ORDER_NUMBER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceOrderNumberData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_ORDER_NUMBER_DATA", toBeDeleted.ID_DEVICE_ORDER_NUMBER_DATA)			
		}
            );
        }

        #endregion
        #region Table DEVICE_ORDER_NUMBER_IN_ARTICLE

        public DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] GetDeviceOrderNumberInArticle()
        {
            return (DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberInArticle",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberInArticle),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", new int[] {})
					}
            );
        }

        public DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] GetDeviceOrderNumberInArticle(int[] Ids)
        {
            return (DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[])DB.ExecuteProcedure(
                "imrse_GetDeviceOrderNumberInArticle",
                new DB.AnalyzeDataSet(GetDeviceOrderNumberInArticle),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", Ids)
					}
            );
        }



        private object GetDeviceOrderNumberInArticle(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] list = new DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_ORDER_NUMBER_IN_ARTICLE insert = new DB_DEVICE_ORDER_NUMBER_IN_ARTICLE();
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_ARTICLE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceOrderNumberInArticle(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceOrderNumberInArticle",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ARTICLE", ToBeSaved.ID_ARTICLE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_ORDER_NUMBER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }




        public void DeleteDeviceOrderNumberInArticle(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceOrderNumberInArticle",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", toBeDeleted.ID_DEVICE_ORDER_NUMBER)			
		}
            );
        }




        #endregion
        #region Table DEVICE_PATTERN

        public DB_DEVICE_PATTERN[] GetDevicePattern()
        {
            return (DB_DEVICE_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDevicePattern",
                new DB.AnalyzeDataSet(GetDevicePattern),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PATTERN", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_DEVICE_PATTERN[] GetDevicePattern(long[] Ids)
        {
            return (DB_DEVICE_PATTERN[])DB.ExecuteProcedure(
                "imrse_GetDevicePattern",
                new DB.AnalyzeDataSet(GetDevicePattern),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PATTERN", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetDevicePattern(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_PATTERN[] list = new DB_DEVICE_PATTERN[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_PATTERN insert = new DB_DEVICE_PATTERN();
                insert.ID_PATTERN = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PATTERN"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.CREATED_BY = GetValue<int>(QueryResult.Tables[0].Rows[i]["CREATED_BY"]);
                insert.ID_TEMPLATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TEMPLATE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDevicePattern(DB_DEVICE_PATTERN ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PATTERN", ToBeSaved.ID_PATTERN);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDevicePattern",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@CREATED_BY", ToBeSaved.CREATED_BY)
														,new DB.InParameter("@ID_TEMPLATE", ToBeSaved.ID_TEMPLATE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PATTERN = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDevicePattern(DB_DEVICE_PATTERN toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDevicePattern",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PATTERN", toBeDeleted.ID_PATTERN)			
		}
            );
        }

        #endregion
        #region Table DEVICE_PATTERN_DATA

        public DB_DEVICE_PATTERN_DATA[] GetDevicePatternData()
        {
            return (DB_DEVICE_PATTERN_DATA[])DB.ExecuteProcedure(
                "imrse_GetDevicePatternData",
                new DB.AnalyzeDataSet(GetDevicePatternData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PATTERN", new long[] {})
					}
            );
        }

        public DB_DEVICE_PATTERN_DATA[] GetDevicePatternData(long[] Ids)
        {
            return (DB_DEVICE_PATTERN_DATA[])DB.ExecuteProcedure(
                "imrse_GetDevicePatternData",
                new DB.AnalyzeDataSet(GetDevicePatternData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PATTERN", Ids)
					}
            );
        }



        private object GetDevicePatternData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_PATTERN_DATA[] list = new DB_DEVICE_PATTERN_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_PATTERN_DATA insert = new DB_DEVICE_PATTERN_DATA();
                insert.ID_PATTERN = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PATTERN"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDevicePatternData(DB_DEVICE_PATTERN_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PATTERN", ToBeSaved.ID_PATTERN);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDevicePatternData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PATTERN = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDevicePatternData(DB_DEVICE_PATTERN_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDevicePatternData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PATTERN", toBeDeleted.ID_PATTERN),			
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE),			
            new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)			
		}
            );
        }

        #endregion
        #region Table DEVICE_SIM_CARD_HISTORY

        public DB_DEVICE_SIM_CARD_HISTORY[] GetDeviceSimCardHistory()
        {
            return (DB_DEVICE_SIM_CARD_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceSimCardHistory",
                new DB.AnalyzeDataSet(GetDeviceSimCardHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_SIM_CARD_HISTORY", new long[] {})
					}
            );
        }

        public DB_DEVICE_SIM_CARD_HISTORY[] GetDeviceSimCardHistory(long[] Ids)
        {
            return (DB_DEVICE_SIM_CARD_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceSimCardHistory",
                new DB.AnalyzeDataSet(GetDeviceSimCardHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_SIM_CARD_HISTORY", Ids)
					}
            );
        }



        private object GetDeviceSimCardHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_SIM_CARD_HISTORY[] list = new DB_DEVICE_SIM_CARD_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_SIM_CARD_HISTORY insert = new DB_DEVICE_SIM_CARD_HISTORY();
                insert.ID_DEVICE_SIM_CARD_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_SIM_CARD_HISTORY"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SIM_CARD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceSimCardHistory(DB_DEVICE_SIM_CARD_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_SIM_CARD_HISTORY", ToBeSaved.ID_DEVICE_SIM_CARD_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceSimCardHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_SIM_CARD", ToBeSaved.ID_SIM_CARD)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
													,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_SIM_CARD_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceSimCardHistory(DB_DEVICE_SIM_CARD_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceSimCardHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_SIM_CARD_HISTORY", toBeDeleted.ID_DEVICE_SIM_CARD_HISTORY)			
		}
            );
        }

        #endregion
        #region Table DEVICE_STATE_HISTORY

        public DB_DEVICE_STATE_HISTORY[] GetDeviceStateHistory()
        {
            return (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceStateHistory",
                new DB.AnalyzeDataSet(GetDeviceStateHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_STATE", new long[] {})
					}
            );
        }

        public DB_DEVICE_STATE_HISTORY[] GetDeviceStateHistory(long[] Ids)
        {
            return (DB_DEVICE_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetDeviceStateHistory",
                new DB.AnalyzeDataSet(GetDeviceStateHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_STATE", Ids)
					}
            );
        }

        private object GetDeviceStateHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_STATE_HISTORY[] list = new DB_DEVICE_STATE_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_STATE_HISTORY insert = new DB_DEVICE_STATE_HISTORY();
                insert.ID_DEVICE_STATE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDeviceStateHistory(DB_DEVICE_STATE_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_STATE", ToBeSaved.ID_DEVICE_STATE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceStateHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DEVICE_STATE_TYPE", ToBeSaved.ID_DEVICE_STATE_TYPE)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_STATE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceStateHistory(DB_DEVICE_STATE_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceStateHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_STATE", toBeDeleted.ID_DEVICE_STATE)			
		}
            );
        }

        #endregion
        #region Table DEVICE_STATE_TYPE

        public DB_DEVICE_STATE_TYPE[] GetDeviceStateType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceStateType",
                new DB.AnalyzeDataSet(GetDeviceStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_STATE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_STATE_TYPE[] GetDeviceStateType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceStateType",
                new DB.AnalyzeDataSet(GetDeviceStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_STATE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDeviceStateType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_STATE_TYPE[] list = new DB_DEVICE_STATE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_STATE_TYPE insert = new DB_DEVICE_STATE_TYPE();
                insert.ID_DEVICE_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_STATE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteDeviceStateType(DB_DEVICE_STATE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceStateType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_STATE_TYPE", toBeDeleted.ID_DEVICE_STATE_TYPE)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TEMPLATE

        public DB_DEVICE_TEMPLATE[] GetDeviceTemplate()
        {
            return (DB_DEVICE_TEMPLATE[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplate",
                new DB.AnalyzeDataSet(GetDeviceTemplate),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TEMPLATE", new int[] {})
					}
            );
        }

        public DB_DEVICE_TEMPLATE[] GetDeviceTemplate(int[] Ids)
        {
            return (DB_DEVICE_TEMPLATE[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplate",
                new DB.AnalyzeDataSet(GetDeviceTemplate),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TEMPLATE", Ids)
					}
            );
        }



        private object GetDeviceTemplate(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TEMPLATE[] list = new DB_DEVICE_TEMPLATE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TEMPLATE insert = new DB_DEVICE_TEMPLATE();
                insert.ID_TEMPLATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TEMPLATE"]);
                insert.ID_TEMPLATE_PATTERN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TEMPLATE_PATTERN"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.FIRMWARE_VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIRMWARE_VERSION"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.CREATED_BY = GetValue<int>(QueryResult.Tables[0].Rows[i]["CREATED_BY"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.CONFIRMED_BY = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["CONFIRMED_BY"]);
                insert.CONFIRM_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["CONFIRM_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTemplate(DB_DEVICE_TEMPLATE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TEMPLATE", ToBeSaved.ID_TEMPLATE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTemplate",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TEMPLATE_PATTERN", ToBeSaved.ID_TEMPLATE_PATTERN)
														,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@FIRMWARE_VERSION", ToBeSaved.FIRMWARE_VERSION)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@CREATED_BY", ToBeSaved.CREATED_BY)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@CONFIRMED_BY", ToBeSaved.CONFIRMED_BY)
														,new DB.InParameter("@CONFIRM_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CONFIRM_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TEMPLATE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTemplate(DB_DEVICE_TEMPLATE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTemplate",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TEMPLATE", toBeDeleted.ID_TEMPLATE)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TEMPLATE_DATA

        public DB_DEVICE_TEMPLATE_DATA[] GetDeviceTemplateData()
        {
            return (DB_DEVICE_TEMPLATE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplateData",
                new DB.AnalyzeDataSet(GetDeviceTemplateData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TEMPLATE", new int[] {})
					}
            );
        }

        public DB_DEVICE_TEMPLATE_DATA[] GetDeviceTemplateData(int[] Ids)
        {
            return (DB_DEVICE_TEMPLATE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTemplateData",
                new DB.AnalyzeDataSet(GetDeviceTemplateData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TEMPLATE", Ids)
					}
            );
        }



        private object GetDeviceTemplateData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TEMPLATE_DATA[] list = new DB_DEVICE_TEMPLATE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TEMPLATE_DATA insert = new DB_DEVICE_TEMPLATE_DATA();
                insert.ID_TEMPLATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TEMPLATE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTemplateData(DB_DEVICE_TEMPLATE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TEMPLATE", ToBeSaved.ID_TEMPLATE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTemplateData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TEMPLATE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTemplateData(DB_DEVICE_TEMPLATE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTemplateData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TEMPLATE", toBeDeleted.ID_TEMPLATE),			
            new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE),			
            new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE

        public DB_DEVICE_TYPE[] GetDeviceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceType",
                new DB.AnalyzeDataSet(GetDeviceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_TYPE[] GetDeviceType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetDeviceType",
                new DB.AnalyzeDataSet(GetDeviceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetDeviceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE[] list = new DB_DEVICE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE insert = new DB_DEVICE_TYPE();
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DEVICE_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_CLASS"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.TWO_WAY_TRANS_AVAILABLE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["TWO_WAY_TRANS_AVAILABLE"]);
                insert.ID_DEVICE_DRIVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_DRIVER"]);
                insert.ID_PROTOCOL = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL"]);
                insert.DEFAULT_ID_DEVICE_ODER_NUMBER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["DEFAULT_ID_DEVICE_ODER_NUMBER"]);
                //Projekt: DEVICE_TYPE bez FRAME_DEFINITION
                //insert.FRAME_DEFINITION = GetValue(QueryResult.Tables[0].Rows[i]["FRAME_DEFINITION"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteDeviceType(DB_DEVICE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE", toBeDeleted.ID_DEVICE_TYPE)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_CLASS

        public DB_DEVICE_TYPE_CLASS[] GetDeviceTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeClass",
                new DB.AnalyzeDataSet(GetDeviceTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_CLASS", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout

            );
        }

        public DB_DEVICE_TYPE_CLASS[] GetDeviceTypeClass(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeClass",
                new DB.AnalyzeDataSet(GetDeviceTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_CLASS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout

            );
        }

        private object GetDeviceTypeClass(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_CLASS[] list = new DB_DEVICE_TYPE_CLASS[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_CLASS insert = new DB_DEVICE_TYPE_CLASS();
                insert.ID_DEVICE_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteDeviceTypeClass(DB_DEVICE_TYPE_CLASS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeClass",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_CLASS", toBeDeleted.ID_DEVICE_TYPE_CLASS)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_DATA

        public DB_DEVICE_TYPE_DATA[] GetDeviceTypeData()
        {
            return (DB_DEVICE_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeData",
                new DB.AnalyzeDataSet(GetDeviceTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_DATA", new long[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_DATA[] GetDeviceTypeData(long[] Ids)
        {
            return (DB_DEVICE_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeData",
                new DB.AnalyzeDataSet(GetDeviceTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_DATA", Ids)
					}
            );
        }



        private object GetDeviceTypeData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_DATA[] list = new DB_DEVICE_TYPE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_DATA insert = new DB_DEVICE_TYPE_DATA();
                insert.ID_DEVICE_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_DATA"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public DB_DEVICE_TYPE_DATA[] GetDeviceTypeDataFilter(long[] IdDeviceTypeData, int[] IdDeviceType, long[] IdDataType, int[] IndexNbr)
        {
            return (DB_DEVICE_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeDataFilter",
                new DB.AnalyzeDataSet(GetDeviceTypeData),
                new DB.Parameter[] { 
					CreateTableParam("@ID_DEVICE_TYPE_DATA", IdDeviceTypeData),
					CreateTableParam("@ID_DEVICE_TYPE", IdDeviceType),
					CreateTableParam("@ID_DATA_TYPE", IdDataType),
					CreateTableParam("@INDEX_NBR", IndexNbr)		}
            );
        }

        public long SaveDeviceTypeData(DB_DEVICE_TYPE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_DATA", ToBeSaved.ID_DEVICE_TYPE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTypeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDeviceTypeData(DB_DEVICE_TYPE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_DATA", toBeDeleted.ID_DEVICE_TYPE_DATA)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_GROUP

        public DB_DEVICE_TYPE_GROUP[] GetDeviceTypeGroup()
        {
            return (DB_DEVICE_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeGroup",
                new DB.AnalyzeDataSet(GetDeviceTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_GROUP[] GetDeviceTypeGroup(int[] Ids)
        {
            return (DB_DEVICE_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeGroup",
                new DB.AnalyzeDataSet(GetDeviceTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_GROUP", Ids)
					}
            );
        }

        private object GetDeviceTypeGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_GROUP[] list = new DB_DEVICE_TYPE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_GROUP insert = new DB_DEVICE_TYPE_GROUP();
                insert.ID_DEVICE_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteDeviceTypeGroup(DB_DEVICE_TYPE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_GROUP", toBeDeleted.ID_DEVICE_TYPE_GROUP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_IN_GROUP

        public DB_DEVICE_TYPE_IN_GROUP[] GetDeviceTypeInGroup()
        {
            return (DB_DEVICE_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeInGroup",
                new DB.AnalyzeDataSet(GetDeviceTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_IN_GROUP[] GetDeviceTypeInGroup(int[] Ids)
        {
            return (DB_DEVICE_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeInGroup",
                new DB.AnalyzeDataSet(GetDeviceTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_GROUP", Ids)
					}
            );
        }

        private object GetDeviceTypeInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_IN_GROUP[] list = new DB_DEVICE_TYPE_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_IN_GROUP insert = new DB_DEVICE_TYPE_IN_GROUP();
                insert.ID_DEVICE_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_GROUP"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion
        #region Table DEVICE_TYPE_PROFILE_MAP

        public DB_DEVICE_TYPE_PROFILE_MAP[] GetDeviceTypeProfileMap()
        {
            return (DB_DEVICE_TYPE_PROFILE_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMap",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_PROFILE_MAP[] GetDeviceTypeProfileMap(int[] Ids)
        {
            return (DB_DEVICE_TYPE_PROFILE_MAP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMap",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMap),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", Ids)
					}
            );
        }



        private object GetDeviceTypeProfileMap(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_PROFILE_MAP[] list = new DB_DEVICE_TYPE_PROFILE_MAP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_PROFILE_MAP insert = new DB_DEVICE_TYPE_PROFILE_MAP();
                insert.ID_DEVICE_TYPE_PROFILE_MAP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_MAP"]);
                insert.ID_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE"]);
                insert.ID_OPERATOR_CREATED = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATED"]);
                insert.CREATED_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATED_TIME"]));
                insert.ID_OPERATOR_MODIFIED = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_MODIFIED"]);
                insert.MODIFIED_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["MODIFIED_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTypeProfileMap(DB_DEVICE_TYPE_PROFILE_MAP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_PROFILE_MAP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_MAP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTypeProfileMap",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE", ToBeSaved.ID_DEVICE_TYPE)
														,new DB.InParameter("@ID_OPERATOR_CREATED", ToBeSaved.ID_OPERATOR_CREATED)
														,new DB.InParameter("@CREATED_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.CREATED_TIME))
														,new DB.InParameter("@ID_OPERATOR_MODIFIED", ToBeSaved.ID_OPERATOR_MODIFIED)
														,new DB.InParameter("@MODIFIED_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.MODIFIED_TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_PROFILE_MAP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTypeProfileMap(DB_DEVICE_TYPE_PROFILE_MAP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeProfileMap",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_MAP", toBeDeleted.ID_DEVICE_TYPE_PROFILE_MAP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_PROFILE_MAP_CONFIG

        public DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] GetDeviceTypeProfileMapConfig()
        {
            return (DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMapConfig",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMapConfig),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] GetDeviceTypeProfileMapConfig(int[] Ids)
        {
            return (DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileMapConfig",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileMapConfig),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_MAP", Ids)
					}
            );
        }



        private object GetDeviceTypeProfileMapConfig(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] list = new DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_PROFILE_MAP_CONFIG insert = new DB_DEVICE_TYPE_PROFILE_MAP_CONFIG();
                insert.ID_DEVICE_TYPE_PROFILE_MAP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_MAP"]);
                insert.ID_DEVICE_TYPE_PROFILE_STEP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP"]);
                insert.ID_DEVICE_TYPE_PROFILE_STEP_KIND = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP_KIND"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTypeProfileMapConfig(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_PROFILE_MAP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_MAP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTypeProfileMapConfig",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP)
														,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP_KIND)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_PROFILE_MAP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTypeProfileMapConfig(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeProfileMapConfig",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_MAP", toBeDeleted.ID_DEVICE_TYPE_PROFILE_MAP)			
            ,new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", toBeDeleted.ID_DEVICE_TYPE_PROFILE_STEP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_PROFILE_STEP

        public DB_DEVICE_TYPE_PROFILE_STEP[] GetDeviceTypeProfileStep()
        {
            return (DB_DEVICE_TYPE_PROFILE_STEP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStep",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStep),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_PROFILE_STEP[] GetDeviceTypeProfileStep(int[] Ids)
        {
            return (DB_DEVICE_TYPE_PROFILE_STEP[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStep",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStep),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP", Ids)
					}
            );
        }



        private object GetDeviceTypeProfileStep(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_PROFILE_STEP[] list = new DB_DEVICE_TYPE_PROFILE_STEP[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_PROFILE_STEP insert = new DB_DEVICE_TYPE_PROFILE_STEP();
                insert.ID_DEVICE_TYPE_PROFILE_STEP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTypeProfileStep(DB_DEVICE_TYPE_PROFILE_STEP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_PROFILE_STEP", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTypeProfileStep",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTypeProfileStep(DB_DEVICE_TYPE_PROFILE_STEP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeProfileStep",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP", toBeDeleted.ID_DEVICE_TYPE_PROFILE_STEP)			
		}
            );
        }

        #endregion
        #region Table DEVICE_TYPE_PROFILE_STEP_KIND

        public DB_DEVICE_TYPE_PROFILE_STEP_KIND[] GetDeviceTypeProfileStepKind()
        {
            return (DB_DEVICE_TYPE_PROFILE_STEP_KIND[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStepKind",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStepKind),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", new int[] {})
					}
            );
        }

        public DB_DEVICE_TYPE_PROFILE_STEP_KIND[] GetDeviceTypeProfileStepKind(int[] Ids)
        {
            return (DB_DEVICE_TYPE_PROFILE_STEP_KIND[])DB.ExecuteProcedure(
                "imrse_GetDeviceTypeProfileStepKind",
                new DB.AnalyzeDataSet(GetDeviceTypeProfileStepKind),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", Ids)
					}
            );
        }



        private object GetDeviceTypeProfileStepKind(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_TYPE_PROFILE_STEP_KIND[] list = new DB_DEVICE_TYPE_PROFILE_STEP_KIND[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_TYPE_PROFILE_STEP_KIND insert = new DB_DEVICE_TYPE_PROFILE_STEP_KIND();
                insert.ID_DEVICE_TYPE_PROFILE_STEP_KIND = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_TYPE_PROFILE_STEP_KIND"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceTypeProfileStepKind(DB_DEVICE_TYPE_PROFILE_STEP_KIND ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP_KIND);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceTypeProfileStepKind",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_TYPE_PROFILE_STEP_KIND = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceTypeProfileStepKind(DB_DEVICE_TYPE_PROFILE_STEP_KIND toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceTypeProfileStepKind",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_TYPE_PROFILE_STEP_KIND", toBeDeleted.ID_DEVICE_TYPE_PROFILE_STEP_KIND)			
		}
            );
        }

        #endregion
        #region Table DEVICE_WARRANTY
        public DB_DEVICE_WARRANTY[] GetDeviceWarranty(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_WARRANTY[])DB.ExecuteProcedure(
                "imrse_GetDeviceWarranty",
                new DB.AnalyzeDataSet(GetDeviceWarranty),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_WARRANTY", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DEVICE_WARRANTY[] GetDeviceWarranty(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DEVICE_WARRANTY[])DB.ExecuteProcedure(
                "imrse_GetDeviceWarranty",
                new DB.AnalyzeDataSet(GetDeviceWarranty),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_WARRANTY", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDeviceWarranty(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DEVICE_WARRANTY[] list = new DB_DEVICE_WARRANTY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DEVICE_WARRANTY insert = new DB_DEVICE_WARRANTY();
                insert.ID_DEVICE_WARRANTY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_WARRANTY"]);
                insert.ID_SHIPPING_LIST = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_COMPONENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_COMPONENT"]);
                insert.ID_CONTRACT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT"]);
                insert.WARRANTY_LENGTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["WARRANTY_LENGTH"]);
                insert.SHIPPING_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SHIPPING_DATE"]));
                insert.INSTALLATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["INSTALLATION_DATE"]));
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        public int SaveDeviceWarranty(DB_DEVICE_WARRANTY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_WARRANTY", ToBeSaved.ID_DEVICE_WARRANTY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDeviceWarranty",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SHIPPING_LIST", ToBeSaved.ID_SHIPPING_LIST)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_COMPONENT", ToBeSaved.ID_COMPONENT)
														,new DB.InParameter("@ID_CONTRACT", ToBeSaved.ID_CONTRACT)
														,new DB.InParameter("@WARRANTY_LENGTH", ToBeSaved.WARRANTY_LENGTH)
														,new DB.InParameter("@SHIPPING_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.SHIPPING_DATE))
														,new DB.InParameter("@INSTALLATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.INSTALLATION_DATE))
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_WARRANTY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDeviceWarranty(DB_DEVICE_WARRANTY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDeviceWarranty",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_WARRANTY", toBeDeleted.ID_DEVICE_WARRANTY)			
		}
            );
        }
        #endregion

        #region Table DIAGNOSTIC_ACTION

        public DB_DIAGNOSTIC_ACTION[] GetDiagnosticAction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticAction",
                new DB.AnalyzeDataSet(GetDiagnosticAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DIAGNOSTIC_ACTION[] GetDiagnosticAction(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticAction",
                new DB.AnalyzeDataSet(GetDiagnosticAction),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDiagnosticAction(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DIAGNOSTIC_ACTION[] list = new DB_DIAGNOSTIC_ACTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_DIAGNOSTIC_ACTION insert = new DB_DIAGNOSTIC_ACTION();
                insert.ID_DIAGNOSTIC_ACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDiagnosticAction(DB_DIAGNOSTIC_ACTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DIAGNOSTIC_ACTION", ToBeSaved.ID_DIAGNOSTIC_ACTION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDiagnosticAction",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DIAGNOSTIC_ACTION = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDiagnosticAction(DB_DIAGNOSTIC_ACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDiagnosticAction",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DIAGNOSTIC_ACTION", toBeDeleted.ID_DIAGNOSTIC_ACTION)			
		}
            );
        }

        #endregion
        #region Table DIAGNOSTIC_ACTION_DATA

        public DB_DIAGNOSTIC_ACTION_DATA[] GetDiagnosticActionData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionData",
                new DB.AnalyzeDataSet(GetDiagnosticActionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DIAGNOSTIC_ACTION_DATA[] GetDiagnosticActionData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_DATA[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionData",
                new DB.AnalyzeDataSet(GetDiagnosticActionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDiagnosticActionData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DIAGNOSTIC_ACTION_DATA[] list = new DB_DIAGNOSTIC_ACTION_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DIAGNOSTIC_ACTION_DATA insert = new DB_DIAGNOSTIC_ACTION_DATA();
                insert.ID_DIAGNOSTIC_ACTION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION_DATA"]);
                insert.ID_DIAGNOSTIC_ACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveDiagnosticActionData(DB_DIAGNOSTIC_ACTION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DIAGNOSTIC_ACTION_DATA", ToBeSaved.ID_DIAGNOSTIC_ACTION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDiagnosticActionData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DIAGNOSTIC_ACTION", ToBeSaved.ID_DIAGNOSTIC_ACTION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DIAGNOSTIC_ACTION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDiagnosticActionData(DB_DIAGNOSTIC_ACTION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDiagnosticActionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DIAGNOSTIC_ACTION_DATA", toBeDeleted.ID_DIAGNOSTIC_ACTION_DATA)			
		}
            );
        }

        #endregion
        #region Table DIAGNOSTIC_ACTION_IN_SERVICE

        public DB_DIAGNOSTIC_ACTION_IN_SERVICE[] GetDiagnosticActionInService(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_IN_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionInService",
                new DB.AnalyzeDataSet(GetDiagnosticActionInService),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DIAGNOSTIC_ACTION_IN_SERVICE[] GetDiagnosticActionInService(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_IN_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionInService",
                new DB.AnalyzeDataSet(GetDiagnosticActionInService),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDiagnosticActionInService(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DIAGNOSTIC_ACTION_IN_SERVICE[] list = new DB_DIAGNOSTIC_ACTION_IN_SERVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_DIAGNOSTIC_ACTION_IN_SERVICE insert = new DB_DIAGNOSTIC_ACTION_IN_SERVICE();
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                insert.ID_DIAGNOSTIC_ACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveDiagnosticActionInService(DB_DIAGNOSTIC_ACTION_IN_SERVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDiagnosticActionInService",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DIAGNOSTIC_ACTION", ToBeSaved.ID_DIAGNOSTIC_ACTION)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }





        public void DeleteDiagnosticActionInService(DB_DIAGNOSTIC_ACTION_IN_SERVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDiagnosticActionInService",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE", toBeDeleted.ID_SERVICE)			
		}
            );
        }




        #endregion
        #region Table DIAGNOSTIC_ACTION_RESULT

        public DB_DIAGNOSTIC_ACTION_RESULT[] GetDiagnosticActionResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionResult",
                new DB.AnalyzeDataSet(GetDiagnosticActionResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION_RESULT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_DIAGNOSTIC_ACTION_RESULT[] GetDiagnosticActionResult(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_DIAGNOSTIC_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetDiagnosticActionResult",
                new DB.AnalyzeDataSet(GetDiagnosticActionResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DIAGNOSTIC_ACTION_RESULT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetDiagnosticActionResult(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DIAGNOSTIC_ACTION_RESULT[] list = new DB_DIAGNOSTIC_ACTION_RESULT[count];
            for (int i = 0; i < count; i++)
            {
                DB_DIAGNOSTIC_ACTION_RESULT insert = new DB_DIAGNOSTIC_ACTION_RESULT();
                insert.ID_DIAGNOSTIC_ACTION_RESULT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION_RESULT"]);
                insert.ID_DIAGNOSTIC_ACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ALLOW_INPUT_TEXT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ALLOW_INPUT_TEXT"]);
                insert.DAMAGE_LEVEL = GetValue<int>(QueryResult.Tables[0].Rows[i]["DAMAGE_LEVEL"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_SERVICE_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_REFERENCE_TYPE"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }

        public int SaveDiagnosticActionResult(DB_DIAGNOSTIC_ACTION_RESULT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DIAGNOSTIC_ACTION_RESULT", ToBeSaved.ID_DIAGNOSTIC_ACTION_RESULT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDiagnosticActionResult",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DIAGNOSTIC_ACTION", ToBeSaved.ID_DIAGNOSTIC_ACTION)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ALLOW_INPUT_TEXT", ToBeSaved.ALLOW_INPUT_TEXT)
														,new DB.InParameter("@DAMAGE_LEVEL", ToBeSaved.DAMAGE_LEVEL)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_SERVICE_REFERENCE_TYPE", ToBeSaved.ID_SERVICE_REFERENCE_TYPE)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DIAGNOSTIC_ACTION_RESULT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDiagnosticActionResult(DB_DIAGNOSTIC_ACTION_RESULT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDiagnosticActionResult",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DIAGNOSTIC_ACTION_RESULT", toBeDeleted.ID_DIAGNOSTIC_ACTION_RESULT)			
		}
            );
        }

        #endregion

        #region Table DISTRIBUTOR_DATA

        public DB_DISTRIBUTOR_DATA[] GetDistributorData()
        {
            return (DB_DISTRIBUTOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetDistributorData",
                new DB.AnalyzeDataSet(GetDistributorData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_DATA", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_DISTRIBUTOR_DATA[] GetDistributorData(long[] Ids)
        {
            return (DB_DISTRIBUTOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetDistributorData",
                new DB.AnalyzeDataSet(GetDistributorData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_DATA", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetDistributorData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DISTRIBUTOR_DATA[] list = new DB_DISTRIBUTOR_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_DISTRIBUTOR_DATA insert = new DB_DISTRIBUTOR_DATA();
                insert.ID_DISTRIBUTOR_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_DATA"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }



        public long SaveDistributorData(DB_DISTRIBUTOR_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR_DATA", ToBeSaved.ID_DISTRIBUTOR_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDistributorData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteDistributorData(DB_DISTRIBUTOR_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDistributorData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DISTRIBUTOR_DATA", toBeDeleted.ID_DISTRIBUTOR_DATA)			
		}
            );
        }

        #endregion
        #region Table DISTRIBUTOR_HIERARCHY

        public DB_DISTRIBUTOR_HIERARCHY[] GetDistributorHierarchy()
        {
            return (DB_DISTRIBUTOR_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetDistributorHierarchy",
                new DB.AnalyzeDataSet(GetDistributorHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_HIERARCHY", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_DISTRIBUTOR_HIERARCHY[] GetDistributorHierarchy(int[] Ids)
        {
            return (DB_DISTRIBUTOR_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetDistributorHierarchy",
                new DB.AnalyzeDataSet(GetDistributorHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_HIERARCHY", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetDistributorHierarchy(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DISTRIBUTOR_HIERARCHY[] list = new DB_DISTRIBUTOR_HIERARCHY[count];
            for (int i = 0; i < count; i++)
            {
                DB_DISTRIBUTOR_HIERARCHY insert = new DB_DISTRIBUTOR_HIERARCHY();
                insert.ID_DISTRIBUTOR_HIERARCHY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_HIERARCHY"]);
                insert.ID_DISTRIBUTOR_HIERARCHY_PARENT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_HIERARCHY_PARENT"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["HIERARCHY"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDistributorHierarchy(DB_DISTRIBUTOR_HIERARCHY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR_HIERARCHY", ToBeSaved.ID_DISTRIBUTOR_HIERARCHY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDistributorHierarchy",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR_HIERARCHY_PARENT", ToBeSaved.ID_DISTRIBUTOR_HIERARCHY_PARENT)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@HIERARCHY", ToBeSaved.HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR_HIERARCHY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDistributorHierarchy(DB_DISTRIBUTOR_HIERARCHY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDistributorHierarchy",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DISTRIBUTOR_HIERARCHY", toBeDeleted.ID_DISTRIBUTOR_HIERARCHY)			
		}
            );
        }

        #endregion
        #region Table DISTRIBUTOR_SUPPLIER

        public DB_DISTRIBUTOR_SUPPLIER[] GetDistributorSupplier()
        {
            return (DB_DISTRIBUTOR_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetDistributorSupplier",
                new DB.AnalyzeDataSet(GetDistributorSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_SUPPLIER", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_DISTRIBUTOR_SUPPLIER[] GetDistributorSupplier(int[] Ids)
        {
            return (DB_DISTRIBUTOR_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetDistributorSupplier",
                new DB.AnalyzeDataSet(GetDistributorSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DISTRIBUTOR_SUPPLIER", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetDistributorSupplier(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_DISTRIBUTOR_SUPPLIER[] list = new DB_DISTRIBUTOR_SUPPLIER[count];
            for (int i = 0; i < count; i++)
            {
                DB_DISTRIBUTOR_SUPPLIER insert = new DB_DISTRIBUTOR_SUPPLIER();
                insert.ID_DISTRIBUTOR_SUPPLIER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_SUPPLIER"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_SUPPLIER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SUPPLIER"]);
                insert.ID_PRODUCT_CODE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRODUCT_CODE"]);
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveDistributorSupplier(DB_DISTRIBUTOR_SUPPLIER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DISTRIBUTOR_SUPPLIER", ToBeSaved.ID_DISTRIBUTOR_SUPPLIER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveDistributorSupplier",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_SUPPLIER", ToBeSaved.ID_SUPPLIER)
														,new DB.InParameter("@ID_PRODUCT_CODE", ToBeSaved.ID_PRODUCT_CODE)
														,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DISTRIBUTOR_SUPPLIER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteDistributorSupplier(DB_DISTRIBUTOR_SUPPLIER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelDistributorSupplier",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DISTRIBUTOR_SUPPLIER", toBeDeleted.ID_DISTRIBUTOR_SUPPLIER)			
		}
            );
        }

        #endregion

        #region Table ERROR_LOG

        public DB_ERROR_LOG[] GetErrorLog()
        {
            return (DB_ERROR_LOG[])DB.ExecuteProcedure(
                "imrse_GetErrorLog",
                new DB.AnalyzeDataSet(GetErrorLog),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ERROR_LOG", new int[] {})
					}
            );
        }

        public DB_ERROR_LOG[] GetErrorLog(int[] Ids)
        {
            return (DB_ERROR_LOG[])DB.ExecuteProcedure(
                "imrse_GetErrorLog",
                new DB.AnalyzeDataSet(GetErrorLog),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ERROR_LOG", Ids)
					}
            );
        }

        private object GetErrorLog(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ERROR_LOG[] list = new DB_ERROR_LOG[count];
            for (int i = 0; i < count; i++)
            {
                DB_ERROR_LOG insert = new DB_ERROR_LOG();
                insert.ID_ERROR_LOG = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ERROR_LOG"]);
                insert.APPLICATION_ID = GetValue<int>(QueryResult.Tables[0].Rows[i]["APPLICATION_ID"]);
                insert.ERROR_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["ERROR_DATE"]));
                insert.CLASS_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["CLASS_NAME"]);
                insert.FUNCTION_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["FUNCTION_NAME"]);
                insert.SOURCE = GetValue<string>(QueryResult.Tables[0].Rows[i]["SOURCE"]);
                insert.MESSAGE = GetValue<string>(QueryResult.Tables[0].Rows[i]["MESSAGE"]);
                insert.STACK_TRACE = GetValue<string>(QueryResult.Tables[0].Rows[i]["STACK_TRACE"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.SEVERITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["SEVERITY"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveErrorLog(DB_ERROR_LOG ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ERROR_LOG", ToBeSaved.ID_ERROR_LOG);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveErrorLog",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@APPLICATION_ID", ToBeSaved.APPLICATION_ID)
														,new DB.InParameter("@ERROR_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.ERROR_DATE))
														,new DB.InParameter("@CLASS_NAME", ToBeSaved.CLASS_NAME)
														,new DB.InParameter("@FUNCTION_NAME", ToBeSaved.FUNCTION_NAME)
														,new DB.InParameter("@SOURCE", ToBeSaved.SOURCE)
														,new DB.InParameter("@MESSAGE", ToBeSaved.MESSAGE)
														,new DB.InParameter("@STACK_TRACE", ToBeSaved.STACK_TRACE)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@SEVERITY", ToBeSaved.SEVERITY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ERROR_LOG = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteErrorLog(DB_ERROR_LOG toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelErrorLog",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ERROR_LOG", toBeDeleted.ID_ERROR_LOG)			
		}
            );
        }

        #endregion

        #region Table ETL

        public DB_ETL[] GetEtl(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ETL[])DB.ExecuteProcedure(
                "imrse_GetEtl",
                new DB.AnalyzeDataSet(GetEtl),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PARAM", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_ETL[] GetEtl(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ETL[])DB.ExecuteProcedure(
                "imrse_GetEtl",
                new DB.AnalyzeDataSet(GetEtl),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PARAM", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetEtl(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ETL[] list = new DB_ETL[count];
            for (int i = 0; i < count; i++)
            {
                DB_ETL insert = new DB_ETL();
                insert.ID_PARAM = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARAM"]);
                insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveEtl(DB_ETL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PARAM", ToBeSaved.ID_PARAM);
            DB.ExecuteNonQueryProcedure(
                "imrse_u_SaveEtl",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@CODE", ToBeSaved.CODE)
														,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
														,ParamObject("@VALUE", ToBeSaved.VALUE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PARAM = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteEtl(DB_ETL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelEtl",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PARAM", toBeDeleted.ID_PARAM)			
		}
            );
        }

        #endregion

        #region Table FAULT_CODE

        public DB_FAULT_CODE[] GetFaultCode()
        {
            return (DB_FAULT_CODE[])DB.ExecuteProcedure(
                "imrse_GetFaultCode",
                new DB.AnalyzeDataSet(GetFaultCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE", new int[] {})
					}
            );
        }

        public DB_FAULT_CODE[] GetFaultCode(int[] Ids)
        {
            return (DB_FAULT_CODE[])DB.ExecuteProcedure(
                "imrse_GetFaultCode",
                new DB.AnalyzeDataSet(GetFaultCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE", Ids)
					}
            );
        }

        private object GetFaultCode(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_FAULT_CODE()
            {
                ID_FAULT_CODE = GetValue<int>(row["ID_FAULT_CODE"]),
                ID_FAULT_CODE_TYPE = GetValue<int>(row["ID_FAULT_CODE_TYPE"]),
                ID_FAULT_CODE_PARENT = GetNullableValue<int>(row["ID_FAULT_CODE_PARENT"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_CLIENT_DESCR = GetNullableValue<long>(row["ID_CLIENT_DESCR"]),
                ID_COMPANY_DESCR = GetNullableValue<long>(row["ID_COMPANY_DESCR"]),
                PRIORITY = GetValue<int>(row["PRIORITY"]),
                FAULT_CODE_HIERARCHY = GetValue<string>(row["FAULT_CODE_HIERARCHY"])
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_FAULT_CODE[] list = new DB_FAULT_CODE[count];
            for (int i = 0; i < count; i++)
            {
                DB_FAULT_CODE insert = new DB_FAULT_CODE();
                insert.ID_FAULT_CODE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE"]);
                insert.ID_FAULT_CODE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_TYPE"]);
                insert.ID_FAULT_CODE_PARENT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_PARENT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_CLIENT_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_CLIENT_DESCR"]);
                insert.ID_COMPANY_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_COMPANY_DESCR"]);
                insert.PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["PRIORITY"]);
                insert.FAULT_CODE_HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["FAULT_CODE_HIERARCHY"]);
                list[i] = insert;
            }
            return list;
             */
            #endregion
        }


        public int SaveFaultCode(DB_FAULT_CODE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_FAULT_CODE", ToBeSaved.ID_FAULT_CODE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveFaultCode",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_FAULT_CODE_TYPE", ToBeSaved.ID_FAULT_CODE_TYPE)
														,new DB.InParameter("@ID_FAULT_CODE_PARENT", ToBeSaved.ID_FAULT_CODE_PARENT)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_CLIENT_DESCR", ToBeSaved.ID_CLIENT_DESCR)
														,new DB.InParameter("@ID_COMPANY_DESCR", ToBeSaved.ID_COMPANY_DESCR)
														,new DB.InParameter("@PRIORITY", ToBeSaved.PRIORITY)
														,new DB.InParameter("@FAULT_CODE_HIERARCHY", ToBeSaved.FAULT_CODE_HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_FAULT_CODE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteFaultCode(DB_FAULT_CODE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelFaultCode",
                new DB.Parameter[] {
            new DB.InParameter("@ID_FAULT_CODE", toBeDeleted.ID_FAULT_CODE)			
		}
            );
        }

        #endregion
        #region Table FAULT_CODE_DATA

        public DB_FAULT_CODE_DATA[] GetFaultCodeData()
        {
            return (DB_FAULT_CODE_DATA[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeData",
                new DB.AnalyzeDataSet(GetFaultCodeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_DATA", new long[] {})
					}
            );
        }

        public DB_FAULT_CODE_DATA[] GetFaultCodeData(long[] Ids)
        {
            return (DB_FAULT_CODE_DATA[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeData",
                new DB.AnalyzeDataSet(GetFaultCodeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_DATA", Ids)
					}
            );
        }



        private object GetFaultCodeData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_FAULT_CODE_DATA()
            {
                ID_FAULT_CODE_DATA = GetValue<long>(row["ID_FAULT_CODE_DATA"]),
                ID_FAULT_CODE = GetValue<int>(row["ID_FAULT_CODE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_FAULT_CODE_DATA[] list = new DB_FAULT_CODE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_FAULT_CODE_DATA insert = new DB_FAULT_CODE_DATA();
									insert.ID_FAULT_CODE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_DATA"]);
												insert.ID_FAULT_CODE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        public long SaveFaultCodeData(DB_FAULT_CODE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_FAULT_CODE_DATA", ToBeSaved.ID_FAULT_CODE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveFaultCodeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_FAULT_CODE", ToBeSaved.ID_FAULT_CODE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_FAULT_CODE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteFaultCodeData(DB_FAULT_CODE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelFaultCodeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_FAULT_CODE_DATA", toBeDeleted.ID_FAULT_CODE_DATA)			
		}
            );
        }

        #endregion
        #region Table FAULT_CODE_SUMMARY

        public DB_FAULT_CODE_SUMMARY[] GetFaultCodeSummary()
        {
            return (DB_FAULT_CODE_SUMMARY[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeSummary",
                new DB.AnalyzeDataSet(GetFaultCodeSummary),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_SUMMARY", new int[] {})
					}
            );
        }

        public DB_FAULT_CODE_SUMMARY[] GetFaultCodeSummary(int[] Ids)
        {
            return (DB_FAULT_CODE_SUMMARY[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeSummary",
                new DB.AnalyzeDataSet(GetFaultCodeSummary),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_SUMMARY", Ids)
					}
            );
        }



        private object GetFaultCodeSummary(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_FAULT_CODE_SUMMARY()
            {
                ID_FAULT_CODE_SUMMARY = GetValue<int>(row["ID_FAULT_CODE_SUMMARY"]),
                ID_FAULT_CODE = GetNullableValue<int>(row["ID_FAULT_CODE"]),
                ID_SERVICE_REFERENCE_TYPE = GetValue<int>(row["ID_SERVICE_REFERENCE_TYPE"]),
                REFERENCE_VALUE = GetValue(row["REFERENCE_VALUE"]),
                FAULTS_COUNT = GetValue<int>(row["FAULTS_COUNT"]),
                TOTAL_COUNT = GetValue<int>(row["TOTAL_COUNT"]),
                ID_FAULT_CODE_SUMMARY_TYPE = GetValue<int>(row["ID_FAULT_CODE_SUMMARY_TYPE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_FAULT_CODE_SUMMARY[] list = new DB_FAULT_CODE_SUMMARY[count];
    for (int i = 0; i < count; i++)
    {
        DB_FAULT_CODE_SUMMARY insert = new DB_FAULT_CODE_SUMMARY();
									insert.ID_FAULT_CODE_SUMMARY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_SUMMARY"]);
												insert.ID_FAULT_CODE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE"]);
												insert.ID_SERVICE_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_REFERENCE_TYPE"]);
												insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
												insert.FAULTS_COUNT = GetValue<int>(QueryResult.Tables[0].Rows[i]["FAULTS_COUNT"]);
												insert.TOTAL_COUNT = GetValue<int>(QueryResult.Tables[0].Rows[i]["TOTAL_COUNT"]);
												insert.ID_FAULT_CODE_SUMMARY_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_SUMMARY_TYPE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        public int SaveFaultCodeSummary(DB_FAULT_CODE_SUMMARY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_FAULT_CODE_SUMMARY", ToBeSaved.ID_FAULT_CODE_SUMMARY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveFaultCodeSummary",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_FAULT_CODE", ToBeSaved.ID_FAULT_CODE)
														,new DB.InParameter("@ID_SERVICE_REFERENCE_TYPE", ToBeSaved.ID_SERVICE_REFERENCE_TYPE)
														,new DB.InParameter("@REFERENCE_VALUE", SqlDbType.Variant, ToBeSaved.REFERENCE_VALUE)
														,new DB.InParameter("@FAULTS_COUNT", ToBeSaved.FAULTS_COUNT)
														,new DB.InParameter("@TOTAL_COUNT", ToBeSaved.TOTAL_COUNT)
														,new DB.InParameter("@ID_FAULT_CODE_SUMMARY_TYPE", ToBeSaved.ID_FAULT_CODE_SUMMARY_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_FAULT_CODE_SUMMARY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        public void DeleteFaultCodeSummary(DB_FAULT_CODE_SUMMARY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelFaultCodeSummary",
                new DB.Parameter[] {
            new DB.InParameter("@ID_FAULT_CODE_SUMMARY", toBeDeleted.ID_FAULT_CODE_SUMMARY)			
		}
            );
        }

        #endregion
        #region Table FAULT_CODE_TYPE

        public DB_FAULT_CODE_TYPE[] GetFaultCodeType()
        {
            return (DB_FAULT_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeType",
                new DB.AnalyzeDataSet(GetFaultCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_TYPE", new int[] {})
					}
            );
        }

        public DB_FAULT_CODE_TYPE[] GetFaultCodeType(int[] Ids)
        {
            return (DB_FAULT_CODE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetFaultCodeType",
                new DB.AnalyzeDataSet(GetFaultCodeType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_FAULT_CODE_TYPE", Ids)
					}
            );
        }



        private object GetFaultCodeType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_FAULT_CODE_TYPE()
            {
                ID_FAULT_CODE_TYPE = GetValue<int>(row["ID_FAULT_CODE_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_FAULT_CODE_TYPE[] list = new DB_FAULT_CODE_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_FAULT_CODE_TYPE insert = new DB_FAULT_CODE_TYPE();
									insert.ID_FAULT_CODE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_FAULT_CODE_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        public int SaveFaultCodeType(DB_FAULT_CODE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_FAULT_CODE_TYPE", ToBeSaved.ID_FAULT_CODE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveFaultCodeType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_FAULT_CODE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        public void DeleteFaultCodeType(DB_FAULT_CODE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelFaultCodeType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_FAULT_CODE_TYPE", toBeDeleted.ID_FAULT_CODE_TYPE)			
		}
            );
        }

        #endregion

        #region Table HOLIDAY_DATE

        public DB_HOLIDAY_DATE[] GetHolidayDate()
        {
            return (DB_HOLIDAY_DATE[])DB.ExecuteProcedure(
                "imrse_GetHolidayDate",
                new DB.AnalyzeDataSet(GetHolidayDate),
                new DB.Parameter[] { 
            CreateTableParam("@DATE", new DateTime[] {})
                    }
            );
        }

        public DB_HOLIDAY_DATE[] GetHolidayDate(DateTime[] Ids)
        {
            return (DB_HOLIDAY_DATE[])DB.ExecuteProcedure(
                "imrse_GetHolidayDate",
                new DB.AnalyzeDataSet(GetHolidayDate),
                new DB.Parameter[] { 
            CreateTableParam("@DATE", Ids)
                    }
            );
        }

        private object GetHolidayDate(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_HOLIDAY_DATE[] list = new DB_HOLIDAY_DATE[count];
            for (int i = 0; i < count; i++)
            {
                DB_HOLIDAY_DATE insert = new DB_HOLIDAY_DATE();
                insert.DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE"]));
                insert.COUNTRY_CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["COUNTRY_CODE"]);
                list[i] = insert;
            }
            return list;
        }


        public DateTime SaveHolidayDate(DB_HOLIDAY_DATE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@DATE", ConvertTimeZoneToUtcTime(ToBeSaved.DATE));
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveHolidayDate",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@COUNTRY_CODE", ToBeSaved.COUNTRY_CODE)
                                    }
            );
            if (!InsertId.IsNull)
                ToBeSaved.DATE = ConvertUtcTimeToTimeZone((DateTime)InsertId.Value);

            return ConvertUtcTimeToTimeZone((DateTime)InsertId.Value);
        }


        public void DeleteHolidayDate(DB_HOLIDAY_DATE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelHolidayDate",
                new DB.Parameter[] {
            new DB.InParameter("@DATE", ConvertTimeZoneToUtcTime(toBeDeleted.DATE))
        }
            );
        }


        #endregion

        #region Table IMR_SERVER

        public DB_IMR_SERVER[] GetImrServer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_IMR_SERVER[])DB.ExecuteProcedure(
                "imrse_GetImrServer",
                new DB.AnalyzeDataSet(GetImrServer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_IMR_SERVER", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_IMR_SERVER[] GetImrServer(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_IMR_SERVER[])DB.ExecuteProcedure(
                "imrse_GetImrServer",
                new DB.AnalyzeDataSet(GetImrServer),
                new DB.Parameter[] { 
			CreateTableParam("@ID_IMR_SERVER", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetImrServer(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_IMR_SERVER[] list = new DB_IMR_SERVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_IMR_SERVER insert = new DB_IMR_SERVER();
                insert.ID_IMR_SERVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER"]);
                insert.IP_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["IP_ADDRESS"]);
                insert.DNS_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["DNS_NAME"]);
                insert.WEBSERVICE_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["WEBSERVICE_ADDRESS"]);
                insert.WEBSERVICE_TIMEOUT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["WEBSERVICE_TIMEOUT"]);
                insert.LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["LOGIN"]);
                insert.PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["PASSWORD"]);
                insert.STANDARD_DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["STANDARD_DESCRIPTION"]);
                insert.IS_GOOD = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_GOOD"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.UTDC_SSEC_USER = GetValue<string>(QueryResult.Tables[0].Rows[i]["UTDC_SSEC_USER"]);
                insert.UTDC_SSEC_PASS = GetValue<string>(QueryResult.Tables[0].Rows[i]["UTDC_SSEC_PASS"]);
                insert.COLLATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["COLLATION"]);
                insert.DB_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["DB_NAME"]);
                insert.IMR_SERVER_VERSION = GetValue<int>(QueryResult.Tables[0].Rows[i]["IMR_SERVER_VERSION"]);
                insert.IS_IMRLT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_IMRLT"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteImrServer(DB_IMR_SERVER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelImrServer",
                new DB.Parameter[] {
            new DB.InParameter("@ID_IMR_SERVER", toBeDeleted.ID_IMR_SERVER)			
		}
            );
        }

        #endregion
        #region Table IMR_SERVER_DATA

        public DB_IMR_SERVER_DATA[] GetImrServerData()
        {
            return (DB_IMR_SERVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetImrServerData",
                new DB.AnalyzeDataSet(GetImrServerData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_IMR_SERVER_DATA", new long[] {})
					}
            );
        }

        public DB_IMR_SERVER_DATA[] GetImrServerData(long[] Ids)
        {
            return (DB_IMR_SERVER_DATA[])DB.ExecuteProcedure(
                "imrse_GetImrServerData",
                new DB.AnalyzeDataSet(GetImrServerData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_IMR_SERVER_DATA", Ids)
					}
            );
        }



        private object GetImrServerData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_IMR_SERVER_DATA[] list = new DB_IMR_SERVER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_IMR_SERVER_DATA insert = new DB_IMR_SERVER_DATA();
                insert.ID_IMR_SERVER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER_DATA"]);
                insert.ID_IMR_SERVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveImrServerData(DB_IMR_SERVER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_IMR_SERVER_DATA", ToBeSaved.ID_IMR_SERVER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveImrServerData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_IMR_SERVER", ToBeSaved.ID_IMR_SERVER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_IMR_SERVER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteImrServerData(DB_IMR_SERVER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelImrServerData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_IMR_SERVER_DATA", toBeDeleted.ID_IMR_SERVER_DATA)			
		}
            );
        }

        #endregion

        #region Table ISSUE

        public DB_ISSUE[] GetIssue(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE[])DB.ExecuteProcedure(
                "imrse_GetIssue",
                new DB.AnalyzeDataSet(GetIssue),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ISSUE[] GetIssue(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE[])DB.ExecuteProcedure(
                "imrse_GetIssue",
                new DB.AnalyzeDataSet(GetIssue),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetIssue(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ISSUE[] list = new DB_ISSUE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ISSUE insert = new DB_ISSUE();
                insert.ID_ISSUE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.ID_OPERATOR_REGISTERING = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_REGISTERING"]);
                insert.ID_OPERATOR_PERFORMER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_PERFORMER"]);
                insert.ID_ISSUE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_TYPE"]);
                insert.ID_ISSUE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_STATUS"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.REALIZATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["REALIZATION_DATE"]));
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.SHORT_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHORT_DESCR"]);
                insert.LONG_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["LONG_DESCR"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.DEADLINE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DEADLINE"]));
                insert.ID_PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRIORITY"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveIssue(DB_ISSUE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE", ToBeSaved.ID_ISSUE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssue",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
														,new DB.InParameter("@ID_OPERATOR_REGISTERING", ToBeSaved.ID_OPERATOR_REGISTERING)
														,new DB.InParameter("@ID_OPERATOR_PERFORMER", ToBeSaved.ID_OPERATOR_PERFORMER)
														,new DB.InParameter("@ID_ISSUE_TYPE", ToBeSaved.ID_ISSUE_TYPE)
														,new DB.InParameter("@ID_ISSUE_STATUS", ToBeSaved.ID_ISSUE_STATUS)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@REALIZATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.REALIZATION_DATE))
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@SHORT_DESCR", ToBeSaved.SHORT_DESCR)
														,new DB.InParameter("@LONG_DESCR", ToBeSaved.LONG_DESCR)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@DEADLINE", ConvertTimeZoneToUtcTime(ToBeSaved.DEADLINE))
														,new DB.InParameter("@ID_PRIORITY", ToBeSaved.ID_PRIORITY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteIssue(DB_ISSUE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssue",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE", toBeDeleted.ID_ISSUE)			
		}
            );
        }

        #endregion
        #region Table ISSUE_DATA

        public DB_ISSUE_DATA[] GetIssueData()
        {
            return (DB_ISSUE_DATA[])DB.ExecuteProcedure(
                "imrse_GetIssueData",
                new DB.AnalyzeDataSet(GetIssueData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_DATA", new long[] {})
					}
            );
        }

        public DB_ISSUE_DATA[] GetIssueData(long[] Ids)
        {
            return (DB_ISSUE_DATA[])DB.ExecuteProcedure(
                "imrse_GetIssueData",
                new DB.AnalyzeDataSet(GetIssueData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_DATA", Ids)
					}
            );
        }

        private object GetIssueData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ISSUE_DATA[] list = new DB_ISSUE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ISSUE_DATA insert = new DB_ISSUE_DATA();
                insert.ID_ISSUE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_DATA"]);
                insert.ID_ISSUE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveIssueData(DB_ISSUE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE_DATA", ToBeSaved.ID_ISSUE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssueData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ISSUE", ToBeSaved.ID_ISSUE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteIssueData(DB_ISSUE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssueData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE_DATA", toBeDeleted.ID_ISSUE_DATA)			
		}
            );
        }

        #endregion
        #region Table ISSUE_GROUP

        public DB_ISSUE_GROUP[] GetIssueGroup(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetIssueGroup",
                new DB.AnalyzeDataSet(GetIssueGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_GROUP", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ISSUE_GROUP[] GetIssueGroup(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetIssueGroup",
                new DB.AnalyzeDataSet(GetIssueGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_GROUP", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }
        
        private object GetIssueGroup(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_ISSUE_GROUP()
            {
                ID_ISSUE_GROUP = GetValue<int>(row["ID_ISSUE_GROUP"]),
                ID_PARENT_GROUP = GetNullableValue<int>(row["ID_PARENT_GROUP"]),
                NAME = GetValue<string>(row["NAME"]),
                DATE_CREATED = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["DATE_CREATED"])),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_ISSUE_GROUP[] list = new DB_ISSUE_GROUP[count];
    for (int i = 0; i < count; i++)
    {
        DB_ISSUE_GROUP insert = new DB_ISSUE_GROUP();
									insert.ID_ISSUE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_GROUP"]);
												insert.ID_PARENT_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARENT_GROUP"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.DATE_CREATED = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE_CREATED"]));
												insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }
        
        public int SaveIssueGroup(DB_ISSUE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE_GROUP", ToBeSaved.ID_ISSUE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssueGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PARENT_GROUP", ToBeSaved.ID_PARENT_GROUP)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DATE_CREATED", ConvertTimeZoneToUtcTime(ToBeSaved.DATE_CREATED))
													,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteIssueGroup(DB_ISSUE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssueGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE_GROUP", toBeDeleted.ID_ISSUE_GROUP)			
		}
            );
        }

        #endregion
        #region Table ISSUE_HISTORY

        public DB_ISSUE_HISTORY[] GetIssueHistory()
        {
            return (DB_ISSUE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetIssueHistory",
                new DB.AnalyzeDataSet(GetIssueHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_HISTORY", new int[] {})
					}
            );
        }

        public DB_ISSUE_HISTORY[] GetIssueHistory(int[] Ids)
        {
            return (DB_ISSUE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetIssueHistory",
                new DB.AnalyzeDataSet(GetIssueHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_HISTORY", Ids)
					}
            );
        }

        private object GetIssueHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ISSUE_HISTORY[] list = new DB_ISSUE_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ISSUE_HISTORY insert = new DB_ISSUE_HISTORY();
                insert.ID_ISSUE_HISTORY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_HISTORY"]);
                insert.ID_ISSUE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
                insert.ID_ISSUE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_STATUS"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.SHORT_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHORT_DESCR"]);
                insert.DEADLINE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DEADLINE"]));
                insert.ID_PRIORITY = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRIORITY"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveIssueHistory(DB_ISSUE_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE_HISTORY", ToBeSaved.ID_ISSUE_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssueHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ISSUE", ToBeSaved.ID_ISSUE)
														,new DB.InParameter("@ID_ISSUE_STATUS", ToBeSaved.ID_ISSUE_STATUS)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@SHORT_DESCR", ToBeSaved.SHORT_DESCR)
														,new DB.InParameter("@DEADLINE", ConvertTimeZoneToUtcTime(ToBeSaved.DEADLINE))
														,new DB.InParameter("@ID_PRIORITY", ToBeSaved.ID_PRIORITY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE_HISTORY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteIssueHistory(DB_ISSUE_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssueHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE_HISTORY", toBeDeleted.ID_ISSUE_HISTORY)			
		}
            );
        }

        #endregion
        #region Table ISSUE_STATUS

        public DB_ISSUE_STATUS[] GetIssueStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetIssueStatus",
                new DB.AnalyzeDataSet(GetIssueStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_STATUS", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ISSUE_STATUS[] GetIssueStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetIssueStatus",
                new DB.AnalyzeDataSet(GetIssueStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_STATUS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetIssueStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ISSUE_STATUS[] list = new DB_ISSUE_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ISSUE_STATUS insert = new DB_ISSUE_STATUS();
                insert.ID_ISSUE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_STATUS"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveIssueStatus(DB_ISSUE_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE_STATUS", ToBeSaved.ID_ISSUE_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssueStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteIssueStatus(DB_ISSUE_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssueStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE_STATUS", toBeDeleted.ID_ISSUE_STATUS)			
		}
            );
        }

        #endregion
        #region Table ISSUE_TYPE

        public DB_ISSUE_TYPE[] GetIssueType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetIssueType",
                new DB.AnalyzeDataSet(GetIssueType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ISSUE_TYPE[] GetIssueType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ISSUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetIssueType",
                new DB.AnalyzeDataSet(GetIssueType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ISSUE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetIssueType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ISSUE_TYPE[] list = new DB_ISSUE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ISSUE_TYPE insert = new DB_ISSUE_TYPE();
                insert.ID_ISSUE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE_TYPE"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveIssueType(DB_ISSUE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ISSUE_TYPE", ToBeSaved.ID_ISSUE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveIssueType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ISSUE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteIssueType(DB_ISSUE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelIssueType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ISSUE_TYPE", toBeDeleted.ID_ISSUE_TYPE)			
		}
            );
        }

        #endregion

        #region Table LANGUAGE

        public DB_LANGUAGE[] GetLanguage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LANGUAGE[])DB.ExecuteProcedure(
                "imrse_GetLanguage",
                new DB.AnalyzeDataSet(GetLanguage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LANGUAGE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_LANGUAGE[] GetLanguage(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LANGUAGE[])DB.ExecuteProcedure(
                "imrse_GetLanguage",
                new DB.AnalyzeDataSet(GetLanguage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LANGUAGE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetLanguage(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LANGUAGE()
            {
                ID_LANGUAGE = GetValue<int>(row["ID_LANGUAGE"]),
                NAME = GetValue<string>(row["NAME"]),
                CULTURE_CODE = GetValue<string>(row["CULTURE_CODE"]),
                CULTURE_LANGID = GetValue<int>(row["CULTURE_LANGID"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_LANGUAGE[] list = new DB_LANGUAGE[count];
    for (int i = 0; i < count; i++)
    {
        DB_LANGUAGE insert = new DB_LANGUAGE();
									insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.CULTURE_CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CULTURE_CODE"]);
												insert.CULTURE_LANGID = GetValue<int>(QueryResult.Tables[0].Rows[i]["CULTURE_LANGID"]);
												insert.ID_FLAG = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FLAG"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public void DeleteLanguage(DB_LANGUAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLanguage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LANGUAGE", toBeDeleted.ID_LANGUAGE)			
		}
            );
        }

        #endregion
        #region Table LANGUAGE_DATA

        public DB_LANGUAGE_DATA[] GetLanguageData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LANGUAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetLanguageData",
                new DB.AnalyzeDataSet(GetLanguageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LANGUAGE_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_LANGUAGE_DATA[] GetLanguageData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LANGUAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetLanguageData",
                new DB.AnalyzeDataSet(GetLanguageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LANGUAGE_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetLanguageData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LANGUAGE_DATA()
            {
                ID_LANGUAGE_DATA = GetValue<long>(row["ID_LANGUAGE_DATA"]),
                ID_LANGUAGE = GetValue<int>(row["ID_LANGUAGE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_LANGUAGE_DATA[] list = new DB_LANGUAGE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_LANGUAGE_DATA insert = new DB_LANGUAGE_DATA();
									insert.ID_LANGUAGE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE_DATA"]);
												insert.ID_LANGUAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LANGUAGE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveLanguageData(DB_LANGUAGE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LANGUAGE_DATA", ToBeSaved.ID_LANGUAGE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLanguageData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LANGUAGE", ToBeSaved.ID_LANGUAGE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LANGUAGE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLanguageData(DB_LANGUAGE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLanguageData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LANGUAGE_DATA", toBeDeleted.ID_LANGUAGE_DATA)			
		}
            );
        }

        #endregion

        #region Table LOCATION
        public DB_LOCATION[] GetLocation(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION[])DB.ExecuteProcedure(
                "imrse_GetLocation",
                new DB.AnalyzeDataSet(GetLocation),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", LocationFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_LOCATION[] GetLocation(long[] Ids, SqlCommand sqlCommand = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION[])DB.ExecuteProcedure(
                "imrse_GetLocation",
                new DB.AnalyzeDataSet(GetLocation),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", Ids != null ? Ids : LocationFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter != null ? DistributorFilter : new int[] {})
					}
                    , sqlCommand: sqlCommand, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetLocation(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION()
            {
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                ID_LOCATION_TYPE = GetValue<int>(row["ID_LOCATION_TYPE"]),
                ID_LOCATION_PATTERN = GetNullableValue<long>(row["ID_LOCATION_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
                ID_CONSUMER = GetNullableValue<int>(row["ID_CONSUMER"]),
                ID_LOCATION_STATE_TYPE = GetValue<int>(row["ID_LOCATION_STATE_TYPE"]),
                IN_KPI = GetValue<bool>(row["IN_KPI"]),
                ALLOW_GROUPING = GetValue<bool>(row["ALLOW_GROUPING"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION[] list = new DB_LOCATION[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION insert = new DB_LOCATION();
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_LOCATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_TYPE"]);
                insert.ID_LOCATION_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_PATTERN"]);
                insert.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR_PATTERN"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_CONSUMER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
                insert.ID_LOCATION_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE_TYPE"]);
                insert.IN_KPI = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IN_KPI"]);
                insert.ALLOW_GROUPING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ALLOW_GROUPING"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveLocation(DB_LOCATION ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocation",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION_TYPE", ToBeSaved.ID_LOCATION_TYPE)
														,new DB.InParameter("@ID_LOCATION_PATTERN", ToBeSaved.ID_LOCATION_PATTERN)
														,new DB.InParameter("@ID_DESCR_PATTERN", ToBeSaved.ID_DESCR_PATTERN)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@ID_LOCATION_STATE_TYPE", ToBeSaved.ID_LOCATION_STATE_TYPE)
														,new DB.InParameter("@IN_KPI", ToBeSaved.IN_KPI)
														,new DB.InParameter("@ALLOW_GROUPING", ToBeSaved.ALLOW_GROUPING)
									}
                                    , sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocation(DB_LOCATION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocation",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION", toBeDeleted.ID_LOCATION)			
		}
            );
        }

        #endregion
        #region Table LOCATION_CONSUMER_HISTORY

        public DB_LOCATION_CONSUMER_HISTORY[] GetLocationConsumerHistory()
        {
            return (DB_LOCATION_CONSUMER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationConsumerHistory",
                new DB.AnalyzeDataSet(GetLocationConsumerHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_CONSUMER_HISTORY", new long[] {})
					}
            );
        }

        public DB_LOCATION_CONSUMER_HISTORY[] GetLocationConsumerHistory(long[] Ids)
        {
            return (DB_LOCATION_CONSUMER_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationConsumerHistory",
                new DB.AnalyzeDataSet(GetLocationConsumerHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_CONSUMER_HISTORY", Ids)
					}
            );
        }

        private object GetLocationConsumerHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_CONSUMER_HISTORY[] list = new DB_LOCATION_CONSUMER_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_CONSUMER_HISTORY insert = new DB_LOCATION_CONSUMER_HISTORY();
                insert.ID_LOCATION_CONSUMER_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_CONSUMER_HISTORY"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_CONSUMER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONSUMER"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveLocationConsumerHistory(DB_LOCATION_CONSUMER_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_CONSUMER_HISTORY", ToBeSaved.ID_LOCATION_CONSUMER_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationConsumerHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_CONSUMER", ToBeSaved.ID_CONSUMER)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_CONSUMER_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationConsumerHistory(DB_LOCATION_CONSUMER_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationConsumerHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_CONSUMER_HISTORY", toBeDeleted.ID_LOCATION_CONSUMER_HISTORY)			
		}
            );
        }

        #endregion
        #region Table LOCATION_DETAILS

        public DB_LOCATION_DETAILS[] GetLocationDetails()
        {
            return (DB_LOCATION_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetLocationDetails",
                new DB.AnalyzeDataSet(GetLocationDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_DETAILS", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_LOCATION_DETAILS[] GetLocationDetails(long[] Ids)
        {
            return (DB_LOCATION_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetLocationDetails",
                new DB.AnalyzeDataSet(GetLocationDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_DETAILS", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetLocationDetails(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_DETAILS[] list = new DB_LOCATION_DETAILS[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_DETAILS insert = new DB_LOCATION_DETAILS();
                insert.ID_LOCATION_DETAILS = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_DETAILS"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.CID = GetValue<string>(QueryResult.Tables[0].Rows[i]["CID"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.CITY = GetValue<string>(QueryResult.Tables[0].Rows[i]["CITY"]);
                insert.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                insert.ZIP = GetValue<string>(QueryResult.Tables[0].Rows[i]["ZIP"]);
                insert.COUNTRY = GetValue<string>(QueryResult.Tables[0].Rows[i]["COUNTRY"]);
                insert.LATITUDE = GetValue<string>(QueryResult.Tables[0].Rows[i]["LATITUDE"]);
                insert.LONGITUDE = GetValue<string>(QueryResult.Tables[0].Rows[i]["LONGITUDE"]);
                insert.IMR_SERVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["IMR_SERVER"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.LOCATION_MEMO = GetValue<string>(QueryResult.Tables[0].Rows[i]["LOCATION_MEMO"]);
                insert.LOCATION_SOURCE_SERVER_ID = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["LOCATION_SOURCE_SERVER_ID"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_LOCATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_TYPE"]);
                insert.ID_LOCATION_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE_TYPE"]);
                insert.IN_KPI = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IN_KPI"]);
                insert.ALLOW_GROUPING = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ALLOW_GROUPING"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveLocationDetails(DB_LOCATION_DETAILS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_DETAILS", ToBeSaved.ID_LOCATION_DETAILS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationDetails",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@CID", ToBeSaved.CID)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@CITY", ToBeSaved.CITY)
														,new DB.InParameter("@ADDRESS", ToBeSaved.ADDRESS)
														,new DB.InParameter("@ZIP", ToBeSaved.ZIP)
														,new DB.InParameter("@COUNTRY", ToBeSaved.COUNTRY)
														,new DB.InParameter("@LATITUDE", ToBeSaved.LATITUDE)
														,new DB.InParameter("@LONGITUDE", ToBeSaved.LONGITUDE)
														,new DB.InParameter("@IMR_SERVER", ToBeSaved.IMR_SERVER)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@LOCATION_MEMO", ToBeSaved.LOCATION_MEMO)
														,new DB.InParameter("@LOCATION_SOURCE_SERVER_ID", ToBeSaved.LOCATION_SOURCE_SERVER_ID)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_LOCATION_TYPE", ToBeSaved.ID_LOCATION_TYPE)
														,new DB.InParameter("@ID_LOCATION_STATE_TYPE", ToBeSaved.ID_LOCATION_STATE_TYPE)
														,new DB.InParameter("@IN_KPI", ToBeSaved.IN_KPI)
														,new DB.InParameter("@ALLOW_GROUPING", ToBeSaved.ALLOW_GROUPING)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_DETAILS = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationDetails(DB_LOCATION_DETAILS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationDetails",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_DETAILS", toBeDeleted.ID_LOCATION_DETAILS)			
		}
            );
        }

        #endregion
        #region Table LOCATION_EQUIPMENT


        private object GetLocationEquipment(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_EQUIPMENT()
            {
                ID_LOCATION_EQUIPMENT = GetValue<long>(row["ID_LOCATION_EQUIPMENT"]),
                ID_LOCATION = GetNullableValue<long>(row["ID_LOCATION"]),
                ID_METER = GetNullableValue<long>(row["ID_METER"]),
                SERIAL_NBR = GetNullableValue<long>(row["SERIAL_NBR"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_EQUIPMENT[] list = new DB_LOCATION_EQUIPMENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_EQUIPMENT insert = new DB_LOCATION_EQUIPMENT();
                insert.ID_LOCATION_EQUIPMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_EQUIPMENT"]);
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                list[i] = insert;
            }
            return list.ToArray();
            */
            #endregion
        }

        public long SaveLocationEquipment(DB_LOCATION_EQUIPMENT ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_EQUIPMENT", ToBeSaved.ID_LOCATION_EQUIPMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationEquipment",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime( ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
									},
                                    sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_EQUIPMENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationEquipment(DB_LOCATION_EQUIPMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationEquipment",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_EQUIPMENT", toBeDeleted.ID_LOCATION_EQUIPMENT)			
		}
            );
        }

        public void UpdateLocationEquipmentWithTriada(long IdLocation, long? IdMeterIMRSC, long? SerialNbr, int IdDistributor, bool isIMRPROD_IMRSC_DB)
        {
            DataTable triada = new DataTable();
            triada.TableName = "triada";
            triada.Columns.Add(new DataColumn("ID_LOCATION"));
            triada.Columns.Add(new DataColumn("ID_METER"));
            triada.Columns.Add(new DataColumn("SERIAL_NBR"));
            triada.Rows.Add(IdLocation, IdMeterIMRSC, SerialNbr);
            DB.ExecuteNonQueryProcedure(
                "sp_UpdateLocationEquipmentWithTriada",
                new DB.Parameter[] {
                    new DB.InParameter("@TRIADA", SqlDbType.Structured, triada),
                    new DB.InParameter("@ID_DISTRIBUTOR", SqlDbType.Int, IdDistributor),
	                new DB.InParameter("@IMRPROD_IMR_SC_DB", SqlDbType.Bit, isIMRPROD_IMRSC_DB)
		}
            );
        }
        #endregion
        #region Table LOCATION_HIERARCHY
        public DB_LOCATION_HIERARCHY[] GetLocationHierarchy(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetLocationHierarchy",
                new DB.AnalyzeDataSet(GetLocationHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_HIERARCHY", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_LOCATION_HIERARCHY[] GetLocationHierarchy(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetLocationHierarchy",
                new DB.AnalyzeDataSet(GetLocationHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_HIERARCHY", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetLocationHierarchy(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_HIERARCHY()
            {
                ID_LOCATION_HIERARCHY = GetValue<long>(row["ID_LOCATION_HIERARCHY"]),
                ID_LOCATION_HIERARCHY_PARENT = GetNullableValue<long>(row["ID_LOCATION_HIERARCHY_PARENT"]),
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                HIERARCHY = GetValue<string>(row["HIERARCHY"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_HIERARCHY[] list = new DB_LOCATION_HIERARCHY[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_HIERARCHY insert = new DB_LOCATION_HIERARCHY();
                insert.ID_LOCATION_HIERARCHY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_HIERARCHY"]);
                insert.ID_LOCATION_HIERARCHY_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_HIERARCHY_PARENT"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["HIERARCHY"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveLocationHierarchy(DB_LOCATION_HIERARCHY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_HIERARCHY", ToBeSaved.ID_LOCATION_HIERARCHY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationHierarchy",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION_HIERARCHY_PARENT", ToBeSaved.ID_LOCATION_HIERARCHY_PARENT)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@HIERARCHY", ToBeSaved.HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_HIERARCHY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationHierarchy(DB_LOCATION_HIERARCHY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationHierarchy",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_HIERARCHY", toBeDeleted.ID_LOCATION_HIERARCHY)			
		}
            );
        }

        #endregion
        #region Table LOCATION_SEAL

        public DB_LOCATION_SEAL[] GetLocationSeal()
        {
            return (DB_LOCATION_SEAL[])DB.ExecuteProcedure(
                "imrse_GetLocationSeal",
                new DB.AnalyzeDataSet(GetLocationSeal),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", new long[] {})
					}
            );
        }

        public DB_LOCATION_SEAL[] GetLocationSeal(long[] Ids)
        {
            return (DB_LOCATION_SEAL[])DB.ExecuteProcedure(
                "imrse_GetLocationSeal",
                new DB.AnalyzeDataSet(GetLocationSeal),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", Ids)
					}
            );
        }

        private object GetLocationSeal(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_SEAL[] list = new DB_LOCATION_SEAL[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_SEAL insert = new DB_LOCATION_SEAL();
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_SEAL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SEAL"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveLocationSeal(DB_LOCATION_SEAL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationSeal",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SEAL", ToBeSaved.ID_SEAL)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationSeal(DB_LOCATION_SEAL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationSeal",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION", toBeDeleted.ID_LOCATION)			
		}
            );
        }

        #endregion
        #region Table LOCATION_STATE_HISTORY

        public DB_LOCATION_STATE_HISTORY[] GetLocationStateHistory()
        {
            return (DB_LOCATION_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationStateHistory",
                new DB.AnalyzeDataSet(GetLocationStateHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_STATE", new long[] {})
					}
            );
        }

        public DB_LOCATION_STATE_HISTORY[] GetLocationStateHistory(long[] Ids)
        {
            return (DB_LOCATION_STATE_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetLocationStateHistory",
                new DB.AnalyzeDataSet(GetLocationStateHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_STATE", Ids)
					}
            );
        }

        private object GetLocationStateHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_STATE_HISTORY[] list = new DB_LOCATION_STATE_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_STATE_HISTORY insert = new DB_LOCATION_STATE_HISTORY();
                insert.ID_LOCATION_STATE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_LOCATION_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE_TYPE"]);
                insert.IN_KPI = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IN_KPI"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveLocationStateHistory(DB_LOCATION_STATE_HISTORY ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_STATE", ToBeSaved.ID_LOCATION_STATE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationStateHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_LOCATION_STATE_TYPE", ToBeSaved.ID_LOCATION_STATE_TYPE)
														,new DB.InParameter("@IN_KPI", ToBeSaved.IN_KPI)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
                                    , sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_STATE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteLocationStateHistory(DB_LOCATION_STATE_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationStateHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_STATE", toBeDeleted.ID_LOCATION_STATE)			
		}
            );
        }

        #endregion
        #region Table LOCATION_STATE_TYPE

        public DB_LOCATION_STATE_TYPE[] GetLocationStateType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationStateType",
                new DB.AnalyzeDataSet(GetLocationStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_STATE_TYPE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_LOCATION_STATE_TYPE[] GetLocationStateType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationStateType",
                new DB.AnalyzeDataSet(GetLocationStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_STATE_TYPE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetLocationStateType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_STATE_TYPE[] list = new DB_LOCATION_STATE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_STATE_TYPE insert = new DB_LOCATION_STATE_TYPE();
                insert.ID_LOCATION_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_STATE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteLocationStateType(DB_LOCATION_STATE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationStateType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_STATE_TYPE", toBeDeleted.ID_LOCATION_STATE_TYPE)			
		}
            );
        }

        #endregion
        #region Table LOCATION_SUPPLIER

        public DB_LOCATION_SUPPLIER[] GetLocationSupplier()
        {
            return (DB_LOCATION_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetLocationSupplier",
                new DB.AnalyzeDataSet(GetLocationSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_SUPPLIER", new int[] {})
					}
            );
        }

        public DB_LOCATION_SUPPLIER[] GetLocationSupplier(int[] Ids)
        {
            return (DB_LOCATION_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetLocationSupplier",
                new DB.AnalyzeDataSet(GetLocationSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_SUPPLIER", Ids)
					}
            );
        }

        private object GetLocationSupplier(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_SUPPLIER[] list = new DB_LOCATION_SUPPLIER[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_SUPPLIER insert = new DB_LOCATION_SUPPLIER();
                insert.ID_LOCATION_SUPPLIER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_SUPPLIER"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_DISTRIBUTOR_SUPPLIER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR_SUPPLIER"]);
                insert.ACCOUNT_NO = GetValue<int>(QueryResult.Tables[0].Rows[i]["ACCOUNT_NO"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveLocationSupplier(DB_LOCATION_SUPPLIER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_SUPPLIER", ToBeSaved.ID_LOCATION_SUPPLIER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationSupplier",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_DISTRIBUTOR_SUPPLIER", ToBeSaved.ID_DISTRIBUTOR_SUPPLIER)
														,new DB.InParameter("@ACCOUNT_NO", ToBeSaved.ACCOUNT_NO)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_SUPPLIER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteLocationSupplier(DB_LOCATION_SUPPLIER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationSupplier",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_SUPPLIER", toBeDeleted.ID_LOCATION_SUPPLIER)			
		}
            );
        }

        #endregion
        #region Table LOCATION_TARIFF

        public DB_LOCATION_TARIFF[] GetLocationTariff()
        {
            return (DB_LOCATION_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetLocationTariff",
                new DB.AnalyzeDataSet(GetLocationTariff),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_TARIFF", new int[] {})
					}
            );
        }

        public DB_LOCATION_TARIFF[] GetLocationTariff(int[] Ids)
        {
            return (DB_LOCATION_TARIFF[])DB.ExecuteProcedure(
                "imrse_GetLocationTariff",
                new DB.AnalyzeDataSet(GetLocationTariff),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_TARIFF", Ids)
					}
            );
        }

        private object GetLocationTariff(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_TARIFF[] list = new DB_LOCATION_TARIFF[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_TARIFF insert = new DB_LOCATION_TARIFF();
                insert.ID_LOCATION_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_TARIFF"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveLocationTariff(DB_LOCATION_TARIFF ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_LOCATION_TARIFF", ToBeSaved.ID_LOCATION_TARIFF);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveLocationTariff",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_LOCATION_TARIFF = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteLocationTariff(DB_LOCATION_TARIFF toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationTariff",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_TARIFF", toBeDeleted.ID_LOCATION_TARIFF)			
		}
            );
        }

        #endregion
        #region Table LOCATION_TYPE

        public DB_LOCATION_TYPE[] GetLocationType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationType",
                new DB.AnalyzeDataSet(GetLocationType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_TYPE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_LOCATION_TYPE[] GetLocationType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_LOCATION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetLocationType",
                new DB.AnalyzeDataSet(GetLocationType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION_TYPE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetLocationType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_LOCATION_TYPE[] list = new DB_LOCATION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_LOCATION_TYPE insert = new DB_LOCATION_TYPE();
                insert.ID_LOCATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_LOCATION_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteLocationType(DB_LOCATION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelLocationType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_LOCATION_TYPE", toBeDeleted.ID_LOCATION_TYPE)			
		}
            );
        }

        #endregion
        #region Table LOCATION_ENTRANCE

        public DB_LOCATION_ENTRANCE[] GetLocationEntrance()
        {
            return (DB_LOCATION_ENTRANCE[])DB.ExecuteProcedure(
                "imrse_GetLocationEntrance",
                new DB.AnalyzeDataSet(GetLocationEntrance),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", new long[] {})
					}
            );
        }

        public DB_LOCATION_ENTRANCE[] GetLocationEntrance(long[] Ids)
        {
            return (DB_LOCATION_ENTRANCE[])DB.ExecuteProcedure(
                "imrse_GetLocationEntrance",
                new DB.AnalyzeDataSet(GetLocationEntrance),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", Ids)
					}
            );
        }

        private object GetLocationEntrance(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_ENTRANCE()
            {
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                ENTRANCE_INDEX_NBR = GetValue<int>(row["ENTRANCE_INDEX_NBR"]),
                SECTION_COUNT = GetValue<int>(row["SECTION_COUNT"]),
                NAME = GetValue<string>(row["NAME"]),
                FLOOR_PLAN = GetValue(row["FLOOR_PLAN"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_LOCATION_ENTRANCE[] list = new DB_LOCATION_ENTRANCE[count];
    for (int i = 0; i < count; i++)
    {
        DB_LOCATION_ENTRANCE insert = new DB_LOCATION_ENTRANCE();
									insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
												insert.ENTRANCE_INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ENTRANCE_INDEX_NBR"]);
												insert.SECTION_COUNT = GetValue<int>(QueryResult.Tables[0].Rows[i]["SECTION_COUNT"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.FLOOR_PLAN = GetValue(QueryResult.Tables[0].Rows[i]["FLOOR_PLAN"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #endregion
        #region Table LOCATION_ENTRANCE_FLOOR

        public DB_LOCATION_ENTRANCE_FLOOR[] GetLocationEntranceFloor()
        {
            return (DB_LOCATION_ENTRANCE_FLOOR[])DB.ExecuteProcedure(
                "imrse_GetLocationEntranceFloor",
                new DB.AnalyzeDataSet(GetLocationEntranceFloor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", new long[] {})
					}
            );
        }

        public DB_LOCATION_ENTRANCE_FLOOR[] GetLocationEntranceFloor(long[] Ids)
        {
            return (DB_LOCATION_ENTRANCE_FLOOR[])DB.ExecuteProcedure(
                "imrse_GetLocationEntranceFloor",
                new DB.AnalyzeDataSet(GetLocationEntranceFloor),
                new DB.Parameter[] { 
			CreateTableParam("@ID_LOCATION", Ids)
					}
            );
        }

        private object GetLocationEntranceFloor(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_LOCATION_ENTRANCE_FLOOR()
            {
                ID_LOCATION = GetValue<long>(row["ID_LOCATION"]),
                ENTRANCE_INDEX_NBR = GetValue<int>(row["ENTRANCE_INDEX_NBR"]),
                FLOOR_INDEX_NBR = GetValue<int>(row["FLOOR_INDEX_NBR"]),
                NAME = GetValue<string>(row["NAME"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_LOCATION_ENTRANCE_FLOOR[] list = new DB_LOCATION_ENTRANCE_FLOOR[count];
    for (int i = 0; i < count; i++)
    {
        DB_LOCATION_ENTRANCE_FLOOR insert = new DB_LOCATION_ENTRANCE_FLOOR();
									insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
												insert.ENTRANCE_INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ENTRANCE_INDEX_NBR"]);
												insert.FLOOR_INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["FLOOR_INDEX_NBR"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #endregion

        #region Table MAPPING

        public DB_MAPPING[] GetMapping(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MAPPING[])DB.ExecuteProcedure(
                "imrse_GetMapping",
                new DB.AnalyzeDataSet(GetMapping),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MAPPING", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_MAPPING[] GetMapping(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MAPPING[])DB.ExecuteProcedure(
                "imrse_GetMapping",
                new DB.AnalyzeDataSet(GetMapping),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MAPPING", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMapping(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_MAPPING()
            {
                ID_MAPPING = GetValue<long>(row["ID_MAPPING"]),
                ID_REFERENCE_TYPE = GetValue<int>(row["ID_REFERENCE_TYPE"]),
                ID_SOURCE = GetValue<long>(row["ID_SOURCE"]),
                ID_DESTINATION = GetValue<long>(row["ID_DESTINATION"]),
                ID_IMR_SERVER = GetValue<int>(row["ID_IMR_SERVER"]),
                TIMESTAMP = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["TIMESTAMP"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_MAPPING[] list = new DB_MAPPING[count];
    for (int i = 0; i < count; i++)
    {
        DB_MAPPING insert = new DB_MAPPING();
									insert.ID_MAPPING = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MAPPING"]);
												insert.ID_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
												insert.ID_SOURCE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SOURCE"]);
												insert.ID_DESTINATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESTINATION"]);
												insert.ID_IMR_SERVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_IMR_SERVER"]);
												insert.TIMESTAMP = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIMESTAMP"]));
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }


        public long SaveMapping(DB_MAPPING ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MAPPING", ToBeSaved.ID_MAPPING);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMapping",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
														,new DB.InParameter("@ID_SOURCE", ToBeSaved.ID_SOURCE)
														,new DB.InParameter("@ID_DESTINATION", ToBeSaved.ID_DESTINATION)
														,new DB.InParameter("@ID_IMR_SERVER", ToBeSaved.ID_IMR_SERVER)
														,new DB.InParameter("@TIMESTAMP", ConvertTimeZoneToUtcTime(ToBeSaved.TIMESTAMP))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MAPPING = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMapping(DB_MAPPING toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMapping",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MAPPING", toBeDeleted.ID_MAPPING)			
		}
            );
        }

        #endregion

        #region Table MESSAGE

        public DB_MESSAGE[] GetMessage()
        {
            return (DB_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetMessage",
                new DB.AnalyzeDataSet(GetMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MESSAGE", new long[] {})
					}
            );
        }

        public DB_MESSAGE[] GetMessage(long[] Ids)
        {
            return (DB_MESSAGE[])DB.ExecuteProcedure(
                "imrse_GetMessage",
                new DB.AnalyzeDataSet(GetMessage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MESSAGE", Ids)
					}
            );
        }



        private object GetMessage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MESSAGE[] list = new DB_MESSAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MESSAGE insert = new DB_MESSAGE();
                insert.ID_MESSAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MESSAGE"]);
                insert.ID_MESSAGE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MESSAGE_TYPE"]);
                insert.ID_MESSAGE_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_MESSAGE_PARENT"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.MESSAGE_CONTENT = GetValue<string>(QueryResult.Tables[0].Rows[i]["MESSAGE_CONTENT"]);
                insert.HAS_ATTACHMENT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["HAS_ATTACHMENT"]);
                insert.POINTS = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["POINTS"]);
                insert.HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["HIERARCHY"]);
                insert.INSERT_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["INSERT_DATE"]));
                list[i] = insert;
            }
            return list;
        }
        public long SaveMessage(DB_MESSAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MESSAGE", ToBeSaved.ID_MESSAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMessage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MESSAGE_TYPE", ToBeSaved.ID_MESSAGE_TYPE)
														,new DB.InParameter("@ID_MESSAGE_PARENT", ToBeSaved.ID_MESSAGE_PARENT)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@MESSAGE_CONTENT", ToBeSaved.MESSAGE_CONTENT)
														,new DB.InParameter("@HAS_ATTACHMENT", ToBeSaved.HAS_ATTACHMENT)
														,new DB.InParameter("@POINTS", ToBeSaved.POINTS)
														,new DB.InParameter("@HIERARCHY", ToBeSaved.HIERARCHY)
														,new DB.InParameter("@INSERT_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.INSERT_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MESSAGE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMessage(DB_MESSAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMessage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MESSAGE", toBeDeleted.ID_MESSAGE)			
		}
            );
        }

        #endregion
        #region Table MESSAGE_DATA

        public DB_MESSAGE_DATA[] GetMessageData()
        {
            return (DB_MESSAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetMessageData",
                new DB.AnalyzeDataSet(GetMessageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MESSAGE_DATA", new long[] {})
					}
            );
        }

        public DB_MESSAGE_DATA[] GetMessageData(long[] Ids)
        {
            return (DB_MESSAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetMessageData",
                new DB.AnalyzeDataSet(GetMessageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MESSAGE_DATA", Ids)
					}
            );
        }



        private object GetMessageData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MESSAGE_DATA[] list = new DB_MESSAGE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_MESSAGE_DATA insert = new DB_MESSAGE_DATA();
                insert.ID_MESSAGE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MESSAGE_DATA"]);
                insert.ID_MESSAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MESSAGE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveMessageData(DB_MESSAGE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MESSAGE_DATA", ToBeSaved.ID_MESSAGE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMessageData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MESSAGE", ToBeSaved.ID_MESSAGE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MESSAGE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMessageData(DB_MESSAGE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMessageData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MESSAGE_DATA", toBeDeleted.ID_MESSAGE_DATA)			
		}
            );
        }

        #endregion

        #region Table METER
        public DB_METER[] GetMeter(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER[])DB.ExecuteProcedure(
                "imrse_GetMeter",
                new DB.AnalyzeDataSet(GetMeter),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER", MeterFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_METER[] GetMeter(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER[])DB.ExecuteProcedure(
                "imrse_GetMeter",
                new DB.AnalyzeDataSet(GetMeter),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER", Ids != null ? Ids : MeterFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", DistributorFilter != null ? DistributorFilter : new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetMeter(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_METER()
            {
                ID_METER = GetValue<long>(row["ID_METER"]),
                ID_METER_TYPE = GetValue<int>(row["ID_METER_TYPE"]),
                ID_METER_PATTERN = GetNullableValue<long>(row["ID_METER_PATTERN"]),
                ID_DESCR_PATTERN = GetNullableValue<long>(row["ID_DESCR_PATTERN"]),
                ID_DISTRIBUTOR = GetValue<int>(row["ID_DISTRIBUTOR"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER[] list = new DB_METER[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER insert = new DB_METER();
                insert.ID_METER = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE"]);
                insert.ID_METER_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER_PATTERN"]);
                insert.ID_DESCR_PATTERN = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR_PATTERN"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public long SaveMeter(DB_METER ToBeSaved, SqlCommand sqlCommand = null)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER", ToBeSaved.ID_METER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMeter",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_METER_TYPE", ToBeSaved.ID_METER_TYPE)
														,new DB.InParameter("@ID_METER_PATTERN", ToBeSaved.ID_METER_PATTERN)
														,new DB.InParameter("@ID_DESCR_PATTERN", ToBeSaved.ID_DESCR_PATTERN)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									},
                                    sqlCommand
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMeter(DB_METER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeter",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER", toBeDeleted.ID_METER)			
		}
            );
        }

        public void UpdateMeterParameters(long IdMeterIMRSC, string SerialNbr, int IdMeterType, bool isIMRPROD_IMRSC_DB)
        {
            DB.ExecuteNonQueryProcedure(
                "sp_UpdateMeterParameters",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_METER_IMRSC", IdMeterIMRSC),
                    new DB.InParameter("@SERIAL_NBR", SerialNbr),
                    new DB.InParameter("@ID_METER_TYPE", IdMeterType),
                    new DB.InParameter("@IMRPROD_IMR_SC_DB", isIMRPROD_IMRSC_DB)
		        }
            );
        }

        #endregion
        #region Table METER_TYPE

        public DB_METER_TYPE[] GetMeterType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMeterType",
                new DB.AnalyzeDataSet(GetMeterType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_METER_TYPE[] GetMeterType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMeterType",
                new DB.AnalyzeDataSet(GetMeterType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetMeterType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER_TYPE[] list = new DB_METER_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER_TYPE insert = new DB_METER_TYPE();
                insert.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_METER_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE_CLASS"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteMeterType(DB_METER_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER_TYPE", toBeDeleted.ID_METER_TYPE)			
		}
            );
        }

        #endregion
        #region Table METER_TYPE_CLASS

        public DB_METER_TYPE_CLASS[] GetMeterTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeClass",
                new DB.AnalyzeDataSet(GetMeterTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_CLASS", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_METER_TYPE_CLASS[] GetMeterTypeClass(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_METER_TYPE_CLASS[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeClass",
                new DB.AnalyzeDataSet(GetMeterTypeClass),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_CLASS", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetMeterTypeClass(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER_TYPE_CLASS[] list = new DB_METER_TYPE_CLASS[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER_TYPE_CLASS insert = new DB_METER_TYPE_CLASS();
                insert.ID_METER_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE_CLASS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteMeterTypeClass(DB_METER_TYPE_CLASS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterTypeClass",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER_TYPE_CLASS", toBeDeleted.ID_METER_TYPE_CLASS)			
		}
            );
        }

        #endregion
        #region Table METER_TYPE_DATA

        public DB_METER_TYPE_DATA[] GetMeterTypeData()
        {
            return (DB_METER_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeData",
                new DB.AnalyzeDataSet(GetMeterTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_DATA", new long[] {})
					}
            );
        }

        public DB_METER_TYPE_DATA[] GetMeterTypeData(long[] Ids)
        {
            return (DB_METER_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeData",
                new DB.AnalyzeDataSet(GetMeterTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_DATA", Ids)
					}
            );
        }



        private object GetMeterTypeData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_METER_TYPE_DATA()
            {
                ID_METER_TYPE_DATA = GetValue<long>(row["ID_METER_TYPE_DATA"]),
                ID_METER_TYPE = GetValue<int>(row["ID_METER_TYPE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_METER_TYPE_DATA[] list = new DB_METER_TYPE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_METER_TYPE_DATA insert = new DB_METER_TYPE_DATA();
									insert.ID_METER_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE_DATA"]);
												insert.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveMeterTypeData(DB_METER_TYPE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER_TYPE_DATA", ToBeSaved.ID_METER_TYPE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMeterTypeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_METER_TYPE", ToBeSaved.ID_METER_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER_TYPE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteMeterTypeData(DB_METER_TYPE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterTypeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER_TYPE_DATA", toBeDeleted.ID_METER_TYPE_DATA)			
		}
            );
        }

        #endregion
        #region Table METER_TYPE_GROUP

        public DB_METER_TYPE_GROUP[] GetMeterTypeGroup()
        {
            return (DB_METER_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeGroup",
                new DB.AnalyzeDataSet(GetMeterTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_METER_TYPE_GROUP[] GetMeterTypeGroup(int[] Ids)
        {
            return (DB_METER_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeGroup",
                new DB.AnalyzeDataSet(GetMeterTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_GROUP", Ids)
					}
            );
        }



        private object GetMeterTypeGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER_TYPE_GROUP[] list = new DB_METER_TYPE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER_TYPE_GROUP insert = new DB_METER_TYPE_GROUP();
                insert.ID_METER_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveMeterTypeGroup(DB_METER_TYPE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER_TYPE_GROUP", ToBeSaved.ID_METER_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMeterTypeGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMeterTypeGroup(DB_METER_TYPE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterTypeGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER_TYPE_GROUP", toBeDeleted.ID_METER_TYPE_GROUP)			
		}
            );
        }

        #endregion
        #region Table METER_TYPE_IN_GROUP

        public DB_METER_TYPE_IN_GROUP[] GetMeterTypeInGroup()
        {
            return (DB_METER_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeInGroup",
                new DB.AnalyzeDataSet(GetMeterTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_GROUP", new int[] {})
					}
            );
        }

        public DB_METER_TYPE_IN_GROUP[] GetMeterTypeInGroup(int[] Ids)
        {
            return (DB_METER_TYPE_IN_GROUP[])DB.ExecuteProcedure(
                "imrse_GetMeterTypeInGroup",
                new DB.AnalyzeDataSet(GetMeterTypeInGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_METER_TYPE_GROUP", Ids)
					}
            );
        }



        private object GetMeterTypeInGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_METER_TYPE_IN_GROUP[] list = new DB_METER_TYPE_IN_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_METER_TYPE_IN_GROUP insert = new DB_METER_TYPE_IN_GROUP();
                insert.ID_METER_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE_GROUP"]);
                insert.ID_METER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_METER_TYPE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveMeterTypeInGroup(DB_METER_TYPE_IN_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_METER_TYPE_GROUP", ToBeSaved.ID_METER_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMeterTypeInGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_METER_TYPE", ToBeSaved.ID_METER_TYPE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_METER_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMeterTypeInGroup(DB_METER_TYPE_IN_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMeterTypeInGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_METER_TYPE_GROUP", toBeDeleted.ID_METER_TYPE_GROUP),			
            new DB.InParameter("@ID_METER_TYPE", toBeDeleted.ID_METER_TYPE)	
		}
            );
        }

        #endregion

        #region Table MOBILE_NETWORK_CODE

        public DB_MOBILE_NETWORK_CODE[] GetMobileNetworkCode()
        {
            return (DB_MOBILE_NETWORK_CODE[])DB.ExecuteProcedure(
                "imrse_GetMobileNetworkCode",
                new DB.AnalyzeDataSet(GetMobileNetworkCode),
                new DB.Parameter[] { 
			CreateTableParam("@CODE", new int[] {})
					}
            );
        }

        public DB_MOBILE_NETWORK_CODE[] GetMobileNetworkCode(int[] Ids)
        {
            return (DB_MOBILE_NETWORK_CODE[])DB.ExecuteProcedure(
                "imrse_GetMobileNetworkCode",
                new DB.AnalyzeDataSet(GetMobileNetworkCode),
                new DB.Parameter[] { 
			CreateTableParam("@CODE", Ids)
					}
            );
        }

        private object GetMobileNetworkCode(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MOBILE_NETWORK_CODE[] list = new DB_MOBILE_NETWORK_CODE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MOBILE_NETWORK_CODE insert = new DB_MOBILE_NETWORK_CODE();
                insert.CODE = GetValue<int>(QueryResult.Tables[0].Rows[i]["CODE"]);
                insert.MMC = GetValue<string>(QueryResult.Tables[0].Rows[i]["MMC"]);
                insert.MNC = GetValue<string>(QueryResult.Tables[0].Rows[i]["MNC"]);
                insert.BRAND = GetValue<string>(QueryResult.Tables[0].Rows[i]["BRAND"]);
                insert.OPERATOR = GetValue<string>(QueryResult.Tables[0].Rows[i]["OPERATOR"]);
                insert.BANDS = GetValue<string>(QueryResult.Tables[0].Rows[i]["BANDS"]);
                insert.SMS_SERVICE_CENTER = GetValue<string>(QueryResult.Tables[0].Rows[i]["SMS_SERVICE_CENTER"]);
                insert.APN_ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_ADDRESS"]);
                insert.APN_LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_LOGIN"]);
                insert.APN_PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_PASSWORD"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveMobileNetworkCode(DB_MOBILE_NETWORK_CODE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@CODE", ToBeSaved.CODE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMobileNetworkCode",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@MMC", ToBeSaved.MMC)
														,new DB.InParameter("@MNC", ToBeSaved.MNC)
														,new DB.InParameter("@BRAND", ToBeSaved.BRAND)
														,new DB.InParameter("@OPERATOR", ToBeSaved.OPERATOR)
														,new DB.InParameter("@BANDS", ToBeSaved.BANDS)
														,new DB.InParameter("@SMS_SERVICE_CENTER", ToBeSaved.SMS_SERVICE_CENTER)
														,new DB.InParameter("@APN_ADDRESS", ToBeSaved.APN_ADDRESS)
														,new DB.InParameter("@APN_LOGIN", ToBeSaved.APN_LOGIN)
														,new DB.InParameter("@APN_PASSWORD", ToBeSaved.APN_PASSWORD)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.CODE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMobileNetworkCode(DB_MOBILE_NETWORK_CODE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMobileNetworkCode",
                new DB.Parameter[] {
            new DB.InParameter("@CODE", toBeDeleted.CODE)			
		}
            );
        }

        #endregion

        #region Table MODULE
        public DB_MODULE[] GetModule(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_MODULE[] GetModule(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MODULE[])DB.ExecuteProcedure(
                "imrse_GetModule",
                new DB.AnalyzeDataSet(GetModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetModule(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE[] list = new DB_MODULE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE insert = new DB_MODULE();
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteModule(DB_MODULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE", toBeDeleted.ID_MODULE)			
		}
            );
        }

        #endregion
        #region Table MODULE_DATA

        public DB_MODULE_DATA[] GetModuleData()
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE_DATA", new long[] {})
					}
            );
        }

        public DB_MODULE_DATA[] GetModuleData(long[] Ids)
        {
            return (DB_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetModuleData",
                new DB.AnalyzeDataSet(GetModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_MODULE_DATA", Ids)
					}
            );
        }



        private object GetModuleData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MODULE_DATA[] list = new DB_MODULE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_MODULE_DATA insert = new DB_MODULE_DATA();
                insert.ID_MODULE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_MODULE_DATA"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveModuleData(DB_MODULE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MODULE_DATA", ToBeSaved.ID_MODULE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveModuleData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MODULE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteModuleData(DB_MODULE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelModuleData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MODULE_DATA", toBeDeleted.ID_MODULE_DATA)			
		}
            );
        }

        #endregion

        #region Table MONITORING_WATCHER

        public DB_MONITORING_WATCHER[] GetMonitoringWatcher(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcher",
                new DB.AnalyzeDataSet(GetMonitoringWatcher),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_MONITORING_WATCHER[] GetMonitoringWatcher(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcher",
                new DB.AnalyzeDataSet(GetMonitoringWatcher),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcher(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_MONITORING_WATCHER()
            {
                ID_MONITORING_WATCHER = GetValue<int>(row["ID_MONITORING_WATCHER"]),
                ID_MONITORING_WATCHER_TYPE = GetValue<int>(row["ID_MONITORING_WATCHER_TYPE"]),
                IS_ACTIVE = GetValue<bool>(row["IS_ACTIVE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MONITORING_WATCHER[] list = new DB_MONITORING_WATCHER[count];
            for (int i = 0; i < count; i++)
            {
                DB_MONITORING_WATCHER insert = new DB_MONITORING_WATCHER();
                                            insert.ID_MONITORING_WATCHER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER"]);
                                                        insert.ID_MONITORING_WATCHER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_TYPE"]);
                                                        insert.IS_ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_ACTIVE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcher(DB_MONITORING_WATCHER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER", ToBeSaved.ID_MONITORING_WATCHER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcher",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MONITORING_WATCHER_TYPE", ToBeSaved.ID_MONITORING_WATCHER_TYPE)
                                                        ,new DB.InParameter("@IS_ACTIVE", ToBeSaved.IS_ACTIVE)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcher(DB_MONITORING_WATCHER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcher",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER", toBeDeleted.ID_MONITORING_WATCHER)
                }
            );
        }

        #endregion
        #region Table MONITORING_WATCHER_DATA

        public DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherData",
                new DB.AnalyzeDataSet(GetMonitoringWatcherData),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_DATA", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_MONITORING_WATCHER_DATA[] GetMonitoringWatcherData(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER_DATA[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherData",
                new DB.AnalyzeDataSet(GetMonitoringWatcherData),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_DATA", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcherData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_MONITORING_WATCHER_DATA()
            {
                ID_MONITORING_WATCHER_DATA = GetValue<int>(row["ID_MONITORING_WATCHER_DATA"]),
                ID_MONITORING_WATCHER = GetValue<int>(row["ID_MONITORING_WATCHER"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX = GetValue<int>(row["INDEX"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MONITORING_WATCHER_DATA[] list = new DB_MONITORING_WATCHER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_MONITORING_WATCHER_DATA insert = new DB_MONITORING_WATCHER_DATA();
                                            insert.ID_MONITORING_WATCHER_DATA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_DATA"]);
                                                        insert.ID_MONITORING_WATCHER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.INDEX = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX"]);
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcherData(DB_MONITORING_WATCHER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER_DATA", ToBeSaved.ID_MONITORING_WATCHER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcherData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_MONITORING_WATCHER", ToBeSaved.ID_MONITORING_WATCHER)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX", ToBeSaved.INDEX)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER_DATA = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcherData(DB_MONITORING_WATCHER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcherData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER_DATA", toBeDeleted.ID_MONITORING_WATCHER_DATA)
                }
            );
        }

        #endregion
        #region Table MONITORING_WATCHER_TYPE

        public DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherType",
                new DB.AnalyzeDataSet(GetMonitoringWatcherType),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_TYPE", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_MONITORING_WATCHER_TYPE[] GetMonitoringWatcherType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_MONITORING_WATCHER_TYPE[])DB.ExecuteProcedure(
                "imrse_GetMonitoringWatcherType",
                new DB.AnalyzeDataSet(GetMonitoringWatcherType),
                new DB.Parameter[] {
            CreateTableParam("@ID_MONITORING_WATCHER_TYPE", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetMonitoringWatcherType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_MONITORING_WATCHER_TYPE()
            {
                ID_MONITORING_WATCHER_TYPE = GetValue<int>(row["ID_MONITORING_WATCHER_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                PLUGIN = GetValue<string>(row["PLUGIN"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_MONITORING_WATCHER_TYPE[] list = new DB_MONITORING_WATCHER_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_MONITORING_WATCHER_TYPE insert = new DB_MONITORING_WATCHER_TYPE();
                                            insert.ID_MONITORING_WATCHER_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MONITORING_WATCHER_TYPE"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.PLUGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveMonitoringWatcherType(DB_MONITORING_WATCHER_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_MONITORING_WATCHER_TYPE", ToBeSaved.ID_MONITORING_WATCHER_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveMonitoringWatcherType",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@PLUGIN", ToBeSaved.PLUGIN)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_MONITORING_WATCHER_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteMonitoringWatcherType(DB_MONITORING_WATCHER_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelMonitoringWatcherType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_MONITORING_WATCHER_TYPE", toBeDeleted.ID_MONITORING_WATCHER_TYPE)
                }
            );
        }

        #endregion

        #region Table NOTIFICATION_DELIVERY_TYPE

        public DB_NOTIFICATION_DELIVERY_TYPE[] GetNotificationDeliveryType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_NOTIFICATION_DELIVERY_TYPE[])DB.ExecuteProcedure(
                "imrse_GetNotificationDeliveryType",
                new DB.AnalyzeDataSet(GetNotificationDeliveryType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_NOTIFICATION_DELIVERY_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_NOTIFICATION_DELIVERY_TYPE[] GetNotificationDeliveryType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_NOTIFICATION_DELIVERY_TYPE[])DB.ExecuteProcedure(
                "imrse_GetNotificationDeliveryType",
                new DB.AnalyzeDataSet(GetNotificationDeliveryType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_NOTIFICATION_DELIVERY_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetNotificationDeliveryType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_NOTIFICATION_DELIVERY_TYPE()
            {
                ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(row["ID_NOTIFICATION_DELIVERY_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_NOTIFICATION_DELIVERY_TYPE[] list = new DB_NOTIFICATION_DELIVERY_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_NOTIFICATION_DELIVERY_TYPE insert = new DB_NOTIFICATION_DELIVERY_TYPE();
									insert.ID_NOTIFICATION_DELIVERY_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_DELIVERY_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveNotificationDeliveryType(DB_NOTIFICATION_DELIVERY_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_NOTIFICATION_DELIVERY_TYPE", ToBeSaved.ID_NOTIFICATION_DELIVERY_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveNotificationDeliveryType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_NOTIFICATION_DELIVERY_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteNotificationDeliveryType(DB_NOTIFICATION_DELIVERY_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelNotificationDeliveryType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_NOTIFICATION_DELIVERY_TYPE", toBeDeleted.ID_NOTIFICATION_DELIVERY_TYPE)			
		}
            );
        }

        #endregion
        #region Table NOTIFICATION_TYPE

        public DB_NOTIFICATION_TYPE[] GetNotificationType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_NOTIFICATION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetNotificationType",
                new DB.AnalyzeDataSet(GetNotificationType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_NOTIFICATION_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_NOTIFICATION_TYPE[] GetNotificationType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_NOTIFICATION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetNotificationType",
                new DB.AnalyzeDataSet(GetNotificationType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_NOTIFICATION_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetNotificationType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_NOTIFICATION_TYPE()
            {
                ID_NOTIFICATION_TYPE = GetValue<int>(row["ID_NOTIFICATION_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_NOTIFICATION_TYPE[] list = new DB_NOTIFICATION_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_NOTIFICATION_TYPE insert = new DB_NOTIFICATION_TYPE();
									insert.ID_NOTIFICATION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_NOTIFICATION_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveNotificationType(DB_NOTIFICATION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_NOTIFICATION_TYPE", ToBeSaved.ID_NOTIFICATION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveNotificationType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_NOTIFICATION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteNotificationType(DB_NOTIFICATION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelNotificationType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_NOTIFICATION_TYPE", toBeDeleted.ID_NOTIFICATION_TYPE)			
		}
            );
        }

        #endregion

        #region Table OPERATOR

        public DB_OPERATOR[] GetOperator(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetOperator",
                new DB.AnalyzeDataSet(GetOperator),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_OPERATOR[] GetOperator(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_OPERATOR[])DB.ExecuteProcedure(
                "imrse_GetOperator",
                new DB.AnalyzeDataSet(GetOperator),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetOperator(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_OPERATOR[] list = new DB_OPERATOR[count];
            for (int i = 0; i < count; i++)
            {
                DB_OPERATOR insert = new DB_OPERATOR();
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_ACTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["LOGIN"]);
                insert.PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["PASSWORD"]);
                insert.IS_BLOCKED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_BLOCKED"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveOperator(DB_OPERATOR ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOperator",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@LOGIN", ToBeSaved.LOGIN)
														,new DB.InParameter("@PASSWORD", ToBeSaved.PASSWORD)
														,new DB.InParameter("@IS_BLOCKED", ToBeSaved.IS_BLOCKED)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_OPERATOR = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteOperator(DB_OPERATOR toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOperator",
                new DB.Parameter[] {
            new DB.InParameter("@ID_OPERATOR", toBeDeleted.ID_OPERATOR)			
		}
            );
        }

        #endregion
        #region Table OPERATOR_DATA

        public DB_OPERATOR_DATA[] GetOperatorData()
        {
            return (DB_OPERATOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetOperatorData",
                new DB.AnalyzeDataSet(GetOperatorData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR_DATA", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_OPERATOR_DATA[] GetOperatorData(long[] Ids)
        {
            return (DB_OPERATOR_DATA[])DB.ExecuteProcedure(
                "imrse_GetOperatorData",
                new DB.AnalyzeDataSet(GetOperatorData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_OPERATOR_DATA", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetOperatorData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_OPERATOR_DATA[] list = new DB_OPERATOR_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_OPERATOR_DATA insert = new DB_OPERATOR_DATA();
                insert.ID_OPERATOR_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_DATA"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveOperatorData(DB_OPERATOR_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_OPERATOR_DATA", ToBeSaved.ID_OPERATOR_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOperatorData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_OPERATOR_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteOperatorData(DB_OPERATOR_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOperatorData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_OPERATOR_DATA", toBeDeleted.ID_OPERATOR_DATA)			
		}
            );
        }

        #endregion

        #region Table ORDER

        public DB_ORDER[] GetOrder()
        {
            return (DB_ORDER[])DB.ExecuteProcedure(
                "imrse_GetOrder",
                new DB.AnalyzeDataSet(GetOrder),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER", new int[] {})
					}
            );
        }

        public DB_ORDER[] GetOrder(int[] Ids)
        {
            return (DB_ORDER[])DB.ExecuteProcedure(
                "imrse_GetOrder",
                new DB.AnalyzeDataSet(GetOrder),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER", Ids)
					}
            );
        }



        private object GetOrder(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ORDER[] list = new DB_ORDER[count];
            for (int i = 0; i < count; i++)
            {
                DB_ORDER insert = new DB_ORDER();
                insert.ID_ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ORDER"]);
                insert.ID_ORDER_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ORDER_STATE_TYPE"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveOrder(DB_ORDER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ORDER", ToBeSaved.ID_ORDER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOrder",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ORDER_STATE_TYPE", ToBeSaved.ID_ORDER_STATE_TYPE)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ORDER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteOrder(DB_ORDER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOrder",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ORDER", toBeDeleted.ID_ORDER)			
		}
            );
        }

        #endregion
        #region Table ORDER_DATA

        public DB_ORDER_DATA[] GetOrderData()
        {
            return (DB_ORDER_DATA[])DB.ExecuteProcedure(
                "imrse_GetOrderData",
                new DB.AnalyzeDataSet(GetOrderData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER_DATA", new long[] {})
					}
            );
        }

        public DB_ORDER_DATA[] GetOrderData(long[] Ids)
        {
            return (DB_ORDER_DATA[])DB.ExecuteProcedure(
                "imrse_GetOrderData",
                new DB.AnalyzeDataSet(GetOrderData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER_DATA", Ids)
					}
            );
        }



        private object GetOrderData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ORDER_DATA[] list = new DB_ORDER_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ORDER_DATA insert = new DB_ORDER_DATA();
                insert.ID_ORDER_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ORDER_DATA"]);
                insert.ID_ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ORDER"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveOrderData(DB_ORDER_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ORDER_DATA", ToBeSaved.ID_ORDER_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOrderData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ORDER", ToBeSaved.ID_ORDER)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ORDER_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteOrderData(DB_ORDER_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOrderData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ORDER_DATA", toBeDeleted.ID_ORDER_DATA)			
		}
            );
        }

        #endregion
        #region Table ORDER_STATE_TYPE

        public DB_ORDER_STATE_TYPE[] GetOrderStateType()
        {
            return (DB_ORDER_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetOrderStateType",
                new DB.AnalyzeDataSet(GetOrderStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER_STATE_TYPE", new int[] {})
					}
            );
        }

        public DB_ORDER_STATE_TYPE[] GetOrderStateType(int[] Ids)
        {
            return (DB_ORDER_STATE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetOrderStateType",
                new DB.AnalyzeDataSet(GetOrderStateType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ORDER_STATE_TYPE", Ids)
					}
            );
        }



        private object GetOrderStateType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ORDER_STATE_TYPE[] list = new DB_ORDER_STATE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ORDER_STATE_TYPE insert = new DB_ORDER_STATE_TYPE();
                insert.ID_ORDER_STATE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ORDER_STATE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveOrderStateType(DB_ORDER_STATE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ORDER_STATE_TYPE", ToBeSaved.ID_ORDER_STATE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveOrderStateType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ORDER_STATE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteOrderStateType(DB_ORDER_STATE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelOrderStateType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ORDER_STATE_TYPE", toBeDeleted.ID_ORDER_STATE_TYPE)			
		}
            );
        }

        #endregion

        #region Table PACKAGE

        public DB_PACKAGE[] GetPackage()
        {
            return (DB_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetPackage",
                new DB.AnalyzeDataSet(GetPackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_PACKAGE[] GetPackage(int[] Ids)
        {
            return (DB_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetPackage",
                new DB.AnalyzeDataSet(GetPackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetPackage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKAGE[] list = new DB_PACKAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKAGE insert = new DB_PACKAGE();
                insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                insert.SEND_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["SEND_DATE"]));
                insert.ID_OPERATOR_CREATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATOR"]);
                insert.ID_OPERATOR_RECEIVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_RECEIVER"]);
                insert.RECEIVED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["RECEIVED"]);
                insert.RECEIVE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["RECEIVE_DATE"]));
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.ID_PACKAGE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_STATUS"]);
                list[i] = insert;
            }
            return list;
        }

        public int SavePackage(DB_PACKAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKAGE", ToBeSaved.ID_PACKAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePackage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@CODE", ToBeSaved.CODE)
														,new DB.InParameter("@SEND_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.SEND_DATE))
														,new DB.InParameter("@ID_OPERATOR_CREATOR", ToBeSaved.ID_OPERATOR_CREATOR)
														,new DB.InParameter("@ID_OPERATOR_RECEIVER", ToBeSaved.ID_OPERATOR_RECEIVER)
														,new DB.InParameter("@RECEIVED", ToBeSaved.RECEIVED)
														,new DB.InParameter("@RECEIVE_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.RECEIVE_DATE))
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_PACKAGE_STATUS", ToBeSaved.ID_PACKAGE_STATUS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKAGE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePackage(DB_PACKAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPackage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKAGE", toBeDeleted.ID_PACKAGE)			
		}
            );
        }

        #endregion
        #region Table PACKAGE_STATUS

        public DB_PACKAGE_STATUS[] GetPackageStatus()
        {
            return (DB_PACKAGE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPackageStatus",
                new DB.AnalyzeDataSet(GetPackageStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_STATUS", new int[] {})
					}
            );
        }

        public DB_PACKAGE_STATUS[] GetPackageStatus(int[] Ids)
        {
            return (DB_PACKAGE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetPackageStatus",
                new DB.AnalyzeDataSet(GetPackageStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_STATUS", Ids)
					}
            );
        }



        private object GetPackageStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKAGE_STATUS[] list = new DB_PACKAGE_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKAGE_STATUS insert = new DB_PACKAGE_STATUS();
                insert.ID_PACKAGE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_STATUS"]);
                insert.PACKAGE_STATUS = GetValue<string>(QueryResult.Tables[0].Rows[i]["PACKAGE_STATUS"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }



        public int SavePackageStatus(DB_PACKAGE_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKAGE_STATUS", ToBeSaved.ID_PACKAGE_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePackageStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@PACKAGE_STATUS", ToBeSaved.PACKAGE_STATUS)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKAGE_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePackageStatus(DB_PACKAGE_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPackageStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKAGE_STATUS", toBeDeleted.ID_PACKAGE_STATUS)			
		}
            );
        }

        #endregion
        #region Table PACKAGE_STATUS_HISTORY

        public DB_PACKAGE_STATUS_HISTORY[] GetPackageStatusHistory()
        {
            return (DB_PACKAGE_STATUS_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPackageStatusHistory",
                new DB.AnalyzeDataSet(GetPackageStatusHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_STATUS_HISTORY", new int[] {})
					}
            );
        }

        public DB_PACKAGE_STATUS_HISTORY[] GetPackageStatusHistory(int[] Ids)
        {
            return (DB_PACKAGE_STATUS_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetPackageStatusHistory",
                new DB.AnalyzeDataSet(GetPackageStatusHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_STATUS_HISTORY", Ids)
					}
            );
        }


        private object GetPackageStatusHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKAGE_STATUS_HISTORY[] list = new DB_PACKAGE_STATUS_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKAGE_STATUS_HISTORY insert = new DB_PACKAGE_STATUS_HISTORY();
                insert.ID_PACKAGE_STATUS_HISTORY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_STATUS_HISTORY"]);
                insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.ID_PACKAGE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_STATUS"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }



        public int SavePackageStatusHistory(DB_PACKAGE_STATUS_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKAGE_STATUS_HISTORY", ToBeSaved.ID_PACKAGE_STATUS_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePackageStatusHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PACKAGE", ToBeSaved.ID_PACKAGE)
														,new DB.InParameter("@ID_PACKAGE_STATUS", ToBeSaved.ID_PACKAGE_STATUS)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKAGE_STATUS_HISTORY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePackageStatusHistory(DB_PACKAGE_STATUS_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPackageStatusHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKAGE_STATUS_HISTORY", toBeDeleted.ID_PACKAGE_STATUS_HISTORY)			
		}
            );
        }

        #endregion
        #region Table PACKAGE_DATA

        public DB_PACKAGE_DATA[] GetPackageData()
        {
            return (DB_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetPackageData",
                new DB.AnalyzeDataSet(GetPackageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_DATA", new long[] {})
					}
            );
        }

        public DB_PACKAGE_DATA[] GetPackageData(long[] Ids)
        {
            return (DB_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetPackageData",
                new DB.AnalyzeDataSet(GetPackageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PACKAGE_DATA", Ids)
					}
            );
        }

        private object GetPackageData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PACKAGE_DATA[] list = new DB_PACKAGE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_PACKAGE_DATA insert = new DB_PACKAGE_DATA();
                insert.ID_PACKAGE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE_DATA"]);
                insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SavePackageData(DB_PACKAGE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PACKAGE_DATA", ToBeSaved.ID_PACKAGE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePackageData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PACKAGE", ToBeSaved.ID_PACKAGE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PACKAGE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeletePackageData(DB_PACKAGE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPackageData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PACKAGE_DATA", toBeDeleted.ID_PACKAGE_DATA)			
		}
            );
        }

        #endregion

        #region Table PAYMENT_MODULE

        public DB_PAYMENT_MODULE[] GetPaymentModule(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE[])DB.ExecuteProcedure(
                "imrse_GetPaymentModule",
                new DB.AnalyzeDataSet(GetPaymentModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PAYMENT_MODULE[] GetPaymentModule(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE[])DB.ExecuteProcedure(
                "imrse_GetPaymentModule",
                new DB.AnalyzeDataSet(GetPaymentModule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetPaymentModule(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_PAYMENT_MODULE()
            {
                ID_PAYMENT_MODULE = GetValue<int>(row["ID_PAYMENT_MODULE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_PAYMENT_MODULE_TYPE = GetValue<int>(row["ID_PAYMENT_MODULE_TYPE"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PAYMENT_MODULE[] list = new DB_PAYMENT_MODULE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PAYMENT_MODULE insert = new DB_PAYMENT_MODULE();
									        insert.ID_PAYMENT_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE"]);
												        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												        insert.ID_PAYMENT_MODULE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE_TYPE"]);
												        insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							        list[i] = insert;
            }
            return list;
	        */
            #endregion
        }

        public int SavePaymentModule(DB_PAYMENT_MODULE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PAYMENT_MODULE", ToBeSaved.ID_PAYMENT_MODULE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePaymentModule",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_PAYMENT_MODULE_TYPE", ToBeSaved.ID_PAYMENT_MODULE_TYPE)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PAYMENT_MODULE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePaymentModule(DB_PAYMENT_MODULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPaymentModule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PAYMENT_MODULE", toBeDeleted.ID_PAYMENT_MODULE)			
		}
            );
        }

        #endregion
        #region Table PAYMENT_MODULE_DATA

        public DB_PAYMENT_MODULE_DATA[] GetPaymentModuleData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetPaymentModuleData",
                new DB.AnalyzeDataSet(GetPaymentModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PAYMENT_MODULE_DATA[] GetPaymentModuleData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE_DATA[])DB.ExecuteProcedure(
                "imrse_GetPaymentModuleData",
                new DB.AnalyzeDataSet(GetPaymentModuleData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetPaymentModuleData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_PAYMENT_MODULE_DATA()
            {
                ID_PAYMENT_MODULE_DATA = GetValue<long>(row["ID_PAYMENT_MODULE_DATA"]),
                ID_PAYMENT_MODULE = GetValue<int>(row["ID_PAYMENT_MODULE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PAYMENT_MODULE_DATA[] list = new DB_PAYMENT_MODULE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_PAYMENT_MODULE_DATA insert = new DB_PAYMENT_MODULE_DATA();
									        insert.ID_PAYMENT_MODULE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE_DATA"]);
												        insert.ID_PAYMENT_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE"]);
												        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							        list[i] = insert;
            }
            return list;
	        */
            #endregion
        }

        public long SavePaymentModuleData(DB_PAYMENT_MODULE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PAYMENT_MODULE_DATA", ToBeSaved.ID_PAYMENT_MODULE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePaymentModuleData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PAYMENT_MODULE", ToBeSaved.ID_PAYMENT_MODULE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PAYMENT_MODULE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeletePaymentModuleData(DB_PAYMENT_MODULE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPaymentModuleData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PAYMENT_MODULE_DATA", toBeDeleted.ID_PAYMENT_MODULE_DATA)			
		}
            );
        }

        #endregion
        #region Table PAYMENT_MODULE_TYPE

        public DB_PAYMENT_MODULE_TYPE[] GetPaymentModuleType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetPaymentModuleType",
                new DB.AnalyzeDataSet(GetPaymentModuleType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PAYMENT_MODULE_TYPE[] GetPaymentModuleType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PAYMENT_MODULE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetPaymentModuleType",
                new DB.AnalyzeDataSet(GetPaymentModuleType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PAYMENT_MODULE_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetPaymentModuleType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_PAYMENT_MODULE_TYPE()
            {
                ID_PAYMENT_MODULE_TYPE = GetValue<int>(row["ID_PAYMENT_MODULE_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PAYMENT_MODULE_TYPE[] list = new DB_PAYMENT_MODULE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PAYMENT_MODULE_TYPE insert = new DB_PAYMENT_MODULE_TYPE();
									        insert.ID_PAYMENT_MODULE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PAYMENT_MODULE_TYPE"]);
												        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												        insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							        list[i] = insert;
            }
            return list;
	        */
            #endregion
        }

        public int SavePaymentModuleType(DB_PAYMENT_MODULE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PAYMENT_MODULE_TYPE", ToBeSaved.ID_PAYMENT_MODULE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePaymentModuleType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PAYMENT_MODULE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePaymentModuleType(DB_PAYMENT_MODULE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPaymentModuleType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PAYMENT_MODULE_TYPE", toBeDeleted.ID_PAYMENT_MODULE_TYPE)			
		}
            );
        }

        #endregion

        #region Table PRIORITY

        public DB_PRIORITY[] GetPriority(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PRIORITY[])DB.ExecuteProcedure(
                "imrse_GetPriority",
                new DB.AnalyzeDataSet(GetPriority),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PRIORITY", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PRIORITY[] GetPriority(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PRIORITY[])DB.ExecuteProcedure(
                "imrse_GetPriority",
                new DB.AnalyzeDataSet(GetPriority),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PRIORITY", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetPriority(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PRIORITY[] list = new DB_PRIORITY[count];
            for (int i = 0; i < count; i++)
            {
                DB_PRIORITY insert = new DB_PRIORITY();
                insert.ID_PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRIORITY"]);
                insert.ID_DESCR = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.VALUE = GetValue<int>(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.REALIZATION_TIME = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["REALIZATION_TIME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SavePriority(DB_PRIORITY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PRIORITY", ToBeSaved.ID_PRIORITY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SavePriority",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@VALUE", ToBeSaved.VALUE)
														,new DB.InParameter("@REALIZATION_TIME", ToBeSaved.REALIZATION_TIME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PRIORITY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeletePriority(DB_PRIORITY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelPriority",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PRIORITY", toBeDeleted.ID_PRIORITY)			
		}
            );
        }

        #endregion

        #region Table PRODUCT_CODE

        public DB_PRODUCT_CODE[] GetProductCode(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PRODUCT_CODE[])DB.ExecuteProcedure(
                "imrse_GetProductCode",
                new DB.AnalyzeDataSet(GetProductCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PRODUCT_CODE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_PRODUCT_CODE[] GetProductCode(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PRODUCT_CODE[])DB.ExecuteProcedure(
                "imrse_GetProductCode",
                new DB.AnalyzeDataSet(GetProductCode),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PRODUCT_CODE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetProductCode(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PRODUCT_CODE[] list = new DB_PRODUCT_CODE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PRODUCT_CODE insert = new DB_PRODUCT_CODE();
                insert.ID_PRODUCT_CODE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRODUCT_CODE"]);
                insert.CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DENSITY = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["DENSITY"]);
                insert.ALPHA = GetValue<Double>(QueryResult.Tables[0].Rows[i]["ALPHA"]);
                insert.CALORIFIC_VALUE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["CALORIFIC_VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveProductCode(DB_PRODUCT_CODE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PRODUCT_CODE", ToBeSaved.ID_PRODUCT_CODE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveProductCode",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@CODE", ToBeSaved.CODE)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DENSITY", ToBeSaved.DENSITY)
														,new DB.InParameter("@ALPHA", ToBeSaved.ALPHA)
														,new DB.InParameter("@CALORIFIC_VALUE", ToBeSaved.CALORIFIC_VALUE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PRODUCT_CODE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteProductCode(DB_PRODUCT_CODE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProductCode",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PRODUCT_CODE", toBeDeleted.ID_PRODUCT_CODE)			
		}
            );
        }

        #endregion

        #region Table PROFILE

        public DB_PROFILE[] GetProfile()
        {
            return (DB_PROFILE[])DB.ExecuteProcedure(
                "imrse_GetProfile",
                new DB.AnalyzeDataSet(GetProfile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROFILE", new int[] {})
					}
            );
        }

        public DB_PROFILE[] GetProfile(int[] Ids)
        {
            return (DB_PROFILE[])DB.ExecuteProcedure(
                "imrse_GetProfile",
                new DB.AnalyzeDataSet(GetProfile),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROFILE", Ids)
					}
            );
        }



        private object GetProfile(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PROFILE[] list = new DB_PROFILE[count];
            for (int i = 0; i < count; i++)
            {
                DB_PROFILE insert = new DB_PROFILE();
                insert.ID_PROFILE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROFILE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveProfile(DB_PROFILE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PROFILE", ToBeSaved.ID_PROFILE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveProfile",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PROFILE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteProfile(DB_PROFILE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProfile",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PROFILE", toBeDeleted.ID_PROFILE)			
		}
            );
        }

        #endregion
        #region Table PROFILE_DATA

        public DB_PROFILE_DATA[] GetProfileData()
        {
            return (DB_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetProfileData",
                new DB.AnalyzeDataSet(GetProfileData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROFILE_DATA", new long[] {})
					}
            );
        }

        public DB_PROFILE_DATA[] GetProfileData(long[] Ids)
        {
            return (DB_PROFILE_DATA[])DB.ExecuteProcedure(
                "imrse_GetProfileData",
                new DB.AnalyzeDataSet(GetProfileData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROFILE_DATA", Ids)
					}
            );
        }



        private object GetProfileData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PROFILE_DATA[] list = new DB_PROFILE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_PROFILE_DATA insert = new DB_PROFILE_DATA();
                insert.ID_PROFILE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_PROFILE_DATA"]);
                insert.ID_PROFILE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROFILE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveProfileData(DB_PROFILE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_PROFILE_DATA", ToBeSaved.ID_PROFILE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveProfileData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PROFILE", ToBeSaved.ID_PROFILE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_PROFILE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteProfileData(DB_PROFILE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProfileData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PROFILE_DATA", toBeDeleted.ID_PROFILE_DATA)			
		}
            );
        }

        #endregion

        #region Table PROTOCOL

        public DB_PROTOCOL[] GetProtocol(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetProtocol",
                new DB.AnalyzeDataSet(GetProtocol),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROTOCOL", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PROTOCOL[] GetProtocol(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PROTOCOL[])DB.ExecuteProcedure(
                "imrse_GetProtocol",
                new DB.AnalyzeDataSet(GetProtocol),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROTOCOL", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetProtocol(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PROTOCOL[] list = new DB_PROTOCOL[count];
            for (int i = 0; i < count; i++)
            {
                DB_PROTOCOL insert = new DB_PROTOCOL();
                insert.ID_PROTOCOL = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_PROTOCOL_DRIVER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_DRIVER"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteProtocol(DB_PROTOCOL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProtocol",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PROTOCOL", toBeDeleted.ID_PROTOCOL)			
		}
            );
        }

        #endregion
        #region Table PROTOCOL_DRIVER

        public DB_PROTOCOL_DRIVER[] GetProtocolDriver(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PROTOCOL_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetProtocolDriver",
                new DB.AnalyzeDataSet(GetProtocolDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROTOCOL_DRIVER", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_PROTOCOL_DRIVER[] GetProtocolDriver(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_PROTOCOL_DRIVER[])DB.ExecuteProcedure(
                "imrse_GetProtocolDriver",
                new DB.AnalyzeDataSet(GetProtocolDriver),
                new DB.Parameter[] { 
			CreateTableParam("@ID_PROTOCOL_DRIVER", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetProtocolDriver(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_PROTOCOL_DRIVER[] list = new DB_PROTOCOL_DRIVER[count];
            for (int i = 0; i < count; i++)
            {
                DB_PROTOCOL_DRIVER insert = new DB_PROTOCOL_DRIVER();
                insert.ID_PROTOCOL_DRIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PROTOCOL_DRIVER"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.PLUGIN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN_NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public void DeleteProtocolDriver(DB_PROTOCOL_DRIVER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelProtocolDriver",
                new DB.Parameter[] {
            new DB.InParameter("@ID_PROTOCOL_DRIVER", toBeDeleted.ID_PROTOCOL_DRIVER)			
		}
            );
        }

        #endregion

        #region Table REFERENCE_TYPE

        public DB_REFERENCE_TYPE[] GetReferenceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReferenceType",
                new DB.AnalyzeDataSet(GetReferenceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_REFERENCE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_REFERENCE_TYPE[] GetReferenceType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetReferenceType",
                new DB.AnalyzeDataSet(GetReferenceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_REFERENCE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetReferenceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_REFERENCE_TYPE[] list = new DB_REFERENCE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_REFERENCE_TYPE insert = new DB_REFERENCE_TYPE();
                insert.ID_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveReferenceType(DB_REFERENCE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveReferenceType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_REFERENCE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteReferenceType(DB_REFERENCE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelReferenceType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_REFERENCE_TYPE", toBeDeleted.ID_REFERENCE_TYPE)			
		}
            );
        }

        #endregion

        #region Table ROLE

        public DB_ROLE[] GetRole()
        {
            return (DB_ROLE[])DB.ExecuteProcedure(
                "imrse_GetRole",
                new DB.AnalyzeDataSet(GetRole),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROLE", new int[] {})
					}
            );
        }

        public DB_ROLE[] GetRole(int[] Ids)
        {
            return (DB_ROLE[])DB.ExecuteProcedure(
                "imrse_GetRole",
                new DB.AnalyzeDataSet(GetRole),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROLE", Ids)
					}
            );
        }

        private object GetRole(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROLE[] list = new DB_ROLE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROLE insert = new DB_ROLE();
                insert.ID_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_MODULE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveRole(DB_ROLE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROLE", ToBeSaved.ID_ROLE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRole",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROLE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRole(DB_ROLE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRole",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROLE", toBeDeleted.ID_ROLE)			
		}
            );
        }

        #endregion
        #region Table ROLE_ACTIVITY

        public DB_ROLE_ACTIVITY[] GetRoleActivity()
        {
            return (DB_ROLE_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetRoleActivity",
                new DB.AnalyzeDataSet(GetRoleActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROLE_ACTIVITY", new long[] {})
					}
            );
        }

        public DB_ROLE_ACTIVITY[] GetRoleActivity(long[] Ids)
        {
            return (DB_ROLE_ACTIVITY[])DB.ExecuteProcedure(
                "imrse_GetRoleActivity",
                new DB.AnalyzeDataSet(GetRoleActivity),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROLE_ACTIVITY", Ids)
					}
            );
        }



        private object GetRoleActivity(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROLE_ACTIVITY[] list = new DB_ROLE_ACTIVITY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROLE_ACTIVITY insert = new DB_ROLE_ACTIVITY();
                insert.ID_ROLE_ACTIVITY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROLE_ACTIVITY"]);
                insert.ID_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROLE"]);
                insert.ID_ACTIVITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTIVITY"]);
                insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
                insert.DENY = GetValue<bool>(QueryResult.Tables[0].Rows[i]["DENY"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveRoleActivity(DB_ROLE_ACTIVITY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROLE_ACTIVITY", ToBeSaved.ID_ROLE_ACTIVITY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRoleActivity",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROLE", ToBeSaved.ID_ROLE)
														,new DB.InParameter("@ID_ACTIVITY", ToBeSaved.ID_ACTIVITY)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE)
														,new DB.InParameter("@DENY", ToBeSaved.DENY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROLE_ACTIVITY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRoleActivity(DB_ROLE_ACTIVITY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRoleActivity",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROLE_ACTIVITY", toBeDeleted.ID_ROLE_ACTIVITY)			
		}
            );
        }

        #endregion

        #region Table ROUTE

        public DB_ROUTE[] GetRoute(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ROUTE[])DB.ExecuteProcedure(
                "imrse_GetRoute",
                new DB.AnalyzeDataSet(GetRoute),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE", new long[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ROUTE[] GetRoute(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ROUTE[])DB.ExecuteProcedure(
                "imrse_GetRoute",
                new DB.AnalyzeDataSet(GetRoute),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetRoute(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE[] list = new DB_ROUTE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE insert = new DB_ROUTE();
                insert.ID_ROUTE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
                insert.ID_ROUTE_DEF = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF"]);
                insert.YEAR = GetValue<int>(QueryResult.Tables[0].Rows[i]["YEAR"]);
                insert.MONTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["MONTH"]);
                insert.WEEK = GetValue<int>(QueryResult.Tables[0].Rows[i]["WEEK"]);
                insert.ID_OPERATOR_EXECUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_EXECUTOR"]);
                insert.ID_OPERATOR_APPROVED = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_APPROVED"]);
                insert.DATE_UPLOADED = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE_UPLOADED"]));
                insert.DATE_APPROVED = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE_APPROVED"]));
                insert.DATE_FINISHED = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE_FINISHED"]));
                insert.ID_ROUTE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_STATUS"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);

                if (QueryResult.Tables[0].Columns.Contains("ID_ROUTE_TYPE"))
                {
                    insert.ID_ROUTE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_TYPE"]);
                }
                if (QueryResult.Tables[0].Columns.Contains("CREATION_DATE"))
                {
                    insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                }
                if (QueryResult.Tables[0].Columns.Contains("EXPIRATION_DATE"))
                {
                    insert.EXPIRATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["EXPIRATION_DATE"]));
                }
                if (QueryResult.Tables[0].Columns.Contains("NAME"))
                {
                    insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                }

                list[i] = insert;
            }
            return list;
        }

        public long SaveRoute(DB_ROUTE ToBeSaved)
        {
            List<DB.Parameter> parameters = new List<Data.DB.DB.Parameter>();
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE);

            parameters.AddRange(new DB.Parameter[] {
                InsertId
				, new DB.InParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF)
		        , new DB.InParameter("@YEAR", ToBeSaved.YEAR)
		        , new DB.InParameter("@MONTH", ToBeSaved.MONTH)
		        , new DB.InParameter("@WEEK", ToBeSaved.WEEK)
		        , new DB.InParameter("@ID_OPERATOR_EXECUTOR", ToBeSaved.ID_OPERATOR_EXECUTOR)
		        , new DB.InParameter("@ID_OPERATOR_APPROVED", ToBeSaved.ID_OPERATOR_APPROVED)
		        , new DB.InParameter("@DATE_UPLOADED", ConvertTimeZoneToUtcTime(ToBeSaved.DATE_UPLOADED))
		        , new DB.InParameter("@DATE_APPROVED", ConvertTimeZoneToUtcTime(ToBeSaved.DATE_APPROVED))
		        , new DB.InParameter("@DATE_FINISHED", ConvertTimeZoneToUtcTime(ToBeSaved.DATE_FINISHED))
		        , new DB.InParameter("@ID_ROUTE_STATUS", ToBeSaved.ID_ROUTE_STATUS)
		        , new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
            });

            if (IsParameterDefined("imrse_SaveRoute", "@ID_ROUTE_TYPE", this))
                parameters.Add(new DB.InParameter("@ID_ROUTE_TYPE", ToBeSaved.ID_ROUTE_TYPE));
            if (IsParameterDefined("imrse_SaveRoute", "@CREATION_DATE", this))
                parameters.Add(new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))); 
            if (IsParameterDefined("imrse_SaveRoute", "@EXPIRATION_DATE", this))
                parameters.Add(new DB.InParameter("@EXPIRATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.EXPIRATION_DATE)));
            if (IsParameterDefined("imrse_SaveRoute", "@NAME", this))
                parameters.Add(new DB.InParameter("@NAME", ToBeSaved.NAME));

            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRoute",
                parameters.ToArray()
            );

            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRoute(DB_ROUTE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRoute",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE", toBeDeleted.ID_ROUTE)			
		}
            );
        }

        #endregion
        #region Table ROUTE_DATA

        public DB_ROUTE_DATA[] GetRouteData()
        {
            return (DB_ROUTE_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteData",
                new DB.AnalyzeDataSet(GetRouteData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DATA", new long[] {})
					}
            );
        }

        public DB_ROUTE_DATA[] GetRouteData(long[] Ids)
        {
            return (DB_ROUTE_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteData",
                new DB.AnalyzeDataSet(GetRouteData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DATA", Ids)
					}
            );
        }

        private object GetRouteData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DATA[] list = new DB_ROUTE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DATA insert = new DB_ROUTE_DATA();
                insert.ID_ROUTE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DATA"]);
                insert.ID_ROUTE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveRouteData(DB_ROUTE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DATA", ToBeSaved.ID_ROUTE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRouteData(DB_ROUTE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DATA", toBeDeleted.ID_ROUTE_DATA)			
		}
            );
        }

        #endregion
        #region Table ROUTE_DEF

        public DB_ROUTE_DEF[] GetRouteDef()
        {
            return (DB_ROUTE_DEF[])DB.ExecuteProcedure(
                "imrse_GetRouteDef",
                new DB.AnalyzeDataSet(GetRouteDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF", new int[] {})
					}
            );
        }

        public DB_ROUTE_DEF[] GetRouteDef(int[] Ids)
        {
            return (DB_ROUTE_DEF[])DB.ExecuteProcedure(
                "imrse_GetRouteDef",
                new DB.AnalyzeDataSet(GetRouteDef),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF", Ids)
					}
            );
        }

        private object GetRouteDef(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEF[] list = new DB_ROUTE_DEF[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEF insert = new DB_ROUTE_DEF();
                insert.ID_ROUTE_DEF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.EXPIRATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["EXPIRATION_DATE"]));
                insert.DELETE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DELETE_DATE"]));
                insert.ID_ROUTE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_TYPE"]);
                insert.ID_ROUTE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_STATUS"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.AD_HOC = GetValue<bool>(QueryResult.Tables[0].Rows[i]["AD_HOC"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRouteDef(DB_ROUTE_DEF ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteDef",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@EXPIRATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.EXPIRATION_DATE))
														,new DB.InParameter("@DELETE_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.DELETE_DATE))
														,new DB.InParameter("@ID_ROUTE_TYPE", ToBeSaved.ID_ROUTE_TYPE)
														,new DB.InParameter("@ID_ROUTE_STATUS", ToBeSaved.ID_ROUTE_STATUS)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@AD_HOC", ToBeSaved.AD_HOC)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_DEF = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteDef(DB_ROUTE_DEF toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteDef",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DEF", toBeDeleted.ID_ROUTE_DEF)			
		}
            );
        }

        #endregion
        #region Table ROUTE_DEF_DATA

        public DB_ROUTE_DEF_DATA[] GetRouteDefData()
        {
            return (DB_ROUTE_DEF_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefData",
                new DB.AnalyzeDataSet(GetRouteDefData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF", new int[] {})
					}
            );
        }

        public DB_ROUTE_DEF_DATA[] GetRouteDefData(int[] Ids)
        {
            return (DB_ROUTE_DEF_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefData",
                new DB.AnalyzeDataSet(GetRouteDefData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF", Ids)
					}
            );
        }

        private object GetRouteDefData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEF_DATA[] list = new DB_ROUTE_DEF_DATA[count];
            bool IndexNbrColumnExists = QueryResult.Tables[0].Columns.Contains("INDEX_NBR");

            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEF_DATA insert = new DB_ROUTE_DEF_DATA();
                insert.ID_ROUTE_DEF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                if (IndexNbrColumnExists)
                {
                    insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                }
                list[i] = insert;
            }
            return list;
        }

        #endregion
        #region Table ROUTE_DEF_HISTORY

        public DB_ROUTE_DEF_HISTORY[] GetRouteDefHistory()
        {
            return (DB_ROUTE_DEF_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetRouteDefHistory",
                new DB.AnalyzeDataSet(GetRouteDefHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_HISTORY", new long[] {})
					}
            );
        }

        public DB_ROUTE_DEF_HISTORY[] GetRouteDefHistory(long[] Ids)
        {
            return (DB_ROUTE_DEF_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetRouteDefHistory",
                new DB.AnalyzeDataSet(GetRouteDefHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_HISTORY", Ids)
					}
            );
        }



        private object GetRouteDefHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEF_HISTORY[] list = new DB_ROUTE_DEF_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEF_HISTORY insert = new DB_ROUTE_DEF_HISTORY();
                insert.ID_ROUTE_DEF_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF_HISTORY"]);
                insert.ID_ROUTE_DEF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF"]);
                insert.ID_ROUTE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_STATUS"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
                insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveRouteDefHistory(DB_ROUTE_DEF_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEF_HISTORY", ToBeSaved.ID_ROUTE_DEF_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteDefHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF)
														,new DB.InParameter("@ID_ROUTE_STATUS", ToBeSaved.ID_ROUTE_STATUS)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
														,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_DEF_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRouteDefHistory(DB_ROUTE_DEF_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteDefHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DEF_HISTORY", toBeDeleted.ID_ROUTE_DEF_HISTORY)			
		}
            );
        }

        #endregion
        #region Table ROUTE_DEF_LOCATION

        public DB_ROUTE_DEF_LOCATION[] GetRouteDefLocation()
        {
            return (DB_ROUTE_DEF_LOCATION[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocation",
                new DB.AnalyzeDataSet(GetRouteDefLocation),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_LOCATION", new long[] {})
					}
            );
        }

        public DB_ROUTE_DEF_LOCATION[] GetRouteDefLocation(long[] Ids)
        {
            return (DB_ROUTE_DEF_LOCATION[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocation",
                new DB.AnalyzeDataSet(GetRouteDefLocation),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_LOCATION", Ids)
					}
            );
        }

        private object GetRouteDefLocation(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEF_LOCATION[] list = new DB_ROUTE_DEF_LOCATION[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEF_LOCATION insert = new DB_ROUTE_DEF_LOCATION();
                insert.ID_ROUTE_DEF_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF_LOCATION"]);
                insert.ID_ROUTE_DEF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ORDER_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER_NBR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveRouteDefLocation(DB_ROUTE_DEF_LOCATION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEF_LOCATION", ToBeSaved.ID_ROUTE_DEF_LOCATION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteDefLocation",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE_DEF", ToBeSaved.ID_ROUTE_DEF)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ORDER_NBR", ToBeSaved.ORDER_NBR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_DEF_LOCATION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRouteDefLocation(DB_ROUTE_DEF_LOCATION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteDefLocation",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DEF_LOCATION", toBeDeleted.ID_ROUTE_DEF_LOCATION)			
		}
            );
        }

        #endregion
        #region Table ROUTE_DEF_LOCATION_DATA

        public DB_ROUTE_DEF_LOCATION_DATA[] GetRouteDefLocationData()
        {
            return (DB_ROUTE_DEF_LOCATION_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocationData",
                new DB.AnalyzeDataSet(GetRouteDefLocationData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_LOCATION", new long[] {})
					}
            );
        }

        public DB_ROUTE_DEF_LOCATION_DATA[] GetRouteDefLocationData(long[] Ids)
        {
            return (DB_ROUTE_DEF_LOCATION_DATA[])DB.ExecuteProcedure(
                "imrse_GetRouteDefLocationData",
                new DB.AnalyzeDataSet(GetRouteDefLocationData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEF_LOCATION", Ids)
					}
            );
        }



        private object GetRouteDefLocationData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEF_LOCATION_DATA[] list = new DB_ROUTE_DEF_LOCATION_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEF_LOCATION_DATA insert = new DB_ROUTE_DEF_LOCATION_DATA();
                insert.ID_ROUTE_DEF_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEF_LOCATION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        #endregion
        #region Table ROUTE_DEVICE

        public DB_ROUTE_DEVICE[] GetRouteDevice()
        {
            return (DB_ROUTE_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetRouteDevice",
                new DB.AnalyzeDataSet(GetRouteDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEVICE", new long[] {})
					}
            );
        }

        public DB_ROUTE_DEVICE[] GetRouteDevice(long[] Ids)
        {
            return (DB_ROUTE_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetRouteDevice",
                new DB.AnalyzeDataSet(GetRouteDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_DEVICE", Ids)
					}
            );
        }

        private object GetRouteDevice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_DEVICE[] list = new DB_ROUTE_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_DEVICE insert = new DB_ROUTE_DEVICE();
                insert.ID_ROUTE_DEVICE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_DEVICE"]);
                insert.ID_ROUTE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveRouteDevice(DB_ROUTE_DEVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_DEVICE", ToBeSaved.ID_ROUTE_DEVICE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteDevice",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_DEVICE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRouteDevice(DB_ROUTE_DEVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteDevice",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_DEVICE", toBeDeleted.ID_ROUTE_DEVICE)			
		}
            );
        }

        #endregion
        #region Table ROUTE_ELEMENT_STATUS

        public DB_ROUTE_ELEMENT_STATUS[] GetRouteElementStatus()
        {
            return (DB_ROUTE_ELEMENT_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteElementStatus",
                new DB.AnalyzeDataSet(GetRouteElementStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_ELEMENT_STATUS", new int[] {})
					}
            );
        }

        public DB_ROUTE_ELEMENT_STATUS[] GetRouteElementStatus(int[] Ids)
        {
            return (DB_ROUTE_ELEMENT_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteElementStatus",
                new DB.AnalyzeDataSet(GetRouteElementStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_ELEMENT_STATUS", Ids)
					}
            );
        }

        private object GetRouteElementStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_ELEMENT_STATUS[] list = new DB_ROUTE_ELEMENT_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_ELEMENT_STATUS insert = new DB_ROUTE_ELEMENT_STATUS();
                insert.ID_ROUTE_ELEMENT_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_ELEMENT_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRouteElementStatus(DB_ROUTE_ELEMENT_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_ELEMENT_STATUS", ToBeSaved.ID_ROUTE_ELEMENT_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteElementStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_ELEMENT_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteElementStatus(DB_ROUTE_ELEMENT_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteElementStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_ELEMENT_STATUS", toBeDeleted.ID_ROUTE_ELEMENT_STATUS)			
		}
            );
        }

        #endregion
        #region Table ROUTE_POINT

        public DB_ROUTE_POINT[] GetRoutePoint()
        {
            return (DB_ROUTE_POINT[])DB.ExecuteProcedure(
                "imrse_GetRoutePoint",
                new DB.AnalyzeDataSet(GetRoutePoint),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_POINT", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_ROUTE_POINT[] GetRoutePoint(int[] Ids)
        {
            return (DB_ROUTE_POINT[])DB.ExecuteProcedure(
                "imrse_GetRoutePoint",
                new DB.AnalyzeDataSet(GetRoutePoint),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_POINT", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetRoutePoint(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_POINT[] list = new DB_ROUTE_POINT[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_POINT insert = new DB_ROUTE_POINT();
                insert.ID_ROUTE_POINT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_POINT"]);
                insert.ID_ROUTE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE"]);
                insert.TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["TYPE"]);
                insert.ID_TASK = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insert.START_DATE_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE_TIME"]));
                insert.END_DATE_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE_TIME"]));
                insert.LATITUDE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["LATITUDE"]);
                insert.LONGITUDE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["LONGITUDE"]);
                insert.KM_COUNTER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["KM_COUNTER"]);
                insert.QUOTE = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["QUOTE"]);
                insert.INVOICE_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["INVOICE_NUMBER"]);
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.SIGNATURE = (byte[])GetValue(QueryResult.Tables[0].Rows[i]["SIGNATURE"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRoutePoint(DB_ROUTE_POINT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_POINT", ToBeSaved.ID_ROUTE_POINT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRoutePoint",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE", ToBeSaved.ID_ROUTE)
														,new DB.InParameter("@TYPE", ToBeSaved.TYPE)
														,new DB.InParameter("@ID_TASK", ToBeSaved.ID_TASK)
														,new DB.InParameter("@START_DATE_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE_TIME))
														,new DB.InParameter("@END_DATE_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE_TIME))
														,new DB.InParameter("@LATITUDE", ToBeSaved.LATITUDE)
														,new DB.InParameter("@LONGITUDE", ToBeSaved.LONGITUDE)
														,new DB.InParameter("@KM_COUNTER", ToBeSaved.KM_COUNTER)
														,new DB.InParameter("@QUOTE", ToBeSaved.QUOTE)
														,new DB.InParameter("@INVOICE_NUMBER", ToBeSaved.INVOICE_NUMBER)
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@SIGNATURE", ToBeSaved.SIGNATURE)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_POINT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRoutePoint(DB_ROUTE_POINT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRoutePoint",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_POINT", toBeDeleted.ID_ROUTE_POINT)			
		}
            );
        }

        #endregion
        #region Table ROUTE_POINT_DATA

        public DB_ROUTE_POINT_DATA[] GetRoutePointData()
        {
            return (DB_ROUTE_POINT_DATA[])DB.ExecuteProcedure(
                "imrse_GetRoutePointData",
                new DB.AnalyzeDataSet(GetRoutePointData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_POINT_DATA", new long[] {})
					}
            );
        }

        public DB_ROUTE_POINT_DATA[] GetRoutePointData(long[] Ids)
        {
            return (DB_ROUTE_POINT_DATA[])DB.ExecuteProcedure(
                "imrse_GetRoutePointData",
                new DB.AnalyzeDataSet(GetRoutePointData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_POINT_DATA", Ids)
					}
            );
        }

        private object GetRoutePointData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_POINT_DATA[] list = new DB_ROUTE_POINT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_POINT_DATA insert = new DB_ROUTE_POINT_DATA();
                insert.ID_ROUTE_POINT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_POINT_DATA"]);
                insert.ID_ROUTE_POINT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_POINT"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveRoutePointData(DB_ROUTE_POINT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_POINT_DATA", ToBeSaved.ID_ROUTE_POINT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRoutePointData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_ROUTE_POINT", ToBeSaved.ID_ROUTE_POINT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_POINT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteRoutePointData(DB_ROUTE_POINT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRoutePointData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_POINT_DATA", toBeDeleted.ID_ROUTE_POINT_DATA)			
		}
            );
        }

        #endregion
        #region Table ROUTE_STATUS

        public DB_ROUTE_STATUS[] GetRouteStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ROUTE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteStatus",
                new DB.AnalyzeDataSet(GetRouteStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_STATUS", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_ROUTE_STATUS[] GetRouteStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_ROUTE_STATUS[])DB.ExecuteProcedure(
                "imrse_GetRouteStatus",
                new DB.AnalyzeDataSet(GetRouteStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_STATUS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetRouteStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_STATUS[] list = new DB_ROUTE_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_STATUS insert = new DB_ROUTE_STATUS();
                insert.ID_ROUTE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRouteStatus(DB_ROUTE_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_STATUS", ToBeSaved.ID_ROUTE_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteStatus(DB_ROUTE_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_STATUS", toBeDeleted.ID_ROUTE_STATUS)			
		}
            );
        }

        #endregion
        #region Table ROUTE_TYPE

        public DB_ROUTE_TYPE[] GetRouteType()
        {
            return (DB_ROUTE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteType",
                new DB.AnalyzeDataSet(GetRouteType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_TYPE", new int[] {})
					}
            );
        }

        public DB_ROUTE_TYPE[] GetRouteType(int[] Ids)
        {
            return (DB_ROUTE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteType",
                new DB.AnalyzeDataSet(GetRouteType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_ROUTE_TYPE", Ids)
					}
            );
        }

        private object GetRouteType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_TYPE[] list = new DB_ROUTE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_TYPE insert = new DB_ROUTE_TYPE();
                insert.ID_ROUTE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRouteType(DB_ROUTE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_TYPE", ToBeSaved.ID_ROUTE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteType(DB_ROUTE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_ROUTE_TYPE", toBeDeleted.ID_ROUTE_TYPE)			
		}
            );
        }

        #endregion
        #region Table ROUTE_TYPE_ACTION_TYPE

        public DB_ROUTE_TYPE_ACTION_TYPE[] GetRouteTypeActionType()
        {
            return (DB_ROUTE_TYPE_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteTypeActionType",
                new DB.AnalyzeDataSet(GetRouteTypeActionType),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_ROUTE_TYPE_ACTION_TYPE", new int[] {})
					}
            );
        }

        public DB_ROUTE_TYPE_ACTION_TYPE[] GetRouteTypeActionType(int[] Ids)
        {
            return (DB_ROUTE_TYPE_ACTION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetRouteTypeActionType",
                new DB.AnalyzeDataSet(GetRouteTypeActionType),
                new DB.Parameter[] { 
			        CreateTableParam("@ID_ROUTE_TYPE_ACTION_TYPE", Ids)
					}
            );
        }



        private object GetRouteTypeActionType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_ROUTE_TYPE_ACTION_TYPE[] list = new DB_ROUTE_TYPE_ACTION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_ROUTE_TYPE_ACTION_TYPE insert = new DB_ROUTE_TYPE_ACTION_TYPE();
                insert.ID_ROUTE_TYPE_ACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_TYPE_ACTION_TYPE"]);
                insert.ID_ROUTE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ROUTE_TYPE"]);
                insert.ID_ACTION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTION_TYPE"]);
                insert.DEFAULT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["DEFAULT"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveRouteTypeActionType(DB_ROUTE_TYPE_ACTION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_ROUTE_TYPE_ACTION_TYPE", ToBeSaved.ID_ROUTE_TYPE_ACTION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveRouteTypeActionType",
                new DB.Parameter[] {
			                            InsertId
										,new DB.InParameter("@ID_ROUTE_TYPE", ToBeSaved.ID_ROUTE_TYPE)
										,new DB.InParameter("@ID_ACTION_TYPE", ToBeSaved.ID_ACTION_TYPE)
										,new DB.InParameter("@DEFAULT", ToBeSaved.DEFAULT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_ROUTE_TYPE_ACTION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteRouteTypeActionType(DB_ROUTE_TYPE_ACTION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelRouteTypeActionType",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_ROUTE_TYPE_ACTION_TYPE", toBeDeleted.ID_ROUTE_TYPE_ACTION_TYPE)			
		        }
            );
        }

        #endregion

        #region Table SCHEDULE

        public DB_SCHEDULE[] GetSchedule()
        {
            return (DB_SCHEDULE[])DB.ExecuteProcedure(
                "imrse_GetSchedule",
                new DB.AnalyzeDataSet(GetSchedule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE", new int[] {})
					}
            );
        }

        public DB_SCHEDULE[] GetSchedule(int[] Ids)
        {
            return (DB_SCHEDULE[])DB.ExecuteProcedure(
                "imrse_GetSchedule",
                new DB.AnalyzeDataSet(GetSchedule),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE", Ids)
					}
            );
        }

        public void DeleteSchedule(DB_SCHEDULE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSchedule",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SCHEDULE", toBeDeleted.ID_SCHEDULE)			
		}
            );
        }

        #endregion
        #region Table SCHEDULE_TYPE

        public DB_SCHEDULE_TYPE[] GetScheduleType()
        {
            return (DB_SCHEDULE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetScheduleType",
                new DB.AnalyzeDataSet(GetScheduleType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE_TYPE", new int[] {})
					}
            );
        }

        public DB_SCHEDULE_TYPE[] GetScheduleType(int[] Ids)
        {
            return (DB_SCHEDULE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetScheduleType",
                new DB.AnalyzeDataSet(GetScheduleType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SCHEDULE_TYPE", Ids)
					}
            );
        }

        private object GetScheduleType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SCHEDULE_TYPE[] list = new DB_SCHEDULE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SCHEDULE_TYPE insert = new DB_SCHEDULE_TYPE();
                insert.ID_SCHEDULE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SCHEDULE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveScheduleType(DB_SCHEDULE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SCHEDULE_TYPE", ToBeSaved.ID_SCHEDULE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveScheduleType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SCHEDULE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteScheduleType(DB_SCHEDULE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelScheduleType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SCHEDULE_TYPE", toBeDeleted.ID_SCHEDULE_TYPE)			
		}
            );
        }

        #endregion

        #region Table SEAL

        public DB_SEAL[] GetSeal()
        {
            return (DB_SEAL[])DB.ExecuteProcedure(
                "imrse_GetSeal",
                new DB.AnalyzeDataSet(GetSeal),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SEAL", new long[] {})
					}
            );
        }

        public DB_SEAL[] GetSeal(long[] Ids)
        {
            return (DB_SEAL[])DB.ExecuteProcedure(
                "imrse_GetSeal",
                new DB.AnalyzeDataSet(GetSeal),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SEAL", Ids)
					}
            );
        }

        private object GetSeal(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SEAL[] list = new DB_SEAL[count];
            for (int i = 0; i < count; i++)
            {
                DB_SEAL insert = new DB_SEAL();
                insert.ID_SEAL = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SEAL"]);
                insert.SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSeal(DB_SEAL ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SEAL", ToBeSaved.ID_SEAL);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSeal",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SEAL = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSeal(DB_SEAL toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSeal",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SEAL", toBeDeleted.ID_SEAL)			
		}
            );
        }

        #endregion

        #region Table SERVICE

        public DB_SERVICE[] GetService(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetService",
                new DB.AnalyzeDataSet(GetService),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE[] GetService(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE[])DB.ExecuteProcedure(
                "imrse_GetService",
                new DB.AnalyzeDataSet(GetService),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetService(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE[] list = new DB_SERVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE insert = new DB_SERVICE();
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DEVICE_ORDER_NUMBER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.DELETE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DELETE_DATE"]));
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveService(DB_SERVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveService",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER)
														,new DB.InParameter("@DELETE_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.DELETE_DATE))
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteService(DB_SERVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelService",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE", toBeDeleted.ID_SERVICE)			
		}
            );
        }

        #endregion
        #region Table SERVICE_ACTION_RESULT

        public DB_SERVICE_ACTION_RESULT[] GetServiceActionResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceActionResult",
                new DB.AnalyzeDataSet(GetServiceActionResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_ACTION_RESULT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_ACTION_RESULT[] GetServiceActionResult(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_ACTION_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceActionResult",
                new DB.AnalyzeDataSet(GetServiceActionResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_ACTION_RESULT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceActionResult(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_ACTION_RESULT[] list = new DB_SERVICE_ACTION_RESULT[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_ACTION_RESULT insert = new DB_SERVICE_ACTION_RESULT();
                insert.ID_SERVICE_ACTION_RESULT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_ACTION_RESULT"]);
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ALLOW_INPUT_TEXT = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ALLOW_INPUT_TEXT"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.ID_SERVICE_REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_REFERENCE_TYPE"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }


        public int SaveServiceActionResult(DB_SERVICE_ACTION_RESULT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_ACTION_RESULT", ToBeSaved.ID_SERVICE_ACTION_RESULT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceActionResult",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ALLOW_INPUT_TEXT", ToBeSaved.ALLOW_INPUT_TEXT)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_SERVICE_REFERENCE_TYPE", ToBeSaved.ID_SERVICE_REFERENCE_TYPE)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
													,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_ACTION_RESULT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteServiceActionResult(DB_SERVICE_ACTION_RESULT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceActionResult",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_ACTION_RESULT", toBeDeleted.ID_SERVICE_ACTION_RESULT)			
		}
            );
        }

        #endregion
        #region Table SERVICE_DATA

        public DB_SERVICE_DATA[] GetServiceData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceData",
                new DB.AnalyzeDataSet(GetServiceData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_DATA[] GetServiceData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceData",
                new DB.AnalyzeDataSet(GetServiceData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_DATA[] list = new DB_SERVICE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_DATA insert = new DB_SERVICE_DATA();
                insert.ID_SERVICE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_DATA"]);
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }


        public long SaveServiceData(DB_SERVICE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_DATA", ToBeSaved.ID_SERVICE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteServiceData(DB_SERVICE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_DATA", toBeDeleted.ID_SERVICE_DATA)			
		}
            );
        }

        #endregion
        #region Table SERVICE_CUSTOM_DATA

        public DB_SERVICE_CUSTOM_DATA[] GetServiceCustomData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_CUSTOM_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceCustomData",
                new DB.AnalyzeDataSet(GetServiceCustomData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_CUSTOM_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_CUSTOM_DATA[] GetServiceCustomData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_CUSTOM_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceCustomData",
                new DB.AnalyzeDataSet(GetServiceCustomData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_CUSTOM_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceCustomData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_CUSTOM_DATA[] list = new DB_SERVICE_CUSTOM_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_CUSTOM_DATA insert = new DB_SERVICE_CUSTOM_DATA();
                insert.ID_SERVICE_CUSTOM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_CUSTOM_DATA"]);
                insert.ID_SERVICE_CUSTOM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_CUSTOM"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveServiceCustomData(DB_SERVICE_CUSTOM_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_CUSTOM_DATA", ToBeSaved.ID_SERVICE_CUSTOM_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceCustomData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_CUSTOM", ToBeSaved.ID_SERVICE_CUSTOM)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_CUSTOM_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServiceCustomData(DB_SERVICE_CUSTOM_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceCustomData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_CUSTOM_DATA", toBeDeleted.ID_SERVICE_CUSTOM_DATA)			
		}
            );
        }

        #endregion
        #region Table SERVICE_CUSTOM

        public DB_SERVICE_CUSTOM[] GetServiceCustom(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_CUSTOM[])DB.ExecuteProcedure(
                "imrse_GetServiceCustom",
                new DB.AnalyzeDataSet(GetServiceCustom),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_CUSTOM", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_CUSTOM[] GetServiceCustom(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_CUSTOM[])DB.ExecuteProcedure(
                "imrse_GetServiceCustom",
                new DB.AnalyzeDataSet(GetServiceCustom),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_CUSTOM", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceCustom(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_CUSTOM[] list = new DB_SERVICE_CUSTOM[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_CUSTOM insert = new DB_SERVICE_CUSTOM();
                insert.ID_SERVICE_CUSTOM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_CUSTOM"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.EDITOR_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["EDITOR_COST"]);
                insert.ENGINEER_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["ENGINEER_COST"]);
                insert.DIRECT_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["DIRECT_COST"]);
                insert.INDIRECT_COST = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["INDIRECT_COST"]);
                insert.MATERIAL_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["MATERIAL_COST"]);
                insert.SUPPLY_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["SUPPLY_COST"]);
                insert.TRANSPORT_COST = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["TRANSPORT_COST"]);
                insert.ID_FILE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_FILE"]);
                insert.CUSTOM_VALUE = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["CUSTOM_VALUE"]);
                insert.EDITOR_TIME = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["EDITOR_TIME"]);
                insert.ENGINEER_TIME = GetNullableValue<Double>(QueryResult.Tables[0].Rows[i]["ENGINEER_TIME"]);
                list[i] = insert;
            }
            return list;
        }



        public long SaveServiceCustom(DB_SERVICE_CUSTOM ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_CUSTOM", ToBeSaved.ID_SERVICE_CUSTOM);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceCustom",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@EDITOR_COST", ToBeSaved.EDITOR_COST)
														,new DB.InParameter("@ENGINEER_COST", ToBeSaved.ENGINEER_COST)
														,new DB.InParameter("@DIRECT_COST", ToBeSaved.DIRECT_COST)
														,new DB.InParameter("@INDIRECT_COST", ToBeSaved.INDIRECT_COST)
														,new DB.InParameter("@MATERIAL_COST", ToBeSaved.MATERIAL_COST)
														,new DB.InParameter("@SUPPLY_COST", ToBeSaved.SUPPLY_COST)
														,new DB.InParameter("@TRANSPORT_COST", ToBeSaved.TRANSPORT_COST)
														,new DB.InParameter("@ID_FILE", ToBeSaved.ID_FILE)
														,new DB.InParameter("@CUSTOM_VALUE", ToBeSaved.CUSTOM_VALUE)
														,new DB.InParameter("@EDITOR_TIME", ToBeSaved.EDITOR_TIME)
														,new DB.InParameter("@ENGINEER_TIME", ToBeSaved.ENGINEER_TIME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_CUSTOM = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteServiceCustom(DB_SERVICE_CUSTOM toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceCustom",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_CUSTOM", toBeDeleted.ID_SERVICE_CUSTOM)			
		}
            );
        }

        #endregion
        #region Table SERVICE_LIST

        public DB_SERVICE_LIST[] GetServiceList(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST[])DB.ExecuteProcedure(
                "imrse_GetServiceList",
                new DB.AnalyzeDataSet(GetServiceList),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST[] GetServiceList(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST[])DB.ExecuteProcedure(
                "imrse_GetServiceList",
                new DB.AnalyzeDataSet(GetServiceList),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceList(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST[] list = new DB_SERVICE_LIST[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST insert = new DB_SERVICE_LIST();
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.ID_OPERATOR_RECEIVER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_RECEIVER"]);
                insert.ID_OPERATOR_SERVICER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_SERVICER"]);
                insert.COURIER_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["COURIER_NAME"]);
                insert.PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["PRIORITY"]);
                insert.SHIPPING_LETTER_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHIPPING_LETTER_NBR"]);
                insert.EXPECTED_FINISH_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["EXPECTED_FINISH_DATE"]));
                insert.SERVICE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERVICE_NAME"]);
                insert.ID_DOCUMENT_PACKAGE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DOCUMENT_PACKAGE"]);
                insert.CONFIRMED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["CONFIRMED"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveServiceList(DB_SERVICE_LIST ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceList",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@ID_OPERATOR_RECEIVER", ToBeSaved.ID_OPERATOR_RECEIVER)
														,new DB.InParameter("@ID_OPERATOR_SERVICER", ToBeSaved.ID_OPERATOR_SERVICER)
														,new DB.InParameter("@COURIER_NAME", ToBeSaved.COURIER_NAME)
														,new DB.InParameter("@PRIORITY", ToBeSaved.PRIORITY)
														,new DB.InParameter("@SHIPPING_LETTER_NBR", ToBeSaved.SHIPPING_LETTER_NBR)
														,new DB.InParameter("@EXPECTED_FINISH_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.EXPECTED_FINISH_DATE))
														,new DB.InParameter("@SERVICE_NAME", ToBeSaved.SERVICE_NAME)
														,new DB.InParameter("@ID_DOCUMENT_PACKAGE", ToBeSaved.ID_DOCUMENT_PACKAGE)
														,new DB.InParameter("@CONFIRMED", ToBeSaved.CONFIRMED)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteServiceList(DB_SERVICE_LIST toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceList",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST", toBeDeleted.ID_SERVICE_LIST)			
		}
            );
        }




        #endregion
        #region Table SERVICE_LIST_DATA

        public DB_SERVICE_LIST_DATA[] GetServiceListData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceListData",
                new DB.AnalyzeDataSet(GetServiceListData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DATA[] GetServiceListData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceListData",
                new DB.AnalyzeDataSet(GetServiceListData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DATA[] list = new DB_SERVICE_LIST_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DATA insert = new DB_SERVICE_LIST_DATA();
                insert.ID_SERVICE_LIST_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST_DATA"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }
        public long SaveServiceListData(DB_SERVICE_LIST_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST_DATA", ToBeSaved.ID_SERVICE_LIST_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServiceListData(DB_SERVICE_LIST_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST_DATA", toBeDeleted.ID_SERVICE_LIST_DATA)			
		}
            );
        }

        #endregion
        #region Table SERVICE_DIAGNOSTIC_RESULT

        public DB_SERVICE_DIAGNOSTIC_RESULT[] GetServiceDiagnosticResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_DIAGNOSTIC_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceDiagnosticResult",
                new DB.AnalyzeDataSet(GetServiceDiagnosticResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_DIAGNOSTIC_RESULT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_DIAGNOSTIC_RESULT[] GetServiceDiagnosticResult(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_DIAGNOSTIC_RESULT[])DB.ExecuteProcedure(
                "imrse_GetServiceDiagnosticResult",
                new DB.AnalyzeDataSet(GetServiceDiagnosticResult),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_DIAGNOSTIC_RESULT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceDiagnosticResult(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_DIAGNOSTIC_RESULT[] list = new DB_SERVICE_DIAGNOSTIC_RESULT[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_DIAGNOSTIC_RESULT insert = new DB_SERVICE_DIAGNOSTIC_RESULT();
                insert.ID_SERVICE_DIAGNOSTIC_RESULT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_DIAGNOSTIC_RESULT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.IS_SERVICE_RESULT = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_SERVICE_RESULT"]);
                insert.IS_DIAGNOSTIC_RESULT = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["IS_DIAGNOSTIC_RESULT"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveServiceDiagnosticResult(DB_SERVICE_DIAGNOSTIC_RESULT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_DIAGNOSTIC_RESULT", ToBeSaved.ID_SERVICE_DIAGNOSTIC_RESULT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceDiagnosticResult",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@IS_SERVICE_RESULT", ToBeSaved.IS_SERVICE_RESULT)
														,new DB.InParameter("@IS_DIAGNOSTIC_RESULT", ToBeSaved.IS_DIAGNOSTIC_RESULT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_DIAGNOSTIC_RESULT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteServiceDiagnosticResult(DB_SERVICE_DIAGNOSTIC_RESULT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceDiagnosticResult",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_DIAGNOSTIC_RESULT", toBeDeleted.ID_SERVICE_DIAGNOSTIC_RESULT)			
		}
            );
        }

        #endregion
        #region Table SERVICE_LIST_DEVICE

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDevice(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDevice",
                new DB.AnalyzeDataSet(GetServiceListDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE[] GetServiceListDevice(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetServiceListDevice",
                new DB.AnalyzeDataSet(GetServiceListDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListDevice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE[] list = new DB_SERVICE_LIST_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE insert = new DB_SERVICE_LIST_DEVICE();
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.FAILURE_DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FAILURE_DESCRIPTION"]);
                insert.FAILURE_SUGGESTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FAILURE_SUGGESTION"]);
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.PHOTO = GetValue<long>(QueryResult.Tables[0].Rows[i]["PHOTO"]);
                insert.ID_OPERATOR_SERVICER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_SERVICER"]);
                insert.SERVICE_SHORT_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERVICE_SHORT_DESCR"]);
                insert.SERVICE_LONG_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERVICE_LONG_DESCR"]);
                insert.PHOTO_SERVICED = GetValue<long>(QueryResult.Tables[0].Rows[i]["PHOTO_SERVICED"]);
                insert.ATEX_OK = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["ATEX_OK"]);
                insert.PAY_FOR_SERVICE = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["PAY_FOR_SERVICE"]);
                insert.ID_SERVICE_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_STATUS"]);
                insert.ID_DIAGNOSTIC_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_STATUS"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                list[i] = insert;
            }
            return list;
        }
        public int SaveServiceListDevice(DB_SERVICE_LIST_DEVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListDevice",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@FAILURE_DESCRIPTION", ToBeSaved.FAILURE_DESCRIPTION)
														,new DB.InParameter("@FAILURE_SUGGESTION", ToBeSaved.FAILURE_SUGGESTION)
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@PHOTO", ToBeSaved.PHOTO)
														,new DB.InParameter("@ID_OPERATOR_SERVICER", ToBeSaved.ID_OPERATOR_SERVICER)
														,new DB.InParameter("@SERVICE_SHORT_DESCR", ToBeSaved.SERVICE_SHORT_DESCR)
														,new DB.InParameter("@SERVICE_LONG_DESCR", ToBeSaved.SERVICE_LONG_DESCR)
														,new DB.InParameter("@PHOTO_SERVICED", ToBeSaved.PHOTO_SERVICED)
														,new DB.InParameter("@ATEX_OK", ToBeSaved.ATEX_OK)
														,new DB.InParameter("@PAY_FOR_SERVICE", ToBeSaved.PAY_FOR_SERVICE)
														,new DB.InParameter("@ID_SERVICE_STATUS", ToBeSaved.ID_SERVICE_STATUS)
														,new DB.InParameter("@ID_DIAGNOSTIC_STATUS", ToBeSaved.ID_DIAGNOSTIC_STATUS)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST = (int)InsertId.Value;

            return (int)InsertId.Value;
        }



        public void DeleteServiceListDevice(DB_SERVICE_LIST_DEVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListDevice",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST", toBeDeleted.ID_SERVICE_LIST)			
		}
            );
        }

        public void DeleteServiceListDevice(int idServiceList, long serialNbr)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_u_DelServiceListDevice",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST", SqlDbType.Int, idServiceList)			,
            new DB.InParameter("@SERIAL_NBR", SqlDbType.BigInt, serialNbr)		
		}
            );
        }

        #endregion
        #region Table SERVICE_LIST_DEVICE_DATA

        public DB_SERVICE_LIST_DEVICE_DATA[] GetServiceListDeviceData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceData",
                new DB.AnalyzeDataSet(GetServiceListDeviceData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DEVICE_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE_DATA[] GetServiceListDeviceData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceData",
                new DB.AnalyzeDataSet(GetServiceListDeviceData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DEVICE_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListDeviceData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE_DATA[] list = new DB_SERVICE_LIST_DEVICE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE_DATA insert = new DB_SERVICE_LIST_DEVICE_DATA();
                insert.ID_SERVICE_LIST_DEVICE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST_DEVICE_DATA"]);
                insert.ID_SERVICE_LIST = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.SERIAL_NBR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }
        public long SaveServiceListDeviceData(DB_SERVICE_LIST_DEVICE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST_DEVICE_DATA", ToBeSaved.ID_SERVICE_LIST_DEVICE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListDeviceData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST_DEVICE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServiceListDeviceData(DB_SERVICE_LIST_DEVICE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListDeviceData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST_DEVICE_DATA", toBeDeleted.ID_SERVICE_LIST_DEVICE_DATA)			
		}
            );
        }

        #endregion
        #region Table SERVICE_LIST_DEVICE_ACTION

        public DB_SERVICE_LIST_DEVICE_ACTION[] GetServiceListDeviceAction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_ACTION[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceAction",
                new DB.AnalyzeDataSet(GetServiceListDeviceAction),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE_ACTION[] GetServiceListDeviceAction(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_ACTION[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceAction",
                new DB.AnalyzeDataSet(GetServiceListDeviceAction),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListDeviceAction(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE_ACTION[] list = new DB_SERVICE_LIST_DEVICE_ACTION[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE_ACTION insert = new DB_SERVICE_LIST_DEVICE_ACTION();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.ID_SERVICE_CUSTOM = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_CUSTOM"]);
                list[i] = insert;
            }
            return list;
        }
        public long SaveServiceListDeviceAction(DB_SERVICE_LIST_DEVICE_ACTION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListDeviceAction",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@ID_SERVICE_CUSTOM", ToBeSaved.ID_SERVICE_CUSTOM)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.SERIAL_NBR = (long)InsertId.Value;

            return (long)InsertId.Value;
        }



        public void DeleteServiceListDeviceAction(DB_SERVICE_LIST_DEVICE_ACTION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListDeviceAction",
                new DB.Parameter[] {
            new DB.InParameter("@SERIAL_NBR", toBeDeleted.SERIAL_NBR)			
		}
            );
        }



        #endregion
        #region Table SERVICE_LIST_DEVICE_ACTION_DETAILS

        public DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] GetServiceListDeviceActionDetails(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceActionDetails",
                new DB.AnalyzeDataSet(GetServiceListDeviceActionDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DEVICE_ACTION_DETAILS", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] GetServiceListDeviceActionDetails(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceActionDetails",
                new DB.AnalyzeDataSet(GetServiceListDeviceActionDetails),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST_DEVICE_ACTION_DETAILS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListDeviceActionDetails(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] list = new DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE_ACTION_DETAILS insert = new DB_SERVICE_LIST_DEVICE_ACTION_DETAILS();
                insert.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST_DEVICE_ACTION_DETAILS"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                insert.INPUT_TEXT = GetValue<string>(QueryResult.Tables[0].Rows[i]["INPUT_TEXT"]);
                insert.ID_SERVICE_ACTION_RESULT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_ACTION_RESULT"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveServiceListDeviceActionDetails(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST_DEVICE_ACTION_DETAILS", ToBeSaved.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListDeviceActionDetails",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE)
														,new DB.InParameter("@INPUT_TEXT", ToBeSaved.INPUT_TEXT)
														,new DB.InParameter("@ID_SERVICE_ACTION_RESULT", ToBeSaved.ID_SERVICE_ACTION_RESULT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServiceListDeviceActionDetails(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListDeviceActionDetails",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST_DEVICE_ACTION_DETAILS", toBeDeleted.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS)			
		}
            );
        }

        #endregion
        #region Table SERVICE_LIST_DEVICE_DIAGNOSTIC

        public DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] GetServiceListDeviceDiagnostic(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceDiagnostic",
                new DB.AnalyzeDataSet(GetServiceListDeviceDiagnostic),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] GetServiceListDeviceDiagnostic(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[])DB.ExecuteProcedure(
                "imrse_GetServiceListDeviceDiagnostic",
                new DB.AnalyzeDataSet(GetServiceListDeviceDiagnostic),
                new DB.Parameter[] { 
			CreateTableParam("@SERIAL_NBR", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceListDeviceDiagnostic(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] list = new DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_LIST_DEVICE_DIAGNOSTIC insert = new DB_SERVICE_LIST_DEVICE_DIAGNOSTIC();
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_DIAGNOSTIC_ACTION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION"]);
                insert.ID_DIAGNOSTIC_ACTION_RESULT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DIAGNOSTIC_ACTION_RESULT"]);
                insert.INPUT_TEXT = GetValue<string>(QueryResult.Tables[0].Rows[i]["INPUT_TEXT"]);
                insert.DAMAGE_LEVEL = GetValue(QueryResult.Tables[0].Rows[i]["DAMAGE_LEVEL"]);
                list[i] = insert;
            }
            return list;
        }
        public long SaveServiceListDeviceDiagnostic(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceListDeviceDiagnostic",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@ID_DIAGNOSTIC_ACTION", ToBeSaved.ID_DIAGNOSTIC_ACTION)
														,new DB.InParameter("@ID_DIAGNOSTIC_ACTION_RESULT", ToBeSaved.ID_DIAGNOSTIC_ACTION_RESULT)
														,new DB.InParameter("@INPUT_TEXT", ToBeSaved.INPUT_TEXT)
														,ParamObject("@DAMAGE_LEVEL", ToBeSaved.DAMAGE_LEVEL, 0, 0)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.SERIAL_NBR = (long)InsertId.Value;

            return (long)InsertId.Value;
        }



        public void DeleteServiceListDeviceDiagnostic(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceListDeviceDiagnostic",
                new DB.Parameter[] {
            new DB.InParameter("@SERIAL_NBR", toBeDeleted.SERIAL_NBR)			
		}
            );
        }

        #endregion
        #region Table SERVICE_IN_PACKAGE

        public DB_SERVICE_IN_PACKAGE[] GetServiceInPackage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_IN_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServiceInPackage",
                new DB.AnalyzeDataSet(GetServiceInPackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_IN_PACKAGE", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_IN_PACKAGE[] GetServiceInPackage(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_IN_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServiceInPackage",
                new DB.AnalyzeDataSet(GetServiceInPackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_IN_PACKAGE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceInPackage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_IN_PACKAGE[] list = new DB_SERVICE_IN_PACKAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_IN_PACKAGE insert = new DB_SERVICE_IN_PACKAGE();
                insert.ID_SERVICE_IN_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_IN_PACKAGE"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.ID_SERVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveServiceInPackage(DB_SERVICE_IN_PACKAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_IN_PACKAGE", ToBeSaved.ID_SERVICE_IN_PACKAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceInPackage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@ID_SERVICE", ToBeSaved.ID_SERVICE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_IN_PACKAGE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServiceInPackage(DB_SERVICE_IN_PACKAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceInPackage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_IN_PACKAGE", toBeDeleted.ID_SERVICE_IN_PACKAGE)			
		}
            );
        }

        #endregion
        #region Table SERVICE_PACKAGE

        public DB_SERVICE_PACKAGE[] GetServicePackage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServicePackage",
                new DB.AnalyzeDataSet(GetServicePackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_PACKAGE", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_PACKAGE[] GetServicePackage(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_PACKAGE[])DB.ExecuteProcedure(
                "imrse_GetServicePackage",
                new DB.AnalyzeDataSet(GetServicePackage),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_PACKAGE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServicePackage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_PACKAGE[] list = new DB_SERVICE_PACKAGE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_PACKAGE insert = new DB_SERVICE_PACKAGE();
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.ID_CONTRACT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.IS_SINGLE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_SINGLE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveServicePackage(DB_SERVICE_PACKAGE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServicePackage",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_CONTRACT", ToBeSaved.ID_CONTRACT)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
														,new DB.InParameter("@IS_SINGLE", ToBeSaved.IS_SINGLE)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_PACKAGE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServicePackage(DB_SERVICE_PACKAGE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServicePackage",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_PACKAGE", toBeDeleted.ID_SERVICE_PACKAGE)			
		}
            );
        }

        #endregion
        #region Table SERVICE_PACKAGE_DATA

        public DB_SERVICE_PACKAGE_DATA[] GetServicePackageData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServicePackageData",
                new DB.AnalyzeDataSet(GetServicePackageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_PACKAGE_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_PACKAGE_DATA[] GetServicePackageData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_PACKAGE_DATA[])DB.ExecuteProcedure(
                "imrse_GetServicePackageData",
                new DB.AnalyzeDataSet(GetServicePackageData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_PACKAGE_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServicePackageData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_PACKAGE_DATA[] list = new DB_SERVICE_PACKAGE_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_PACKAGE_DATA insert = new DB_SERVICE_PACKAGE_DATA();
                insert.ID_SERVICE_PACKAGE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE_DATA"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.MIN = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["MIN"]);
                insert.MAX = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["MAX"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveServicePackageData(DB_SERVICE_PACKAGE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_PACKAGE_DATA", ToBeSaved.ID_SERVICE_PACKAGE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServicePackageData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,new DB.InParameter("@MIN", ToBeSaved.MIN)
														,new DB.InParameter("@MAX", ToBeSaved.MAX)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_PACKAGE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteServicePackageData(DB_SERVICE_PACKAGE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServicePackageData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_PACKAGE_DATA", toBeDeleted.ID_SERVICE_PACKAGE_DATA)			
		}
            );
        }

        #endregion
        #region Table SERVICE_REFERENCE_TYPE

        public DB_SERVICE_REFERENCE_TYPE[] GetServiceReferenceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetServiceReferenceType",
                new DB.AnalyzeDataSet(GetServiceReferenceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_REFERENCE_TYPE[] GetServiceReferenceType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_REFERENCE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetServiceReferenceType",
                new DB.AnalyzeDataSet(GetServiceReferenceType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_REFERENCE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceReferenceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_REFERENCE_TYPE[] list = new DB_SERVICE_REFERENCE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_REFERENCE_TYPE insert = new DB_SERVICE_REFERENCE_TYPE();
                insert.ID_SERVICE_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_REFERENCE_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveServiceReferenceType(DB_SERVICE_REFERENCE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_REFERENCE_TYPE", ToBeSaved.ID_SERVICE_REFERENCE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceReferenceType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_REFERENCE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteServiceReferenceType(DB_SERVICE_REFERENCE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceReferenceType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_REFERENCE_TYPE", toBeDeleted.ID_SERVICE_REFERENCE_TYPE)			
		}
            );
        }

        #endregion
        #region Table SERVICE_SUMMARY

        public DB_SERVICE_SUMMARY[] GetServiceSummary(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_SUMMARY[])DB.ExecuteProcedure(
                "imrse_GetServiceSummary",
                new DB.AnalyzeDataSet(GetServiceSummary),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_SUMMARY[] GetServiceSummary(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_SUMMARY[])DB.ExecuteProcedure(
                "imrse_GetServiceSummary",
                new DB.AnalyzeDataSet(GetServiceSummary),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_LIST", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceSummary(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_SUMMARY[] list = new DB_SERVICE_SUMMARY[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_SUMMARY insert = new DB_SERVICE_SUMMARY();
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.AMOUNT = GetValue<int>(QueryResult.Tables[0].Rows[i]["AMOUNT"]);
                insert.COST = GetValue<Double>(QueryResult.Tables[0].Rows[i]["COST"]);
                list[i] = insert;
            }
            return list;
        }
        public int SaveServiceSummary(DB_SERVICE_SUMMARY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceSummary",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@AMOUNT", ToBeSaved.AMOUNT)
														,new DB.InParameter("@COST", ToBeSaved.COST)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_LIST = (int)InsertId.Value;

            return (int)InsertId.Value;
        }



        public void DeleteServiceSummary(DB_SERVICE_SUMMARY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceSummary",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_LIST", toBeDeleted.ID_SERVICE_LIST)			
		}
            );
        }



        #endregion
        #region Table SERVICE_SUMMARY_DATA

        public DB_SERVICE_SUMMARY_DATA[] GetServiceSummaryData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_SUMMARY_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceSummaryData",
                new DB.AnalyzeDataSet(GetServiceSummaryData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_SUMMARY_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SERVICE_SUMMARY_DATA[] GetServiceSummaryData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SERVICE_SUMMARY_DATA[])DB.ExecuteProcedure(
                "imrse_GetServiceSummaryData",
                new DB.AnalyzeDataSet(GetServiceSummaryData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SERVICE_SUMMARY_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetServiceSummaryData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SERVICE_SUMMARY_DATA[] list = new DB_SERVICE_SUMMARY_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SERVICE_SUMMARY_DATA insert = new DB_SERVICE_SUMMARY_DATA();
                insert.ID_SERVICE_SUMMARY_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_SUMMARY_DATA"]);
                insert.ID_SERVICE_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_LIST"]);
                insert.ID_SERVICE_PACKAGE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SERVICE_PACKAGE"]);
                insert.COST = GetValue<Double>(QueryResult.Tables[0].Rows[i]["COST"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }


        public long SaveServiceSummaryData(DB_SERVICE_SUMMARY_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SERVICE_SUMMARY_DATA", ToBeSaved.ID_SERVICE_SUMMARY_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveServiceSummaryData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SERVICE_LIST", ToBeSaved.ID_SERVICE_LIST)
														,new DB.InParameter("@ID_SERVICE_PACKAGE", ToBeSaved.ID_SERVICE_PACKAGE)
														,new DB.InParameter("@COST", ToBeSaved.COST)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SERVICE_SUMMARY_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteServiceSummaryData(DB_SERVICE_SUMMARY_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelServiceSummaryData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SERVICE_SUMMARY_DATA", toBeDeleted.ID_SERVICE_SUMMARY_DATA)			
		}
            );
        }

        #endregion

        #region Table SESSION

        public DB_SESSION[] GetSession()
        {
            return (DB_SESSION[])DB.ExecuteProcedure(
                "imrse_GetSession",
                new DB.AnalyzeDataSet(GetSession),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION", new long[] {})
					}
            );
        }

        public DB_SESSION[] GetSession(long[] Ids)
        {
            return (DB_SESSION[])DB.ExecuteProcedure(
                "imrse_GetSession",
                new DB.AnalyzeDataSet(GetSession),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION", Ids)
					}
            );
        }

        private object GetSession(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SESSION[] list = new DB_SESSION[count];
            for (int i = 0; i < count; i++)
            {
                DB_SESSION insert = new DB_SESSION();
                insert.ID_SESSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION"]);
                insert.SESSION_GUID = ConvertToGuid(QueryResult.Tables[0].Rows[i]["SESSION_GUID"]);
                insert.ID_MODULE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_MODULE"]);
                insert.ID_OPERATOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.ID_SESSION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SESSION_STATUS"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSession(DB_SESSION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SESSION", ToBeSaved.ID_SESSION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSession",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@SESSION_GUID", ToBeSaved.SESSION_GUID)
														,new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@ID_SESSION_STATUS", ToBeSaved.ID_SESSION_STATUS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SESSION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSession(DB_SESSION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSession",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SESSION", toBeDeleted.ID_SESSION)			
		}
            );
        }

        #endregion
        #region Table SESSION_DATA

        public DB_SESSION_DATA[] GetSessionData()
        {
            return (DB_SESSION_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionData",
                new DB.AnalyzeDataSet(GetSessionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_DATA", new long[] {})
					}
            );
        }

        public DB_SESSION_DATA[] GetSessionData(long[] Ids)
        {
            return (DB_SESSION_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionData",
                new DB.AnalyzeDataSet(GetSessionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_DATA", Ids)
					}
            );
        }

        private object GetSessionData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SESSION_DATA[] list = new DB_SESSION_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SESSION_DATA insert = new DB_SESSION_DATA();
                insert.ID_SESSION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION_DATA"]);
                insert.ID_SESSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSessionData(DB_SESSION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SESSION_DATA", ToBeSaved.ID_SESSION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSessionData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SESSION", ToBeSaved.ID_SESSION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SESSION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSessionData(DB_SESSION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSessionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SESSION_DATA", toBeDeleted.ID_SESSION_DATA)			
		}
            );
        }

        #endregion
        #region Table SESSION_EVENT

        public DB_SESSION_EVENT[] GetSessionEvent()
        {
            return (DB_SESSION_EVENT[])DB.ExecuteProcedure(
                "imrse_GetSessionEvent",
                new DB.AnalyzeDataSet(GetSessionEvent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_EVENT", new long[] {})
					}
            );
        }

        public DB_SESSION_EVENT[] GetSessionEvent(long[] Ids)
        {
            return (DB_SESSION_EVENT[])DB.ExecuteProcedure(
                "imrse_GetSessionEvent",
                new DB.AnalyzeDataSet(GetSessionEvent),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_EVENT", Ids)
					}
            );
        }

        private object GetSessionEvent(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SESSION_EVENT[] list = new DB_SESSION_EVENT[count];
            for (int i = 0; i < count; i++)
            {
                DB_SESSION_EVENT insert = new DB_SESSION_EVENT();
                insert.ID_SESSION_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION_EVENT"]);
                insert.ID_SESSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION"]);
                insert.ID_EVENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_EVENT"]);
                insert.TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME"]));
                list[i] = insert;
            }
            return list;
        }

        public long SaveSessionEvent(DB_SESSION_EVENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SESSION_EVENT", ToBeSaved.ID_SESSION_EVENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSessionEvent",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SESSION", ToBeSaved.ID_SESSION)
														,new DB.InParameter("@ID_EVENT", ToBeSaved.ID_EVENT)
														,new DB.InParameter("@TIME", ConvertTimeZoneToUtcTime(ToBeSaved.TIME))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SESSION_EVENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSessionEvent(DB_SESSION_EVENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSessionEvent",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SESSION_EVENT", toBeDeleted.ID_SESSION_EVENT)			
		}
            );
        }

        #endregion
        #region Table SESSION_EVENT_DATA

        public DB_SESSION_EVENT_DATA[] GetSessionEventData()
        {
            return (DB_SESSION_EVENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionEventData",
                new DB.AnalyzeDataSet(GetSessionEventData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_EVENT_DATA", new long[] {})
					}
            );
        }

        public DB_SESSION_EVENT_DATA[] GetSessionEventData(long[] Ids)
        {
            return (DB_SESSION_EVENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetSessionEventData",
                new DB.AnalyzeDataSet(GetSessionEventData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_EVENT_DATA", Ids)
					}
            );
        }

        private object GetSessionEventData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SESSION_EVENT_DATA[] list = new DB_SESSION_EVENT_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SESSION_EVENT_DATA insert = new DB_SESSION_EVENT_DATA();
                insert.ID_SESSION_EVENT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION_EVENT_DATA"]);
                insert.ID_SESSION_EVENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SESSION_EVENT"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSessionEventData(DB_SESSION_EVENT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SESSION_EVENT_DATA", ToBeSaved.ID_SESSION_EVENT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSessionEventData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SESSION_EVENT", ToBeSaved.ID_SESSION_EVENT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SESSION_EVENT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSessionEventData(DB_SESSION_EVENT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSessionEventData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SESSION_EVENT_DATA", toBeDeleted.ID_SESSION_EVENT_DATA)			
		}
            );
        }

        #endregion
        #region Table SESSION_STATUS

        public DB_SESSION_STATUS[] GetSessionStatus()
        {
            return (DB_SESSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetSessionStatus",
                new DB.AnalyzeDataSet(GetSessionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_STATUS", new int[] {})
					}
            );
        }

        public DB_SESSION_STATUS[] GetSessionStatus(int[] Ids)
        {
            return (DB_SESSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetSessionStatus",
                new DB.AnalyzeDataSet(GetSessionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SESSION_STATUS", Ids)
					}
            );
        }

        private object GetSessionStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SESSION_STATUS[] list = new DB_SESSION_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_SESSION_STATUS insert = new DB_SESSION_STATUS();
                insert.ID_SESSION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SESSION_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveSessionStatus(DB_SESSION_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SESSION_STATUS", ToBeSaved.ID_SESSION_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSessionStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SESSION_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSessionStatus(DB_SESSION_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSessionStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SESSION_STATUS", toBeDeleted.ID_SESSION_STATUS)			
		}
            );
        }

        #endregion

        #region Table SHIPPING_LIST

        public DB_SHIPPING_LIST[] GetShippingList(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SHIPPING_LIST[])DB.ExecuteProcedure(
                "imrse_GetShippingList",
                new DB.AnalyzeDataSet(GetShippingList),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SHIPPING_LIST[] GetShippingList(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SHIPPING_LIST[])DB.ExecuteProcedure(
                "imrse_GetShippingList",
                new DB.AnalyzeDataSet(GetShippingList),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetShippingList(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SHIPPING_LIST[] list = new DB_SHIPPING_LIST[count];
            for (int i = 0; i < count; i++)
            {
                DB_SHIPPING_LIST insert = new DB_SHIPPING_LIST();
                insert.ID_SHIPPING_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                insert.CONTRACT_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONTRACT_NBR"]);
                insert.SHIPPING_LIST_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHIPPING_LIST_NBR"]);
                insert.ID_OPERATOR_CREATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATOR"]);
                insert.ID_OPERATOR_CONTACT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CONTACT"]);
                insert.ID_OPERATOR_QUALITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_QUALITY"]);
                insert.ID_OPERATOR_PROTOCOL = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_PROTOCOL"]);
                insert.SHIPPING_LETTER_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SHIPPING_LETTER_NBR"]);
                insert.ID_LOCATION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_SHIPPING_LIST_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST_TYPE"]);
                insert.CREATION_DATE = GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]);
                insert.FINISH_DATE = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["FINISH_DATE"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveShippingList(DB_SHIPPING_LIST ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SHIPPING_LIST", ToBeSaved.ID_SHIPPING_LIST);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveShippingList",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@CONTRACT_NBR", ToBeSaved.CONTRACT_NBR)
														,new DB.InParameter("@SHIPPING_LIST_NBR", ToBeSaved.SHIPPING_LIST_NBR)
														,new DB.InParameter("@ID_OPERATOR_CREATOR", ToBeSaved.ID_OPERATOR_CREATOR)
														,new DB.InParameter("@ID_OPERATOR_CONTACT", ToBeSaved.ID_OPERATOR_CONTACT)
														,new DB.InParameter("@ID_OPERATOR_QUALITY", ToBeSaved.ID_OPERATOR_QUALITY)
														,new DB.InParameter("@ID_OPERATOR_PROTOCOL", ToBeSaved.ID_OPERATOR_PROTOCOL)
														,new DB.InParameter("@SHIPPING_LETTER_NBR", ToBeSaved.SHIPPING_LETTER_NBR)
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_SHIPPING_LIST_TYPE", ToBeSaved.ID_SHIPPING_LIST_TYPE)
														,new DB.InParameter("@CREATION_DATE", ToBeSaved.CREATION_DATE)
														,new DB.InParameter("@FINISH_DATE", ToBeSaved.FINISH_DATE)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SHIPPING_LIST = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteShippingList(DB_SHIPPING_LIST toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelShippingList",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SHIPPING_LIST", toBeDeleted.ID_SHIPPING_LIST)			
		}
            );
        }

        #endregion
        #region Table SHIPPING_LIST_DATA

        public DB_SHIPPING_LIST_DATA[] GetShippingListData()
        {
            return (DB_SHIPPING_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetShippingListData",
                new DB.AnalyzeDataSet(GetShippingListData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_DATA", new long[] {})
					}
            );
        }

        public DB_SHIPPING_LIST_DATA[] GetShippingListData(long[] Ids)
        {
            return (DB_SHIPPING_LIST_DATA[])DB.ExecuteProcedure(
                "imrse_GetShippingListData",
                new DB.AnalyzeDataSet(GetShippingListData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_DATA", Ids)
					}
            );
        }



        private object GetShippingListData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SHIPPING_LIST_DATA[] list = new DB_SHIPPING_LIST_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SHIPPING_LIST_DATA insert = new DB_SHIPPING_LIST_DATA();
                insert.ID_SHIPPING_LIST_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST_DATA"]);
                insert.ID_SHIPPING_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveShippingListData(DB_SHIPPING_LIST_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SHIPPING_LIST_DATA", ToBeSaved.ID_SHIPPING_LIST_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveShippingListData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SHIPPING_LIST", ToBeSaved.ID_SHIPPING_LIST)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SHIPPING_LIST_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteShippingListData(DB_SHIPPING_LIST_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelShippingListData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SHIPPING_LIST_DATA", toBeDeleted.ID_SHIPPING_LIST_DATA)			
		}
            );
        }

        #endregion
        #region Table SHIPPING_LIST_DEVICE

        public DB_SHIPPING_LIST_DEVICE[] GetShippingListDevice()
        {
            return (DB_SHIPPING_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetShippingListDevice",
                new DB.AnalyzeDataSet(GetShippingListDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_DEVICE", new int[] {})
					}
            );
        }

        public DB_SHIPPING_LIST_DEVICE[] GetShippingListDevice(int[] Ids)
        {
            return (DB_SHIPPING_LIST_DEVICE[])DB.ExecuteProcedure(
                "imrse_GetShippingListDevice",
                new DB.AnalyzeDataSet(GetShippingListDevice),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_DEVICE", Ids)
					}
            );
        }

        private object GetShippingListDevice(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SHIPPING_LIST_DEVICE[] list = new DB_SHIPPING_LIST_DEVICE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SHIPPING_LIST_DEVICE insert = new DB_SHIPPING_LIST_DEVICE();
                insert.ID_SHIPPING_LIST_DEVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST_DEVICE"]);
                insert.ID_SHIPPING_LIST = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST"]);
                insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.ID_METER = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_METER"]);
                insert.ID_ARTICLE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_ARTICLE"]);
                insert.INSERT_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["INSERT_DATE"]));
                list[i] = insert;
            }
            return list;
        }


        public int SaveShippingListDevice(DB_SHIPPING_LIST_DEVICE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SHIPPING_LIST_DEVICE", ToBeSaved.ID_SHIPPING_LIST_DEVICE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveShippingListDevice",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SHIPPING_LIST", ToBeSaved.ID_SHIPPING_LIST)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@ID_METER", ToBeSaved.ID_METER)
														,new DB.InParameter("@ID_ARTICLE", ToBeSaved.ID_ARTICLE)
														,new DB.InParameter("@INSERT_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.INSERT_DATE))
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SHIPPING_LIST_DEVICE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteShippingListDevice(DB_SHIPPING_LIST_DEVICE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelShippingListDevice",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SHIPPING_LIST_DEVICE", toBeDeleted.ID_SHIPPING_LIST_DEVICE)			
		}
            );
        }

        #endregion
        #region Table SHIPPING_LIST_TYPE

        public DB_SHIPPING_LIST_TYPE[] GetShippingListType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SHIPPING_LIST_TYPE[])DB.ExecuteProcedure(
                "imrse_GetShippingListType",
                new DB.AnalyzeDataSet(GetShippingListType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SHIPPING_LIST_TYPE[] GetShippingListType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SHIPPING_LIST_TYPE[])DB.ExecuteProcedure(
                "imrse_GetShippingListType",
                new DB.AnalyzeDataSet(GetShippingListType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SHIPPING_LIST_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetShippingListType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SHIPPING_LIST_TYPE[] list = new DB_SHIPPING_LIST_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SHIPPING_LIST_TYPE insert = new DB_SHIPPING_LIST_TYPE();
                insert.ID_SHIPPING_LIST_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SHIPPING_LIST_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveShippingListType(DB_SHIPPING_LIST_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SHIPPING_LIST_TYPE", ToBeSaved.ID_SHIPPING_LIST_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveShippingListType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SHIPPING_LIST_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteShippingListType(DB_SHIPPING_LIST_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelShippingListType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SHIPPING_LIST_TYPE", toBeDeleted.ID_SHIPPING_LIST_TYPE)			
		}
            );
        }

        #endregion

        #region Table SIM_CARD
        public DB_SIM_CARD[] GetSimCard()
        {
            return (DB_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_GetSimCard",
                new DB.AnalyzeDataSet(GetSimCard),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SIM_CARD", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_SIM_CARD[] GetSimCard(int[] Ids)
        {
            return (DB_SIM_CARD[])DB.ExecuteProcedure(
                "imrse_GetSimCard",
                new DB.AnalyzeDataSet(GetSimCard),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SIM_CARD", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }

        private object GetSimCard(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SIM_CARD[] list = new DB_SIM_CARD[count];
            for (int i = 0; i < count; i++)
            {
                DB_SIM_CARD insert = new DB_SIM_CARD();
                insert.ID_SIM_CARD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.PIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["PIN"]);
                insert.PUK = GetValue<string>(QueryResult.Tables[0].Rows[i]["PUK"]);
                insert.MOBILE_NETWORK_CODE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["MOBILE_NETWORK_CODE"]);
                insert.IP = GetValue<string>(QueryResult.Tables[0].Rows[i]["IP"]);
                insert.APN_LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_LOGIN"]);
                insert.APN_PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_PASSWORD"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveSimCard(DB_SIM_CARD ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SIM_CARD", ToBeSaved.ID_SIM_CARD);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSimCard",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@SERIAL_NBR", ToBeSaved.SERIAL_NBR)
														,new DB.InParameter("@PHONE", ToBeSaved.PHONE)
														,new DB.InParameter("@PIN", ToBeSaved.PIN)
														,new DB.InParameter("@PUK", ToBeSaved.PUK)
														,new DB.InParameter("@MOBILE_NETWORK_CODE", ToBeSaved.MOBILE_NETWORK_CODE)
														,new DB.InParameter("@IP", ToBeSaved.IP)
														,new DB.InParameter("@APN_LOGIN", ToBeSaved.APN_LOGIN)
														,new DB.InParameter("@APN_PASSWORD", ToBeSaved.APN_PASSWORD)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SIM_CARD = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSimCard(DB_SIM_CARD toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSimCard",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SIM_CARD", toBeDeleted.ID_SIM_CARD)			
		}
            );
        }

        #endregion
        #region Table SIM_CARD_DATA

        public DB_SIM_CARD_DATA[] GetSimCardData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SIM_CARD_DATA[])DB.ExecuteProcedure(
                "imrse_GetSimCardData",
                new DB.AnalyzeDataSet(GetSimCardData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SIM_CARD_DATA", new long[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_SIM_CARD_DATA[] GetSimCardData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_SIM_CARD_DATA[])DB.ExecuteProcedure(
                "imrse_GetSimCardData",
                new DB.AnalyzeDataSet(GetSimCardData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SIM_CARD_DATA", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetSimCardData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SIM_CARD_DATA[] list = new DB_SIM_CARD_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SIM_CARD_DATA insert = new DB_SIM_CARD_DATA();
                insert.ID_SIM_CARD_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD_DATA"]);
                insert.ID_SIM_CARD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSimCardData(DB_SIM_CARD_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SIM_CARD_DATA", ToBeSaved.ID_SIM_CARD_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSimCardData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_SIM_CARD", ToBeSaved.ID_SIM_CARD)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SIM_CARD_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSimCardData(DB_SIM_CARD_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSimCardData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SIM_CARD_DATA", toBeDeleted.ID_SIM_CARD_DATA)			
		}
            );
        }

        #endregion

        #region Table SLA

        public DB_SLA[] GetSla()
        {
            return (DB_SLA[])DB.ExecuteProcedure(
                "imrse_GetSla",
                new DB.AnalyzeDataSet(GetSla),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SLA", new int[] {})
					}
            );
        }

        public DB_SLA[] GetSla(int[] Ids)
        {
            return (DB_SLA[])DB.ExecuteProcedure(
                "imrse_GetSla",
                new DB.AnalyzeDataSet(GetSla),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SLA", Ids)
					}
            );
        }

        private object GetSla(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SLA[] list = new DB_SLA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SLA insert = new DB_SLA();
                insert.ID_SLA = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SLA"]);
                insert.ID_DESCR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.ISSUE_RECEIVE_WD = GetValue<string>(QueryResult.Tables[0].Rows[i]["ISSUE_RECEIVE_WD"]);
                insert.ISSUE_RECEIVE_FD = GetValue<string>(QueryResult.Tables[0].Rows[i]["ISSUE_RECEIVE_FD"]);
                insert.TECHNICAL_SUPPORT_WD = GetValue<string>(QueryResult.Tables[0].Rows[i]["TECHNICAL_SUPPORT_WD"]);
                insert.TECHNICAL_SUPPORT_FD = GetValue<string>(QueryResult.Tables[0].Rows[i]["TECHNICAL_SUPPORT_FD"]);
                insert.ISSUE_RESPONSE_WD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ISSUE_RESPONSE_WD"]);
                insert.ISSUE_RESPONSE_FD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ISSUE_RESPONSE_FD"]);
                insert.SYSTEM_FAILURE_REMOVE_WD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SYSTEM_FAILURE_REMOVE_WD"]);
                insert.SYSTEM_FAILURE_REMOVE_FD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["SYSTEM_FAILURE_REMOVE_FD"]);
                insert.OBJECT_FAILURE_REMOVE_WD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["OBJECT_FAILURE_REMOVE_WD"]);
                insert.OBJECT_FAILURE_REMOVE_FD = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["OBJECT_FAILURE_REMOVE_FD"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveSla(DB_SLA ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SLA", ToBeSaved.ID_SLA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSla",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@ISSUE_RECEIVE_WD", ToBeSaved.ISSUE_RECEIVE_WD)
														,new DB.InParameter("@ISSUE_RECEIVE_FD", ToBeSaved.ISSUE_RECEIVE_FD)
														,new DB.InParameter("@TECHNICAL_SUPPORT_WD", ToBeSaved.TECHNICAL_SUPPORT_WD)
														,new DB.InParameter("@TECHNICAL_SUPPORT_FD", ToBeSaved.TECHNICAL_SUPPORT_FD)
														,new DB.InParameter("@ISSUE_RESPONSE_WD", ToBeSaved.ISSUE_RESPONSE_WD)
														,new DB.InParameter("@ISSUE_RESPONSE_FD", ToBeSaved.ISSUE_RESPONSE_FD)
														,new DB.InParameter("@SYSTEM_FAILURE_REMOVE_WD", ToBeSaved.SYSTEM_FAILURE_REMOVE_WD)
														,new DB.InParameter("@SYSTEM_FAILURE_REMOVE_FD", ToBeSaved.SYSTEM_FAILURE_REMOVE_FD)
														,new DB.InParameter("@OBJECT_FAILURE_REMOVE_WD", ToBeSaved.OBJECT_FAILURE_REMOVE_WD)
														,new DB.InParameter("@OBJECT_FAILURE_REMOVE_FD", ToBeSaved.OBJECT_FAILURE_REMOVE_FD)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SLA = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSla(DB_SLA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSla",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SLA", toBeDeleted.ID_SLA)			
		}
            );
        }

        #endregion

        #region Table SLOT_TYPE

        public DB_SLOT_TYPE[] GetSlotType()
        {
            return (DB_SLOT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetSlotType",
                new DB.AnalyzeDataSet(GetSlotType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SLOT_TYPE", new int[] {})
					}
            );
        }

        public DB_SLOT_TYPE[] GetSlotType(int[] Ids)
        {
            return (DB_SLOT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetSlotType",
                new DB.AnalyzeDataSet(GetSlotType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SLOT_TYPE", Ids)
					}
            );
        }

        private object GetSlotType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SLOT_TYPE[] list = new DB_SLOT_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_SLOT_TYPE insert = new DB_SLOT_TYPE();
                insert.ID_SLOT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SLOT_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveSlotType(DB_SLOT_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SLOT_TYPE", ToBeSaved.ID_SLOT_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSlotType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SLOT_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSlotType(DB_SLOT_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSlotType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SLOT_TYPE", toBeDeleted.ID_SLOT_TYPE)			
		}
            );
        }

        #endregion

        #region Table SUPPLIER

        public DB_SUPPLIER[] GetSupplier()
        {
            return (DB_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetSupplier",
                new DB.AnalyzeDataSet(GetSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SUPPLIER", new int[] {})
					}
            );
        }

        public DB_SUPPLIER[] GetSupplier(int[] Ids)
        {
            return (DB_SUPPLIER[])DB.ExecuteProcedure(
                "imrse_GetSupplier",
                new DB.AnalyzeDataSet(GetSupplier),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SUPPLIER", Ids)
					}
            );
        }

        private object GetSupplier(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SUPPLIER[] list = new DB_SUPPLIER[count];
            for (int i = 0; i < count; i++)
            {
                DB_SUPPLIER insert = new DB_SUPPLIER();
                insert.ID_SUPPLIER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SUPPLIER"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveSupplier(DB_SUPPLIER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SUPPLIER", ToBeSaved.ID_SUPPLIER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSupplier",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SUPPLIER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSupplier(DB_SUPPLIER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSupplier",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SUPPLIER", toBeDeleted.ID_SUPPLIER)			
		}
            );
        }

        #endregion

        #region Table SYSTEM_DATA

        public DB_SYSTEM_DATA[] GetSystemData()
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SYSTEM_DATA", new long[] {})
					}
            );
        }

        public DB_SYSTEM_DATA[] GetSystemData(long[] Ids)
        {
            return (DB_SYSTEM_DATA[])DB.ExecuteProcedure(
                "imrse_GetSystemData",
                new DB.AnalyzeDataSet(GetSystemData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SYSTEM_DATA", Ids)
					}
            );
        }



        private object GetSystemData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_SYSTEM_DATA[] list = new DB_SYSTEM_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_SYSTEM_DATA insert = new DB_SYSTEM_DATA();
                insert.ID_SYSTEM_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_SYSTEM_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveSystemData(DB_SYSTEM_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SYSTEM_DATA", ToBeSaved.ID_SYSTEM_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSystemData",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                    ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
					,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SYSTEM_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteSystemData(DB_SYSTEM_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSystemData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SYSTEM_DATA", toBeDeleted.ID_SYSTEM_DATA)			
		}
            );
        }

        #endregion

        #region Table TARIFF_DATA

        public DB_TARIFF_DATA[] GetTariffData()
        {
            return (DB_TARIFF_DATA[])DB.ExecuteProcedure(
                "imrse_GetTariffData",
                new DB.AnalyzeDataSet(GetTariffData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF_DATA", new long[] {})
					}
            );
        }

        public DB_TARIFF_DATA[] GetTariffData(long[] Ids)
        {
            return (DB_TARIFF_DATA[])DB.ExecuteProcedure(
                "imrse_GetTariffData",
                new DB.AnalyzeDataSet(GetTariffData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF_DATA", Ids)
					}
            );
        }

        private object GetTariffData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TARIFF_DATA[] list = new DB_TARIFF_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_TARIFF_DATA insert = new DB_TARIFF_DATA();
                insert.ID_TARIFF_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TARIFF_DATA"]);
                insert.ID_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTariffData(DB_TARIFF_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TARIFF_DATA", ToBeSaved.ID_TARIFF_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTariffData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TARIFF_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTariffData(DB_TARIFF_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTariffData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TARIFF_DATA", toBeDeleted.ID_TARIFF_DATA)			
		}
            );
        }

        #endregion
        #region Table TARIFF_SETTLEMENT_PERIOD
        public DB_TARIFF_SETTLEMENT_PERIOD[] GetTariffSettlementPeriod(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TARIFF_SETTLEMENT_PERIOD[])DB.ExecuteProcedure(
                "imrse_GetTariffSettlementPeriod",
                new DB.AnalyzeDataSet(GetTariffSettlementPeriod),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF_SETTLEMENT_PERIOD", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TARIFF_SETTLEMENT_PERIOD[] GetTariffSettlementPeriod(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TARIFF_SETTLEMENT_PERIOD[])DB.ExecuteProcedure(
                "imrse_GetTariffSettlementPeriod",
                new DB.AnalyzeDataSet(GetTariffSettlementPeriod),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TARIFF_SETTLEMENT_PERIOD", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTariffSettlementPeriod(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_TARIFF_SETTLEMENT_PERIOD()
            {
                ID_TARIFF_SETTLEMENT_PERIOD = GetValue<int>(row["ID_TARIFF_SETTLEMENT_PERIOD"]),
                ID_TARIFF = GetValue<int>(row["ID_TARIFF"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_TARIFF_SETTLEMENT_PERIOD[] list = new DB_TARIFF_SETTLEMENT_PERIOD[count];
    for (int i = 0; i < count; i++)
    {
        DB_TARIFF_SETTLEMENT_PERIOD insert = new DB_TARIFF_SETTLEMENT_PERIOD();
									insert.ID_TARIFF_SETTLEMENT_PERIOD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF_SETTLEMENT_PERIOD"]);
												insert.ID_TARIFF = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TARIFF"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveTariffSettlementPeriod(DB_TARIFF_SETTLEMENT_PERIOD ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TARIFF_SETTLEMENT_PERIOD", ToBeSaved.ID_TARIFF_SETTLEMENT_PERIOD);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTariffSettlementPeriod",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TARIFF", ToBeSaved.ID_TARIFF)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TARIFF_SETTLEMENT_PERIOD = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTariffSettlementPeriod(DB_TARIFF_SETTLEMENT_PERIOD toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTariffSettlementPeriod",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TARIFF_SETTLEMENT_PERIOD", toBeDeleted.ID_TARIFF_SETTLEMENT_PERIOD)			
		}
            );
        }

        #endregion

        #region Table SETTLEMENT_DOCUMENT_TYPE

        public DB_SETTLEMENT_DOCUMENT_TYPE[] GetSettlementDocumentType()
        {
            return (DB_SETTLEMENT_DOCUMENT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetSettlementDocumentType",
                new DB.AnalyzeDataSet(GetSettlementDocumentType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SETTLEMENT_DOCUMENT_TYPE", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}
            );
        }

        public DB_SETTLEMENT_DOCUMENT_TYPE[] GetSettlementDocumentType(int[] Ids)
        {
            return (DB_SETTLEMENT_DOCUMENT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetSettlementDocumentType",
                new DB.AnalyzeDataSet(GetSettlementDocumentType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_SETTLEMENT_DOCUMENT_TYPE", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", new int[] {})
					}
            );
        }



        private object GetSettlementDocumentType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_SETTLEMENT_DOCUMENT_TYPE()
            {
                ID_SETTLEMENT_DOCUMENT_TYPE = GetValue<int>(row["ID_SETTLEMENT_DOCUMENT_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                DESCRIPTION = GetValue<string>(row["DESCRIPTION"]),
                ACTIVE = GetValue<bool>(row["ACTIVE"]),
                ID_DISTRIBUTOR = GetNullableValue<int>(row["ID_DISTRIBUTOR"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pêtl¹
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_SETTLEMENT_DOCUMENT_TYPE[] list = new DB_SETTLEMENT_DOCUMENT_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_SETTLEMENT_DOCUMENT_TYPE insert = new DB_SETTLEMENT_DOCUMENT_TYPE();
									insert.ID_SETTLEMENT_DOCUMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SETTLEMENT_DOCUMENT_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
												insert.ACTIVE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["ACTIVE"]);
												insert.ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public int SaveSettlementDocumentType(DB_SETTLEMENT_DOCUMENT_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_SETTLEMENT_DOCUMENT_TYPE", ToBeSaved.ID_SETTLEMENT_DOCUMENT_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveSettlementDocumentType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@ACTIVE", ToBeSaved.ACTIVE)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_SETTLEMENT_DOCUMENT_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteSettlementDocumentType(DB_SETTLEMENT_DOCUMENT_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelSettlementDocumentType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_SETTLEMENT_DOCUMENT_TYPE", toBeDeleted.ID_SETTLEMENT_DOCUMENT_TYPE)			
		}
            );
        }

        #endregion

        #region Table TASK

        public DB_TASK[] GetTask(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK[])DB.ExecuteProcedure(
                "imrse_GetTask",
                new DB.AnalyzeDataSet(GetTask),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK", new int[] {})
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_TASK[] GetTask(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK[])DB.ExecuteProcedure(
                "imrse_GetTask",
                new DB.AnalyzeDataSet(GetTask),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK", Ids)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }



        private object GetTask(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK[] list = new DB_TASK[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK insert = new DB_TASK();
                insert.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insert.ID_TASK_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_TYPE"]);
                insert.ID_ISSUE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_ISSUE"]);
                insert.ID_TASK_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_GROUP"]);
                insert.ID_PLANNED_ROUTE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_PLANNED_ROUTE"]);
                insert.ID_OPERATOR_REGISTERING = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_REGISTERING"]);
                insert.ID_OPERATOR_PERFORMER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_PERFORMER"]);
                insert.ID_TASK_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_STATUS"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.ACCEPTED = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["ACCEPTED"]);
                insert.ID_OPERATOR_ACCEPTED = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_ACCEPTED"]);
                insert.ACCEPTANCE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["ACCEPTANCE_DATE"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                insert.DEADLINE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DEADLINE"]));
                insert.ID_LOCATION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_LOCATION"]);
                insert.ID_ACTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_ACTOR"]);
                insert.TOPIC_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["TOPIC_NUMBER"]);
                insert.PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["PRIORITY"]);
                insert.OPERATION_CODE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["OPERATION_CODE"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTask(DB_TASK ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK", ToBeSaved.ID_TASK);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTask",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TASK_TYPE", ToBeSaved.ID_TASK_TYPE)
														,new DB.InParameter("@ID_ISSUE", ToBeSaved.ID_ISSUE)
														,new DB.InParameter("@ID_TASK_GROUP", ToBeSaved.ID_TASK_GROUP)
														,new DB.InParameter("@ID_PLANNED_ROUTE", ToBeSaved.ID_PLANNED_ROUTE)
														,new DB.InParameter("@ID_OPERATOR_REGISTERING", ToBeSaved.ID_OPERATOR_REGISTERING)
														,new DB.InParameter("@ID_OPERATOR_PERFORMER", ToBeSaved.ID_OPERATOR_PERFORMER)
														,new DB.InParameter("@ID_TASK_STATUS", ToBeSaved.ID_TASK_STATUS)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@ACCEPTED", ToBeSaved.ACCEPTED)
														,new DB.InParameter("@ID_OPERATOR_ACCEPTED", ToBeSaved.ID_OPERATOR_ACCEPTED)
														,new DB.InParameter("@ACCEPTANCE_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.ACCEPTANCE_DATE))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
														,new DB.InParameter("@DEADLINE", ConvertTimeZoneToUtcTime(ToBeSaved.DEADLINE))
														,new DB.InParameter("@ID_LOCATION", ToBeSaved.ID_LOCATION)
														,new DB.InParameter("@ID_ACTOR", ToBeSaved.ID_ACTOR)
														,new DB.InParameter("@TOPIC_NUMBER", ToBeSaved.TOPIC_NUMBER)
														,new DB.InParameter("@PRIORITY", ToBeSaved.PRIORITY)
														,new DB.InParameter("@OPERATION_CODE", ToBeSaved.OPERATION_CODE)
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTask(DB_TASK toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTask",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK", toBeDeleted.ID_TASK)			
		}
            );
        }

        #endregion
        #region Table TASK_DATA

        public DB_TASK_DATA[] GetTaskData()
        {
            return (DB_TASK_DATA[])DB.ExecuteProcedure(
                "imrse_GetTaskData",
                new DB.AnalyzeDataSet(GetTaskData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_DATA", new long[] {})
					}
            );
        }

        public DB_TASK_DATA[] GetTaskData(long[] Ids)
        {
            return (DB_TASK_DATA[])DB.ExecuteProcedure(
                "imrse_GetTaskData",
                new DB.AnalyzeDataSet(GetTaskData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_DATA", Ids)
					}
            );
        }

        private object GetTaskData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_DATA[] list = new DB_TASK_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_DATA insert = new DB_TASK_DATA();
                insert.ID_TASK_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TASK_DATA"]);
                insert.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTaskData(DB_TASK_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_DATA", ToBeSaved.ID_TASK_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TASK", ToBeSaved.ID_TASK)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTaskData(DB_TASK_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_DATA", toBeDeleted.ID_TASK_DATA)			
		}
            );
        }

        #endregion
        #region Table TASK_GROUP

        public DB_TASK_GROUP[] GetTaskGroup(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskGroup",
                new DB.AnalyzeDataSet(GetTaskGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_GROUP", TaskGroupFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TASK_GROUP[] GetTaskGroup(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskGroup",
                new DB.AnalyzeDataSet(GetTaskGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_GROUP", Ids != null ? Ids : TaskGroupFilter)
							,CreateTableParam("@ID_DISTRIBUTOR", this.DistributorFilter)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetTaskGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_GROUP[] list = new DB_TASK_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_GROUP insert = new DB_TASK_GROUP();
                insert.ID_TASK_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_GROUP"]);
                insert.ID_PARRENT_GROUP = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_PARRENT_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DATE_CREATED = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["DATE_CREATED"]));
                insert.ID_DISTRIBUTOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTaskGroup(DB_TASK_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_GROUP", ToBeSaved.ID_TASK_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_PARRENT_GROUP", ToBeSaved.ID_PARRENT_GROUP)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DATE_CREATED", ConvertTimeZoneToUtcTime(ToBeSaved.DATE_CREATED))
														,new DB.InParameter("@ID_DISTRIBUTOR", ToBeSaved.ID_DISTRIBUTOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTaskGroup(DB_TASK_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_GROUP", toBeDeleted.ID_TASK_GROUP)			
		}
            );
        }

        #endregion
        #region Table TASK_HISTORY

        public DB_TASK_HISTORY[] GetTaskHistory()
        {
            return (DB_TASK_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetTaskHistory",
                new DB.AnalyzeDataSet(GetTaskHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_HISTORY", new int[] {})
					}
            );
        }

        public DB_TASK_HISTORY[] GetTaskHistory(int[] Ids)
        {
            return (DB_TASK_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetTaskHistory",
                new DB.AnalyzeDataSet(GetTaskHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_HISTORY", Ids)
					}
            );
        }

        private object GetTaskHistory(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_HISTORY[] list = new DB_TASK_HISTORY[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_HISTORY insert = new DB_TASK_HISTORY();
                insert.ID_TASK_HISTORY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_HISTORY"]);
                insert.ID_TASK = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK"]);
                insert.ID_TASK_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_STATUS"]);
                insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
                insert.START_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_DATE"]));
                insert.END_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_DATE"]));
                insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTaskHistory(DB_TASK_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_HISTORY", ToBeSaved.ID_TASK_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TASK", ToBeSaved.ID_TASK)
														,new DB.InParameter("@ID_TASK_STATUS", ToBeSaved.ID_TASK_STATUS)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@START_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.START_DATE))
														,new DB.InParameter("@END_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.END_DATE))
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_HISTORY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTaskHistory(DB_TASK_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_HISTORY", toBeDeleted.ID_TASK_HISTORY)			
		}
            );
        }

        #endregion
        #region Table TASK_STATUS

        public DB_TASK_STATUS[] GetTaskStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTaskStatus",
                new DB.AnalyzeDataSet(GetTaskStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_STATUS", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TASK_STATUS[] GetTaskStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTaskStatus",
                new DB.AnalyzeDataSet(GetTaskStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_STATUS", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTaskStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_STATUS[] list = new DB_TASK_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_STATUS insert = new DB_TASK_STATUS();
                insert.ID_TASK_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_STATUS"]);
                insert.ID_DESCR = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.DESCRIPTION = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCRIPTION"]);
                insert.IS_FINISHED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_FINISHED"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTaskStatus(DB_TASK_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_STATUS", ToBeSaved.ID_TASK_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@DESCRIPTION", ToBeSaved.DESCRIPTION)
														,new DB.InParameter("@IS_FINISHED", ToBeSaved.IS_FINISHED)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTaskStatus(DB_TASK_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_STATUS", toBeDeleted.ID_TASK_STATUS)			
		}
            );
        }

        #endregion
        #region Table TASK_TYPE

        public DB_TASK_TYPE[] GetTaskType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTaskType",
                new DB.AnalyzeDataSet(GetTaskType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_TYPE", new int[] {})
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        public DB_TASK_TYPE[] GetTaskType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTaskType",
                new DB.AnalyzeDataSet(GetTaskType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_TYPE", Ids)
					}, autoTransaction, transactionLevel, commandTimeout
            );
        }

        private object GetTaskType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_TYPE[] list = new DB_TASK_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_TYPE insert = new DB_TASK_TYPE();
                insert.ID_TASK_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_TYPE"]);
                insert.ID_TASK_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_TYPE_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.REMOTE = GetValue<bool>(QueryResult.Tables[0].Rows[i]["REMOTE"]);
                insert.DURATION_ESTIMATE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["DURATION_ESTIMATE"]);
                insert.ID_OPERATOR_CREATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_CREATOR"]);
                insert.CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["CREATION_DATE"]));
                insert.AUTHORIZED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["AUTHORIZED"]);
                insert.IMR_OPERATOR = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IMR_OPERATOR"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTaskType(DB_TASK_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_TYPE", ToBeSaved.ID_TASK_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_TASK_TYPE_GROUP", ToBeSaved.ID_TASK_TYPE_GROUP)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
														,new DB.InParameter("@REMOTE", ToBeSaved.REMOTE)
														,new DB.InParameter("@DURATION_ESTIMATE", ToBeSaved.DURATION_ESTIMATE)
														,new DB.InParameter("@ID_OPERATOR_CREATOR", ToBeSaved.ID_OPERATOR_CREATOR)
														,new DB.InParameter("@CREATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CREATION_DATE))
														,new DB.InParameter("@AUTHORIZED", ToBeSaved.AUTHORIZED)
														,new DB.InParameter("@IMR_OPERATOR", ToBeSaved.IMR_OPERATOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTaskType(DB_TASK_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_TYPE", toBeDeleted.ID_TASK_TYPE)			
		}
            );
        }

        #endregion
        #region Table TASK_TYPE_GROUP

        public DB_TASK_TYPE_GROUP[] GetTaskTypeGroup(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskTypeGroup",
                new DB.AnalyzeDataSet(GetTaskTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_TYPE_GROUP", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_TASK_TYPE_GROUP[] GetTaskTypeGroup(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_TASK_TYPE_GROUP[])DB.ExecuteProcedure(
                "imrse_GetTaskTypeGroup",
                new DB.AnalyzeDataSet(GetTaskTypeGroup),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TASK_TYPE_GROUP", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetTaskTypeGroup(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TASK_TYPE_GROUP[] list = new DB_TASK_TYPE_GROUP[count];
            for (int i = 0; i < count; i++)
            {
                DB_TASK_TYPE_GROUP insert = new DB_TASK_TYPE_GROUP();
                insert.ID_TASK_TYPE_GROUP = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_TYPE_GROUP"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);
                insert.ID_TASK_TYPE_GROUP_PARENT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_TASK_TYPE_GROUP_PARENT"]);
                insert.I_LEFT = GetValue<int>(QueryResult.Tables[0].Rows[i]["I_LEFT"]);
                insert.I_RIGHT = GetValue<int>(QueryResult.Tables[0].Rows[i]["I_RIGHT"]);
                insert.ROW_VERSION = GetValue(QueryResult.Tables[0].Rows[i]["ROW_VERSION"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTaskTypeGroup(DB_TASK_TYPE_GROUP ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TASK_TYPE_GROUP", ToBeSaved.ID_TASK_TYPE_GROUP);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTaskTypeGroup",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@DESCR", ToBeSaved.DESCR)
														,new DB.InParameter("@ID_TASK_TYPE_GROUP_PARENT", ToBeSaved.ID_TASK_TYPE_GROUP_PARENT)
														,new DB.InParameter("@I_LEFT", ToBeSaved.I_LEFT)
														,new DB.InParameter("@I_RIGHT", ToBeSaved.I_RIGHT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TASK_TYPE_GROUP = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTaskTypeGroup(DB_TASK_TYPE_GROUP toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTaskTypeGroup",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TASK_TYPE_GROUP", toBeDeleted.ID_TASK_TYPE_GROUP)			
		}
            );
        }

        #endregion

        #region Table TRANSMISSION_STATUS

        public DB_TRANSMISSION_STATUS[] GetTransmissionStatus()
        {
            return (DB_TRANSMISSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTransmissionStatus",
                new DB.AnalyzeDataSet(GetTransmissionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_STATUS", new int[] {})
					}
            );
        }

        public DB_TRANSMISSION_STATUS[] GetTransmissionStatus(int[] Ids)
        {
            return (DB_TRANSMISSION_STATUS[])DB.ExecuteProcedure(
                "imrse_GetTransmissionStatus",
                new DB.AnalyzeDataSet(GetTransmissionStatus),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_STATUS", Ids)
					}
            );
        }

        private object GetTransmissionStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_STATUS[] list = new DB_TRANSMISSION_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_STATUS insert = new DB_TRANSMISSION_STATUS();
                insert.ID_TRANSMISSION_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionStatus(DB_TRANSMISSION_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_STATUS", ToBeSaved.ID_TRANSMISSION_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionStatus",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTransmissionStatus(DB_TRANSMISSION_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionStatus",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_STATUS", toBeDeleted.ID_TRANSMISSION_STATUS)			
		}
            );
        }

        #endregion
        #region Table TRANSMISSION_TYPE

        public DB_TRANSMISSION_TYPE[] GetTransmissionType()
        {
            return (DB_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionType",
                new DB.AnalyzeDataSet(GetTransmissionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_TYPE", new int[] {})
					}
            );
        }

        public DB_TRANSMISSION_TYPE[] GetTransmissionType(int[] Ids)
        {
            return (DB_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetTransmissionType",
                new DB.AnalyzeDataSet(GetTransmissionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRANSMISSION_TYPE", Ids)
					}
            );
        }

        private object GetTransmissionType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRANSMISSION_TYPE[] list = new DB_TRANSMISSION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRANSMISSION_TYPE insert = new DB_TRANSMISSION_TYPE();
                insert.ID_TRANSMISSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TRANSMISSION_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveTransmissionType(DB_TRANSMISSION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRANSMISSION_TYPE", ToBeSaved.ID_TRANSMISSION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTransmissionType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRANSMISSION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteTransmissionType(DB_TRANSMISSION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTransmissionType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRANSMISSION_TYPE", toBeDeleted.ID_TRANSMISSION_TYPE)			
		}
            );
        }

        #endregion

        #region Table TRIGGERS

        public DB_TRIGGER[] GetTriggers()
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRIGGER", new long[] {})
					}
            );
        }

        public DB_TRIGGER[] GetTriggers(long[] Ids)
        {
            return (DB_TRIGGER[])DB.ExecuteProcedure(
                "imrse_GetTrigger",
                new DB.AnalyzeDataSet(GetTriggers),
                new DB.Parameter[] { 
			CreateTableParam("@ID_TRIGGER", Ids)
					}
            );
        }

        private object GetTriggers(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_TRIGGER[] list = new DB_TRIGGER[count];
            for (int i = 0; i < count; i++)
            {
                DB_TRIGGER insert = new DB_TRIGGER();
                insert.ID_TRIGGER = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_TRIGGER"]);
                insert.TABLE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE_NAME"]);
                insert.AUDIT = GetValue<string>(QueryResult.Tables[0].Rows[i]["AUDIT"]);
                insert.DATA_CHANGE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DATA_CHANGE"]);
                insert.DICT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DICT_TABLE"]);
                insert.OBJECT_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["OBJECT_TABLE"]);
                insert.DATA_TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["DATA_TABLE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveTriggers(DB_TRIGGER ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_TRIGGER", ToBeSaved.ID_TRIGGER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveTrigger",
                new DB.Parameter[] {
			        InsertId
					,new DB.InParameter("@TABLE_NAME", ToBeSaved.TABLE_NAME)
                    ,new DB.InParameter("@AUDIT", ToBeSaved.AUDIT)
                    ,new DB.InParameter("@DATA_CHANGE", ToBeSaved.DATA_CHANGE)
                    ,new DB.InParameter("@DICT_TABLE", ToBeSaved.DICT_TABLE)
                    ,new DB.InParameter("@OBJECT_TABLE", ToBeSaved.OBJECT_TABLE)
                    ,new DB.InParameter("@DATA_TABLE", ToBeSaved.DATA_TABLE)
					
				}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_TRIGGER = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteTriggers(DB_TRIGGER toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelTrigger",
                new DB.Parameter[] {
            new DB.InParameter("@ID_TRIGGER", toBeDeleted.ID_TRIGGER)			
		}
            );
        }

        #endregion

        #region Table UNIQUE_TYPE

        public DB_UNIQUE_TYPE[] GetUniqueType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_UNIQUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetUniqueType",
                new DB.AnalyzeDataSet(GetUniqueType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_UNIQUE_TYPE", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_UNIQUE_TYPE[] GetUniqueType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_UNIQUE_TYPE[])DB.ExecuteProcedure(
                "imrse_GetUniqueType",
                new DB.AnalyzeDataSet(GetUniqueType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_UNIQUE_TYPE", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetUniqueType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_UNIQUE_TYPE[] list = new DB_UNIQUE_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_UNIQUE_TYPE insert = new DB_UNIQUE_TYPE();
                insert.ID_UNIQUE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIQUE_TYPE"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                insert.PROC_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PROC_NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveUniqueType(DB_UNIQUE_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_UNIQUE_TYPE", ToBeSaved.ID_UNIQUE_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveUniqueType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@PROC_NAME", ToBeSaved.PROC_NAME)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_UNIQUE_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteUniqueType(DB_UNIQUE_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelUniqueType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_UNIQUE_TYPE", toBeDeleted.ID_UNIQUE_TYPE)			
		}
            );
        }

        #endregion

        #region Table UNIT

        public DB_UNIT[] GetUnit(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_UNIT[])DB.ExecuteProcedure(
                "imrse_GetUnit",
                new DB.AnalyzeDataSet(GetUnit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_UNIT", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_UNIT[] GetUnit(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_UNIT[])DB.ExecuteProcedure(
                "imrse_GetUnit",
                new DB.AnalyzeDataSet(GetUnit),
                new DB.Parameter[] { 
			CreateTableParam("@ID_UNIT", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetUnit(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_UNIT[] list = new DB_UNIT[count];
            for (int i = 0; i < count; i++)
            {
                DB_UNIT insert = new DB_UNIT();
                insert.ID_UNIT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT"]);
                insert.ID_UNIT_BASE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_UNIT_BASE"]);
                insert.SCALE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["SCALE"]);
                insert.BIAS = GetValue<Double>(QueryResult.Tables[0].Rows[i]["BIAS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveUnit(DB_UNIT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_UNIT", ToBeSaved.ID_UNIT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveUnit",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_UNIT_BASE", ToBeSaved.ID_UNIT_BASE)
														,new DB.InParameter("@SCALE", ToBeSaved.SCALE)
														,new DB.InParameter("@BIAS", ToBeSaved.BIAS)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_UNIT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteUnit(DB_UNIT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelUnit",
                new DB.Parameter[] {
            new DB.InParameter("@ID_UNIT", toBeDeleted.ID_UNIT)			
		}
            );
        }

        #endregion
        
        #region Table VERSION

        public DB_VERSION[] GetVersion(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION[])DB.ExecuteProcedure(
                "imrse_GetVersion",
                new DB.AnalyzeDataSet(GetVersion),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION[] GetVersion(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION[])DB.ExecuteProcedure(
                "imrse_GetVersion",
                new DB.AnalyzeDataSet(GetVersion),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersion(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION()
            {
                ID_VERSION = GetValue<long>(row["ID_VERSION"]),
                ID_VERSION_STATE = GetValue<int>(row["ID_VERSION_STATE"]),
                ID_VERSION_ELEMENT_TYPE = GetNullableValue<int>(row["ID_VERSION_ELEMENT_TYPE"]),
                ID_VERSION_TYPE = GetValue<int>(row["ID_VERSION_TYPE"]),
                VERSION_NBR = GetValue<string>(row["VERSION_NBR"]),
                VERSION_RAW = GetNullableValue<long>(row["VERSION_RAW"]),
                RELEASE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["RELEASE_DATE"])),
                ID_OPERATOR_PUBLISHER = GetNullableValue<int>(row["ID_OPERATOR_PUBLISHER"]),
                NOTIFICATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["NOTIFICATION_DATE"])),
                ID_OPERATOR_NOTIFIER = GetNullableValue<int>(row["ID_OPERATOR_NOTIFIER"]),
                CONFIRM_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["CONFIRM_DATE"])),
                ID_OPERATOR_COFIRMING = GetNullableValue<int>(row["ID_OPERATOR_COFIRMING"]),
                RELEASE_NOTES = GetValue<string>(row["RELEASE_NOTES"]),
                ID_PRIORITY = GetValue<int>(row["ID_PRIORITY"]),
                DEADLINE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["DEADLINE"])),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VERSION[] list = new DB_VERSION[count];
            for (int i = 0; i < count; i++)
            {
                DB_VERSION insert = new DB_VERSION();
									        insert.ID_VERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
												        insert.ID_VERSION_STATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_STATE"]);
												        insert.ID_VERSION_ELEMENT_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE"]);
												        insert.ID_VERSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_TYPE"]);
												        insert.VERSION_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["VERSION_NBR"]);
												        insert.VERSION_RAW = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["VERSION_RAW"]);
												        insert.RELEASE_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["RELEASE_DATE"]));
												        insert.ID_OPERATOR_PUBLISHER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_PUBLISHER"]);
												        insert.NOTIFICATION_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["NOTIFICATION_DATE"]));
												        insert.ID_OPERATOR_NOTIFIER = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_NOTIFIER"]);
												        insert.CONFIRM_DATE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["CONFIRM_DATE"]));
												        insert.ID_OPERATOR_COFIRMING = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR_COFIRMING"]);
												        insert.RELEASE_NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["RELEASE_NOTES"]);
												        insert.ID_PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PRIORITY"]);
												        insert.DEADLINE = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["DEADLINE"]));
												        insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												        insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
							        list[i] = insert;
            }
            return list;
	        */
            #endregion
        }

        public long SaveVersion(DB_VERSION ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION", ToBeSaved.ID_VERSION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersion",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_STATE", ToBeSaved.ID_VERSION_STATE)
														,new DB.InParameter("@ID_VERSION_ELEMENT_TYPE", ToBeSaved.ID_VERSION_ELEMENT_TYPE)
														,new DB.InParameter("@ID_VERSION_TYPE", ToBeSaved.ID_VERSION_TYPE)
														,new DB.InParameter("@VERSION_NBR", ToBeSaved.VERSION_NBR)
														,new DB.InParameter("@VERSION_RAW", ToBeSaved.VERSION_RAW)
														,new DB.InParameter("@RELEASE_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.RELEASE_DATE))
													,new DB.InParameter("@ID_OPERATOR_PUBLISHER", ToBeSaved.ID_OPERATOR_PUBLISHER)
														,new DB.InParameter("@NOTIFICATION_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.NOTIFICATION_DATE))
													,new DB.InParameter("@ID_OPERATOR_NOTIFIER", ToBeSaved.ID_OPERATOR_NOTIFIER)
														,new DB.InParameter("@CONFIRM_DATE", ConvertTimeZoneToUtcTime(ToBeSaved.CONFIRM_DATE))
													,new DB.InParameter("@ID_OPERATOR_COFIRMING", ToBeSaved.ID_OPERATOR_COFIRMING)
														,new DB.InParameter("@RELEASE_NOTES", ToBeSaved.RELEASE_NOTES)
														,new DB.InParameter("@ID_PRIORITY", ToBeSaved.ID_PRIORITY)
														,new DB.InParameter("@DEADLINE", ConvertTimeZoneToUtcTime(ToBeSaved.DEADLINE))
													,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersion(DB_VERSION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersion",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION", toBeDeleted.ID_VERSION)			
		}
            );
        }

        #endregion
        #region Table VERSION_DATA

        public DB_VERSION_DATA[] GetVersionData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionData",
                new DB.AnalyzeDataSet(GetVersionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_DATA[] GetVersionData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionData",
                new DB.AnalyzeDataSet(GetVersionData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_DATA()
            {
                ID_VERSION_DATA = GetValue<long>(row["ID_VERSION_DATA"]),
                ID_VERSION = GetValue<long>(row["ID_VERSION"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_DATA[] list = new DB_VERSION_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_DATA insert = new DB_VERSION_DATA();
									insert.ID_VERSION_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_DATA"]);
												insert.ID_VERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionDataFilter

        public DB_VERSION_DATA[] GetVersionDataFilter(long[] IdVersionData, long[] IdVersion, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionData != null && IdVersionData.Length > MaxItemsPerSqlTableType) ||
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionData != null) args.AddRange(IdVersionData.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_VERSION_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_DATA[])DB.ExecuteProcedure(
                        "imrse_GetVersionDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionData),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_DATA[])DB.ExecuteProcedure(
                    "imrse_GetVersionDataFilter",
                    new DB.AnalyzeDataSet(GetVersionData),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_DATA", IdVersionData),
				CreateTableParam("@ID_VERSION", IdVersion),
				CreateTableParam("@ID_DATA_TYPE", IdDataType),
				CreateTableParam("@INDEX_NBR", IndexNbr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public long SaveVersionData(DB_VERSION_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_DATA", ToBeSaved.ID_VERSION_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION", ToBeSaved.ID_VERSION)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersionData(DB_VERSION_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_DATA", toBeDeleted.ID_VERSION_DATA)			
		    });
        }

        #endregion
        #region Table VERSION_ELEMENT

        public DB_VERSION_ELEMENT[] GetVersionElement(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT[])DB.ExecuteProcedure(
                "imrse_GetVersionElement",
                new DB.AnalyzeDataSet(GetVersionElement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT[] GetVersionElement(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT[])DB.ExecuteProcedure(
                "imrse_GetVersionElement",
                new DB.AnalyzeDataSet(GetVersionElement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionElement(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT()
            {
                ID_VERSION_ELEMENT = GetValue<int>(row["ID_VERSION_ELEMENT"]),
                ID_VERSION_ELEMENT_TYPE = GetValue<int>(row["ID_VERSION_ELEMENT_TYPE"]),
                ID_VERSION_NBR_FORMAT = GetNullableValue<int>(row["ID_VERSION_NBR_FORMAT"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
                REFERENCE_TYPE = GetNullableValue<int>(row["REFERENCE_TYPE"]),
                REFERENCE_VALUE = GetValue(row["REFERENCE_VALUE"]),
                ID_VERSION = GetNullableValue<long>(row["ID_VERSION"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT[] list = new DB_VERSION_ELEMENT[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT insert = new DB_VERSION_ELEMENT();
									insert.ID_VERSION_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT"]);
												insert.ID_VERSION_ELEMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE"]);
												insert.ID_VERSION_NBR_FORMAT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_NBR_FORMAT"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
												insert.REFERENCE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["REFERENCE_TYPE"]);
												insert.REFERENCE_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
												insert.ID_VERSION = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementFilter

        public DB_VERSION_ELEMENT[] GetVersionElementFilter(int[] IdVersionElement, int[] IdVersionElementType, int[] IdVersionNbrFormat, string Name, long[] IdDescr, int[] ReferenceType,
                            long[] IdVersion, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElement != null && IdVersionElement.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementType != null && IdVersionElementType.Length > MaxItemsPerSqlTableType) ||
                (IdVersionNbrFormat != null && IdVersionNbrFormat.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType) ||
                (ReferenceType != null && ReferenceType.Length > MaxItemsPerSqlTableType) ||
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElement != null) args.AddRange(IdVersionElement.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElementType != null) args.AddRange(IdVersionElementType.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionNbrFormat != null) args.AddRange(IdVersionNbrFormat.Select(s => new Tuple<int, object>(3, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(5, s)));
                if (ReferenceType != null) args.AddRange(ReferenceType.Select(s => new Tuple<int, object>(6, s)));
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(7, s)));

                DB_VERSION_ELEMENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElement),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementFilter",
                    new DB.AnalyzeDataSet(GetVersionElement),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT", IdVersionElement),
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE", IdVersionElementType),
				CreateTableParam("@ID_VERSION_NBR_FORMAT", IdVersionNbrFormat),
				new DB.InParameter("@NAME", Name),
				CreateTableParam("@ID_DESCR", IdDescr),
				CreateTableParam("@REFERENCE_TYPE", ReferenceType),
				CreateTableParam("@ID_VERSION", IdVersion),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionElement(DB_VERSION_ELEMENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT", ToBeSaved.ID_VERSION_ELEMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElement",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT_TYPE", ToBeSaved.ID_VERSION_ELEMENT_TYPE)
														,new DB.InParameter("@ID_VERSION_NBR_FORMAT", ToBeSaved.ID_VERSION_NBR_FORMAT)
														,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@REFERENCE_TYPE", ToBeSaved.REFERENCE_TYPE)
														,ParamObject("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE, 0, 0)
													,new DB.InParameter("@ID_VERSION", ToBeSaved.ID_VERSION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteVersionElement(DB_VERSION_ELEMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElement",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_ELEMENT", toBeDeleted.ID_VERSION_ELEMENT)			
		    });
        }
        #endregion
        #region Table VERSION_ELEMENT_DATA

        public DB_VERSION_ELEMENT_DATA[] GetVersionElementData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionElementData",
                new DB.AnalyzeDataSet(GetVersionElementData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_DATA[] GetVersionElementData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionElementData",
                new DB.AnalyzeDataSet(GetVersionElementData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionElementData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_DATA()
            {
                ID_VERSION_ELEMENT_DATA = GetValue<long>(row["ID_VERSION_ELEMENT_DATA"]),
                ID_VERSION_ELEMENT = GetValue<int>(row["ID_VERSION_ELEMENT"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_DATA[] list = new DB_VERSION_ELEMENT_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_DATA insert = new DB_VERSION_ELEMENT_DATA();
									insert.ID_VERSION_ELEMENT_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_DATA"]);
												insert.ID_VERSION_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementDataFilter

        public DB_VERSION_ELEMENT_DATA[] GetVersionElementDataFilter(long[] IdVersionElementData, int[] IdVersionElement, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementData != null && IdVersionElementData.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElement != null && IdVersionElement.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementData != null) args.AddRange(IdVersionElementData.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElement != null) args.AddRange(IdVersionElement.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_VERSION_ELEMENT_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_DATA[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementData),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_DATA[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementDataFilter",
                    new DB.AnalyzeDataSet(GetVersionElementData),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_DATA", IdVersionElementData),
				CreateTableParam("@ID_VERSION_ELEMENT", IdVersionElement),
				CreateTableParam("@ID_DATA_TYPE", IdDataType),
				CreateTableParam("@INDEX_NBR", IndexNbr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public long SaveVersionElementData(DB_VERSION_ELEMENT_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_DATA", ToBeSaved.ID_VERSION_ELEMENT_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT", ToBeSaved.ID_VERSION_ELEMENT)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }
        
        public void DeleteVersionElementData(DB_VERSION_ELEMENT_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementData",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_VERSION_ELEMENT_DATA", toBeDeleted.ID_VERSION_ELEMENT_DATA)			
		        }
            );
        }

        #endregion
        #region Table VERSION_ELEMENT_HIERARCHY

        public DB_VERSION_ELEMENT_HIERARCHY[] GetVersionElementHierarchy(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementHierarchy",
                new DB.AnalyzeDataSet(GetVersionElementHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_HIERARCHY", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_HIERARCHY[] GetVersionElementHierarchy(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementHierarchy",
                new DB.AnalyzeDataSet(GetVersionElementHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_HIERARCHY", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionElementHierarchy(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_HIERARCHY()
            {
                ID_VERSION_ELEMENT_HIERARCHY = GetValue<long>(row["ID_VERSION_ELEMENT_HIERARCHY"]),
                ID_VERSION_ELEMENT_HIERARCHY_PARENT = GetNullableValue<long>(row["ID_VERSION_ELEMENT_HIERARCHY_PARENT"]),
                ID_VERSION_ELEMENT = GetValue<int>(row["ID_VERSION_ELEMENT"]),
                HIERARCHY = GetValue<string>(row["HIERARCHY"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_HIERARCHY[] list = new DB_VERSION_ELEMENT_HIERARCHY[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_HIERARCHY insert = new DB_VERSION_ELEMENT_HIERARCHY();
									insert.ID_VERSION_ELEMENT_HIERARCHY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_HIERARCHY"]);
												insert.ID_VERSION_ELEMENT_HIERARCHY_PARENT = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_HIERARCHY_PARENT"]);
												insert.ID_VERSION_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT"]);
												insert.HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["HIERARCHY"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementHierarchyFilter

        public DB_VERSION_ELEMENT_HIERARCHY[] GetVersionElementHierarchyFilter(long[] IdVersionElementHierarchy, long[] IdVersionElementHierarchyParent, int[] IdVersionElement, string Hierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementHierarchyFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementHierarchy != null && IdVersionElementHierarchy.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementHierarchyParent != null && IdVersionElementHierarchyParent.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElement != null && IdVersionElement.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementHierarchy != null) args.AddRange(IdVersionElementHierarchy.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElementHierarchyParent != null) args.AddRange(IdVersionElementHierarchyParent.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionElement != null) args.AddRange(IdVersionElement.Select(s => new Tuple<int, object>(3, s)));

                DB_VERSION_ELEMENT_HIERARCHY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_HIERARCHY[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementHierarchyFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementHierarchy),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@HIERARCHY", Hierarchy),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_HIERARCHY[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementHierarchyFilter",
                    new DB.AnalyzeDataSet(GetVersionElementHierarchy),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_HIERARCHY", IdVersionElementHierarchy),
				CreateTableParam("@ID_VERSION_ELEMENT_HIERARCHY_PARENT", IdVersionElementHierarchyParent),
				CreateTableParam("@ID_VERSION_ELEMENT", IdVersionElement),
				new DB.InParameter("@HIERARCHY", Hierarchy),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public long SaveVersionElementHierarchy(DB_VERSION_ELEMENT_HIERARCHY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_HIERARCHY", ToBeSaved.ID_VERSION_ELEMENT_HIERARCHY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementHierarchy",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT_HIERARCHY_PARENT", ToBeSaved.ID_VERSION_ELEMENT_HIERARCHY_PARENT)
														,new DB.InParameter("@ID_VERSION_ELEMENT", ToBeSaved.ID_VERSION_ELEMENT)
														,new DB.InParameter("@HIERARCHY", ToBeSaved.HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_HIERARCHY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersionElementHierarchy(DB_VERSION_ELEMENT_HIERARCHY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementHierarchy",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_ELEMENT_HIERARCHY", toBeDeleted.ID_VERSION_ELEMENT_HIERARCHY)			
		    });
        }

        #endregion
        #region Table VERSION_ELEMENT_HISTORY

        public DB_VERSION_ELEMENT_HISTORY[] GetVersionElementHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementHistory",
                new DB.AnalyzeDataSet(GetVersionElementHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_HISTORY", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_HISTORY[] GetVersionElementHistory(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementHistory",
                new DB.AnalyzeDataSet(GetVersionElementHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_HISTORY", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionElementHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_HISTORY()
            {
                ID_VERSION_ELEMENT_HISTORY = GetValue<long>(row["ID_VERSION_ELEMENT_HISTORY"]),
                ID_VERSION_ELEMENT = GetValue<int>(row["ID_VERSION_ELEMENT"]),
                ID_VERSION = GetValue<long>(row["ID_VERSION"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                ID_OPERATOR = GetValue<int>(row["ID_OPERATOR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_HISTORY[] list = new DB_VERSION_ELEMENT_HISTORY[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_HISTORY insert = new DB_VERSION_ELEMENT_HISTORY();
									insert.ID_VERSION_ELEMENT_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_HISTORY"]);
												insert.ID_VERSION_ELEMENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT"]);
												insert.ID_VERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
												insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementHistoryFilter

        public DB_VERSION_ELEMENT_HISTORY[] GetVersionElementHistoryFilter(long[] IdVersionElementHistory, int[] IdVersionElement, long[] IdVersion, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, int[] IdOperator,
                             long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementHistoryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementHistory != null && IdVersionElementHistory.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElement != null && IdVersionElement.Length > MaxItemsPerSqlTableType) ||
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementHistory != null) args.AddRange(IdVersionElementHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElement != null) args.AddRange(IdVersionElement.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(3, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(6, s)));

                DB_VERSION_ELEMENT_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementHistory),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					CreateTableParam("@START_TIME_CODE", StartTime),
					CreateTableParam("@END_TIME_CODE", EndTime),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementHistoryFilter",
                    new DB.AnalyzeDataSet(GetVersionElementHistory),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_HISTORY", IdVersionElementHistory),
				CreateTableParam("@ID_VERSION_ELEMENT", IdVersionElement),
				CreateTableParam("@ID_VERSION", IdVersion),
				CreateTableParam("@START_TIME_CODE", StartTime),
				CreateTableParam("@END_TIME_CODE", EndTime),
				CreateTableParam("@ID_OPERATOR", IdOperator),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion


        public long SaveVersionElementHistory(DB_VERSION_ELEMENT_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_HISTORY", ToBeSaved.ID_VERSION_ELEMENT_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT", ToBeSaved.ID_VERSION_ELEMENT)
														,new DB.InParameter("@ID_VERSION", ToBeSaved.ID_VERSION)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
													,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersionElementHistory(DB_VERSION_ELEMENT_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_ELEMENT_HISTORY", toBeDeleted.ID_VERSION_ELEMENT_HISTORY)			
		    });
        }

        #endregion
        #region Table VERSION_ELEMENT_TYPE

        public DB_VERSION_ELEMENT_TYPE[] GetVersionElementType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetVersionElementType",
                new DB.AnalyzeDataSet(GetVersionElementType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_TYPE[] GetVersionElementType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE[])DB.ExecuteProcedure(
                "imrse_GetVersionElementType",
                new DB.AnalyzeDataSet(GetVersionElementType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionElementType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_TYPE()
            {
                ID_VERSION_ELEMENT_TYPE = GetValue<int>(row["ID_VERSION_ELEMENT_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_TYPE[] list = new DB_VERSION_ELEMENT_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_TYPE insert = new DB_VERSION_ELEMENT_TYPE();
									insert.ID_VERSION_ELEMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementTypeFilter

        public DB_VERSION_ELEMENT_TYPE[] GetVersionElementTypeFilter(int[] IdVersionElementType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementType != null && IdVersionElementType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementType != null) args.AddRange(IdVersionElementType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_VERSION_ELEMENT_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementType),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementTypeFilter",
                    new DB.AnalyzeDataSet(GetVersionElementType),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE", IdVersionElementType),
				new DB.InParameter("@NAME", Name),
				CreateTableParam("@ID_DESCR", IdDescr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionElementType(DB_VERSION_ELEMENT_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_TYPE", ToBeSaved.ID_VERSION_ELEMENT_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteVersionElementType(DB_VERSION_ELEMENT_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_ELEMENT_TYPE", toBeDeleted.ID_VERSION_ELEMENT_TYPE)			
		    });
        }

        #endregion
        #region Table VERSION_ELEMENT_TYPE_DATA

        public DB_VERSION_ELEMENT_TYPE_DATA[] GetVersionElementTypeData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionElementTypeData",
                new DB.AnalyzeDataSet(GetVersionElementTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE_DATA", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_TYPE_DATA[] GetVersionElementTypeData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE_DATA[])DB.ExecuteProcedure(
                "imrse_GetVersionElementTypeData",
                new DB.AnalyzeDataSet(GetVersionElementTypeData),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE_DATA", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionElementTypeData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_TYPE_DATA()
            {
                ID_VERSION_ELEMENT_TYPE_DATA = GetValue<long>(row["ID_VERSION_ELEMENT_TYPE_DATA"]),
                ID_VERSION_ELEMENT_TYPE = GetValue<int>(row["ID_VERSION_ELEMENT_TYPE"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_TYPE_DATA[] list = new DB_VERSION_ELEMENT_TYPE_DATA[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_TYPE_DATA insert = new DB_VERSION_ELEMENT_TYPE_DATA();
									insert.ID_VERSION_ELEMENT_TYPE_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE_DATA"]);
												insert.ID_VERSION_ELEMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        public long SaveVersionElementTypeData(DB_VERSION_ELEMENT_TYPE_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_TYPE_DATA", ToBeSaved.ID_VERSION_ELEMENT_TYPE_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementTypeData",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT_TYPE", ToBeSaved.ID_VERSION_ELEMENT_TYPE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
								}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_TYPE_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersionElementTypeData(DB_VERSION_ELEMENT_TYPE_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementTypeData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_ELEMENT_TYPE_DATA", toBeDeleted.ID_VERSION_ELEMENT_TYPE_DATA)			
		}
            );
        }
        
        #region GetVersionElementTypeDataFilter

        public DB_VERSION_ELEMENT_TYPE_DATA[] GetVersionElementTypeDataFilter(long[] IdVersionElementTypeData, int[] IdVersionElementType, long[] IdDataType, int[] IndexNbr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeDataFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementTypeData != null && IdVersionElementTypeData.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementType != null && IdVersionElementType.Length > MaxItemsPerSqlTableType) ||
                (IdDataType != null && IdDataType.Length > MaxItemsPerSqlTableType) ||
                (IndexNbr != null && IndexNbr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementTypeData != null) args.AddRange(IdVersionElementTypeData.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElementType != null) args.AddRange(IdVersionElementType.Select(s => new Tuple<int, object>(2, s)));
                if (IdDataType != null) args.AddRange(IdDataType.Select(s => new Tuple<int, object>(3, s)));
                if (IndexNbr != null) args.AddRange(IndexNbr.Select(s => new Tuple<int, object>(4, s)));

                DB_VERSION_ELEMENT_TYPE_DATA[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_TYPE_DATA[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementTypeDataFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementTypeData),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_TYPE_DATA[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementTypeDataFilter",
                    new DB.AnalyzeDataSet(GetVersionElementTypeData),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE_DATA", IdVersionElementTypeData),
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE", IdVersionElementType),
				CreateTableParam("@ID_DATA_TYPE", IdDataType),
				CreateTableParam("@INDEX_NBR", IndexNbr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion
        #endregion
        #region Table VERSION_ELEMENT_TYPE_HIERARCHY

        public DB_VERSION_ELEMENT_TYPE_HIERARCHY[] GetVersionElementTypeHierarchy(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementTypeHierarchy",
                new DB.AnalyzeDataSet(GetVersionElementTypeHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE_HIERARCHY", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_ELEMENT_TYPE_HIERARCHY[] GetVersionElementTypeHierarchy(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_ELEMENT_TYPE_HIERARCHY[])DB.ExecuteProcedure(
                "imrse_GetVersionElementTypeHierarchy",
                new DB.AnalyzeDataSet(GetVersionElementTypeHierarchy),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_ELEMENT_TYPE_HIERARCHY", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionElementTypeHierarchy(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_ELEMENT_TYPE_HIERARCHY()
            {
                ID_VERSION_ELEMENT_TYPE_HIERARCHY = GetValue<int>(row["ID_VERSION_ELEMENT_TYPE_HIERARCHY"]),
                ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT = GetNullableValue<int>(row["ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT"]),
                ID_VERSION_ELEMENT_TYPE = GetValue<int>(row["ID_VERSION_ELEMENT_TYPE"]),
                REQUIRED = GetValue<bool>(row["REQUIRED"]),
                HIERARCHY = GetValue<string>(row["HIERARCHY"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_ELEMENT_TYPE_HIERARCHY[] list = new DB_VERSION_ELEMENT_TYPE_HIERARCHY[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_ELEMENT_TYPE_HIERARCHY insert = new DB_VERSION_ELEMENT_TYPE_HIERARCHY();
									insert.ID_VERSION_ELEMENT_TYPE_HIERARCHY = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE_HIERARCHY"]);
												insert.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT"]);
												insert.ID_VERSION_ELEMENT_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_ELEMENT_TYPE"]);
												insert.REQUIRED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["REQUIRED"]);
												insert.HIERARCHY = GetValue<string>(QueryResult.Tables[0].Rows[i]["HIERARCHY"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionElementTypeHierarchyFilter

        public DB_VERSION_ELEMENT_TYPE_HIERARCHY[] GetVersionElementTypeHierarchyFilter(int[] IdVersionElementTypeHierarchy, int[] IdVersionElementTypeHierarchyParent, int[] IdVersionElementType, bool? Required, string Hierarchy, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeHierarchyFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionElementTypeHierarchy != null && IdVersionElementTypeHierarchy.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementTypeHierarchyParent != null && IdVersionElementTypeHierarchyParent.Length > MaxItemsPerSqlTableType) ||
                (IdVersionElementType != null && IdVersionElementType.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionElementTypeHierarchy != null) args.AddRange(IdVersionElementTypeHierarchy.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersionElementTypeHierarchyParent != null) args.AddRange(IdVersionElementTypeHierarchyParent.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionElementType != null) args.AddRange(IdVersionElementType.Select(s => new Tuple<int, object>(3, s)));

                DB_VERSION_ELEMENT_TYPE_HIERARCHY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_ELEMENT_TYPE_HIERARCHY[])DB.ExecuteProcedure(
                        "imrse_GetVersionElementTypeHierarchyFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionElementTypeHierarchy),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@REQUIRED", Required),
					new DB.InParameter("@HIERARCHY", Hierarchy),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_ELEMENT_TYPE_HIERARCHY[])DB.ExecuteProcedure(
                    "imrse_GetVersionElementTypeHierarchyFilter",
                    new DB.AnalyzeDataSet(GetVersionElementTypeHierarchy),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE_HIERARCHY", IdVersionElementTypeHierarchy),
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT", IdVersionElementTypeHierarchyParent),
				CreateTableParam("@ID_VERSION_ELEMENT_TYPE", IdVersionElementType),
				new DB.InParameter("@REQUIRED", Required),
				new DB.InParameter("@HIERARCHY", Hierarchy),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionElementTypeHierarchy(DB_VERSION_ELEMENT_TYPE_HIERARCHY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_ELEMENT_TYPE_HIERARCHY", ToBeSaved.ID_VERSION_ELEMENT_TYPE_HIERARCHY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionElementTypeHierarchy",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT", ToBeSaved.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT)
														,new DB.InParameter("@ID_VERSION_ELEMENT_TYPE", ToBeSaved.ID_VERSION_ELEMENT_TYPE)
														,new DB.InParameter("@REQUIRED", ToBeSaved.REQUIRED)
														,new DB.InParameter("@HIERARCHY", ToBeSaved.HIERARCHY)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_ELEMENT_TYPE_HIERARCHY = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteVersionElementTypeHierarchy(DB_VERSION_ELEMENT_TYPE_HIERARCHY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionElementTypeHierarchy",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_VERSION_ELEMENT_TYPE_HIERARCHY", toBeDeleted.ID_VERSION_ELEMENT_TYPE_HIERARCHY)			
		    });
        }

        #endregion
        #region Table VERSION_HISTORY

        public DB_VERSION_HISTORY[] GetVersionHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetVersionHistory",
                new DB.AnalyzeDataSet(GetVersionHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_HISTORY", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_HISTORY[] GetVersionHistory(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetVersionHistory",
                new DB.AnalyzeDataSet(GetVersionHistory),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_HISTORY", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_HISTORY()
            {
                ID_VERSION_HISTORY = GetValue<long>(row["ID_VERSION_HISTORY"]),
                ID_VERSION = GetValue<long>(row["ID_VERSION"]),
                ID_VERSION_STATE = GetNullableValue<int>(row["ID_VERSION_STATE"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                ID_OPERATOR = GetValue<int>(row["ID_OPERATOR"]),
                NOTES = GetValue<string>(row["NOTES"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_HISTORY[] list = new DB_VERSION_HISTORY[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_HISTORY insert = new DB_VERSION_HISTORY();
									insert.ID_VERSION_HISTORY = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_HISTORY"]);
												insert.ID_VERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
												insert.ID_VERSION_STATE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_STATE"]);
												insert.START_TIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["START_TIME"]));
												insert.END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["END_TIME"]));
												insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
												insert.NOTES = GetValue<string>(QueryResult.Tables[0].Rows[i]["NOTES"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionHistoryFilter

        public DB_VERSION_HISTORY[] GetVersionHistoryFilter(long[] IdVersionHistory, long[] IdVersion, int[] IdVersionState, TypeDateTimeCode StartTime, TypeDateTimeCode EndTime, int[] IdOperator,
                            string Notes, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionHistoryFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionHistory != null && IdVersionHistory.Length > MaxItemsPerSqlTableType) ||
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType) ||
                (IdVersionState != null && IdVersionState.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionHistory != null) args.AddRange(IdVersionHistory.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionState != null) args.AddRange(IdVersionState.Select(s => new Tuple<int, object>(3, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(6, s)));

                DB_VERSION_HISTORY[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_HISTORY[])DB.ExecuteProcedure(
                        "imrse_GetVersionHistoryFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionHistory),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					CreateTableParam("@START_TIME_CODE", StartTime),
					CreateTableParam("@END_TIME_CODE", EndTime),
					new DB.InParameter("@NOTES", Notes),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_HISTORY[])DB.ExecuteProcedure(
                    "imrse_GetVersionHistoryFilter",
                    new DB.AnalyzeDataSet(GetVersionHistory),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_HISTORY", IdVersionHistory),
				CreateTableParam("@ID_VERSION", IdVersion),
				CreateTableParam("@ID_VERSION_STATE", IdVersionState),
				CreateTableParam("@START_TIME_CODE", StartTime),
				CreateTableParam("@END_TIME_CODE", EndTime),
				CreateTableParam("@ID_OPERATOR", IdOperator),
				new DB.InParameter("@NOTES", Notes),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public long SaveVersionHistory(DB_VERSION_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_HISTORY", ToBeSaved.ID_VERSION_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionHistory",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION", ToBeSaved.ID_VERSION)
														,new DB.InParameter("@ID_VERSION_STATE", ToBeSaved.ID_VERSION_STATE)
														,new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME))
													,new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME))
													,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
														,new DB.InParameter("@NOTES", ToBeSaved.NOTES)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteVersionHistory(DB_VERSION_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionHistory",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_HISTORY", toBeDeleted.ID_VERSION_HISTORY)			
		}
            );
        }
        #endregion
        #region Table VERSION_NBR_FORMAT

        public DB_VERSION_NBR_FORMAT[] GetVersionNbrFormat(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_NBR_FORMAT[])DB.ExecuteProcedure(
                "imrse_GetVersionNbrFormat",
                new DB.AnalyzeDataSet(GetVersionNbrFormat),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_NBR_FORMAT", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_NBR_FORMAT[] GetVersionNbrFormat(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_NBR_FORMAT[])DB.ExecuteProcedure(
                "imrse_GetVersionNbrFormat",
                new DB.AnalyzeDataSet(GetVersionNbrFormat),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_NBR_FORMAT", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionNbrFormat(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_NBR_FORMAT()
            {
                ID_VERSION_NBR_FORMAT = GetValue<int>(row["ID_VERSION_NBR_FORMAT"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
                ID_DATA_TYPE_CLASS = GetValue<int>(row["ID_DATA_TYPE_CLASS"]),
                REGULAR_EXPRESSION = GetValue<string>(row["REGULAR_EXPRESSION"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_NBR_FORMAT[] list = new DB_VERSION_NBR_FORMAT[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_NBR_FORMAT insert = new DB_VERSION_NBR_FORMAT();
									insert.ID_VERSION_NBR_FORMAT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_NBR_FORMAT"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
												insert.ID_DATA_TYPE_CLASS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE_CLASS"]);
												insert.REGULAR_EXPRESSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["REGULAR_EXPRESSION"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionNbrFormatFilter

        public DB_VERSION_NBR_FORMAT[] GetVersionNbrFormatFilter(int[] IdVersionNbrFormat, string Name, long[] IdDescr, int[] IdDataTypeClass, string RegularExpression, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionNbrFormatFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionNbrFormat != null && IdVersionNbrFormat.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType) ||
                (IdDataTypeClass != null && IdDataTypeClass.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionNbrFormat != null) args.AddRange(IdVersionNbrFormat.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));
                if (IdDataTypeClass != null) args.AddRange(IdDataTypeClass.Select(s => new Tuple<int, object>(4, s)));

                DB_VERSION_NBR_FORMAT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_NBR_FORMAT[])DB.ExecuteProcedure(
                        "imrse_GetVersionNbrFormatFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionNbrFormat),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@REGULAR_EXPRESSION", RegularExpression),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_NBR_FORMAT[])DB.ExecuteProcedure(
                    "imrse_GetVersionNbrFormatFilter",
                    new DB.AnalyzeDataSet(GetVersionNbrFormat),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_NBR_FORMAT", IdVersionNbrFormat),
				new DB.InParameter("@NAME", Name),
				CreateTableParam("@ID_DESCR", IdDescr),
				CreateTableParam("@ID_DATA_TYPE_CLASS", IdDataTypeClass),
				new DB.InParameter("@REGULAR_EXPRESSION", RegularExpression),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionNbrFormat(DB_VERSION_NBR_FORMAT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_NBR_FORMAT", ToBeSaved.ID_VERSION_NBR_FORMAT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionNbrFormat",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
														,new DB.InParameter("@ID_DATA_TYPE_CLASS", ToBeSaved.ID_DATA_TYPE_CLASS)
														,new DB.InParameter("@REGULAR_EXPRESSION", ToBeSaved.REGULAR_EXPRESSION)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_NBR_FORMAT = (int)InsertId.Value;

            return (int)InsertId.Value;
        }
        
        public void DeleteVersionNbrFormat(DB_VERSION_NBR_FORMAT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionNbrFormat",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_VERSION_NBR_FORMAT", toBeDeleted.ID_VERSION_NBR_FORMAT)			
		        }
            );
        }

        #endregion
        #region Table VERSION_REQUIREMENT

        public DB_VERSION_REQUIREMENT[] GetVersionRequirement(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_REQUIREMENT[])DB.ExecuteProcedure(
                "imrse_GetVersionRequirement",
                new DB.AnalyzeDataSet(GetVersionRequirement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_REQUIREMENT", new long[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_REQUIREMENT[] GetVersionRequirement(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_REQUIREMENT[])DB.ExecuteProcedure(
                "imrse_GetVersionRequirement",
                new DB.AnalyzeDataSet(GetVersionRequirement),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_REQUIREMENT", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionRequirement(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_REQUIREMENT()
            {
                ID_VERSION_REQUIREMENT = GetValue<long>(row["ID_VERSION_REQUIREMENT"]),
                ID_VERSION = GetValue<long>(row["ID_VERSION"]),
                ID_VERSION_REQUIRED = GetValue<long>(row["ID_VERSION_REQUIRED"]),
                ID_OPERATOR = GetValue<int>(row["ID_OPERATOR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_REQUIREMENT[] list = new DB_VERSION_REQUIREMENT[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_REQUIREMENT insert = new DB_VERSION_REQUIREMENT();
									insert.ID_VERSION_REQUIREMENT = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_REQUIREMENT"]);
												insert.ID_VERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION"]);
												insert.ID_VERSION_REQUIRED = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VERSION_REQUIRED"]);
												insert.ID_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OPERATOR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionRequirementFilter

        public DB_VERSION_REQUIREMENT[] GetVersionRequirementFilter(long[] IdVersionRequirement, long[] IdVersion, long[] IdVersionRequired, int[] IdOperator, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionRequirementFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionRequirement != null && IdVersionRequirement.Length > MaxItemsPerSqlTableType) ||
                (IdVersion != null && IdVersion.Length > MaxItemsPerSqlTableType) ||
                (IdVersionRequired != null && IdVersionRequired.Length > MaxItemsPerSqlTableType) ||
                (IdOperator != null && IdOperator.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionRequirement != null) args.AddRange(IdVersionRequirement.Select(s => new Tuple<int, object>(1, s)));
                if (IdVersion != null) args.AddRange(IdVersion.Select(s => new Tuple<int, object>(2, s)));
                if (IdVersionRequired != null) args.AddRange(IdVersionRequired.Select(s => new Tuple<int, object>(3, s)));
                if (IdOperator != null) args.AddRange(IdOperator.Select(s => new Tuple<int, object>(4, s)));

                DB_VERSION_REQUIREMENT[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_REQUIREMENT[])DB.ExecuteProcedure(
                        "imrse_GetVersionRequirementFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionRequirement),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_REQUIREMENT[])DB.ExecuteProcedure(
                    "imrse_GetVersionRequirementFilter",
                    new DB.AnalyzeDataSet(GetVersionRequirement),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_REQUIREMENT", IdVersionRequirement),
				CreateTableParam("@ID_VERSION", IdVersion),
				CreateTableParam("@ID_VERSION_REQUIRED", IdVersionRequired),
				CreateTableParam("@ID_OPERATOR", IdOperator),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public long SaveVersionRequirement(DB_VERSION_REQUIREMENT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_REQUIREMENT", ToBeSaved.ID_VERSION_REQUIREMENT);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionRequirement",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_VERSION", ToBeSaved.ID_VERSION)
														,new DB.InParameter("@ID_VERSION_REQUIRED", ToBeSaved.ID_VERSION_REQUIRED)
														,new DB.InParameter("@ID_OPERATOR", ToBeSaved.ID_OPERATOR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_REQUIREMENT = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteVersionRequirement(DB_VERSION_REQUIREMENT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionRequirement",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_REQUIREMENT", toBeDeleted.ID_VERSION_REQUIREMENT)			
		}
            );
        }

        #endregion
        #region Table VERSION_STATE

        public DB_VERSION_STATE[] GetVersionState(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_STATE[])DB.ExecuteProcedure(
                "imrse_GetVersionState",
                new DB.AnalyzeDataSet(GetVersionState),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_STATE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_STATE[] GetVersionState(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_STATE[])DB.ExecuteProcedure(
                "imrse_GetVersionState",
                new DB.AnalyzeDataSet(GetVersionState),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_STATE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetVersionState(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_STATE()
            {
                ID_VERSION_STATE = GetValue<int>(row["ID_VERSION_STATE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_STATE[] list = new DB_VERSION_STATE[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_STATE insert = new DB_VERSION_STATE();
									insert.ID_VERSION_STATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_STATE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }

        #region GetVersionStateFilter

        public DB_VERSION_STATE[] GetVersionStateFilter(int[] IdVersionState, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionStateFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionState != null && IdVersionState.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionState != null) args.AddRange(IdVersionState.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_VERSION_STATE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_STATE[])DB.ExecuteProcedure(
                        "imrse_GetVersionStateFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionState),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_STATE[])DB.ExecuteProcedure(
                    "imrse_GetVersionStateFilter",
                    new DB.AnalyzeDataSet(GetVersionState),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_STATE", IdVersionState),
				new DB.InParameter("@NAME", Name),
				CreateTableParam("@ID_DESCR", IdDescr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionState(DB_VERSION_STATE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_STATE", ToBeSaved.ID_VERSION_STATE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionState",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_STATE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteVersionState(DB_VERSION_STATE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionState",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VERSION_STATE", toBeDeleted.ID_VERSION_STATE)			
		    });
        }
        #endregion
        #region Table VERSION_TYPE

        public DB_VERSION_TYPE[] GetVersionType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetVersionType",
                new DB.AnalyzeDataSet(GetVersionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_TYPE", new int[] {})
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VERSION_TYPE[] GetVersionType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VERSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetVersionType",
                new DB.AnalyzeDataSet(GetVersionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_VERSION_TYPE", Ids)
					},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetVersionType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VERSION_TYPE()
            {
                ID_VERSION_TYPE = GetValue<int>(row["ID_VERSION_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
    int count = QueryResult.Tables[0].Rows.Count;
    DB_VERSION_TYPE[] list = new DB_VERSION_TYPE[count];
    for (int i = 0; i < count; i++)
    {
        DB_VERSION_TYPE insert = new DB_VERSION_TYPE();
									insert.ID_VERSION_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VERSION_TYPE"]);
												insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
												insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
							list[i] = insert;
    }
    return list;
	*/
            #endregion
        }        

        #region GetVersionTypeFilter

        public DB_VERSION_TYPE[] GetVersionTypeFilter(int[] IdVersionType, string Name, long[] IdDescr, long? topCount, string customWhereClause, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0
        )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionTypeFilter"));

            if (UseBulkInsertSqlTableType && (
                (IdVersionType != null && IdVersionType.Length > MaxItemsPerSqlTableType) ||
                (IdDescr != null && IdDescr.Length > MaxItemsPerSqlTableType)
                ))
            {
                long id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

                List<Tuple<int, object>> args = new List<Tuple<int, object>>();
                if (IdVersionType != null) args.AddRange(IdVersionType.Select(s => new Tuple<int, object>(1, s)));
                if (IdDescr != null) args.AddRange(IdDescr.Select(s => new Tuple<int, object>(3, s)));

                DB_VERSION_TYPE[] result = null;
                try
                {
                    InsertImrseProcArg(id, args);
                    result = (DB_VERSION_TYPE[])DB.ExecuteProcedure(
                        "imrse_GetVersionTypeFilterTableArgs",
                        new DB.AnalyzeDataSet(GetVersionType),
                        new DB.Parameter[] { 
					new DB.InParameter("@ID", SqlDbType.BigInt, id),
					new DB.InParameter("@NAME", Name),
					new DB.InParameter("@TOP_COUNT", topCount),
					new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                        autoTransaction, transactionLevel, commandTimeout
                    );
                }
                catch (Exception ex)
                {
                    DeleteImrseProcArg(id);
                    throw ex;
                }
                // Usuniecie wierszy z IMR_PROC_ARGS wykonuje sie w sp imrse_XXXFilterTableArgs oraz w przypadku Exception
                //finally
                //{
                //	DeleteImrseProcArg(id);
                //}
                return result;
            }
            else
            {
                return (DB_VERSION_TYPE[])DB.ExecuteProcedure(
                    "imrse_GetVersionTypeFilter",
                    new DB.AnalyzeDataSet(GetVersionType),
                    new DB.Parameter[] { 
				CreateTableParam("@ID_VERSION_TYPE", IdVersionType),
				new DB.InParameter("@NAME", Name),
				CreateTableParam("@ID_DESCR", IdDescr),
				new DB.InParameter("@TOP_COUNT", topCount),
				new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, 4000)},
                    autoTransaction, transactionLevel, commandTimeout
                );
            }
        }
        #endregion

        public int SaveVersionType(DB_VERSION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VERSION_TYPE", ToBeSaved.ID_VERSION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveVersionType",
                new DB.Parameter[] {
			        InsertId
										        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
														        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VERSION_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteVersionType(DB_VERSION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelVersionType",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_VERSION_TYPE", toBeDeleted.ID_VERSION_TYPE)			
		        }
            );
        }
        #endregion
        
        #region Table VIEW

        public DB_VIEW[] GetView(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW[])DB.ExecuteProcedure(
                "imrse_GetView",
                new DB.AnalyzeDataSet(GetView),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW[] GetView(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW[])DB.ExecuteProcedure(
                "imrse_GetView",
                new DB.AnalyzeDataSet(GetView),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetView(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW()
            {
                ID_VIEW = GetValue<int>(row["ID_VIEW"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_REFERENCE_TYPE = GetValue<int>(row["ID_REFERENCE_TYPE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW[] list = new DB_VIEW[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW insert = new DB_VIEW();
                                            insert.ID_VIEW = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.ID_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }

        public int SaveView(DB_VIEW ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW", ToBeSaved.ID_VIEW);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveView",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteView(DB_VIEW toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelView",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW", toBeDeleted.ID_VIEW)
                }
            );
        }

        #endregion
        #region Table VIEW_COLUMN

        public DB_VIEW_COLUMN[] GetViewColumn(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN[])DB.ExecuteProcedure(
                "imrse_GetViewColumn",
                new DB.AnalyzeDataSet(GetViewColumn),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW_COLUMN[] GetViewColumn(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN[])DB.ExecuteProcedure(
                "imrse_GetViewColumn",
                new DB.AnalyzeDataSet(GetViewColumn),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetViewColumn(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW_COLUMN()
            {
                ID_VIEW_COLUMN = GetValue<int>(row["ID_VIEW_COLUMN"]),
                ID_VIEW = GetValue<int>(row["ID_VIEW"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                TABLE = GetValue<string>(row["TABLE"]),
                PRIMARY_KEY = GetValue<bool>(row["PRIMARY_KEY"]),
                DEFAULT_VALUE = GetValue(row["DEFAULT_VALUE"]),
                SIZE = GetNullableValue<long>(row["SIZE"]),
                ID_VIEW_COLUMN_SOURCE = GetValue<int>(row["ID_VIEW_COLUMN_SOURCE"]),
                ID_VIEW_COLUMN_TYPE = GetValue<int>(row["ID_VIEW_COLUMN_TYPE"]),
                KEY = GetValue<bool>(row["KEY"]),
                ID_KEY_VIEW_COLUMN = GetNullableValue<int>(row["ID_KEY_VIEW_COLUMN"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW_COLUMN[] list = new DB_VIEW_COLUMN[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW_COLUMN insert = new DB_VIEW_COLUMN();
                                            insert.ID_VIEW_COLUMN = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN"]);
                                                        insert.ID_VIEW = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.TABLE = GetValue<string>(QueryResult.Tables[0].Rows[i]["TABLE"]);
                                                        insert.PRIMARY_KEY = GetValue<bool>(QueryResult.Tables[0].Rows[i]["PRIMARY_KEY"]);
                                                        insert.DEFAULT_VALUE = GetValue(QueryResult.Tables[0].Rows[i]["DEFAULT_VALUE"]);
                                                        insert.SIZE = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["SIZE"]);
                                                        insert.ID_VIEW_COLUMN_SOURCE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN_SOURCE"]);
                                                        insert.ID_VIEW_COLUMN_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN_TYPE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }


        public int SaveViewColumn(DB_VIEW_COLUMN ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW_COLUMN", ToBeSaved.ID_VIEW_COLUMN);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveViewColumn",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_VIEW", ToBeSaved.ID_VIEW)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@TABLE", ToBeSaved.TABLE)
                                                        ,new DB.InParameter("@PRIMARY_KEY", ToBeSaved.PRIMARY_KEY)
                                                        ,ParamObject("@DEFAULT_VALUE", ToBeSaved.DEFAULT_VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                                    ,new DB.InParameter("@SIZE", ToBeSaved.SIZE)
                                                        ,new DB.InParameter("@ID_VIEW_COLUMN_SOURCE", ToBeSaved.ID_VIEW_COLUMN_SOURCE)
                                                        ,new DB.InParameter("@ID_VIEW_COLUMN_TYPE", ToBeSaved.ID_VIEW_COLUMN_TYPE)
                                                        ,new DB.InParameter("@KEY", ToBeSaved.KEY)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW_COLUMN = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteViewColumn(DB_VIEW_COLUMN toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelViewColumn",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW_COLUMN", toBeDeleted.ID_VIEW_COLUMN)
                }
            );
        }

        #endregion
        #region Table VIEW_COLUMN_DATA

        public DB_VIEW_COLUMN_DATA[] GetViewColumnData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_DATA[])DB.ExecuteProcedure(
                "imrse_GetViewColumnData",
                new DB.AnalyzeDataSet(GetViewColumnData),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_DATA", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW_COLUMN_DATA[] GetViewColumnData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_DATA[])DB.ExecuteProcedure(
                "imrse_GetViewColumnData",
                new DB.AnalyzeDataSet(GetViewColumnData),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_DATA", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetViewColumnData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW_COLUMN_DATA()
            {
                ID_VIEW_COLUMN_DATA = GetValue<long>(row["ID_VIEW_COLUMN_DATA"]),
                ID_VIEW_COLUMN = GetValue<int>(row["ID_VIEW_COLUMN"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW_COLUMN_DATA[] list = new DB_VIEW_COLUMN_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW_COLUMN_DATA insert = new DB_VIEW_COLUMN_DATA();
                                            insert.ID_VIEW_COLUMN_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN_DATA"]);
                                                        insert.ID_VIEW_COLUMN = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }


        public long SaveViewColumnData(DB_VIEW_COLUMN_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW_COLUMN_DATA", ToBeSaved.ID_VIEW_COLUMN_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveViewColumnData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_VIEW_COLUMN", ToBeSaved.ID_VIEW_COLUMN)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW_COLUMN_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteViewColumnData(DB_VIEW_COLUMN_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelViewColumnData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW_COLUMN_DATA", toBeDeleted.ID_VIEW_COLUMN_DATA)
                }
            );
        }

        #endregion
        #region Table VIEW_COLUMN_SOURCE

        public DB_VIEW_COLUMN_SOURCE[] GetViewColumnSource(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetViewColumnSource",
                new DB.AnalyzeDataSet(GetViewColumnSource),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_SOURCE", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW_COLUMN_SOURCE[] GetViewColumnSource(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_SOURCE[])DB.ExecuteProcedure(
                "imrse_GetViewColumnSource",
                new DB.AnalyzeDataSet(GetViewColumnSource),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_SOURCE", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetViewColumnSource(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW_COLUMN_SOURCE()
            {
                ID_VIEW_COLUMN_SOURCE = GetValue<int>(row["ID_VIEW_COLUMN_SOURCE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW_COLUMN_SOURCE[] list = new DB_VIEW_COLUMN_SOURCE[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW_COLUMN_SOURCE insert = new DB_VIEW_COLUMN_SOURCE();
                                            insert.ID_VIEW_COLUMN_SOURCE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN_SOURCE"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }


        public int SaveViewColumnSource(DB_VIEW_COLUMN_SOURCE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW_COLUMN_SOURCE", ToBeSaved.ID_VIEW_COLUMN_SOURCE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveViewColumnSource",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW_COLUMN_SOURCE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteViewColumnSource(DB_VIEW_COLUMN_SOURCE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelViewColumnSource",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW_COLUMN_SOURCE", toBeDeleted.ID_VIEW_COLUMN_SOURCE)
                }
            );
        }

        #endregion
        #region Table VIEW_COLUMN_TYPE

        public DB_VIEW_COLUMN_TYPE[] GetViewColumnType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_TYPE[])DB.ExecuteProcedure(
                "imrse_GetViewColumnType",
                new DB.AnalyzeDataSet(GetViewColumnType),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_TYPE", new int[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW_COLUMN_TYPE[] GetViewColumnType(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_COLUMN_TYPE[])DB.ExecuteProcedure(
                "imrse_GetViewColumnType",
                new DB.AnalyzeDataSet(GetViewColumnType),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_COLUMN_TYPE", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetViewColumnType(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW_COLUMN_TYPE()
            {
                ID_VIEW_COLUMN_TYPE = GetValue<int>(row["ID_VIEW_COLUMN_TYPE"]),
                NAME = GetValue<string>(row["NAME"]),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW_COLUMN_TYPE[] list = new DB_VIEW_COLUMN_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW_COLUMN_TYPE insert = new DB_VIEW_COLUMN_TYPE();
                                            insert.ID_VIEW_COLUMN_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW_COLUMN_TYPE"]);
                                                        insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                                                        insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }


        public int SaveViewColumnType(DB_VIEW_COLUMN_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW_COLUMN_TYPE", ToBeSaved.ID_VIEW_COLUMN_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveViewColumnType",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@NAME", ToBeSaved.NAME)
                                                        ,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                                            }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW_COLUMN_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteViewColumnType(DB_VIEW_COLUMN_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelViewColumnType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW_COLUMN_TYPE", toBeDeleted.ID_VIEW_COLUMN_TYPE)
                }
            );
        }

        #endregion
        #region Table VIEW_DATA

        public DB_VIEW_DATA[] GetViewData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_DATA[])DB.ExecuteProcedure(
                "imrse_GetViewData",
                new DB.AnalyzeDataSet(GetViewData),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_DATA", new long[] {})
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_VIEW_DATA[] GetViewData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_VIEW_DATA[])DB.ExecuteProcedure(
                "imrse_GetViewData",
                new DB.AnalyzeDataSet(GetViewData),
                new DB.Parameter[] {
            CreateTableParam("@ID_VIEW_DATA", Ids)
                            },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetViewData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_VIEW_DATA()
            {
                ID_VIEW_DATA = GetValue<long>(row["ID_VIEW_DATA"]),
                ID_VIEW = GetValue<int>(row["ID_VIEW"]),
                ID_DATA_TYPE = GetValue<long>(row["ID_DATA_TYPE"]),
                INDEX_NBR = GetValue<int>(row["INDEX_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
            #region Wersja z pętlą
            /*
            int count = QueryResult.Tables[0].Rows.Count;
            DB_VIEW_DATA[] list = new DB_VIEW_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_VIEW_DATA insert = new DB_VIEW_DATA();
                                            insert.ID_VIEW_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_VIEW_DATA"]);
                                                        insert.ID_VIEW = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_VIEW"]);
                                                        insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                                                        insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                                                        insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                                    list[i] = insert;
            }
            return list;
            */
            #endregion
        }


        public long SaveViewData(DB_VIEW_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_VIEW_DATA", ToBeSaved.ID_VIEW_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveViewData",
                new DB.Parameter[] {
            InsertId
                                        ,new DB.InParameter("@ID_VIEW", ToBeSaved.ID_VIEW)
                                                        ,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
                                                        ,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
                                                        ,ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
                                        }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_VIEW_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }


        public void DeleteViewData(DB_VIEW_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelViewData",
                new DB.Parameter[] {
            new DB.InParameter("@ID_VIEW_DATA", toBeDeleted.ID_VIEW_DATA)
                }
            );
        }

        #endregion

        #region Table WORK
        private object GetWork(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_WORK()
            {
                ID_WORK = GetValue<long>(row["ID_WORK"]),
                ID_WORK_TYPE = GetValue<long>(row["ID_WORK_TYPE"]),
                ID_WORK_STATUS = GetValue<int>(row["ID_WORK_STATUS"]),
                ID_WORK_DATA = GetValue<long>(row["ID_WORK_DATA"]),
                ID_WORK_PARENT = GetNullableValue<long>(row["ID_WORK_PARENT"]),
                ID_MODULE = GetNullableValue<int>(row["ID_MODULE"]),
                ID_OPERATOR = GetNullableValue<int>(row["ID_OPERATOR"]),
                CREATION_DATE = ConvertUtcTimeToTimeZone(GetValue<DateTime>(row["CREATION_DATE"])),
            }).ToArray();
        }

        public void DeleteWork(DB_WORK toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWork",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_WORK", toBeDeleted.ID_WORK)
                }
            );
        }
        #endregion
        #region Table WORK_DATA
        public DB_WORK_DATA[] GetWorkData()
        {
            return (DB_WORK_DATA[])DB.ExecuteProcedure(
                "imrse_GetWorkData",
                new DB.AnalyzeDataSet(GetWorkData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_DATA", new long[] {})
                 }
            );
        }

        public DB_WORK_DATA[] GetWorkData(long[] Ids)
        {
            return (DB_WORK_DATA[])DB.ExecuteProcedure(
                "imrse_GetWorknData",
                new DB.AnalyzeDataSet(GetWorkData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_DATA", Ids)
                }
            );
        }

        private object GetWorkData(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WORK_DATA[] list = new DB_WORK_DATA[count];
            for (int i = 0; i < count; i++)
            {
                DB_WORK_DATA insert = new DB_WORK_DATA();
                insert.ID_WORK_DATA = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_WORK_DATA"]);
                insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
                insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveWorkData(DB_WORK_DATA ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_DATA", ToBeSaved.ID_WORK_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkData",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE),
                    new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR),
                    ParamObject("@VALUE", ToBeSaved.VALUE, ToBeSaved.ID_DATA_TYPE, ToBeSavedType == null ? 0 : ToBeSavedType.ID_DATA_TYPE_CLASS)
            });
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteWorkData(DB_WORK_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkData",
                new DB.Parameter[] {
                new DB.InParameter("@ID_WORK_DATA", toBeDeleted.ID_WORK_DATA),
                new DB.InParameter("@ID_DATA_TYPE", toBeDeleted.ID_DATA_TYPE),
                new DB.InParameter("@INDEX_NBR", toBeDeleted.INDEX_NBR)
            });
        }
        #endregion
        #region Table WORK_HISTORY
        public DB_WORK_HISTORY[] GetWorkHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetWorkHistory",
                new DB.AnalyzeDataSet(GetWorkHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_HISTORY", new long[] {})},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }
        public DB_WORK_HISTORY[] GetWorkHistory(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_HISTORY[])DB.ExecuteProcedure(
                "imrse_GetWorkHistory",
                new DB.AnalyzeDataSet(GetWorkHistory),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_HISTORY", Ids)},
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }
        private object GetWorkHistory(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_WORK_HISTORY()
            {
                ID_WORK_HISTORY = GetValue<long>(row["ID_WORK_HISTORY"]),
                ID_WORK = GetValue<long>(row["ID_WORK"]),
                ID_WORK_STATUS = GetValue<int>(row["ID_WORK_STATUS"]),
                START_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["START_TIME"])),
                END_TIME = ConvertUtcTimeToTimeZone(GetNullableValue<DateTime>(row["END_TIME"])),
                ID_DESCR = GetNullableValue<long>(row["ID_DESCR"]),
                ID_MODULE = GetNullableValue<int>(row["ID_MODULE"]),
                ASSEMBLY = GetValue<string>(row["ASSEMBLY"]),
            }).ToArray();
        }
        public long SaveWorkHistory(DB_WORK_HISTORY ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_HISTORY", ToBeSaved.ID_WORK_HISTORY);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkHistory",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@ID_WORK", ToBeSaved.ID_WORK),
                    new DB.InParameter("@ID_WORK_STATUS", ToBeSaved.ID_WORK_STATUS),
                    new DB.InParameter("@START_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.START_TIME)),
                    new DB.InParameter("@END_TIME", ConvertTimeZoneToUtcTime(ToBeSaved.END_TIME)),
                    new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR),
                    new DB.InParameter("@ID_MODULE", ToBeSaved.ID_MODULE),
                    new DB.InParameter("@ASSEMBLY", ToBeSaved.ASSEMBLY)
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_HISTORY = (long)InsertId.Value;

            return (long)InsertId.Value;
        }
        public void DeleteWorkHistory(DB_WORK_HISTORY toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkHistory",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_WORK_HISTORY", toBeDeleted.ID_WORK_HISTORY)
                }
            );
        }
        #endregion
        #region Table WORK_HISTORY_DATA
        public DB_WORK_HISTORY_DATA[] GetWorkHistoryData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_HISTORY_DATA[])DB.ExecuteProcedure(
                "imrse_GetWorkHistoryData",
                new DB.AnalyzeDataSet(GetWorkHistoryData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_HISTORY_DATA", new long[] {})
                },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK_HISTORY_DATA[] GetWorkHistoryData(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_HISTORY_DATA[])DB.ExecuteProcedure(
                "imrse_GetWorkHistoryData",
                new DB.AnalyzeDataSet(GetWorkHistoryData),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_HISTORY_DATA", Ids)
                },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }
        private object GetWorkHistoryData(DataSet QueryResult)
        {
            return QueryResult.Tables[0].Rows.OfType<DataRow>().AsEnumerable().Select(row => new DB_WORK_HISTORY_DATA()
            {
                ID_WORK_HISTORY_DATA = GetValue<long>(row["ID_WORK_HISTORY_DATA"]),
                ID_WORK_HISTORY = GetValue<long>(row["ID_WORK_HISTORY"]),
                ARG_NBR = GetValue<int>(row["ARG_NBR"]),
                VALUE = GetValue(row["VALUE"]),
            }).ToArray();
        }
        public long SaveWorkHistoryData(DB_WORK_HISTORY_DATA ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_HISTORY_DATA", ToBeSaved.ID_WORK_HISTORY_DATA);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkHistoryData",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@ID_WORK_HISTORY", ToBeSaved.ID_WORK_HISTORY),
                    new DB.InParameter("@ARG_NBR", ToBeSaved.ARG_NBR),
                    ParamObject("@VALUE", ToBeSaved.VALUE, 0, 0)
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_HISTORY_DATA = (long)InsertId.Value;

            return (long)InsertId.Value;
        }
        public void DeleteWorkHistoryData(DB_WORK_HISTORY_DATA toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkHistoryData",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_WORK_HISTORY_DATA", toBeDeleted.ID_WORK_HISTORY_DATA)
                }
            );
        }
        #endregion
        #region Table WORK_STATUS
        public DB_WORK_STATUS[] GetWorkStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetWorkStatus",
                new DB.AnalyzeDataSet(GetWorkStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_STATUS", new int[] {})
                },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK_STATUS[] GetWorkStatus(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_STATUS[])DB.ExecuteProcedure(
                "imrse_GetWorkStatus",
                new DB.AnalyzeDataSet(GetWorkStatus),
                new DB.Parameter[] {
                    CreateTableParam("@ID_WORK_STATUS", Ids)
                },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetWorkStatus(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WORK_STATUS[] list = new DB_WORK_STATUS[count];
            for (int i = 0; i < count; i++)
            {
                DB_WORK_STATUS insert = new DB_WORK_STATUS();
                insert.ID_WORK_STATUS = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_WORK_STATUS"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveWorkStatus(DB_WORK_STATUS ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_STATUS", ToBeSaved.ID_WORK_STATUS);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkStatus",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@NAME", ToBeSaved.NAME),
                    new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_STATUS = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteWorkStatus(DB_WORK_STATUS toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkStatus",
                new DB.Parameter[] {
                    new DB.InParameter("@ID_WORK_STATUS", toBeDeleted.ID_WORK_STATUS)
                }
            );
        }
        #endregion
        #region Table WORK_TYPE
        public DB_WORK_TYPE[] GetWorkType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWorkType",
                new DB.AnalyzeDataSet(GetWorkType),
                new DB.Parameter[] {
            CreateTableParam("@ID_WORK_TYPE", new int[] {})
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK_TYPE[] GetWorkType(long[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWorkType",
                new DB.AnalyzeDataSet(GetWorkType),
                new DB.Parameter[] {
            CreateTableParam("@ID_WORK_TYPE", Ids)
                    },
                AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetWorkType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WORK_TYPE[] list = new DB_WORK_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_WORK_TYPE insert = new DB_WORK_TYPE();
                insert.ID_WORK_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_WORK_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.PLUGIN_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["PLUGIN_NAME"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveWorkType(DB_WORK_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_TYPE", ToBeSaved.ID_WORK_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkType",
                new DB.Parameter[] {
                    InsertId,
                    new DB.InParameter("@NAME", ToBeSaved.NAME),
                    new DB.InParameter("@PLUGIN_NAME", ToBeSaved.PLUGIN_NAME)
                }
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_TYPE = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteWorkType(DB_WORK_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_WORK_TYPE", toBeDeleted.ID_WORK_TYPE)
        }
            );
        }
        #endregion

        #region Table WORK_ORDER_JCI

        public DB_WORK_ORDER_JCI[] GetWorkOrderJci(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_ORDER_JCI[])DB.ExecuteProcedure(
                "imrse_GetWorkOrderJci",
                new DB.AnalyzeDataSet(GetWorkOrderJci),
                new DB.Parameter[] { 
			CreateTableParam("@ID_WORK_ORDER_JCI", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WORK_ORDER_JCI[] GetWorkOrderJci(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WORK_ORDER_JCI[])DB.ExecuteProcedure(
                "imrse_GetWorkOrderJci",
                new DB.AnalyzeDataSet(GetWorkOrderJci),
                new DB.Parameter[] { 
			CreateTableParam("@ID_WORK_ORDER_JCI", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        private object GetWorkOrderJci(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WORK_ORDER_JCI[] list = new DB_WORK_ORDER_JCI[count];
            for (int i = 0; i < count; i++)
            {
                DB_WORK_ORDER_JCI insert = new DB_WORK_ORDER_JCI();
                insert.ID_WORK_ORDER_JCI = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_WORK_ORDER_JCI"]);
                insert.WONUM = GetValue<string>(QueryResult.Tables[0].Rows[i]["WONUM"]);
                insert.SENTDATETIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["SENTDATETIME"]));
                insert.WODESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["WODESC"]);
                insert.IVRCODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["IVRCODE"]);
                insert.SUBCASDESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["SUBCASDESC"]);
                insert.PROBLEMDESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["PROBLEMDESC"]);
                insert.PRIORITY = GetValue<int>(QueryResult.Tables[0].Rows[i]["PRIORITY"]);
                insert.TYPEDESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["TYPEDESC"]);
                insert.STATUSDESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["STATUSDESC"]);
                insert.REPORTDATETIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["REPORTDATETIME"]));
                insert.RFDATETIME = ConvertUtcTimeToTimeZone(GetValue<DateTime>(QueryResult.Tables[0].Rows[i]["RFDATETIME"]));
                insert.PONUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["PONUMBER"]);
                insert.SUPPLIERID = GetValue<string>(QueryResult.Tables[0].Rows[i]["SUPPLIERID"]);
                insert.SUPPLIERNAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["SUPPLIERNAME"]);
                insert.SITEID = GetValue<string>(QueryResult.Tables[0].Rows[i]["SITEID"]);
                insert.SITENAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["SITENAME"]);
                insert.SITEPHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["SITEPHONE"]);
                insert.EQUIPID = GetValue<string>(QueryResult.Tables[0].Rows[i]["EQUIPID"]);
                insert.EQUIPDESC = GetValue<string>(QueryResult.Tables[0].Rows[i]["EQUIPDESC"]);
                insert.DOWNTIME = GetValue<string>(QueryResult.Tables[0].Rows[i]["DOWNTIME"]);
                insert.LUMPSUM = GetValue<string>(QueryResult.Tables[0].Rows[i]["LUMPSUM"]);
                insert.DISCLAIMER_TEXT = GetValue<string>(QueryResult.Tables[0].Rows[i]["DISCLAIMER_TEXT"]);
                list[i] = insert;
            }
            return list;
        }

        public int SaveWorkOrderJci(DB_WORK_ORDER_JCI ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WORK_ORDER_JCI", ToBeSaved.ID_WORK_ORDER_JCI);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWorkOrderJci",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@WONUM", ToBeSaved.WONUM)
														,new DB.InParameter("@SENTDATETIME", ConvertTimeZoneToUtcTime(ToBeSaved.SENTDATETIME))
														,new DB.InParameter("@WODESC", ToBeSaved.WODESC)
														,new DB.InParameter("@IVRCODE", ToBeSaved.IVRCODE)
														,new DB.InParameter("@SUBCASDESC", ToBeSaved.SUBCASDESC)
														,new DB.InParameter("@PROBLEMDESC", ToBeSaved.PROBLEMDESC)
														,new DB.InParameter("@PRIORITY", ToBeSaved.PRIORITY)
														,new DB.InParameter("@TYPEDESC", ToBeSaved.TYPEDESC)
														,new DB.InParameter("@STATUSDESC", ToBeSaved.STATUSDESC)
														,new DB.InParameter("@REPORTDATETIME", ConvertTimeZoneToUtcTime(ToBeSaved.REPORTDATETIME))
														,new DB.InParameter("@RFDATETIME", ConvertTimeZoneToUtcTime(ToBeSaved.RFDATETIME))
														,new DB.InParameter("@PONUMBER", ToBeSaved.PONUMBER)
														,new DB.InParameter("@SUPPLIERID", ToBeSaved.SUPPLIERID)
														,new DB.InParameter("@SUPPLIERNAME", ToBeSaved.SUPPLIERNAME)
														,new DB.InParameter("@SITEID", ToBeSaved.SITEID)
														,new DB.InParameter("@SITENAME", ToBeSaved.SITENAME)
														,new DB.InParameter("@SITEPHONE", ToBeSaved.SITEPHONE)
														,new DB.InParameter("@EQUIPID", ToBeSaved.EQUIPID)
														,new DB.InParameter("@EQUIPDESC", ToBeSaved.EQUIPDESC)
														,new DB.InParameter("@DOWNTIME", ToBeSaved.DOWNTIME)
														,new DB.InParameter("@LUMPSUM", ToBeSaved.LUMPSUM)
														,new DB.InParameter("@DISCLAIMER_TEXT", ToBeSaved.DISCLAIMER_TEXT)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WORK_ORDER_JCI = (int)InsertId.Value;

            return (int)InsertId.Value;
        }

        public void DeleteWorkOrderJci(DB_WORK_ORDER_JCI toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWorkOrderJci",
                new DB.Parameter[] {
            new DB.InParameter("@ID_WORK_ORDER_JCI", toBeDeleted.ID_WORK_ORDER_JCI)			
		}
            );
        }

        #endregion

        #region Table WARRANTY_CONTRACT

        public DB_WARRANTY_CONTRACT[] GetWarrantyContract(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WARRANTY_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetWarrantyContract",
                new DB.AnalyzeDataSet(GetWarrantyContract),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", new int[] {})
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }

        public DB_WARRANTY_CONTRACT[] GetWarrantyContract(int[] Ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return (DB_WARRANTY_CONTRACT[])DB.ExecuteProcedure(
                "imrse_GetWarrantyContract",
                new DB.AnalyzeDataSet(GetWarrantyContract),
                new DB.Parameter[] { 
			CreateTableParam("@ID_DEVICE_ORDER_NUMBER", Ids)
					}, AutoTransaction: autoTransaction, IsolationLevel: transactionLevel, CommandTimeout: commandTimeout
            );
        }



        private object GetWarrantyContract(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WARRANTY_CONTRACT[] list = new DB_WARRANTY_CONTRACT[count];
            for (int i = 0; i < count; i++)
            {
                DB_WARRANTY_CONTRACT insert = new DB_WARRANTY_CONTRACT();
                insert.ID_DEVICE_ORDER_NUMBER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE_ORDER_NUMBER"]);
                insert.ID_COMPONENT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_COMPONENT"]);
                insert.ID_CONTRACT = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONTRACT"]);
                insert.WARRANTY_TIME = GetValue<int>(QueryResult.Tables[0].Rows[i]["WARRANTY_TIME"]);
                insert.ID_WARRANTY_START_DATE_CALCULATION_METHOD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_WARRANTY_START_DATE_CALCULATION_METHOD"]);
                list[i] = insert;
            }
            return list;
        }


        public int SaveWarrantyContract(DB_WARRANTY_CONTRACT ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_DEVICE_ORDER_NUMBER", ToBeSaved.ID_DEVICE_ORDER_NUMBER);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWarrantyContract",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_COMPONENT", ToBeSaved.ID_COMPONENT)
														,new DB.InParameter("@ID_CONTRACT", ToBeSaved.ID_CONTRACT)
														,new DB.InParameter("@WARRANTY_TIME", ToBeSaved.WARRANTY_TIME)
                                                        ,new DB.InParameter("@ID_WARRANTY_START_DATE_CALCULATION_METHOD", ToBeSaved.ID_WARRANTY_START_DATE_CALCULATION_METHOD)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_DEVICE_ORDER_NUMBER = (int)InsertId.Value;

            return (int)InsertId.Value;
        }


        public void DeleteWarrantyContract(DB_WARRANTY_CONTRACT toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWarrantyContract",
                new DB.Parameter[] {
            new DB.InParameter("@ID_DEVICE_ORDER_NUMBER", toBeDeleted.ID_DEVICE_ORDER_NUMBER)			
		}
            );
        }

        #endregion

        #region Table WMBUS_TRANSMISSION_TYPE

        public DB_WMBUS_TRANSMISSION_TYPE[] GetWmbusTransmissionType()
        {
            return (DB_WMBUS_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWmbusTransmissionType",
                new DB.AnalyzeDataSet(GetWmbusTransmissionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_WMBUS_TRANSMISSION_TYPE", new long[] {})
					}
            );
        }

        public DB_WMBUS_TRANSMISSION_TYPE[] GetWmbusTransmissionType(long[] Ids)
        {
            return (DB_WMBUS_TRANSMISSION_TYPE[])DB.ExecuteProcedure(
                "imrse_GetWmbusTransmissionType",
                new DB.AnalyzeDataSet(GetWmbusTransmissionType),
                new DB.Parameter[] { 
			CreateTableParam("@ID_WMBUS_TRANSMISSION_TYPE", Ids)
					}
            );
        }



        private object GetWmbusTransmissionType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            DB_WMBUS_TRANSMISSION_TYPE[] list = new DB_WMBUS_TRANSMISSION_TYPE[count];
            for (int i = 0; i < count; i++)
            {
                DB_WMBUS_TRANSMISSION_TYPE insert = new DB_WMBUS_TRANSMISSION_TYPE();
                insert.ID_WMBUS_TRANSMISSION_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_WMBUS_TRANSMISSION_TYPE"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.ID_DESCR = GetNullableValue<long>(QueryResult.Tables[0].Rows[i]["ID_DESCR"]);
                list[i] = insert;
            }
            return list;
        }

        public long SaveWmbusTransmissionType(DB_WMBUS_TRANSMISSION_TYPE ToBeSaved)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_WMBUS_TRANSMISSION_TYPE", ToBeSaved.ID_WMBUS_TRANSMISSION_TYPE);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveWmbusTransmissionType",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@NAME", ToBeSaved.NAME)
														,new DB.InParameter("@ID_DESCR", ToBeSaved.ID_DESCR)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_WMBUS_TRANSMISSION_TYPE = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteWmbusTransmissionType(DB_WMBUS_TRANSMISSION_TYPE toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelWmbusTransmissionType",
                new DB.Parameter[] {
            new DB.InParameter("@ID_WMBUS_TRANSMISSION_TYPE", toBeDeleted.ID_WMBUS_TRANSMISSION_TYPE)			
		}
            );
        }

        #endregion

        #region Table CONVERSION

        public DB_CONVERSION[] GetConversion()
        {
            return (DB_CONVERSION[])DB.ExecuteProcedure(
                "imrse_GetConversion",
                new DB.AnalyzeDataSet(GetConversion),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONVERSION", new long[] {})
					}
            );
        }

        public DB_CONVERSION[] GetConversion(long[] Ids)
        {
            return (DB_CONVERSION[])DB.ExecuteProcedure(
                "imrse_GetConversion",
                new DB.AnalyzeDataSet(GetConversion),
                new DB.Parameter[] { 
			CreateTableParam("@ID_CONVERSION", Ids)
					}
            );
        }

        private object GetConversion(DataSet QueryResult)
        {
           
            int count = QueryResult.Tables[0].Rows.Count;
            DB_CONVERSION[] list = new DB_CONVERSION[count];
            for (int i = 0; i < count; i++)
            {
                DB_CONVERSION insert = new DB_CONVERSION();
									insert.ID_CONVERSION = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_CONVERSION"]);
												insert.ID_REFERENCE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_REFERENCE_TYPE"]);
												insert.REFERENCE_VALUE = GetValue<long>(QueryResult.Tables[0].Rows[i]["REFERENCE_VALUE"]);
												insert.ID_DATA_TYPE = GetValue<long>(QueryResult.Tables[0].Rows[i]["ID_DATA_TYPE"]);
												insert.INDEX_NBR = GetValue<int>(QueryResult.Tables[0].Rows[i]["INDEX_NBR"]);
												insert.SLOPE = GetValue<Double>(QueryResult.Tables[0].Rows[i]["SLOPE"]);
												insert.BIAS = GetValue<Double>(QueryResult.Tables[0].Rows[i]["BIAS"]);
				list[i] = insert;
            }
            return list;   
        }

        public long SaveConversion(DB_CONVERSION ToBeSaved, DB_DATA_TYPE ToBeSavedType)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_CONVERSION", ToBeSaved.ID_CONVERSION);
            DB.ExecuteNonQueryProcedure(
                "imrse_SaveConversion",
                new DB.Parameter[] {
			InsertId
										,new DB.InParameter("@ID_REFERENCE_TYPE", ToBeSaved.ID_REFERENCE_TYPE)
														,new DB.InParameter("@REFERENCE_VALUE", ToBeSaved.REFERENCE_VALUE)
														,new DB.InParameter("@ID_DATA_TYPE", ToBeSaved.ID_DATA_TYPE)
														,new DB.InParameter("@INDEX_NBR", ToBeSaved.INDEX_NBR)
														,new DB.InParameter("@SLOPE", ToBeSaved.SLOPE)
														,new DB.InParameter("@BIAS", ToBeSaved.BIAS)
									}
            );
            if (!InsertId.IsNull)
                ToBeSaved.ID_CONVERSION = (long)InsertId.Value;

            return (long)InsertId.Value;
        }

        public void DeleteConversion(DB_CONVERSION toBeDeleted)
        {
            DB.ExecuteNonQueryProcedure(
                "imrse_DelConversion",
                    new DB.Parameter[] {
                        new DB.InParameter("@ID_CONVERSION", toBeDeleted.ID_CONVERSION)			
		            }
            );
        }
        #endregion

        #region GuidConversion
        private Guid? ConvertToNullableGuid(object objectValue)
        {
            if (objectValue == null || objectValue == DBNull.Value)
                return null;
            else
                return (Guid)objectValue;
        }

        private Guid ConvertToGuid(object objectValue)
        {
            if (objectValue == null || objectValue == DBNull.Value)
                return Guid.Empty;
            else
                return (Guid)objectValue;
        }
        #endregion
    }
}
