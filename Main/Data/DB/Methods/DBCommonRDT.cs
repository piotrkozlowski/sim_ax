﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using IMR.Suite.Data.DB.RDT;
using System.Globalization;

namespace IMR.Suite.Data.DB
{
    public class DBCommonRDT : DBCommon
    {
        #region Ctor
        public DBCommonRDT(string ServerName)
            : base(ServerName)
        {
        }

        public DBCommonRDT(string ServerName, int ShortTimeout, int LongTimeout)
            : base(ServerName, ShortTimeout, LongTimeout)
        {
        }

        public DBCommonRDT(string server, string db, string userEncrypted, string passwordEncrypted, int ShortTimeout, int LongTimeout)
            : base(server, db, userEncrypted, passwordEncrypted, ShortTimeout, LongTimeout, TimeSpan.FromSeconds(30), 2, TimeSpan.FromSeconds(5))
        {
        }
        #endregion

        public enum SimCardParamType
        {
            SentSmsNumber = 1,
            IncludedPinRequest = 2,
            SimCardName = 3,
            SimCardState = 4,
            GprsCard = 5,
            RoamingCard = 6,
        }

        #region GetDevicesInPackage
        public RDTNDeviceInPackage[] GetDevicesInPackage(string serialNbr)
        {
            if (serialNbr == null || serialNbr.Length > 16)
                throw new Exception("Serial number length has to be less or equal 16");
            return (RDTNDeviceInPackage[])DB.ExecuteProcedure(
                "tst_GetDevicesInPackageFromSerial",
                new DB.AnalyzeDataSet(GetDevicesInPackage),
                new DB.Parameter[] { 
                    new DB.InParameter("@SerialNbr", serialNbr)
                }
            );
        }

        private object GetDevicesInPackage(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceInPackage[] list = new RDTNDeviceInPackage[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceInPackage insert = new RDTNDeviceInPackage();
                insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.PACKAGE_SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["PACKAGE_SERIAL_NBR"]);
                insert.FINISHED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["FINISHED"]);
                insert.MAX_DEVICES_IN_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["MAX_DEVICES_IN_PACKAGE"]);
                insert.DEVICE_SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_SERIAL_NBR"]);
                insert.ID_DEVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE"]);
                insert.ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion      

        #region GetDeviceBatchNumber
        public RDTNDeviceBatchNumber[] GetDeviceBatchNumber(DateTime? referenceDate)
        {
            return (RDTNDeviceBatchNumber[])DB.ExecuteProcedure(
                "tst_GetDeviceBatchNumber",
                new DB.AnalyzeDataSet(GetDeviceBatchNumber),
                new DB.Parameter[] { 
                    new DB.InParameter("@REFERENCE_DATE", referenceDate)
                }
            );
        }

        private object GetDeviceBatchNumber(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceBatchNumber[] list = new RDTNDeviceBatchNumber[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceBatchNumber insert = new RDTNDeviceBatchNumber();
                insert.SerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.BatchNumber = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE_NBR"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion  
    
        
        #region GetManufacturingDeviceBatchNumber
        public RDTNDeviceBatchNumber[] GetManufacturingDeviceBatchNumber()
        {
            return (RDTNDeviceBatchNumber[])DB.ExecuteProcedure(
                "tst_GetManufacturingBatchNumber",
                new DB.AnalyzeDataSet(GetManufacturingDeviceBatchNumber),
                new DB.Parameter[] { 
                }
            );
        }

        private object GetManufacturingDeviceBatchNumber(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceBatchNumber[] list = new RDTNDeviceBatchNumber[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceBatchNumber insert = new RDTNDeviceBatchNumber();
                insert.SerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.BatchNumber = GetValue<string>(QueryResult.Tables[0].Rows[i]["CODE_NBR"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion  

        #region GetDeviceProductionOrderNumber
        public RDTNDeviceOrderNumber[] GetDeviceProductionOrderNumber(List<string> SerialNbr)
        {
            string serialNbrList = String.Join(",", SerialNbr);
            return (RDTNDeviceOrderNumber[])DB.ExecuteProcedure(
                "tst_GetDeviceProductionOrderNumber",
                new DB.AnalyzeDataSet(GetDeviceProductionOrderNumber),
                new DB.Parameter[] { 
                    new DB.InParameter("@SERIAL_NBR_LIST", serialNbrList)
                },CommandTimeout: 1800
            );
        }

        private object GetDeviceProductionOrderNumber(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceOrderNumber[] list = new RDTNDeviceOrderNumber[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceOrderNumber insert = new RDTNDeviceOrderNumber();
                insert.SerialNbr = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.FactoryNumber = GetValue<string>(QueryResult.Tables[0].Rows[i]["FACTORY_NUMBER"]);
                insert.Name = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.FirstQuarter = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIRST_QUARTER"]);
                insert.SecondQuarter = GetValue<string>(QueryResult.Tables[0].Rows[i]["SECOND_QUARTER"]);
                insert.ThirdQuarter = GetValue<string>(QueryResult.Tables[0].Rows[i]["THIRD_QUARTER"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion      

        #region GetDevicePossibleTests
        public RDTNTest[] GetDevicePossibleTests(string factoryNbr, bool testOutdoorUse)
        {
            return (RDTNTest[])DB.ExecuteProcedure(
                "tst_GetDevicePossibleTests",
                new DB.AnalyzeDataSet(GetDevicePossibleTests),
                new DB.Parameter[] { 
                    new DB.InParameter("@FACTORY_NBR", factoryNbr),
                    new DB.InParameter("@OUTDOOR_USE", testOutdoorUse)
                }, CommandTimeout: 1800
            );
        }

        private object GetDevicePossibleTests(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNTest[] list = new RDTNTest[count];
            for (int i = 0; i < count; i++)
            {
                RDTNTest insert = new RDTNTest();

                insert.IdTest = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_TEST_KIND"]);
                insert.Name = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion      

        #region GetDeviceTypeCertyficate
        public RDTNDeviceTypeCertyficate[] GetDeviceTypeCertyficate(int[] idDeviceTypes, bool required, int idLanguage)
        {
            //Typ tablicowy SQL: [TYPE_BIGINT_VALUE]
            DataTable dtDeviceTypes = new DataTable();
            dtDeviceTypes.Columns.Add("VALUE", typeof(Int64));

            if (idDeviceTypes != null)
            {
                for (int i = 0; i < idDeviceTypes.Length; i++)
                {
                    dtDeviceTypes.Rows.Add(idDeviceTypes[i]);
                }
            }

            return (RDTNDeviceTypeCertyficate[])DB.ExecuteProcedure(
                "RadioDeviceTester.dbo.tst_RadioDeviceTypeCertyficate",
                new DB.AnalyzeDataSet(GetDeviceTypeCertyficate),
                new DB.Parameter[] { new DB.InParameter("@ID_DEVICE_TYPES", SqlDbType.Structured, dtDeviceTypes),
                                     new DB.InParameter("@REQUIRED", required),
                                     new DB.InParameter("@LANGUAGE", idLanguage)}
            );
        }

        private object GetDeviceTypeCertyficate(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceTypeCertyficate[] list = new RDTNDeviceTypeCertyficate[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceTypeCertyficate insert = new RDTNDeviceTypeCertyficate();
                insert.ID_RADIO_DEVICE_TYPE_CERTYFICATE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_RADIO_DEVICE_TYPE_CERTYFICATE"]);
                insert.ID_RADIO_DEVICE_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_RADIO_DEVICE_TYPE"]);
                insert.DEVICE_TYPE_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_TYPE_NAME"]);
                insert.DOCUMENT_NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["DOCUMENT_NAME"]);
                insert.DOCUMENT_LINK = GetValue<string>(QueryResult.Tables[0].Rows[i]["DOCUMENT_LINK"]);
                insert.SYMBOL = GetValue<string>(QueryResult.Tables[0].Rows[i]["SYMBOL"]);
                insert.REQUIRED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["REQUIRED"]);
                insert.ATEX = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["ATEX"]);
                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region GetDeviceTypeCertyficate
        public List<long> GetDevicesInSet(int setNumber)
        {
            return (List<long>)DB.ExecuteProcedure(
                "tst_LP_GetDevicesInSet",
                new DB.AnalyzeDataSet(GetDevicesInSet),
                new DB.Parameter[] { new DB.InParameter("@ID_SET", SqlDbType.Int, setNumber) }
            );
        }

        private object GetDevicesInSet(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                int count = QueryResult.Tables[0].Rows.Count;
                List<long> list = new List<long>();
                for (int i = 0; i < count; i++)
                {
                    list.Add(Convert.ToInt64(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]));
                }
                return list;
            }
            else
                return null;
        }
        #endregion

        #region GetSetTypes
        public RDTNDeviceInPackage[] GetSetTypes(string serialNbr)
        {
            if (serialNbr == null || serialNbr.Length > 16)
                throw new Exception("Serial number length has to be less or equal 16");
            return (RDTNDeviceInPackage[])DB.ExecuteProcedure(
                "tst_GetDevicesInPackageFromSerial",
                new DB.AnalyzeDataSet(GetDevicesInPackage),
                new DB.Parameter[] { 
                    new DB.InParameter("@SerialNbr", serialNbr)
                }
            );
        }

        private object GetSetTypes(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNDeviceInPackage[] list = new RDTNDeviceInPackage[count];
            for (int i = 0; i < count; i++)
            {
                RDTNDeviceInPackage insert = new RDTNDeviceInPackage();
                insert.ID_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_PACKAGE"]);
                insert.PACKAGE_SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["PACKAGE_SERIAL_NBR"]);
                insert.FINISHED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["FINISHED"]);
                insert.MAX_DEVICES_IN_PACKAGE = GetValue<int>(QueryResult.Tables[0].Rows[i]["MAX_DEVICES_IN_PACKAGE"]);
                insert.DEVICE_SERIAL_NBR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DEVICE_SERIAL_NBR"]);
                insert.ID_DEVICE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DEVICE"]);
                insert.ORDER = GetValue<int>(QueryResult.Tables[0].Rows[i]["ORDER"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion      

        #region GetSimCardByPhone
        public RDTNSimCard[] GetSimCardByPhone(string phone)
        {
            return (RDTNSimCard[])DB.ExecuteProcedure(
                "tst_GetSimCardByPhone",
                new DB.AnalyzeDataSet(GetSimCardByPhone),
                new DB.Parameter[] { 
                    new DB.InParameter("@PHONE", phone)
                }
            );
        }

        private object GetSimCardByPhone(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNSimCard[] list = new RDTNSimCard[count];
            for (int i = 0; i < count; i++)
            {
                RDTNSimCard insert = new RDTNSimCard();
                insert.IdSimCard = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.SerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.Phone = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.Pin = GetValue<string>(QueryResult.Tables[0].Rows[i]["PIN"]);
                insert.Puk = GetValue<string>(QueryResult.Tables[0].Rows[i]["PUK"]);
                insert.IpNumber = GetValue<string>(QueryResult.Tables[0].Rows[i]["IP_NUMBER"]);
                insert.ApnLogin = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_LOGIN"]);
                insert.ApnPassword = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_PASSWORD"]);
                insert.IsWorking = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_WORKING"]);
                insert.IdDistributor = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.IdOwner = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OWNER"]);
                insert.TimeCreated = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME_CREATED"]);
                insert.IdGsmOperator = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_GSM_OPERATOR"]);
                insert.OperatorCode = GetValue<string>(QueryResult.Tables[0].Rows[i]["OPERATOR_CODE"]);

                list[i] = insert;
            }
            return list;
        }
        #endregion

        #region GetSimCards

        public RDTNSimCard[] GetSimCards(int? idDistributor, int? idOwner)
        {
            return (RDTNSimCard[])DB.ExecuteProcedure(
                "tst_LP_GetSimCards",
                new DB.AnalyzeDataSet(GetSimCards),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_DISTRIBUTOR", idDistributor),
                    new DB.InParameter("@ID_OWNER", idOwner)
                }
            );
        }

        private object GetSimCards(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNSimCard[] list = new RDTNSimCard[count];
            for (int i = 0; i < count; i++)
            {
                RDTNSimCard insert = new RDTNSimCard();
                insert.IdSimCard = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.SerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                insert.Phone = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                insert.Pin = GetValue<string>(QueryResult.Tables[0].Rows[i]["PIN"]);
                insert.Puk = GetValue<string>(QueryResult.Tables[0].Rows[i]["PUK"]);
                insert.IpNumber = GetValue<string>(QueryResult.Tables[0].Rows[i]["IP_NUMBER"]);
                insert.ApnLogin = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_LOGIN"]);
                insert.ApnPassword = GetValue<string>(QueryResult.Tables[0].Rows[i]["APN_PASSWORD"]);
                insert.IsWorking = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_WORKING"]);
                insert.IdDistributor = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_DISTRIBUTOR"]);
                insert.IdOwner = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_OWNER"]);
                insert.TimeCreated = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[i]["TIME_CREATED"]);
                insert.IdGsmOperator = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_GSM_OPERATOR"]);

                list[i] = insert;
            }
            return list;
        }

        #endregion

        #region GetSimCardParam

        public RDTNSimCardParam[] GetSimCardParam(int? idSimCardParamType)
        {
            return (RDTNSimCardParam[])DB.ExecuteProcedure(
                "tst_LP_GetSimCardParams",
                new DB.AnalyzeDataSet(GetSimCardParam),
                new DB.Parameter[] { 
                    new DB.InParameter("@IdSimCardParamType", idSimCardParamType)
                }
            );
        }

        private object GetSimCardParam(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNSimCardParam[] list = new RDTNSimCardParam[count];
            for (int i = 0; i < count; i++)
            {
                RDTNSimCardParam insert = new RDTNSimCardParam();
                insert.ID_SIM_CARD = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD"]);
                insert.ID_SIM_CARD_PARAM_TYPE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_SIM_CARD_PARAM_TYPE"]);
                insert.VALUE = GetValue(QueryResult.Tables[0].Rows[i]["VALUE"]);

                list[i] = insert;
            }
            return list;
        }

        #endregion      

        #region ExportSimCard

        public void ExportSimCard(string serialNbr, string phone, string pin, string puk, int idOwner, int idDistributor, int operatorCode,
            string ipAddress = null, bool? isRoaming = null, bool? isGprs = null)
        {
            if (serialNbr.Length > 50)
                throw new Exception("Serial number too long, max character number 50");
            if (phone.Length > 20)
                throw new Exception("Phone too long, max character number 20");
            if (pin.Length != 4)
                throw new Exception("Required pin lenght 4");
            if (puk.Length != 8)
                throw new Exception("Required puk lenght 8");

            DB.ExecuteNonQueryProcedure(
                        "imrsc_InsertSimCard",
                        new DB.Parameter[] { 
                    new DB.InParameter("@SIM_SN", serialNbr),
                    new DB.InParameter("@PHONE_NUMBER", phone),
                    new DB.InParameter("@PIN", pin),
                    new DB.InParameter("@PUK", puk),
                    new DB.InParameter("@ID_OWNER", idOwner),
                    new DB.InParameter("@ID_DISTRIBUTOR", idDistributor),
                    new DB.InParameter("@OPERATOR_CODE", operatorCode),
                    new DB.InParameter("@IP_ADDRESS", ipAddress),
                    new DB.InParameter("@IS_ROAMING", isRoaming),
                    new DB.InParameter("@IS_GPRS", isGprs)
                }
                    );
        }

        #endregion

        #region GetGsmOperator

        public RDTNGsmOperator[] GetGsmOperator()
        {
            return (RDTNGsmOperator[])DB.ExecuteProcedure(
                "tst_LP_GetGsmOperators",
                new DB.AnalyzeDataSet(GetGsmOperator)
            );
        }

        private object GetGsmOperator(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            RDTNGsmOperator[] list = new RDTNGsmOperator[count];
            for (int i = 0; i < count; i++)
            {
                RDTNGsmOperator insert = new RDTNGsmOperator();
                insert.ID_GSM_OPERATOR = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_GSM_OPERATOR"]);
                insert.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                insert.SMS_CENTER_NUMBER = GetValue<string>(QueryResult.Tables[0].Rows[i]["SMS_CENTER_NUMBER"]);
                insert.SIM_SERIAL_LENGTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["SIM_SERIAL_LENGTH"]);
                insert.PHONE_NUMBER_LENGTH = GetValue<int>(QueryResult.Tables[0].Rows[i]["PHONE_NUMBER_LENGTH"]);
                insert.OPERATOR_CODE = GetValue<string>(QueryResult.Tables[0].Rows[i]["OPERATOR_CODE"]);

                list[i] = insert;
            }
            return list;
        }

        #endregion      

        #region GetDeviceIdConfiguration

        public int GetDeviceIdConfiguration(string SerialNbr)
        {
            object IdConfiguration = DB.ExecuteScalar(
                "tst_KF_GetDeviceConfiguration",
                new DB.Parameter[] { 
                    new DB.InParameter("@serialNbr", SerialNbr)
                }
            );
            return IdConfiguration == null ? 0 : (int)IdConfiguration;
        }

        public int GetDeviceIdConfiguration(long SerialNbr)
        {
            long RadioAddress = Convert.ToInt64(SerialNbr.ToString(), 16);
            object IdConfiguration = DB.ExecuteScalar(
                "tst_KF_GetDeviceConfiguration",
                new DB.Parameter[] { 
                    new DB.InParameter("@RadioAddress", RadioAddress)
                }
            );

            return IdConfiguration == null ? 0 : (int)IdConfiguration;
        }

        #endregion
        #region GetManyDevicesIdConfiguration
        public Dictionary<string,int> GetDeviceIdConfiguration(List<string> SerialNbr)
        {
            DataTable deviceSnList = new DataTable();
            deviceSnList.Columns.Add("VALUE", typeof(String));
            foreach (string dsn in SerialNbr)
            {
                deviceSnList.Rows.Add(dsn);
            }
            return (Dictionary<string,int>)DB.ExecuteProcedure(
                "tst_PW_GetManyDeviceConfiguration", new DB.AnalyzeDataSet(GetDeviceIdConfiguration),
                new DB.Parameter[] { 
                     new DB.InParameter("@serialNbr", SqlDbType.Structured, deviceSnList)
                }
            );
        }
        private object GetDeviceIdConfiguration(DataSet QueryResult)
        {
            Dictionary<string, int> Result = new Dictionary<string, int>();
            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                Result.Add((string)QueryResult.Tables[0].Rows[i][0], (int)QueryResult.Tables[0].Rows[i][1]);
            }
            return Result;
        }
        #endregion

        #region GetConfigurationDeviceType

        public List<RDTNAiutDeviceType> GetConfigurationDeviceType(int IdConfiguration, int ConfigurationFlag)
        {
            return (List<RDTNAiutDeviceType>)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_ShowAiutDBDevices", new DB.AnalyzeDataSet(GetConfigurationDeviceType),
                new DB.Parameter[] { 
                    new DB.InParameter("@IDSoftwareConfiguration", IdConfiguration),
                    new DB.InParameter("@ConfigurableFlag", ConfigurationFlag)
                }
            );
        }

        public RDTNAiutDeviceType GetAiutDeviceType(int IdAiutDeviceType)
        {
            return ((List<RDTNAiutDeviceType>)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetAiutDeviceType", new DB.AnalyzeDataSet(GetConfigurationDeviceType),
                new DB.Parameter[] { 
                    new DB.InParameter("@IDAiutDeviceType", IdAiutDeviceType)
                }
            )).FirstOrDefault();
        }

        public object GetConfigurationDeviceType(DataSet QueryResult)
        {
            int count = QueryResult.Tables[0].Rows.Count;
            List<RDTNAiutDeviceType> list = new List<RDTNAiutDeviceType>();
            for (int i = 0; i < count; i++)
            {
                RDTNAiutDeviceType toAdd = new RDTNAiutDeviceType();
                toAdd.IdDeviceType = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_aiutDB_DEVICE_TYPE"]);
                toAdd.DeviceTypeName = GetValue<string>(QueryResult.Tables[0].Rows[i]["STANDARD_DESCRIPTION"]);
                toAdd.IsConfigurable = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_CONFIGURABLE"]);
                toAdd.IsChecked = GetValue<bool>(QueryResult.Tables[0].Rows[i]["IS_CHECKED"]);
                list.Add(toAdd);
            }
            return list;
        }

        #endregion

        #region ValidateDevice

        public bool ValidateDevice(string serialNbr, out int errorCode, out string errorMsg)
        {
            DB.OutParameter ResultErrorCode = new DB.OutParameter("@ErrorCode", SqlDbType.Int);
            DB.OutParameter ResultErrorMsg = new DB.OutParameter("@ErrorMsg", SqlDbType.NVarChar, 300);

            DB.ExecuteNonQueryProcedure("RadioDeviceTesterNew.dbo.tst_ValidateDevice",
                    new DB.Parameter[]
					{
						new DB.InParameter ("@DeviceSerialNbr", serialNbr),
						ResultErrorCode,
                        ResultErrorMsg
					});
            errorCode = (int)ResultErrorCode.Value;
            errorMsg = (string)ResultErrorMsg.Value;
            return errorCode == 0;
        }

        #endregion

        #region GetPackageContent


        public Dictionary<long, int> GetDeviceSetContent(List<string> SerialNbr, bool? IsConfigurable = null)
        {
            DataTable deviceSnList = new DataTable();
            deviceSnList.Columns.Add("VALUE", typeof(String));
            foreach (string dsn in SerialNbr)
            {
                deviceSnList.Rows.Add(dsn);
            }
            return (Dictionary<long, int>)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetMultipleDeviceSetContent", new DB.AnalyzeDataSet(GetDeviceSetContent),
                new DB.Parameter[] { 
                    new DB.InParameter("@DeviceSerialNbr", SqlDbType.Structured, deviceSnList),
                    new DB.InParameter("@IsConfigurable", SqlDbType.Bit, IsConfigurable)
                }
            );
        }

        public Dictionary<long, int> GetDeviceSetContent(string SerialNbr, bool? IsConfigurable = null)
        {
            return (Dictionary<long, int>)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetDeviceSetContent", new DB.AnalyzeDataSet(GetDeviceSetContent),
                new DB.Parameter[] { 
                    new DB.InParameter("@DeviceSerialNbr", SerialNbr),
                    new DB.InParameter("@IsConfigurable", SqlDbType.Bit, IsConfigurable)
                }
            );
        }

        private object GetDeviceSetContent(DataSet QueryResult)
        {
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                int count = QueryResult.Tables[0].Rows.Count;
                Dictionary<long, int> list = new Dictionary<long, int>();
                for (int i = 0; i < count; i++)
                {
                    list.Add(Convert.ToInt64(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]), Convert.ToInt32(QueryResult.Tables[0].Rows[i]["ID_SET"]));
                }
                return list;
            }
            else
                return null;
        }

        #endregion

        #region GetDeviceSimCardSerialNbr

        public string GetDeviceSimCardSerialNbr(string DeviceFactoryNbr, bool useRadioDeviceTester = true)
        {
            string serialNbrSim = null;

            DB.OutParameter SimCardIDParam = new DB.OutParameter("@SimCardID", SqlDbType.Int);
            DB.OutParameter PhoneParam = new DB.OutParameter("@Phone", SqlDbType.NVarChar, 20);
            DB.OutParameter PinParam = new DB.OutParameter("@Pin", SqlDbType.VarChar, 4);
            DB.OutParameter PukParam = new DB.OutParameter("@Puk", SqlDbType.VarChar, 8);
            DB.OutParameter GsmOperatorIDParam = new DB.OutParameter("@GsmOperatorID", SqlDbType.Int);
            DB.OutParameter GsmOperatorCodeParam = new DB.OutParameter("@GsmOperatorCode", SqlDbType.NVarChar, 50);
            DB.OutParameter ErrorCodeParam = new DB.OutParameter("@ErrorCode", SqlDbType.Int);
            DB.OutParameter ErrorMsgParam = new DB.OutParameter("@ErrorMsg", SqlDbType.NVarChar, 100);
            DB.OutParameter SIMCardSerialNbrParam = new DB.OutParameter("@SimCardSerialNbr", SqlDbType.NVarChar, 50);
            DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetSimCardDataForDevice", new DB.AnalyzeDataSet(GetDeviceSimCardSerialNbr),
                new DB.Parameter[] { 
                    new DB.InParameter("@DeviceSerialNbr", SqlDbType.NVarChar, DeviceFactoryNbr, 16),
                    SimCardIDParam,
                    PhoneParam,
                    PinParam,
                    PukParam,
                    GsmOperatorIDParam,
                    GsmOperatorCodeParam,
                    ErrorCodeParam,
                    ErrorMsgParam,
                    SIMCardSerialNbrParam
                }
            );
            serialNbrSim = SIMCardSerialNbrParam.Value == null ? null : SIMCardSerialNbrParam.Value.ToString();

            if (useRadioDeviceTester && String.IsNullOrEmpty(serialNbrSim))
            {
                DB.OutParameter ExistsParam = new DB.OutParameter("@Exists", SqlDbType.Bit);
                DB.OutParameter TestedOkParam = new DB.OutParameter("@TestedOK", SqlDbType.Bit);
                DB.OutParameter ServicedParam = new DB.OutParameter("@Serviced", SqlDbType.Bit);
                DB.OutParameter DeletedParam = new DB.OutParameter("@Deleted", SqlDbType.Bit);
                SIMCardSerialNbrParam = new DB.OutParameter("@SIMCardSerialNbr", SqlDbType.NVarChar, 50);
                DB.ExecuteProcedure(
                    "RadioDeviceTester.dbo.tst_GetDeviceAtributes", new DB.AnalyzeDataSet(GetDeviceSimCardSerialNbr),
                    new DB.Parameter[] { 
                    new DB.InParameter("@SerialNumber", SqlDbType.NVarChar, DeviceFactoryNbr, 16),
                    ExistsParam,
                    TestedOkParam,
                    ServicedParam,
                    DeletedParam,
                    SIMCardSerialNbrParam
                }
                );

                serialNbrSim = SIMCardSerialNbrParam.Value == null ? null : SIMCardSerialNbrParam.Value.ToString();
            }

            return serialNbrSim;
        }

        private object GetDeviceSimCardSerialNbr(DataSet QueryResult)
        {
            return null;
        }

        #endregion

        #region CheckIfSimCardExists

        public bool CheckIfSimCardExists(string SerialNbr)
        {
            bool exists = false;

            DB.OutParameter ExistsParam = new DB.OutParameter("@EXISTS", SqlDbType.Bit);
            DB.ExecuteNonQueryProcedure(
                "RadioDeviceTesterNew.dbo.tst_CheckIfSimCardExists",
                new DB.Parameter[] { 
                    new DB.InParameter("@SERIAL_NBR", SqlDbType.NVarChar, SerialNbr, 50),
                    ExistsParam
                }
            );
            exists = ExistsParam.Value == null ? false : Convert.ToBoolean(ExistsParam.Value);


            return exists;
        }

        #endregion

        #region Pobranie listy urządzeń dla SIMA SERVER
        public SimaServerDevice[] GetSimaServerDevices(uint IdShippingList)
        {
            return (SimaServerDevice[])DB.ExecuteProcedure(
                    "RadioDeviceTesterNew.dbo.imrse_GetDeviceInformationForSimaServer",
                    new DB.AnalyzeDataSet(GetSimaServerDevices),
                    new DB.Parameter[]
					    {						
                            new DB.InParameter ("@IdShippingList", IdShippingList)
					    }
                        , true
                        , IsolationLevel.ReadUncommitted
                        , 0
                );
        }

        private object GetSimaServerDevices(DataSet QueryResult)
        {
            List<SimaServerDevice> foundResults = new List<SimaServerDevice>(/*QueryResult.Tables[0].Rows.Count*/);

            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                SimaServerDevice item = new SimaServerDevice();
                uint RadioAddress, RadioAddress2, RadioAddress3, RadioAddress4;

                item.DeviceSerialNbr = ((string)QueryResult.Tables[0].Rows[i][0]).Trim();
                RadioAddress = (uint)(int)QueryResult.Tables[0].Rows[i][1];
                RadioAddress2 = (uint)(int)QueryResult.Tables[0].Rows[i][2];
                RadioAddress3 = (uint)(int)QueryResult.Tables[0].Rows[i][3];
                RadioAddress4 = (uint)(int)QueryResult.Tables[0].Rows[i][4];

                item.DeviceName = ((string)QueryResult.Tables[0].Rows[i][5]).Trim();
                item.IdAiutDBDeviceType = (uint)(int)QueryResult.Tables[0].Rows[i][6];
                item.IdSoftwareConfiguration = (uint)(int)QueryResult.Tables[0].Rows[i][7];
                item.WarrantyDate = (DateTime)QueryResult.Tables[0].Rows[i][8];

                item.EncryptedServicePin1 = QueryResult.Tables[0].Rows[i][9].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][9]).Trim();
                item.EncryptedServicePin2 = QueryResult.Tables[0].Rows[i][10].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][10]).Trim();
                item.EncryptedServicePin3 = QueryResult.Tables[0].Rows[i][11].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][11]).Trim();
                item.EncryptedServicePin4 = QueryResult.Tables[0].Rows[i][12].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][12]).Trim();

                item.EncryptedValvePin1 = QueryResult.Tables[0].Rows[i][13].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][13]).Trim();
                item.EncryptedValvePin2 = QueryResult.Tables[0].Rows[i][14].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][14]).Trim();
                item.EncryptedValvePin3 = QueryResult.Tables[0].Rows[i][15].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][15]).Trim();
                item.EncryptedValvePin4 = QueryResult.Tables[0].Rows[i][16].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][16]).Trim();
                item.EncryptedValvePin5 = QueryResult.Tables[0].Rows[i][17].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][17]).Trim();
                item.EncryptedValvePin6 = QueryResult.Tables[0].Rows[i][18].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][18]).Trim();
                item.EncryptedValvePin7 = QueryResult.Tables[0].Rows[i][19].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][19]).Trim();
                item.EncryptedValvePin8 = QueryResult.Tables[0].Rows[i][20].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][20]).Trim();
                item.EncryptedValvePin9 = QueryResult.Tables[0].Rows[i][21].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][21]).Trim();

                item.CryptographyKey = QueryResult.Tables[0].Rows[i].IsNull(22) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][22]);
                item.CryptographyMacKey = QueryResult.Tables[0].Rows[i].IsNull(23) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][23]);
                item.CryptographyKey2 = QueryResult.Tables[0].Rows[i].IsNull(24) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][24]);
                item.CryptographyMacKey2 = QueryResult.Tables[0].Rows[i].IsNull(25) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][25]);
                item.CryptographyKey3 = QueryResult.Tables[0].Rows[i].IsNull(26) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][26]);
                item.CryptographyMacKey3 = QueryResult.Tables[0].Rows[i].IsNull(27) == true ? null : ((byte[])QueryResult.Tables[0].Rows[i][27]);

                item.MeterSerialNbr = QueryResult.Tables[0].Rows[i][28].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][28]).Trim();
                item.MeterType = QueryResult.Tables[0].Rows[i][29].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][29]).Trim();

                //Przypisanie wykorzystywanego adresu radiowego
                if (RadioAddress2 != 0 && item.DeviceName.StartsWith("OLAN"))
                    item.RadioAddress = RadioAddress2;
                else
                    item.RadioAddress = RadioAddress;

                item.PackageSerialNbr = QueryResult.Tables[0].Rows[i][30].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][30]).Trim();

                item.GsmOperatorCode = QueryResult.Tables[0].Rows[i][31].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][31]).Trim();

                item.DeviceDriverFileName = QueryResult.Tables[0].Rows[i][32].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][32]).Trim();

                item.impulseWeight = QueryResult.Tables[0].Rows[i][33].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][33]).Trim();

                item.patternSerialNbr = QueryResult.Tables[0].Rows[i][34].ToString().Length == 0 ? null : ((string)QueryResult.Tables[0].Rows[i][34]).Trim();
                if (!QueryResult.Tables[0].Rows[i].IsNull(35)) item.IdMeterType = (int)QueryResult.Tables[0].Rows[i][35];

                if (!QueryResult.Tables[0].Rows[i].IsNull(36)) item.ITNumber = int.Parse((string)QueryResult.Tables[0].Rows[i][36]);

                if (!QueryResult.Tables[0].Rows[i].IsNull(37)) item.deviceIvOut = Convert.ToInt64(QueryResult.Tables[0].Rows[i][37]);

                if (!QueryResult.Tables[0].Rows[i].IsNull(38)) item.deviceIvOutChange = Convert.ToInt64(QueryResult.Tables[0].Rows[i][38]);

                foundResults.Add(item);

            }
            return foundResults.ToArray();
        }
        #endregion

        #region GetDeviceInformation

        public DeviceInformation GetDeviceInformation(string deviceSerialNbr)
        {
            return (DeviceInformation)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetIMRDeviceInformation",
                new DB.AnalyzeDataSet(GetDeviceInformation),
                new DB.Parameter[]	{
										new DB.InParameter ("@DeviceSerialNbr", deviceSerialNbr)						
									}
                );
        }

        public DeviceInformation GetDeviceInformation(long SerialNbr)
        {
            long RadioAddress = Convert.ToInt64(SerialNbr.ToString(), 16);
            return (DeviceInformation)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetIMRDeviceInformation",
                new DB.AnalyzeDataSet(GetDeviceInformation),
                new DB.Parameter[]	{
										new DB.InParameter ("@DeviceRadioAddress", RadioAddress)						
									}
                );
        }

        private object GetDeviceInformation(DataSet QueryResult)
        {
            DeviceInformation Result = new DeviceInformation();
            Result.DeviceSerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[0]["SERIAL_NBR"]);
            Result.SimCardSerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[0]["SIM_CARD_SERIAL_NBR"]);

            Result.CodeName = GetValue<string>(QueryResult.Tables[0].Rows[0]["CODE_NAME"]);
            Result.FirstQuarter = GetValue<string>(QueryResult.Tables[0].Rows[0]["FIRST_QUARTER"]);
            Result.SecondQuarter = GetValue<string>(QueryResult.Tables[0].Rows[0]["SECOND_QUARTER"]);
            Result.ThirdQuarter = GetValue<string>(QueryResult.Tables[0].Rows[0]["THIRD_QUARTER"]);

            Result.RadioFrequency0 = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_FREQUENCY_0"]);
            Result.RadioFrequency1 = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_FREQUENCY_1"]);
            Result.RadioFrequency2 = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_FREQUENCY_2"]);
            Result.RadioFrequency3 = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_FREQUENCY_3"]);
            Result.CounterQueryFrequency = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["COUNTER_QUERY_FREQUENCY"]);

            Result.OlanMainAddress = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_ADDRESS"]);
            Result.OlanMainCryptKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY"];
            Result.OlanMainMacKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY"];

            Result.OlanInterface1Address = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_ADDRESS_2"]);
            Result.OlanInterface1CryptKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY_2"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY_2"];
            Result.OlanInterface1MacKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY_2"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY_2"];

            Result.OlanInterface2Address = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_ADDRESS_3"]);
            Result.OlanInterface2CryptKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY_3"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_KEY_3"];
            Result.OlanInterface2MacKey = QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY_3"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["CRYPTOGRAPHY_MAC_KEY_3"];

            Result.OlanInterface3Address = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["RADIO_ADDRESS_4"]);

            Result.SimPhone = GetValue<string>(QueryResult.Tables[0].Rows[0]["PHONE"]);
            Result.SimPin = GetValue<string>(QueryResult.Tables[0].Rows[0]["PIN"]);
            Result.SimPuk = GetValue<string>(QueryResult.Tables[0].Rows[0]["PUK"]);
            Result.SimSegregator = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["SEGREGATOR"]);
            Result.SimPage = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["PAGE"]);
            Result.SimSection = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["SECTION"]);
            Result.SimIsWorking = GetNullableValue<bool>(QueryResult.Tables[0].Rows[0]["IS_WORKING"]);
            Result.SimTimeCreated = GetNullableValue<DateTime>(QueryResult.Tables[0].Rows[0]["TIME_CREATED"]);
            Result.SimIdGsmOperator = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_GSM_OPERATOR"]);

            //Pobieranie kodów jedrazowego otwarcia zaworu VALVE_PINS i SERVICE_PINS
            byte[] valvePins = Result.OlanMainCryptKey = QueryResult.Tables[0].Rows[0]["VALVE_PINS"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["VALVE_PINS"];
            byte[] servicePins = Result.OlanMainCryptKey = QueryResult.Tables[0].Rows[0]["SERVICE_PINS"] == DBNull.Value ? new byte[] { } : (byte[])QueryResult.Tables[0].Rows[0]["SERVICE_PINS"];

            Dictionary<int, object> DeviceParams = GetDeviceParams(Result.DeviceSerialNbr);

            #region Zapisywanie kodów otwarcia zaworu lub null gdy uklad ich nie posiada (VALVE_PINS)

            if (valvePins.Length == 0)
            {
                //Sprawdzenie czy PINY nie sa zapisane jako DEVICE_PARAM
                Result.ValveOpenCode1 = DeviceParams.ContainsKey(53) ? (string)DeviceParams[53] : null;
                Result.ValveOpenCode2 = DeviceParams.ContainsKey(54) ? (string)DeviceParams[54] : null;
                Result.ValveOpenCode3 = DeviceParams.ContainsKey(55) ? (string)DeviceParams[55] : null;
                Result.ValveOpenCode4 = DeviceParams.ContainsKey(56) ? (string)DeviceParams[56] : null;
                Result.ValveOpenCode5 = DeviceParams.ContainsKey(57) ? (string)DeviceParams[57] : null;
                Result.ValveOpenCode6 = DeviceParams.ContainsKey(58) ? (string)DeviceParams[58] : null;
                Result.ValveOpenCode7 = DeviceParams.ContainsKey(59) ? (string)DeviceParams[59] : null;
                Result.ValveOpenCode8 = DeviceParams.ContainsKey(60) ? (string)DeviceParams[60] : null;
                Result.ValveOpenCode9 = DeviceParams.ContainsKey(61) ? (string)DeviceParams[61] : null;
            }
            else
            {
                //Podział odebranych tablic na poszczegolne piny i kody
                string valvePinString = System.Text.Encoding.ASCII.GetString(valvePins);
                string[] valvePinTable = valvePinString.Split('\0');

                Result.ValveOpenCode1 = valvePinTable[0];
                Result.ValveOpenCode2 = valvePinTable[1];
                Result.ValveOpenCode3 = valvePinTable[2];
                Result.ValveOpenCode4 = valvePinTable[3];
                Result.ValveOpenCode5 = valvePinTable[4];
                Result.ValveOpenCode6 = valvePinTable[5];
                Result.ValveOpenCode7 = valvePinTable[6];
                Result.ValveOpenCode8 = valvePinTable[7];
                Result.ValveOpenCode9 = valvePinTable[8];
            }

            if (servicePins.Length == 0)
            {
                //Sprawdzenie czy PINY nie sa zapisane jako DEVICE_PARAM
                Result.ExtendedMenuCode1 = DeviceParams.ContainsKey(62) ? (string)DeviceParams[62] : null;
                Result.ExtendedMenuCode2 = DeviceParams.ContainsKey(63) ? (string)DeviceParams[63] : null;
                Result.ExtendedMenuCode3 = DeviceParams.ContainsKey(64) ? (string)DeviceParams[64] : null;
                Result.ExtendedMenuCode4 = DeviceParams.ContainsKey(65) ? (string)DeviceParams[65] : null;
            }
            else
            {
                //Podział odebranych tablic na poszczegolne piny i kody
                string servicePinString = System.Text.Encoding.ASCII.GetString(servicePins);
                string[] servicePinTable = servicePinString.Split('\0');

                Result.ExtendedMenuCode1 = servicePinTable[0];
                Result.ExtendedMenuCode2 = servicePinTable[1];
                Result.ExtendedMenuCode3 = servicePinTable[2];
                Result.ExtendedMenuCode4 = servicePinTable[3];
            }

            #endregion Zapisywanie kodów otwarcia zaworu lub null gdy uklad ich nie posiada (VALVE_PINS)

            Result.RadioAddress = GetValue<uint>(QueryResult.Tables[0].Rows[0]["RADIO_ADDRESS"]);
            Result.GsmOperatorCode = GetValue<string>(QueryResult.Tables[0].Rows[0]["OPERATOR_CODE"]);
            Result.SimIp = GetValue<string>(QueryResult.Tables[0].Rows[0]["IP_NUMBER"]);

            return Result;
        }

        #endregion

        #region GetSet

        public RDTSet GetSet(int IdSet)
        {
            return (RDTSet)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_GetSet",
                new DB.AnalyzeDataSet(GetSet),
                new DB.Parameter[]	{
										new DB.InParameter ("@IdSet", IdSet)						
									}
                );
        }

        private object GetSet(DataSet QueryResult)
        {
            RDTSet Result = new RDTSet();
            Result.IdSet = GetValue<int>(QueryResult.Tables[0].Rows[0]["ID_SET"]);
            Result.IdSetType = GetValue<int>(QueryResult.Tables[0].Rows[0]["ID_SET_TYPE"]);
            Result.CreationDate = GetValue<DateTime>(QueryResult.Tables[0].Rows[0]["CREATION_DATE"]);
            Result.IdEmployee = GetValue<int>(QueryResult.Tables[0].Rows[0]["ID_EMPLOYEE"]);
            Result.IdSetState = GetValue<int>(QueryResult.Tables[0].Rows[0]["ID_SET_STATE"]);
            Result.IdImrServer = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_IMR_SERVER"]);
            Result.SetSerialNbr = GetValue<string>(QueryResult.Tables[0].Rows[0]["SET_SERIAL_NBR"]);
            Result.IdInstallation = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_INSTALATION_GROUP"]);
            Result.IdArchEntry = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_ARCH_ENTRY"]);
            Result.IdDistributor = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_DISTRIBUTOR"]);
            Result.IdDepositoryLocation = GetNullableValue<int>(QueryResult.Tables[0].Rows[0]["ID_DEPOSITORY_LOCATION"]);

            return Result;
        }

        #endregion

        #region GetDeviceDetails

        public DeviceDetails GetDeviceDetails(string SerialNbr)
        {
            return (DeviceDetails)DB.ExecuteProcedure("tst_GetDeviceDetails", new DB.AnalyzeDataSet(GetDeviceDetails), new DB.Parameter[]{
                                                        new DB.InParameter("@DeviceSerialNbr", SerialNbr)});
        }

        private object GetDeviceDetails(DataSet QueryResult)
        {
            DeviceDetails Result = new DeviceDetails();
            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                if (GetValue<string>(QueryResult.Tables[0].Rows[i][1]) == "Numer telefonu karty SIM")
                {
                    Result.Phone = GetValue<string>(QueryResult.Tables[0].Rows[i][2]);
                }
            }
            return Result;
        }

        #endregion

        #region GetDeviceParams

        public Dictionary<int, object> GetDeviceParams(string SerialNbr)
        {
            return (Dictionary<int, object>)DB.ExecuteProcedure("tst_GetDeviceParamsBySerial", new DB.AnalyzeDataSet(GetDeviceParams), new DB.Parameter[]{
                                                        new DB.InParameter("@SERIAL_NBR", SerialNbr)});
        }

        private object GetDeviceParams(DataSet QueryResult)
        {
            Dictionary<int, object> Result = new Dictionary<int, object>();

            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
                Result.Add((int)QueryResult.Tables[0].Rows[i]["ID_DEVICE_PARAM_TYPE"], QueryResult.Tables[0].Rows[i]["VALUE"]);

            return Result;
        }

        #endregion

        #region GetDevicesConfigurationFirmware

        public Dictionary<long, RDTNDeviceConfigurationFirmware> GetDevicesConfigurationFirmware()
        {
            return (Dictionary<long, RDTNDeviceConfigurationFirmware>)DB.ExecuteProcedure("tst_GetDevicesConfigurationAndFirmwareInformation",
                                                        new DB.AnalyzeDataSet(GetDevicesConfigurationFirmware), new DB.Parameter[]{});
        }

        private object GetDevicesConfigurationFirmware(DataSet QueryResult)
        {
            Dictionary<long, RDTNDeviceConfigurationFirmware> Result = new Dictionary<long, RDTNDeviceConfigurationFirmware>();
            if (QueryResult != null && QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
                {
                    RDTNDeviceConfigurationFirmware insert = new RDTNDeviceConfigurationFirmware();
                    insert.SERIAL_NBR = GetValue<long>(QueryResult.Tables[0].Rows[i]["SERIAL_NBR"]);
                    insert.ID_CONFIGURATION = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_CONFIGURATION"]);
                    insert.CONFIGURATION_DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONFIGURATION_DESCR"]);
                    insert.CONFIGURATION_ALLOWED_TO_PRODUCTION = GetNullableValue<bool>(QueryResult.Tables[0].Rows[i]["CONFIGURATION_ALLOWED_TO_PRODUCTION"]);
                    insert.CONFIGURATION_VERIFICATED = GetValue<bool>(QueryResult.Tables[0].Rows[i]["CONFIGURATION_VERIFICATED"]);
                    insert.CONFIGURATION_VERIFICATED_BY = GetValue<string>(QueryResult.Tables[0].Rows[i]["CONFIGURATION_VERIFICATED_BY"]);
                    insert.CONFIGURATION_ID_DISTRIBUTOR = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["CONFIGURATION_ID_DISTRIBUTOR"]);
                    insert.ID_FIRMWARE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FIRMWARE"]);
                    insert.ID_FIRMWARE_TYPE = GetNullableValue<int>(QueryResult.Tables[0].Rows[i]["ID_FIRMWARE_TYPE"]);
                    insert.FIRMWARE_INFORMATION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIRMWARE_INFORMATION"]);
                    insert.PROCESSOR_TYPE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PROCESSOR_TYPE"]);
                    insert.FIRMWARE_VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["FIRMWARE_VERSION"]);
                    insert.HARDWARE_VERSION = GetValue<string>(QueryResult.Tables[0].Rows[i]["HARDWARE_VERSION"]);
                    if(!Result.ContainsKey(insert.SERIAL_NBR))
                        Result.Add(insert.SERIAL_NBR, insert);
                }
            }
            return Result;
        }

        #endregion

        #region GetEmployee

        public List<RDTNEmployee> GetEmployees(int? idEmployee = null)
        {
            return (List<RDTNEmployee>)DB.ExecuteProcedure(
                "RadioDeviceTesterNew.dbo.tst_PM_GetEmployees", new DB.AnalyzeDataSet(GetEmployees),
                new DB.Parameter[] { 
                    new DB.InParameter("@ID_EMPLOYEE", idEmployee)
                }
            );
        }

        private object GetEmployees(DataSet QueryResult)
        {
            List<RDTNEmployee> Result = new List<RDTNEmployee>();

            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                RDTNEmployee newItem = new RDTNEmployee();
                newItem.ID_EMPLOYEE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_EMPLOYEE"]);
                newItem.NAME = GetValue<string>(QueryResult.Tables[0].Rows[i]["NAME"]);
                newItem.LOGIN = GetValue<string>(QueryResult.Tables[0].Rows[i]["LOGIN"]);
                //newItem.PASSWORD = GetValue<string>(QueryResult.Tables[0].Rows[i]["PASSWORD"]);
                newItem.ID_EMPLOYEE_ROLE = GetValue<int>(QueryResult.Tables[0].Rows[i]["ID_EMPLOYEE_ROLE"]);
                newItem.ADDRESS = GetValue<string>(QueryResult.Tables[0].Rows[i]["ADDRESS"]);
                newItem.PHONE = GetValue<string>(QueryResult.Tables[0].Rows[i]["PHONE"]);
                newItem.MOBILE = GetValue<string>(QueryResult.Tables[0].Rows[i]["MOBILE"]);
                newItem.FAX = GetValue<string>(QueryResult.Tables[0].Rows[i]["FAX"]);
                newItem.MAIL = GetValue<string>(QueryResult.Tables[0].Rows[i]["MAIL"]);
                newItem.DESCR = GetValue<string>(QueryResult.Tables[0].Rows[i]["DESCR"]);

                Result.Add(newItem);
            }
            return Result;
        }

        #endregion

        #region SaveEmployee

        public int SaveEmployee(RDTNEmployee objectToSave)
        {
            DB.InOutParameter InsertId = new DB.InOutParameter("@ID_EMPLOYEE", objectToSave.ID_EMPLOYEE);

            DB.ExecuteNonQueryProcedure(
                        "tst_LP_SaveEmployee",
                        new DB.Parameter[] { 
                    InsertId,
                    new DB.InParameter("@NAME", objectToSave.NAME),
                    new DB.InParameter("@LOGIN", objectToSave.LOGIN),
                    new DB.InParameter("@PASSWORD", objectToSave.PASSWORD),
                    new DB.InParameter("@ID_EMPLOYEE_ROLE", objectToSave.ID_EMPLOYEE_ROLE),
                    new DB.InParameter("@ADDRESS", objectToSave.ADDRESS),
                    new DB.InParameter("@PHONE", objectToSave.PHONE),
                    new DB.InParameter("@MOBILE", objectToSave.MOBILE),
                    new DB.InParameter("@FAX", objectToSave.FAX),
                    new DB.InParameter("@MAIL", objectToSave.MAIL),
                    new DB.InParameter("@DESCR", objectToSave.DESCR)
                }
                    );

            if (!InsertId.IsNull)
                objectToSave.ID_EMPLOYEE = (int)InsertId.Value;

            return (int)InsertId.Value;
            
        }

        #endregion

        #region SaveDistributor

        public int SaveDistributor(RDTNDistributor objectToSave)
        {
            DB.InParameter InsertId = new DB.InParameter("@ID_DISTRIBUTOR", objectToSave.ID_DISTRIBUTOR);

            if (!String.IsNullOrEmpty(objectToSave.NAME) && objectToSave.NAME.Length > 50)
                objectToSave.NAME = objectToSave.NAME.Substring(0, 50);

            DB.ExecuteNonQueryProcedure(
                        "tst_CreateDistributorForSimCard",
                        new DB.Parameter[] { 
                    InsertId,
                    new DB.InParameter("@DISTRIBUTOR_NAME", objectToSave.NAME)
                }
                    );

            if (!InsertId.IsNull)
                objectToSave.ID_DISTRIBUTOR = (int)InsertId.Value;

            return (int)InsertId.Value;

        }

        #endregion

        #region GetDeviceSimCardData

        public void GetDeviceSimCardData(string DeviceSerialNbr, out int SimCardID, out string SimCardSerialNbr, out string Phone, out string Pin, out string Puk,
            out int GsmOperatorID, out string GsmOperatorCode, out int ErrorCode, out string ErrorMsg)
        {
            SimCardID = GsmOperatorID = ErrorCode = 0;
            SimCardSerialNbr = Phone = Pin = Puk = GsmOperatorCode = ErrorMsg = "";

            DB.OutParameter pSimCardID = new DB.OutParameter("@SimCardID", SqlDbType.Int);
            DB.OutParameter pSimCardSerialNbr = new DB.OutParameter("@SimCardSerialNbr", SqlDbType.NVarChar, 50);
            DB.OutParameter pPhone = new DB.OutParameter("@Phone", SqlDbType.NVarChar, 20);
            DB.OutParameter pPin = new DB.OutParameter("@Pin", SqlDbType.NVarChar, 4);
            DB.OutParameter pPuk = new DB.OutParameter("@Puk", SqlDbType.NVarChar, 8);
            DB.OutParameter pGsmOperatorID = new DB.OutParameter("@GsmOperatorID", SqlDbType.Int);
            DB.OutParameter pGsmOperatorCode = new DB.OutParameter("@GsmOperatorCode", SqlDbType.NVarChar, 50);
            DB.OutParameter pErrorCode = new DB.OutParameter("@ErrorCode", SqlDbType.Int);
            DB.OutParameter pErrorMsg = new DB.OutParameter("@ErrorMsg", SqlDbType.NVarChar, 50);

            DB.ExecuteNonQueryProcedure("tst_GetSimCardDataForDevice",
                    new DB.Parameter[]
					{						
                        new DB.InParameter ("@DeviceSerialNbr", DeviceSerialNbr),
                        pSimCardID, pSimCardSerialNbr, pPhone, pPin, pPuk, pGsmOperatorID, pGsmOperatorCode, pErrorCode, pErrorMsg
					});

            ErrorCode = pErrorCode.Value == DBNull.Value ? 0 : (int)pErrorCode.Value;
            ErrorMsg = pErrorMsg.Value == DBNull.Value ? "" : (string)pErrorMsg.Value;
            if (ErrorCode == 0)
            {
                SimCardID = (int)pSimCardID.Value;
                SimCardSerialNbr = (string)pSimCardSerialNbr.Value;
                Phone = (string)pPhone.Value;
                Pin = (string)pPin.Value;
                Puk = (string)pPuk.Value;
                GsmOperatorID = (int)pGsmOperatorID.Value;
                GsmOperatorCode = (string)pGsmOperatorCode.Value;
            }
        }

        #endregion

        #region GetShippingListWarehouseReport

        public List<RDTWarehouseReportEntry> GetShippingListWarehouseReport(List<long> shippingListDeviceIdList)
        {

            DataTable idTable = new DataTable();
            idTable.Columns.Add("VALUE", typeof(String));
            foreach (long deviceId in shippingListDeviceIdList)
            {
                idTable.Rows.Add(deviceId.ToString());
            }

            return (List<RDTWarehouseReportEntry>)DB.ExecuteProcedure(
                                            "RadioDeviceTesterNew.dbo.imrsc_ShippingListWarehouseReportGet",
                                            new DB.AnalyzeDataSet(GetShippingListWarehouseReport),
                                            new DB.Parameter[]	{
										        new DB.InParameter ("@SerialNumbers", SqlDbType.Structured, idTable)
			});
        }

        private List<RDTWarehouseReportEntry> GetShippingListWarehouseReport(DataSet queryResult)
        {
            var result = new List<RDTWarehouseReportEntry>();

            foreach (DataRow row in queryResult.Tables[0].Rows)
            {
                result.Add(new RDTWarehouseReportEntry()
                {
                    DeviceSerialNumber  = GetValue<string>(row["SERIAL_NBR"]),
                    SetSerialNumber     = GetValue<string>(row["SET_SERIAL_NBR"]),
                    PackageSerialNumber = GetValue<string>(row["PACKAGE_SERIAL_NBR"])
                });
            }

            return result;
        }

        #endregion

        #region GetConfigurations
        public List<RDTNConfiguration> GetAllConfigurations()
        { 
            return (List<RDTNConfiguration>) DB.ExecuteProcedure("RadioDeviceTesterNew.dbo.imrsc_GetAllConfigurations", new DB.AnalyzeDataSet(GetAllConfigurationsAnalyzer));
        }
        private object GetAllConfigurationsAnalyzer(DataSet QueryResult)
        {
            List<RDTNConfiguration> Result = new List<RDTNConfiguration>();
            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                RDTNConfiguration toAdd = new RDTNConfiguration();
                toAdd.ID_CONFIGURATION = (int)QueryResult.Tables[0].Rows[i][0];
                toAdd.ORDER_NUMBER = (string)QueryResult.Tables[0].Rows[i][1];
                toAdd.DESCRIPTION = (string)QueryResult.Tables[0].Rows[i][2];
                toAdd.PATTERN_SN = (string)QueryResult.Tables[0].Rows[i][3];
                toAdd.PATTERN_DESCRIPTION = (string)QueryResult.Tables[0].Rows[i][4];
                toAdd.IS_GOOD = (bool)QueryResult.Tables[0].Rows[i][5];
                toAdd.IS_VERYFICATED = (bool)QueryResult.Tables[0].Rows[i][6];
                toAdd.OpState = 3;
                Result.Add(toAdd);
            }
            return Result;
        }
        #endregion

        #region GetDevicePackageSerialNbr
        public string GetDevicePackageSerialNbr(string DeviceSN)
        {
            return (string)DB.ExecuteScalar("RadioDeviceTesterNew.dbo.imrsc_GetDevicePackageSerialNbr", new DB.Parameter[] {new DB.InParameter("@DEVICE_SN", DeviceSN)});
        }
        #endregion

        #region CheckIfPackageIsClosed
        public bool CheckIfPackageIsClosed(string PackageSN)
        {
            return (bool)DB.ExecuteScalar("RadioDeviceTesterNew.dbo.imrsc_CheckIfPackageIsClosed", new DB.Parameter[] { new DB.InParameter("@PACKAGE_SN", PackageSN) });
        }
        #endregion

        #region GetPackageListForShippingList

        public List<string> GetPackageListForShippingList(List<string> DeviceSnList)
        {
            DataTable deviceSnList = new DataTable();
            deviceSnList.Columns.Add("VALUE", typeof(String));
            foreach(string dsn in DeviceSnList)
            {
                deviceSnList.Rows.Add(dsn);
            }
            return (List<string>)DB.ExecuteProcedure("RadioDeviceTesterNew.dbo.imrsc_GetPackageListForShippingList", new DB.AnalyzeDataSet(GetPackageListForShippingListAnalyzer), new DB.Parameter[] 
            { 
                new DB.InParameter("@DEVICE_SN_LIST", SqlDbType.Structured, deviceSnList)
            }
            );
        }

        #endregion

        #region GetPackageListForShippingListAnalyzer

        private object GetPackageListForShippingListAnalyzer(DataSet QueryResult)
        {
            List<string> Result = new List<string>();
            for (int i = 0; i < QueryResult.Tables[0].Rows.Count; i++)
            {
                Result.Add((string)QueryResult.Tables[0].Rows[i][0]);
            }
            return Result;
        }

        #endregion

        #region GetDeviceChange

        public RDTNDeviceChange[] GetDeviceChange(DateTime StartDate, DateTime EndDate)
        {
            return (RDTNDeviceChange[])DB.ExecuteProcedure(
                   "RadioDeviceTesterNew.dbo.tst_GetDeviceChange",
                   new DB.AnalyzeDataSet(GetDeviceChange),
                   new DB.Parameter[]	{
										new DB.InParameter ("@START_DATE", ConvertTimeZoneToUtcTime(StartDate)),
										new DB.InParameter ("@END_DATE", ConvertTimeZoneToUtcTime(EndDate))	
									}
                   );
        }

        private object GetDeviceChange(DataSet QueryResult)
        {
            if (QueryResult == null || QueryResult.Tables == null || QueryResult.Tables.Count == 0)
                return new List<RDTNDeviceChange>().ToArray();
            List<RDTNDeviceChange> retList = new List<RDTNDeviceChange>();
            foreach (DataRow drItem in QueryResult.Tables[0].Rows)
            {
                RDTNDeviceChange item = new RDTNDeviceChange();
                item.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                item.FACTORY_NUMBER = GetValue<string>(drItem["FACTORY_NUMBER"]);
                item.FIRST_QUARTER = GetValue<string>(drItem["FIRST_QUARTER"]);
                item.SECOND_QUARTER = GetValue<string>(drItem["SECOND_QUARTER"]);
                item.THIRD_QUARTER = GetValue<string>(drItem["THIRD_QUARTER"]);
                item.NAME = GetValue<string>(drItem["NAME"]);
                item.ID_DEVICE_TYPE = GetValue<int>(drItem["ID_DEVICE_TYPE"]);
                retList.Add(item);
            }

            return retList.ToArray();
        }

        #endregion

        #region GetDevice

        public RDTNDeviceChange[] GetDevice(string SerialNbr, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (RDTNDeviceChange[])DB.ExecuteProcedure(
                   "RadioDeviceTesterNew.dbo.tst_LP_GetDevice",
                   new DB.AnalyzeDataSet(GetDevice),
                   new DB.Parameter[]	{
										new DB.InParameter ("@SERIAL_NBR", SerialNbr)
									}, autoTransaction, transactionLevel, commandTimeout
                   );
        }

        private object GetDevice(DataSet QueryResult)
        {
            if (QueryResult == null || QueryResult.Tables == null || QueryResult.Tables.Count == 0)
                return new List<RDTNDeviceChange>().ToArray();
            List<RDTNDeviceChange> retList = new List<RDTNDeviceChange>();
            foreach (DataRow drItem in QueryResult.Tables[0].Rows)
            {
                RDTNDeviceChange item = new RDTNDeviceChange();
                item.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                item.FACTORY_NUMBER = GetValue<string>(drItem["FACTORY_NUMBER"]);
                item.FIRST_QUARTER = GetValue<string>(drItem["FIRST_QUARTER"]);
                item.SECOND_QUARTER = GetValue<string>(drItem["SECOND_QUARTER"]);
                item.THIRD_QUARTER = GetValue<string>(drItem["THIRD_QUARTER"]);
                item.NAME = GetValue<string>(drItem["NAME"]);
                item.ID_DEVICE_TYPE = GetValue<int>(drItem["ID_DEVICE_TYPE"]);
                retList.Add(item);
            }

            return retList.ToArray();
        }

        #endregion

        #region GetDevices

        public RDTNDeviceChange[] GetDevices(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (RDTNDeviceChange[])DB.ExecuteProcedure(
                   "RadioDeviceTesterNew.dbo.tst_LP_GetDevices",
                   new DB.AnalyzeDataSet(GetDevices),
                   new DB.Parameter[]	{
									}, autoTransaction, transactionLevel, commandTimeout
                   );
        }

        private object GetDevices(DataSet QueryResult)
        {
            if (QueryResult == null || QueryResult.Tables == null || QueryResult.Tables.Count == 0)
                return new List<RDTNDeviceChange>().ToArray();
            List<RDTNDeviceChange> retList = new List<RDTNDeviceChange>();
            foreach (DataRow drItem in QueryResult.Tables[0].Rows)
            {
                RDTNDeviceChange item = new RDTNDeviceChange();
                item.SERIAL_NBR = GetValue<long>(drItem["SERIAL_NBR"]);
                item.FACTORY_NUMBER = GetValue<string>(drItem["FACTORY_NUMBER"]);
                item.FIRST_QUARTER = GetValue<string>(drItem["FIRST_QUARTER"]);
                item.SECOND_QUARTER = GetValue<string>(drItem["SECOND_QUARTER"]);
                item.THIRD_QUARTER = GetValue<string>(drItem["THIRD_QUARTER"]);
                item.NAME = GetValue<string>(drItem["NAME"]);
                item.ID_DEVICE_TYPE = GetValue<int>(drItem["ID_DEVICE_TYPE"]);
                retList.Add(item);
            }

            return retList.ToArray();
        }

        #endregion

        #region GetDeviceData

        public RDTNDeviceData[] GetDeviceData(string SerialNbr, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return (RDTNDeviceData[])DB.ExecuteProcedure(
                   "RadioDeviceTesterNew.dbo.imrsc_GetDeviceData",
                   new DB.AnalyzeDataSet(GetDeviceData),
                   new DB.Parameter[]	{
										new DB.InParameter ("@SERIAL_NBR", SerialNbr)
									}, autoTransaction, transactionLevel, commandTimeout
                   );
        }

        private object GetDeviceData(DataSet QueryResult)
        {
            if (QueryResult == null || QueryResult.Tables == null || QueryResult.Tables.Count == 0)
                return new List<RDTNDeviceData>().ToArray();
            List<RDTNDeviceData> retList = new List<RDTNDeviceData>();
            foreach (DataRow drItem in QueryResult.Tables[0].Rows)
            {
                RDTNDeviceData item = new RDTNDeviceData();
                item.ID_DATA_TYPE = GetValue<long>(drItem["ID_DATA_TYPE"]);
                item.INDEX_NBR = GetValue<int>(drItem["INDEX_NBR"]);
                item.VALUE = GetValue(drItem["VALUE"]);                
                retList.Add(item);
            }

            return retList.ToArray();
        }

        #endregion
    }
}
