﻿using System;
using System.Configuration;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace IMR.Suite.Data.DB
{
    #region SqlServerSection
    public class SqlServerSection : ConfigurationSection
    {
        [ConfigurationProperty("SqlServers", IsDefaultCollection = false),
         ConfigurationCollection(typeof(SqlServerCollection), AddItemName = "Server", ClearItemsName = "clearServers", RemoveItemName = "removeServer")]
        public SqlServerCollection SqlServers
        {
            get { return this["SqlServers"] as SqlServerCollection; }
        }
    }
    #endregion
    #region SqlServerElement
    public class SqlServerElement : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name
        {
            get { return this["Name"] as string; }
        }
        [ConfigurationProperty("Server", IsRequired = true)]
        public string Server
        {
            get { return this["Server"] as string; }
        }
        [ConfigurationProperty("DB", IsRequired = true)]
        public string DB
        {
            get { return this["DB"] as string; }
        }
        [ConfigurationProperty("User", IsRequired = false, DefaultValue = null)]
        public string User
        {
            get { return this["User"] as string; }
        }
        [ConfigurationProperty("Pwd", IsRequired = false, DefaultValue = null)]
        public string Pwd
        {
            get { return this["Pwd"] as string; }
        }
        [ConfigurationProperty("Description", IsRequired = false)]
        public string Description
        {
            get { return this["Description"] as string; }
        }
        [ConfigurationProperty("UseTrustedConnection", IsRequired = false, DefaultValue = false)]
        public bool UseTrustedConnection
        {
            get { return (bool)this["UseTrustedConnection"]; }
        }

    }
    #endregion
    #region SqlServerCollection
    public class SqlServerCollection : ConfigurationElementCollection
    {
        public SqlServerElement this[int index]
        {
            get { return base.BaseGet(index) as SqlServerElement; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }
        public new SqlServerElement this[string key]
        {
            get { return base.BaseGet(key) as SqlServerElement; }
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new SqlServerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SqlServerElement)element).Name;
        }
    }
    #endregion
}
