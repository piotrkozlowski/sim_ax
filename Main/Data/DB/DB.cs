﻿//#define USE_SERVER_TIME_ZONE

using System;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using IMR.Suite.Common;

namespace IMR.Suite.Data.DB
{
    public class DB
    {
        private bool m_bDebug = true;

        private TimeZoneInfo timeZoneInfo = null;
        public TimeZoneInfo TimeZoneInfo
        {
#if USE_SERVER_TIME_ZONE
            get { return timeZoneInfo ?? TimeZoneInfo.Local; }
#else
            get { return timeZoneInfo; }
#endif
            set { timeZoneInfo = value; }
        }

        private string sOperatorLogin = null;

        #region Start
        public void Start(string server, string description, string database, string user, string password, bool useTrustedConnection, string operatorLogin)
        {
            this.sServer = server;
            this.sDatabase = database;
            this.sUser = user;
            this.sPassword = password;
            this.bUseTrustedConnection = useTrustedConnection;
            this.sConnectionDescription = description;
            this.sOperatorLogin = operatorLogin;

            if ((String.IsNullOrEmpty(user) || String.IsNullOrEmpty(password)) && useTrustedConnection == false)
                return;

            Initialize(m_bDebug);

            for (int SqlServerConnectRetry = 0; SqlServerConnectRetry < ConnectRetryCount; SqlServerConnectRetry++)
            {
                if (sShutdownEvent.WaitOne(0, false))
                    return;

                if (SqlServerConnectRetry != 0)
                {
                    Log(EventID.DB.ModuleUpRetry, ConnectRetryCount - SqlServerConnectRetry, ReconnectTime.TotalSeconds);
                    Thread.Sleep((int)ReconnectTime.TotalMilliseconds);
                }

                if (sShutdownEvent.WaitOne(0, false))
                    return;

                Console.Write(@"Checking connection to {0}\{1}...", Server, Database);

                bConnected = false;
                try
                {
#if USE_SERVER_TIME_ZONE
                    object timeOffset = ExecuteQuerySql("SELECT DATEPART(tz, SYSDATETIMEOFFSET()) AS [OFFSET]",
                        new AnalyzeDataSet((query) => { return query.Tables[0].Rows[0]["OFFSET"]; }),
                        CommandTimeout: 1);
                    if (timeOffset != null)
                        TimeZoneInfo = TimeZoneInfo.CreateCustomTimeZone("IMR", TimeSpan.FromMinutes(GenericConverter.Parse<int>(timeOffset)), "IMR", "IMR");
                    else
                        TimeZoneInfo = TimeZoneInfo.CreateCustomTimeZone("IMR", TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow), "IMR", "IMR");
#else
                    ExecuteNonQuerySql("SELECT TOP 0 * FROM sysobjects", AutoTransaction: false, CommandTimeout: 1);
#endif
                    bConnected = true;
                }
                catch (Exception Exception)
                {
                    Console.WriteLine(" Failure");
                    Log(EventID.DB.ModuleUpError, Exception.Message);
                }

                if (bConnected)
                {
                    Console.WriteLine(" Success");
                    //Log(ModuleUp, AssemblyWrapper.NameAndVersion, Environment.MachineName);
                    break;
                }
            }
        }
        #endregion

        #region Stop
        public void Stop()
        {
            try
            {
                //Log(ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
            }
            catch
            {
            }
            Deinitialize();
        }
        #endregion

        #region Ustawienia polaczenia z serwerem
        private bool bConnected = false;
        private string sConnectionDescription = "";
        private string sServer = null;
        private string sDatabase = null;
        private string sPersistSecurityInfo = "false";
        private string sUser = null;
        private string sPassword = null;
        private bool bUseTrustedConnection = false;
        private TimeSpan connectTime = TimeSpan.FromSeconds(15);
        private TimeSpan reconnectTime = TimeSpan.FromSeconds(30);
        private int connectRetryCount = 10;

        #region Connected
        public bool Connected
        {
            get { return bConnected; }
        }
        #endregion
        #region Server
        public string Server
        {
            get { return sServer; }
        }
        #endregion
        #region ConnectionDescription
        public string ConnectionDescription
        {
            get { return sConnectionDescription; }
            set { sConnectionDescription = value; }
        }
        #endregion
        #region Database
        public string Database
        {
            get { return sDatabase; }
        }
        #endregion
        #region User
        public string User
        {
            get { return sUser; }
        }
        #endregion
        #region Password
        public string Password
        {
            get { return sPassword; }
        }
        #endregion
        #region UseTrustedConnection
        public bool UseTrustedConnection
        {
            get { return bUseTrustedConnection; }
        }
        #endregion
        #region ConnectTime
        public TimeSpan ConnectTime
        {
            get { return connectTime; }
            set { connectTime = value; }
        }
        #endregion
        #region ConnectRetryCount
        public int ConnectRetryCount
        {
            get { return connectRetryCount; }
            set { connectRetryCount = value; }
        }
        #endregion
        #region ReconnectTime
        public TimeSpan ReconnectTime
        {
            get { return reconnectTime; }
            set { reconnectTime = value; }
        }
        #endregion
        #endregion

        #region Ustawienia ponawiania wykonania operacji w przypadku zakleszczenia
        private readonly int sRetrySleepTime = 25;
        private readonly float sRetrySleepTimeMultiplier = 2;
        #region RetryCount
        private int sRetryCount = 3;
        public int RetryCount
        {
            get { return sRetryCount; }
            set { sRetryCount = value; }
        }
        #endregion
        #region RetryCountForDeadLock
        private int sRetryCountForDeadLock = 1800;
        public int RetryCountForDeadLock
        {
            get { return sRetryCountForDeadLock; }
            set { sRetryCountForDeadLock = value; }
        }
        #endregion
        #region MaxRetryTime
        private int sMaxRetryTime = 60000; //ms
        public int MaxRetryTime
        {
            get { return sMaxRetryTime; }
            set { sMaxRetryTime = value; }
        }
        #endregion
        #region MaxRetryTimeForDeadlock
        private int sMaxRetryTimeForDeadlock = 1000; //ms
        public int MaxRetryTimeForDeadlock
        {
            get { return sMaxRetryTimeForDeadlock; }
            set { sMaxRetryTimeForDeadlock = value; }
        }
        #endregion
        #endregion

        #region Deklaracja poziomu waznosci zapisow zwiazanych z blednym wykonaniem operacji na serwerze
        public uint SeverityCutOffLevel = 7;
        #endregion

        #region Ustawienia zamykania nieuzywanych polaczen
        private readonly int sMaxConnectionIdleMinutes = 2;
        #endregion

        #region Ustawienia liczby polaczen z baza
        private readonly static int sConnectionCount = 20;
        #endregion

        #region Deklaracja TimestampBaseTime i procedur z tym zwiazanych
        public readonly DateTime TimestampBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        public DateTime ConvertToDateTime(double Seconds)
        {
            return TimestampBaseTime.AddSeconds(Seconds);
        }
        public double ConvertToSeconds(DateTime Timestamp)
        {
            return Timestamp.Subtract(TimestampBaseTime).TotalSeconds;
        }
        #endregion

        #region Deklaracja DebugMode
        private bool sDebugMode = false;
        #endregion

        #region Funkcje pomocnicze
        #region SwapBitShift(64bit)
        public Int64 SwapBitShift(Int64 value)
        {
            UInt64 uvalue = (UInt64)value;
            UInt64 swapped = ((0x00000000000000FF) & (uvalue >> 56) |
                                (0x000000000000FF00) & (uvalue >> 40) |
                                (0x0000000000FF0000) & (uvalue >> 24) |
                                (0x00000000FF000000) & (uvalue >> 8) |
                                (0x000000FF00000000) & (uvalue << 8) |
                                (0x0000FF0000000000) & (uvalue << 24) |
                                (0x00FF000000000000) & (uvalue << 40) |
                                (0xFF00000000000000) & (uvalue << 56));
            return (Int64)swapped;
        }
        #endregion
        #region SwapBitShift(32bit)
        public Int32 SwapBitShift(Int32 value)
        {
            UInt32 uvalue = (UInt32)value;
            UInt32 swapped = ((0x000000FF) & (uvalue >> 24) |
                                (0x0000FF00) & (uvalue >> 8) |
                                (0x00FF0000) & (uvalue << 8) |
                                (0xFF000000) & (uvalue << 24));
            return (Int32)swapped;
        }
        #endregion
        #endregion

        #region Naglowki procedur do analizy wynikow z bazy
        public delegate object AnalyzeDataSet(DataSet QueryResult);
        public delegate object AnalyzeDataSetAndParameters(DataSet QueryResult, Parameter[] Parameters);

        public delegate object AnalyzeDataReader(SqlDataReader QueryResult);
        public delegate object AnalyzeDataReaderAndParameters(SqlDataReader QueryResult, Parameter[] Parameters);

        public delegate object AnalyzeParameters(Parameter[] Parameters);
        public delegate object AnalyzeResultAndParameters(Object Result, Parameter[] Parameters);
        #endregion

        #region Deklaracja klas parametrow procedur skladowanych
        #region Parameter
        public abstract class Parameter
        {
            public readonly string Name;
            public readonly SqlDbType Type;
            public readonly int Size;
            public object Value;
            public readonly ParameterDirection Direction;

            public bool IsNull
            {
                get { return Value == DBNull.Value; }
            }

            public Parameter(string pName, SqlDbType pType, object pValue, ParameterDirection pDirection)
                : this(pName, pType, pValue, pDirection, 0)
            {
                //Name = pName;

                //if (pValue != null)
                //{
                //	Value = pValue;
                //	Type = pType;
                //	Direction = pDirection;
                //	Size = 0;
                //}
                //else
                //{
                //	Value = DBNull.Value;
                //	Type = SqlDbType.Int;
                //}
            }

            public Parameter(string pName, SqlDbType pType, object pValue, ParameterDirection pDirection, int pSize)
            {
                Name = pName;
                Type = pType;
                Value = pValue ?? DBNull.Value;
                Direction = pDirection;
                Size = pSize;
            }
        }
        #endregion
        #region InParameter
        public class InParameter : Parameter
        {
            public InParameter(string pName, int pValue) :
                base(pName, SqlDbType.Int, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, int? pValue) :
                base(pName, SqlDbType.Int, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, uint pValue) :
                base(pName, SqlDbType.Int, (int)pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, long pValue) :
                base(pName, SqlDbType.BigInt, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, long? pValue) :
                base(pName, SqlDbType.BigInt, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, ulong pValue) :
                base(pName, SqlDbType.BigInt, (long)pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, double pValue) :
                base(pName, SqlDbType.Float, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, double? pValue) :
                base(pName, SqlDbType.Float, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, string pValue) :
                base(pName, SqlDbType.NVarChar, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, DateTime pValue) :
                base(pName, SqlDbType.DateTime, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, DateTime? pValue) :
                base(pName, SqlDbType.DateTime, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, bool pValue) :
                base(pName, SqlDbType.Bit, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, bool? pValue) :
                base(pName, SqlDbType.Bit, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, byte[] pValue) :
                base(pName, SqlDbType.VarBinary, pValue == null ? (object)DBNull.Value : (object)pValue, ParameterDirection.Input, pValue == null ? 0 : pValue.Length)
            {
            }
            public InParameter(string pName, Guid pGuid) :
                base(pName, SqlDbType.UniqueIdentifier, pGuid, ParameterDirection.Input)
            {
            }

            public InParameter(string pName, SqlDbType pType, object pValue) :
                base(pName, pType, pValue, ParameterDirection.Input)
            {
            }
            public InParameter(string pName, SqlDbType pType, object pValue, int pSize) :
                base(pName, pType, pValue, ParameterDirection.Input, pSize)
            {
            }
        }
        #endregion
        #region OutParameter
        public class OutParameter : Parameter
        {
            public OutParameter(string pName, SqlDbType pType) :
                base(pName, pType, System.DBNull.Value, ParameterDirection.Output)
            {
            }
            public OutParameter(string pName, SqlDbType pType, int pSize) :
                base(pName, pType, System.DBNull.Value, ParameterDirection.Output, pSize)
            {
            }
        }
        #endregion
        #region InOutParameter
        public class InOutParameter : Parameter
        {
            public InOutParameter(string pName, int pValue) :
                base(pName, SqlDbType.Int, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, uint pValue) :
                base(pName, SqlDbType.Int, (int)pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, long pValue) :
                base(pName, SqlDbType.BigInt, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, ulong pValue) :
                base(pName, SqlDbType.BigInt, (long)pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, double pValue) :
                base(pName, SqlDbType.Float, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, string pValue) :
                base(pName, SqlDbType.NVarChar, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, DateTime pValue) :
                base(pName, SqlDbType.DateTime, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, bool pValue) :
                base(pName, SqlDbType.Int, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, long? pValue) :
                base(pName, SqlDbType.BigInt, pValue, ParameterDirection.InputOutput)
            {
            }
            public InOutParameter(string pName, int? pValue) :
                base(pName, SqlDbType.Int, pValue, ParameterDirection.InputOutput)
            {
            }

            public InOutParameter(string pName, SqlDbType pType, object pValue) :
                base(pName, pType, pValue, ParameterDirection.InputOutput)
            {
            }
        }
        #endregion
        #region InNullParameter
        public class InNullParameter : InParameter
        {
            public InNullParameter(string pName) :
                base(pName, SqlDbType.Int, DBNull.Value)
            {
            }
        }
        #endregion
        #region ReturnValue
        public class ReturnValue : Parameter
        {
            public ReturnValue(SqlDbType pType) :
                base("RETURN_VALUE", pType, System.DBNull.Value, ParameterDirection.ReturnValue)
            {
            }
        }
        #endregion
        #endregion

        #region Deklaracje zwiazane z polaczeniami z baza
        private struct Connection
        {
            public SqlConnection mSqlConnection;
            public SqlCommand mSqlCommand;
            public volatile bool mUsed;
            public DateTime mLastUsedAt;
            public int mTimeout;
        }

        private Connection[] sConnections = new Connection[sConnectionCount];

        //semafor zliczajacy liczbe wolnych polaczen
        private Semaphore sUnusedConnections = new Semaphore(sConnectionCount, sConnectionCount);

        //sekcja krytyczna do ochrony tabeli
        private object sConnectionsCS = (int)0;
        #endregion

        #region Rezerwowanie i zwalanianie polaczenia z baza
        #region Rezerwowanie polaczenia
        private SqlCommand GetCommand(ref int Cookie)
        {
            return GetCommand(ref Cookie, true);
        }
        //ta funcja zostaje dla kompatybilności wstecz
        private SqlCommand GetCommand(ref int Cookie, bool AutoTransaction)
        {
            return GetCommand(ref Cookie, AutoTransaction, IsolationLevel.Serializable);
        }
        private SqlCommand GetCommand(ref int Cookie, bool AutoTransaction = true, IsolationLevel isolationLevel = IsolationLevel.Serializable)
        {
            SqlCommand Command;

            bool Open = false;
            int ConnectionIndex = -1;

            //oczekiwanie na zwolnienie elementu tablicy sConnections
            sUnusedConnections.WaitOne();

            if (sShutdownEvent.WaitOne(0, false))
            {
                sUnusedConnections.Release();
                throw new IMRDBException("DB.GetCommand () BOKO2 Shutting down");
            }

            try
            {
                //sekcja krytyczna, chroniaca tablice
                Monitor.Enter(sConnectionsCS);

                for (ConnectionIndex = 0; ConnectionIndex < sConnectionCount; ConnectionIndex++)
                    if (!sConnections[ConnectionIndex].mUsed)
                        break;

                if (ConnectionIndex >= sConnectionCount)
                    throw new IMRDBException("DB.GetCommand () i >= sConnectionCount");

                //po ustawieniu na true wiemy, ze nikt nie bedzie uzywal tego
                //elementu tablicy, wiec mozemy opuscic sekcje krytyczna
                //dzieki czemu dlugotrwale nawiazywanie polaczenia z serwerem nie bedzie
                //wstrzymywalo korzystania z pozostalych polaczen
                sConnections[ConnectionIndex].mUsed = true;
            }
            finally
            {
                Monitor.Exit(sConnectionsCS);
            }

            try
            {
                if (sConnections[ConnectionIndex].mSqlConnection != null)
                {
                    Command = sConnections[ConnectionIndex].mSqlCommand;
                }
                else
                {
                    if (bUseTrustedConnection && String.IsNullOrEmpty(sUser) == false && String.IsNullOrEmpty(sPassword) == false && sUser.Contains(@"\"))
                    {
                        string[] userData = sUser.Split('\\');
                        if (userData.Length < 2)
                            return null;
                        using (ImpersonationHelper impersonation = new ImpersonationHelper(userData[1], userData[0], sPassword))
                        {
                            sConnections[ConnectionIndex].mSqlConnection = new SqlConnection(String.Format("server={0}; initial catalog={1}; persist security info={2}; Trusted_Connection=True; Application Name={3}; Connect Timeout={4}",
                                sServer, sDatabase, sPersistSecurityInfo, sConnectionDescription, connectTime.TotalSeconds));

                            sConnections[ConnectionIndex].mSqlConnection.Open();
                        }
                    }
                    else
                    {
                        if (bUseTrustedConnection)
                                sConnections[ConnectionIndex].mSqlConnection = new SqlConnection(String.Format("server={0}; initial catalog={1}; persist security info={2}; Trusted_Connection=True; Application Name={3}; Connect Timeout={4}",
                                    sServer, sDatabase, sPersistSecurityInfo, sConnectionDescription, connectTime.TotalSeconds));
                        else
                            sConnections[ConnectionIndex].mSqlConnection = new SqlConnection(String.Format("server={0}; initial catalog={1}; persist security info={2}; user id={3}; pwd={4}; Application Name={5}; Connect Timeout={6}",
                                sServer, sDatabase, sPersistSecurityInfo, sUser, sPassword, sConnectionDescription, connectTime.TotalSeconds));
                        sConnections[ConnectionIndex].mSqlConnection.Open();
                    }

                    if (!String.IsNullOrWhiteSpace(this.sOperatorLogin))
                    {
                        try
                        {
                            SqlCommand contextInfoCommand = new SqlCommand();
                            contextInfoCommand.Connection = sConnections[ConnectionIndex].mSqlConnection;
                            contextInfoCommand.CommandText = String.Format(@"DECLARE @context_info varbinary(100) = CAST(N'{0}' AS varbinary(100)) SET CONTEXT_INFO @context_info", this.sOperatorLogin);
                            contextInfoCommand.CommandType = CommandType.Text;
                            contextInfoCommand.ExecuteNonQuery();
                        }
                        catch { }
                    }

                    Command = new SqlCommand();
                    Command.Connection = sConnections[ConnectionIndex].mSqlConnection;
                    sConnections[ConnectionIndex].mSqlCommand = Command;
                }

                if (AutoTransaction || isolationLevel != IsolationLevel.Serializable)
                {
                    //Command.Transaction = Command.Connection.BeginTransaction(IsolationLevel.Serializable, "IMR_Transaction");
                    Command.Transaction = Command.Connection.BeginTransaction(isolationLevel, "IMR_Transaction");
                    //Command.CommandText = "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; BEGIN TRANSACTION; --preparing to run";
                    //Command.ExecuteNonQuery();
                }

                sConnections[ConnectionIndex].mTimeout = Command.CommandTimeout;
                Cookie = ConnectionIndex;
                Open = true;
            }
            finally
            {
                //jesli w czasie nawiazywania polaczenia lub inicjalizacji poszedl wyjatek
                //to trzeba zwolnic element tablicy, do tej operacji nie trzeba sekcji kryt.
                //jesli zadnego wyjatku nie bylo - ponizsza linia i tak niczego nie zepsuje
                sConnections[ConnectionIndex].mUsed = Open;

                //jesli nie udalo sie otworzyc polaczenia - trzeba zwiekszyc liczbe
                //dostepnych elementow tablicy
                if (!Open)
                {
                    sConnections[ConnectionIndex].mSqlConnection = null;
                    sUnusedConnections.Release();
                }
            }
            return Command;
        }
        #endregion
        #region Zwalnianie polaczenia
        private void CommitAndRelease(int Cookie)
        {
            Release(Cookie, true, false, null);
        }

        private void RollbackAndRelease(int Cookie)
        {
            Release(Cookie, false, true, null);
        }

        private void RollbackAndRelease(int Cookie, Exception Exception)
        {
            Release(Cookie, false, true, Exception);
        }

        private void Release(int Cookie, bool Commit, bool ForceClose, Exception Exception)
        {
            if (Cookie < 0)
                return;

            bool ReleaseConnection = false;

            try
            {
                Monitor.Enter(sConnectionsCS);

                if (!(0 <= Cookie && Cookie < sConnectionCount))
                    throw new IMRDBException("DB.Release () !(0 <= Cookie && Cookie < sConnectionCount)");

                if (!sConnections[Cookie].mUsed)
                    throw new IMRDBException("DB.Release () !sConnections [Cookie].mUsed");

                ReleaseConnection = true;

#if UTC
				if (ForceClose)
					sConnections[Cookie].mLastUsedAt = DateTime.UtcNow.AddMinutes(-2 * sMaxConnectionIdleMinutes);
				else
					sConnections[Cookie].mLastUsedAt = DateTime.UtcNow;
#else
                if (ForceClose)
                    sConnections[Cookie].mLastUsedAt = DateTime.Now.AddMinutes(-2 * sMaxConnectionIdleMinutes);
                else
                    sConnections[Cookie].mLastUsedAt = DateTime.Now;
#endif

                SqlCommand Command = sConnections[Cookie].mSqlCommand;

                string CommandText = "";
                string ParametersText = "";

                if (!Commit)
                {
                    CreateCommandDump(Command, out CommandText, out ParametersText);
                }

                Command.Parameters.Clear();
                Command.CommandTimeout = sConnections[Cookie].mTimeout;
                ////Command.CommandType = CommandType.Text;

                if (Commit)
                {
                    if (Command.Transaction != null)
                    //if (AutoTransaction)
                    {
                        //Command.CommandText = "COMMIT; --commiting";
                        //Command.ExecuteNonQuery();
                        Command.Transaction.Commit();
                    }
                }
                else
                {
                    try
                    {
                        if (Command.Transaction != null)
                        {
                            //Command.CommandText = "ROLLBACK; --canceling";
                            //Command.ExecuteNonQuery();
                            Command.Transaction.Rollback();
                        }
                    }
                    catch (SqlException RollbackException)
                    {
                        if (!NoTransaction(RollbackException))
                        {
                            if (Exception != null)
                                throw Exception;
                            else
                                throw;
                        }
                    }
                    if (Exception != null)
                    {
                        LogException(Command, Exception, CommandText, ParametersText);

                        throw Exception;
                    }

                }
            }
            finally
            {
                if (ReleaseConnection)
                {
                    sConnections[Cookie].mUsed = false;
                    sUnusedConnections.Release();
                }

                Monitor.Exit(sConnectionsCS);
                sCheckAndReleaseConnectionsEvent.Set();
            }
        }
        #endregion
        #region CreateCommandDump
        private void CreateCommandDump(SqlCommand Command, out string CommandText, out string ParametersText)
        {
            if (Command.CommandType == CommandType.Text)
            {
                CommandText = string.Format("Command Text: {0}", Command.CommandText);
                ParametersText = "";
                return;
            }

            CommandText = string.Format("Stored Procedure: {0}", Command.CommandText);

            StringBuilder ParametersDumpBuilder = new StringBuilder();

            bool FirstParameter = true;
            foreach (SqlParameter Parameter in Command.Parameters)
            {
                if (FirstParameter)
                    ParametersDumpBuilder.Append(". Parameters: ");
                else
                    ParametersDumpBuilder.Append(", ");

                if (Parameter.Value == DBNull.Value)
                    ParametersDumpBuilder.Append(string.Format("{0}=NULL", Parameter.ParameterName));
                else
                    ParametersDumpBuilder.Append(string.Format("{0}='{1}'", Parameter.ParameterName, Parameter.Value));

                FirstParameter = false;
            }

            ParametersText = ParametersDumpBuilder.ToString();
        }
        #endregion
        #region LogException
        private void LogException(SqlCommand Command, Exception Exception, string CommandText, string ParametersText)
        {
            try
            {
                string Message = string.Format(
                    "{0}{1} {2}{3}",
                    Exception.Message,
                    Exception.Message.EndsWith(".") ? "" : ".",
                    CommandText,
                    ParametersText
                    );

                if (sDebugMode)
                {
                    //najlepiej w lokalnym czasie - tak sie lepiej czyta
                    DateTime Now = DateTime.Now;
                    Console.WriteLine();
                    Console.WriteLine(
                        "{0:0000}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}.{6:000}:",
                        Now.Year,
                        Now.Month,
                        Now.Day,
                        Now.Hour,
                        Now.Minute,
                        Now.Second,
                        Now.Millisecond
                        );
                    Console.WriteLine(CommandText);

                    if (ParametersText.Length > 2)
                        Console.WriteLine(ParametersText.Remove(0, 2));

                    Console.WriteLine(string.Format("Message: {0}", Exception.Message));
                }

            }
            catch
            {
            }
        }
        #endregion
        #endregion

        #region Watek roboczy zwalniajacy nieuzywane polaczenia
        private Thread sThread;

        //event do poinformowania watku o wylaczaniu modulu
        private ManualResetEvent sShutdownEvent = new ManualResetEvent(false);
        //event do poinformowania watku o sprawdzeniu i ewentualnym zwolnieniu nieuzywanych polaczen
        private AutoResetEvent sCheckAndReleaseConnectionsEvent = new AutoResetEvent(false);

        public void Initialize(bool DebugMode)
        {
            sThread = new Thread(new ThreadStart(WorkerThreadProc));
            sThread.CurrentCulture = Thread.CurrentThread.CurrentCulture;
            sThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
            sThread.IsBackground = true;
            sThread.Start();

            sDebugMode = DebugMode;
        }

        public void Deinitialize()
        {
            if (sThread == null)
                return;

            sShutdownEvent.Set();
        }

        private void WorkerThreadProc()
        {
            bool ShuttingDown = false;
            WaitHandle[] Events = new WaitHandle[] { sShutdownEvent, sCheckAndReleaseConnectionsEvent };
            while (true)
            {																//	250 to 1000 / 4 :)
                //ShuttingDown = sShutdownEvent.WaitOne(sMaxConnectionIdleMinutes * 60 * 250, false);
                //if (ShuttingDown)
                //	for (int i = 0; i < sConnectionCount; i++)
                //		sUnusedConnections.WaitOne();
                int waitRes = WaitHandle.WaitAny(Events, sMaxConnectionIdleMinutes * 60 * 250, false);
                switch (waitRes)
                {
                    case 0:		// ShutDownEvent
                        ShuttingDown = true;
                        for (int i = 0; i < sConnectionCount; i++)
                            sUnusedConnections.WaitOne();
                        break;
                    case 1:		// CheckAndReleaseConnectionsEvent
                    default:	// timeout
                        break;
                }

                try
                {
                    Monitor.Enter(sConnectionsCS);
#if UTC
					DateTime MinLastUsedTime = DateTime.UtcNow.AddMinutes(-sMaxConnectionIdleMinutes);
#else
                    DateTime MinLastUsedTime = DateTime.Now.AddMinutes(-sMaxConnectionIdleMinutes);
#endif
                    for (int i = 0; i < sConnectionCount; i++)
                    {
                        if (sConnections[i].mUsed)
                            continue;

                        if (sConnections[i].mSqlConnection == null)
                            continue;

                        if (!ShuttingDown && sConnections[i].mLastUsedAt >= MinLastUsedTime)
                            continue;

                        //rezerwujemy sobie element tablicy na wylacznosc
                        sConnections[i].mUsed = true;

                        SqlConnection Connection = sConnections[i].mSqlConnection;

                        sConnections[i].mSqlConnection = null;
                        sConnections[i].mSqlCommand = null;
                        sConnections[i].mUsed = false;

                        Monitor.Exit(sConnectionsCS);

                        try
                        {
                            SqlConnection.ClearPool(Connection);
                            Connection.Close();
                            Connection.Dispose();
                            Connection = null;
                        }
                        finally
                        {
                            Monitor.Enter(sConnectionsCS);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(sConnectionsCS);
                }

                if (ShuttingDown)
                    break;
            }

            //dzieki tej linii zadania zlozone po wlaczeniu deinitialize beda mogly przejsc
            //w GetCommand () linie oczekujaca na zwolnienie polaczenia, po czym nawiazanie
            //polaczenia sie nie uda. bez tej linii czekalyby w nieskonczonosc
            sUnusedConnections.Release();
        }
        #endregion

        #region Analiza wyjatkow rzucanych przez baze
        private bool NoTransaction(SqlException Exception)
        {
            if (Exception.Number == 3902)
                return true; //wykonano commit poza tranzakcja

            if (Exception.Number == 3903)
                return true; //wykonano rollback poza tranzakcja

            return false;

            //mozna tez wykorzystac ponizszy test, jednak wymaga on, by odpowiedni wpis
            //w bazie master w tabeli sysmessages byl zgodny z ponizszym strongiem

            //return Exception.Message.EndsWith ("request has no corresponding BEGIN TRANSACTION.");
        }

        //sprawdza, czy wyjatek oznacza deadlock'a
        public bool Deadlocked(Exception Exception)
        {
            SqlException Exception2 = Exception as SqlException;
            if (Exception2 == null)
                return false;

            if (Exception2.Number == 1205)
                return true;

            return false;

            //mozna tez wykorzystac ponizszy test, jednak wymaga on, by odpowiedni wpis
            //w bazie master w tabeli sysmessages byl zgodny z ponizszym strongiem

            //return Exception.Message.EndsWith ("was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction.");
        }
        #endregion

        #region Klasa do jednokrotnego uruchamienia komendy
        private class CommandExecute
        {
            public CommandExecute(bool ReturnsScalar)
            {
                mReturnsScalar = ReturnsScalar;
            }

            public CommandExecute(Parameter[] Parameters, bool ReturnsScalar)
            {
                mParameters = Parameters;

                mReturnsScalar = ReturnsScalar;
            }

            public CommandExecute(Parameter[] Parameters, bool ReturnsScalar, AnalyzeResultAndParameters ClientProcedure)
            {
                mParameters = Parameters;

                mReturnsScalar = ReturnsScalar;

                mClientProcedure_ResultAndParameters = ClientProcedure;
            }

            public CommandExecute(AnalyzeDataSet ClientProcedure)
            {
                mClientProcedure_DataSet = ClientProcedure;
            }

            public CommandExecute(Parameter[] Parameters, AnalyzeDataSetAndParameters ClientProcedure)
            {
                mParameters = Parameters;

                mClientProcedure_DataSetAndParameters = ClientProcedure;
            }

            public CommandExecute(AnalyzeDataReader ClientProcedure)
            {
                mClientProcedure_DataReader = ClientProcedure;
            }

            public CommandExecute(Parameter[] Parameters, AnalyzeDataReaderAndParameters ClientProcedure)
            {
                mParameters = Parameters;

                mClientProcedure_DataReaderAndParameters = ClientProcedure;
            }

            public CommandExecute(Parameter[] Parameters, AnalyzeParameters ClientProcedure)
            {
                mParameters = Parameters;

                mClientProcedure_Parameters = ClientProcedure;
            }

            public object Execute(SqlCommand Command, int MaxRetryTime = 60000, int RetrySTime = 25, float RetrySleepTimeMultiplier = 2, int RetryDeadLockCount = 40)
            {
                #region wykonanie polecenia zwracajacego skalar
                if (mReturnsScalar)
                {
                    object Result = Command.ExecuteScalar();

                    ReadParameterValues(Command);

                    if (mClientProcedure_ResultAndParameters != null)
                        return mClientProcedure_ResultAndParameters(Result, mParameters);

                    return Result;
                }
                #endregion
                #region wykonanie polecenia i zbuforowanie wynikow w DataSet
                if (mClientProcedure_DataSet != null ||
                    mClientProcedure_DataSetAndParameters != null)
                {
                    SqlDataAdapter DataAdapter = new SqlDataAdapter(Command);
                    DataSet Result = new DataSet();

                    DateTime startTime = DateTime.Now;

                    int RetryNumForDeadLock = 0;
                    maxRetryTime = MaxRetryTime;
                    sRetrySleepTime = RetrySTime;
                    sRetrySleepTimeMultiplier = RetrySleepTimeMultiplier;
                    retryCountForDeadLock = RetryDeadLockCount;

                    while (true)
                    {
                        try
                        {
                            DataAdapter.Fill(Result);
                            break;
                        }
                        catch (ThreadAbortException ex)
                        {
                            throw new ThreadInterruptedException();
                        }
                        catch (SqlException ex)
                        {
                            if (ex.Number == 0)
                                throw new ThreadInterruptedException();
                            else if (ex.InnerException is Win32Exception && (ex.InnerException as Win32Exception).NativeErrorCode == 258) //timeout
                                throw ex;
                            else if (Deadlocked(ex))
                            {
                                if ((++RetryNumForDeadLock > retryCountForDeadLock))
                                {
                                    throw ex;
                                }
                                Thread.Sleep(RetrySleepTime(RetryNumForDeadLock));
                            }
                            else
                                throw ex;
                        }
                    }
#if DEBUG
                    try
                    {
                        TimeSpan procedureDuration = DateTime.Now - startTime;
                        StringBuilder strParameters = new StringBuilder();
                        foreach (SqlParameter parameter in Command.Parameters)
                        {
                            if (parameter.SqlDbType == SqlDbType.Structured)
                            {
                                if (parameter.Value is DataTable)
                                {
                                    if ((parameter.Value as DataTable).Rows.Count > 0)
                                        strParameters.AppendFormat("UDTT {0}:{1}; ", parameter.ParameterName, (parameter.Value as DataTable).Rows.Count);
                                }
                            }
                            else
                            {
                                if (parameter.Value != null && parameter.Value != DBNull.Value)
                                    strParameters.AppendFormat("{0}:{1}; ", parameter.ParameterName, parameter.Value);
                            }
                        }
                        #region StackTrace
                        List<StackFrame> stackFrames = new StackTrace().GetFrames().ToList();  // get method calls (frames)
                        int level = stackFrames.Count(f => f.GetMethod().Name == "LoadNavigationProperties");

                        StringBuilder levelStr = new StringBuilder();
                        for (int i = 0; i < level; i++)
                        {
                            levelStr.Append("->");
                        }
                        #endregion
                        System.Diagnostics.Debug.WriteLine("{0} {1}[ms] | {2, -30} | {3, -10}", levelStr, procedureDuration.TotalMilliseconds, Command.CommandText, strParameters);
                    }
                    catch (Exception)
                    {
                        //nie chcemy tutaj żadnych wyjatkow
                    }
#endif

                    if (mClientProcedure_DataSet != null)
                    {
                        object QueryResult = mClientProcedure_DataSet(Result);

                        ReadParameterValues(Command);

                        return QueryResult;
                    }

                    ReadParameterValues(Command);

                    return mClientProcedure_DataSetAndParameters(Result, mParameters);
                }
                #endregion
                #region wykonanie polecenia i odebranie ich za pomoca SqlDataReader
                if (mClientProcedure_DataReader != null ||
                    mClientProcedure_DataReaderAndParameters != null)
                {
                    SqlDataReader DataReader = Command.ExecuteReader();
                    object Result;

                    try
                    {
                        if (mClientProcedure_DataReader != null)
                        {
                            Result = mClientProcedure_DataReader(DataReader);
                        }
                        else
                        {
                            ReadParameterValues(Command);

                            Result = mClientProcedure_DataReaderAndParameters(DataReader, mParameters);
                        }
                    }
                    finally
                    {
                        DataReader.Close();
                    }

                    return Result;
                }
                #endregion
                //wykonanie polecenia nie zwracajacego danych
                //ewentualnie tylko przez parametry output
                Command.ExecuteNonQuery();

                ReadParameterValues(Command);

                if (mClientProcedure_Parameters != null)
                    return mClientProcedure_Parameters(mParameters);

                return null;
            }

            private void ReadParameterValues(SqlCommand Command)
            {
                if (mParameters == null)
                    return;

                SqlParameterCollection SqlParameters = Command.Parameters;

                for (int i = 0; i < mParameters.Length; i++)
                {
                    SqlParameter SqlParam = SqlParameters[i];
                    if (SqlParam.Direction != ParameterDirection.Input)
                        mParameters[i].Value = SqlParam.Value;
                }
            }

            //sprawdza, czy wyjatek oznacza deadlock'a
            public bool Deadlocked(Exception Exception)
            {
                SqlException Exception2 = Exception as SqlException;
                if (Exception2 == null)
                    return false;

                if (Exception2.Number == 1205)
                    return true;

                return false;

                //mozna tez wykorzystac ponizszy test, jednak wymaga on, by odpowiedni wpis
                //w bazie master w tabeli sysmessages byl zgodny z ponizszym strongiem

                //return Exception.Message.EndsWith ("was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction.");
            }

            public int RetrySleepTime(int RetryNum)
            {
                int retrySleepTime = (int)(sRetrySleepTime * Math.Pow(sRetrySleepTimeMultiplier, RetryNum));
                if (retrySleepTime > maxRetryTime)
                    retrySleepTime = maxRetryTime;
                return retrySleepTime;
            }

            private readonly bool mReturnsScalar = false;
            private readonly DB.Parameter[] mParameters = null;
            private readonly AnalyzeDataSet mClientProcedure_DataSet = null;
            private readonly AnalyzeDataSetAndParameters mClientProcedure_DataSetAndParameters = null;
            private readonly AnalyzeDataReader mClientProcedure_DataReader = null;
            private readonly AnalyzeDataReaderAndParameters mClientProcedure_DataReaderAndParameters = null;
            private readonly AnalyzeParameters mClientProcedure_Parameters = null;
            private readonly AnalyzeResultAndParameters mClientProcedure_ResultAndParameters = null;
            private int maxRetryTime = 60000; // ms
            private int sRetrySleepTime = 25;
            private float sRetrySleepTimeMultiplier = 2;
            private int retryCountForDeadLock = 40;
        }
        #endregion

        #region Procedura do uruchamienia komendy i ewentualnego powtarzania w przypadku zakleszczenia
        private void PrepareToRerun(SqlCommand Command)
        {
            //SqlCommand ResetCommand = new SqlCommand ("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; BEGIN TRANSACTION; --preparing to rerun", Command.Connection);
            //ResetCommand.ExecuteNonQuery ();
        }

        public int RetrySleepTime(int RetryNum)
        {
            int retrySleepTime = (int)(sRetrySleepTime * Math.Pow(sRetrySleepTimeMultiplier, RetryNum));
            if (retrySleepTime > MaxRetryTime || retrySleepTime < 0)
                retrySleepTime = MaxRetryTime;
            return retrySleepTime;
        }
        public int RetrySleepTimeForDeadlock(int RetryNum)
        {
            int retrySleepTime = (int)(sRetrySleepTime * Math.Pow(sRetrySleepTimeMultiplier, RetryNum));
            if (retrySleepTime > MaxRetryTimeForDeadlock || retrySleepTime < 0)
                retrySleepTime = MaxRetryTimeForDeadlock;
            return retrySleepTime;
        }

        private object Execute(CommandExecute Command, SqlCommand SqlCommand)
        {
            object Result;

            int RetryNum = 0;
            while (true)
            {
                try
                {
                    Result = Command.Execute(SqlCommand);
                    break;
                }
                catch (SqlException Exception)
                {
                    if ((++RetryNum > sRetryCount) || !Deadlocked(Exception))
                        throw;

                    //!!!PrepareToRerun (SqlCommand);
                    Thread.Sleep(RetrySleepTime(RetryNum));
                    continue;
                }
            }

            return Result;
        }
        #endregion

        #region Advanced
        public class Advanced
        {
            private DB DB;
            private bool mInitialized;
            private int mCookie = -1;
            private SqlCommand mCommand;
            private bool AutoTransaction = true;

            #region Constructor
            public Advanced(DB db)
            {
                this.DB = db;
            }
            #endregion
            #region Destructor
            ~Advanced()
            {
                if (mInitialized)
                    Rollback();
            }
            #endregion

            #region GetCommand
            public SqlCommand GetCommand()
            {
                return GetCommand(true);
            }
            public SqlCommand GetCommand(bool AutoTransaction)
            {
                if (mInitialized)
                    throw new IMRDBException("DB.Advanced.GetCommand () mInitialized");

                this.mCommand = DB.GetCommand(ref mCookie, AutoTransaction);
                this.mInitialized = true;
                this.AutoTransaction = AutoTransaction;

                return mCommand;
            }
            #endregion
            #region Commit
            public void Commit()
            {
                if (!mInitialized)
                    throw new IMRDBException("DB.Advanced.Commit () !mInitialized");

                this.mInitialized = false;
                DB.CommitAndRelease(mCookie);//, AutoTransaction);
            }
            #endregion
            #region Rollback
            public void Rollback()
            {
                if (mInitialized)
                {
                    this.mInitialized = false;
                    DB.RollbackAndRelease(mCookie);//, AutoTransaction);
                }
            }
            #endregion

            #region Deadlocked
            public bool Deadlocked(Exception Exception)
            {
                return DB.Deadlocked(Exception);
            }
            #endregion
            #region CreateCommandDump
            public void CreateCommandDump(SqlCommand Command, out string CommandText, out string ParametersText)
            {
                DB.CreateCommandDump(Command, out CommandText, out ParametersText);
            }
            #endregion
            #region LogException
            public void LogException(SqlCommand Command, Exception Exception, string CommandText, string ParametersText)
            {
                DB.LogException(Command, Exception, CommandText, ParametersText);
            }
            #endregion
        }
        #endregion

        #region Metoda Log
        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(EventData, Parameters);
        }

        #endregion

        #region Uruchamianie lancuchow jako polecen SQL
        public void ExecuteNonQuerySql(string Query)
        {
            PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(false));
        }

        public void ExecuteNonQuerySql(string Query, bool AutoTransaction)
        {
            PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(false), AutoTransaction);
        }

        public void ExecuteNonQuerySql(string Query, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(false), AutoTransaction, IsolationLevel, CommandTimeout);
        }

        public object ExecuteQuerySql(string Query, AnalyzeDataSet ClientProcedure)
        {
            return PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(ClientProcedure));
        }

        public object ExecuteQuerySql(string Query, AnalyzeDataSet ClientProcedure, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }

        public object ExecuteQuerySql(string Query, AnalyzeDataReader ClientProcedure)
        {
            return PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(ClientProcedure));
        }

        public object ExecuteScalarSql(string Query)
        {
            return PrepareAndExecute(CommandType.Text, Query, null, new CommandExecute(true));
        }
        #endregion

        #region Uruchamianie procedur skladowanych nie zwracajacych tabel
        #region ExecuteNonQueryProcedure(ProcedureName, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public void ExecuteNonQueryProcedure(string ProcedureName)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(false));
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, bool AutoTransaction)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(false), AutoTransaction);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, int CommandTimeout)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(false), CommandTimeout);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, bool AutoTransaction, int CommandTimeout)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(false), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, bool AutoTransaction, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(false), AutoTransaction, IsolationLevel, CommandTimeout);
        }

        #endregion
        #region ExecuteNonQueryProcedure(ProcedureName, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false));
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, SqlCommand sqlCommand = null)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false), true, IsolationLevel.Serializable, 0, sqlCommand);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, bool AutoTransaction)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false), AutoTransaction);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, int CommandTimeout)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false), CommandTimeout);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public void ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, false), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteNonQueryProcedure(ProcedureName, Parameters, AnalyzeParameters ClientProcedure, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, AnalyzeParameters ClientProcedure)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure));
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, AnalyzeParameters ClientProcedure, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, AnalyzeParameters ClientProcedure, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), CommandTimeout);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, AnalyzeParameters ClientProcedure, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, Parameter[] Parameters, AnalyzeParameters ClientProcedure, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteNonQueryProcedure(ProcedureName, AnalyzeParameters ClientProcedure, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteNonQueryProcedure(string ProcedureName, AnalyzeParameters ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure));
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, AnalyzeParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, AnalyzeParameters ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), CommandTimeout);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, AnalyzeParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteNonQueryProcedure(string ProcedureName, AnalyzeParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #endregion

        #region Uruchamianie procedur skladowanych zwracajacych tabele
        #region ExecuteProcedure(ProcedureName, AnalyzeDataSet ClientProcedure, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteProcedure(ProcedureName, AnalyzeDataSet ClientProcedure, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, SqlCommand sqlCommand = null)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), true, IsolationLevel.Serializable, 0, sqlCommand);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSet ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0, SqlCommand sqlCommand = null)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout, sqlCommand);
        }
        #endregion
        #region ExecuteProcedure(ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Paramaeters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataSetAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteProcedure(ProcedureName, AnalyzeDataReader ClientProcedure, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteProcedure(ProcedureName, AnalyzeDataReader ClientProcedure, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReader ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteProcedure(ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure));
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteProcedure(string ProcedureName, AnalyzeDataReaderAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #endregion

        #region Uruchamianie procedur skladowanych zwracajacych skalar
        #region ExecuteScalar(ProcedureName, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteScalar(string ProcedureName)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true));
        }
        public object ExecuteScalar(string ProcedureName, SqlCommand sqlCommand = null)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true), true, IsolationLevel.Serializable, 0, sqlCommand);
        }
        public object ExecuteScalar(string ProcedureName, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true), AutoTransaction);
        }
        public object ExecuteScalar(string ProcedureName, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true), CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, null, new CommandExecute(true), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteScalar(ProcedureName, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true));
        }
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters, SqlCommand sqlCommand = null)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true), true, IsolationLevel.Serializable, 0, sqlCommand);
        }
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true), AutoTransaction);
        }
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true), CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #region ExecuteScalar(ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameters, <AutoTransaction>, <IsolationLevel>, <CommandTimeout>)
        public object ExecuteScalar(string ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameter[] Parameters)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true, ClientProcedure));
        }
        public object ExecuteScalar(string ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true, ClientProcedure), AutoTransaction);
        }
        public object ExecuteScalar(string ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameter[] Parameters, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true, ClientProcedure), CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction, int CommandTimeout)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true, ClientProcedure), AutoTransaction: AutoTransaction, CommandTimeout: CommandTimeout);
        }
        public object ExecuteScalar(string ProcedureName, AnalyzeResultAndParameters ClientProcedure, Parameter[] Parameters, bool AutoTransaction = true, IsolationLevel IsolationLevel = IsolationLevel.Serializable, int CommandTimeout = 0)
        {
            return PrepareAndExecute(CommandType.StoredProcedure, ProcedureName, Parameters, new CommandExecute(Parameters, true, ClientProcedure), AutoTransaction, IsolationLevel, CommandTimeout);
        }
        #endregion
        #endregion

        #region Procedura wykonujaca operacje na bazie
        private object PrepareAndExecute(
            CommandType CommandType,
            string CommandText,
            Parameter[] Parameters,
            CommandExecute CommandExecute
            )
        {
            return PrepareAndExecute(CommandType, CommandText, Parameters, CommandExecute, AutoTransaction: true);
        }

        private object PrepareAndExecute(
            CommandType CommandType,
            string CommandText,
            Parameter[] Parameters,
            CommandExecute CommandExecute,
            bool AutoTransaction
            )
        {
            return PrepareAndExecute(CommandType, CommandText, Parameters, CommandExecute, AutoTransaction: AutoTransaction, CommandTimeout: 0);
        }

        private object PrepareAndExecute(
            CommandType CommandType,
            string CommandText,
            Parameter[] Parameters,
            CommandExecute CommandExecute,
            int CommandTimeout
            )
        {
            return PrepareAndExecute(CommandType, CommandText, Parameters, CommandExecute, AutoTransaction: true, CommandTimeout: CommandTimeout);
        }

        private object PrepareAndExecute(
            CommandType CommandType,
            string CommandText,
            Parameter[] Parameters,
            CommandExecute CommandExecute,
            bool AutoTransaction = true,
            IsolationLevel IsolationLevel = IsolationLevel.Serializable,
            int CommandTimeout = 0,
            SqlCommand sqlCommand = null
            )
        {
            int Cookie = -1;
            object Result = null;
            int RetryNum = 0;
            int RetryNumForDeadLock = 0;
            SqlCommand Command = new SqlCommand();
            Task dataFillTask = null;

            while (true)
            {
                try
                {
                    if (sqlCommand == null)
                        Command = GetCommand(ref Cookie, AutoTransaction, IsolationLevel);
                    else
                    {
                        Command = sqlCommand;
                        Command.Parameters.Clear();
                    }
                    Command.CommandType = CommandType;
                    Command.CommandText = CommandText;

                    // timeout < 0 jest bez sensu, timeout = 0 oznacza "default timeout" (30) czyli nie przestawiamy, timeout = SqlServerDefaultTimeout (int.MaxValue) oznacza, ze chcemy ustawic 0
                    if (CommandTimeout == DBCommon.SqlServerDefaultTimeout)
                    {
                        Command.CommandTimeout = 0;
                    }
                    else if (CommandTimeout > 0)
                    {
                        Command.CommandTimeout = CommandTimeout;
                    }

                    SqlParameter[] SqlParameters = null;
                    if (Parameters != null)
                    {
                        SqlParameters = new SqlParameter[Parameters.Length];

                        for (int i = 0; i < Parameters.Length; i++)
                        {
                            Parameter Param = Parameters[i];

                            SqlParameter SqlParam = new SqlParameter(Param.Name, Param.Type);
                            SqlParam.Value = Param.Value;
                            SqlParam.Direction = Param.Direction;
                            SqlParam.Size = Param.Size;

                            SqlParameters[i] = SqlParam;

                            Command.Parameters.Add(SqlParam);
                        }
                    }

                    //Result = Execute(CommandExecute, Command);

                    dataFillTask = new Task(() =>
                                             {
                                                 try
                                                 {
                                                     Result = CommandExecute.Execute(Command, MaxRetryTime, sRetrySleepTime, sRetrySleepTimeMultiplier, RetryCountForDeadLock);
                                                 }
                                                 catch (ThreadInterruptedException ex) { }
                                             });
                    dataFillTask.Start();
                    dataFillTask.Wait();

                    CommitAndRelease(Cookie);
                    break;
                }
                catch (SqlException sqlException)
                {
                    if (sqlException.Number == 10054 || sqlException.Number == 121 ||
                        sqlException.Number == -2 || sqlException.Number == 11)
                    {
                        RollbackAndRelease(Cookie, sqlException);
                        if ((++RetryNum > sRetryCount))
                        {
                            throw;
                        }
                        Thread.Sleep(RetrySleepTime(RetryNum));
                    }
                    else if (Deadlocked(sqlException))
                    {
                        RollbackAndRelease(Cookie);
                        if ((++RetryNumForDeadLock > RetryCountForDeadLock))
                        {
                            throw;
                        }
                        Thread.Sleep(RetrySleepTimeForDeadlock(RetryNumForDeadLock));
                    }
                    else
                    {
                        RollbackAndRelease(Cookie, sqlException);
                        throw;
                    }
                }
                catch (ThreadInterruptedException Exception)
                {
                    if (Command != null)
                        Command.Cancel();
                    if (dataFillTask != null)
                        dataFillTask.Wait(1000);
                    RollbackAndRelease(Cookie, Exception);

                    throw;
                }
                catch (ThreadAbortException Exception)
                {
                    if (Command != null)
                        Command.Cancel();
                    if (dataFillTask != null)
                        dataFillTask.Wait(1000);
                    RollbackAndRelease(Cookie, Exception);

                    throw;
                }
                catch (Exception Exception)
                {
                    if (Exception.InnerException != null && Exception.InnerException is SqlException)
                    {
                        SqlException sqlException = Exception.InnerException as SqlException;
                        if (sqlException.Number == 10054 || sqlException.Number == 121 ||
                        sqlException.Number == -2 || sqlException.Number == 11)
                        {
                            RollbackAndRelease(Cookie, sqlException);
                            if ((++RetryNum > sRetryCount))
                            {
                                throw;
                            }
                            Thread.Sleep(RetrySleepTime(RetryNum));
                        }
                        else if (Deadlocked(sqlException))
                        {
                            RollbackAndRelease(Cookie);
                            if ((++RetryNumForDeadLock > RetryCountForDeadLock))
                            {
                                throw;
                            }
                            Thread.Sleep(RetrySleepTimeForDeadlock(RetryNumForDeadLock));
                        }
                        else
                        {
                            RollbackAndRelease(Cookie, sqlException);
                            throw;
                        }
                    }
                    else
                    {
                        RollbackAndRelease(Cookie, Exception);

                        throw;
                    }
                }
            }

            return Result;
        }
        #endregion
    }
}

#region przyklad wykorzystania klasy DB.Advanced

// klasa umozliwia wykonania na bazie danych kilku operacji ramach jednej transakcji

// uzycie klasy DB.Advanced polega na skopiowaniu ponizszej procedury i nastepnie
// zmianie oznaczonego fragmentu zgodnie z wymaganiami (oczywiscie mozna tez dodac
// parametry do procedury i ich uzywac :)
// pozostale fragmenty musza pozostac na swoich miejscach, gdyz:
// - zapewniaja one uruchamianie polecen na serwerze w ramach transakcji
// - zapewniaja automatyczne zatwierdzanie efektow jej dzialania lub tez wycofanie,
// jezeli kod pisany przez uzytkownika zakonczyl sie wyjatkiem. w tym ostatnim przypadku
// wyjatek zostanie odebrany przez procedure wywolujaca utworzona na podstawie 
// wzorca procedure
// - zapewniaja ponawianie napisanego przez uzytkownika kodu w przypadku przerwania 
// jego wykonywania spowodowanego zakleszczeniem transakcji (tylko okreslona za pomoca
// stalej liczbe razy, potem calosc konczy sie wyjatkiem informujacym o zakleszczeniu)

namespace DB_T
{
    //class DB_T
    //{
    //    public object Advanced()
    //    {
    //        object Result; //ta zmienna zostanie zwrocona jako wynik dzialania metody

    //        #region inicjalizacja komunikacji z baza

    //        int RetryNum = 0;
    //        bool AllOk = false;

    //        IMR.Suite.Common.DB.DB.Advanced DBAdvanced = new IMR.Suite.Common.DB.DB.Advanced();

    //        while (true)
    //        {
    //            SqlCommand Command = null;

    //            try
    //            {
    //                //nastepna linia automatycznie rozpoczyna transakcje !!!
    //                Command = DBAdvanced.GetCommand();

    //        #endregion

    //                //poczatek kodu pisanego przez uzytkownika
    //                Command.CommandType = CommandType.Text;
    //                Command.CommandText = "exec procedure1";

    //                int Result1 = (int)Command.ExecuteScalar();

    //                if (Result1 > 0)
    //                {
    //                    Command.CommandType = CommandType.Text;
    //                    Command.CommandText = "exec procedure2";

    //                    Command.ExecuteNonQuery();

    //                    Result = 0;
    //                }
    //                else
    //                    Result = 1;

    //                //koniec kodu pisanego przez uzytkownika

    //                #region zakonczenie komunikacji z baza

    //                AllOk = true;
    //                break;
    //            }
    //            catch (Exception Exception)
    //            {
    //                string CommandText = "";
    //                string ParametersText = "";

    //                if (Command != null)
    //                    DBAdvanced.CreateCommandDump(Command, out CommandText, out ParametersText);

    //                DBAdvanced.Rollback();

    //                if (Command != null)
    //                    DBAdvanced.LogException(Command, Exception, CommandText, ParametersText);

    //                if (++RetryNum > DBAdvanced.MaxRetryCount || !DBAdvanced.Deadlocked(Exception))
    //                    throw;

    //                continue;
    //            }
    //            finally
    //            {
    //                if (AllOk)
    //                    DBAdvanced.Commit();
    //                else
    //                    DBAdvanced.Rollback();
    //            }
    //        }
    //                #endregion

    //        return Result;
    //    }
    //}
}
#endregion