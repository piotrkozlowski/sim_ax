﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;


namespace IMR.Suite.Data.DB
{
    class EventID
    {
        public class DB
        {
            public static readonly LogData ModuleUp = new LogData(1000, LogLevel.Debug, "Module {0} started at {1}");
            public static readonly LogData ModuleDown = new LogData(1001, LogLevel.Debug, "Module {0} stopped at {1}");
            public static readonly LogData ModuleUpError = new LogData(1002, LogLevel.Fatal, "Exception: Initialization failed ({0}).");
            public static readonly LogData ModuleUpRetry = new LogData(1003, LogLevel.Fatal, "Try {0}. Sleeping for {1}s before reconnecting...");

            public static readonly LogData DestructorDBCheck = new LogData(1020, LogLevel.Fatal, "Tested for (DB == NULL) and result was true!");
            public static readonly LogData DestructorDBStopped = new LogData(1021, LogLevel.Info, "DB.Stop() has been performed successfully.");

            public static readonly LogData ErrorCastingData = new LogData(22010, LogLevel.Error, "Error casting data. IdDataType: {0}, IdDataTypeClass: {1}, value: {2}. Details: {3}");
            


        }
    }
}
