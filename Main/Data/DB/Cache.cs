﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Common;
using Tuple = System.Tuple;

namespace IMR.Suite.Data.DB
{
    [Serializable]
    public class Cache : IDisposable
    {
        // Uwaga!! dodając nowy cache należy w Clear wywołać ClearAll
        #region TableSchema
        public static CacheTableSchema TableSchema = new CacheTableSchema();
        #endregion
        #region UserTableTypeSchema
        public static CacheUserTableTypeSchema UserTableTypeSchema = new CacheUserTableTypeSchema();
        #endregion
        #region StoredProcSchema
        public static CacheStoredProcSchema StoredProcSchema = new CacheStoredProcSchema();
        #endregion

        #region Constructor
        static Cache()
        {
        }
        #endregion
        #region Destructor
        ~Cache()
        {
            Dispose();
        }
        #endregion
        #region IDisposable Members - Dispose()
        public void Dispose()
        {
            ClearAll();
        }
        #endregion
        #region static ClearAll
        public static void ClearAll()
        {
            TableSchema.ClearAll();
            UserTableTypeSchema.ClearAll();
            StoredProcSchema.ClearAll();
        }
        #endregion
        #region ClearCache
        public static void ClearCache(DBCommon db)
        {
            TableSchema.ClearCache(db);
            UserTableTypeSchema.ClearCache(db);
            StoredProcSchema.ClearCache(db);
        }
        #endregion
    }

    #region CacheTableSchema
    [Serializable]
    public class CacheTableSchema
    {
        public CacheDictionary<string, DataTable> cacheDAQ = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheCORE = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheDW = new CacheDictionary<string, DataTable>();
        private TimeSpan ItemReleaseTime = TimeSpan.FromDays(7);
        internal object objLock = new object();

        public CacheTableSchema() { }
        #region Init
        public void Init(TimeSpan ItemReleaseTime)
        {
            this.ItemReleaseTime = ItemReleaseTime;
        }
        #endregion
        #region Get
        public DataTable Get(string TableName, DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                {
                    if (!cacheDAQ.ContainsKey(TableName))
                        return GetFromDB(TableName, db);
                    cacheDAQ.Set(TableName, cacheDAQ[TableName], ItemReleaseTime);
                    return cacheDAQ[TableName].Clone();
                }
                else if (db is DBCommonCORE)
                {
                    if (!cacheCORE.ContainsKey(TableName))
                        return GetFromDB(TableName, db);
                    cacheCORE.Set(TableName, cacheCORE[TableName], ItemReleaseTime);
                    return cacheCORE[TableName].Clone();
                }
                else if (db is DBCommonDW)
                {
                    if (!cacheDW.ContainsKey(TableName))
                        return GetFromDB(TableName, db);
                    cacheDW.Set(TableName, cacheDW[TableName], ItemReleaseTime);
                    return cacheDW[TableName].Clone();
                }
                return new DataTable();
            }
        }
        #endregion
        #region Remove
        public void Remove(string TableName)
        {
            lock (objLock)
            {
                if (cacheDAQ.ContainsKey(TableName))
                    cacheDAQ.Remove(TableName);
                if (cacheCORE.ContainsKey(TableName))
                    cacheCORE.Remove(TableName);
                if (cacheDW.ContainsKey(TableName))
                    cacheDW.Remove(TableName);
            }
        }
        #endregion
        #region ClearAll
        public void ClearAll()
        {
            lock (objLock)
            {
                cacheDAQ.Clear();
                cacheCORE.Clear();
                cacheDW.Clear();
            }
        }
        #endregion
        #region ClearCache
        public void ClearCache(DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                    cacheDAQ.Clear();
                else if (db is DBCommonCORE)
                    cacheCORE.Clear();
                else if (db is DBCommonDW)
                    cacheDW.Clear();
            }
        }
        #endregion

        #region internal GetFromDB
        internal DataTable GetFromDB(string TableName, DBCommon db)
        {
            if (db == null)
            {
                throw new ArgumentNullException("db", "Cache for TableSchema not Initialized !!!");
            }

            DB.Advanced DBAdvanced = new DB.Advanced(db.DB);
            SqlCommand Command = DBAdvanced.GetCommand(false);	// nie potrzebujemy tutaj tranzakcji

            Command.CommandText = string.Format("SELECT TOP 0 * FROM {0}", TableName);

            CommandType previousType = Command.CommandType;
            Command.CommandType = CommandType.Text;

            //pobieramy infromacje o typach kolumn
            DataTable dataTableSchema = new DataTable();
            SqlDataReader dr = Command.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
            dataTableSchema = dr.GetSchemaTable();
            dr.Dispose();

            Command.CommandType = previousType;

            //pobieramy informacje o strukturze tabeli
            DataTable dataTable = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(Command.CommandText, Command.Connection);
            ad.Fill(dataTable);//ad.FillSchema(dataTable, SchemaType.Source);
            ad.Dispose();
            DBAdvanced.Commit();

            //sama w sobie DataTable nie zawiera informacji o typie SqlDbType
            //dodajemy ją więc sobie do Column ExtendedProperties
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                foreach (DataRow drItem in dataTableSchema.Rows)
                {
                    if (String.Equals(drItem["ColumnName"].ToString(), dataTable.Columns[i].ColumnName))
                    {
                        dataTable.Columns[i].ExtendedProperties.Add("ProviderType", drItem["ProviderType"]);
                        dataTable.Columns[i].ExtendedProperties.Add("ColumnSize", drItem["ColumnSize"]);
                        dataTable.Columns[i].ExtendedProperties.Add("IsKey", drItem["IsKey"]);
                        break;
                    }
                }
            }

            dataTable.Clear();

            if (db is DBCommonDAQ)
            {
                if (!cacheDAQ.ContainsKey(TableName))
                    cacheDAQ.Add(TableName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonCORE)
            {
                if (!cacheCORE.ContainsKey(TableName))
                    cacheCORE.Add(TableName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonDW)
            {
                if (!cacheDW.ContainsKey(TableName))
                    cacheDW.Add(TableName, dataTable, ItemReleaseTime);
            }

            return dataTable.Clone();
        }
        #endregion
    }
    #endregion
    #region CacheUserTableTypeSchema
    [Serializable]
    public class CacheUserTableTypeSchema
    {
        public CacheDictionary<string, DataTable> cacheDAQ = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheCORE = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheDW = new CacheDictionary<string, DataTable>();
        private TimeSpan ItemReleaseTime = TimeSpan.FromDays(7);
        internal object objLock = new object();

        public CacheUserTableTypeSchema() { }
        #region Init
        public void Init(TimeSpan ItemReleaseTime)
        {
            this.ItemReleaseTime = ItemReleaseTime;
        }
        #endregion
        #region Get
        public DataTable Get(string UserTableTypeName, DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                {
                    if (!cacheDAQ.ContainsKey(UserTableTypeName))
                        return GetFromDB(UserTableTypeName, db);
                    cacheDAQ.Set(UserTableTypeName, cacheDAQ[UserTableTypeName], ItemReleaseTime);
                    return cacheDAQ[UserTableTypeName].Clone();
                }
                else if (db is DBCommonCORE)
                {
                    if (!cacheCORE.ContainsKey(UserTableTypeName))
                        return GetFromDB(UserTableTypeName, db);
                    cacheCORE.Set(UserTableTypeName, cacheCORE[UserTableTypeName], ItemReleaseTime);
                    return cacheCORE[UserTableTypeName].Clone();
                }
                else if (db is DBCommonDW)
                {
                    if (!cacheDW.ContainsKey(UserTableTypeName))
                        return GetFromDB(UserTableTypeName, db);
                    cacheDW.Set(UserTableTypeName, cacheDW[UserTableTypeName], ItemReleaseTime);
                    return cacheDW[UserTableTypeName].Clone();
                }
                return new DataTable();
            }
        }
        #endregion
        #region Remove
        public void Remove(string UserTableTypeName)
        {
            lock (objLock)
            {
                if (cacheDAQ.ContainsKey(UserTableTypeName))
                    cacheDAQ.Remove(UserTableTypeName);
                if (cacheCORE.ContainsKey(UserTableTypeName))
                    cacheCORE.Remove(UserTableTypeName);
                if (cacheDW.ContainsKey(UserTableTypeName))
                    cacheDW.Remove(UserTableTypeName);
            }
        }
        #endregion
        #region ClearAll
        public void ClearAll()
        {
            lock (objLock)
            {
                cacheDAQ.Clear();
                cacheCORE.Clear();
                cacheDW.Clear();
            }
        }
        #endregion
        #region ClearCache
        public void ClearCache(DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                    cacheDAQ.Clear();
                else if (db is DBCommonCORE)
                    cacheCORE.Clear();
                else if (db is DBCommonDW)
                    cacheDW.Clear();
            }
        }
        #endregion

        #region internal GetFromDB
        internal DataTable GetFromDB(string UserTableTypeName, DBCommon db)
        {
            if (db == null)
            {
                throw new ArgumentNullException("db", "Cache for UserTableTypeName not Initialized !!!");
            }

            DB.Advanced DBAdvanced = new DB.Advanced(db.DB);
            SqlCommand Command = DBAdvanced.GetCommand(false);	// nie potrzebujemy tutaj tranzakcji

            DataTable dataTable = new DataTable();
            Command.CommandText = string.Format("DECLARE @UDT {0}; SELECT TOP 0 * FROM @UDT;", UserTableTypeName);
            SqlDataAdapter ad = new SqlDataAdapter(Command.CommandText, Command.Connection);
            ad.Fill(dataTable);
            ad.Dispose();
            DBAdvanced.Commit();

            dataTable.Clear();

            if (db is DBCommonDAQ)
            {
                if (!cacheDAQ.ContainsKey(UserTableTypeName))
                    cacheDAQ.Add(UserTableTypeName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonCORE)
            {
                if (!cacheCORE.ContainsKey(UserTableTypeName))
                    cacheCORE.Add(UserTableTypeName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonDW)
            {
                if (!cacheDW.ContainsKey(UserTableTypeName))
                    cacheDW.Add(UserTableTypeName, dataTable, ItemReleaseTime);
            }

            return dataTable.Clone();
        }
        #endregion
    }
    #endregion
    #region CacheStoredProcSchema
    [Serializable]
    public class CacheStoredProcSchema
    {
        public CacheDictionary<string, DataTable> cacheDAQ = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheCORE = new CacheDictionary<string, DataTable>();
        public CacheDictionary<string, DataTable> cacheDW = new CacheDictionary<string, DataTable>();

        public CacheDictionary<string, Dictionary<string, string>> parameterCacheDAQ = new CacheDictionary<string, Dictionary<string, string>>();
        public CacheDictionary<string, Dictionary<string, string>> parameterCacheCORE = new CacheDictionary<string, Dictionary<string, string>>();
        public CacheDictionary<string, Dictionary<string, string>> parameterCacheDW = new CacheDictionary<string, Dictionary<string, string>>();

        private TimeSpan ItemReleaseTime = TimeSpan.FromDays(7);
        internal object objLock = new object();
        internal object paramObjLock = new object();

        public CacheStoredProcSchema() { }
        #region Init
        public void Init(TimeSpan ItemReleaseTime)
        {
            this.ItemReleaseTime = ItemReleaseTime;
        }
        #endregion
        #region Get
        public DataTable Get(string StorecProcName, DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                {
                    if (!cacheDAQ.ContainsKey(StorecProcName))
                        return GetFromDB(StorecProcName, db);
                    cacheDAQ.Set(StorecProcName, cacheDAQ[StorecProcName], ItemReleaseTime);
                    return cacheDAQ[StorecProcName];
                }
                else if (db is DBCommonCORE)
                {
                    if (!cacheCORE.ContainsKey(StorecProcName))
                        return GetFromDB(StorecProcName, db);
                    cacheCORE.Set(StorecProcName, cacheCORE[StorecProcName], ItemReleaseTime);
                    return cacheCORE[StorecProcName];
                }
                else if (db is DBCommonDW)
                {
                    if (!cacheDW.ContainsKey(StorecProcName))
                        return GetFromDB(StorecProcName, db);
                    cacheDW.Set(StorecProcName, cacheDW[StorecProcName], ItemReleaseTime);
                    return cacheDW[StorecProcName];
                }
                return new DataTable();
            }
        }
        #endregion
        #region GetParameterDict
        public Dictionary<string, string> GetParameterDict(string StoredProcName, DBCommon db)
        {
            lock (paramObjLock)
            {
                if (db is DBCommonDAQ)
                {
                    if (!parameterCacheDAQ.ContainsKey(StoredProcName))
                        GetParameterDictFromDataTable(StoredProcName, Get(StoredProcName, db), db);
                    else
                        parameterCacheDAQ.Set(StoredProcName, parameterCacheDAQ[StoredProcName], ItemReleaseTime);
                    return parameterCacheDAQ[StoredProcName];
                }
                else if (db is DBCommonCORE)
                {
                    if (!parameterCacheCORE.ContainsKey(StoredProcName))
                        GetParameterDictFromDataTable(StoredProcName, Get(StoredProcName, db), db);
                    else
                        parameterCacheCORE.Set(StoredProcName, parameterCacheCORE[StoredProcName], ItemReleaseTime);
                    return parameterCacheCORE[StoredProcName];
                }
                else if (db is DBCommonDW)
                {
                    if (!parameterCacheDW.ContainsKey(StoredProcName))
                        GetParameterDictFromDataTable(StoredProcName, Get(StoredProcName, db), db);
                    else
                        parameterCacheDW.Set(StoredProcName, parameterCacheDW[StoredProcName], ItemReleaseTime);
                    return parameterCacheDW[StoredProcName];
                }
                return new Dictionary<string, string>();
            }
        }
        #endregion
        #region Remove
        public void Remove(string StorecProcName)
        {
            lock (objLock)
            {
                if (cacheDAQ.ContainsKey(StorecProcName))
                    cacheDAQ.Remove(StorecProcName);
                if (cacheCORE.ContainsKey(StorecProcName))
                    cacheCORE.Remove(StorecProcName);
                if (cacheDW.ContainsKey(StorecProcName))
                    cacheDW.Remove(StorecProcName);
            }
            lock (paramObjLock)
            {
                if (parameterCacheDAQ.ContainsKey(StorecProcName))
                    parameterCacheDAQ.Remove(StorecProcName);
                if (parameterCacheCORE.ContainsKey(StorecProcName))
                    parameterCacheCORE.Remove(StorecProcName);
                if (parameterCacheDW.ContainsKey(StorecProcName))
                    parameterCacheDW.Remove(StorecProcName);
            }
        }
        #endregion
        #region ClearAll
        public void ClearAll()
        {
            lock (objLock)
            {
                cacheDAQ.Clear();
                cacheCORE.Clear();
                cacheDW.Clear();
            }
            lock (paramObjLock)
            {
                parameterCacheDAQ.Clear();
                parameterCacheCORE.Clear();
                parameterCacheDW.Clear();
            }
        }
        #endregion
        #region ClearCache
        public void ClearCache(DBCommon db)
        {
            lock (objLock)
            {
                if (db is DBCommonDAQ)
                    cacheDAQ.Clear();
                else if (db is DBCommonCORE)
                    cacheCORE.Clear();
                else if (db is DBCommonDW)
                    cacheDW.Clear();
            }
            lock (paramObjLock)
            {
                if (db is DBCommonDAQ)
                    parameterCacheDAQ.Clear();
                else if (db is DBCommonCORE)
                    parameterCacheCORE.Clear();
                else if (db is DBCommonDW)
                    parameterCacheDW.Clear();
            }
        }
        #endregion

        #region internal GetFromDB
        internal DataTable GetFromDB(string StorecProcName, DBCommon db)
        {
            if (db == null)
            {
                throw new ArgumentNullException("db", "Cache for StorecProc not Initialized !!!");
            }

            DB.Advanced DBAdvanced = new DB.Advanced(db.DB);
            SqlCommand Command = DBAdvanced.GetCommand(false);	// nie potrzebujemy tutaj tranzakcji

            DataTable dataTable = new DataTable();
            Command.CommandType = CommandType.StoredProcedure;
            Command.CommandText = string.Format(@"sp_sproc_columns '{0}'", StorecProcName);
            SqlDataAdapter ad = new SqlDataAdapter(Command.CommandText, Command.Connection);
            ad.Fill(dataTable);
            ad.Dispose();
            DBAdvanced.Commit();

            //dataTable.Clear();

            if (db is DBCommonDAQ)
            {
                if (!cacheDAQ.ContainsKey(StorecProcName))
                    cacheDAQ.Add(StorecProcName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonCORE)
            {
                if (!cacheCORE.ContainsKey(StorecProcName))
                    cacheCORE.Add(StorecProcName, dataTable, ItemReleaseTime);
            }
            else if (db is DBCommonDW)
            {
                if (!cacheDW.ContainsKey(StorecProcName))
                    cacheDW.Add(StorecProcName, dataTable, ItemReleaseTime);
            }

            return dataTable;
        }
        #endregion
        #region private GetParameterDictFromDataTable
        private void GetParameterDictFromDataTable(string StoredProcName, DataTable dataTable, DBCommon db)
        {
            Dictionary<string, string> paramsDict = new Dictionary<string, string>();
            if (dataTable != null && dataTable.Columns.Contains("COLUMN_NAME"))
            {
                foreach (DataRow row in dataTable.Rows.OfType<DataRow>())
                {
                    if (row["COLUMN_NAME"] != null)
                        paramsDict[row["COLUMN_NAME"].ToString()] = row["COLUMN_NAME"].ToString();
                }
            }
            if (db is DBCommonDAQ)
            {
                if (!parameterCacheDAQ.ContainsKey(StoredProcName))
                    parameterCacheDAQ.Add(StoredProcName, paramsDict, ItemReleaseTime);
            }
            else if (db is DBCommonCORE)
            {
                if (!parameterCacheCORE.ContainsKey(StoredProcName))
                    parameterCacheCORE.Add(StoredProcName, paramsDict, ItemReleaseTime);
            }
            else if (db is DBCommonDW)
            {
                if (!parameterCacheDW.ContainsKey(StoredProcName))
                    parameterCacheDW.Add(StoredProcName, paramsDict, ItemReleaseTime);
            }
        }
        #endregion
    }
    #endregion
}
