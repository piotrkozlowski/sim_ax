﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IMR.Suite.Common.Drivers
{
    [Serializable]
    public class Specification
    {
        #region Members
        private string filePath;
        private SpecificationCollection specificationCollection;
        #endregion

        #region Properties
        public int ConfigVersion { get { return specificationCollection != null ? specificationCollection.ConfigVersion : 0; } }
        //        #region AllDescr
        //        public List<DeviceDriver> AllConfigurations
        //        {
        //            get
        //            {
        //                if (specificationCollection == null)
        //                    if (filePath != null)
        //                        LoadSpecification();
        //                    else
        //                    {
        //                        specificationCollection = new SpecificationCollection();
        //                        specificationCollection.DeviceDrivers = new List<DeviceDriver>();
        //                    }
        //                if (specificationCollection != null && specificationCollection.DeviceDrivers.Count == 0)
        //                    if (filePath != null)
        //                        LoadSpecification();
        //
        //
        //                return specificationCollection.DeviceDrivers;
        //            }
        //        }
        //        #endregion
        #endregion

        #region Ctor
        public Specification(string filePath = null)
        {
            this.filePath = filePath;
            specificationCollection = new SpecificationCollection();
        }
        #endregion

        #region Load
        public void Load()
        {
            Stream stream = null;
            try
            {
                string path = filePath ?? (Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "IMR.Suite.Common.Drivers.Specification.xml"));
                filePath = path;

                if (!File.Exists(path))
                    return;

                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                XmlSerializer serializer = new XmlSerializer(typeof(SpecificationCollection));

                specificationCollection = (SpecificationCollection)serializer.Deserialize(stream);
                stream.Close();

                foreach (DeviceDriver deviceDriver in specificationCollection.DeviceDrivers)
                {
                    deviceDriver.GetAllCommands().ForEach(c =>
                                                          {
                                                              if (c.ChildCommands != null) 
                                                                  c.ChildCommands.ForEach(child => child.ParentCommand = c);
                                                          });
                }

                return;
            }
            catch (Exception ex)
            {
                if (stream != null)
                    stream.Close();
            }
        }
        #endregion
        #region Save
        public bool Save(string savePath = null)
        {
            try
            {
                if (String.IsNullOrEmpty(savePath))
                    savePath = filePath;
                using (FileStream file = new FileStream(savePath, FileMode.OpenOrCreate))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SpecificationCollection));
                    specificationCollection.ConfigVersion = specificationCollection.ConfigVersion + 1;
                    serializer.Serialize(file, specificationCollection);
                    file.Flush();
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region DeviceDriver
        #region GetDeviceDriver
        public DeviceDriver GetDeviceDriver(string name)
        {
            return specificationCollection.DeviceDrivers.FirstOrDefault(w => w.DeviceDriverName == name);
        }
        #endregion
        #region GetAllDeviceDriver
        public List<DeviceDriver> GetAllDeviceDriver()
        {
            return specificationCollection.DeviceDrivers.ToList();
        }
        #endregion
        #region AddReplaceDeviceDriver
        public bool AddReplaceDeviceDriver(DeviceDriver devDescr)
        {
            return specificationCollection.AddReplaceDeviceDriver(devDescr);
        }
        #endregion
        #endregion

        #region Classes
        #region class CommandDescription
        [Serializable]
        public class CommandDescription
        {
            [XmlElement(Type = typeof(int))]
            public int IdLanguage { get; set; }
            [XmlElement(Type = typeof(string))]
            public string Description { get; set; }
            [XmlElement(Type = typeof(string))]
            public string ExtendedDescription { get; set; }
            public CommandDescription()
            { }
            public CommandDescription(int idLang, string d, string dE)
            {
                IdLanguage = idLang;
                Description = d;
                ExtendedDescription = dE;
            }
        }
        #endregion
        #region class Command
        [Serializable]
        public class Command
        {
            [XmlElement(Type = typeof(int))]
            public int OMB { get; set; }
            [XmlElement(Type = typeof(string))]
            public string Name { get; set; }
            [XmlElement(Type = typeof(int))]
            public int Index { get; set; }

            [XmlElement(Type = typeof(long?))]
            public long? IdDataType { get; set; }

            [XmlElement(Type = typeof(long?))]
            public long? SizeInBits { get; set; }
            [XmlElement(Type = typeof(string))]
            public string TypeOfData { get; set; }
            [XmlElement(Type = typeof(string))]
            public string DeviceCommandType { get; set; }

            [XmlArray]
            [XmlArrayItem(typeof(CommandDescription))]
            public List<CommandDescription> CommandDescriptions { get; set; }


            [XmlArray]
            [XmlArrayItem(typeof(Command))]
            public List<Command> ChildCommands { get; set; }
            [XmlIgnore]
            public Command ParentCommand { get; set; }

            public Command()
            {
                //OMB = -1;
                Name = "";
                //Index = -1;
            }
            public Command(string name, int omb, long? idDataType, int index)
            {
                OMB = omb;
                Name = name;
                if (idDataType.HasValue)
                    IdDataType = idDataType;
                Index = index;
            }
            public Command(string name, int omb, long? idDataType, int index, long bitsSize, string typeOfData, string dCommandType)
            {
                OMB = omb;
                Name = name;
                if (idDataType.HasValue)
                    IdDataType = idDataType;
                Index = index;
                SizeInBits = bitsSize;
                TypeOfData = typeOfData;
                DeviceCommandType = dCommandType;
            }

            #region AddNewElements
            public bool AddReplaceNewChildCommand(Command newCommandDescr)
            {
                if (ChildCommands == null)
                    ChildCommands = new List<Command>();

                #region Merge existings
                if (ChildCommands.Any(i => i.OMB == newCommandDescr.OMB && i.Index == newCommandDescr.Index && i.Name == newCommandDescr.Name))
                {

                    Command old = ChildCommands.First(i => i.IdDataType == newCommandDescr.IdDataType && i.OMB == newCommandDescr.OMB && i.Name == newCommandDescr.Name && i.Index == newCommandDescr.Index);
                    ChildCommands.Remove(old);
                    if (newCommandDescr.ChildCommands != null)
                        for (int i = 0; i < newCommandDescr.ChildCommands.Count; i++)
                        {
                            Command cmd = newCommandDescr.ChildCommands[i];
                            old.AddReplaceNewChildCommand(cmd);
                        }
                    if (newCommandDescr.CommandDescriptions != null)
                        for (int i = 0; i < newCommandDescr.CommandDescriptions.Count; i++)
                        {
                            CommandDescription cmd = newCommandDescr.CommandDescriptions[i];
                            old.AddReplaceNewCommandDescription(cmd);
                        }
                    //foreach (CommandDescription desr in newCommandDescr.CommandDescriptions)
                    //{
                    //    old.AddReplaceNewCommandDescription(desr);

                    //}

                    newCommandDescr = old;
                }
                #endregion

                ChildCommands.Add(newCommandDescr);

                return true;
            }
            public bool AddReplaceNewCommandDescription(CommandDescription newCommandDescr)
            {
                if (CommandDescriptions == null)
                    CommandDescriptions = new List<CommandDescription>();

                #region Replace
                if (CommandDescriptions.Any(i => i.IdLanguage == newCommandDescr.IdLanguage))
                {
                    CommandDescription old = CommandDescriptions.Where(i => i.IdLanguage == newCommandDescr.IdLanguage).First();
                    CommandDescriptions.Remove(old);
                }
                #endregion

                CommandDescriptions.Add(newCommandDescr);

                return true;
            }
            #endregion
        }
        #endregion

        #region class DeviceDriver
        [Serializable]
        public class DeviceDriver
        {
            #region Properties
            [XmlElement(Type = typeof(string))]
            public string DeviceDriverName { get; set; }

            [XmlArray]
            [XmlArrayItem(typeof(Command))]
            public List<Command> Commands { get; set; }
            #endregion

            #region Ctor
            public DeviceDriver()
            {
                DeviceDriverName = "";
            }

            public DeviceDriver(string driverName)
            {
                DeviceDriverName = driverName;
            }
            #endregion

            #region AddReplaceNewCommand
            public bool AddReplaceNewCommand(Command command)
            {
                if (Commands == null)
                    Commands = new List<Command>();

                #region MergeExisting
                if (Commands.Any(i => i.IdDataType == command.IdDataType && i.OMB == command.OMB && i.Name == command.Name && i.Index == command.Index))
                {
                    Command old = Commands.First(i => i.IdDataType == command.IdDataType && i.OMB == command.OMB && i.Name == command.Name && i.Index == command.Index);
                    Commands.Remove(old);
                    if (command.ChildCommands != null)
                    {
                        for (int i = 0; i < command.ChildCommands.Count; i++)
                        {
                            Command cmd = command.ChildCommands[i];
                            old.AddReplaceNewChildCommand(cmd);
                        }
                        //foreach (Command cmd in command.ChildCommands)
                        //{
                        //    old.AddReplaceNewChildCommand(cmd);

                        //}
                    }
                    if (command.CommandDescriptions != null)
                        for (int i = 0; i < command.CommandDescriptions.Count; i++)
                        {
                            CommandDescription cmd = command.CommandDescriptions[i];
                            old.AddReplaceNewCommandDescription(cmd);
                        }
                    //foreach (CommandDescription desr in command.CommandDescriptions)
                    //{
                    //    old.AddReplaceNewCommandDescription(desr);

                    //}

                    command = old;
                }
                #endregion

                Commands.Add(command);

                return true;
            }
            #endregion
            #region GetAllCommands
            public List<Command> GetAllCommands()
            {
                List<Command> result = new List<Command>();

                result.AddRange(Commands);
                foreach (Command command in Commands)
                    result.AddRange(GetChildCommands(command));

                return result;
            }
            #endregion
            #region GetChildCommands
            private List<Command> GetChildCommands(Command command)
            {
                List<Command> result = new List<Command>();

                result.AddRange(command.ChildCommands);
                foreach (Command childCommand in command.ChildCommands)
                    result.AddRange(GetChildCommands(childCommand));

                return result;
            }
            #endregion
        }
        #endregion
        #region class SpecificationCollection
        [Serializable]
        public class SpecificationCollection
        {
            #region Properties
            [XmlElement(Type = typeof(int))]
            public int ConfigVersion { get; set; }

            [XmlArray]
            [XmlArrayItem(typeof(DeviceDriver))]
            public List<DeviceDriver> DeviceDrivers { get; set; }
            #endregion

            #region Ctor
            public SpecificationCollection()
            {
                ConfigVersion = -2;
                DeviceDrivers = new List<DeviceDriver>();
            }
            #endregion

            #region AddReplaceDeviceDriver
            public bool AddReplaceDeviceDriver(DeviceDriver devDescr)
            {
                if (DeviceDrivers == null)
                    DeviceDrivers = new List<DeviceDriver>();

                #region MergeExisting
                if (DeviceDrivers.Any(i => i.DeviceDriverName == devDescr.DeviceDriverName))
                {
                    DeviceDriver oldDriver = DeviceDrivers.Find(i => i.DeviceDriverName == devDescr.DeviceDriverName);
                    DeviceDrivers.Remove(oldDriver);
                    for (int i = 0; i < devDescr.Commands.Count; i++)
                    {
                        Command cmd = devDescr.Commands[i];
                        oldDriver.AddReplaceNewCommand(cmd);
                    }
                    //foreach (Command cmd in devDescr.Commands)
                    //{
                    //    oldDriver.AddReplaceNewCommand(cmd); 
                    //}
                    devDescr = oldDriver;

                }
                #endregion

                DeviceDrivers.Add(devDescr);

                return true;
            }
            #endregion

        }
        #endregion
        #endregion
    }


}
