﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.Remoting.Lifetime;

using IMR.Suite.Common;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Common.Drivers.Transmission
{
#if !WindowsCE && !Android
	[Serializable]
	public class TransmissionDriverWrapper : MarshalByRefObjectDisposable
	{
		public TransmissionDriver Driver = default(TransmissionDriver);
		private AppDomain Domain;

		private static Dictionary<string, Type[]> TypesInAssemblyDictionary = new Dictionary<string, Type[]>();
		private static Dictionary<string, Type> DerivedTypeDictionary = new Dictionary<string, Type>();

		private Type[] TypesInAssembly = null;
		private Type DerivedType = null;

		#region Constructor
		public TransmissionDriverWrapper()
		{
		}
		public TransmissionDriverWrapper(string DriverPath, object[] DriverConstructorArgs)
		{
			string assemblyPath = DriverPath;
			if (!System.IO.Path.IsPathRooted(assemblyPath))
				assemblyPath = AppDomain.CurrentDomain.BaseDirectory + assemblyPath;
			assemblyPath = Path.GetFullPath(assemblyPath);

			string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
			string DllPath = Path.GetDirectoryName(assemblyPath);
			if (DllPath.IndexOf(AppPath) == -1)
			{

				string dllDirectoryName = Path.Combine(AppPath, "Cache");
				DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
				if (!di.Exists)
				{
					di.Create();
					di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
				}

				string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(assemblyPath);
				try
				{
					File.Copy(assemblyPath, newAssemblyPath, true);
				}
				catch { }
				assemblyPath = newAssemblyPath;
				DllPath = Path.GetDirectoryName(assemblyPath);
			}

			AppDomainSetup ads = new AppDomainSetup();
			ads.ApplicationName = "TransmissionDriverWrapper";
			ads.ApplicationBase = AppPath;
			ads.PrivateBinPath = new Uri(AppPath + "\\").MakeRelativeUri(new Uri(DllPath)).ToString();
            //Using protocol drivers in transmission drivers 
            ads.PrivateBinPath += ";Drivers/Protocol";
			ads.ShadowCopyFiles = "true";

			Domain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(assemblyPath), null, ads);
			//Domain.ReflectionOnlyAssemblyResolve += new ResolveEventHandler(OnReflectionOnlyAssemblyResolve);

			lock (TypesInAssemblyDictionary)
			{
				if (!TypesInAssemblyDictionary.TryGetValue(assemblyPath, out TypesInAssembly))
				{
					TypesInAssembly = AssemblyWrapper.GetAssemblyTypes(assemblyPath);
					TypesInAssemblyDictionary.Add(assemblyPath, TypesInAssembly);
				}
			}
			lock (DerivedTypeDictionary)
			{
				if (!DerivedTypeDictionary.TryGetValue(assemblyPath, out DerivedType))
				{
					DerivedType = GetDerivedType(TypesInAssembly, typeof(TransmissionDriver));
					DerivedTypeDictionary.Add(assemblyPath, DerivedType);
				}
			}

			//Driver = (TransmissionDriver)Activator.CreateInstance(DerivedType, DriverConstructorArgs);
			TransmissionDriverWrapper Wrapper = (TransmissionDriverWrapper)Domain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(TransmissionDriverWrapper).FullName);
			Driver = Wrapper.CreateInstance(DerivedType, DriverConstructorArgs);

            //FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assemblyPath);
            //Driver.UpdateAssemblyName(String.Format("{0}{1}", Path.GetFileNameWithoutExtension(assemblyPath), String.IsNullOrEmpty(fvi.FileVersion) ? String.Empty : " " + fvi.FileVersion));

            AssemblyName assemblyName = AssemblyWrapper.GetAssemblyName(assemblyPath);
            Driver.UpdateAssemblyName(String.Format("{0}, {1}", assemblyName.Name, assemblyName.Version.ToString()));
		}
		#endregion
		#region Destructor
		~TransmissionDriverWrapper()
		{
			TypesInAssemblyDictionary.Clear();
			DerivedTypeDictionary.Clear();
		}
		#endregion
		#region Dispose
		public void Dispose()
		{
			if (Driver != null)
			{
				try
				{
					AppDomain.Unload(Domain);
				}
				catch (Exception)
				{
				}
				finally
				{
					Driver = default(TransmissionDriver);
				}

			}
		}
		#endregion

		#region private GetDerivedType
		private Type GetDerivedType(Type[] assemblyTypes, Type pluginType)
		{
			bool isPlugin = false;
			foreach (Type TypeInAssembly in assemblyTypes)
			{
				if (TypeInAssembly.IsClass && TypeInAssembly.IsPublic && !TypeInAssembly.IsAbstract)
				{
					Type baseClass = TypeInAssembly;
					do
					{
						isPlugin = pluginType.Equals(baseClass);
						baseClass = baseClass.BaseType;
					}
					while (baseClass != null && !isPlugin);

					if (isPlugin)
					{
						return TypeInAssembly;
					}
				}
			}
			return default(Type);
		}
		#endregion
		#region private CreateInstance
		private TransmissionDriver CreateInstance(Type type, object[] DriverConstructorArgs)
		{
			return (TransmissionDriver)Activator.CreateInstance(type, DriverConstructorArgs);
		}
		#endregion

		#region OnReflectionOnlyAssemblyResolve
		public static Assembly OnReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
		{
			return System.Reflection.Assembly.ReflectionOnlyLoad(args.Name);
		}
		#endregion
		#region MarshalByRefObjectDisposable overrides
		public override object InitializeLifetimeService()
		{
			return null;
		}
		#endregion
	}
#endif
}
