using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using IMR.Suite.Common;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Common.Drivers.Transmission
{
    [Serializable]
    public abstract class TransmissionDriver : MarshalByRefObjectDisposable
    {
        #region Definitions
        public enum CommandType
        {
            DumpLog = 1
        }
        #endregion Definitions

        #region EventArgs
        #region ModuleEventDataArgs
        [Serializable]
        public class ModuleEventDataArgs : EventArgs
        {
            public readonly ModuleEventData ModuleEventData;
            public bool? Healthy = null;

            public ModuleEventDataArgs(ModuleEventData moduleEventData, bool? healthy = null)
            {
                this.ModuleEventData = moduleEventData;
                this.Healthy = healthy;
                if (this.ModuleEventData == null)
                    this.ModuleEventData = new ModuleEventData(false, new DataValue[0]);
            }
        }
        #endregion
        #endregion

        #region Events
        public event EventHandler<PacketXmitEventArgs> PacketXmitReceivedEvent;
        public event EventHandler<PacketXmitEventArgs> AckPacketXmitEvent;
        public event EventHandler<ModuleEventDataArgs> ModuleEventDataEvent;
        #endregion

        #region Members
        /// <summary>
        /// Identyfikator plugina
        /// </summary>
        public ushort IdDriver;
        /// <summary>
        /// Adres IPv6 tego drivera
        /// </summary>
        public IPv6Address IPv6;
        /// <summary>
        /// Parametry drivera TransmissionDriverParamType -> Transmission np. COM_Port -> COM1, PIN -> 1234
        /// </summary>
        //protected Dictionary<long, object> DriverParameters;
        protected List<DataValue> DriverParameters;
        /// <summary>
        /// Zdarzenie s�u��ce do zatrzymywania w�tk�w plugina
        /// </summary>
        protected readonly ManualResetEvent StopEvent;
        /// <summary>
        /// Zdarzenie s�u��ce do notyfikacji o zako�czeniu dzia�ania modu�u
        /// </summary>
        protected ManualResetEvent StoppedEvent;
        /// <summary>
        /// Flaga oznaczaj�ca stan zainicjowania plugina, zmienna wewn�trzna udost�pniania przez property
        /// </summary>
        protected bool Initiated;
        /// <summary>
        /// Flaga oznaczaj�ca stan zainicjowania plugina 
        /// </summary>
        public bool IsInitiated
        {
            get { return Initiated; }
        }
        /// <summary>
        /// Nazwa biblioteki, u�ywana do przekazywania informacji w DiagnosticInfo
        /// </summary>
        private string assemblyName = "";
        public string AssemblyName { get { return assemblyName; } }
        /// <summary>
        /// Oznaczenie czy biblioteka dzia�a poprawnie, czy te� jest jaki� b��d powoduj�cy problem w dzia�aniu
        /// </summary>
        private bool healthy = false;
        public bool Healthy { get { return healthy; } }
        #endregion Members

        #region Constructor
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="IPv6">Address IPv6 drivera,
        /// pozwala klientowy na identyfikacj� drivera, z kt�rego pochodzi pakiet przychodz�cy lub potwierdzenie</param>
        public TransmissionDriver(IPv6Address IPv6Driver)
        {
            this.StopEvent = new ManualResetEvent(false);
            this.Initiated = false;
            this.IdDriver = IPv6Driver.IDDriver.Value;
            this.IPv6 = IPv6Driver;
        }
        #endregion

        #region Init
        /// <summary>
        /// Inicjuje prac� plugina
        /// </summary>
        /// <param name="DriverParameters">Parametry modu�u, np. COM_Port -> COM1, PIN -> 1234</param>
        /// <returns>Czy inicjalizacja powiod�a si�</returns>
        [Obsolete("Obsolete. Override Init(List<DataValue>) - parameters with List, not Dictionary")]
        public virtual bool Init(Dictionary<long, object> ModuleParameters)
        {
            List<DataValue> moduleParameters = new List<DataValue>();
            foreach (KeyValuePair<long, object> kvp in ModuleParameters)
                moduleParameters.Add(new DataValue(kvp.Key, kvp.Value));
            return Init(moduleParameters);
        }
        public virtual bool Init(List<DataValue> ModuleParameters)
        {
            this.DriverParameters = ModuleParameters;
            return true;
        }
        #endregion
        #region Stop
        /// <summary>
        /// Inicjuje zatrzymywanie w�tk�w zwi�zanych z pluginem (nieblokuj�co)
        /// </summary>
        /// <returns>Zdarzenie, po kt�rego wyst�pieniu mo�na uzna�, �e w�tek jest zatrzymany</returns>
        public abstract ManualResetEvent Stop();
        #endregion
        #region AddToSend
        /// <summary>
        /// Dodaje pakiet do kolejki do wys�ania
        /// </summary>
        /// <param name="Packet">Pakiet, kt�ry ma by� wys�any. Wska�nik b�dzie przesy�any w potwierdzeniach,
        /// co pozwoli klientowy na ��czenie potwierdze� z ��daniami wys�ania pakietu</param>
        public abstract void AddToSend(PacketXmit Packet);
        #endregion
        #region ProcessCommand
        public virtual object ProcessCommand(CommandType commandType, params object[] param)
        {
            return null;
        }
        #endregion
        #region UpdateAssemblyName
        public void UpdateAssemblyName(string assemblyName)
        {
            this.assemblyName = assemblyName;
        }
        #endregion

        #region InvokePacketXmitReceivedEvent
        /// <summary>
        /// Wywoluje procedure klienta do obslugi pakietow przychodzacych (o ile klient zasubsrybowal)
        /// </summary>
        /// <param name="e"></param>
        protected virtual void InvokePacketXmitReceivedEvent(PacketXmitEventArgs e)
        {
            EventHandler<PacketXmitEventArgs> handler = PacketXmitReceivedEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion
        #region InvokeAckPacketXmitEvent
        /// <summary>
        /// Wywoluje procedure klienta do obslugi potwierdzen (o ile klient zasubsrybowal)
        /// </summary>
        /// <param name="e"></param>
        protected virtual void InvokeAckPacketXmitEvent(PacketXmitEventArgs e)
        {
            EventHandler<PacketXmitEventArgs> handler = AckPacketXmitEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion
        #region InvokeModuleEventDataEvent
        /// <summary>
        /// Wywoluje procedure klienta do obslugi informacji od danej biblioteki (o ile klient zasubskrybowal)
        /// </summary>
        /// <param name="e"></param>
        protected virtual void InvokeModuleEventDataEvent(ModuleEventData e, bool? healthy = null)
        {
            if (healthy != null)
                this.healthy = healthy.Value;
            EventHandler<ModuleEventDataArgs> handler = ModuleEventDataEvent;
            if (handler != null)
            {
                e.UpdateHealthy(this.healthy);
                e.UpdateAssembly(this.assemblyName);
                e.AddEventData(new DataValue(DataType.MON_TRANSMISSION_DRIVER_ID, IdDriver));
                handler(this, new ModuleEventDataArgs(e));
            }
        }
        #endregion

        #region GetDiagnosticInfo
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, assemblyName);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, assemblyName, description);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description, object extensionError)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, assemblyName, description, extensionError);
        }
        #endregion


        #region MarshalByRefObjectDisposable overrides
#if !WindowsCE
        public override object InitializeLifetimeService()
        {
            return null;
        }
#endif
        #endregion
    }
}
