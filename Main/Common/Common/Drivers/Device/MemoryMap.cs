﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using IMR.Suite.Common;

namespace IMR.Suite.Common.Drivers.Device
{
    [Serializable]
    public class MemoryMap : IEnumerable
    {
        #region Members
        protected Dictionary<int, List<DeviceCommand>> GlobalOffsetDict;
        protected Dictionary<long, List<DeviceCommand>> GlobalTypeDict;
        protected Dictionary<DeviceCommand.SLOTINFO, List<DeviceCommand>> GlobalSlotDict;
        protected DeviceCommandList Commands; //OK, ale w statycznej nie przechowujemy wartości
        protected bool Inited = false;
        public int InstanceAmount;
        #region SystemRequiredDataTypes
        private List<Tuple<long, int>> systemRequiredDataTypes = new List<Tuple<long, int>>();
        public List<Tuple<long, int>> SystemRequiredDataTypes { get { return systemRequiredDataTypes; } set { systemRequiredDataTypes = value; } }
        #endregion
        #region IsConfigurationDeviceCommands
        private List<DeviceCommand> isConfigurationDeviceCommands = new List<DeviceCommand>();
        public List<DeviceCommand> IsConfigurationDeviceCommands { get { return isConfigurationDeviceCommands; } set { isConfigurationDeviceCommands = value; } }
        #endregion


        public DataValue[] DeviceDriverParams = new DataValue[0];

        public List<Enums.Protocol> SupportedProtocols = new List<Enums.Protocol>(); //OK, choć można wyrugować lub uprościć do flagi SupportWAN2
        #endregion

        #region GetDataValues
        public delegate DataValue[] GetDataValuesDelegate(SN SerialNbr, long[] IdDataTypes);
        protected GetDataValuesDelegate GetDataValues;
        #endregion
        //===Projekt #001 "Wysyłanie zapytań przez wymuszoną hierarchie urządzeń"
        //#region GetSlotsOutForDevice
        //public delegate Slot[] GetSlotsOutForDeviceDelegate(SN SerialNbr, DataMeasureValue[] Values, Enums.ProcessType ProcessType, Enums.Protocol Protocol);
        //protected GetSlotsOutForDeviceDelegate GetSlotsOutForDevice;
        //#endregion
        //======================END Projekt #001
        #region GetValuesForItems
        [Obsolete("In drivers with static MemoryMap use GetValuesForItems2.")]
        public virtual void GetValuesForItems(SN SerialNbr, DeviceCommand parent, DeviceCommand[] receivedChildren)
        {
            if (GetDataValues == null)
                return;

            List<DeviceCommand> UnknownCommands = new List<DeviceCommand>();
            Stack<DeviceCommand> stack = new Stack<DeviceCommand>();
            stack.Push(parent);
            while (stack.Count > 0)
            {
                DeviceCommand current = stack.Pop();
                if (current.IdDataType.HasValue == true &&
                   (from item in receivedChildren where item == current select item).FirstOrDefault() == null)
                {
                    UnknownCommands.Add(current);
                }
                foreach (DeviceCommand item in current.Items)
                {
                    stack.Push(item);
                }
            }
            if (UnknownCommands.Count > 0)
            {
                //long[] dataTypes = (from item in UnknownCommands
                //                    select item.IdDataType.Value).Distinct().ToArray();

                DataValue[] db_result = GetDataValues(SerialNbr, (from item in UnknownCommands
                                                                  select item.IdDataType.Value).ToArray());

                string missing_values = "";

                foreach (DeviceCommand command in UnknownCommands)
                {
                    object result = null;
                    if (db_result != null && db_result.Length > 0)
                    {
                        result = (from item in db_result
                                  where item.Type.IdDataType == command.IdDataType.Value && item.Index == command.Index
                                  select item.Value).FirstOrDefault();
                        if (result != null)
                        {
                            try
                            {
                                command.Value = result;
                            }
                            catch (Exception ex)
                            {
                                #region Exception...
                                string valueReceived = "null";
                                string klasaReceived = "?";

                                if (result.GetType().HasElementType && result.GetType().GetElementType() == typeof(byte))
                                    valueReceived = ((byte[])result).ToHexString();
                                else if (result is DateTime)
                                    valueReceived = string.Format("{0:yyyy-MM-dd HH:mm:ss}", result);
                                //else if (result is Single || result is Double)
                                //    valueReceived = Convert.ToDouble(result).ToString("N");
                                else if (result is Single)
                                    valueReceived = Convert.ToDouble(Convert.ToDecimal(result)).ToString("N");
                                else if (result is Double)
                                    valueReceived = Convert.ToDouble(result).ToString("N");
                                else
                                    valueReceived = result.ToString();

                                klasaReceived = result.GetType().Name;
                                if (result.GetType().HasElementType && result.GetType().GetElementType() == typeof(byte))
                                    klasaReceived = klasaReceived.Replace("[]", string.Format("[{0}]", ((byte[])result).Length));

                                string valueExpected = "";
                                if (command.Value.GetType().HasElementType && command.Value.GetType().GetElementType() == typeof(byte))
                                    valueExpected = ((byte[])command.Value).ToHexString();
                                else if (command.Value is DateTime)
                                    valueExpected = string.Format("{0:yyyy-MM-dd HH:mm:ss}", command.Value);
                                //else if (command.Value is Single || command.Value is Double)
                                //    valueExpected = Convert.ToDouble(command.Value).ToString("N");
                                else if (command.Value is Single)
                                    valueExpected = Convert.ToDouble(Convert.ToDecimal(command.Value)).ToString("N");
                                else if (command.Value is Double)
                                    valueExpected = Convert.ToDouble(command.Value).ToString("N");
                                else
                                    valueExpected = command.Value.ToString();

                                string klasaExpected = command.Value.GetType().Name;
                                if (command.Value.GetType().HasElementType && command.Value.GetType().GetElementType() == typeof(byte))
                                    klasaExpected = klasaReceived.Replace("[]", string.Format("[{0}]", command.Bytes.Length));

                                string msg = string.Format("[{0}] Get value from database DataType={1}[{2}] Exception: {3}, Received: {4} ({5}) but Expected: {6} ({7})!",
                                    SerialNbr,
                                    command.IdDataType, command.Index,
                                    ex.Message,
                                    valueReceived, klasaReceived,
                                    valueExpected, klasaExpected
                                );

                                throw new Exception(msg, ex);
                                #endregion
                            }
                        }
                        else if (command.DefaultValue != null)
                            command.Value = command.DefaultValue;
                        else
                            missing_values += "\r\n- " + DataType.GetName(command.IdDataType.Value) + "[" + command.Index + "]";
                    }
                    command.Status = Enums.CommandStatus.PutOK;
                }

                if (missing_values != "")
                {
                    //throw new Exception("Missing values for device " + SerialNbr.ToString() + " in DATA table and default values are not allowed!" + missing_values);
                    Logger.Add(new LogData(9, LogLevel.Error, "Missing values for device {0} in DATA table and default values are not allowed!{1}"), SerialNbr, missing_values);
                }
            }
            #region //old
            /*
                        if (UnknownCommands.Count > 0)
                        {
                            DeviceCommand[] regular = (from item in UnknownCommands
                                                       where item.SlotType == Enums.SlotType.None
                                                    select item).ToArray();
                            if (regular != null && regular.Length > 0)
                            {
                                DataValue[] db_result = GetDataValues((from item in regular
                                                                       select item.IdDataType.Value).ToArray(), null, Enums.SlotType.None, 0);
                                foreach (DeviceCommand command in regular)
                                {
                                    object result = (from item in db_result
                                                     where item.Type.IdDataType == command.IdDataType.Value
                                                     select item.Value).FirstOrDefault();
                                    if (result != null)
                                        command.Value = result;
                                    command.Status = Enums.CommandStatus.PutOK;
                                }
                            }
                            var gBySlotType = (from item in UnknownCommands
                                               where item.SlotType != Enums.SlotType.None
                                               group item by item.SlotType into g
                                               select new { SlotType = g.Key, Commands = g });
                            foreach (var SlotTypeGroup in gBySlotType)
                            {
                                var gBySlotNbr = (from item in SlotTypeGroup.Commands
                                                  group item by item.SlotInfo.idx into g
                                                  select new
                                                  {
                                                      SlotNbr = g.Key,
                                                      DataTypes = (from item in g
                                                                   select item.IdDataType.Value).ToArray()
                                                  });
                                foreach (var SlotNbrGroup in gBySlotNbr)
                                {
                                    DataValue[] db_result = GetDataValues(SlotNbrGroup.DataTypes, null, SlotTypeGroup.SlotType, SlotNbrGroup.SlotNbr);
                                    foreach (var command in SlotTypeGroup.Commands)
                                    {
                                        object result = (from item in db_result
                                                         where command.SlotInfo.idx == SlotNbrGroup.SlotNbr
                                                               && command.IdDataType.Value == item.Type.IdDataType
                                                         select item.Value).FirstOrDefault();
                                        if (result != null)
                                            command.Value = result;
                                        command.Status = Enums.CommandStatus.PutOK;
                                    }
                                }
                            }
                        }
            */
            #endregion
        }
        public void GetValuesForItems2(MemoryMapForDevice map, SN SerialNbr, DeviceCommand parent, DeviceCommand[] receivedChildren)
        {
            if (map.GetDataValues == null)
                return;

            List<DeviceCommand> UnknownCommands = new List<DeviceCommand>();
            Stack<DeviceCommand> stack = new Stack<DeviceCommand>();
            stack.Push(parent);
            while (stack.Count > 0)
            {
                DeviceCommand current = stack.Pop();
                if (current.IdDataType.HasValue == true &&
                   (from item in receivedChildren where item == current select item).FirstOrDefault() == null)
                {
                    UnknownCommands.Add(current);
                }
                foreach (DeviceCommand item in current.Items)
                {
                    stack.Push(item);
                }
            }
            if (UnknownCommands.Count > 0)
            {
                DataValue[] db_result = map.GetDataValues(SerialNbr, (from item in UnknownCommands
                                                                      select item.IdDataType.Value).ToArray());
                if (db_result == null)
                {
                    Logger.Add(new LogData(9, LogLevel.Error, "Unable to get data form DB  for  SerialNbr={0}, Driver={1}, Command = {2} "), SerialNbr.DBValue, this.GetType().ToString(), parent != null ? parent.GlobalOffset.ToString() : "UNKNOWN");
                    throw new Exception(String.Format("Unable to get data form DB  for SerialNbr={0}, Driver={1}, Command = {2}", SerialNbr.DBValue, this.GetType().ToString(), parent != null ? parent.GlobalOffset.ToString() : "UNKNOWN"));
                }

                string missing_values = "";

                foreach (DeviceCommand command in UnknownCommands)
                {
                    object result = null;
                    if (db_result != null && db_result.Length > 0)
                    {
                        result = (from item in db_result
                                  where item.Type.IdDataType == command.IdDataType.Value && item.Index == command.Index
                                  select item.Value).FirstOrDefault();
                        if (result != null)
                        {
                            try
                            {
                                //command.Value = result;
                                map.SetValue(command, result); //zamiast
                            }
                            catch (Exception ex)
                            {
                                #region Exception...
                                string valueReceived = "null";
                                string klasaReceived = "?";

                                if (result.GetType().HasElementType && result.GetType().GetElementType() == typeof(byte))
                                    valueReceived = ((byte[])result).ToHexString();
                                else if (result is DateTime)
                                    valueReceived = string.Format("{0:yyyy-MM-dd HH:mm:ss}", result);
                                //else if (result is Single || result is Double)
                                //    valueReceived = Convert.ToDouble(result).ToString("N");
                                else if (result is Single)
                                    valueReceived = Convert.ToDouble(Convert.ToDecimal(result)).ToString("N");
                                else if (result is Double)
                                    valueReceived = Convert.ToDouble(result).ToString("N");
                                else
                                    valueReceived = result.ToString();

                                klasaReceived = result.GetType().Name;
                                if (result.GetType().HasElementType && result.GetType().GetElementType() == typeof(byte))
                                    klasaReceived = klasaReceived.Replace("[]", string.Format("[{0}]", ((byte[])result).Length));

                                string valueExpected = "";
                                //if (command.Value.GetType().HasElementType && command.Value.GetType().GetElementType() == typeof(byte))
                                //    valueExpected = ((byte[])command.Value).ToHexString();
                                //else if (command.Value is DateTime)
                                //    valueExpected = string.Format("{0:yyyy-MM-dd HH:mm:ss}", command.Value);
                                //else if (command.Value is Single || command.Value is Double)
                                //    valueExpected = Convert.ToDouble(command.Value).ToString("N");
                                //else
                                //    valueExpected = command.Value.ToString();

                                //string klasaExpected = command.Value.GetType().Name;

                                //if (command.Value.GetType().HasElementType && command.Value.GetType().GetElementType() == typeof(byte))
                                //    klasaExpected = klasaReceived.Replace("[]", string.Format("[{0}]", command.Bytes.Length));

                                //zamiast:
                                object commandValue = map.GetValue(command);
                                byte[] commandBytes = map.GetBytes(command);
                                if (commandValue.GetType().HasElementType && commandValue.GetType().GetElementType() == typeof(byte))
                                    valueExpected = ((byte[])commandValue).ToHexString();
                                else if (commandValue is DateTime)
                                    valueExpected = string.Format("{0:yyyy-MM-dd HH:mm:ss}", commandValue);
                                //else if (commandValue is Single || commandValue is Double)
                                //    valueExpected = Convert.ToDouble(commandValue).ToString("N");
                                else if (command.Value is Single)
                                    valueExpected = Convert.ToDouble(Convert.ToDecimal(command.Value)).ToString("N");
                                else if (command.Value is Double)
                                    valueExpected = Convert.ToDouble(command.Value).ToString("N");
                                else
                                    valueExpected = commandValue.ToString();

                                string klasaExpected = commandValue.GetType().Name;

                                if (commandValue.GetType().HasElementType && commandValue.GetType().GetElementType() == typeof(byte))
                                    klasaExpected = klasaReceived.Replace("[]", string.Format("[{0}]", commandBytes.Length));

                                string msg = string.Format("[{0}] Get value from database DataType={1}[{2}] Exception: {3}, Received: {4} ({5}) but Expected: {6} ({7})!",
                                    SerialNbr,
                                    command.IdDataType, command.Index,
                                    ex.Message,
                                    valueReceived, klasaReceived,
                                    valueExpected, klasaExpected
                                );

                                throw new Exception(msg, ex);
                                #endregion
                            }
                        }
                        //else if (command.DefaultValue != null)
                        //    command.Value = command.DefaultValue; //podczas pobierania (w GetValue) jak nie ma wartości to brana jest default
                        else
                            missing_values += "\r\n- " + DataType.GetName(command.IdDataType.Value) + "[" + command.Index + "]";
                    }
                    else
                        missing_values += "\r\n- " + DataType.GetName(command.IdDataType.Value) + "[" + command.Index + "]";

                    map.SetStatus(command, Enums.CommandStatus.PutOK);
                }

                if (missing_values != "")
                {
                    Logger.Add(new LogData(9, LogLevel.Error, "Missing values for device {0} in DATA table and default values are not allowed!{1}"), SerialNbr, missing_values);
                    throw new Exception("Missing values for device " + SerialNbr.ToString() + " in DATA table and default values are not allowed!" + missing_values);
                }
            }
            #region //old
            //opróżniłem
            #endregion
        }
        #endregion

        #region Constructor
        //===Projekt #001 "Wysyłanie zapytań przez wymuszoną hierarchie urządzeń"
        //public MemoryMap(GetDataValuesDelegate GetDataValues, GetSlotsOutForDeviceDelegate GetSlotsOutForDevice = null)
        //{
        //    this.GetDataValues = GetDataValues;
        //    this.GetSlotsOutForDevice = GetSlotsOutForDevice;
        //    InstanceAmount = 0;
        //    //Init();
        //}
        //======================END Projekt #001
        public MemoryMap(GetDataValuesDelegate GetDataValues)
        {
            this.GetDataValues = GetDataValues;
            InstanceAmount = 0;
            //Init();
        }
        #endregion

        #region Init
        public virtual void Init(DataValue[] deviceDriverParams)
        {
            if (Inited)
                return;

            DeviceDriverParams = deviceDriverParams;

            Commands = new DeviceCommandList(null);

            FillMemoryMap();

            #region Common Objects
            if (SupportedProtocols.Contains(Enums.Protocol.ImrWan2))
            {
                Commands.Add(new DeviceObjectCommand(DataType.DEVICE_ORDER_NUMBER_NAME, 0, 21, null, null) { ValueToBytesMethod = v => StringWith0x00(v, 21), BytesToValueMethod = StringWith0x00 });
                Commands.Add(new DeviceObjectCommand(DataType.DEVICE_ORDER_NUMBER_FIRST_QUARTER, 1, 5, null, null) { ValueToBytesMethod = v => StringWith0x00(v, 5), BytesToValueMethod = StringWith0x00 });
                Commands.Add(new DeviceObjectCommand(DataType.DEVICE_ORDER_NUMBER_SECOND_QUARTER, 2, 5, null, null) { ValueToBytesMethod = v => StringWith0x00(v, 5), BytesToValueMethod = StringWith0x00 });
                Commands.Add(new DeviceObjectCommand(DataType.DEVICE_ORDER_NUMBER_THIRD_QUARTER, 3, 5, null, null) { ValueToBytesMethod = v => StringWith0x00(v, 5), BytesToValueMethod = StringWith0x00 });
                Commands.Add(new DeviceObjectCommand(DataType.DEVICE_SERIAL_NBR, 4, DeviceCommand.VALUETYPE.SN, null, null));
                for (int i = 0; i < 6; i++)
                {
                    DeviceObjectCommand mainFirmware = new DeviceObjectCommand(5 + i, 41) { Name = "DEVICE_FIRMWARE " + i.ToString() };
                    {
                        mainFirmware.Items.AddAt(i, new DeviceObjectCommand(DataType.DEVICE_FIRMWARE_INFORMATION, 0, 17, new[] { mainFirmware }, new[] { mainFirmware }) { ValueToBytesMethod = v => StringWith0x00(v, 17), BytesToValueMethod = StringWith0x00 });
                        mainFirmware.Items.AddAt(i, new DeviceObjectCommand(DataType.DEVICE_FIRMWARE_VERSION, 17, 2, new[] { mainFirmware }, new[] { mainFirmware }) { BytesToValueMethod = (byte[] bytes) => string.Format("{0:X2}{1:X2}", bytes.Length > 1 ? bytes[1] : 0x00, bytes[0]), ValueToBytesMethod = (object value) => { byte[] bytes = value.ToString().ToBytes().Reverse().ToArray(); return bytes.Length == 2 ? bytes : new byte[] { 0x00, 0x00 }; } });
                        mainFirmware.Items.AddAt(i, new DeviceObjectCommand(DataType.DEVICE_HARDWARE_VERSION, 19, 1, new[] { mainFirmware }, new[] { mainFirmware }) { BytesToValueMethod = (byte[] bytes) => string.Format("{0:X2}", bytes[0]), ValueToBytesMethod = (object value) => { byte[] bytes = value.ToString().ToBytes(); return bytes.Length == 1 ? bytes : new byte[] { 0x00 }; } });
                        mainFirmware.Items.AddAt(i, new DeviceObjectCommand(DataType.DEVICE_FIRMWARE_BUILD_DATE, 20, 21, new[] { mainFirmware }, new[] { mainFirmware }) { ValueToBytesMethod = v => StringWith0x00(v, 21), BytesToValueMethod = StringWith0x00 });
                    }
                    Commands.Add(mainFirmware);
                }
            }
            #endregion

            GlobalOffsetDict = new Dictionary<int, List<DeviceCommand>>();
            GlobalTypeDict = new Dictionary<long, List<DeviceCommand>>();
            GlobalSlotDict = new Dictionary<DeviceCommand.SLOTINFO, List<DeviceCommand>>();

            foreach (DeviceCommand comm in Commands)
            {
                GlobalOffsetDict.Add(comm);
                GlobalTypeDict.Add(comm);
                GlobalSlotDict.Add(comm);
            }
            Inited = true;
        }
        #endregion
        #region FillMemoryMap
        protected virtual void FillMemoryMap()
        {
            return;
        }
        #endregion
        #region Clear
        public virtual void Clear()
        {
            foreach (var key in GlobalOffsetDict.Keys)
            {
                foreach (var item in GlobalOffsetDict[key])
                {
                    item.BytesToValueMethod = null;
                    item.DataGetMeasureValueCreateMethod = null;
                    item.DataPutMeasureValueCreateMethod = null;
                    item.DataGetMeasureValueCreate2Method = null;
                    item.DataPutMeasureValueCreate2Method = null;
                    if (item.GetRequestDeviceCommands != null)
                    {
                        for (int i = 0; i < item.GetRequestDeviceCommands.Length; i++)
                        {
                            item.GetRequestDeviceCommands[i] = null;
                        }
                    }
                    item.Items.Clear();
                    item.Parent = null;
                    if (item.PutRequestDeviceCommands != null)
                    {
                        for (int i = 0; i < item.PutRequestDeviceCommands.Length; i++)
                        {
                            item.PutRequestDeviceCommands[i] = null;
                        }
                    }
                    item.ValueToBytesMethod = null;
                }
            }

            GlobalOffsetDict.Clear();
            GlobalSlotDict.Clear();
            GlobalTypeDict.Clear();
            Commands.Clear();

            GlobalOffsetDict = null;
            GlobalSlotDict = null;
            GlobalTypeDict = null;
            Commands = null;
            DeviceDriverParams = null;

            GetDataValues = null;

            Inited = false;
        }
        #endregion

        #region GetDeviceCommand
        /// <summary>
        /// Gets DeviceCommand for specified IdDataType.
        /// Only one commmand with this IdDataType is allowed to exist, it could not be a slot object or any part of such object.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommand with specified IdDataType or null if not found.</returns>
        public virtual DeviceCommand GetDeviceCommand(long IdDataType)
        {
            if (!Inited)
                Init(DeviceDriverParams);
            if (GlobalTypeDict.ContainsKey(IdDataType))
                return (GlobalTypeDict[IdDataType].FirstOrDefault(c => c.Index == 0 && !c.IsMultiple) ?? GlobalTypeDict[IdDataType][0]);
            else
                return null;
        }

        /// <summary>
        /// Gets 'slot type' DeviceCommand for specified IdDataType, Index.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <param name="Index">Index, it is zero-based index.</param>
        /// <returns>DeviceCommand with specified IdDataType, SlotType and Slot Number or null if not found.</returns>
        public virtual DeviceCommand GetDeviceCommand(long IdDataType, int Index)
        {
            //Logger.Add(new LogData(9, LogLevel.Error, "GetDeviceCommand!, IdDatatype={0}, Inited={1}"), IdDataType, Inited);

            if (!Inited)
                Init(DeviceDriverParams);
            if (GlobalTypeDict.ContainsKey(IdDataType))
            {
                return GlobalTypeDict[IdDataType].FirstOrDefault(c => c.Index == Index && !c.IsMultiple);
            }
            return null;
        }

        /// <summary>
        /// Gets  DeviceCommand for specified IdDataType, Index.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommand with specified IdDataType, SlotType and Slot Number or null if not found.</returns>
        public virtual DeviceCommand GetTableDeviceCommand(long IdDataType)
        {
            if (Inited == false)
                Init(DeviceDriverParams);
            if (GlobalTypeDict.ContainsKey(IdDataType))
            {
                return GlobalTypeDict[IdDataType].FirstOrDefault(c => !c.IsMultiple);
            }
            return null;
        }
        #endregion
        #region GetDeviceCommandsByDataType
        /// <summary>
        /// Gets DeviceCommands for specified IdDataType.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommands with specified IdDataType or null if not found.</returns>
        public virtual DeviceCommand[] GetDeviceCommandsByDataType(long IdDataType)
        {
            if (!Inited)
                Init(DeviceDriverParams);
            if (GlobalTypeDict.ContainsKey(IdDataType))
                return (GlobalTypeDict[IdDataType].Where(c => c.Index == 0 && !c.IsMultiple).ToArray() ?? GlobalTypeDict[IdDataType].ToArray());
            else
                return new DeviceCommand[] { };
        }

        /// <summary>
        /// Gets DeviceCommands for specified IdDataType, Index.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <param name="Index">Index, it is zero-based index.</param>
        /// <returns>DeviceCommands with specified IdDataType and Index.</returns>
        public virtual DeviceCommand[] GetDeviceCommandsByDataType(long IdDataType, int Index)
        {
            //Logger.Add(new LogData(9, LogLevel.Error, "GetDeviceCommand!, IdDatatype={0}, Inited={1}"), IdDataType, Inited);

            if (!Inited)
                Init(DeviceDriverParams);
            if (GlobalTypeDict.ContainsKey(IdDataType))
            {
                return GlobalTypeDict[IdDataType].Where(c => c.Index == Index && !c.IsMultiple).ToArray();
            }
            return new DeviceCommand[] { };
        }
        #endregion
        #region GetDeviceCommandsByOffset
        /// <summary>
        /// Returns all DeviceCommands with specified offset. The return value at index 0 is the
        /// higher level object in memory tree. The following objects are set in deepen order.
        /// </summary>
        /// <param name="offset">Offset in Device Memory Map</param>
        /// <returns>All DeviceCommands with specified offset</returns>
        public virtual DeviceCommand[] GetDeviceCommandsByOffset(int offset)
        {
            if (Inited == false)
                Init(DeviceDriverParams);

            if (GlobalOffsetDict.ContainsKey(offset))
                return GlobalOffsetDict[offset].Where(c => !c.IsMultiple).ToArray();
            else
                return new DeviceCommand[] { };
        }
        #endregion
        #region GetDeviceCommandBySlot
        public virtual DeviceCommand[] GetDeviceCommandsBySlot(DeviceCommand.SLOTINFO info)
        {
            if (Inited == false)
                Init(DeviceDriverParams);

            if (GlobalSlotDict.ContainsKey(info))
                return GlobalSlotDict[info].Where(c => !c.IsMultiple).ToArray();
            else
                return new DeviceCommand[] { };
        }
        public virtual DeviceCommand[] GetDeviceCommandsBySlot(Slot info)
        {
            return GetDeviceCommandsBySlot(new DeviceCommand.SLOTINFO(info.SlotNbr, info.SlotType));
        }
        public virtual DeviceCommand GetDeviceCommandBySlot(Slot info)
        {
            return GetDeviceCommandsBySlot(info).FirstOrDefault();
        }
        #endregion

        #region IEnumerable Members
        public virtual IEnumerator GetEnumerator()
        {
            if (!Inited)
                Init(DeviceDriverParams);
            return Commands.GetEnumerator();
        }
        #endregion

        #region StringWith0x00
        private static object StringWith0x00(byte[] b)
        {
            byte[] bytes = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
                bytes[i] = b[i] == 0x00 ? (byte)' ' : b[i];
            return ASCIIString.BytesToString(bytes);
        }
        private static byte[] StringWith0x00(object v, int len)
        {
            byte[] result = new byte[len];
            char[] value = v.ToString().ToCharArray();
            for (int i = 0; i < value.Length && i < len; i++)
                result[i] = (byte)value[i];
            return result;
        }
        #endregion
    }

    #region Class DictionaryExtension
    /// <summary>
    /// Extends Dictionary&lt;T&gt; type.
    /// </summary>
    public static class DictionaryExtension
    {
        #region (extension) Dictionary<int, List<DeviceCommand>>.Add(DeviceCommand item)
        /// <summary>
        /// Adds item to list at specified index.
        /// </summary>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="item">Item</param>
        public static void Add(this Dictionary<int, List<DeviceCommand>> dictionary, DeviceCommand item)
        {
            dictionary.Add(item.GlobalOffset, item);
            foreach (DeviceCommand comm in item.Items)
            {
                dictionary.Add(comm);
            }
        }
        #endregion

        #region (extension) Dictionary<long, List<DeviceCommand>>.Add(DeviceCommand item)
        /// <summary>
        /// Adds item to list at specified index.
        /// </summary>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="item">Item</param>
        public static void Add(this Dictionary<long, List<DeviceCommand>> dictionary, DeviceCommand item)
        {
            if (item.IdDataType.HasValue)
            {
                dictionary.Add(item.IdDataType.Value, item);
            }
            foreach (DeviceCommand comm in item.Items)
            {
                dictionary.Add(comm);
            }
        }
        #endregion

        #region (extension) Dictionary<DeviceCommand.SLOTINFO, DeviceCommand>.Add(DeviceCommand item)
        /// <summary>
        /// Adds item to list at specified index.
        /// </summary>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="item">Item</param>
        public static void Add(this Dictionary<DeviceCommand.SLOTINFO, List<DeviceCommand>> dictionary, DeviceCommand item)
        {
            if (item.SlotInfo != null)
            {
                dictionary.Add(item.SlotInfo, item);
            }
            foreach (DeviceCommand comm in item.Items)
            {
                dictionary.Add(comm);
            }
        }
        #endregion
    }
    #endregion

    public abstract class MemoryMapForDevice : MemoryMap
    {
        protected CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes;
        protected CacheDictionary<DeviceCommand, Enums.CommandStatus> DeviceCommandStatus;
        protected List<Tuple<Guid?, DeviceCommand>> DeviceCommandsToClean;

        //===Projekt #001 "Wysyłanie zapytań przez wymuszoną hierarchie urządzeń"
        //public MemoryMapForDevice(GetDataValuesDelegate GetDataValues, GetSlotsOutForDeviceDelegate GetSlotsOutForDevice = null)
        //    : base(GetDataValues, GetSlotsOutForDevice)
        //======================END Projekt #001
        public MemoryMapForDevice(GetDataValuesDelegate GetDataValues)
            : base(GetDataValues)
        {
            this.DeviceCommandBytes = new CacheDictionary<DeviceCommand, byte[]>();
            this.DeviceCommandStatus = new CacheDictionary<DeviceCommand, Enums.CommandStatus>();
            DeviceCommandsToClean = new List<Tuple<Guid?, DeviceCommand>>();
        }

        #region StaticMap functions
        public abstract MemoryMap GetStaticMap();
        public abstract void CreateStaticMap();
        public abstract void FreeStaticMap();
        #endregion

        #region Init
        public override void Init(DataValue[] deviceDriverParams)
        {
            base.Init(deviceDriverParams);
            #region StaticMap
            lock (this.GetType())
            {
                if (GetStaticMap() == null)
                {
                    CreateStaticMap();
                    GetStaticMap().Init(deviceDriverParams);
                }
                GetStaticMap().InstanceAmount++;
            }
            #endregion
        }
        #endregion

        #region Clear
        public override void Clear()
        {
            base.Clear();
            lock (DeviceCommandBytes)
            {
                DeviceCommandBytes.Clear();
            }
            lock (DeviceCommandStatus)
            {
                DeviceCommandStatus.Clear();
            }

            #region StaticMap
            lock (this.GetType()) //blokada na tym samym obiekcie co w funkcji Init. Robimy Init lub czyszczenie i tylko jeden wątek naraz.
            {
                //gdy zwalniany jest ostatnia instancja danego typu, to czyszczona jest mapa statyczna
                if (--GetStaticMap().InstanceAmount == 0)
                {
                    GetStaticMap().Clear();
                    FreeStaticMap();
                }
            }
            #endregion
        }
        #endregion

        #region GetValuesForItems
        public override void GetValuesForItems(SN SerialNbr, DeviceCommand parent, DeviceCommand[] receivedChildren)
        {
            GetValuesForItems2(this, SerialNbr, parent, receivedChildren);
        }
        #endregion

        #region GetDeviceCommand
        /// <summary>
        /// Gets DeviceCommand for specified IdDataType.
        /// Only one commmand with this IdDataType is allowed to exist, it could not be a slot object or any part of such object.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommand with specified IdDataType or null if not found.</returns>
        public override DeviceCommand GetDeviceCommand(long IdDataType)
        {
            return GetStaticMap().GetDeviceCommand(IdDataType);
        }

        /// <summary>
        /// Gets 'slot type' DeviceCommand for specified IdDataType, Index.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <param name="Index">Index, it is zero-based index.</param>
        /// <returns>DeviceCommand with specified IdDataType, SlotType and Slot Number or null if not found.</returns>
        public override DeviceCommand GetDeviceCommand(long IdDataType, int Index)
        {
            return GetStaticMap().GetDeviceCommand(IdDataType, Index);
        }

        /// <summary>
        /// Gets  DeviceCommand for specified IdDataType, Index.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommand with specified IdDataType, SlotType and Slot Number or null if not found.</returns>
        public override DeviceCommand GetTableDeviceCommand(long IdDataType)
        {
            return GetStaticMap().GetTableDeviceCommand(IdDataType);
        }
        #endregion
        #region GetDeviceCommandsByDataType
        /// <summary>
        /// Gets DeviceCommands for specified IdDataType.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommands with specified IdDataType or null if not found.</returns>
        public override DeviceCommand[] GetDeviceCommandsByDataType(long IdDataType)
        {
            return GetStaticMap().GetDeviceCommandsByDataType(IdDataType);
        }
        /// <summary>
        /// Gets DeviceCommands for specified IdDataType.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommands with specified IdDataType or null if not found.</returns>
        public override DeviceCommand[] GetDeviceCommandsByDataType(long IdDataType, int Index)
        {
            return GetStaticMap().GetDeviceCommandsByDataType(IdDataType, Index);
        }
        #endregion
        #region GetDeviceCommandsByOffset
        public override DeviceCommand[] GetDeviceCommandsByOffset(int offset)
        {
            return GetStaticMap().GetDeviceCommandsByOffset(offset);
        }
        #endregion
        #region GetDeviceCommandBySlot
        public override DeviceCommand[] GetDeviceCommandsBySlot(DeviceCommand.SLOTINFO info)
        {
            return GetStaticMap().GetDeviceCommandsBySlot(info);
        }
        public override DeviceCommand[] GetDeviceCommandsBySlot(Slot info)
        {
            return GetStaticMap().GetDeviceCommandsBySlot(info);
        }
        public override DeviceCommand GetDeviceCommandBySlot(Slot info)
        {
            return GetStaticMap().GetDeviceCommandBySlot(info);
        }
        #endregion

        #region IEnumerable Members
        public override IEnumerator GetEnumerator()
        {
            return GetStaticMap().GetEnumerator();
            //????? 
            //może tu zwracać obiekty innego typu niż DeviceCommand, typu dziedziczącego po DeviceCommand, 
            //który ma przeciążone bytes, value i status w ten sposób, że odwołanie do takiego pola powoduje błąd kompilacji najlepiej
            //return ((IEnumerable<DeviceCommand>)GetStaticMap()).Select(dc => new DeviceCommandForDevice() { map = this, command = dc }).GetEnumerator();
        }
        #endregion
        #region //class DeviceCommandForDevice
        //public class DeviceCommandForDevice : DeviceCommand
        //{
        //    public MemoryMapForDevice map;
        //    public DeviceCommand command;

        //    public override byte[] Bytes
        //    {
        //        get
        //        {
        //            return map.GetBytes(command);
        //        }
        //        set
        //        {
        //            map.SetBytes(command, value);
        //        }
        //    }
        //    public override object Value
        //    {
        //        get
        //        {
        //            return map.GetValue(command);
        //        }
        //        set
        //        {
        //            map.SetValue(command, value);
        //        }
        //    }
        //    public override Enums.CommandStatus Status
        //    {
        //        get
        //        {
        //            return map.GetStatus(command);
        //        }
        //        set
        //        {
        //            map.SetStatus(command, value);
        //        }
        //    }
        //}
        #endregion

        public byte[] GetBytes(DeviceCommand command)
        {
            lock (DeviceCommandBytes)
            {
                return command.GetBytes(this.DeviceCommandBytes);                
            }
        }
        public void SetBytes(DeviceCommand command, byte[] value)
        {
            lock (DeviceCommandBytes)
            {
                command.SetBytes(this.DeviceCommandBytes, value);                
            }
        }
        public object GetValue(DeviceCommand command)
        {
            lock (DeviceCommandBytes)
            {
                return command.GetValue(this.DeviceCommandBytes);               
            }
        }
        public void SetValue(DeviceCommand command, object value)
        {
            lock (DeviceCommandBytes)
            {
                command.SetValue(this.DeviceCommandBytes, value);                
            }
        }
        public Enums.CommandStatus GetStatus(DeviceCommand command)
        {
            lock (DeviceCommandStatus)
            {
                return command.GetStatus(this.DeviceCommandStatus);                
            }
        }
        public void SetStatus(DeviceCommand command, Enums.CommandStatus value)
        {
            lock (DeviceCommandStatus)
            {
                command.SetStatus(this.DeviceCommandStatus, value);                
            }
        }
        public void ClearCommand(DeviceCommand command)
        {
            lock (DeviceCommandBytes)
            {
                command.ClearBytes(this.DeviceCommandBytes);                
            }
            lock (DeviceCommandStatus)
            {
                command.ClearStatus(this.DeviceCommandStatus);                
            }
        }

        #region AddDeviceCommandToClean
        //        public void AddDeviceCommandToClean(DeviceCommand deviceCommand)
        //        {
        //            this.DeviceCommandsToClean.Add(new Tuple<Guid?, DeviceCommand>(null,deviceCommand));
        //        }
        public void AddDeviceCommandToClean(DeviceCommand deviceCommand, Guid? guid)
        {
            lock (DeviceCommandsToClean)
            {
                this.DeviceCommandsToClean.Add(new Tuple<Guid?, DeviceCommand>(guid, deviceCommand));
            }
        }
        #endregion

        #region CleanUnusedCommands
        public void CleanUnusedCommands(Guid? guid)
        {
            List<Tuple<Guid?, DeviceCommand>> toRemove = new List<Tuple<Guid?, DeviceCommand>>();
            lock (DeviceCommandsToClean)
            {
                if (guid.HasValue && this.DeviceCommandsToClean.Any(c => guid.Value.Equals(c.Item1)))
                {
                    //Logger.Add(new LogData(9, LogLevel.Error, "!!!!!!!!! CleanUnusedCommands, GUID={0}, idPacket={1} , commands FOUND"), guid, idPacket);
                    toRemove.AddRange(this.DeviceCommandsToClean.Where(t => t.Item1.HasValue && t.Item1.Value.Equals(guid.Value)));
                }
                else if (!guid.HasValue)
                {
                    //Logger.Add(new LogData(9, LogLevel.Error, "!!!!!!!!! CleanUnusedCommands, NO GUID VALUE!!!"));
                    toRemove.AddRange(this.DeviceCommandsToClean);
                }
                else
                {
                    //Logger.Add(new LogData(9, LogLevel.Error, "!!!!!!!!! CleanUnusedCommands, GUID={0}, idPacket={1} , commands NOT FOUND"), guid, idPacket);
                }
            }

            toRemove.ForEach(c =>
            {
                DeviceCommand command = c.Item2;
                ClearCommand(command);
                command = null;
            });
            lock (DeviceCommandsToClean)
            {
                toRemove.ForEach(t => this.DeviceCommandsToClean.Remove(t));
            }
        }
        #endregion
    }
}
