﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Reflection;
using IMR.Suite.Common.Code;
#if !WindowsCE
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
#endif

using IMR.Suite.Common;

namespace IMR.Suite.Common.Drivers.Device
{
#if !WindowsCE && !Android
    [Serializable]
    public class DeviceDriverWrapper : MarshalByRefObjectDisposable
    {
        #region public Members
        public AssemblyName DeviceDriverName = null;    // Jest to jednocześnie flaga mówiąca czy DLLka jest już załadowana do pamięci (==null) => nie, trzeba załadować)
        public Type DeviceDriverType = default(Type);
        public string DeviceDriverPath = string.Empty;
        public long InstanceCount = 0;

        public bool DriverDependenciesOutdated = false;
        #endregion

        #region private Members
        private bool UseAppDomain = false;
        private readonly AppDomain DeviceDriverDomain;
        private object deviceCreateLockObject = new object();        
        #endregion

        #region Events
        public event EventHandler<EventArgs<string>> DeviceDriverLoadedEvent;
        #endregion

        #region Constructor
        public DeviceDriverWrapper()
        {
        }
        public DeviceDriverWrapper(string deviceDriverPath, string protocolsPath, bool useAppDomain, bool driverDependenciesOutdated = false)
        {
            DeviceDriverPath = deviceDriverPath;
            UseAppDomain = useAppDomain;
            DriverDependenciesOutdated = driverDependenciesOutdated;

            if (!System.IO.Path.IsPathRooted(DeviceDriverPath))
                DeviceDriverPath = AppDomain.CurrentDomain.BaseDirectory + DeviceDriverPath;
            DeviceDriverPath = Path.GetFullPath(DeviceDriverPath);

            #region Jeśli ścieżka jest poza (not relative to base) ścieżką bazową EXE to kopiujemy do Cache (relative to base)
            string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
            string DllPath = Path.GetDirectoryName(DeviceDriverPath);
            if (DllPath.IndexOf(AppPath) == -1)
            {
                string dllDirectoryName = Path.Combine(AppPath, "Cache");
                DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
                if (!di.Exists)
                {
                    di.Create();
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }

                string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(DeviceDriverPath);
                try
                {
                    File.Copy(DeviceDriverPath, newAssemblyPath, true);
                }
                catch
                {
                }
                DeviceDriverPath = newAssemblyPath;
            }
            #endregion

            if (UseAppDomain)
            {
                AppDomainSetup ads = new AppDomainSetup();
                ads.ApplicationName = "DeviceDriverWrapper";
                ads.ApplicationBase = AppPath;
                ads.PrivateBinPath = new Uri(AppPath + "\\").MakeRelativeUri(new Uri(DllPath)).ToString();
                ads.PrivateBinPath += ";" + new Uri(AppPath + "\\").MakeRelativeUri(new Uri(protocolsPath)).ToString();
                ads.ShadowCopyFiles = "true";

                DeviceDriverDomain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(DeviceDriverPath), null, ads);
             }
        }
        #endregion
        #region Destructor
        ~DeviceDriverWrapper()
        {
            Dispose(false);
        }
        #endregion

        #region Unload
        public void Unload(bool forceDispose)
        {
            try
            {
                DeviceDriverName = null;
                if (UseAppDomain)
                {
                    if (!forceDispose && Interlocked.Read(ref InstanceCount) == 0)
                        AppDomain.Unload(DeviceDriverDomain);
                    else if (forceDispose)
                        AppDomain.Unload(DeviceDriverDomain);
                }
                GC.Collect();
            }
            catch { }
        }
        #endregion

        #region CreateInstance
        public DeviceDriver CreateInstance(object[] DriverConstructorArgs)
        {
            lock (deviceCreateLockObject)
            {
                if (DeviceDriverName == null) // jeszcze nie załadowana lub została odładowana
                {
                    Assembly assembly = null;

                    if (UseAppDomain && DeviceDriverDomain != null)
                    {
                        using (DeviceDriverWrapper wrapper = (DeviceDriverWrapper)DeviceDriverDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(DeviceDriverWrapper).FullName))
                            assembly = wrapper.LoadAssembly(DeviceDriverPath);
                    }
                    else
                        assembly = AssemblyWrapper.LoadAssembly(DeviceDriverPath);

                    if (assembly == null)
                        return null;

                    DeviceDriverType = GetDerivedType(assembly.GetTypes(), typeof(DeviceDriver));
                    if (DeviceDriverType == default(Type))
                        return null;

                    DeviceDriverName = assembly.GetName();

                    // poniższy IF mozna zastąpić tym (jak będzie VS2015 lub wyższy): DeviceDriverCreatedEvent?.Invoke(deviceDriverName, new EventArgs<string>(Path.GetFileName(DeviceDriverPath)));
                    if (DeviceDriverLoadedEvent != null)
                        DeviceDriverLoadedEvent.Invoke(this, new EventArgs<string>(Path.GetFileName(DeviceDriverPath)));
                }

                DeviceDriver driver = null;
                // tu bezpieczenstwo zapewnia nam lock (deviceInfo.LockObjForGetDriver)
                if (UseAppDomain && DeviceDriverDomain != null)
                    driver = (DeviceDriver)DeviceDriverDomain.CreateInstanceAndUnwrap(DeviceDriverName.FullName, DeviceDriverType.ToString(), false, BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, DriverConstructorArgs, null, null);
                else
                    driver = (DeviceDriver)Activator.CreateInstance(DeviceDriverType, DriverConstructorArgs);

                if (driver == null)
                    return null;

                driver.NameAndVersion = string.Format("{0}, {1}", DeviceDriverName.Name, DeviceDriverName.Version.ToString());
                driver.DriverDependenciesOutdated = DriverDependenciesOutdated;
                Interlocked.Increment(ref InstanceCount);
                return driver;
           }
        }
        #endregion
        #region FreeInstance
        public void FreeInstance(DeviceDriver driver)
        {
            if (driver != null)
            {
                try
                {
                    Interlocked.Decrement(ref InstanceCount);

                }
                finally
                {
                    driver = default(DeviceDriver);
                }
            }
        }
        #endregion

        #region IsProtocolDriverLoaded
        public bool IsProtocolDriverLoaded(string protocolDriverName)
        {
            Assembly assembly = AssemblyWrapper.LoadAssembly(DeviceDriverPath);
            return assembly.GetReferencedAssemblies().Any(w => w.Name == Path.GetFileNameWithoutExtension(protocolDriverName));
        }
        #endregion

        #region private GetDerivedType
        private Type GetDerivedType(Type[] assemblyTypes, Type typeToFind)
        {
            foreach (Type typeInAssembly in assemblyTypes)
            {
                if (typeInAssembly.IsClass && typeInAssembly.IsPublic && !typeInAssembly.IsAbstract)
                {
                    var found = false;
                    Type baseClass = typeInAssembly;
                    do
                    {
                        found = typeToFind.GUID == baseClass.GUID;
                        baseClass = baseClass.BaseType;
                    }
                    while (baseClass != null && !found);

                    if (found)
                        return typeInAssembly;
                }
            }
            return default(Type);
        }
        #endregion
        #region private LoadAssembly
        private Assembly LoadAssembly(string deviceDriverPath)
        {
            Assembly assembly = Assembly.Load(AssemblyName.GetAssemblyName(deviceDriverPath));
            if (assembly == null)
                return null;

            foreach (AssemblyName an in assembly.GetReferencedAssemblies())
                Assembly.Load(an);

            return assembly;
        }
        #endregion
    }
#endif
}
