﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using IMR.Suite.Common;

namespace IMR.Suite.Common.Drivers.Device
{
    #region Class DeviceCommand
    /// <summary>
    /// Base class for all DeviceCommand, it does not occur in MemoryMap. 
    /// </summary>
    [Serializable]
    public class DeviceCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="Size">Size in bits.</param>
        public DeviceCommand(int ParentOffset, int Size)
            : this(null, ParentOffset, Size, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="Size">Size in bits.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, int Size, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, Size, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="Size">Size in bits.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, int Size, int? Index, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, Size, Index, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="Size">Size in bits.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, int Size, int? Index, TABLEInfo tableInfo, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
        {
            this.IdDataType = IdDataType;
            this.GetRequestDeviceCommands = GetRequestDeviceCommands;
            this.PutRequestDeviceCommands = PutRequestDeviceCommands;
            this.GlobalOffset = ParentOffset;
            this.ParentOffset = ParentOffset;
            this.IsGUIVisible = true;
            //this.IsProduceValue = true;
            this.Items = new DeviceCommandList(this);
            this.Index = Index ?? 0;
            if (tableInfo != null)
                this.TableInfo = new TABLEInfo() { AccessParameter = tableInfo.AccessParameter, RowBegin = tableInfo.RowBegin, RowEnd = tableInfo.RowEnd, ColBegin = tableInfo.ColBegin, ColEnd = tableInfo.ColEnd };
            this.DataGetMeasureValueCreateMethod = SimpleCreateDataMeasureValue;
            this.DataGetMeasureValueCreate2Method = SimpleCreateDataMeasureValue2;
            this.DataPutMeasureValueCreateMethod = SimpleCreateDataMeasureValue;
            this.DataPutMeasureValueCreate2Method = SimpleCreateDataMeasureValue2;
            #region this part is different for next constructor
            this.Size = Size;

            this.ValueToBytesMethod = v => (byte[])v;
            this.BytesToValueMethod = b => (object)b;

            this.bytes = new byte[(int)Math.Ceiling((double)Size / 8.0)];
            #endregion
        }

        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        public DeviceCommand(int ParentOffset, VALUETYPE ValueType)
            : this(null, ParentOffset, ValueType, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, int? Index, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, Index, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, int? Index, TABLEInfo tableInfo, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, 8, Index, tableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands)
        {
            switch (ValueType)
            {
                #region short
                case VALUETYPE.INT16:
                case VALUETYPE.SHORT:
                    this.Size = 16;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToInt16(v));
                    this.BytesToValueMethod = b => b.Length == 2 ? BitConverter.ToInt16(b, 0) : (Int16)0;
                    break;
                #endregion
                #region int
                case VALUETYPE.INT32:
                    this.Size = 32;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToInt32(v));
                    this.BytesToValueMethod = b => b.Length == 4 ? BitConverter.ToInt32(b, 0) : (Int32)0;
                    break;
                #endregion
                #region long
                case VALUETYPE.INT64:
                case VALUETYPE.LONG:
                    this.Size = 64;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToInt64(v));
                    this.BytesToValueMethod = b => b.Length == 8 ? BitConverter.ToInt64(b, 0) : (Int64)0;
                    break;
                #endregion
                #region ushort
                case VALUETYPE.UINT16:
                case VALUETYPE.USHORT:
                    this.Size = 16;
                    this.ValueToBytesMethod = v => Convert.ToInt32(v) > ushort.MaxValue ? new byte[] { 0xFF, 0xFF } : BitConverter.GetBytes(Convert.ToUInt16(v));
                    this.BytesToValueMethod = b =>
                    {
                        short i = b.Length == 2 ? BitConverter.ToInt16(b, 0) : (Int16)0;
                        if (i < 0)
                            return BitConverter.ToInt32(new byte[4] { b[0], b[1], 0x00, 0x00 }, 0);
                        else
                            return i;
                    };
                    break;
                #endregion
                #region uint
                case VALUETYPE.UINT32:
                    this.Size = 32;
                    this.ValueToBytesMethod = v => Convert.ToInt64(v) > uint.MaxValue ? new byte[] { 0xFF, 0xFF, 0xFF, 0xFF } : BitConverter.GetBytes(Convert.ToUInt32(v));
                    this.BytesToValueMethod = b =>
                    {
                        int i = b.Length == 4 ? BitConverter.ToInt32(b, 0) : (Int32)0;
                        return (i < 0) ? BitConverter.ToInt64(new byte[] { b[0], b[1], b[2], b[3], 0x00, 0x00, 0x00, 0x00 }, 0) : i;
                    };
                    break;
                #endregion
                #region ulong !!
                case VALUETYPE.UINT64:
                case VALUETYPE.ULONG:
                    this.Size = 64;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToUInt64(v));
                    this.BytesToValueMethod = b => b.Length == 8 ? BitConverter.ToUInt64(b, 0) : (UInt64)0;
                    break;
                #endregion
                #region byte
                case VALUETYPE.BYTE:
                    this.Size = 8;
                    this.ValueToBytesMethod = v => new byte[] { Convert.ToByte(v) };
                    this.BytesToValueMethod = b => b[0];
                    break;
                #endregion
                #region sbyte
                case VALUETYPE.SBYTE:
                    this.Size = 8;
                    this.ValueToBytesMethod = v =>
                    {
                        byte[] result = new byte[1];
                        sbyte[] sbytes = new sbyte[] { unchecked((sbyte)Convert.ToInt16(v)) };
                        Buffer.BlockCopy(sbytes, 0, result, 0, 1);
                        return result;
                    };
                    this.BytesToValueMethod = b =>
                    {
                        sbyte[] sbytes = new sbyte[1];
                        Buffer.BlockCopy(b, 0, sbytes, 0, 1);
                        return sbytes[0];
                    };
                    break;
                #endregion
                #region char
                case VALUETYPE.CHAR_UNICODE:
                    this.Size = 16;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToChar(v));
                    this.BytesToValueMethod = b => b.Length == 2 ? BitConverter.ToChar(b, 0) : '?';
                    break;
                #endregion
                #region float
                case VALUETYPE.FLOAT:
                    this.Size = 32;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToSingle(Convert.ToDecimal(Convert.ToDouble(v))));
                    this.BytesToValueMethod = b => b.Length == 4 ? Convert.ToDouble(Convert.ToDecimal(BitConverter.ToSingle(b, 0))) : (Double)0.0;
                    break;
                #endregion
                #region double
                case VALUETYPE.DOUBLE:
                    this.Size = 64;
                    this.ValueToBytesMethod = v => BitConverter.GetBytes(Convert.ToDouble(v));
                    this.BytesToValueMethod = b => b.Length == 8 ? BitConverter.ToDouble(b, 0) : 0.0;
                    break;
                #endregion
                #region decimal
                case VALUETYPE.DECIMAL:
                    this.Size = 128;
                    this.ValueToBytesMethod = v => ByteArrayAndBitConverterExtension.GetBytes(Convert.ToDecimal(v));
                    this.BytesToValueMethod = b => b.Length == 16 ? ByteArrayAndBitConverterExtension.ToDecimal(b) : (decimal)0;
                    break;
                #endregion
                #region bool
                case VALUETYPE.BOOL:
                    this.Size = 8;
                    this.OnBytesToValueEvent += (byte[] bytes) => bytes[0] != 0x00;
                    this.OnValueToBytesEvent += (object value) => new byte[] { (byte)(Convert.ToBoolean(value) == true ? 0x01 : 0x00) };
                    break;
                #endregion
                #region DateTime
                case VALUETYPE.DATETIME:
                    this.Size = 32;
                    this.OnBytesToValueEvent += (byte[] bytes) => (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddSeconds(Convert.ToDouble(BitConverter.ToInt32(bytes, 0)));
                    this.OnValueToBytesEvent += (object value) => BitConverter.GetBytes(Convert.ToInt32(new TimeSpan((Convert.ToDateTime(value)).Subtract(new DateTime(1970, 1, 1)).Ticks).TotalSeconds));
                    break;
                #endregion
                #region SN
                case VALUETYPE.SN:
                    this.Size = 32;
                    this.OnBytesToValueEvent += (byte[] bytes) => { return (long)((new SN(bytes)).DBValue ?? 0); };
                    this.OnValueToBytesEvent += (object value) => (byte[])SN.FromDBValue(Convert.ToUInt64(value));
                    break;
                #endregion
                #region IP
                case VALUETYPE.IP:
                    this.Size = 32;
                    this.OnBytesToValueEvent += (byte[] bytes) => string.Format("{0}.{1}.{2}.{3}", bytes[0], bytes[1], bytes[2], bytes[3]);
                    this.OnValueToBytesEvent += (object value) =>
                    {
                        string tmp = value != null ? value.ToString() : "0";
                        if (tmp == "0")
                            tmp = "0.0.0.0";
                        string[] d = tmp.Split(new char[] { '.' });
                        return new byte[] { byte.Parse(d[0]), byte.Parse(d[1]), byte.Parse(d[2]), byte.Parse(d[3]) };
                    };
                    break;
                #endregion
                #region int24
                case VALUETYPE.INT24:
                    this.Size = 24;
                    this.OnBytesToValueEvent += (byte[] b) => BitConverter.ToInt32(new byte[] { b[0], b[1], b[2], (byte)(((b[2] & 0x80) == 0x80) ? 0xFF : 0x00) }, 0);
                    this.OnValueToBytesEvent += (object v) => BitConverter.GetBytes(Convert.ToInt32(v)).Take(3).ToArray();
                    break;
                case VALUETYPE.UINT24:
                    this.Size = 24;
                    this.OnBytesToValueEvent += (byte[] b) => BitConverter.ToInt32(new byte[] { b[0], b[1], b[2], 0 }, 0);
                    this.OnValueToBytesEvent += (object v) => BitConverter.GetBytes(Convert.ToInt32(v)).Take(3).ToArray();
                    break;
                #endregion
                default:
                    break;
            }

            this.bytes = new byte[Size / 8];
        }
        #endregion

        #region Offsets (Global and Parent)
        /// <summary>
        /// Global offset in Device Memory Map
        /// </summary>
        public int GlobalOffset { get; set; }

        /// <summary>
        /// Offset in Parent object
        /// </summary>
        public int ParentOffset { get; set; }
        #endregion

        #region Size (in bits!)
        /// <summary>
        /// Size in bits
        /// </summary>
        public int Size { get; private set; }
        #endregion

        #region Parent
        /// <summary>
        /// Reference to higher level object, if it is null
        /// </summary>
        public DeviceCommand Parent { get; set; }
        #endregion

        #region Items
        /// <summary>
        /// List of component objects
        /// </summary>
        public DeviceCommandList Items { get; private set; }
        #endregion

        #region Bytes (obsolete)
        //deprecated (po zmianie na mapę statyczną), z tego property korzystają tylko drivery w starym modelu
        [Obsolete("This property is obsolete due to the use of static MemoryMap")]
        byte[] bytes;
        /// <summary>
        /// Byte array. Those are bytes which are put to device (on put request) or get from device (on get request).
        /// </summary>
        [Obsolete("This property is obsolete due to the use of static MemoryMap")]
        public virtual byte[] Bytes
        {
            get
            {
                if (bytes != null)
                {
                    if (Items.Count > 0)
                    {
                        BitArray bits = new BitArray(bytes);
                        int byteOffset = 0;
                        foreach (DeviceCommand item in Items)
                        {
                            byteOffset = item.GlobalOffset - this.GlobalOffset;
                            if (item is DeviceBitCommand)
                            {
                                bits.Set(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1, (bool)((DeviceBitCommand)item).Value);
                            }
                            else
                            {
                                BitArray val = new BitArray(item.Bytes);
                                for (int i = 0; i < item.Size; i++)
                                {
                                    bits.Set(byteOffset * 8 + i, val.Get(i));
                                }
                            }
                        }
                        bits.CopyTo(bytes, 0);
                    }
                }
                return bytes;
            }
            set
            {
                //this.bytes = new byte[0]; return; //??? czemu to tu dałem? do testów?
                //if ((this is DeviceObjectCommand && value.Length <= this.Size / 8) || ((this is DeviceObjectCommand == false) && value.Length == this.bytes.Length))
                if (this is DeviceObjectCommand || ((this is DeviceObjectCommand == false) && value.Length == this.bytes.Length))
                {
                    if (this is DeviceObjectCommand && value.Length > this.Size / 8)
                    {
                        this.bytes = value.Take(this.Size / 8).ToArray();
                        Logger.Add(new LogData(0, LogLevel.Fatal, String.Format("DeviceDriver is trying illegal write do DeviceObjectCommand: GlobalOffset={0} IdDataType={1} ", this.GlobalOffset, this.IdDataType)));
                    }
                    else
                        this.bytes = value;
                    if (Items.Count > 0)
                    {
                        BitArray bits = new BitArray(value);
                        int byteOffset = 0;
                        foreach (DeviceCommand item in Items)
                        {
                            byteOffset = item.GlobalOffset - this.GlobalOffset;
                            if (item is DeviceBitCommand)
                            {
                                //if (bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1) == true)
                                //    item.Bytes = new byte[] { 0x01 };
                                //else
                                //    item.Bytes = new byte[] { 0x00 };
                                item.Value = bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1);
                            }
                            else
                            {
                                byte[] b = new byte[item.Size / 8];
                                Buffer.BlockCopy(value, byteOffset, b, 0, b.Length);
                                item.Bytes = b;
                            }
                        }
                    }
                }
                else if (this is DeviceTableCommand)
                {
                    this.bytes = value;
                }
                else
                {
                    if (Parent != null)
                        Parent.Bytes = value;
                    else
                        throw new Exception(String.Format("Value length invalid, need: {0} bytes, got: {1} bytes!", this.bytes.Length, value.Length));
                }
            }
        }
        #endregion
        #region GetBytes
        public virtual byte[] GetBytes(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes)
        {
            byte[] bytes;
            bool found = DeviceCommandBytes.TryGetValue(this, out bytes);
            if (!found)
            {
                try
                {
                    if (this.defaultValue != null)
                        bytes = ValueToBytesMethod(this.defaultValue);
                    if (bytes == null) //zamiast w konstruktorze DeviceCommand
                        bytes = new byte[(int)Math.Ceiling((double)Size / 8.0)]; //zamiast w konstruktorze DeviceCommand
                    //bytes = new byte[(Size+7)/8]; //lepiej tak, zamiast przechodzić przez Double, ale na razie absolutnie nic nie zmieniam
                }
                catch
                {
                    bytes = null;
                }
            }

            if (bytes != null)
            {
                if (Items.Count > 0) // && !Items.Any(item => item.DeviceCommandType != null && item.DeviceCommandType == COMMAND_TYPE.FLEXIBLE))
                {
                    BitArray bits = new BitArray(bytes);
                    int byteOffset = 0;
                    foreach (DeviceCommand item in Items)
                    {
                        byteOffset = item.GlobalOffset - this.GlobalOffset;
                        if (item is DeviceBitCommand)
                        {
                            //bits.Set(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1, (bool)((DeviceBitCommand)item).Value);
                            bits.Set(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1, (bool)((DeviceBitCommand)item).GetValue(DeviceCommandBytes)); //zamiast
                        }
                        //else if(item.DeviceCommandType != null &&  item.DeviceCommandType == COMMAND_TYPE.FLEXIBLE)
                        //{
                        //    //BitArray val = new BitArray(item.Bytes);
                        //    BitArray val = new BitArray(item.GetBytes(DeviceCommandBytes)); //zamiast
                        //    for (int i = 0; i < item.Size; i++)
                        //    {
                        //        bits.Set(byteOffset * 8 + i, val.Get(i));
                        //    }
                        //}
                        else
                        {
                            //BitArray val = new BitArray(item.Bytes);
                            BitArray val = new BitArray(item.GetBytes(DeviceCommandBytes)); //zamiast
                            for (int i = 0; i < item.Size; i++)
                            {
                                bits.Set(byteOffset * 8 + i, val.Get(i));
                            }
                        }
                    }
                    bits.CopyTo(bytes, 0);
                }
            }
            return bytes;
        }
        #endregion
        #region SetBytes
        public virtual void SetBytes(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes, byte[] value)
        {
            byte[] bytes;
            bool found = DeviceCommandBytes.TryGetValue(this, out bytes);
            if (!found)
                bytes = new byte[0];
            if (this.DeviceCommandType != null && (this.DeviceCommandType == COMMAND_TYPE.DIAGNOSTICS || this.DeviceCommandType == COMMAND_TYPE.FLEXIBLE || this.DeviceCommandType == COMMAND_TYPE.TABLE) && value.Length <= this.Size/8)//MAX SIZE - ale wiekszy niż poprzedio zapisany też może przecież być
            {
                DeviceCommandBytes[this] = value; //zamiast
                if (Items.Count > 0)
                {
                    BitArray bits = new BitArray(value);
                    int byteOffset = 0;
                    foreach (DeviceCommand item in Items)
                    {
                        byteOffset = item.GlobalOffset - this.GlobalOffset;
                        if (item is DeviceBitCommand)
                        {
                            //if (bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1) == true)
                            //    item.Bytes = new byte[] { 0x01 };
                            //else
                            //    item.Bytes = new byte[] { 0x00 };
                            //item.Value = bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1);
                            item.SetValue(DeviceCommandBytes, bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1)); //zamiast
                        }
                        //else if(item.DeviceCommandType != null && item.DeviceCommandType == COMMAND_TYPE.FLEXIBLE)
                        //{
                        //    byte[] b = new byte[value.Length - byteOffset ]; //always last FLEXIBLE OBJ (to the end of the value)
                        //    Buffer.BlockCopy(value, byteOffset, b, 0, b.Length);
                        //    //item.Bytes = b;
                        //    item.SetBytes(DeviceCommandBytes, b);
                        //}
                        else
                        {
                            byte[] b = new byte[item.Size / 8];
                            Buffer.BlockCopy(value, byteOffset, b, 0, b.Length);
                            //item.Bytes = b;
                            item.SetBytes(DeviceCommandBytes, b);
                        }
                    }
                }
 
            }
            else if (this is DeviceObjectCommand || this is DeviceBytesCommand || ((this is DeviceObjectCommand == false) && value.Length == bytes.Length))
            {
                //this.bytes = value;
                DeviceCommandBytes[this] = value; //zamiast
                if (Items.Count > 0)
                {
                    BitArray bits = new BitArray(value);
                    int byteOffset = 0;
                    foreach (DeviceCommand item in Items)
                    {
                        byteOffset = item.GlobalOffset - this.GlobalOffset;
                        if (item is DeviceBitCommand)
                        {
                            //if (bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1) == true)
                            //    item.Bytes = new byte[] { 0x01 };
                            //else
                            //    item.Bytes = new byte[] { 0x00 };
                            //item.Value = bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1);
                            item.SetValue(DeviceCommandBytes, bits.Get(byteOffset * 8 + ((DeviceBitCommand)item).BitNumber - 1)); //zamiast
                        }
                        else
                        {
                            byte[] b = new byte[item.Size / 8];
                            Buffer.BlockCopy(value, byteOffset, b, 0, b.Length);
                            //item.Bytes = b;
                            item.SetBytes(DeviceCommandBytes, b);
                        }
                    }
                }
            }
            else if (this is DeviceTableCommand)
            {
                //this.bytes = value;
                DeviceCommandBytes[this] = value; //zamiast
            }
            else if (this is DeviceBitCommand) //nowe, wcześniej nieobsługiwane
            {
                DeviceCommandBytes[this] = value; //zamiast
            }
            else
            {
                if (Parent != null)
                    //Parent.Bytes = value;
                    Parent.SetBytes(DeviceCommandBytes, value); //zamiast
                else
                    //throw new Exception(String.Format("Value length invalid, need: {0} bytes, got: {1} bytes!", this.bytes.Length, value.Length));
                    throw new Exception(String.Format("Value length invalid, need: {0} bytes, got: {1} bytes!", bytes.Length, value.Length)); //zamiast
            }
        }
        #endregion
        #region ClearBytes
        public bool ClearBytes(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes)
        {
            return DeviceCommandBytes.Remove(this);
        }
        #endregion

        #region Value (obsolete)
        //deprecated (po zmianie na mapę statyczną), z tego property korzystają tylko drivery w starym modelu
        /// <summary>
        /// Object value, it is always transcoded to/from bytes. It could be of any type.
        /// Warning: If it is one of predefined VALUETYPE it must be cast to exactly its type otherwise it will crash application.
        /// Example: if value is of type VALUETYPE.UINT32 and you want to assign Int32 to its value, it will crash! You must cast it to UInt32 before.
        /// </summary>
        [Obsolete("This property is obsolete due to the use of static MemoryMap")]
        public virtual object Value
        {
            get { return BytesToValueMethod(Bytes); }
            set
            {
                if (OnValueValidation != null)
                {
                    InvalidValueInfo info = new InvalidValueInfo();
                    OnValueValidation(value, ref info);
                    if (info.IsInvalid)
                        throw new Exception(info.Message);
                }
                Bytes = ValueToBytesMethod(value);
            }
        }
        #endregion
        #region GetValue
        public object GetValue(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes)
        {
            return BytesToValueMethod(GetBytes(DeviceCommandBytes));
        }
        #endregion
        #region SetValue
        public void SetValue(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes, object value)
        {
            if (OnValueValidation != null)
            {
                InvalidValueInfo info = new InvalidValueInfo();
                OnValueValidation(value, ref info);
                if (info.IsInvalid)
                    throw new Exception(info.Message);
            }
            //Bytes = ValueToBytesMethod(value);
            SetBytes(DeviceCommandBytes, ValueToBytesMethod(value)); //zamiast
        }
        #endregion

        #region Value convertion
        #region ValueToBytes
        /// <summary>
        /// Converts value to byte array. Returned array length must be the same as Bytes length, otherwise it will crash!
        /// </summary>
        /// <param name="value">value of any type</param>
        /// <returns>byte array</returns>
        //[field: NonSerialized]
        public delegate byte[] ValueToBytesDelegate(object value);
        /// <summary>
        /// Converts command value to byte array.
        /// </summary>
        [field: NonSerialized]
        public ValueToBytesDelegate ValueToBytesMethod;
        /// <summary>
        /// Method assigned to this event converts value to byte array when needed.
        /// If value is one of predefined type (VALUETYPE) or is byte array (byte[]) this event do not have to be overriden.
        /// </summary>
        //[field: NonSerialized]
        public event ValueToBytesDelegate OnValueToBytesEvent
        {
            add { ValueToBytesMethod = value; }
            remove { }
        }
        #endregion
        #region BytesToValue
        /// <summary>
        /// Converts byte array to value of specified type. The type is specified during memory map creation.
        /// </summary>
        /// <param name="bytes">byte array</param>
        /// <returns>object of specified type</returns>
        //[field: NonSerialized]
        public delegate object BytesToValueDelegate(byte[] bytes);
        /// <summary>
        /// Converts byte array to object.
        /// </summary>
        [NonSerialized]
        public BytesToValueDelegate BytesToValueMethod;
        /// <summary>
        /// Method assigned to this event converts byte array to value of specified type when needed.
        /// If value is one of predefined type (VALUETYPE) or is byte array (byte[]) this event do not have to be overriden.
        /// </summary>
        //[field: NonSerialized]
        public event BytesToValueDelegate OnBytesToValueEvent
        {
            add { BytesToValueMethod = value; }
            remove { }
        }
        #endregion
        #region enum VALUETYPE
        /// <summary>
        /// Value of object can be one of predefined types. If not OnValueToBytesEvent and OnBytesToValueEvent must be set!
        /// </summary>
        public enum VALUETYPE
        {
            /// <summary>
            /// 16 bit signed integer (Int16, short).
            /// </summary>
            INT16,
            /// <summary>
            /// 32 bit signed integer (Int32, int).
            /// </summary>
            INT32,
            /// <summary>
            /// 64 bit signed integer (Int64, long).
            /// </summary>
            INT64,
            /// <summary>
            /// 16 bit unsigned integer (UInt16, ushort).
            /// </summary>
            UINT16,
            /// <summary>
            /// 32 bit unsigned integer (UInt32, uint).
            /// </summary>
            UINT32,
            /// <summary>
            /// 64 bit unsigned integer (UInt64, ulong).
            /// </summary>
            UINT64,
            /// <summary>
            /// 16 bit signed integer (Int16, short).
            /// </summary>
            SHORT,
            /// <summary>
            /// 16 bit unsigned integer (UInt16, ushort).
            /// </summary>
            USHORT,
            /// <summary>
            /// 64 bit signed integer (Int64, long).
            /// </summary>
            LONG,
            /// <summary>
            /// 64 bit unsigned integer (Int64, ulong).
            /// </summary>
            ULONG,
            /// <summary>
            /// 8 bit byte (Byte, byte).
            /// </summary>
            BYTE,
            /// <summary>
            /// 8 bit signed byte (SByte, sbyte).
            /// </summary>
            SBYTE,
            /// <summary>
            /// 16 bit unicode char (Char, char).
            /// </summary>
            CHAR_UNICODE,
            /// <summary>
            /// 32 bit single precision float (Single, float).
            /// </summary>
            FLOAT,
            /// <summary>
            /// 64 bit double precision float (Double, double).
            /// </summary>
            DOUBLE,
            /// <summary>
            /// 128 bit decimal number.
            /// </summary>
            DECIMAL,
            /// <summary>
            /// 8 bit to Boolean.
            /// </summary>
            BOOL,
            /// <summary>
            /// Seconds since 1970 converted to DateTime
            /// </summary>
            DATETIME,
            /// <summary>
            /// SN.FromDBValue (long &lt;-&gt; 4B)
            /// </summary>
            SN,
            /// <summary>
            /// IP Address like: &quot;192.168.41.21&quot; (string &lt;-&gt; 4B) 
            /// </summary>
            IP,
            /// <summary>
            /// 24 bits long signed int
            /// </summary>
            INT24,
            /// <summary>
            /// 24 bits long unsigned int
            /// </summary>
            UINT24
        }
        #endregion
        #endregion

        #region SetValue (After Get and Before Put)
        /// <summary>
        /// Prepares Value before Put or analyzes it after Get Action or Event.
        /// </summary>
        /// <param name="command">Method is executed for this command.</param>
        /// <param name="listeners">All commands that pointed at this object to be sent instead of them.</param>
        //[field: NonSerialized]
        [Obsolete("Use SetValueDelegate2 instead of, when static MemoryMap is used")]
        public delegate void SetValueDelegate(SN SerialNbr, DeviceCommand command, DeviceCommand[] listeners);
        //[field: NonSerialized]
        public delegate void SetValueDelegate2(MemoryMapForDevice map, SN SerialNbr, DeviceCommand command, DeviceCommand[] listeners);

        #region Before Put
        /// <summary>
        /// Prepares value before Put Action.
        /// </summary>
        [NonSerialized]
        [Obsolete("Use SetValueBeforePut2Method instead of, when static MemoryMap is used")]
        public SetValueDelegate SetValueBeforePutMethod;
        [field: NonSerialized]
        public SetValueDelegate2 SetValueBeforePut2Method;
        /// <summary>
        /// Method set to this event is fired when preparing command to be sent to device.
        /// </summary>
        //[field: NonSerialized]
        [Obsolete("Use OnSetValueBeforePut2Event instead of, when static MemoryMap is used")]
        public event SetValueDelegate OnSetValueBeforePutEvent
        {
            add { SetValueBeforePutMethod += value; }
            remove { SetValueBeforePutMethod -= value; }
        }
        //[field: NonSerialized]
        public event SetValueDelegate2 OnSetValueBeforePut2Event
        {
            add { SetValueBeforePut2Method += value; }
            remove { SetValueBeforePut2Method -= value; }
        }
        //[field: NonSerialized]
        [Obsolete("Use UpdateValueBeforePut2 instead of, when static MemoryMap is used")]
        public void UpdateValueBeforePut(SN SerialNbr, DeviceCommand[] commandListeners)
        {
            if (SetValueBeforePutMethod != null)
                SetValueBeforePutMethod(SerialNbr, this, commandListeners);
        }
        //[field: NonSerialized]
        public void UpdateValueBeforePut2(MemoryMapForDevice map, SN SerialNbr, DeviceCommand[] commandListeners)
        {
            if (SetValueBeforePut2Method != null)
                SetValueBeforePut2Method(map, SerialNbr, this, commandListeners);
        }
        #endregion

        #region After Get
        /// <summary>
        /// Value analyze after Get Action or Event. 
        /// </summary>
        [NonSerialized]
        [Obsolete("Use SetValueAfterGet2Method instead of, when static MemoryMap is used")]
        public SetValueDelegate SetValueAfterGetMethod;
        [field: NonSerialized]
        public SetValueDelegate2 SetValueAfterGet2Method;
        /// <summary>
        /// Method set to this event is fired when command is received from device.
        /// </summary>
        //[field: NonSerialized]
        [Obsolete("Use OnSetValueAfterGet2Event instead of, when static MemoryMap is used")]
        public event SetValueDelegate OnSetValueAfterGetEvent
        {
            add { SetValueAfterGetMethod = value; }
            remove { }
        }
        //[field: NonSerialized]
        public event SetValueDelegate2 OnSetValueAfterGet2Event
        {
            add { SetValueAfterGet2Method = value; }
            remove { }
        }

        //[field: NonSerialized]
        [Obsolete("Use UpdateValueAfterGet2 instead of, when static MemoryMap is used")]
        public void UpdateValueAfterGet(SN SerialNbr, DeviceCommand[] commandListeners)
        {
            if (SetValueAfterGetMethod != null)
                SetValueAfterGetMethod(SerialNbr, this, commandListeners);
        }
        //[field: NonSerialized]
        public void UpdateValueAfterGet2(MemoryMapForDevice map, SN SerialNbr, DeviceCommand[] commandListeners)
        {
            if (SetValueAfterGet2Method != null)
                SetValueAfterGet2Method(map, SerialNbr, this, commandListeners);
        }
        #endregion
        #endregion

        #region DataMeasureValueCreate
        #region DataMeasureValueCreateDelegate
        /// <summary>
        /// Creates DataValues to be sent to CORE after Action or Event.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        //[field: NonSerialized]
        [Obsolete("Use DataMeasureValueCreate2Delegate instead of, when static MemoryMap is used")]
        public delegate DataMeasureValue[] DataMeasureValueCreateDelegate(DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType);
        //[field: NonSerialized]
        public delegate DataMeasureValue[] DataMeasureValueCreate2Delegate(MemoryMapForDevice map, DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType);
        #endregion
        #region SimpleCreateDataMeasureValue
        [Obsolete("Use SimpleCreateDataMeasureValue2 instead of, when static MemoryMap is used")]
        public static DataMeasureValue[] SimpleCreateDataMeasureValue(DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType)
        {
            Enums.MeasureValueStatus status = CommandStatusToMeasureValueStatus(Command.Status);
            bool isAction;
            bool isAlarm;
            Command.GetFlags(ProcessType, out isAction, out isAlarm);
            return new DataMeasureValue[] { new DataMeasureValue(Command.IdDataType.Value, Command.Index ?? 0, Command.Value, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket } };
        }
        public static DataMeasureValue[] SimpleCreateDataMeasureValue2(MemoryMapForDevice map, DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType)
        {
            Enums.MeasureValueStatus status = CommandStatusToMeasureValueStatus(map.GetStatus(Command));
            bool isAction;
            bool isAlarm;
            Command.GetFlags(ProcessType, out isAction, out isAlarm);
            //return new DataMeasureValue[] { new DataMeasureValue(Command.IdDataType.Value, Command.Index ?? 0, Command.Value, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket } };
            return new DataMeasureValue[] { new DataMeasureValue(Command.IdDataType.Value, Command.Index ?? 0, map.GetValue(Command), Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket } };
        }
        #endregion

        #region Get
        /// <summary>
        /// Creates DataValues to be sent to CORE after Get Action or Event.
        /// </summary>
        [NonSerialized]
        [Obsolete("Use DataGetMeasureValueCreate2Method instead of, when static MemoryMap is used")]
        public DataMeasureValueCreateDelegate DataGetMeasureValueCreateMethod;
        /// <summary>
        /// Method set to this event is fired when preparing resonse to CORE after Get Request or when Even occurs, it construct DataMeasureValues which will be sent to CORE.
        /// When set to null, no response for this command will be sent to CORE.
        /// </summary>
        //[field: NonSerialized]
        [Obsolete("Use OnDataGetMeasureValueCreate2Event instead of, when static MemoryMap is used")]
        public event DataMeasureValueCreateDelegate OnDataGetMeasureValueCreateEvent
        {
            add { DataGetMeasureValueCreateMethod = value; }
            remove { }
        }

        /// <summary>
        /// Creates DataValues to be sent to CORE after Get Action or Event.
        /// </summary>
        [NonSerialized]
        public DataMeasureValueCreate2Delegate DataGetMeasureValueCreate2Method;
        /// <summary>
        /// Method set to this event is fired when preparing resonse to CORE after Get Request or when Even occurs, it construct DataMeasureValues which will be sent to CORE.
        /// When set to null, no response for this command will be sent to CORE.
        /// </summary>
        //[field: NonSerialized]
        public event DataMeasureValueCreate2Delegate OnDataGetMeasureValueCreate2Event
        {
            add { DataGetMeasureValueCreate2Method = value; }
            remove { }
        }
        #endregion

        #region Put
        /// <summary>
        /// Creates DataValues to be sent to CORE after Put Action.
        /// </summary>
        [NonSerialized]
        [Obsolete("Use DataPutMeasureValueCreate2Method instead of, when static MemoryMap is used")]
        public DataMeasureValueCreateDelegate DataPutMeasureValueCreateMethod;
        /// <summary>
        /// Method set to this event is fired when preparing resonse to CORE after Put Request, it construct DataMeasureValues which will be sent to CORE.
        /// When set to null, no response for this command will be sent to CORE.
        /// </summary>
        //[field: NonSerialized]
        [Obsolete("Use OnDataGetMeasureValueCreate2Event instead of, when static MemoryMap is used")]
        public event DataMeasureValueCreateDelegate OnDataPutMeasureValueCreateEvent
        {
            add { DataPutMeasureValueCreateMethod = value; }
            remove { }
        }

        /// <summary>
        /// Creates DataValues to be sent to CORE after Put Action.
        /// </summary>
        [NonSerialized]
        public DataMeasureValueCreate2Delegate DataPutMeasureValueCreate2Method;
        /// <summary>
        /// Method set to this event is fired when preparing resonse to CORE after Put Request, it construct DataMeasureValues which will be sent to CORE.
        /// When set to null, no response for this command will be sent to CORE.
        /// </summary>
        //[field: NonSerialized]
        public event DataMeasureValueCreate2Delegate OnDataPutMeasureValueCreate2Event
        {
            add { DataPutMeasureValueCreate2Method = value; }
            remove { }
        }
        #endregion
        #endregion

        #region IdDataType
        /// <summary>
        /// Object IdDataType, it responds to object datatype in database. When set to null: object does not exist in database. 
        /// </summary>
        public long? IdDataType { get; private set; }
        #endregion

        #region GetRequestDeviceCommands
        /// <summary>
        /// Object that are send instead of this one on get request. When set to null, this object is send.
        /// </summary>
        public DeviceCommand[] GetRequestDeviceCommands { get; private set; }
        #endregion
        #region PutRequestDeviceCommands
        /// <summary>
        /// Object that are send instead of this one on put request. When set to null, this object is send.
        /// </summary>
        public DeviceCommand[] PutRequestDeviceCommands { get; private set; }
        #endregion

        #region Index
        public int? Index { get; set; }
        #endregion

        #region SLOTINFO
        [Serializable]
        public class SLOTINFO
        {
            public int idx { get; set; }
            public Enums.SlotType type { get; set; }
            public SLOTINFO() { }
            public SLOTINFO(int idx, Enums.SlotType type)
            {
                this.idx = idx;
                this.type = type;
            }

            #region Equals
            public static bool operator ==(SLOTINFO left, object right)
            {
                return ((object)left == null) ? ((object)right == null) : left.Equals(right);
            }
            public static bool operator !=(SLOTINFO left, object right)
            {
                return ((object)left == null) ? ((object)right != null) : !left.Equals(right);
            }
            public override bool Equals(object obj)
            {
                return (!(obj is SLOTINFO)) ? false : this.GetHashCode() == obj.GetHashCode();
            }
            public override int GetHashCode()
            {
                return string.Format("{0}{1}", idx, type).GetHashCode();
            }
            #endregion
            #region ToString
            public override string ToString()
            {
                return string.Format("[{0}, {1}]", idx, type);
            }
            #endregion
        }
        public SLOTINFO SlotInfo { get; set; }
        #endregion

        #region TableInfo
        [Serializable]
        public class TABLEInfo
        {
            #region enum TAB_ACC
            public enum TAB_ACC
            {
                TBL_ACC_IDX = 0,
                TBL_ACC_TIME = 1,
                TBL_ACC_IDX_BACK = 2,
                TBL_ACC_TIME_BACK = 3,
                TBL_ACC_IDX_REL = 4,
                TBL_ACC_IDX_BACK_REL = 5
            }
            #endregion

            #region AccessParameter
            private TAB_ACC accessParameter = TAB_ACC.TBL_ACC_IDX;
            public TAB_ACC AccessParameter
            {
                get { return accessParameter; }
                set
                {
                    accessParameter = value;
                }
            }
            #endregion

            #region RowBegin
            private uint rowBegin = 0;
            public uint RowBegin
            {
                get { return rowBegin; }
                set { rowBegin = value; }
            }
            #endregion

            #region RowEnd
            private ushort rowEnd = 0;
            public ushort RowEnd
            {
                get { return rowEnd; }
                set { rowEnd = value; }
            }
            #endregion

            #region ColBegin
            private byte colBegin = 0;
            public byte ColBegin
            {
                get { return colBegin; }
                set { colBegin = value; }
            }
            #endregion

            #region ColEnd
            private byte colEnd = 0;
            public byte ColEnd
            {
                get { return colEnd; }
                set { colEnd = value; }
            }
            #endregion

            #region Constructor
            public TABLEInfo()
            {
            }

            public TABLEInfo(TAB_ACC AccessParameter, uint RowBegin, ushort RowEnd, byte ColBegin, byte ColEnd)
            {
                this.accessParameter = AccessParameter;
                this.rowBegin = RowBegin;
                this.rowEnd = RowEnd;
                this.colBegin = ColBegin;
                this.colEnd = ColEnd;
            }
            #endregion
        }
        public TABLEInfo TableInfo { get; set; }
        #endregion

        #region Status
        [Obsolete("This property is obsolete due to the use of static MemoryMap")]
        public virtual Enums.CommandStatus Status { get; set; }
        public Enums.CommandStatus GetStatus(CacheDictionary<DeviceCommand, Enums.CommandStatus> DeviceCommandStatus)
        {
            Enums.CommandStatus status = Enums.CommandStatus.None;
            bool found = DeviceCommandStatus.TryGetValue(this, out status);
            return status;
        }

        public virtual void SetStatus(CacheDictionary<DeviceCommand, Enums.CommandStatus> DeviceCommandStatus, Enums.CommandStatus value)
        {
            DeviceCommandStatus[this] = value;
        }

        public bool ClearStatus(CacheDictionary<DeviceCommand, Enums.CommandStatus> DeviceCommandStatus)
        {
            return DeviceCommandStatus.Remove(this);
        }

        #region CommandStatusToMeasureValueStatus
        public static Enums.MeasureValueStatus CommandStatusToMeasureValueStatus(Enums.CommandStatus status)
        {
            switch (status)
            {
                case Enums.CommandStatus.PutOK:
                case Enums.CommandStatus.GetOK:
                    return Enums.MeasureValueStatus.Ok;
                case Enums.CommandStatus.NotFound:
                    return Enums.MeasureValueStatus.NotFound;
                case Enums.CommandStatus.AccessDenied:
                    return Enums.MeasureValueStatus.AccessDenied;
                case Enums.CommandStatus.ReadOnly:
                    return Enums.MeasureValueStatus.ReadOnly;
                case Enums.CommandStatus.WrongSize:
                    return Enums.MeasureValueStatus.WrongSize;
                case Enums.CommandStatus.ParameterIncorrect:
                    return Enums.MeasureValueStatus.ParameterIncorrect;
                case Enums.CommandStatus.NotTable:
                    return Enums.MeasureValueStatus.NotTable;
                case Enums.CommandStatus.NotSupported:
                    return Enums.MeasureValueStatus.NotSupported;
                case Enums.CommandStatus.BusBusy:
                    return Enums.MeasureValueStatus.BusBusy;
                case Enums.CommandStatus.EEpromFail:
                    return Enums.MeasureValueStatus.EEpromFail;
                case Enums.CommandStatus.TableOutOfRange:
                    return Enums.MeasureValueStatus.TableOutOfRange;
                case Enums.CommandStatus.OrderNotCorrect:
                    return Enums.MeasureValueStatus.OrderNotCorrect;
                case Enums.CommandStatus.BadState:
                    return Enums.MeasureValueStatus.BadState;
                case Enums.CommandStatus.WriteError:
                    return Enums.MeasureValueStatus.WriteError;
                case Enums.CommandStatus.TableAccessError:
                    return Enums.MeasureValueStatus.TableAccessError;
                case Enums.CommandStatus.ReadError:
                    return Enums.MeasureValueStatus.ReadError;
                case Enums.CommandStatus.FtpError:
                    return Enums.MeasureValueStatus.FtpError;
                case Enums.CommandStatus.UnknownError:
                    return Enums.MeasureValueStatus.UnknownError;
                case Enums.CommandStatus.New:
                    return Enums.MeasureValueStatus.New;
                default:
                    return Enums.MeasureValueStatus.Error;
            }
        }
        #endregion
        #endregion

        #region VALUEFLAG
        #region MASK
        public enum VALUEFLAG : int
        {
            IS_ACTION_PUT = 0x20,
            IS_ACTION_GET = 0x10,
            IS_ACTION_EVENT = 0x08,
            IS_ALARM_PUT = 0x04,
            IS_ALARM_GET = 0x02,
            IS_ALARM_EVENT = 0x01
        }                                  //      IsAction     ||     IsAlarm
        // PUT | GET | EVENT || PUT | GET | EVENT
        //protected byte VALUEMASK = 0x1B; //  0  |  1  |   1   ||  0  |  1  |   1
        private int mask = (byte)(VALUEFLAG.IS_ACTION_GET | VALUEFLAG.IS_ACTION_EVENT | VALUEFLAG.IS_ALARM_GET | VALUEFLAG.IS_ALARM_EVENT);
        public byte VALUEMASK { get { return (byte)mask; } }
        #endregion

        #region SetFlag
        public void SetFlag(VALUEFLAG flag, bool value)
        {
            if (value)
                mask |= (int)flag;
            else
                mask &= ~(int)flag;
        }
        #endregion
        #region GetFlags
        public void GetFlags(Enums.ProcessType processType, out bool isAction, out bool isAlarm)
        {
            switch (processType)
            {
                case Enums.ProcessType.Event:
                    isAction = (mask & (int)VALUEFLAG.IS_ACTION_EVENT) == (int)VALUEFLAG.IS_ACTION_EVENT;
                    isAlarm = (mask & (int)VALUEFLAG.IS_ALARM_EVENT) == (int)VALUEFLAG.IS_ALARM_EVENT;
                    break;
                case Enums.ProcessType.Get:
                    isAction = (mask & (int)VALUEFLAG.IS_ACTION_GET) == (int)VALUEFLAG.IS_ACTION_GET;
                    isAlarm = (mask & (int)VALUEFLAG.IS_ALARM_GET) == (int)VALUEFLAG.IS_ALARM_GET;
                    break;
                case Enums.ProcessType.Put:
                    isAction = (mask & (int)VALUEFLAG.IS_ACTION_PUT) == (int)VALUEFLAG.IS_ACTION_PUT;
                    isAlarm = (mask & (int)VALUEFLAG.IS_ALARM_PUT) == (int)VALUEFLAG.IS_ALARM_PUT;
                    break;
                default:
                    isAction = false;
                    isAlarm = false;
                    break;
            }
        }
        #endregion
        #endregion

        #region Name
        public string Name { get; set; }
        #endregion

        #region GetDataMeasureValues
        [Obsolete("Use GetDataMeasureValues2 instead of, when static MemoryMap is used")]
        public List<DataMeasureValue> GetDataMeasureValues(Enums.ProcessType type, SN serial, DateTime timestamp, ulong idPacket)
        {
            List<DataMeasureValue> result = new List<DataMeasureValue>();
            Queue<DeviceCommand> stack = new Queue<DeviceCommand>();
            stack.Enqueue(this);
            while (stack.Count > 0)
            {
                DeviceCommand current = stack.Dequeue();
                current.Status = Status;
                DataMeasureValueCreateDelegate method = type == Enums.ProcessType.Put ? current.DataPutMeasureValueCreateMethod : current.DataGetMeasureValueCreateMethod;
                if (current.IdDataType.HasValue && method != null)
                    result.AddRange(method(current, timestamp, serial, idPacket, type));
                foreach (DeviceCommand item in current.Items)
                {
                    stack.Enqueue(item);
                }
            }
            return result;
        }
        public List<DataMeasureValue> GetDataMeasureValues2(MemoryMapForDevice map, Enums.ProcessType type, SN serial, DateTime timestamp, ulong idPacket)
        {
            List<DataMeasureValue> result = new List<DataMeasureValue>();
            Queue<DeviceCommand> stack = new Queue<DeviceCommand>();
            stack.Enqueue(this);
            while (stack.Count > 0)
            {
                DeviceCommand current = stack.Dequeue();
                map.SetStatus(current, map.GetStatus(this));
                DataMeasureValueCreate2Delegate method = type == Enums.ProcessType.Put ? current.DataPutMeasureValueCreate2Method : current.DataGetMeasureValueCreate2Method;
                if (current.IdDataType.HasValue && method != null)
                    result.AddRange(method(map, current, timestamp, serial, idPacket, type));
                foreach (DeviceCommand item in current.Items)
                {
                    stack.Enqueue(item);
                }
            }
            return result;
        }
        #endregion

        #region IsRead
        public bool IsRead { get; set; }
        #endregion
        #region IsWrite
        public bool IsWrite { get; set; }
        #endregion
        #region IsMultiple
        public bool IsMultiple { get; set; }
        #endregion
        #region IsArchive
        public bool IsArchive { get; set; }
        #endregion
        #region IsGUIVisible
        public bool IsGUIVisible { get; set; }
        #endregion
        #region Description
        public string Description { get; set; }
        #endregion
        #region Unit
        public string Unit { get; set; }
        #endregion

        #region IsValueValid
        public class InvalidValueInfo
        {
            public bool IsInvalid { get; set; }
            public string Message { get; set; }
        }
        //[field: NonSerialized]
        public delegate void OnValueValidationDelegate(object value, ref InvalidValueInfo info);
        [field: NonSerialized]
        public event OnValueValidationDelegate OnValueValidation;
        #endregion
        #region DefaultValue
        private object defaultValue;
        public object DefaultValue { get { return defaultValue; } set { defaultValue = value; Value = value; } }
        #endregion

        #region DeviceOrderNumberList
        List<string> DeviceOrderNumberList = new List<string>();
        #endregion

        #region DeviceCommandType
        /// <summary>
        /// CommandType
        /// </summary>
        public COMMAND_TYPE DeviceCommandType { get; set; }
        #region enum COMMAND_TYPE
        /// <summary>
        /// Command can be one of predefined types.
        /// </summary>
        public enum COMMAND_TYPE
        {
            /// <summary>
            /// Single type f.ex. Serial_Nbr
            /// </summary>
            SINGLE,
            /// <summary>
            /// Single unit dependent f.ex energy, volume 
            /// </summary>
            SINGLE_UNIT_DEPENDENT,
            /// <summary>
            /// Flexible size f.ex. CID
            /// </summary>
            FLEXIBLE,
            /// <summary>
            /// Structure f.ex FREQ
            /// </summary>
            STRUCTURE,
            /// <summary>
            /// Schedule 
            /// </summary>
            SCHEDULE,
            /// <summary>
            /// Table all kinds of ask
            /// </summary>
            TABLE,
            /// <summary>
            /// Table simple (with datatypes not ask kind selection)
            /// </summary>
            TABLE_SIMPLE,
            /// <summary>
            /// WPP_ACCESS_PERMISSIONS [WAN3]
            /// </summary>
            ACCESS_PERMISSIONS_TABLE,
            /// <summary>
            /// WPP_ACCESS_PASS_TABLE [WAN3]
            /// </summary>
            ACCESS_PASSWORD_TABLE,
            /// <summary>
            /// Virtual command f.ex to ask for table, not present in the device
            /// </summary>
            VIRTUAL,
            /// <summary>
            /// Can have size 0 or default  (depending on whether the module is active)
            /// </summary>
            DIAGNOSTICS,
            /// <summary>
            /// Group objects, not present in the device
            /// </summary>
            GROUPING
  

        }
        #endregion
        #endregion

        #region ToString()
        /// <summary>
        /// Prints object in human-readable format.
        /// </summary>
        /// <returns>nice command description</returns>
        public override string ToString()
        {
            int numOfTabs = 0;
            DeviceCommand parent = Parent;
            string header = string.Format("[{0}]", Index);
            while (parent != null)
            {
                numOfTabs++;
                parent = parent.Parent;
            }
            string result = string.Empty;
            for (int i = 0; i < numOfTabs; i++)
            {
                result += "   ";
            }
            result += String.Format("{0}{1}{2}{3} GlobalOffset: {4} ParentOffset: {5} Value: 0x",
                                    header,
                                    GetType().Name,
                                    this is DeviceBitCommand ? String.Format("({0})", ((DeviceBitCommand)this).BitNumber) : "",
                                    IdDataType != null ? String.Format(" IdDataType: {0}", IdDataType.Value) : SlotInfo != null ? String.Format(" {0}[{1}]", SlotInfo.type, SlotInfo.idx) : "",
                                    GlobalOffset,
                                    ParentOffset);
            //SlotType != Enums.SlotType.None ? String.Format(" SlotType: {0}", SlotType) : "");
            byte[] bytes = Bytes;
            foreach (byte b in bytes)
            {
                result += Convert.ToString(b, 16).PadLeft(2, '0').ToUpper();
            }
            result += "\n";
            foreach (DeviceCommand comm in Items)
            {
                result += comm.ToString();
            }
            return result;
        }
        #endregion

        #region Guid
        //public Guid Guid = Guid.NewGuid();

        //public static bool operator ==(DeviceCommand left, object right)
        //{
        //    if ((object)left == null)
        //        return ((object)right == null);
        //    return left.Equals(right);
        //}

        //public static bool operator !=(DeviceCommand left, object right)
        //{
        //    if ((object)left == null)
        //        return ((object)right != null);
        //    return !left.Equals(right);
        //}

        //public override bool Equals(object obj)
        //{
        //    if (!(obj is DeviceCommand)) return false;
        //    return this.GetHashCode() == obj.GetHashCode();
        //}

        //public override int GetHashCode()
        //{
        //    return Guid.GetHashCode();
        //}
        #endregion

        #region Clone()
        [Obsolete("Use Clone2 instead of, when static MemoryMap is used")]
        /// <summary>
        /// Creates a copy of object with all tree structure.
        /// </summary>
        /// <returns>Copy - clone</returns>
        public DeviceCommand Clone()
        {
            try
            {
                DeviceCommand result = null;

                if (this is DeviceBitCommand)
                    result = new DeviceBitCommand(IdDataType, ParentOffset * 8 + ((DeviceBitCommand)this).BitNumber, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceBytesCommand)
                    result = new DeviceBytesCommand(IdDataType, ParentOffset, ((DeviceBytesCommand)this).Count, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceObjectCommand)
                    result = new DeviceObjectCommand(IdDataType, ParentOffset, Size / 8, Index, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceTableCommand)
                    result = new DeviceTableCommand(IdDataType, ParentOffset, Size / 8, TableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else
                    result = new DeviceCommand(IdDataType, ParentOffset, Size, Index, TableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands);

                //                result.Guid = Guid;
                result.GlobalOffset = this.GlobalOffset;
                result.ParentOffset = this.ParentOffset;
                result.Parent = this.Parent;
                result.ValueToBytesMethod = this.ValueToBytesMethod;
                result.BytesToValueMethod = this.BytesToValueMethod;
                result.SetValueBeforePutMethod = this.SetValueBeforePutMethod;
                result.SetValueAfterGetMethod = this.SetValueAfterGetMethod;
                result.DataGetMeasureValueCreateMethod = this.DataGetMeasureValueCreateMethod;
                result.DataPutMeasureValueCreateMethod = this.DataPutMeasureValueCreateMethod;
                result.Status = this.Status;
                result.Name = this.Name;
                result.Index = this.Index;
                if (this.SlotInfo != null)
                    result.SlotInfo = new SLOTINFO(this.SlotInfo.idx, this.SlotInfo.type);
                result.Status = this.Status;
                result.mask = this.mask;
                result.IsRead = this.IsRead;
                result.IsWrite = this.IsWrite;
                if (this.OnValueValidation != null)
                    result.OnValueValidation += this.OnValueValidation;
                result.defaultValue = this.defaultValue;
                result.Value = this.Value;

                if (this.Items.Count > 0)
                {
                    List<DeviceCommand> childItems = new List<DeviceCommand>();
                    foreach (DeviceCommand item in this.Items)
                    {
                        childItems.Add(item.Clone());

                    }
                    foreach (DeviceCommand item in childItems)
                        result.Items.AddAt(item.Index ?? 0, item);
                }

                result.bytes = this.bytes;
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Creates a copy of object with all tree structure.
        /// </summary>
        /// <param name="map">Memory Map for device</param>
        /// <param name="guid">Process data guid</param>
        /// <returns>Copy - clone</returns>
        public DeviceCommand Clone2(MemoryMapForDevice map, Guid? guid)
        {
            try
            {
                DeviceCommand result = null;

                if (this is DeviceBitCommand)
                    result = new DeviceBitCommand(IdDataType, ParentOffset * 8 + ((DeviceBitCommand)this).BitNumber, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceBytesCommand)
                    result = new DeviceBytesCommand(IdDataType, ParentOffset, ((DeviceBytesCommand)this).Count, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceObjectCommand)
                    result = new DeviceObjectCommand(IdDataType, ParentOffset, Size / 8, Index, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else if (this is DeviceTableCommand)
                    result = new DeviceTableCommand(IdDataType, ParentOffset, Size / 8, TableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands);
                else
                    result = new DeviceCommand(IdDataType, ParentOffset, Size, Index, TableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands);


                map.AddDeviceCommandToClean(result, guid);
                //                result.Guid = Guid;
                result.GlobalOffset = this.GlobalOffset;
                result.ParentOffset = this.ParentOffset;
                result.Parent = this.Parent;
                result.ValueToBytesMethod = this.ValueToBytesMethod;
                result.BytesToValueMethod = this.BytesToValueMethod;
                //result.SetValueBeforePutMethod = this.SetValueBeforePutMethod;
                result.SetValueBeforePut2Method = this.SetValueBeforePut2Method; //zamiast
                //result.SetValueAfterGetMethod = this.SetValueAfterGetMethod;
                result.SetValueAfterGet2Method = this.SetValueAfterGet2Method; //zamiast
                //result.DataGetMeasureValueCreateMethod = this.DataGetMeasureValueCreateMethod;
                result.DataGetMeasureValueCreate2Method = this.DataGetMeasureValueCreate2Method; //zamiast
                //result.DataPutMeasureValueCreateMethod = this.DataPutMeasureValueCreateMethod;
                result.DataPutMeasureValueCreate2Method = this.DataPutMeasureValueCreate2Method; //zamiast
                //result.Status = this.Status;
                map.SetStatus(result, map.GetStatus(this)); //zamiast
                result.Name = this.Name;
                result.Index = this.Index;
                if (this.SlotInfo != null)
                    result.SlotInfo = new SLOTINFO(this.SlotInfo.idx, this.SlotInfo.type);
                //result.Status = this.Status;
                map.SetStatus(result, map.GetStatus(this)); //zamiast
                result.mask = this.mask;
                result.IsRead = this.IsRead;
                result.IsWrite = this.IsWrite;
                if (this.OnValueValidation != null)
                    result.OnValueValidation += this.OnValueValidation;
                result.defaultValue = this.defaultValue;
                //result.Value = this.Value;
                map.SetValue(result, map.GetValue(this));

                if (this.Items.Count > 0)
                {
                    List<DeviceCommand> childItems = new List<DeviceCommand>();
                    foreach (DeviceCommand item in this.Items)
                    {
                        childItems.Add(item.Clone2(map, guid));
                    }
                    foreach (DeviceCommand item in childItems)
                        result.Items.AddAt(item.Index ?? 0, item);
                }

                //result.bytes = this.bytes;
                map.SetBytes(result, map.GetBytes(this)); //zamiast
                return result;
            }
            catch (Exception ex)
            {
                Logger.Add(new LogData(0, LogLevel.Error, String.Format("Clone2 returned exception. Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace)));
                return null;
            }

        }

        [Obsolete("Use Clone2 with guid and idPacket parameter instead of, when static MemoryMap is used")]
        public DeviceCommand Clone2(MemoryMapForDevice map)
        {
            return Clone2(map, null);
        }
        #endregion
    }
    #endregion

    #region Class DeviceCommandList
    /// <summary>
    /// Stores DeviceCommands, extends List&lt;DeviceCommand&gt;
    /// </summary>
    [Serializable]
    public class DeviceCommandList : List<DeviceCommand>
    {
        DeviceCommand Parent;

        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="Parent">Parent item for all DeviceCommand in this list, usually this.</param>
        public DeviceCommandList(DeviceCommand Parent)
        {
            this.Parent = Parent;
        }
        #endregion

        #region Add
        /// <summary>
        /// Adds item to List and updates its GlobalOffset.
        /// </summary>
        /// <param name="item">DeviceCommand to add.</param>
        public new void Add(DeviceCommand item)
        {
            int idx = -1;
            foreach (DeviceCommand command in this)
            {
                if (command.ParentOffset > item.ParentOffset)
                {
                    idx = IndexOf(command);
                    base.Insert(idx, item);
                    break;
                }
            }
            if (idx < 0)
                base.Add(item);

            UpdateItem(item);
        }
        #endregion
        #region AddAt
        /// <summary>
        /// Adds item to List at specified slot.
        /// </summary>
        /// <param name="index">index number to add at.</param>
        /// <param name="item">DeviceCommand to add.</param>
        public void AddAt(int index, DeviceCommand item)
        {
            item.Index = index;
            Add(item);
        }
        #endregion
        #region //AddMany
        /// <summary>
        /// Adds copy of item at slots begining with slot 0.
        /// </summary>
        /// <param name="firstItem">First DeviceCommand, the rest will be its copy.</param>
        /// <param name="count">Number of DeviceCommands to add.</param>
        //public void AddMany(DeviceCommand firstItem, int count)
        //{
        //    for (int i = 0; i < count; i++)
        //    {
        //        DeviceCommand command = firstItem.Clone();
        //        command.Index = firstItem.Index;
        //        command.ParentOffset = firstItem.ParentOffset + i * (firstItem.Size / 8);
        //        Add(command);
        //    }
        //}
        #endregion
        #region //AddManyAt
        /// <summary>
        /// Adds copy of item at slots begining with specified slot number.
        /// </summary>
        /// <param name="startIndexNbr">Index number to start from.</param>
        /// <param name="firstItem">First DeviceCommand, the rest will be its copy.</param>
        /// <param name="count">Number of DeviceCommands to add.</param>
        //public void AddManyAt(int startIndex, DeviceCommand firstItem, int count)
        //{
        //    for (int i = startIndex; i < count + startIndex; i++)
        //    {
        //        DeviceCommand command = firstItem.Clone();
        //        command.Index = firstItem.Index;
        //        command.ParentOffset = firstItem.ParentOffset + (i - startIndex) * (firstItem.Size / 8);
        //        Add(command);
        //    }
        //}
        #endregion

        #region GetDeviceCommand
        /// <summary>
        /// Gets DeviceCommand for specified IdDataType.
        /// Only one commmand with this IdDataType is allowed to exist, it could not be a slot object or any part of such object.
        /// Raczej nieużywana to metoda będzie bo w MemoryMap jest robiony słownik.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <returns>DeviceCommand with specified IdDataType or null if not found.</returns>
        public DeviceCommand GetDeviceCommand(long IdDataType)
        {
            foreach (DeviceCommand command in this)
            {
                Stack<DeviceCommand> stack = new Stack<DeviceCommand>();
                stack.Push(command);
                while (stack.Count > 0)
                {
                    DeviceCommand current = stack.Pop();
                    if (current.IdDataType.HasValue && current.IdDataType.Value == IdDataType)
                    {
                        return current;
                    }
                    foreach (DeviceCommand item in current.Items)
                    {
                        stack.Push(item);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets 'slot type' DeviceCommand for specified IdDataType, SlotType and Slot Number.
        /// </summary>
        /// <param name="IdDataType">Wanted IdDataType.</param>
        /// <param name="Index">Index number, it is zero-based index.</param>
        /// <returns>DeviceCommand with specified IdDataType, SlotType and Slot Number or null if not found.</returns>
        public DeviceCommand GetDeviceCommand(long IdDataType, int Index)
        {
            DeviceCommand[] result = GetDeviceCommands(IdDataType);
            foreach (DeviceCommand item in result)
            {
                if (item.Index == Index)
                    return item;
            }
            return null;
        }

        public DeviceCommand[] GetDeviceCommands(long IdDataType)
        {
            List<DeviceCommand> result = new List<DeviceCommand>();
            foreach (DeviceCommand command in this)
            {
                Stack<DeviceCommand> stack = new Stack<DeviceCommand>();
                stack.Push(command);
                while (stack.Count > 0)
                {
                    DeviceCommand current = stack.Pop();
                    if (current.IdDataType.HasValue && current.IdDataType.Value == IdDataType)
                    {
                        result.Add(current);
                    }
                    foreach (DeviceCommand item in current.Items)
                    {
                        stack.Push(item);
                    }
                }
            }
            return result.ToArray();
        }
        #endregion

        #region UpdateItem
        private void UpdateItem(DeviceCommand item)
        {
            item.Parent = this.Parent;
            item.GlobalOffset = item.ParentOffset;
            if (this.Parent != null)
            {
                item.GlobalOffset += this.Parent.GlobalOffset;
                //if (this.Parent.Index.HasValue && !item.Index.HasValue)
                //    item.Index = this.Parent.Index;
            }

            foreach (DeviceCommand comm in item.Items)
            {
                item.Items.UpdateItem(comm);
            }
        }
        #endregion
    }
    #endregion

    #region Class DeviceObjectCommand
    /// <summary>
    /// Represents object in Device memory map.
    /// </summary>
    [Serializable]
    public class DeviceObjectCommand : DeviceCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        public DeviceObjectCommand(int ParentOffset, int ByteCount)
            : this(null, ParentOffset, ByteCount, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceObjectCommand(long? IdDataType, int ParentOffset, int ByteCount, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ByteCount, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceObjectCommand(long? IdDataType, int ParentOffset, int ByteCount, int? Index, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ByteCount * 8, Index, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        public DeviceObjectCommand(int ParentOffset, VALUETYPE ValueType)
            : this(null, ParentOffset, ValueType, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceObjectCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceObjectCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, int? Index, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ValueType, Index, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        #endregion
    }
    #endregion
    #region Class DeviceObjectCommandMultiple
    /// <summary>
    /// Represents object in Device memory map (multuple IdDataType&Index instance).
    /// </summary>
    [Serializable]
    public class DeviceObjectCommandMultiple : DeviceObjectCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        public DeviceObjectCommandMultiple(int ParentOffset, int ByteCount)
            : this(null, ParentOffset, ByteCount, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        public DeviceObjectCommandMultiple(long? IdDataType, int ParentOffset, int ByteCount, DeviceCommand[] GetRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ByteCount, null, GetRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        public DeviceObjectCommandMultiple(long? IdDataType, int ParentOffset, int ByteCount, int? Index, DeviceCommand[] GetRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ByteCount, Index, GetRequestDeviceCommands, null)
        {
            this.IsMultiple = true;
        }

        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        public DeviceObjectCommandMultiple(int ParentOffset, VALUETYPE ValueType)
            : this(null, ParentOffset, ValueType, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        public DeviceObjectCommandMultiple(long? IdDataType, int ParentOffset, VALUETYPE ValueType, DeviceCommand[] GetRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, null, GetRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        public DeviceObjectCommandMultiple(long? IdDataType, int ParentOffset, VALUETYPE ValueType, int? Index, DeviceCommand[] GetRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ValueType, Index, GetRequestDeviceCommands, null)
        {
            this.IsMultiple = true;
        }

        #endregion
    }
    #endregion

    #region Class DeviceBytesCommand
    /// <summary>
    /// Represents bytes in Device memory map, when sending request to this command it will be transformed into GetBytes.
    /// </summary>
    [Serializable]
    public class DeviceBytesCommand : DeviceCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        public DeviceBytesCommand(int ParentOffset, int ByteCount)
            : this(null, ParentOffset, ByteCount, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceBytesCommand(long? IdDataType, int ParentOffset, int ByteCount, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ByteCount * 8, GetRequestDeviceCommands, PutRequestDeviceCommands)
        {
            this.Count = ByteCount;
        }

        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        public DeviceBytesCommand(int ParentOffset, VALUETYPE ValueType)
            : this(null, ParentOffset, ValueType, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        /// <param name="SlotType">Slot type: Analog, Multiread or Trs, for None use shorter constructor.</param>
        public DeviceBytesCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ValueType, GetRequestDeviceCommands, PutRequestDeviceCommands)
        {
            this.Count = Size / 8;
        }
        #endregion

        /// <summary>
        /// Number of bytes. Useless.
        /// </summary>
        public int Count { get; private set; }
    }
    #endregion

    #region Class DeviceBitCommand
    /// <summary>
    /// Represents one bit in Device memory map.
    /// </summary>
    [Serializable]
    public class DeviceBitCommand : DeviceCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="BitNumber">Bit number in parent bytes, first bit is number 1. For first bit in next parent byte proper value is 9 (8+1).</param>
        public DeviceBitCommand(int BitNumber)
            : this(null, BitNumber, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="BitNumber">Bit number in parent bytes, first bit is number 1. For first bit in next parent byte proper value is 9 (8+1).</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceBitCommand(long? IdDataType, int BitNumber, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, (BitNumber - 1) / 8, 1, GetRequestDeviceCommands, PutRequestDeviceCommands)
        {
            this.BitNumber = BitNumber - ((BitNumber - 1) / 8) * 8;
            BytesToValueMethod = b => b[0] != 0x00;
            ValueToBytesMethod = v => new byte[] { Convert.ToBoolean(v) ? (byte)0x01 : (byte)0x00 };
        }
        #endregion

        /// <summary>
        /// Bit number (strats from 1 (not zero)).
        /// </summary>
        public int BitNumber { get; private set; }
    }
    #endregion
    #region Class DeviceBitCommandMultiple
    /// <summary>
    /// Represents one bit in Device memory map. (multuple IdDataType&Index instance).
    /// </summary>
    [Serializable]
    public class DeviceBitCommandMultiple : DeviceBitCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="BitNumber">Bit number in parent bytes, first bit is number 1. For first bit in next parent byte proper value is 9 (8+1).</param>
        public DeviceBitCommandMultiple(int BitNumber)
            : this(null, BitNumber, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="BitNumber">Bit number in parent bytes, first bit is number 1. For first bit in next parent byte proper value is 9 (8+1).</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        public DeviceBitCommandMultiple(long? IdDataType, int BitNumber, DeviceCommand[] GetRequestDeviceCommands)
            : base(IdDataType, BitNumber, GetRequestDeviceCommands, null)
        {
            this.IsMultiple = true;
        }
        #endregion

        /// <summary>
        /// Bit number (starts from 1 (not zero)).
        /// </summary>
        public int BitNumber { get; private set; }
    }
    #endregion

    #region Class DeviceRawDataCommand
    [Serializable]
    [Obsolete("Use DeviceRawDataCommand2 instead of, when static MemoryMap is used")]
    public class DeviceRawDataCommand : DeviceCommand
    {
        #region ID_SELECTOR
        private int ID_SELECTOR;
        #endregion
        #region RAW_DATA
        private byte[] RAW_DATA;
        public override byte[] Bytes
        {
            get
            {
                return RAW_DATA;
            }
            set
            {
                RAW_DATA = value;
            }
        }
        #endregion

        #region Constructor
        public DeviceRawDataCommand(int ID_SELECTOR)
            : this(ID_SELECTOR, new byte[] { 0x00 })
        { }

        [Obsolete("Use DeviceRawDataCommand2 instead of, when static MemoryMap is used")]
        public DeviceRawDataCommand(int ID_SELECTOR, byte[] RAW_DATA)
            : base(DataType.DEVICE_RAW_DATA, ID_SELECTOR, 1, null, null)
        {
            this.ID_SELECTOR = ID_SELECTOR;
            this.RAW_DATA = RAW_DATA;

            DataGetMeasureValueCreateMethod = (DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType) =>
            {
                bool isAction;
                bool isAlarm;
                Command.GetFlags(ProcessType, out isAction, out isAlarm);
                DeviceRawDataCommand rawCommand = (DeviceRawDataCommand)Command;
                Enums.MeasureValueStatus status = DeviceCommand.CommandStatusToMeasureValueStatus(Command.Status);
                DataMeasureValue id_selector = new DataMeasureValue(DataType.DEVICE_RAW_DATA_ID_SELECTOR, rawCommand.ID_SELECTOR, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket };
                string raw_data_value = "0x";
                foreach (byte b in rawCommand.RAW_DATA)
                {
                    raw_data_value += string.Format("{0:X2}", b);
                }
                DataMeasureValue raw_data = new DataMeasureValue(DataType.DEVICE_RAW_DATA, raw_data_value, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket };
                return status == Enums.MeasureValueStatus.Ok ? new DataMeasureValue[] { raw_data, id_selector } : new DataMeasureValue[] { id_selector };
            };
        }
        #endregion
    }
    #endregion
    #region Class DeviceRawDataCommand2
    [Serializable]
    public class DeviceRawDataCommand2 : DeviceCommand
    {
        #region ID_SELECTOR
        private int ID_SELECTOR;
        #endregion
        #region RAW_DATA
        private byte[] RAW_DATA;
        //public override byte[] Bytes
        //{
        //    get
        //    {
        //        return RAW_DATA;
        //    }
        //    set
        //    {
        //        RAW_DATA = value;
        //    }
        //}
        public override byte[] GetBytes(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes)
        {
            //return base.GetBytes(DeviceCommandBytes);
            return RAW_DATA;
        }
        public override void SetBytes(CacheDictionary<DeviceCommand, byte[]> DeviceCommandBytes, byte[] value)
        {
            //base.SetBytes(DeviceCommandBytes, value);
            RAW_DATA = value;
        }
        #endregion
        #region private Status bez Obsolete
        public new Enums.CommandStatus Status { get; set; }
        #endregion

        #region Constructor
        public DeviceRawDataCommand2(int ID_SELECTOR)
            : this(ID_SELECTOR, new byte[] { 0x00 })
        { }

        public DeviceRawDataCommand2(int ID_SELECTOR, byte[] RAW_DATA)
            : base(DataType.DEVICE_RAW_DATA, ID_SELECTOR, 1, null, null)
        {
            this.ID_SELECTOR = ID_SELECTOR;
            this.RAW_DATA = RAW_DATA;

            //DataGetMeasureValueCreateMethod = (DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType) =>
            DataGetMeasureValueCreate2Method = (MemoryMapForDevice map, DeviceCommand Command, DateTime Time, SN SerialNbr, ulong IdPacket, Enums.ProcessType ProcessType) =>
            {
                bool isAction;
                bool isAlarm;
                Command.GetFlags(ProcessType, out isAction, out isAlarm);
                DeviceRawDataCommand2 rawCommand = (DeviceRawDataCommand2)Command;
                Enums.MeasureValueStatus status = DeviceCommand.CommandStatusToMeasureValueStatus(rawCommand.Status);
                //Enums.MeasureValueStatus status = DeviceCommand.CommandStatusToMeasureValueStatus(map.GetStatus(Command)); //zamiast
                DataMeasureValue id_selector = new DataMeasureValue(DataType.DEVICE_RAW_DATA_ID_SELECTOR, rawCommand.ID_SELECTOR, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket };
                string raw_data_value = "0x";
                foreach (byte b in rawCommand.RAW_DATA)
                //foreach (byte b in map.GetBytes(rawCommand)) //zamiast
                {
                    raw_data_value += string.Format("{0:X2}", b);
                }
                DataMeasureValue raw_data = new DataMeasureValue(DataType.DEVICE_RAW_DATA, raw_data_value, Time, isAction, isAlarm, status) { SerialNbr = SerialNbr, IdPacket = IdPacket };
                return status == Enums.MeasureValueStatus.Ok ? new DataMeasureValue[] { raw_data, id_selector } : new DataMeasureValue[] { id_selector };
            };
        }
        #endregion
    }
    #endregion

    #region Class DeviceTableCommand
    /// <summary>
    /// Represents table item in Device memory map.
    /// </summary>
    [Serializable]
    public class DeviceTableCommand : DeviceCommand
    {
        #region Constructors
        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        public DeviceTableCommand(int ParentOffset, int ByteCount)
            : this(null, ParentOffset, ByteCount, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceTableCommand(long? IdDataType, int ParentOffset, int ByteCount, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ByteCount, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ByteCount">Size in bytes.</param>
        /// <param name="tableInfo">DataType info</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceTableCommand(long? IdDataType, int ParentOffset, int ByteCount, TABLEInfo tableInfo, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ByteCount * 8, null, tableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Short constructor.
        /// </summary>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        public DeviceTableCommand(int ParentOffset, VALUETYPE ValueType)
            : this(null, ParentOffset, ValueType, null, null)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceTableCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, null, null, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceTableCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, TABLEInfo tableInfo, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : this(IdDataType, ParentOffset, ValueType, null, tableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="IdDataType">Unique identifier, it responds to object datatype in database. When set to null: object does not exist in database.</param>
        /// <param name="ParentOffset">Offest in bytes in parent object.</param>
        /// <param name="ValueType">One of predefined value types. Object size in device must equal to value type.</param>
        /// <param name="Index">Index</param>
        /// <param name="GetRequestDeviceCommands">Object that are send instead of this one on get reques. When set to null, this object is send.</param>
        /// <param name="PutRequestDeviceCommands">Object that are send instead of this one on put reques. When set to null, this object is send.</param>
        public DeviceTableCommand(long? IdDataType, int ParentOffset, VALUETYPE ValueType, int? Index, TABLEInfo tableInfo, DeviceCommand[] GetRequestDeviceCommands, DeviceCommand[] PutRequestDeviceCommands)
            : base(IdDataType, ParentOffset, ValueType, Index, tableInfo, GetRequestDeviceCommands, PutRequestDeviceCommands)
        { }
        #endregion
    }
    #endregion

    #region Class ByteArray and BitConverter Extensions
    /// <summary>
    /// This class is so super it does not need description.
    /// </summary>
    public static class ByteArrayAndBitConverterExtension
    {
        #region BitConverter.ToDecimal()
        /// <summary>
        /// Converts bytes to decimal number.
        /// </summary>
        /// <param name="bytes">bytes with decimal number, it must be 16 bytes</param>
        /// <returns>decimal number</returns>
        public static decimal ToDecimal(byte[] bytes)
        {
            int[] bits = new int[4];
            bits[0] = ((bytes[0] | (bytes[1] << 8)) | (bytes[2] << 0x10)) | (bytes[3] << 0x18); //lo
            bits[1] = ((bytes[4] | (bytes[5] << 8)) | (bytes[6] << 0x10)) | (bytes[7] << 0x18); //mid
            bits[2] = ((bytes[8] | (bytes[9] << 8)) | (bytes[10] << 0x10)) | (bytes[11] << 0x18); //hi
            bits[3] = ((bytes[12] | (bytes[13] << 8)) | (bytes[14] << 0x10)) | (bytes[15] << 0x18); //flags

            return new decimal(bits);
        }
        #endregion

        #region BitConverter.GetBytes(decimal d)
        /// <summary>
        /// Gets bytes from decimal number.
        /// </summary>
        /// <param name="d">decimal number</param>
        /// <returns>16 bytes</returns>
        public static byte[] GetBytes(decimal d)
        {
            byte[] bytes = new byte[16];

            int[] bits = decimal.GetBits(d);
            int lo = bits[0];
            int mid = bits[1];
            int hi = bits[2];
            int flags = bits[3];

            bytes[0] = (byte)lo;
            bytes[1] = (byte)(lo >> 8);
            bytes[2] = (byte)(lo >> 0x10);
            bytes[3] = (byte)(lo >> 0x18);
            bytes[4] = (byte)mid;
            bytes[5] = (byte)(mid >> 8);
            bytes[6] = (byte)(mid >> 0x10);
            bytes[7] = (byte)(mid >> 0x18);
            bytes[8] = (byte)hi;
            bytes[9] = (byte)(hi >> 8);
            bytes[10] = (byte)(hi >> 0x10);
            bytes[11] = (byte)(hi >> 0x18);
            bytes[12] = (byte)flags;
            bytes[13] = (byte)(flags >> 8);
            bytes[14] = (byte)(flags >> 0x10);
            bytes[15] = (byte)(flags >> 0x18);

            return bytes;
        }
        #endregion
    }
    #endregion

    #region Class ASCII string with 0x00 or not
    [Serializable]
    public class ASCIIString
    {
        #region Constructor
        private int count = 1;
        public bool NullTerminated { get; set; }
        public ASCIIString(int count)
            : this(count, true)
        {
            this.count = count;
        }
        public ASCIIString(int count, bool null_terminated)
        {
            this.count = count;
            this.NullTerminated = null_terminated;
        }
        #endregion

        #region BytesToString
        public static object BytesToString(byte[] bytes)
        {
            string result = string.Empty;
            foreach (byte b in bytes)
            {
                if (b != 0x00)
                    result += new string((char)b, 1);
            }
            return result;
        }
        #endregion
        #region StringToBytes
        public byte[] StringToBytes(object value)
        {
            byte[] bytes = new byte[count];
            string tel = (string)value;
            int len = Math.Min(tel.Length, count - (NullTerminated ? 1 : 0));
            for (int i = 0; i < len; i++)
            {
                bytes[i] = (byte)tel[i];
            }
            return bytes;
        }
        #endregion

        #region BytesToStringNullSpace
        public static object BytesToStringNullSpace(byte[] bytes)
        {
            string result = string.Empty;
            foreach (byte b in bytes)
            {
                if (b != 0x00)
                    result += new string((char)b, 1);
                else
                    result += " ";
            }

            return result;
        }
        #endregion
        #region StringToBytesNullSpace
        public static byte[] StringToBytesNullSpace(object v, int len)
        {
            byte[] result = new byte[len];
            char[] value = v.ToString().ToCharArray();
            for (int i = 0; i < value.Length && i < len; i++)
                result[i] = (value[i] == ' ') ? (byte)0x00 : (byte)value[i];
            return result;
        }
        #endregion
        #region BytesToStringNullEnding
        public static object BytesToStringNullEnding(byte[] bytes)
        {
            string result = string.Empty;
            foreach (byte b in bytes)
            {
                if (b != 0x00)
                    result += new string((char)b, 1);
                else
                    break;
            }
            return result;
        }
        #endregion
        #region StringToBytesNullEnding
        public static byte[] StringToBytesNullEnding(object v, int len)
        {
            byte[] result = new byte[len];
            char[] value = v.ToString().ToCharArray();
            for (int i = 0; i < value.Length && i < len; i++)
                result[i] = (byte)value[i];
            return result;
        }
        #endregion
        #region BytesToStringNullEndingProperURL
        public static object BytesToStringNullEndingProperURL(byte[] bytes)
        {
            string result = string.Empty;
            foreach (byte b in bytes)
            {
                if (b != 0x00)
                    result += new string((char)b, 1);
                else
                    break;
            }
            //Regex regular= new Regex("^[ a-zA-Z0-9_.~-]*$");
            //if (!regular.IsMatch(result) && result != "")
            //{
            //    result = "";
            //    throw new Exception("Wrong URL string!");
            //}
            return result;
        }
        #endregion
        #region ValueToBytesProperURL
        public static byte[] ValueToBytesProperURL(object v, int len)
        {
            string result = (string)v;
            Regex regular = new Regex("^[ a-zA-Z0-9_.~-]*$");
            if (!regular.IsMatch(result) && result != "")
            {
                result = "";
                throw new Exception("Wrong URL string!");
            }
            return StringToBytesNullEnding(result, len);
        }
        #endregion
    }
    #endregion

    #region Class DateTimeExt
    public static class DateTimeExt
    {
        public static int SecondsSince1970(this DateTime value)
        {
            return (int)(new TimeSpan(value.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, value.Kind)).Ticks)).TotalSeconds;
        }
        public static DateTime DateTimeFromSecondsSince1970(this int value, DateTimeKind kind)
        {
            return (new DateTime(1970, 1, 1, 0, 0, 0, kind)).AddSeconds(value);
        }
    }
    #endregion
}
