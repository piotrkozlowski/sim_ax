#define ISERIALIZABLE_TEST

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using IMR.Suite.Common.Code;
#if !WindowsCE
using System.Runtime.Remoting;
#endif

using IMRAction = IMR.Suite.Common.Action;

namespace IMR.Suite.Common.Drivers.Device
{
    #region DeviceDriverEventArgs
    [Serializable]
    public class DeviceDriverEventArgs : EventArgs
    {
        public readonly DeviceDriverData[] DeviceDriverData;
        public readonly ProcessData ProcessData = null;
        public readonly DateTime Time;

        public DeviceDriverEventArgs()
        {
            this.DeviceDriverData = null;
            this.ProcessData = null;
            this.Time = DateTime.UtcNow;
        }
        public DeviceDriverEventArgs(DeviceDriverData[] deviceDriverData, DateTime time)
        {
            this.DeviceDriverData = deviceDriverData;
            if (deviceDriverData != null && deviceDriverData.Length > 0)
            {
                this.ProcessData = new ProcessData(deviceDriverData[0].Guid)
                {
                    IdProcess = deviceDriverData[0].IdProcess,
                    ProcessType = deviceDriverData[0].ProcessType,
                    ProcessStatus = deviceDriverData[0].ProcessStatus,
                    Values = deviceDriverData[0].Values,
                    DiagnosticInfos = (deviceDriverData[0].HasSlots && deviceDriverData[0].Slot[0].Packet != null) ? deviceDriverData[0].Slot[0].Packet.DiagnosticInfos : new List<DiagnosticInfo>()
                };
            }
            this.Time = time;
        }
        public DeviceDriverEventArgs(ProcessData processData)
        {
            this.DeviceDriverData = new DeviceDriverData[0];
            this.ProcessData = processData;
            this.Time = DateTime.UtcNow;
        }
    }
    #endregion
    #region SendPacketEventArgs
    [Serializable]
    public class SendPacketEventArgs : EventArgs
    {
        public readonly ProcessData ProcessData;
        public readonly Slot Slot;

        public readonly DateTime Time;

        public SendPacketEventArgs()
        {
            this.ProcessData = null;
            this.Slot = null;
            this.Time = DateTime.UtcNow;
        }
        public SendPacketEventArgs(ProcessData processData, Slot slot)
        {
            this.ProcessData = processData;
            this.Slot = slot;
            this.Time = DateTime.UtcNow;
        }
    }
    #endregion


    #region Slot
    [Serializable]
#if !WindowsCE
    [TypeConverter(typeof(SlotConverter))]
#endif
#if ISERIALIZABLE_TEST
    public class Slot : ISerializable
#else
    public class Slot
#endif
    {
        #region SerializationVersion
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion

        #region SlotType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private Enums.SlotType slotType = Enums.SlotType.None;
        public Enums.SlotType SlotType
        {
            get
            {
                return slotType;
            }
            set
            {
                slotType = value;
            }
        }
        #endregion
        #region SlotNbr
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        protected int slotNbr = 0;
        public int SlotNbr
        {
            get
            {
                return slotNbr;
            }
            set
            {
                slotNbr = value;
            }
        }
        #endregion

        #region Packet
        public Packet Packet { get; set; }
        #endregion
        #region Values
        public DataMeasureValue[] Values { get; set; }
        #endregion
        #region Params
        public DataValue[] Params { get; set; }
        #endregion

        #region Guid
#if !WindowsCE
        [Browsable(false)]
#endif
        public readonly Guid Guid = Guid.NewGuid();

        public static bool operator ==(Slot left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(Slot left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Slot)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region Ctor
        public Slot()
        {
            Params = new DataValue[0];
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected Slot(SerializationInfo info, StreamingContext context)
        {
            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            slotType = (Enums.SlotType)info.GetValue("slotType", typeof(int));
            slotNbr = (int)info.GetValue("slotNbr", typeof(int));
            Guid = (Guid)info.GetValue("Guid", typeof(Guid));

            if (serializationVersion == 0)
            {
                Packet = (Packet)info.GetValue("<Packet>k__BackingField", typeof(Packet));
                Values = (DataMeasureValue[])info.GetValue("<Values>k__BackingField", typeof(DataMeasureValue[]));
                //nie by�o tego przed serializacj�
                Params = new DataValue[0];
            }
            else if (serializationVersion == 1)
            {
                Packet = (Packet)info.GetValue("Packet", typeof(Packet));
                Values = (DataMeasureValue[])info.GetValue("Values", typeof(DataMeasureValue[]));
                Params = (DataValue[])info.GetValue("Params", typeof(DataValue[]));
            }
        }
#endif
        #endregion

        #region override ToString()
        public override string ToString()
        {
            return string.Format("Nbr[{0}] Type[{1}] Values[{2}] Guid[{3}] Packet({4})",
                SlotNbr, SlotType,
                Values == null ? "null" : Values.Length.ToString(),
                ToStringGUID(),
                Packet == null ? "null" : Packet.ToString());
        }

        public string ToStringGUID()
        {
            string strGuid = Guid.ToString().ToLower();
            return strGuid.Remove(0, strGuid.LastIndexOf("-") + 1);
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("slotType", (int)slotType, typeof(int));
            info.AddValue("slotNbr", slotNbr, typeof(int));
            info.AddValue("Packet", Packet, typeof(Packet));
            info.AddValue("Values", Values, typeof(DataMeasureValue[]));
            info.AddValue("Params", Params, typeof(DataValue[]));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion

    }
    #region SlotConverter
#if !WindowsCE
    internal class SlotConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destType)
        {
            if (destType == typeof(string) && value is Slot)
            {
                // Cast the value to an Slot type
                Slot emp = (Slot)value;

                return emp.SlotNbr + ", " + emp.SlotType;
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }
#endif
    #endregion
    #endregion
    #region SlotEventArgs
    [Serializable]
    public class SlotEventArgs : EventArgs
    {
        /// <summary>
        /// Czas nadej�cia Slot
        /// </summary>
        public DateTime Time { get; private set; }
        /// <summary>
        /// Zawartosc Slota
        /// </summary>
        public Slot Slot { get; private set; }

        public SlotEventArgs(DateTime time, Slot slot)
        {
            this.Time = time;
            this.Slot = slot;
        }
        public SlotEventArgs(Slot slot) :
            this(DateTime.Now, slot)
        { }
    }
    #endregion

    #region DeviceInfo
    [Serializable]
    public class DeviceInfo
    {
        public SN SerialNbr;
        public DEVICE Device;
        public DEVICE_TYPE DeviceType;
        public DEVICE_DRIVER DeviceDriver;
        public PROTOCOL Protocol;
        public PROTOCOL_DRIVER ProtocolDriver;

        public List<DataValue> Data;
        public List<DataValue> DeviceDriverData;
        public List<DEVICE_HIERARCHY> HierachyIn;
        public DEVICE_HIERARCHY HierarchyOut;

        public DeviceDriver Driver;
        public Task LastTask;

        public object LockObjForGetDriver = new object();
        public object LockObjForAddToProcess = new object();
        public object LockObjForPacketFromRouter = new object();
        public object LockObjForAckPacket = new object();
        public object LockObjForProcessCommand = new object();
    }
    #endregion
    #region ProcessData
    [Serializable]
#if ISERIALIZABLE_TEST
    public class ProcessData : ISerializable
#else
    public class ProcessData
#endif
    {
        #region Members
        #region SerializationVersion
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion

        public long? IdProcess { get; set; }
        public Enums.ProcessType ProcessType { get; set; }
        public Enums.ProcessStatus ProcessStatus { get; set; }

        public DataMeasureValue[] Values { get; set; }
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }
        #endregion

        #region ctor
        public ProcessData()
        {
            this.Guid = Guid.NewGuid();
            this.DiagnosticInfos = new List<DiagnosticInfo>();
        }
        public ProcessData(Guid guid)
        {
            this.Guid = guid;
            this.DiagnosticInfos = new List<DiagnosticInfo>();
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected ProcessData(SerializationInfo info, StreamingContext context)
        {
            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            Guid = (Guid)info.GetValue("Guid", typeof(Guid));

            if (serializationVersion == 0)
            {
                IdProcess = (long?)info.GetValue("<IdProcess>k__BackingField", typeof(long?));
                Values = (DataMeasureValue[])info.GetValue("<Values>k__BackingField", typeof(DataMeasureValue[]));
                DiagnosticInfos = (List<DiagnosticInfo>)info.GetValue("<DiagnosticInfos>k__BackingField", typeof(List<DiagnosticInfo>));
                ProcessType = (Enums.ProcessType)info.GetValue("<ProcessType>k__BackingField", typeof(int));
                ProcessStatus = (Enums.ProcessStatus)info.GetValue("<ProcessStatus>k__BackingField", typeof(int));
            }
            else if (serializationVersion == 1)
            {
                IdProcess = (long?)info.GetValue("IdProcess", typeof(long?));
                Values = (DataMeasureValue[])info.GetValue("Values", typeof(DataMeasureValue[]));
                DiagnosticInfos = (List<DiagnosticInfo>)info.GetValue("DiagnosticInfos", typeof(List<DiagnosticInfo>));
                ProcessType = (Enums.ProcessType)info.GetValue("ProcessType", typeof(int));
                ProcessStatus = (Enums.ProcessStatus)info.GetValue("ProcessStatus", typeof(int));
            }
        }
#endif
        #endregion

        #region Guid
#if !WindowsCE
        [Browsable(false)]
#endif
        public readonly Guid Guid = Guid.NewGuid();

        public static bool operator ==(ProcessData left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(ProcessData left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ProcessData)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region Clone
#if !WindowsCE
        [Browsable(false)]
#endif
        public ProcessData Clone
        {
            get
            {
                ProcessData processData = new ProcessData(Guid);
                processData.IdProcess = IdProcess;
                processData.ProcessType = ProcessType;
                processData.ProcessStatus = ProcessStatus;

                if (Values != null)
                    processData.Values = (new List<DataMeasureValue>(Values)).ToArray();

                return processData;
            }
        }
        #endregion
        #region Copy
#if !WindowsCE
        [Browsable(false)]
#endif
        public ProcessData Copy
        {
            get
            {
                ProcessData processData = new ProcessData(Guid.NewGuid());
                processData.IdProcess = IdProcess;
                processData.ProcessType = ProcessType;
                processData.ProcessStatus = ProcessStatus;

                if (Values != null)
                    processData.Values = (new List<DataMeasureValue>(Values)).ToArray();

                return processData;
            }
        }
        #endregion

        #region override ToString()
        public override string ToString()
        {
            string strGuid = Guid.ToString();

            return string.Format("Id[{0}] Type[{1}] Status[{2}] Values[{3}] Guid[{4}]",
                IdProcess == null ? "null" : IdProcess.Value.ToString(),
                ProcessType, ProcessStatus,
                Values == null ? "null" : Values.Length.ToString(),
                strGuid.Remove(0, strGuid.LastIndexOf("-") + 1));
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("IdProcess", IdProcess, typeof(long?));
            info.AddValue("ProcessType", (int)ProcessType, typeof(int));
            info.AddValue("ProcessStatus", (int)ProcessStatus, typeof(int));
            info.AddValue("Values", Values, typeof(DataMeasureValue[]));
            info.AddValue("DiagnosticInfos", DiagnosticInfos, typeof(List<DiagnosticInfo>));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion

    }
    #endregion

    #region DeviceDriverData
    [Serializable]
    public class DeviceDriverData
    {
        #region Members
        public long? IdProcess { get; set; }
        public Enums.ProcessType ProcessType { get; set; }
        public Enums.ProcessStatus ProcessStatus { get; set; }

        public SN SerialNbr { get; set; }
        public SN SerialNbrSource { get; set; }	// np SN systemu aby wstawic w protokole WAN2

        public Enums.DeviceType DeviceType { get; set; }
        public Enums.Protocol ProtocolIn { get; set; }
        public Enums.Protocol ProtocolOut { get; set; }
        public DataValue[] Params { get; set; }

        public DataMeasureValue[] Values { get; set; }
        public Slot[] Slot { get; set; }

        public long? IdDeviceDriver { get; set; }
        #endregion

        #region ctor
        public DeviceDriverData()
        {
            this.Guid = Guid.NewGuid();
        }
        public DeviceDriverData(Guid guid)
        {
            this.Guid = guid;
        }
        #endregion

        #region Guid
#if !WindowsCE
        [Browsable(false)]
#endif
        public readonly Guid Guid = Guid.NewGuid();

        public static bool operator ==(DeviceDriverData left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(DeviceDriverData left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DeviceDriverData)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region HasSlots
        public bool HasSlots
        {
            get { return Slot != null && Slot.Length > 0; }
        }
        #endregion
        #region Clone
#if !WindowsCE
        [Browsable(false)]
#endif
        public DeviceDriverData Clone
        {
            get
            {
                DeviceDriverData deviceDriverData = new DeviceDriverData(Guid);
                deviceDriverData.IdProcess = IdProcess;
                deviceDriverData.ProcessType = ProcessType;
                deviceDriverData.ProcessStatus = ProcessStatus;

                deviceDriverData.SerialNbr = SerialNbr;
                deviceDriverData.SerialNbrSource = SerialNbrSource;

                deviceDriverData.DeviceType = DeviceType;
                deviceDriverData.ProtocolIn = ProtocolIn;
                deviceDriverData.ProtocolOut = ProtocolOut;
                if (Params != null)
                    deviceDriverData.Params = (new List<DataValue>(Params)).ToArray();
                if (Values != null)
                    deviceDriverData.Values = (new List<DataMeasureValue>(Values)).ToArray();
                if (Slot != null)
                    deviceDriverData.Slot = (new List<Slot>(Slot)).ToArray();

                deviceDriverData.IdDeviceDriver = IdDeviceDriver;

                return deviceDriverData;
            }
        }
        #endregion
        #region Copy
#if !WindowsCE
        [Browsable(false)]
#endif
        public DeviceDriverData Copy
        {
            get
            {
                DeviceDriverData deviceDriverData = new DeviceDriverData(Guid.NewGuid());
                deviceDriverData.IdProcess = IdProcess;
                deviceDriverData.ProcessType = ProcessType;
                deviceDriverData.ProcessStatus = ProcessStatus;

                deviceDriverData.SerialNbr = SerialNbr;
                deviceDriverData.SerialNbrSource = SerialNbrSource;

                deviceDriverData.DeviceType = DeviceType;
                deviceDriverData.ProtocolIn = ProtocolIn;
                deviceDriverData.ProtocolOut = ProtocolOut;
                if (Params != null)
                    deviceDriverData.Params = (new List<DataValue>(Params)).ToArray();
                if (Values != null)
                    deviceDriverData.Values = (new List<DataMeasureValue>(Values)).ToArray();
                if (Slot != null)
                    deviceDriverData.Slot = (new List<Slot>(Slot)).ToArray();

                deviceDriverData.IdDeviceDriver = IdDeviceDriver;

                return deviceDriverData;
            }
        }
        #endregion

        #region override ToString()
        public override string ToString()
        {
            string strGuid = Guid.ToString();

            return string.Format("SN[{0}] Id[{1}] Type[{2}] Status[{3}] DeviceType[{4}] ProtocolIn[{5}] ProtocolOut[{6}] Slots[{7}] Params[{8}] Values[{9}] Guid[{10}]",
                SerialNbr, IdProcess, ProcessType, ProcessStatus,
                DeviceType, ProtocolIn, ProtocolOut,
                Slot == null ? "null" : Slot.Length.ToString(),
                Params == null ? "null" : Params.Length.ToString(),
                Values == null ? "null" : Values.Length.ToString(),
                strGuid.Remove(0, strGuid.LastIndexOf("-") + 1));
        }
        #endregion
    }
    #endregion


    #region DeviceDriver
    [Serializable]
    public abstract class DeviceDriver : MarshalByRefObjectDisposable, IDisposable
    {
        #region Definitions
        public enum CommandType
        {
            DumpLog = 1,
            GetState = 2,
            SaveState = 3,
            LoadState = 4,
            RemoveProcess = 5,
        }
        #endregion Definitions

        #region Events
        /// <summary>
        /// Confirms performing process
        /// </summary>
        public event EventHandler<DeviceDriverEventArgs> AckEvent;
        /// <summary>
        /// Requests sending outgoing packet(s)
        /// </summary>
        public event EventHandler<SendPacketEventArgs> SendPacketEvent;
        /// <summary>
        /// Confirms performing packet
        /// </summary>
        public event EventHandler<SlotEventArgs> AckPacketEvent;
        /// <summary>
        /// Requests carries out unknown packet
        /// </summary>
        public event EventHandler<PacketEventArgs> IncomingUnknownPacketEvent;

        /// <summary>
        /// Get missing Values for specified DataTypes
        /// </summary>
        private event GetDataValuesDelegate _getDataValuesEvent;
        public event GetDataValuesDelegate GetDataValuesEvent
        {
            add
            {
                try
                {
                    _getDataValuesEvent = (GetDataValuesDelegate)Delegate.Combine(_getDataValuesEvent, value);
                }
                catch (Exception ex)
                {
                }
            }
            remove
            {
                _getDataValuesEvent = (GetDataValuesDelegate)Delegate.Remove(_getDataValuesEvent, value);
            }
        }

        //===Projekt #001 "Wysy�anie zapyta� przez wymuszon� hierarchie urz�dze�"
        ///// <summary>
        ///// Get slots out requested for another device
        ///// </summary>
        //private event GetSlotsOutForDeviceDelegate _getSlotsOutForDeviceEvent;
        //public event GetSlotsOutForDeviceDelegate GetSlotsOutForDeviceEvent
        //{
        //    add
        //    {
        //        try
        //        {
        //            _getSlotsOutForDeviceEvent = (GetSlotsOutForDeviceDelegate)Delegate.Combine(_getSlotsOutForDeviceEvent, value);
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //    }
        //    remove
        //    {
        //        _getSlotsOutForDeviceEvent = (GetSlotsOutForDeviceDelegate)Delegate.Remove(_getSlotsOutForDeviceEvent, value);
        //    }
        //}
        //======================END Projekt #001
        #endregion

        #region Delegates
        public delegate DataValue[] GetDataValuesDelegate(SN SerialNbr, long[] IdDataTypes);
        //===Projekt #001 "Wysy�anie zapyta� przez wymuszon� hierarchie urz�dze�"
        //public delegate Slot[] GetSlotsOutForDeviceDelegate(SN SerialNbr, DataMeasureValue[] Values, Enums.ProcessType ProcessType, Enums.Protocol Protocol);
        //======================END Projekt #001
        #endregion

        #region Members
        public DeviceInfo DeviceInfo;
        //public Task LastTask;

        #region AssemblyName
        /// <summary>
        /// Nazwa biblioteki drivera wraz z wersj�
        /// </summary>
        public string NameAndVersion;
        #endregion
        #region DriverDependenciesOutdated
        /// <summary>
        /// Flaga m�wi�ca czy zale�ne drivery (za�adowane do AppDomain) s� w niew�a�ciwej/starej wersji
        /// </summary>
        public bool DriverDependenciesOutdated = false;
        #endregion

        #region Map
        public MemoryMapForDevice MemoryMap { get; set; }
        #endregion

        #region LogEnabled
        private bool logEnabled = true;
        /// <summary>
        /// Enable/Disable logging to file. Default is true;
        /// </summary>
        public bool LogEnabled
        {
            get { return logEnabled; }
            set { logEnabled = value; }
        }
        #endregion
        #region LogLevel
        private LogLevel logLevel = LogLevel.Trace;
        /// <summary>
        /// Logging level (if enabled). Default is Trace.
        /// </summary>
        public LogLevel LogLevel
        {
            get { return logLevel; }
            set { logLevel = value; }
        }
        #endregion
        #endregion

        #region Init
        /// <summary>
        /// Initializes module
        /// </summary>
        /// <returns>Initilization succesfully</returns>
        public virtual bool Init()
        {
            this.DeviceInfo = null;
            return true;
        }
        public virtual bool Init(DataValue[] Params)
        {
            return true;
        }

        public virtual bool Init(DeviceInfo DeviceInfo)
        {
            this.DeviceInfo = DeviceInfo;
            return Init(DeviceInfo.DeviceDriverData.ToArray());
        }

        #endregion Init
        #region Clear
        public virtual void Clear() { }
        #endregion
        #region Abort
        public virtual void Abort() { }
        #endregion

        #region AddToProcess
        /// Gets or puts parameters of given device(s)
        public virtual void AddToProcess(DeviceDriverData[] Data)
        { }

        //===Projekt #001 "Wysy�anie zapyta� przez wymuszon� hierarchie urz�dze�"
        ///// Gets or puts parameters of given device(s)
        //public void AddToProcess(ref DeviceDriverData[] Data)
        //{
        //    AddToProcess(Data);
        //}
        //======================END Projekt #001

        /// <summary>
        /// Gets or puts parameters of given device(s)
        /// </summary>
        /// <param name="ProcessData">Parametry procesu/akcji. Tablica: mo�e kiedy� b�d� get/put w jednym zleceniu</param>
        /// <param name="SlotsIn">Wej�ciowe sloty jakie ma przetworzy� driver</param>
        /// <param name="SlotsOut">Wyj�ciowe sloty stworzone przez driver</param>
        public virtual bool AddToProcess(ProcessData[] ProcessData, Slot[] SlotsIn, out Slot[] SlotsOut)
        {
            SlotsOut = new Slot[0];

            if (ProcessData != null && ProcessData.Length > 0)
            {
                DeviceDriverData[] deviceDriverData = new DeviceDriverData[ProcessData.Length];
                for (int i = 0; i < ProcessData.Length; i++)
                {
                    deviceDriverData[i] = new DeviceDriverData(ProcessData[i].Guid)
                    {
                        SerialNbr = DeviceInfo.SerialNbr,
                        IdProcess = ProcessData[i].IdProcess,
                        ProcessType = ProcessData[i].ProcessType,
                        ProcessStatus = ProcessData[i].ProcessStatus,
                        DeviceType = DeviceInfo.Device.IdDeviceType,
                        ProtocolOut = DeviceInfo.HierarchyOut.ProtocolOut,
                        Slot = SlotsIn,
                        Values = ProcessData[i].Values,
                        Params = DeviceInfo.DeviceDriverData.ToArray()
                    };
                }

                AddToProcess(deviceDriverData);

                if (deviceDriverData.Length > 0)
                {
                    List<Slot> slotsOutList = new List<Slot>();
                    bool bResult = true;
                    foreach (DeviceDriverData ddd in deviceDriverData)
                    {
                        slotsOutList.AddRange(deviceDriverData[0].Slot);
                        if (!ddd.ProcessStatus.In(Enums.ProcessStatus.New, Enums.ProcessStatus.Added, Enums.ProcessStatus.FinishedOk))
                            bResult = false;
                    }

                    SlotsOut = slotsOutList.ToArray();

                    return bResult;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets or puts parameters of given device(s)
        /// </summary>
        /// <param name="ProcessData">Parametry procesu/akcji. Tablica: mo�e kiedy� b�d� get/put w jednym zleceniu</param>
        /// <param name="SlotsIn">Wej�ciowe sloty jakie ma przetworzy� driver</param>
        /// <param name="SlotsOut">Wyj�ciowe sloty stworzone przez driver</param>
        public virtual bool AddToProcess(ref ProcessData[] ProcessData, Slot[] SlotsIn, out Slot[] SlotsOut)
        {
            return AddToProcess(ProcessData, SlotsIn, out SlotsOut);
        }
        #endregion AddToProcess
        #region OnAckPacket
        /// <summary>
        /// Acknowledges sending outgoing packet
        /// </summary>
        /// <param name="PacketOutAck"></param>
        public virtual void OnAckPacket(PacketEventArgs e)
        {
        }
        /// <summary>
        /// Acknowledges sending outgoing packet
        /// </summary>
        /// <param name="PacketOutAck"></param>
        public virtual void OnAckPacket(Slot slot)
        {
            OnAckPacket(new PacketEventArgs(slot.Packet));
        }
        #endregion OnAckPacket
        #region OnPacketReceived
        /// <summary>
        /// Carries out incoming packet(s)
        /// </summary>
        /// <param name="Packet">Packet(s) incoming</param>
        public virtual void OnPacketReceived(ref DeviceDriverData[] Data)
        { }

        public virtual void OnPacketReceived(Slot SlotIn, Enums.Protocol ProtocolIn, out Slot[] ReceivedSlots, out ProcessData ProcessData)
        {
            ProcessData[] ProcessDataTable;
            OnPacketReceived(SlotIn, ProtocolIn, out ReceivedSlots, out ProcessDataTable);

            if (ProcessDataTable != null && ProcessDataTable.Length > 0)
                ProcessData = ProcessDataTable[0];
            else
                ProcessData = new ProcessData() { ProcessStatus = Enums.ProcessStatus.Unknown };
        }

        public virtual void OnPacketReceived(Slot SlotIn, Enums.Protocol ProtocolIn, out Slot[] ReceivedSlots, out ProcessData[] ProcessData)
        {
            DeviceDriverData[] deviceDriverData = new DeviceDriverData[]
            {
                new DeviceDriverData()
                {
                    SerialNbr = DeviceInfo.SerialNbr,
                    DeviceType = DeviceInfo.Device.IdDeviceType,
                    ProtocolIn = ProtocolIn,
                    Slot = new Slot[] { SlotIn },
                    Params = DeviceInfo.DeviceDriverData.ToArray()
                }
            };
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            OnPacketReceived(ref deviceDriverData);
            sw.Stop();
            //DebugLog(String.Format("!!!!!! DD_OnPacketReceived: Time[{2}ms] [{0}] SN[{1}]", DeviceInfo.DeviceDriver.Name, DeviceInfo.SerialNbr.DBValue, sw.ElapsedMilliseconds));
            //sw.Restart();

            ProcessData processData = new ProcessData();
            ReceivedSlots = new Slot[0];

            if (deviceDriverData != null && deviceDriverData.Length > 0)
            {
                processData = new ProcessData(deviceDriverData[0].Guid);
                processData.IdProcess = deviceDriverData[0].IdProcess;
                processData.ProcessType = deviceDriverData[0].ProcessType;
                processData.ProcessStatus = deviceDriverData[0].ProcessStatus;

                if (deviceDriverData[0].Values != null && deviceDriverData[0].Values.Length > 0)
                    processData.Values = deviceDriverData[0].Values.ToArray();
                else
                    processData.Values = new DataMeasureValue[0];

                if (deviceDriverData[0].HasSlots)
                    ReceivedSlots = deviceDriverData[0].Slot.Where(w => w.SlotType != Enums.SlotType.None).ToArray();
            }
            else
            {
                processData.ProcessStatus = Enums.ProcessStatus.Unknown;
            }

            ProcessData = new ProcessData[] { processData };

            //sw.Stop();
            //DebugLog(String.Format("!!!!!! DD_CreateProcessData: Time[{2}ms] [{0}] SN[{1}]", DeviceInfo.DeviceDriver.Name, DeviceInfo.SerialNbr.DBValue, sw.ElapsedMilliseconds));
            //sw.Restart();

            if (MemoryMap != null && deviceDriverData != null && deviceDriverData.Length > 0 && deviceDriverData[0].Slot.Length > 0)
                MemoryMap.CleanUnusedCommands(processData.Guid);

            //sw.Stop();
            //DebugLog(String.Format("!!!!!! DD_CleanUnusedCommands: Time[{2}ms] [{0}] SN[{1}]", DeviceInfo.DeviceDriver.Name, DeviceInfo.SerialNbr.DBValue, sw.ElapsedMilliseconds));
            //DebugLog(String.Format("!!!!!! DD_EndOfAll: Time[{2}ms] [{0}] SN[{1}] [{3}]", DeviceInfo.DeviceDriver.Name, DeviceInfo.SerialNbr.DBValue, sw.ElapsedMilliseconds, DateTime.Now.ToString("hh:mm:ss.fff tt")));            
        }
        #endregion OnPacketReceived
        #region ProcessCommand
        public virtual object ProcessCommand(CommandType commandType, params object[] param)
        {
            return null;
        }
        #endregion

        #region InvokeSendPacketEvent
        /// <summary>
        /// Calls client procedure sending outgoing packet(s) (if client subscribed to event)
        /// </summary>
        /// <param name="e"></param>
        public void InvokeSendPacketEvent(DeviceDriverEventArgs e)
        {
            EventHandler<SendPacketEventArgs> handler = SendPacketEvent;
            if (handler != null)
            {
                if (e.DeviceDriverData.Length > 0 && e.DeviceDriverData[0].HasSlots)
                    handler(this, new SendPacketEventArgs(
                        new ProcessData() { IdProcess = e.DeviceDriverData[0].IdProcess, ProcessType = e.DeviceDriverData[0].ProcessType, ProcessStatus = e.DeviceDriverData[0].ProcessStatus, Values = e.DeviceDriverData[0].Values },
                        e.DeviceDriverData[0].Slot[0]));
                else
                    handler(this, new SendPacketEventArgs(new ProcessData(), new Slot()));
            }
        }
        /// <summary>
        /// Calls client procedure sending outgoing packet(s) (if client subscribed to event)
        /// </summary>
        /// <param name="e"></param>
        public void InvokeSendPacketEvent(SendPacketEventArgs e)
        {
            EventHandler<SendPacketEventArgs> handler = SendPacketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion InvokeSendPacketEvent
        #region InvokeAckPacketEvent
        /// <summary>
        /// Calls client procedure confirming performing packet (if client subscribed to event)
        /// </summary>
        public virtual void InvokeAckPacketEvent(PacketEventArgs e)
        {
            EventHandler<SlotEventArgs> handler = AckPacketEvent;
            if (handler != null)
            {
                handler(this, new SlotEventArgs(new Slot() { Packet = e.Packet }));
            }
        }
        public virtual void InvokeAckPacketEvent(SlotEventArgs e)
        {
            EventHandler<SlotEventArgs> handler = AckPacketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion InvokeIncomingUnknownPacketEvent
        #region InvokeIncomingUnknownPacketEvent
        /// <summary>
        /// Calls client procedure carrying out unknown packet (if client subscribed to event)
        /// </summary>
        /// <param name="e"></param>
        protected virtual void InvokeIncomingUnknownPacketEvent(PacketEventArgs e)
        {
            EventHandler<PacketEventArgs> handler = IncomingUnknownPacketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion InvokeIncomingUnknownPacketEvent
        #region InvokeAckEvent
        /// <summary>
        /// Calls client procedure confirming performing process (if client subscribed to event)
        /// </summary>
        /// <param name="e"></param>
        public virtual void InvokeAckEvent(DeviceDriverEventArgs e)
        {
            EventHandler<DeviceDriverEventArgs> handler = AckEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion InvokeAckProcessEvent

        #region GetDataValues
        protected DataValue[] GetDataValues(SN SerialNbr, long[] IdDataTypes)
        {
            GetDataValuesDelegate handler = _getDataValuesEvent;
            if (handler != null)
                return handler(SerialNbr, IdDataTypes);
            else
            {
                return null;
            }
        }
        #endregion
        #region ReloadParamsForMemoryMap
        public void ReloadParamsForMemoryMap(IEnumerable<DataValue> parameters)
        {
            if (MemoryMap != null)
                MemoryMap.DeviceDriverParams = parameters.ToArray();
        }
        #endregion

        #region GetDiagnosticInfo
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion, description);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description, object extensionError)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion, description, extensionError);
        }
        #endregion


        //===Projekt #001 "Wysy�anie zapyta� przez wymuszon� hierarchie urz�dze�"
        //#region GetSlotsOutForDevice
        //protected Slot[] GetSlotsOutForDevice(SN SerialNbr, DataMeasureValue[] Values, Enums.ProcessType ProcessType, Enums.Protocol Protocol)
        //{
        //    GetSlotsOutForDeviceDelegate handler = _getSlotsOutForDeviceEvent;
        //    if (handler != null)
        //        return handler(SerialNbr, Values, ProcessType, Protocol);
        //    else
        //    {
        //        return null;
        //    }
        //}
        //#endregion
        //======================END Projekt #001

        #region Log
        public void Log(LogData EventData, params object[] Parameters)
        {
            if (LogEnabled && EventData.LogLevel >= LogLevel)
                Logger.Add(EventData, Parameters);
        }
        public void DebugLog(string f, params object[] args)
        {
            Logger.Add(new LogData(1, LogLevel.Error, string.Format(f, args)), null);
        }
        #endregion

    }
    #endregion
}
