//#define DRV_V2 - to jest zdefiniowane w Conditional compilation symbols (Properties projektu)

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Reflection;
//using System.Runtime.Remoting;
using System.ComponentModel;

using IMR.Suite.Common;
using IMR.Suite.Common.Code;
using IMR.Suite.Common.Drivers.Protocol;

namespace IMR.Suite.Common.Drivers.Protocol
{
	#region AckTaskEventArgs
	public class AckTaskEventArgs<TProtocolTask> : EventArgs
	{
		public readonly TProtocolTask Task;
		public readonly DateTime Time;

		public AckTaskEventArgs()
		{
		}
		public AckTaskEventArgs(TProtocolTask Task, DateTime Time)
		{
			this.Task = Task;
			this.Time = Time;
		}
	}
	#endregion

	#region ProtocolCommand
	[Serializable]
#if !WindowsCE
	[TypeConverter(typeof(ExpandableObjectConverter))]
#endif
	public abstract class ProtocolCommand : MarshalByRefObjectDisposable
	{
		#region IdCommand
		/// <summary>
		/// Identyfikator komendy - Get, Put, Alone, None,
		/// </summary>
		private int? idCommand;
#if !WindowsCE
		[Browsable(false)]
		[ReadOnly(true)]
#endif
		public int? IdCommand
		{
			get { return idCommand; }
			set { idCommand = value; }
		}
		#endregion
		#region Status
		/// <summary>
		/// 
		/// </summary> 
		private Enums.CommandStatus status;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public Enums.CommandStatus Status
		{
			get { return status; }
			set { status = value; }
		}
		#endregion
		#region Type
		/// <summary>
		/// Typ komendy - Get, Put, Alone, None,
		/// </summary>
		private Enums.CommandType type;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public Enums.CommandType Type
		{
			get { return type; }
			set { type = value; }
		}
		#endregion
		#region Address
		/// <summary>
		/// Adres zmiennej - Get, Put, Alone, None,
		/// </summary>
		private uint address;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public uint Address
		{
			get { return address; }
			set { address = value; }
		}
		#endregion
		#region Value
		/// <summary>
		/// Warto�� zmiennej - Get, Put, Alone, None,
		/// </summary>
		private byte[] _Value;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public byte[] Value
		{
            get { return _Value; }
            set { _Value = value; }
		}
		#endregion

		#region MarshalByRefObjectDisposable overrides
#if !WindowsCE
        public override object InitializeLifetimeService()
        {
            return null;
        }
#endif
		#endregion
	}
	#endregion

	#region ProtocolTask
	[Serializable]
#if !WindowsCE
	[TypeConverter(typeof(ExpandableObjectConverter))]
#endif
	public abstract class ProtocolTask<TProtocolCommand> : MarshalByRefObjectDisposable
	{
		#region IdTask
		/// <summary>
		/// Identyfikator Taska
		/// </summary>
		private int? idTask;
#if !WindowsCE
		[Browsable(false)]
		[ReadOnly(true)]
#endif

		public int? IdTask
		{
			get { return idTask; }
			set { idTask = value; }
		}
		#endregion
		#region Status
		/// <summary>
		/// 
		/// </summary>
		private Enums.TaskStatus status;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public Enums.TaskStatus Status
		{
			get { return status; }
			set { status = value; }
		}
		#endregion
		#region TransmissionType
		/// <summary>
		/// Typ transmisji
		/// </summary>
		private Enums.TransmissionType transmissionType;
#if !WindowsCE
		[Browsable(false)]
		[ReadOnly(true)]
#endif
		public Enums.TransmissionType TransmissionType
		{
			get { return transmissionType; }
			set { transmissionType = value; }
		}
		#endregion
		#region TaskTimeout
		/// <summary>
		/// Timeout na realizacj� ca�ego zlecenia
		/// </summary>
		private TimeSpan taskTimeout = TimeSpan.Zero;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public TimeSpan TaskTimeout
		{
			get { return taskTimeout; }
			set { taskTimeout = value; }
		}
		#endregion
		#region TimeStamp
		/// <summary>
		/// Metka czasowa zlecenia
		/// </summary>
#if !WindowsCE
		private DateTimeOffset timeStamp;
		[ReadOnly(true)]
		public DateTimeOffset TimeStamp
		{
			get { return timeStamp; }
			set { timeStamp = value; }
		}
#else
		private DateTime timeStamp;
		public DateTime TimeStamp
		{
			get { return timeStamp; }
			set { timeStamp = value; }
		}
#endif
		#endregion
        #region Address
        /// <summary>
        /// Adres urzadzenia
        /// </summary>
        private string address;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public string Address
        {
            get { return address; }
            set { address = value; }
        }
        #endregion
        #region SCSerialNbr
        private string scSerialNbr = "";
		/// <summary>
		/// Dla taskow wychodzacych jest to SerialNbr systemu
		/// </summary>
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public string SCSerialNbr
        {
            get { return scSerialNbr; }
            set { scSerialNbr = value; }
        }
        #endregion
        #region RTUSerialNbr
        private string rtuSerialNbr = "";
		/// <summary>
		/// Dla taskow przychodzacych jest to SerialNbr urzadzenia
		/// </summary>
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public string RTUSerialNbr
        {
            get { return rtuSerialNbr; }
            set { rtuSerialNbr = value; }
        }
        #endregion
		#region Command
		/// <summary>
		/// Lista komend do realizacji
		/// </summary>
		private TProtocolCommand[] command;
#if !WindowsCE
		[ReadOnly(true)]
#endif
		public TProtocolCommand[] Command
		{
			get { return command; }
			set { command = value; }
		}
		#endregion

		public Guid Guid = Guid.NewGuid();

		#region MarshalByRefObjectDisposable overrides
#if !WindowsCE
        public override object InitializeLifetimeService()
        {
            return null;
        }
#endif
		#endregion
	}
	#endregion

	#region ProtocolDriver
	[Serializable]
	public abstract class ProtocolDriver<TProtocolTask> : MarshalByRefObjectDisposable
	{
		#region Definitions
		public enum CommandType
		{
			DumpLog = 1,
			GetState = 2,
			SaveState = 3,
			LoadState = 4,
            RemoveTask = 5,
            RestoreTask = 6,
		}
		#endregion Definitions

		#region Members
		#region LogEnabled
		private bool logEnabled = true;
		/// <summary>
		/// Enable/Disable logging to file. Default is true;
		/// </summary>
		public bool LogEnabled
		{
			get { return logEnabled; }
			set { logEnabled = value; }
		}
		#endregion
		#region LogLevel
		private LogLevel logLevel = LogLevel.Trace;
		/// <summary>
		/// Logging level (if enabled). Default is Trace.
		/// </summary>
		public LogLevel LogLevel
		{
			get { return logLevel; }
			set { logLevel = value; }
		}
        #endregion
        #region NameAndVersion
        /// <summary>
        /// Nazwa biblioteki, u�ywana do przekazywania informacji w DiagnosticInfo
        /// </summary>
        public string NameAndVersion { get; set; }
	    #endregion
        #endregion

		public event EventHandler<AckTaskEventArgs<TProtocolTask>> AckTaskEvent;
#if DRV_V2
		[Obsolete("Not needed anymore. DeviceDriver should send packet manually")]
#endif
		public event EventHandler<PacketEventArgs> SendPacketEvent;
#if DRV_V2
		[Obsolete("Not needed anymore")]
#endif
		public event EventHandler<PacketEventArgs> IncomingUnknownPacketEvent;

		public ProtocolDriver() {}
		public abstract bool Init();

		public abstract ManualResetEvent Stop();

#if DRV_V2
		public abstract Enums.TaskStatus AddToSend(TProtocolTask ProtocolTask);
#else
		public abstract void AddToSend(TProtocolTask ProtocolTask);
#endif
		public abstract void Remove(TProtocolTask ProtocolTask);

#if DRV_V2
		public abstract TProtocolTask OnAckPacket(PacketEventArgs e);
#else
		public abstract void OnAckPacket(PacketEventArgs e);
#endif
#if DRV_V2
		public abstract TProtocolTask OnPacketReceived(PacketEventArgs e);
#else
		public abstract void OnPacketReceived(PacketEventArgs e);
#endif

		public virtual object ProcessCommand(CommandType commandType, params object[] param) { return null; }
		public virtual bool TryGetSerialNbrOrAddress(Body packet, out SN SerialNbr, out List<DataValue> Address) { SerialNbr = null; Address = new List<DataValue>();  return false; }
        public virtual bool TryGetIVIn(Body packet, out uint? IVIn) { IVIn = null; return false; }

        public virtual bool UpdateRetAddress(string retAddress, Enums.TransmissionDriverType DriverType, ref Body body, SN DeviceSerialNbr, byte[] privateKey, byte[] macKey, ref uint? IV_out)
        {
            return UpdateRetAddress(retAddress, DriverType, ref body);
        }
		public virtual bool UpdateRetAddress(string retAddress, Enums.TransmissionDriverType DriverType, ref Body body) { return true; }
 

		#region Log
		public void Log(LogData EventData, params object[] Parameters)
		{
			if (LogEnabled && EventData.LogLevel >= LogLevel)
				Logger.Add(EventData, Parameters);
		}
		#endregion

		#region Invoke...
		protected virtual void InvokeAckTaskEvent(AckTaskEventArgs<TProtocolTask> e)
		{
            // poni�szy IF mozna zast�pi� tym (jak b�dzie VS2015 lub wy�szy): AckTaskEvent?.Invoke(this, e);
            if (AckTaskEvent != null)
                AckTaskEvent.Invoke(this, e);
		}
        protected virtual void InvokeSendPacketEvent(PacketEventArgs e)
        {
            // poni�szy IF mozna zast�pi� tym (jak b�dzie VS2015 lub wy�szy): SendPacketEvent?.Invoke(this, e);
            if (SendPacketEvent != null)
                SendPacketEvent.Invoke(this, e);
		}
        protected virtual void InvokeIncomingUnknownPacketEvent(PacketEventArgs e)
        {
            // poni�szy IF mozna zast�pi� tym (jak b�dzie VS2015 lub wy�szy): IncomingUnknownPacketEvent?.Invoke(this, e);
            if (IncomingUnknownPacketEvent != null)
                IncomingUnknownPacketEvent.Invoke(this, e);
		}
		#endregion

        #region GetDiagnosticInfo
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion, description);
        }
        public DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description, object extensionError)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, NameAndVersion, description, extensionError);
        }
        #endregion

		#region MarshalByRefObjectDisposable overrides
#if !WindowsCE
        public override object InitializeLifetimeService()
        {
            return null;
        }
#endif
		#endregion
	}
	#endregion
}
