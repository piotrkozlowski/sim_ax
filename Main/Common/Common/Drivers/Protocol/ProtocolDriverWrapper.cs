﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Common.Drivers.Protocol
{
#if !WindowsCE && !Android
    [Serializable]
    public class ProtocolDriverWrapper : MarshalByRefObjectDisposable
    {
        public AssemblyName ProtocolDriverName = null;    // Jest to jednocześnie flaga mówiąca czy DLLka jest już załadowana do pamięci (==null) => nie, trzeba załadować)
        public Type ProtocolDriverType = default(Type);
        public string ProtocolDriverPath = string.Empty;
        public long InstanceCount = 0;

        #region private Members
        private dynamic Driver = null;
        private bool UseAppDomain = false;
        private readonly AppDomain ProtocolDriverDomain;
        //private object protocolCreateLockObject = new object();
        #endregion

        #region Constructor
        public ProtocolDriverWrapper()
        {
        }
        public ProtocolDriverWrapper(string protoclDriverPath, bool useAppDomain = false)
        {
            ProtocolDriverPath = protoclDriverPath;
            UseAppDomain = useAppDomain;

            if (!Path.IsPathRooted(ProtocolDriverPath))
                ProtocolDriverPath = AppDomain.CurrentDomain.BaseDirectory + ProtocolDriverPath;
            ProtocolDriverPath = Path.GetFullPath(ProtocolDriverPath);

            #region Jeśli ścieżka jest poza (not relative to base) ścieżką bazową EXE to kopiujemy do Cache (relative to base)
            string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
            string DllPath = Path.GetDirectoryName(ProtocolDriverPath);
            if (DllPath.IndexOf(AppPath) == -1)
            {

                string dllDirectoryName = Path.Combine(AppPath, "Cache");
                DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
                if (!di.Exists)
                {
                    di.Create();
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }

                string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(ProtocolDriverPath);
                try
                {
                    File.Copy(ProtocolDriverPath, newAssemblyPath, true);
                }
                catch { }
                ProtocolDriverPath = newAssemblyPath;
            }
            #endregion

            if (UseAppDomain)
            {
                AppDomainSetup ads = new AppDomainSetup();
                ads.ApplicationName = "ProtoclDriverWrapper";
                ads.ApplicationBase = AppPath;
                ads.PrivateBinPath = new Uri(AppPath + "\\").MakeRelativeUri(new Uri(DllPath)).ToString();
                ads.ShadowCopyFiles = "true";

                ProtocolDriverDomain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(ProtocolDriverPath), null, ads);
            }
        }

        #endregion
        #region Destructor
        ~ProtocolDriverWrapper()
        {
            Dispose(false);
        }
        #endregion

        #region Unload
        public void Unload(bool forceDispose)
        {
            try
            {
                ProtocolDriverName = null;
                if (UseAppDomain)
                {
                    if (!forceDispose && Interlocked.Read(ref InstanceCount) == 0)
                        AppDomain.Unload(ProtocolDriverDomain);
                    else if (forceDispose)
                        AppDomain.Unload(ProtocolDriverDomain);
                }
                GC.Collect();
            }
            catch { }
        }
        #endregion

        #region CreateInstance
        public dynamic CreateInstance(object[] DriverConstructorArgs)
        {
            //lock (protocolCreateLockObject)
            lock ("LoadAssemblyLock")
            {
                if (ProtocolDriverName == null) // jeszcze nie załadowana lub została odładowana
                {
                    Assembly assembly = null;
                    
                    if (UseAppDomain && ProtocolDriverDomain != null)
                    {
                        using (ProtocolDriverWrapper wrapper = (ProtocolDriverWrapper)ProtocolDriverDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(ProtocolDriverWrapper).FullName))
                            assembly = wrapper.LoadAssembly(ProtocolDriverPath);
                    }
                    else
                        assembly = AssemblyWrapper.LoadAssembly(ProtocolDriverPath);

                    if (assembly == null)
                        return null;

                    ProtocolDriverType = GetDerivedType(assembly.GetTypes(), typeof(ProtocolDriver<ProtocolTask<ProtocolCommand>>));
                    if (ProtocolDriverType == default(Type))
                        return null;

                    ProtocolDriverName = assembly.GetName();
                }
                if (Driver == null)
                {
                    if (UseAppDomain && ProtocolDriverDomain != null)
                        Driver = ProtocolDriverDomain.CreateInstanceAndUnwrap(ProtocolDriverName.FullName, ProtocolDriverType.ToString(), false, BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, DriverConstructorArgs, null, null);
                    else
                        Driver = Activator.CreateInstance(ProtocolDriverType, DriverConstructorArgs);
                }
                if (Driver == null)
                    return null;

                Driver.NameAndVersion = string.Format("{0}, {1}", ProtocolDriverName.Name, ProtocolDriverName.Version.ToString()); ;
                Interlocked.Increment(ref InstanceCount);
                return Driver;
            }
        }
        #endregion
        #region FreeInstance
        public void FreeInstance(dynamic driver)
        {
            if (driver != null)
            {
                try
                {
                    Interlocked.Decrement(ref InstanceCount);
                }
                finally
                {
                    driver = null;
                }
            }
        }
        #endregion

        #region TryGetSerialNbrOrAddress
        public bool TryGetSerialNbrOrAddress(Body packet, out SN serialNbr, out List<DataValue> address)
        {
            serialNbr = null;
            address = new List<DataValue>();

            if (ProtocolDriverName == null || Driver == null) // jeszcze nie załadowana lub została odładowana
            {
                Driver = CreateInstance(null);
                if (Driver == null)
                    return false;
            }
            return Driver.TryGetSerialNbrOrAddress(packet, out serialNbr, out address);
            //object[] args = new object[] { packet, serialNbr, address };
            //bool result = (bool)Driver.GetType().GetMethod("TryGetSerialNbrOrAddress").Invoke(Driver, args);
            //serialNbr = (SN)args[1];
            //address = (List<DataValue>)args[2];
            //return result;
        }
        #endregion
        #region UpdateRetAddress
        public bool UpdateRetAddress(string retAddress, Enums.TransmissionDriverType driverType, ref Body body, SN deviceSerialNbr, byte[] privateKey, byte[] macKey, ref uint? IVOut)
        {
            if (ProtocolDriverName == null || Driver == null) // jeszcze nie załadowana lub została odładowana
            {
                Driver = CreateInstance(null);
                if (Driver == null)
                    return false;
            }
            return Driver.UpdateRetAddress(retAddress, driverType, ref body, deviceSerialNbr, privateKey, macKey, ref IVOut);
            //object[] args = new object[] { retAddress, driverType, body, deviceSerialNbr, privateKey, macKey, IVOut };
            //MethodInfo[] allMethods = Driver.GetType().GetMethods();
            //MethodInfo foundMethod = allMethods.FirstOrDefault(mi => mi.Name == "UpdateRetAddress" && mi.GetParameters().Count() == 7);
            //if (foundMethod != null)
            //{
            //    //bool result = (bool)Driver.GetType().GetMethod("UpdateRetAddress", new[] {typeof(string), typeof(Enums.TransmissionDriverType), typeof(Body), typeof(SN), typeof(byte[]),typeof(byte[]), typeof(uint?)}).Invoke(Driver, args);
            //    bool result = (bool)foundMethod.Invoke(Driver, args);
            //    body = (Body)args[2];
            //    IVOut = (uint?)args[6];

            //    return result;
            //}
            //return false;
        }
        #endregion
        #region TryGetIVIn
        public bool TryGetIVIn(Body packet, out uint? IVIn)
        {
            IVIn = null;
            if (ProtocolDriverName == null || Driver == null) // jeszcze nie załadowana lub została odładowana
            {
                Driver = CreateInstance(null);
                if (Driver == null)
                    return false;
            }
            return Driver.TryGetIVIn(packet, out IVIn);
            //object[] args = new object[] { packet, IVIn };
            //bool result = (bool)Driver.GetType().GetMethod("TryGetIVIn").Invoke(Driver, args);
            //IVIn = (uint?)args[1];
            //return result;
        }
        #endregion

        #region private GetDerivedType
        private Type GetDerivedType(Type[] assemblyTypes, Type typeToFind)
        {
            foreach (Type typeInAssembly in assemblyTypes)
            {
                if (typeInAssembly.IsClass && typeInAssembly.IsPublic && !typeInAssembly.IsAbstract)
                {
                    var found = false;
                    Type baseClass = typeInAssembly;
                    do
                    {
                        found = baseClass.IsGenericType && typeToFind.GetGenericTypeDefinition().GUID == baseClass.GetGenericTypeDefinition().GUID;
                        baseClass = baseClass.BaseType;
                    }
                    while (baseClass != null && !found);

                    if (found)
                        return typeInAssembly;
                }
            }
            return default(Type);
        }
        #endregion
        #region private LoadAssembly
        private Assembly LoadAssembly(string protocolDriverPath)
        {
            Assembly assembly = Assembly.Load(AssemblyName.GetAssemblyName(protocolDriverPath));
            if (assembly == null)
                return null;

            foreach (AssemblyName an in assembly.GetReferencedAssemblies())
                Assembly.Load(an);

            return assembly;
        }
        #endregion
    }
#endif
}
