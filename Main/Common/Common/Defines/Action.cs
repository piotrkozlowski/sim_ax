#define ISERIALIZABLE_TEST
//#define OLD_COMMON_WITHOUT_DIAGNOSTIC_INFO

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Threading;
using System.Configuration;
//using System.Runtime.Remoting.Lifetime;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security.Permissions;
using IMR.Suite.Common.Code;
using System.Xml.Serialization;


namespace IMR.Suite.Common
{
    #region class Action
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [ProtoBuf.ProtoInclude(100, typeof(DataValue))]
    [ProtoBuf.ProtoInclude(101, typeof(DiagnosticInfo))]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
#endif
#if ISERIALIZABLE_TEST
    public class Action : ISerializable
#else
    public class Action
#endif
    {
        #region static Empty
        public static readonly Action Empty = new Action() { serialNbr = SN.Any };
        #endregion

        #region Members

        #region SerializationVersion
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(98, Name = "Version")]
        [Newtonsoft.Json.JsonProperty("Version")]
#endif
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion

        #region IdAction
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(1, Name = "IdAction")]
        [Newtonsoft.Json.JsonProperty("IdAction", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idAction = null;
        public long? IdAction
        {
            get { return idAction; }
            set { idAction = value; }
        }
        #endregion
        #region Type
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(2, Name = "Type")]
        [Newtonsoft.Json.JsonProperty("Type")]
#endif
        private Enums.ActionType type = Enums.ActionType.Unknown;
        public Enums.ActionType Type
        {
            get { return type; }
            set { type = value; }
        }
        #endregion
        #region Status
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(3, Name = "Status")]
        [Newtonsoft.Json.JsonProperty("Status")]
#endif
        private Enums.ActionStatus status = Enums.ActionStatus.Unknown;
        public Enums.ActionStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        #endregion

        #region //Step
        //private int step = 0;

        //public int Step
        //{
        //    get
        //    {
        //        return step;
        //    }
        //    set
        //    {
        //        step = value;
        //    }
        //}
        #endregion

        #region SerialNbr
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [Newtonsoft.Json.JsonProperty("SerialNbr")]
#endif
        private SN serialNbr = null;
        public SN SerialNbr
        {
            get { return serialNbr; }
        }

        #region SerialNbr for ProtoBuf
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(4, Name = "SerialNbr")]
#endif
        private ulong? ProtoBufSerialNbr;
        #endregion

        #endregion
        #region IdMeter
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(5, Name = "IdMeter")]
        [Newtonsoft.Json.JsonProperty("IdMeter", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        //[DataMember(Name = "SerialNbr", EmitDefaultValue = false)]
        private long? idMeter = null;
        public long? IdMeter
        {
            get { return idMeter; }
        }
        #endregion
        #region IdLocation
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(6, Name = "IdLocation")]
        [Newtonsoft.Json.JsonProperty("IdLocation", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idLocation = null;
        public long? IdLocation
        {
            get { return idLocation; }
        }
        #endregion

        #region IdActionData
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(7, Name = "IdActionData")]
        [Newtonsoft.Json.JsonProperty("IdActionData", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idActionData = null;
        public long? IdActionData
        {
            get { return idActionData; }
            set { idActionData = value; }

        }
        #endregion
        #region ActionData
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(8, Name = "ActionData")]
        [Newtonsoft.Json.JsonProperty("ActionData")]
#endif
        private DataValue[] actionData = null;
        public DataValue[] ActionData
        {
            get { return actionData; }
            set { actionData = value; }
        }
        #endregion

        #region IdActionParent
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(9, Name = "IdActionParent")]
        [Newtonsoft.Json.JsonProperty("IdActionParent", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idActionParent = null;
        public long? IdActionParent
        {
            get { return idActionParent; }
            set { idActionParent = value; }
        }
        #endregion
        #region IdDataArch
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(10, Name = "IdDataArch")]
        [Newtonsoft.Json.JsonProperty("IdDataArch", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idDataArch = null;
        public long? IdDataArch
        {
            get { return idDataArch; }
            set { idDataArch = value; }
        }
        #endregion

        #region MCostOut
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(11, Name = "MCostOut")]
        [Newtonsoft.Json.JsonProperty("MCostOut")]
#endif
        private int mCostOut = 20;
        public int MCostOut
        {
            get { return mCostOut; }
            set { mCostOut = value; }
        }
        #endregion
        #region MCostIn
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(12, Name = "MCostIn")]
        [Newtonsoft.Json.JsonProperty("MCostIn")]
#endif
        private int mCostIn = 20;
        public int MCostIn
        {
            get { return mCostIn; }
            set { mCostIn = value; }

        }
        #endregion
        #region SendTimeOut
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(13, Name = "SendTimeOut")]
        [Newtonsoft.Json.JsonProperty("SendTimeOut")]
#endif
        private TimeSpan sendTimeOut = TimeSpan.Zero;
        public TimeSpan SendTimeOut
        {
            get { return sendTimeOut; }
            set { sendTimeOut = value; }
        }
        #endregion
        #region SendIdConnection
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(14, Name = "SendIdConnection")]
        [Newtonsoft.Json.JsonProperty("SendIdConnection", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private uint? sendIdConnection = null;
        public uint? SendIdConnection
        {
            get { return sendIdConnection; }
            set { sendIdConnection = value; }
        }
        #endregion

        #region //ScheduleMode
        //#if !WindowsCE
        //        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        //#endif
        //        private Enums.ActionScheduleMode scheduleMode = Enums.ActionScheduleMode.Normal;
        //        public Enums.ActionScheduleMode ScheduleMode
        //        {
        //            get { return scheduleMode; }
        //            set { scheduleMode = value; }
        //        }
        #endregion
        #region //ExecuteTime
        //#if !WindowsCE
        //        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        //#endif
        //        private DateTime executeTime = DateTime.MinValue;
        //        public DateTime ExecuteTime
        //        {
        //            get { return executeTime; }
        //            set { executeTime = value; }
        //        }
        #endregion
        #region //WaitEndTime
        //#if !WindowsCE
        //        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        //#endif
        //        private DateTime waitEndTime = DateTime.MinValue;
        //        public DateTime WaitEndTime
        //        {
        //            get { return waitEndTime; }
        //            set { waitEndTime = value; }
        //        }
        #endregion
        #region //WaitObject
        //#if !WindowsCE
        //        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        //#endif
        //        private object waitObject = null;
        //        public object WaitObject
        //        {
        //            get { return waitObject; }
        //            set { waitObject = value; }
        //        }
        #endregion

        #region Invoker
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(15, Name = "Invoker")]
        [Newtonsoft.Json.JsonProperty("Invoker")]
#endif
        private Enums.Module invoker = Enums.Module.CORE_Actions;
        public Enums.Module Invoker
        {
            get { return invoker; }
            set { invoker = value; }
        }
        #endregion
        #region IdOperator
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(16, Name = "IdOperator")]
        [Newtonsoft.Json.JsonProperty("IdOperator", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private long? idOperator = null;
        public long? IdOperator
        {
            get { return idOperator; }
            set { idOperator = value; }
        }
        #endregion
        #region CreationDate
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(17, Name = "CreationDate")]
        [Newtonsoft.Json.JsonProperty("CreationDate")]
#endif
        private DateTime? creationDate = null;
        public DateTime? CreationDate
        {
            get { return creationDate; }
            set { creationDate = value; }
        }
        #endregion

        #region DiagnosticInfos
#if (!Android)
        [ProtoBuf.ProtoMember(18, Name = "DiagnosticInfos")]
        [Newtonsoft.Json.JsonProperty("DiagnosticInfos")]
#endif
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }
        #endregion

        #endregion

        #region Constructor
        public Action()
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
        }

        public Action(Enums.ActionType Type, SN SerialNbr, long? IdMeter, long? IdLocation)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            this.type = Type;
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
        }

        public Action(int IdActionType, SN SerialNbr, long? IdMeter, long? IdLocation)
            : this((Enums.ActionType)IdActionType, SerialNbr, IdMeter, IdLocation) { }

        public Action(int IdActionType, SN SerialNbr)
            : this((Enums.ActionType)IdActionType, SerialNbr, null, null) { }

        public Action(Enums.ActionType Type, SN SerialNbr)
            : this(Type, SerialNbr, null, null) { }

        public Action(Action action)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            //this.Guid = action.Guid;

            this.idAction = action.IdAction;
            this.type = action.Type;
            this.status = action.Status;

            this.serialNbr = action.SerialNbr;
            this.idMeter = action.IdMeter;
            this.idLocation = action.IdLocation;

            this.idActionData = action.IdActionData;
            this.actionData = action.ActionData;

            this.idActionParent = action.IdActionParent;
            this.idDataArch = action.IdDataArch;

            this.mCostIn = action.MCostIn;
            this.mCostOut = action.MCostOut;
            this.sendTimeOut = action.SendTimeOut;

            this.invoker = action.Invoker;
            this.idOperator = action.IdOperator;
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected Action(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected Action(SerializationInfo info, StreamingContext context, bool fromDerived)
        {
            DiagnosticInfos = new List<DiagnosticInfo>();

            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            if (serializationVersion == 0)
            {
                this.idAction = (long?)info.GetValue("idAction", typeof(long?));
                this.type = (Enums.ActionType)info.GetValue("type", typeof(int));
                this.status = (Enums.ActionStatus)info.GetValue("status", typeof(int));

                this.serialNbr = (SN)info.GetValue("serialNbr", typeof(SN));
                this.idMeter = (long?)info.GetValue("idMeter", typeof(long?));
                this.idLocation = (long?)info.GetValue("idLocation", typeof(long?));

                this.idActionData = (long?)info.GetValue("idActionData", typeof(long?));
                this.actionData = (DataValue[])info.GetValue("actionData", typeof(DataValue[]));

                this.idActionParent = (long?)info.GetValue("idActionParent", typeof(long?));
                this.idDataArch = (long?)info.GetValue("idDataArch", typeof(long?));

                this.mCostIn = (int)info.GetValue("mCostIn", typeof(int));
                this.mCostOut = (int)info.GetValue("mCostOut", typeof(int));
                this.sendTimeOut = (TimeSpan)info.GetValue("sendTimeOut", typeof(TimeSpan));
                this.sendIdConnection = (uint?)info.GetValue("sendIdConnection", typeof(uint?));

                this.invoker = (Enums.Module)info.GetValue("invoker", typeof(int));
                this.idOperator = (long?)info.GetValue("idOperator", typeof(long?));

                this.creationDate = (DateTime?)info.GetValue("creationDate", typeof(DateTime?));

                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
            }
            else if (serializationVersion == 1)
            {
                this.idAction = (long?)info.GetValue("idAction", typeof(long?));
                this.type = (Enums.ActionType)info.GetValue("type", typeof(int));
                this.status = (Enums.ActionStatus)info.GetValue("status", typeof(int));

                ulong? snDBValue = (ulong?)info.GetValue("serialNbr", typeof(ulong?));
                if (snDBValue != null)
                    this.serialNbr = SN.FromDBValue(snDBValue);
                this.idMeter = (long?)info.GetValue("idMeter", typeof(long?));
                this.idLocation = (long?)info.GetValue("idLocation", typeof(long?));

                this.idActionData = (long?)info.GetValue("idActionData", typeof(long?));
                this.actionData = (DataValue[])info.GetValue("actionData", typeof(DataValue[]));

                this.idActionParent = (long?)info.GetValue("idActionParent", typeof(long?));
                this.idDataArch = (long?)info.GetValue("idDataArch", typeof(long?));

                this.mCostIn = (int)info.GetValue("mCostIn", typeof(int));
                this.mCostOut = (int)info.GetValue("mCostOut", typeof(int));
                this.sendTimeOut = (TimeSpan)info.GetValue("sendTimeOut", typeof(TimeSpan));
                this.sendIdConnection = (uint?)info.GetValue("sendIdConnection", typeof(uint?));

                this.invoker = (Enums.Module)info.GetValue("invoker", typeof(int));
                this.idOperator = (long?)info.GetValue("idOperator", typeof(long?));

                this.creationDate = (DateTime?)info.GetValue("creationDate", typeof(DateTime?));

                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
                DiagnosticInfos = (List<DiagnosticInfo>)info.TryGetValue("DiagnosticInfos", typeof(List<DiagnosticInfo>), new List<DiagnosticInfo>());
            }
        }
#endif
        #endregion

        #region GetValuesDictionary
        public Dictionary<long, List<DataValue>> GetValuesDictionary()
        {
            Dictionary<long, List<DataValue>> result = new Dictionary<long, List<DataValue>>();

            // !!!TW!!! To samo mozna zrobic tez tak:
            //
            //foreach (long idDataType in (from p in actionData select p.Type.IdDataType).Distinct())
            //	result.Add(idDataType, (from d in actionData where d.Type.IdDataType == idDataType select d).ToList<DataValue>());
            //
            // lub tak:
            //
            //foreach (long idDataType in actionData.Select(p => p.Type.IdDataType).Distinct())
            //	result.Add(idDataType, actionData.Where(d => d.Type.IdDataType == idDataType).Select(d => d).ToList<DataValue>());

            long idDataType;
            List<DataValue> valueList = new List<DataValue>();

            foreach (DataValue actionValue in actionData)
            {
                idDataType = actionValue.Type.IdDataType;

                if (result.ContainsKey(idDataType))
                {
                    result[idDataType].Add(actionValue);
                }
                else
                {
                    valueList = new List<DataValue>();
                    valueList.Add(actionValue);
                    result.Add(idDataType, valueList);
                }
            }

            return result;
        }
        #endregion

        #region EncodeMessage
        public static Dictionary<string, object> EncodeMessage(Action action)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (action != null)
            {
                result["Type"] = (int)action.Type;
                if (action.SerialNbr != null)
                    result["SerialNbr"] = action.SerialNbr.DBValue;
                result["IdMeter"] = action.IdMeter;
                result["IdLocation"] = action.IdLocation;

                result["IdAction"] = action.IdAction;
                result["Status"] = (int)action.Status;

                result["IdActionData"] = action.IdActionData;
                if (action.ActionData != null)
                    result["ActionData"] = action.ActionData.Select(q => new Tuple<long, int, object>(q.IdDataType, q.Index, q.Value)).ToList();

                result["IdActionParent"] = action.IdActionParent;
                result["IdDataArch"] = action.IdDataArch;

                result["MCostOut"] = action.MCostOut;
                result["MCostIn"] = action.MCostIn;
                result["SendTimeOut"] = action.SendTimeOut.TotalSeconds;

                result["Invoker"] = (int)action.Invoker;
                result["IdOperator"] = action.IdOperator;
            }
            return result;
        }
        #endregion
        #region DecodeMessage
        public static Action DecodeMessage(object message)
        {
            if (message is Action)
                return (Action)message;
            else if (message.GetType() == typeof(Dictionary<string, object>))
            {
                Dictionary<string, object> values = (Dictionary<string, object>)message;
                object value = null;

                #region Type
                value = values.FirstOrDefault(w => w.Key == "Type").Value;
                int Type = value == null ? default(int) : Convert.ToInt32(value);
                #endregion
                #region SerialNbr
                value = values.FirstOrDefault(w => w.Key == "SerialNbr").Value;
                SN SerialNbr = SN.FromDBValue((value == null ? default(ulong?) : Convert.ToUInt64(value)));
                #endregion
                #region IdMeter
                value = values.FirstOrDefault(w => w.Key == "IdMeter").Value;
                long? IdMeter = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region IdLocation
                value = values.FirstOrDefault(w => w.Key == "IdLocation").Value;
                long? IdLocation = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion

                Action result = new Action(Type, SerialNbr, IdMeter, IdLocation);

                #region IdAction
                value = values.FirstOrDefault(w => w.Key == "IdAction").Value;
                result.IdAction = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region Status
                value = values.FirstOrDefault(w => w.Key == "Status").Value;
                result.Status = value == null ? Enums.ActionStatus.Unknown : (Enums.ActionStatus)Convert.ToInt32(value);
                #endregion

                #region IdActionData
                value = values.FirstOrDefault(w => w.Key == "IdActionData").Value;
                result.IdActionData = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region ActionData
                value = values.FirstOrDefault(w => w.Key == "ActionData").Value;
                if (value != null)
                {
                    if (value is List<Tuple<long, int, object>>)
                    {
                        List<Tuple<long, int, object>> actionDataValue = value as List<Tuple<long, int, object>>;
                        List<DataValue> actionData = new List<DataValue>();
                        actionDataValue.ForEach(tuple => actionData.Add(new DataValue(tuple.Item1, tuple.Item2, tuple.Item3)));
                        result.ActionData = actionData.ToArray();
                    }
                    else if (value is List<object[]>)
                    {
                        List<object[]> actionDataValue = value as List<object[]>;
                        List<DataValue> actionData = new List<DataValue>();
                        actionDataValue.Where(w => w.Length >= 3 && w[0] is long && w[1] is int).ToList().ForEach(tuple => actionData.Add(new DataValue((long)tuple[0], (int)tuple[1], tuple[2])));
                        result.ActionData = actionData.ToArray();
                    }
                }
                #endregion

                #region IdActionParent
                value = values.FirstOrDefault(w => w.Key == "IdActionParent").Value;
                result.IdActionParent = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region IdDataArch
                value = values.FirstOrDefault(w => w.Key == "IdDataArch").Value;
                result.IdDataArch = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion

                #region MCostOut
                value = values.FirstOrDefault(w => w.Key == "MCostOut").Value;
                if (value != null)
                    result.MCostOut = Convert.ToInt32(value);
                #endregion
                #region MCostIn
                value = values.FirstOrDefault(w => w.Key == "MCostIn").Value;
                if (value != null)
                    result.MCostIn = Convert.ToInt32(value);
                #endregion
                #region SendTimeOut
                value = values.FirstOrDefault(w => w.Key == "SendTimeOut").Value;
                if (value != null)
                    result.SendTimeOut = TimeSpan.FromSeconds(Convert.ToInt64(value));
                #endregion

                #region Invoker
                value = values.FirstOrDefault(w => w.Key == "Invoker").Value;
                result.Invoker = value == null ? Enums.Module.Unknown : (Enums.Module)Convert.ToInt64(value);
                #endregion
                #region IdOperator
                value = values.FirstOrDefault(w => w.Key == "IdOperator").Value;
                result.IdOperator = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion

                return result;
            }
            return null;
        }
        #endregion
        #region SqlTypeToSystem
        static object SqlTypeToSystem(object value)
        {
            if (value == null || value is DBNull)
                return null;
            else if (value.GetType().Name.IndexOf("Sql") == 0)
                if ((bool)value.GetType().GetProperty("IsNull").GetValue(value, null))
                    return null;
                else
                    return value.GetType().GetProperty("Value").GetValue(value, null);
            else
                return value;
        }
        #endregion

        #region Guid
#if (!Android)
        [ProtoBuf.ProtoMember(99, Name = "Guid")]
        [Newtonsoft.Json.JsonProperty("Guid")]
#endif
        public readonly Guid Guid = Guid.NewGuid();

        public static bool operator ==(Action left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(Action left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Action)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region override ToString()
        public override string ToString()
        {
            return string.Format("Id:{0} Type:{1} Status:{2} SN:{3} IdLocation:{4} IdMeter:{5}",
                IdAction, Type, Status, SerialNbr, IdLocation, IdMeter
                );
        }
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("idAction", idAction, typeof(long?));
            info.AddValue("type", type, typeof(int));
            info.AddValue("status", status, typeof(int));
            info.AddValue("serialNbr", serialNbr != null ? serialNbr.DBValue : null, typeof(ulong?));
            info.AddValue("idMeter", idMeter, typeof(long?));
            info.AddValue("idLocation", idLocation, typeof(long?));
            info.AddValue("idActionData", idActionData, typeof(long?));
            info.AddValue("actionData", actionData, typeof(DataValue[]));
            info.AddValue("idActionParent", idActionParent, typeof(long?));
            info.AddValue("idDataArch", idDataArch, typeof(long?));
            info.AddValue("mCostIn", mCostIn, typeof(int));
            info.AddValue("mCostOut", mCostOut, typeof(int));
            info.AddValue("sendTimeOut", sendTimeOut, typeof(TimeSpan));
            info.AddValue("sendIdConnection", sendIdConnection, typeof(uint?));
            info.AddValue("invoker", invoker, typeof(int));
            info.AddValue("idOperator", idOperator, typeof(long?));
            info.AddValue("creationDate", creationDate, typeof(DateTime?));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
            info.AddValue("DiagnosticInfos", DiagnosticInfos, typeof(List<DiagnosticInfo>));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion
    }
    #endregion
    #region class EventAction
    [Serializable]
    public class EventAction : Action
    {
        #region Constructor
        public EventAction(SN SerialNbr, DataMeasureValue[] Values)
            : base(Enums.ActionType.EventData, SerialNbr)
        {
            ActionData = Values;
        }
#if ISERIALIZABLE_TEST
        public EventAction(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
#endif
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
#endif
        #endregion
    }
    #endregion
    #region class ScheduledAction
    [Serializable]
    public class ScheduledAction : Action
    {
        #region Members
        #region IdActionDef
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private long? idActionDef = null;
        public long? IdActionDef
        {
            get { return idActionDef; }
            set { idActionDef = value; }
        }
        #endregion
        #region IdOperatorSch
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private int? idOperatorSch = null;
        public int? IdOperatorSch
        {
            get { return idOperatorSch; }
            set { idOperatorSch = value; }
        }
        #endregion
        #region IdProfile
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private int? idProfile = null;
        public int? IdProfile
        {
            get { return idProfile; }
            set { idProfile = value; }
        }
        #endregion

        #endregion

        #region Constructor
        public ScheduledAction(int IdActionType, SN SerialNbr, long? IdMeter, long? IdLocation)
            : base(IdActionType, SerialNbr, IdMeter, IdLocation)
        {

        }
#if ISERIALIZABLE_TEST
        public ScheduledAction(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        public ScheduledAction(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, fromDerived)
        {
            if (fromDerived == false || serializationVersion > 0)
            {
                idActionDef = (long?)info.GetValue("idActionDef", typeof(long?));
                idOperatorSch = (int?)info.GetValue("idOperatorSch", typeof(int?));
                idProfile = (int?)info.GetValue("idProfile", typeof(int?));
            }
            else
            {
                idActionDef = (long?)info.GetValue("ScheduledAction+idActionDef", typeof(long?));
                idOperatorSch = (int?)info.GetValue("ScheduledAction+idOperatorSch", typeof(int?));
                idProfile = (int?)info.GetValue("ScheduledAction+idProfile", typeof(int?));
            }

            /*
                        if (serializationVersion == 0)
                        {
                            if (fromDerived == false)
                            {
                            }
                            else
                            {
                            }
                        }
                        else if (serializationVersion == 1)
                        {
                        }
            */


        }
#endif

        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("idActionDef", idActionDef, typeof(long?));
            info.AddValue("idOperatorSch", idOperatorSch, typeof(int?));
            info.AddValue("idProfile", idProfile, typeof(int?));
        }
#endif
        #endregion
    }
    #endregion

    #region class DiagnosticInfo
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
#endif

#if ISERIALIZABLE_TEST
    public class DiagnosticInfo : ISerializable
#else
    public class DiagnosticInfo
#endif
    {
        #region Members
        #region IdHistoryDescr
#if (!Android)
        [ProtoBuf.ProtoMember(102, Name = "IdHistoryDescr")]
        [Newtonsoft.Json.JsonProperty("IdHistoryDescr")]
#endif
        private long idHistoryDescr;
        public long IdHistoryDescr
        {
            get { return idHistoryDescr; }
        }
        #endregion
        #region HistoryDescrArgs
#if (!Android)
        [Newtonsoft.Json.JsonProperty("HistoryDescrArgs")]
#endif
        private object[] historyDescrArgs = new object[0];
        public object[] HistoryDescrArgs
        {
            get { return historyDescrArgs; }
        }

        #region HistoryDescrArgs for ProtoBuf
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(103, Name = "HistoryDescrArgs")]
#endif
        private List<Tuple<string, string>> ProtoBufHistoryDescrArgs = null;
        #endregion
        #endregion
        #region Timestamp
#if (!Android)
        [ProtoBuf.ProtoMember(105, Name = "Timestamp")]
        [Newtonsoft.Json.JsonProperty("Timestamp")]
#endif
        private DateTime timestamp;
        public DateTime Timestamp
        {
            get { return timestamp; }
        }
        #endregion
        #region Module
#if (!Android)
        [ProtoBuf.ProtoMember(106, Name = "Module")]
        [Newtonsoft.Json.JsonProperty("Module")]
#endif
        private Enums.Module module;
        public Enums.Module Module
        {
            get { return module; }
        }
        #endregion
        #region Assembly

#if (!Android)
        [ProtoBuf.ProtoMember(107, Name = "Assembly")]
        [Newtonsoft.Json.JsonProperty("Assembly")]
#endif
        private string assembly;
        public string Assembly
        {
            get { return assembly; }
        }
        #endregion
        #region Description
#if (!Android)
        [ProtoBuf.ProtoMember(108, Name = "Description")]
        [Newtonsoft.Json.JsonProperty("Description")]
#endif
        private string description = String.Empty;
        public string Description
        {
            get { return description; }
        }
        #endregion
        #region ExtensionError
#if (!Android)
        [Newtonsoft.Json.JsonProperty("ExtensionError")]
#endif
        private object extensionError = null;
        public object ExtensionError
        {
            get { return extensionError; }
        }
        #region ExtensionError for ProtoBuf
        [XmlIgnore]
        [NonSerialized]
#if !(Android)
        [ProtoBuf.ProtoMember(109, Name = "ExtensionErrorType")]
#endif
        private string ProtoBufExtensionErrorType;
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(110, Name = "ExtensionError")]
#endif
        private string ProtoBufExtensionErrorStr;
        #endregion
        #endregion
        #region SentToLog
#if (!Android)
        [ProtoBuf.ProtoMember(111, Name = "SentToLog")]
        [Newtonsoft.Json.JsonProperty("SentToLog")]
#endif
        private bool sentToLog;
        public bool SentToLog
        {
            get { return sentToLog; }
            set { sentToLog = value; }
        }
        #endregion

        #endregion

        #region Constructor
        protected DiagnosticInfo() { }
#if OLD_COMMON_WITHOUT_DIAGNOSTIC_INFO
        [Obsolete]
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, Enums.Module module, string assembly)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, module, assembly, null, null) { }
#endif
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, null, null, null) { }
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string assembly)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, assembly, null, null) { }
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string assembly, string description)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, assembly, description, null) { }
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string assembly, string description, object extensionError)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, assembly, description, extensionError) { }
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, Enums.Module module, string assembly, string description, object extensionError)
            : this(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, module, assembly, description, extensionError) { }
        public DiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, DateTime timestamp, Enums.Module module, string assembly, string description, object extensionError)
        {
            this.idHistoryDescr = idHistoryDescr;
            this.historyDescrArgs = historyDescrArgs;
            this.timestamp = timestamp;
            this.module = module;
            this.assembly = assembly;
            this.description = description;
            this.extensionError = extensionError;
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DiagnosticInfo(SerializationInfo info, StreamingContext context)
        {
            idHistoryDescr = (long)info.GetValue("idHistoryDescr", typeof(long));
            historyDescrArgs = (object[])info.GetValue("historyDescrArgs", typeof(object[]));
            timestamp = (DateTime)info.GetValue("timestamp", typeof(DateTime));
            module = (Enums.Module)info.GetValue("module", typeof(int));
            assembly = (string)info.GetValue("assembly", typeof(string));
            description = (string)info.GetValue("description", typeof(string));
            extensionError = (object)info.GetValue("extensionError", typeof(object));
        }
#endif
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is DiagnosticInfo)
            {
                DiagnosticInfo other = obj as DiagnosticInfo;
                return this.idHistoryDescr == other.idHistoryDescr && timestamp.Equals(other.timestamp) && module == other.module && string.Equals(assembly, other.assembly);
            }
            else
                return base.Equals(obj);
        }
        public static bool operator ==(DiagnosticInfo left, DiagnosticInfo right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(DiagnosticInfo left, DiagnosticInfo right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        public override int GetHashCode()
        {
            int hashCode = idHistoryDescr.GetHashCode();
            hashCode = hashCode ^ timestamp.GetHashCode();
            hashCode = hashCode ^ (int)module;
            hashCode = hashCode ^ (assembly != null ? assembly.GetHashCode() : 0);
            return hashCode;
        }
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("idHistoryDescr", idHistoryDescr, typeof(long));
            info.AddValue("historyDescrArgs", historyDescrArgs, typeof(object[]));
            info.AddValue("timestamp", timestamp, typeof(DateTime));
            info.AddValue("module", (int)module, typeof(int));
            info.AddValue("assembly", assembly, typeof(string));
            info.AddValue("description", description, typeof(string));
            info.AddValue("extensionError", extensionError, typeof(object));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion
        #region UpdateModule
        public void UpdateModule(Enums.Module module)
        {
            this.module = module;
        }
        #endregion
        #region UpdateAssembly
        public void UpdateAssembly(string assemblyName)
        {
            this.assembly = assemblyName;
        }
        #endregion

        #region Prototbuf method for serialization
        #region OnProtobufBeforeSerialization
#if (!Android)
        [ProtoBuf.ProtoBeforeSerialization]
#endif
        private void OnProtobufBeforeSerialization()
        {
            if (ProtoBufHistoryDescrArgs == null) // nie wiem czemu, ale czasem, przy Deserializaci wchodzi tutaj !!! Mo�e b�ad w protobuf-net?
            {
                if (historyDescrArgs == null || historyDescrArgs.Length == 0)
                {
                    ProtoBufHistoryDescrArgs = null;
                }
                else
                {
                    ProtoBufHistoryDescrArgs = new List<Tuple<string, string>>();
                    foreach (object obj in historyDescrArgs)
                        ProtoBufHistoryDescrArgs.Add(Tuple.Create<string, string>(obj.GetType().FullName, obj.ToString()));
                }
            }

            if (string.IsNullOrEmpty(ProtoBufExtensionErrorStr)) // nie wiem czemu, ale czasem, przy Deserializaci wchodzi tutaj !!! Mo�e b�ad w protobuf-net?
            {
                if (extensionError == null)
                {
                    ProtoBufExtensionErrorStr = "null";
                }
                else
                {
                    ProtoBufExtensionErrorType = extensionError.GetType().FullName;
                    ProtoBufExtensionErrorStr = extensionError.ToString();
                }
            }
        }
        #endregion
        #region OnProrobufAfterDeserialization
#if (!Android)
        [ProtoBuf.ProtoAfterDeserialization]
#endif
        private void OnProrobufAfterDeserialization()
        {
            historyDescrArgs = new object[0];
            if (ProtoBufHistoryDescrArgs != null)
            {
                List<object> listHistoryDescrArg = new List<object>();
                foreach (Tuple<string, string> tuple in ProtoBufHistoryDescrArgs)
                {
                    Type typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.FullName == tuple.Item1);
                    if (typeFromProtobuf == null)
                        typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.Name == tuple.Item1);
                    if (typeFromProtobuf != null)
                        listHistoryDescrArg.Add(tuple.Item2.Parse(typeFromProtobuf));
                }
                historyDescrArgs = listHistoryDescrArg.ToArray();
            }

            extensionError = null;
            if (!string.IsNullOrEmpty(ProtoBufExtensionErrorStr) && ProtoBufExtensionErrorStr.ToLower() != "null")
            {
                Type typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.FullName == ProtoBufExtensionErrorType);
                if (typeFromProtobuf == null)
                    typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.Name == ProtoBufExtensionErrorType);
                if (typeFromProtobuf != null)
                    extensionError = ProtoBufExtensionErrorStr.Parse(typeFromProtobuf);
            }
        }
        #endregion
        #endregion

        #region static GetDiagnosticInfo
        public static DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, null, null, null);
        }
        public static DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, null, description, null);
        }
        public static DiagnosticInfo GetDiagnosticInfo(long idHistoryDescr, object[] historyDescrArgs, string description, object extensionError)
        {
            return new DiagnosticInfo(idHistoryDescr, historyDescrArgs, DateTime.UtcNow, Enums.Module.Unknown, null, description, extensionError);
        }
        #endregion

    }
    #endregion
    #region class ListDiagnosticInfoExtension
    public static class ListDiagnosticInfoExtension
    {
        #region (extension) List<DiagnosticInfo>.UpdateNewItemsModule(Enums.Module module)
        public static void UpdateNewItemsModule(this List<DiagnosticInfo> list, Enums.Module module, string moduleAssemblyName)
        {
            list.Where(d => d.Module == Enums.Module.Unknown).ToList().ForEach(d =>
                                                                               {
                                                                                   d.UpdateModule(module);
                                                                                   if (String.IsNullOrEmpty(d.Assembly))
                                                                                       d.UpdateAssembly(moduleAssemblyName);
                                                                               });
        }
        #endregion
    }
    #endregion


    #region NotifyAction
    [Serializable]
    public class NotifyAction
    {
        #region NotifyType
        public enum NotifyType
        {
            Action,
            ChangeInTable,
            SysMessage,
        }
        #endregion
        public NotifyType Type;
        public object Notify;

        #region Constructor
        public NotifyAction(Action action)
        {
            Type = NotifyType.Action;
            Notify = action;
        }
        public NotifyAction(ChangeInTable changeInTable)
        {
            Type = NotifyType.ChangeInTable;
            Notify = changeInTable;
        }
        public NotifyAction(ChangeInTable[] changesInTable)
        {
            Type = NotifyType.ChangeInTable;
            Notify = changesInTable;
        }
        public NotifyAction(SysMessage systemMessage)
        {
            Type = NotifyType.SysMessage;
            Notify = systemMessage;
        }
        #endregion
    }
    #endregion

    #region IActionPlugIn
    public interface IActionPlugIn
    {
        Action Action { get; set; }

        bool Init(params object[] args);
        void Clear();
        bool Execute();
        void Notify(NotifyAction notifyAction);

        object ProcessCommand(Enums.ModuleCommandType moduleCommandType, params object[] args);
    }
    #endregion

    #region ActionPlugInWrapper
#if !WindowsCE && !Android
    [Serializable]
    public class ActionPlugInWrapper : MarshalByRefObjectDisposable
    {
        #region Members
        public AssemblyName ActionPluginName = null;    // Jest to jednocze�nie flaga m�wi�ca czy DLLka jest ju� za�adowana do pami�ci (==null) => nie, trzeba za�adowa�)
        public Type ActionPluginType = default(Type);
        public string ActionPluginPath = string.Empty;
        public static long InstanceCount = 0;
        #endregion

        #region Events
        public event EventHandler<EventArgs<string>> ActionPluginLoadedEvent;
        #endregion

        #region Constructor
        public ActionPlugInWrapper()
        {
        }
        public ActionPlugInWrapper(string plugInPath)
        {
            ActionPluginPath = plugInPath;

            if (!System.IO.Path.IsPathRooted(ActionPluginPath))
                ActionPluginPath = AppDomain.CurrentDomain.BaseDirectory + ActionPluginPath;
            ActionPluginPath = Path.GetFullPath(ActionPluginPath);

            #region Je�li �cie�ka jest poza (not relative to base) �cie�k� bazow� EXE to kopiujemy do Cache (relative to base)
            string appPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
            string dllPath = Path.GetDirectoryName(ActionPluginPath);
            if (dllPath != null && dllPath.IndexOf(appPath, StringComparison.InvariantCultureIgnoreCase) == -1)
            {
                string dllDirectoryName = Path.Combine(appPath, "Cache");
                DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
                if (!di.Exists)
                {
                    di.Create();
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }

                string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(ActionPluginPath);
                try
                {
                    File.Copy(ActionPluginPath, newAssemblyPath, true);
                }
                catch
                {
                    // ignored
                }
                ActionPluginPath = newAssemblyPath;
            }
            #endregion
        }
        #endregion
        #region Destructor
        ~ActionPlugInWrapper()
        {
            Dispose(false);
        }
        #endregion

        #region Unload
        public void Unload(bool forceDispose)
        {
            try
            {
                ActionPluginName = null;
                ActionPluginType = null;
                GC.Collect();
            }
            catch (Exception)
            {
                // ignored
            }
            base.Dispose();
        }
        #endregion

        #region CreateInstance
        public IActionPlugIn CreateInstance(object[] PlugInConstructorArgs)
        {
            if (ActionPluginName == null) // jeszcze nie za�adowana lub zosta�a od�adowana
            {
                Assembly assembly = null;

                assembly = AssemblyWrapper.LoadAssembly(ActionPluginPath);

                if (assembly == null)
                    return null;

                ActionPluginType = GetDerivedType(assembly.GetTypes(), typeof(IActionPlugIn));
                if (ActionPluginType == default(Type))
                    return null;

                ActionPluginName = assembly.GetName();

                // poni�szy IF mozna zast�pi� tym (jak b�dzie VS2015 lub wy�szy): ActionPluginLoadedEvent?.Invoke(this, new EventArgs<string>(Path.GetFileName(ActionPluginPath)));
                if (ActionPluginLoadedEvent != null)
                    ActionPluginLoadedEvent.Invoke(this, new EventArgs<string>(Path.GetFileNameWithoutExtension(ActionPluginPath)));
            }

            IActionPlugIn plugIn = (IActionPlugIn)Activator.CreateInstance(ActionPluginType, PlugInConstructorArgs);

            if (plugIn == null)
                return null;

            //plugIn.NameAndVersion = string.Format("{0}, {1}", ActionPluginName.Name, ActionPluginName.Version.ToString());
            Interlocked.Increment(ref InstanceCount);
            return plugIn;
        }
        #endregion
        #region FreeInstance
        public void FreeInstance(IActionPlugIn plugIn)
        {
            if (plugIn != null)
            {
                try
                {
                    Interlocked.Decrement(ref InstanceCount);
                }
                catch
                {
                    // ignored
                }
                finally
                {
                    plugIn = default(IActionPlugIn);
                }

            }
        }
        #endregion

        #region private GetDerivedType
        private Type GetDerivedType(Type[] assemblyTypes, Type pluginType)
        {
            foreach (Type typeInAssembly in assemblyTypes)
            {
                if (typeInAssembly.IsClass && typeInAssembly.IsPublic)
                {
                    if (typeInAssembly.GetInterface(pluginType.FullName, true) != null)
                        return typeInAssembly;
                }
            }
            return default(Type);
        }
        #endregion

        #region MarshalByRefObjectDisposable overrides
        public override object InitializeLifetimeService()
        {
            return null;
        }
        #endregion
    }
#endif
    #endregion
}
