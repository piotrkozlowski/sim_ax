﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Reflection;

namespace IMR.Suite.Common
{
    #region DEVICE
    [Serializable]
    public class DEVICE
    {
        public SN SerialNbr;
        public Enums.DeviceType IdDeviceType;
        public SN SerialNbrPattern;
        public long? IdDescrPattern;
        public uint IdOrderNbr;
        public uint IdDistributor;
        public uint StateType;
        public uint? IdSIMCard;
    }
    #endregion
    #region DEVICE_ORDER_NUMBER
    [Serializable]
    public class DEVICE_ORDER_NUMBER
    {
        public uint IdOrderNbr;
        public string Name;
        public string FirstQuarter;
        public string SecondQuarter;
        public string ThirdQuarter;
    }
    #endregion
    #region DEVICE_TYPE
    [Serializable]
    public class DEVICE_TYPE
    {
        public Enums.DeviceType IdDeviceType;
        public string Name;
        public bool TwoWayTransAvailable;
        public uint? IdDeviceDriver;
        public Enums.Protocol IdProtocol;
        public Enums.DeviceTypeClass? IdDeviceTypeClass;
    }
    #endregion
    #region DEVICE_TYPE_IN_GROUP
    [Serializable]
    public class DEVICE_TYPE_IN_GROUP
    {
        public Enums.DeviceType IdDeviceType;
        public Enums.DeviceTypeGroup IdDeviceTypeGroup;
    }
    #endregion
    #region DEVICE_HIERARCHY
    [Serializable]
    public class DEVICE_HIERARCHY
    {
        public ulong IdDeviceHierarchy;
        public SN SerialNbrParent;
        public SN SerialNbr;
        public Enums.SlotType SlotType;
        public int SlotNbr;
        public Enums.Protocol ProtocolIn;
        public Enums.Protocol ProtocolOut;
        public bool IsParentActive;

        #region Clone
        public DEVICE_HIERARCHY Clone()
        {
            DEVICE_HIERARCHY dh = new DEVICE_HIERARCHY();
            dh.IdDeviceHierarchy = this.IdDeviceHierarchy;
            dh.SerialNbrParent = this.SerialNbrParent;
            dh.SerialNbr = this.SerialNbr;
            dh.SlotType = this.SlotType;
            dh.SlotNbr = this.SlotNbr;
            dh.ProtocolIn = this.ProtocolIn;
            dh.ProtocolOut = this.ProtocolOut;
            dh.IsParentActive = this.IsParentActive;
            return dh;
        }
        #endregion
    }
    #endregion
    #region DEVICE_DRIVER
    [Serializable]
    public class DEVICE_DRIVER
    {
        public uint IdDeviceDriver;
        public string Name;
        public string PlugInName;
        public bool AutoUpdate;
    }
    #endregion

    #region PROTOCOL_DRIVER
    [Serializable]
    public class PROTOCOL_DRIVER
    {
        public uint IdProtocolDriver;
        public string Name;
        public string PlugInName;
    }
    #endregion
    #region PROTOCOL
    [Serializable]
    public class PROTOCOL
    {
        public Enums.Protocol IdProtocol;
        public string Name;
        public uint? IdProtocolDriver;
    }
    #endregion
}
