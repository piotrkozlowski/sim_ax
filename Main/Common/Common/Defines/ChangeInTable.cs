﻿using System;
using System.Linq;
using System.Collections.Generic;
#if !Android
using System.Data.SqlTypes;
#endif
using System.Runtime.Serialization;

namespace IMR.Suite.Common
{
    #region ChangeType - shoud be the same like in TableChange.cs i SqlExt project
    public enum ChangeType : byte
    {
        Unknown = 0,
        ChangeInDictTable = 1,
        ChangeInDataTable = 2,
        ChangeInObjectDataTable = 3,
    }
    #endregion
    #region ChangeAction - shoud be the same like in TableChange.cs i SqlExt project
    public enum ChangeAction : byte
    {
        Unknown = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
    }
    #endregion

    #region class ChangeInTable
    [Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class ChangeInTable
    {
        #region static Empty
        public static readonly ChangeInTable Empty = new ChangeInTable(ChangeType.Unknown, "");
        #endregion

        #region TableName
#if !WindowsCE && !Android
		[DataMember]
#endif
        private string tableName = "UNKNOWN";
		public string TableName
        {
            get
            {
                return tableName;
            }
	     }
        #endregion
        #region ChangeType
#if !WindowsCE && !Android
        [DataMember]
#endif
        private ChangeType changeType = ChangeType.Unknown;
        public ChangeType ChangeType
        {
            get
            {
                return changeType;
            }
        }
        #endregion
        #region ChangeAction
#if !WindowsCE && !Android
        [DataMember]
#endif
        private ChangeAction changeAction = ChangeAction.Unknown;
        public ChangeAction ChangeAction
        {
            get
            {
                return changeAction;
            }
        }
        #endregion
        #region BatchId
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected long batchId = 0;
        public long BatchId
        {
            get
            {
                return batchId;
            }
        }
        #endregion
        #region Columns
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected List<Tuple<string, object>> columns = new List<Tuple<string, object>>();
        public List<Tuple<string, object>> Columns
        {
            get
            {
                return columns;
            }
        }

        #endregion
        #region ColumnName
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected string columnName = "UNKNOWN";
        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }
        #endregion
        #region OldValue
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected object oldValue = null;
        public object OldValue
        {
            get
            {
                return oldValue;
            }
        }
        #endregion
        #region NewValue
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected object newValue = null;
        public object NewValue
        {
            get
            {
                return newValue;
            }
        }
        #endregion
        #region Source
#if !WindowsCE && !Android
        [DataMember]
#endif
        protected Enums.Module source = Enums.Module.Unknown;
        public Enums.Module Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value; 
            }
        }
        #endregion

        #region Constructor
        public ChangeInTable(ChangeType changeType, string tableName)
        {
            this.changeType = changeType;
            this.tableName = tableName;
        }
        public ChangeInTable(ChangeType changeType, ChangeAction changeAction, string tableName, long batchId, List<Tuple<string,object>> columns, string columnName, object oldValue, object newValue)
        {
            this.changeType = changeType;
            this.changeAction = changeAction;
            this.tableName = tableName;
            this.batchId = batchId;
            this.columns = columns;
            this.columnName = columnName;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }
        #endregion

        #region DecodeMessage
        public static ChangeInTable[] DecodeMessage(object message)
        {
			if (message is ChangeInTable)
				return new ChangeInTable[] { (ChangeInTable)message };
            else if (message.GetType().IsArray && message.GetType().HasElementType == true)
            {
                if (message.GetType().GetElementType() == typeof(ChangeInTable))
                    return (ChangeInTable[])message;
                else if (message.GetType().GetElementType() == typeof(Dictionary<string, object>))
                {
                    Dictionary<string, object>[] msgs = (Dictionary<string, object>[])message;
                    List<ChangeInTable> result = new List<ChangeInTable>();
                    foreach (Dictionary<string, object> msg in msgs)
                    {
                        string tableName = (from item in msg where item.Key == "TableName" select Convert.ToString(item.Value)).FirstOrDefault();
                        byte changeType = (from item in msg where item.Key == "ChangeType" select Convert.ToByte(item.Value)).FirstOrDefault();
                        long idDataType = 0;
                        object value = (from item in msg where item.Key == "Value" select item.Value).FirstOrDefault();
                        if (value != null)
                            value = SqlTypeToSystem(value);
                        switch ((ChangeType)changeType)
                        {
                            case ChangeType.ChangeInDictTable:
                                result.Add(new ChangeInDictTable(tableName));
                                break;
                            case ChangeType.ChangeInDataTable:
                                long? SerialNbr = (from item in msg where item.Key == "SerialNbr" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                long? IdMeter = (from item in msg where item.Key == "IdMeter" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                long? IdLocation = (from item in msg where item.Key == "IdLocation" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                idDataType = (from item in msg where item.Key == "IdDataType" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                DataCurrentValue changedDataValue = new DataCurrentValue(SN.FromDBValue((ulong?)SerialNbr), IdMeter, IdLocation, idDataType, value);
                                result.Add(new ChangeInDataTable(changedDataValue));
                                break;
                            case ChangeType.ChangeInObjectDataTable:
                                long? IdObject = (from item in msg where item.Key == "IdObject" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                idDataType = (from item in msg where item.Key == "IdDataType" select Convert.ToInt64(item.Value)).FirstOrDefault();
                                DataObjectValue changedDataObjectValue = new DataObjectValue(IdObject, idDataType, value);
                                result.Add(new ChangeInObjectDataTable(tableName, changedDataObjectValue));
                                break;
                            default:
                                break;
                        }
                    }
                    return result.ToArray();
                }
                else  if (message.GetType().GetElementType() == typeof(Tuple<string, object>[]))
                {
                    Tuple<string, object>[][] msgs = (Tuple<string, object>[][])message;
                    List<ChangeInTable> result = new List<ChangeInTable>();

                    foreach (Tuple<string, object>[] msg in msgs)
                    {
                        string tableName = string.Empty, columnName = string.Empty;
                        byte changeType = 0, changeAction = 0;
                        int index = 0;
                        long idData = 0, idDataType = 0, batchId = 0;
                        long? serialNbr = null, idLocation = null, idMeter = null, idObject = null;
                        object value = null, oldValue = null, newValue = null;
                        List<Tuple<string,object>> columns = new List<Tuple<string,object>>();

                        msg.AsParallel().ForAll(v =>
                        //foreach (Tuple<string, object> v in msg)
                        {
                            switch (v.Item1)
                            {
                                case "TableName": tableName = Convert.ToString(v.Item2); break;
                                case "ChangeType": changeType = Convert.ToByte(v.Item2); break;
                                case "ChangeAction": changeAction = Convert.ToByte(v.Item2); break;
                                case "IdData": idData = Convert.ToInt64(v.Item2); break;
                                case "SerialNbr": serialNbr = Convert.ToInt64(v.Item2); break;
                                case "IdLocation": idLocation = Convert.ToInt64(v.Item2); break;
                                case "IdMeter": idMeter = Convert.ToInt64(v.Item2); break;
                                case "IdDataType": idDataType = Convert.ToInt64(v.Item2); break;
                                case "Index": index = Convert.ToInt16(v.Item2); break;
                                case "Value": value = v.Item2 != null ? SqlTypeToSystem(v.Item2) : null; break;
                                case "IdObject": idObject = Convert.ToInt64(v.Item2); break;
                                case "BatchId": batchId = Convert.ToInt64(v.Item2); break;
                                case "Columns": columns = (List<Tuple<string, object>>)v.Item2; break;
                                case "ColumnName": columnName = Convert.ToString(v.Item2); break;
                                case "OldValue": oldValue = v.Item2 != null ? SqlTypeToSystem(v.Item2) : null; break;
                                case "NewValue": newValue = v.Item2 != null ? SqlTypeToSystem(v.Item2) : null; break;
                            }
                        });

                        switch ((ChangeType)changeType)
                        {
                            case ChangeType.ChangeInDictTable:
                                result.Add(new ChangeInDictTable((ChangeAction)changeAction, tableName, batchId, columns, columnName, oldValue, newValue));
                                break;
                            case ChangeType.ChangeInDataTable:
                                DataCurrentValue changedDataValue = new DataCurrentValue(idData, SN.FromDBValue((ulong?)serialNbr), idMeter, idLocation, idDataType, index, value);
                                result.Add(new ChangeInDataTable((ChangeAction)changeAction, batchId, columns, columnName, oldValue, newValue, changedDataValue));
                                break;
                            case ChangeType.ChangeInObjectDataTable:
                                DataObjectValue changedDataObjectValue = new DataObjectValue(idObject, idDataType, value);
                                result.Add(new ChangeInObjectDataTable((ChangeAction)changeAction, tableName, batchId, columns, columnName, oldValue, newValue, changedDataObjectValue));
                                break;
                            default:
                                break;
                        }
                    }
                    return result.ToArray();
                }
                else if (message.GetType().GetElementType() == typeof(KeyValuePair<string, object>[]))
                {
                    KeyValuePair<string, object>[][] msgs = (KeyValuePair<string, object>[][])message;
                    List<ChangeInTable> result = new List<ChangeInTable>();

                    foreach (KeyValuePair<string, object>[] msg in msgs)
                    {
                        string tableName = string.Empty, columnName = string.Empty;
                        byte changeType = 0, changeAction = 0;
                        int index = 0;
                        long idData = 0, idDataType = 0, batchId = 0;
                        long? serialNbr = null, idLocation = null, idMeter = null, idObject = null;
                        object value = null, oldValue = null, newValue = null;
                        List<Tuple<string, object>> columns = new List<Tuple<string, object>>();

                        msg.AsParallel().ForAll(v =>
                        //foreach (Tuple<string, object> v in msg)
                        {
                            switch (v.Key)
                            {
                                case "TableName": tableName = Convert.ToString(v.Value); break;
                                case "ChangeType": changeType = Convert.ToByte(v.Value); break;
                                case "ChangeAction": changeAction = Convert.ToByte(v.Value); break;
                                case "IdData": idData = Convert.ToInt64(v.Value); break;
                                case "SerialNbr": serialNbr = Convert.ToInt64(v.Value); break;
                                case "IdLocation": idLocation = Convert.ToInt64(v.Value); break;
                                case "IdMeter": idMeter = Convert.ToInt64(v.Value); break;
                                case "IdDataType": idDataType = Convert.ToInt64(v.Value); break;
                                case "Index": index = Convert.ToInt16(v.Value); break;
                                case "Value": value = v.Value != null ? SqlTypeToSystem(v.Value) : null; break;
                                case "IdObject": idObject = Convert.ToInt64(v.Value); break;
                                case "BatchId": batchId = Convert.ToInt64(v.Value); break;
                                case "Columns": columns = ((List<KeyValuePair<string, object>>)v.Value).Select(s => Tuple.Create<string, object>(s.Key, s.Value)).ToList(); break;
                                case "ColumnName": columnName = Convert.ToString(v.Value); break;
                                case "OldValue": oldValue = v.Value != null ? SqlTypeToSystem(v.Value) : null; break;
                                case "NewValue": newValue = v.Value != null ? SqlTypeToSystem(v.Value) : null; break;
                            }
                        });

                        switch ((ChangeType)changeType)
                        {
                            case ChangeType.ChangeInDictTable:
                                result.Add(new ChangeInDictTable((ChangeAction)changeAction, tableName, batchId, columns, columnName, oldValue, newValue));
                                break;
                            case ChangeType.ChangeInDataTable:
                                DataCurrentValue changedDataValue = new DataCurrentValue(idData, SN.FromDBValue((ulong?)serialNbr), idMeter, idLocation, idDataType, index, value);
                                result.Add(new ChangeInDataTable((ChangeAction)changeAction, batchId, columns, columnName, oldValue, newValue, changedDataValue));
                                break;
                            case ChangeType.ChangeInObjectDataTable:
                                DataObjectValue changedDataObjectValue = new DataObjectValue(idObject, idDataType, value);
                                result.Add(new ChangeInObjectDataTable((ChangeAction)changeAction, tableName, batchId, columns, columnName, oldValue, newValue, changedDataObjectValue));
                                break;
                            default:
                                break;
                        }
                    }
                    return result.ToArray();
                }
            }
            return null;
        }
        #endregion
        #region SqlTypeToSystem
        static object SqlTypeToSystem(object value)
        {
            //if (value is SqlInt64)
            //    return ((SqlInt64)value).Value;
            //else if (value is SqlInt32)
            //    return ((SqlInt32)value).Value;
            //else if (value is SqlInt16)
            //    return ((SqlInt16)value).Value;
            //else if (value is SqlByte)
            //    return ((SqlByte)value).Value;
            //else if (value is SqlString)
            //    return ((SqlString)value).Value;
            //else if (value is SqlDateTime)
            //    return ((SqlDateTime)value).Value;
            //else if (value is SqlBoolean)
            //    return ((SqlBoolean)value).Value;
            //else if (value is SqlChars)
            //    return ((SqlChars)value).Value;
            //else if (value is SqlBinary)
            //    return ((SqlBinary)value).Value;
            //else if (value is SqlDouble)
            //    return ((SqlDouble)value).Value;
            //else if (value is SqlSingle)
            //    return ((SqlSingle)value).Value;
            //else if (value is DBNull)
            //    return null;
            //else if (value is SqlBytes)
            //    return ((SqlBytes)value).Value;
            //else
            //    return value;

            if (value == null || value is DBNull)
                return null;
            else if (value.GetType().Name.IndexOf("Sql") == 0)
                if ((bool)value.GetType().GetProperty("IsNull").GetValue(value, null))
                    return null;
                else
                    return value.GetType().GetProperty("Value").GetValue(value, null);
            else
                return value;
        }
        #endregion
    }
    #endregion
    #region class ChangeInDictTable
    [Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class ChangeInDictTable : ChangeInTable
    {
        #region Constructor
        public ChangeInDictTable(string tableName)
            :base (ChangeType.ChangeInDictTable, tableName)
        {
        }
        public ChangeInDictTable(ChangeAction changeAction, string tableName, long batchId, List<Tuple<string, object>> columns, string columnName, object oldValue, object newValue)
            : base(ChangeType.ChangeInDictTable, changeAction, tableName, batchId, columns, columnName, oldValue, newValue)
        {
        }
        #endregion
    }
    #endregion
    #region class ChangeInObjectDataTable
    [Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class ChangeInObjectDataTable : ChangeInTable
    {
        #region ObjectDataValue
#if !WindowsCE && !Android
		[DataMember]
#endif
        private DataObjectValue objectDataValue = null;
        public DataObjectValue ObjectDataValue
        {
            get
            {
                return objectDataValue;
            }
        }
        #endregion
        #region IsValueNull
        public bool IsValueNull
        {
            get
            {
                return (this.ObjectDataValue.Value == null);
            }
        }
        #endregion

        #region Constructor
        public ChangeInObjectDataTable(string tableName, DataObjectValue objectDataValue)
            :base (ChangeType.ChangeInObjectDataTable, tableName)
        {
            this.objectDataValue = objectDataValue;
        }
        public ChangeInObjectDataTable(ChangeAction changeAction, string tableName, long batchId, List<Tuple<string, object>> columns, string columnName, object oldValue, object newValue, DataObjectValue objectDataValue)
            : base(ChangeType.ChangeInObjectDataTable, changeAction, tableName, batchId, columns, columnName, oldValue, newValue)
        {
            this.objectDataValue = objectDataValue;
        }
        #endregion
    }
    #endregion
    #region class ChangeInDataTable
    [Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class ChangeInDataTable : ChangeInTable
    {
        #region DataValue
#if !WindowsCE && !Android
		[DataMember]
#endif
        private DataCurrentValue dataValue = null;
        public DataCurrentValue DataValue
        {
            get
            {
                return dataValue;
            }
        }
        #endregion
        #region IsValueNull
        public bool IsValueNull
        {
            get
            {
                return (this.dataValue.Value == null);
            }
        }
        #endregion

        #region Constructor
        public ChangeInDataTable(ChangeAction changeAction, long batchId, List<Tuple<string, object>> columns, string columnName, object oldValue, object newValue, DataCurrentValue dataValue)
            : base(ChangeType.ChangeInDataTable, changeAction, "DATA", batchId, columns, columnName, oldValue, newValue)
        {
            this.dataValue = dataValue;
        }
        public ChangeInDataTable(DataCurrentValue dataValue)
            : base(ChangeType.ChangeInDataTable, "DATA")
        {
            this.dataValue = dataValue;
        }
        #endregion
    }
    #endregion
}
