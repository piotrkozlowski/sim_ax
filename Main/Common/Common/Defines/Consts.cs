﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IMR.Suite.Common
{
    public static class Consts
    {
        public static readonly int AiutDistributorId = 1;

        public static double MapLatitudeAIUT = 50.32241;
		public static double MapLongitudeAIUT = 18.60249;
		public static int MapZoomAIUT = 5;

		public static string LOCATION_COLLECTOR_TOOL_PATH = @"\\imrprod\LocationCollector\IMRLocationCollector.exe";
        public static string PERIODIC_WORKER_TOOL_PATH = @"\\195.137.208.67\c$\IMR System Interfaces\Collector\IMRSCPeriodicWork.exe";
        public static string IMR_OPERATOR_REGISTRY_KEY = @"Software\IMR Server 4\Operator";

        public const string FileDialogXlsxFilter = "Excel *.xlsx| *.xlsx";
        public const string FileDialogXlsFilter = "Excel *.xls| *.xls";
        public const string FileDialogPDFFilter = "PDF *.pdf| *.pdf";
        public const string FileDialogRTFFilter = "Formatted text *.rtf| *.rtf";
        public const string FileDialogTXTFilter = "Text *.txt| *.txt";
        public const string FileDialogHTMLFilter = "HTML *.html| *.html";
        public const string FileDialogXMLFilter = "XML *.xml| *.xml";
		public const string FileDialogCsvFilter = "CSV *.csv| *.csv";
        public const string FileDialogPngFilter = "Png *.png| *.png";
        public const string FileDialogBmpFilter = "Bmp *.bmp| *.bmp";
        public const string FileDialogJpegFilter = "Jpeg *.jpeg| *.jpeg";
        public const string FileDialogTiffFilter = "Tiff *.tiff| *.tiff";
        public const string FileDialogGifFilter = "Gif *.gif| *.gif";
        public const double GravitationalConstant = 9.80665;
        public const string OSK_PATH = @"C:\windows\system32\osk.exe";
        //public static readonly DateTime NULL_DATE = new DateTime(1970, 1, 1, 12, 0, 0, 0);

        public const double PropanLiquidStateScaleParam = 0.254; // 1 [l] (liquid) = 0.254 [m3] (gas)
        public const double FuelLiquidToGasStateMultiplierLpgDefault = 254d; // 1 [l] (LPG liquid) = 254 [l] (LPG gas)
        public const double PropanLiquidDensity = 0.525; //[kg/dm3]

        public const long SystemSerialNumber = 10000000;

        public static class SerialPort
        {
            public static int[] BaudRate = new int[] { 9600, 19200, 28800, 38400, 57600, 115200 };
        }
    }
}
