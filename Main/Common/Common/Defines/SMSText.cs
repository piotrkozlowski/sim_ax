﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace IMR.Suite.Common
{
    [Serializable]
    public class SMSText
    {
        #region Members
        #region static Empty
		public static readonly SMSText Empty = new SMSText() { serialNbr = SN.Any, Body = new Body("", BodyType.Text7bit), status = Enums.TransmissionStatus.Unknown };
        #endregion

        #region SerialNbr
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private SN serialNbr = null;
        public SN SerialNbr
        {
            get { return serialNbr; }
        }
        #endregion
        #region Status
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private Enums.TransmissionStatus status = Enums.TransmissionStatus.Unknown;
        public Enums.TransmissionStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        #endregion
        #region Body
        public Body Body { get; set; }
        #endregion

        #region ExpectedPacketIn
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private Boolean expectedPacketIn = false;
        public Boolean ExpectedPacketIn 
        {
            set { expectedPacketIn = value; }
            get { return expectedPacketIn; }
        }
        #endregion
        #region SendTimeOut
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private TimeSpan sendTimeOut = TimeSpan.Zero;
        public TimeSpan SendTimeOut
        {
            set { sendTimeOut = value; }
            get { return sendTimeOut; }
        }
        #endregion

        #region MCostIn
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private int mCostIn = 20;
        public int MCostIn
        {
            get { return mCostIn; }
        }
        #endregion
        #region MCostOut
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private int mCostOut = 20;
        public int MCostOut
        {
            get { return mCostOut; }
        }
        #endregion

        #region IdAlarm
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private long? idAlarm = null;
        public long? IdAlarm
        {
            get { return idAlarm; }
        }
        #endregion
		#region IdAction
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private long? idAction = null;
		public long? IdAction
		{
			get { return idAction; }
			set { idAction = value; }
		}
		#endregion

        #region DiagnosticInfos
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }
        #endregion
		#endregion

        #region Constructor
		public SMSText()
		{
            this.DiagnosticInfos = new List<DiagnosticInfo>();
		}
        public SMSText(SN SerialNbr, string Message, Enums.TransmissionType TransmissionType)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            this.serialNbr = SerialNbr;
            this.status = Enums.TransmissionStatus.New;
            this.Body = new Body(Message, (BodyType)((int)TransmissionType));
        }
        public SMSText(SN SerialNbr, string Message, Enums.TransmissionType TransmissionType, long IdAlarm)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            this.serialNbr = SerialNbr;
            this.status = Enums.TransmissionStatus.New;
            this.Body = new Body(Message, (BodyType)((int)TransmissionType));
            this.idAlarm = IdAlarm;
        }
        public SMSText(SN SerialNbr, string Message, Enums.TransmissionType TransmissionType, long IdAlarm, TimeSpan SendTimeOut)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            this.serialNbr = SerialNbr;
            this.status = Enums.TransmissionStatus.New;
            this.Body = new Body(Message, (BodyType)((int)TransmissionType));
            this.expectedPacketIn = true;
            this.sendTimeOut = SendTimeOut;
            this.idAlarm = IdAlarm;
        }
        public SMSText(SN SerialNbr, Body Body)
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
            this.serialNbr = SerialNbr;
            this.Body = Body;
        }
        #endregion
    }
}