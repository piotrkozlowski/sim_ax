using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.Common
{
    public partial class Enums
    {
        // UWAGA: prosze zachowac porzadek alfabetyczny nazw enumeratorow

        #region ActionMode
        public enum ActionMode
        {
            Add,
            ReRun
        }
        #endregion
        #region ActionScheduleMode
        [Flags]
        public enum ActionScheduleMode : int
        {
            Normal = 0x00,
            ExecuteAtTime = 0x01,
            WaitForPacket = 0x02,
            WaitForData = 0x04,
            CalculateWakeUp = 0x08,
        }
        #endregion
        #region ActionStatus
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum ActionStatus
        {
            Unknown = 0,
            New = 1,
            Running = 2,
            Succeeded = 3,
            Error = 4,
            Partly = 5,
            Stop = 6,
            Stopped = 7,
            Abort = 8,
            Aborted = 9,
            Waiting = 10,
        }
        #endregion
        #region ActionType
        // UWAGA: Nie rusza� tego enuma.
        public enum ActionType : int
        {
            Unknown = 0,
            EventData = 1,
            GetData = 2,
            PutData = 3
        }
        #endregion
        #region ActionTypeClass
        // UWAGA: zmiany wprowadzac rowniez w BD
        public enum ActionTypeClass : int
        {
            Unknown = 0,
            Get = 1,
            Put = 2,
            System = 3,
            SMS = 4,
            Special = 5,
            Mobile = 6,
        }
        #endregion
        #region ActionTypeGroupeType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum ActionTypeGroupeType
        {
            Location = 1,
            Meter = 2,
            Device = 3,
        }
        #endregion

        #region AggregationType
        public enum AggregationType : int
        {
            Unknown = 0,
            Minute = 1,
            FiveMinutes = 2,
            Quarter = 3,
            HalfAnHour = 4,
            Hour = 5,
            FourHours = 6,
            SixHours = 7,
            EightHours = 8,
            TwelveHours = 9,
            Day = 10,
            Week = 11,
            Month = 12,
            Year = 13,
        }
        #endregion

        #region AlarmStatus
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum AlarmStatus
        {
            Unknown = 0,
            New = 1,
            Running = 2,
            Sent = 3,
            Delivered = 4,
            Error = 5,
            Confirmed = 6,
        }
        #endregion
        #region AlarmType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum AlarmType
        {
            Unknown = 0,
            ValueGreater = 1,
            ValueSmaller = 2,
            ValueGreaterOrEqual = 3,
            ValueSmallerOrEqual = 4,
            ValueEqual = 5,
            ValueNotEqual = 6,
            UnconditionalAcceptance = 7,
        }
        #endregion

        #region Article

        public enum ArticleElementType
        {
            Heatmeter = 1,
            Watermeter = 2,
        }

        #endregion

        #region CalculationMethod

        public enum CalculationMethod
        {
            BasedOnReadouts = 0,
            BasedOnPrediction = 1
        }

        #endregion

        #region CalibrationStatus

        public enum CalibrationStatus
        {
            New = 0,
            InProgress = 1,
            Finished = 2,
            FinishedWithErrors = 3
        }

        #endregion

        #region ChartType

        public enum ChartType
        {
            Bar = 0,
            Pie = 5,
            Doughnut = 6,
            Funnel = 7,
            Point = 8,
            Bubble = 9,
            Line = 10,
            Spline = 14,
            ScatterLine = 15,
            SwiftPlot = 16,
            Area = 17,
            Stock = 25,
            CandleStick = 26,
            Gantt = 30,
        }

        #endregion

        #region CodesType
        // UWAGA: zmiany wprowadzac rowniez w BD! (tbl: CODE_TYPE)
        public enum CodesType : int
        {
            ExtendendMenu = 1,
            ValveOpen = 2,
        }
        #endregion

        #region CommandCode
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum CommandCode
        {
            COMMAND_ID_NONE = 0,
            COMMAND_ID_FORCE_RADIO433_FRAME = 1,
            COMMAND_ID_FORCE_RADIO869_FRAME = 2,
            COMMAND_ID_PERIODIC_ARCHIVE_SAVE = 4,
            COMMAND_ID_MONTHLONG_ARCHIVE_SAVE = 5,
            COMMAND_ID_READ_TEMPERATURE = 6,
            COMMAND_ID_OPTO_ON = 7,
            COMMAND_ID_SET_TIME = 16,
            COMMAND_ID_DEVICE_RESET = 17,
            COMMAND_ID_OPTO_OFF = 18,
            COMMAND_ID_DIRECT_ACCESS = 19,
            COMMAND_ID_TEMPORARY_GE_ON = 20,
            COMMAND_ID_PACKET_ARCHIVE_CLEAR = 21,
            COMMAND_ID_DIAGNOSTICS_CLEAR = 22,
            COMMAND_ID_DAYLONG_ARCHIVE_CLEAR = 32,
            COMMAND_ID_DEFAULT_CONFIGURATION = 33,
            COMMAND_ID_LCD_OFF = 34,
            COMMAND_ID_SET_CARRIER = 35,
            COMMAND_ID_SET_OFTEN_FRAME433 = 36,
            COMMAND_ID_CLEAR_OFTEN_FRAME433 = 37,
            COMMAND_ID_AIR_RADIO_LEVEL_TEST = 38,
            COMMAND_ID_SET_OFTEN_FRAME869 = 39,
            COMMAND_ID_CLEAR_OFTEN_FRAME869 = 40,
            COMMAND_ID_LOG_WRITE = 41,
            COMMAND_ID_2ND_COUNTER_START = 42,
            COMMAND_ID_2ND_COUNTER_STOP = 43,
            COMMAND_ID_RAPID_HANDLE = 44,
            COMMAND_ID_LATEST_SACK_ARCHIVE = 45,
            COMMAND_ID_WAKEUP_GSM = 48,
            COMMAND_ID_OPTO_LOG = 49,
            COMMAND_ID_FREQ = 50,
            COMMAND_ID_WAKEUP_RADIO = 51,
            COMMAND_ID_GSM_TEST = 52,
            COMMAND_ID_GSM_RESET = 53,
            COMMAND_ID_DEVICE_STATUS_CLEAR = 54,
            COMMAND_ID_SACK_FREQ = 55,
            COMMAND_ID_MULTIREAD_FREQ = 56,
            COMMAND_ID_GSM_SHUTDOWN = 57,
            COMMAND_ID_GET_REQUEST = 58,
            COMMAND_ID_TEMPORARY_GE_OFF = 59,
            COMMAND_ID_ANALOG_ARCHIVE_FREQ = 60,
            COMMAND_ID_SACK_UNIQUE_FREQ = 61,
            COMMAND_ID_SEND_FRAMES_FROM_FLASH = 62,
            COMMAND_ID_SACK_KNOWN_NODES = 63,
            COMMAND_ID_INSTALL = 64,
            COMMAND_ID_UNINSTALL = 65,
            COMMAND_ID_WAIT_FOR_SEAL = 66,
            COMMAND_ID_FULL_INSTALLATION = 67,
            COMMAND_ID_SEAL = 68,
            COMMAND_ID_REFLASH_DEVICE = 69,
            COMMAND_ID_ANALOG_ARCHIVE_SAVE = 70,
            COMMAND_ID_ANALOG_ARCHIVE_CLEAR = 71,
            COMMAND_ID_ANALOG_ARCHIVE_LATCH = 72,
            COMMAND_ID_DIFF_COPY = 73,
            COMMAND_ID_WAKEUP_GPS = 74,
            COMMAND_ID_ERASE_FLASH = 75,
            COMMAND_ID_ERASE_SACK = 76,
            COMMAND_ID_SACK_ARCHIVE = 77,
            COMMAND_ID_LON_RESET = 78,
            COMMAND_ID_REFLASH_INIT = 79,
            COMMAND_ID_I2C_FORCE_MEASUREMENT = 80,
            COMMAND_ID_WMBUS_SEND_UD2 = 81,
            COMMAND_ID_MAGNETO_COUNTERS_RESET = 82,
            COMMAND_ID_GET_DIAGNOSTICS = 83,
            COMMAND_ID_GET_OMB = 84,
            COMMAND_ID_PREPAID_TEST = 95,
            COMMAND_ID_PREPAID_COMMISSION = 96,
            COMMAND_ID_PREPAID_DECOMMISSION = 97,
            COMMAND_ID_PREPAID_EC_CONF = 98,
            COMMAND_ID_WMBUS_MODE_NORMAL = 99,
            COMMAND_ID_WMBUS_MODE_TEMPORAL = 100,
            COMMAND_ID_WALK_BY_FRAME = 101,
            COMMAND_ID_VALVE_OPEN_UNSAFE = 102,
            COMMAND_ID_VALVE_CLOSE_UNSAFE = 103,
            COMMAND_ID_RADIO_RESET = 104,
            COMMAND_ID_STATIONARY_FRAME = 105,
            COMMAND_ID_PREPAID_RESUME = 106,
            COMMAND_ID_UDI_RESET = 107,
            COMMAND_ID_PREPAID_SUSPEND = 108,
            COMMAND_ID_CLEAR_R2S_TABLE = 109,
            COMMAND_ID_DIFF_READ = 110,
            COMMAND_ID_LCD_SET_ALL = 111,
            COMMAND_ID_EVC = 112,
            COMMAND_ID_ANALOG_LOG_ARCHIVE_CLEAR = 113,
            COMMAND_ID_SEND_TEXT_SMS = 114,
            COMMAND_ID_VALVE_CLOSE_SAFE = 115,
            COMMAND_ID_VALVE_OPEN_CONFIRM = 116,
            COMMAND_ID_VALVE_OPEN_SAFE = 117,
            COMMAND_ID_SEND_RADIO_SYNC_FRAME = 118,
            COMMAND_ID_REGULATOR_FREQ = 119,
            COMMAND_ID_LAN3_QUERY_SLAVE = 120,
            COMMAND_ID_DANFOSS_REQUEST = 121,
            COMMAND_ID_RADIO_PROTOCOL_MODE = 122,
            COMMAND_ID_LAN3_CLEAR_KNOWN_NODES = 123,
            COMMAND_ID_MODBUS_REQUEST = 124,
            COMMAND_ID_MODBUS_DATA_TYPE_REQUEST = 125,
            COMMAND_ID_RADIO_SEND_RADIAN_REQUEST = 126,
            COMMAND_ID_LAN3_SEND_SACK_UNIQUE = 127,
            COMMAND_ID_RADIO_REFLASH_PROCEDURE = 128,
            COMMAND_ID_KNOW_NODES_QUERY = 129,
            COMMAND_ID_RADIO_SEND_WAVENIS_REQUEST = 130,
            COMMAND_ID_CANCEL_LOADED_FV_MSG = 131,
            COMMAND_ID_CHECK_CONDITION_TANKING = 132,
            COMMAND_ID_ARCHIVE_TIME_BACK_QUERY = 133,
            //LUKA
            COMMAND_ID_GET_CHARACTERISTICS = 0x87,  //  135
            COMMAND_ID_SIGFOX_SEND = 0x88,  //  136
            COMMAND_ID_LCD_WARNING_ICON_CLEAR = 137,

            COMMAND_ID_DEVICE_RESET2 = 77717,
            COMMAND_ID_DEVICE_RESET3 = 77817,
        }
        #endregion

        #region CommandStatus
        // UWAGA! Zmieniaj�c co� poni�ej uaktualnij Enums.MeasureValueStatus
        public enum CommandStatus
        {
            None = 0,
            New = 1,

            PutOK = 101,
            GetOK = 102,

            NotFound = 200,
            AccessDenied = 201,
            ReadOnly = 202,
            WrongSize = 203,
            ParameterIncorrect = 204,
            NotSupported = 205,
            BusBusy = 206,
            EEpromFail = 207,
            NotTable = 208,
            TableOutOfRange = 209,
            OrderNotCorrect = 210,
            BadState = 211,
            WriteError = 212,
            TableAccessError = 213,
            ReadError = 214,
            FtpError = 215,
            UnknownError = 220
        }
        #endregion CommandStatus
        #region CommandType
        public enum CommandType
        {
            Get,
            Put,
            Alone,
            None,
        }
        #endregion CommandType

        #region Component
        public enum Component
        {
            Entire = 1,
            Battery = 2,
            BatteryPack = 3,
            GasMeter = 4,
        }
        #endregion

        #region ConsumerTransactionType
        public enum ConsumerTransactionType : int
        {
            Consumption = 1,
            DeliveryCharge = 2,
            StandingCharge = 3,
            CarbonTax = 4,
            TopUp = 5,
            Refund = 6,
            Manual = 7,
            MonthlyAdditionalCharge = 8
        }
        #endregion

        #region CustomEventClass
        public enum CustomEventClass : int
        {
            Unknown = 0,
            PumpOut = 1,
            GhostTransaction = 2,
            MissingTransaction = 3,
            ChangeOfConfiguration = 4,
            OtherTechnicalIssue = 5,
            Repumping = 6
        }
        #endregion

        #region DatabaseType
        public enum DatabaseType : int
        {
            Unknown = 0,
            CORE = 1,
            DAQ = 2,
            DW = 3,
            CORE_IMRSC = 4,
        }
        #endregion

        #region DataChange

        public enum DataChange
        {
            Insert = 1,
            Update = 2,
            Delete = 3
        }

        #endregion

        #region DataProviderType
        // enum sluzacy do rozroznienia DataProvider-ow
        public enum DataProviderType
        {
            DataProvider = 0,
            DataProviderFP = 1
        }
        #endregion

        #region DashboardItemClass
        public enum DashboardItemClass
        {
            TILE = 0,
            GAUGE = 1,
            CHART = 2,
            GRID = 3,
            CARD = 4,
            MAP = 5,
            SCHEMA = 6
        }
        #endregion
        #region DataSourceType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DataSourceType
        {
            Unknown = 0,
            OKO = 1,
            Fitter = 2,
            RemoteDiagnostic = 3,
            Psion = 4,
            Estimation = 5,
            SystemFunction = 6,
            ManualDataEntry = 7,
            SqlJob = 8,
            File = 9,
            AWSR = 10,
            WeatherForecast = 11,
            Interface = 12,
            RefuelEnhanced = 13,
            Flowmeter = 14,
            SIR2 = 15,
            Stocktaking = 16,
            GainLoss = 17
        }
        #endregion
        #region DataStatus
        // UWAGA: zmiany wprowadzac rowniez w BD!
        // ponizsze swiadomie jest long, jakby w przyszlosci w bazie tez bylo long
        [Flags]
        public enum DataStatus : long
        {
            Unknown = 0,
            Normal = 0x01,
            DuringRefuelling = 0x02,
            ActiveInDevice = 0x04,
            TemperatureError = 0x08,
            Predicted = 0x10,
            Synchronized = 0x20,
            Outlier = 0x40,
            Doublet = 0x4000000000000000
        }
        #endregion
        #region DataTypeAccesibility
        //Logical groups used by IMR Operator:
        public enum DataTypeAccesibility
        {
            Undefined = 0,              //any combination not specified by following:
            ReadOnly = 1,               //(!is_editable && !is_remote_write)
            ReadWrite = 2,              //(is_editable)
            ReadRemoteWrite = 3,        //(!is_editable && is_remote_write)
        }
        #endregion
        #region DataTypeClass
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DataTypeClass
        {
            Unknown = 0,
            Integer = 1,
            Real = 2,
            Text = 3,
            Date = 4,
            Time = 5,
            Datetime = 6,
            Boolean = 7,
            Object = 8,
            Xml = 9,
            Color = 10,
            Binary = 11,
            Decimal = 12,
        }
        #endregion
        #region DataTypeFormat
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DataTypeFormat
        {
            /*
             * Reguly dodawania nazw tego enumeratora - w zwiazku z tym, ze tabelka nie posiada zadnej kolumny z nazwa:
             * 1. Jesli jakas kolumna jest uzupelniona (poza kolumnami ID_DATA_TYPE_FORMAT, ID_DESCR, ID_UNIQUE_TYPE) - nazwa kolumny wraz z wartoscia ma pojawic sie w nazwie.
             * 2. Slowa, wartosci i grupy kolumn oddzielac znakiem _
             * 3. Dla wartosci z przecinkiem uzywac slowa dot zamiast kropki/przecinka
             * 4. Reguly nazewnictwa dla poszczegolnych grup kolumn:
             *      xxx - 'Min' jesli jest zdefiniowana tylko wartosc MIN, 'Max' jesli jest zdefiniowana tylko wartosc MAX, 'Min_Max' jesli sa zdefiniowane obie
             *      yyy - <minValue> jesli tylko MIN, <maxValue> jesli tylko MAX, <minValue>_<maxValue> jesli obie
             *      4.1. TEXT_MIN_LENGTH i TEXT_MAX_LENGTH - Text_xxx_Length_yyy:
             *      4.2. NUMBER_MIN_PRECISION i NUMBER_MAX_PRECISION - Number_xxx_Precision_yyy
             *      4.3. NUMBER_MIN_SCALE i NUMBER_MAX_SCALE - Number_xxx_Scale_yyy
             *      4.4. NUMBER_MIN_VALUE i NUMBER_MAX_VALUE - Number_xxx_Value_yyy
             *      4.5. DATETIME_FORMAT - Datetime_Format_fff - gdzie fff to string z zapisanym formatem (moze byc krotki opis)
             *      4.6. REGULAR_EXPRESSION - Regular_Expression_rrr - gdzie rrr to jakis krotki opis RegEx (tak, aby nazwa enuma mogla pozostac unikatowa)
             *      4.7. IS_REQUIRED - Is_Required_b - gdzie b to 0 lub 1
             *      przyklady:
             *      Text_Min_Max_Length_1_50 - text min length = 1, text max length = 50
             *      Text_Max_Length_50 - text max length = 50
             *      Number_Max_Precision_2_Number_Min_Max_Value_0_100_Is_Required_1 - number max precision = 2, number min value = 0, number max value = 100, is required
             *      Number_Min_Max_Value_0dot1_0dot99 - number min value = 0.1, number max value = 0.99
            */

            Unknown = 0,
            Number_Min_Max_Value_0_1024 = 1, // number [0 - 1024]
            Number_Min_Max_Value_1_65535 = 2, // number [1 - 65535]
            Text_Min_Max_Length_1_50_Is_Required_1 = 3, // text [1 - 50] required
            Text_Min_Max_Length_1_9_Is_Required_1 = 4, // text [1 - 9] required
            Number_Max_Precision_5_Is_Required_1 = 5, // number precision [ - 5] required
            Number_Max_Precision_10_Is_Required_1 = 6, // number precision [ - 10] required
            Number_Min_Max_Value_0_100 = 7, // number [0 - 100]
            Number_Min_Value_0 = 8, // number [0 - ]
            Number_Max_Precision_0 = 9, // number precision [ - 0]
            Number_Max_Precision_1 = 10, // number precision [ - 1]
            Number_Max_Precision_2 = 11, // number precision [ - 2]
            Datetime_Format_ShortDateNoTime = 12, // yyyy-MM-dd
            Datetime_Format_ShortDateShortTime = 13, // yyyy-MM-dd HH:mm
            Datetime_Format_ShortDateLongTime = 14, // yyyy-MM-dd HH:mm:ss
            Number_Max_Precision_3 = 15, // number precision [ - 3]
        }
        #endregion

        #region DataTypeGroupOLD - po wyeliminowaniu IMR SIMA (Operator) do usuniecia
        public enum DataTypeGroupOLD
        {
            SIMA_LOCATION_PARAMS_TO_DISPLAY = 1,
            SIMA_METER_PARAMS_TO_DISPLAY = 2,
            SIMA_DEVICE_PARAMS_TO_DISPLAY = 3,
            SIMA_PARAMS_TO_SYNCHRO = 4,
            SIMA_METER_PARAMS_TO_EDIT = 5,
            INDEXABLE = 6
        }
        #endregion
        #region DataTypeGroup
        public class DataTypeGroup
        {
            public const string SIMA_LOCATION_GRID_PARAMS = "SIMA_LOCATION_GRID_PARAMS";
            public const string SIMA_LOCATION_GRID_CUSTOM_PARAMS = "SIMA_LOCATION_GRID_CUSTOM_PARAMS";
            public const string SIMA_LOCATION_PARAMS_TO_DISPLAY = "SIMA_LOCATION_PARAMS_TO_DISPLAY";
            public const string SIMA_LOCATION_PARAMS_TO_EDIT = "SIMA_LOCATION_PARAMS_TO_EDIT";
            public const string SIMA_LOCATION_INSTALLED_METER_PARAMS = "SIMA_LOCATION_INSTALLED_METER_PARAMS";
            public const string SIMA_LOCATION_MAP_PARAMS = "SIMA_LOCATION_MAP_PARAMS";

            public const string SIMA_METER_GRID_PARAMS = "SIMA_METER_GRID_PARAMS";
            public const string SIMA_METER_GRID_CUSTOM_PARAMS = "SIMA_METER_GRID_CUSTOM_PARAMS";
            public const string SIMA_METER_PARAMS_TO_DISPLAY = "SIMA_METER_PARAMS_TO_DISPLAY";
            public const string SIMA_METER_PARAMS_TO_EDIT = "SIMA_METER_PARAMS_TO_EDIT";

            public const string SIMA_DEVICE_GRID_PARAMS = "SIMA_DEVICE_GRID_PARAMS";
            public const string SIMA_DEVICE_GRID_CUSTOM_PARAMS = "SIMA_DEVICE_GRID_CUSTOM_PARAMS";
            public const string SIMA_DEVICE_PARAMS_TO_DISPLAY = "SIMA_DEVICE_PARAMS_TO_DISPLAY";
            public const string SIMA_DEVICE_PARAMS_TO_EDIT = "SIMA_DEVICE_PARAMS_TO_EDIT";

            public const string SIMA_OPERATOR_GRID_PARAMS_TO_DISPLAY = "SIMA_OPERATOR_GRID_PARAMS_TO_DISPLAY";
            public const string SIMA_OPERATOR_PARAMS_TO_EDIT = "SIMA_OPERATOR_PARAMS_TO_EDIT";

            public const string SIMA_PARAMS_TO_SYNCHRO = "SIMA_PARAMS_TO_SYNCHRO";
            public const string MEASURE_DEVICE_DATA = "MEASURE_DEVICE_DATA";     // !!! z tego korzysta tylko IMRSIMA

            //dodane 04.10.2011 bblach
            public const string SIMA_LOCATION_MEASUREMENTS = "SIMA_LOCATION_MEASUREMENTS";
            public const string SIMA_DEVICE_MEASUREMENTS = "SIMA_DEVICE_MEASUREMENTS";
            public const string SIMA_METER_MEASUREMENTS = "SIMA_METER_MEASUREMENTS";

            //af 17-10.2011 used by onsite connection agent:
            public const string SIMA_OPTO_SYNCHRONIZABLE_DATA = "SIMA_OPTO_SYNCHRONIZABLE_DATA";

            //bblach 29.11.2011
            public const string SIMA_LOCATION_PATTERN_PARAMS_TO_SYNCHRO = "SIMA_LOCATION_PATTERN_PARAMS_TO_SYNCHRO";

            //af 07-02.2012 used by driver edit windows:
            public const string SIMA_SYSTEM_DRIVERS_DATA_TO_EDIT = "SIMA_SYSTEM_DRIVERS_DATA_TO_EDIT";

            //sw 11.04.2012
            public const string WEBIMR_LOCATION_MEASURES_TO_DISPLAY = "WEBIMR_LOCATION_MEASURES_TO_DISPLAY";

            //sw 05.07.2012
            public const string SIMA_MODULE_DATA_TYPES_TO_EDIT = "SIMA_MODULE_DATA_TYPES_TO_EDIT";
            public const string SIMA_SYSTEM_DATA_TYPES_TO_EDIT = "SIMA_SYSTEM_DATA_TYPES_TO_EDIT";

            //sw 03.09.2012
            public const string COMMON_PROFILE_PARAMS_TO_DISPLAY = "COMMON_PROFILE_PARAMS_TO_DISPLAY";
            public const string COMMON_PROFILE_PARAMS_TO_EDIT = "COMMON_PROFILE_PARAMS_TO_EDIT";

            //sw 14.09.2012
            public const string DIMA_DELIVERY_ADVISOR_LPG_LOCATION_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_LPG_LOCATION_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_OIL_LOCATION_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_OIL_LOCATION_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_ATG_LOCATION_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_ATG_LOCATION_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_LPG_DATA_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_LPG_DATA_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_OIL_DATA_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_OIL_DATA_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_ATG_DATA_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_ATG_DATA_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_OIL_TANK_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_OIL_TANK_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_ATG_TANK_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_ATG_TANK_GRID_PARAMS";

            //05.20.2013
            public const string SGM_DEVICE_MEASUREMENTS = "SGM_DEVICE_MEASUREMENTS";
            public const string SGM_METER_MEASUREMENTS = "SGM_METER_MEASUREMENTS";
            public const string SGM_LOCATION_MEASUREMENTS = "SGM_LOCATION_MEASUREMENTS";

            // rmrozek 02.09.2013
            public const string SGM_BASIC_DEVICE_PARAMETERS = "SGM_BASIC_DEVICE_PARAMETERS";
            public const string SGM_LOCATION_EXTENDED_PARAMETERS = "SGM_LOCATION_EXTENDED_PARAMETERS";

            //tstankiewicz 19.11.2013
            public const string SIMA_PATTERN_PARAMS_TO_EDIT = "SIMA_PATTERN_PARAMS_TO_EDIT";
            public const string SIMA_TEMPLATE_PATTERN_PARAMS_BLOCKED = "SIMA_TEMPLATE_PATTERN_PARAMS_BLOCKED";
            public const string SYSTEM_DEVICE_REQUIRED_PARAMS = "SYSTEM_DEVICE_REQUIRED_PARAMS";

            //rmrozek 14.11.2013Z:\Developer\Apps\Aiut\Repo\Data\DataBase\CORE\Update\ChangeLog.txt
            public const string SGM_KAZAKHSTAN_TARIFF = "POSTPAID_TARIFF_TEMPLATE";

            // rmrozek 09.12.2013;
            public const string SGM_DATA_TYPES_FOR_INSPECTION = "SGM_DATA_TYPES_FOR_INSPECTION";

            // rmrozek 10.02.2014
            public const string SGM_DEVICE_TYPE_MEASURES_TO_DISPLAY_BASIC = "SGM_DEVICE_TYPE_MEASURES_TO_DISPLAY_BASIC";

            // rmrozek 13.02.2014
            public const string SGM_MAIN_DATA_TYPES_TO_DISPLAY_BASIC = "SGM_MAIN_DATA_TYPES_TO_DISPLAY_BASIC";

            public const string SGM_DEVICE_TYPE_MEASURES_TO_DISPLAY_DETAILED = "SGM_DEVICE_TYPE_MEASURES_TO_DISPLAY_DETAILED";

            // pbartkowiak (wondervision) 25.02.2014
            public const string DIMA_LOCATION_INSTALLED_METER_PARAMS = "DIMA_LOCATION_INSTALLED_METER_PARAMS";
            public const string DIMA_METER_PARAMS_TO_DISPLAY = "DIMA_METER_PARAMS_TO_DISPLAY";
            public const string DIMA_METER_PARAMS_TO_EDIT = "DIMA_METER_PARAMS_TO_EDIT";
            public const string DIMA_PARAMS_TO_SYNCHRO = "DIMA_PARAMS_TO_SYNCHRO";

            //dodane 09.12.2014 lpiela
            public const string IMRSC_MEASUREMENTS = "IMRSC_MEASUREMENTS";

            // sdudzik 03.03.2015
            public const string SIMA_METER_TYPE_PARAMS_TO_EDIT = "SIMA_METER_TYPE_PARAMS_TO_EDIT";

            // lpiela 01.04.2015
            public const string DATA_TEMPORAL_PERFORMANCES = "DATA_TEMPORAL_PERFORMANCES";
            public const string DISTRIBUTOR_PERFORMANCES = "DISTRIBUTOR_PERFORMANCES";
            public const string IMR_SERVER_PERFORMANCES = "IMR_SERVER_PERFORMANCES";
            public const string REPORT_PERFORMANCES = "REPORT_PERFORMANCES";
            public const string IMRSC_READOUT_TYPE = "IMRSC_READOUT_TYPE";

            // pbombik 03.04.2015
            public const string QUEUE_PERFORMANCES = "QUEUE_PERFORMANCES";

            // sdudzik 29.04.2015
            public const string IMRSC_DEVICE_DATA_GRID_PARAMS = "IMRSC_DEVICE_DATA_GRID_PARAMS";

            // sdudzik 05.05.2015
            public const string IMRSC_METER_MEASURES_PARAMS_TO_EDIT = "IMRSC_METER_MEASURES_PARAMS_TO_EDIT";

            // pbombik 05.05.2015
            public const string PROCESS_PERFORMANCES = "PROCESS_PERFORMANCES";

            // sdudzik 06.05.2015
            public const string IMRSC_TASK_DEVICE_DATA_GRID_PARAMS = "IMRSC_TASK_DEVICE_DATA_GRID_PARAMS";

            public const string IMRSC_LOCATION_GRID_PARAMS = "IMRSC_LOCATION_GRID_PARAMS";
            public const string IMRSC_DEVICE_GRID_PARAMS = "IMRSC_DEVICE_GRID_PARAMS";
            public const string IMRSC_TASK_DETAILS_DEVICE_GRID_PARAMS = "IMRSC_TASK_DETAILS_DEVICE_GRID_PARAMS";
            public const string IMRSC_TASK_DETAILS_METER_GRID_PARAMS = "IMRSC_TASK_DETAILS_METER_GRID_PARAMS";
            public const string IMRSC_METER_GRID_PARAMS = "IMRSC_METER_GRID_PARAMS";
            public const string IMRSC_TASK_GRID_PARAMS = "IMRSC_TASK_GRID_PARAMS";

            // sdudzik 21.09.2015
            public const string DIMA_DELIVERY_ADVISOR_METER_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_METER_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_LOCATION_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_LOCATION_GRID_PARAMS";
            public const string DIMA_DELIVERY_ADVISOR_METER_DATA_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_METER_DATA_GRID_PARAMS";

            // sdudzik 06.11.2015
            public const string SIMA_LOCATION_GRID_CUSTOM_MEASURES = "SIMA_LOCATION_GRID_CUSTOM_MEASURES";
            public const string SIMA_METER_GRID_CUSTOM_MEASURES = "SIMA_METER_GRID_CUSTOM_MEASURES";
            public const string SIMA_DEVICE_GRID_CUSTOM_MEASURES = "SIMA_DEVICE_GRID_CUSTOM_MEASURES";
            public const string IMRSC_LOCATION_GRID_CUSTOM_MEASURES = "IMRSC_LOCATION_GRID_CUSTOM_MEASURES";
            public const string IMRSC_METER_GRID_CUSTOM_MEASURES = "IMRSC_METER_GRID_CUSTOM_MEASURES";
            public const string IMRSC_DEVICE_GRID_CUSTOM_MEASURES = "IMRSC_DEVICE_GRID_CUSTOM_MEASURES";
            public const string IMRSC_TASK_GRID_CUSTOM_MEASURES = "IMRSC_TASK_GRID_CUSTOM_MEASURES";

            // sdudzik 23.12.2015
            public const string SIMA_DEVICE_TYPE_PARAMS_TO_EDIT = "SIMA_DEVICE_TYPE_PARAMS_TO_EDIT";

            // pbombik 07.01.2016
            public const string TRANSMISSION_DRIVER_PERFORMANCES = "TRANSMISSION_DRIVER_PERFORMANCES";

            // pbombik 09.05.2016
            public const string IMRSC_DISTRIBUTOR_GRID_PARAMS = "IMRSC_DISTRIBUTOR_GRID_PARAMS";
            public const string IMRSC_IMR_SERVER_GRID_PARAMS = "IMRSC_IMR_SERVER_GRID_PARAMS";

            // psyrica 15.06.2016
            public const string IMRSC_TASK_DETAILS_LOCATION_PANEL_PARAMS = "IMRSC_TASK_DETAILS_LOCATION_PANEL_PARAMS";

            // psyrica 29.06.2016
            public const string SIMA_LOCATION_PANEL_PARAMS_TO_DISPLAY = "SIMA_LOCATION_PANEL_PARAMS_TO_DISPLAY";
            public const string SIMA_LOCATION_PANEL_PARAMS_TO_EDIT = "SIMA_LOCATION_PANEL_PARAMS_TO_EDIT";

            // psyrica 01.07.2016
            public const string SIMA_DISTRIBUTOR_PANEL_PARAMS_TO_DISPLAY = "SIMA_DISTRIBUTOR_PANEL_PARAMS_TO_DISPLAY";
            public const string SIMA_DISTRIBUTOR_PANEL_PARAMS_TO_EDIT = "SIMA_DISTRIBUTOR_PANEL_PARAMS_TO_EDIT";

            // sdudzik 05.07.2016
            public const string DIMA_DELIVERY_ADVISOR_LOCATION_DATA_GRID_PARAMS = "DIMA_DELIVERY_ADVISOR_LOCATION_DATA_GRID_PARAMS";

            // lpiela 14.07.2016
            public const string WEBSIMAX_LOCATION_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_LOCATION_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_LOCATION_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_LOCATION_PANEL_PARAMS_TO_EDIT";

            // pbombik 14.07.2016
            public const string ETL_PERFORMANCES = "ETL_PERFORMANCES";

            // sdudzik 21.07.2016
            public const string SIMA_OPERATOR_PANEL_PARAMS_TO_DISPLAY = "SIMA_OPERATOR_PANEL_PARAMS_TO_DISPLAY";
            public const string SIMA_OPERATOR_PANEL_PARAMS_TO_EDIT = "SIMA_OPERATOR_PANEL_PARAMS_TO_EDIT";

            // psyrica 09.08.2016
            public const string WEBSIMAX_METER_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_METER_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_METER_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_METER_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_DEVICE_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_DEVICE_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_DEVICE_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_DEVICE_PANEL_PARAMS_TO_EDIT";

            // psyrica 17.08.2016
            public const string WEBSIMAX_LOCATION_DEFAULT_GRID_PARAMS = "WEBSIMAX_LOCATION_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_LOCATION_GRID_PARAMS = "WEBSIMAX_LOCATION_GRID_PARAMS";
            public const string WEBSIMAX_DEVICE_DEFAULT_GRID_PARAMS = "WEBSIMAX_DEVICE_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_DEVICE_GRID_PARAMS = "WEBSIMAX_DEVICE_GRID_PARAMS";
            public const string WEBSIMAX_METER_DEFAULT_GRID_PARAMS = "WEBSIMAX_METER_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_METER_GRID_PARAMS = "WEBSIMAX_METER_GRID_PARAMS";
            public const string WEBSIMAX_LOCATION_MEASUREMENTS = "WEBSIMAX_LOCATION_MEASUREMENTS";
            public const string WEBSIMAX_DEVICE_MEASUREMENTS = "WEBSIMAX_DEVICE_MEASUREMENTS";
            public const string WEBSIMAX_METER_MEASUREMENTS = "WEBSIMAX_METER_MEASUREMENTS";
            public const string WEBSIMAX_OPERATOR_DEFAULT_GRID_PARAMS = "WEBSIMAX_OPERATOR_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_OPERATOR_GRID_PARAMS = "WEBSIMAX_OPERATOR_GRID_PARAMS";
            public const string WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_TASK_DEFAULT_GRID_PARAMS = "WEBSIMAX_TASK_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_TASK_GRID_PARAMS = "WEBSIMAX_TASK_GRID_PARAMS";
            public const string WEBSIMAX_TASK_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_TASK_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_TASK_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_TASK_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_ISSUE_DEFAULT_GRID_PARAMS = "WEBSIMAX_ISSUE_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_ISSUE_GRID_PARAMS = "WEBSIMAX_ISSUE_GRID_PARAMS";
            public const string WEBSIMAX_ISSUE_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_ISSUE_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_ISSUE_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_ISSUE_PANEL_PARAMS_TO_EDIT";

            // psyrica 23.08.2016
            public const string IMRSC_IMR_SERVER_PANEL_PARAMS_TO_DISPLAY = "IMRSC_IMR_SERVER_PANEL_PARAMS_TO_DISPLAY";
            public const string IMRSC_IMR_SERVER_PANEL_PARAMS_TO_EDIT = "IMRSC_IMR_SERVER_PANEL_PARAMS_TO_EDIT";

            // psyrica 26.08.2016
            public const string WEBSIMAX_DEVICE_PARAMS_REQUIRED = "WEBSIMAX_DEVICE_PARAMS_REQUIRED";
            public const string WEBSIMAX_ISSUE_PARAMS_REQUIRED = "WEBSIMAX_ISSUE_PARAMS_REQUIRED";
            public const string WEBSIMAX_LOCATION_PARAMS_REQUIRED = "WEBSIMAX_LOCATION_PARAMS_REQUIRED";
            public const string WEBSIMAX_METER_PARAMS_REQUIRED = "WEBSIMAX_METER_PARAMS_REQUIRED";
            public const string WEBSIMAX_OPERATOR_PARAMS_REQUIRED = "WEBSIMAX_OPERATOR_PARAMS_REQUIRED";
            public const string WEBSIMAX_TASK_PARAMS_REQUIRED = "WEBSIMAX_TASK_PARAMS_REQUIRED";
            //lpiela 31.08.2016
            public const string WEBSIMAX_ACTOR_DEFAULT_GRID_PARAMS = "WEBSIMAX_ACTOR_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_ACTOR_GRID_PARAMS = "WEBSIMAX_ACTOR_GRID_PARAMS";
            public const string WEBSIMAX_ACTOR_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_ACTOR_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_ACTOR_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_ACTOR_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_ACTOR_PARAMS_REQUIRED = "WEBSIMAX_ACTOR_PARAMS_REQUIRED";

            // psyrica 06.09.2016
            public const string WEBSIMAX_DEVICE_GRID_PARAMS_TO_DISPLAY = "WEBSIMAX_DEVICE_GRID_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_DEVICE_GRID_PARAMS_TO_EDIT = "WEBSIMAX_DEVICE_GRID_PARAMS_TO_EDIT";

            // psyrica 08.09.2016
            public const string IMRSC_TASK_PANEL_PARAMS_TO_EDIT = "IMRSC_TASK_PANEL_PARAMS_TO_EDIT";
            public const string IMRSC_TASK_PANEL_PARAMS_TO_DISPLAY = "IMRSC_TASK_PANEL_PARAMS_TO_DISPLAY";

            //lpiela 19.09.2016
            public const string WEBSIMAX_DISTRIBUTOR_DEFAULT_GRID_PARAMS = "WEBSIMAX_DISTRIBUTOR_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_DISTRIBUTOR_GRID_PARAMS = "WEBSIMAX_DISTRIBUTOR_GRID_PARAMS";
            public const string WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_DISTRIBUTOR_PARAMS_REQUIRED = "WEBSIMAX_DISTRIBUTOR_PARAMS_REQUIRED";

            // psyrica 30.09.2016
            public const string IMRSC_ISSUE_PANEL_PARAMS_TO_EDIT = "IMRSC_ISSUE_PANEL_PARAMS_TO_EDIT";
            public const string IMRSC_ISSUE_PANEL_PARAMS_TO_DISPLAY = "IMRSC_ISSUE_PANEL_PARAMS_TO_DISPLAY";

            // pbombik 03.11.2016
            public const string IMRSC_OPERATOR_FITTER_PANEL_PARMAS_TO_EDIT = "IMRSC_OPERATOR_FITTER_PANEL_PARMAS_TO_EDIT";
            public const string IMRSC_OPERATOR_FITTER_PANEL_PARMAS_TO_DISPLAY = "IMRSC_OPERATOR_FITTER_PANEL_PARMAS_TO_DISPLAY";

            // lpiela 07.11.2016
            public const string WEBSIMAX_ALARM_DEFAULT_GRID_PARAMS = "WEBSIMAX_ALARM_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_ALARM_GRID_PARAMS = "WEBSIMAX_ALARM_GRID_PARAMS";
            public const string WEBSIMAX_ALARM_PARAMS_REQUIRED = "WEBSIMAX_ALARM_PARAMS_REQUIRED";
            public const string WEBSIMAX_ALARM_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_ALARM_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_ALARM_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_ALARM_PANEL_PARAMS_TO_EDIT";

            // psyrica 10.11.2016
            public const string WEBSIMAX_LOCATION_DEVICE_GRID_PARAMS = "WEBSIMAX_LOCATION_DEVICE_GRID_PARAMS";
            public const string WEBSIMAX_LOCATION_METER_GRID_PARAMS = "WEBSIMAX_LOCATION_METER_GRID_PARAMS";
            public const string WEBSIMAX_ISSUE_TASK_GRID_PARAMS = "WEBSIMAX_ISSUE_TASK_GRID_PARAMS";

            public const string WEBSIMAX_ROUTE_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_ROUTE_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_ROUTE_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_ROUTE_PANEL_PARAMS_TO_EDIT";

            // psyrica 15.11.2016
            public const string WEBSIMAX_ROUTE_PARAMS_REQUIRED = "WEBSIMAX_ROUTE_PARAMS_REQUIRED";
            public const string WEBSIMAX_ROUTE_GRID_PARAMS = "WEBSIMAX_ROUTE_GRID_PARAMS";
            public const string WEBSIMAX_ROUTE_DEFAULT_GRID_PARAMS = "WEBSIMAX_ROUTE_DEFAULT_GRID_PARAMS";

            // PSYRICA 17.11.2016
            public const string WEBSIMAX_DATA_TYPE_CHART_TYPE = "WEBSIMAX_DATA_TYPE_CHART_TYPE";

            //lpiela 12.12.2016
            public const string WEBSIMAX_METER_DEVICE_GRID_PARAMS = "WEBSIMAX_METER_DEVICE_GRID_PARAMS";

            // psyrica 23-12-2016
            public const string WEBSIMAX_REPORT_GRID_PARAMS = "WEBSIMAX_REPORT_GRID_PARAMS";
            public const string WEBSIMAX_REPORT_DEFAULT_GRID_PARAMS = "WEBSIMAX_REPORT_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_REPORT_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_REPORT_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_REPORT_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_REPORT_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_REPORT_PARAMS_REQUIRED = "WEBSIMAX_REPORT_PARAMS_REQUIRED";
            public const string WEBSIMAX_SIM_CARD_GRID_PARAMS = "WEBSIMAX_SIM_CARD_GRID_PARAMS";
            public const string WEBSIMAX_SIM_CARD_DEFAULT_GRID_PARAMS = "WEBSIMAX_SIM_CARD_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_SIM_CARD_PARAMS_REQUIRED = "WEBSIMAX_SIM_CARD_PARAMS_REQUIRED";
            public const string WEBSIMAX_ACTION_PANEL_PARAMS_TO_EDIT = "WEBSIMAX_ACTION_PANEL_PARAMS_TO_EDIT";
            public const string WEBSIMAX_ACTION_PANEL_PARAMS_TO_DISPLAY = "WEBSIMAX_ACTION_PANEL_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_ACTION_PARAMS_REQUIRED = "WEBSIMAX_ACTION_PARAMS_REQUIRED";

            //pbombik 09.01.2017
            public const string WEBSIMAX_LOCATION_PREVIEW_PANEL_PARAMS = "WEBSIMAX_LOCATION_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_METER_PREVIEW_PANEL_PARAMS = "WEBSIMAX_METER_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_DEVICE_PREVIEW_PANEL_PARAMS = "WEBSIMAX_DEVICE_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_OPERATOR_PREVIEW_PANEL_PARAMS = "WEBSIMAX_OPERATOR_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_TASK_PREVIEW_PANEL_PARAMS = "WEBSIMAX_TASK_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_ISSUE_PREVIEW_PANEL_PARAMS = "WEBSIMAX_ISSUE_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_ACTOR_PREVIEW_PANEL_PARAMS = "WEBSIMAX_ACTOR_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_DISTRIBUTOR_PREVIEW_PANEL_PARAMS = "WEBSIMAX_DISTRIBUTOR_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_ALARM_PREVIEW_PANEL_PARAMS = "WEBSIMAX_ALARM_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_REPORT_PREVIEW_PANEL_PARAMS = "WEBSIMAX_REPORT_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_SIM_CARD_PREVIEW_PANEL_PARAMS = "WEBSIMAX_SIM_CARD_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_ACTION_PREVIEW_PANEL_PARAMS = "WEBSIMAX_ACTION_PREVIEW_PANEL_PARAMS";
            public const string WEBSIMAX_ROUTE_TASK_GRID_PARAMS = "WEBSIMAX_ROUTE_TASK_GRID_PARAMS";
            public const string WEBSIMAX_ACTOR_LOCATION_GRID_PARAMS = "WEBSIMAX_ACTOR_LOCATION_GRID_PARAMS";
            public const string WEBSIMAX_ROUTE_OPERATOR_GRID_PARAMS = "WEBSIMAX_ROUTE_OPERATOR_GRID_PARAMS";

            //psyrica 17.01.2017
            public const string WEBSIMAX_ACTION_GRID_PARAMS = "WEBSIMAX_ACTION_GRID_PARAMS";
            public const string WEBSIMAX_ACTION_DEFAULT_GRID_PARAMS = "WEBSIMAX_ACTION_DEFAULT_GRID_PARAMS";
            //lpiela 27.01.2017
            public const string WEBSIMAX_LOCATION_GRID_PARAMS_TO_DISPLAY = "WEBSIMAX_LOCATION_GRID_PARAMS_TO_DISPLAY";
            public const string WEBSIMAX_LOCATION_GRID_PARAMS_TO_EDIT = "WEBSIMAX_LOCATION_GRID_PARAMS_TO_EDIT";

            public const string WEBSIMAX_ROUTE_LOCATION_GRID_PARAMS = "WEBSIMAX_ROUTE_LOCATION_GRID_PARAMS";
            public const string WEBSIMAX_DEVICE_CONNECTION_GRID_PARAMS = "WEBSIMAX_DEVICE_CONNECTION_GRID_PARAMS";
            public const string WEBSIMAX_DEVICE_CONNECTION_DEFAULT_GRID_PARAMS = "WEBSIMAX_DEVICE_CONNECTION_DEFAULT_GRID_PARAMS";
            public const string WEBSIMAX_DEVICE_CONNECTION_PREVIEW_PANEL_PARAMS = "WEBSIMAX_DEVICE_CONNECTION_PREVIEW_PANEL_PARAMS";

            // sdudzik 15.02.2017
            public const string DIMA_STATION_ANALYSIS_DETAILS_LOCATION_PARAMS = "DIMA_STATION_ANALYSIS_DETAILS_LOCATION_PARAMS";
            public const string DIMA_STATION_ANALYSIS_DETAILS_METER_PARAMS = "DIMA_STATION_ANALYSIS_DETAILS_METER_PARAMS";
        }
        #endregion
        #region DataTypeGroupType
        public enum DataTypeGroupType
        {
            //TS: NARAZIE jescze nie uporz�dkowane, trzeba porobic porz�dki!

            /*SIMA_PARAMS_DISPLAY_GROUP = 1,
            SIMA_PARAMS_SYNCHRO_GROUP = 2,
            SIMA_PARAMS_EDIT_GROUP = 3,
            SIMA_GENERAL = 4,
            WEBIMR_MEASURES_DISPLAY_GROUP = 5,
            WEBIMR_ALARM_TEXT_MANAGEMENT_GROUP = 6,
            TARIFF_GROUP = 7,
            WEBSIMAX_CLASSIFY_GROUP = 20,*/
        }
        #endregion

        #region DeliveryAdvice
        public enum DeliveryAdvice
        {
            Unknown = 0,
            MustGo = 1,
            CanGo = 2,
            CanGoAlmostMustGo = 3,
            AlmostMustGo = 4,
            AlmostCanGo = 5,
            NN = 6,
            NA = 7,
            InDelivery = 8,
            Rejected = 9,
            Planned = 10
        }
        #endregion
        #region DeliveryBatchStatus
        public enum DeliveryBatchStatus : int
        {
            New = 1,
            Commited = 2
        }
        #endregion
        #region DeliveryCommitMethod
        public enum DeliveryCommitMethod
        {
            Unknown = 0,
            Simple = 1,
            Lomosoft = 2,
            GBS = 3,
            File = 4,
        }
        #endregion

        #region DetailView
        public enum DetailView
        {
            LocationInfo = 1,
            CurrentState = 2,
            MeasurementHistory = 3,
            Reconciliation = 4,
            TankAutoCalibration = 5,
            RefuelHistory = 6,
            RiskAssessment = 7,
            Issues = 8,
            Predictor = 9,
            GainLoss = 10,
            StationAnalysis = 11
        }
        #endregion

        #region DeviceDashboard
        public enum DeviceDashboardMainChartView
        {
            Device = 1,
            Shipping = 2,
            Service = 3,
            ServicePool = 4,
            Serviceman = 5
        }
        public enum DeviceDashboardAdditionalChartView
        {
            Consumption = 1,
            Amount = 2,
            BatchesFailure = 3,
            SmsConsumption = 4,
            DeviceInvoice = 5,
            ServiceDetailsAmount = 6,
            ServiceDetailsPercentile = 7,
            AverageCommunicationServiceActivationTime = 8,
            AverageOKOActivationTime = 9,
            FitterTask = 10,
            FitterPackage = 11,
            FitterDepository = 12,
        }
        #endregion

        #region DeviceInstalationPlace
        public enum DeviceInstalationPlace
        {
            Garage = 0,
            CityPole = 1,
            Basement = 2,
            Well = 3,
            Apartments = 4,
            Other = 5,
            Unknown = 6,
        }
        #endregion

        #region DeviceLocationCompassDirection
        public enum DeviceLocationCompassDirection
        {
            North = 0,
            East = 1,
            West = 2,
            South = 3,
        }
        #endregion

        #region DeviceServiceDiagnostic
        public enum DeviceServiceDiagnostic
        {
            SERVICE = 1,
            SCRAPPING = 2,
            REPLACE = 3,
            OPERATING = 4,
            WORKING = 5,
            BEFORE_SERVICE = 7,
            BEFORE_DIAGNOSTIC = 8,
            SCRAPPED = 9,
            REPLACED = 10,
            REPAIR_REFUSED = 11,
            PREVENTIVE_REPLACE = 12,
        }
        #endregion

        #region DeviceState
        public enum DeviceState
        {
            NEW = 1,
            IDLE = 2,
            PASSIVE = 3,
            ACTIVE = 4,
            INSPECTED = 5,
            SERVICED = 6,
            ERASED = 7,
            PRODUCED_NOTWORKING = 8,
            PRODUCED_WORKING = 9,
            SENT = 10,
            AFTER_SERVICE = 11,
            PRODUCED_WRONG_CONF = 12,
            STORED_AFTER_SERVICE = 13,
            REMOVED = 14,
            SOT_RECEIVED = 15,
            PREVENTIVELY_REPLACED = 16,
        }
        #endregion
        #region DeviceStateValidateOption
        public enum DeviceStateValidateOption
        {
            ShippingListNewDevices = 1,
            ShippingListServicedDevices = 2,
            ShippingListIsuDevices = 7,
            ShippingListSoldDevices = 10,
            ShippingListLegalizationDevices = 11,
            ServicePool = 3,
            ServiceList = 4,
            ServiceReplacementDiagnostic = 8,
            ServiceReplacementService = 9,
            PackageFromFitter = 5,
            PackageFromAiut = 6,
        }
        #endregion

        #region DeviceType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DeviceType
        {
            Unknown = 0,

            OLAN_ver_1_0 = 2,
            Licznik_Landis = 3,
            Licznik_JM_Tronik_L1Fn = 4,
            Licznik_JM_Tronik_EVO_3T = 5,
            Licznik_Pozyton_LA4 = 6,
            KDL = 7,
            EAP = 9,
            LAP = 10,
            OKO_2xxx = 11,
            AKAM = 12,
            OLAN_I5 = 13, // radio frame
            ALEVEL_0212 = 14,
            OKO_53xx_Gallus_2002 = 15,
            OKO_5310B_Plytka_B = 16,
            GALLUS_2002_IMR = 17,
            OKO_5111 = 18,
            ACTAR_1011_2 = 19,
            OKO_5150_4100_60xx = 20, // tego u Wojtasa w DB nie ma a jest na IMR02
            ALAND_10xx_1 = 21,
            OKO_PEC_Ciechanow = 22,
            Siemens_2WR5_MBUS = 23,
            KamstrupMultical_C66_MBUS = 24,
            ALUM = 25,
            APULSE_0113 = 26,
            AKAM_3011 = 27,
            AAB_3051 = 28,
            OKO_2xxx_ = 29,
            OKO_ABB_ = 30,
            OKO_5150_4100_00x9 = 31, // tego u Wojtasa w DB nie ma a jest na IMR02
            ARANGE_3051 = 32,
            OKO_5142_Wan_1_0 = 33, // tego u Wojtasa w DB nie ma a jest na IMR02
            AIMS_101x = 35,
            OKO_53xx_Mulitread_Prepaid = 36, // tego u Wojtasa w DB nie ma a jest na AMERIGAS
            OKO_53xx_Mulitread = 37, // tego u Wojtasa w DB nie ma a jest na AMERIGAS
            AMPLI_6860 = 40,
            ARANGE_10xx = 42,
            ACTAR_1122_xxxx_xxx2 = 43,
            APAT_1022 = 44,
            AKFAP_1x1x_2 = 45,
            ACTAR_1030 = 46,
            OKO_5441_C100_slupowe = 47,
            DANFOS_ECL_200_300 = 49,
            AKAM_1022_xx_pozostale_x_xxx3 = 50,
            OKO_5141_4520_0000 = 51,
            AKFAP_13x2_xxxx_xxx3 = 52,
            AKFAP_1xx2_xxxx_xxx1 = 53,
            ACAL_1030_xxxx_xxx0 = 54,
            AKAM_12x2_xx_pozostale_x_xxx0 = 55,
            AKFAP_1030_xxxx_xxx0 = 56,
            AKFAP_1130_0400_7200 = 58,
            AKFAP_1x11_xxxx_xxx0 = 59,
            ATEMP_0122 = 60,
            ALAND_1120_xxxx_xxx1 = 61,
            MacR_G_BAT = 62,
            ALAND_1022_xx0x_xxx0 = 63,
            APULSE_0113_0132_3003 = 64,  // tego u Wojtasa w DB nie ma a jest na AMERIGAS
            PLUM_CPG = 65,
            ACTAR_1222_xxxx_xxx1 = 67,
            OKO_5441 = 68,
            AMBUS_4130 = 69,
            AMBUS_4230 = 70,
            AMPLI_6980_9500 = 71,
            AMPLI_6840 = 72,
            AKAM_1132_xx_pozostale_x_xxx0 = 73,
            AKAM_13x2_xx0x_xxx0 = 74,
            ALAND_11x2_xx0x_xxx0 = 75,
            APAT_11x2_xxx_xxx0 = 76,
            ALAND_1022_xx6x_xxx3 = 77,
            ACTAR_1122_xx2_3x_xxx3 = 78,
            OKO_5142_6100_2007_1_2_ALEVEL = 79,
            OKO_5142_6100_2007_3_6_ALEVEL = 80,
            ACTAR_2010 = 81,
            Shell_OKO_5444 = 82,
            OKO_5441_Multiread = 83,
            OLAN_Prepaid_v2 = 84,
            OLAN_5310_C713 = 85,
            Veeder_Root_TLS350 = 86,
            ARANGE_3051_ = 87,
            AKFAP_12x2_xxxx_xxx0 = 88,
            ALEVEL_0214 = 89,
            Petrovend_Site_Sentinel_2 = 90,
            OKO_5142 = 91,
            AKAM_1422_xxxx_xxx0 = 92,
            AKFAP_1022_xxxx_xxx2 = 93,
            OLAN_53xx = 94, // tego u Wojtasa w DB nie ma, a jest na AMERIGAS
            ADANF = 95,
            AKAM_10x2_xx_pozostale_x_xxx0 = 96,
            OKO_5142_BP_Netherlands = 97,
            OKO_5441_Alevel_Nowa_Ramka = 98,
            ALAND_11x2_xx0x_2 = 99,
            ALAND_11x2_xx2x_2 = 100,
            Gallus_1_ALvl = 101, // tego u Wojtasa w DB nie ma
            Gallus_2_ALvls = 102, // tego u Wojtasa w DB nie ma
            AKAM_13x2_xx6x_0 = 103,
            AKAM_1332_0001_3000 = 104,
            AKAM_1022_xx_pozostale_x_xxx4 = 105,
            AMBUS_4320_0 = 106,
            OKO_5142_BP_SIGNALIX = 107,
            ACTAR_12x2_xxxx_xxx4 = 108,
            APAT_1222_xxx_xx00 = 109,
            APULSE_I010_S010_0210 = 110,
            OKO_5152 = 111,
            ATG = 112,
            ATG_IMRLAN = 113,
            APULSE_0200 = 114,
            AMPLI_Szwecja = 115,
            APLUSE_I270 = 116,
            ALAND_1232_xx6x_xxx1 = 117,
            AMBUS_4030 = 118,
            ALAND_1132_xx6x_xxx1 = 119,
            AKAM_12x2_xx_pozostale_x_xxx1 = 120,
            AMBUS_4510 = 121,
            OKO_I5xx = 122,
            OKO_5500 = 123,
            OKO_5510 = 124,
            AMPLI_6872 = 125,
            OKO_5540 = 126,
            ALEVEL_0300 = 127,
            AMPLI_6871 = 128,
            OKO_5172 = 129,
            OKO_5501 = 130,
            AIMS_1xx2_xxxx_xxx1 = 131, //?? podwojone !!!
            AKAM_15x2_xxxx_xxx0 = 132,
            AMPLI_5501 = 134,
            AKAM_15x2_xxxx_xxx1 = 133,
            APAT_12x2_xxxx_xx02 = 135,
            APAT_12x2_xxxx_xx22 = 136,
            APULSE_0113_131_2_inputs_PRO = 137,
            OLAN_i540 = 138,
            ALEVEL_301_3 = 139,
            ALEVEL_301_4 = 140,
            APULSE_0200_Pumper = 141,
            OKO_I533 = 142, // tego u Wojtasa w DB nie ma, na IMR02 i AMERIGAS jest jako "OKO I5x3"
            APULSE_I5x3 = 143,
            AKAM_14x2_xxxx_xxx1 = 144,
            ACTAR_1122_xx0x_xxx3 = 145,
            ADANF_1010_0 = 146,
            ACAL_1010_1 = 147,
            ALAND_11x2_xx0x_xxx3 = 148,
            //AIMS_1xx2_xxxx_xxx1             = 149, //?? podowjone !!!
            ALAND_12x2_xx0x_xx01 = 150,
            OPTO_0101 = 151,
            ACTAR_1222_xxxx_xxx5 = 152,
            Fuel_Nozzle = 153,
            OLAN_I573_ = 154,
            APAT_12x2_xxxx_xx41 = 155,
            IS = 156,
            ALEVEL_0300_3 = 157, // tego u Wojtasa w DB nie ma, na IMR02 jest jako "ALEVEL 0301-0104-0003"
            OLAN_I573 = 158,
            OKO_5501WL = 159,
            Fuel_Station = 160,
            OKO_S533 = 161,
            ALEVEL_0333 = 162,
            AKFAP = 163, // tego u Wojtasa w DB nie ma, a jest na AMERIGAS
            OLAN_I573_2 = 164,
            ACAL_1013 = 165,
            AKAM_1622_xxxx_7203 = 166,
            OKO_I503_1100_xxxx = 167,
            OKO_55x3_xx0x = 168,
            AMIO_56x1_xxx0 = 169,
            ALAND_12x2_xx6x_xxx0 = 170,
            AKAM_1322_xx3x_xxx0 = 171,
            ACAL_1x11_2 = 172,
            APULSE_i523_A24 = 173,
            APULSE_x373 = 174,
            OKO_x373 = 175,
            OKO_55x3_xx32 = 176,
            OKO_55x3_15v6 = 177,
            OKO_x303_xx64 = 178,
            ACTAR_1011_3 = 179,
            ALEVEL_0333_3 = 180,
            APULSE_x373_TxRx = 181,
            OKO_55x3_xxC0 = 182,
            OKO_55x3_xxA0 = 183,
            AKAM_1011_xxxx_5xx4 = 184,
            OKO_55x3_xx90 = 185,
            AMBUS_4420 = 186,
            OKO_x373_xxB0 = 187,
            OKO_I5x4_xxD0 = 189,
            OKO_x373_xxB1 = 190,
            APULSE_x373_xxA4 = 191,
            OKO_55x3_xxC1 = 192,
            OKO_55x3_xx90CL = 193,
            OKO_x373_xxB2 = 194,
            ARANGE_6070 = 195,
            OKO_I5x4_xxD1 = 196, //zachowuje Konfiguracje
            ACTAR = 197, // tego u Wojtasa w DB nie ma, a jest na AMERIGAS
            OKO_55x3_xxC2 = 198,
            EK_280_IMR_driver = 202,
            OKO_x554_xxD2 = 204,
            OKOi554_xxD2 = 205,
            ARANGE6070_xxL3 = 206,
            DisDoms_PSS5000 = 207,
            OKO_x373_xxB3 = 210,
            ALEVELx3x3_xxx4 = 211,
            OKO55x3_xx91 = 212,
            //213	APAT 13x2-xx30 -> na ImrPROD
            MacR_G_BAT_v2 = 214,
            OKO_55x3_xxC3 = 215,
            EK_220_IMR_driver = 216,
            OKO_55x3_xx92 = 217,
            APULSE_C373_xxS1 = 218,
            OKO_55x3_xx93 = 219,
            OKO_55x3_xxC4 = 220,
            ALEVELx3x3_xx06 = 222,
            OKO_5573_xxE0 = 224,
            OKO_55x3_xx94 = 225,
            OKO_x373_xxB4 = 226,
            OKO_55x3_xxC5 = 227,
            OKO55x3_xx95 = 228,
            OKOi5x5_xxD0 = 229,
            GeniusSlim = 230,
            OKO_55x3_xx96 = 231,
            EK_220_xxA1_IMR_driver = 232,
            OKO_55x3_xxA1 = 233,
            EK_230_xxA1_IMR_driver = 234,
            EK_280_xxA1_IMR_driver = 235,
            OLANi5x5_xxD0 = 236,
            OKO_x3x3_xxB5 = 237,
            Itron_Corus_xxA1_IMR_driver = 238,
            Vector_VTM_G005_A05 = 239,
            EK_220_FE260 = 240,
            OKOi5x5_xxD1 = 241,
            Dresser_xxA1 = 242,
            MiniElcor_xxA1 = 243,
            OKO_55x3_xxA2 = 244,
            Romet_xxA2 = 245,
            SmartGasGenericDevice = 246,
            OKO_55x5_xxC0 = 247,
            APULSE_x373_xxA41 = 248,
            APULSE_x373_xxA5 = 249,
            Flowgas_xxA2 = 250,
            Danfoss_ECL_200_300_C6x_IMR_driver = 251,
            Diehl_Izar_xx = 252,
            OKO_55x3_xxA3 = 253,
            Itron_Corus_xxA2_IMR_driver = 254,
            EDM607AR_01_v01_IMR_driver = 255,
            MiniElcor_xxA2 = 256,
            APULSEWx1x5_xxx1 = 257,
            APULSE_I523 = 258,  // tego u Wojtasa w DB nie ma a jest na IMR02
            IMRModbus = 259, //typ dla uniwersalnego drivera - aby m�c testowac i wykonywa� mapy dla pojedynczych sztuk (bez potrzeby nowego deviceType)
            EK_260_xxA1_IMR_driver = 260,
            EK_205_xxA1_IMR_driver = 261,
            Itron_Cyble = 262,
            Apator_AT_xx = 263,
            B_METERS_xx = 264,
            Kamstru_Multical_21 = 265,
            OKO_x3x3_xxB6 = 266,
            Sensus_Scout = 267,
            ALEVEL_0275 = 268,
            Diehl_RealData_xx_IMR_driver = 269,
            OKOi5x5_xxD2 = 270,
            Diehl_OMS_xx_IMR_driver = 271,
            FuelPrimeCosole_IMR_driver = 272,
            ALEVEL_0275_SIGFOX = 273,
            OKO_5xx5_xxC1 = 274,
            OKO_55x3_xxA3_GNF = 275,
            LandisGyr_G350_IMR_driver = 276,

            IMR_Suite_Operator = 1000,
            IMR_Server = 1001,
            AMPLI_6850_C100 = 1032,
            OKO_5501_3 = 1303,  // tego u Wojtasa w DB nie ma a jest na IMR02
            OPROGRAMOWANIE__Aiut = 1002,
            ADOC6000_stacja_dokujaca = 1003,
            ALEVEL_0202 = 1004,
            OKO_2102 = 1005,
            APULSE_101 = 1006,
            OKO_2103 = 1007,
            ERP_6xxx = 1008,
            CR_21 = 1009,
            CR_22 = 1010,
            EAB_7 = 1011,
            VMW_2 = 1012,
            ARANGE_1030 = 1013,
            ACTAR_101 = 1014,
            AMIO_11x1_xxx0 = 1015,
            Panel_Operatorski = 1016,
            OKO_2104_ = 1017,
            ARANGE_6060_1200 = 1018,
            ARANGE_1021 = 1019,
            OKO_2104 = 1020,
            OKO_5441_x141 = 1021,
            AKFAP_1832 = 1022,
            IMR17 = 1023,
            ATEMP_0500_36F0_0000 = 1024,
            ATEMP_0500_0000_0000 = 1025,
            AMIO_1xx2_xxx0 = 1026,
            IST_0213 = 1027,
            IST_5000 = 1028,
            Kamstrup_Multical_401 = 1029,
            ACD_G10C = 1030,
            AIR_6x50_xxx2 = 1031,
            SZAFKA_TELEMETRYCZNA_IMR22 = 1033,
            INTERGAZ_BK_G1_6 = 1034,
            INTERGAZ_BK_G2_5 = 1035,
            INTERGAZ_BK_G4 = 1036,
            INTERGAZ_BK_G6 = 1037,
            INTERGAZ_BK_G10 = 1038,
            INTERGAZ_BK_G16 = 1039,
            APATOR_LQM_III_K = 1040,
            _02_11 = 1041,
            Modem_Fastrack_Supreme20 = 1042,
            Sierra_Wireless = 1043,
            SITA = 1044,
            IS38_magnetostrictive_probe = 1045,
            IS27_pressure_sensor = 1046,
            WayneController = 1047,
            APULSE_0200_Switcher = 2000,
        }
        #endregion
        #region DeviceTypeClass
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DeviceTypeClass
        {
            OKO = 1,
            RADIO_DEVICE = 2,
            ATG = 3,
            OTHER = 4,
            AMPLI = 5
        }
        #endregion
        #region DeviceTypeGroup
        public enum DeviceTypeGroup
        {
            ALEVELS = 1,
            SIMA_SYNCHRONIZABLE = 2,
            HAVING_CALIBRATION_TABLE = 3,
            HAVING_VOLUME_TABLE = 4,
            HAVING_VALVE_CONTROL = 5,
            HAVING_CODES_TABLE = 6,
            HAVING_READOUT_CONFIGURATION = 9,
            SIMA_ADVANCED_SCHEDULE_CONFIGURATION_V1 = 10,
            SIMA_ADVANCED_SCHEDULE_CONFIGURATION_V2 = 11,
            SIMA_ADVANCED_ANALOG_CONFIGURATION = 12,
            HAVING_KEEP_CONFIGURATION = 13,
            SIMA_ADVANCED_ANALOG_CONFIGURATION_ALARM_UP_DOWN = 14,
            SIMA_ADVANCED_UDI_CONFIGURATION = 15,
            HAVING_AUTO_GPRS_MODE = 16,
            HAVING_I2C = 17,
            SITA_OKO_DEVICES_FOR_PRESSURE = 18,
            LOCATION_DETAILS_AVAILABLE_DEVICES = 19,
            EVC_ELSTER = 20,
            EVC_ITRON = 21,
            EVC_DRESSER = 22,
            EVC_ELGAS = 23,
            EVC_FLOWGAS = 24,
            ARM_PROCESSOR = 25,
            SITA_DEVICES_FOR_WATER = 26,
            SITA_DEVICES_FOR_GAS = 27,
            SITA_DEVICES_FOR_HEAT = 28,
            SITA_DEVICES_FOR_LPG = 29,
            SIMA_ADVANCED_SCHEDULE_CONFIGURATION_DIFF_PROTOCOL = 30,
        }
        #endregion

        #region DeviceTypeProfileStep
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DeviceTypeProfileStep
        {
            RADIO_LISTEN_TIME = 1,
            MODEM_ACTIVATION_SCHEDULE = 2,
            FREQ_SEND_SCHEDULE = 3,
            WL_ACTIVATION = 4,
            ALARM_LEVELS_FOR_SINGLE_ANALOG = 5,
            ALARM_LEVELS_FOR_AVERAGE_VALUES = 6,
            NVR_CONFIGURATION = 7,
            DEAD_AND_LOST_LINKS_CONFIGURATION = 8,
            RAPID_CONFIGURATION = 9,
            ARCHIVE_CONFIGURATION = 10,
            RADIO_ACTIVATION_SCHEDULE = 11,
            CUSTOM_ACTIVATION_SCHEDULE = 12,
        }
        #endregion
        #region DeviceTypeProfileStepKind
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum DeviceTypeProfileStepKind
        {
            OKO_5501 = 1,
            OKO_5501_WL = 2,
            OKO_55x3 = 3,
            OKO_55x3_xxA0 = 4,
            OKO_55x3_xxC5 = 5
        }
        #endregion

        #region DfcModule
        public enum DfcModule
        {
            Unknown = 0,
            DfcAnomalyDetection = 1,
            DfcWeatherClimate = 2,
            DfcConsumption = 3,
            DfcDecisionModule = 4,
            DfcDelivery = 5,
            DfcDeliveryAdvisor = 6,
            DfcDeliveryBalance = 7,
            DfcGoogleDistances = 9,
            DfcLevelEnhancer = 10,
            DfcOutlierRemover = 11,
            DfcPredictionTemperaturePython = 12,
            DfcPredictionLinear = 13,
            DfcPredictionNontelemetric = 14,
            DfcRefuelDetection = 15,
            DfcStock = 16,
            DfcStockBalance = 17,
            DfcWeatherForLocation = 18,
            DfcWeatherForecast = 19,
            DfcWeatherHistory = 20,
            DfcSIR2StockBalance = 21,
            DfcSIR2AnomalyDetection = 22,
            DfcSir2Output = 23,
            DfcSir2AdvOutput = 24,
            DfcSir2 = 25,
            DfcPredictionFuelPython_168 = 26,
            DfcWaterConsumption = 27,
            DfcGLValidation = 28,
            DfcGLComponents = 29,
            DfcGLRefuel = 30,
            DfcPredictionAlarm = 31,
            DfcPredictionFuelPython_Linear = 32,
            DfcPredictionFuelPython_Probabilistic = 33,
            DfcWinDMS = 34,
            DfcEstimationByConsumption = 35,
            DfcACODeliveryAdvisor = 36,
            DfcACODecisionModule = 37,
            DfcPredictionUnited = 38,
            DfcTableExport = 39,
            DfcMultipleMeters = 40,
            DfcCurveCalibration = 41,
            DfcHeatModel = 42
        }
        #endregion
        #region DflModule
        public enum DflModule
        {
            DflMain,
            DflFuelStation,
            DflScadaExport,
            DflWeather,
            DflPrediction,
            DflPredictionNonTelemetric,
            DflDeliveryVerification,
            DflSir,
            DflGoogleMapsApi,
            DflWater,
            DflForecourt,
            DflGainLoss,
            DflHeatmeter,
            DflExternalInterface,
            DflACOModule,
            DflTankParameters,
            DlfCustom,
        }
        #endregion

        #region DispenserPumpType
        public enum DispenserPumpType
        {
            Unknown = 0,
            SuctionPump = 1,
            SubmersiblePump = 2
        }
        #endregion

        #region Distributor

        public enum DistributorImportance
        {
            Small = 1,
            Large = 3,
        }

        public enum DistributorDeviceOwnership
        {
            Property = 1,
            Lease = 2,
            Share = 3,
        }

        public enum DistributorLocationGroup//zwi�zane z DataType.LOCATION_DISTRIBUTOR_GROUP_ID
        {
            Domestic = 1,//Amerigas - domestic, id_location = 342
            Blocks = 2,//Amerigas - bloki, id_location = 160
            Industrial = 3,//Amerigas - przemys�owe, id_location = 222
        }

        #endregion

        #region DWTaskType
        public enum DWTaskType
        {
            Unknown = 0,
            DAExport = 1,
            DACalculate = 2,
            PredictFutureMeasures = 3,
            WeatherForecast = 4,
            EstimateMeasures = 5,
            RecountFromDate = 6,
            IntelligenceDeliveryVerification = 7,
            GoogleDistances = 8,
            IrcSir2LeakDetection = 9,
            IntelligenceProcessMeasuresFromDB = 10,
            ProcessNontelemetric = 11,
            LearnPredictionParameters = 12,
            CalculateGainLoss = 13,
            MissingDataProblemDetection = 14,
            CalculateLostSalesByStockOut = 15,
            CalculateDeliveryAdvices = 16,
            LateDeliveryPreventionInterface = 17,
            ACOMissingDataProblemDetection = 18,
            AnalyzeConsumptionsForPredictorUnitedProfile = 19,
            CalculateDeclaredConsumptionDifference = 20,
        }
        #endregion

        #region EmailSendingMode

        public enum EmailSendingMode
        {
            SMTP = 0,
            DBMail = 1
        }

        #endregion

        #region ExportStatus

        public enum ExportStatus
        {
            Unknown = 0,
            NotStarted = 1,
            Succeded = 2,
            Failed = 3,
            InProgress = 4,
            Validated = 5,
            NotValid = 6,
            Loaded = 7,
        }

        #endregion

        #region FaultCodeSummaryType
        public enum FaultCodeSummaryType
        {
            Minor = 1,// kod b��du prezentacyjny, uwzgledniaj�ce b��dy grube - potrzebny do poprawnych statystyk
            Major = 2// kod b��du serwisowy - potrzebny do sensownej historii z praktycznego punktu widzenia
        }
        #endregion

        #region FaultCodeType
        public enum FaultCodeType
        {
            FaultCode = 1,
            FaultOccurenceRisk = 2,
        }
        #endregion

        #region File

        #region FileType

        public enum FileType
        {
            Unknown = 0,
            Photo = 1,
            ClientSignature = 2,
            FitterSignature = 3,
            CSVLog = 4,
            Protocol = 5,
            AutoGeneratedProtocol = 6,
        }

        #endregion

        #region FileExtension

        public enum FileExtension
        {
            Unknown = 0,
            Pdf = 1,
            Xlsx = 2,
            Xls = 3,
            Csv = 4,
            Jpg = 5,
            Png = 6,
            Bmp = 7,
            Log = 8,
            Sql = 9,
            Rtf = 10,
            Svg = 11,
        }

        #endregion

        #endregion

        #region FilterType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum FilterType
        {
            Unknown = 0,
            AlwaysIgnore = 1,
            OnlyNew = 2,
            OnlyNewWithArchie = 3,
        }
        #endregion

        #region FRAMStatus
        public enum FRAMStatus : byte
        {
            NO_FREE_SLOT = 0xC0,
            UNKNOWN_COMMAND = 0xC1,
            INVALID_COMMAND_PARAMETERS = 0xC2,
            COMMAND_BAD_FORMAT = 0xC3,
            INVALID_OKO_CONFIGURATION = 0xC4,
            NO_FREE_MEMORY = 0xC5,
            LEAK_DETECTED = 0x81,
            TIMEOUT = 0xFF
        }
        #endregion

        #region FuelFillingPointType
        // Typ chlebaka dla metera na stacji
        public enum FuelFillingPointType
        {
            Gravity = 1, // nad zbiornikiem
            Pumper = 2, // kolankowy
        }
        #endregion

        #region GLProblemIDs
        public enum GLProblemIDs
        {
            MissingDispencedGross = 1,
            MissingDispencedNet = 2,
            MissingDispencedRaw = 3,
            MissingDispencedPos = 4,
            MissingDispencedHeat = 5,
            MissingOutcomeGross = 6,
            MissingOutcomeNet = 7,
            MissingDeliveryCalcGross = 8,
            MissingDeliveryCalcNet = 9,
            MissingDeliveryTerminalGross = 10,
            MissingDeliveryTerminalNet = 11,
            MissingHeightMeasure = 12,
            MissingVolumeMeasure = 13,
            MissingTemperatureMeasure = 14,
            NegativeDispencedGross = 15,
            NegativeDispencedNet = 16,
            NegativeDispencedRaw = 17,
            NegativeDispencedPos = 18,
            NegativeDispencedHeat = 19,
            SoldGrossNetRatio = 20,
            SoldGrossGrossRatio = 21,
            NegativeDeliveryCalcGross = 22,
            NegativeDeliveryCalcNet = 23,
            NegativeDeliveryTerminalGross = 24,
            NegativeDeliveryTerminalNet = 25,
            DeliveryCalcTerminalGrossRatio = 26,
            DeliveryCalcTerminalNetRatio = 27,
            OutcomeDontMetHeight = 28,
            OutcomeGrossNetRatio = 29,
            OutcomeSoldGrossRatio = 30,
            OutcomeSoldNetRatio = 31,
            InconsistentRefuels = 32,
            RefuelHeightMissing = 33,
            RefuelVolumeMissing = 34,
            RefuelTemperatureMissing = 35
        }
        #endregion

        #region ImportStatus

        public enum ImportStatus
        {
            Unknown = 0,
            NotStarted = 1,
            Succeded = 2,
            Failed = 3,
            InProgress = 4,
            Validated = 5,
            NotValid = 6,
            Loaded = 7,
        }

        #endregion

        #region ImrServerTableSynchronizationDirection

        public enum ImrServerTableSynchronizationDirection
        {
            Undefined = 0,
            Download = 1,
            Upload = 2,
        }

        #endregion

        #region ImrServerTableSynchronizationType

        public enum ImrServerTableSynchronizationType
        {
            AllContent = 1,
            Changes = 2,
            ParticularElements = 3,
        }

        #endregion

        #region ImrServerVersion
        /// <summary>
        /// ImrServerVersion from table IMR_SERVER.
        /// </summary>
        public enum ImrServerVersion
        {
            IMR3 = 3,
            IMR4 = 4
        }
        #endregion

        #region IMRServerReleaseVersion
        /// <summary>
        /// Release versions. Add new const after new Release.
        /// </summary>
        public class IMRServerReleaseVersion
        {
            /// <summary>
            /// Release 4.3.16326.1 from 2016-11-21
            /// </summary>
            public const string Release_4_3_16326_1 = "4.3.16326.1";
            /// <summary>
            /// Release 4.3.16356.1 from 2016-12-21
            /// </summary>
            public const string Release_4_3_16356_1 = "4.3.16356.1";
        }
        #endregion

        #region ItemStatus
        public enum ItemStatus
        {
            Undefined = 0,
            New = 1,
            Existing = 2,
            Modified = 3,
            ToBeDeleted = 8,
            Deleted = 9,
        }
        #endregion

        #region IRCDecision
        public enum IRCDecision
        {
            Invalid = 0,
            Leak = 1,
            Tight = 2,
            Inconclusive = 3
        }
        #endregion

        #region IRCDataSource
        public enum IRCDataSource
        {
            Undefined = 0,
            Auto = 1,
            Manual = 2,
            AutoManual = 3,
            ManualAuto = 4
        }
        #endregion

        #region JCI
        public class JCI
        {
            #region FILE_TYPE
            public enum FILE_TYPE
            {
                WORK_ORDER = 0,
                WORT_ORDER_STATUS_UPDATE = 1
            }
            #endregion
        }
        #endregion

        #region Language
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum Language
        {
            Unknown = 0,
            Polish = 1,
            English = 2,
            Danish = 3,
            Italian = 4,
            Russian = 5,
            Hebrew = 6,
            Czech = 7,
            Serbian = 8,
            Spanish = 9,
            Default = English,
        }
        #endregion

        #region LocationState
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum LocationState
        {
            UNKNOWN = -1,
            UNDEFINED = 0,
            NEW = 1,
            SCHEDULED = 2,
            INTERVENTION = 3,
            PENDING = 4,
            OPERATIONAL = 5,
            SUSPENDED = 6,
            UNUSED = 7,
            DELETED = 8,
            APPROVED = 9,
            READY = 10,
            DISASSEMBLING = 11,
            ASSEMBLED = 12,
            SOLD = 13,
            CANCELED = 14,
            OWNER_CHANGE = 15,
            IN_TRANFER = 16,
            CONTRACT_CHANGE = 17,
            CONTRACT_REWRITE = 18,
            RECONSTRUCTION = 19,
            CONSTRUCTION = 20,
        }
        #endregion

        #region LocationType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum LocationType
        {
            Unknown = 0,
            CustomerLocation = 1,
            DepositoryLocation = 2,
            ServiceLocation = 3,
            GroupingLocation = 4,
            RelatedLocation = 5,
            DraftLocation = 6,
            UnidentifiedLocation = 7,
            PrepaidAreaLocation = 8,
            PatternLocation = 9,
            DestinationMagazine = 10,
            Truck = 11,
            FuelTerminal = 12,
            WeatherStation = 13,
            NonTelemetricLocation = 14,
            ActionGroupingLocation = 15,
            CountyCapital = 16,
            AreaLocation = 17,
            SgmGroupingLocationType1 = 18,
            SgmGroupingLocationType2 = 19,
            SgmGroupingLocationType3 = 20,
            SgmGroupingLocationType4 = 21,
            SgmGroupingLocationType5 = 22,
            HubLocation = 23,
        }
        #endregion

        #region MapMarkerTextType
        // Dodal: sdudzik
        // Typ tekstu pojawiajacego sie w markerze na mapie
        public enum MapMarkerTextType
        {
            MaxTruckSize = 1,
            TotalTanks = 2,
        }
        #endregion

        #region MeasureValueStatus
        // UWAGA: zmiany wprowadzac rowniez w BD! - tabela DATA_ARCH_TRASH_STATUS
        public enum MeasureValueStatus
        {
            Unknown = 0,
            Ok = 1,
            Error = 2,
            WanError = 3,
            New = 4,
            Uncertain = 5,
            UnknownMeter = 6,
            DeviceDriverError = 7,

            NotFound = 200,
            AccessDenied = 201,
            ReadOnly = 202,
            WrongSize = 203,
            ParameterIncorrect = 204,
            NotSupported = 205,
            BusBusy = 206,
            EEpromFail = 207,
            NotTable = 208,
            TableOutOfRange = 209,
            OrderNotCorrect = 210,
            BadState = 211,
            WriteError = 212,
            TableAccessError = 213,
            ReadError = 214,
            FtpError = 215,
            UnknownError = 220
        }
        #endregion DeviceParamStatus

        #region MeterType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum MeterType
        {
            Unknown = 0,
            FuelTank = 1,
            Gallus2002 = 2,
            LPGTank = 3,
            ElsterBKG4 = 4,
            Actaris_CF_50 = 5,
            Actaris_CF_51 = 6,
            Ultraheat_2WR4 = 7,
            Ultraheat_2WR5 = 8,
            LQM = 9,
            Multical_601 = 10,
            Multical_66CDE = 11,
            LQM_III = 12,
            Multical_III = 13,
            Sonometer_1000 = 14,
            Uknown_watermeter = 15,
            LQM_II = 16,
            LEC_5 = 17,
            Aquatherm_Supercal_432 = 18,
            Landis_Gyr_WSD = 19,
            Aquatherm_Supercal_531 = 20,
            Antap_Hydrometer_Sharky_772_773 = 21,
            Multical_401 = 22,
            WSD4 = 23,
            Landis_GYR = 24,
            Actaris_CF_Combi = 25,
            Actaris_CF_Echo = 26,
            LEC_4 = 27,
            PolluStat_E_v2 = 28,
            Multical_Compact = 29,
            Multical_402 = 30,
            Gasmeter = 31,
            Siemens_UH_50 = 32,
            Aquatherm_Supercal_430 = 33,
            Aquatherm_Supercal_431 = 34,
            Danfos_INFOCAL_5 = 35,
            PolluStat_E_Sensus = 36,
            Danfos_773 = 37,
            Danfos_Sonometer_1000 = 38,
            Multical_Kamstrup_67C = 39,
            Aquatherm_Supercal_439 = 40,
            Thermiflu_T_Schlumberger = 41,
            UHA_50 = 42,
            FuelNozzle = 43,
            DN15 = 44,
            DN20 = 45,
            DN25 = 46,
            DN32 = 47,
            EEM_C = 48,
            ACTARIS_RF1_G25 = 49,
            Metronic_3 = 50,
            Hydrosplit_R_21 = 51,
            Hydrosplit = 52,
            Schlumberger_CF100 = 53,
            LEC_3 = 54,
            GALLUS_2000 = 55,
            METRIX = 56,
            RAYCHEM = 57,
            Compartment = 58,
            Dresser = 59,
            Powogaz = 60,
            Actaris_CF_55 = 61,
            PolluCom = 62,
            Itron_G16 = 63,
            Itron_G25 = 64,
            Itron_G40 = 65,
            Actaris_G16 = 66,
            Actaris_G40 = 67,
            Elster_G16 = 68,
            Elster_G25 = 69,
            Elster_G40 = 70,
            Schlumberger_G16 = 71,
            Schlumberger_G25 = 72,
            Schlumberger_G40 = 73,
            Pollustat_E_Sensus_MBUS = 74,
            Pressure_sensor = 75,
            Slum_shut = 76,
            TankInstallation = 77,
            INTERGAZ_BK_G1_6 = 78,
            INTERGAZ_BK_G2_5 = 79,
            INTERGAZ_BK_G4 = 80,
            INTERGAZ_BK_G6 = 81,
            INTERGAZ_BK_G10 = 82,
            INTERGAZ_BK_G16 = 83,
            INTERGAZ_BK_G25 = 84,
            JS_130_10_NK = 85,
            JS_130_3_5_NK = 86,
            JS_130_6_NK = 87,
            JS_90_2_5_NK = 88,
            JS_90_4_NK = 89,
            Elster_G4 = 90,
            Elster_G10 = 91,
            Elster_G6 = 92,
            Apator_FAUN = 93,
            ItronG100 = 94,
            BottleSwitcher = 95,
            ELSTER_EK_280 = 100,
            Dresser_MRD0450B4D = 101,
            Jeavons_U100 = 102,
            Krom_Schroder_MDK16 = 103,
            Krom_Schroder_MDK40 = 104,
            Jeavons_BK_G10 = 105,
            Dresser_MRD0310B4D = 106,
            Dresser_MTD0100VF = 107,
            ELSTER_BK_G1_6 = 108,
            Rotary_Gasmeter = 109,
            Schlumberger_R5 = 110,
            ELSTER_EK_220 = 111,
            Actaris_G65 = 112,
            Elster_G100 = 113,
            Elster_G65 = 114,
            Elektrometal = 115,
            ACTARIS_G10 = 116,
            ITRON_G10 = 117,
            METRIX_G10N = 118,
            METRIX_G10 = 119,
            METRIX_G65L = 120,
            METRIX_G25 = 121,
            METRIX_G16 = 122,
            METRIX_G40 = 123,
            I2C_PRESSURE_TEMPERATURE = 124,
            I2C_PRESSURE = 125,
            I2C_TEMPERATURE = 126,
            GENERAL_DIGITAL_INPUT = 127,
            TECLAB_Genius_Slim = 128,
            ELSTER_EK_230 = 129,
            Elster_MARLEY_II_HWM = 130,
            ItronCorus = 131,
            Elcor = 132,
            Romet = 133,
            Flowgas = 134,
            Danfoss = 135,
            Diehl_Izar = 136,
            Itron_G1_6 = 137,
            Itron_G2_5 = 138,
            Itron_G4 = 139,
            Itron_G6 = 140,
            ELSTER_EK_260 = 141,
            ELSTER_EK_205 = 142,
            Apator_AT_WMBUS = 143,
            B_Meters = 144,
            Kamstrup_Multical_21 = 145,
            Heat_Controller = 146, // Jest tyle �e utrzymanie stwierdzi�o �e chc� 1 typ metera...
            Itron_Cyble_Sensor = 147,
            ACTARIS_G100 = 148,
            ACTARIS_G160 = 149,
            ACTARIS_G25 = 150,
            ACTARIS_G4 = 151,
            ACTARIS_G40_6_DIAL = 152,
            ACTARIS_G4SC = 153,
            ACTARIS_G65_6_DIAL = 154,
            ACTARIS_G65_7_DIAL = 155,
            ACTARIS_MDA100 = 156,
            ACTARIS_MDA16 = 157,
            ACTARIS_MDA160 = 158,
            ACTARIS_MDA160W = 159,
            ACTARIS_MDA16W = 160,
            ACTARIS_MDA25 = 161,
            ACTARIS_MDA25W = 162,
            ACTARIS_MDA40 = 163,
            ACTARIS_MDA40W = 164,
            ACTARIS_MDA65 = 165,
            ACTARIS_MDA65W = 166,
            ACTARIS_MRA1000E = 167,
            ACTARIS_MRA1000SE = 168,
            ACTARIS_MRA100B = 169,
            ACTARIS_MRA100PB = 170,
            ACTARIS_MRA160C = 171,
            ACTARIS_MRA160PC = 172,
            ACTARIS_MRA200C = 173,
            ACTARIS_MRA250C_8_DIAL = 174,
            ACTARIS_MRA300D = 175,
            ACTARIS_MRA300R = 176,
            ACTARIS_MRA400D = 177,
            ACTARIS_MRA650E = 178,
            ACTARIS_MRA65B = 179,
            ACTARIS_MTA1000TE = 180,
            ACTARIS_MTA2500F = 181,
            ACTARIS_MTA400R = 182,
            ACTARIS_MTA400TC = 183,
            ACTARIS_SC6 = 184,
            ACTARIS_TALEXUS = 185,
            COMMON_CGR_01 = 186,
            DELTA_MRA1000SE = 187,
            DRESSER_1_5M125 = 188,
            DRESSER_102M125 = 189,
            DRESSER_11M125 = 190,
            DRESSER_15CT = 191,
            DRESSER_16M125 = 192,
            DRESSER_16MT = 193,
            DRESSER_23M125 = 194,
            DRESSER_23MT = 195,
            DRESSER_38M125 = 196,
            DRESSER_38MT = 197,
            DRESSER_3M125 = 198,
            DRESSER_3MT = 199,
            DRESSER_56M125 = 200,
            DRESSER_5M125 = 201,
            DRESSER_5MT = 202,
            DRESSER_7M125 = 203,
            DRESSER_7MT = 204,
            DRESSER_G100 = 205,
            DRESSER_G1000 = 206,
            DRESSER_G16 = 207,
            DRESSER_G160 = 208,
            DRESSER_G25 = 209,
            DRESSER_G2500DN250 = 210,
            DRESSER_G250DN100 = 211,
            DRESSER_G40 = 212,
            DRESSER_G65 = 213,
            DRESSER_INSTECH_METERMAN_184_IMPERIAL = 214,
            DRESSER_INSTECH_METERMAN_184_METRIC = 215,
            DRESSER_INSTECH_METERMAN_292 = 216,
            DRESSER_INSTECH_METERMAN_292_100 = 217,
            DRESSER_MICRO_197_6_DIAL_IMPERIAL = 218,
            DRESSER_MICRO_197_6_DIAL_METRIC = 219,
            DRESSER_MICRO_197_7_DIAL_IMPERIAL = 220,
            DRESSER_MICRO_197_7_DIAL_METRIC = 221,
            DRESSER_MICRO_197_8_DIAL_IMPERIAL = 222,
            DRESSER_MICRO_197_8_DIAL_METRIC = 223,
            DRESSER_MRD0022A1M = 224,
            DRESSER_MRD0025B4B = 225,
            DRESSER_MRD0040B4B = 226,
            DRESSER_MRD0042A1M = 227,
            DRESSER_MRD0065B4B = 228,
            DRESSER_MRD0085A1B = 229,
            DRESSER_MRD0085A1P = 230,
            DRESSER_MRD0100B4B = 231,
            DRESSER_MRD0100B4P = 232,
            DRESSER_MRD0141A1C = 233,
            DRESSER_MRD0141A1Q = 234,
            DRESSER_MRD0160B4C = 235,
            DRESSER_MRD0200A1C = 236,
            DRESSER_MRD0200A1Q = 237,
            DRESSER_MRD0250B4C = 238,
            DRESSER_MRD0250B4D = 239,
            DRESSER_MRD0310A1D = 240,
            DRESSER_MRD0310A1R = 241,
            DRESSER_MRD0450A1D = 242,
            DRESSER_MRD0450A1R = 243,
            DRESSER_MRD0650A1E = 244,
            DRESSER_MRD0650A1S = 245,
            DRESSER_MRD0650B4D = 246,
            DRESSER_MRD0650B4E = 247,
            DRESSER_MRD1075A1E = 248,
            DRESSER_MRD1075A1S = 249,
            DRESSER_MRD1075B4E = 250,
            DRESSER_MRD1585A1N = 251,
            DRESSER_MRD1600B4F = 252,
            DRESSER_MTD01000VE = 253,
            DRESSER_MTD01600VG = 254,
            DRESSER_MTD02500VF = 255,
            DRESSER_MTD10000VK = 256,
            ELSTER_BK_G40E_MDK_65 = 257,
            ELSTER_INSTROMET_NFC_500 = 258,
            ELSTER_INSTROMET_NFC_510 = 259,
            ELSTER_MDK16 = 260,
            ELSTER_RVG_250 = 261,
            EUROMETERS_E6 = 262,
            EUROMETERS_G4 = 263,
            G4SGENERICGAS = 264,
            GEORGE_WILSON_E6 = 265,
            GEORGE_WILSON_G4 = 266,
            GEORGE_WILSON_U100_R5 = 267,
            GEORGE_WILSON_U16 = 268,
            GEORGE_WILSON_U25 = 269,
            GEORGE_WILSON_U25_R5 = 270,
            GEORGE_WILSON_U40 = 271,
            GEORGE_WILSON_U40_R5 = 272,
            GEORGE_WILSON_U6 = 273,
            GEORGE_WILSON_U6_R5 = 274,
            GEORGE_WILSON_U65 = 275,
            GEORGE_WILSON_U65_R5 = 276,
            GOLDI_TURBINE_200MM = 277,
            HOLMES_CONNERSVILLE_6X18 = 278,
            HOLMES_IN_LINE_310 = 279,
            IGA_11M_6_DIAL = 280,
            IGA_16M = 281,
            IGA_25M = 282,
            IGA_3_5M = 283,
            IGA_AL800 = 284,
            IGA_G16 = 285,
            IGA_G25 = 286,
            IGA_G40 = 287,
            IGA_G65 = 288,
            INSTROMEC_G100 = 289,
            INSTROMEC_G160 = 290,
            INSTROMEC_G160DN100 = 291,
            INSTROMEC_G25 = 292,
            INSTROMEC_G250 = 293,
            INSTROMEC_G250DN100 = 294,
            INSTROMEC_G40 = 295,
            INSTROMEC_G400DN100 = 296,
            INSTROMEC_G65 = 297,
            INSTROMEC_Q7510_METRIC = 298,
            INSTROMET_333_7_DIAL_IMPERIAL = 299,
            INSTROMET_333_7_DIAL_METRIC = 300,
            INSTROMET_333_8_DIAL_IMPERIAL = 301,
            INSTROMET_333_8_DIAL_METRIC = 302,
            INSTROMET_I_50 = 303,
            INVENSYS_G4 = 304,
            ITRON_G160 = 305,
            ITRON_G250 = 306,
            ITRON_G40_6_DIAL = 307,
            ITRON_G40_7_DIAL = 308,
            ITRON_G400 = 309,
            ITRON_G65_6_DIAL = 310,
            ITRON_MDA100 = 311,
            ITRON_MDA16 = 312,
            ITRON_MDA160 = 313,
            ITRON_MDA160W = 314,
            ITRON_MDA16W = 315,
            ITRON_MDA25 = 316,
            ITRON_MDA25W = 317,
            ITRON_MDA40 = 318,
            ITRON_MDA40W = 319,
            ITRON_MDA65 = 320,
            ITRON_MDA65W = 321,
            ITRON_MRA100PP = 322,
            ITRON_MRA1600PF = 323,
            ITRON_MRA160C = 324,
            ITRON_MRA200C = 325,
            ITRON_MRA200Q = 326,
            ITRON_MRA300D = 327,
            ITRON_MRA400D = 328,
            ITRON_MRA450D = 329,
            ITRON_MRA650E = 330,
            ITRON_MZ50 = 331,
            ITRON_SC6 = 332,
            JEAVONS_G1000DN150 = 333,
            JEAVONS_G100DN80_6_DIAL = 334,
            JEAVONS_G100DN80_7_DIAL = 335,
            JEAVONS_G160_7_DIAL = 336,
            JEAVONS_G160DN80 = 337,
            JEAVONS_G250 = 338,
            JEAVONS_G250DN100 = 339,
            JEAVONS_G65 = 340,
            JEAVONS_Q100 = 341,
            JEAVONS_Q1000_IMPERIAL = 342,
            JEAVONS_Q160 = 343,
            JEAVONS_Q1600_METRIC = 344,
            JEAVONS_Q400_METRIC = 345,
            JEAVONS_Q65 = 346,
            JEAVONS_U100_R5 = 347,
            JEAVONS_U16_R5 = 348,
            JEAVONS_U25_R5 = 349,
            JEAVONS_U40_R5 = 350,
            JEAVONS_U65_R5 = 351,
            KROMSCHROEDER_G10 = 352,
            KROMSCHROEDER_G100 = 353,
            KROMSCHROEDER_G16 = 354,
            KROMSCHROEDER_G25 = 355,
            KROMSCHROEDER_G4 = 356,
            KROMSCHROEDER_G40 = 357,
            KROMSCHROEDER_G65 = 358,
            KROMSCHROEDER_MDK100 = 359,
            KROMSCHROEDER_MDK160 = 360,
            KROMSCHROEDER_MDK25 = 361,
            KROMSCHROEDER_MDK65 = 362,
            KROMSCHROEDER_METKR = 363,
            MAGNOL_G4 = 364,
            MAGNOL_G40 = 365,
            MAGNOL_G65 = 366,
            METRIX_G4 = 367,
            PARKINSON_COWAN_200T_C = 368,
            PARKINSON_COWAN_3000T_C = 369,
            PARKINSON_COWAN_700T_C = 370,
            PARKINSON_COWAN_SC6 = 371,
            PARKINSON_COWAN_U100 = 372,
            PARKINSON_COWAN_U100_R5 = 373,
            PARKINSON_COWAN_U16 = 374,
            PARKINSON_COWAN_U16_R5 = 375,
            PARKINSON_COWAN_U160 = 376,
            PARKINSON_COWAN_U25 = 377,
            PARKINSON_COWAN_U25_R5 = 378,
            PARKINSON_COWAN_U40 = 379,
            PARKINSON_COWAN_U40_R5 = 380,
            PARKINSON_COWAN_U6 = 381,
            PARKINSON_COWAN_U6_R5 = 382,
            PARKINSON_COWAN_U65 = 383,
            PARKINSON_COWAN_U65_R5 = 384,
            PC_COMPTEURS_4000_300 = 385,
            PC_COMPTEURS_800_150 = 386,
            PC_COMPTEURS_D14_7_DIAL = 387,
            PC_COMPTEURS_D14_8_DIAL = 388,
            PC_COMPTEURS_D20_8_DIAL = 389,
            PC_COMPTEURS_D5_6 = 390,
            PC_COMPTEURS_G160 = 391,
            QUANTO = 392,
            RMG_MTP00160LC = 393,
            ROMET_ECM11000 = 394,
            ROMET_G160 = 395,
            ROMET_MRR0100GB = 396,
            ROMET_MRR0650GD = 397,
            ROMET_RG1000 = 398,
            ROMET_RG11000 = 399,
            ROMET_RG1500 = 400,
            ROMET_RG16000 = 401,
            ROMET_RG2000 = 402,
            ROMET_RG3000 = 403,
            ROMET_RG5000 = 404,
            ROMET_RG7000 = 405,
            ROMET_RM1000_7_DIAL = 406,
            ROMET_RM1000_8_DIAL = 407,
            ROMET_RM11000_7_DIAL = 408,
            ROMET_RM11000_8_DIAL = 409,
            ROMET_RM2000_7_DIAL = 410,
            ROMET_RM2000_8_DIAL = 411,
            ROMET_RM3000_7_DIAL = 412,
            ROMET_RM3000_8_DIAL = 413,
            ROMET_RM5000_7_DIAL = 414,
            ROMET_RM5000_8_DIAL = 415,
            ROMET_RM7000_7_DIAL = 416,
            ROMET_RM7000_8_DIAL = 417,
            SCHLUMBERGER_ACTARIS_CORUS = 418,
            SCHLUMBERGER_D0_8 = 419,
            SCHLUMBERGER_D1_5 = 420,
            SCHLUMBERGER_D11_8_DIAL = 421,
            SCHLUMBERGER_D11_9_DIAL = 422,
            SCHLUMBERGER_D16 = 423,
            SCHLUMBERGER_D23 = 424,
            SCHLUMBERGER_D3_8_DIAL = 425,
            SCHLUMBERGER_D3_9_DIAL = 426,
            SCHLUMBERGER_D5 = 427,
            SCHLUMBERGER_D5_8_DIAL = 428,
            SCHLUMBERGER_D5_9_DIAL = 429,
            SCHLUMBERGER_D7_8_DIAL = 430,
            SCHLUMBERGER_D7_9_DIAL = 431,
            SCHLUMBERGER_D88 = 432,
            SCHLUMBERGER_D9_9_DIAL = 433,
            SCHLUMBERGER_F35 = 434,
            SCHLUMBERGER_F56 = 435,
            SCHLUMBERGER_F8_8 = 436,
            SCHLUMBERGER_F88 = 437,
            SCHLUMBERGER_G10 = 438,
            SCHLUMBERGER_G100 = 439,
            SCHLUMBERGER_G1000TZ200 = 440,
            SCHLUMBERGER_G4 = 441,
            SCHLUMBERGER_G40_6_DIAL = 442,
            SCHLUMBERGER_G40_7_DIAL = 443,
            SCHLUMBERGER_G65_6_DIAL = 444,
            SCHLUMBERGER_G65_7_DIAL = 445,
            SCHLUMBERGER_MDA100 = 446,
            SCHLUMBERGER_MDA100W = 447,
            SCHLUMBERGER_MDA16 = 448,
            SCHLUMBERGER_MDA160 = 449,
            SCHLUMBERGER_MDA160W = 450,
            SCHLUMBERGER_MDA16W = 451,
            SCHLUMBERGER_MDA25 = 452,
            SCHLUMBERGER_MDA25W = 453,
            SCHLUMBERGER_MDA40 = 454,
            SCHLUMBERGER_MDA40W = 455,
            SCHLUMBERGER_MDA65 = 456,
            SCHLUMBERGER_MDA65W = 457,
            SCHLUMBERGER_MRA1000E = 458,
            SCHLUMBERGER_MRA100B = 459,
            SCHLUMBERGER_MRA100PB = 460,
            SCHLUMBERGER_MRA160C = 461,
            SCHLUMBERGER_MRA200C = 462,
            SCHLUMBERGER_MRA250C = 463,
            SCHLUMBERGER_MRA25A = 464,
            SCHLUMBERGER_MRA300D = 465,
            SCHLUMBERGER_MRA400D = 466,
            SCHLUMBERGER_MRA400PD = 467,
            SCHLUMBERGER_MRA40A = 468,
            SCHLUMBERGER_MRA650E = 469,
            SCHLUMBERGER_MTA1000E = 470,
            SCHLUMBERGER_MTA4000G = 471,
            SCHLUMBERGER_U100 = 472,
            SCHLUMBERGER_U100_R5 = 473,
            SCHLUMBERGER_U16 = 474,
            SCHLUMBERGER_U16_R5 = 475,
            SCHLUMBERGER_U160_IMPERIAL = 476,
            SCHLUMBERGER_U160_METRIC = 477,
            SCHLUMBERGER_U160_R5 = 478,
            SCHLUMBERGER_U25 = 479,
            SCHLUMBERGER_U25_R5 = 480,
            SCHLUMBERGER_U40 = 481,
            SCHLUMBERGER_U40_R5 = 482,
            SCHLUMBERGER_U6 = 483,
            SCHLUMBERGER_U6_R5 = 484,
            SCHLUMBERGER_U65 = 485,
            SCHLUMBERGER_U65_R5 = 486,
            SENSUS_U6 = 487,
            SIEMENS_E6 = 488,
            T_GLOVER_700T_C = 489,
            TACAR_C201_P = 490,
            TACAR_GVC81 = 491,
            TECHNOCORR = 492,
            THORN_EMI_U16 = 493,
            THORN_EMI_U40 = 494,
            THORN_EMI_U6 = 495,
            UGI_100T_C = 496,
            UGI_200T_C = 497,
            UGI_G4 = 498,
            UGI_MDU100W = 499,
            UGI_MDU160W = 500,
            UGI_MDU16W = 501,
            UGI_MDU25W = 502,
            UGI_MDU40W = 503,
            UGI_MDU65W = 504,
            UGI_RG100 = 505,
            UGI_RG1000 = 506,
            UGI_RG160 = 507,
            UGI_RG250 = 508,
            UGI_RG400 = 509,
            UGI_RG65 = 510,
            UGI_RG650 = 511,
            UGI_U100_IMPERIAL = 512,
            UGI_U100_METRIC = 513,
            UGI_U100_R5 = 514,
            UGI_U16_IMPERIAL = 515,
            UGI_U16_METRIC = 516,
            UGI_U16_R5 = 517,
            UGI_U160_IMPERIAL = 518,
            UGI_U160_METRIC = 519,
            UGI_U160_R5 = 520,
            UGI_U25_IMPERIAL = 521,
            UGI_U25_METRIC = 522,
            UGI_U25_R5 = 523,
            UGI_U40_IMPERIAL = 524,
            UGI_U40_METRIC = 525,
            UGI_U40_R5 = 526,
            UGI_U6_IMPERIAL = 527,
            UGI_U6_METRIC = 528,
            UGI_U6_R5 = 529,
            UGI_U65_IMPERIAL = 530,
            UGI_U65_METRIC = 531,
            UGI_U65_R5 = 532,
            ACTARIS_11M = 533,
            ACTARIS_DELTA_G16 = 534,
            ACTARIS_G250 = 535,
            ACTARIS_G40_7_DIAL = 536,
            ACTARIS_G400 = 537,
            ACTARIS_G650 = 538,
            ACTARIS_MDA100W = 539,
            ACTARIS_MDA25_IMPERIAL = 540,
            ACTARIS_MDA25_METRIC = 541,
            ACTARIS_MDA65_IMPERIAL = 542,
            ACTARIS_METAC = 543,
            ACTARIS_MRA100PP = 544,
            ACTARIS_MRA1600PF = 545,
            ACTARIS_MRA1600PN = 546,
            ACTARIS_MRA160PQ = 547,
            ACTARIS_MRA200Q = 548,
            ACTARIS_MRA2500PG = 549,
            ACTARIS_MRA2500PO = 550,
            ACTARIS_MRA250C = 551,
            ACTARIS_MRA250C_7_DIAL = 552,
            ACTARIS_MRA250Q = 553,
            ACTARIS_MRA25A = 554,
            ACTARIS_MRA400PD = 555,
            ACTARIS_MRA400PR = 556,
            ACTARIS_MRA40A = 557,
            ACTARIS_MRA450D = 558,
            ACTARIS_MRA650SE = 559,
            ACTARIS_MTA10000J = 560,
            ACTARIS_MTA10000TJ = 561,
            ACTARIS_MTA10000TK = 562,
            ACTARIS_MTA1000E = 563,
            ACTARIS_MTA1000F = 564,
            ACTARIS_MTA1000FE = 565,
            ACTARIS_MTA1000S = 566,
            ACTARIS_MTA1000TF = 567,
            ACTARIS_MTA100TB = 568,
            ACTARIS_MTA1600E = 569,
            ACTARIS_MTA1600F = 570,
            ACTARIS_MTA1600FF = 571,
            ACTARIS_MTA1600G = 572,
            ACTARIS_MTA1600N = 573,
            ACTARIS_MTA1600TE = 574,
            ACTARIS_MTA1600TF = 575,
            ACTARIS_MTA1600TG = 576,
            ACTARIS_MTA160C = 577,
            ACTARIS_MTA160TC = 578,
            ACTARIS_MTA2500FG = 579,
            ACTARIS_MTA2500G = 580,
            ACTARIS_MTA2500H = 581,
            ACTARIS_MTA2500O = 582,
            ACTARIS_MTA2500TF = 583,
            ACTARIS_MTA2500TG = 584,
            ACTARIS_MTA2500TH = 585,
            ACTARIS_MTA250C = 586,
            ACTARIS_MTA250D = 587,
            ACTARIS_MTA250FC = 588,
            ACTARIS_MTA250Q = 589,
            ACTARIS_MTA250TC = 590,
            ACTARIS_MTA250TD = 591,
            ACTARIS_MTA4000FG = 592,
            ACTARIS_MTA4000FH = 593,
            ACTARIS_MTA4000G = 594,
            ACTARIS_MTA4000H = 595,
            ACTARIS_MTA4000J = 596,
            ACTARIS_MTA4000O = 597,
            ACTARIS_MTA4000T = 598,
            ACTARIS_MTA4000TG = 599,
            ACTARIS_MTA4000TH = 600,
            ACTARIS_MTA4000TJ = 601,
            ACTARIS_MTA400C = 602,
            ACTARIS_MTA400D = 603,
            ACTARIS_MTA400FD = 604,
            ACTARIS_MTA400TD = 605,
            ACTARIS_MTA6500FJ = 606,
            ACTARIS_MTA6500H = 607,
            ACTARIS_MTA6500J = 608,
            ACTARIS_MTA6500TH = 609,
            ACTARIS_MTA6500TJ = 610,
            ACTARIS_MTA6500TK = 611,
            ACTARIS_MTA6500U = 612,
            ACTARIS_MTA650D = 613,
            ACTARIS_MTA650E = 614,
            ACTARIS_MTA650FE = 615,
            ACTARIS_MTA650TD = 616,
            ACTARIS_MTA650TE = 617,
            ACTARIS_MZ80 = 618,
            ACTARIS_TZ100G160 = 619,
            ACTARIS_TZ100G250 = 620,
            ACTARIS_TZ100G400 = 621,
            ACTARIS_TZ150G1000 = 622,
            ACTARIS_TZ150G400 = 623,
            ACTARIS_TZ150G650 = 624,
            ACTARIS_TZ200G1000 = 625,
            ACTARIS_TZ200G1600 = 626,
            ACTARIS_TZ200G650 = 627,
            ACTARIS_TZ250G1000 = 628,
            ACTARIS_TZ250G1600 = 629,
            ACTARIS_TZ250G2500 = 630,
            ACTARIS_TZ300G1600 = 631,
            ACTARIS_TZ300G2500 = 632,
            ACTARIS_TZ300G4000 = 633,
            ACTARIS_TZ400G2500 = 634,
            ACTARIS_TZ400G4000 = 635,
            ACTARIS_TZ400G6500 = 636,
            ACTARIS_TZ80G100 = 637,
            ACTARIS_TZ80G160 = 638,
            ACTARIS_TZ80G250 = 639,
            ACTARIS_U25 = 640,
            ACTARIS_U6 = 641,
            ACTARIS_U6_IMPERIAL = 642,
            ACTARIS_U65 = 643,
            ADCOCK_SHIPLEY_1200T_C = 644,
            AICHI_TOKEI_TBX100 = 645,
            AICHI_TOKEI_TBZ300 = 646,
            ALDER_MACKAY_100T_C = 647,
            ALDER_MACKAY_1200T_C = 648,
            ALDER_MACKAY_200T_C = 649,
            ALDER_MACKAY_350T_C = 650,
            ALDER_MACKAY_400T_C = 651,
            ALDER_MACKAY_700T_C = 652,
            AQUA_METRO_VZO_8_RE = 653,
            BEGWACO_100T_C = 654,
            BEGWACO_1200T_C = 655,
            BEGWACO_200T_C = 656,
            BRADDOCK_100T_C = 657,
            BRADDOCK_200T_C = 658,
            BRADDOCK_350T_C = 659,
            BRADDOCK_400T_C = 660,
            BRADDOCK_700T_C = 661,
            BRITISH_GAS_1200T_C = 662,
            BRITISH_GAS_1800T_C = 663,
            BRITISH_GAS_200T_C = 664,
            BRITISH_GAS_3000T_C = 665,
            BRITISH_GAS_400T_C = 666,
            BRITISH_GAS_700T_C = 667,
            CUBIX_U6 = 668,
            DELTA_D5 = 669,
            DELTA_G40 = 670,
            DRESSER_11M = 671,
            DRESSER_11MT = 672,
            DRESSER_2MT = 673,
            DRESSER_38M = 674,
            DRESSER_8C125 = 675,
            DRESSER_8CT = 676,
            DRESSER_9MT = 677,
            DRESSER_G10000DN500 = 678,
            DRESSER_G10000DN600 = 679,
            DRESSER_G1000DN150 = 680,
            DRESSER_G1000DN200 = 681,
            DRESSER_G1000DN250 = 682,
            DRESSER_G16000DN600 = 683,
            DRESSER_G1600DN200 = 684,
            DRESSER_G1600DN250 = 685,
            DRESSER_G1600DN300 = 686,
            DRESSER_G160DN100 = 687,
            DRESSER_G160DN80 = 688,
            DRESSER_G2500DN300 = 689,
            DRESSER_G2500DN400 = 690,
            DRESSER_G250DN80 = 691,
            DRESSER_G400 = 692,
            DRESSER_G4000DN300 = 693,
            DRESSER_G4000DN400 = 694,
            DRESSER_G400DN100 = 695,
            DRESSER_G400DN150 = 696,
            DRESSER_G6500DN400 = 697,
            DRESSER_G6500DN500 = 698,
            DRESSER_G650DN150 = 699,
            DRESSER_G650DN200 = 700,
            DRESSER_I19771 = 701,
            DRESSER_I19780_1 = 702,
            DRESSER_I19781 = 703,
            DRESSER_M19771 = 704,
            DRESSER_M197810 = 705,
            DRESSER_MRD0040B4M = 706,
            DRESSER_MRD0100B4B_IMPERIAL = 707,
            DRESSER_MRD0160B4Q = 708,
            DRESSER_MRD2885A10 = 709,
            DRESSER_MTD00250VC = 710,
            DRESSER_MTD00250VD = 711,
            DRESSER_MTD00400VC = 712,
            DRESSER_MTD00400VD = 713,
            DRESSER_MTD00650VD = 714,
            DRESSER_MTD00650VE = 715,
            DRESSER_MTD01000VF = 716,
            DRESSER_MTD01600VE = 717,
            DRESSER_MTD01600VF = 718,
            DRESSER_MTD02500VG = 719,
            DRESSER_MTD02500VH = 720,
            DRESSER_MTD02500VN = 721,
            DRESSER_MTD04000VG = 722,
            DRESSER_MTD04000VH = 723,
            DRESSER_MTD04000VJ = 724,
            DRESSER_MTD06500VH = 725,
            DRESSER_MTD06500VJ = 726,
            DRESSER_MTD10000VJ = 727,
            DRESSER_MTD16000VK = 728,
            DRESSER_MTD16000VL = 729,
            DRESSER_MTD25000VL = 730,
            ELSTER_BK__G10_MDK16 = 731,
            ELSTER_BK__G16_MDK25 = 732,
            ELSTER_BK__G25_MDK40 = 733,
            ELSTER_BK__G40 = 734,
            ELSTER_BK__G40_MDK65 = 735,
            ELSTER_BK__G65 = 736,
            ELSTER_BK_G25M = 737,
            ELSTER_INSTROMET = 738,
            ELSTER_INSTROMET_333_ON_METRIC_METER = 739,
            ELSTER_INSTROMET_NFC_802 = 740,
            ELSTER_INSTROMET_NFC_804 = 741,
            ELSTER_INSTROMET_NFC_900 = 742,
            ELSTER_JEAVONS_BK__G100_MDK160 = 743,
            ELSTER_JEAVONS_BK__G40 = 744,
            ELSTER_JEAVONS_BK__G40_MDK65 = 745,
            ELSTER_JEAVONS_KROMSCHROEDER_BK__G10_MDK16 = 746,
            ELSTER_JEAVONS_KROMSCHROEDER_BK__G16_MDK25 = 747,
            ELSTER_JEAVONS_KROMSCHROEDER_BK__G25_MDK40 = 748,
            ELSTER_JEAVONS_MDK100 = 749,
            ELSTER_JEAVONS_MDK65 = 750,
            ELSTER_JEAVONS_Q1600_DN200P = 751,
            ELSTER_JEAVONS_RVG__G16 = 752,
            ELSTER_MDK160 = 753,
            ELSTER_MGK160 = 754,
            ELSTER_QA10 = 755,
            ELSTER_QA100 = 756,
            ELSTER_QA25 = 757,
            ELSTER_QA250_100_TURBINE = 758,
            ELSTER_QUANTOMETER_QA40 = 759,
            ELSTER_RVG100 = 760,
            EUROMETERS_SC6 = 761,
            FLOBOSS_S600 = 762,
            GAS_METER_CO_100T_C = 763,
            GAS_METER_CO_200T_C = 764,
            GAS_METER_CO_350T_C = 765,
            GAS_METER_CO_400T_C = 766,
            GAS_METER_CO_700T_C = 767,
            GEORGE_GLOVER_100T_C = 768,
            GEORGE_GLOVER_200T_C = 769,
            GEORGE_GLOVER_350T_C = 770,
            GEORGE_GLOVER_400T_C = 771,
            GEORGE_GLOVER_700T_C = 772,
            GEORGE_ORM_100T_C = 773,
            GEORGE_ORM_200T_C = 774,
            GEORGE_ORM_400T_C = 775,
            GEORGE_WILSON_100T_C = 776,
            GEORGE_WILSON_1200T_C = 777,
            GEORGE_WILSON_1800T_C = 778,
            GEORGE_WILSON_200T_C = 779,
            GEORGE_WILSON_3000T_C = 780,
            GEORGE_WILSON_350T_C = 781,
            GEORGE_WILSON_400T_C = 782,
            GEORGE_WILSON_700T_C = 783,
            GEORGE_WILSON_G4SC = 784,
            GEORGE_WILSON_METGW = 785,
            GEORGE_WILSON_METUG = 786,
            GEORGE_WILSON_U100 = 787,
            GEORGE_WILSON_U16_R5 = 788,
            GEORGE_WILSON_U160 = 789,
            GEORGE_WILSON_U160_R5 = 790,
            GWF_SUB_METER = 791,
            HOLMES_CONNERSVILLE_10X30 = 792,
            HOLMES_CONNERSVILLE_12X36 = 793,
            HOLMES_CONNERSVILLE_14X42 = 794,
            HOLMES_CONNERSVILLE_16X48 = 795,
            HOLMES_CONNERSVILLE_4X12 = 796,
            HOLMES_CONNERSVILLE_5X15 = 797,
            HOLMES_CONNERSVILLE_8X24 = 798,
            HOLMES_IN_LINE_1030 = 799,
            HOLMES_IN_LINE_1442 = 800,
            HOLMES_IN_LINE_412 = 801,
            HOLMES_IN_LINE_515 = 802,
            HOLMES_IN_LINE_618 = 803,
            HOLMES_IN_LINE_824 = 804,
            IFM__SD2000 = 805,
            IGA_11M = 806,
            IGA_11M_8_DIAL = 807,
            IGA_2M = 808,
            IGA_2MT = 809,
            IGA_38M = 810,
            IGA_5_3M = 811,
            IGA_5_3M_6_DIAL = 812,
            IGA_5_3M_8_DIAL = 813,
            IGA_6GT_G650 = 814,
            IGA_8GT_G1000 = 815,
            IGA_AL250 = 816,
            IGA_G100 = 817,
            IGA_G160 = 818,
            IGA_G250 = 819,
            IGA_G250DN100 = 820,
            IGA_MRI0025RA = 821,
            IGA_MRI0025RB = 822,
            IGA_MRI0040RA = 823,
            IGA_MRI0040RB = 824,
            IGA_MRI0065RB = 825,
            IGA_MRI0100RB = 826,
            IGA_MRI0160RC = 827,
            IGA_MRI0250RC = 828,
            IGA_MRI400RD = 829,
            IGA_QA16080Z = 830,
            IGA_QA650150Z = 831,
            IGA_TURBINES_G10000DN500 = 832,
            IGA_TURBINES_G10000DN600 = 833,
            IGA_TURBINES_G1000DN150 = 834,
            IGA_TURBINES_G1000DN200 = 835,
            IGA_TURBINES_G100DN80_6_DIAL = 836,
            IGA_TURBINES_G100DN80_7_DIAL = 837,
            IGA_TURBINES_G16000DN600 = 838,
            IGA_TURBINES_G1600DN200 = 839,
            IGA_TURBINES_G1600DN250 = 840,
            IGA_TURBINES_G160DN100 = 841,
            IGA_TURBINES_G160DN80 = 842,
            IGA_TURBINES_G2500DN250 = 843,
            IGA_TURBINES_G2500DN300 = 844,
            IGA_TURBINES_G250DN100 = 845,
            IGA_TURBINES_G250DN80 = 846,
            IGA_TURBINES_G4000DN300 = 847,
            IGA_TURBINES_G4000DN400 = 848,
            IGA_TURBINES_G400DN100 = 849,
            IGA_TURBINES_G400DN150 = 850,
            IGA_TURBINES_G6500DN400 = 851,
            IGA_TURBINES_G6500DN500 = 852,
            IGA_TURBINES_G650DN150_7_DIAL = 853,
            IGA_TURBINES_G650DN150_8_DIAL = 854,
            IGA_TURBINES_G65DN50 = 855,
            IGA_TURBINES_MTI10000GJ = 856,
            IGA_TURBINES_MTI10000GK = 857,
            IGA_TURBINES_MTI1000EE = 858,
            IGA_TURBINES_MTI1000GE = 859,
            IGA_TURBINES_MTI1000GFE = 860,
            IGA_TURBINES_MTI1000QE = 861,
            IGA_TURBINES_MTI100GB = 862,
            IGA_TURBINES_MTI100QB = 863,
            IGA_TURBINES_MTI16000GK = 864,
            IGA_TURBINES_MTI16000GL = 865,
            IGA_TURBINES_MTI1600EE = 866,
            IGA_TURBINES_MTI1600EF = 867,
            IGA_TURBINES_MTI1600GE = 868,
            IGA_TURBINES_MTI1600GF = 869,
            IGA_TURBINES_MTI1600GGF = 870,
            IGA_TURBINES_MTI1600QF = 871,
            IGA_TURBINES_MTI160EC = 872,
            IGA_TURBINES_MTI160GC = 873,
            IGA_TURBINES_MTI160QC = 874,
            IGA_TURBINES_MTI25000GL = 875,
            IGA_TURBINES_MTI2500EF = 876,
            IGA_TURBINES_MTI2500GF = 877,
            IGA_TURBINES_MTI2500GG = 878,
            IGA_TURBINES_MTI2500QF = 879,
            IGA_TURBINES_MTI2500QG = 880,
            IGA_TURBINES_MTI250EC = 881,
            IGA_TURBINES_MTI250GC = 882,
            IGA_TURBINES_MTI250GD = 883,
            IGA_TURBINES_MTI250QC = 884,
            IGA_TURBINES_MTI4000GG = 885,
            IGA_TURBINES_MTI4000GH = 886,
            IGA_TURBINES_MTI4000QG = 887,
            IGA_TURBINES_MTI4000QH = 888,
            IGA_TURBINES_MTI400EC = 889,
            IGA_TURBINES_MTI400GC = 890,
            IGA_TURBINES_MTI400GD = 891,
            IGA_TURBINES_MTI400GED = 892,
            IGA_TURBINES_MTI400QD = 893,
            IGA_TURBINES_MTI6500GH = 894,
            IGA_TURBINES_MTI6500GJ = 895,
            IGA_TURBINES_MTI6500QJ = 896,
            IGA_TURBINES_MTI650ED = 897,
            IGA_TURBINES_MTI650GD = 898,
            IGA_TURBINES_MTI650GE = 899,
            IGA_TURBINES_MTI650GFE = 900,
            IGA_TURBINES_MTI650QE = 901,
            IGA_TURBINES_Q100 = 902,
            IGA_TURBINES_Q1000 = 903,
            IGA_TURBINES_Q160 = 904,
            IGA_TURBINES_Q1600 = 905,
            IGA_TURBINES_Q250 = 906,
            IGA_TURBINES_Q2500 = 907,
            IGA_TURBINES_Q400 = 908,
            IGA_TURBINES_Q4000 = 909,
            IGA_TURBINES_Q65 = 910,
            IGA_TURBINES_Q650 = 911,
            IMAC_SYSTEMS_EA100 = 912,
            INSTROMEC__333 = 913,
            INSTROMEC_10_IMPERIAL = 914,
            INSTROMEC_10_METRIC = 915,
            INSTROMEC_12_IMPERIAL = 916,
            INSTROMEC_12_METRIC = 917,
            INSTROMEC_16_IMPERIAL = 918,
            INSTROMEC_16_METRIC = 919,
            INSTROMEC_2_IMPERIAL = 920,
            INSTROMEC_2_METRIC = 921,
            INSTROMEC_20_IMPERIAL = 922,
            INSTROMEC_20_METRIC = 923,
            INSTROMEC_24_IMPERIAL = 924,
            INSTROMEC_24_METRIC = 925,
            INSTROMEC_3_IMPERIAL = 926,
            INSTROMEC_3_METRIC = 927,
            INSTROMEC_3_SMRILP = 928,
            INSTROMEC_30_IMPERIAL = 929,
            INSTROMEC_30_METRIC = 930,
            INSTROMEC_333 = 931,
            INSTROMEC_4_IMPERIAL = 932,
            INSTROMEC_4_METRIC = 933,
            INSTROMEC_6_IMPERIAL = 934,
            INSTROMEC_6_METRIC = 935,
            INSTROMEC_8_IMPERIAL = 936,
            INSTROMEC_8_METRIC = 937,
            INSTROMEC_G10 = 938,
            INSTROMEC_G10000DN500 = 939,
            INSTROMEC_G10000DN600 = 940,
            INSTROMEC_G1000DN150 = 941,
            INSTROMEC_G1000DN200 = 942,
            INSTROMEC_G1000DN250 = 943,
            INSTROMEC_G100DN80 = 944,
            INSTROMEC_G16 = 945,
            INSTROMEC_G16000DN600 = 946,
            INSTROMEC_G1600DN200 = 947,
            INSTROMEC_G1600DN250 = 948,
            INSTROMEC_G1600DN300 = 949,
            INSTROMEC_G160DN80 = 950,
            INSTROMEC_G2500DN250 = 951,
            INSTROMEC_G2500DN300 = 952,
            INSTROMEC_G2500DN400 = 953,
            INSTROMEC_G250DN80 = 954,
            INSTROMEC_G4000DN300 = 955,
            INSTROMEC_G4000DN400 = 956,
            INSTROMEC_G4000DN500 = 957,
            INSTROMEC_G400DN150 = 958,
            INSTROMEC_G40DN50 = 959,
            INSTROMEC_G6500DN400 = 960,
            INSTROMEC_G6500DN500 = 961,
            INSTROMEC_G6500DN600 = 962,
            INSTROMEC_G650DN150 = 963,
            INSTROMEC_G650DN200 = 964,
            INSTROMEC_G65DN50 = 965,
            INSTROMEC_MTN00065QB = 966,
            INSTROMEC_MTN00065SB = 967,
            INSTROMEC_MTN00100QB = 968,
            INSTROMEC_MTN00100SB = 969,
            INSTROMEC_MTN00160QC = 970,
            INSTROMEC_MTN00160SC = 971,
            INSTROMEC_MTN00250QC = 972,
            INSTROMEC_MTN00250QD = 973,
            INSTROMEC_MTN00250SC = 974,
            INSTROMEC_MTN00250SD = 975,
            INSTROMEC_MTN00400QC = 976,
            INSTROMEC_MTN00400QD = 977,
            INSTROMEC_MTN00400SC = 978,
            INSTROMEC_MTN00400SD = 979,
            INSTROMEC_MTN00650QD = 980,
            INSTROMEC_MTN00650QE = 981,
            INSTROMEC_MTN00650SD = 982,
            INSTROMEC_MTN00650SE = 983,
            INSTROMEC_MTN01000QE = 984,
            INSTROMEC_MTN01000QF = 985,
            INSTROMEC_MTN01000SE = 986,
            INSTROMEC_MTN01000SF = 987,
            INSTROMEC_MTN01600QE = 988,
            INSTROMEC_MTN01600QF = 989,
            INSTROMEC_MTN01600QG = 990,
            INSTROMEC_MTN01600SE = 991,
            INSTROMEC_MTN01600SF = 992,
            INSTROMEC_MTN01600SG = 993,
            INSTROMEC_MTN02500QF = 994,
            INSTROMEC_MTN02500QG = 995,
            INSTROMEC_MTN02500QH = 996,
            INSTROMEC_MTN02500SF = 997,
            INSTROMEC_MTN02500SG = 998,
            INSTROMEC_MTN02500SH = 999,
            INSTROMEC_MTN04000QG = 1000,
            INSTROMEC_MTN04000QH = 1001,
            INSTROMEC_MTN04000QJ = 1002,
            INSTROMEC_MTN04000SG = 1003,
            INSTROMEC_MTN04000SH = 1004,
            INSTROMEC_MTN06500QH = 1005,
            INSTROMEC_MTN06500QJ = 1006,
            INSTROMEC_MTN06500QK = 1007,
            INSTROMEC_MTN06500SH = 1008,
            INSTROMEC_MTN06500SJ = 1009,
            INSTROMEC_MTN06500SK = 1010,
            INSTROMEC_MTN10000QJ = 1011,
            INSTROMEC_MTN10000QK = 1012,
            INSTROMEC_MTN10000QL = 1013,
            INSTROMEC_MTN10000SJ = 1014,
            INSTROMEC_MTN10000SK = 1015,
            INSTROMEC_MTN10000SL = 1016,
            INSTROMEC_MTN16000QK = 1017,
            INSTROMEC_MTN16000QL = 1018,
            INSTROMEC_MTN16000SK = 1019,
            INSTROMEC_MTN16000SL = 1020,
            INSTROMEC_MTN25000SL = 1021,
            INSTROMEC_Q7510_IMPERIAL = 1022,
            INSTROMEC_Q7512 = 1023,
            INSTROMEC_Q7516_100FT = 1024,
            INSTROMEC_Q7516_10FT = 1025,
            INSTROMEC_Q752 = 1026,
            INSTROMEC_Q7520_IMPERIAL = 1027,
            INSTROMEC_Q7520_METRIC = 1028,
            INSTROMEC_Q7524 = 1029,
            INSTROMEC_Q753 = 1030,
            INSTROMEC_Q754 = 1031,
            INSTROMEC_Q756_IMPERIAL = 1032,
            INSTROMEC_Q756_METRIC = 1033,
            INSTROMEC_Q758_IMPERIAL = 1034,
            INSTROMEC_Q758_METRIC = 1035,
            INSTROMEC_SM_RI_W_LP = 1036,
            INSTROMEC_SMRI150 = 1037,
            INSTROMET_1_100 = 1038,
            INSTROMET_555 = 1039,
            INSTROMET_999 = 1040,
            INSTROMET_Q75 = 1041,
            INVENSYS_SONIX = 1042,
            ITRON__MRA400D = 1043,
            ITRON_G4SC = 1044,
            ITRON_G65_7_DIAL = 1045,
            ITRON_G650 = 1046,
            ITRON_MDA100W = 1047,
            ITRON_METAC = 1048,
            ITRON_MRA1000E = 1049,
            ITRON_MRA1000SE = 1050,
            ITRON_MRA100B = 1051,
            ITRON_MRA100PB = 1052,
            ITRON_MRA1600PN = 1053,
            ITRON_MRA160PC = 1054,
            ITRON_MRA160PQ = 1055,
            ITRON_MRA2500PG = 1056,
            ITRON_MRA2500PO = 1057,
            ITRON_MRA250C_7_DIAL = 1058,
            ITRON_MRA250C_8_DIAL = 1059,
            ITRON_MRA250Q = 1060,
            ITRON_MRA25A = 1061,
            ITRON_MRA300R = 1062,
            ITRON_MRA400PD = 1063,
            ITRON_MRA400PR = 1064,
            ITRON_MRA40A = 1065,
            ITRON_MRA650SE = 1066,
            ITRON_MRA65B = 1067,
            ITRON_MTA10000J = 1068,
            ITRON_MTA10000TJ = 1069,
            ITRON_MTA10000TK = 1070,
            ITRON_MTA1000E = 1071,
            ITRON_MTA1000F = 1072,
            ITRON_MTA1000FE = 1073,
            ITRON_MTA1000S = 1074,
            ITRON_MTA1000TE = 1075,
            ITRON_MTA1000TF = 1076,
            ITRON_MTA100TB = 1077,
            ITRON_MTA1600E = 1078,
            ITRON_MTA1600F = 1079,
            ITRON_MTA1600FF = 1080,
            ITRON_MTA1600G = 1081,
            ITRON_MTA1600N = 1082,
            ITRON_MTA1600TE = 1083,
            ITRON_MTA1600TF = 1084,
            ITRON_MTA1600TG = 1085,
            ITRON_MTA160C = 1086,
            ITRON_MTA160TC = 1087,
            ITRON_MTA2500F = 1088,
            ITRON_MTA2500FG = 1089,
            ITRON_MTA2500G = 1090,
            ITRON_MTA2500H = 1091,
            ITRON_MTA2500O = 1092,
            ITRON_MTA2500TF = 1093,
            ITRON_MTA2500TG = 1094,
            ITRON_MTA2500TH = 1095,
            ITRON_MTA250C = 1096,
            ITRON_MTA250D = 1097,
            ITRON_MTA250FC = 1098,
            ITRON_MTA250Q = 1099,
            ITRON_MTA250TC = 1100,
            ITRON_MTA250TD = 1101,
            ITRON_MTA4000FG = 1102,
            ITRON_MTA4000FH = 1103,
            ITRON_MTA4000G = 1104,
            ITRON_MTA4000H = 1105,
            ITRON_MTA4000J = 1106,
            ITRON_MTA4000O = 1107,
            ITRON_MTA4000T = 1108,
            ITRON_MTA4000TG = 1109,
            ITRON_MTA4000TH = 1110,
            ITRON_MTA4000TJ = 1111,
            ITRON_MTA400C = 1112,
            ITRON_MTA400D = 1113,
            ITRON_MTA400FD = 1114,
            ITRON_MTA400R = 1115,
            ITRON_MTA400TC = 1116,
            ITRON_MTA400TD = 1117,
            ITRON_MTA6500FJ = 1118,
            ITRON_MTA6500H = 1119,
            ITRON_MTA6500J = 1120,
            ITRON_MTA6500TH = 1121,
            ITRON_MTA6500TJ = 1122,
            ITRON_MTA6500TK = 1123,
            ITRON_MTA6500U = 1124,
            ITRON_MTA650D = 1125,
            ITRON_MTA650E = 1126,
            ITRON_MTA650FE = 1127,
            ITRON_MTA650TD = 1128,
            ITRON_MTA650TE = 1129,
            ITRON_TALEXUS = 1130,
            ITRON_TZ100G160 = 1131,
            ITRON_TZ100G250 = 1132,
            ITRON_TZ100G400 = 1133,
            ITRON_TZ150G1000 = 1134,
            ITRON_TZ150G400 = 1135,
            ITRON_TZ150G650 = 1136,
            ITRON_TZ200G1000 = 1137,
            ITRON_TZ200G1600 = 1138,
            ITRON_TZ200G650 = 1139,
            ITRON_TZ250G1000 = 1140,
            ITRON_TZ250G1600 = 1141,
            ITRON_TZ250G2500 = 1142,
            ITRON_TZ300G1600 = 1143,
            ITRON_TZ300G2500 = 1144,
            ITRON_TZ300G4000 = 1145,
            ITRON_TZ400G2500 = 1146,
            ITRON_TZ400G4000 = 1147,
            ITRON_TZ400G6500 = 1148,
            ITRON_TZ80G100 = 1149,
            ITRON_TZ80G160 = 1150,
            ITRON_TZ80G250 = 1151,
            J_ROBINSON_100T_C = 1152,
            J_ROBINSON_1200T_C = 1153,
            J_ROBINSON_200T_C = 1154,
            J_ROBINSON_350T_C = 1155,
            J_ROBINSON_400T_C = 1156,
            J_ROBINSON_700T_C = 1157,
            JEAVONS_BK__G4 = 1158,
            JEAVONS_BK_G16 = 1159,
            JEAVONS_BK_G25 = 1160,
            JEAVONS_G10 = 1161,
            JEAVONS_G100 = 1162,
            JEAVONS_G100_R5 = 1163,
            JEAVONS_G10000DN500 = 1164,
            JEAVONS_G10000DN600 = 1165,
            JEAVONS_G1000DN200 = 1166,
            JEAVONS_G16 = 1167,
            JEAVONS_G160 = 1168,
            JEAVONS_G160_8_DIAL = 1169,
            JEAVONS_G16000DN600 = 1170,
            JEAVONS_G1600DN200 = 1171,
            JEAVONS_G1600DN250 = 1172,
            JEAVONS_G160DN100 = 1173,
            JEAVONS_G25 = 1174,
            JEAVONS_G2500DN250 = 1175,
            JEAVONS_G2500DN300 = 1176,
            JEAVONS_G250DN80 = 1177,
            JEAVONS_G40 = 1178,
            JEAVONS_G4000DN300 = 1179,
            JEAVONS_G4000DN400 = 1180,
            JEAVONS_G400DN100 = 1181,
            JEAVONS_G400DN150 = 1182,
            JEAVONS_G6500DN400 = 1183,
            JEAVONS_G6500DN500 = 1184,
            JEAVONS_G650DN150_7_DIAL = 1185,
            JEAVONS_G650DN150_8_DIAL = 1186,
            JEAVONS_G65DN50 = 1187,
            JEAVONS_KROMSCHROEDER_BK__G10 = 1188,
            JEAVONS_KROMSCHROEDER_BK__G25 = 1189,
            JEAVONS_KROMSCHROEDER_ELSTER_BK_G10 = 1190,
            JEAVONS_KROMSCHROEDER_ELSTER_BK_G16 = 1191,
            JEAVONS_Q1000_METRIC = 1192,
            JEAVONS_Q1600_IMPERIAL = 1193,
            JEAVONS_Q250_IMPERIAL = 1194,
            JEAVONS_Q250_METRIC = 1195,
            JEAVONS_Q2500_IMPERIAL = 1196,
            JEAVONS_Q2500_METRIC = 1197,
            JEAVONS_Q400_IMPERIAL = 1198,
            JEAVONS_Q4000 = 1199,
            JEAVONS_Q650_IMPERIAL = 1200,
            JEAVONS_Q650_METRIC = 1201,
            JEAVONS_QA25 = 1202,
            JEAVONS_U160 = 1203,
            JEAVONS_U160_R5 = 1204,
            JEAVONS_U40 = 1205,
            KROMSCHRODER_MDK25 = 1206,
            KROMSCHROEDER_BK__G4 = 1207,
            LANDIS_GYR_ETGW = 1208,
            LANDIS_GYR_ETUG = 1209,
            LPG_E6VLG310 = 1210,
            LPG_ETX6 = 1211,
            LPG_METLG210 = 1212,
            MAGNOL_G10 = 1213,
            MAGNOL_G100 = 1214,
            MAGNOL_G25 = 1215,
            METERS_LTD_100T_C = 1216,
            METERS_LTD_200T_C = 1217,
            METERS_LTD_400T_C = 1218,
            METROPOLITAN_100T_C = 1219,
            METROPOLITAN_200T_C = 1220,
            METROPOLITAN_350T_C = 1221,
            METROPOLITAN_400T_C = 1222,
            METROPOLITAN_700T_C = 1223,
            MILNES_METERS_100T_C = 1224,
            MILNES_METERS_200T_C = 1225,
            MILNES_METERS_350T_C = 1226,
            NUOVO_PIGNONE_G10_U16 = 1227,
            NUOVO_PIGNONE_G16_U25 = 1228,
            NUOVO_PIGNONE_G25_U40 = 1229,
            NUOVO_PIGNONE_G4_U6 = 1230,
            NUOVO_PIGNONE_G40_U65 = 1231,
            NUOVO_PIGNONE_G65_U100 = 1232,
            PARKINSON_COWAN_100T_C = 1233,
            PARKINSON_COWAN_1200T_C = 1234,
            PARKINSON_COWAN_1800T_C = 1235,
            PARKINSON_COWAN_350T_C = 1236,
            PARKINSON_COWAN_400T_C = 1237,
            PARKINSON_COWAN_4500T_C = 1238,
            PARKINSON_COWAN_6000T_C = 1239,
            PARKINSON_COWAN_9000T_C = 1240,
            PARKINSON_COWAN_MDA16 = 1241,
            PARKINSON_COWAN_U160_R5 = 1242,
            PC_COMPTEURS_100_50 = 1243,
            PC_COMPTEURS_1600_200 = 1244,
            PC_COMPTEURS_200_100 = 1245,
            PC_COMPTEURS_200_80 = 1246,
            PC_COMPTEURS_2500_250 = 1247,
            PC_COMPTEURS_3200_300 = 1248,
            PC_COMPTEURS_350_100 = 1249,
            PC_COMPTEURS_400_100 = 1250,
            PC_COMPTEURS_400_150 = 1251,
            PC_COMPTEURS_550_150 = 1252,
            PC_COMPTEURS_6400_400 = 1253,
            PC_COMPTEURS_800_200 = 1254,
            PC_COMPTEURS_D14 = 1255,
            PC_COMPTEURS_D20_7_DIAL = 1256,
            PC_COMPTEURS_D3_5 = 1257,
            PC_COMPTEURS_D35 = 1258,
            PC_COMPTEURS_D35_7_DIAL = 1259,
            PC_COMPTEURS_D35_8_DIAL = 1260,
            PC_COMPTEURS_D56_7_DIAL = 1261,
            PC_COMPTEURS_D56_8_DIAL = 1262,
            PC_COMPTEURS_D8_8_7_DIAL = 1263,
            PC_COMPTEURS_D8_8_8_DIAL = 1264,
            PC_COMPTEURS_D88_7_DIAL = 1265,
            PC_COMPTEURS_D88_8_DIAL = 1266,
            PC_COMPTEURS_DELTA_800_150 = 1267,
            PC_COMPTEURS_G1000 = 1268,
            PC_COMPTEURS_G1600 = 1269,
            PC_COMPTEURS_G250 = 1270,
            PC_COMPTEURS_G400 = 1271,
            PC_COMPTEURS_G65 = 1272,
            PC_COMPTEURS_G650 = 1273,
            PC_COMPTEURS_NINETEEN_80 = 1274,
            PEEBLES_1200T_C = 1275,
            PEEBLES_3000T_C = 1276,
            PEEBLES_350T_C = 1277,
            PEEBLES_400T_C = 1278,
            PEEBLES_6000T_C = 1279,
            PEEBLES_700T_C = 1280,
            QUANTOMETER__CPT01 = 1281,
            QUANTOMETER_7_DIAL = 1282,
            RMG_MTP00065GB = 1283,
            RMG_MTP00065LB = 1284,
            RMG_MTP00065QB = 1285,
            RMG_MTP00100GB = 1286,
            RMG_MTP00100LB = 1287,
            RMG_MTP00100QB = 1288,
            RMG_MTP00160GC = 1289,
            RMG_MTP00160QC = 1290,
            RMG_MTP00250GC = 1291,
            RMG_MTP00250GD = 1292,
            RMG_MTP00250LC = 1293,
            RMG_MTP00250LD = 1294,
            RMG_MTP00250QC = 1295,
            RMG_MTP00250QD = 1296,
            RMG_MTP00400GD = 1297,
            RMG_MTP00400LC = 1298,
            RMG_MTP00400LD = 1299,
            RMG_MTP00400QC = 1300,
            RMG_MTP00400QD = 1301,
            RMG_MTP00650GD = 1302,
            RMG_MTP00650GE = 1303,
            RMG_MTP00650LD = 1304,
            RMG_MTP00650LE = 1305,
            RMG_MTP00650QD = 1306,
            RMG_MTP00650QE = 1307,
            RMG_MTP01000GE = 1308,
            RMG_MTP01000LE = 1309,
            RMG_MTP01000QE = 1310,
            RMG_MTP01600GE = 1311,
            RMG_MTP01600GF = 1312,
            RMG_MTP01600GG = 1313,
            RMG_MTP01600LE = 1314,
            RMG_MTP01600LF = 1315,
            RMG_MTP01600LG = 1316,
            RMG_MTP01600QE = 1317,
            RMG_MTP01600QF = 1318,
            RMG_MTP01600QG = 1319,
            RMG_MTP02500GF = 1320,
            RMG_MTP02500GG = 1321,
            RMG_MTP02500LF = 1322,
            RMG_MTP02500LG = 1323,
            RMG_MTP02500QF = 1324,
            RMG_MTP02500QG = 1325,
            RMG_MTP04000GG = 1326,
            RMG_MTP04000GH = 1327,
            RMG_MTP04000LG = 1328,
            RMG_MTP04000LH = 1329,
            RMG_MTP04000QG = 1330,
            RMG_MTP04000QH = 1331,
            RMG_MTP06500GH = 1332,
            RMG_MTP06500GJ = 1333,
            RMG_MTP06500LH = 1334,
            RMG_MTP06500LJ = 1335,
            RMG_MTP10000GJ = 1336,
            RMG_MTP10000GK = 1337,
            RMG_MTP10000LJ = 1338,
            RMG_MTP10000LK = 1339,
            RMG_MTP16000GK = 1340,
            RMG_MTP16000LK = 1341,
            RMG_MTP25000GL = 1342,
            RMG_MTP25000LL = 1343,
            RMG_Q100 = 1344,
            RMG_Q1000 = 1345,
            RMG_Q160 = 1346,
            RMG_Q1600 = 1347,
            RMG_Q250 = 1348,
            RMG_Q2500 = 1349,
            RMG_Q40 = 1350,
            RMG_Q400 = 1351,
            RMG_Q65 = 1352,
            RMG_Q650 = 1353,
            RMG_TRZ03G100 = 1354,
            RMG_TRZ03G1000 = 1355,
            RMG_TRZ03G10000 = 1356,
            RMG_TRZ03G160 = 1357,
            RMG_TRZ03G1600 = 1358,
            RMG_TRZ03G16000 = 1359,
            RMG_TRZ03G250 = 1360,
            RMG_TRZ03G2500 = 1361,
            RMG_TRZ03G40 = 1362,
            RMG_TRZ03G400 = 1363,
            RMG_TRZ03G4000 = 1364,
            RMG_TRZ03G65 = 1365,
            RMG_TRZ03G650 = 1366,
            RMG_TRZ03G6500 = 1367,
            RMG_TRZ03Q100 = 1368,
            RMG_TRZ03Q1000 = 1369,
            RMG_TRZ03Q160 = 1370,
            RMG_TRZ03Q1600 = 1371,
            RMG_TRZ03Q250 = 1372,
            RMG_TRZ03Q2500 = 1373,
            RMG_TRZ03Q40 = 1374,
            RMG_TRZ03Q400 = 1375,
            RMG_TRZ03Q65 = 1376,
            RMG_TRZ03Q650 = 1377,
            ROCKWELL_1500 = 1378,
            ROCKWELL_750 = 1379,
            ROCKWELL_R_3 = 1380,
            ROCKWELL_R_5 = 1381,
            ROCKWELL_T140 = 1382,
            ROCKWELL_T18 = 1383,
            ROCKWELL_T30 = 1384,
            ROCKWELL_T60 = 1385,
            ROMET_ECM_102411 = 1386,
            ROMET_ECM16000 = 1387,
            ROMET_ECM2000 = 1388,
            ROMET_ECM3000 = 1389,
            ROMET_ECM5000 = 1390,
            ROMET_ECM7000 = 1391,
            ROMET_G100 = 1392,
            ROMET_G16 = 1393,
            ROMET_G25 = 1394,
            ROMET_G250 = 1395,
            ROMET_G40 = 1396,
            ROMET_G400 = 1397,
            ROMET_G65 = 1398,
            ROMET_MRR0025GA = 1399,
            ROMET_MRR0040GA = 1400,
            ROMET_MRR0065GB = 1401,
            ROMET_MRR0160GC = 1402,
            ROMET_MRR0250GC = 1403,
            ROMET_MRR0400GD = 1404,
            ROMET_RM11000 = 1405,
            ROMET_RM1500 = 1406,
            ROMET_RM1500_7_DIAL = 1407,
            ROMET_RM1500_8_DIAL = 1408,
            ROMET_RM16000 = 1409,
            ROMET_RM3000 = 1410,
            ROMET_RM5000 = 1411,
            ROMET_RM7000 = 1412,
            SAWER_PURVES_100T_C = 1413,
            SAWER_PURVES_200T_C = 1414,
            SCHLUMBERGER_7926_FLOW_COMPUTER = 1415,
            SCHLUMBERGER_D14 = 1416,
            SCHLUMBERGER_D35 = 1417,
            SCHLUMBERGER_D56 = 1418,
            SCHLUMBERGER_D9_8_DIAL = 1419,
            SCHLUMBERGER_DELTA_D1_5_2040_40A3 = 1420,
            SCHLUMBERGER_DELTA_D7_2080_200 = 1421,
            SCHLUMBERGER_DELTA_G400 = 1422,
            SCHLUMBERGER_E6 = 1423,
            SCHLUMBERGER_F14 = 1424,
            SCHLUMBERGER_F140 = 1425,
            SCHLUMBERGER_F23 = 1426,
            SCHLUMBERGER_F230 = 1427,
            SCHLUMBERGER_FLUXI_90_F88 = 1428,
            SCHLUMBERGER_G1000TZ150 = 1429,
            SCHLUMBERGER_G160 = 1430,
            SCHLUMBERGER_G1600TZ250 = 1431,
            SCHLUMBERGER_G160TZ100 = 1432,
            SCHLUMBERGER_G250 = 1433,
            SCHLUMBERGER_G2500TZ300 = 1434,
            SCHLUMBERGER_G250TZ100 = 1435,
            SCHLUMBERGER_G400 = 1436,
            SCHLUMBERGER_G4000TZ300 = 1437,
            SCHLUMBERGER_G4000TZ400 = 1438,
            SCHLUMBERGER_G400TZ100 = 1439,
            SCHLUMBERGER_G4SC = 1440,
            SCHLUMBERGER_G650 = 1441,
            SCHLUMBERGER_G650TZ150 = 1442,
            SCHLUMBERGER_G650TZ200 = 1443,
            SCHLUMBERGER_MRA100PP = 1444,
            SCHLUMBERGER_MRA1600PF = 1445,
            SCHLUMBERGER_MRA1600PN = 1446,
            SCHLUMBERGER_MRA160PC = 1447,
            SCHLUMBERGER_MRA160PQ = 1448,
            SCHLUMBERGER_MRA2500PG = 1449,
            SCHLUMBERGER_MRA2500PO = 1450,
            SCHLUMBERGER_MRA300R = 1451,
            SCHLUMBERGER_MRA400PR = 1452,
            SCHLUMBERGER_MRA65B = 1453,
            SCHLUMBERGER_MTA10000J = 1454,
            SCHLUMBERGER_MTA1000F = 1455,
            SCHLUMBERGER_MTA1000FE = 1456,
            SCHLUMBERGER_MTA1000S = 1457,
            SCHLUMBERGER_MTA1600E = 1458,
            SCHLUMBERGER_MTA1600F = 1459,
            SCHLUMBERGER_MTA1600FF = 1460,
            SCHLUMBERGER_MTA1600G = 1461,
            SCHLUMBERGER_MTA160C = 1462,
            SCHLUMBERGER_MTA2500F = 1463,
            SCHLUMBERGER_MTA2500FG = 1464,
            SCHLUMBERGER_MTA2500G = 1465,
            SCHLUMBERGER_MTA2500H = 1466,
            SCHLUMBERGER_MTA250C = 1467,
            SCHLUMBERGER_MTA250D = 1468,
            SCHLUMBERGER_MTA250FC = 1469,
            SCHLUMBERGER_MTA250Q = 1470,
            SCHLUMBERGER_MTA4000FG = 1471,
            SCHLUMBERGER_MTA4000FH = 1472,
            SCHLUMBERGER_MTA4000H = 1473,
            SCHLUMBERGER_MTA4000J = 1474,
            SCHLUMBERGER_MTA400C = 1475,
            SCHLUMBERGER_MTA400D = 1476,
            SCHLUMBERGER_MTA400FD = 1477,
            SCHLUMBERGER_MTA6500FJ = 1478,
            SCHLUMBERGER_MTA6500H = 1479,
            SCHLUMBERGER_MTA6500J = 1480,
            SCHLUMBERGER_MTA650D = 1481,
            SCHLUMBERGER_MTA650E = 1482,
            SCHLUMBERGER_MTA650FE = 1483,
            SCHLUMBERGER_SC6_IMPERIAL = 1484,
            SCHLUMBERGER_SC6_METRIC = 1485,
            SCHLUMBERGER_SEVC_D = 1486,
            SCHLUMBERGER_TZ100G160 = 1487,
            SCHLUMBERGER_TZ100G250 = 1488,
            SCHLUMBERGER_TZ100G400 = 1489,
            SCHLUMBERGER_TZ150G1000 = 1490,
            SCHLUMBERGER_TZ150G400 = 1491,
            SCHLUMBERGER_TZ150G650 = 1492,
            SCHLUMBERGER_TZ200G1000 = 1493,
            SCHLUMBERGER_TZ200G1600 = 1494,
            SCHLUMBERGER_TZ200G650 = 1495,
            SCHLUMBERGER_TZ250G1000 = 1496,
            SCHLUMBERGER_TZ250G1600 = 1497,
            SCHLUMBERGER_TZ250G2500 = 1498,
            SCHLUMBERGER_TZ300G1600 = 1499,
            SCHLUMBERGER_TZ300G2500 = 1500,
            SCHLUMBERGER_TZ300G4000 = 1501,
            SCHLUMBERGER_TZ400G2500 = 1502,
            SCHLUMBERGER_TZ400G4000 = 1503,
            SCHLUMBERGER_TZ400G6500 = 1504,
            SCHLUMBERGER_TZ80G100 = 1505,
            SCHLUMBERGER_TZ80G160 = 1506,
            SCHLUMBERGER_TZ80G250 = 1507,
            SCHLUMBERGER_U10 = 1508,
            SCHLUMBERGER_U40_R5_5_DIAL = 1509,
            SCHLUMBERGER_U40_R5_5_DIAL_IMPERIAL = 1510,
            SCHLUMBERGER_U40_R5_6_DIAL = 1511,
            SEN_G4 = 1512,
            SEN_SONIX = 1513,
            SENSUS_KROMSCHROEDER_ELSTER_U6 = 1514,
            SIEMENS_E6X = 1515,
            SIEMENS_ETX6 = 1516,
            SIEMENS_METUG = 1517,
            SIEMENS_SC6 = 1518,
            SIEMENS_X6 = 1519,
            SITE_SURVEY = 1520,
            T_BRADDOCK_100T_C = 1521,
            T_BRADDOCK_200T_C = 1522,
            T_BRADDOCK_400T_C = 1523,
            T_GLOVER_100T_C = 1524,
            T_GLOVER_1200T_C = 1525,
            T_GLOVER_1800T_C = 1526,
            T_GLOVER_200T_C = 1527,
            T_GLOVER_3000T_C = 1528,
            T_GLOVER_350T_C = 1529,
            T_GLOVER_400T_C = 1530,
            T_GLOVER_4500T_C = 1531,
            T_GLOVER_6000T_C = 1532,
            TACAR_C200_IMPERIAL = 1533,
            TACAR_C200_METRIC = 1534,
            TACAR_C202 = 1535,
            TACAR_C205 = 1536,
            TACAR_C208 = 1537,
            TACAR_C209 = 1538,
            TACAR_IC20271 = 1539,
            THORN_EMI_U100 = 1540,
            THORN_EMI_U160 = 1541,
            THORN_EMI_U25 = 1542,
            THORN_EMI_U65 = 1543,
            UGI_1200T_C = 1544,
            UGI_1800T_C = 1545,
            UGI_3000T_C = 1546,
            UGI_350T_C = 1547,
            UGI_400T_C = 1548,
            UGI_4500T_C = 1549,
            UGI_6000T_C = 1550,
            UGI_700T_C = 1551,
            UGI_9000T_C = 1552,
            UGI_MDA40W = 1553,
            UGI_R5 = 1554,
            UGI_TG1000 = 1555,
            UGI_TG1600 = 1556,
            UGI_U10 = 1557,
            UGI_U100 = 1558,
            UGI_U16 = 1559,
            UGI_U160 = 1560,
            UGI_U25 = 1561,
            UGI_U40 = 1562,
            UGI_U6 = 1563,
            UGI_U65 = 1564,
            VALOR_200T_C = 1565,
            VALOR_U6 = 1566,
            VALOR_U6_R5 = 1567,
            VANDA_METERS_1200T_C = 1568,
            VANDA_METERS_1800T_C = 1569,
            VANDA_METERS_200T_C = 1570,
            VANDA_METERS_3000T_C = 1571,
            VANDA_METERS_400T_C = 1572,
            VANDA_METERS_700T_C = 1573,
            WILLEY_100T_C = 1574,
            WILLEY_1200T_C = 1575,
            WILLEY_1800T_C = 1576,
            WILLEY_200T_C = 1577,
            WILLEY_3000T_C = 1578,
            WILLEY_350T_C = 1579,
            WILLEY_400T_C = 1580,
            WILLEY_700T_C = 1581,
            Unknown_Heatmeter = 1582,
            DIEHL_REAL_DATA = 1583,
            DIEHL_OMS = 1584,
            FuelDistributor = 1585,
            ABB_AQUAMASTER = 1586,
            ABB_AQUAMASTER_6DIALS = 1587,
            ABB_KENT_HELIX_3000 = 1588,
            ABB_KENT_PSMT = 1589,
            ABB_MAGMASTER_ELECTROMAGNETIC_FLOW_METER = 1590,
            ABB_MSN = 1591,
            ABB_PSMT = 1592,
            ACTARIS_5_DIALS = 1593,
            ACTARIS_FLOSTAR = 1594,
            ACTARIS_FLOWSTAR = 1595,
            ACTARIS_K1 = 1596,
            ACTARIS_P50E = 1597,
            ACTARIS_PHPE290 = 1598,
            ACTARIS_PULSE1 = 1599,
            ACTARIS_WATERMETER = 1600,
            ACTARIS_WEG = 1601,
            ACTARIS_WEG50 = 1602,
            ACTARIS_WOLTEX_M = 1603,
            ACTARIS_WTX = 1604,
            AHS = 1605,
            AQUADIS = 1606,
            AQUAMASTER = 1607,
            AQUAMETRO = 1608,
            ARAD_MS40 = 1609,
            ARAD_WST50 = 1610,
            ATEAU_DCWW03 = 1611,
            B_METER_K30 = 1612,
            BMETERS = 1613,
            ELSTER__MKH4381 = 1614,
            ELSTER_H4000 = 1615,
            ELSTER_MKH4381 = 1616,
            ELSTER_MSN = 1617,
            ELSTER_S2000 = 1618,
            ELSTER_WATERMETER = 1619,
            GWK_GWF = 1620,
            HALMAGENERICWATER = 1621,
            INVENSYS_6DIALS = 1622,
            INVENSYS_WPD = 1623,
            JEAVONS_QUANTOMETER_QA25 = 1624,
            KENT__PSMT = 1625,
            KENT_H = 1626,
            KENT_H4000 = 1627,
            KENT_HELIX = 1628,
            KENT_HELIX_3000 = 1629,
            KENT_MSM = 1630,
            KENT_PSMT = 1631,
            KENT_PSMT_CL02_6_DIAL = 1632,
            KENT_V100 = 1633,
            KENT_V200 = 1634,
            KENT_V210 = 1635,
            MEINECKE = 1636,
            MEINECKE_COMBINATION_BYPASS = 1637,
            MEINECKE_COMBINATION_MAIN = 1638,
            MEINEKE = 1639,
            MTK_CLORIUS = 1640,
            MW25CP = 1641,
            SAPPEL_ALTAIR = 1642,
            SAPPEL_ALTAIR_4_DIALS = 1643,
            SAPPEL_VEGA = 1644,
            SCHLUMBERGER__AQUADIS = 1645,
            SCHLUMBERGER_MS = 1646,
            SCHLUMBERGER_P50 = 1647,
            SCHLUMBERGER_P50E = 1648,
            SCHLUMBERGER_P95 = 1649,
            SENSUS__WP_DYNAMIC_100 = 1650,
            TIMELY_EQUIPMENT__LXSC_25E = 1651,
            WATERTECH__WTC40P = 1652,
            WATERTECH_7_DIAL = 1653,
            WATERTECH_D6_WTC15P = 1654,
            WATERTECH_WTCP_6DIALS = 1655,
            WIATEAU_KIWA = 1656,
            ZENNER = 1657,
            Diehl_Altair_V3_15 = 1658,
            Diehl_Altair_V3_20 = 1659,
            Diehl_Altair_V3_25 = 1660,
            Diehl_Altair_V3_32 = 1661,
            Diehl_Altair_V3_40 = 1662,
            Diehl_Altair_V4_15 = 1663,
            Diehl_Aquarius_V3_15 = 1664,
            Diehl_Aquarius_V3_20 = 1665,
            Diehl_Aquila_V3_100 = 1666,
            Diehl_Aquila_V3_50 = 1667,
            Diehl_Aquila_V3_80 = 1668,
            Diehl_Aquila_V4_100 = 1669,
            Diehl_Aquila_V4_50 = 1670,
            Diehl_Aquila_V4_65 = 1671,
            Diehl_Aquila_V4_80 = 1672,
            Itron_Aquadis_15 = 1673,
            Itron_Aquadis_20 = 1674,
            Itron_Aquadis_25 = 1675,
            Itron_Aquadis_32 = 1676,
            Itron_Aquadis_40 = 1677,
            Itron_Flodis_15 = 1678,
            Itron_Flodis_20 = 1679,
            Itron_Flodis_25 = 1680,
            Itron_Flodis_32 = 1681,
            Itron_Flostar_100 = 1682,
            Itron_Flostar_150 = 1683,
            Itron_Flostar_40 = 1684,
            Itron_Flostar_50 = 1685,
            Itron_Flostar_65 = 1686,
            Itron_Flostar_80 = 1687,
            Itron_MSD_Cyble_25 = 1688,
            Itron_MSD_Cyble_32 = 1689,
            Itron_MSD_Cyble_40 = 1690,
            Itron_MSD_Cyble_50 = 1691,
            Itron_UnimagCyble_15 = 1692,
            Itron_UnimagCyble_20 = 1693,
            Itron_Woltex_M_100 = 1694,
            Itron_Woltex_M_125 = 1695,
            Itron_Woltex_M_150 = 1696,
            Itron_Woltex_M_200 = 1697,
            Itron_Woltex_M_250 = 1698,
            Itron_Woltex_M_300 = 1699,
            Itron_Woltex_M_400 = 1700,
            Itron_Woltex_M_50 = 1701,
            Itron_Woltex_M_500 = 1702,
            Itron_Woltex_M_65 = 1703,
            Itron_Woltex_M_80 = 1704,
            Sensus_120_15 = 1705,
            Sensus_120_20 = 1706,
            Sensus_120C_15 = 1707,
            Sensus_405S_15 = 1708,
            Sensus_405S_20 = 1709,
            Sensus_405S_25 = 1710,
            Sensus_405S_32 = 1711,
            Sensus_405S_40 = 1712,
            Sensus_420_420PC_15 = 1713,
            Sensus_420_420PC_20 = 1714,
            Sensus_420_420PC_25 = 1715,
            Sensus_420_420PC_30 = 1716,
            Sensus_420_420PC_40 = 1717,
            Sensus_620_15 = 1718,
            Sensus_620_20 = 1719,
            Sensus_620_25 = 1720,
            Sensus_620_30 = 1721,
            Sensus_620_40 = 1722,
            Sensus_620PC_15 = 1723,
            Sensus_620PC_20 = 1724,
            Sensus_620PC_25 = 1725,
            Sensus_820_15 = 1726,
            Sensus_820_20 = 1727,
            Baylan_KK12 = 1728,
            Baylan_KK14 = 1729,
            Baylan_KK13 = 1730,
            Baylan_VK11 = 1731,
            Baylan_KK1 = 1732,
            Baylan_TK2 = 1733,
            Baylan_KK3 = 1734,
            LandisGyr_G350 = 1735
        }
        #endregion

        #region MeterTypeClass
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum MeterTypeClass
        {
            Unknown = 0,
            Tank = 1,
            GasMeter = 2,
            HeatMeter = 3,
            WaterMeter = 4,
            ElectricityMeter = 5,
            Nozzle = 6,
            Switcher = 7,
            PressureSensor = 8,
            I2CPressureTemperature = 9,
            I2CPressure = 10,
            I2CTemperature = 11,
            DigitalInput = 12,
            EVC = 13,
            HeatController = 14,
            Distributor = 15,
        }
        #endregion

        #region MeterTypeGroup
        public enum MeterTypeGroup
        {
            HAVING_VOLUME_TABLE = 1,
        }
        #endregion

        #region Module
        // UWAGA: zmiany wprowadzac rowniez w BD! - [DB].[MODULE]
        public enum Module : int
        {
            Unknown = 0,
            CORE_Controller = 1,
            CORE_Scheduler = 2,
            CORE_Actions = 3,
            CORE_Alarms = 4,
            DAQ_Controller = 5,
            DAQ_Router = 6,
            DAQ_Transmission = 7,
            DW_Controller = 8,
            DW_Scheduler = 9,
            DW_Estimator = 10,
            DW_ETL = 11,
            Operator = 12,
            Basic = 13,
            WebIMR = 14,
            DataService = 15,
            RDPConnection = 16,
            SDServicePostVerifier = 17,
            TMQ_ShippingTest = 18,
            SRTPostVerifier = 19,
            SIMA = 20,
            SITA = 21,
            DB_StoredProcedure = 22,
            IMRServiceCentre = 23,
            IMRMyServiceCentre = 24,
            IMRServiceTerminal = 25,
            IMRPrepaidManager = 26,
            DW_Forecourt = 27,
            DW_SIR = 28,
            DIMA = 29,
            TankMonitoring = 30,
            MON_Monitoring = 31,
            IMRMeterManager = 32,
            SmartGasMetering = 33,
            WebAIUT = 34,
            FDService = 35,
            DW_Intelligence = 36,
            TMUserPortal = 37,
            Logger = 38,
            DBCollector = 39,
            GSMApi = 40,
            ESBDriver = 41,
            DW_Billing = 42,
            WCF_Billing = 43,
            OrangePL_SMS_EPayments = 44,
            OrangePL_USSD_EPayments = 45,
            OrangePL_OFIN_EPayments = 46,
            SmartGas = 47,
            SmartGasTester = 48,
            WCF_Futuristic = 49,
            WebSIMAX = 50,
            NGNInterface = 51,
            ViewsManager = 52,
            Maximo = 53,
            AGsmScada = 54, // specjalny modu� (systemowy serwis) dla Italianc�w (pilota� 50 urz�dze� :)
            SITAInterface = 55,
            Worker = 56,
        }
        #endregion

        #region ModuleCommandType
        public enum ModuleCommandType : int
        {
            DumpLog = 1,
            GetState = 2,
            SaveState = 3,
            LoadState = 4,
        }
        #endregion Definitions

        #region NotificationDeliveryType
        public enum NotificationDeliveryType
        {
            SMS = 1,
            GCM = 2,
            EMail = 3,
        }
        #endregion

        #region NotificationType

        public enum NotificationType
        {
            TopUpActionPending = 1,
            TopUpActionDelivered = 2,
            TopUpActionError = 3,
            CommissionActionPending = 4,
            CommissionActionDelivered = 5,
            CommissionActionError = 6,
            DecommissionActionPending = 7,
            DecommissionActionDelivered = 8,
            DecommissionActionError = 9,
            ChangeTariffActionPending = 10,
            ChangeTariffActionDelivered = 11,
            ChangeTariffActionError = 12,
            ChangeECParametersActionPending = 13,
            ChangeECParametersActionDelivered = 14,
            ChangeECParametersActionError = 15,
            ValveOpened = 16,
            ValveClosed = 17,
            CreditBelowAlarmLevel = 18,
            CreditNotification = 19,
            TextNotification = 20,
            GetEmergencyCredit = 21,
            ValveClosedReadyToOpen = 22,
            CreditStatus = 23,
            ImageNotification = 24
        }

        #endregion

        #region NozzleState
        public enum NozzleState
        {
            None = 0,
            Inoperative = 1,
            Closed = 2,
            Idle = 3,
            Calling = 4,
            Authorised = 5,
            Started = 6,
            SuspendStarted = 7,
            Fueling = 8,
            SuspendedFuelling = 9,
        }
        #endregion

        #region NozzleType
        public enum NozzleType
        {
            Unknown = 0,
            LON = 1,
            Wayne = 2,
            Bennett = 3,
            Gilbarco = 4,
            DOMS = 5,
            Tokheim = 6,
            TESS = 7,
            DART = 8
        }
        #endregion

        #region ObjectHistoryEventType
#if !__MOBILE__
        [DataContract]
        public enum ObjectHistoryEventType
        {
            [EnumMember]
            ShippedToClient = 1,
            [EnumMember]
            ApprovedForService = 2,
            [EnumMember]
            ServicePerformed = 3,
            [EnumMember]
            InstalledAtLocation = 4,
            [EnumMember]
            RemovedFromLocation = 5,
            [EnumMember]
            AddedToPackage = 6,
            [EnumMember]
            ReceivedByFitter = 7,
            [EnumMember]
            DeinstalledByFitter = 8,
            [EnumMember]
            RelatedWithTask = 9,
            [EnumMember]
            SimCardAssigned = 10,
            [EnumMember]
            SimCardRemoved = 11,
            [EnumMember]
            SendToServer = 12,
            [EnumMember]
            ChangedStatus = 13,
            [EnumMember]
            RelatedWithIssue = 14,
            [EnumMember]
            ChangedEquipment = 15,
            [EnumMember]
            StepPerformed = 16
        }
#endif
        #endregion

        #region OKOError
        // UWAGA: nie zmieniac identyfikatorow sa istotne!
        public enum OKOError
        {
            ApulseError = 1,
            ValdriverError = 2,
            ModemError = 3,
            SIMCardError = 4,
            LoggingError = 5,
            GPRSError = 6,
            LowLevelBattery = 7,
            VeryLowLevelBattery = 8,
        }
        #endregion OKOError

        #region OperatorLoginStatus
        // sluzy do okreslania statusu logowania operatora, czy udane/niepoprawne haslo/niewazne konto itd.
        public enum OperatorLoginStatus
        {
            Unknown = 0,
            Valid = 1,
            InvalidPassword = 2,
            Blocked = 3,
            AccountNotValid = 4,
            NoPermission = 5,
        }
        #endregion

        #region PackageStatus

        public enum PackageStatus
        {
            New = 1,
            Sent = 2,
            Wrong = 3,
            Accepted = 4,
            Distributed = 5,
            Expired = 6,
            Removed = 7,
            NewOrder = 8,
            Ordered = 9,
            ToCompletion = 10,
            InCompletion = 11,
            OrderPartiallyCompleted = 12,
            OrderCompleted = 13,
        }

        #endregion

        #region PacketStatus
        // UWAGA: zmiany wprowadzac rowniez w BD! - [DAQ].[PACKET_STATUS]
        public enum PacketStatus : int
        {
            Unknown = 0,
            AddedToSend = 1,
            Sent = 2,
            Delivered = 3,
            Received = 4,

            NotDelivered = 10,
            SendError = 11,

            // Router
            SendTimeOut = 20,	//???
            InvalidTransmissionDriver = 21,
            SerialNumberNotFoundInPacket = 22,

            //GPRS i SMS
            DriverNotInitiated = 30,
            PacketEmpty = 31,
            InvalidTransmissionType = 32,
            InvalidAddress = 33,
            EncodePacketError = 34,

            //TCP
            DeviceDisconnected = 40,
        }
        #endregion

        #region PaymentModule
        public enum PaymentModule
        {
            SGM = 1,
            TMUserPortal = 2,
            OrangePL_SMS = 3,
            OrangePL_USSD = 4,
            OrangePL_Ofin = 5,
            Billing = 6,
            MobilePOS = 7,
            SmartGas = 8
        }
        #endregion

        #region PerformanceMonitor
        public enum PerformanceMonitorObjects
        {
            Unknown = 0,
            Total = 1,
            Process = 2,
            Tank = 3,
            Service = 4,
            Aggregate = 5
        }

        public enum PerfMonitorFileType
        {
            Unknown = 0,
            Measurements = 1,
            Statistics = 2,
            Aggregates = 3
        }
        #endregion

        #region ProblemClass
        public enum ProblemClass
        {
            ProbeDrowned = 1,
            ProbeHang = 2,
            SingleLoss = 3,
            SingleSurplus = 4,
            SingleSurplusWaterInflux = 5,
            SingleSurplusMicromixing = 6,
            PermanentLossFuelLeak = 7,
            PermanentLossInnerLeak = 8,
            PermanentLossNozzleDecalibration = 9,
            PermanentSurplusWaterInflux = 10,
            PermanentSurplusInnerLeak = 11,
            PermanentSurplusNozzleDecalibration = 12,
            CalibrationCurveError = 13,
            FuelHighLevel = 14,
            FuelLowLevel = 15,
            Deadstock = 16,
            WaterHighLevel = 17,
            SirenFailed = 18,
            Density = 19,
            Overfill = 20,
            FuelLeakageDW = 21,
            FuelLeakageP = 22,
            //PermanentLossFuelLeak = 23,
            //PermanentSurplusInnerLeak = 24,
            UnderCountingGasMeter = 25,
            OverCountingGasMeter = 26,
            CounterMalfunction = 27,
            NoProblem = 28,
            IllegalDelivery = 29,
            TooSmallDetectedDelivery = 30,
            TooLargeDetectedDelivery = 31,
            PermanentSurplus = 32,
            PermanentLoss = 33,
            IrcSir2Loss = 34,
            ConstantLevel = 35,
            ProbeFailure = 36,
            DispenserFailure = 37,
            ATGSensorLiquid = 38,
            ATGSensorVapor = 39,
            MissingAlevelData = 40,
            InvalidAlevelData = 41,
            MissingOKOPacket = 42,
            StockOutRisk = 43,
            Overfill2 = 44,
            LostSaleByStockOut = 45,
            LateDelivery = 46,
            LateDeliveryRisk = 47,
            //New Problems frm ACO DecisionModule for bottle switcher
            MissingData = 48,
            FaultyDevice = 49,
            LostBankDetection = 50,
            UnauthorisedCylinderExchanged = 51,
            EarlyDelivery = 52,
            DeliveryOverdue = 53,
            NoDeliveryPlanned = 54,
            HumanInterferenceLeftToRightKnobChanged = 55,
            HumanInterferenceRightToLeftKnobChanged = 56,
            LeftBankActiveWithEmptySites = 57,
            RightBankActiveWithEmptySites = 58,
            LeftBankActiveNoDeliveryDetected = 59,
            RightBankActiveNoDeliveryDetected = 60,
            TotalCounterReset = 61
        }
        #endregion

        #region ProblemLevel
        public enum ProblemLevel
        {
            Unknown = 0,
            Info = 1,
            Warning = 2,
            Error = 3,
            ErrorMail = 4,
            ErrorSMS = 5,
            WarningMail = 6,
            WarningSMS = 7,
            DailyError = 8,
            DailyWarning = 9,
            SM = 10,
            CM = 11,
            TM = 12,
            RSAA = 13,
            PlipReportedEmail = 14,
            PlipReportedSMS = 15,
            PlipEscalatedEmail = 16,
            PlipEscalatedSMS = 17,
            PlipConfirmedEmail = 18,
            PlipConfirmedSMS = 19,
            PlipBLeak = 20
        }
        #endregion

        #region ProblemOperatorDecision
        public enum ProblemOperatorDecision
        {
            Undecided = 0,
            Confirmed = 1,
            IssueCreated = 2,
            Escalated = 3
        }
        #endregion

        #region ProcessingMethod
        public enum ProcessingMethod
        {
            ImrLanAnalog,
            ImrLanWithoutAddress,
            ImrLanFull,
            Ecl201,
            Ecl202,
            WmbusFull,
            WmbusWithoutAddress,
            Ampli,
            OKO5172Wan2
        }
        #endregion

        #region ProcessStatus
        public enum ProcessStatus
        {
            New = 1,
            Received = 2,
            Pending = 3,
            PartlyFinished = 4,
            FinishedOk = 5,
            NotFinishedYet = 6,

            Unknown = 7,
            ProtocolDriverError = 8,
            TransmissionDriverError = 9,
            DeviceDriverError = 10,
            ProcessTimeout = 11,
            ProcessBreak = 12,
            NoAddressForDevice = 13,
            DeviceNotFound = 14,

            Sent = 15,
            Delivered = 16,
            Confirmed = 17,
            Processing = 18,
            ErrorInDevice = 19,

            SendError = 20,
            Timeout = 21,

            Added = 22
        }
        #endregion

        #region ProcessType
        public enum ProcessType
        {
            Unknown = 0,
            Event = 1,
            Get = 2,
            Put = 3,
            Action = 4, // ?? nie wiem jeszcze po co
        }
        #endregion

        #region ProductCode
        public enum ProductCode
        {
            // jbularz: nazwy sie zmienily w bazie:
            // Pb_95 -> SFS_95, ON_Extra -> SFS_Diesel
            Unknown = 0,
            Pb_95 = 1,
            V_Power = 2,
            VP_Racing = 3,
            ON_Extra = 4,
            VP_Diesel = 5,
            LPG = 6,
            Oil = 7,
            Water = 8,
            Mono_Ethylene_Glycol = 9,
            Propan = 10,
            v108_Fyringsolie_Plus = 11,
            v116_Fyringsolie_Basis = 12,
            v200_Landbrugsdiesel = 13,
            v230_Transportdiesel = 14,
            v251_Off_Road_Diesel = 15,
            v515_Blyfri_95_Benzin = 16,
            v35430_Mobil_DTE_25 = 17,
            v233_Transportdiesel = 18,
            Master = 19,
            DB0 = 20,
            FDI = 21,
            TB7 = 22,
            FGX = 23,
            FYX = 24,
            BLY = 25,
            BGS = 26,
            BGX = 27,
            FDX = 28,
            Transportdiesel = 29,
            v110_Fyringsolie_Uden_Afgift = 30,
            Ursa_Premium_TD_10w40 = 31,
            Ursa_Premium_TD_15w40 = 32,
            Ursa_Ultra_LE_15w40 = 33,
            AdBlue = 34,
            Diesel = 35,
            Dye_Diesel = 36,
            Gasoline = 37,
            AVGas = 38,
            Jet_A1 = 39,
            v32020_UHPD_15W_40 = 40,
            v32080_UTTO_GL4_80W = 41,
            v34802_Mobil_Delvac_Synthetic_Gear_Oil_75W_140 = 42,
            v40950_Mobil_Delvac_XHP_ESP_10W_40 = 43,
            v31780_Mobil_Univis_N46 = 44,
            v34542_Mobil_ATF_320 = 45,
            v38622_OK_Bio_Hydraulik_Universal_32_46_68 = 46,
            v134770_Mobil_Delvac_Synthetic_Gear_Oil_75W_90 = 47,
            v134120_Mobil_Delvac_MX_15W_40 = 48,
            v134200_Mobilube_HDn_80W_140 = 49,
            v34192_Mobilube_HD_A_80W_90 = 50,
            Dangoedning = 51,
            v240950_Mobil_Delvac_XHP_ESP_10W_40 = 52,
            v234202_Mobilube_HD_N_80W_140 = 53,
            Gas = 54,
            Natural_Gas = 55,
            LNG = 56,
            Marine_Diesel_Oil = 57,
            THF_1000 = 58,
        }
        #endregion

        #region ProfileType
        public enum ProfileType
        {
            Unknown = 0,
            Oil = 1,
            LPG = 2,
            ATG = 3
        }
        #endregion

        #region Protocol
        // UWAGA: zmiany wprowadzac rowniez w BD
        public enum Protocol : int
        {
            Unknown = 0,
            Direct = 1,
            ImrLan = 2,
            WMBus = 3,
            WMBusR2S = 4,
            Analog = 5,
            ImrWan1 = 6,
            ImrWan2 = 7,
            ARange = 8,
            Air = 9,
            Asi = 10,
            MacR = 11,
            ModbusEVC = 12,
            MBUS = 13,
            ImrWan3 = 14,
            CtrUni = 15,
            MacRv2 = 16,
            Modbus = 17,
            Danfoss = 18,
            WMBusStandard = 19,
            ImrModbus = 20,
            Radian = 21,
            Wavenis = 22, 
            SmartGas = 23,
            Sigfox = 24
        }
        #endregion

        #region  ReconProblemType
        public enum ReconProblemType : int
        {
            Unknown = 0,
            OK = 1,
            NoChosenDayDispencer = 2,
            NoPreviousDayDispencer = 3,
            NoDataToRecalculate = 4
        }
        #endregion

        #region ReconType
        public enum ReconType : int
        {
            Unknown,
            Manual,
            Calculated,
            Mixed
        }
        #endregion

        #region ReferenceType - prosze przeczytac instrukcje w komentarzu przed dodaniem
        // UWAGA: zmiany wprowadzac rowniez w BD! 
        /*
         * UWAGA: jak dodawac obsluge nowych ReferenceType:
         * 1. UI.Business.Controls\Utils\ReferenceTypeManager
         *      1.1. GetRepositoryItem - dodaj case z nowym ReferenceType w odpowiednim miejscu w zaleznosci od typu RepositoryItem, ktory chcemy uzyc
         *      - prosze zachowac porzadek alfabetyczny w switch-case!
         * 2. UI.Business\Components\CORE\ReferenceTypeComponent
         *      2.1. GetReferenceObjectList - dodaj case z nowym ReferenceType, a w nim zaladuj liste wszystkich elementow z bazy (jesli w bazie istnieja takie obiekty) lub z enuma
         *      - prosze zachowac porzadek alfabetyczny w switch-case!
         *      2.2. GetReferenceObject - dodaj case z nowym ReferenceType, a w nim zaladuj konkretny element po referenceValue z bazy (jesli w bazie istnieja takie obiekty) lub z enuma
         *      - prosze zachowac porzadek alfabetyczny w switch-case!
         *      2.3. GetReferenceObjectType - dodaj case z nowym ReferenceType, a w nim zwroc typ elementu, ktory bedzie zwracany dla danego referenceType
         *      - prosze zachowac porzadek alfabetyczny w switch-case!
         * 3. Jesli w bazie istnieje obiekt z nowego ReferenceType
         *      3.1. dodaj do klasy OpXXX dziedziczenie po interface IReferenceType
         *      3.2. dodaj implementacje interface - GetReferenceKey zwraca klucz obiektu (Id), GetReferenceValue zwraca dany obiekt
         * 4. Jesli w bazie nie istnieje obiekt z nowego ReferenceType
         *      4.1. dodaj w UI.Business\Components\CORE\ReferenceTypeComponent -> GetReferenceObjectKey zwracanie wartosci enuma
         *      
         * Prosze o tym pamietac na biezaco, gdyz aplikacje korzystaja z tych mechanizmow przy wyswietlaniu wartosci bedacych referencja.
        */
        public enum ReferenceType
        {
            None = 0,
            IdOperator = 1,
            IdAtgType = 2,
            IdReport = 3,
            IdReportDataType = 4,
            IdDataType = 5,
            SerialNbr = 6,
            IdMeter = 7,
            IdLocation = 8,
            IdProductCode = 9,
            IdDeviceDriver = 10,
            IdDistributor = 11,
            IdDelivery = 12,
            IdMeterType = 13,
            IdDeviceType = 14,
            IdValveState = 15,
            IdIssueStatus = 16,
            IdIssueType = 17,
            IdTaskStatus = 18,
            IdTaskType = 19,
            IdRoute = 20,
            IdRouteStatus = 21,
            IdRole = 22,
            /// <summary>
            /// Lista ID_OPERATOR podawana jako string: 1;2;45;678
            /// </summary>
            OperatorList = 23,
            /// <summary>
            /// dopisa� do DB, podawana jako nazwa typu akcji w formacie SMS_GET_DATA
            /// </summary>
            ActionType = 24,
            IdLanguage = 25,
            IdIMRServer = 26,
            IdFile = 27,
            IdDataSourceType = 28,
            IdActionDef = 29,
            IdProfile = 30,
            IdSlotType = 31,
            IdDetailView = 32,
            IdModule = 33,
            IdActivity = 34,
            IdCommandCodeType = 35,
            IdActionSmsText = 36,
            IdDepot = 37,
            IdDeliveryAdvice = 38,
            IdLocationStateType = 39,
            IdLocationType = 40,
            IdAggregationType = 41,
            IdConfigurationProfile = 42,
            IdTariff = 43,
            IdWMBUSTransmissionType = 44,
            IdMeterTypeGroup = 45,
            IdDescr = 46,
            IdSupplier = 47,
            IdAlarmText = 48,
            IdDeviceDashboardMainChartView = 49,
            IdDeviceDashboardAdditionalChartView = 50,
            IdMeterTypeClass = 51,
            IdFileExtension = 52,
            IdDeviceStateType = 53,
            /// <summary>
            /// Lista ID_TASK_STATUS podawana jako string: 1;2;45;678
            /// </summary>
            TaskStatusList = 54,
            IdGross = 55,
            IdNet = 56,
            IdReportType = 57,
            IdFaultCode = 58,
            IdTransmissionType = 59,
            IdUIObjectProperties = 60,
            IdDataFormatGroup = 61,
            IdArticle = 62,
            IdTaskGroup = 63,
            IdFileType = 64,
            IdIssue = 65,
            IdTask = 66,
            IdReferenceType = 67,
            IdMapMarkerTextType = 68,
            IdTransmissionDriver = 69,
            IdSimCard = 70,
            SITAUIObjectProperties = 71,
            IdVersion = 72,
            IdActor = 73,
            IdDeviceTypeGroup = 74,
            IdEtl = 75,
            IdOperatorLoginStatus = 76,
            TextValue = 77,
            IdDepositoryElement = 78,
            IdDeviceOrderNumber = 79,
            IdRefuel = 80,
            IdUnit = 81,
            IdSitaCommandType = 82,
            IdEmailSendingMode = 83,
            IdVersionState = 84,
            IdIssueGroup = 85,
            IdPriority = 86,
            IdSitaFitterDeposiotryMode = 87,
            IdAlarm = 88,
            IdAlarmGroup = 89,
            IdAlarmStatus = 90,
            IdRouteType = 91,
            IdRouteDef = 92,
            IdChartType = 93,
            IdView = 94,
            IdAlarmType = 95,
            IdAlarmDef = 96,
            IdDataArch = 97,
            IdFuelFillingPointType = 98,
            IdDispenserPumpType = 99,
            IdStationType = 100,
            IdStationProfileType = 101,
            IdAction = 102,
            IdActorGroup = 103,
            IdVersionElement = 104,
            IdViewColumn = 105,
            IdProtocol = 106,
            IdActionStatus = 107,
            IdDatabaseType = 108,
            LocationList = 109,
            IdDeviceInstalationPlace = 110,
            IdDeviceLocationCompassDirection = 111,
            IdDeviceConnection = 112,
            IdSitaInstallationFormType = 113,
            IdAlarmEvent = 114,
            DistributorList = 115,
            MeterTypeList = 116,
            DeviceTypeList = 117,
            AreaList = 118,
            IdDataTypeGroup = 119,
            IdDataTypeInGroup = 120,
            IdTaskTypeGroup = 121,
            IdCurrency = 122
        }
        #endregion
        #region RefuelState
        public enum RefuelState
        {
            UNKNOWN = 0,
            START_REFUEL = 1,
            REFUEL = 2,
            BREAK = 3,
            WAIT = 4,
            FAIL = 5,
            END_REFUEL = 6,
            COMMIT_REFUEL = 7,
            DELETED = 8,
            ENDING_REFUEL = 9,
            START_RESIDUAL = 10,
            RESIDUAL = 11,
            END_RESIDUAL = 12
        };
        #endregion

        #region ReportScheduleMode
        public enum ReportScheduleMode : int
        {
            Unknown = 0,
            First = 1,
            Closest = 2,
            Last = 3
        }
        #endregion
        #region ReportScheduleUnit
        public enum ReportScheduleUnit : int
        {
            Unknown = 0,
            Month = 1,
            Week = 2,
            Day = 3,
            Hour = 4,
            Minute = 5
        }
        #endregion
        #region ReportPriority
        public enum ReportPriority : int
        {
            Unknown = 0,
            Low = 1,
            Standard = 2,
            Height = 3,
            Urgent = 4
        }
        #endregion
        #region ReportState
        public enum ReportState : int
        {
            Unknown = 0,
            New = 1,
            Escalated = 2,
            Reported = 3,
            Confirmed = 4
        }
        #endregion
        #region ReportType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum ReportType : int
        {
            Unknown = 0,
            MeasuresReport = 1,
            PredefinedOnStoredProcedure = 2,
            FileReport = 3,
            PlipA = 4,
            PlipB = 5,
            PlipC = 6,
            JobReport = 7,
            ServerReportingServices = 8
        }
        #endregion

        #region RouteStatus
        public enum RouteStatus
        {
            UNKNOWN = 0,
            PLANNED = 1,
            COMMITTED = 2,
            IN_PROGRESS = 3,
            FINISHED = 4,
            CLOSED = 5,
            DOWNLOADED = 6,
            UPLOADED = 7,
            ACCOMPLISHED = 8,
            CANCELLED = 9
        };
        #endregion
        #region RouteType
        public enum RouteType
        {
            UNKNOWN = 0,
            READOUT = 1,
            INSTALLATION = 2,
            SERVICE = 3,
            DELIVERY = 4
        }
        #endregion

        #region RunActionResult
        // wynik proby uruchomienia akcji
        public enum RunActionResult
        {
            OK = 0,
            Exception,
            MSMQNotInstalled,
            MSMQParamsNotFound,
            CouldNotRunAction,
            MappingNotFound,
        }
        #endregion

        #region ScheduleRelativeDay
        // UWAGA: nie zmieniac identyfikatorow sa istotne!
        public enum ScheduleRelativeDay
        {
            Unknown = 0,
            Sunday = 1,
            Monday = 2,
            Tuesday = 3,
            Wednesday = 4,
            Thursday = 5,
            Friday = 6,
            Saturday = 7,
            Day = 8,
            WeekDay = 9,
            WeekendDay = 10,
        }
        #endregion
        #region ScheduleRelativeInterval
        // UWAGA: nie zmieniac identyfikatorow sa istotne!
        public enum ScheduleRelativeInterval
        {
            Unknown = 0,
            First = 1,
            Second = 2,
            Third = 4,
            Fourth = 8,
            Last = 16,
        }
        #endregion
        #region ScheduleSubdayType
        // UWAGA: nie zmieniac identyfikatorow sa istotne!
        public enum ScheduleSubdayType
        {
            Unknown = 0,
            SpecifiedTime = 1,
            Seconds = 2,
            Minutes = 4,
            Hours = 8,
        }
        #endregion
        #region ScheduleType
        // UWAGA: nie zmieniac identyfikatorow sa istotne!
        public enum ScheduleType
        {
            Unknown = 0,
            Once = 1,
            Daily = 4,
            Weekly = 8,
            Monthly = 16,
            MonthlyRelative = 32,
        }
        #endregion ScheduleType

        #region Service

        public enum DamageLevel
        {
            NotAvailable = 0,
            Lack = 1,
            Acceptable = 2,
            ToService = 3,
            Disqualifying = 4
        }

        public enum ServiceListReportType
        {
            Service = 1,
            ReceptionToService = 2,
        }

        // UWAGA: zmiany wprowadzac rowniez w BD! - [CORE_IMRSC].[SERVICE_REFERENCE_TYPE]
        public enum ServiceReferenceType
        {
            IdShippingList = 5,
            IdServiceList = 6,
            BatteryLevelUnderLoad = 7,
            IdServicePool = 8,
            BatteryVolume = 9,
            BatteryLevel = 10,
            Collector = 11,
            IdImrServer = 12,
            SimCardPhone = 13,
            SimCardSerialNbr = 14,
            SimCardOperator = 15,
            SoftwareVersion = 16,
            SimHolderVcc = 17,
            StandardDescription = 18,
            MechanicalIndex = 19,
            ImrDeviceIndex = 20,
            CurrentValue = 21,
            RadioFrame = 22,
            BatchNumber = 23,
            IdDeviceOrderNumber = 24,
            PhotoDescription = 25,
            ManufacturingBatchNumber = 26,
            IdTask = 27,
        }

        #endregion

        #region SessionStatus
        // UWAGA: zmiany wprowadzac rowniez w BD! - [CORE].[SESSION_STATUS]
        public enum SessionStatus
        {
            Unknown = 0,
            New = 1,
            Initialized = 2,
            Validated = 3,
            NotValid = 4,
            Running = 5,
            Closed = 6,
            AutoClosed = 7,
            Timeouted = 8,
        }
        #endregion

        #region ShippingListDocumentType
        public enum ShippingListDocumentType
        {
            GASPOLTextFile = 1,
            IMRDeviceFile = 2,
            ShippingListProtocol = 3,
            ShippingListReport = 4,
            SimaServerXML = 5
        }
        #endregion
        #region ShippingListType
        // UWAGA: zmiany wprowadzac rowniez w BD! - [CORE_IMRSC].[SHIPPING_LIST_TYPE]
        public enum ShippingListType
        {
            NewDevices = 1,
            ServicedDevices = 2,
            AfterLegalizationDevices = 3,
            ISUDevices = 4,
            SoldSharedDevices = 5,//Lista urz�dze� sprzedanych, wcze�niej udost�pnionych klientowi
            SentWithoutService = 6,
        }

        #endregion

        #region SlotType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum SlotType
        {
            None = 0,
            Analog = 1,
            Direct = 2,
            ImrLan = 3,
            Multiread = 4,
            Trs = 5,
            Rtrs = 6,
            Wmbus = 7,
            Atg = 8,
            Nozzle = 9,
            RS232 = 10,
            Bluetooth = 11,
            Digital = 12,
            Modbus = 13,
            WMBusStandard = 14,
            ImrModbus = 15,
            Radian = 16,
            ImrWan3Frame = 17,
            Wavenis = 18,
        }
        #endregion

        #region StationType
        // typ stacji
        public enum StationType
        {
            HighwayStation = 1, // stacja autostradowa
            UrbanStation = 2, // stacja miejska
        }
        #endregion

        #region StationProfileType
        public enum StationProfileType
        {
            Unknown = 0,
            Automatic = 1,
            Unmanned = 2,
            Manual = 3,
        }
        #endregion

        #region SynchroStatus
        public enum SynchroStatus
        {
            Undefinded,
            Sync,
            NotSync,
            Pending,
        }
        #endregion
        #region SynchroType
        public enum SynchroType
        {
            Unknown = 0,
            AggregateDaily = 1,
            NozzleTotalCounter = 2,
            TankMeasure = 3,
            Refuels = 4,
            StationInactivity = 5,
            Alarms = 6,
            Reconciliation = 7
        }
        #endregion

        #region RestartRequest
        public enum RestartRequest
        {
            Unknown = 0,
            TeamViewer = 1,
            IMRATGService = 2,
            PC = 3
        }
        #endregion

        #region Tables

        public enum Tables
        {
            UNKNOWN = 1000000,

            DATA = 0,

            DEVICE = 1,
            DEVICE_STATE_TYPE = 2,
            DEVICE_TYPE = 3,
            DEVICE_TYPE_CLASS = 4,
            DEVICE_STATE_HISTORY = 5,
            DEVICE_HIERARCHY = 6,
            DEVICE_DRIVER = 8,
            DEVICE_ORDER_NUMBER = 9,
            DEVICE_PATTERN = 100,

            SLOT_TYPE = 10,
            PROTOCOL = 11,
            PROTOCOL_DRIVER = 12,

            DISTRIBUTOR = 30,
            DISTRIBUTOR_PERFORMANCE = 31,
            DISTRIBUTOR_DATA = 32,

            METER = 40,
            METER_TYPE = 41,
            METER_TYPE_CLASS = 42,

            LOCATION = 50,
            LOCATION_TYPE = 51,
            LOCATION_STATE_TYPE = 52,
            LOCATION_EQUIPMENT = 53,
            LOCATION_STATE_HISTORY = 54,
            LOCATION_HIERARCHY = 55,

            SIM_CARD = 60,
            DEVICE_SIM_CARD_HISTORY = 61,
            SIM_CARD_DATA = 62,

            LANGUAGE = 70,
            DESCR = 71,
            PRIORITY = 72,

            TASK = 110,
            TASK_GROUP = 111,
            TASK_STATUS = 112,
            TASK_TYPE = 113,
            TASK_TYPE_GROUP = 114,
            TASK_HISTORY = 115,
            TASK_DATA = 116,

            ISSUE = 120,
            ISSUE_TYPE = 121,
            ISSUE_STATUS = 122,
            ISSUE_HISTORY = 123,

            ROUTE = 130,
            ROUTE_POINT = 131,
            ROUTE_DATA = 132,

            OPERATOR = 140,
            OPERATOR_DATA = 141,
            OPERATOR_ROLE = 142,

            DATA_TEMPORAL = 150,

            ACTOR = 160,
            ACTOR_GROUP = 161,
            ACTOR_IN_GROUP = 162,

            ARTICLE = 170,

            DEPOSITORY_ELEMENT = 180,

            SHIPPING_LIST = 200,
            SHIPPING_LIST_DATA = 201,
            SHIPPING_LIST_DEVICE = 202,
            SHIPPING_LIST_TYPE = 203,

            SERVICE_LIST = 210,
            SERVICE_LIST_DATA = 211,
            SERVICE_LIST_DEVICE = 212,
            SERVICE_LIST_DEVICE_DATA = 213,
            SERVICE_LIST_DEVICE_ACTION = 214,
            SERVICE_ACTION_RESULT = 215,
            SERVICE_LIST_DEVICE_ACTION_DETAILS = 216,
            SERVICE_LIST_DEVICE_DIAGNOSTIC = 217,
            SERVICE = 218,
            SERVICE_DATA = 219,
            SERVICE_PACKAGE = 220,
            SERVICE_PACKAGE_DATA = 221,
            SERVICE_IN_PACKAGE = 222,
            SERVICE_SUMMARY = 223,
            SERVICE_SUMMARY_DATA = 224,
            SERVICE_REFERENCE_TYPE = 225,
            SERVICE_DIAGNOSTIC_RESULT = 226,
            DIAGNOSTIC_ACTION = 227,
            DIAGNOSTIC_ACTION_DATA = 228,
            DIAGNOSTIC_ACTION_RESULT = 229,
            COMPONENT = 230,

            REPORT = 250,
            REPORT_DATA = 251,
            REPORT_PERFORMANCE = 252,

            IMR_SERVER_PERFORMANCE = 260,

            TRANSMISSION_DRIVER = 270,
            TRANSMISSION_DRIVER_PERFORMANCE = 271,
            TRANSMISSION_DRIVER_DATA = 272,

            DATA_TRANSFER = 280,

            CODE = 290,

            AUDIT = 300,
            DATA_TYPE = 301,
            DATA_TYPE_GROUP = 302,
            DATA_TYPE_IN_GROUP = 303,

            UNIT = 304,

            ETL = 350,
            ETL_PERFORMANCE = 351,

            ROLE = 360,
            ROLE_ACTIVITY = 361,

            REFUEL = 370,
            REFUEL_DATA = 371,

            DATA_ARCH = 400,
            PROFILE = 401,
            PRODUCT_CODE = 402,
            PROFILE_DATA = 403,
            CONFIGURATION_PROFILE = 404,
            ACTIVITY = 405,
            ACTION = 406,
            ACTION_DATA = 407,
            MODULE = 408,
            ISSUE_DATA = 409,
            DEVICE_DISTRIBUTOR_HISTORY = 410,
            DEVICE_WARRANTY = 411,
            DATA_TEMPORTAL = 412,
            ROUTE_TYPE = 413,
            ROUTE_STATUS = 414,
            ALARM = 415,
            ALARM_DATA = 416,
            ALARM_DEF = 417,
            ALARM_DEF_GROUP = 418,
            ALARM_EVENT = 419,
            ALARM_GROUP = 420,
            ALARM_GROUP_OPERATOR = 421,
            ALARM_HISTORY = 422,
            ALARM_MESSAGE = 423,
            ALARM_STATUS = 424,
            ALARM_TEXT = 425,
            ALARM_TEXT_DATA_TYPE = 426,
            ALARM_TYPE = 427,
            IMR_SERVER = 428,
            TRANSMISSION_TYPE = 429,
            DEVICE_CONNECTION = 430,
            TARIFF = 431,
            TARIFF_DATA = 432,

            VIEW = 500,
            VIEW_COLUMN = 501,
            VIEW_COLUMN_DATA = 502,
            VIEW_COLUMN_SOURCE = 503,
            VIEW_COLUMN_TYPE = 504,
            VIEW_DATA = 505,

            DEDICATED_DISTRIBUTOR_LOCATION_GROUPS = 1000,
        }

        #endregion

        #region Tariff

        #region TariffType

        public enum TariffType
        {
            PL = 1,
            ROAMING_EU = 2,
            ROAMING_GBL = 3,
        }

        #endregion

        #region TariffService

        public enum TariffService
        {
            SMS = 1,
            GPRS = 2,
        }

        #endregion

        #endregion

        #region Task

        #region TaskStatus
        public enum TaskStatus
        {
            //Unknown = 1,
            New = 2,       // domyslny status gdy tworzymy nowy obiekt
            Added = 3,     // gdy WAN zaakceptuje pusci do wyslania
            AddedToSend = 4,     // gdy wroci od Routera ze przyjal
            //Pending = 4,
            Error = 5,     // jakis bald nastal od Routera
            //IWPError = 6,
            Timeout = 7,   // czas na odebranie odpowiedzi uplynal
            Sent = 8,      // wyslany przez Router
            //NotSent = 9,
            //SendError = 10,
            //SendTimeOut = 11,
            Delivered = 12,// dostarczony (potwierdzenie odboiru jak w SMS)
            //NotDelivered = 13,
            Received = 14, // odebrany Event (akcja od RTU)
            PartlyFinished = 15, // odpowiedz na taska niepelna
            Finished = 16,       // poprawna odpowiedz na Taska od SC
            //DriverNotInitiated = 17,
            //DriverError = 18,
            Broken = 19,         // usuniety na sile z WANa
            //Confirmed = 20,
            //Processing = 21,
            //ErrorInDevice = 22,
        }
        #endregion TaskStatus

        #region TaskSuspensionReason

        public enum TaskSuspensionReason
        {
            ClientFault = 0,
            AiutFault = 1,
        }

        #endregion
        #region TaskProceedProblem

        public enum TaskProceedProblem
        {
            Other = 0,
            GSMCoverageLack = 1,
            TwoManJob = 2,
            ChamberCannotHouseLogger = 3,
            CustomerRefusal = 4,
            FaultyLogger = 5,
            FaultyMeter = 6,
            IncorrectMeterDetails = 7,
            MeterDisconected = 8,
            MeterNotPulsed = 9,
            MeterPulseOutputFaulty = 10,
            NoAccessToMeter = 11,
            NoAccessToSite = 12,
            NoPulseOutputAvailable = 13,
            NonStandardEquipmentRequired = 14,
            SiteUnsafe = 15,
            UnableToLocateLogger = 16,
            UnableToLocateMeter = 17,
            UnableToLocateSite = 18
        }

        #endregion

        #region TaskOperationType

        public enum TaskOperationType
        {
            Location = 1,
            Distributor = 2
        }

        #endregion

        #region TaskServiceFormType

        public enum TaskServiceFormType
        {
            FlogasServiceForm = 0,
            AmerigasServiceForm = 1,
            AmerigasInstallationForm = 2,
            Unknown = 3,
            OrlenInstallationForm = 4,
        }

        #endregion

        #region TaskFormType

        public enum TaskFormType
        {
            Standard = 0,
            Heating = 1,
            FuelSupplyAudit = 2,
            Water = 3,
            Gas = 4,
            Pressure = 5,
            LPG = 6,
        }

        #endregion

        #region TaskAppointmentScheduleMode

        public enum TaskAppointmentScheduleMode
        {
            Other = 1,
            Phone = 2,
            Email = 3,

        }

        #endregion
        #region TaskAppointmentResult

        public enum TaskAppointmentResult
        {
            Success = 1,
            Failure = 2,
        }

        #endregion

        #region TaskPaymentPartType

        public enum TaskPaymentPartType
        {
            //Fitter < 1000
            FitterBase = 1,
            FitterPremium = 2,
            FitterRevisit = 3,
            FitterSecondSetInstallation = 4,//instalacja drugiego zestawu na lokalziacji, czasami inna kwota wynikaj�ca z umowy
            FitterInstallationAberdeenArea = 5,//instalacja w rejonie Aberdeen - umowa Avanti
            FitterAberdeenAreaSecondSetInstallation = 6,
            FitterSetInstallation = 7,

            //Client >= 1000
            ClientBase = 1000,
            ClientDevice = 1001,//per urz�dzenie zamontowane w zadaniu
            ClientInstallationTransitAccomoodataion = 1002,//przejazy, zakwaterowanie serwisanta podczas instalacji
            ClientTankMonitoringHardwareRemoval = 1003,
            ClientOKOX303SmartMeterPlugInUnitRemoval = 1004,
            ClientSecondSetInstallation = 1005,//instalacja drugiego zestawu na lokalziacji, czasami inna kwota wynikaj�ca z umowy
            ClientInstallationAberdeenArea = 1006,//instalacja w rejonie Aberdeen - umowa Avanti
            ClientAberdeenAreaSecondSetInstallation = 1007,
            ClientSetInstallation = 1008,
            ClientSimCardActivation = 1009,
            ClientServiceTransitAccomoodataion = 1010,//przejazy, zakwaterowanie serwisanta podczas serwisu
            ClientServiceAberdeenArea = 1011,//Serwis Aberdeen 
            ClientServiceNotCoveredByFlatFee = 1012,//Serwis serwis nie obj�ty sta�� op�at� 
            ClientServiceHardwarePenalty = 1013,//Kara za op�nienia podczas serwisu dotycz�cego Hardware
            ClientServiceSoftwarePenalty = 1014,//Kara za op�nienia podczas serwisu dotycz�cego Software
            ClientSetInstallationPremium = 1015, // to samo co 1008 ale premium
            ClientSecondSetInstallationPremium = 1016, //instalacja drugiego zestawu na lokalziacji, czasami inna kwota wynikaj�ca z umowy (premium)
            ClientSetInstallationAberdeenAreaPremium = 1017 //instalacja w rejonie Aberdeen - umowa Avanti (premium)
        }

        #endregion
        #region TaskPaymentType

        public enum TaskPaymentType
        {
            Fitter = 1,
            Client = 2,
        }

        #endregion
        #region TaaskWizardStepType

        public enum TaskWizardStepType
        {
            Unknown = 0,
            SingleTextStep = 1,
            SingleImageTextStep = 2,
            SingleImageDoubleTextStep = 3,
            DoubleImageDoubleTextStep = 4,
            Service_DownloadConfiguration = 5,
            Service_DownloadArchive = 6,
            Service_ReprogramDevice = 7,
            Service_RestoreConfiguration = 8,
            Service_OKOExchange = 9,
            Service_DeviceDiagnostic = 10,
            Service_DownloadEventLogs = 11,
            Service_BatteryExchange = 12,
            Service_SIMCardTest = 13,
            Service_ValveTest = 14,
            TripleImageWithText = 15,
            Service_Synchronization = 16,

            Service_ReadDiagnosticData = 17,
            Service_ChangeFrequency = 18,
            GSMTest = 19,
            ClusterScan = 20,
            Service_KnowNodes = 21,
            Service_MultihopConfiguration = 22,

            InstallationForm1Activity = 101,
            InstallationForm1aLocationDetalis_BuildingModelActivity = 102,
            InstallationForm1aLocationDetalisActivity = 103,
            InstallationForm1bAdapter = 104,
            InstallationForm1SCActivity = 105,
            InstallationForm2aGasmeterActivity = 106,
            InstallationForm2bGasmeterActivity = 107,
            InstallationForm3Activity = 108,
            InstallationForm3NetworkAction = 109,
            InstallationForm3SMSAction = 110,
            InstallationFormEVC = 111,
            InstallationFormEVC_SMS = 112,
            InstallationFormEVC_Modem = 113,
            InstallationFormSendAction = 114,
        }

        #endregion

        #region TaskVisitStepType
        public enum TaskVisitStepType
        {
            Unknown = 0,
            ElementInstallation = 1,
            ElementUninstall = 2,
            DeviceExchange = 3,
            ActionSending = 4,
            MeterAddtion = 5,
            DeviceMove = 6,
            PlannedVisitDate = 7,
            CurrentMeterReadout = 8,
            PhotoDone = 9,
            PhotoDoneProblem = 10,
            PhotoUploaded = 11,
            PhotoUploadedProblem = 12,
            Export = 13,
            ExportStep = 14,
            MappingsChanged = 15,
            HeatOkoInstallation = 16,
            WatermeterInstallation = 17,
            ConverterInstallation = 18,
            BatteryChange = 19,
            ValveTestResult = 20,
            DeviceReprogram = 21,
            SimCardTest = 22,
            DeviceCriticalErrorTest = 23,
            ValveStateChange = 24,
            TankPosition = 25,
            ArchiveRead = 26,
            EventLog = 27,
            NewMeterCounterResynchronization = 28,
            OldMeterCounterResynchronization = 29,
            ConfigurationRestore = 30,
            InstalledGasmeter = 31,
            DeinstalledGasmeter = 33,
            ConfigurationDownload = 34,
            ClusterTest = 35,
            InstallationWithSynchronization = 36,
            PinValue = 37,
            Diagnostic = 38,
            InstalledGasmeterRadioQualityVerificationDeviceSerialNbr = 39,
            InstalledMeterAdapterSerialNbr = 40,
            DeviceValidImpulseCountTest = 41,
            InstalledWaterMeterDevice = 42,
            HubInstallation = 43,
            WalkedByReadout = 44,
        }
        #endregion

        #region TaskExportStepResult
        public enum TaskExportStepResult
        {
            NotStarted = 0,
            Failed = 1,
            Succeded = 2,
            Exporting = 3,
        }
        #endregion
        #region TaskExportStepType
        public enum TaskExportStepType
        {
            ExportLocation = 1,
            LocationMappingsChange = 2,
            ExportDevices = 3,
            ExportMeters = 4,
            ExportLocationEquipment = 5,
            ExportDeviceHierarchy = 6,
            ExportLocationHierarchy = 7,
            ActionSent = 8,
            ActionReceived = 9,
        }
        #endregion

#endregion

        #region TimePeriodType

        public enum TimePeriodType
        {
            Day = 1,
            Week = 2,
            Month = 3,
            Custom = 4
        }

        #endregion TimePeriodType

        #region TMQObjectServiceAction
        [Flags]
        public enum TMQObjectServiceAction : long
        {
            NONE = 0,
            REPLACE_OKO_CIRCUIT = 1,
            REPROGRAM_OKO = 2,
            REPLACE_BATTERY = 4,
            REPLACE_ALEVEL = 8,
            COUNTER_SYNCHRONIZATION = 16,
            CLOCK_SYNCHRONIZATION = 32
        }
        #endregion

        #region TransmissionDriverModemFlowControl
        public enum TransmissionDriverModemFlowControl
        {
            None,
            Hardware,
            XonXoff
        }
        #endregion
        #region TransmissionDriverStatus
        public enum TransmissionDriverStatus
        {
            Unknown = 0,
            Info = 1,
            ModemDisconnected = 2,
            ModemConnected = 3,
            ModemRemoved = 4,
            SIMRemoved = 5,
            SIMInserted = 6,
            NetworkDisconnected = 7,
            NetworkConnected = 8,
        }
        #endregion TransmissionDriverStatus
        #region TransmissionDriverType
        // UWAGA: zmiany wprowadzac rowniez w BD! - [DAQ].[TRANSMISSION_DRIVER_TYPE]
        public enum TransmissionDriverType : ushort
        {
            Unknown = 0,
            SMS = 1,
            GPRS = 2,
            TRS = 3,
            RS = 4,
            RING = 5,
            FTP = 6,
            TCP = 7,
        }
        #endregion TransmissionDriverType
        #region TransmissionStatus
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum TransmissionStatus
        {
            Unknown = 0,
            New = 1,
            Running = 2,
            Sent = 3,
            Delivered = 4,
            Error = 5,
            Confirmed = 6,
            Received = 7,
        }
        #endregion
        #region TransmissionType
        // UWAGA: zmiany wprowadzac rowniez w BD! - [DAQ].[TRANSMISSION_TYPE], [CORE].[TRANSMISSION_TYPE]
        public enum TransmissionType
        {
            /// <summary>
            /// Unknonw
            /// </summary>
            Unknown = 0,
            /// <summary>
            /// Zmodyfikowana wersja binary na potrzeby AIUT (miedzy innym IWD)
            /// </summary>
            Binary = 1,
            /// <summary>
            /// Kodowanie UCS2, Encoding.BigEndianUnicode
            /// </summary>
            Text = 2,
            /// <summary>
            /// Kodowanie 7bitowe, Encoding.ASCII
            /// </summary>
            Text7bit = 3,
            /// <summary>
            /// Oryginalne binary (miedzy innymi PDU, pakiety GPRS)
            /// </summary>
            PDUBinary = 4,
            /// <summary>
            /// E-mail (np alarmowy)
            /// </summary>
            EMail = 5,
            /// <summary>
            /// Dzwony
            /// </summary>
            Ring = 6,
            /// <summary>
            /// Przeznaczone do wyslania do WebService-u
            /// </summary>
            WebService = 7,
        }
        #endregion TransmissionType

        #region UIObjectProperties
        public enum UIObjectProperties
        {
            DeviceType = 1,
            DeviceStateType = 2,
            DeviceDistributor = 3,
            DeviceDataList = 4,
            TaskFormTaskType = 5,
            TaskGroup = 6,
            TaskPriority = 7,
            TaskPaymentAndPriorityGuidelines = 8,
            TaskIMRMySCVisible = 9,
            TaskIssue = 10,
            TaskOperationCode = 11,
            TaskSalesOrderNumber = 12,
            SimCardCollectingPerson = 13,
            SimCardCollectionDate = 14,
            TaskFitterOrderedToPay = 15,
            TaskFitterPaid = 16,
            TaskFitterPaymentDate = 17,
            TaskClientOrderedToPay = 18,
            TaskClientPaid = 19,
            TaskClientPaymentDate = 20,
            TaskCreationDate = 21,
            TaskDeadline = 22,
            TaskLocation = 23,
            TaskArrangedFitterVisitDate = 24,
            TaskArrangedFitterVisitBeginHour = 25,
            TaskArrangedFitterVisitEndHour = 26,
            TaskPlannedVisitPlanMode = 27,
            TaskPlannedVisitNotes = 28,
            TaskDistributor = 29,
            TaskType = 30,
            TaskStatus = 31,
            TaskStatusNotes = 32,
            TaskOperatorRegistering = 33,
            TaskOperatorPerforming = 34,
            TaskFitterVisitDate = 35,
            TaskValidationDate = 36,
            TaskAdditionalPayment = 37,
            TaskWarrantyService = 38,
            TaskDueDate = 39,
            TaskComplain = 40,
            TaskNotes = 41,
            TaskMaintenanceNotes = 42,
            RoutePointAccomodation = 43,
            TaskSummaryAttachments = 44,
            TaskSummaryPayments = 45,
            TaskSummaryReadoutsView = 46,
            TaskSummaryMapView = 47,
            TaskSummaryLocationEquipment = 48,
            TaskSummaryPlannedVisit = 49,
            RoutePointBaseInformation = 50,
            RoutePointOtherInformation = 51,
            TaskSummaryPoutePoint = 52,
            RoutePointStartDate = 53,
            RoutePointEndDate = 54,
            RoutePointType = 55,
            RoutePointDistributor = 56,
            RoutePointCoordinates = 57,
            RoutePointMileage = 58,
            RoutePointNotes = 59,
            ActionDefId = 60,
            ActionDefDevice = 61,
            ActionDefMeter = 62,
            ActionDefLocation = 63,
            ActionDefDataType = 64,
            ActionDefActionDataId = 65,
            ActionDefActionDataAdd = 66,
            ActionDefActionDataRemove = 67,
            ActionDefActionDataEdit = 68,
            ActionDefActionDataCopy = 69,
            ConfigurationControlDistributors = 70,
            TaskPlannedVisitPlanResult = 71,
            MeasurementEditTemplates = 72,
            LocationSize = 73,
            LocationOperationalCapacity = 74,
            LocationLOLevel = 75,
            LocationLOLOLevel = 76,
            LocationDaysToStockOut = 77,
            LocationNotes = 78,
            LocationIdLocation = 79,
            LocationActor = 80,
            LocationConsumer = 81,
            LocationHostCID = 82,
            DeviceState = 84,
            DevicePattern = 85,
            DeviceWarrantyDate = 86,
            DeviceGeneral = 87,
            DeviceDevices = 88,
            DeviceSchedules = 89,
            DeviceCodes = 90,
            DeviceHistory = 91,
            DeviceHistoryState = 92,
            DeviceHistoryEquipemnt = 93,
            DeviceHistoryHierarchy = 94,
            DeviceOperations = 95,
            DeviceCurrentLocation = 96,
            DeviceDetailsNotePanel = 97,
            DeviceHistoryNotes = 98,
            DeviceAnalogConfiguration = 99,
            DeviceOnSite = 100,
            DeviceProductionConfiguration = 101,
            DeviceSchedulesConfiguration = 102,
            DeviceUdiConfiguration = 103,
            DeviceMeasurements = 104,
            RoutePointDuration = 105,
            LocationPattern = 106,
            LocationImrServer = 107,
            LocationParameters = 108,
            LocationGroups = 109,
            LocationSchedules = 110,
            LocationDevices = 111,
            LocationInstalledDevices = 112,
            LocationInstalledMeters = 113,
            LocationHistory = 114,
            LocationHistoryState = 115,
            LocationHistoryEquipment = 116,
            LocationHistoryIssueTask = 117,
            LocationDevicesAll = 118,
            LocationHistoryStateNotes = 119,
            LocationInstallationParamsGroup = 120,
            LocationInstallationDIParamsGroup = 121,
            LocationInstallationTankParams = 122,
            LocationDistributor = 123,

            LocationAddRemoveDevices = 124,
            LocationAddRemoveMeters = 125,
            IssueHistoryDetails = 126,
            TasksModuleShowSOTAreas = 127,
            TasksModuleShowOperatorsAreas = 128,
            SimCardHistory = 129,
            LocationDataTypePanel = 130,
            TaskLocationPanelParams = 131,
            TaskAdditionalParameters = 132,
            DeviceParameters = 133,
            DeviceParenntDevices = 134,
            DeviceChildDevices = 135,
            MeterHistory = 136,
            MeterParameters = 137,
            DeviceExtendedParameters = 138,
            DistributorParameters = 139,
            TaskParameters = 140,
            IssueParameters = 141,
            IssueAdditionalParameters = 142,
            MeterDevices = 143,
            IssueAttachments = 144,
            TaskAttachments = 145,
            IssueTasks = 146,
            IssueHistory = 147,
            TaskHistory = 148,
            IssueEquipment = 149,
            TaskEquipment = 150,
            OperatorParameters = 151,
            OperatorSignature = 152,
            OperatorAvatar = 153,
            OperatorCarrier = 154,
            AlarmParameters = 155,
            AlarmMap = 156,
            RouteParameters = 157,
            RoutePoints = 158,
            RouteAssignedOperators = 159,
            RouteMap = 160,
            LocationSchema = 161,
            ReportParameters = 162,
            ReportGenereation = 163,
            ActorParameters = 164,
            ActorGroups = 165,
            ActorLocations = 165,
            SimCardParameters = 166,
            ActionParameters = 167,
            LocationGeneralSchema = 168,
            DeviceGeneralSchema = 169,
            MeterGeneralSchema = 170,
            LocationMap = 171,
            LocationExtendedParameters = 172,
            LocationMeasurement = 173,
            AlarmHistory = 174,
            LocationMeasurementPanel = 175,

        }
        #endregion

        #region UniqueType
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum UniqueType : int
        {
            Unknown = 0,
            UniqueInCustomerLocations = 1,
        }
        #endregion

        #region Unit
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum Unit : int
        {
            Unknown = 0,
            Base = 1,
            Time = 2,
            UnitError = 3,
            ConversionError = 4,
            WattHour = 5,
            KilowattHour = 6,
            MegawattHour = 7,
            Megajoule = 8,
            Gigajoule = 9,
            Meter = 10,
            Centimeter = 11,
            Millimeter = 12,
            Kilometer = 13,
            CubicMeter = 14,
            Liter = 15,
            CubicCentimeter = 16,
            Megahenry = 17,
            Watt = 19,
            Kilowatt = 20,
            Megawatt = 21,
            Gigawatt = 22,
            CubicMeterPerHour = 23,
            LiterPerHour = 24,
            CubicMeterPerSecond = 25,
            LiterPerSecond = 26,
            FractionalPercentPerSecond = 27, // 1/s
            FractionalPercentPerHour = 28, // 1/h
            PercentPerSecond = 29, // %/s
            PercentPerHour = 30, // %/h
            PermilPerSecond = 31, // �/s
            PermilPerHour = 32, // �/h
            Celsius = 33,
            Kelvin = 34,
            Fahrenheit = 35,
            Second = 36,
            Minute = 37,
            Hour = 38,
            Days = 39,
            Weeks = 40,
            FractionalPercent = 41, // [0-1]
            Percent = 42, // %
            Permil = 43, // �
            Volt = 44,
            Millivolt = 45,
            Ampere = 46,
            Milliampere = 47,
            // jednostka nazwana w bazie "-" = 48,
            Hertz = 49,
            Kilohertz = 50,
            Megahertz = 51,
            Gigahertz = 52,
            LGD = 53,
            LSP = 54,
            EKK = 55,
            SLB = 56,
            KAM = 57,
            // jednostka nazwana w bazie "@@@" = 58,
            HYD = 59,
            AQS = 60,
            UG = 61, // .UG
            // jednostka nazwana w bazie "@@@" = 62,
            SPX = 63,
            PLN = 64,
            Gr = 65,
            Pascal = 66,
            Bar = 67,
            Imp = 68,
            GigajoulePerHour = 69, // GJ/h
            MegajoulePerHour = 70, // MJ/h
            Millisecond = 71,
            Varh = 72,
            Var = 73,
            VAh = 74,
            VA = 75,
            Kvarh = 76,
            Kvar = 77,
            Decibel = 78,
            Megapascal = 79,
            KilogramPerHour = 80,
            TonnePerHour = 81,
            Kilogram = 82,
            Tonne = 83,
            Kilopascal = 84,
            Mlgr = 85,
            Euro = 89,
            Eurocent = 90,
            KilogramPerCubicMeter = 91, // kg/m3
            Millibar = 92,
            Hectopascal = 93,
            Decipascal = 94,
            MilliwattHour = 95,
            DecibelMilliwatts = 96, // dBm
            KilometerPerHour = 97,
            MolePercent = 98,
            KilowattHourPerCubicMeter = 99, // kWh/m3
            Bit = 100,//b
            Byte = 101,//B *8
            Kibibyte = 102,//KiB * 1024
            Mebibyte = 103, //MiB *1048576
            Gibibyte = 104, //GiB *1073741824
            Tebibyte = 105,//TiB *1099511627776
            Kilobyte = 106,//kB*1000
            Megabyte = 107,//MB *1000000
            Gigabyte = 108, //GB *1000000000
            Terabyte = 109,//TB *1000000000000
            Milliamperehour = 110,//mAh
            CubicFeet = 111//ft3

        }
        #endregion

        #region WorkStatus
        // UWAGA: zmiany wprowadzac rowniez w BD!
        public enum WorkStatus
        {
            Unknown = 0,
            New = 1,
            Running = 2,
            Succeeded = 3,
            Error = 4,
            Partly = 5,
            Stop = 6,
            Stopped = 7,
            Abort = 8,
            Aborted = 9,
            Waiting = 10,
        }
        #endregion

        #region ValveState
        public enum ValveState
        {
            Closed = 0, //VALVE_LOGICAL_CLOSE
            Opened = 1, //VALVE_LOGICAL_OPEN    
            LeakTestInProgress = 2,
            ValveIsClosing = 3,
            ValveIsOpening = 4,
            WaitingForOpen = 16,
            WaitingForClose = 32,
            //New WAN3 64 + n
            ClosedWaitingForConfirmOpen = 65, //VALVE_LOGICAL_CLOSE_CONFIRM_OPEN 
            OpenedLeakTestInProgress = 66, //VALVE_LOGICAL_OPEN_LEAK_TEST
            ClosedUnsuccessfulOpen = 67, //VALVE_LOGICAL_CLOSE_UNSUCCESSFUL_OPEN
            Unknown = 255,
        }
        #endregion ValveState

        #region VersionNbrFormat

        public enum VersionNbrFormat
        {
            AssemblyVersion4 = 1
        }

        #endregion

        #region VersionState

        public enum VersionState
        {
            RequestForChange = 1,
            Preparation = 2,
            Production = 3,
            PackageTransferToDMLLibrary = 4,
            Testing = 5,
            Validation = 6,
            ChangeDeployment = 7,
            ChangeRollback = 8,
            EarlyPostDeploymentSupport = 9
        }

        #endregion

        #region View

        #region ViewOperation 

        public enum ViewOperation
        {
            Unknown = 0,

            InsertRow = 1,
            UpdateRow = 2,
            DeleteRow = 3,
            MergeRow = 4,

            RebuildView = 5,

            AddColumn = 6,
            RemoveColumn = 7,
            RebuildColumn = 8,
			
			CreateView = 9,
			DeleteView = 10
        }

        #endregion 
        #region ViewColumnSource
        public enum ViewColumnSource
        {
            CORE = 1,
            DW = 2,
        }
        #endregion
        #region ViewColumnType
        public enum ViewColumnType
        {
            Unknown = 0,
            Standard = 1,
            BaseHelper = 2,
            ExtendedHelper = 3,
        }
        #endregion

        #endregion

        #region Vife
        /// <summary>
        /// Mo�liwe warto��i drugiego bajtu VIFE z konstrukcji DIV-VIFE (0F FF xx)
        /// </summary>
        public enum Vife : byte
        {
            IMR_WAN = 0x0,
            AIR = 0x1,
            RS2 = 0x2,
            IMRLAN = 0x3,
            IMRCABLE = 0x4,
            AMPLI = 0x5,
            AIR_ID_REQUEST = 0x11,
            RS2_ID_REQUEST = 0x12,
            IMRLAN_ID_REQUEST = 0x13,
            IMRCABLE_ID_REQUEST = 0x14
        }
        #endregion

        #region WarrantyLengthCalculationMode
        public enum WarrantyLengthCalculationMode
        {
            ShippingDate = 1,
            InstallationDate = 2,
            LastMonthDay = 3
        }
        #endregion

        #region SIMCardError
        public enum SIMCardError
        {
            NoError = 0,
            NotInserted = 2,
            Failure = 3,
            PINError = 4,
            PUKRequired = 5,
            TimeoutException = 7,
            NoConnectionWithGSM = 8,
            DeviceStateDepassivation = 9,
        }
        #endregion

        #region SIR2Decision
        public enum SIR2Decision : int
        {
            Inconclusive,
            Fail,
            Pass
        }
        #endregion
        #region SIR2InconclusiveCause
        public enum SIR2InconclusiveCause : int
        {
            RunError = 0,
            Parametres,
            DataFormat,
            MissingDays,
            Outliers,
            TrendRising,
            TrendChanging
        }
        #endregion

        #region Sita
        #region SitaWalkedByScenerioType
        public enum SitaWalkedByScenerioType
        {
            Broadcast,
            Multicast,
            StopGo,
            DailArchive
        }
        #endregion
        #region SitaOperationType
        public enum SitaOperationType
        {
            Unknown = 0,
            SMS = 1,
            GPRS = 2,
            OPTO = 3,
            Radio = 4,
            Manual = 5,
        }
        #endregion
        #region SitaCommandType
        public enum SitaCommandType
        {
            Unknown = 0,
            GetSitaVersion = 1,
            SendLogFile = 2,
            SendDbFile = 3,
            ExecuteDbCommand = 4,
        }
        #endregion
        #region SITACommunicationType
        public enum SITACommunicationType
        {
            Unknown = 0,
            RadioAIR = 1,
            RadioCC = 2,
            Opto = 3
        }
        #endregion
        #region SITACommunicationCommandType
        public enum SITACommunicationCommandType
        {
            Unknown = 0,
            WaterReadoutOneMonth = 1,
            WaterReadoutTwoMonth = 2,
            WaterReadoutThreeMonth = 3,
            WaterReadoutOneYear = 4,
            WaterReadoutDiagnostic = 5,
            WaterReadoutClearLatch = 6,
            WaterReadoutReadArchive = 7,
            WaterReadoutSleep = 8,
        }
        #endregion
        #region SitaCommandType
        public enum SitaFitterDeposiotryMode
        {
            OnlyDepositoryElement = 0,
            OnlyLocationEquipment = 1,
            Both = 2,
        }
        #endregion
        #region SitaInstallationFormType
        public enum SitaInstallationFormType
        {
            Unknown = 0,
            Gas = 1,
            LPG = 2,
            Heat = 3,
            Pressure = 4,
            GasImrSc = 5,
            Water = 6,
            WaterImrSc = 7,
            Oil = 8
        }
        #endregion
        #region SitaInstallationStatus
        public enum InstallationStatus
        {
            NotStarted = 0,
            Running = 1,
            Finished = 2,
        }
        #endregion
        #region SitaClusterTestApulseStatus
        public enum SitaClusterTestApulseStatus
        {
            ConfiguredReceived = 1,
            ConfiguredNotReceived = 2,
            NotConfiguredReceived = 3,
        }
        public enum SitaOperationSession
        {
            enumTryFaild = 0,
            enumTry = 1,
            enumCatch = 2,
            enumFinally = 3,
        }
        #endregion
        #region SITAWizardParameter
        public enum SitaWizardParameter
        {
            ValidateSerialNumber = 0,
            DedicatedDevice = 1,
        }
        #endregion
        #region SitaOKOPINMenuOption
        public enum SitaOKOPINMenuOption
        {
            ServiceMenu = 0,
            ValveMenu = 1,
        }
        #endregion
        #region SitaTaskProceedStepLatch
        public enum SitaTaskProceedStepLatch
        {
            TaskProceedAttachmentPhotoDoneStepLatch = 0,
            TaskProceedAttachmentPhotoDoneProblemStepLatch = 1,
            TaskProceedAttachmentPhotoUploadedStepLatch = 2,
            TaskProceedAttachmentPhotoUploadedProblemStepLatch = 3,
            TaskProceedProblemStepLatch = 4,
            TaskProceedMeterSerialNbrChangeStepLatch = 5,
            TaskProceedChangeBatteryActionStepLatch = 6,
            TaskProceedDeviceExchangeStepLatch = 7,
            TaskProceedSIMCardTestStepLatch = 8,
            TaskProceedValveStatusStepLatch = 9,
            TaskProceedDeviceReprogramStepLatch = 10,
            HeatInstallationProtocol = 11,
            TaskProceedTankMeterIsUndergroundStepLatch = 12,
            TaskProceedDeviceCriticalErrorsStepLatch = 13,
            TaskProceedArchiveReadStepLatch = 14,
            TaskProceedEventLogStepLatch = 15,
            TaskProceedNewMeterCounterResynchronizationStepLatch = 16,
            TaskProceedOldMeterCounterResynchronizationStepLatch = 17,
            TaskProceedConfigurationRestoreStepLatch = 18,
            TaskProceedInstalledGasmeterStepLatch = 19,
            TaskProceedDeinstalledGasmeterStepLatch = 20,
            TaskProceedConfigurationDownloadStepLatch = 21,
            TaskProceedClusterTestStepLatch = 22,
            TaskProceedInstallationWithSynchronizationStepLatch = 23,   //Informacje o synchronizacji OKOI505 z podaniem warto�ci liczyd�a
            TaskProceedPinValueStepLatch = 24,                          //Informacje o pobraniu pinu do menu
            TaskProceedDeviceDiagnosticStepLatch = 25,                  //Informacje o wykonaniu diagnostyki urz�dzenia
            TaskProceedInstalledGasmeterRadioQualityVerificationStepLatch = 26,
            TaskProceedInstalledMeterAdapterStepLatch = 27,
            TaskProceedDeviceValidImpulseCountTestStepLatch = 28,
            TaskProceedInstalledWatermeterStepLatch = 29,
            TaskProceedHubInstallationStepLatch = 30,
            TaskProceedWalkedByReadoutStepLatch = 31, 
        }
        #endregion
        #region SITAUIObjectProperties
        public enum SITAUIObjectProperties
        {
            GasInstallationMeterType = 0,
            GasInstallationMeterSeries = 1,

            WaterInstallationMeterType = 2,
            WaterInstallationMeterSeries = 3,
            WaterInstallationMeterInstelledOnWaterType = 4,

            WaterInstallationStep1Activity = 5,
            WaterInstallationStep2aActivity = 6,
            WaterInstallationStep2bActivity = 7,
            WaterInstallationStep3Activity = 8,
            WaterInstallationStep1aDeviceLocationPlaceActivity = 9,
            WaterInstallationStep2cActivity = 10,
            WaterInstallationStep3SMSActivity = 11,
            WaterInstallationStep3NetworkActivity = 12,

            ReadoutSettings_WalkedByScenerioType = 13,
            ReadoutSettings_ReadDiagnostics = 14,
            ReadoutSettings_SendDiagnosticsToServer = 15,
            ReadoutSettings_DeleteLatchedErrors = 16,
            ReadoutSettings_ReadDailyArchive = 17,
            ReadoutSettings_WalkedByStopAndGoDuration = 18,
            ReadoutSettings_ReadDailyArchiveBackDayCount = 19,
            ReadoutSettings_WalkedByArchiveMode = 20,
            ReadoutSettings_UseGPSToCreateMulticast = 21,
            ReadoutSettings_SendAdditionalPacket = 22,

            //TaskAction
            TaskAction_SuspendReopenDate = 100,
        }
        #endregion
        #region SitaDeviceConnection
        public enum SitaDeviceConnection
        {
            Server = 1,
            SITA = 2,
        }
        #endregion
        #region SitaFrameType
        public enum SITAFrameType
        {
            Unknown = 0,
            WalkedByFrame = 1,
            Diagnostic = 2,
            Archives = 3,
        }
        #endregion
        #endregion
    }
}
