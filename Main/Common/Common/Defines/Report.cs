using System;
using System.Collections.Generic;

namespace IMR.Suite.Common
{
    #region struct DATA_TYPE_UNIT
    [Serializable]
    public struct DATA_TYPE_UNIT
    {
        public long IdDataType;
        public int? IdUnit;
    }
    #endregion
    #region class Report
    [Serializable]
    public class Report
    {
        #region Members

        /* Each member should occure in:
         *
         * - FillReportData()
         * - GetReportData()
         * - Copy constructor
         * - Main constructor's default values (optionally)
         * 
         * */

        public int IdReport { get; private set; }
        public Enums.ReportType ReportType { get; private set; }

        public string Name {get; set;}
        public string Description { get; set; } 

        public int StartDaysBack { get; set; }
        public int? StartMinutesBack { get; set; }
        public TimeSpan StartTime { get; set; }
        public int EndDaysBack { get; set; }
        public int? EndMinutesBack { get; set; }
        public TimeSpan EndTime { get; set; }

        public bool UseSchedule { get; set; }
        public int ScheduleFrequency { get; set; }
        public Enums.ReportScheduleUnit ScheduleFrequencyUnit { get; set; }
        public int ScheduleToleranceBack { get; set; }
        public Enums.ReportScheduleUnit ScheduleToleranceBackUnit { get; set; }
        public int ScheduleToleranceForw { get; set; }
        public Enums.ReportScheduleUnit ScheduleToleranceForwUnit { get; set; }
        public Enums.ReportScheduleMode ScheduleMode { get; set; }
        public bool ScheduleEmptyRows { get; set; }

        public int PageSize { get; set; }
        public bool IncludeMetersInRows { get; set; }
        public bool IncludeDevicesInRows { get; set; }

        public ReportDataTypeSet RdtLocation  { get; set; }
        public ReportDataTypeSet RdtMeter { get; set; }
        public ReportDataTypeSet RdtDevice { get; set; }
        public ReportDataTypeSet RdtMeasures { get; set; }
        #endregion

        #region Constructor
        public Report(int IdReport, int IdReportType)
        {
            this.IdReport = IdReport;

            if (Enum.IsDefined(typeof(Enums.ReportType), IdReportType))
                this.ReportType = (Enums.ReportType)IdReportType;
            else
                this.ReportType = Enums.ReportType.Unknown;


            #region Default values
            this.StartDaysBack = 1;
            this.StartMinutesBack = null;
            this.StartTime = new TimeSpan(0, 0, 0);
            this.EndDaysBack = 0;
            this.EndMinutesBack = null;
            this.EndTime = new TimeSpan(23, 59, 59);

            this.UseSchedule = true;

            this.PageSize = 10;

            this.IncludeMetersInRows = true;
            this.IncludeDevicesInRows = true;
            #endregion
        }

        public Report(int IdReport)
            :this (IdReport, 0)
        {

        }

        public Report(int IdReport, Report Pattern)
        {
            this.IdReport = IdReport;
            this.ReportType = Pattern.ReportType;

            this.Name = Pattern.Name;
            this.Description = Pattern.Description;

            this.StartDaysBack = Pattern.StartDaysBack;
            this.StartMinutesBack = Pattern.StartMinutesBack;
            this.StartTime = Pattern.StartTime;
            this.EndDaysBack = Pattern.EndDaysBack;
            this.EndMinutesBack = Pattern.EndMinutesBack;
            this.EndTime = Pattern.EndTime;

            this.PageSize = Pattern.PageSize;
            this.IncludeMetersInRows = Pattern.IncludeMetersInRows;
            this.IncludeDevicesInRows = Pattern.IncludeDevicesInRows;

            this.UseSchedule = Pattern.UseSchedule;
            this.ScheduleFrequency = Pattern.ScheduleFrequency;
            this.ScheduleFrequencyUnit = Pattern.ScheduleFrequencyUnit;
            this.ScheduleToleranceBack = Pattern.ScheduleToleranceBack;
            this.ScheduleToleranceBackUnit = Pattern.ScheduleToleranceBackUnit;
            this.ScheduleToleranceForw = Pattern.ScheduleToleranceForw;
            this.ScheduleToleranceForwUnit = Pattern.ScheduleToleranceForwUnit;
            this.ScheduleMode = Pattern.ScheduleMode;
            this.ScheduleEmptyRows = Pattern.ScheduleEmptyRows;

            // dopoki nie ma tego na stronce raportow
            this.RdtMeter = Pattern.RdtMeter;
            this.RdtDevice = Pattern.RdtDevice;
        }

        #endregion

        #region FillReportData
        public void FillReportData(DataValue[] ReportDataValues)
        {
            foreach (DataValue dataValue in ReportDataValues)
            {
                if (dataValue == null || dataValue.Value == null)
                    continue;

                switch (dataValue.Type.IdDataType)
                {
                    #region REPORT_NAME
                    case DataType.REPORT_NAME:
                    this.Name = dataValue.Value.ToString();
                    break;
                #endregion
                    #region REPORT_DESCRIPTION
                    case DataType.REPORT_DESCRIPTION:
                    this.Description = dataValue.Value.ToString();
                    break;
                    #endregion

                    #region REPORT_START_DAYS_BACK
                    case DataType.REPORT_START_DAYS_BACK:
                    this.StartDaysBack = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_START_MINUTES_BACK
                    case DataType.REPORT_START_MINUTES_BACK:
                    this.StartMinutesBack = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_START_TIME
                    case DataType.REPORT_START_TIME:
                    this.StartTime = TimeSpan.Parse(dataValue.Value.ToString());
                    break;
                    #endregion
                    #region REPORT_END_DAYS_BACK
                    case DataType.REPORT_END_DAYS_BACK:
                    this.EndDaysBack = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_END_MINUTES_BACK
                    case DataType.REPORT_END_MINUTES_BACK:
                    this.EndMinutesBack = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_END_TIME
                    case DataType.REPORT_END_TIME:
                    this.EndTime = TimeSpan.Parse(dataValue.Value.ToString());
                    break;
                    #endregion

                    #region REPORT_USE_SCHEDULE
                    case DataType.REPORT_USE_SCHEDULE:
                    this.UseSchedule = Convert.ToBoolean(dataValue.Value);
                    break;
                    #endregion                                        
                    #region REPORT_SCHEDULE_FREQUENCY_VALUE
                    case DataType.REPORT_SCHEDULE_FREQUENCY_VALUE:
                    this.ScheduleFrequency = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_FREQUENCY_UNIT
                    case DataType.REPORT_SCHEDULE_FREQUENCY_UNIT:
                    this.ScheduleFrequencyUnit = (Enums.ReportScheduleUnit)Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_TOLERANCE_BACK_VALUE
                    case DataType.REPORT_SCHEDULE_TOLERANCE_BACK_VALUE:
                    this.ScheduleToleranceBack = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_TOLERANCE_BACK_UNIT
                    case DataType.REPORT_SCHEDULE_TOLERANCE_BACK_UNIT:
                    this.ScheduleToleranceBackUnit = (Enums.ReportScheduleUnit)Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_TOLERANCE_FORW_VALUE
                    case DataType.REPORT_SCHEDULE_TOLERANCE_FORW_VALUE:
                    this.ScheduleToleranceForw = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_TOLERANCE_FORW_UNIT
                    case DataType.REPORT_SCHEDULE_TOLERANCE_FORW_UNIT:
                    this.ScheduleToleranceForwUnit = (Enums.ReportScheduleUnit)Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_MODE
                    case DataType.REPORT_SCHEDULE_MODE:
                    this.ScheduleMode = (Enums.ReportScheduleMode)Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_SCHEDULE_EMPTY_ROWS
                    case DataType.REPORT_SCHEDULE_EMPTY_ROWS:
                    this.ScheduleEmptyRows = Convert.ToBoolean(dataValue.Value);
                    break;
                    #endregion

                    #region REPORT_PAGE_SIZE
                    case DataType.REPORT_PAGE_SIZE:
                    this.PageSize = Convert.ToInt32(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_INCLUDE_METERS_IN_ROWS
                    case DataType.REPORT_INCLUDE_METERS_IN_ROWS:
                    this.IncludeMetersInRows = Convert.ToBoolean(dataValue.Value);
                    break;
                    #endregion
                    #region REPORT_INCLUDE_DEVICES_IN_ROWS
                    case DataType.REPORT_INCLUDE_DEVICES_IN_ROWS:
                    this.IncludeDevicesInRows = Convert.ToBoolean(dataValue.Value);
                    break;
                    #endregion

                    #region REPORT_LOCATION_DATA
                    case DataType.REPORT_LOCATION_DATA:
                    this.RdtLocation = new ReportDataTypeSet(Convert.ToInt32(dataValue.Value));
                    break;
                    #endregion
                    #region REPORT_METER_DATA
                    case DataType.REPORT_METER_DATA:
                    this.RdtMeter = new ReportDataTypeSet(Convert.ToInt32(dataValue.Value));
                    break;
                    #endregion
                    #region REPORT_DEVICE_DATA
                    case DataType.REPORT_DEVICE_DATA:
                    this.RdtDevice = new ReportDataTypeSet(Convert.ToInt32(dataValue.Value));
                    break;
                    #endregion
                    #region REPORT_MEASURES_DATA
                    case DataType.REPORT_MEASURES_DATA:
                    this.RdtMeasures = new ReportDataTypeSet(Convert.ToInt32(dataValue.Value));
                    break;
                    #endregion
                }
            }

        }
        #endregion
        #region GetReportData
        public DataValue[] GetReportData()
        {
            List<DataValue> resList = new List<DataValue>();
            
            resList.Add(new DataValue(DataType.REPORT_NAME, this.Name));
            resList.Add(new DataValue(DataType.REPORT_DESCRIPTION, this.Description));

            resList.Add(new DataValue(DataType.REPORT_START_DAYS_BACK, this.StartDaysBack));
            if (this.StartMinutesBack.HasValue)
                resList.Add(new DataValue(DataType.REPORT_START_MINUTES_BACK, this.StartMinutesBack));
            resList.Add(new DataValue(DataType.REPORT_START_TIME, this.StartTime));

            resList.Add(new DataValue(DataType.REPORT_END_DAYS_BACK, this.EndDaysBack));
            if (this.EndMinutesBack.HasValue)
                resList.Add(new DataValue(DataType.REPORT_END_MINUTES_BACK, this.EndMinutesBack));
            resList.Add(new DataValue(DataType.REPORT_END_TIME, this.EndTime));

            resList.Add(new DataValue(DataType.REPORT_USE_SCHEDULE, this.UseSchedule));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_FREQUENCY_VALUE, this.ScheduleFrequency));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_FREQUENCY_UNIT, (int)this.ScheduleFrequencyUnit));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_TOLERANCE_BACK_VALUE, this.ScheduleToleranceBack));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_TOLERANCE_BACK_UNIT, (int)this.ScheduleToleranceBackUnit));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_TOLERANCE_FORW_VALUE, this.ScheduleToleranceForw));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_TOLERANCE_FORW_UNIT, (int)this.ScheduleToleranceForwUnit));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_MODE, (int)this.ScheduleMode));
            resList.Add(new DataValue(DataType.REPORT_SCHEDULE_EMPTY_ROWS, this.ScheduleEmptyRows));

            resList.Add(new DataValue(DataType.REPORT_PAGE_SIZE, this.PageSize));
            resList.Add(new DataValue(DataType.REPORT_INCLUDE_METERS_IN_ROWS, this.IncludeMetersInRows));
            resList.Add(new DataValue(DataType.REPORT_INCLUDE_DEVICES_IN_ROWS, this.IncludeDevicesInRows));

            if (this.RdtLocation != null)
                resList.Add(new DataValue(DataType.REPORT_LOCATION_DATA, this.RdtLocation.IdReportDataType));
            if (this.RdtMeter != null)
                resList.Add(new DataValue(DataType.REPORT_METER_DATA, this.RdtMeter.IdReportDataType));
            if (this.RdtDevice != null)
                resList.Add(new DataValue(DataType.REPORT_DEVICE_DATA, this.RdtDevice.IdReportDataType));
            if (this.RdtMeasures != null)
                resList.Add(new DataValue(DataType.REPORT_MEASURES_DATA, this.RdtMeasures.IdReportDataType));

            return resList.ToArray();
        }
        #endregion
    }
    #endregion
    #region class ReportDataTypeSet
    [Serializable]
    public class ReportDataTypeSet
    {
        #region Members
        public long IdReportDataType { get; private set; }
        public ReportDataType[] ReportDt { get; private set; }
        public List<ReportDataType> ReportDtList { get; private set; }
        public Dictionary<long, ReportDataType> ReportDtDict { get; private set; }
        public DATA_TYPE_UNIT[] TypesWithUnits { get; private set; }
        #endregion

        #region Constructor
        public ReportDataTypeSet(long IdReportDataType, ReportDataType[] ReportDt)
        {
            this.IdReportDataType = IdReportDataType;
            this.ReportDt = ReportDt;

            this.FillRdtStructs(ReportDt);
        }

        public ReportDataTypeSet(long IdReportDataType)
            : this(IdReportDataType, new ReportDataType[0]) 
        {
        }
        #endregion

        #region FillRdtStructs
        public void FillRdtStructs(ReportDataType[] ReportDt)
        {
            ReportDtDict = new Dictionary<long, ReportDataType>();
            ReportDtList = new List<ReportDataType>();
            TypesWithUnits = new DATA_TYPE_UNIT[ReportDt.Length];
            for (int i = 0; i < ReportDt.Length; i++)
            {
                ReportDataType rdt = ReportDt[i];

                // zakladam, ze nie bedzie 2 takich samych dla jednego id_report_data_type
                ReportDtDict.Add(rdt.DataType.IdDataType, rdt);

                TypesWithUnits[i] = new DATA_TYPE_UNIT();
                TypesWithUnits[i].IdDataType = rdt.DataType.IdDataType;
                TypesWithUnits[i].IdUnit = rdt.IdUnit;
            }


            ReportDtList.AddRange(ReportDt);
            ReportDtList.Sort();

            this.ReportDt = ReportDt;
        }
        #endregion

    }
    #endregion
    #region class ReportDataType
    [Serializable]
    public class ReportDataType : IComparable<ReportDataType>
    {
        #region Members
        #region Signature
        private string signature = null;
        public string Signature
        {
            get { return signature; }
        }
        #endregion

        #region DataType
        private DataType dataType = null;
        public DataType DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }
        #endregion
        #region IdUnit
        private int idUnit = -1;
        public int IdUnit
        {
            get { return idUnit; }
        }
        #endregion
        #region UnitSymbol
        private string unitSymbol = "";
        public string UnitSymbol
        {
            get { return unitSymbol; }
        }
        #endregion
        #region DigitsAfterComma
        private int? digitsAfterComma = null;
        public int? DigitsAfterComma
        {
            get { return digitsAfterComma; }
        }
        #endregion
        #region SequenceNbr
        private int? sequenceNbr = null;
        public int? SequenceNbr
        {
            get { return sequenceNbr; }
        }
        #endregion
        #endregion

        #region Constructor
        public ReportDataType(string Signature, DataType DataType, int IdUnit, int? DigitsAfterComma, int? SequenceNbr, string UnitSymbol)
        {
            this.signature = Signature;
            this.dataType = DataType;
            this.idUnit = IdUnit;
            this.digitsAfterComma = DigitsAfterComma;
            this.sequenceNbr = SequenceNbr;
            this.unitSymbol = UnitSymbol;
        }

        public ReportDataType(string Signature, long DataTypeId, int IdUnit, int? DigitsAfterComma, int? SequenceNbr, string UnitSymbol)
            : this(Signature, new DataType(DataTypeId), IdUnit, DigitsAfterComma, SequenceNbr, UnitSymbol)
        {
        }
        #endregion

        #region CompareTo
        public int CompareTo(ReportDataType obj)
        {
            // kryteria porownania: sequenceNbr, signature, datatype.Description
            // sequenceNbr = null: najwi�kszy (�eby na listach by� na samym ko�cu)

            if (this.sequenceNbr.HasValue && !obj.sequenceNbr.HasValue)
            {
                return -1;
            }

            if (!this.sequenceNbr.HasValue && obj.sequenceNbr.HasValue)
            {
                return 1;
            }

            if (this.sequenceNbr.HasValue && obj.sequenceNbr.HasValue)
            {
                if (this.sequenceNbr.Value != obj.sequenceNbr.Value)
                {
                    return this.sequenceNbr.Value.CompareTo(obj.sequenceNbr.Value);
                }
            }

            // nie da sie rozstrzygnac po sequenceNbr

            if (this.signature != obj.signature)
            {
                return this.signature.CompareTo(obj.signature);
            }

            // nie da sie rozstrzygnac po signature

            if (this.DataType != null && obj.DataType != null)
            {
                return this.DataType.Description.CompareTo(obj.DataType.Description);
            }

            // nie da sie rozstrzygnac
            return 2;
        }
        #endregion
    }

    #endregion
}
