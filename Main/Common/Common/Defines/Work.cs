using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Runtime.Serialization;

using IMR.Suite.Common.Code;

namespace IMR.Suite.Common
{
    #region Work
    [Serializable]
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [ProtoBuf.ProtoInclude(100, typeof(DataValue))]
    [ProtoBuf.ProtoInclude(101, typeof(DiagnosticInfo))]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class Work : ISerializable
    {
        #region SerializationVersion
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
        [ProtoBuf.ProtoMember(98, Name = "Version")]
        [Newtonsoft.Json.JsonProperty("Version")]
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion

        #region Members
        #region IdWork
        [ProtoBuf.ProtoMember(1, Name = "IdWork")]
        [Newtonsoft.Json.JsonProperty("IdWork", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public long? IdWork { get; set; }
        #endregion
        #region IdWorkType
        [ProtoBuf.ProtoMember(2, Name = "IdWorkType")]
        [Newtonsoft.Json.JsonProperty("IdWorkType")]
        public long IdWorkType { get; set; }
        #endregion
        #region Status
        [ProtoBuf.ProtoMember(3, Name = "Status")]
        [Newtonsoft.Json.JsonProperty("Status")]
        public Enums.WorkStatus Status { get; set; }
        #endregion

        #region IdWorkData
        [ProtoBuf.ProtoMember(4, Name = "IdWorkData")]
        [Newtonsoft.Json.JsonProperty("IdWorkData", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public long? IdWorkData { get; set; }

        #endregion
        #region WorkData
        [ProtoBuf.ProtoMember(5, Name = "WorkData")]
        [Newtonsoft.Json.JsonProperty("WorkData")]
        public DataValue[] WorkData { get; set; }
        #endregion

        #region IdWorkParent
        [ProtoBuf.ProtoMember(9, Name = "IdWorkParent")]
        [Newtonsoft.Json.JsonProperty("IdWorkParent", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public long? IdWorkParent { get; set; }
        #endregion

        #region Module
        [ProtoBuf.ProtoMember(15, Name = "Module")]
        [Newtonsoft.Json.JsonProperty("Module")]
        public Enums.Module Module { get; set; }
        #endregion
        #region IdOperator
        [ProtoBuf.ProtoMember(16, Name = "IdOperator")]
        [Newtonsoft.Json.JsonProperty("IdOperator", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public long? IdOperator { get; set; }
        #endregion

        #region DiagnosticInfos
        [ProtoBuf.ProtoMember(18, Name = "DiagnosticInfos")]
        [Newtonsoft.Json.JsonProperty("DiagnosticInfos")]
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }
        #endregion
        #endregion

        #region Constructor
        public Work()
        {
            this.DiagnosticInfos = new List<DiagnosticInfo>();
        }
        protected Work(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected Work(SerializationInfo info, StreamingContext context, bool fromDerived)
        {
            DiagnosticInfos = new List<DiagnosticInfo>();

            //ustalenie wersji obiektu
            if (info.FieldExists("Version"))
                serializationVersion = (byte)info.GetValue("Version", typeof(byte));
            else
                serializationVersion = 0;

            this.IdWork = (long?)info.GetValue("IdWork", typeof(long?));
            this.IdWorkType = (long)info.GetValue("IdWorkType", typeof(long));
            this.Status = (Enums.WorkStatus)info.GetValue("Status", typeof(int));

            this.IdWorkData = (long?)info.GetValue("IdWorkData", typeof(long?));
            this.WorkData = (DataValue[])info.GetValue("WorkData", typeof(DataValue[]));

            this.IdWorkParent = (long?)info.GetValue("IdWorkParent", typeof(long?));

            this.Module = (Enums.Module)info.GetValue("Module", typeof(int));
            this.IdOperator = (long?)info.GetValue("IdOperator", typeof(long?));

            Guid = (Guid)info.GetValue("Guid", typeof(Guid));
            DiagnosticInfos = (List<DiagnosticInfo>)info.TryGetValue("DiagnosticInfos", typeof(List<DiagnosticInfo>), new List<DiagnosticInfo>());
        }
        #endregion

        #region EncodeMessage
        public static Dictionary<string, object> EncodeMessage(Work work)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (work != null)
            {
                result["IdWork"] = work.IdWork;
                result["IdWorkType"] = work.IdWorkType;
                result["Status"] = (int)work.Status;

                result["IdWorkData"] = work.IdWorkData;
                if (work.WorkData != null)
                    result["WorkData"] = work.WorkData.Select(q => new Tuple<long, int, object>(q.IdDataType, q.Index, q.Value)).ToList();

                result["IdWorkParent"] = work.IdWorkParent;

                result["Module"] = (int)work.Module;
                result["IdOperator"] = work.IdOperator;
            }
            return result;
        }
        #endregion
        #region DecodeMessage
        public static Work DecodeMessage(object message)
        {
            if (message is Work)
                return (Work)message;
            else if (message.GetType() == typeof(Dictionary<string, object>))
            {
                Dictionary<string, object> values = (Dictionary<string, object>)message;
                object value = null;

                Work result = new Work();

                #region IdWork
                value = values.FirstOrDefault(w => w.Key == "IdWork").Value;
                result.IdWork = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region IdWorkType
                value = values.FirstOrDefault(w => w.Key == "IdWorkType").Value;
                result.IdWorkType = value == null ? default(int) : Convert.ToInt32(value);
                #endregion
                #region Status
                value = values.FirstOrDefault(w => w.Key == "Status").Value;
                result.Status = value == null ? Enums.WorkStatus.Unknown : (Enums.WorkStatus)Convert.ToInt32(value);
                #endregion
                #region IdWorkData
                value = values.FirstOrDefault(w => w.Key == "IdWorkData").Value;
                result.IdWorkData = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region WorkData
                value = values.FirstOrDefault(w => w.Key == "WorkData").Value;
                if (value != null)
                {
                    if (value is List<Tuple<long, int, object>>)
                    {
                        List<Tuple<long, int, object>> workDataValue = value as List<Tuple<long, int, object>>;
                        List<DataValue> workData = new List<DataValue>();
                        workDataValue.ForEach(tuple => workData.Add(new DataValue(tuple.Item1, tuple.Item2, tuple.Item3)));
                        result.WorkData = workData.ToArray();
                    }
                    else if (value is List<object[]>)
                    {
                        List<object[]> workDataValue = value as List<object[]>;
                        List<DataValue> WorkData = new List<DataValue>();
                        workDataValue.Where(w => w.Length >= 3 && w[0] is long && w[1] is int).ToList().ForEach(tuple => WorkData.Add(new DataValue((long)tuple[0], (int)tuple[1], tuple[2])));
                        result.WorkData = WorkData.ToArray();
                    }
                }
                #endregion
                #region IdWorkParent
                value = values.FirstOrDefault(w => w.Key == "IdWorkParent").Value;
                result.IdWorkParent = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion
                #region Module
                value = values.FirstOrDefault(w => w.Key == "Module").Value;
                result.Module = value == null ? Enums.Module.Unknown : (Enums.Module)Convert.ToInt64(value);
                #endregion
                #region IdOperator
                value = values.FirstOrDefault(w => w.Key == "IdOperator").Value;
                result.IdOperator = value == null ? default(long?) : Convert.ToInt64(value);
                #endregion

                return result;
            }
            return null;
        }
        #endregion

        #region GetObjectData
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("IdWork", IdWork, typeof(long?));
            info.AddValue("IdWorkType", IdWorkType, typeof(long));
            info.AddValue("Status", Status, typeof(int));
            info.AddValue("IdWorkData", IdWorkData, typeof(long?));
            info.AddValue("WorkData", WorkData, typeof(DataValue[]));
            info.AddValue("IdWorkParent", IdWorkParent, typeof(long?));
            info.AddValue("Module", Module, typeof(int));
            info.AddValue("IdOperator", IdOperator, typeof(long?));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("Version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
            info.AddValue("DiagnosticInfos", DiagnosticInfos, typeof(List<DiagnosticInfo>));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
        #endregion
        #region Guid
        [ProtoBuf.ProtoMember(99, Name = "Guid")]
        [Newtonsoft.Json.JsonProperty("Guid")]
        public readonly Guid Guid = Guid.NewGuid();
        public static bool operator ==(Work left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(Work left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is Work)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }
        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region override ToString()
        public override string ToString()
        {
            return string.Format("Id:{0} Type:{1} Status:{2}", IdWork, IdWorkType, Status);
        }
        #endregion
    }
    #endregion

    #region NotifyWork
    [Serializable]
    public class NotifyWork
    {
        #region NotifyType
        public enum NotifyType
        {
            Work,
            ChangeInTable,
            SysMessage,
        }
        #endregion
        public NotifyType Type;
        public object Notify;

        #region Constructor
        public NotifyWork(Work work)
        {
            Type = NotifyType.Work;
            Notify = work;
        }
        public NotifyWork(ChangeInTable changeInTable)
        {
            Type = NotifyType.ChangeInTable;
            Notify = changeInTable;
        }
        public NotifyWork(ChangeInTable[] changesInTable)
        {
            Type = NotifyType.ChangeInTable;
            Notify = changesInTable;
        }
        public NotifyWork(SysMessage systemMessage)
        {
            Type = NotifyType.SysMessage;
            Notify = systemMessage;
        }
        #endregion
    }
    #endregion

    #region IWorkPlugIn
    public interface IWorkPlugIn
    {
        Work Work { get; set; }

        bool Init(params object[] args);
        void Clear();
        bool Execute();

        void Notify(NotifyWork notifyWork);

        object ProcessCommand(Enums.ModuleCommandType moduleCommandType, params object[] args);
    }
    #endregion

    #region WorkPlugInWrapper
    [Serializable]
    public class WorkPlugInWrapper : MarshalByRefObjectDisposable
    {
        #region public Members
        public AssemblyName WorkPluginName = null;    // Jest to jednocze�nie flaga m�wi�ca czy DLLka jest ju� za�adowana do pami�ci (==null) => nie, trzeba za�adowa�)
        public Type WorkPluginType = default(Type);
        public string WorkPluginPath = string.Empty;
        public long InstanceCount = 0;

        #endregion

        #region private Members
        private readonly AppDomain DeviceDriverDomain;
        private object workCreateLockObject = new object();
        #endregion

        #region Events
        public event EventHandler<EventArgs<string>> WorkPluginLoadedEvent;
        #endregion

        #region Constructor
        public WorkPlugInWrapper()
        {
        }
        public WorkPlugInWrapper(string plugInPath)
        {
            WorkPluginPath = plugInPath;

            if (!System.IO.Path.IsPathRooted(WorkPluginPath))
                WorkPluginPath = AppDomain.CurrentDomain.BaseDirectory + WorkPluginPath;
            WorkPluginPath = Path.GetFullPath(WorkPluginPath);

            #region Je�li �cie�ka jest poza (not relative to base) �cie�k� bazow� EXE to kopiujemy do Cache (relative to base)
            string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
            string DllPath = Path.GetDirectoryName(WorkPluginPath);
            if (DllPath.IndexOf(AppPath) == -1)
            {
                string dllDirectoryName = Path.Combine(AppPath, "Cache");
                DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
                if (!di.Exists)
                {
                    di.Create();
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }

                string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(WorkPluginPath);
                try
                {
                    File.Copy(WorkPluginPath, newAssemblyPath, true);
                }
                catch
                {
                }
                WorkPluginPath = newAssemblyPath;
            }
            #endregion
        }
        #endregion
        #region Destructor
        ~WorkPlugInWrapper()
        {
            Dispose(false);
        }
        #endregion

        #region Unload
        public void Unload(bool forceDispose)
        {
            try
            {
                WorkPluginName = null;
                WorkPluginType = default(Type);
                GC.Collect();
            }
            catch { }
            base.Dispose();
        }
        #endregion

        #region CreateInstance
        public IWorkPlugIn CreateInstance(object[] PlugInConstructorArgs)
        {
            lock (workCreateLockObject)
            {
                if (WorkPluginName == null) // jeszcze nie za�adowana lub zosta�a od�adowana
                {
                    Assembly assembly = AssemblyWrapper.LoadAssembly(WorkPluginPath);
                    if (assembly == null)
                        return null;

                    WorkPluginType = GetDerivedType(assembly.GetTypes(), typeof(IWorkPlugIn));
                    if (WorkPluginType == default(Type))
                        return null;

                    WorkPluginName = assembly.GetName();

                    // poni�szy IF mozna zast�pi� tym (jak b�dzie VS2015 lub wy�szy): WorkPluginLoadedEvent?.Invoke(deviceDriverName, new EventArgs<string>(Path.GetFileName(WorkPluginPath)));
                    if (WorkPluginLoadedEvent != null)
                        WorkPluginLoadedEvent.Invoke(this, new EventArgs<string>(Path.GetFileName(WorkPluginPath)));
                }

                IWorkPlugIn plugIn = (IWorkPlugIn)Activator.CreateInstance(WorkPluginType, PlugInConstructorArgs);
                if (plugIn == null)
                    return null;

                Interlocked.Increment(ref InstanceCount);
                return plugIn;
            }
        }
        #endregion
        #region FreeInstance
        public void FreeInstance(IWorkPlugIn plugin)
        {
            if (plugin != null)
            {
                try
                {
                    Interlocked.Decrement(ref InstanceCount);

                }
                finally
                {
                    plugin = default(IWorkPlugIn);
                }
            }
        }
        #endregion

        #region private GetDerivedType
        private Type GetDerivedType(Type[] assemblyTypes, Type pluginType)
        {
            foreach (Type typeInAssembly in assemblyTypes)
            {
                if (typeInAssembly.IsClass && typeInAssembly.IsPublic)
                {
                    if (typeInAssembly.GetInterface(pluginType.FullName, true) != null)
                        return typeInAssembly;
                }
            }
            return default(Type);
        }
        #endregion

        #region MarshalByRefObjectDisposable overrides
        public override object InitializeLifetimeService()
        {
            return null;
        }
        #endregion
    }
    #endregion
}
