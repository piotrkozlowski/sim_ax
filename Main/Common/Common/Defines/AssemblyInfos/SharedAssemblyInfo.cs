using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.[assembly: AssemblyCompany("AIUT")]
[assembly: AssemblyProduct("IMR Suite")]
[assembly: AssemblyCompany("AIUT Corporation")]
[assembly: AssemblyCopyright("Copyright © AIUT 2016")]
//[assembly: AssemblyTrademark("")]
//[assembly: AssemblyCulture("")]

#if DEBUG
    [assembly: AssemblyConfiguration("Debug")]
#else
    [assembly: AssemblyConfiguration("Release")]
#endif

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.3.0.*")]
// !!! Nie dodawać FileVersion i InformationalVersion !!! wygenerują sie dokładnie takie same jak AssemblyVersion
