﻿#if DOT_NET_35 || WindowsCE
using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace IMR.Suite.Common
{
	[Serializable]
	public static class Tuple
	{
		public static Tuple<T1, T2> Create<T1, T2>(T1 t1, T2 t2) { return new Tuple<T1, T2>(t1, t2); }
		public static Tuple<T1, T2, T3> Create<T1, T2, T3>(T1 t1, T2 t2, T3 t3) { return new Tuple<T1, T2, T3>(t1, t2, t3); }
		public static Tuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, T4 t4) { return new Tuple<T1, T2, T3, T4>(t1, t2, t3, t4); }
	}

	#region Tuple<T1, T2>
	[DebuggerNonUserCode]
#if !WindowsCE
	[DebuggerDisplay("{Item1}, {Item2}")]
#endif
	[Serializable]
	public struct Tuple<T1, T2> : IEquatable<Tuple<T1, T2>>
	{
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T1 item1;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T2 item2;

		public Tuple(T1 t1, T2 t2)
		{
			this.item1 = t1;
			this.item2 = t2;
		}

		public T1 Item1 { get { return item1; } }
		public T2 Item2 { get { return item2; } }

		public override int GetHashCode()
		{
			return item1.GetHashCode() ^ item2.GetHashCode();
		}
		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}
			return Equals((Tuple<T1, T2>)obj);
		}
		public bool Equals(Tuple<T1, T2> other)
		{
			return other.item1.Equals(item1) && other.item2.Equals(item2);
		}
	}
	#endregion
	#region Tuple<T1, T2, T3>
	[DebuggerNonUserCode]
#if !WindowsCE
	[DebuggerDisplay("{Item1}, {Item2}, {Item3}")]
#endif
	[Serializable]
	public struct Tuple<T1, T2, T3> : IEquatable<Tuple<T1, T2, T3>>
	{
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T1 item1;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T2 item2;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T3 item3;

		public Tuple(T1 t1, T2 t2, T3 t3)
		{
			this.item1 = t1;
			this.item2 = t2;
			this.item3 = t3;
		}

		public T1 Item1 { get { return item1; } }
		public T2 Item2 { get { return item2; } }
		public T3 Item3 { get { return item3; } }

		public override int GetHashCode()
		{
			return item1.GetHashCode() ^ item2.GetHashCode() ^ item3.GetHashCode();
		}
		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}
			return Equals((Tuple<T1, T2, T3>)obj);
		}
		public bool Equals(Tuple<T1, T2, T3> other)
		{
			return other.item1.Equals(item1) && other.item2.Equals(item2) && other.item3.Equals(item3);
		}
	}
	#endregion
	#region Tuple<T1, T2, T3, T4>
	[DebuggerNonUserCode]
#if !WindowsCE
	[DebuggerDisplay("{Item1}, {Item2}, {Item3}, {Item4}")]
#endif
	[Serializable]
	public struct Tuple<T1, T2, T3, T4> : IEquatable<Tuple<T1, T2, T3, T4>>
	{
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T1 item1;
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        readonly T2 item2;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T3 item3;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		readonly T4 item4;

		public Tuple(T1 t1, T2 t2, T3 t3, T4 t4)
		{
			this.item1 = t1;
			this.item2 = t2;
			this.item3 = t3;
			this.item4 = t4;
		}

		public T1 Item1 { get { return item1; } }
		public T2 Item2 { get { return item2; } }
		public T3 Item3 { get { return item3; } }
		public T4 Item4 { get { return item4; } }

		public override int GetHashCode()
		{
			return  item1.GetHashCode() ^ item2.GetHashCode() ^
					item3.GetHashCode() ^ item4.GetHashCode();
		}
		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}
			return Equals((Tuple<T1, T2, T3, T4>)obj);
		}
		public bool Equals(Tuple<T1, T2, T3, T4> other)
		{
            return other.item1.Equals(item1) && other.item2.Equals(item2) &&
                   other.item3.Equals(item3) && other.item4.Equals(item4);
		}
	}
	#endregion
}
#endif