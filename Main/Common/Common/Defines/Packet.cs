#define ISERIALIZABLE_TEST

using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace IMR.Suite.Common
{
    #region PacketEventArgs
    [Serializable]
    public class PacketEventArgs : EventArgs
    {
        /// <summary>
        /// Czas nadejścia pakietu do systemu (czas odczytany z SMS proponuję ignorować)
        /// </summary>
        public DateTime Time { get; private set; }
        /// <summary>
        /// Zawartosc pakietu
        /// </summary>
        public Packet Packet { get; private set; }
        /// <summary>
        /// Dodatkowe (jesli sa potrzebne) informacje
        /// </summary>
        public DataValue[] Params { get; private set; }

        public PacketEventArgs(DateTime Time, Packet Packet, DataValue[] Params)
        {
            this.Time = Time;
            this.Packet = Packet;
            this.Params = Params;
        }
        public PacketEventArgs(DateTime Time, Packet Packet) :
            this(Time, Packet, new DataValue[0])
        { }
        public PacketEventArgs(Packet Packet) :
            this(DateTime.Now, Packet, new DataValue[0])
        { }

        public PacketEventArgs(Packet Packet, DataValue[] Params) :
            this(DateTime.Now, Packet, Params)
        { }
    }
    #endregion
    #region PacketXmitEventArgs
    [Serializable]
    public class PacketXmitEventArgs : EventArgs
    {
        public readonly int IdDriver;
        public readonly IPv6Address IPv6;

        public readonly Enums.TransmissionDriverType TransmissionDriverType;

        /// <summary>
        /// Czas nadejścia pakietu do systemu (czas odczytany z SMS proponuję ignorować)
        /// </summary>
        public readonly DateTime Time;
        /// <summary>
        /// Zawartosc pakietu
        /// </summary>
        public readonly PacketXmit Packet;

        public PacketXmitEventArgs(IPv6Address IPv6, Enums.TransmissionDriverType transmissionDriverType, DateTime time, PacketXmit packet)
        {
            this.IdDriver = IPv6.IDDriver.Value;
            this.IPv6 = IPv6;
            this.TransmissionDriverType = transmissionDriverType;
            this.Time = time;
            this.Packet = packet;
        }
    }
    #endregion

    #region BodyType
    [Serializable]
    public enum BodyType
    {
        Unknown = Enums.TransmissionType.Unknown,
        Binary = Enums.TransmissionType.Binary,
        WAN2 = Enums.TransmissionType.Binary,
        Text = Enums.TransmissionType.Text,
        Text7bit = Enums.TransmissionType.Text7bit,
        PDUBinary = Enums.TransmissionType.PDUBinary,
        WAN1 = Enums.TransmissionType.PDUBinary,
        EMail = Enums.TransmissionType.EMail,
        Ring = Enums.TransmissionType.Ring,
    }
    #endregion
    #region Body
    [Serializable]
#if ISERIALIZABLE_TEST
    public class Body : ISerializable
#else
    public class Body
#endif
    {
        #region Type
        public BodyType Type;
        #endregion
        #region Bytes
        public byte[] Bytes;
        #endregion
        #region Length
        public int Length
        {
            get { return Bytes.Length; }
        }
        #endregion

        #region Constructors
        public Body()
        {
            this.Bytes = new byte[0];
            this.Type = BodyType.Unknown;
        }
        public Body(byte[] bytes, BodyType type)
        {
            this.Bytes = bytes;
            this.Type = type;
        }
        public Body(string text, BodyType type)
            : this(text, type, Enums.Language.English) { }

        public Body(string text, BodyType type, Enums.Language language)
        {
            this.Type = type;
            switch (type)
            {
                case BodyType.Text:
                    this.Bytes = Encoding.BigEndianUnicode.GetBytes(text);
                    break;
                case BodyType.Text7bit:
                    if (language == Enums.Language.Default || language == Enums.Language.Polish)
                    {
                        byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(text);
                        this.Bytes = GSM7bitAscii_GetBytes(Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                    }
                    else
                        this.Bytes = GSM7bitAscii_GetBytes(text);
                    break;
                default:
                    throw new Exception(String.Format("Unknown BodyType '{0}' for string", type));
            }
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected Body(SerializationInfo info, StreamingContext context)
        {
            Type = (BodyType)info.GetValue("Type", typeof(int));
            Bytes = (byte[])info.GetValue("Bytes", typeof(byte[]));
        }
#endif
        #endregion

        #region GSM7bitAsciiEncoding
        #region GSMAsciiTable
        // ` is not a conversion, just a untranslatable letter
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        readonly string GSMAsciiTable = "@£$¥èéùìòÇ`Øø`Åå" +
                                        "Δ_ΦΓΛΩΠΨΣΘΞ`ÆæßÉ" +
                                        " !\"#¤%&'()*+,-./" +
                                        "0123456789:;<=>?" +
                                        "¡ABCDEFGHIJKLMNO" +
                                        "PQRSTUVWXYZÄÖÑÜ§" +
                                        "¿abcdefghijklmno" +
                                        "pqrstuvwxyzäöñüà";
        #endregion
        #region ExtendedGSMAsciiTable
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        readonly string ExtendedGSMAsciiTable = "````````````````" +
                                                "````^```````````" +
                                                "````````{}`````\\" +
                                                "````````````[~]`" +
                                                "|```````````````" +
                                                "````````````````" +
                                                "`````€``````````" +
                                                "````````````````";
        #endregion
        #region GSM7bitAscii_GetBytes
        public byte[] GSM7bitAscii_GetBytes(string text)
        {
            List<byte> bytes = new List<byte>();

            foreach (char c in text.ToCharArray())
            {
                if (c == '\x0A' || c == '\x0D')
                {
                    bytes.Add((byte)c);
                    continue;
                }

                int intGSMAsciiTable = GSMAsciiTable.IndexOf(c);
                if (intGSMAsciiTable != -1)
                {
                    bytes.Add((byte)intGSMAsciiTable);
                    continue;
                }
                int intExtendedGSMAsciiTable = ExtendedGSMAsciiTable.IndexOf(c);
                if (intExtendedGSMAsciiTable != -1)
                {
                    bytes.Add(27);// ESC
                    bytes.Add((byte)intExtendedGSMAsciiTable);
                }
            }
            return bytes.ToArray();
        }
        #endregion
        #region GSM7bit_GetString
        public string GSM7bitAscii_GetString(byte[] bytes)
        {
            StringBuilder text = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                if (bytes[i] == 27) //ESC
                    text.Append(ExtendedGSMAsciiTable[bytes[++i]]);
                else
                    text.Append(GSMAsciiTable[bytes[i]]);
            }
            return text.ToString();
        }
        #endregion
        #endregion

        #region implicit operators
        public static implicit operator Body(byte[] arg)
        {
            return new Body(arg, BodyType.Binary);
        }
        public static implicit operator byte[](Body arg)
        {
            return arg.Bytes;
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            string result = string.Empty;

            switch (Type)
            {
                case BodyType.Binary:
                case BodyType.PDUBinary:
                    StringBuilder sb = new StringBuilder();
                    foreach (byte b in Bytes)
                        sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
                    result = "0x" + sb.ToString().ToUpper();
                    break;
                case BodyType.Text:
                    result = Encoding.BigEndianUnicode.GetString(Bytes, 0, Bytes.Length);
                    break;
                case BodyType.Text7bit:
                    try
                    {
                        result = GSM7bitAscii_GetString(Bytes);
                    }
                    catch
                    {
                        result = Bytes.ToHexString();
                    }
                    break;
            }
            return result;
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Type", (int)Type, typeof(int));
            info.AddValue("Bytes", Bytes, typeof(byte[]));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion
    }
    #endregion

    #region Packet
    [Serializable]
#if !WindowsCE
    [TypeConverter(typeof(PacketConverter))]
#endif
#if ISERIALIZABLE_TEST
    public class Packet : ISerializable
#else
    public class Packet
#endif
    {
        #region SerializationVersion
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion

        public SN SerialNumber { get; set; }
        public string Address { get; set; }	// tylko dla pakietow przychodzacych: PhoneNumber lub IP:Port (np. Zmiana karty SIM)

        public Guid SessionGuid { get; set; }
        public bool SessionAction { get; set; }

        public int MCostOut { get; set; }
        public int MCostIn { get; set; }
        public bool ExpectedPacketIn { get; set; }
        public TimeSpan SendTimeOut { get; set; }
        public uint? SendIdConnection { get; set; }

        public Body Body { get; set; }

        public ulong IdPacket { get; set; }
#if WindowsCE
        public DateTime TimeStamp { get; set; }
		public DateTime SystemTimeStamp { get; set ;}
#else
        public DateTimeOffset TimeStamp { get; set; }
        public DateTimeOffset SystemTimeStamp { get; set; }
#endif
        public Enums.PacketStatus Status { get; set; }

        public int TransmittedPackets { get; set; }
        public int Bytes { get; set; }
        public int TransmittedBytes { get; set; }
        public List<DataValue> Params { get; set; }
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }


        #region Constructor
        public Packet()
        {
            Body = new Body(new byte[0], BodyType.Binary);
            Params = new List<DataValue>();
            DiagnosticInfos = new List<DiagnosticInfo>();
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected Packet(SerializationInfo info, StreamingContext context)
        {
            Params = new List<DataValue>();
            DiagnosticInfos = new List<DiagnosticInfo>();

            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            if (serializationVersion == 0)
            {
                SerialNumber = (SN)info.GetValue("<SerialNumber>k__BackingField", typeof(SN));

                Address = (string)info.GetValue("<Address>k__BackingField", typeof(string));
                SessionGuid = (Guid)info.GetValue("<SessionGuid>k__BackingField", typeof(Guid));
                SessionAction = (bool)info.GetValue("<SessionAction>k__BackingField", typeof(bool));
                MCostOut = (int)info.GetValue("<MCostOut>k__BackingField", typeof(int));
                MCostIn = (int)info.GetValue("<MCostIn>k__BackingField", typeof(int));
                ExpectedPacketIn = (bool)info.GetValue("<ExpectedPacketIn>k__BackingField", typeof(bool));
                SendTimeOut = (TimeSpan)info.GetValue("<SendTimeOut>k__BackingField", typeof(TimeSpan));
                SendIdConnection = (uint?)info.TryGetValue("<SendIdConnection>k__BackingField", typeof(uint?));
                Body = (Body)info.GetValue("<Body>k__BackingField", typeof(Body));
                IdPacket = (ulong)info.GetValue("<IdPacket>k__BackingField", typeof(ulong));
#if WindowsCE
            TimeStamp = (DateTime)info.GetValue("<TimeStamp>k__BackingField", typeof(DateTime));
            SystemTimeStamp = (DateTime)info.GetValue("<SystemTimeStamp>k__BackingField", typeof(DateTime));
#else
                TimeStamp = (DateTimeOffset)info.GetValue("<TimeStamp>k__BackingField", typeof(DateTimeOffset));
                //??
                SystemTimeStamp = (DateTimeOffset)info.TryGetValue("<SystemTimeStamp>k__BackingField", typeof(DateTimeOffset), DateTimeOffset.UtcNow);
#endif
                Status = (Enums.PacketStatus)info.GetValue("<Status>k__BackingField", typeof(int));
                TransmittedPackets = (int)info.GetValue("<TransmittedPackets>k__BackingField", typeof(int));
                Bytes = (int)info.GetValue("<Bytes>k__BackingField", typeof(int));
                TransmittedBytes = (int)info.GetValue("<TransmittedBytes>k__BackingField", typeof(int));
                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
                //nie było tego przed serializacją
				//Params = (List<DataValue>)info.TryGetValue("<Params>k__BackingField", typeof(List<DataValue>), new List<DataValue>());
                //DiagnosticInfos = (List<DiagnosticInfo>)info.TryGetValue("<DiagnosticInfos>k__BackingField", typeof(List<DiagnosticInfo>), new List<DiagnosticInfo>());
            }
            else if (serializationVersion == 1)
            {
                ulong? snDBValue = (ulong?)info.GetValue("SerialNumber", typeof(ulong?));
                if (snDBValue != null)
                    SerialNumber = SN.FromDBValue(snDBValue);

                Address = (string)info.GetValue("Address", typeof(string));
                SessionGuid = (Guid)info.GetValue("SessionGuid", typeof(Guid));
                SessionAction = (bool)info.GetValue("SessionAction", typeof(bool));
                MCostOut = (int)info.GetValue("MCostOut", typeof(int));
                MCostIn = (int)info.GetValue("MCostIn", typeof(int));
                ExpectedPacketIn = (bool)info.GetValue("ExpectedPacketIn", typeof(bool));
                SendTimeOut = (TimeSpan)info.GetValue("SendTimeOut", typeof(TimeSpan));
                SendIdConnection = (uint?)info.GetValue("SendIdConnection", typeof(uint?));
                Body = (Body)info.GetValue("Body", typeof(Body));
                IdPacket = (ulong)info.GetValue("IdPacket", typeof(ulong));
#if WindowsCE
            TimeStamp = (DateTime)info.GetValue("TimeStamp", typeof(DateTime));
            SystemTimeStamp = (DateTime)info.GetValue("SystemTimeStamp", typeof(DateTime));
#else
                TimeStamp = (DateTimeOffset)info.GetValue("TimeStamp", typeof(DateTimeOffset));
                SystemTimeStamp = (DateTimeOffset)info.GetValue("SystemTimeStamp", typeof(DateTimeOffset));
#endif
                Status = (Enums.PacketStatus)info.GetValue("Status", typeof(int));
                TransmittedPackets = (int)info.GetValue("TransmittedPackets", typeof(int));
                Bytes = (int)info.GetValue("Bytes", typeof(int));
                TransmittedBytes = (int)info.GetValue("TransmittedBytes", typeof(int));
                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
                Params = ((DataValue[])info.TryGetValue("Params", typeof(DataValue[]), new DataValue[] { })).ToList();
                DiagnosticInfos = ((DiagnosticInfo[])info.TryGetValue("DiagnosticInfos", typeof(DiagnosticInfo[]), new DiagnosticInfo[] { })).ToList();
            }
        }
#endif
        #endregion

        #region HasBody
        public bool HasBody
        {
            get
            {
                return Body != null && Body.Bytes != null && Body.Bytes.Length > 0;
            }
        }
        #endregion

        #region Guid
        public Guid Guid = Guid.NewGuid();

        public static bool operator ==(Packet left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(Packet left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Packet)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion

        #region Clone
        public Packet Clone()
        {
            Packet p = new Packet();
            p.Guid = Guid;
            p.SerialNumber = SerialNumber;
            p.Address = Address;
            p.SessionGuid = SessionGuid;
            p.SessionAction = SessionAction;
            p.MCostOut = MCostOut;
            p.MCostIn = MCostIn;
            p.ExpectedPacketIn = ExpectedPacketIn;
            p.SendTimeOut = SendTimeOut;
            p.SendIdConnection = SendIdConnection;
            p.Body = Body;
            p.IdPacket = IdPacket;
            p.TimeStamp = TimeStamp;
            p.SystemTimeStamp = SystemTimeStamp;
            p.Status = Status;
            return p;
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            string result = string.Format("[{0}] SN[{1}] Id[{2}] Address[{3}] Status[{4}] Session[{5},{6}] Cost[{7},{8}]",
                ToStringGUID(),
                SerialNumber,
                IdPacket,
                Address,
                Status,
                ToStringSessionGUID(), SessionAction,
                MCostOut, MCostIn
            );
            return result;
        }
        public string ToStringGUID()
        {
            string strGuid = Guid.ToString().ToLower();
            return strGuid.Remove(0, strGuid.LastIndexOf("-") + 1);
        }
        public string ToStringSessionGUID()
        {
            string strGuid = SessionGuid.ToString().ToLower();
            return strGuid.Remove(0, strGuid.LastIndexOf("-") + 1);
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SerialNumber", SerialNumber != null ? SerialNumber.DBValue : null, typeof(ulong?));
            info.AddValue("Address", Address, typeof(string));
            info.AddValue("SessionGuid", SessionGuid, typeof(Guid));
            info.AddValue("SessionAction", SessionAction, typeof(bool));
            info.AddValue("MCostOut", MCostOut, typeof(int));
            info.AddValue("MCostIn", MCostIn, typeof(int));
            info.AddValue("ExpectedPacketIn", ExpectedPacketIn, typeof(bool));
            info.AddValue("SendTimeOut", SendTimeOut, typeof(TimeSpan));
            info.AddValue("SendIdConnection", SendIdConnection, typeof(uint?));
            info.AddValue("Body", Body, typeof(Body));
            info.AddValue("IdPacket", IdPacket, typeof(ulong));
#if WindowsCE
            info.AddValue("TimeStamp", TimeStamp, typeof(DateTime));
            info.AddValue("SystemTimeStamp", SystemTimeStamp, typeof(DateTime));
#else
            info.AddValue("TimeStamp", TimeStamp, typeof(DateTimeOffset));
            info.AddValue("SystemTimeStamp", SystemTimeStamp, typeof(DateTimeOffset));
#endif
            info.AddValue("Status", (int)Status, typeof(int));
            info.AddValue("TransmittedPackets", TransmittedPackets, typeof(int));
            info.AddValue("Bytes", Bytes, typeof(int));
            info.AddValue("TransmittedBytes", TransmittedBytes, typeof(int));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
            info.AddValue("Params", Params.ToArray(), typeof(DataValue[]));
            info.AddValue("DiagnosticInfos", DiagnosticInfos.ToArray(), typeof(DiagnosticInfo[]));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion

    }

    #region PacketConverter
#if !WindowsCE
    internal class PacketConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destType)
        {
            if (destType == typeof(string) && value is Packet)
            {
                // Cast the value to an Packet type
                Packet obj = (Packet)value;
                return obj.Body;
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }
#endif
    #endregion
    #endregion
    #region PacketXmit
    [Serializable]
#if !WindowsCE && !Android
    [DataContract]
#endif
//TS: Niestety nie może być jednocześnie ISerializable i DataContract :/
//#if ISERIALIZABLE_TEST
//	public class PacketXmit : ISerializable
//#else
    public class PacketXmit
//#endif
    {
/*
        #region SerializationVersion
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
        protected readonly byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion
*/
        #region public Guid SessionGuid
#if !WindowsCE && !Android
        [DataMember]
#endif
        public Guid SessionGuid;
        #endregion
        #region public IPv6Address HopAddress
#if !WindowsCE && !Android
        [DataMember]
#endif
        public IPv6Address HopAddress;
        #endregion

        #region public string Address
#if !WindowsCE && !Android
        [DataMember]
#endif
        public string Address;
        #endregion
        #region public Body Body
#if !WindowsCE && !Android
        [DataMember]
#endif
        public Body Body;
        #endregion

        #region public bool RequestReport
#if !WindowsCE && !Android
        [DataMember]
#endif
        public bool RequestReport;
        #endregion
        #region public TimeSpan RequestReportValidity
#if !WindowsCE && !Android
        [DataMember]
#endif
        public TimeSpan RequestReportValidity;
        #endregion
        #region public DateTimeOffset TimeStamp
#if WindowsCE || Android
        public DateTime TimeStamp;
#else
        [DataMember]
        public DateTimeOffset TimeStamp;
#endif
        #endregion
        #region public DateTimeOffset SystemTimeStamp
#if WindowsCE || Android
        public DateTime SystemTimeStamp;
#else
        [DataMember]
        public DateTimeOffset SystemTimeStamp;
#endif
        #endregion
        #region public Enums.PacketStatus Status
#if !WindowsCE && !Android
        [DataMember]
#endif
        public Enums.PacketStatus Status;
        #endregion

        #region public int TransmittedPackets
#if !WindowsCE && !Android
        [DataMember]
#endif
        public int TransmittedPackets;
        #endregion
        #region public int Bytes
#if !WindowsCE && !Android
        [DataMember]
#endif
        public int Bytes;
        #endregion
        #region public int TransmittedBytes
#if !WindowsCE && !Android
        [DataMember]
#endif
        public int TransmittedBytes;
        #endregion
        #region public List<DataValue> Params
#if !WindowsCE && !Android
        [DataMember]
#endif
        public List<DataValue> Params { get; set; }
        #endregion
        #region public List<DiagnosticInfo> DiagnosticInfos
#if !WindowsCE && !Android
        [DataMember]
#endif
        public List<DiagnosticInfo> DiagnosticInfos { get; set; }
        #endregion

        #region Ctor
        public PacketXmit()
        {
            Params = new List<DataValue>();
            DiagnosticInfos = new List<DiagnosticInfo>();
        }

/*
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected PacketXmit(SerializationInfo info, StreamingContext context)
        {
            Params = new List<DataValue>();
            DiagnosticInfos = new List<DiagnosticInfo>();

            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            if (serializationVersion == 0)
            {
                HopAddress = (IPv6Address)info.GetValue("HopAddress", typeof(IPv6Address));
                SessionGuid = (Guid)info.GetValue("SessionGuid", typeof(Guid));
                Address = (string)info.GetValue("Address", typeof(string));
                Body = (Body)info.GetValue("Body", typeof(Body));
                RequestReport = (bool)info.GetValue("RequestReport", typeof(bool));
                RequestReportValidity = (TimeSpan)info.GetValue("RequestReportValidity", typeof(TimeSpan));
#if WindowsCE
            TimeStamp = (DateTime)info.GetValue("TimeStamp", typeof(DateTime));
            SystemTimeStamp = (DateTime)info.GetValue("SystemTimeStamp", typeof(DateTime));
#else
                TimeStamp = (DateTimeOffset)info.GetValue("TimeStamp", typeof(DateTimeOffset));
                SystemTimeStamp = (DateTimeOffset)info.TryGetValue("SystemTimeStamp", typeof(DateTimeOffset), DateTimeOffset.UtcNow);
#endif
                Status = (Enums.PacketStatus)info.GetValue("Status", typeof(Enums.PacketStatus));
                TransmittedPackets = (int)info.GetValue("TransmittedPackets", typeof(int));
                TransmittedBytes = (int)info.GetValue("TransmittedBytes", typeof(int));
                Bytes = (int)info.GetValue("Bytes", typeof(int));
                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
				//nie było tego przed serializacją
                //Params = (List<DataValue>)info.TryGetValue("Params", typeof(List<DataValue>), new List<DataValue>());
                //DiagnosticInfos = (List<DiagnosticInfo>)info.TryGetValue("DiagnosticInfos", typeof(List<DiagnosticInfo>), new List<DiagnosticInfo>());
            }
            else if (serializationVersion == 1)
            {
                HopAddress = (IPv6Address)info.GetValue("HopAddress", typeof(IPv6Address));
                SessionGuid = (Guid)info.GetValue("SessionGuid", typeof(Guid));
                Address = (string)info.GetValue("Address", typeof(string));
                Body = (Body)info.GetValue("Body", typeof(Body));
                RequestReport = (bool)info.GetValue("RequestReport", typeof(bool));
                RequestReportValidity = (TimeSpan)info.GetValue("RequestReportValidity", typeof(TimeSpan));
#if WindowsCE
            TimeStamp = (DateTime)info.GetValue("TimeStamp", typeof(DateTime));
            SystemTimeStamp = (DateTime)info.GetValue("SystemTimeStamp", typeof(DateTime));
#else
                TimeStamp = (DateTimeOffset)info.GetValue("TimeStamp", typeof(DateTimeOffset));
                SystemTimeStamp = (DateTimeOffset)info.TryGetValue("SystemTimeStamp", typeof(DateTimeOffset), DateTimeOffset.UtcNow);
#endif
                Status = (Enums.PacketStatus)info.GetValue("Status", typeof(int));
                TransmittedPackets = (int)info.GetValue("TransmittedPackets", typeof(int));
                TransmittedBytes = (int)info.GetValue("TransmittedBytes", typeof(int));
                Bytes = (int)info.GetValue("Bytes", typeof(int));
                Guid = (Guid)info.GetValue("Guid", typeof(Guid));
                Params = ((DataValue[])info.TryGetValue("Params", typeof(DataValue[]), new DataValue[] { })).ToList();
                DiagnosticInfos = ((DiagnosticInfo[])info.TryGetValue("DiagnosticInfos", typeof(DiagnosticInfo[]), new DiagnosticInfo[] { })).ToList();
            }
        }
#endif
*/

        #endregion

        #region Guid
#if !WindowsCE && !Android
        [DataMember]
#endif
        private readonly Guid Guid = Guid.NewGuid();

        public static bool operator ==(PacketXmit left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }

        public static bool operator !=(PacketXmit left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PacketXmit)) return false;
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion
/*
        #region GetObjectData

#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("HopAddress", HopAddress, typeof(IPv6Address));
            info.AddValue("SessionGuid", SessionGuid, typeof(Guid));
            info.AddValue("Address", Address, typeof(string));
            info.AddValue("Body", Body, typeof(Body));
            info.AddValue("RequestReport", RequestReport, typeof(bool));
            info.AddValue("RequestReportValidity", RequestReportValidity, typeof(bool));
#if WindowsCE
            info.AddValue("TimeStamp", TimeStamp, typeof(DateTime));
            info.AddValue("SystemTimeStamp", SystemTimeStamp, typeof(DateTime));
#else
            info.AddValue("TimeStamp", TimeStamp, typeof(DateTimeOffset));
            info.AddValue("SystemTimeStamp", SystemTimeStamp, typeof(DateTimeOffset));
#endif
            info.AddValue("Status", (int)Status, typeof(int));
            info.AddValue("TransmittedPackets", TransmittedPackets, typeof(int));
            info.AddValue("TransmittedBytes", TransmittedBytes, typeof(int));
            info.AddValue("Bytes", Bytes, typeof(int));
            info.AddValue("Guid", Guid, typeof(Guid));
            info.AddValue("Params", Params.ToArray(), typeof(DataValue[]));
            info.AddValue("DiagnosticInfos", DiagnosticInfos.ToArray(), typeof(DiagnosticInfo[]));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif

        #endregion
*/

    }
    #endregion
}