using System;
using System.Linq;
using System.Reflection;

namespace IMR.Suite.Common
{
    [Serializable]
    public class Unit
    {
        #region Members
        private static readonly string UNKNOWN_UNIT = "Unknown Unit";
        #endregion

        #region Units
        public const int METER = 10;    
         
        public const int MILLIMETER = 12; 
        
        public const int M3 = 14;
        public const int LITER = 15;

        public const int M3_PER_HOUR = 23;
        public const int LITER_PER_HOUR = 24;

        public const int CELSIUS = 33;

        public const int PERCENT = 42;

        public const int KILOGRAM = 82;

        public const int KILOGRAM_NA_METR3 = 91;

        public const int KILOMETRY_NA_GODZINE = 97;
        #endregion

        #region GetName
        internal static System.Reflection.FieldInfo[] dataTypeFields = null;
        public static string GetName(long idUnit)
        {
            try
            {
                if (dataTypeFields == null)
                {
                    Type t = Type.GetType("IMR.Suite.Common.Unit");
                    if (t == null)
                        return UNKNOWN_UNIT;

                    dataTypeFields = t.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
                    if (dataTypeFields == null)
                        return UNKNOWN_UNIT;
                }

                FieldInfo field = dataTypeFields.SingleOrDefault(f => (long)f.GetValue(null) == idUnit);
                if (field != null)
                    return field.Name;

                return UNKNOWN_UNIT;
            }
            catch
            {
                return UNKNOWN_UNIT;
            }
        }
        #endregion
    }
}

