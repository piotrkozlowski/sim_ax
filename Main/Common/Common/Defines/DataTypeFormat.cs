using System;
using System.Collections.Generic;
using System.Text;

namespace IMR.Suite.Common
{
#if !(WINDOWS_PHONE)
    [Serializable]
#endif
    public class DataTypeFormat
    {
        #region Members

        #region IdDataTypeFormat
        private int idDataTypeFormat = 0;

        public int IdDataTypeFormat
        {
            get
            {
                return idDataTypeFormat;
            }
        }
        #endregion

        public string Description = null;

        public int? TextMinLength = null;
        public int? TextMaxLength = null;
        public int? NumberMinPrecision = null;
        public int? NumberMaxPrecision = null;
        public int? NumberMinScale = null;
        public int? NumberMaxScale = null;
        public double? NumberMinValue = null;
        public double? NumberMaxValue = null;
        public string DatetimeFormat = null;
        public string RegularExpression = null;
        public bool? IsRequired = null;
        public int? IdUniqueType = null;

        #endregion


        #region Constructor
        public DataTypeFormat(int IdDataTypeFormat, string Description, int? TextMinLength, int? TextMaxLength, int? NumberMinPrecision, int? NumberMaxPrecision, int? NumberMinScale, int? NumberMaxScale, double? NumberMinValue, double? NumberMaxValue, string DatetimeFormat, string RegularExpression, bool? IsRequired, int? IdUniqueType)
        {
            this.idDataTypeFormat = IdDataTypeFormat;
            this.Description = Description;
            this.TextMinLength = TextMinLength;
            this.TextMaxLength = TextMaxLength;
            this.NumberMinPrecision = NumberMinPrecision;
            this.NumberMaxPrecision = NumberMaxPrecision;
            this.NumberMinScale = NumberMinScale;
            this.NumberMaxScale = NumberMaxScale;
            this.NumberMinValue = NumberMinValue;
            this.NumberMaxValue = NumberMaxValue;
            this.DatetimeFormat = DatetimeFormat;
            this.RegularExpression = RegularExpression;
            this.IsRequired = IsRequired;
            this.IdUniqueType = IdUniqueType;
        }
        #endregion

    }

}
