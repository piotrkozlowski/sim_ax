﻿namespace IMR.Suite.Common
{
	// !!! UWAGA !!! 
	// 23.08.2012: poniżej to chyba nie jest prawda, przecież to są liczby, które zostaną wkompilowane, więc chyba nie jest potrzeba podmiany
	//
	// Definiujac nowe Actions należy pamiętać iż wymagana będzie 
	// kompilacja biblioteki 'AIUT.IMR.Suite.Actions.Library.dll' jak i jej podmiana na serwerze docelowym 
	//
	// Dlatego w przypadku testów nowych PlugIn'ow z nowo definiowanymi Actions zalecane jest:
	// 1. W klasie poniżej zdefniować nowe ID z odpowiednim komentarzem (patrz niżej NowyID)
	// 2. W PlugIn wykorzystującym nowo zdefinowany ID należy wpisać liczbę jak i wykorzystanie klasy w komentarzu np.
	//			ActionResult result = new ActionResult(Enums.ActionStatus.Running, 20000/*DiagDescrID.Actions.NowyID*/, ....);
	//
    public partial class DiagDescrID
    {
        public class Actions
        {
            // Wykorzystywane w PlugIn'e NowyPlugIn.dll, 
            public const long NowyID = 20000;

            
            public const long NewActionAdded = 20001;
            public const long Running = 20002;
            public const long Succeeded = 20003;
            public const long Error = 20004;
            public const long Partly = 20005;
            public const long Stopped = 20006;
            public const long Aborted = 20007;
            public const long UnknownState = 20008;
            public const long Waiting = 20009;

            public const long InputArgsIncorrect = 20010;
            public const long NoActionData = 20011;
            public const long InitActionOK = 20012;
            public const long InitActionError = 20013;
            public const long InitActionException = 20014;
            public const long AddActionDataError = 20015;
            public const long UpdateDataError = 20016;
            public const long IncorrectMeasureData = 20017;
            public const long UnknownSerialNbr = 20018;
            public const long ActionNote = 20019;//"{0}";          "  {0}"

            public const long GetPutException = 20020;
            public const long ExecuteException = 20021;

            public const long NotImplementedForSerialNbr = 20030;
            public const long NotImplementedForMeter = 20031;
            public const long NotImplementedForLocation = 20032;
            public const long SerialNbrCanNotBeNULL = 20033;
            public const long MeterCanNotBeNULL = 20034;
            public const long LocationCanNotBeNULL = 20035;
            public const long CanNotInvokedForSerialNbr = 20036;
            public const long CanNotInvokedForMeter = 20037;
            public const long CanNotInvokedForLocation = 20038;
            public const long NeitherActionToRun = 20039;

            public const long MeterDoesNotHaveParent = 20040;
            public const long MeterDoesNotHaveActiveParent = 20041;
            public const long SerialNbrIsNotParentForMeter = 20042;
            public const long LocationDoesNotHaveAnyEquipment = 20043;
            public const long SerialNbrNotInAnyLocation = 20044;

            public const long NoPermissionsForSMSActions = 20045;
            public const long AccessDeniedForSMSAction = 20046;

            public const long InputSMSArgsIncorrect = 20049;
            public const long UnknownOrBlockedOperator = 20050;
            public const long OperatorCanNotBeNULL = 20051;

            public const long CheckSynchronizeClock_NoDevicesToSynchronize = 20052;
            public const long CheckSynchronizeClock_InvalidTolerance = 20053;
            public const long CheckSynchronizeClock_UnknownProtocol = 20054;
            public const long CheckSynchronizeClock_NoClockFromDevice = 20055;
            public const long CheckSynchronizeClock_DeviceSynchronized = 20056;
            public const long CheckSynchronizeClock_NothingToProcess = 20057;
            public const long CheckSynchronizeClock_FramStatus = 20058;

            public const long SMSStationCurrentState_StationCodeNotProvided = 20060;
            public const long SMSStationCurrentState_NoAtgImrDevicesInLocation = 20061;
            public const long SMSStationCurrentState_LocationNotExists = 20063;

            public const long ErrorFramStatus = 20062;

            public const long ChangeCID_NOTnewCID = 20064;
            public const long ChangeCID_NOTbattChange = 20065;
            public const long ChangeCID_NObatts = 20066;

            public const long PsionConfig_JACKA_BELO = 20067;

            public const long SMSCurrentState_CIDNotProvided = 20070;
            public const long SMSCurrentState_NoMeterDevicesInLocation = 20071;
            public const long SMSCurrentState_LocationNotExists = 20072;

            public const long ChangeALevel_NeitherALevelAddress = 20075;
            public const long ChangeALevel_BadDeviceProtocol = 20076;
            public const long ChangeALevel_NoFreeSlot = 20077;
            public const long ChangeALevel_ErrorInDB = 20078;
            public const long ChangeALevel_MissingInvalidParameter = 20079;//"Missing/Invalid Parameter";          "  Missing/Invalid {0} in device DATA for SN: {1}! "

            public const long ChangeCID_IncorrectCID = 20080;
            public const long ChangeCID_BadDeviceProtocol = 20076; // to samo co ChangeALevel_BadDeviceProtocol bo ten sam komunikat
            public const long ChangeCID_ErrorInDB = 20081;

            public const long ChangeSIMCard_ErrorInDB = 20082;

            public const long CheckFreezeDevice_BadSerialNbr = 20085;
            public const long CheckFreezeDevice_CouldNotGetDevices = 20086;
            public const long CheckFreezeDevice_NoDevicesToFreeze = 20087;

            public const long CheckSleepyDevice_BadSerialNbr = 20090;
            public const long CheckSleepyDevice_CouldNotGetDevices = 20091;
            public const long CheckSleepyDevice_NoDevicesToWakeUp = 20092;

            public const long GetDataForWSGApulse_NoSerialNbrList = 20100;
            public const long GetDataForWSGApulse_SerialNbrParseError = 20101;
            public const long GetDataForWSGApulse_UnknownSerialNbr = 20102;
            public const long GetDataForWSGApulse_NumberOfProperSerialNbrsOnList = 20103;
            public const long GetDataForWSGApulse_NoProperSerialNbrsOnList = 20104;
            public const long GetDataForWSGApulse_ActionNoServerSerialNbrAndSerialNbrsOnList = 20105;
            public const long GetDataForWSGApulse_NoActionData = 20106;
            public const long GetDataForWSGApulse_WaitingForResponse = 20107;
            public const long GetDataForWSGApulse_PartlyFinished = 20110;
            public const long GetDataForWSGApulse_AttemptNumber = 20111;

            public const long CreditControl_NoData = 20120; //do 20139
            public const long CreditControl_DeviceOnly = 20121;
            public const long CreditControl_NoConsumerDevice = 20122;
            public const long CreditControl_UnknownCommand = 20123;

            public const long WaitingAndWillStartAt = 20140; //do 20159
            public const long WaitingForAndWillStartAt = 20141;
            public const long WaitingForAndWillTimeout = 20142;
            public const long WaitingAndWillStartIncorrectParams = 20143;
            public const long ReceivedAwaitedPacket = 20144;
            public const long CalculatedTimeForScheduleAction = 20145;
            public const long CalculateTimeErrorForScheduleAction = 20146;

            public const long InstallGasMeter_IncorrectCID = 20160; //do 20179
            public const long InstallGasMeter_BadDeviceProtocol = 20161;
            public const long InstallGasMeter_ErrorInDB = 20162;
            public const long InstallGasMeter_ImpulseWeightCoefficientIsMissing = 20163;
            public const long InstallGasMeter_ErrorReadingLiterOffset = 20164;

            public const long CheckRTRSData_NoLocationForOKO = 20180; //do 20189

            public const long ChangeDevice_NeitherDeviceAddress = 20190; //do 20210
            public const long ChangeDevice_BadDeviceProtocol = 20191;
            public const long ChangeDevice_NoFreeSlot = 20192;
            public const long ChangeDevice_ErrorInDB = 20193;
            public const long ChangeDevice_NoSlotType = 20194;

            public const long FlashFirmware_InputArgsIncorrect = 20210; // do 20230
            public const long FlashFirmware_FirmwareVerNotProvided = 20211;
            public const long FlashFirmware_DeviceHasNotCID = 20212;
            public const long FlashFirmware_CouldNotSetCID = 20213;
            public const long FlashFirmware_FirmwareNotDownloaded = 20214;
            public const long FlashFirmware_FirwareNotInitialized = 20215;
            public const long FlashFirmware_CouldNotSetParams = 20216;
            public const long FlashFirmware_CouldNotGetFirmwareVer = 20217;
            public const long FlashFirmware_TheSameFirmwareVer = 20218;
            public const long FlashFirmware_DifferentSerialNbr = 20219;
            public const long FlashFirmware_ActionMode = 20220;
            public const long FlashFirmware_VersionInDB = 20221;
            public const long FlashFirmware_VersionInDevice = 20222;
            public const long FlashFirmware_CouldNotGetLiterCounter = 20224;
            public const long FlashFirmware_FirmwareHasBeenSuccessfullyFlashed = 20225;
            public const long FlashFirmware_FirmwareWasntFlashed = 20226;
            public const long FlashFirmware_CouldNotSetCounters = 20227;
            public const long FlashFirmware_LiterCounterIsDifferent = 20228;
            public const long FlashFirmware_EnergyCounterIsDifferent = 20229;
            public const long FlashFirmware_CouldNotCheckFirmwareProperlyFlashed = 20290;
            public const long FlashFirmware_CouldNotSetSerialNumberAndGPRS = 20340;
            public const long FlashFirmware_CouldNotFindDriverWithPluginName = 20341;
            public const long FlashFirmware_MoreThanOneDriverWithPluginName = 20342;
            public const long FlashFirmware_CouldNotFindDeviceTypeWithDriver = 20343;
            public const long FlashFirmware_MoreThanOneDeviceTypeWithDriver = 20344;
            public const long FlashFirmware_NewDeviceTypeNotDefined = 20345;
            public const long FlashFirmware_NetworkDeviceIsNotSuppliedByAC = 20346;
            public const long FlashFirmware_DeviceHasErrorLockingRemoteFlashFirmware = 20347;

            public const long ChangeOKOForProbe_ErrorInDB = 20223;

            public const long ChangeCustomerAlevel_IncorrectCID = 20230; //do 20240
            public const long ChangeCustomerAlevel_ErrorInDB = 20231;

            public const long SMSLatchBatteryUsage_BadDeviceProtocol = 20241; //do 20250

            public const long SMSMultireadReqCurrentState_NoImrDevicesInHierarchy = 20251; //do 20260

            public const long ProfileConfiguration_ProfileNotSelected = 20261; //do 20270
            public const long ProfileConfiguration_ProfileNotFoundInDB = 20262;
            public const long ProfileConfiguration_DeviceTypeNotSupported = 20263;
            public const long ProfileConfiguration_DeviceTypeIsMultitypeKindAndNotMultidatagram = 20264;
            public const long ProfileConfiguration_ActionTypeNotFound = 20265;
            public const long ProfileConfiguration_CouldNotLoadMapConfig = 20266;
            public const long ProfileConfiguration_Timeout = 20267;
            public const long ProfileConfiguration_DataBaseTimeout = 20274;
            public const long ProfileConfiguration_DeviceIsServicedOrErased = 20275;

            public const long PrepaidOKOFix_CanBeInvokedOnlyForIMRServer = 20268;
            public const long PrepaidOKOFix_NoDevicesToRepair = 20269;
            public const long PrepaidOKOFix_ActionTypeNotFound = 20270;
            public const long PrepaidOKOFix_CantSetClockForDevice = 20271;
            public const long PrepaidOKOFix_MissingParameter = 20272;
            public const long PrepaidOKOFix_RepairProcedureReturnsError = 20273;

            public const long OperationValve_OperationResult = 20280;
            public const long OperationValve_OperationError = 20281;
            public const long OperationValve_OperationResultVolume = 20282;//"GAS_METER_USAGE_VALUE[m3]= {0} [UTC.Time {1}]";          "  OperationValve_OperationResultVolume"

            //20290-FlashFirmware_CouldNotCheckFirmwareProperlyFlashed
            public const long RawDataEventsDisable_FirmwareVerNotSupported = 20291;
            public const long RawDataEventsDisable_CouldNotGetFirmwareVer = 20292;
            public const long RawDataEventsDisable_DeviceWasAlreadyConfiguredByThisAction = 20293;

            public const long SendActionSequence_NoActionsSelectedToRun = 20300;
            public const long SendActionSequence_MoreThanOneActionOnTheSameIndex = 20301;

            public const long CheckResetDeviceWithCommand_BadSerialNbr = 20310;
            public const long CheckResetDeviceWithCommand_FalilureSendingMail = 20311;

            public const long TestIMRCreditCheck_CouldNotGetConsumerForDevice = 20320;
            public const long TestIMRCreditCheck_CouldNotGetParametersForConsumer = 20321;

            public const long GetDiagnostics_ErrorGettingDiagnostics = 20331;
            public const long GetDiagnostics_DeviceTypeNotSupported = 20332;
            public const long GetDiagnostics_DiagnosticsUpToDate = 20333;

            public const long CheckGetArchiveData_DeviceTypeNotSupportedForMultiread = 20334;

            public const long CheckGetAnalogArchiveMissingReadouts_DeviceTypeNotSupported = 20335;
            public const long CheckGetAnalogArchiveMissingReadouts_StartsForDevice = 20336;
            public const long CheckGetAnalogArchiveMissingReadouts_NoAnalogReadoutsForDevice = 20337;
            public const long CheckGetAnalogArchiveMissingReadouts_NoAnalogArchiveLatchPeriod = 20338;
            public const long CheckGetAnalogArchiveMissingReadouts_FoundMissingData = 20339;

            //20340 do 20349 zarezerwowane dla FlashFirmware


            //20350 do 20359 CheckSetGPRS
            public const long CheckSetGPRS_MissingParameter = 20350;
            public const long CheckSetGPRS_AverageGSMQualityTooLow = 20351;
            public const long CheckSetGPRS_SingleGSMQualityMeasereBelowMinimumValue = 20352;

            //20360 do 20369 DWTask
            public const long DWTask_ErrorMsgFromIntellingence = 20360;
            public const long DWTask_IdLocationParseError = 20361;

            //20370 do 20379 InstallGasMeterElster
            public const long InstallGasMeterElster_IncorrectDepositoryCID = 20370;

            //20380 do 20389 UserPortalDeviceConfiguration
            public const long UserPortalDeviceConfiguration_ErrorInDB = 20380;
            public const long UserPortalDeviceConfiguration_BadDeviceProtocol = 20381;


            //20390 do 20399 SynchronizeApulse
            public const long SynchronizeApulse_ActionsRunForDevices = 20390;                   //"Actions started for selected devices. ";          "  Actions started for devices {0}."       
            public const long SynchronizeApulse_DeviceNotInUTCTime = 20391;                     //"Invalid time in device! ";          "  Device {0} time not in UTC!"       
            public const long SynchronizeApulse_CouldntFindProperSchedule = 20392;              //"Couldnt find proper schedule! ";          "  Couldnt find proper schedule with command IMRWAN2_COMMAND_SEND_FRAMES_FROM_FLASH for device {0}"       
            public const long SynchronizeApulse_NoBroadcastApulseInDeviceHierarchy = 20393;     //"No broadcast Apulse in Device Hierarchy! ";          "  Couldnt find any APULSE with broadcast address in device Hierarchy for OKO {0}"

            //20400 do 20419 DeviceManagement
            public const long DeviceManagement_DataBaseTimeout = 20400;//"Timeout bazy danych ";          "  Nie udało się pobrać danych z bazy, prawdopodobnie z powodu upłynięcia limitu czasu żadania"       
            public const long DeviceManagement_MissingParameter = 20401;//"Brakuje parametru ";          "  Wymagany parametr {0} nie został przekazany do akcji"
            public const long DeviceManagement_NotCustomerLocation = 20402;//"Brakuje parametru ";          "  Wymagany parametr {0} nie został przekazany do akcji"

            //20420 do 20429 SMSText
            public const long SMSText_MessageSentToOperator = 20420;//"Wiadomość wysłana do operatora.  ";          "  Wiadomość wysłana do operatora id_operatora: {0}; treść wiadomości {1}."       
            public const long SMSText_UnknownOperator = 20421;//"Nieznany operator.  ";          "  Nieznany operator -  id {0}"       
            public const long SMSText_NoKnownOperator = 20422;//"Brak rozpoznawalnego operatora.  ";          "  Brak rozpoznawalnego operatora."       
            public const long SMSText_SeveralUnknownOperators = 20423;//"Kilku nierpozpoznawalnych operatorów. ";          "  Kilku nierpozpoznawalnych operatorów."

            //od 20430 InstallTankMeter
            public const long InstallTankMeter_LocationCIDNotProvided = 20430;//"CID Code not provided  ";          "  Location CID Code not provided in action parameters"       
            public const long InstallTankMeter_LocationNotExists = 20431;//"Location not exist ";          "  Location not found for CID : {0}"       
            public const long InstallTankMeter_MissingAlevelAddress = 20432;//"No Alevel address provided ";          "  No Alevel address provided"       
            public const long InstallTankMeter_AlevelNotExists = 20433;//"Alevel not exist ";          "  Alevel for address: {0} not exists"       
            public const long InstallTankMeter_InvalidTankParameters = 20434;//"Invalid tank parameters ";          "  Invalid tank params. Capacity:{0}, Meter SN: {1}"       
            public const long InstallTankMeter_MissingAllTankParameters = 20435;//"Missing tank parameters ";          "  Missing all required tank parameters. "

            //od 20440 SMSRegisterPhoneNbr
            public const long SMSRegisterPhoneNbr_BadSerialNbr = 20440; //"Incorrect serial number";        "Is already registered"

            //od 20450 ChangeMonthlySchedule
            public const long ChangeMonthlySchedule_NoFreeIndexesToSendPut = 20450;//"Not enough free schedule indexes to send full put action ";          "  Not enough free schedule indexes to send full put action"       
            public const long ChangeMonthlySchedule_MissingInvalidParameter = 20451;//"Misssing/Invalid param ";          "  Misssing/Invalid param:  {0} "       
            public const long ChangeMonthlySchedule_NotImplementedForThisDevice = 20452;//" Action not supported for device type ";          "  Action not supported for device type: {0}({1}) nad id protocol {2}"       
            public const long ChangeMonthlySchedule_StartsForDevice = 20453;//"Starting to change montly schedule ";          "  Starting to change montly schedule for device {1}"

            //od 20460 SMSRegisterPhoneNbr
            public const long SMSRegisterPhoneNbr_PhoneNumberNotProvided = 20460;//"Phone number not privided ";          "  Phone number not privided [DEVICE_PHONE_NUMBER]"       
            public const long SMSRegisterPhoneNbr_MissingAllowedDistributors = 20461;//"Missing Allowed distributors ";          "  Missing Allowed distributors [ACTION_DISTRIBUTOR_ID]"       
            public const long SMSRegisterPhoneNbr_MissingOperator = 20462;//"Missing added operator ";          "  Missing added operator"       
            public const long SMSRegisterPhoneNbr_OperatorIsBlocked = 20463;//"Operator is already blocked ";          "  Operator is already blocked for phone number {0}"       
            public const long SMSRegisterPhoneNbr_SavingSchedule = 20464;//"Saving schedule to block operator ";          "  Saving schedule to block operator {0} on {1}"       
            public const long SMSRegisterPhoneNbr_ScheduleSavingException = 20465;//"Saving schedule to block operator exception ";          "  Saving schedule to block operator exception"       
            public const long SMSRegisterPhoneNbr_OperatorDoesntExist = 20466;//"Unknown id operator ";          "  Unknown id operator {0}"       
            public const long SMSRegisterPhoneNbr_SavingOperatorException = 20467;//"Saving operator exception  ";          "  Saving operator {0}  exception. "


            //20470 InstallGasMeterGaspol

            public const long InstallGasMeterGaspol_MissingInvalidParameter = 20470;//"Błędny/brakujący parametr ";          "  Błędny/brakujący parametr {0}"       
            public const long InstallGasMeterGaspol_DeviceNotFoundForThisSetNumber = 20471;//"Nie znaleziono urządzenia.  ";          "  Nie znaleziono urządzenia dla DEVICE_SET_NUMBER: {0}"       
            public const long InstallGasMeterGaspol_BattryChangeDateChanged = 20472;//"Zmieniono DEVICE_BATTERY_CHANGE_DATE ";          "  Zmieniono DEVICE_BATTERY_CHANGE_DATE"       
            public const long InstallGasMeterGaspol_BattryChangeDateNotChanged = 20473;//"Nie udało się zmienić DEVICE_BATTERY_CHANGE_DATE ";          "  Nie udało się zmienić DEVICE_BATTERY_CHANGE_DATE"       
            public const long InstallGasMeterGaspol_CouldntGetCounter = 20474;//"Nie pobrano GAS_METERU_USAGE_VALUE  ";          "  Nie pobrano GAS_METERU_USAGE_VALUE  dla {0}"       
            public const long InstallGasMeterGaspol_ConversionChanged = 20475;//"Zmieniono/dodano wspis do CONVERSION ";          "  Nowe przeliczenie dla GAS_METER_USAGE_VALUE urządzenie {0}, Slope{1}, bias{2}"       
            public const long InstallGasMeterGaspol_ConversionNotChanged = 20476;//"Nie udało się zmienić/dodać wspisu do CONVERSION ";          "  Nie udało się zmienić/dodać wspisu do CONVERSION dla GAS_METER_USAGE_VALUE urządzenie {0}, Slope{1}, bias{2}"       
            public const long InstallGasMeterGaspol_RecountingGasMeterUsageValue = 20477;//"Przeliczenie  GAS_METERU_USAGE_VALUE  ";          "  Przeliczenie  GAS_METERU_USAGE_VALUE  z drivera: {0}, stary Slope{1}, stary bias{2}"       
            public const long InstallGasMeterGaspol_NotImplementedForThisDevice = 20478;//"Nie zaimplementowano dla tego urządzenia  ";          "  Nie zaimplementowano dla urządzenia typu{0}({1}) i protokołu {3}"

            //20480 TestIMR
            public const long TestIMR_ParentDeviceNotFound = 20480;//"Potential parent device not found";          "  Potential parent device not found {0}"       
            public const long TestIMR_NoProperParentDeviceFound = 20481;//"Missing proper data for potential parent devices";          "  Missing proper data for potential parent devices"       
            public const long TestIMR_ReceivedDataFromDevice = 20482;//"Received data from device";          "  Received data from device, order {0}, SN {1}, Radio Quality {2}"

            //20490 OKOxxx3ConfigFix
            public const long OKOxxx3ConfigFix_DeviceTypeNotSupported = 20490;//"Device Type not supported!";          "  Device Type {0}  not supported!"       
            public const long OKOxxx3ConfigFix_ActionInformationChangeParam = 20491;//"Change parameter!";          "  Change {0} to {1}. Reason: {2}!"       
            public const long OKOxxx3ConfigFix_ActionWarningMissingParam = 20492;//"Missing parameter in device!";          "  Change {0} Fail. Missing {1} in {2}!"       
            public const long OKOxxx3ConfigFix_ActionDiodes = 20493;//"DEVICE_PRODUCTION_DATE found!";          "  DEVICE_PRODUCTION_DATE {0}. Diodes problem: {1}."       
            public const long OKOxxx3ConfigFix_MNCFound = 20494;//"DEVICE_GSM_PROVIDER_MOBILE_NETWORK_CODE Found. ";          "  DEVICE_GSM_PROVIDER_MOBILE_NETWORK_CODE {0}"       
            public const long OKOxxx3ConfigFix_NITZ_ON = 20495;//"Enable synchronization from NITZ";          "  Enable synchronization from NITZ"       
            public const long OKOxxx3ConfigFix_GSM_BAND_EU = 20496;//"Enable GSM BAND European!";          "  GSM BAND European for MNC: {0} [{1}]"       
            public const long OKOxxx3ConfigFix_GSM_BAND_AM = 20497;//"American GSM BAND ";          "  American GSM BAND for: {0} [{1}]"       
            public const long OKOxxx3ConfigFix_MNC_NOT_Supported = 20498;//"MNC  Not supported!";          "  MNC {0} Not supported!"       
            public const long OKOxxx3ConfigFix_PowerMode = 20499;//"DEVICE_POWER_CONFIG_MODE = 1 found. Scheduling gsm reset!";          "  DEVICE_POWER_CONFIG_MODE = 1 found. Scheduling gsm reset!"       
            public const long OKOxxx3ConfigFix_NoFreeSchedule = 20500;//"No free Schedule index found!!";          "  No free Schedule index found!!"       
            public const long OKOxxx3ConfigFix_NoScheduleData = 20501;//"No schedule data found!!";          "  No schedule data found!!"

            //20510 do 20519 ChangeCryptographySettings
            public const long ChangeCryptographySettings_MissingNewKey = 20510;
            public const long ChangeCryptographySettings_ActionProcessingTimeout = 20511;//"Action processing timeout!";          "  Time allowed to proceed this action has passed ({0} seconds). Device didn't responded in that time"
            
            //20520 do 20519 InstallDualMeter                   
            public const long InstallDualMeter_LocationCIDNotProvided = 20520;//"Missing LOCATION_CID!";          "  Missing LOCATION_CID!"       
            public const long InstallDualMeter_ErrorInDB = 20521;//"Error in DB!";          "  Result {0} [{1}]. CID {2}. SN{3}"       
            public const long InstallDualMeter_MissingM3PerImpulse = 20522;//"Missing METER_M3_PER_IMPULSE!";          "  Missing METER_M3_PER_IMPULSE!"       
            public const long InstallDualMeter_BadDeviceProtocol = 20523;//"Unsupported protocol!";          "  Wrong device {0} Protocol {1}! Only WAN3!"
        }
    }
}
