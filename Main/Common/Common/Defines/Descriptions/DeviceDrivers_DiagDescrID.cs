using System;
using System.Text;


namespace IMR.Suite.Common
{
    public partial class DiagDescrID
    {
        public class DeviceDrivers
        {
            //62001-64000 DIAGNOSTIC for Device Drivers
            public const long Debug1 = 62001;

            public const long ProtocolNotSupported = 62002; //"Protocol not supported";         "Protocol {0} is not supported by this device driver"

            public const long NoProcessData = 62003;                        //"No ProcessData ";          "  Received empty ProcessData - nothing to process"       
            public const long ProcessTypeUndefined = 62004;                 //"ProcessType Undefined ";          "  ProcessType undefined for ProcessData and ProcessData.Values !!! SN {0}"       
            public const long SlotInMissing = 62005;                        //"SlotIn Missing ";          "  No SlotIn to process"       
            public const long MissingDataTypeInDeviceDriverData = 62006;    //"Missing DataType in DeviceDriverData ";          "  Missing {2}  in DevieDriverData (Device Parameters) for SN:{0} index: {1} "       
            public const long NoProperDataValues = 62007;                   //"No Proper DataValues ";          "  No data.Values - nothing to process ! SN: {0}"       
            public const long InvalidProcessType = 62008;                   //"Invalid Process Type ";          "  ProcessType={2} not supported for object: {1}!  SN:{0}"       
            public const long TableObjectIdNotFound = 62009;                //"Table ObjectId Not Found ";          "  DEVICE_TABLE_OBJECT_ID = {1} not found, or not found as a table! SN:{0}"       
            public const long CommandNotMatched = 62010;                    //"Command Not Matched ";          "  SN: {0} Could not match command! address: {1}, len: {2}!"       
            public const long CouldntDecodeCommandValues = 62011;           //"Couldn't Decode Command Values ";          "  Couldn't decode command values, global offset: {1}  SN: {0}"       
            public const long CommandLengthIncorrect = 62012;               //"Command Length Incorrect ";          "   Number of comands different than expected - sentCommands.Length({1}) != e.Task.Command.Length({2})! SN: {0}"       
            public const long CommandOffsetIncorrect = 62013;               //"Command Offset Incorrect ";          "  Received incorrect commad -  sentCommands[{1}].GlobalOffset({2}) != e.Task.Command[{3}].Address({4})! SN: {0}"       
            public const long MissingReceivedData = 62014;                  //"Missing Received Data ";          "  Missing {0} data to decode  IDDataType[{1}] in propoer unit or time! SN: {2}"       
            public const long MissingDataBeforePut = 62015;                 //"Missing Data Before Put ";          "  Missing {0} data (in PutData or DeviceDriverData) to endcode  IDDataType[{1}] in propoer unit! SN: {2}"       
            public const long ConvertValuesOutInvalidValue = 62016;         //"Convert Values Out Invalid Value ";          "  Invalid value found in IDDataType[{0}] !  SN: {1}"       
            public const long GetBytesError = 62017;                        //"Get Bytes Error ";          "  [{0}] Could not get bytes! Exception: \"{1}\" : OMB[{2}]: {3} !"       
            public const long WrongPutBytesSize = 62018;                    //"Wrong Put Bytes Size ";          "  [{0}] Wrong bytes size!  OMB[{1}]: {2}, sended: {3}B, expected {4}B!"

        }
    }
}