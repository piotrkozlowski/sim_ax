using System;
using System.Text;


namespace IMR.Suite.Common
{
	public partial class DiagDescrID
	{
		public class COREActions
		{
            //60601-60800 DIAGNOSTIC for CORE ACTIONS
            public const long Debug1 = 60601;

            public const long ActionStartedFromSmsByOperator = 60602;//"Action started by SMS";          "Action started by SMS sent from operator {0}. Message text: {1}"
        }	
	}
}