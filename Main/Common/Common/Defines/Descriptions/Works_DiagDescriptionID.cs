using System;
using System.Text;

namespace IMR.Suite.Common
{
    public partial class DiagDescrID
    {
        public class Works
        {
            public const long NewID = 23000;

            public const long NewWorkAdded = 23001;
            public const long Running = 23002;
            public const long Succeeded = 23003;
            public const long Error = 23004;
            public const long Partly = 23005;
            public const long Stopped = 23006;
            public const long Aborted = 23007;
            public const long UnknownState = 23008;
            public const long Waiting = 23009;

            public const long InputArgsIncorrect = 23010;
            public const long NoWorkData = 23011;
            public const long InitWorkOK = 23012;
            public const long InitWorkError = 23013;
            public const long InitWorkException = 23014;
            public const long AddWorkDataError = 23015;
            public const long UpdateDataError = 23016;

            public const long ExecuteException = 23021;
        }
    }
}