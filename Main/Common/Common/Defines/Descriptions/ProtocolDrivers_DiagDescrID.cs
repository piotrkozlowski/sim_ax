using System;
using System.Text;


namespace IMR.Suite.Common
{
	public partial class DiagDescrID
	{
		public class ProtocolDrivers
		{
            //64501-65000 DIAGNOSTIC for Protocol Drivers
            public const long Debug1 = 64501;

            public const long DuplicatedIdTask = 64502;             //"Duplicated IdTask ";          "  Could not add to process Task({0}) because IdTask still in process. "       
            public const long AssociateIdTaskError = 64503;         //"Associate IdTask Error ";          "  Associate IdTask Error for SN: {0}"       
            public const long WPFrameEncodeError = 64504;           //"WP Frame  Encode Error ";          "  Could not encode frame WP to bytes for Task({0}).[{1}] Remove from process. "       
            public const long WPFrameDecodeEncryptionError = 64505; //"WP Encoded Frame Decode Error ";          "  Could not decode bytes to WP frame whith encryption enabled (keys mismatch). Device Address:{0}. [{1}]."       
            public const long WPFrameDecodeError = 64506;           //"WP Frame Decode Error ";          "  Could not decode bytes to WP frame. Device Address: {0}. [{1}] "       
            public const long IncorrectIVIn = 64507;                //"Incorrect IV In ";          "  Incorrect WP_IV_IN for SN {0}. Last remembered IV value: {1}, Received IV Value {2}!"       
            public const long DifferentObject = 64508;              //"Different Object Received ";          "  Object with offset: {0} in WP frame for Task({1}) is not the same with processing object."       
            public const long DifferentTask = 64509;                //"Different Task Received ";          "  WP frame for Task ({0})  is not the same with processing IdTask. Different count objects ( Expected {1} objects, received: {2})!"       
            public const long DifferentObjectType = 64510;          //"Different Object Type Received ";          "  Wrong object type, expected: {0}, received: {1}. "
        }
	}
}