using System;
using System.Text;


namespace IMR.Suite.Common
{
	public partial class DiagDescrID
	{
		public class DAQRouter
		{
            //60001-60200 DIAGNOSTIC for DAQ ROUTER
            public const long Debug1 = 60001;

            public const long RouteNotFound = 60002;//"Route not found";          "Route not found for SN[{0}] OutAddress[{1},{2}] InAddress[{3},{4}]"
            public const long SerialNbrHasNotIDConnection = 60003;//"Id connection not defined";          "  SN[{0}] has not defined any IdConnection (DEVICE_ID_CONNECTION)"       
            public const long SerialNbrHasNotPhoneNumber = 60004;//"Phone number not defined";          "  SN[{0}] has not defined any PhoneNumber (DEVICE_PHONE_NUMBER)"       
            public const long SerialNbrHasNotAPNIPPort = 60005;//"IP and port not defined";          "  SN[{0}] has not defined any APN IP:Port (DEVICE_APN_IP:DEVICE_APN_PORT)"       
            public const long SerialNbrHasNotRadioAddress = 60006;//"Radio address not defined";          "  SN[{0}] has not defined any RadioAddress (DEVICE_RADIO_ADDRESS)"       
            public const long SerialNbrHasNotDestAddress = 60007;//"Destination address not defined";          "  SN[{0}] has not defined any Destination Address (DEVICE_PHONE_NUMBER, DEVICE_APN_IP:DEVICE_APN_PORT, DEVICE_RADIO_ADDRESS)"       
            public const long UnknowLinkType = 60008;//"Unknown link type";          "  Unknown LinkType[{0}] in Address[{1}] for SN[{2}]"
        }
	}
}