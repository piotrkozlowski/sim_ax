using System;
using System.Text;


namespace IMR.Suite.Common
{
    public partial class DiagDescrID
    {
        public class TransmissionDrivers
        {
            //64001-64500 DIAGNOSTIC for Transission Drivers
            public const long Debug1 = 64001;

            public const long ClientHasExceededInactivityTimeDisconnecting = 64002; //'Inactivity time exceeded','Client {0} has exceeded inactivity time. Connection has been disconnected'
            public const long GotEmptyPacketToSent = 64003;//"Got EMPTY packet to send";          "  Got EMPTY packet to {0}. Packet will not send"       
            public const long GotPacketInvalidAddress = 64004;//"Got packet with Invalid Address";          "  Got packet with Invalid Address: {0}. Packet will not send"       
            public const long GotPacketEncodePacketError = 64005;//"Encode packet error!";          "  Encode packet error! Packet to {0}, CodingScheme: {1}, Body: {2}. Packet will not send"
        }
    }
}