using System;
using System.Text;


namespace IMR.Suite.Common
{
	public partial class DiagDescrID
	{
		public class DAQController
		{
            //60201-60400 DIAGNOSTIC for DAQ CONTROLLER
            public const long Debug1 = 60201;

            public const long DriverReturnedErrorAfterProcessingDataToSend = 60202;//"Driver returned error while processing data to send";          "Device driver with serial number {0} returned error after processing data to send"
            public const long UnexpectedErrorDuringProcessingData = 60203;//"Unexpected error during processing data";          "  Unexpected error during processing data: {0}"
        }
	}
}