﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
    #region class CustomDataTable
    [Serializable]
#if !(Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
#endif
    public class CustomDataTable : ISerializable
    {
        #region Members
        #region Name
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#if (!Android)
        [ProtoBuf.ProtoMember(1, Name = "Name")]
        [Newtonsoft.Json.JsonProperty("Name", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
#endif
        private string name = string.Empty;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion
        #region Columns
#if (!Android)
        [ProtoBuf.ProtoMember(2, Name = "Columns")]
        [Newtonsoft.Json.JsonProperty("Columns")]
#endif
        public string[] Columns
        { get; private set; }
        #region DO NOT USE ! It's seems this is not needed for now! // columnDict - NonSerialized!!!
        /*
        [XmlIgnore]
        [NonSerialized]
        private Dictionary<string,int> columnDict = null;
        */
        #endregion
        #endregion
        #region Rows
#if (!Android)
        [ProtoBuf.ProtoMember(3, Name = "Rows")]
        [Newtonsoft.Json.JsonProperty("Rows")]
#endif
        public List<object[]> Rows { get; private set; }
        #endregion
        #endregion
        #region Constructor
        public CustomDataTable()
        {
            this.Rows = new List<object[]>();
        }
        /// <summary>
        /// Column index should be consistent with value index in row!
        /// </summary>
        /// <param name="name - Table Name"></param>
        /// <param name="columns - Table columns UNIQUE!"></param>
        /// <param name="rows - Element in list - table row, element in object[] - row value (f.ex. colums[3] = 'ID_METER', so ID_METER in row is row[3] "></param>
        public CustomDataTable(string name, string[] columns, List<object[]> rows)
        {
            this.Name = name;
            this.Columns = columns != null ? columns.Distinct(i => i).ToArray() : null;
            this.Rows = new List<object[]>();
            
            if(columns != null && columns.Length > 0 && rows != null && rows.Count > 0 && rows.All(i => i.Length == columns.Length))
                this.Rows = rows;

        }

        public CustomDataTable(string name, string[] columns)
            : this(name, columns, null) { }

        public CustomDataTable(string name)
            : this(name, null, null) { }

        protected CustomDataTable(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected CustomDataTable(SerializationInfo info, StreamingContext context, bool fromDerived)
        {
            Rows = new List<object[]>();
            this.Name = (string)info.GetValue("name", typeof(string));
            this.Columns = (string[])info.GetValue("Columns", typeof(string[]));
            this.Rows = ((object[][])info.TryGetValue("Rows", typeof(object[][]), (new List<object[]>()).ToArray())).ToList();
        }
        #endregion
        #region override ToString()
        public override string ToString()
        {
            string columns = "empty";
            if (Columns != null && columns.Length > 0)
            {
                columns = "";
                Columns.ToList().ForEach(i => columns += string.Format("({0}) ", i));
            }
            return string.Format("Name: {0} Columns: [{1}] Rows.Count {2}",
                name, columns, Rows != null ? Rows.Count.ToString() : "null");
        }
        #endregion
        #region GetObjectData
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", Name, typeof(string));
            info.AddValue("Columns", Columns, typeof(string[]));
            info.AddValue("Rows", Rows.ToArray(), typeof(object[][]));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
        #endregion

        #region AddRow
        public bool AddRow(object[] row)
        {
            if (Columns != null && Columns.Length > 0 && row != null && row.Length == Columns.Length)
            {
                Rows.Add(row);
                return true;
            }
            return false;
        }
        public bool AddRows(List<object[]> rows)
        {
            if (Columns != null && Columns.Length > 0 && rows != null && rows.Count > 0 && rows.All(i => i.Length == Columns.Length))
            {
                this.Rows.AddRange(rows);
                return true;
            }
            return false;
        }
        #endregion
        #region DO NOT USE ! It's seems this is not needed for now!
        /*
        public bool AddRow(Dictionary<string, object> row)
        {
            if (Columns != null && Columns.Length > 0 && row != null )
            {
                object[] newRow = new object[Columns.Length];
                foreach (KeyValuePair<string, object> rowValue in row)
                {
                    int? index = GetColumnIndex(rowValue.Key);
                    if(index == null)
                        return false;
                    newRow[index.Value] = rowValue.Value;
                }
            }
            return false;
        }
        public bool AddRows(List<Dictionary<string, object>> rows)
        {
            if (Columns != null && Columns.Length > 0 && rows != null )
            {
                foreach (Dictionary<string, object> rowValue in rows)
                {
                    if(!AddRow(rowValue))
                        return false;
                }
            }
            return false;
        }
        #endregion
        */
        #region GetRow 
        /*
        Dictionary<string, object> GetRow(int index)
        {
            Dictionary<string, object> row = null;
            if (Rows != null && Rows.Count >= index)
            {
                if (columnDict == null)
                    columnDict = Enumerable.Range(0, Columns.Length).ToDictionary(x => Columns[x], x => x);

                return columnDict.ToDictionary(i => i.Key,i => Rows[index][i.Value]);
            }
            return row;
        }
        List<Dictionary<string, object>> GetRows()
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            if (Rows != null && Rows.Count > 0)
            {
                if (columnDict == null)
                    columnDict = Enumerable.Range(0, Columns.Length).ToDictionary(x => Columns[x], x => x);
                Rows.ForEach(r => rows.Add(columnDict.ToDictionary(i => i.Key,i => r[i.Value])));
            }
            return rows;
        }
        */
        #endregion
        #region GetColumnIndex 
        /*
        public int? GetColumnIndex(string columnName)
        {
            if (Columns != null && Columns.Length > 0)
            {
                if (columnDict == null)
                    columnDict = Enumerable.Range(0, Columns.Length).ToDictionary(x => Columns[x], x => x);
                int idx = 0;
                if (columnDict.TryGetValue(columnName, out idx))
                    return idx;
            }
            return null;

        }
        */
        #endregion
        #region RemoveRow
        //not needed either, it should by as simple as it can - only to send tables between services in MSMQ
        //so we create one and we're sending it to other service, do not need other operations
        #endregion
        #endregion
    }
    #endregion
}
