﻿using System;
using System.Collections.Generic;
using System.Text;
#if !Android
using System.Data;
#endif
using System.Collections;

namespace IMR.Suite.Common
{
    #region LocationEquipment
    [Serializable]
    public class LocationEquipment
    {
        #region Members

        #region IdLocationEquipment
        private long? idLocationEquipment;

        public long? IdLocationEquipment
        {
            get
            {
                return idLocationEquipment;
            }
            set
            {
                idLocationEquipment = value;
            }
        }
        #endregion        

        #region SerialNbr
        private SN serialNbr = null;

        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }
            set
            {
                serialNbr = value;
            }
        }
        #endregion

        #region IdMeter
        private long? idMeter = null;

        public long? IdMeter
        {
            get
            {
                return idMeter;
            }
            set
            {
                idMeter = value;
            }
        }
        #endregion

        #region IdLocation
        private long? idLocation = null;

        public long? IdLocation
        {
            get
            {
                return idLocation;
            }
        }
        #endregion

        #region StartTime
        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        #endregion

        #region EndTime
        private DateTime? endTime = null;
        public DateTime? EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
        #endregion

        #endregion

        #region Constructor
        public LocationEquipment()
        {
        }

        public LocationEquipment(SN SerialNbr, long? IdMeter, long? IdLocation, DateTime StartTime, DateTime? EndTime)
        {
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
            this.startTime = StartTime;
            this.endTime = EndTime;
        }

        public LocationEquipment(long? IdLocationEquipment, SN SerialNbr, long? IdMeter, long? IdLocation, DateTime StartTime, DateTime? EndTime)
        {
            this.idLocationEquipment = IdLocationEquipment;
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
            this.startTime = StartTime;
            this.endTime = EndTime;
        }
        #endregion        
    }
    #endregion LocationEquipment
}
