#define ISERIALIZABLE_TEST

using System;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace IMR.Suite.Common
{
    #region class DataValue
    [Serializable]
#if !WindowsCE
    [TypeConverter(typeof(DataValueConverter))]
#endif
#if ISERIALIZABLE_TEST
#if !(Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [ProtoBuf.ProtoInclude(30, typeof(DataObjectValue))]
    [ProtoBuf.ProtoInclude(35, typeof(DataProcessValue))]
    [ProtoBuf.ProtoInclude(100, typeof(DataCurrentValue))]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
#else
    [XmlInclude(typeof(DataObjectValue))]
    [XmlInclude(typeof(DataProcessValue))]
    [XmlInclude(typeof(DataCurrentValue))]
#endif
    public class DataValue : ISerializable
#else
    public class DataValue
#endif
    {
        #region Members
        #region SerializationVersion
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        protected const byte CURRENT_SERIALIZATION_VERSION = 1;
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(1, Name = "Version")]
        [Newtonsoft.Json.JsonProperty("Version")] 
#endif
        protected byte serializationVersion = CURRENT_SERIALIZATION_VERSION;
        #endregion
        #region Type
#if (!Android)
        [ProtoBuf.ProtoMember(2, Name = "IdDataType")]
        [Newtonsoft.Json.JsonProperty("IdDataType")] 
#endif
        public long IdDataType { get; set; }

        #region IdDataTypeClass for ProtoBuf
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(3, Name = "IdDataTypeClass"), DefaultValue(Enums.DataTypeClass.Unknown)] 
#endif
        private Enums.DataTypeClass ProtoBufIdDataTypeClass;
        #endregion

#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        protected DataType type = null;
        [XmlIgnore]
        public DataType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                if (type != null)
                    IdDataType = type.IdDataType;
            }
        }
        #endregion
        #region Value
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlElement(ElementName = "Value")]
#if (!Android)
        [Newtonsoft.Json.JsonProperty("Value")] 
#endif
        protected object value = null;
        [XmlIgnore]
        public object Value
        {
            get
            {
                return value;
            }
        }

        #region Value for ProtoBuf
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlIgnore]
        [NonSerialized]
#if (!Android)  
        [ProtoBuf.ProtoMember(4, Name = "ValueType")] 
#endif
        private string ProtoBufValueType;
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(5, Name = "Value")] 
#endif
        private string ProtoBufValueStr;
        #endregion
        #endregion
        #region Index
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(6, Name = "Index")]
        [Newtonsoft.Json.JsonProperty("Index")] 
#endif
        protected int index = 0;
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }
        #endregion
        #endregion

        #region Constructor
        public DataValue() { }
        public DataValue(DataType Type, int Index, object Value)
        {
            this.type = Type;
            if (type != null)
                IdDataType = type.IdDataType;
            this.index = Index;
            this.value = Value;
        }

        public DataValue(DataType Type, object Value)
            : this(Type, 0, Value)
        { }

        public DataValue(DataType Type)
            : this(Type, 0, null)
        { }

        public DataValue(long TypeId, int Index, object Value)
            : this(new DataType(TypeId), Index, Value)
        { }

        public DataValue(long TypeId, object Value)
            : this(new DataType(TypeId), 0, Value)
        { }

        public DataValue(long TypeId)
            : this(TypeId, null)
        { }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataValue(SerializationInfo info, StreamingContext context, bool fromDerived)
        {
            //ustalenie wersji obiektu
            if (info.FieldExists("version"))
                serializationVersion = (byte)info.GetValue("version", typeof(byte));
            else
                serializationVersion = 0;

            if (fromDerived == false || serializationVersion > 0)
            {
                index = (int)info.GetValue("index", typeof(int));
                value = (object)info.GetValue("value", typeof(object));
            }
            else
            {
                index = (int)info.GetValue("DataValue+index", typeof(int));
                value = (object)info.GetValue("DataValue+value", typeof(object));
            }

            if (serializationVersion == 0)
            {
                if (fromDerived == false)
                {
                    Type = (DataType)info.GetValue("type", typeof(DataType));
                }
                else
                {
                    Type = (DataType)info.GetValue("DataValue+type", typeof(DataType));
                }
            }
            else if (serializationVersion == 1)
            {
                //                if (fromDerived == false)
                //                {
                IdDataType = (long)info.GetValue("IdDataType", typeof(long));
                //                }
                //                else
                //                {
                //                    IdDataType = (long)info.GetValue("DataValue+IdDataType", typeof(long));
                //                }
                type = IdDataType > 0 ? new DataType(IdDataType) : new DataType(0);
            }
        }
#endif
        #endregion

        #region SetValue
        public void SetValue(object value)
        {
            this.value = value;
        }
        public void SetValue(string strValue)
        {
            this.SetValue(strValue, Type.Class);
        }
        public void SetValue(string strValue, Enums.DataTypeClass dataTypeClass)
        {
            try
            {
                switch (dataTypeClass)
                {
                    case Enums.DataTypeClass.Boolean:
                        value = Convert.ToBoolean(strValue);
                        break;
                    case Enums.DataTypeClass.Date:
                    case Enums.DataTypeClass.Datetime:
                        value = Convert.ToDateTime(strValue);
                        break;
                    case Enums.DataTypeClass.Integer:
                        value = Convert.ToInt64(strValue);
                        break;
                    case Enums.DataTypeClass.Real:
                        //value = Convert.ToDouble(strValue);
                        value = double.Parse(strValue.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                        break;
                    case Enums.DataTypeClass.Decimal:
                        //value = Convert.ToDouble(strValue);
                        value = decimal.Parse(strValue.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                        break;
                    case Enums.DataTypeClass.Text:
                        value = Convert.ToString(strValue);
                        break;
                    default:
                        value = null;
                        break;
                }
            }
            catch (Exception)
            {
                value = null;
            }
        }
        #endregion

        #region Operators
        public static bool operator ==(DataValue left, object right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(DataValue left, object right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        #endregion
        #region override Equals
        public override bool Equals(object obj)
        {
            if (!(obj is DataValue)) 
                return false;

            // First method
            // Niezbyt dziala np dla przypadku:
            // DataValue dt1 = new DataValue(DataType.DEVICE_APN_PORT, 5554);
            // DataValue dt2 = new DataValue(DataType.DEVICE_APN_PORT, 5554);
            // if (dt1 == dt2) i if (dt1.Value == dt2.Value)
            //
            //return type == ((DataValue)obj).type && value == ((DataValue)obj).value;

            // Second method
            if (type != ((DataValue)obj).type)
                return false;

            if (this.index != ((DataValue)obj).index)
                return false;

            if (value == null && ((DataValue)obj).value == null)
                return true;

            if (value == null && ((DataValue)obj).value != null)
                return false;

            if (value != null && ((DataValue)obj).value == null)
                return false;

            try
            {
#if WindowsCE
				return (bool)(value.GetType().InvokeMember("Equals", BindingFlags.InvokeMethod, null, value, new object[] { Convert.ChangeType(((DataValue)obj).value, value.GetType(), null) }));
#else
                return (bool)(value.GetType().InvokeMember("Equals", BindingFlags.InvokeMethod, null, value, new object[] { Convert.ChangeType(((DataValue)obj).value, value.GetType()) }));
#endif
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        #region override GetHashCode
        public override int GetHashCode()
        {
            #region Old - nie dziala z hashem od 'Value'
            /*
			int hashType = type.GetHashCode();
			int hashValue = value == null ? 0 : value.GetHashCode();

			return hashType ^ hashValue;
			*/
            #endregion
            if (type == null)
                return base.GetHashCode();
            return type.GetHashCode() ^ index.GetHashCode();	// HasCode z Type wystarczy bo i tak jest unikalny w naszym rozwiazaniu
        }
        #endregion
        #region override ToString
        public override string ToString()
        {
            return String.Format("{0}{1} - {2}", type.IdDataType, string.IsNullOrEmpty(type.Name) ? "" : "[" + type.Name + "]", value == null ? "null" : value);
        }
        #endregion

        #region CompareTo
        public int CompareTo(object obj)
        {
            if (value == null && obj == null)
                return 0;

            if (value == null && obj != null)
                return 1;

            if (obj is DataValue)
            {
                obj = ((DataValue)obj).value;
                if (obj == null) return 1;
            }

            try
            {
#if WindowsCE
				return (int)value.GetType().InvokeMember("CompareTo", BindingFlags.InvokeMethod, null, value, new object[] { Convert.ChangeType(obj, value.GetType(), null) });
#else
                return (int)value.GetType().InvokeMember("CompareTo", BindingFlags.InvokeMethod, null, value, new object[] { Convert.ChangeType(obj, value.GetType()) });
#endif
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("IdDataType", type != null ? type.IdDataType : IdDataType, typeof(long));
            info.AddValue("index", index, typeof(int));
            info.AddValue("value", value, typeof(object));
            info.AddValue("version", CURRENT_SERIALIZATION_VERSION, typeof(byte));
        }


        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
#endif
        #endregion

        #region Prototbuf method for serialization
        #region OnProtobufBeforeSerialization
#if (!Android)
        [ProtoBuf.ProtoBeforeSerialization] 
#endif
        private void OnProtobufBeforeSerialization()
        {
            if (!string.IsNullOrEmpty(ProtoBufValueStr))
                return; // nie wiem czemu, ale czase, przy Deserializaci wchodzi tutaj !!! Mo�e b�ad w protobuf-net? Og�lnie nullowa� ProtoBufValueStr, co skutowa�o, �e w ostateczno�ci Value zawsze by�o null

            if (value == null)
            {
                ProtoBufValueStr = "null";
            }
            else
            {
                ProtoBufValueType = value.GetType().FullName;
                ProtoBufValueStr = value.ToString();
            }
            ProtoBufIdDataTypeClass = Enums.DataTypeClass.Unknown;
            if (type != null)
                ProtoBufIdDataTypeClass = type.Class;
        }
        #endregion
        #region OnProrobufAfterDeserialization
#if (!Android)
        [ProtoBuf.ProtoAfterDeserialization] 
#endif
        private void OnProrobufAfterDeserialization()
        {
            value = null;

            if (!string.IsNullOrEmpty(ProtoBufValueStr) && ProtoBufValueStr.ToLower() != "null")
            {
                Type typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.FullName == ProtoBufValueType);
                if (typeFromProtobuf == null)
                {
                    typeFromProtobuf = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).FirstOrDefault(x => x.Name == ProtoBufValueType);
                    if (typeFromProtobuf == null)
                    {
                        if (ProtoBufIdDataTypeClass != Enums.DataTypeClass.Unknown)
                        {
                            SetValue(ProtoBufValueStr, ProtoBufIdDataTypeClass);
                            return;
                        }
                    }
                }
                try
                {
                    System.TypeCode typeCode = System.Type.GetTypeCode(typeFromProtobuf);
                    switch (typeCode)
                    {
                        case TypeCode.Double:
                            value = Double.Parse(ProtoBufValueStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                            break;
                        case TypeCode.Single:
                            value = Single.Parse(ProtoBufValueStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                            break;
                        case TypeCode.Decimal:
                            value = Decimal.Parse(ProtoBufValueStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                            break;

                        //Inne case?? by� moze
                        default:
                            value = Convert.ChangeType(ProtoBufValueStr, typeFromProtobuf, CultureInfo.InvariantCulture);
                            break;
                    }
                }
                catch (Exception ex) { throw new FormatException("Parse value Exception in ProtoBuf deserialization", ex); }
            }
        }
        #endregion
        #endregion
    }
    #region DataValueConverter
#if !WindowsCE
    internal class DataValueConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destType)
        {
            if (destType == typeof(string) && value is DataValue)
            {
                // Cast the value to an DataValue type
                DataValue obj = (DataValue)value;

                return obj.ToString();
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }
#endif
    #endregion
    #endregion
    #region class DataCurrentValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [ProtoBuf.ProtoInclude(120, typeof(DataArchValue))]
    [ProtoBuf.ProtoInclude(140, typeof(DataTemporalValue))]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]

#endif
    [XmlInclude(typeof(DataArchValue))]
    [XmlInclude(typeof(DataTemporalValue))]
    public class DataCurrentValue : DataValue
    {
        #region Members
        #region IdData
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(101, Name = "IdData")]
        [Newtonsoft.Json.JsonProperty("IdData", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idData = null;
        public long? IdData
        {
            get { return idData; }
        }
        #endregion

        #region SerialNbr
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [Newtonsoft.Json.JsonProperty("SerialNbr")] 
#endif
        private SN serialNbr = null;
        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }
        }

        #region SerialNbr for ProtoBuf
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(102, Name = "SerialNbr")] 
#endif
        private ulong? ProtoBufSerialNbr;
        #endregion
        #endregion
        #region IdMeter
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(103, Name = "IdMeter")]
        [Newtonsoft.Json.JsonProperty("IdMeter", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idMeter = null;
        public long? IdMeter
        {
            get
            {
                return idMeter;
            }
        }
        #endregion
        #region IdLocation
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(104, Name = "IdLocation")]
        [Newtonsoft.Json.JsonProperty("IdLocation", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idLocation = null;
        public long? IdLocation
        {
            get
            {
                return idLocation;
            }
        }
        #endregion

        #region Time
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(105, Name = "Time")]
        [Newtonsoft.Json.JsonProperty("Time")] 
#endif
        protected DateTime time = DateTime.MinValue;
        public DateTime Time
        {
            get { return time; }
        }
        #endregion
        #region Unit
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private string unit = "";
        public string Unit
        {
            get { return unit; }
        }
        #endregion
        #region Status
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(107, Name = "Status")]
        [Newtonsoft.Json.JsonProperty("Status")] 
#endif
        private Enums.DataStatus status = Enums.DataStatus.Unknown;
        public Enums.DataStatus Status
        {
            get { return status; }
        }
        #endregion

        #region IdDataSource
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(108, Name = "IdDataSource")]
        [Newtonsoft.Json.JsonProperty("IdDataSource", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idDataSource = null;
        public long? IdDataSource
        {
            get { return idDataSource; }
            set { idDataSource = value; }
        }
        #endregion
        #region SourceType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(109, Name = "SourceType")]
        [Newtonsoft.Json.JsonProperty("SourceType")] 
#endif
        private Enums.DataSourceType sourceType = Enums.DataSourceType.Unknown;
        public Enums.DataSourceType SourceType
        {
            get { return sourceType; }
        }
        #endregion
        #endregion

        #region Constructor
        public DataCurrentValue() { }
        public DataCurrentValue(long? IdData, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value, DateTime Time, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId)
            : base(TypeId, Index, Value)
        {
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;

            this.idData = IdData;

            this.time = Time;
            this.unit = Unit;
            // jb: zmienilem ponizsze, bo chcemy tez statusy nie zdefiniowane w Enums
            //if (StatusId.HasValue && Enum.IsDefined(typeof(Enums.DataStatus), StatusId))
            if (StatusId.HasValue)
                this.status = (Enums.DataStatus)StatusId;
            else
                this.status = Enums.DataStatus.Unknown;

            this.idDataSource = IdDataSource;
            if (SourceTypeId.HasValue && Enum.IsDefined(typeof(Enums.DataSourceType), SourceTypeId))
                this.sourceType = (Enums.DataSourceType)SourceTypeId;
            else
                this.sourceType = Enums.DataSourceType.Unknown;
        }

        public DataCurrentValue(long? IdData, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, object Value, DateTime Time, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId)
            : this(null, SerialNbr, IdMeter, IdLocation, TypeId, 0, Value, Time, Unit, StatusId, IdDataSource, SourceTypeId)
        { }
        public DataCurrentValue(long? IdData, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value)
            : this(IdData, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, DateTime.MinValue, "", null, null, null)
        { }
        public DataCurrentValue(SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, object Value)
            : this(null, SerialNbr, IdMeter, IdLocation, TypeId, 0, Value, DateTime.MinValue, "", null, null, null)
        { }
        public DataCurrentValue(SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value)
            : this(null, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, DateTime.MinValue, "", null, null, null)
        { }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataCurrentValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataCurrentValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
            {
                idData = (long?)info.GetValue("idData", typeof(long?));
                idMeter = (long?)info.GetValue("idMeter", typeof(long?));
                idLocation = (long?)info.GetValue("idLocation", typeof(long?));
                time = (DateTime)info.GetValue("time", typeof(DateTime));
                unit = (string)info.GetValue("unit", typeof(string));
                status = (Enums.DataStatus)info.GetValue("status", typeof(long));
                idDataSource = (long?)info.GetValue("idDataSource", typeof(long?));
                sourceType = (Enums.DataSourceType)info.GetValue("sourceType", typeof(int));
            }
            else
            {
                idData = (long?)info.GetValue("DataCurrentValue+idData", typeof(long?));
                idMeter = (long?)info.GetValue("DataCurrentValue+idMeter", typeof(long?));
                idLocation = (long?)info.GetValue("DataCurrentValue+idLocation", typeof(long?));
                time = (DateTime)info.GetValue("DataCurrentValue+time", typeof(DateTime));
                unit = (string)info.GetValue("DataCurrentValue+unit", typeof(string));
                status = (Enums.DataStatus)info.GetValue("DataCurrentValue+status", typeof(long));
                idDataSource = (long?)info.GetValue("DataCurrentValue+idDataSource", typeof(long?));
                sourceType = (Enums.DataSourceType)info.GetValue("DataCurrentValue+sourceType", typeof(int));
            }

            if (serializationVersion == 0)
            {
                if (fromDerived == false)
                {
                    serialNbr = (SN)info.GetValue("serialNbr", typeof(SN));
                }
                else
                {
                    serialNbr = (SN)info.GetValue("DataCurrentValue+serialNbr", typeof(SN));
                }
            }
            else if (serializationVersion == 1)
            {
                ulong? snDBValue = null;
                //                if (fromDerived == false)
                //                {
                snDBValue = (ulong?)info.GetValue("serialNbr", typeof(ulong?));
                //                }
                //                else
                //                {
                //                    snDBValue = (ulong?)info.GetValue("DataCurrentValue+serialNbr", typeof(ulong?));
                //                }

                if (snDBValue != null)
                    serialNbr = SN.FromDBValue(snDBValue);
            }
        }
#endif
        #endregion

        #region IsStatusProper
        public bool IsStatusProper()
        {
            return ((int)status & 1) == 1;
        }
        #endregion
        #region SetTime
        public void SetTime(DateTime Time)
        {
            this.time = Time;
        }
        #endregion
        #region SetStatus
        public void SetStatus(Enums.DataStatus NewStatus)
        {
            this.status = NewStatus;
        }
        #endregion
        #region SetIdDataSource
        public void SetIdDataSource(long IdDataSource)
        {
            this.idDataSource = IdDataSource;
        }
        #endregion
        #region TimeToLocal
#if !WindowsCE
        public void TimeToLocal(int LocalTimeOffset)
        {
            TimeToLocal(null, LocalTimeOffset);
        }
        public void TimeToLocal(string TimeZoneId)
        {
            TimeToLocal(TimeZoneId, 0);
        }
        public void TimeToLocal(string TimeZoneId, int LocalTimeOffset)
        {
            time = time.ToLocalTime(TimeZoneId, LocalTimeOffset);
        }
#endif
        public void TimeToLocal()
        {
            time = time.ToLocalTime();
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (!(obj is DataCurrentValue)) return false;

            if (serialNbr != ((DataCurrentValue)obj).serialNbr)
                return false;

            if (this.idMeter != ((DataCurrentValue)obj).idMeter)
                return false;

            if (this.idLocation != ((DataCurrentValue)obj).idLocation)
                return false;

            if (this.time != ((DataCurrentValue)obj).time)
                return false;

            return base.Equals(obj);
        }
        #endregion
        #region override GetHashCode
        public override int GetHashCode()
        {
            int hash = 0;
            if (serialNbr != null)
                hash ^= serialNbr.GetHashCode();
            if (idMeter.HasValue)
                hash ^= idMeter.GetHashCode();
            if (idLocation.HasValue)
                hash ^= idLocation.GetHashCode();
            hash ^= time.GetHashCode();
            hash ^= base.GetHashCode();
            return hash;
        }
        #endregion
        #region override ToString
        public override string ToString()
        {
            return String.Format("SN: {0}, IdMeter: {1}, IdLocation {2}, {3}", serialNbr, idMeter, idLocation, base.ToString());
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("idData", idData, typeof(long?));
            info.AddValue("serialNbr", serialNbr != null ? serialNbr.DBValue : null, typeof(ulong?));
            info.AddValue("idMeter", idMeter, typeof(long?));
            info.AddValue("idLocation", idLocation, typeof(long?));
            info.AddValue("time", time, typeof(DateTime));
            info.AddValue("unit", unit, typeof(string));
            info.AddValue("status", (long)status, typeof(long));
            info.AddValue("idDataSource", idDataSource, typeof(long?));
            info.AddValue("sourceType", (int)sourceType, typeof(int));
        }
#endif
        #endregion

        #region Prototbuf method for serialization
        #region OnProtobufBeforeSerialization
#if (!Android)
        [ProtoBuf.ProtoBeforeSerialization] 
#endif
        private void OnProtobufBeforeSerialization()
        {
            if (SerialNbr != null)
                ProtoBufSerialNbr = SerialNbr.DBValue;
        }
        #endregion
        #region OnProrobufAfterDeserialization
#if (!Android)
        [ProtoBuf.ProtoAfterDeserialization] 
#endif
        private void OnProrobufAfterDeserialization()
        {
            serialNbr = null;
            if (ProtoBufSerialNbr.HasValue && ProtoBufSerialNbr.Value != 0)
                serialNbr = SN.FromDBValue(ProtoBufSerialNbr.Value);
        }
        #endregion
        #endregion
    }
    #endregion
    #region class DataArchValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
#endif
    public class DataArchValue : DataCurrentValue
    {
        #region Members
        #region IdDataArch
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(121, Name = "IdDataArch")]
        [Newtonsoft.Json.JsonProperty("IdDataArch", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idDataArch = null;
        public long? IdDataArch
        {
            get { return idDataArch; }
        }
        #endregion
        #region IdAlarmEvent
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(122, Name = "IdAlarmEvent")]
        [Newtonsoft.Json.JsonProperty("IdAlarmEvent", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idAlarmEvent = null;
        public long? IdAlarmEvent
        {
            get { return idAlarmEvent; }
        }
        #endregion

        #region IdPacket
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(123, Name = "IdPacket")]
        [Newtonsoft.Json.JsonProperty("IdPacket", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private ulong? idPacket = null;
        public ulong? IdPacket
        {
            get { return idPacket; }
            set { idPacket = value; }
        }
        #endregion
        #region IsAction
#if (!Android)
        [ProtoBuf.ProtoMember(124, Name = "IsAction")]
        [Newtonsoft.Json.JsonProperty("IsAction")] 
#endif
        public bool IsAction { get; set; }
        #endregion
        #region IsAlarm
#if (!Android)
        [ProtoBuf.ProtoMember(125, Name = "IsAlarm")]
        [Newtonsoft.Json.JsonProperty("IsAlarm")] 
#endif
        public bool IsAlarm { get; set; }
        #endregion
        #endregion

        #region Constructor
        public DataArchValue() { }
        public DataArchValue(long? IdDataArch, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value, DateTime Time, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent, ulong? IdPacket, bool isAction, bool isAlarm)
            : base(null, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, Time, Unit, StatusId, IdDataSource, SourceTypeId)
        {
            this.idDataArch = IdDataArch;
            this.idAlarmEvent = IdAlarmEvent;
            this.idPacket = IdPacket;
            this.IsAction = isAction;
            this.IsAlarm = isAlarm;
        }
        public DataArchValue(long? IdDataArch, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value, DateTime Time, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent)
            : this(IdDataArch, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, Time, Unit, StatusId, IdDataSource, SourceTypeId, IdAlarmEvent, null, true, true)
        {
        }
        public DataArchValue(long? IdDataArch, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, object Value, DateTime Time, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent)
            : this(IdDataArch, SerialNbr, IdMeter, IdLocation, TypeId, 0, Value, Time, Unit, StatusId, IdDataSource, SourceTypeId, IdAlarmEvent, null, true, true)
        {
        }

        public DataArchValue(DataMeasureValue DataMeasure)
            : this(null, DataMeasure.SerialNbr, null, null, DataMeasure.Type.IdDataType, DataMeasure.Index, DataMeasure.Value, (DateTime)DataMeasure.Time, "", (long?)Enums.DataStatus.Normal, null, (int?)Enums.DataSourceType.OKO, null, DataMeasure.IdPacket, DataMeasure.IsAction, DataMeasure.IsAlarm)
        {
        }

        public DataArchValue(DataMeasureValue DataMeasure, long IdPacket)
            : this(null, DataMeasure.SerialNbr, null, null, DataMeasure.Type.IdDataType, DataMeasure.Index, DataMeasure.Value, (DateTime)DataMeasure.Time, "", (long?)Enums.DataStatus.Normal, IdPacket, (int?)Enums.DataSourceType.OKO, null, DataMeasure.IdPacket, DataMeasure.IsAction, DataMeasure.IsAlarm)
        {
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataArchValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataArchValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
            {
                idDataArch = (long?)info.GetValue("idDataArch", typeof(long?));
                idAlarmEvent = (long?)info.GetValue("idAlarmEvent", typeof(long?));
                idPacket = (ulong?)info.GetValue("idPacket", typeof(ulong?));
            }
            else
            {
                idDataArch = (long?)info.GetValue("DataArchValue+idDataArch", typeof(long?));
                idAlarmEvent = (long?)info.GetValue("DataArchValue+idAlarmEvent", typeof(long?));
                idPacket = (ulong?)info.GetValue("DataArchValue+idPacket", typeof(ulong?));
            }

            if (serializationVersion == 0)
            {
                if (fromDerived == false)
                {
                    IsAction = (bool)info.GetValue("<IsAction>k__BackingField", typeof(bool));
                    IsAlarm = (bool)info.GetValue("<IsAlarm>k__BackingField", typeof(bool));
                }
                else
                {
                    IsAction = (bool)info.GetValue("DataArchValue+<IsAction>k__BackingField", typeof(bool));
                    IsAlarm = (bool)info.GetValue("DataArchValue+<IsAlarm>k__BackingField", typeof(bool));
                }
            }
            else if (serializationVersion == 1)
            {
                //                if (fromDerived == false)
                //                {
                IsAction = (bool)info.GetValue("IsAction", typeof(bool));
                IsAlarm = (bool)info.GetValue("IsAlarm", typeof(bool));
                //                }
                //                else
                //                {
                //                    IsAction = (bool)info.GetValue("DataArchValue+IsAction", typeof(bool));
                //                    IsAlarm = (bool)info.GetValue("DataArchValue+IsAlarm", typeof(bool));
                //                }
            }
        }
#endif
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("idDataArch", idDataArch, typeof(long?));
            info.AddValue("idAlarmEvent", idAlarmEvent, typeof(long?));
            info.AddValue("idPacket", idPacket, typeof(ulong?));
            info.AddValue("IsAction", IsAction, typeof(bool));
            info.AddValue("IsAlarm", IsAlarm, typeof(bool));
        }
#endif
        #endregion
    }
    #endregion
    #region class DataProcessValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    [ProtoBuf.ProtoInclude(50, typeof(DataMeasureValue))] 
#endif
    [XmlInclude(typeof(DataMeasureValue))]
    public class DataProcessValue : DataValue
    {
        #region ProcessType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(36, Name = "ProcessType")]
        [Newtonsoft.Json.JsonProperty("ProcessType")] 
#endif
        private Enums.ProcessType processType = Enums.ProcessType.Unknown;
        public Enums.ProcessType ProcessType
        {
            get
            {
                return processType;
            }
            set
            {
                processType = value;
            }
        }
        #endregion

        #region Constructor
        public DataProcessValue() { }
        /// <summary>
        /// Creates DataProcessValue for DAQ.
        /// </summary>
        /// <param name="Status"></param>
        public DataProcessValue(DataType DataType, object Value, Enums.ProcessType ProcessType)
            : base(DataType, 0, Value)
        {
            this.processType = ProcessType;
        }

        /// <summary>
        /// Creates DataMeasureValue for CORE.
        /// </summary>
        /// <param name="DataType"></param>
        /// <param name="Value"></param>
        /// <param name="Index"></param>
        public DataProcessValue(DataType DataType, int Index, object Value, Enums.ProcessType ProcessType)
            : base(DataType, Index, Value)
        {
            this.processType = ProcessType;
        }
        public DataProcessValue(DataType DataType, int Index, object Value)
            : this(DataType, Index, Value, Enums.ProcessType.Unknown)
        {
        }
        public DataProcessValue(long TypeId, int Index, object Value, Enums.ProcessType ProcessType)
            : this(new DataType(TypeId), Index, Value, ProcessType)
        { }
        public DataProcessValue(long TypeId, int Index, object Value)
            : this(new DataType(TypeId), Index, Value)
        { }
        public DataProcessValue(DataType DataType, object Value)
            : this(DataType, 0, Value)
        { }
        public DataProcessValue(long TypeId, object Value, Enums.ProcessType ProcessType)
            : this(new DataType(TypeId), 0, Value, ProcessType)
        { }
        public DataProcessValue(long TypeId, object Value)
            : this(new DataType(TypeId), 0, Value)
        { }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataProcessValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataProcessValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
                ProcessType = (Enums.ProcessType)info.GetValue("processType", typeof(int));
            else
                ProcessType = (Enums.ProcessType)info.GetValue("DataProcessValue+processType", typeof(int));
        }
#endif
        #endregion

        #region CompareByIndex
        public static int CompareByIndex(DataMeasureValue x, DataMeasureValue y)
        {
            return x.Index.CompareTo(y.Index);
        }
        #endregion

        #region override ToString
        public override string ToString()
        {
            return string.Format("({0}[{2}] - {3}) {1}[{2}] - {3} ({4}) | process type:{5}", type.IdDataType, DataType.GetName(type.IdDataType), index, value, value.GetType().Name, ProcessType.ToString());
        }
        #endregion
        #region Clone
        public DataProcessValue Clone()
        {
            DataProcessValue result = new DataProcessValue(this.Type, this.Index, this.Value, this.ProcessType);
            return result;
        }
        #endregion
        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("processType", (int)ProcessType, typeof(int));
        }
#endif
        #endregion
    }
    #endregion
    #region class DataMeasureValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
#endif
    public class DataMeasureValue : DataProcessValue
    {
        #region Id
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(51, Name = "Id")]
        [Newtonsoft.Json.JsonProperty("Id", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? id = null;
        [XmlIgnore]
        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        #endregion

        #region SerialNbr
#if (!Android)
        [Newtonsoft.Json.JsonProperty("SerialNbr")] 
#endif
        public SN SerialNbr { get; set; }

        #region SerialNbr for ProtoBuf
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        [XmlIgnore]
        [NonSerialized]
#if (!Android)
        [ProtoBuf.ProtoMember(52, Name = "SerialNbr")] 
#endif
        private ulong? ProtoBufSerialNbr;
        #endregion
        #endregion

        #region IdPacket
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(53, Name = "IdPacket")]
        [Newtonsoft.Json.JsonProperty("IdPacket", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private ulong? idPacket = null;
        public ulong? IdPacket
        {
            get { return idPacket; }
            set { idPacket = value; }
        }
        #endregion
        #region Time
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(54, Name = "Time")]
        [Newtonsoft.Json.JsonProperty("Time")] 
#endif
        private DateTime? time = null;
        public DateTime? Time
        {
            get { return time; }
            set { time = value; }
        }
        #endregion

        #region IsAction
#if (!Android)
        [ProtoBuf.ProtoMember(55, Name = "IsAction")]
        [Newtonsoft.Json.JsonProperty("IsAction")] 
#endif
        public bool IsAction { get; set; }
        #endregion
        #region IsAlarm
#if (!Android)
        [ProtoBuf.ProtoMember(56, Name = "IsAlarm")]
        [Newtonsoft.Json.JsonProperty("IsAlarm")] 
#endif
        public bool IsAlarm { get; set; }
        #endregion

        #region MeasureValueStatus
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(57, Name = "Status")] 
#endif
        private Enums.MeasureValueStatus status = Enums.MeasureValueStatus.Unknown;
        public Enums.MeasureValueStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        #endregion

        #region Constructor
        public DataMeasureValue() { }
        /// <summary>
        /// Creates DataMeasureValue for DAQ.
        /// </summary>
        /// <param name="SerialNbr"></param>
        /// <param name="DataType"></param>
        /// <param name="Value"></param>
        /// <param name="Time"></param>
        /// <param name="IsAction"></param>
        /// <param name="IsAlarm"></param>
        /// <param name="Status"></param>
        public DataMeasureValue(SN SerialNbr, DataType DataType, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : base(DataType, Value)
        {
            this.SerialNbr = SerialNbr;
            this.time = Time;
            this.IsAction = IsAction;
            this.IsAlarm = IsAlarm;
            this.status = Status;
        }
        public DataMeasureValue(DataType DataType, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(null, DataType, Value, Time, IsAction, IsAlarm, Status)
        { }
        public DataMeasureValue(SN SerialNbr, long TypeId, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(SerialNbr, new DataType(TypeId), Value, Time, IsAction, IsAlarm, Status)
        { }
        public DataMeasureValue(long TypeId, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(new DataType(TypeId), Value, Time, IsAction, IsAlarm, Status)
        { }

        /// <summary>
        /// Creates DataMeasureValue for DAQ with SlotNbr.
        /// </summary>
        /// <param name="SerialNbr"></param>
        /// <param name="DataType"></param>
        /// <param name="Value"></param>
        /// <param name="Index"></param>
        /// <param name="Time"></param>
        /// <param name="IsAction"></param>
        /// <param name="IsAlarm"></param>
        /// <param name="Status"></param>
        public DataMeasureValue(SN SerialNbr, DataType DataType, int Index, object Value, DateTime? Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : base(DataType, Index, Value)
        {
            this.SerialNbr = SerialNbr;
            this.time = Time;
            this.IsAction = IsAction;
            this.IsAlarm = IsAlarm;
            this.status = Status;
        }
        public DataMeasureValue(DataType DataType, int Index, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(null, DataType, Index, Value, Time, IsAction, IsAlarm, Status)
        { }
        public DataMeasureValue(SN SerialNbr, long TypeId, int Index, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(SerialNbr, new DataType(TypeId), Index, Value, Time, IsAction, IsAlarm, Status)
        { }
        public DataMeasureValue(long TypeId, int Index, object Value, DateTime Time, bool IsAction, bool IsAlarm, Enums.MeasureValueStatus Status)
            : this(new DataType(TypeId), Index, Value, Time, IsAction, IsAlarm, Status)
        { }

        /// <summary>
        /// Creates DataMeasureValue for CORE.
        /// </summary>
        /// <param name="SerialNbr"></param>
        /// <param name="DataType"></param>
        /// <param name="Value"></param>
        /// <param name="Index"></param>
        public DataMeasureValue(SN SerialNbr, DataType DataType, int Index, object Value)
            : base(DataType, Index, Value)
        {
            this.SerialNbr = SerialNbr;
            this.time = null;
            this.IsAction = true;
            this.IsAlarm = true;
            this.status = Enums.MeasureValueStatus.Unknown;
        }
        public DataMeasureValue(DataType DataType, int Index, object Value)
            : this(null, DataType, Index, Value)
        { }
        public DataMeasureValue(SN SerialNbr, long TypeId, int Index, object Value)
            : this(SerialNbr, new DataType(TypeId), Index, Value)
        { }
        public DataMeasureValue(long TypeId, int Index, object Value)
            : this(new DataType(TypeId), Index, Value)
        { }
        public DataMeasureValue(SN SerialNbr, DataType DataType, object Value)
            : this(SerialNbr, DataType, 0, Value)
        { }
        public DataMeasureValue(DataType DataType, object Value)
            : this(DataType, 0, Value)
        { }
        public DataMeasureValue(SN SerialNbr, long TypeId, object Value)
            : this(SerialNbr, new DataType(TypeId), 0, Value)
        { }
        public DataMeasureValue(long TypeId, object Value)
            : this(new DataType(TypeId), 0, Value)
        { }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataMeasureValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataMeasureValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
            {
                id = (long?)info.GetValue("id", typeof(long?));
                idPacket = (ulong?)info.GetValue("idPacket", typeof(ulong?));
                time = (DateTime?)info.GetValue("time", typeof(DateTime?));
                status = (Enums.MeasureValueStatus)info.GetValue("status", typeof(int));
            }
            else
            {
                id = (long?)info.GetValue("DataMeasureValue+id", typeof(long?));
                idPacket = (ulong?)info.GetValue("DataMeasureValue+idPacket", typeof(ulong?));
                time = (DateTime?)info.GetValue("DataMeasureValue+time", typeof(DateTime?));
                status = (Enums.MeasureValueStatus)info.GetValue("DataMeasureValue+status", typeof(int));
            }

            if (serializationVersion == 0)
            {
                if (fromDerived == false)
                {
                    IsAction = (bool)info.GetValue("<IsAction>k__BackingField", typeof(bool));
                    IsAlarm = (bool)info.GetValue("<IsAlarm>k__BackingField", typeof(bool));
                    SerialNbr = (SN)info.GetValue("<SerialNbr>k__BackingField", typeof(SN));
                }
                else
                {
                    IsAction = (bool)info.GetValue("DataMeasureValue+<IsAction>k__BackingField", typeof(bool));
                    IsAlarm = (bool)info.GetValue("DataMeasureValue+<IsAlarm>k__BackingField", typeof(bool));
                    SerialNbr = (SN)info.GetValue("DataMeasureValue+<SerialNbr>k__BackingField", typeof(SN));
                }
            }
            else if (serializationVersion == 1)
            {
                ulong? snDBValue = null;
                //                if (fromDerived == false)
                //                {
                IsAction = (bool)info.GetValue("IsAction", typeof(bool));
                IsAlarm = (bool)info.GetValue("IsAlarm", typeof(bool));
                snDBValue = (ulong?)info.GetValue("SerialNbr", typeof(ulong?));
                //                }
                //                else
                //                {
                //                    IsAction = (bool)info.GetValue("DataMeasureValue+IsAction", typeof(bool));
                //                    IsAlarm = (bool)info.GetValue("DataMeasureValue+IsAlarm", typeof(bool));
                //                    snDBValue = (ulong?)info.GetValue("DataMeasureValue+SerialNbr", typeof(ulong?));
                //                }

                if (snDBValue != null)
                    SerialNbr = SN.FromDBValue(snDBValue);
            }
        }
#endif
        #endregion

        #region CompareByIndex
        public static int CompareByIndex(DataMeasureValue x, DataMeasureValue y)
        {
            return x.Index.CompareTo(y.Index);
        }
        #endregion

        #region override ToString
        public override string ToString()
        {
            return string.Format("({0}[{2}] - {3}) {1}[{2}] - {3} ({4}) | status:{5} timestamp:{6} idPacket:{7} processType:{8}", type.IdDataType, DataType.GetName(type.IdDataType), index, value, value.GetType().Name, status, time, idPacket, ProcessType);
        }
        #endregion
        #region Clone
        public DataMeasureValue Clone()
        {
            DataMeasureValue result = new DataMeasureValue(this.SerialNbr, this.Type, this.Index, this.Value, this.Time, this.IsAction, this.IsAlarm, this.Status);
            result.IdPacket = this.IdPacket;

            return result;
        }
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("id", id, typeof(long?));
            info.AddValue("idPacket", idPacket, typeof(ulong?));
            info.AddValue("IsAction", IsAction, typeof(bool));
            info.AddValue("IsAlarm", IsAlarm, typeof(bool));
            info.AddValue("SerialNbr", SerialNbr != null ? SerialNbr.DBValue : null, typeof(ulong?));
            info.AddValue("status", (int)status, typeof(int));
            info.AddValue("time", time, typeof(DateTime?));
        }
#endif
        #endregion

        #region Prototbuf method for serialization
        #region OnProtobufBeforeSerialization
#if (!Android)
        [ProtoBuf.ProtoBeforeSerialization] 
#endif
        private void OnProtobufBeforeSerialization()
        {
            if (SerialNbr != null)
                ProtoBufSerialNbr = SerialNbr.DBValue;
        }
        #endregion
        #region OnProrobufAfterDeserialization
#if (!Android)
        [ProtoBuf.ProtoAfterDeserialization] 
#endif
        private void OnProrobufAfterDeserialization()
        {
            SerialNbr = null;
            if (ProtoBufSerialNbr.HasValue && ProtoBufSerialNbr.Value != 0)
                SerialNbr = SN.FromDBValue(ProtoBufSerialNbr.Value);
        }
        #endregion
        #endregion
    }
    #endregion
    #region class DataTemporalValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
#endif
    public class DataTemporalValue : DataCurrentValue
    {
        #region Members
        #region IdDataTemporal
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(141, Name = "IdDataTemporal")]
        [Newtonsoft.Json.JsonProperty("IdDataTemporal", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idDataTemporal = null;
        public long? IdDataTemporal
        {
            get { return idDataTemporal; }
        }
        #endregion
        #region IdAlarmEvent
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(142, Name = "IdAlarmEvent")]
        [Newtonsoft.Json.JsonProperty("IdAlarmEvent", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idAlarmEvent = null;
        public long? IdAlarmEvent
        {
            get { return idAlarmEvent; }
        }
        #endregion

        #region StartTime
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(143, Name = "StartTime")]
        [Newtonsoft.Json.JsonProperty("StartTime")] 
#endif
        protected DateTime startTime = DateTime.MinValue;
        public DateTime StartTime
        {
            get { return startTime; }
        }
        #endregion
        #region EndTime
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(144, Name = "EndTime")]
        [Newtonsoft.Json.JsonProperty("EndTime")] 
#endif
        protected DateTime endTime = DateTime.MinValue;
        public DateTime EndTime
        {
            get { return endTime; }
        }
        #endregion

        #region AggregationType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(145, Name = "AggregationType")]
        [Newtonsoft.Json.JsonProperty("AggregationType")] 
#endif
        protected Enums.AggregationType aggregationType = Enums.AggregationType.Unknown;
        public Enums.AggregationType AggregationType
        {
            get { return aggregationType; }
        }
        #endregion
        #endregion

        #region Constructor
        public DataTemporalValue() { }
        public DataTemporalValue(long? IdDataTemporal, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value, DateTime? StartTime, DateTime? EndTime, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent)
            : base(null, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, DateTime.MinValue, Unit, StatusId, IdDataSource, SourceTypeId)
        {
            this.idDataTemporal = IdDataTemporal;
            this.idAlarmEvent = IdAlarmEvent;
            this.startTime = StartTime ?? DateTime.MinValue;
            this.endTime = EndTime ?? DateTime.MinValue;
        }
        public DataTemporalValue(long? IdDataTemporal, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, int Index, object Value, DateTime? StartTime, DateTime? EndTime, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent, Enums.AggregationType AggregationType)
            : this(IdDataTemporal, SerialNbr, IdMeter, IdLocation, TypeId, Index, Value, StartTime, EndTime, Unit, StatusId, IdDataSource, SourceTypeId, IdAlarmEvent)
        {
            this.aggregationType = AggregationType;
        }
        public DataTemporalValue(long? IdDataTemporal, SN SerialNbr, long? IdMeter, long? IdLocation, long TypeId, object Value, DateTime StartTime, DateTime EndTime, string Unit, long? StatusId, long? IdDataSource, int? SourceTypeId, long? IdAlarmEvent)
            : this(IdDataTemporal, SerialNbr, IdMeter, IdLocation, TypeId, 0, Value, StartTime, EndTime, Unit, StatusId, IdDataSource, SourceTypeId, IdAlarmEvent)
        {
        }
        public DataTemporalValue(long TypeId, int Index, object Value, DateTime? StartTime, DateTime? EndTime, long? StatusId)
            : this(null, SN.Any, null, null, TypeId, Index, Value, StartTime ?? DateTime.MinValue, EndTime ?? DateTime.MinValue, "", StatusId, null, null, null)
        {
        }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataTemporalValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataTemporalValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
            {
                idDataTemporal = (long?)info.GetValue("idDataTemporal", typeof(long?));
                idAlarmEvent = (long?)info.GetValue("idAlarmEvent", typeof(long?));
                startTime = (DateTime)info.GetValue("startTime", typeof(DateTime));
                endTime = (DateTime)info.GetValue("endTime", typeof(DateTime));
                aggregationType = (Enums.AggregationType)info.GetValue("aggregationType", typeof(int));
            }
            else
            {
                idDataTemporal = (long?)info.GetValue("DataTemporalValue+idDataTemporal", typeof(long?));
                idAlarmEvent = (long?)info.GetValue("DataTemporalValue+idAlarmEvent", typeof(long?));
                startTime = (DateTime)info.GetValue("DataTemporalValue+startTime", typeof(DateTime));
                endTime = (DateTime)info.GetValue("DataTemporalValue+endTime", typeof(DateTime));
                aggregationType = (Enums.AggregationType)info.GetValue("DataTemporalValue+aggregationType", typeof(int));
            }
        }
#endif
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("idDataTemporal", idDataTemporal, typeof(long?));
            info.AddValue("idAlarmEvent", idAlarmEvent, typeof(long?));
            info.AddValue("startTime", startTime, typeof(DateTime));
            info.AddValue("endTime", endTime, typeof(DateTime));
            info.AddValue("aggregationType", (int)aggregationType, typeof(int));
        }
#endif
        #endregion
    }
    #endregion
    #region class DataObjectValue
    [Serializable]
#if (!Android)
    [ProtoBuf.ProtoContract(EnumPassthru = true, SkipConstructor = true)]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
#endif
    public class DataObjectValue : DataValue
    {
        #region Members
        #region IdObject
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
#if (!Android)
        [ProtoBuf.ProtoMember(31, Name = "IdObject")]
        [Newtonsoft.Json.JsonProperty("IdObject", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)] 
#endif
        private long? idObject = null;
        public long? IdObject
        {
            get
            {
                return idObject;
            }
        }
        #endregion
        #endregion

        #region Constructor
        public DataObjectValue() { }
        public DataObjectValue(long? IdObject, long TypeId, int Index, object Value)
            : base(TypeId, Index, Value)
        {
            this.idObject = IdObject;
        }

        public DataObjectValue(long? IdObject, long TypeId, object Value)
            : this(IdObject, TypeId, 0, Value)
        { }
        public DataObjectValue(long TypeId, object Value)
            : this(null, TypeId, Value)
        { }
        public DataObjectValue(long TypeId, int Index, object Value)
            : this(null, TypeId, Index, Value)
        { }
#if ISERIALIZABLE_TEST
        // The special constructor is used to deserialize values. 
        protected DataObjectValue(SerializationInfo info, StreamingContext context)
            : this(info, context, false) { }
        protected DataObjectValue(SerializationInfo info, StreamingContext context, bool fromDerived)
            : base(info, context, true)
        {
            if (fromDerived == false || serializationVersion > 0)
                idObject = (long?)info.GetValue("idObject", typeof(long?));
            else
                idObject = (long?)info.GetValue("DataObjectValue+idObject", typeof(long?));
        }
#endif
        #endregion

        #region GetObjectData
#if ISERIALIZABLE_TEST
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("idObject", idObject, typeof(long?));
        }
#endif
        #endregion
    }
    #endregion

    #region class IEnumerable<DataValue>Ext
    public static class IEnumerableDataValueExt
    {
        #region (extension) GetValueOrDefault
        public static T GetValueOrDefault<T>(this IEnumerable<DataValue> DataValues, long IdDataType)
        {
            return DataValues.GetValueOrDefault<T>(IdDataType, default(T));
        }
        public static T GetValueOrDefault<T>(this IEnumerable<DataValue> DataValues, long IdDataType, T Default)
        {
            return DataValues.GetValueOrDefault<T>(IdDataType, null,Default);
        }
        public static T GetValueOrDefault<T>(this IEnumerable<DataValue> DataValues, long IdDataType, int? Index, T Default)
        {
            if (DataValues == null || !DataValues.Any())
                return Default;

            DataValue dataValue = Index == null ? DataValues.FirstOrDefault(p => p.Type.IdDataType == IdDataType) : DataValues.FirstOrDefault(p => p.Type.IdDataType == IdDataType && p.Index == Index.Value);
            if (dataValue == null)
                return Default;

#if WindowsCE
			return dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T), null) : Default;
#else
            return dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T)) : Default;
#endif
        }
        #endregion
        #region (extension) GetNullableValueOrDefault
        public static Nullable<T> GetNullableValueOrDefault<T>(this IEnumerable<DataValue> DataValues, long IdDataType) where T : struct
        {
            return DataValues.GetNullableValueOrDefault<T>(IdDataType, null);
        }
        public static Nullable<T> GetNullableValueOrDefault<T>(this IEnumerable<DataValue> DataValues, long IdDataType, Nullable<T> Default) where T : struct
        {
            if (DataValues == null || DataValues.Count() == 0)
                return Default;

            DataValue dataValue = DataValues.FirstOrDefault(p => p.Type.IdDataType == IdDataType);
            if (dataValue == null)
                return Default;

#if WindowsCE
			return dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T), null) : (Nullable<T>)null;
#else
            return dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T)) : (Nullable<T>)null;
#endif
        }
        #endregion
        #region (extension) TryGetValue
        public static bool TryGetValue(this IEnumerable<DataValue> DataValues, long IdDataType, out object value)
        {
            value = null;
            if (DataValues == null || DataValues.Count() == 0)
                return false;

            DataValue dataValue = DataValues.FirstOrDefault(p => p.Type.IdDataType == IdDataType);
            if (dataValue == null)
                return false;

            value = dataValue.Value;

            return true;
        }
        public static bool TryGetValue<T>(this IEnumerable<DataValue> DataValues, long IdDataType, out Nullable<T> value) where T : struct
        {
            value = (Nullable<T>)null;
            if (DataValues == null || DataValues.Count() == 0)
                return false;

            DataValue dataValue = DataValues.FirstOrDefault(p => p.Type.IdDataType == IdDataType);
            if (dataValue == null)
                return false;
#if WindowsCE
            value = dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T), null) : (Nullable<T>)null;
#else
            value = dataValue.Value != null ? (T)Convert.ChangeType(dataValue.Value, typeof(T)) : (Nullable<T>)null;
#endif
            return true;
        }
        #endregion

        #region (extension) RemoveDataValue(DataType)
        public static IEnumerable<TSource> RemoveDataValue<TSource>(this IEnumerable<TSource> DataValues, long IdDataType) where TSource : DataValue
        {
            return DataValues.RemoveDataValue(new long[] { IdDataType });
        }
        public static IEnumerable<TSource> RemoveDataValue<TSource>(this IEnumerable<TSource> DataValues, long[] IdDataTypes) where TSource : DataValue
        {
            return (from p in DataValues
                    where !(from q in IdDataTypes select q).Contains(p.Type.IdDataType)
                    select p);
        }
        #endregion
        #region (extension) RemoveDataValue(DataValue)
        public static IEnumerable<TSource> RemoveDataValue<TSource>(this IEnumerable<TSource> source, TSource Value) where TSource : DataValue
        {
            return source.RemoveDataValue(new TSource[] { Value });
        }
        public static IEnumerable<TSource> RemoveDataValue<TSource>(this IEnumerable<TSource> source, TSource[] Values) where TSource : DataValue
        {
            return (from p in source
                    where !(from q in Values select q).Contains(p)
                    select p);
        }
        #endregion

        #region (extension) AddRange
        public static IEnumerable<TSource> AddRange<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> Values) where TSource : DataValue
        {
            List<TSource> result = new List<TSource>();
            if (source != null)
                result = source.ToList();

            if (Values != null)
                result.AddRange(Values);

            if (source == null && Values == null)
                return new List<TSource>();

            return result;
        }
        #endregion
        #region (extension) AddAndUpdateNewValues
        public static IEnumerable<TSource> AddAndUpdateNewValues<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second) where TSource : DataValue
        {
            List<TSource> dest = new List<TSource>(first);

            foreach (TSource item in second)
            {
                if (item == null)
                    continue;

                TSource existedItem = dest.FirstOrDefault(p => p.Type.IdDataType == item.Type.IdDataType && p.Index == item.Index);
                if (existedItem == null)
                {
                    // Nie ma takiego dataType w first
                    dest.Add(item);
                }
                else
                {
                    if (existedItem != item)
                        dest[dest.IndexOf(existedItem)] = item;
                }
            }
            return dest;
        }
        #endregion
        #region (extension) UpdateNewValues
        public static IEnumerable<TSource> UpdateNewValues<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second) where TSource : DataValue
        {
            List<TSource> dest = new List<TSource>();

            foreach (TSource item in first)
            {
                if (item == null)
                    continue;

                TSource existedItem = second.FirstOrDefault(p => p.Type.IdDataType == item.Type.IdDataType && p.Index == item.Index);
                if (existedItem == null)
                {
                    // Nie ma takiego dataType w second
                    dest.Add(item);
                }
                else
                {
                    if (existedItem != item)
                        dest.Add(existedItem);
                    else
                        dest.Add(item);
                }
            }
            return dest;
        }
        #endregion
    }
    #endregion
}
