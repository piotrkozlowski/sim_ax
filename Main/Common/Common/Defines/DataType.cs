using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace IMR.Suite.Common
{
#if !(WINDOWS_PHONE)
    [Serializable]
#endif
    public class DataType
    {
        #region Members
        private static readonly string UNKNOWN_DATA_TYPE = "Unknown DataType";
        #region IdDataType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private long idDataType = 0;
        public long IdDataType
        {
            get { return idDataType; }
        }
        #endregion

        #region Name
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private string name = "";
        public string Name
        {
            get { return name; }
        }
        #endregion
        #region Description
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private string description = "";
        public string Description
        {
            get { return description; }
        }
        #endregion
        #region ExtDescription
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private string extDescription = "";
        public string ExtDescription
        {
            get { return extDescription; }
        }
        #endregion

        #region Class
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        internal Enums.DataTypeClass dtClass = Enums.DataTypeClass.Unknown;
        public Enums.DataTypeClass Class
        {
            get { return dtClass; }
        }
        #endregion
        #region ReferenceType
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        internal Enums.ReferenceType referenceType = Enums.ReferenceType.None;
        public Enums.ReferenceType ReferenceType
        {
            get { return referenceType; }
        }
        #endregion
        #region Format
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private DataTypeFormat format = null;
        public DataTypeFormat Format
        {
            get { return format; }
            set { format = value; }
        }
        #endregion

        #region IsRemoteRead
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private bool isRemoteRead = false;
        public bool IsRemoteRead
        {
            get { return isRemoteRead; }
            set { isRemoteRead = value; }
        }
        #endregion
        #region IsRemoteWrite
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private bool isRemoteWrite = false;
        public bool IsRemoteWrite
        {
            get { return isRemoteWrite; }
            set { isRemoteWrite = value; }
        }
        #endregion
        #region IsArchiveOnly
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private bool? isArchiveOnly = false;
        public bool? IsArchiveOnly
        {
            get { return isArchiveOnly; }
        }
        #endregion
        #region IsEditable
        private bool? isEditable = false;
        public bool? IsEditable
        {
            get
            {
                return isEditable;
            }
        }
        #endregion

        #region IdUnit
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private int idUnit = 0;
        public int IdUnit
        {
            get { return idUnit; }
        }
        #endregion
        #region UnitSymbol
#if !WindowsCE
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
        private string unitSymbol = "";
        public string UnitSymbol
        {
            get { return unitSymbol; }
        }
        #endregion
        #endregion

        #region Any
        public static DataType Any
        {
            get { return new DataType(-1); }
        }
        #endregion
        #region IsAny
        public bool IsAny
        {
            get { return idDataType == -1; }
        }
        #endregion
        #region static GetSystemType
        public static Type GetSystemType(int idDataTypeClass)
        {
            return GetSystemType((Enums.DataTypeClass)idDataTypeClass);
        }
        public static Type GetSystemType(Enums.DataTypeClass dataTypeClass)
        {
            switch (dataTypeClass)
            {
                case Enums.DataTypeClass.Boolean:
                    return typeof(bool);
                case Enums.DataTypeClass.Date:
                case Enums.DataTypeClass.Time:
                case Enums.DataTypeClass.Datetime:
                    return typeof(DateTime);
                case Enums.DataTypeClass.Integer:
                    return typeof(long);
                case Enums.DataTypeClass.Real:
                    return typeof(double);
                case Enums.DataTypeClass.Decimal:
                    return typeof(decimal);
#if !(__MOBILE__ || WINDOWS_PHONE)
                case Enums.DataTypeClass.Color:
                    return typeof(System.Drawing.Color);
#endif
                case Enums.DataTypeClass.Binary:
                    return typeof(byte[]);
                case Enums.DataTypeClass.Object:
                    return typeof(object);
                case Enums.DataTypeClass.Xml:
                case Enums.DataTypeClass.Unknown:
                case Enums.DataTypeClass.Text:
                default:
                    return typeof(string);
            }
        }
        #endregion
        #region CorrectValueType
        public static object CorrectValueType(int idDataTypeClass, object value)
        {
            return CorrectValueType((Enums.DataTypeClass)idDataTypeClass, value);
        }
        public static object CorrectValueType(Enums.DataTypeClass dataTypeClass, object value)
        {
            if (value != null && value is IConvertible)
            {
                switch (dataTypeClass)
                {
                    case Enums.DataTypeClass.Boolean:
                        if (value is bool)
                            return value;
                        return Convert.ToBoolean(value);

                    case Enums.DataTypeClass.Date:
                        if (value is DateTime)
                            return value;
                        return Convert.ToDateTime(value).Date;

                    case Enums.DataTypeClass.Datetime:
                        if (value is DateTime)
                            return value;
                        return Convert.ToDateTime(value);

                    case Enums.DataTypeClass.Integer:
                        if (value is long)
                            return value;
                        return Convert.ToInt64(value);

                    case Enums.DataTypeClass.Real:
                        if (value is double)
                            return value;
                        if (value is string)
                            return double.Parse(value.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        else
                            return Convert.ToDouble(value);

                    case Enums.DataTypeClass.Decimal:
                        if (value is decimal)
                            return value;
                        if (value is string)
                            return decimal.Parse(value.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        else
                            return Convert.ToDecimal(value);

                    case Enums.DataTypeClass.Text:
                        if (value is string)
                            return value;
                        return Convert.ToString(value);

                    case Enums.DataTypeClass.Time:
                        if (value is TimeSpan)
                            return value;
                        return Convert.ToDateTime(value).TimeOfDay;
                }
            }
            return value;
        }
        #endregion
        #region GetHelperDataTypes

        public static List<long> GetHelperDataTypes(Enums.ReferenceType referenceType, bool columnHelpersOnly)
        {
            List<long> retList = new List<long>();
            switch (referenceType)
            {
                case Enums.ReferenceType.IdAction:
                    #region Action
                    retList.Add(DataType.HELPER_ID_ACTION);
                    retList.Add(DataType.HELPER_SERIAL_NBR);
                    retList.Add(DataType.HELPER_ID_METER);
                    retList.Add(DataType.HELPER_ID_LOCATION);
                    retList.Add(DataType.HELPER_ID_ACTION_TYPE);
                    retList.Add(DataType.HELPER_ID_ACTION_STATUS);
                    retList.Add(DataType.HELPER_ID_ACTION_DATA);
                    retList.Add(DataType.HELPER_ID_ACTION_PARENT);
                    retList.Add(DataType.HELPER_ID_DATA_ARCH);
                    retList.Add(DataType.HELPER_ID_MODULE);
                    retList.Add(DataType.HELPER_ID_OPERATOR);
                    retList.Add(DataType.HELPER_CREATION_DATE);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdActor:
                    #region Actor
                    retList.Add(DataType.HELPER_ID_ACTOR);
                    retList.Add(DataType.HELPER_NAME);
                    retList.Add(DataType.HELPER_SURNAME);
                    retList.Add(DataType.HELPER_CITY);
                    retList.Add(DataType.HELPER_ADDRESS);
                    retList.Add(DataType.HELPER_POSTCODE);
                    retList.Add(DataType.HELPER_EMAIL);
                    retList.Add(DataType.HELPER_MOBILE);
                    retList.Add(DataType.HELPER_PHONE);
                    retList.Add(DataType.HELPER_ID_LANGUAGE);
                    retList.Add(DataType.HELPER_DESCRIPTION);
                    retList.Add(DataType.HELPER_AVATAR);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdActorGroup:
                    #region ActorGroup
                    retList.Add(DataType.HELPER_ID_ACTOR_GROUP);
                    retList.Add(DataType.HELPER_NAME);
                    retList.Add(DataType.HELPER_BUILT_IN);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_DESCR);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdAlarm:
                    #region Alarm
                    retList.Add(DataType.HELPER_ID_ALARM);
                    retList.Add(DataType.HELPER_ID_ALARM_EVENT);
                    retList.Add(DataType.HELPER_ID_ALARM_DEF);
                    retList.Add(DataType.HELPER_SERIAL_NBR);
                    retList.Add(DataType.HELPER_ID_LOCATION);
                    retList.Add(DataType.HELPER_ID_METER);
                    retList.Add(DataType.HELPER_ID_ALARM_TYPE);
                    retList.Add(DataType.HELPER_ID_DATA_TYPE_ALARM);
                    retList.Add(DataType.HELPER_SYSTEM_ALARM_VALUE);
                    retList.Add(DataType.HELPER_ALARM_VALUE);
                    retList.Add(DataType.HELPER_ID_OPERATOR);
                    retList.Add(DataType.HELPER_ID_ALARM_STATUS);
                    retList.Add(DataType.HELPER_TRANSMISSION_TYPE);
                    retList.Add(DataType.HELPER_ID_TRANSMISSION_TYPE);
                    retList.Add(DataType.HELPER_ID_ALARM_GROUP);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdDistributor:
                    #region Distributor
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_NAME);
                    retList.Add(DataType.HELPER_CITY);
                    retList.Add(DataType.HELPER_ADDRESS);
                    retList.Add(DataType.HELPER_ID_DESCR);
                    if (!columnHelpersOnly)
                    {
                        retList.Add(DataType.HELPER_DISTRIBUTOR_ID_ROLE);
                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdIssue:
                    #region Issue
                    retList.Add(DataType.HELPER_ID_ISSUE);
                    retList.Add(DataType.HELPER_ID_ISSUE_TYPE);
                    retList.Add(DataType.HELPER_ID_ISSUE_STATUS);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_LOCATION);
                    retList.Add(DataType.HELPER_ID_ACTOR);
                    retList.Add(DataType.HELPER_CREATION_DATE);
                    retList.Add(DataType.HELPER_REALIZATION_DATE);
                    retList.Add(DataType.HELPER_DEADLINE);
                    retList.Add(DataType.HELPER_ID_PLANNED_ROUTE);
                    retList.Add(DataType.HELPER_SHORT_DESCR);
                    retList.Add(DataType.HELPER_LONG_DESCR);
                    retList.Add(DataType.HELPER_ID_OPERATOR_REGISTERING);
                    retList.Add(DataType.HELPER_ID_OPERATOR_PERFORMER);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdMeter:
                    #region Meter
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_METER_TYPE);
                    retList.Add(DataType.HELPER_ID_METER);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdLocation:
                    #region Location
                    retList.Add(DataType.HELPER_ID_LOCATION);
                    retList.Add(DataType.HELPER_ID_LOCATION_TYPE);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_CONSUMER);
                    retList.Add(DataType.HELPER_ID_LOCATION_STATE_TYPE);
                    retList.Add(DataType.HELPER_LOCATION_IN_KPI);
                    retList.Add(DataType.HELPER_LOCATION_ALLOW_GROUPING);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdOperator:
                    #region Operator
                    retList.Add(DataType.HELPER_ID_OPERATOR);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_ACTOR);
                    retList.Add(DataType.HELPER_LOGIN);
                    retList.Add(DataType.HELPER_PASSWORD);
                    retList.Add(DataType.HELPER_SERIAL_NBR);
                    retList.Add(DataType.HELPER_DESCRIPTION);
                    retList.Add(DataType.HELPER_IS_BLOCKED);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdTask:
                    #region Task
                    retList.Add(DataType.HELPER_ID_TASK);
                    retList.Add(DataType.HELPER_ID_TASK_TYPE);
                    retList.Add(DataType.HELPER_ID_TASK_STATUS);
                    retList.Add(DataType.HELPER_ID_TASK_GROUP);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_PLANNED_ROUTE);
                    retList.Add(DataType.HELPER_ID_OPERATOR_REGISTERING);
                    retList.Add(DataType.HELPER_ID_OPERATOR_PERFORMER);
                    retList.Add(DataType.HELPER_CREATION_DATE);
                    retList.Add(DataType.HELPER_ACCEPTED);
                    retList.Add(DataType.HELPER_ID_OPERATOR_ACCEPTED);
                    retList.Add(DataType.HELPER_ACCEPTANCE_DATE);
                    retList.Add(DataType.HELPER_NOTES);
                    retList.Add(DataType.HELPER_DEADLINE);
                    retList.Add(DataType.HELPER_TOPIC_NUMBER);
                    retList.Add(DataType.HELPER_PRIORITY);
                    retList.Add(DataType.HELPER_OPERATION_CODE);
                    retList.Add(DataType.HELPER_ID_LOCATION);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.SerialNbr:
                    #region Device
                    retList.Add(DataType.HELPER_SERIAL_NBR);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_DEVICE_TYPE);
                    retList.Add(DataType.HELPER_ID_DEVICE_STATE_TYPE);
                    retList.Add(DataType.HELPER_ID_DEVICE_ORDER_NUMBER);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdSimCard:
                    #region SimCard
                    retList.Add(DataType.HELPER_ID_SIM_CARD);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_SIM_CARD_SERIAL_NBR);
                    retList.Add(DataType.HELPER_PHONE);
                    retList.Add(DataType.HELPER_PIN);
                    retList.Add(DataType.HELPER_PUK);
                    retList.Add(DataType.HELPER_MOBILE_NETWORK_CODE);
                    retList.Add(DataType.HELPER_IP);
                    retList.Add(DataType.HELPER_APN_LOGIN);
                    retList.Add(DataType.HELPER_APN_PASSWORD);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdTariff:
                    #region SimCard
                    retList.Add(DataType.HELPER_ID_TARIFF);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdRoute:
                    #region Route
                    retList.Add(DataType.HELPER_ID_ROUTE);
                    retList.Add(DataType.HELPER_ID_ROUTE_DEF);
                    retList.Add(DataType.HELPER_YEAR);
                    retList.Add(DataType.HELPER_MONTH);
                    retList.Add(DataType.HELPER_WEEK);
                    retList.Add(DataType.HELPER_ID_OPERATOR_EXECUTOR);
                    retList.Add(DataType.HELPER_ID_OPERATOR_APPROVED);
                    retList.Add(DataType.HELPER_DATE_UPLOADED);
                    retList.Add(DataType.HELPER_DATE_APPROVED);
                    retList.Add(DataType.HELPER_DATE_FINISHED);
                    retList.Add(DataType.HELPER_ID_ROUTE_STATUS);
                    retList.Add(DataType.HELPER_ID_DISTRIBUTOR);
                    retList.Add(DataType.HELPER_ID_ROUTE_TYPE);
                    retList.Add(DataType.HELPER_NAME);
                    retList.Add(DataType.HELPER_CREATION_DATE);
                    retList.Add(DataType.HELPER_EXPIRATION_DATE);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                case Enums.ReferenceType.IdReport:
                    #region Report
                    retList.Add(DataType.HELPER_ID_REPORT);
                    retList.Add(DataType.HELPER_ID_REPORT_TYPE);
                    if (!columnHelpersOnly)
                    {

                    }
                    #endregion
                    break;
                default:
                    break;
            }
            return retList;
        }

        #endregion

        #region Data types
        #region SQL query
        /*
        select 'public const long ' + 
        upper (dt.name) + 
        space (50 - len (dt.[name])) +
        ' = ' + cast (dt.id_data_type as varchar) + ';' +
        space (10 - len (dt.id_data_type)) +  
        '// '  + isnull(d.description, '<Brak opisu>')
        from data_type dt left outer join descr d on dt.id_descr = d.id_descr and id_language = 1
        order by dt.name 
        */

        // exec test_CreateDataTypeForC#

        /*
        SET NOCOUNT ON
        DECLARE @TXT_TBL TABLE (ID INT IDENTITY(1,1), TXT NVARCHAR(max))
        --------------------------------
        -- 1-1000 --atrybuty lokalizacji
        --------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region LOCATION attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 1 and 1000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion' 
        ----------------------------------
        -- 1001-2000 --atrybuty urzadzenia
        ----------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DEVICE attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 1001 and 2000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------
        -- 2001-3000 --atrybuty metera
        ------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region METER attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 2001 and 3000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ----------------------------------------
        -- 3001-3500 --atrybuty data_source_type
        ----------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA_SOURCE_TYPE attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 3001 and 3500
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ----------------------------------
        -- 3501-4000 --parametry operatora
        ----------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region OPERATOR params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
         * SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 3501 and 4000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ----------------------------------
        -- 12001-13000 --parametry raportow
        ----------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region REPORT params'
         * INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 12001 and 13000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        --------------------------------
        -- 4001-5000 --parametry modulow
        --------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region MODULE params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 4001 and 5000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ---------------------------------
        -- 5001-6000 --parametry driverow
        ----------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DRIVER params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 5001 and 6000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------
        -- 6001-7000 -- atrybuty akcji
        ------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region ACTION attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 6001 and 7000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------
        -- 11001-12000 -- atrybuty alarmow
        ------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region ALARM attributes'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 11001 and 12000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        --------------------------------
        -- 7001-8000 --parametry systemu
        --------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region SYSTEM params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 7001 and 8000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------
        -- 8001-9000 --parametry sesji
        ------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region SESSION params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
         * WHERE DT.ID_DATA_TYPE BETWEEN 8001 and 9000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ---------------------------------------
        -- 9001-10000 --parametry zdarzen sesji
        ---------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region EVENT SESSION params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 9001 and 10000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ----------------------------------
        -- 13001-14000 --parametry estymacji
        ----------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region ESTIM params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 13001 and 14000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------
        -- 10001-11000 --parametry inne
        ------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region OTHER params'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE BETWEEN 10001 and 11000
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
         * ------------------------------------------------------------
        -- 100001-...	pomiary i wartosci konfiguracyjne READ/WRITE
        ------------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA read/write'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE >= 100001 and DT.IS_REMOTE_WRITE = 1 AND DT.IS_REMOTE_READ = 1
        ORDER BY DT.ID_DATA_TYPE
         * INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------------------------------
        -- 100001-...	pomiary i wartosci konfiguracyjne READ
        ------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA read only'
        INSERT INTO @TXT_TBL 
         * * SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE >= 100001 and DT.IS_REMOTE_WRITE = 0 AND DT.IS_REMOTE_READ = 1
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------------------------------
        -- 100001-...	pomiary i wartosci konfiguracyjne WRITE
        ------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA write only'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE >= 100001 and DT.IS_REMOTE_WRITE = 1 AND DT.IS_REMOTE_READ = 0
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------------------------------
        -- 100001-...	pomiary i wartosci konfiguracyjne ARCHIVE
        ------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA archive only'
        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE >= 100001 and DT.IS_ARCHIVE_ONLY = 1
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------------------------------
        -- 100001-...	pomiary i wartosci konfiguracyjne NO READ, NO WRITE, NO ARCHIVE
        ------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA !!! NO read, NO write, NO archive !!!'

        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE DT.ID_DATA_TYPE >= 100001 and DT.IS_ARCHIVE_ONLY = 0 and DT.IS_REMOTE_WRITE = 0 AND DT.IS_REMOTE_READ = 0
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        ------------------------------------------------------
        -- xxx-...	-REST
        ------------------------------------------------------
        INSERT INTO @TXT_TBL SELECT '		#region DATA !!! REST !!! not categorized !!!'

        INSERT INTO @TXT_TBL 
        SELECT	'		public const long ' + UPPER(DT.NAME) + SPACE(100-LEN(DT.NAME)) + ' = ' + CAST(DT.ID_DATA_TYPE AS VARCHAR) + ';' +
                 SPACE(10-LEN(DT.ID_DATA_TYPE)) + '// '  + ISNULL(D.DESCRIPTION, '<Brak opisu>')
        FROM DATA_TYPE DT LEFT OUTER JOIN DESCR D ON DT.ID_DESCR = D.ID_DESCR AND ID_LANGUAGE = 1
        WHERE	DT.ID_DATA_TYPE >= 13000 and DT.ID_DATA_TYPE < 100001 or
                DT.ID_DATA_TYPE >= 100001 and DT.IS_ARCHIVE_ONLY = 1 and DT.IS_REMOTE_WRITE = 1 AND DT.IS_REMOTE_READ = 1
        ORDER BY DT.ID_DATA_TYPE
        INSERT INTO @TXT_TBL SELECT	'		#endregion'
        SELECT TXT FROM @TXT_TBL order by ID

        SET NOCOUNT OFF
        */
        #endregion

        #region LOCATION attributes
        public const long LOCATION_NAME = 1;         // Nazwa
        public const long LOCATION_CITY = 2;         // Miejscowo��
        public const long LOCATION_ADDRESS = 3;         // Adres
        public const long LOCATION_ZIP = 4;         // Kod pocztowy
        public const long LOCATION_COUNTRY = 5;         // Kraj
        public const long LOCATION_CID = 6;         // CID
        public const long LOCATION_LATITUDE = 7;         // Szeroko�� geograficzna
        public const long LOCATION_LONGITUDE = 8;         // D�ugo�� geograficzna
        public const long LOCATION_CREATION_DATE = 9;         // Data utworzenia
        public const long LOCATION_MEMO = 10;        // Notatki
        public const long LOCATON_LAST_MAINTENANCE_DATE = 11;        // Ostatni serwis
        public const long LOCATION_CONTACT_PHONE_1 = 12;        // Telefon kontaktowy 1
        public const long LOCATION_CONTACT_PHONE_2 = 13;        // Telefon kontaktowy 2
        public const long LOCATION_CONTACT_PHONE_3 = 14;        // Telefon kontaktowy 3
        public const long LOCATION_RESTRICTIONS = 15;        // Restrykcje na lokalizacji
        public const long LOCATION_IS_INITIALIZED = 16;        // Czy lokalizacja zainicjalizowana?
        public const long LOCATION_IS_EQUIPPED = 17;        // Czy na lokalizacji znajduj� sie urz�dzenia telemetryczne?
        public const long LOCATION_IS_ACTIVE = 18;        // Czy lokalizacja jest aktywna?
        public const long LOCATION_IS_RUNNING = 19;        // Czy lokalizacja jest dzia�aj�ca?
        public const long LOCATION_MOUNT_PRIORITY = 20;        // Priorytet monta�u
        public const long LOCATION_USER_PARAM_1 = 21;        // Parametr u�ytkownika 1
        public const long LOCATION_USER_PARAM_2 = 22;        // Parametr u�ytkownika 2
        public const long LOCATION_USER_PARAM_3 = 23;        // Parametr u�ytkownika 3
        public const long LOCATION_USER_PARAM_4 = 24;        // Parametr u�ytkownika 4
        public const long LOCATION_USER_PARAM_5 = 25;        // Parametr u�ytkownika 5
        public const long LOCATION_SECTOR = 31;        // Sektor
        public const long LOCATION_STATION_CODE = 32;        // Kod stacji
        public const long LOCATION_SITE_MANAGER_OPERATOR_ID = 33;        // <Brak opisu>
        public const long LOCATION_CLUSTER_MANAGER_OPERATOR_ID = 34;        // <Brak opisu>
        public const long LOCATION_TERRITORY_MANAGER_OPERATOR_ID = 35;        // <Brak opisu>
        public const long LOCATION_GROUP_MAP_FILE = 36;        // <Brak opisu>
        public const long LOCATION_GROUP_LATITUDE_NW = 37;        // <Brak opisu>
        public const long LOCATION_GROUP_LONGITUDE_NW = 38;        // <Brak opisu>
        public const long LOCATION_GROUP_LATITUDE_SE = 39;        // <Brak opisu>
        public const long LOCATION_GROUP_LONGITUDE_SE = 40;        // <Brak opisu>
        public const long LOCATION_NICK = 41;        // Nick
        public const long LOCATION_SRT_PERMANENT = 42;        // Czy lokalizacja domy�lnie wykorzystuje SRT do wprowadzania pomiar�w
        public const long LOCATION_SRT_CONTACT_ENABLED = 43;        // Czy specjalista mo�e skontaktowa� si� z lokalizacj�
        public const long LOCATION_SRT_ACCESS_ENABLED = 44;        // Czy lokalizacja powinna otrzymywa� ��danie wprowadzenia danych przez SRT
        public const long LOCATION_SRT_ACCOUNT_ENABLED = 45;        // Czy jest zezwolone logowanie do SRT
        public const long LOCATION_SRT_PASSWORD = 46;        // Has�o u�ytkownika SRT
        public const long LOCATION_SRT_OPERATOR_ID = 47;        // U�ytkownik SRT
        public const long LOCATION_FAILURE_REC_LAST_TIME = 48;        // Czas ostatniego przeprowadzenia procedury FR
        public const long LOCATION_FAILURE_REC_MIN_INTERVAL = 49;        // Minimalny okres procedury FR
        public const long LOCATION_FAILURE_REC_PROBABILITY = 50;        // Prawdopodobie�stwo przeprowadzenia procedury FR
        public const long LOCATION_SRT_LAST_ACCESS = 51;        // Czas ostatniego dost�pu przez SRT
        public const long LOCATION_MEASURED_LATITUDE = 52;        // <Brak opisu>
        public const long LOCATION_MEASURED_LONGITUDE = 53;        // <Brak opisu>
        public const long LOCATION_IMRSC_ID = 54;        // <Brak opisu>
        public const long LOCATION_EXTERNAL_SOT = 55;        // <Brak opisu>
        public const long LOCATION_HAULIER_DISPOSITOR = 56;        // <Brak opisu>
        public const long LOCATION_IMRSC_EXCLUDE_FROM_SYNCHRONIZATION = 57;        // <Brak opisu>
        public const long LOCATION_MBR_ID = 58;        // <Brak opisu>
        public const long LOCATION_CONSUMER_OPERATOR_ID = 59;        // U�ytkownik IMR Consumer
        public const long LOCATION_SALES_SUPPORT_OPERATOR_ID = 60;
        public const long LOCATION_TECHNICAL_SUPPORT_OPERATOR_ID = 61;
        public const long LOCATION_IS_MAIN_GROUP = 62; // czy dana grupa jest defaultowa grupa dla dystrybutora
        public const long LOCATION_LOCATION_NUMBER_OF_TANKS = 63; // <Brak opisu>
        public const long LOCATION_PRODUCT_CODE = 64; // typ paliwa dla lokalizacji lub obszaru
        public const long LOCATION_RECEPIENT = 65; // odbiorca medium/wlasciciel lokalizacji
        public const long LOCATION_MOUNTED_RADIO_DEVICE = 66; // zamontowany ukladzik radiowy (nie OKO i nie AMPLI)
        public const long LOCATION_IS_WATERMETER = 67; // czy obslugiwany wodomierz
        public const long LOCATION_ESTIMATED_ANNUAL_CONSUMPTION = 68; // Estymowane roczne zu�ycie
        public const long LOCATION_DELIVERY_ADVICE = 69; // Warto�� ze s�ownika DELIVERY_ADVICE (MustGo, CanGo, itd) - wykorzystywane w DIMA
        public const long LOCATION_LOMOSOFT_SITE_ID = 71;
        public const long LOCATION_LOMOSOFT_CUSTOMER_NUMBER = 72;
        public const long LOCATION_SERVICE_VALIDATION_PERIOD = 73;  //czas walidacji serwisu na lokalizacji
        public const long LOCATION_PROJECTION_X = 74;
        public const long LOCATION_PROJECTION_Y = 75;
        public const long LOCATION_DISTRIBUTOR_GROUP_ID = 76;   //Identyfikator grupy lokalizacji dedykowanej dla dystrybutora np. Amerigas (podzia� na Bloki, Przemys�owe i Domestic - uwaga! jedna lokalizacja mo�e by�  w wielu grupach - kolejne grupy z kolejnymi indexami)
        public const long LOCATION_OKO_REQUIRED = 77;
        public const long LOCATION_PWG_WEATHER_STATION_ID = 78;
        public const long LOCATION_INSTALLATION_TYPE = 79;
        public const long LOCATION_IS_IMR_ATG_INSTALLED = 80;
        public const long LOCATION_TECHNICAL_SUPERVISOR = 81;
        public const long LOCATION_CONFIGURATION_PROFILE = 82;
        public const long LOCATION_CONFIGURATION_PROFILE_APPLIED_DATE = 83;
        public const long LOCATION_CONFIGURATION_PROFILE_APPLIED_OPERATOR = 84;
        public const long LOCATION_CONFIGURATION_PROFILE_APPLIED = 85;
        public const long LOCATION_BP_SUN_CLIENT_LOCATION = 86;
        public const long LOCATION_READY_FOR_INSTALLATION_DATE = 87;
        public const long LOCATION_FIRST_FILL = 88;
        public const long LOCATION_AMPLI_6850_SN = 89;
        public const long LOCATION_CUSTOMER_NUMBER = 90;
        public const long LOCATION_BP_SUN_CLIENT = 91;
        public const long LOCATION_RDG_CODE = 92;
        public const long LOCATION_WEATHER_STATION_CODE = 93;
        public const long LOCATION_HEATING_SURFACE = 94;
        public const long LOCATION_ADDITIONAL_HEATING_SURFACE = 95;
        public const long LOCATION_BATH_SURFACE = 96;
        public const long LOCATION_INHABITANT_COUNT = 97;
        public const long LOCATION_BLOCKED = 98;
        public const long LOCATION_DELIVERY_ADVICE_LATCHED = 99;
        public const long LOCATION_HOST_CID = 100;
        public const long LOCATION_CONTACT_PHONE_4 = 101;        // Telefon kontaktowy 4
        public const long LOCATION_DELIVERY_ORDER_STATUS = 102;
        public const long LOCATION_REJECTION_END_DATE = 103;
        public const long LOCATION_ACTION_TYPE = 104;
        public const long LOCATION_INVOICE_ID = 105;
        public const long LOCATION_AREA_NAME = 106;
        public const long LOCATION_ENTRANCE_INDEX_NBR = 107;
        public const long LOCATION_FLOOR_INDEX_NBR = 108;
        public const long LOCATION_SECTION_NBR = 109;
        public const long LOCATION_CLUSTER_CLASS = 110;
        public const long LOCATION_COUNTY_CAPITAL_ID = 111; // Identyfikator stolicy powiatu
        public const long LOCATION_SCHEDULER_OPERATOR_ID = 112; //Dla Macka T. Planista dostaw
        public const long LOCATION_DETAILS_EDITING_ENABLED = 113;
        public const long LOCATION_INSTALLATION_PHOTOS = 114;
        public const long LOCATION_PERFORMANCE_VALUE = 115;
        public const long LOCATION_ALARM_USER = 116;
        public const long LOCATION_PARENT_GROUP_LOCATION = 117;
        public const long LOCATION_LAST_DECISION_REFUEL_CHECKED_TIME = 118;
        public const long LOCATION_DISP_CONNECTION_TYPE = 119;
        public const long LOCATION_DISP_PUMPOUT_DATE = 120;
        public const long LOCATION_DISP_LEGALIZATION_DATE = 121;
        public const long LOCATION_FP_CONFIRMED = 122;
        public const long LOCATION_ATG_TYPE = 123;
        public const long LOCATION_NERA_RANKING = 124;
        public const long LOCATION_ACCESS_CONDITIONS = 125;
        public const long LOCATION_NOZZLE_CALIBRATION_NEXT_DATE = 126;
        public const long LOCATION_NOZZLE_CALIBRATION_LAST_DATE = 127;
        public const long LOCATION_CONTRACT_NAME = 128;
        public const long LOCATION_CONTRACT_START_TIME = 129;
        public const long LOCATION_CONTRACT_END_TIME = 130;
        public const long LOCATION_SALES_FROM_COUNTERS_ENABLED = 131;
        public const long LOCATION_INVOICE_DAY = 132;
        public const long LOCATION_FP_ENABLED = 133;
        public const long LOCATION_LOW_GSM_AREA = 134;
        public const long LOCATION_AVG_GAS_STOCK_LEVEL = 135;
        public const long LOCATION_AVG_GAS_STOCK_LEVEL_TIMESTAMP = 136;
        public const long LOCATION_CARRIER = 137;
        public const long LOCATION_TRUCK = 138;
        public const long LOCATION_TRAILER = 139;
        public const long LOCATION_GSM_QUALITY_DESCRIPTION = 140;
        public const long LOCATION_DISPENSER_PUMP_TYPE = 141;
        public const long LOCATION_EXTERNAL_ID = 142;
        public const long LOCATION_TIME_ZONE_ID = 143;
        public const long LOCATION_WEATHER_HISTORY_MINIMUM_DATE_TO_DOWNLOAD = 144;
        public const long LOCATION_REJECTION_START_DATE = 145;
        public const long LOCATION_SCHEMA_DEFINITION = 146;
        public const long LOCATION_SCHEMA_CONFIGURATION = 147;
        public const long LOCATION_PROVINCE = 148; // wojewodztwo
        public const long LOCATION_IS_DRIVEWAY = 149; // czy jest podjazd
        public const long LOCATION_IS_OPENED_ALL_DAY = 150; // czy calodobowe
        public const long LOCATION_STATION_TYPE = 151; // typ stacji
        public const long LOCATION_IS_CLUSTER = 152; // czy lokalizacja jest klastrem
        public const long LOCATION_FLOODING_THREAT = 153; // zagrozenie zalaniem
        public const long LOCATION_STATION_PROFILE_TYPE = 154;
        public const long LOCATION_DEVICE_INSTALLATION_PLACE = 155;
        public const long LOCATION_DEVICE_LOCATION_COMPASS_DIRECTION = 156;
        public const long LOCATION_DEVICE_INSTALLATION_PLACE_NOTE = 157;
        public const long LOCATION_ID_ROUTE_DEF = 158;
        public const long LOCATION_ROUTE_EXTERNAL_ID = 159;

        public const long LOCATION_LEGALIZATION_EXPIRATION = 160; // legalizacja do
        public const long LOCATION_SEAL_ON_HALF_THREAD = 161; // plomba na p���rubunku
        public const long LOCATION_STREET = 162; // ulica
        public const long LOCATION_HOUSE_NUMBER = 163; // numer domu
        public const long LOCATION_PEOPLE_AMOUNT = 164; // ilo�� os�b
        public const long LOCATION_SUBBILING_FLAG = 165; // flaga podlicznika
        public const long LOCATION_IS_LEGALIZED = 166; // Jest legalizacja


        #endregion
        #region DEVICE attributes
        public const long DEVICE_FACTORY_NUMBER = 1001;       // Produkcyjny numer seryjny urz�dzenia
        public const long DEVICE_HARDWARE_VERSION = 1002;       // 
        public const long DEVICE_BATTERY_VERSION = 1003;       // 
        public const long DEVICE_CONFIGURATION_CREATION_DATE = 1004;       // 
        public const long DEVICE_BATTERY_CHANGE_DATE = 1005;       // 
        public const long DEVICE_FIRMWARE_CHANGE_DATE = 1006;       //
        public const long DEVICE_LONG_ANTENNA = 1007;       // 
        public const long DEVICE_ESTIMATED_DEAD_DATE = 1008;       // 
        public const long DEVICE_NCAB = 1009;       // 
        public const long DEVICE_PHONE_NUMBER = 1010;      // <Brak opisu>
        public const long DEVICE_APN_IP = 1011;      // <Brak opisu>
        public const long DEVICE_APN_PORT = 1012;      // <Brak opisu>
        public const long DEVICE_PACKAGE_NBR = 1013;      // <Brak opisu>
        public const long DEVICE_DRIVER = 1014;      // <Brak opisu>
        public const long DEVICE_WARRANTY_DATE = 1015;
        public const long DEVICE_ID_DELIVERY = 1016;
        public const long DEVICE_PRODUCTION_DATE = 1017;
        public const long DEVICE_PRODUCTION_YEAR = 1018;
        public const long DEVICE_ID_CONNECTION = 1020;      // <Brak opisu>
        public const long DEVICE_ID_PROVIDER = 1021;      // <Brak opisu>
        public const long DEVICE_MOBILE_NETWORK_CODE = 1022;      // <Brak opisu>
        public const long DEVICE_PRIVATE_KEY = 1023;    // <Brak opisu>
        public const long DEVICE_MAC_KEY = 1024;    // <Brak opisu>
        public const long DEVICE_IV_OUT = 1025;    // <Brak opisu>
        public const long DEVICE_IV_IN = 1026;    // <Brak opisu>
        public const long DEVICE_SYNCHRONIZATION_DATA_FLAG = 1027;      // Flaga danych synchronizacji
        public const long DEVICE_SYNCHRONIZATION_IMPULSE_VALUE = 1028;      // Liczba impuls�w przy synchronizacji
        public const long DEVICE_SYNCHRONIZATION_IMPULSE_WEIGHT = 1029;      // Waga impuls�w przy synchronizacji
        public const long DEVICE_SYNCHRONIZATION_METER_VALUE = 1030;      // Warto�� na liczniku podczas synchronizacji
        public const long DEVICE_SYNCHRONIZATION_DEVICE_VALUE = 1031;      // Warto�� na urz�dzeniu podczas synchronizacji
        public const long DEVICE_SYNCHRONIZATION_UNIT = 1032;      // Jednostka podczas synchronizacji
        public const long DEVICE_SYNCHRONIZATION_TYPE = 1033;
        public const long DEVICE_CRYPTOGRAPHY_KEY = 1034;
        public const long DEVICE_CRYPTOGRAPHY_MAC_KEY = 1035;
        public const long DEVICE_MBR_ID = 1036;
        public const long DEVICE_MALFUNCTION_SUSPECTED = 1037;
        public const long DEVICE_FIRMWARE_MD5 = 1038;
        public const long DEVICE_TEMPERATURE_PRESENT = 1039;
        public const long DEVICE_SLEEP_PERIOD = 1040;
        public const long DEVICE_MAX_INCOMING_SMS_PACKETS = 1041;
        public const long DEVICE_IS_BATTERY = 1042;
        public const long DEVICE_ASI_ADDRESS = 1043;
        public const long DEVICE_IS_SPARE = 1044;
        public const long DEVICE_ACTIVATION_TIME = 1045;
        public const long DEVICE_RUBBER_SEAL_VERSION = 1046;
        public const long DEVICE_FIRMWARE_MAJOR_VERSION = 1047;
        public const long DEVICE_FIRMWARE_MINOR_VERSION = 1048;
        public const long DEVICE_IS_BRAND = 1049;
        public const long DEVICE_CONFIGRATION_DISTRIBUTOR = 1050;
        public const long DEVICE_CONFIGURATION_PROFILE_APPLIED = 1051;
        public const long DEVICE_CONFIGURATION_PROFILE_APPLIED_DATE = 1052;
        public const long DEVICE_CONFIGURATION_PROFILE_APPLIED_OPERATOR = 1053;
        public const long DEVICE_LAST_DIAGNOSTIC_READOUT_TIMESTAMP = 1054;
        public const long DEVICE_DIAGNOSTIC_READOUT_ATTEMPTS = 1055;
        public const long DEVICE_ESTIMATED_BATTERY_RUN_OUT_DATE = 1056;
        public const long DEVICE_OBSERVER_OPERATOR_ID = 1057;
        public const long DEVICE_SENSOR_CL_NORMALISATION_MAX_VALUE = 1058;
        public const long DEVICE_BATCH_NUMBER = 1059;
        public const long DEVICE_FREQUENCY_OFFSET = 1060;
        public const long DEVICE_MEASURED_FREQUENCY = 1061;
        public const long DEVICE_LAST_SHIPPING_ID_IMR_SERVER = 1062;//Identyfikator ostatniego serwera telemetrycznego na kt�ry zosta�o zwysy�kowane urz�dzenie
        public const long DEVICE_IS_CONFIGURATION_SYNCHRONIZED = 1063;
        public const long DEVICE_LAST_SHIPPING_DATE = 1064;
        public const long DEVICE_IS_CONFIGURATION_ACTIVE_IN_DEVICE = 1065;
        public const long DEVICE_METER_REQUIRED = 1066;
        public const long DEVICE_BARCODE = 1067;
        public const long DEVICE_SET_NBR = 1068;
        public const long DEVICE_USER_PORTAL_CONFIGURATION_IN_PROGRESS = 1069;
        public const long DEVICE_SERIAL_NBR_IN_WAN3_FRAME = 1070;//czy w ka�dej ramce wychodz�cej z systemu ma si� znajdowa� SN urz�dzenia
        public const long DEVICE_RUN_SYNCHRONISATION_ACTION = 1071; //czy wykonywac synchronizacj� wywo�ywan� z akcji 
        public const long DEVICE_MEASURE_ALARM_VALIDITY = 1072; //ilo�� godzin wa�no�ci 
        public const long DEVICE_MEASURE_ACTION_VALIDITY = 1073; //czy wykonywac synchronizacj� wywo�ywan� z akcji 
        public const long DEVICE_FAILURE_RISK_TYPE = 1074;
        public const long DEVICE_NOTE = 1075;
        public const long DEVICE_MANUFACTURING_BATCH_NUMBER = 1076;
        public const long DEVICE_LEADING_READOUT_DATA_TYPE = 1077;
        public const long DEVICE_IV_OUT_CHANGE = 1078;
        public const long DEVICE_IV_IN_CHANGE = 1079;
        public const long DEVICE_ACCEPTABLE_SCHEDULE_READOUT_DELAY = 1080;//podawany w minutach
        public const long DEVICE_PROBE_ACCURACY = 1081;
        public const long DEVICE_IS_INTEGRATED = 1082;
        public const long DEVICE_EVC_CONFIGURATION_PARAMS = 1083;
        public const long DEVICE_LATITUDE = 1084;
        public const long DEVICE_LONGITUDE = 1085;
        public const long DEVICE_CRYPTOGRAPHY_KEY_ENCRYPTED = 1086;
        public const long DEVICE_PRIVATE_KEY_ENCRYPTED = 1087;    // <Brak opisu>
        public const long DEVICE_MAC_KEY_ENCRYPTED = 1088;    // <Brak opisu>
        public const long DEVICE_ID_ARTICLE = 1089;    // <Brak opisu>
        public const long DEVICE_PROTOCOL_ADDRESS = 1090;    // SerialNbr/Addres from frames; string!
        public const long DEVICE_PROTOCOL_ID = 1091;    // DeviceProtocol (DEVICE_PROTOCOL_ADDRESS +DEVICE_PROTOCOL_ID => unique key)


        #endregion
        #region METER attributes
        public const long METER_NUMBER = 2001;      // <Brak opisu>
        public const long METER_NAME = 2002;      // <Brak opisu>
        public const long METER_HIGH_WATER_LEVEL_DRIVER = 2003;      // <Brak opisu>
        public const long METER_DEADSTOCK_LEVEL_DRIVER = 2004;      // <Brak opisu>
        public const long METER_TANK_CAPACITY = 2005;      // Pojemno�� zbiornika
        public const long METER_SERIAL_NUMBER = 2006;
        public const long METER_MEDIUM_REFERENCE_DENSITY = 2007;
        public const long METER_MEDIUM_COEFFICIENT_DENSITY = 2008;
        public const long METER_TANK_HEIGHT = 2009;
        public const long METER_VOLUME_TABLE = 2010;
        public const long METER_MBR_ID = 2011;
        public const long METER_M3_PER_IMPULSE = 2012;
        public const long METER_GAUGE = 2013;
        public const long METER_SHAPE_AND_POSITION = 2014;
        public const long METER_OPERATIONAL_CAPACITY = 2015;
        public const long METER_VOLUME_TABLE_HEIGHT = 2016;
        public const long METER_VOLUME_TABLE_VALUE = 2017;
        public const long METER_FORCED_TEMPERATURE = 2018;
        public const long METER_NOZZLE_CALIBRATION_ERROR = 2019;
        public const long METER_DELIVERY_ADVICE = 2020; //Warto�� ze s�ownika DELIVERY_ADVICE (MustGo, CanGo, itd) - wykorzystywane w DIMA
        public const long METER_TANK_IS_VERTICAL = 2023; // czy poziomy czy pionowy
        public const long METER_TANK_IS_UNDERGROUND = 2024; // czy podziemny, jak nie to naziemny ;)
        public const long METER_SENSOR_HEIGHT_BIAS = 2025;
        public const long METER_TANK_CHASSIS = 2026;
        public const long METER_TANK_WIDTH = 2027;
        public const long METER_LOMOSOFT_TANK_ID = 2028;
        public const long METER_LEARNING_SET_HEIGHT = 2029;
        public const long METER_LEARNING_SET_VOLUME = 2030;
        public const long METER_IMRSC_ID = 2031;    //ID METER'a w bazie IMR_Suite_Core_IMRSC
        public const long METER_TEMPERATURE_SENSOR = 2032;
        public const long METER_FLOW_SENSOR_POSITION = 2033;
        public const long METER_NOZZLE_ID = 2034;
        public const long METER_NOZZLE_IS_ACTIVE = 2035;
        public const long METER_STATUS = 2036;
        public const long METER_PRODUCT_CODE = 2037;
        public const long METER_PRODUCT_NAME = 2038;
        public const long METER_DEPOT_ID = 2039;
        public const long METER_SERIES = 2040;   //typoszereg gazomierza - BK-G4, BK-G6 itp.
        public const long METER_Q_MIN = 2041; //Minimalny przep�yw godzinowy gazomierza - Q_MIN
        public const long METER_Q_MAX = 2042; //Maksymalny przep�yw godzinowy gazomierza - Q_MAX
        public const long METER_SIZE = 2043;//Rozmiar gazomierza
        public const long METER_COUNTER_FORMAT = 2044; //Format liczyd�a gazomierza
        public const long METER_ACTIVE_IN_SUMMER = 2045;
        public const long METER_CALIBRATION_TIME = 2046; //data ostatniej kalibracji 
        public const long METER_MIN_FUEL_HEIGHT = 2047; //minimalna wysoko�� paliwa do przeliczen
        public const long METER_LOGICAL_PARENT_ID = 2048; //
        public const long METER_NOZZLE_TYPE = 2049; //
        public const long METER_SENSOR_FUEL_HEIGHT_BIAS = 2050; //
        public const long METER_SENSOR_WATER_HEIGHT_BIAS = 2051;
        public const long METER_CYLINDER_REPLACED = 2052;
        public const long METER_CYLINDER_SWITCH_SEQUENCE_SUSPECTED = 2053;
        public const long METER_PRODUCTION_DATE = 2054;
        public const long METER_INSTALLATION_DATE = 2055;
        public const long METER_ACO_TOTAL_CONSUMPTION = 2056;
        public const long METER_CYLINDER_WEIGHT_CAPACITY = 2057;
        public const long METER_FINAL_OUTLET_PRESSURE = 2058;
        public const long METER_CYLINDER_STOCK = 2059;
        public const long METER_ACO_KNOB_DIRECTION_DETECTION = 2060;
        public const long METER_CONSUMER_USAGE_TYPE = 2061;
        public const long METER_APPLICATION = 2062;
        public const long METER_DELIVERY_WINDOW_DAYS = 2063;
        public const long METER_DELIVERY_SECTOR = 2064;
        public const long METER_BARCODE = 2065;
        public const long METER_RAPID_TRIGGER_LEVEL = 2066;
        public const long METER_RAPID_ACTIVE = 2067;
        public const long METER_PLANNED_DELIVERY_TIME = 2068;
        public const long METER_SCAN_PERIOD = 2069;
        public const long METER_ARCHIVE_LATCH_PERIOD = 2070;
        public const long METER_DIGITAL_ACTIVE_VALUE = 2071;
        public const long METER_SENSOR_MIN_VALUE = 2072;
        public const long METER_SENSOR_MAX_VALUE = 2073;
        public const long METER_EVENTS_BASED_ON_AVG_ENABLED = 2074;
        public const long METER_LO_PRESSURE_VALUE = 2075;
        public const long METER_LOLO_PRESSURE_VALUE = 2076;
        public const long METER_HI_PRESSURE_VALUE = 2077;
        public const long METER_HIHI_PRESSURE_VALUE = 2078;
        public const long METER_LO_UP_EVENT_ENABLE = 2079;
        public const long METER_LO_DOWN_EVENT_ENABLE = 2080;
        public const long METER_LOLO_UP_EVENT_ENABLE = 2081;
        public const long METER_LOLO_DOWN_EVENT_ENABLE = 2082;
        public const long METER_HI_UP_EVENT_ENABLE = 2083;
        public const long METER_HI_DOWN_EVENT_ENABLE = 2084;
        public const long METER_HIHI_UP_EVENT_ENABLE = 2085;
        public const long METER_HIHI_DOWN_EVENT_ENABLE = 2086;
        public const long METER_STEPREL_PRESSURE_VALUE = 2087;
        public const long METER_STEPREL_EVENT_ENABLE = 2088;
        public const long METER_NVR_UP_EVENT_ENABLE = 2089;
        public const long METER_NVR_DOWN_EVENT_ENABLE = 2090;
        public const long METER_UDI_UP_EVENT_ENABLE = 2091;
        public const long METER_UDI_DOWN_EVENT_ENABLE = 2092;
        public const long METER_NOZZLE_POSITION = 2093;
        public const long METER_NOZZLE_TC_OFFSET = 2094;
        public const long METER_ELECTRONIC_INDEX_SYNC_VALUE = 2095;
        public const long METER_MECHANICAL_INDEX_SYNC_VALUE = 2096;
        public const long METER_TANK_IRC_OFFSET = 2097;
        public const long METER_IGNORE_DEVICE = 2098;
        public const long METER_GBS_ID_TANK = 2099;
        public const long METER_EXCEPTION_CONFIRMED_OPERATOR_ID = 2100;
        public const long METER_PREDICTOR_BLOB = 2101;
        public const long METER_ADAPTER_SERIAL_NBR = 2102;
        public const long METER_LOST_SALES_LAST_CALCULATION_DATE = 2103;
        public const long METER_DIAMETER = 2104;
        public const long METER_LOST_SALE_CALCULATION_STOCK_OUT_BEGIN_DATE = 2105;
        public const long METER_CURRENT_LOST_SALE_BY_STOCK_OUT = 2106;
        public const long METER_INSTALLED_ON_HEAT_WATER = 2107;
        public const long METER_INSTALLED_ON_COLD_WATER = 2108;
        public const long METER_LOST_SALE_CALCULATION_STOCK_OUT_END_DATE = 2109;
        public const long METER_NOZZLE_IS_SUSPENDED = 2110;
        public const long METER_PRESSURE_REFERENCE_PARENT_ID = 2111;
        public const long METER_PRODUCER = 2112;
        public const long METER_COMPARTMENT_COUNT = 2113;
        public const long METER_PROBE_COMPANY = 2114;
        public const long METER_LAST_REVISION_DATE = 2115;
        public const long METER_IS_DOUBLE_WALL = 2116;
        public const long METER_IS_TANK_MONITORED = 2117;
        public const long METER_IS_TANK_SUMP_MONITORED = 2118;
        public const long METER_FUEL_FILLING_POINT_TYPE = 2119;
        public const long METER_WAS_PRODUCT_CHANGED = 2120;
        public const long METER_CLUSTER_ID_LOCATION = 2121;
        public const long METER_PIPE_IN_IS_DOUBLE_WALL = 2122;
        public const long METER_PIPE_IN_IS_DOUBLE_WALL_MONITORED = 2123;
        public const long METER_PIPE_IN_LENGTH = 2124;
        public const long METER_PIPE_IN_MAP = 2125;
        public const long METER_FUEL_DISTRIBUTOR_ID = 2126;
        public const long METER_FUEL_DISTRIBUTOR_PRODUCT_COUNT = 2127;
        public const long METER_TIMESPAN_BETWEEN_REVISIONS = 2128;
        public const long METER_FUEL_TERMINAL_ID = 2129; // id rozlewni
        public const long METER_DECLARED_CONSUMPTION_DIFFERENCE_LAST_CALCULATION_DATE = 2130;
        public const long METER_PREDICTION_UNITED_LAST_LEARNING_TIME = 2131;
        public const long METER_PERMANENT_FLOW_RATE = 2132;
        public const long METER_SECONDARY_COUNTER_ID = 2133;
        public const long SECONDARY_METER_M3_PER_IMPULSE = 2134;
        public const long SECONDARY_METER_SERIAL_NUMBER = 2135;

        #endregion
        #region DATA_SOURCE attributes
        public const long DATA_SOURCE_SERIAL_NBR = 3001;      // Numer seryjny OKO
        public const long DATA_SOURCE_ID_PACKET = 3002;      // Identyfikator pakietu
        public const long DATA_SOURCE_PACKET_TIME = 3003;      // Czas otrzymania pakietu
        public const long DATA_SOURCE_ADD_TIME = 3004;      // <Brak opisu>
        public const long DATA_SOURCE_SESSION_GUID = 3005;      // <Brak opisu>
        public const long DATA_SOURCE_OPERATOR_ID = 3006;
        public const long DATA_SOURCE_FILE_NAME = 3007;
        public const long DATA_SOURCE_ID_ESTIMATION = 3008;
        public const long DATA_SOURCE_PARENT_ID = 3009;
        public const long DATA_SOURCE_DISTRIBUTOR_ID = 3010;
        public const long DATA_SOURCE_NAME = 3011;
        public const long DATA_SOURCE_AGGREGATION_MODULE_NAME = 3012;   //Nazwa modu�u agreguj�cego dane z DATA_ARCH do tabeli DATA_ARCH_AGGREGATED
        public const long DATA_SOURCE_ID_DELIVERY = 3013;
        #endregion
        #region OPERATOR params
        public const long OPERATOR_STOCK_DS_ID_REPORT_DATA = 3501;      // Id typ�w danych (REPORT_DATA_TYPE) jakie s� pobierane przez StockDataService
        public const long OPERATOR_STOCK_DS_ALLOW_PARALLEL_CALLS = 3502;      // Czy zezwala� na r�wnoleg�e wykonywanie funkcji StockDataService
        public const long OPERATOR_STOCK_DS_TIME_TO_CHECK_PARALLEL_CALLS = 3503;      // Czas w [s] ile jest sprawdzane r�wnoleg�e wykonywanie funkcji StockDataService
        public const long OPERATOR_WINDOWS_LOGIN = 3504;      // <Brak opisu>
        public const long OPERATOR_CURRENT_DATA_TIME = 3505;      // <Brak opisu>
        public const long OPERATOR_GROUP_DESCR_DATA_TYPE = 3506;      // <Brak opisu>
        public const long OPERATOR_GROUP_SYMBOL_FILE = 3507;      // <Brak opisu>
        public const long OPERATOR_HIGHEST_LOCATION_GROUP = 3508;      // <Brak opisu>
        public const long OPERATOR_LANGUAGES_TO_LOG = 3509;      // <Brak opisu>
        public const long OPERATOR_LAST_MEASURE_TIME_DATA_TYPE = 3510;      // <Brak opisu>
        public const long OPERATOR_LOCATION_DESCR_DATA_TYPE = 3511;      // <Brak opisu>
        public const long OPERATOR_MAIN_MAP_AUTO_REFRESH = 3512;      // <Brak opisu>
        public const long OPERATOR_ARCH_DATA_DEFAULT_REPORT = 3513;      // <Brak opisu>
        public const long OPERATOR_LAST_DATA_DEFAULT_REPORT = 3514;      // <Brak opisu>
        public const long OPERATOR_POPUP_REPORT_DATA = 3515;      // <Brak opisu>
        public const long OPERATOR_SELECT_ITEM_BORDER_COLOR = 3516;      // <Brak opisu>
        public const long OPERATOR_SHOW_BAD_GROUP_SYMBOL = 3517;      // <Brak opisu>
        public const long OPERATOR_STATION_REPORT_LOCATION_DATA = 3518;      // <Brak opisu>
        public const long OPERATOR_STATION_REPORT_METER_DATA = 3519;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_LOCATION_RED_TIME = 3520;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_LOCATION_YELLOW_TIME = 3521;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_SIZE_LOCATION_GREEN = 3522;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_SIZE_LOCATION_EMPTY = 3523;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_SIZE_LOCATION_GROUP = 3524;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_SIZE_LOCATION_RED = 3525;      // <Brak opisu>
        public const long OPERATOR_SYMBOL_SIZE_LOCATION_YELLOW = 3526;      // <Brak opisu>
        public const long OPERATOR_VISUALIZATION_TIMEOUT = 3527;      // <Brak opisu>
        public const long OPERATOR_REPORTS = 3528;      // <Brak opisu>
        public const long OPERATOR_REPORT_MAX_ELEMENTS = 3529;      // <Brak opisu>
        public const long OPERATOR_REPORT_MAX_INTERVAL = 3530;      // <Brak opisu>
        public const long OPERATOR_STOCK_DS_USE_LOCAL_TIMES = 3531;      // <Brak opisu>
        public const long OPERATOR_CUSTOMER_LIST_LOCATION_DATA = 3532;      // <Brak opisu>
        public const long OPERATOR_STATION_MEASURES_LOC_DATA = 3533;      // <Brak opisu>
        public const long OPERATOR_STATION_MEASURES_MET_DATA = 3534;      // <Brak opisu>
        public const long OPERATOR_STATION_MEASURES_MSR_DATA = 3535;      // <Brak opisu>
        public const long OPERATOR_START_PAGE = 3536;      // <Brak opisu>
        public const long OPERATOR_MAX_PASSWORD_INCORRECT = 3537;      // <Brak opisu>
        public const long OPERATOR_MAX_PASSWORD_CORRECT = 3538;      // <Brak opisu>
        public const long OPERATOR_MAX_DATA_SUBMITTED = 3539;      // <Brak opisu>
        public const long OPERATOR_STATION_MEASURES_WATER_HOURS = 3540;      // <Brak opisu>
        public const long OPERATOR_POPUP_HEADER_DATA_TYPE = 3541;      // <Brak opisu>
        public const long OPERATOR_MAIN_MAP_LIST_SELECTING_MODE = 3542;      // <Brak opisu>
        public const long OPERATOR_LAST_EVENT_RESET_TIME = 3543;      // <Brak opisu>
        public const long OPERATOR_MANUAL_FILE = 3544;      // <Brak opisu>
        public const long OPERATOR_ACTIONS_LOCATIONS_DATA = 3545;      // <Brak opisu>
        public const long OPERATOR_ACTIONS_OPERATORS_DATA = 3546;      // <Brak opisu>
        public const long OPERATOR_DATA_PROPER_ONLY = 3547;      // <Brak opisu>
        public const long OPERATOR_DATA_STATUS_COLORS = 3548;      // <Brak opisu>
        public const long OPERATOR_LOCATION_INFO_LOC_DATA = 3549;      // <Brak opisu>
        public const long OPERATOR_LOCATION_INFO_MET_DATA = 3550;      // <Brak opisu>
        public const long OPERATOR_PATTERN_LOCATION = 3551;      // <Brak opisu>
        public const long OPERATOR_REPORTS_POSSIBLE_LOC_DATA = 3552;      // <Brak opisu>
        public const long OPERATOR_REPORTS_POSSIBLE_MSR_DATA = 3553;      // <Brak opisu>
        public const long OPERATOR_MAIN_MAP_LOC_LEFT_CLICK = 3554;      // <Brak opisu>
        public const long OPERATOR_TOP_BANNER = 3555;      // <Brak opisu>
        public const long OPERATOR_DATA_SOURCE_COLORS = 3556;      // <Brak opisu>
        public const long OPERATOR_MBR_PSION_USER_ID = 3557;
        public const long OPERATOR_PAGE_THEME = 3558;
        public const long OPERATOR_REPORTS_POSSIBLE_MET_DATA = 3559;
        public const long OPERATOR_DS_PASSWORD = 3560;
        public const long OPERATOR_DS_LOC_DATA_TYPE = 3561;
        public const long OPERATOR_DS_MSR_DATA = 3562;
        public const long OPERATOR_DS_MAX_INTERVAL = 3563;
        public const long OPERATOR_DEPOSITORY_CID = 3564;
        public const long OPERATOR_METERS_INFO_MET_DATA = 3565;
        public const long OPERATOR_DS_PREPARE_TIME = 3566;
        public const long OPERATOR_LAST_MEASURE_TIME_ALL_METERS = 3567;
        public const long OPERATOR_POPUP_METER_DATA_TYPE = 3568;
        public const long OPERATOR_CONS_APP_NAME = 3569;
        public const long OPERATOR_TIME_ZONE_ID = 3570;
        public const long OPERATOR_TOP_LOGO = 3571;
        public const long OPERATOR_GMAP_CHART_DATA_TYPE = 3572;
        public const long OPERATOR_GMAP_CHART_DAYS_BACK = 3573;
        public const long OPERATOR_GMAP_GRID_REPORT_DATA_TYPE = 3574;
        public const long OPERATOR_VALVE_CLOSE_ACTION = 3575;
        public const long OPERATOR_VALVE_OPEN_ACTION = 3576;
        public const long OPERATOR_GET_CURRENT_LOCATION_DATA_ACTION = 3577;
        public const long OPEARTOR_ALLOWED_IP = 3578;
        public const long OPERATOR_INCLUDE_SIGNATURE = 3579;
        public const long OPERATOR_DIMA_REPORT_DATA_TYPE = 3580;

        public const long OPERATOR_MDE_TEMPLATE = 3801;
        public const long OPERATOR_FAVOURITE_DATA_TYPE_DEVICE = 3802;
        public const long OPERATOR_FAVOURITE_DATA_TYPE_METER = 3803;
        public const long OPERATOR_FAVOURITE_DATA_TYPE_LOCATION = 3804;

        public const long OPERATOR_CUSTOM_COLUMN_DEVICE = 3810;
        public const long OPERATOR_CUSTOM_COLUMN_METER = 3811;
        public const long OPERATOR_CUSTOM_COLUMN_LOCATION = 3812;
        public const long OPERATOR_CUSTOM_COLUMN_PACKET = 3813;

        public const long OPERATOR_ROUTE_SUFFIX = 3817;
        public const long OPERATOR_MIN_LOG_LEVEL = 3818;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE = 3821;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK = 3822;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ROUTE = 3823;

        public const long OPERATOR_TANK_MONITORING_SUPPORTED_COUNTRIES = 3824;  //Jakie kraje obs�uguje dany konsultant portalu TankMonitoring (All - wszystkie, lub na poszczeg�lnych indexach kody kraj�w)
        public const long OPERATOR_COUNTRY = 3825;  //Z jakiego kraju pochodzi operator (np. utworzony przez TankMonitoring) - wpisujemy kod kraju
        public const long OPERATOR_SKYPE_NAME = 3826; //Skype name dla operatora
        public const long OPERATOR_CONFIMED_BY_OPERATOR_ID = 3827; //ID_OPERATOR konsultanta TankMonitoring kt�ry zatwierdzi� rejestracj� klienta w portalu
        public const long OPERATOR_BUSINESS_TYPE = 3828;//business type u�ytkownika wybrany podczas rejestracji
        public const long OPERATOR_COMPANY_NAME = 3829;//nazwa firmy uzytkownika podana przy rejestracji
        public const long OPERATOR_ID_IMR_SUITE_CORE = 3830;//operator id w bazie Imr_Suite_CORE, mo�e by� inny ni� w Imr_Suite_CORE_IMRSC
        public const long OPERATOR_ACTOR_ID_IMR_SUITE_CORE = 3831;//aktor id w bazie Imr_Suite_CORE, mo�e by� inny ni� w Imr_Suite_CORE_IMRSC
        public const long OPERATOR_ID_MODULE = 3832;//identyfikator modu�u do kt�rego mo�e si� zalogowa� operator

        public const long OPERATOR_IMR_ST_OPERATOR_RESPONSIBILITIES = 3833; //zakres odpowiedzialno�ci operatora
        public const long OPERATOR_SOT_ADDITIONAL_PACKAGE_SEND = 3834;//id operatora do ktorego SOT moze dodatkowo wysylac paczke
        public const long OPERATOR_SOT_CORRESPONDENCE_ADDRESS = 3835;
        public const long OPERATOR_SOT_CORRESPONDENCE_CITY = 3836;
        public const long OPERATOR_SOT_CORRESPONDENCE_ZIP_CODE = 3837;
        public const long OPERATOR_SOT_CONTRACT = 3838;
        public const long OPERATOR_SOT_CONTRACT_STATE = 3839;
        public const long OPERATOR_SOT_CONTRACT_FROM = 3840;
        public const long OPERATOR_SOT_CONTRACT_TO = 3841;
        public const long OPERATOR_SOT_CONTRACT_ADDITIONAL_INFO = 3842;
        public const long OPERATOR_SOT_SEP_E = 3843;
        public const long OPERATOR_SOT_SEP_E_TO = 3844;
        public const long OPERATOR_SOT_SEP_G = 3845;
        public const long OPERATOR_SOT_SEP_G_TO = 3846;

        public const long OPERATOR_SOT_AMERIGAS_PASS_NBR = 3847;
        public const long OPERATOR_SOT_AMERIGAS_PASS_TO = 3848;
        public const long OPERATOR_SOT_SHELL_PASS_NBR = 3849;
        public const long OPERATOR_SOT_SHELL_PASS_TO = 3850;
        public const long OPERATOR_SOT_GASPOL_PASS_NBR = 3851;
        public const long OPERATOR_SOT_GASPOL_PASS_TO = 3852;
        public const long OPERATOR_SOT_BP_PASS_NBR = 3853;
        public const long OPERATOR_SOT_BP_PASS_TO = 3854;
        public const long OPERATOR_SOT_BHP_TRAINING = 3855;
        public const long OPERATOR_SOT_CORRESPONDENCE_ADDRESS_LATITUDE = 3856;
        public const long OPERATOR_SOT_CORRESPONDENCE_ADDRESS_LONGITUDE = 3857;
        public const long OPERATOR_SOT_FINANCIAL_CONDITIONS_ID_DISTRIBUTOR = 3858;
        public const long OPERATOR_SOT_FINANCIAL_CONDITIONS_ID_TASK_TYPE = 3859;
        public const long OPERATOR_SOT_FINANCIAL_CONDITIONS_AMOUNT = 3860;

        public const long OPERATOR_IMR_SERVER_ID = 3861;
        public const long OPERATOR_IMR_SERVER_OPERATOR_ID = 3862;
        public const long OPERATOR_IMR_SERVER_ACTOR_ID = 3863;
        public const long OPERATOR_LOAD_LOCATIONS_ASSOCIATIONS = 3864;
        public const long OPERATOR_LOAD_LOCATIONS_ACTIVE_ASSOCIATIONS = 3865;
        public const long OPERATOR_SHOW_LOCATIONS_ASSOCIATIONS = 3866;
        public const long OPERATOR_SHOW_LOCATIONS_ACTIVE_ASSOCIATIONS = 3867;

        public const long OPERATOR_SYMBOL_SIZE_LOCATION = 3868;
        public const long OPERATOR_COORDINATES_FORMAT = 3869;
        public const long OPERATOR_SOT_AREA = 3870;
        public const long OPERATOR_CARD_TYPE = 3872;
        public const long OPERATOR_CARD_PHONE1 = 3873;
        public const long OPERATOR_CARD_PHONE2 = 3874;
        public const long OPERATOR_CARD_EMAIL = 3875;
        public const long OPERATOR_CARD_IMG_NAME = 3876;
        public const long OPERATOR_CARD_ID = 3877;
        public const long OPERATOR_PROTECTOR_CARD_ID = 3878;
        public const long OPERATOR_SOT_ALTITUDE_TEST = 3886;
        public const long OPERATOR_DISTRIBUTOR_AUTOUPDATE = 3887;
        public const long OPERATOR_PROFILE_TYPE = 3888;
        public const long OPERATOR_PROBLEM_VALIDITY = 3889;

        public const long OPERATOR_USERPORTAL_SETTINGS = 3900;
        public const long OPERATOR_DIMA_LOCATION_GROUP_TAB = 3901;

        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_SHIPPING_LIST_CLOSE = 3902;

        public const long OPERATOR_SKIP_METER_TYPE_DEVICE_TYPE_CHECK = 3903;
        public const long OPERATOR_ALLOWED_METER_TYPE_GROUP = 3904;

        public const long OPERATOR_RADIO_DEVICE_TESTER_NEW_ID = 3905;

        public const long OPERATOR_FITTER_MESSAGE_CONTENT_ID = 3906;
        public const long OPERATOR_FITTER_MESSAGE_READ = 3907;
        public const long OPERATOR_FITTER_MESSAGE_VISIBLE = 3908;

        public const long OPERATOR_IMRSC_EXCLUDE_FROM_SYNCHRONIZATION = 3909;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_PACKAGE = 3910;

        public const long OPERATOR_DIMA_INSTALLATION_METER_VISIBLE = 3911;

        public const long OPERATOR_IMR_SERVER_OPERATOR_LOGIN = 3912;
        public const long OPERATOR_IMR_SERVER_OPERATOR_PASSWORD = 3913;
        public const long OPERATOR_IMR_SERVER_OPERATOR_SERIAL_NBR = 3914;
        public const long OPERATOR_FITTER_AREA_RADIUS = 3915;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR = 3916;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR = 3917;
        public const long OPERATOR_ALL_ALLOWED_DISTRIBUTOR = 3918;

        public const long OPERATOR_FP_MANUAL_FILE = 3919;
        public const long OPERATOR_SIGNATURE = 3920;

        public const long OPERATOR_SOT_FINANCIAL_CONDITIONS_ID_TASK_GROUP = 3921;

        public const long OPERATOR_FP_CARRIER = 3922;

        public const long OPERATOR_SITA_UPDATE_LINK = 3923;
        public const long OPERATOR_SITA_VERSION = 3924;

        public const long OPERATOR_SITA_DEFAULT_METER_TYPE_ID = 3925;
        public const long OPERATOR_SITA_DEFAULT_PULSE_WEIGHT = 3926;
        public const long OPERATOR_DEFAULT_COUNTER_FORMAT = 3927;
        public const long OPERATOR_SERVICE_WIZARD = 3928;

        public const long OPERATOR_GCM_REGISTRATION_ID = 3929;
        public const long OPERATOR_SITA_INSTALLATION_FORM_TYPE = 3930;
        public const long OPERATOR_GCM_MOBILE_DEVICE_UNIQUE_ID = 3931;

        public const long OPERATOR_DASHBOARD_INDICATOR_TYPE = 3932;
        public const long OPERATOR_DASHBOARD_INDICATOR_CLASS = 3933;
        public const long OPERATOR_DASHBOARD_INDICATOR_PROCEDURE = 3934;
        public const long OPERATOR_DASHBOARD_INDICATOR_REFERENCE_TYPE = 3935;
        public const long OPERATOR_DASHBOARD_INDICATOR_GROUP = 3936;
        public const long OPERATOR_DASHBOARD_GROUP = 3937;
        public const long OPERATOR_DASHBOARD_GROUP_NAME = 3938;
        public const long OPERATOR_DASHBOARD_GROUP_ID_DESCR = 3939;
        public const long OPERATOR_DASHBOARD_GROUP_BORDER_VISIBLE = 3940;

        public const long OPERATOR_SITA_DEFAULT_CONTROL_NAME = 3941;
        public const long OPERATOR_SITA_DEFAULT_CONTROL_VALUE = 3942;

        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION = 3943;
        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR = 3944;

        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE = 3945;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_TASK = 3946;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_ROUTE = 3947;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_PACKAGE = 3948;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_LOCATION = 3949;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR = 3950;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR = 3951;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR = 3952;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_SHIPPING_LIST_CLOSE = 3953;
        public const long OPERATOR_FREQ_REASON_ENABLED = 3954;

        public const long OPERATOR_ADVANCED_SYMBOL_LOCATION_RED_TIME = 3955;
        public const long OPERATOR_ADVANCED_SYMBOL_LOCATION_YELLOW_TIME = 3956;

        public const long OPERATOR_VALIDITY_FROM_TIME = 3957; // data poczatkowa waznosci konta operatora
        public const long OPERATOR_VALIDITY_TO_TIME = 3958; // data koncowa waznosci konta operatora

        public const long OPERATOR_BLOCKED_TO_TIME = 3959;
        public const long OPERATOR_PASSWORD_SALT = 3960;

        public const long OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE = 3961;
        public const long OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE = 3962;

        public const long OPERATOR_PASSWORD_CREATION_TIME = 3963;
        public const long OPERATOR_CHANGE_PASSWORD_DURING_NEXT_LOGIN = 3964;

        public const long OPERATOR_INVALID_PASSWORD_RETRY_TIME = 3965;
        public const long OPERATOR_BLOCKED_RETRY_TIME = 3966;

        public const long OPERATOR_DASHBOARD_GROUP_REFRESH_PERIOD = 3967;
        public const long OPERATOR_DASHBOARD_GROUP_WIDTH = 3968;
        public const long OPERATOR_DASHBOARD_INDICATOR_WIDTH = 3969;

        public const long OPERATOR_SITA_CURRENT_SITA_VERSION = 3970;
        public const long OPERATOR_SITA_CURRENT_SITA_VERSION_UPDATE_TIME = 3971;

        public const long OPERATOR_VERSION_STATE_FLOW_PREV = 3972;
        public const long OPERATOR_VERSION_STATE_FLOW_NEXT = 3973;

        public const long OPERATOR_FITTER_DEPOSITORY_REQUIRED = 3974;
        public const long OPERATOR_FITTER_DEPOSITORY_CONTENT_MODE = 3975;

        public const long OPERATOR_SMARTGAS_UPDATE_LINK = 3976;
        public const long OPERATOR_SMARTGAS_VERSION = 3977;
        public const long OPERATOR_CURRENT_SMARTGAS_VERSION = 3978;

        public const long OPERATOR_DASHBOARD_INDICATOR_IS_ALARM = 3979;
        public const long OPERATOR_DASHBOARD_INDICATOR_ID_MODULE = 3980;
        public const long OPERATOR_DASHBOARD_GROUP_ID_MODULE = 3981;

        public const long OPERATOR_LOCAL_SETTINGS = 3982;
        public const long OPERATOR_LOCAL_SETTINGS_ID_MODULE = 3983;

        public const long OPERATOR_IMR_SERVER_OPERATOR_SALT = 3984;

        public const long OPERATOR_DASHBOARD_FIT_TO_SCREEN = 3985;
        public const long OPERATOR_DASHBOARD_USE_MARGIN = 3986;
        public const long OPERATOR_DASHBOARD_USE_PADDING = 3987;

        public const long OPERATOR_DASHBOARD_KEEP_ASPECT_RATIO = 3988;
        public const long OPERATOR_DASHBOARD_WIDTH = 3989;
        public const long OPERATOR_DASHBOARD_MAX_WIDTH = 3990;
        public const long OPERATOR_DASHBOARD_MAX_HEIGHT = 3991;

        #endregion
        #region ORDER params
        public const long ORDER_CREATION_DATE = 20001;  //Data utworzenia zam�wienia

        public const long ORDER_PACKAGE_TYPE_ID = 20002;            //Zawarto�� zam�wienia - jaki typ zestawu jest w zam�wieniu (jego ID_PACKAGE_TYPE z UTD.dbo.PACKAGE_TYPE)
        public const long ORDER_PACKAGE_TYPE_ITEM_PRICE = 20003;    //Zawarto�� zam�wienia - cena po jakiej sprzedano egzemplarz typu zestawu w chwili zapisu zam�wienia (cena mo�e si� zmieni� w czasie wi�c zapisujemy to w historii)
        public const long ORDER_PACKAGE_TYPE_CURRENCY = 20004;      //Zawarto�� zam�wienia - waluta w jakiej sprzedano egzemplarz typu zestawu w chwili zapisu zam�wienia 
        public const long ORDER_PACKAGE_TYPE_COUNT = 20005;         //Zawarto�� zam�wienia - ilo�� sztuk typu zestawu jaki sprzedano w ramach zam�wienia

        public const long ORDER_ADDITIONAL_ITEM_PDT_ID = 20006;     //Zawarto�� zam�wienia - elementy dodatkowe - identyfikator nazwy elementu dodatkowego (Rochester JR gauge, Rochester SR gauge, Magnatel adapter) - jest do ID_PACKAGE_TYPE_DATA z tabeli UTD.dbo.PACKAGE_TYPE_DATA zawieraj�cy nazw� danego elementu
        public const long ORDER_ADDITIONAL_ITEM_PRICE = 20007;      //Zawarto�� zam�wienia - elementy dodatkowe - cena po jakiej sprzedano egzemplarz elementu dodatkowego w chwili zapisu zam�wienia
        public const long ORDER_ADDITIONAL_ITEM_CURRENCY = 20008;   //Zawarto�� zam�wienia - elementy dodatkowe - waluta w jakiej sprzedano egzemplarz elementu dodatkowego w chwili zapisu zam�wienia
        public const long ORDER_ADDITIONAL_ITEM_COUNT = 20009;      //Zawarto�� zam�wienia - elementy dodatkowe - ilo�� element�w dodatkowych jakie zosta�y sprzedane w ramach zam�wienia

        public const long ORDER_FINAL_TOTAL_PRICE = 20010;                  //Ostateczna cena zam�wienia ustalana przez konsultanta (mo�e tam wpisa� swoj� cen� na podstawie ustnego/mailowego ustalenia z klientem)
        public const long ORDER_FINAL_TOTAL_PRICE_JUSTIFICATION = 20011;    //Uzasadnienie dla kt�rego ostateczna cena zosta�a zmieniona przez konsultanta

        public const long ORDER_COMPANY_NAME = 20012;
        public const long ORDER_COMPANY_VAT_NBR = 20013;
        public const long ORDER_COMPANY_STREET = 20014;
        public const long ORDER_COMPANY_ZIP = 20015;
        public const long ORDER_COMPANY_CITY = 20016;
        public const long ORDER_COMPANY_COUNTRY = 20017;
        public const long ORDER_BILLING_ADDRESS_STREET = 20018;
        public const long ORDER_BILLING_ADDRESS_ZIP = 20019;
        public const long ORDER_BILLING_ADDRESS_CITY = 20020;
        public const long ORDER_BILLING_ADDRESS_COUNTRY = 20021;
        public const long ORDER_CONTACT_PERSON_NAME = 20022;
        public const long ORDER_CONTACT_PERSON_SURNAME = 20023;
        public const long ORDER_CONTACT_PERSON_EMAIL = 20024;
        public const long ORDER_CONTACT_PERSON_PHONE = 20025;
        public const long ORDER_CONTACT_PERSON_SKYPE = 20026;
        public const long ORDER_SHIPPING_WAY = 20027;
        public const long ORDER_TRANSPORT_WITH_ASSURANCE = 20028;
        public const long ORDER_TRANSPORT_CARRIER_NAME = 20029;
        public const long ORDER_TRANSPORT_CUSTOMER_NUMBER = 20030;



        #endregion
        #region REPORT params
        public const long REPORT_NAME = 12001;     // <Brak opisu>
        public const long REPORT_DESCRIPTION = 12002;     // <Brak opisu>
        public const long REPORT_START_DAYS_BACK = 12003;     // <Brak opisu>
        public const long REPORT_END_DAYS_BACK = 12004;     // <Brak opisu>
        public const long REPORT_START_MINUTES_BACK = 12005;     // <Brak opisu>
        public const long REPORT_END_MINUTES_BACK = 12006;     // <Brak opisu>
        public const long REPORT_SCHEDULE_FREQUENCY_VALUE = 12007;     // <Brak opisu>
        public const long REPORT_SCHEDULE_FREQUENCY_UNIT = 12008;     // <Brak opisu>
        public const long REPORT_SCHEDULE_TOLERANCE_BACK_VALUE = 12009;     // <Brak opisu>
        public const long REPORT_SCHEDULE_TOLERANCE_BACK_UNIT = 12010;     // <Brak opisu>
        public const long REPORT_SCHEDULE_TOLERANCE_FORW_VALUE = 12011;     // <Brak opisu>
        public const long REPORT_SCHEDULE_TOLERANCE_FORW_UNIT = 12012;     // <Brak opisu>
        public const long REPORT_SCHEDULE_MODE = 12013;     // <Brak opisu>
        public const long REPORT_SCHEDULE_EMPTY_ROWS = 12014;     // <Brak opisu>
        public const long REPORT_LOCATION_DATA = 12015;     // <Brak opisu>
        public const long REPORT_METER_DATA = 12016;     // <Brak opisu>
        public const long REPORT_DEVICE_DATA = 12017;     // <Brak opisu>
        public const long REPORT_MEASURES_DATA = 12018;     // <Brak opisu>
        public const long REPORT_PAGE_SIZE = 12019;     // <Brak opisu>
        public const long REPORT_INCLUDE_METERS_IN_ROWS = 12020;     // <Brak opisu>
        public const long REPORT_INCLUDE_DEVICES_IN_ROWS = 12021;     // <Brak opisu>
        public const long REPORT_START_TIME = 12022;     // <Brak opisu>
        public const long REPORT_END_TIME = 12023;     // <Brak opisu>
        public const long REPORT_USE_SCHEDULE = 12024;     // <Brak opisu>
        public const long REPORT_STORED_PROCEDURE = 12025;     // <Brak opisu>
        public const long REPORT_SAVE_PATH_MANUALLY_CREATED = 12026;
        public const long REPORT_SOURCE_SERVER_ID = 12027;//ID_REPORT
        public const long REPORT_IMR_SERVER_ID = 12028;//ID_IMR_SERVER
        public const long REPORT_FIRST_GENERATION_DATE = 12029;//Data pierwszego wygenerowania raportu
        public const long REPORT_DATA_GENERATION_CHECK = 12030;
        public const long REPORT_EMAIL_SENT_CHECK = 12031;
        public const long REPORT_FTP_SENT_CHECK = 12032;
        public const long REPORT_ID = 12033;
        public const long REPORT_TYPE = 12034;
        public const long REPORT_STATE = 12035;
        public const long REPORT_OPERATOR_NAME = 12036;
        public const long REPORT_STATE_TIME = 12037;
        public const long REPORT_VALUE_BOOL = 12038;
        public const long REPORT_VALUE_VAR = 12039;
        public const long REPORT_VALUE_NOTE = 12040;
        public const long REPORT_DEADLINE = 12041;
        public const long REPORT_STATE_NOTE = 12042;
        public const long REPORT_PRIORITY = 12043;
        public const long REPORT_REASON = 12044;
        public const long REPORT_GUID = 12045;
        public const long REPORT_USE_CORE_DATABASE = 12046;
        public const long REPORT_PDF_EXPORT_OPTIONS = 12047;
        public const long REPORT_XLS_EXPORT_OPTIONS = 12048;
        public const long REPORT_XLSX_EXPORT_OPTIONS = 12049;
        public const long REPORT_CSV_EXPORT_OPTIONS = 12050;
        public const long REPORT_TXT_EXPORT_OPTIONS = 12051;
        public const long REPORT_ID_DISTRIBUTOR = 12052;
        public const long REPORT_SSRS_ADDRESS = 12053;
        #endregion
        #region MODULE params
        //DAQ_ROUTER 4501- 4529
        public const long DAQ_ROUTER_CACHE_TIME_FOR_APN_DEVICES = 4501;      // [min] czas "zycia" w cache dla urzadzen w prywatnym APN
        public const long DAQ_ROUTER_CACHE_TIME_FOR_NO_APN_DEVICES = 4502;      // [min] czas "zycia" w cache dla urzadzen w publicznym APN
        public const long DAQ_ROUTER_AMOUNT_OF_SIMULTANEOUS_PACKET = 4510;      // ilosc pakietow(zebranych w buf) po ktorej nastapi zapis do DB
        public const long DAQ_ROUTER_FLUSH_TIME_PACKET = 4511;      // [sec] po ktorym nastapi zapis pakietow do DB (niezaleznie od ilosci zebranych pakietow w buf)
        public const long DAQ_ROUTER_AMOUNT_OF_SIMULTANEOUS_PACKET_TRASH = 4512;      // ilosc blednych pakietow(zebranych w buf) po ktorej nastapi zapis do DB
        public const long DAQ_ROUTER_FLUSH_TIME_PACKET_TRASH = 4513;      // [sec] po ktorym nastapi zapis blednych pakietow do DB (niezaleznie od ilosci zebranych pakietow w buf)
        public const long DAQ_ROUTER_RETRY_INSERT_PACKET = 4514;      // ilosc prob wstawienia pakietu w przypadku blednych BulkInsertow
        public const long DAQ_ROUTER_RETRY_INSERT_PACKET_TRASH = 4515;      // ilosc prob wstawienia blednego pakietu w przypadku blednych BulkInsertow
        public const long DAQ_ROUTER_MAX_THREADS_IN_POOL = 4516;      // maksymalna liczba watkow jaka moze zostac utworzona podczas obslugi jobow
        public const long DAQ_ROUTER_CACHE_TIME_FOR_IV = 4517;
        public const long DAQ_ROUTER_CACHE_SAVE_IV_INTERVAL = 4518;
        
        //DAQ_TRANSMISSION 4530 - 4539
        public const long DAQ_TRANSMISSION_DRIVERS_CHECK_AND_RESTART_DRIVERS_TIME = 4530;      // [min] czas (cykliczny) po ktorym beda sprawdzane (czy dzialaja, startowane) drivery transmisyjne. 0(zero) znaczy nie sprawdzaj. Default=10
        public const long DAQ_TRANSMISSION_DRIVERS_CHECK_AND_RESTART_SERVICE_TIME = 4531;      // [min] czas (cykliczny) po ktorym bedzie sprawdzane (tylkko dla trybu Remote) polaczenie ze zdalnym serwisem. 0(zero)-nie sprawdzaj. Default=10
        public const long DAQ_TRANSMISSION_DRIVERS_FREE_ID = 4532;
        public const long DAQ_TRANSMISSION_AMOUNT_OF_SIMULTANEOUS_PACKET = 4533;      // ilosc pakietow(zebranych w buf) po ktorej nastapi przes�anie do Router 
        public const long DAQ_TRANSMISSION_FLUSH_TIME_PACKET = 4534;      // [sec] po ktorym nastapi przes�anie pakiet�w do Router (niezaleznie od ilosci zebranych pakietow w buf)
        public const long DAQ_TRANSMISSION_RETRY_SEND_PACKET = 4535;      // ilosc prob wys�ania pakietu w przypadku bledn�w

        //DAQ_CONTROLLER 4540 - 4559
        //FILL if needed 4540 - 4543!!
        public const long DAQ_CONTROLLER_INIT_CACHE = 4544;                          // czy inicjalizowa�/�adowa� cache
        public const long DAQ_CONTROLLER_CACHE_TIME_FOR_DEVICES = 4545;              // [min] czas "zycia" w cache dla urzadzen
        public const long DAQ_CONTROLLER_AMOUNT_OF_SIMULTANEOUS_DATA = 4546;         // ilosc danych[DataArch](zebranych w buf) po ktorej nastapi zapis do DB
        public const long DAQ_CONTROLLER_FLUSH_TIME_DATA = 4547;                     // [sec] czas po ktorym nastapi zapis danych[DataArch] do DB (niezaleznie od ilosci zebranych danych w buf)
        public const long DAQ_CONTROLLER_RETRY_INSERT_DATA = 4548;		             // ilosc prob wstawienia danych[DataArch] w przypadku blednych BulkInsertow
        public const long DAQ_CONTROLLER_AMOUNT_OF_SIMULTANEOUS_DATA_TRASH = 4549;   // ilosc blednych danych[DataArchTrash](zebranych w buf) po ktorej nastapi zapis do DB
        public const long DAQ_CONTROLLER_FLUSH_TIME_DATA_TRASH = 4550;		         // [sec] czas po ktorym nastapi zapis blednych danych[DataArchTrash] do DB (niezaleznie od ilosci zebranych danych w buf)
        public const long DAQ_CONTROLLER_RETRY_INSERT_DATA_TRASH = 4551;		     // ilosc prob wstawienia blednej danej[DataArchTrash] w przypadku blednych BulkInsertow
        public const long DAQ_CONTROLLER_AMOUNT_OF_SIMULTANEOUS_ACTION_PACKET = 4552;// ilosc danych[tabela ACTION_PACKET](zebranych w buf) po ktorej nastapi zapis do DB
        public const long DAQ_CONTROLLER_FLUSH_TIME_ACTION_PACKET = 4553;		     // [sec] czas po ktorym nastapi zapis danych[tabela ACTION_PACKET] do DB (niezaleznie od ilosci zebranych danych w buf)
        public const long DAQ_CONTROLLER_RETRY_INSERT_ACTION_PACKET = 4554;		     // ilosc prob wstawienia danych[tabela ACTION_PACKET] w przypadku blednych BulkInsertow
        public const long DAQ_CONTROLLER_MAX_THREADS_IN_POOL = 4555;                 // maksymalna liczba watkow jaka moze zostac utworzona podczas obslugi jobow
        public const long DAQ_CONTROLLER_ACTION_TO_DW_QUEUE_TIMEOUT = 4556;          // czas przechowywania akcji w kolejce do DW
        public const long DAQ_CONTROLLER_AMOUNT_OF_SIMULTANEOUS_ACTION = 4557;       // ilosc ackcji/pomiar�w(zebranych w buf) po ktorej nastapi przes�anie do CORE
        public const long DAQ_CONTROLLER_FLUSH_TIME_ACTION = 4558;                   // [sec] po ktorym nastapi przes�anie akcji/pomiarow do CORE (niezaleznie od ilosci zebranych akcji/pomiarow w buf)
        public const long DAQ_CONTROLLER_RETRY_SEND_ACTION = 4559;                   // ilosc prob wys�ania akcji/pomiarow w przypadku bledn�w
        //CORE_ACTIONS 4560 - 4564
        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_HISTORY_INFO = 4560;      // ilosc HistoryInfo(zebranych w buf) po ktorej nastapi zapis do DB
        public const long CORE_ACTIONS_FLUSH_TIME_HISTORY_INFO = 4561;      // [sec] czas po ktorym nastapi zapis HistoryInfo do DB (niezaleznie od ilosci zebranych w buf)
        public const long CORE_ACTIONS_RETRY_INSERT_HISTORY_INFO = 4562;      // ilosc prob wstawienia HistoryInfo w przypadku blednych insertow
        public const long CORE_ACTIONS_MAX_THREADS_IN_POOL = 4563;		 // maksymalna liczba watkow jaka moze zostac utworzona podczas obsulgi akcji
        public const long CORE_ACTIONS_CACHE_TIME_FOR_DEVICES = 4564;      // [min] czas "zycia" w cache dla urzadzen
        //MON_MONITORING 4565 - 4569
        public const long MON_MONITORING_WATCHER_LOOP_DELAY = 4565;
        public const long MON_MONITORING_WATCHER_TRANSIENT_TIME = 4566;
        public const long MON_MONITORING_ALARM_KEEP_TIME = 4567;
        public const long MON_MONITORING_WATCHER_DB_TYPE = 4568;

        //CORE_ALARMS 4600 - 4649
        public const long CORE_ALARMS_SQL_MAIL_PROFILE_NAME = 4600;      // <Brak opisu>
        public const long CORE_ALARMS_DEFAULT_MAIL_SUBJECT = 4601;      // <Brak opisu>
        public const long CORE_ALARMS_DEFAULT_SENDER = 4602;      // <Brak opisu>

        //DW_ETL 4650 - 4679
        public const long DW_ETL_SIMULTANEOUS_PACKETS_TO_DATA_ARCH = 4650;
        public const long DW_ETL_FLUSH_TIME_DATA_ARCH = 4651;
        public const long DW_ETL_RETRY_INSERT_DATA_ARCH = 4652;
        public const long DW_ETL_DEVICE_FILTER = 4653;
        public const long DW_ETL_MAIN_BATCH_SIZE = 4654;
        public const long DW_ETL_RETRY_BATCH_SIZE = 4655;
        public const long DW_ETL_MAIN_BATCH_TIMEOUT = 4656;
        public const long DW_ETL_RETRY_BATCH_TIMEOUT = 4657;
        //4680 - 4699 DW_CONTROLLER
        public const long DW_CONTROLLER_CHANGE_IN_TABLE_MAX_TIME_IN_QUEUE = 4680;// [min] timeout z jakim przekazywane s� zmiany do kolejek innych modu��w default: 10 min



        //OTHERS
        public const long SITA_VERSION = 4700;
        public const long SITA_UPDATE_PATH = 4701;

        public const long SMARTGAS_UPDATE_LINK = 4710;
        public const long SMARTGAS_VERSION = 4711;

        public const long SERVICE_CENTRE_FTP_ENABLED = 4750;      // <Brak opisu>
        public const long SERVICE_CENTRE_FTP_ADDRESS = 4751;      // <Brak opisu>
        public const long SERVICE_CENTRE_FTP_LOGIN = 4752;      // <Brak opisu>
        public const long SERVICE_CENTRE_FTP_PASSWORD = 4753;      // <Brak opisu>

        public const long TANK_MONITORING_UTD_INTERFACE_URL = 4754;     //URL do webservice'u UTD z kt�rego korzysta TANK_MONITORING
        public const long TANK_MONITORING_WEBIMR_URL = 4755;            //URL do strony logowania wizualizacji WebIMR z kt�rej b�dzie korzysta� TANK_MONITORING np. http://tele.aiut.com.pl/imr.suite.webimr.02/Pages/General/Login.aspx
        public const long TANK_MONITORING_TMQ_EMAIL_GROUP = 4756;       //adres email pod kt�ry s� wysy�ane maile odno�nie zestawu kt�ry zosta� zam�wiony i kt�ry nale�y przygotowa�
        public const long TANK_MONITORING_MAINTENANCE_GROUP = 4757;     //adresy mailowe do otrzymania TankMonitoring
        public const long TANK_MONITORING_WEB_IMR_LIVE_DEMO_OPERATOR_ID = 4758; //id operatora TankMonitoring na serwerze imr02a, ktory ma zadanie pokazac niezarejestrowanemu klientowi jak dziala WebIMR

        public const long SMART_GAS_METERING_WEB_IMR_LIVE_DEMO_OPERATOR_ID = 4759;

        public const long SMART_GAS_METERING_REPORT_TIMEOUT_IN_SECS = 4760; //Timeout wykonania raportu w SGM
        public const long DAILY_CONSUMPTION_SUMMARY_HOUR = 4761;    //Godzina pocz�tku doby gazowniczej
        public const long DAILY_CONSUMPTION_SUMMARY_MINUTE = 4762;    //Minuta pocz�tku doby gazowniczej
        public const long DOMAIN_NAME = 4763;
        public const long DOMAIN_LOGGING_ENABLED = 4764;
        public const long DOMAIN_GUID = 4765;
        public const long MONTHLY_CONSUMPTION_SUMMARY_DAY = 4766;
        public const long CORE_ACTIONS_DAYS_ACTION_EXPIRED = 4767;
        public const long CORE_ACTIONS_CHECK_OLD_ACTIONS_PERIOD = 4768;

        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_STATUS_INFO = 4769;
        public const long CORE_ACTIONS_FLUSH_TIME_STATUS_INFO = 4770;
        public const long CORE_ACTIONS_RETRY_CHANGE_STATUS = 4771;
        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_ACTION_INFO = 4772;
        public const long CORE_ACTIONS_FLUSH_TIME_ACTION_INFO = 4773;
        public const long CORE_ACTIONS_RETRY_INSERT_ACTION = 4774;

        public const long NUMBER_OF_RECORDS_TO_READ_FROM_DAILY_ARCHIVE = 4775; //Ilosc rekord�w do odczytania w archiwum dobowym APULSE podczas odczytu po interfejsie optycznym. Np. wartosc 31 - odczytuje 31 dni wstecz.
        public const long NUMBER_OF_RECORDS_TO_READ_FROM_PERIODIC_ARCHIVE = 4776; //Ilosc rekord�w do odczytania w archiwum okresowym APULSE podczas odczytu po interfejsie optycznym. Np. wartosc 12 - odczytuje 12 miesiecy wstecz.
        public const long MODULE_ALL_ALLOWED_DISTRIBUTOR = 4777;//Wymusza branie pod uwag� wszystkich dost�pnych dystrybutor�w

        public const long DFL_SCADA_EXPORT_DB_HOST = 4778;
        public const long DFL_SCADA_EXPORT_DB_PORT = 4779;
        public const long DFL_SCADA_EXPORT_DB_SID = 4780;
        public const long DFL_SCADA_EXPORT_DB_USERNAME = 4781;
        public const long DFL_SCADA_EXPORT_DB_PASSWORD = 4782;
        public const long DFL_SCADA_EXPORT_DB_TABLE_NAME = 4783;

        public const long WCF_BILLING_USER = 4784;
        public const long WCF_BILLING_PASSWORD = 4785;
        public const long WCF_BILLING_BINDING = 4786;
        public const long WCF_BILLING_URL = 4787;

        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_ACTION_DATA_INFO = 4788;
        public const long CORE_ACTIONS_FLUSH_TIME_ACTION_DATA_INFO = 4789;
        public const long CORE_ACTIONS_RETRY_INSERT_ACTION_DATA = 4790;
        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_ACTION_DATA_UPDATE_INFO = 4791;
        public const long CORE_ACTIONS_AMOUNT_OF_SIMULTANEOUS_DATA_STATUS_FLAG_INFO = 4792;
        public const long CORE_ACTIONS_FLUSH_TIME_ACTION_DATA_UPDATE_INFO = 4793;
        public const long CORE_ACTIONS_RETRY_UPDATE_ACTION_DATA = 4794;

        public const long WEBSIMAX_PASSWORD_RESET_REDIRECTION_URL = 4795;

        public const long ACTIVE_LOCATION_VIEW = 4796;
        public const long ACTIVE_DEVICE_VIEW = 4797;
        public const long ACTIVE_METER_VIEW = 4798;
        public const long ACTIVE_OPERATOR_VIEW = 4799;
        public const long ACTIVE_ISSUE_VIEW = 4800;
        public const long ACTIVE_TASK_VIEW = 4801;
        public const long ACTIVE_DISTRIBUTOR_VIEW = 4802;
        public const long ACTIVE_ALARM_VIEW = 4803;
        public const long ACTIVE_ACTOR_VIEW = 4804;
        public const long ACTIVE_ROUTE_VIEW = 4805;
        public const long ACTIVE_ACTION_VIEW = 4806;
        public const long ACTIVE_SIM_CARD_VIEW = 4807;
        public const long ACTIVE_REPORT_VIEW = 4808;
        public const long ACTIVE_ALARM_EVENT_VIEW = 4809;
        public const long ACTIVE_DEVICE_CONNECTION_VIEW = 4810;
        
        public const long SITA_WALKEDBY_FRAME_TYPE = 4811;
        public const long SITA_CONTAINS_ENCRYPTION_KEYS = 4812;
        public const long SITA_WALKEDBY_FRAME = 4813;//public const long SITA_WALKEDBY_FRAME = 4810;

        #endregion
        #region DRIVER params
        public const long TRANSMISSION_DRIVER_ID_DISTRIBUTOR = 5001;
        public const long TRANSMISSION_DRIVER_ID_TRANSMISSION_DISTRIBUTOR = 5002;
        public const long TRANSMISSION_DRIVER_MOBILE_NETWORK_CODE = 5003;
        public const long TRANSMISSION_DRIVER_SERIAL_PORT_NUMBER = 5010;
        public const long TRANSMISSION_DRIVER_SERIAL_PORT_BAUD_RATE = 5011;
        public const long TRANSMISSION_DRIVER_SERIAL_PORT_DATA_BITS = 5012;
        public const long TRANSMISSION_DRIVER_SERIAL_PORT_PARITY = 5013;
        public const long TRANSMISSION_DRIVER_SERIAL_PORT_STOP_BITS = 5014;
        public const long TRANSMISSION_DRIVER_MODEM_TYPE = 5015;	// 1-Wavecom, 2-WavecomTCP
        public const long TRANSMISSION_DRIVER_ID_SIMCARD = 5016;
        public const long TRANSMISSION_DRIVER_PIN_NUMBER = 5017;
        public const long TRANSMISSION_DRIVER_SERVICE_CENTER_ADDRES = 5018;
        public const long TRANSMISSION_DRIVER_FLOW_CONTROL = 5019;
        public const long TRANSMISSION_DRIVER_TIMEOUT_FOR_AT_COMMAND = 5020;
        public const long TRANSMISSION_DRIVER_LISTEN_IP_ADDRESS = 5030;
        public const long TRANSMISSION_DRIVER_LISTEN_PORT = 5031;
        public const long TRANSMISSION_DRIVER_DATAGRAM_POLL = 5032;
        public const long TRANSMISSION_DRIVER_RESPONSE_ADDRESS = 5033;
        public const long TRANSMISSION_DRIVER_BYTES_INTERVAL = 5034;
        public const long TRANSMISSION_DRIVER_WAN2_MULTI_MODE = 5035;
        public const long TRANSMISSION_DRIVER_PACKET_TYPE = 5036;
        public const long TRANSMISSION_DRIVER_STATIC_ADDRESS = 5037;
        public const long TRANSMISSION_DRIVER_STATIC_SERIAL_NBR = 5038;
        public const long TRANSMISSION_DRIVER_GATEWAY_ADDRESS = 5039;
        public const long TRANSMISSION_DRIVER_GATEWAY_PORT = 5040;
        public const long TRANSMISSION_DRIVER_GATEWAY_USER = 5041;
        public const long TRANSMISSION_DRIVER_GATEWAY_PASSWORD = 5042;
        public const long TRANSMISSION_DRIVER_UCP_AUTHENTICATION_CODE = 5043;
        public const long TRANSMISSION_DRIVER_NATIONAL_NUMBER_PREFIX = 5044;
        public const long TRANSMISSION_DRIVER_GATEWAY_TRY_COUNT = 5045;
        public const long TRANSMISSION_DRIVER_GATEWAY_TRY_TIME = 5046;
        public const long TRANSMISSION_DRIVER_GATEWAY_KEEPALIVE_TIME = 5047;
        public const long TRANSMISSION_DRIVER_SMPP_SYSTEM_TYPE = 5048;
        public const long TRANSMISSION_DRIVER_TCP_MODE = 5049; // Specifies connection options for TCP driver (0 - TCP Server(single), 1 - TCP Client(multiple))
        public const long TRANSMISSION_DRIVER_MODEM_TRANSMISSION_TYPE = 5050; //1-RS, 2-TCP
        public const long TRANSMISSION_DRIVER_FTP_PATH = 5051;
        public const long TRANSMISSION_DRIVER_FTP_FILE_CHECK_RETRIES = 5052;
        public const long TRANSMISSION_DRIVER_FTP_IP = 5053;
        public const long TRANSMISSION_DRIVER_FTP_PORT = 5054;
        public const long TRANSMISSION_DRIVER_FTP_ADDRESS = 5055;
        public const long TRANSMISSION_DRIVER_FTP_USER = 5056;
        public const long TRANSMISSION_DRIVER_FTP_PASSWORD = 5057;
        public const long TRANSMISSION_DRIVER_FTP_FILE_ACCESS_MODE = 5058;//0 - FTP, 1 - SHARED_DIRECTORY
        public const long TRANSMISSION_DRIVER_FTP_POLLING_PERIOD = 5059;
        public const long TRANSMISSION_DRIVER_INACTIVE_TIME_TO_DISCONNECT = 5060;
        public const long TRANSMISSION_DRIVER_SOURCE_ADDRESS = 5061;
        public const long TRANSMISSION_DRIVER_FTP_MODE = 5062;//0 - SERVER, 1 - CLIENT
        public const long TRANSMISSION_DRIVER_GATEWAY_ID = 5063;
        public const long TRANSMISSION_DRIVER_GATEWAY_BINDING = 5064;
        public const long TRANSMISSION_DRIVER_TRS_MODE = 5065;
        public const long TRANSMISSION_DRIVER_TRS_TYPE = 5066;
        public const long TRANSMISSION_DRIVER_MAX_DATAGRAM_LENGTH = 5067;
        public const long TRANSMISSION_DRIVER_FTP_FILE_NAME_FORMAT = 5068;
        public const long TRANSMISSION_DRIVER_FTP_FILE_DATA_FORMAT = 5069;
        public const long TRANSMISSION_DRIVER_FTP_FILE_IN_ARCHIVE_TIME = 5070;
        public const long TRANSMISSION_DRIVER_FTP_FILE_IN_TRASH_TIME = 5071;
        public const long TRANSMISSION_DRIVER_ALWAYS_USE_8BIT_ENCODING = 5072;
        public const long TRANSMISSION_DRIVER_IMR_SERVER_ID = 5073;//ID_IMR_SEVER z jakiego pochodzi TRANSMISSION_DRIVER
        public const long TRANSMISSION_DRIVER_SOURCE_SERVER_ID = 5074;// ID_TRANSMISSION_DRIVER z serwera IMR
        public const long TRANSMISSION_DRIVER_IS_ACTIVE = 5075; // Czy transmission driver jest aktywny
        public const long TRANSMISSION_DRIVER_SMPP_SYSTEM_VERSION = 5076;
        public const long TRANSMISSION_DRIVER_GATEWAY_SPECIFIC_VERSION = 5077;
        public const long TRANSMISSION_DRIVER_GATEWAY_USER2 = 5078;
        public const long TRANSMISSION_DRIVER_GATEWAY_PASSWORD_HASHED = 5079;

        public const long DRIVER_FILE = 5101;
        public const long DEVICE_DRIVER_FOR_DEVICE_CFG = 5102;
        public const long DEVICE_DRIVER_LOG_ENABLED = 5103;
        public const long DEVICE_DRIVER_LOG_LEVEL = 5104;
        public const long DEVICE_DRIVER_IS_RTU = 5105;

        #endregion
        #region ACTION attributes
        public const long ACTION_SEND_TIMEOUT = 6001;      // <Brak opisu>
        public const long ACTION_ID_ACTION = 6002;      // <Brak opisu>
        public const long ACTION_DEVICE_SERIAL_NBR = 6005;      // <Brak opisu>
        public const long ACTION_ID_METER = 6006;      // <Brak opisu>
        public const long ACTION_ID_LOCATION = 6007;      // <Brak opisu>
        public const long ACTION_GET_VALUES_FROM_DB = 6008;      // <Brak opisu>
        public const long ACTION_UPDATE_VALUES_IN_DB = 6009;      // <Brak opisu>
        public const long ACTION_ONLY_ACTIVE_DEVICE = 6010;      // <Brak opisu>
        public const long ACTION_INCLUDE_LOCATION_HIERARCHY = 6011;      // <Brak opisu>
        public const long ACTION_INCLUDE_DEVICE_HIERARCHY = 6012;      // <Brak opisu>
        public const long ACTION_TIME_TO_CHECK = 6013;      // <Brak opisu>
        public const long ACTION_SYNCHRONIZATION_TOLERANCE = 6014;      // <Brak opisu>
        public const long ACTION_ALEVEL_SLOT_NBR = 6015;      // <Brak opisu>
        public const long ACTION_MANUAL_READOUT_WITH_REQUEST = 6016;      // <Brak opisu>
        public const long ACTION_AIR_CALLBACK_DELAY = 6017;
        public const long ACTION_AIR_CALLBACK_RESULTS = 6018;
        public const long ACTION_ALWAYS_QUERY = 6019;
        public const long ACTION_FORCE_INSERT_DATA_ARCH = 6020;
        public const long ACTION_CHANGE_CID_CREATE_LOCATION = 6021;
        public const long ACTION_CHANGE_ALEVEL_MODE = 6022;
        public const long ACTION_OPERATION_VALVE_RUN_COUNT = 6023;
        public const long ACTION_OPERATION_VALVE_EXTEND_MODEM_LIFE = 6024;
        public const long ACTION_DATA_SOURCE_TYPE_TO_CHECK = 6025;
        public const long ACTION_ONLY_HIERARCHY_DEVICES = 6026;
        public const long ACTION_CHANGE_SIM_CARD_MODE = 6027;
        public const long ACTION_RESET_DEVICE_VIA_SMS = 6028;
        public const long ACTION_STATION_SEND_DATA = 6029;
        public const long ACTION_STATION_SEND_REFUEL = 6030;
        public const long ACTION_SCHEDULE_TIME = 6031;
        public const long ACTION_SCHEDULE_MODE = 6032;
        public const long ACTION_QUERY_ATTEMPTS = 6033;
        public const long ACTION_QUERY_INTERVAL = 6034;
        public const long ACTION_QUERY_INTERVAL_FOR_DEVICE = 6035;
        public const long ACTION_SERIAL_NBRS_LIST = 6036;
        public const long ACTION_SEND_ID_CONNECTION = 6037;
        public const long ACTION_SCHEDULE_TOLERANCE = 6038;

        public const long ACTION_SEND_USING_SESSION = 6038;
        public const long ACTION_SEND_FAKE_LOST_SESSION = 6039;
        public const long ACTION_DW_TASK_TYPE = 6041;
        public const long ACTION_STATION_SEND_PARAMS = 6042;
        public const long ACTION_PROCESSING_DELAY = 6043;
        public const long ACTION_STATION_SEND_ALL = 6044;
        public const long ACTION_SEND_CONFIGURATION = 6045;
        public const long ACTION_CHANGE_DEVICE_MODE = 6046;
        public const long ACTION_DEVICE_SLOT_TYPE = 6047;
        public const long ACTION_OPERATOR_ID = 6048;
        public const long ACTION_TRANSFER_TARGET_QUEUE = 6049;
        public const long ACTION_TRANSFER_MIN_INTERVAL = 6050;
        public const long ACTION_STATION_MEASURES_TRANSFER = 6051;
        public const long ACTION_LOCATION_FORCE_PROFILE_APPLY = 6052;
        public const long ACTION_LOCATION_PROFILE_APPLY_TIMEOUT = 6053;
        public const long ACTION_APPLY_CONFIGURATION_PROFILE = 6054;
        public const long ACTION_FLASH_FIRMWARE_KEEP_CONFIGURATION = 6055;
        public const long ACTION_FLASH_FIRMWARE_RETRIES = 6056;
        public const long ACTION_FORCE_FREQ_GENERATION = 6057;
        public const long ACTION_STATION_SEND_PERFORMANCE = 6058;
        public const long ACTION_FLASH_FIRMWARE_DEVICE_TYPE = 6059;
        public const long ACTION_SEND_ID_CONNECTION_FORCED = 6060;
        public const long ACTION_STOP_ACTION_SEQUENCE_AFTER_ERROR = 6061;
        public const long ACTION_DEFINITION_TO_RUN_IN_SEQUENCE = 6062;
        public const long ACTION_DAYS_SINCE_LAST_DIAGNOSTIC_QUERY = 6063;
        public const long ACTION_ARCHIVE_REQUEST_RETRIES = 6064;//102258;
        public const long ACTION_MISSING_READOUTS_COUNT_TRESHOLD = 6065;//102259;
        public const long ACTION_MISSING_READOUTS_COUNT_BUFFER = 6066;//102476;
        public const long ACTION_MISSING_READOUTS_TIME_TOLERANCE = 6067;//102260;
        public const long ACTION_MISSING_READOUTS_DAYS_MAXIMUM = 6068;//102477;
        public const long ACTION_ARCHIVE_REQUEST_MAX_RETRIES = 6069;//102261;
        public const long ACTION_STATION_WEB_SERVICE_FUNCTION = 6070;
        public const long ACTION_STATION_WEB_SERVICE_URL = 6071;
        public const long ACTION_ARCHIVE_MULTIREAD_DATA_TYPE = 6072;
        public const long ACTION_MOVE_DEVICE_WITH_METER = 6073;
        public const long ACTION_USE_OPERATOR_DISTRIBUTOR = 6074;
        public const long ACTION_INCLUDE_DISTRIBUTOR_HIERARCHY = 6075;
        public const long ACTION_ONE_METER_DEVICE_IN_LOCATION = 6076;
        public const long ACTION_STATION_MEASURES_ALARM = 6077;
        public const long ACTION_START_TIME = 6078;
        public const long ACTION_STATION_GENERATE_PERFORMANCE_AGGR = 6079;
        public const long ACTION_STATION_GENERATE_RATING = 6080;
        public const long ACTION_END_TIME = 6081;
        public const long ACTION_PLIP_AUTOESCALATE = 6082;
        public const long ACTION_SIR2_INITIALIZATION = 6083;
        public const long ACTION_SYNCHRONIZATION_INITIALIZATION = 6084;
        public const long ACTION_DAYS_BACK_TO_GET_PLANNED_DELIVERY = 6085;
        public const long ACTION_STATION_INACTIVITY_SYNCHRONIZATION = 6086;
        public const long ACTION_STATION_RECEIVE_CONFIGURATION = 6087;

        public const long ACTION_RUN_FROM_SMS = 6100;
        public const long ACTION_SMS_REQUEST_TEXT = 6101;
        public const long ACTION_SMS_ID_TEXT_START = 6102;
        public const long ACTION_SMS_ID_TEXT_ERROR = 6103;
        public const long ACTION_SMS_ID_TEXT_RESULT = 6104;
        public const long ACTION_SMS_ID_TEXT_NO_ATG_DEVICES = 6105;
        public const long ACTION_SMS_ID_TEXT_NO_METER_DEVICES = 6106;
        public const long ACTION_SMS_ID_TEXT_ACCESS_DENIED = 6107;
        public const long ACTION_SEND_GPRS_CONFIGURATION = 6108;
        public const long ACTION_SEND_PATTERN_DIFFERENCES = 6109;
        public const long ACTION_GPRS_ID_CONNECTION = 6110;
        public const long ACTION_SMS_GPRS_ID_CONNECTION = 6112;
        public const long ACTION_SAVE_DEVICE_COUNTERS = 6113;
        public const long ACTION_PATTERN_SERIAL_NUMBER = 6114;
        public const long ACTION_CHECK_FIRMWARE_VERSION = 6115;
        public const long ACTION_GET_ADDITIONAL_COUNTER = 6116;
        public const long ACTION_STEP_RETRIES = 6117;
        public const long ACTION_KEEP_CONFIGURATION = 6118;
        public const long ACTION_RERUN_ID_ACTION = 6119;
        public const long ACTION_DONT_DOWNLOAD_FIRMWARE = 6120;
        public const long ACTION_AVERAGE_GSM_QUALITY = 6121;
        public const long ACTION_MINIMUM_GSM_QUALITY = 6122;
        public const long ACTION_EXTEND_DEVICE_MODEM_TIMEOUT = 6123;
        public const long ACTION_SMS_ID_CONNECTION = 6124;
        public const long ACTION_GASMETER_COUNTER_FORMAT = 6125;
        public const long ACTION_INTELLIGENCE_ERROR_MSG = 6126;
        public const long ACTION_DEVICE_TYPE = 6127;
        public const long ACTION_CID_AND_FREQ_IN_ONE_PACKET = 6128;
        public const long ACTION_DEVICES_TO_QUERY_RATIO = 6129;
        public const long ACTION_INCLUDE_ONLY_OKO_DEVICES = 6130;
        public const long ACTION_DEVICE_MANAGEMENT_MODE = 6131;
        public const long ACTION_SMS_TEXT_BODY_TYPE = 6132;
        public const long ACTION_VOLUME_CORRECTOR_TYPE = 6133;
        public const long ACTION_AWSR_AGGREGATE_COMPLETENESS = 6134;
        public const long ACTION_LOCATION_MSN_UNIQUE = 6135;
        public const long ACTION_EMAIL_NOTIFICATION_ADDRESS = 6136;
        public const long ACTION_SCHEDULE_READOUT_EVERY_DAY = 6137;
        public const long ACTION_DISTRIBUTOR_ID = 6138;
        public const long ACTION_OPERATOR_LANGUAGE_ID = 6139;
        public const long ACTION_DAILY_FREQ_COUNT = 6140;
        public const long ACTION_FORCED_ID_PROTOCOL_OUT = 6141;
        public const long ACTION_TEST_IMR_DURATION = 6142;
        public const long ACTION_NEW_CHECK_FREEZE_DEVICE = 6143;
        public const long ACTION_SITA_COMMAND = 6144;
        public const long ACTION_SITA_COMMAND_RESULT = 6145;
        public const long ACTION_PROCESSING_TIMEOUT = 6146;
        public const long ACTION_ENCRYPT_CRYPTOGRAPHY_KEYS = 6147;
        public const long ACTION_SITA_COMMAND_PARAMS = 6148;
        public const long ACTION_USE_DELAY = 6149;
        public const long ACTION_DATA_INDEX_NBR = 6150;
        public const long ACTION_PIN_TYPE = 6151;
        public const long ACTION_IS_GASMETER_DEVICE = 6152;
        public const long ACTION_STATION_CHECK_CONFIGURATION = 6153;
        public const long ACTION_CALCULATE_GAS_CONSUMPTION_HOURLY = 6154;
        public const long ACTION_NOTE = 6155;
        public const long ACTION_OPERATOR_LDAP_SYNCHRONIZATION = 6156;
        public const long ACTION_STATION_SYNCHRONIZATION = 6157;
        public const long ACTION_CORE_TO_FP_CONFIG_SYNCHRONIZATION = 6158;
        public const long ACTION_DISABLE_SMS_TEXT = 6159;
        public const long ACTION_ID_METER_TYPE_CLASS = 6160;
        

        public const long ACTION_SMS_ACTION_0 = 6900;
        public const long ACTION_SMS_ACTION_1 = 6901;
        public const long ACTION_SMS_ACTION_2 = 6902;
        public const long ACTION_SMS_ACTION_3 = 6903;
        public const long ACTION_SMS_ACTION_4 = 6904;
        public const long ACTION_SMS_ACTION_5 = 6905;
        public const long ACTION_SMS_ACTION_6 = 6906;
        public const long ACTION_SMS_ACTION_7 = 6907;
        public const long ACTION_SMS_ACTION_8 = 6908;
        public const long ACTION_SMS_ACTION_9 = 6909;
        public const long ACTION_SMS_ACTION_10 = 6910;
        public const long ACTION_SMS_ACTION_11 = 6911;
        public const long ACTION_SMS_ACTION_12 = 6912;
        public const long ACTION_SMS_ACTION_13 = 6913;
        public const long ACTION_SMS_ACTION_14 = 6914;
        public const long ACTION_SMS_ACTION_15 = 6915;
        public const long ACTION_SMS_ACTION_16 = 6916;
        public const long ACTION_SMS_ACTION_17 = 6917;
        public const long ACTION_SMS_ACTION_18 = 6918;
        public const long ACTION_SMS_ACTION_19 = 6919;
        public const long ACTION_SMS_ACTION_20 = 6920;
        public const long ACTION_SMS_ACTION_21 = 6921;
        public const long ACTION_SMS_ACTION_22 = 6922;
        public const long ACTION_SMS_ACTION_23 = 6923;
        public const long ACTION_SMS_ACTION_24 = 6924;
        public const long ACTION_SMS_ACTION_25 = 6925;
        public const long ACTION_SMS_ACTION_26 = 6926;
        public const long ACTION_SMS_ACTION_27 = 6927;
        public const long ACTION_SMS_ACTION_28 = 6928;
        public const long ACTION_SMS_ACTION_29 = 6929;
        public const long ACTION_SMS_ACTION_30 = 6930;
        public const long ACTION_SMS_ACTION_31 = 6931;
        public const long ACTION_SMS_ACTION_32 = 6932;
        public const long ACTION_SMS_ACTION_33 = 6933;
        public const long ACTION_SMS_ACTION_34 = 6934;
        public const long ACTION_SMS_ACTION_35 = 6935;
        public const long ACTION_SMS_ACTION_36 = 6936;
        public const long ACTION_SMS_ACTION_37 = 6937;
        public const long ACTION_SMS_ACTION_38 = 6938;
        public const long ACTION_SMS_ACTION_39 = 6939;
        public const long ACTION_SMS_ACTION_40 = 6940;
        public const long ACTION_SMS_ACTION_41 = 6941;
        public const long ACTION_SMS_ACTION_42 = 6942;
        public const long ACTION_SMS_ACTION_43 = 6943;
        public const long ACTION_SMS_ACTION_44 = 6944;
        public const long ACTION_SMS_ACTION_45 = 6945;
        public const long ACTION_SMS_ACTION_46 = 6946;
        public const long ACTION_SMS_ACTION_47 = 6947;
        public const long ACTION_SMS_ACTION_48 = 6948;
        public const long ACTION_SMS_ACTION_49 = 6949;
        public const long ACTION_SMS_ACTION_50 = 6950;
        public const long ACTION_SMS_ACTION_51 = 6951;
        public const long ACTION_SMS_ACTION_52 = 6952;
        public const long ACTION_SMS_ACTION_53 = 6953;
        public const long ACTION_SMS_ACTION_54 = 6954;
        public const long ACTION_SMS_ACTION_55 = 6955;
        public const long ACTION_SMS_ACTION_56 = 6956;
        public const long ACTION_SMS_ACTION_57 = 6957;
        public const long ACTION_SMS_ACTION_58 = 6958;
        public const long ACTION_SMS_ACTION_59 = 6959;
        public const long ACTION_SMS_ACTION_60 = 6960;
        public const long ACTION_SMS_ACTION_61 = 6961;
        public const long ACTION_SMS_ACTION_62 = 6962;
        public const long ACTION_SMS_ACTION_63 = 6963;
        public const long ACTION_SMS_ACTION_64 = 6964;
        public const long ACTION_SMS_ACTION_65 = 6965;
        public const long ACTION_SMS_ACTION_66 = 6966;
        public const long ACTION_SMS_ACTION_67 = 6967;
        public const long ACTION_SMS_ACTION_68 = 6968;
        public const long ACTION_SMS_ACTION_69 = 6969;
        public const long ACTION_SMS_ACTION_70 = 6970;
        public const long ACTION_SMS_ACTION_71 = 6971;
        public const long ACTION_SMS_ACTION_72 = 6972;
        public const long ACTION_SMS_ACTION_73 = 6973;
        public const long ACTION_SMS_ACTION_74 = 6974;
        public const long ACTION_SMS_ACTION_75 = 6975;
        public const long ACTION_SMS_ACTION_76 = 6976;
        public const long ACTION_SMS_ACTION_77 = 6977;
        public const long ACTION_SMS_ACTION_78 = 6978;
        public const long ACTION_SMS_ACTION_79 = 6979;
        public const long ACTION_SMS_ACTION_80 = 6980;
        public const long ACTION_SMS_ACTION_81 = 6981;
        public const long ACTION_SMS_ACTION_82 = 6982;
        public const long ACTION_SMS_ACTION_83 = 6983;
        public const long ACTION_SMS_ACTION_84 = 6984;
        public const long ACTION_SMS_ACTION_85 = 6985;
        public const long ACTION_SMS_ACTION_86 = 6986;
        public const long ACTION_SMS_ACTION_87 = 6987;
        public const long ACTION_SMS_ACTION_88 = 6988;
        public const long ACTION_SMS_ACTION_89 = 6989;
        public const long ACTION_SMS_ACTION_90 = 6990;
        public const long ACTION_SMS_ACTION_91 = 6991;
        public const long ACTION_SMS_ACTION_92 = 6992;
        public const long ACTION_SMS_ACTION_93 = 6993;
        public const long ACTION_SMS_ACTION_94 = 6994;
        public const long ACTION_SMS_ACTION_95 = 6995;
        public const long ACTION_SMS_ACTION_96 = 6996;
        public const long ACTION_SMS_ACTION_97 = 6997;
        public const long ACTION_SMS_ACTION_98 = 6998;

        public const long ACTION_RUN_IN_FULL_DEBUG_MODE = 6999;
        #endregion
        #region ALARM attributes
        public const long ALARM_TEXT_MESSAGE_TO_SEND = 11001;     // <Brak opisu>
        public const long ALARM_MANUAL_READOUT_ASSESSMENT = 11003;     // <Brak opisu>
        public const long ALARM_MANUAL_READOUT_REQUEST = 11004;     // <Brak opisu>
        public const long ALARM_OPERATION_VALVE_ACTION = 11005;
        public const long ALARM_MONITOR_GET_QUEUES_EXCEPTION = 11006;
        public const long ALARM_MONITOR_QUEUE_FULL = 11007;
        public const long ALARM_ACTION_DEF_TO_SEND = 11008;
        public const long ALARM_MONITOR_NO_DATA = 11009;
        public const long ALARM_MONITOR_NO_AWSR_DATA = 11010;
        public const long ALARM_AWSR_STATION_PROBLEM = 11011;
        public const long ALARM_CONFIRMATION_DATE = 11012;
        public const long ALARM_CONFIRMATION_OPERATOR_ID = 11013;
        public const long ALARM_CONFIRMATION_ID_ALARM_EVENT = 11014;
        public const long ALARM_PLIPA_ESCALATE = 11015;
        public const long ALARM_PLIPB_ESCALATE = 11016;
        public const long ALARM_PLIPC_ESCALATE = 11017;
        public const long ALARM_PLIPA_REPORTED = 11018;
        public const long ALARM_PLIPB_REPORTED = 11019;
        public const long ALARM_PLIPC_REPORTED = 11020;
        public const long ALARM_PLIPA_CONFIRMED = 11021;
        public const long ALARM_PLIPB_CONFIRMED = 11022;
        public const long ALARM_PLIPC_CONFIRMED = 11023;
        public const long ALARM_PLIP_LEAK = 11024;
        #endregion
        #region SYSTEM params
        public const long SYSTEM_CHANGE_IN_CORE_TABLE_QUEUE_PATH = 7001;      // �cie�ka kolejki MSMQ, do kt�rej przesy�ane s� zmiany w tabelach CORE
        public const long SYSTEM_CHANGE_IN_DAQ_TABLE_QUEUE_PATH = 7002;      // �cie�ka kolejki MSMQ, do kt�rej przesy�ane s� zmiany w tabelach DAQ
        public const long SYSTEM_CHANGE_IN_DW_TABLE_QUEUE_PATH = 7003;      // �cie�ka kolejki MSMQ, do kt�rej przesy�ane s� zmiany w tabelach DW
        public const long SYSTEM_CHANGE_IN_TABLE_QUEUE_TIMEOUT = 7004;      // Czas w [s] po kt�rym zmiany w tabelach s� usuwane z kolejki
        public const long SYSTEM_CHANGE_IN_TABLE_COLLECT_TIME = 7005;      // <Brak opisu>
        public const long SYSTEM_ADD_ACTION_QUEUE_PATH = 7006;			// Sciezka kolejki MSMQ, do ktorej przesyalne sa nowe/dodane akcje z poziomu DB/StoredProcedure
        public const long SYSTEM_ADD_ACTION_QUEUE_TIMEOUT = 7007;		// Czas w [s] po ktorym dodana akcja zostanie usunieta z kolejki
        public const long SYSTEM_DICTIONARY_TABLES_VERSION = 7008;		// Czas w [s] po ktorym dodana akcja zostanie usunieta z kolejki
        public const long SYSTEM_DATABASE_UPDATE_VERSION = 7009;        // wersja ostatniego skryptu puszczonego na bazie
        public const long SYSTEM_SQL_TRIGGER_LOG_ENABLED = 7010;        // Czy ma by� logowanie do Logger.exe (tylko UDP) z SQL.Triggers [default=false]
        public const long SYSTEM_SQL_TRIGGER_LOG_HOST = 7011;           // IP/Host gdzie b�d� wysy�ane Logi (tylko UDP)
        public const long SYSTEM_SQL_TRIGGER_LOG_PORT = 7012;           // Port gdzie b�d� wysy�ane Logi (tylko UDP)
        public const long SYSTEM_SPECIFICATION_FILE_VERSION = 7013;        // wersja opis�w driver�w itd. za�adowana do bazy
        public const long SYSTEM_SQL_SERVER_INSTANCE = 7014;           // Je�li SQL dzia�a z InstanceName (potrzebne do ponownego otwarcia po��czenia w DataChangeTableWorkingThread)
        public const long SYSTEM_RUNNING_ACTION_ENABLED = 7015;         // Czy uruchamianie akcji jest wlaczone - istotne, aby dla baz _CORE_IMRSC bylo wylaczone, gdyz ta baza wspoldzieli z baza _CORE baze _DAQ, a to moze stwarzac problemy podczas przetwarzania akcji
        public const long SYSTEM_ID_DISTRIBUTOR_POOL_BEGIN = 7016;      //Pocz�tek puli identyfikator�w dystrybutor�w
        public const long SYSTEM_ID_DISTRIBUTOR_POOL_END = 7017;      //Koniec puli identyfikator�w dystrybutor�w
        public const long SERVER_FTP_ADDRESS = 7018;
        public const long SERVER_FTP_LOGIN = 7019;
        public const long SERVER_FTP_PASSWORD = 7020;
        public const long SYSTEM_IMR_SERVER_VERSION = 7021;     // numer wersji dzia�aj�cego systemu IMR (na DAQ, CORE, DW oczywi�cie mo�e by� r��na)
        public const long SYSTEM_DB_SCHEMA_VERSION = 7022;      // numer wersji DB (na DAQ, CORE, DW oczywi�cie mo�e by� r��na)
        #endregion
        #region SESSION params
        public const long SESSION_TIME_STARTED = 8001;      // <Brak opisu>
        public const long SESSION_TIME_LAST_ACTIVITY = 8002;      // <Brak opisu>
        public const long SESSION_TIME_ENDED = 8003;      // <Brak opisu>
        public const long SESSION_HOST_IP = 8004;      // <Brak opisu>
        public const long SESSION_DURANCE = 8005;      // <Brak opisu>
        public const long SESSION_LOGIN_TYPE = 8006;      // <Brak opisu>
        public const long SESSION_LOGOUT_TYPE = 8007;      // <Brak opisu>
        public const long SESSION_RDP_CONNECTION_GUID = 8008;      // <Brak opisu>
        public const long SESSION_COMPUTER_NAME = 8009;      // <Brak opisu>
        public const long SESSION_OPERATOR_LOGIN_STATUS = 8010; // Status logowania operatora w sesji
        public const long SESSION_RELEASE_TIME_PERIOD = 8011;
        public const long SESSION_MOBILE_NBR = 8012;
        public const long SESSION_REDIRECTION_URL = 8013;
        public const long SESSION_NEW_PASSWORD = 8014;
        #endregion
        #region EVENT SESSION params
        public const long SESSION_EVENT_ID_LOCATION = 9001;      // <Brak opisu>
        public const long SESSION_EVENT_METHOD_DURATION = 9002;      // <Brak opisu>
        public const long SESSION_EVENT_OBJECTS_COUNT = 9003;      // <Brak opisu>
        public const long SESSION_EVENT_RESULTS_COUNT = 9004;      // <Brak opisu>
        public const long SESSION_EVENT_REGISTRATION_EMAIL = 9005;  //Adres email podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_NAME = 9006;  //Imi� podawane w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_SURNAME = 9007;//Nazwisko podawane w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_PHONE = 9008;//Telefon podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_SKYPE = 9009;//SKYPE_ID podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_NAME = 9010;//Nazwa firmy podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_ADDRESS = 9011;//Adres firmy podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_CITY = 9012;//Miasto w kt�rym jest firma podawane w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_ZIP = 9013;//Kod pocztowy miasta podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_COUNTRY = 9014;//Kraj podawany w formularzu rejestracji w aplikacji TankMonitoring
        public const long SESSION_EVENT_REGISTRATION_COMPANY_BUSINESS_TYPE = 9015;//Typ kontaktu biznesowego podawany w formularzu rejestracji w aplikacji TankMonitoring (zapisywane tekstowo)
        public const long SESSION_EVENT_REGISTRATION_COMPANY_MORE_INFORMATION = 9016;//Opcjonalen dodatkowe informacje wprowadzane w formularzu rejestracyjnym
        public const long SESSION_EVENT_RELATED_DAY = 9017;
        #endregion
        #region ESTIM params
        public const long ESTIM_ERROR_HORIZON = 13001;     // <Brak opisu>
        public const long ESTIM_HORIZON = 13002;      // <Brak opisu>
        public const long ESTIM_INTERVAL_COUNT = 13003;     // <Brak opisu>
        public const long ESTIM_INTERVAL_LAST_TIME = 13004;     // <Brak opisu>
        public const long ESTIM_INTERVAL_SIZE = 13005;     // <Brak opisu>
        public const long ESTIM_INTERVAL_TOLERANCE = 13006;     // <Brak opisu>
        public const long ESTIM_MC_RATIO = 13007;     // <Brak opisu>
        public const long ESTIM_MAX_HORIZON_ERROR = 13008;     // <Brak opisu>
        public const long ESTIM_MAX_LEVEL_ERROR = 13009;     // <Brak opisu>
        public const long ESTIM_MAIN_GROUP_ID = 13010;     // <Brak opisu>
        public const long ESTIM_ADD_TO_GROUPS_PART = 13011;     // <Brak opisu>
        public const long ESTIM_MAX_DELTA_IN_GROUP = 13012;     // <Brak opisu>
        public const long ESTIM_SAVE_ESTIM_DATA_TO_DB = 13013;     // <Brak opisu>
        public const long ESTIM_SAVE_LOC_IN_GROUPS_TO_DB = 13014;     // <Brak opisu>
        public const long ESTIM_SAVE_MEASURES_TO_DB = 13015;     // <Brak opisu>
        public const long ESTIM_SAVE_NEW_STATUS_TO_DB = 13016;     // <Brak opisu>
        public const long ESTIM_SAVE_RESULTS_TO_FILE = 13017;     // <Brak opisu>
        public const long ESTIM_MAX_GAP_DAY_COUNT = 13018;     // <Brak opisu>
        public const long ESTIM_HOUR_UTC = 13019;     // <Brak opisu>
        public const long ESTIM_ID_DATA_TYPE = 13020;     // <Brak opisu>
        public const long ESTIM_ID_DEVICE_TYPE = 13021;     // <Brak opisu>
        public const long ESTIM_ID_DISTRIBUTOR = 13022;     // <Brak opisu>
        public const long ESTIM_LAST_LAUNCH_TIME = 13023;     // <Brak opisu>
        public const long ESTIM_RANGE_START_TIME = 13024;     // <Brak opisu>
        public const long ESTIM_RANGE_END_TIME = 13025;     // <Brak opisu>

        #endregion
        #region ROUTE params
        public const long ROUTE_ELEMENT_STATUS_ID = 15001;        // <Brak opisu>
        public const long ROUTE_ELEMENT_STATUS_DESCRIPTION = 15002;        // <Brak opisu>
        public const long ROUTE_READOUT_FORM_AVAILABLE = 15003;        // <Brak opisu>
        public const long ROUTE_METER_VERIFICATION_FORM_AVAILABLE = 15004;        // <Brak opisu>
        public const long ROUTE_DEVICE_VERIFICATION_AVAILABLE = 15005;        // <Brak opisu>
        public const long ROUTE_SEAL_VERIFICATION_AVAILABLE = 15006;        // <Brak opisu>
        public const long ROUTE_ABNORMALITIES_FORM_AVAILABLE = 15007;        // <Brak opisu>
        public const long ROUTE_NOTES_FORM_AVAILABLE = 15008;        // <Brak opisu>
        public const long ROUTE_SIGNATURE_FORM_AVAILABLE = 15009;        // <Brak opisu>
        public const long ROUTE_OPTO_ARCHIVE_FORM_AVAILABLE = 15010;        // <Brak opisu>
        public const long ROUTE_OPTO_DIAGNOSTIC_FORM_AVAILABLE = 15011;        // <Brak opisu>
        public const long ROUTE_OPTO_CURRENT_READOUT_FORM_AVAILABLE = 15012;        // <Brak opisu>
        public const long ROUTE_MANUAL_READOUT = 15013;        // <Brak opisu>
        public const long ROUTE_RADIO_READOUT = 15014;        // <Brak opisu>
        public const long ROUTE_OPTICAL_CURRENT_READOUT = 15015;        // <Brak opisu>
        public const long ROUTE_OPTICAL_DIAGNOSTIC_READOUT = 15016;        // <Brak opisu>
        public const long ROUTE_OPTICAL_ARCHIVE_READOUT = 15017;        // <Brak opisu>
        public const long ROUTE_MDR_ID = 15018;
        public const long ROUTE_READOUT_VERIFICATION_DATETIME = 15019;
        public const long ROUTE_READOUT_VERIFICATION_VALUE = 15020;
        public const long ROUTE_READOUT_VERIFICATION_NOTE = 15021;
        public const long ROUTE_READOUT_VERIFICATION_ESTIMATED_MIN_VALUE = 15022;
        public const long ROUTE_READOUT_VERIFICATION_ESTIMATED_MAX_VALUE = 15023;
        public const long ROUTE_ELEMENT_PERFORMER_OPERATOR_ID = 15024;
        public const long ROUTE_ELEMENT_READOUT_SOURCE_ID = 15025;
        public const long ROUTE_ELEMENT_READOUT_SOURCE_DESCRIPTION = 15026;
        public const long ROUTE_ELEMENT_READOUT_ABSENCE_ID = 15027;
        public const long ROUTE_ELEMENT_READOUT_ABSENCE_DESCRIPTION = 15028;
        public const long ROUTE_ELEMENT_ENTERED_ABNROMALITY_ID = 15029;
        public const long ROUTE_ELEMENT_ENTERED_ABNORMALITY_DESCRIPTION = 15030;
        public const long ROUTE_ELEMENT_ENTERED_NOTE = 15031;
        public const long ROUTE_ID_PACKAGE = 15032;//id paczki przypisanej do trasy
        public const long ROUTE_ADDITIONAL_SYMBOL = 15033;//dodatkowy symbol do trasy
        public const long ROUTE_DEF_LOCATION_ACTION_TYPE_ID = 15034;//Identyfikator typu akcji (ID_ACTION_TYPE) kt�ry ma by� wykonany w punkcie trasy (lokalizacji). Typy akcji wykonywane w ten spos�b to akcje o ID_ACTION_TYPE_CLASS = 6
        public const long ROUTE_DEF_START_DEPOT_ID = 15035;
        public const long ROUTE_DEF_END_DEPOT_ID = 15036;
        public const long ROUTE_DEF_LOCATION_STATUS = 15037;
        public const long REFUEL_TOTAL_VOLUME_MANUAL = 15038;
        public const long ROUTE_DEF_DISTANCE = 15039;
        public const long ROUTE_DEF_ALLOWED_OPERATOR_ID = 15040;
        public const long ROUTE_DELETE_DATE = 15041;
        public const long ROUTE_DESCRIPTION = 15042;
        public const long ROUTE_ID_OPERATOR_CREATOR = 15043;
        public const long ROUTE_ID_OPERATOR_PERFORMER = 15044;
        public const long ROUTE_TASK_SUPPORT = 15045;
        public const long ROUTE_EXTERNAL_ID = 15046;
        public const long ROUTE_EXPORT_ID_FILE = 15047;

        #endregion
        #region DEVICE_TYPE params
        public const long DEVICE_TYPE_FRAME_MAP = 14001;     // Definicja rozszycia
        public const long DEVICE_TYPE_FRAME_MAP_ANALOG = 14002;     // Definicja rozszycia analoga
        public const long DEVICE_TYPE_PROGRAM_ID = 14003;     // S�ownik ProgramID
        public const long DEVICE_TYPE_FROM_IMR_SERVER = 14004;
        public const long DEVICE_TYPE_SCHEDULE_ACTIVATION_COUNT = 14005;
        public const long DEVICE_TYPE_SCHEDULE_READOUT_COUNT = 14006;
        public const long DEVICE_TYPE_PACKET_TIME_FOR_READOUTS = 14007;
        public const long DEVICE_TYPE_MULTIREAD_ACTIVE = 14008;
        public const long DEVICE_TYPE_ANALOG_ACTIVE = 14009;
        public const long DEVICE_TYPE_ANALOG_WITH_MULTIREAD_ACTIVE = 14010;
        public const long DEVICE_TYPE_MULTIREAD_WL_ACTIVE = 14011;
        public const long DEVICE_TYPE_ANALOG_WL_ACTIVE = 14012;
        public const long DEVICE_TYPE_SUPPORTS_MULTIDATAGRAM = 14013;
        public const long DEVICE_TYPE_MENU_PINS_AVAILABLE = 14014;
        public const long DEVICE_TYPE_VALVE_AVAILABLE = 14015;
        public const long DEVICE_TYPE_AVAILABLE_ANALOGS = 14016;
        public const long DEVICE_TYPE_AVAILABLE_PREPAID = 14017;
        public const long DEVICE_TYPE_OBJECTS_MAP = 14018;     // Definicja rozszycia obiekt�w(Datatyp�w) urz�dzenia
        #endregion
        #region OTHER params
        public const long PATTERN_NAME = 10001;     // <Brak opisu>
        public const long HELPER_ID_LOCATION = 10002;
        public const long HELPER_ID_METER = 10003;
        public const long HELPER_SERIAL_NBR = 10004;
        public const long HELPER_ID_DISTRIBUTOR = 10005;
        public const long HELPER_ID_CONSUMER = 10006;
        public const long HELPER_ID_LOCATION_TYPE = 10007;
        public const long HELPER_ID_LOCATION_STATE_TYPE = 10008;
        public const long HELPER_LOCATION_IN_KPI = 10009;
        public const long HELPER_LOCATION_ALLOW_GROUPING = 10010;
        public const long GOOGLE_MAP_URL = 10011;
        public const long GOOGLE_MAP_LATITUDE = 10012;
        public const long GOOGLE_MAP_LONGITUDE = 10013;
        public const long GOOGLE_MAP_ZOOM = 10014;
        public const long HELPER_ID_DEVICE_TYPE = 10015;
        public const long HELPER_ID_DEVICE_STATE_TYPE = 10016;
        public const long HELPER_ID_DEVICE_ORDER_NUMBER = 10017;
        public const long HELPER_ID_METER_TYPE = 10018;
        public const long HELPER_DEVICE_TEMPLATE = 10019;
        public const long PROFILE_ID = 10020; // id profilu przypisywane do distributor/location/meter/device
        public const long TEMPORAL_START_TIME = 10021;
        public const long TEMPORAL_END_TIME = 10022;
        public const long TEMPORAL_ID_AGGREGATION_TYPE = 10023;
        public const long DATA_FORMAT_GROUP_ID = 10024;
        public const long HELPER_ID_METER_TYPE_CLASS = 10025;

        public const long SGM_GROUPING_LOCATION_TYPE_NAME = 10026;
        public const long SGM_GROUPING_LOCATION_TYPE_IS_ENABLED = 10027;

        public const long HELPER_INSTALLATION_DATE = 10028;
        public const long HELPER_LOCATION_PARENT_GROUPS = 10029;
        public const long TEMPORAL_ID_DATA_SOURCE = 10030;
        public const long HELPER_ID_TASK = 10031;
        public const long HELPER_ID_OPERATOR = 10032;
        public const long HELPER_ID_ISSUE = 10033;

        public const long FAILED_LOGIN_RETRY_COUNT = 10034;
        public const long FAILED_LOGIN_BLOCK_TIME = 10035;
        public const long FAILED_LOGIN_RETRY_CHECK_TIME = 10036;
        public const long LOG_OPERATOR_LOGIN_IN_SESSION = 10037;

        public const long PASSWORD_MIN_LENGTH = 10038;
        public const long PASSWORD_MIN_UPPER_LETTERS_COUNT = 10039;
        public const long PASSWORD_MIN_LOWER_LETTERS_COUNT = 10040;
        public const long PASSWORD_MIN_DIGITS_COUNT = 10041;
        public const long PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT = 10042;

        public const long GENERATE_OPERATOR_PASSWORD_SALT = 10043;

        public const long PASSWORD_VALIDITY_DAYS = 10044;

        public const long HELPER_ID_ACTOR = 10045;

        public const long PASSWORD_EXPIRY_NOTIFICATION_DAYS = 10046;

        public const long HELPER_LOGIN = 10047;
        //public const long HELPER_SERIAL_NBR = 10048;
        public const long HELPER_PASSWORD = 10049;
        public const long HELPER_IS_BLOCKED = 10050;
        //public const long HELPER_DESCRIPTION = 10051;

        public const long FAILED_LOGIN_BLOCKED_TO_TIME_RETRY_COUNT = 10052;

        public const long HELPER_ID_TASK_GROUP = 10053;
        public const long HELPER_ID_PLANNED_ROUTE = 10054;
        public const long HELPER_ID_OPERATOR_REGISTERING = 10055;
        public const long HELPER_ID_OPERATOR_PERFORMER = 10056;
        public const long HELPER_ID_TASK_STATUS = 10057;
        public const long HELPER_CREATION_DATE = 10058;
        public const long HELPER_ACCEPTED = 10059;
        public const long HELPER_ID_OPERATOR_ACCEPTED = 10060;
        public const long HELPER_ACCEPTANCE_DATE = 10061;
        public const long HELPER_NOTES = 10062;
        public const long HELPER_DEADLINE = 10063;
        public const long HELPER_TOPIC_NUMBER = 10064;
        public const long HELPER_PRIORITY = 10065;
        public const long HELPER_OPERATION_CODE = 10066;
        public const long HELPER_ID_TASK_TYPE = 10067;

        public const long LAST_MEASURE_RANGE_TIME_COLOR = 10068;

        public const long HELPER_ID_ISSUE_TYPE = 10069;
        public const long HELPER_ID_ISSUE_STATUS = 10070;
        public const long HELPER_REALIZATION_DATE = 10071;
        public const long HELPER_SHORT_DESCR = 10072;
        public const long HELPER_LONG_DESCR = 10073;

        public const long HELPER_NAME = 10074;
        public const long HELPER_SURNAME = 10075;
        public const long HELPER_CITY = 10076;
        public const long HELPER_ADDRESS = 10077;
        public const long HELPER_POSTCODE = 10078;
        public const long HELPER_EMAIL = 10079;
        public const long HELPER_MOBILE = 10080;
        public const long HELPER_PHONE = 10081;
        public const long HELPER_ID_LANGUAGE = 10082;
        public const long HELPER_DESCRIPTION = 10083;
        public const long HELPER_AVATAR = 10084;

        public const long LOCATION_VIEWER_ALL_INSTALLED_METERS_CHECKED = 10085;

        public const long HELPER_ID_DESCR = 10086;

        public const long LOGIN_AUTOFILL_DISABLED = 10087;

        public const long LAYOUT_ID_FILE = 10088;

        public const long HELPER_ID_IMR_SERVER = 10089;

        public const long HELPER_ID_ALARM = 10090;
        public const long HELPER_ID_ALARM_EVENT = 10091;
        public const long HELPER_ID_ALARM_GROUP = 10092;
        public const long HELPER_ID_ALARM_STATUS = 10093;
        public const long HELPER_ID_TRANSMISSION_TYPE = 10094;

        public const long HELPER_ID_ROUTE = 10095;
        public const long HELPER_ID_ROUTE_DEF = 10096;
        public const long HELPER_YEAR = 10097;
        public const long HELPER_MONTH = 10098;
        public const long HELPER_WEEK = 10099;
        public const long HELPER_ID_OPERATOR_EXECUTOR = 10100;
        public const long HELPER_ID_OPERATOR_APPROVED = 10101;
        public const long HELPER_DATE_UPLOADED = 10102;
        public const long HELPER_DATE_APPROVED = 10103;
        public const long HELPER_DATE_FINISHED = 10104;
        public const long HELPER_ID_ROUTE_STATUS = 10105;
        public const long HELPER_ID_ROUTE_TYPE = 10106;
        public const long HELPER_EXPIRATION_DATE = 10107;
        public const long HELPER_ID_ALARM_DEF = 10108;
        public const long HELPER_ID_DATA_ARCH = 10109;
        public const long HELPER_IS_ACTIVE = 10110;

        public const long HELPER_ID_ALARM_TYPE = 10111;
        public const long HELPER_ID_DATA_TYPE_ALARM = 10112;
        public const long HELPER_SYSTEM_ALARM_VALUE = 10113;
        public const long HELPER_ALARM_VALUE = 10114;
        public const long HELPER_TRANSMISSION_TYPE = 10115;

        public const long HELPER_DISTRIBUTOR_ID_ROLE = 10116;

        public const long HELPER_ID_REPORT = 10117;
        public const long HELPER_ID_REPORT_TYPE = 10118;

        public const long HELPER_ID_ACTION = 10119;
        public const long HELPER_ID_ACTION_TYPE = 10120;
        public const long HELPER_ID_ACTION_STATUS = 10121;
        public const long HELPER_ID_ACTION_DATA = 10122;
        public const long HELPER_ID_ACTION_PARENT = 10123;
        public const long HELPER_ID_MODULE = 10124;

        public const long HELPER_ID_ACTOR_GROUP = 10125;
        public const long HELPER_BUILT_IN = 10126;

        public const long HELPER_ACTION_HIERARCHY = 10127;
        public const long HELPER_ACTION_HIERARCHY_LEVEL = 10128;
        public const long HELPER_ACTION_HIERARCHY_ROOT = 10129;

        public const long HELPER_SERIAL_NBR_PARENT = 10130;
        public const long HELPER_TIME = 10131;
        public const long HELPER_ALARM_MESSAGE = 10132;
        public const long HELPER_IS_CONFIRMED = 10133;
        public const long HELPER_CONFIRMED_BY = 10134;
        public const long HELPER_CONFIRM_TIME = 10135;

        public const long HELPER_DEVICE_HIERARCHY = 10136;
        public const long HELPER_DEVICE_HIERARCHY_LEVEL = 10137;
        public const long HELPER_DEVICE_HIERARCHY_ROOT = 10138;

        public const long LDAP_PATH = 10139;
        public const long LDAP_LOGIN = 10140;
        public const long LDAP_PASSWORD = 10141;

        public const long WESB_PROVIDER_HOST = 10142;
        public const long WESB_PROVIDER_USERNAME = 10143;
        public const long WESB_PROVIDER_PASSWORD = 10144;
        public const long WESB_PROVIDER_IS_TEST_HOST = 10145;

        #endregion
        #region IMR Service Centre params
        public const long ISSUE_WORK_ORDER_JCI_ID = 17001;     // <Brak opisu> 
        public const long LOCATION_IMR_SERVER_ID = 17002;     // <Brak opisu>
        public const long DISTRIBUTOR_SLA_ID = 17003;     // <Brak opisu>
        public const long ISSUE_ATTACHMENT = 17004;     // <Brak opisu>
        public const long DEPOSITORY_ID = 17005;     // <Brak opisu>
        public const long CREDIT_DEPOSITORY_ID = 17006;     // urzadzenia, ktore zosta�y �ci�gni�te z lokalizacji, a by�y jeszcze na gwarancji
        public const long METER_IMR_SERVER_ID = 17007;
        public const long OPERATOR_IMRSC_COLOR = 17008;     // <Brak opisu>
        public const long LOCATION_ACTOR_ID = 17009;     // <Brak opisu>
        public const long LOCATION_SOURCE_SERVER_ID = 17010;     // <Brak opisu>
        public const long ISSUE_IMRMYSC_VISIBLE = 17011;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_OKO_SERIAL = 17012;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_ALEVEL_SERIAL = 17013;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_SIM_SERIAL = 17014;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_DEVICE_VERSION = 17015;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_MENU_PIN = 17016;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_SIM_PIN = 17017;     // <Brak opisu>
        public const long IMRSC_TMQ_LOCATION_SIM_PUK = 17018;     // <Brak opisu>
        public const long PACKAGE_DIRECTION = 17020;     // <Brak opisu>
        public const long PACKAGE_ADDRESS = 17021;     // <Brak opisu>
        public const long TASK_FITTER_VISIT_DATE = 17100;     // <Brak opisu>
        public const long TASK_FITTER_PAYMENT_DONE = 17101;     // <Brak opisu>
        public const long TASK_CLIENT_PAYMENT_DONE = 17102;     // <Brak opisu>
        public const long TASK_ATTACHMENT = 17103;     // <Brak opisu>
        public const long TASK_IMRMYSC_VISIBLE = 17104;     // <Brak opisu>
        public const long TASK_PAYMENT_AMOUNT = 17105;     // <Brak opisu>
        public const long TASK_PAYMENT_NOTE = 17106;     // <Brak opisu>
        public const long TASK_FITTER_PAYMENT_DATE = 17107;     // <Brak opisu>
        public const long PAYMENT_AND_PRIORITY_GUIDELINES = 17108;
        public const long ISSUE_DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_ID = 17109;//ID wybranej opcji serwisowej dla zg�oszenia
        public const long DEVICE_WARRANTY_DATE_DURING_DEINSTALLATION = 17110;//Data gwarancji urz�dzenia w momencie demonta�u z obiektu
        public const long DEVICE_WARRANTY_IS_VALID_DURING_DEINSTALLATION = 17111;//Czy urz�dzenie by�o na gwarancji w momencie demonta�u z obiektu
        public const long DEVICE_WARRANTY_SERIAL_NUMBER = 17112;//numer seryjny urz�dzenia kt�re zosta�o zast�pione
        public const long ID_FORM_TASK_TYPE = 17113;//rodzaj formularza (rodzaje zaszyte w kodzie), wykorzystywany w TaskAddTasksControl, 0-standart, 1-heating
        public const long TASK_ADDITIONAL_PAYMENT = 17114;
        public const long TASK_WARRANTY_SERVICE = 17115;
        public const long DEVICE_HOSTING_INVOICE = 17116;
        public const long DEVICE_MAINTENANCE_INVOICE = 17117;
        public const long DEVICE_COMMUNICATION_INVOICE = 17118;
        public const long DEVICE_INVOICE_ID_DEVICE_STATE_TYPE = 17119;
        public const long DEVICE_INVOICE_ID_SERVICE_LIST = 17120;
        public const long DEVICE_INVOICE_PHONE = 17121;
        public const long DEVICE_INVOICE_ID_RELATED_TASK = 17122;
        public const long DEVICE_INVOICE_ID_LAST_SHIPPING_LIST = 17123;
        public const long DEVICE_COMMUNICATION_INVOICE_AGGREGATE = 17124;
        public const long DEVICE_HOSTING_INVOICE_AGGREGATE = 17125;
        public const long DEVICE_MAINTENANCE_INVOICE_AGGREGATE = 17126;
        public const long DEVICE_HOSTING_MONTHLY_FEE = 17127;
        public const long DEVICE_HOSTING_MONTHLY_FEE_MAX = 17128;
        public const long DEVICE_TARIFF_ID = 17129;
        public const long DEVICE_MAINTENANCE_MONTHLY_FEE = 17130;
        public const long DEVICE_MAINTENANCE_MONTHLY_FEE_MAX = 17131;
        public const long DEVICE_COMMUNICATION_MONTHLY_FEE = 17132;
        public const long METER_SOURCE_SERVER_ID = 17133;
        #endregion
        #region DISTRIBUTOR params
        public const long DISTRIBUTOR_DATA_ARCHIVE_DAYS = 17504;
        //public const long DISTRIBUTOR_DEVICE_POOL = 17505;
        public const long DISTRIBUTOR_DELIVERY_COMMIT_METHOD = 17506;
        public const long DISTRIBUTOR_COUNTRY = 17507;
        public const long DISTRIBUTOR_SYSTEM_IMPLEMENTATION_STAGE = 17508;//faza wdrozenia rozwi�za� 
        public const long DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_ID = 17509;  //Identyfikator opcji umowy serwisowej (np. dla Amerigas - 1. Przydomowe/blokowe, 2. Przemys�owe Standard, 3. Przemys�owe Express)
        public const long DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_NAME = 17510; //Nazwa opcji umowy serwisowej (np. dla Amerigas - 1. Przydomowe/blokowe, 2. Przemys�owe Standard, 3. Przemys�owe Express)
        public const long DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_DEADLINE_IN_DAYS = 17511;//Ilo�� dni na wykonanie danego zlecenia serwisowego (np. dla Amerigas - 1. Przydomowe/blokowe - 14 dni - , 2. Przemys�owe Standard - 6 dni, 3. Przemys�owe Express - 3 dni)
        public const long DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_LOCATION_GROUP_IDS = 17512; //Identyfikator grupy lokalizacji jakiego tyczy si� umowa serwisowa (np. dla Amerigas - 1. Bloki, 2. Przemys�owe, 3. Domestic) - string z ID oddzielany �rednikami

        public const long DISTRIBUTOR_SERVICE_GROUP_ID = 17513;     //Identyfikator grupy serwisowej do kt�rej przypisana zostanie lokalizacja (zdefiniowane w DISTRIBUTOR_DATA) (np. dla Amerigas - 1. Przydomowe/blokowe, 2. Przemys�owe Standard, 3. Przemys�owe Express)
        public const long DISTRIBUTOR_SERVICE_GROUP_NAME = 17514;   //Nazwa grupy serwisowej do kt�rej przypisana zostanie lokalizacja (zdefiniowane w DISTRIBUTOR_DATA) (np. dla Amerigas - 1. Przydomowe/blokowe, 2. Przemys�owe Standard, 3. Przemys�owe Express)
        public const long DISTRIBUTOR_IMR_SERVER_ID = 17515;        //Identyfikator serwera IMR, kt�ry obs�uguje dystrybutora

        public const long DISTRIBUTOR_IMR_ST_FILE_ID = 17516;       //identyfikator pliku do wyswietlenia w KnowledgeBase

        public const long DISTRIBUTOR_PROTECTOR_OPERATOR_ID = 17517; //id operatora kt�ry jest opiekunem danego dystrybura
        public const long DISTRIBUTOR_REPRESENTATIVE_ID_OPER = 17518;//id operatora reprezentujacego dystrybutora
        public const long DISTRIBUTOR_AUTOMATICALLY_SUSPEND_ISSUE = 17519;//automatycznie ustaw statu issue na wstrzymany kiedy wysp�pi problem z realizacja taska do niego przynalezacego
        public const long DISTRIBUTOR_LOCATION_OPERATIONAL_ALLOW_EQUIPMENT_CHANGE = 17520;//zabron edycji wyposazenia lokalizacji gdy jest w stanie operational
        public const long DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID = 17521;//id lokalizacji serwisowej do ktorej wpadajaurzadzenia wracajace w paczkach od SOT

        public const long DISTRIBUTOR_ONE_SLAVE_ONE_MASTER = 17522;

        public const long DISTRIBUTOR_LOGO_95x65 = 17523;   // logo dystrybutora w formacie PNG i rozmiarze 95x65
        public const long DISTRIBUTOR_DEFAULT_CURRENCY_ID = 17524;// domy�lna waluta dystrybutora
        public const long DISRIBUTOR_PAYMENT_RECEIPT_TYPE = 17525;
        public const long DISRIBUTOR_MONTHLY_STATEMENT_TYPE = 17526;
        public const long DISTRIBUTOR_DISPLAY_LOCATION_RDG = 17527;
        public const long DISTRIBUTOR_DIMA_LOCATION_GROUP_TAB = 17528;
        public const long DISTRIBUTOR_RDG_CODE = 17529;     //Kod RDG z PSG dla distributora
        public const long DISTRIBUTOR_SERVICE_FORM_TYPE = 17530;
        public const long DISTRIBUTOR_SEND_SMS_ACTION_WHEN_ASSIGNING_OKO_TO_LOCATION = 17531;   //Podczas przypisywania urz�dzenia klasy OKO do lokalizacji wysy�ana jest akcja monta�owa SMS (IGME1/MDG)
        public const long DISTRIBUTOR_SITA_INSTALLATION_FORM_TYPE = 17532;
        public const long DISTRIBUTOR_SITA_GASMETER_INSTALLATION_FORM_MODE = 17533;
        public const long DISTRIBUTOR_SITA_LPG_INSTALLATION_FORM_MODE = 17534;
        public const long DISTRIBUTOR_SITA_ASK_IF_DOWNLOAD_ALL_INSTALLED_LOCATIONS = 17535;     //Je�li data_type si� pojawi z warto�ci� True - SITA pyta czy pobra� wszystkie zamontowane wcze�niej lokalizacje (do np. wykonywania akcji serwisowych)
        public const long DISTRIBUTOR_SITA_WALKED_BY_SCENARIO_TYPE = 17536;                     //Scenariusz odczytu obchodzonego w SITA: 1 - Broadcast, 2 - Multicast, 3 - Algorytm Artura i Zbyszka (odpytanie o adresy w zasi�gu, i odpytywanie adres�w w zasi�gu przez multicast). W przypadku braku takiego data_Type'u domy�lnie brany jest odczyt w scenariuszu 1
        public const long DISTRIBUTOR_IMPORTANCE = 17537;//znaczenie/wa�no��/wielko�� klienta
        public const long DISTRIBUTOR_DEVICE_DISTRIBUTOR_AFTER_DEINSTALLATION = 17538;//zmiana dystrybutora w momencie deinstalacji z obiektu
        public const long DISTRIBUTOR_DEVICE_OWNERSHIP = 17539;
        public const long DISTRIBUTOR_DEVICE_OWNERSHIP_ID_OPERATOR = 17540;//operator wprowadzajacy zmiane
        public const long DISTRIBUTOR_DEVICE_OWNERSHIP_TIMESTAMP = 17541;
        public const long DISTRIBUTOR_DEVICE_OWNERSHIP_RENT_ID_DISTRIBUTOR = 17542;//faktyczny posiadacz u�yczonego urz�dzenia
        public const long DISTRIBUTOR_AIUT_SERVICE_PARTNER = 17543;
        public const long DISTRIBUTOR_MAINTENANCE_SERVICE = 17544;
        public const long DISTRIBUTOR_RELATED_ID_DISTRIBUTOR = 17545;
        public const long DISTRIBUTOR_DEVICE_OWNERSHIP_SHARE_ID_DISTRIBUTOR = 17546;

        public const long DISTRIBUTOR_MONTHLY_HOSTING_FEE = 17547;
        public const long DISTRIBUTOR_MONTHLY_HOSTING_FEE_MAX = 17548;
        public const long DISTRIBUTOR_MONTHLY_MAINTENANCE_FEE = 17549;
        public const long DISTRIBUTOR_MONTHLY_MAINTENANCE_FEE_MAX = 17550;
        public const long DISTRIBUTOR_TARIFF_ID = 17551;

        public const long DISTRIBUTOR_CID_UNIQUE = 17552;

        public const long DISTRIBUTOR_AIUT_SERVICE_STANDARD_PERIOD = 17553;//podstawowy czas trwania serwisu
        public const long DISTRIBUTOR_AIUT_SERVICE_DEVICES_PER_WEEK = 17554;//ilo�� urz�dze� serwisowanych w tydzie�
        public const long DISTRIBUTOR_AIUT_SERVICE_LEGALIZATION_TIME = 17555;//okres przebywania na legalizacji
        public const long DISTRIBUTOR_AWSR_IMR_SERVER_ID = 17556;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_TYPE = 17557;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_SERVICE_OPTION = 17558;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_FROM = 17559;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_TO = 17560;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_AMOUNT = 17561;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_CANCELED = 17562;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_CREATED = 17563;

        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_ID_TASK_TYPE = 17564;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_ID_SERVICE_OPTION = 17565;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_CREATION_DATE = 17566;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_CANCELATION_DATE = 17567;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_AMOUNT = 17568;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_ALLOW_TASK_SUSPENSION = 17569;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_TASK_DONE_IN_DAYS = 17570;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_PROTOCOL_SUBMIT_IN_DAYS = 17571;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_FROM = 17572;
        public const long DISTRIBUTOR_OBJECT_SERVICE_FITTER_PREMIUM_TO = 17573;

        public const long DISTRIBUTOR_GREACEFUL_PERIOD_MONTHS = 17574;
        public const long DISTRIBUTOR_DEFAULT_OPERATOR_TARIFF_ID = 17575;
        public const long DISTRIBUTOR_PERFORMANCE_VALUE = 17576;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM = 17577;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_PARENT = 17578;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_WEIGHT = 17579;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_RED = 17580;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_YELLOW = 17581;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_IGNORED = 17582;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS = 17583;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_ADDITIONAL_EMAIL = 17584;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_INDEX = 17585;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_ID_REFERENCE_TYPE = 17586;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_REFERENCE_VALUE = 17587;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ID_REFERENCE_TYPE = 17588;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_LOCATION_IN_KPI = 17589;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AGGREGATION_COMPUTE_TYPE = 17590;

        public const long SHIPPING_LIST_DOCUMENT_TYPE = 17591;
        public const long DISTRIBUTOR_FTP_LOGIN = 17592;
        public const long DISTRIBUTOR_FTP_PASSWORD = 17593;
        public const long DISTRIBUTOR_SHIPPING_MAIL_NOTE = 17594;

        public const long DISTRIBUTOR_READOUT_ID_DATA_TYPE = 17595;
        public const long DISTRIBUTOR_READOUT_ID_DEVICE_TYPE = 17596;
        public const long DISTRIBUTOR_READOUT_ID_METER_TYPE_CLASS = 17597;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_DOT_VISIBLE = 17598;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_AGGREGATION_TYPE = 17599;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR = 17600;
        public const long DISTRIBUTOR_TASK_PAYMENT_TYPE = 17601;
        public const long DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END = 17602;
        public const long DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END_UNINSTALL_ID_DISTRIBUTOR_OWNER = 17603;//id w�a�ciciela jakiego ma otrzymac urzadzenie po deinstalacji
        public const long DISTRIBUTOR_READOUT_DELAY_TIME_MINUTE = 17604;

        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_PART_TYPE = 17605;//Identyfikator sk�adowej sumy rozliczenia np, us�uga, dojazd, wykonanie jakiej� specyficznej czynno�ci
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TYPE = 17606;//1 - Fitter, 2 - Client
        public const long DISTRIBUTOR_TASK_DETAILS_DEFAULT_ID_FITTER_AREA_VISIBLE = 17607;

        public const long DISTRIBUTOR_FUELPRIME_SCANS_FTP_ADDRESS = 17608;
        public const long DISTRIBUTOR_FUELPRIME_SCANS_FTP_LOGIN = 17609;
        public const long DISTRIBUTOR_FUELPRIME_SCANS_FTP_PASSWORD = 17610;
        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_ARTICLE = 17611;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_TYPE = 17612;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_VALUE = 17613;
        public const long DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_DEADLINE_IS_CALENDAR_DAYS = 17614;
        public const long DISTRIBUTOR_ALL_ALLOWED_DISTRIBUTOR = 17615;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_OKO_READOUT_COMPUTING_MODE = 17616;

        public const long DISTRIBUTOR_SEASON_NAME = 17617;
        public const long DISTRIBUTOR_SEASON_FROM = 17618;
        public const long DISTRIBUTOR_SEASON_TO = 17619;
        public const long DISTRIBUTOR_SEASON_CREATION_DATE = 17620;
        public const long DISTRIBUTOR_SEASON_CANCELLATION_DATE = 17621;
        public const long DISTRIBUTOR_SEASON_CREATION_ID_OPERATOR = 17622;
        public const long DISTRIBUTOR_SEASON_CANCELLATION_ID_OPERATOR = 17623;
        public const long DISTRIBUTOR_SEASON_ID_REFERENCE_TYPE = 17624;
        public const long DISTRIBUTOR_SEASON_PERIOD_VALUE = 17625;
        public const long DISTRIBUTOR_SEASON_PERIOD_UNIT = 17626;
        public const long DISTRIBUTOR_SEASON_ID_REFERENCE_TYPE_TYPE = 17627;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE = 17628;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE_WITH_TASK = 17629;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_TASK = 17630;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME = 17631;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME_UNIT = 17632;

        public const long DISTRIBUTOR_INSTALLATION_FORM_TYPE = 17633;

        public const long DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_GROUP = 17634;
        public const long DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID = 17635;

        public const long DISTRIBUTOR_PRIORITY = 17636;
        public const long DISTRIBUTOR_PRIORITY_FROM = 17637;
        public const long DISTRIBUTOR_PRIORITY_TO = 17638;
        public const long DISTRIBUTOR_PRIORITY_CREATION_DATE = 17639;
        public const long DISTRIBUTOR_PRIORITY_CANCELLATION_DATE = 17640;
        public const long DISTRIBUTOR_PRIORITY_CREATION_ID_OPERATOR = 17641;
        public const long DISTRIBUTOR_PRIORITY_CANCELLATION_ID_OPERATOR = 17642;

        public const long DISTRIBUTOR_SITA_UPDATE_LINK = 17643;
        public const long DISTRIBUTOR_SITA_VERSION = 17644;

        public const long DISTRIBUTOR_LOCATION_DETAILS_WIZARD_ID_LOCATION_GROUP = 17645;
        public const long DISTRIBUTOR_DEFAULT_TARIFF_ID = 17646;//nie dotyczy taryf operatora (nasze taryfy wewn�trze zwi�zane z fakturowaniem klienta - inny typ taryfy)
        public const long DISTRIBUTOR_DEFAULT_SIM_CARD_ID_OWNER = 17647;
        public const long DISTRIBUTOR_SERVICE_WIZARD = 17648;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_RESPONSIBLE_OPERATOR = 17649;

        public const long DISTRIBUTOR_LOCATION_INSTALLATION_WIZARD_MAIN_ID_LOCATION_GROUP = 17650;
        public const long DISTRIBUTOR_LOCATION_INSTALLATION_WIZARD_SECONDARY_ID_LOCATION_GROUP = 17651;
        public const long DISTRIBUTOR_AVAILABLE_TANK_CAPACITY = 17652;
        public const long DISTRIBUTOR_DEFAULT_TANK_ID_METER_TYPE = 17653;
        public const long DISTRIBUTOR_DEFAULT_GAS_METER_ID_METER_TYPE = 17654;
        public const long DISTRIBUTOR_DEFAULT_PARENT_TANK_ID_PRODUCT_CODE = 17655;

        public const long DISTRIBUTOR_TASK_STATUS_FLOW_PREV = 17656;
        public const long DISTRIBUTOR_TASK_STATUS_FLOW_NEXT = 17657;
        public const long DISTRIBUTOR_ISSUE_STATUS_FLOW_PREV = 17658;
        public const long DISTRIBUTOR_ISSUE_STATUS_FLOW_NEXT = 17659;

        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_CLASS = 17660;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_PROCEDURE = 17661;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_REFERENCE_TYPE = 17662;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_GROUP = 17663;
        public const long DISTRIBUTOR_DASHBOARD_GROUP = 17664;
        public const long DISTRIBUTOR_DASHBOARD_GROUP_NAME = 17665;
        public const long DISTRIBUTOR_DASHBOARD_GROUP_ID_DESCR = 17666;
        public const long DISTRIBUTOR_DASHBOARD_GROUP_BORDER_VISIBLE = 17667;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_TYPE = 17668;

        public const long DISTRIBUTOR_SITA_DEFAULT_CONTROL_NAME = 17669;
        public const long DISTRIBUTOR_SITA_DEFAULT_CONTROL_VALUE = 17670;

        public const long DISTRIBUTOR_INSTALLATION_WIZARD_GROUP_REQUIRED = 17671;

        public const long DISTRIBUTOR_SITA_WIZARD_ID = 17672;

        public const long DISTRIBUTOR_EVC_ID_DEVICE_TYPE_GROUP = 17673;

        public const long DISTRIBUTOR_SITA_WIZARD_FILE_INDEX = 17674;
        public const long DISTRIBUTOR_SITA_WIZARD_FILE_ID = 17675;

        public const long DISTRIBUTOR_MAIL_ALARM_AS_TOPIC = 17676;

        public const long DISTRIBUTOR_DASHBOARD_GROUP_REFRESH_PERIOD = 17677;
        public const long DISTRIBUTOR_DASHBOARD_GROUP_WIDTH = 17678;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_WIDTH = 17679;

        public const long DISTRIBUTOR_VERSION_STATE_FLOW_PREV = 17680;
        public const long DISTRIBUTOR_VERSION_STATE_FLOW_NEXT = 17681;

        public const long DISTRIBUTOR_FITTER_DEPOSITORY_REQUIRED = 17682;
        public const long DISTRIBUTOR_FITTER_DEPOSITORY_CONTENT_MODE = 17683;

        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_DAY_ACCEPTABLE_INCORRECT_TIME = 17684;
        public const long DISTRIBUTOR_SMARTGAS_UPDATE_LINK = 17685;
        public const long DISTRIBUTOR_SMARTGAS_VERSION = 17686;

        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_IS_ALARM = 17687;
        public const long DISTRIBUTOR_DASHBOARD_INDICATOR_ID_MODULE = 17688;
        public const long DISTRIBUTOR_DASHBOARD_GROUP_ID_MODULE = 17689;
        public const long DISTRIBUTOR_ID_IMR_SERVER_EXISTANCE = 17690;
        public const long DISTRIBUTOR_IMPORT_DLL_NAME = 17691;
        public const long DISTRIBUTOR_IMPORT_DLL_TYPE = 17692;
        public const long DISTRIBUTOR_SITA_PHONE_NUMBER = 17693;
        public const long DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MONITORED_TYPE_OF_AGGREGATION = 17694;
        public const long DISTRIBUTOR_SMARTGAS_LOGO = 17695;
        public const long DISTRIBUTOR_TECHNICAL_SUPPORT_TEL1 = 17696;
        public const long DISTRIBUTOR_TECHNICAL_SUPPORT_TEL2 = 17697;
        public const long DISTRIBUTOR_TECHNICAL_SUPPORT_EMAIL = 17698;

        public const long DISTRIBUTOR_SITA_ACTION_TIMEOUT = 17699;
        public const long DISTRIBUTOR_LOGO = 17700;
        public const long DISTRIBUTOR_OPERATOR_LDAP_SYNCHRONIZATION_ACTION_TYPE = 17701;

        public const long DISTRIBUTOR_HIERARCHY_DEVICE_BELOW_NODES_LOAD = 17702;

        public const long DITRIBUTOR_TASK_STATUS_CHANGE_ID_LOCATION_STATE_TYPE = 17703;

        public const long DISTRIBUTOR_DASHBOARD_FIT_TO_SCREEN = 17704;
        public const long DISTRIBUTOR_DASHBOARD_USE_MARGIN = 17705;
        public const long DISTRIBUTOR_DASHBOARD_USE_PADDING = 17706;

        public const long DISTRIBUTOR_EXPORT_DLL_NAME = 17707;
        public const long DISTRIBUTOR_EXPORT_DLL_TYPE = 17708;

        public const long DISTRIBUTOR_DASHBOARD_KEEP_ASPECT_RATIO = 17709;
        public const long DISTRIBUTOR_DASHBOARD_WIDTH = 17710;
        public const long DISTRIBUTOR_DASHBOARD_MAX_WIDTH = 17711;
        public const long DISTRIBUTOR_DASHBOARD_MAX_HEIGHT = 17712;

        public const long DISTRIBUTOR_EMAIL_FOOTER = 17713;

        #endregion
        #region TARRIF params
        public const long TARIFF_PRODUCT_CODE = 18510;
        public const long TARIFF_CALOFIFIC_VALUE = 18511;

        public const long TARIFF_STANDING_CHARGE = 18512;
        public const long TARIFF_STANDING_CHARGE_VAT = 18513;

        public const long TARIFF_CARBON_TAX = 18514;
        public const long TARIFF_CARBON_TAX_VAT = 18515;


        //This data types are defined in tariff options and uses indexes
        public const long TARIFF_IS_ACTIVE = 18516;
        public const long TARIFF_ACTIVATION_TIME = 18517;
        public const long TARIFF_DEACTIVATION_TIME = 18518;

        public const long TARIFF_PRODUCT_CHARGE = 18519;
        public const long TARIFF_PRODUCT_CHARGE_VAT = 18520;
        public const long TARIFF_NOTES = 18521;
        public const long TARIFF_MONTHLY_CHARGE = 18522;
        public const long TARIFF_GAS_M3_COST = 18523;
        public const long TARIFF_MONTHLY_CHARGE_PER_GAS_RECEIVER = 18524;
        public const long TARIFF_GAS_RECEIVER_NUMBER = 18525;
        public const long TARIFF_ID_DATA_TYPE_GROUP = 18526;
        public const long TARIFF_MINIMUM_CONTRACTUAL_POWER = 18527;         //Minimalna moc umowna
        public const long TARIFF_MINIMUM_ANNUAL_GAS_CONSUMPTION = 18528;    //Minimalna roczna ilo�� pobieranego paliwa gazowego
        public const long TARIFF_NUMBER_OF_READOUTS_PER_YEAR = 18529;       //Liczba odczyt�w uk�adu pomiarowego
        public const long TARIFF_MAXIMUM_ANNUAL_GAS_CONSUMPTION = 18530;    //Maksymalna roczna ilo�� pobieranego paliwa gazowego
        public const long TARIFF_MAXIMUM_CONTRACTUAL_POWER = 18531;         //Maksymalna moc umowna

        public const long TARIFF_TAX = 18532;
        public const long TARIFF_PAYMENT_CONDITION = 18533;
        public const long TARIFF_PAYMENT_CONDITION_DEVICES_AMOUNT = 18534;
        public const long TARIFF_ACCOUNT_INFINITE_VALIDITY = 18535;
        public const long TARIFF_ACCOUNT_TIME_VALIDITY_SCENARIO = 18536;
        public const long TARIFF_ACCOUNT_CREDIT_VALIDITY_SCENARIO = 18537;
        public const long TARIFF_ACCOUNT_NUMBER = 18538;
        public const long TARIFF_SERVICE = 18539;
        public const long TARIFF_TYPE = 18540;
        public const long TARIFF_SMS_COST = 18541;
        public const long TARIFF_GPRS_COST = 18542;
        public const long TARIFF_COUNTRY = 18543;
        public const long TARIFF_DEPRECATED = 18544;
        public const long TARIFF_CODE = 18545;

        public const long TARIFF_ANNUAL_GAS_CONSUMPTION = 18546;        //Roczna ilo�� odbieranego  paliwa gazowego
        public const long TARIFF_ANNUAL_POWER_CONSUMPTION = 18547;      //Roczna ilo�� odbieranej mocy
        public const long TARIFF_CONTRACTUAL_GAS_CONSUMPTION = 18548;   //Moc umowna (wyra�ona w jednostkach obj�to�ci na godzin�)
        public const long TARIFF_CONTRACTUAL_POWER = 18549;             //Moc umowna (wyra�ona w jednostkach mocy na godzin�)

        public const long TARIFF_CALCULATE_TIME_PERIOD_TYPE = 18550;   //Typ okresu przeliczania taryfy
        public const long TARIFF_CALCULATE_START_TIME = 18551;         //Pocz�tek obowi�zywania przeliczania taryfy
        public const long TARIFF_CALCULATE_EXECUTE_TIME = 18552;       //Czas wykonywania przeliczania taryfy
        public const long TARIFF_CUSTOM_INTERVAL_BETWEEN_CALCULATE_EXECUTE = 18553;  //dowolny odst�p (w godzinach) pomi�dzy kolejnymi wywo�aniami przeliczania

        public const long TARIFF_MONTHLY_ADDITIONAL_CHARGE = 18554;
        public const long TARIFF_MONTHLY_MIN_CONSUMPTION_FOR_NO_ADDITIONAL_CHARGE = 18555;
        public const long TARIFF_ADDITIONAL_CHARGE_PLUGIN_NAME = 18556;

        public const long TARIFF_BILLING_PLUGIN_PREPAID_FREQ_RECEIVED = 18600;
        public const long TARIFF_BILLING_PLUGIN_TOPUP_ACTION_FINISHED = 18601;

        public const long HELPER_ID_TARIFF = 18602;

        #endregion
        #region CONSUMER & CONSUMER_TYPE params
        public const long CONSUMER_TAG_NO = 18001;
        public const long CONSUMER_DEBT = 18002;
        public const long CONSUMER_CREDIT = 18003;
        public const long CONSUMER_RECOMMENDED_TOP_UP = 18004;
        public const long CONSUMER_MESSAGE_BODY = 18005;
        public const long CONSUMER_MESSAGE_TYPE = 18006;

        public const long CONSUMER_LAST_AVG_DAILY_CALCULATION = 18007;
        public const long CONSUMER_LAST_AVG_WEEKLY_CALCULATION = 18008;

        public const long CONSUMER_MODULE_PAYMENT_NBR = 18009;
        public const long CONSUMER_MODULE_ID = 18010;

        public const long CONSUMER_NOTIFICATION_OPERATOR_ID = 18011;
        public const long CONSUMER_NOTIFICATION_GCM_ID = 18012;
        public const long CONSUMER_NOTIFICATION_GCM_ENABLED = 18013;
        public const long CONSUMER_NOTIFICATION_SMS_ENABLED = 18014;
        public const long CONSUMER_NOTIFICATION_TAG = 18015;
        public const long CONSUMER_NOTIFICATION_UNIQUE_MOBILE_DEVICE_ID = 18016;
        //
        public const long CONSUMER_TYPE_AGREED_OWED = 18101;
        public const long CONSUMER_TYPE_EMERGENCY_CREDIT = 18102;
        public const long CONSUMER_TYPE_REPAYMENT_RULE = 18103;
        public const long CONSUMER_TYPE_PREPAID_ENABLED = 18104;
        public const long CREDIT_LEVEL_NOTIFICATION = 18105;
        #endregion
        #region REFUEL params
        public const long REFUEL_START_TIME = 19001;
        public const long REFUEL_END_TIME = 19002;
        public const long REFUEL_TOTAL_VOLUME = 19003;
        public const long REFUEL_TOTAL_REFERENCE_VOLUME = 19004;
        public const long REFUEL_TOTAL_WEIGHT = 19005;
        public const long REFUEL_START_VOLUME = 19006;
        public const long REFUEL_END_VOLUME = 19007;
        public const long REFUEL_BREAKS_COUNT = 19008;
        public const long REFUEL_STEP_VOLUME = 19009;
        public const long REFUEL_START_TEMPERATURE = 19010;
        public const long REFUEL_END_TEMPERATURE = 19011;
        public const long REFUEL_STATE = 19012;
        public const long REFUEL_ORDER_NUMBER = 19013;
        public const long REFUEL_TRUCK_NUMBER = 19014;
        public const long REFUEL_TRAILER_NUMBER = 19062;
        public const long REFUEL_DRIVER_NAME = 19015;
        public const long REFUEL_TERMINAL_NAME = 19016;
        public const long REFUEL_CARRIER_NAME = 19017;
        public const long REFUEL_LOADING_DENSITY = 19018;
        public const long REFUEL_LOADING_TEMPERATURE = 19019;
        public const long REFUEL_TEMPERATURE = 19020;
        public const long REFUEL_RESIDUAL_START_TIME = 19021;
        public const long REFUEL_RESIDUAL_END_TIME = 19022;
        public const long REFUEL_RESIDUAL_TOTAL_VOLUME = 19023;
        public const long REFUEL_RESIDUAL_REF_TOTAL_VOLUME = 19024;

        public const long REFUEL_DELIVERY_START_TIME = 19025;
        public const long REFUEL_DELIVERY_START_TIME_CLICK = 19026;
        public const long REFUEL_DELIVERY_END_TIME = 19027;
        public const long REFUEL_DELIVERY_END_TIME_CLICK = 19028;
        public const long REFUEL_DELIVERY_COMMIT_TIME_CLICK = 19029;
        public const long REFUEL_OPEN_SEAL_NUMBER = 19030;
        public const long REFUEL_CLOSE_SEAL_NUMBER = 19031;
        public const long REFUEL_COMPARTMENTS = 19032;
        public const long REFUEL_GUID = 19051;

        public const long REFUEL_TRUCK_LOCATION_ID = 19033;
        public const long REFUEL_TRUCK_MAX_SIZE = 19034;
        public const long REFUEL_DRIVERS_COUNT = 19035;
        public const long SIMULATION_ID = 19036;
        public const long REFUEL_DELIVERY_COST = 19037;

        public const long DI_SIMULATION_PARAM_C_KM = 19038;
        public const long DI_SIMULATION_PARAM_C_CONST = 19039;
        public const long DI_SIMULATION_PARAM_C_PUMP = 19040;
        public const long DI_SIMULATION_PARAM_C_ROUTE = 19041;
        public const long DI_SIMULATION_PARAM_C_TANKER = 19042;
        public const long DI_SIMULATION_PARAM_C_D_MIN = 19043;
        public const long DI_SIMULATION_PARAM_C_INCIDENT = 19044;
        public const long DI_SIMULATION_PARAM_C_DRIVER = 19045;
        public const long DI_SIMULATION_PARAM_T_MAX = 19046;
        public const long DI_SIMULATION_PARAM_T_DELTA = 19047;
        public const long DI_SIMULATION_PARAM_T_MIN = 19048;
        public const long DI_SIMULATION_PARAM_SEED = 19049;
        public const long DI_SIMULATION_PARAM_N_DAYS = 19050;

        public const long REFUEL_STEP_SOLD_VOLUME_GROSS = 19052;
        public const long REFUEL_STEP_SOLD_VOLUME_NET = 19053;
        public const long REFUEL_STEP_FUEL_TEMPERATURE = 19054;
        public const long REFUEL_STEP_IDDLE_STATUS = 19055;

        public const long REFUEL_SOLD_VOLUME_GROSS = 19056;
        public const long REFUEL_SOLD_VOLUME_NET = 19057;
        public const long REFUEL_DATA_STATUS = 19058;

        public const long REFUEL_DRIVER = 19059; // JB: nieuzywane (jest REFUEL_DRIVER_NAME)
        public const long REFUEL_TRUCK = 19060; // JB: nieuzywane (jest REFUEL_TRUCK_NUMBER)
        public const long REFUEL_DEPOT = 19061; // JB: nieuzywane (jest REFUEL_TERMINAL_NAME)

        public const long REFUEL_FLOWMETER_SERIAL_NUMBER = 19063;
        public const long REFUEL_SHIPMENT_NUMBER = 19064;
        public const long REFUEL_CUSTOMER_START_TIME = 19065;

        public const long REFUEL_ESTIM_START_TIME = 19066;
        public const long REFUEL_ESTIM_START_VOLUME = 19067;
        public const long REFUEL_ESTIM_END_TIME = 19068;
        public const long REFUEL_ESTIM_END_VOLUME = 19069;
        public const long REFUEL_IDLE_ENDING = 19070;

        public const long REFUEL_STATUS = 19071;
        public const long REFUEL_NOTES = 19072;
        public const long REFUEL_CONFIRMARION_TIME = 19073;
        public const long REFUEL_VOLUME_DETECTED_BY_OUTLIER = 19074;

        public const long REFUEL_AUDIT_ID = 19075;
        public const long REFUEL_AUDIT_DATE = 19076;
        public const long REFUEL_AUDIT_CREATION_DATE = 19077;
        public const long REFUEL_AUDIT_DEADLINE = 19078;
        public const long REFUEL_AUDIT_ACKNOWLEDGING_PERSON_NAME = 19079;
        public const long REFUEL_AUDIT_PERFORMING_PERSON_NAME = 19080;
        public const long REFUEL_AUDIT_REGISTRANT_NAME = 19081;
        public const long REFUEL_AUDIT_PRIORITY = 19082;
        public const long REFUEL_AUDIT_STATE = 19083;
        public const long REFUEL_AUDIT_SUMMARY = 19084;

        public const long REFUEL_START_REF_VOLUME = 19085;
        public const long REFUEL_END_REF_VOLUME = 19086;
        public const long REFUEL_DELIVERY_DAY = 19087;
        public const long REFUEL_ORDINAL_NUMBER = 19088;
        public const long REFUEL_DOC_NUMBER = 19089;
        public const long REFUEL_SOLD_VOLUME_START = 19090;
        public const long REFUEL_SOLD_VOLUME_END = 19091;
        #endregion
        #region CONSUMER_TRANSACTION

        public const long CONSUMER_TRANSACTION_NOTES = 19501;
        public const long CONSUMER_TRANSACTION_PAYMENT_ACCOUNTING_MONTH = 19502;

        #endregion
        #region MONITORING params
        public const long MON_QUEUE_WATCHER_QUEUE_NAME = 21001;
        public const long MON_QUEUE_WATCHER_QUEUE_HI_LEVEL = 21002;
        public const long MON_QUEUE_WATCHER_QUEUE_HI_LEVEL_IGNORE_COUNT = 21003;

        public const long MON_PERC_CPU = 21010;
        public const long MON_BYTE_RAM = 21011;
        public const long MON_PAGE_FILE = 21012;
        public const long MON_DISK = 21013;
        public const long MON_NETWORK = 21014;
        public const long MON_PROCESS_NAME = 21015;
        public const long MON_EVENT_LOG_NBR = 21016;
        public const long MON_TEMPERATURE_CPU = 21017;
        public const long MON_TEMPERATURE_MB = 21018;
        public const long MON_GENERATE_PERFORMANCE = 21019;

        public const long MON_AWSR_TANK_HEIGHT = 21020;
        public const long MON_AWSR_TANK_VOLUME = 21021;
        public const long MON_AWSR_TANK_TEMPERATURE = 21022;
        public const long MON_AWSR_TANK_DENSITY = 21023;
        public const long MON_AWSR_TANK_NOZZLES = 21024;
        public const long MON_AWSR_ATG = 21025;
        public const long MON_AWSR_LON = 21026;
        public const long MON_AWSR_PUMP_CONVERTER = 21027;
        public const long MON_AWSR_IO_CONTROLLER = 21028;
        public const long MON_AWSR_TCP = 21029;
        public const long MON_AWSR_RADIO_FRAMES = 21030;
        public const long MON_AWSR_RADIO_AMPLIFIER = 21031;
        public const long MON_IS_WORKING = 21032;

        public const long MON_CPU_THRESHOLD = 21033;
        public const long MON_CPU_TIME = 21034;
        public const long MON_RAM_THRESHOLD = 21035;
        public const long MON_RAM_THRESHOLD_PERC = 21056;
        public const long MON_RAM_TIME = 21036;
        public const long MON_DISC_THRESHOLD = 21037;
        public const long MON_DISC_THRESHOLD_PERC = 21055;
        public const long MON_DISC_TIME = 21038;
        public const long MON_DISC_DRIVE_LETTERS = 21039;
        public const long MON_MESSAGE = 21040;
        public const long MON_FILE_SAVE = 21041;
        public const long MON_LOOP_TIME = 21042;
        public const long MON_FILE_PATH = 21043;
        public const long MON_DISK_MB = 21044;
        public const long MON_SEND_EMAIL = 21045;

        public const long MON_FP_AGGR_PC = 21046;
        public const long MON_FP_AGGR_SERVICE = 21047;
        public const long MON_FP_AGGR_TANKS = 21048;
        public const long MON_FP_AGGR_SALES = 21049;
        public const long MON_FP_AGGR_TOTAL = 21050;
        public const long MON_FP_AGGR_SALES_IDENTIFIED = 21051;
        public const long MON_FP_AGGR_DELIVERIES_IDENTIFIED = 21052;
        public const long MON_FP_AGGR_SRV_DELIVERIES = 21053;
        public const long MON_FP_AGGR_SRV_INTERVALS = 21054;

        public const long MON_SMTP_PORT = 21080;
        public const long MON_SMTP_ADDRESS = 21081;
        public const long MON_EMAIL_RECIPIENTS = 21082;
        public const long MON_EMAIL_SUBJECT = 21083;

        public const long MON_PROCESS_VISIBLE_NAME = 21084;
        public const long MON_PROCESS_EXEC_FILE_PATH = 21085;
        public const long MON_QUEUE_WATCHER_QUEUE_CURRENT_LEVEL = 21086;
        public const long MON_RAM_CURRENT = 21087;
        public const long MON_CPU_CURRENT_PERC = 21088;

        public const long MON_DIAGNOSTIC_CODE = 21100;
        public const long MON_TRANSMISSION_DRIVER_RECEIVED_PACKETS = 21101;
        public const long MON_TRANSMISSION_DRIVER_SENT_PACKETS = 21102;
        public const long MON_TRANSMISSION_DRIVER_ID = 21103;
        public const long MON_TRANSMISSION_DRIVER_SIGNAL_QUALITY = 21104;

        public const long MON_EMAIL_SENDING_MODE = 21105;
        public const long MON_SMTP_SENDER = 21106;
        public const long MON_SMTP_PASSWORD = 21107;
        public const long MON_SMTP_ENABLE_SSL = 21108;

        public const long MON_MINUTES_TO_START_AFTER_QUARTER = 21109; // ilo�� minut PO najbli�szym kwadransie (0 - 14) m�wi�ca o ktorej zostanie uruchomiony MON.Monitoring

        public const long MON_DOUBLE_DATA_WATCHER_NON_DOUBLED_AGGR_PERC = 21110;
        public const long MON_DOUBLE_DATA_WATCHER_NON_DOUBLED_AGGR = 21111;
        public const long MON_DOUBLE_DATA_WATCHER_NON_DOUBLED_REFUEL_PERC = 21112;
        public const long MON_DOUBLE_DATA_WATCHER_NON_DOUBLED_REFUEL = 21113;

        public const long MON_SALES_DATA_WATCHER_MAN_POS_COMPLETENESS_PERC = 21114;
        public const long MON_SALES_DATA_WATCHER_POS_DIS_MATCH_PERC = 21115;
        public const long MON_SALES_DATA_WATCHER_POS_INT_MAN_MATCH_PERC = 21116;

        public const long MON_GENERATE_PERFORMANCE_TIME = 21117;
        public const long MON_AWSR_RESTART = 21118;
        public const long MON_AWSR_RESTART_COUNT = 21119;

        public const long MON_LOCATION_FPDWH_DA_RECEIVED_COUNT = 21120;
        public const long MON_LOCATION_FPDWH_DT_RECEIVED_COUNT = 21121;
        #endregion
        #region PROFILE params
        public const long PROFILE_INSTALLATION_TYPE = 22001;
        public const long PROFILE_DA_EXPORT_IMMEDIATELY = 22002;
        public const long PROFILE_DA_EXPORT_TRESHOLD_LEVEL = 22003;
        public const long PROFILE_DA_EXPORT_TRESHOLD_VOLUME = 22004;
        public const long PROFILE_DELIVERY_VOLUME_ROUNDING = 22005;

        public const long PROFILE_MEASURE_ANALYSIS_DELAY = 22006;
        public const long PROFILE_WEATHER_UPDATE_PERIOD = 22007;
        public const long PROFILE_REFUEL_MAX_BREAKS_TIME = 22008;
        public const long PROFILE_REFUEL_MIN_SPEED = 22009;
        public const long PROFILE_REFUEL_INFO_TOLERANCE = 22010;
        public const long PROFILE_REFUEL_MIN_VOLUME = 22011;
        public const long PROFILE_OUT_TEMPERATURE_TOLERANCE = 22012;
        public const long PROFILE_REC_AGGREGATION_TYPE = 22013;
        public const long PROFILE_NOZZLE_TOLERANCE = 22014;
        public const long PROFILE_TEMPERATURE_TOLERANCE = 22015;
        public const long PROFILE_REFUEL_AFTER_END_DELAY = 22016;
        public const long PROFILE_TAKE_IDDLE_ON_TANK = 22017;
        public const long PROFILE_TAKE_IDDLE_ON_NOZZLE = 22018;
        public const long PROFILE_RESUME_TOLERANCE = 22019;
        public const long PROFILE_REFUEL_COMMIT_TOLERANCE = 22020;
        public const long PROFILE_FUEL_DISPENCED_DISTRIBUTION_COEFFICIENT = 22021;

        public const long PROFILE_MAX_PROLONGATION_TIME = 22022;
        public const long PROFILE_MIN_OUTCOME_VOLUME = 22023;

        public const long PROFILE_CALIB_THRESHOLD = 22024;
        public const long PROFILE_CALIB_NUM_OF_POINTS = 22025;
        public const long PROFILE_CALIB_MODE = 22026;
        public const long PROFILE_CALIB_ENSEMBLE_NUMBER = 22027;
        public const long PROFILE_CALIB_TRAIN_RATIO = 22028;
        public const long PROFILE_CALIB_VAL_RATIO = 22029;

        public const long PROFILE_ARCHIVE_READOUTS_BACK_ANALYSIS_DAYS = 22030;
        public const long PROFILE_CONSUMPTION_AGGREGATION_TYPE = 22031;

        public const long PROFILE_DECISION_LEAK_TRESHOLD = 22032;
        public const long PROFILE_DECISION_SURPLUS_TRESHOLD = 22033;
        public const long PROFILE_DECISION_MIRROR_TRESHOLD = 22034;
        public const long PROFILE_DECISION_INITIAL_PROBABILITY = 22035;
        public const long PROFILE_DECISION_LCF = 22036;
        public const long PROFILE_DECISION_PERMANENT_SURPLUS_TRESHOLD = 22037;
        public const long PROFILE_DECISION_PERMANENT_LEAK_TRESHOLD = 22038;
        public const long PROFILE_DECISION_TRENDS_INTERVAL = 22039;
        public const long PROFILE_DECISION_ACCEPTABLE_SLOPE_ANGLE = 22040;

        public const long PROFILE_METER_OPERATIONAL_CAPACITY_PERCENT = 22041;

        public const long PROFILE_CONSUMPTION_ANNUAL_AVG_WEIGHT_PERCENT = 22042;
        public const long PROFILE_CONSUMPTION_MONTHLY_AVG_WEIGHT_PERCENT = 22043;
        public const long PROFILE_CONSUMPTION_WEEKLY_AVG_WEIGHT_PERCENT = 22044;
        public const long PROFILE_CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT = 22045;

        public const long PROFILE_HOURS_TO_STOCKOUT_FOR_MUSTGO = 22046;

        public const long PROFILE_CALCULATE_ROUTES_FOR_DAYS = 22047;
        public const long PROFILE_MAX_ROUTE_DURATION = 22048;
        public const long PROFILE_MAX_ROUTE_LENGTH = 22049;
        public const long PROFILE_ROUTE_START_HOUR = 22050;
        public const long PROFILE_ROUTES_DURING_DAY = 22051;
        public const long PROFILE_SIMULATE_FOR_DAYS = 22052;
        public const long PROFILE_ALGORITHM_TURBO_VERSION_ENABLED = 22053;
        public const long PROFILE_MAX_ROUTE_DURATION_ENABLED = 22054;
        public const long PROFILE_MAX_ROUTE_LENGTH_ENABLED = 22055;
        public const long PROFILE_ROUTE_VERIFICATION_DATE_START = 22056;
        public const long PROFILE_ROUTE_VERIFICATION_SHOW_ALL_CALCULATED_ROUTES = 22057;
        public const long PROFILE_ROUTE_VERIFICATION_ONLY_FILTERED_LOCATIONS = 22058;
        public const long PROFILE_ROUTE_VERIFICATION_MODE_ENABLED = 22059;
        public const long PROFILE_ROUTE_1KM_DRIVE_DURATION = 22060;
        public const long PROFILE_ROUTE_CONNECT_TANKER_TO_TANK_DURATION = 22061;
        public const long PROFILE_ROUTE_PUMP_1KG_GAS_DURATION = 22062;
        public const long PROFILE_ROUTE_DURATION = 22063;
        public const long PROFILE_ROUTE_DRIVING_COST = 22064;
        public const long PROFILE_ROUTE_DAYS_TO_STOCK_OUT = 22065;
        public const long PROFILE_ROUTE_STOCK_OUT_COST = 22066;
        public const long PROFILE_ROUTE_SEED = 22067;
        public const long PROFILE_ROUTE_SIMULATED_ANNEALING_TEMPERATURE_MIN = 22068;
        public const long PROFILE_ROUTE_SIMULATED_ANNEALING_TEMPERATURE_MAX = 22069;
        public const long PROFILE_ROUTE_SIMULATED_ANNEALING_TEMPERATURE_DELTA = 22070;
        public const long PROFILE_ROUTE_MAXIMUM_CALCULATION_DURATION = 22071;
        public const long PROFILE_ROUTE_ITERATIONS_COUNT = 22076;

        public const long PROFILE_VIRTUAL_MEASURE_TOLERANCE = 22072;
        public const long PROFILE_LOCATION_AGGREGATION_ENABLED = 22073;
        public const long PROFILE_AGGREGATED_TIME_TOLERANCE = 22074;
        public const long PROFILE_RELIABLE_DELIVERY_VOLUME_PERCENTAGE = 22075;
        public const long PROFILE_ADVANCED_PARAMETERS_ENABLED = 22077;

        public const long PROFILE_OUTLIER_LPS_HISTORY_COUNT = 22078;
        public const long PROFILE_OUTLIER_LPS_ACCEPTANCE_RANGE = 22079;
        public const long PROFILE_OUTLIER_LPS_FORGET_COUNT = 22080;
        public const long PROFILE_OUTLIER_LPS_INITIAL_ANGLE = 22081;
        public const long PROFILE_OUTLIER_LPS_INITIAL_VALUES_COUNT = 22082;
        public const long PROFILE_OUTLIER_REFILL_THRESHOLD = 22083;
        public const long PROFILE_OUTLIER_ANGLE_LOW = 22084;
        public const long PROFILE_OUTLIER_ANGLE_HIGH = 22085;
        public const long PROFILE_OUTLIER_CONVERSION_NAME = 22086;
        public const long PROFILE_OUTLIER_VAPOR_M3_TO_LIQUID_M3_MULTIPLIER = 22087;
        public const long PROFILE_OUTLIER_LIQUID_M3_TO_KG_MULTIPLIER = 22088;
        public const long PROFILE_OUTLIER_SUPPORTED_DATA_TYPE_FOR_GAS_METER_ALGORITHM = 22162;
        public const long PROFILE_OUTLIER_SUPPORTED_DATA_TYPE_FOR_ALEVEL_ALGORITHM = 22163;
        public const long PROFILE_OUTLIER_SUPPORTED_DATA_TYPE_FOR_UNIVERSAL_ALGORITHM = 22164;
        public const long PROFILE_OUTLIER_MAXIMUM_TEMPORARY_FLOW = 22181;
        public const long PROFILE_OUTLIER_MAXIMUM_TOTAL_MEASURES_COUNT = 22182;
        public const long PROFILE_OUTLIER_MAXIMUM_NEW_MEASURES_COUNT = 22183;

        public const long PROFILE_MON_VOLUME_TIMESTAMP = 22089;
        public const long PROFILE_MON_VOLUME_KPI = 22090;
        public const long PROFILE_MON_SALES_TIMESTAMP = 22091;
        public const long PROFILE_MON_SALES_KPI = 22092;
        public const long PROFILE_MON_RECONCILIATION_TIMESTAMP = 22093;

        public const long PROFILE_PORTAL_PRODUCT_ADDRESS = 22094;
        public const long PROFILE_PORTAL_FUNCTION_AVAILIBILITY = 22095;
        public const long PROFILE_PORTAL_METER_TYPE_GROUP = 22096;
        public const long PROFILE_PORTAL_THEME = 22097;
        public const long PROFILE_PORTAL_DEFAULT_LAYOUT_MAIN = 22098;
        public const long PROFILE_PORTAL_DEFAULT_LAYOUT_DETAIL = 22099;
        public const long PROFILE_PORTAL_DEFAULT_LAYOUT_EDIT = 22100;
        public const long PROFILE_PORTAL_AVAILABLE_PRODUCT_CODE = 22101;
        public const long PROFILE_PORTAL_CALIBRATION_TABLE_METER_TYPE_GROUP = 22102;
        public const long PROFILE_PORTAL_LPG_METER_TYPE_GROUP = 22103;
        public const long PROFILE_PORTAL_LPG_LOLO_ALARM_TEXT = 22104;
        public const long PROFILE_PORTAL_LPG_LO_ALARM_TEXT = 22105;
        public const long PROFILE_PORTAL_LPG_HI_ALARM_TEXT = 22106;
        public const long PROFILE_PORTAL_LPG_HIHI_ALARM_TEXT = 22107;
        public const long PROFILE_PORTAL_FUEL_LOLO_ALARM_TEXT = 22108;
        public const long PROFILE_PORTAL_FUEL_LO_ALARM_TEXT = 22109;
        public const long PROFILE_PORTAL_FUEL_HI_ALARM_TEXT = 22110;
        public const long PROFILE_PORTAL_FUEL_HIHI_ALARM_TEXT = 22111;
        public const long PROFILE_PORTAL_LOCATION_READOUT_DATA_TYPE = 22112;

        public const long PROFILE_TANK_LEVEL_AVG_DAYS = 22113;
        public const long PROFILE_REFUEL_HISTORY_LAST_COUNT = 22114;

        public const long PROFILE_DECISION_PROBE_HANG_TRESHOLD = 22115;
        public const long PROFILE_DECISION_GASMETER_PROBLEMS_TIMESPAN = 22116;
        public const long PROFILE_DECISION_DEAD_GASMETER_TRESHOLD = 22117;
        public const long PROFILE_DECISION_PERMANENT_SURPLUS_TIMESPAN = 22118;
        public const long PROFILE_DECISION_PERMANENT_LEAK_TIMESPAN = 22119;
        public const long PROFILE_DECISION_GAS_METER_ACCURACY = 22120;
        public const long PROFILE_DECISION_LEAK_TRESHOLD_PERCENTAGE = 22121;
        public const long PROFILE_DECISION_SURPLUS_TRESHOLD_PERCENTAGE = 22122;
        public const long PROFILE_DECISION_MIRROR_TRESHOLD_PERCENTAGE = 22123;
        public const long PROFILE_DECISION_PERMANENT_SURPLUS_TRESHOLD_PERCENTAGE = 22124;
        public const long PROFILE_DECISION_PERMANENT_LEAK_TRESHOLD_PERCENTAGE = 22125;
        public const long PROFILE_DECISION_PROBE_HANG_TRESHOLD_PERCENTAGE = 22126;
        public const long PROFILE_DECISION_DEAD_GASMETER_TRESHOLD_PERCENTAGE = 22127;

        public const long PROFILE_REFUEL_HISTORY_DATA_SOURCE_TYPE = 22128;

        public const long PROFILE_ROUTE_DRIVE_1_HOUR_COST = 22129;
        public const long PROFILE_ROUTE_DRIVE_1_KM_COST = 22130;

        public const long PROFILE_RECONCILIATION_MEASUREMENT_TIME_TOLERANCE = 22131;

        public const long PROFILE_PORTAL_VALVE_CLOSE_ACTION = 22132;
        public const long PROFILE_PORTAL_VALVE_OPEN_ACTION = 22133;

        public const long PROFILE_TANKS_TO_AGGREGATE = 22134;
        public const long PROFILE_REFUEL_DETECTED_MAX_BREAK_TIME = 22135;
        public const long PROFILE_REFUEL_DIFFERENT_SOURCE_CONNECTION_TIME = 22136;
        public const long PROFILE_DECISION_CONNECTED_REFUEL_THRESHOLD = 22137;
        public const long PROFILE_DECISION_CONNECTED_REFUEL_THRESHOLD_PERCETAGE = 22138;
        public const long PROFILE_DECISION_REFUEL_WAIT_TIME = 22139;
        public const long PROFILE_RECONCILIATION_INTERVAL_TOLLERANCE = 22140;
        public const long PROFILE_FUEL_PUMP_START_DURATION = 22141;

        public const long PROFILE_RATING_THRESHOLD_RED = 22142;
        public const long PROFILE_RATING_THRESHOLD_YELLOW = 22143;

        public const long PROFILE_PORTAL_PRESSURE_LOLO_UP_ALARM_TEXT = 22144;
        public const long PROFILE_PORTAL_PRESSURE_LO_UP_ALARM_TEXT = 22145;
        public const long PROFILE_PORTAL_PRESSURE_HI_UP_ALARM_TEXT = 22146;
        public const long PROFILE_PORTAL_PRESSURE_HIHI_UP_ALARM_TEXT = 22147;

        public const long PROFILE_DECISION_MIRROR_TIMESPAN = 22148;
        public const long PROFILE_DECISION_PROBE_HANG_TIMESPAN = 22149;
        public const long PROFILE_DECISION_DEAD_GASMETER_TIMESPAN = 22150;
        public const long PROFILE_DECISION_PROBE_ACCURACY = 22151;
        public const long PROFILE_TEMPERATURE_TIME_TOLLERANCE = 22152;

        public const long PROFILE_PORTAL_LPG_LEAKAGE_ALARM_TEXT = 22153;
        public const long PROFILE_PORTAL_FUEL_LEAKAGE_ALARM_TEXT = 22154;

        public const long PROFILE_PROBLEM_WARNING_LITERS = 22155;
        public const long PROFILE_PROBLEM_WARNING_PERC = 22156;
        public const long PROFILE_PROBLEM_ERROR_LITERS = 22157;
        public const long PROFILE_PROBLEM_ERROR_PERC = 22158;
        public const long PROFILE_PORTAL_CONFIGURATION_ACTION = 22159;
        public const long PROFILE_DECISION_SIMPLE_ENABLED = 22160;
        public const long PROFILE_DECISION_SIMPLE_MAX_JOIN_TIME = 22161;

        public const long PROFILE_PROBLEM_TO_PROCESS = 22165;
        public const long PROFILE_DFL_NAME = 22166;

        public const long PROFILE_PORTAL_PRESSURE_LOLO_DOWN_ALARM_TEXT = 22167;
        public const long PROFILE_PORTAL_PRESSURE_LO_DOWN_ALARM_TEXT = 22168;
        public const long PROFILE_PORTAL_PRESSURE_HI_DOWN_ALARM_TEXT = 22169;
        public const long PROFILE_PORTAL_PRESSURE_HIHI_DOWN_ALARM_TEXT = 22170;
        public const long PROFILE_PORTAL_PRESSURE_EMERGENCY_STEPREL_ALARM_TEXT = 22171;
        public const long PROFILE_PORTAL_PRESSURE_NVR_UP_ALARM_TEXT = 22172;
        public const long PROFILE_PORTAL_PRESSURE_NVR_DOWN_ALARM_TEXT = 22173;
        public const long PROFILE_PORTAL_PRESSURE_UDI_UP_ALARM_TEXT = 22174;
        public const long PROFILE_PORTAL_PRESSURE_UDI_DOWN_ALARM_TEXT = 22175;

        public const long PROFILE_PORTAL_AVERAGE_LEVEL_DISPALY_ENABLED = 22176;
        public const long PROFILE_DECISION_ADVANCED_ENABLED = 22177;
        public const long PROFILE_DECISION_ADVANCED_ALARM_VALIDITY = 22178;
        public const long PROFILE_PORTAL_PROBLEM_TO_PROCESS = 22179;

        public const long PROFILE_IRC_SIR2_LEAK_THRESHOLD = 22180;
        public const long PROFILE_IRC_SIR2_OUTLIER_THRESHOLD = 22204;

        public const long PROFILE_DECISION_CONSTANT_LEVEL_TRESHOLD_PERCENTAGE = 22184;
        public const long PROFILE_DECISION_CONSTANT_LEVEL_TIMESPAN = 22185;
        public const long PROFILE_DECISION_CONSTANT_LEVEL_DETECTION_ENABLED = 22186;

        public const long PROFILE_DECISION_OVERFILL_TRESHOLD_PERCENTAGE = 22187;

        public const long PROFILE_MAP_MARKER_TEXT_TYPE = 22188;//tekst widoczny na markerze na mapie Google (np. ilo�� meter�w w lokalizacji, maksymalny tona� cysterny)

        public const long PROFILE_IRC_STOCK_DATA_TYPE = 22189;
        public const long PROFILE_IRC_SALES_DATA_TYPE = 22190;
        public const long PROFILE_IRC_STOCK_DATA_SOURCE = 22191;
        public const long PROFILE_IRC_SALES_DATA_SOURCE = 22192;
        public const long PROFILE_IRC_DELIVERY_DATA_SOURCE = 22193;
        public const long PROFILE_IRC_DELIVERY_DATA_NET = 22194;
        public const long PROFILE_REFUEL_MAX_ENDING_TIME = 22195;
        public const long PROFILE_ESBDRIVER_SEND_TO_WEB_SERVICE = 22196;
        public const long PROFILE_METER_DEADSTOCK_CAPACITY_PERCENT = 22197;
        public const long PROFILE_DFC_PREDICTOR_SUPPORTED_BY_METER = 22198;
        public const long PROFILE_DFC_PREDICTOR_USED_BY_METER = 22199;

        public const long PROFILE_PREDICTION_LINEAR_CONSUMPTION_ANNUAL = 22200;
        public const long PROFILE_PREDICTION_LINEAR_CONSUMPTION_MONTHLY = 22201;
        public const long PROFILE_PREDICTION_LINEAR_CONSUMPTION_WEEKLY = 22202;
        public const long PROFILE_PREDICTION_LINEAR_CONSUMPTION_DAILY = 22203;

        public const long PROFILE_PREDICTION_TEMPERATURE_LEARNED_RAW_DATA = 22205;

        public const long PROFILE_CONSUMPTION_START_HOUR = 22206;
        public const long PROFILE_CONSUMPTION_START_DAY_OF_WEEK = 22207;
        public const long PROFILE_CONSUMPTION_START_DAY_OF_MONTH = 22208;
        public const long PROFILE_PREDICTION_MINIMUM_LEARNING_DATE = 22209;

        public const long PROFILE_LOCATION_TYPE = 22210;

        public const long PROFILE_HEATMETER_SECOND_MAIN_WATERMETER_VOLUME_DIFF_ACTIVE = 22211;
        public const long PROFILE_HEATMETER_VOLUME_DIFF_ACTIVE = 22212;
        public const long PROFILE_HEATMETER_FLOW_DIFF_TEMPERATURE_ACTIVE = 22213;
        public const long PROFILE_HEATMETER_POWER_CALCULATED_ACTIVE = 22214;

        public const long PROFILE_PREDICTION_FUEL_LEARNED_RAW_DATA = 22215;
        public const long PROFILE_PREDICTION_ALARM_LEARNED_RAW_DATA = 22216;
        public const long PROFILE_DAILY_CONSUMPTION_BASED_ON_TEMPORARY_FLOW_ACTIVE = 22217;
        public const long PROFILE_DAILY_AVG_CONSUMPTION_CALCULATION_TIME_RANGE = 22218;


        public const long PROFILE_DECISION_STOCK_OUT_RISK_THRESHOLD_PERCENTAGE = 22219;
        public const long PROFILE_DECISION_OVERFILL2_THRESHOLD_PERCENTAGE = 22220;
        public const long PROFILE_DECISION_LO_LEVEL_THRESHOLD_PERCENTAGE = 22221;
        public const long PROFILE_DECISION_STOCK_OUT_RISK_DETECTION_ENABLED = 22222;
        public const long PROFILE_DECISION_OVERFILL_DETECTION_ENABLED = 22223;
        public const long PROFILE_DECISION_OVERFILL2_DETECTION_ENABLED = 22224;
        public const long PROFILE_DECISION_MISSING_ADVANCED_DATA_TIMESPAN = 22225;
        public const long PROFILE_DECISION_MISSING_CRITICAL_DATA_TIMESPAN = 22226;
        public const long PROFILE_DECISION_MISSING_DATA_TIMESPAN = 22227;
        public const long PROFILE_DECISION_MISSING_ADVANCED_CRITICAL_DATA_TIMESPAN = 22228;

        public const long PROFILE_DFC_WINDMS_FUEL_VOLUME_FOR_DELIVERY_HOURS_TOLERANCE = 22229;

        public const long PROFILE_DELIVERY_MUST_GO_TIMESPAN = 22230;
        public const long PROFILE_DELIVERY_CAN_GO_TIMESPAN = 22231;

        public const long PROFILE_DECISION_EARLY_DELIVERY_TIMESPAN = 22232;
        public const long PROFILE_DECISION_DELIVERY_OVERDUE_TIMESPAN = 22233;
        public const long PROFILE_DECISION_NO_DELIVERY_PLANNED_TIMESPAN = 22234;
        public const long PROFILE_DECISION_NO_DELIVERY_DETECTED_TIMESPAN = 22235;

        public const long PROFILE_DFC_PREDICTOR_MINIMUM_LEARNING_DELAY = 22236;
        public const long PROFILE_DFC_PREDICTOR_MINIMUM_DELIVERY_ADVICE_DELAY = 22237;
        public const long PROFILE_DFC_PREDICTOR_MINIMUM_NEXT_LO_LEVELS_DELAY = 22238;

        public const long PROFILE_RECONCILIATION_ALLOW_INACTIVE_COUNTERS = 22239;

        public const long PROFILE_PREDICTION_FUEL_168_LEARNED_RAW_DATA = 22240;
        public const long PROFILE_PREDICTION_FUEL_LINEAR_LEARNED_RAW_DATA = 22241;
        public const long PROFILE_PREDICTION_FUEL_PROBABILISTIC_LEARNED_RAW_DATA = 22242;

        public const long PROFILE_PRESSURE_TIME_TOLLERANCE = 22243;

        public const long PROFILE_PREDICTION_UNITED_MINIMUM_HOURLY_CONSUMPTION = 22244;
        public const long PROFILE_PREDICTION_UNITED_MAXIMUM_HOURLY_CONSUMPTION = 22245;
        public const long PROFILE_PREDICTION_UNITED_USED_PREDICTOR_FOR_PROFILE = 22246;
        public const long PROFILE_PREDICTION_UNITED_USE_DECLARED_FUEL_CONSUMPTION_VALUES = 22247;
        public const long PROFILE_FUEL_LIQUID_TO_GAS_STATE_MULTIPLIER = 22248;
        public const long PROFILE_PREDICTION_NAME = 22249;

        public const long PROFILE_HEATMETER_WATERMETER_DIFF_AND_VOLUME_DIFF_DIFF_ACTIVE = 22250;
        public const long PROFILE_HEATMETER_WATERMETER_DIFF_AND_VOLUME_DIFF_DIFF_AGGREGATION_TIME = 22251;
        public const long PROFILE_HEATMETER_WATERMETER_DIFF_AND_VOLUME_DIFF_DIFF_AGGREGATION_TOLERANCE = 22252;
        public const long PROFILE_HEATMETER_ADDITIONAL_VOLUME_DIFF_ACTIVE = 22253;

        public const long PROFILE_DFC_TABLE_EXPORT_ACTIVE = 22254;
        public const long PROFILE_DFC_TABLE_EXPORT_TABLE_NAME = 22255;
        public const long PROFILE_DFC_TABLE_EXPORT_COLUMN_NAME = 22256;
        public const long PROFILE_DFC_TABLE_EXPORT_COLUMN_ID_DATA_TYPE = 22257;

        public const long PROFILE_DFC_MULTIPLE_METERS_TRANSLATION_MODE = 22258;
        


        #endregion
        #region TASK params
        public const long TASK_CONVERTER_AT_WHICH_MODULE_INSTALLED_ENERGY_INDICATION = 22500;//Przelicznik, w kt�rym zainstalowany zosta� modu� - wskazanie energii
        public const long TASK_CONVERTER_AT_WHICH_MODULE_INSTALLED_CAPACITY_INDICATION = 22501;//Przelicznik, w kt�rym zainstalowany zosta� modu� - wskazanie objetosci
        public const long TASK_CONVERTER_AT_WHICH_MODULE_INSTALLED_ENERGY_INDICATION_UNIT = 22502;//Przelicznik, w kt�rym zainstalowany zosta� modu� - wskazanie energii - jednostka
        public const long TASK_CONVERTER_AT_WHICH_MODULE_INSTALLED_CAPACITY_INDICATION_UNIT = 22503;//Przelicznik, w kt�rym zainstalowany zosta� modu� - wskazanie objetosci - jednostka
        public const long TASK_CF51_TEMPERATURE_SENSOR = 22504;//Parametr montowanego przelicznika CF51 - czujnik temperatury
        public const long TASK_CF51_INSTALLATION_PLACE = 22505;//Parametr montowanego przelicznika CF51 - miejsce instalacji
        public const long TASK_CF51_PULSE = 22506;//Parametr montowanego przelicznika CF51 - impulsowanie
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_TYPE = 22507;//Przelicznik przed wymiana - typ
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_SERIAL_NUMBER = 22508;//Przelicznik przed wymiana - numer seryjny
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_ENERGY_INDICATION = 22509;//Przelicznik przed wymiana - wskazanie energii
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_CAPACITY_INDICATION = 22510;//Przelicznik przed wymiana - wskazanie objetosci
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_ENERGY_INDICATION_UNIT = 22511;//Przelicznik przed wymiana - wskazanie energii - jednostka
        public const long TASK_CONVERTER_BEFORE_INSTALLATION_CAPACITY_INDICATION_UNIT = 22512;//Przelicznik przed wymiana - wskazanie energii - jednostka
        public const long TASK_OKO_SEAL_NUMBER = 22513;//Numer plomby OKO
        public const long TASK_T_MOBILE_SIGNAL_LEVEL = 22514;//T-Mobile poziom sygnalu
        public const long TASK_PLUS_SIGNAL_LEVEL = 22515;//Plus poziom sygnalu
        public const long TASK_ORANGE_SIGNAL_LEVEL = 22516;//Orange poziom sygnalu
        public const long TASK_WATER_METER_AT_WHICH_MODULE_INSTALLED_CAPACITY_INDICATION_M3 = 22517;//Wodomierz przy kt�rym b�dzie zainstalowany modu� - Wskazanie obj�to�ci [m3]
        public const long TAKS_REPLACED_WATER_METER_TYPE = 22518;//Wodomierz kt�ry zosta� wymienionY - typ
        public const long TASK_REPLACED_WATER_METER_SERIAL_NUMBER = 22519;//Wodomierz kt�ry zosta� wymienionY - serial number
        public const long TASK_REPLACEG_WATER_METER_CAPACITY_INDICATION_M3 = 22520;//Wodomierz kt�ry zosta� wymienionY - wskazanie objetosci - m3
        public const long TASK_HEAT_METER_INDEX = 22521;//Numer indexu cieplomierza o id_meter w komorce value
        public const long TASK_WATER_METER_INDEX = 22522;//Numer indexu wodomierza o id_meter w komorce value
        public const long TASK_OKO_INDEX = 22523;//Numer indexu OKO
        public const long TASK_INSTALLATION_PROTOCOL_DATE = 22524;//data protokolu instalacji w lokalizacji
        public const long TASK_INSTALLATION_PROTOCOL_ERROR_INFORMATION = 22525;//opis problemu z instalacja
        public const long TASK_INSTALLATION_PROTOCOL_WATER_METER_SIZE = 22526;//wielkosc wodomierza
        public const long TASK_INSTALLATION_PROTOCOL_INDICATIONS_SUMMARY = 22527;
        public const long TASK_CONVERTER_DEVICE_TYPE = 22528;
        public const long TASK_CONVERTER_DEVICE_SERIAL_NBR = 22529;
        public const long TAKS_CONVERTER_TYPE = 22530;
        public const long TASK_CONVERTER_SERIAL_NBR = 22531;
        public const long TAKS_OKO_SERIAL_NBR = 22532;
        public const long TASK_WATERMETER_DEVICE_TYPE = 22533;
        public const long TASK_WATERMETER_DEVICE_SERIAL_NBR = 22534;
        public const long TAKS_WATERMETER_SERIAL_NBR = 22535;

        public const long TASK_DUE_DATE_DONE_IN_TIME = 22536;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_DISTRIBUTOR_FAULT = 22537;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_CLIENT_FAULT_NO_CONTACT = 22538;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_CLIENT_FAULT_LAST_MINUTE_POSTPONEMENT = 22539;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_CLIENT_FAULT_AFTER_DEADLINE_REQUEST = 22540;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_AIUT_FAULT_FITTER = 22541;
        public const long TASK_DUE_DATE_DONE_AFTER_DEADLINE_AIUT_FAULT_ORGANIZATION = 22542;
        public const long TASK_COMPLAINT_NOT_PUNCTUALITY = 22543;
        public const long TASK_COMPLAINT_INCORRECT_BEHAVIOUR = 22544;
        public const long TASK_COMPLAINT_UNPREPARED_FITTER = 22545;
        public const long TASK_INSTALLATION_PROTOCOL_MBUS = 22546;
        public const long TASK_ARRANGED_FITTER_VISIT_DATE = 22547;
        public const long TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR = 22548;
        public const long TASK_ARRANGED_FITTER_VISIT_END_HOUR = 22549;
        public const long TASK_DUE_DATE_DONE_IN_TIME_CLIENT_INACCESSIBILITY = 22550;

        public const long TASK_PROTOCOL_DEVICE_MOVE = 22551;
        public const long TASK_PROTOCOL_DEVICE_MOVE_DESTINATION = 22552;
        public const long TASK_PROTOCOL_DEVICE_EXCHANGE = 22553;
        public const long TASK_PROTOCOL_AIUT_DEVICE_MOUNT_ON_MAGAZINE = 22554;
        public const long TASK_PROTOCOL_EXTERNAL_DEVICE_MOUNT_ON_MAGAZINE = 22555;
        public const long TASK_PROTOCOL_AIUT_DEVICE_UNMOUNT_FROM_MAGAZINE = 22556;
        public const long TASK_PROTOCOL_EXTERNAL_DEVICE_UNMOUNT_FROM_MAGAZINE = 22557;
        public const long TASK_PROTOCOL_DEVICE_ID_ARTICLE = 22558;
        public const long TASK_PROTOCOL_TIMESTAMP = 22559;
        public const long TASK_PROTOCOL_ID_ACTION = 22560;
        public const long TASK_PROTOCOL_ACTION_TYPE = 22561;
        public const long TASK_PROTOCOL_ACTION_DEVICE = 22562;
        public const long TASK_PROTOCOL_ACTION_STATUS = 22563;

        public const long TASK_OPERATION_TYPE = 22564;//Location = 1, Distributor = 2
        public const long TASK_OPERATION_SUBTYPE = 22565;//Location {GAS, HEAT, WATER, ELECTRIC} - due meter type
        public const long TASK_OPERATION_ID_DESCR = 22566;
        public const long TASK_OPERATION_ASSIGNED = 22567;
        public const long TASK_SUSPENSION_REASON = 22568;
        public const long TASK_MOUNTED_DEVICES_WARRANTY_PROLONGATION_ANALYZED = 22569;
        public const long TASK_MOUNTED_DEVICES_WARRANTY_PROLONGATION_ANALYZED_TIMESTAMP = 22570;
        public const long TASK_EXPECTED_REOPEN_DATE = 22571;
        public const long TASK_DEADLINE_PROLONGATION_DUE_ISSUE_SUSPENSION_LAST_DATE = 22572;

        //Audyt dostawy paliwa
        public const long TASK_ORDER_NUMBER = 22573;
        public const long TASK_TANK_REGISTRATION_NUMBER = 22574;
        public const long TASK_TRAILER_REGISTRATION_NUMBER = 22575;
        public const long TASK_TANK_DRIVER = 22576;
        public const long TASK_CARRIER = 22577;
        public const long TASK_TANK_LOADING_PLACE = 22578;
        public const long TASK_VOLUME_CONTAINTER = 22579;
        public const long TASK_PRODUCT_CODE = 22580;
        public const long TASK_FILE_LINK = 22581;
        public const long TASK_FILE_NAME = 22583;
        public const long TASK_OTHER_CONTAINER = 22584;
        //Audyt dostawy paliwa

        public const long TASK_PROTOCOL_SUBMIT_DATE = 22582;

        public const long TASK_PAYMENT_DETAILS_AMOUNT = 22585;
        public const long TASK_PAYMENT_DETAILS_TYPE = 22586;
        public const long TASK_PAYMENT_DETAILS_CONDITION = 22587;

        public const long TASK_FITTER_ORDERED_TO_PAY = 22588;
        public const long TASK_CLIENT_ORDERED_TO_PAY = 22589;
        public const long TASK_CLIENT_PAYMENT_AMOUNT = 22590;
        public const long TASK_CLIENT_PAYMENT_DATE = 22591;

        public const long TASK_INITIATION_DATE = 22592;
        public const long TASK_RESULTS = 22593;
        public const long TASK_COMPLETION_DATE = 22594;
        public const long TASK_VERIFICATION = 22595;
        public const long TASK_ACTION_DATE = 22596;

        public const long TASK_MAINTENANCE_NOTE = 22597;
        public const long TASK_IS_TRANSFERRED_FROM_OTHER_SERVER = 22598;//task przenisiony z innego serwera

        public const long TASK_PAYMENT_DETAILS_REFERENCE_VALUE = 22599;
        public const long TASK_SALES_ORDER_NUMBER = 22600;
        public const long TASK_PROTOCOL_ACTION_TEXT = 22601;
        public const long TASK_PROTOCOL_ADDED_METER_SERIAL_NBR = 22602;
        public const long TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE = 22603;
        public const long TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES = 22604;
        public const long TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR = 22605;
        public const long TASK_CURRENT_METER_READOUT = 22606;
        public const long TASK_PROTOCOL_METER_SERIAL_NBR = 22607;
        public const long TASK_PROTOCOL_ALEVEL_SERIAL_NBR = 22608;
        public const long TASK_PAYMENT_DETAILS_NOTES = 22609;

        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_NAME = 22610;
        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NAME = 22611;
        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NOTE = 22612;
        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_NAME = 22613;
        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NAME = 22614;
        public const long TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NOTE = 22615;

        public const long TASK_TYPE_RECOGNIZED = 22616;

        public const long TASK_PERFORMANCE_ID_REFERENCE_TYPE = 22617;
        public const long TASK_PERFORMANCE_ID_DATA_TYPE = 22618;
        public const long TASK_PERFORMANCE_START_TIME = 22619;
        public const long TASK_PERFORMANCE_END_TIME = 22620;
        public const long TASK_PERFORMANCE_ID_AGGREGATION_TYPE = 22621;
        public const long TASK_PERFORMANCE_REFERENCE_VALUE = 22622;
        public const long TASK_PERFORMANCE_VALUE = 22623;

        public const long TASK_EXPORT_STEP = 22624;
        public const long TASK_EXPORT_STEP_STATUS = 22625;
        public const long TASK_EXPORT_STEP_NOTE = 22626;
        public const long TASK_EXPORT_STATUS = 22627;
        public const long TASK_EXPORT_NOTE = 22628;

        public const long TASK_AUTO_GENERATED = 22629;
        public const long TASK_METER_ID = 22630;
        public const long TASK_DEVICE_SERIAL_NBR = 22631;

        public const long TASK_PROCEED_PROBLEM_TYPE = 22632;
        public const long TASK_PROCEED_PROBLEM_NOTE = 22633;

        public const long TASK_ATTACHMENT_TYPE = 22634;

        public const long TASK_EXPORT_ARCH_LOCATION_ID_IMR_SERVER = 22635;
        public const long TASK_EXPORT_ARCH_LOCATION_SOURCE_SERVER_ID = 22636;
        public const long TASK_EXPORT_ARCH_METER_ID_IMR_SERVER = 22637;
        public const long TASK_EXPORT_ARCH_METER_SOURCE_SERVER_ID = 22638;

        public const long TASK_PLAY_SIGNAL_LEVEL = 22639;//Play poziom sygnalu
        public const long TASK_INSTALLATION_PROTOCOL_WATERMETER_INDICATIONS_SUMMARY = 22640;

        public const long TASK_EXTERNAL_ID = 22641;

        public const long TASK_PROTOCOL_OLD_METER_SERIAL_NBR = 22642;
        public const long TASK_PROTOCOL_NEW_METER_SERIAL_NBR = 22643;
        public const long TASK_PROTOCOL_ID_OPERATOR = 22644;
        public const long TASK_EXPORT_ID_ACTION = 22645;
        public const long TASK_EXPORT_SUCCESS = 22646;
        public const long TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT = 22647;

        public const long TASK_INSTALLATION_PROTOCOL_WATERMETER_NOMINAL_FLOW = 22648;
        public const long TASK_INSTALLATION_PROTOCOL_WATERMETER_PULSE_WEIGHT = 22649;

        public const long TASK_PROTOCOL_BATTERY_CHANGE_DEVICE_SERIAL_NBR = 22650;
        public const long TASK_PROTOCOL_BATTERY_CHANGE_OLD_SERIAL_NBR = 22651;
        public const long TASK_PROTOCOL_BATTERY_CHANGE_NEW_SERIAL_NBR = 22652;
        public const long TASK_PROTOCOL_BATTERY_CHANGE_OLD_BATTERY_EFFICIENT = 22653;

        public const long TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR = 22654;
        public const long TASK_PROTOCOL_VALVE_OPERATION_TYPE = 22655;//Enums.ValveState
        public const long TASK_PROTOCOL_VALVE_OPERATION_RESULT = 22656;
        public const long TASK_PROTOCOL_VALVE_OPERATION_STATUS = 22657;//Enums.ValveState

        public const long TASK_PROTOCOL_REPROGRAM_DEVICE_SERIAL_NBR = 22658;
        public const long TASK_PROTOCOL_REPROGRAM_FILE_NAME = 22659;
        public const long TASK_PROTOCOL_REPROGRAM_OLD_VERSION_NUMBER = 22660;
        public const long TASK_PROTOCOL_REPROGRAM_NEW_VERSION_NUMBER = 22661;
        public const long TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME = 22662;

        public const long TASK_PROTOCOL_SIM_CARD_TEST_DEVICE_SERIAL_NBR = 22663;
        public const long TASK_PROTOCOL_SIM_CARD_TEST_PHONE_NBR = 22664;
        public const long TASK_PROTOCOL_SIM_CARD_TEST_ERROR_TYPE = 22665;

        public const long TASK_PROTOCOL_CRITICAL_ERROR_DEVICE_SERIAL_NBR = 22666;
        public const long TASK_PROTOCOL_CRITICAL_ERROR_TYPE = 22667;
        public const long TASK_PROTOCOL_CRITICAL_ERROR_VALUE = 22668;

        public const long TASK_PROTOCOL_ARCHIVE_READ_DEVICE_SERIAL_NBR = 22669;
        public const long TASK_PROTOCOL_ARCHIVE_READ_FROM = 22670;
        public const long TASK_PROTOCOL_ARCHIVE_READ_TO = 22671;
        public const long TASK_PROTOCOL_ARCHIVE_READ_TYPE = 22672;
        public const long TASK_PROTOCOL_ARCHIVE_READ_FRAME_COUNT = 22673;

        public const long TASK_PROTOCOL_EVENT_LOG_DEVICE_SERIAL_NBR = 22674;
        public const long TASK_PROTOCOL_EVENT_LOG_REFERENCE_DATE = 22675;
        public const long TASK_PROTOCOL_EVENT_LOG_ENTRY_COUNT = 22676;

        public const long TASK_PROTOCOL_NEW_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR = 22677;
        public const long TASK_PROTOCOL_OLD_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR = 22678;

        public const long TASK_PROTOCOL_CONFIGURATION_RESTORE_DEVICE_SERIAL_NBR = 22679;
        public const long TASK_PROTOCOL_RESTORED_CONFIGURATION = 22680;

        public const long TASK_PROTOCOL_INSTALLED_GASMETER_DEVICE_SERIAL_NBR = 22681;
        public const long TASK_PROTOCOL_METER_TYPE = 22682;

        public const long TASK_PROTOCOL_OPERATION_TYPE = 22683;

        public const long TASK_PROTOCOL_DEINSTALLED_GASMETER_DEVICE_SERIAL_NBR = 22684;
        public const long TASK_PROTOCOL_DEINSTALLED_MAGAZINE_CID = 22685;

        public const long TASK_PROTOCOL_CONFIGURATION_DOWNLOAD_DEVICE_SERIAL_NBR = 22686;

        public const long TASK_MX_JPNUM = 22687;
        public const long TASK_MX_PMNUM = 22688;
        public const long TASK_ID_OPERATOR_PERFORMER = 22689;
        public const long TASK_MAXIMO_ID = 22690;

        public const long TASK_PROTOCOL_CLUSTER_TEST_OKO = 22691;
        public const long TASK_PROTOCOL_CLUSTER_TEST_APULSE = 22692;
        public const long TASK_PROTOCOL_CLUSTER_TEST_LATITUDE = 22693;
        public const long TASK_PROTOCOL_CLUSTER_TEST_LONGITUDE = 22694;
        public const long TASK_PROTOCOL_CLUSTER_TEST_FILE_NAME = 22695;
        public const long TASK_PROTOCOL_CLUSTER_TEST_APULSE_STATUS = 22696;
        public const long TASK_PROTOCOL_CLUSTER_TEST_APULSE_EFFICIENCY = 22697;
        public const long TASK_PROTOCOL_CLUSTER_TEST_APULSE_FRAME_COUNT = 22698;

        public const long TASK_SOURCE_DESCRIPTION = 22699;

        public const long TASK_PROTOCOL_INSTALLATION_WITH_SYNCHRONIZATION_SERIAL_NBR = 22700;

        public const long TASK_REALIZATION_START = 22701;
        public const long TASK_REALIZATION_END = 22702;
        public const long TASK_LONG_DESCRIPTION = 22703;
        public const long TASK_ARRANGED_FITTER_VISIT_DATE_END = 22704;
        public const long TASK_SOURCE_SYSTEM = 22705;

        public const long TASK_PROTOCOL_DAILY_WORK_SYSTEM = 22706;
        public const long TASK_PROTOCOL_DAILY_WORK_TYPE = 22707;
        public const long TASK_PROTOCOL_DAILY_WORK_SUMMARY = 22708;
        public const long TASK_PROTOCOL_DAILY_WORK_DESCRIPTION = 22709;

        public const long TASK_PROTOCOL_PIN_VALUE_SERIAL_NBR = 22710;
        public const long TASK_PROTOCOL_PIN_NUMBER = 22711;
        public const long TASK_PROTOCOL_PIN_TYPE = 22712;
        public const long TASK_PROTOCOL_PIN_VALUE = 22713;

        public const long TASK_PROTOCOL_DIAGNOSTIC_SERIAL_NBR = 22714;

        public const long TASK_ROUTE_ORDER_NBR = 22715;
        public const long TASK_PROTOCOL_INSTALLED_GASMETER_RADIO_QUALITY_VERIFICATION_DEVICE_SERIAL_NBR = 22716;
        public const long TASK_PROTOCOL_INSTALLED_METER_ADAPTER_SERIAL_NBR = 22717;

        public const long TASK_PROTOCOL_DEVICE_VALID_IMPULSE_COUNT_TEST_SERIAL_NBR = 22718;
        public const long TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_PRIOR_TEST = 22719;
        public const long TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_AFTER_TEST = 22720;
        public const long TASK_PROTOCOL_DEVICE_TEST_DURATION = 22721;
        public const long TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR = 22722;
        public const long TASK_PROTOCOL_HUB_INSTALLATION_DEVICE_SERIAL_NBR = 22723;
        public const long TASK_PROTOCOL_INSTALLATION_FORM_TYPE = 22724;
        public const long TASK_PROTOCOL_WALKED_BY_READOUT_DEVICE_SERIAL_NBR = 22725;
        public const long TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE = 22726;

        public const long TASK_PROTOCOL_GAS_METER_USAGE_VALUE = 22727;
        public const long TASK_PROTOCOL_DEVICE_COUNTER_LITER_MAX_VALUE = 22728;
        public const long TASK_PROTOCOL_DEVICE_COUNTER_COEFFICIENT = 22729;

        public const long TASK_INSTALLATION_PROTOCOL_WALKEDBY_MEASUREMENT_VALUE = 22730;

        #endregion
        #region DEPOSITORY_ELEMENT params
        public const long DEPOSITORY_ELEMENT_PACKAGE_ITEM_ERROR_MSG = 23001;//informacja dotyczaca rodzaju popelnionego bledu dla wprowadzonego do paczki urzadzenia
        public const long DEPOSITORY_ELEMENT_MAGAZINE = 23002;//informacja z jakiego magazynu jest pobierany sprzet
        public const long DEPOSITORY_ELEMENT_MAGAZINE_REPORT_GENERATED = 23003;//raport magazynowy zosta� ju� wygenerowany
        public const long DEPOSITORY_ELEMENT_OTHER_DEVICE_ID_DISTRIBUTOR = 23004;//id dystrybutora urz�dzenia nie AIUT'owego
        public const long DEPOSITORY_ELEMENT_DEVICE_ID_DISTRIBUTOR = 23005;//id dystrybutora urz�dzenia dla biezacej sytuacji
        public const long DEPOSITORY_ELEMENT_DEVICE_EXCHANGED_ON_WARRANTY = 23006;

        public const long DEPOSITORY_ELEMENT_TASK_AMS_SERVICE_REPORT_INVOICED = 23007;
        public const long DEPOSITORY_ELEMENT_ADDITIONAL_NOTE = 23008;
        public const long DEPOSITORY_ELEMENT_PERFORMED_BY = 23009;

        public const long DEPOSITORY_ELEMENT_DEVICE_SETTLED = 23010;
        public const long DEPOSITORY_ELEMENT_PACKAGE_ITEM_REMOVED = 23011;
        public const long DEPOSITORY_ELEMENT_PACKAGE_FITTER_ID_OPERATOR_ACCEPTED = 23012;//operator akceptujacy paczk� w zamian za serwisanta
        public const long DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_LATCH = 23013;//zatrzask id dystrybutora urz�dzenia przy zatwierdzaniu zadania
        public const long DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_OWNER_LATCH = 23014;//zatrzask id dystrybutora w�a�ciciela urz�dzenia przy zatwierdzaniu zadania

        public const long DEPOSITORY_ELEMENT_MODIFY_ID_OPERATOR = 23015;
        public const long DEPOSITORY_ELEMENT_MODIFY_ID_MODULE = 23016;
        public const long DEPOSITORY_ELEMENT_MODIFY_TIME_STAMP = 23017;

        public const long DEPOSITORY_ELEMENT_DEVICE_LATIDUDE = 23018;
        public const long DEPOSITORY_ELEMENT_DEVICE_LONGITUDE = 23019;

        #endregion
        #region CONTRACT params
        public const long CONTRACT_ATTACHMENT = 23501;
        public const long CONTRACT_FILE = 23502;
        public const long CONTRACT_NEW_CONTRACT_ID = 23503;//id umowy stworzonej jako kontynuacja poprzedniej
        #endregion
        #region SERVICE_PACKAGE params
        public const long SERVICE_PACKAGE_COST = 24001;
        public const long SERVICE_PACKAGE_COST_UNUSED = 24002;
        public const long SERVICE_PACKAGE_COST_START_TIMESTAMP = 24003;
        public const long SERVICE_PACKAGE_COST_END_TIMESTAMP = 24004;
        public const long SERVICE_PACKAGE_COST_ID_OPERATOR_ADD = 24005;
        public const long SERVICE_PACKAGE_COST_ID_OPERATOR_REMOVE = 24006;
        public const long SERVICE_PACKAGE_COST_TYPE = 24007;
        #endregion
        #region SERVICE_LIST params
        public const long SERVICE_LIST_DEVICE_ATTACHMENT = 24501;
        public const long SERVICE_LIST_MAGAZINE = 24502;
        public const long SERVICE_LIST_DEVICE_METER_SERIAL_NBR = 24503;
        public const long SERVICE_LIST_DEVICE_REPLACEMENT_SERIAL_NBR = 24504;
        public const long SERVICE_LIST_DEVICE_REPLACEMENT_METER_SERIAL_NBR = 24505;
        public const long SERVICE_LIST_DEVICE_OBJECT_REPLACEMENT_SERIAL_NBR = 24506;
        public const long SERVICE_LIST_DEVICE_OBJECT_REPLACEMENT_ID_TASK = 24507;
        public const long SERVICE_LIST_DEVICE_OBJECT_REPLACEMENT_ID_SHIPPING_LIST = 24508;
        public const long SERVICE_LIST_DEVICE_SERVICE_TYPE = 24509;//1 - Service, 2 - Refurbishment
        public const long SERVICE_LIST_DEVICE_FAULT_CODE = 24510;
        public const long SERVICE_LIST_DEVICE_FAULT_CODE_START_DATE = 24511;
        public const long SERVICE_LIST_DEVICE_FAULT_CODE_END_DATE = 24512;
        public const long SERVICE_LIST_DEVICE_FAULT_CODE_OPERATOR_ID = 24513;
        public const long SERVICE_LIST_DEVICE_IS_REFURBISHMENT = 24514;
        public const long SERVICE_LIST_DEVICE_IS_DAMAGE_CAUSED_BY_CLIENT = 24515;
        public const long SERVICE_LIST_DEVICE_DIAGNOSTIC_PHOTO = 24516;
        public const long SERVICE_LIST_DEVICE_DIAGNOSTIC_PHOTO_DIAGNOSTIC_ACTION = 24517;
        public const long SERVICE_LIST_DEVICE_DIAGNOSTIC_PHOTO_DIAGNOSTIC_ACTION_RESULT = 24518;
        public const long SERVICE_LIST_DEVICE_MAJOR_FAULT_CODE = 24519;
        public const long SERVICE_LIST_DEVICE_MAJOR_FAULT_CODE_START_DATE = 24520;
        public const long SERVICE_LIST_DEVICE_MAJOR_FAULT_CODE_END_DATE = 24521;
        public const long SERVICE_LIST_DEVICE_MAJOR_FAULT_CODE_OPERATOR_ID = 24522;
        public const long ESTIMATE_BATTERY_REMAIN_VOLUME = 24523;
        public const long MEASURED_DEVICE_BATTERY_MODEM_CURRENT = 24524;
        public const long MEASURED_DEVICE_BATTERY_SLEEP_CURRENT = 24525;
        public const long MEASURED_DEVICE_BATTERY_RADIO_CURRENT = 24526;
        public const long MEASURED_DEVICE_WMBUS_AVERAGE_CURRENT = 24527;
        public const long LOCATION_TIMER_DURATION = 24528;
        public const long ESTIMATE_MODEM_TIMER_DURATION = 24529;
        public const long ESTIMATE_RADIO_TIMER_DURATION = 24530;
        public const long DB_SMS_RECEIVED_COUNT = 24531;
        public const long DB_SMS_SENT_COUNT = 24532;
        public const long BATTERY_VOLUME_AFTER_DISCHARGE = 24533;
        public const long DEVICE_MODULE_TEST_ERROR = 24534;
        public const long SERVICE_LIST_DEVICE_FORWARDING_OPERATOR_ID = 24535;
        public const long SERVICE_LIST_DEVICE_FORWARD_DATE = 24536;
        public const long SERVICE_LIST_DEVICE_RECIPIENT_OPERATOR_ID = 24537;
        public const long SERVICE_LIST_DEVICE_SERVICE_RECIPIENT_OPERATOR_ID = 24538;
        public const long SERVICE_LIST_DEVICE_RECEPTION_DATE = 24539;
        public const long SERVICE_LIST_DEVICE_BATTERY_PACK_SERIAL_NBR = 24540;
        public const long ESTIMATE_ENERGY_CONSUMPED = 24541;
        public const long ESTIMATE_ENERGY_CONSUMPED_FOR_SIM_CARD_LOCATION_OPERATOR = 24542;
        public const long SIM_CARD_LOCATION_OPERATOR = 24543;
        public const long SIM_CARD_CURRENT_TEST_OPERATOR = 24544;
        public const long BATTERY_CAPACITY_COMPATIBILITY_RESULT = 24545;


        #endregion

        #region DEVICE_ORDER_NUMBER params
        public const long DEVICE_ORDER_NUMBER_ATTACHMENT = 25001;
        public const long DEVICE_ORDER_NUMBER_FILE_TYPE = 25002;
        public const long DEVICE_ORDER_NUMBER_FILE_MD5 = 25003;
        #endregion
        #region MESSAGE params
        public const long MESSAGE_ATTACHMENT = 25501;
        public const long MESSAGE_BODY_RTF = 25502;
        public const long MESSAGE_ATTACHMENT_MD5 = 25503;
        #endregion
        #region IMR_SERVER params
        public const long IMR_SERVER_USER_PORTAL_WEBSERVICE_ADDRESS = 26001;
        public const long IMR_SERVER_DW_SERVER_ID = 26002;
        public const long IMR_SERVER_SMART_GAS_METERING_INTERFACE_WEBSERVICE_ADDRESS = 26003;
        public const long IMR_SERVER_AIUT_HOSTING = 26004;

        public const long IMR_SERVER_SYNCHRONIZED_DATA_ID_DATA_TYPE = 26005;//id_data_type z tabeli data synchronizowany przez aplikacje Collector
        public const long IMR_SERVER_SYNCHRONIZED_TABLE = 26006;//tabela synchronizowana przez Collector
        public const long IMR_SERVER_TABLE_SYNCHRONIZATION_TYPE = 26007;//typ synchronizacji: 1 - Ca�o��, 2 - Zmiany, 3 - Wskazane elementy
        public const long IMR_SERVER_SYNCHRONIZATION_START = 26008;
        public const long IMR_SERVER_SYNCHRONIZATION_PERIOD_MINUTES = 26009;
        public const long IMR_SERVER_SYNCHRONIZABLE = 26010;
        public const long IMR_SERVER_SYNCHRONIZATION_DIRECTION = 26011;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_TEMPORAL_ID_DATA_TYPE = 26012;//id_data_type z tabeli data_temporal synchronizowany przez aplikacje Collector

        public const long IMR_SERVER_PERFORMANCE_VALUE = 26013;
        public const long IMR_SERVER_IS_LOCALHOST = 26014;
        public const long IMR_SERVER_CORE_SERVER_ID = 26015;
        public const long IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS = 26016;

        public const long IMR_SERVER_SYNCHRONIZATION_DISTRIBUTOR = 26017;

        public const long IMR_SERVER_MSMQ_HOST = 26018;
        public const long IMR_SERVER_MSMQ_SEND_ACTION = 26019;

        public const long IMR_SERVER_IS_CORE_IMRSC = 26020;
        public const long IMR_SERVER_SYNCHRONIZED_TASK_DATA_ID_DATA_TYPE = 26021;
        public const long IMR_SERVER_SYNCHRONIZED_DEVICE_DATA_ID_DATA_TYPE = 26022;
        public const long IMR_SERVER_TEST_SERVER_ID = 26023;
        public const long IMR_SERVER_PHONE = 26024;
        public const long IMR_SERVER_SYNCHRONIZATION_CUSTOM_ARGS = 26025;
        public const long IMR_SERVER_ESB_DRIVER_WEBSERVICE_ADDRESS = 26026;
        public const long IMR_SERVER_USE_DATA_TRANSFER = 26027;
        public const long IMR_SERVER_SUPPORTED_EXCEL_EXTENSION = 26028;
        public const long IMR_SERVER_REPORT_GENERATOR = 26029;

        public const long IMR_SERVER_DBCOLLECTOR_WEBSERVICE_BINDING = 26030;

        public const long IMR_SERVER_ISSUES_TASKS_ENABLED = 26031;

        public const long IMR_SERVER_SYNCHRONIZED_DISTRIBUTOR_DATA_ID_DATA_TYPE = 26032;
        public const long IMR_SERVER_SYNCHRONIZED_PROFILE_DATA_ID_DATA_TYPE = 26033;
        public const long IMR_SERVER_SYNCHRONIZED_OPERATOR_DATA_ID_DATA_TYPE = 26034;

        public const long IMR_SERVER_LAST_ANALYZED_ID_DATA_ARCH = 26035;
        public const long IMR_SERVER_SYNCHRONIZATION_MAPPING_MODE = 26036;
        public const long IMR_SERVER_LAST_ANALYZED_ID_REFUEL = 26037;
        public const long IMR_SERVER_SYNCHRONIZED_REFUEL_DATA_ID_DATA_TYPE = 26038;

        public const long IMR_SERVER_SYNCHRONIZED_TASK_ID_TASK_STATUS = 26039;
        public const long IMR_SERVER_SINGLE_SYNCHRONIZATION_ADDITIONAL_ARGS = 26040;

        public const long IMR_SERVER_SYNCHRONIZED_DATA_ARCH_ID_DATA_TYPE = 26041;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_ARCH_DEVICE_ID_DATA_TYPE = 26042;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_ARCH_METER_ID_DATA_TYPE = 26043;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_ARCH_LOCATION_ID_DATA_TYPE = 26044;

        public const long IMR_SERVER_SYNCHRONIZATION_DATABASE_TYPE = 26045;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_DW_ID_DATA_TYPE = 26046;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_DW_DEVICE_ID_DATA_TYPE = 26047;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_DW_METER_ID_DATA_TYPE = 26048;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_DW_LOCATION_ID_DATA_TYPE = 26049;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_METER_ID_DATA_TYPE = 26050;
        public const long IMR_SERVER_SYNCHRONIZED_DATA_LOCATION_ID_DATA_TYPE = 26051;

        public const long IMR_SERVER_SYNCHRONIZATION_IGNORE_DISTRIBUTOR = 26052;
        public const long IMR_SERVER_SYNCHRONIZATION_FORCE_OPERATOR_PASSWORD_SAVE = 26053;
        public const long IMR_SERVER_LOCAL_LAST_ANALYZED_ID_DATA_ARCH = 26054;
        public const long IMR_SERVER_SSRS_ADDRESS = 26055;


        #endregion
        #region DIAGNOSTIC_ACTION params
        public const long DIAGNOSTIC_ACTION_DEVICE_ORDER_NUMBER_ASSIGMENT_LEVEL = 26501;//akcja diagnostyczna przypisana do numeru zam�wieniowego po 0 - po nazwie, 1 - nazwa + pierwsza kwadra, 2 - nazwa + druga kwadra
        public const long DIAGNOSTIC_ACTION_ADDITIONAL_DEVICE_ORDER_NUMBER = 26502;
        public const long DIAGNOSTIC_ACTION_USED_IN_DIAGNOSTIC_TEST = 26503;
        public const long DIAGNOSTIC_ACTION_INVISIBLE_IN_SERVICE_REPORT = 26504;
        public const long DIAGNOSTIC_ACTION_RESULT_EDITABLE_AFTER_SERVICE = 26505;
        public const long DIAGNOSTIC_ACTION_IS_REFURBISHMENT = 26506;
        #endregion
        #region SERVICE_SUMMARY params
        public const long SERVICE_SUMMARY_ID_SERVICE_PACKAGE_DATA_COST_BASE = 27001;
        public const long SERVICE_SUMMARY_DEVICE_SERIAL_NBR = 27002;
        #endregion
        #region SERVICE params
        public const long SERVICE_DEVICE_ORDER_NUMBER_ASSIGMENT_LEVEL = 27501;//serwis przypisany do numeru zam�wieniowego po 0 - po nazwie, 1 - nazwa + pierwsza kwadra, 2 - nazwa + druga kwadra
        public const long SERVICE_ADDITIONAL_DEVICE_ORDER_NUMBER = 27502;
        #endregion
        #region SIM_CARD params
        public const long SIM_CARD_TARIFF = 28001;
        public const long SIM_CARD_HLR = 28002;
        public const long SIM_CARD_PIN2 = 28003;
        public const long SIM_CARD_PUK2 = 28004;
        public const long SIM_CARD_APN = 28005;
        public const long SIM_CARD_DUPLICATE_PRODUCTION_DATE = 28006;
        public const long SIM_CARD_CONTRACT_CONCLUDE_DATE = 28007;
        public const long SIM_CARD_TOPIC = 28008;
        public const long SIM_CARD_COLLECTION_DATE = 28009;
        public const long SIM_CARD_COLLECTING_PERSON = 28010;
        public const long SIM_CARD_UE_ACCOUNT = 28011;
        public const long SIM_CARD_PL_ACCOUNT = 28012;
        public const long SIM_CARD_GPRS = 28013;
        public const long SIM_CARD_GPRS_ON_DATE = 28014;
        public const long SIM_CARD_GPRS_OFF_DATE = 28015;
        public const long SIM_CARD_ADDITIONAL_INFO = 28016;
        public const long SIM_CARD_OFF_DATE = 28017;
        public const long SIM_CARD_CONTRACT_NBR = 28018;
        public const long SIM_CARD_CONTRACT_EXPIRATION_DATE = 28019;
        public const long SIM_CARD_OPERATOR_TARIFF = 28020;
        public const long SIM_CARD_ID_DISTRIBUTOR_OWNER = 28021;
        public const long SIM_CARD_IS_ACTIVE = 28022;
        public const long SIM_CARD_DUPLICATE_ORDER_DATE = 28023;
        public const long SIM_CARD_DUPLICATE_REQUEST_DATE = 28024;
        public const long SIM_CARD_DUPLICATE_PRODUCTION_OPERATOR_ID = 28025;
        public const long SIM_CARD_DUPLICATE_ORDER_OPERATOR_ID = 28026;
        public const long SIM_CARD_DUPLICATE_REQUEST_OPERATOR_ID = 28027;
        public const long SIM_CARD_CONTRACT_TERMINATION_DATE = 28028;

        public const long HELPER_ID_SIM_CARD = 28029;
        public const long HELPER_PIN = 28030;
        public const long HELPER_PUK = 28031;
        public const long HELPER_MOBILE_NETWORK_CODE = 28032;
        public const long HELPER_IP = 28033;
        public const long HELPER_APN_LOGIN = 28034;
        public const long HELPER_APN_PASSWORD = 28035;
        public const long HELPER_SIM_CARD_SERIAL_NBR = 28036;
        public const long HELPER_ID_DEVICE_CONNECTION = 28037;
        #endregion
        #region PACKAGE params
        public const long PACKAGE_ISU_REALIZATION_DATE = 28501;
        public const long PACKAGE_PRODUCTION_REALIZATION_DATE = 28502;
        public const long PACKAGE_WAYBILL = 28503;
        public const long PACKAGE_OPERATOR_ACCEPTING_ORDER = 28504;
        public const long PACKAGE_OPERATOR_COMPLETING_ORDER = 28505;
        public const long PACKAGE_RECEIPT_CONFIRMATION_PROVIDED = 28506;
        public const long PACKAGE_RECEIPT_CONFIRMATION = 28507;
        public const long PACKAGE_FITTER_ID_OPERATOR_ACCEPTED = 28508;//operator akceptujacy paczk� w zamian za serwisanta
        #endregion
        #region SHIPPING_LIST params
        public const long SHIPPING_LIST_DEVICE_OWNERSHIP = 29001;
        public const long SHIPPING_LIST_DEVICE_OWNERSHIP_RENT_ID_DISTRIBUTOR = 29002;
        public const long SHIPPING_LIST_DEVICE_OWNERSHIP_SHARE_ID_DISTRIBUTOR = 29003;
        public const long SHIPPING_LIST_CLOSING_OPERATOR_ID = 29004;
        public const long SHIPPING_LIST_CLOSED_SUCCESSFULLY = 29005;
        public const long SHIPPING_LIST_ID_ARTICLE = 29006;
        public const long SHIPPING_LIST_ARTICLE_COUNT = 29007;
        public const long SHIPPING_ALLOWED_CONFIGS = 29008;
        public const long SHIPPING_LIST_PURCHASE_ORDER = 29009;

        #endregion
        #region ISSUE params
        //29501 - 30000
        public const long ISSUE_METER_ID = 29501;
        public const long ISSUE_DEVICE_SERIAL_NBR = 29502;
        public const long ISSUE_SUGGESTION = 29503;
        public const long ISSUE_INITIATION_DATE = 29504;
        public const long ISSUE_RESULTS = 29505;
        public const long ISSUE_COMPLETION_DATE = 29506;
        public const long ISSUE_VERIFICATION = 29507;
        public const long ISSUE_ACTION_DATE = 29508;
        public const long ISSUE_IS_TRANSFERRED_FROM_OTHER_SERVER = 29509;//issue przenisione z innego serwera
        public const long ISSUE_SERVER_ID = 29510;
        public const long ISSUE_REPORT_ID = 29511;

        public const long ISSUE_PERFORMANCE_ID_REFERENCE_TYPE = 29512;
        public const long ISSUE_PERFORMANCE_ID_DATA_TYPE = 29513;
        public const long ISSUE_PERFORMANCE_START_TIME = 29514;
        public const long ISSUE_PERFORMANCE_END_TIME = 29515;
        public const long ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE = 29516;
        public const long ISSUE_PERFORMANCE_REFERENCE_VALUE = 29517;
        public const long ISSUE_PERFORMANCE_VALUE = 29518;

        public const long ISSUE_AUTO_GENERATED = 29519;

        public const long ISSUE_PLIP_METER = 29520;
        public const long ISSUE_PLIP_PRODUCT = 29521;
        public const long ISSUE_PLIP_DATE = 29522;
        public const long ISSUE_PLIP_TYPE = 29523;
        public const long ISSUE_PLIP_PRIORITY = 29524;

        public const long ISSUE_ID_ISSUE_GROUP = 29525;
        public const long ISSUE_EVENT_TIME = 29526;

        public const long ISSUE_TRANSMISSION_DRIVER_ID = 29527;

        public const long ISSUE_PERFORMANCE_DESCRIPTION = 29528;
        public const long ISSUE_PERFORMANCE_INDEX = 29529;

        #endregion
        #region PERFORMANCE params
        //30001 - 30500
        public const long PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_PC_AGGR = 30001;
        public const long PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_SERVICE_AGGR = 30002;
        public const long PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_TANKS_AGGR = 30003;
        public const long PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_SALES_AGGR = 30004;
        public const long PERFORMANCE_IMR_SERVER_WEBSERVICE = 30005;
        public const long PERFORMANCE_IMR_SERVER_UTD_WEBSERVICE = 30006;
        public const long PERFORMANCE_IMR_SERVER_COMMUNICATION = 30007;
        public const long PERFORMANCE_IMR_SERVER_PACKET_TRASH_STATE = 30008;
        public const long PERFORMANCE_REPORT_DATA_PREPARED = 30009;
        public const long PERFORMANCE_REPORT_EMAIL_SENT = 30010;
        public const long PERFORMANCE_REPORT_FTP_SENT = 30011;
        public const long PERFORMANCE_IMR_SERVER_WCF_DBCOLLECTOR_WEBSERVICE = 30012;
        public const long PERFORMANCE_REPORT_GENERATION = 30013;
        public const long PERFORMANCE_DEVICE_SCHEDULED_MEASUREMENT_READOUT = 30014;
        public const long PERFORMANCE_DISTRIBUTOR_DEVICE = 30015;
        public const long PERFORMANCE_IMR_SERVER_WCF_TMUSERPORTAL_WEBSERVICE = 30016;
        public const long PERFORMANCE_DEVICE_READOUT_ENTRY_TIME = 30017;//zgodno�� metki czasowej pomiaru z czasem pojawienia si� w systemie
        public const long PERFORMANCE_TRANSMISSION_DRIVER_HEALTHY = 30018;
        public const long PERFORMANCE_IMR_SERVER_ESB_DRIVER_WEBSERVICE = 30019;
        public const long PERFORMANCE_DEVICE_ESB_DRIVER_DATA_FLOW = 30020;
        public const long PERFORMANCE_ETL_GENERATION = 30021;
        public const long PERFORMANCE_METER_NOT_DUPLICATE_AGGREGATES = 30022;
        public const long PERFORMANCE_METER_NOT_DUPLICATE_REFUELS = 30023;
        public const long PERFORMANCE_LOCATION_MANUAL_POS_COMPLETENESS = 30024;
        public const long PERFORMANCE_LOCATION_POS_DIS_MATCH = 30025;
        public const long PERFORMANCE_LOCATION_POS_INT_MAN_MATCH = 30026;
        public const long PERFORMANCE_LOCATION_SLS_POS_COMPLETENESS = 30027;
        public const long PERFORMANCE_LOCATION_HOIS_POS_COMPLETENESS = 30028;
        public const long PERFORMANCE_DELIV_FULL_JOIN = 30029;
        public const long PERFORMANCE_DELIV_TERMINAL_JOIN = 30030;
        public const long PERFORMANCE_DELIV_PLANNED_JOIN = 30031;
        public const long PERFORMANCE_DELIV_CALCULATED_JOIN = 30032;
        public const long PERFORMANCE_DELIV_PLANNED_INTERFACE = 30033;
        public const long PERFORMANCE_DELIV_PLANNED_COMPLETENESS = 30034;
        public const long PERFORMANCE_DELIV_TERMINAL_INTERFACE = 30035;
        public const long PERFORMANCE_DELIV_TERMINAL_COMPLETENESS = 30036;
        public const long PERFORMANCE_DELIV_TERMINAL_LOCATION = 30037;
        public const long PERFORMANCE_DELIV_TERMINAL_ORDER_NUMBER = 30038;
        public const long PERFORMANCE_DELIV_TERMINAL_PRODUCT = 30039;
        public const long PERFORMANCE_DELIV_TERMINAL_DRIVER = 30040;
        public const long PERFORMANCE_DELIV_TERMINAL_DEPOT = 30041;
        public const long PERFORMANCE_DELIV_TERMINAL_TRUCK = 30042;
        public const long PERFORMANCE_DELIV_TERMINAL_TRAILER = 30043;
        public const long PERFORMANCE_DELIV_TERMINAL_VOLUME = 30045;
        public const long PERFORMANCE_LOCATION_PACKETS_RECEIVED = 30046;
        public const long PERFORMANCE_LOCATION_PACKETS_RECEIVED_COUNT = 30047;
        public const long PERFORMANCE_IMR_SERVER_PACKETS_RECEIVED = 30048;
        public const long PERFORMANCE_IMR_SERVER_PACKETS_RECEIVED_COUNT = 30049;
        public const long PERFORMANCE_FPPORTAL_USERS_ONLINE = 30050;
        public const long PERFORMANCE_FPPORTAL_MAIN_ACTIVITY = 30051;
        public const long PERFORMANCE_LOCATION_SIR2_COMPLETENESS = 30052;
        public const long PERFORMANCE_LOCATION_FP_CORE_EQUAL = 30053;
        public const long PERFORMANCE_LOCATION_FP_CONSOLE_EQUAL = 30054;
        public const long PERFORMANCE_LOCATION_CORE_CONSOLE_EQUAL = 30055;
        public const long PERFORMANCE_LOCATION_ACTIVITY = 30056;
        public const long PERFORMANCE_IMR_SERVER_LOCATION_ACTIVITY = 30057;
        public const long PERFORMANCE_REFUEL_SINGLE_PLANNED = 30058;
        public const long PERFORMANCE_REFUEL_SINGLE_TERMINAL = 30059;
        public const long PERFORMANCE_REFUEL_SINGLE_CALCULATED = 30060;
        public const long PERFORMANCE_IMR_SERVER_ACTIVEMQ_HAS_CONSUMERS = 30061;
        public const long PERFORMANCE_LOCATION_FPDWH_DA_RECEIVED = 30062;
        public const long PERFORMANCE_LOCATION_FPDWH_DT_RECEIVED = 30063;
        public const long PERFORMANCE_KPI_DISTRIBUTOR_FUEL_VOLUME = 30064;
        public const long PERFORMANCE_KPI_DISTRIBUTOR_ADBLUE_VOLUME = 30065;
        public const long PERFORMANCE_KPI_DISTRIBUTOR_LPG_LEVEL = 30066;
        public const long PERFORMANCE_PLANNED_DELIVERIES = 30067;
        public const long PERFORMANCE_TERMINAL_DELIVERIES = 30068;
        public const long PERFORMANCE_AVAILABILITY = 30069;
        public const long PERFORMANCE_JOINED_REFUELS = 30070;
        public const long PERFORMANCE_FUEL_STOCK_KPI = 30071;
        #endregion
        #region FAULT_CODES params
        //30501 - 30700
        public const long FAULT_CODES_DIAGNOSTIC_ACTION_RESULTS = 30501;
        public const long FAULT_CODES_DIAGNOSTIC_ACTION_RESULTS_START_DATE = 30502;
        public const long FAULT_CODES_DIAGNOSTIC_ACTION_RESULTS_END_DATE = 30503;
        public const long FAULT_CODES_DIAGNOSTIC_ACTION_RESULTS_START_OPERATOR_ID = 30504;
        public const long FAULT_CODES_DIAGNOSTIC_ACTION_RESULTS_END_OPERATOR_ID = 30505;
        public const long FAULT_CODES_BIG_FAULT = 30506;

        #endregion
        #region ARTICLE params
        //30701 - 30800

        public const long ARTICLE_ELEMENT_TYPE = 30701;

        #endregion
        #region FILTER params
        //32001 - 32500
        public const long FILTER_DATA_ARCH_FILTER_UNIT_TIME = 32001;
        public const long FILTER_DATA_ARCH_FILTER_RANGE_TIME = 32002;
        public const long FILTER_DATA_ARCH_FILTER_SAVE_CACHE_HISTORY = 32003;
        #endregion

        #region CALIBRATION params
        //31501 - 32000
        public const long CALIBRATION_INPUT_METER = 31501;
        public const long CALIBRATION_MULTITHREAD = 31502;
        public const long CALIBRATION_THREAD_NUMBER = 31503;
        public const long CALIBRATION_NUM_OF_REFUELS_GROSS = 31504;
        public const long CALIBRATION_NUM_OF_ERROR_REFUELS_GROSS = 31505;
        public const long CALIBRATION_NUM_OF_NOT_CONNETED_REFUELS = 31506;
        public const long CALIBRATION_NUM_OF_TANKS = 31507;
        public const long CALIBRATION_PERC_OF_ERROR_RECONCILIATION = 31508;
        public const long CALIBRATION_NUM_OF_LOST_HEIGHTS = 31509;
        public const long CALIBRATION_NUM_OF_LOST_CAPACITY = 31510;
        public const long CALIBRATION_CALIB_TABLE_NUM_OF_POINTS = 31511;
        public const long CALIBRATION_HEIGHT_WINDOW_SIZE = 31512;
        public const long CALIBRATION_VOLUME_WINDOW_SIZE = 31513;
        public const long CALIBRATION_RECON_MAX_PERC_ERROR = 31514;
        public const long CALIBRATION_LEARNING_SET_MIN_SIZE = 31515;
        public const long CALIBRATION_REFUEL_NOT_CONNECTED = 31516;
        public const long CALIBRATION_LOST_HEIGHT = 31517;
        public const long CALIBRATION_LOST_MAX_HEIGHT = 31518;
        public const long CALIBRATION_LOST_TANK_CAPACITY = 31519;
        public const long CALIBRATION_ERROR_LOG = 31520;
        public const long CALIBRATION_EXECUTION_TIME = 31521;
        public const long CALIBRATION_REFUEL_QUALITY_GROSS = 31522;
        public const long CALIBRATION_REFUEL_QUALITY_NET = 31523;
        public const long CALIBRATION_CV_RANGE_GROSS = 31524;
        public const long CALIBRATION_CV_RANGE_NET = 31525;
        public const long CALIBRATION_CV_PERC_GROSS = 31526;
        public const long CALIBRATION_CV_PERC_NET = 31527;
        public const long CALIBRATION_CV_AREA_GROSS = 31528;
        public const long CALIBRATION_CV_AREA_NET = 31529;
        public const long CALIBRATION_CHART_CV_VALUE_GROSS = 31530;
        public const long CALIBRATION_CHART_CV_TIME_GROSS = 31531;
        public const long CALIBRATION_CHART_CV_VALUE_NET = 31532;
        public const long CALIBRATION_CHART_CV_TIME_NET = 31533;
        public const long CALIBRATION_CHART_HEIGHT_VALUE = 31534;
        public const long CALIBRATION_CHART_HEIGHT_TIME = 31535;
        public const long CALIBRATION_CHART_TIME = 31536;
        public const long CALIBRATION_NEURAL_NETWORK_PLAIN = 31537;
        #endregion

        #region LANGUAGE params
        // 32501 - 33000
        public const long LANGUAGE_SMALL_FLAG_ID_FILE = 32501;
        #endregion

        #region VERSION params
        // 33001 - 33500

        public const long VERSION_CHANGE_SCHEDULE_NOTE = 33001;
        public const long VERSION_THREAT_ANALYSIS_NOTE = 33002;
        public const long VERSION_INSTALLATION_PLAN_NOTE = 33003;
        public const long VERSION_ROLLBACK_PLAN_NOTE = 33004;
        public const long VERSION_TESTS_SCENARIO_NOTE = 33005;
        public const long VERSION_INVOLVED_OPERATOR_ID = 33006;
        public const long VERSION_ESTIMATED_SYSTEM_DOWNTIME_START_TIME = 33007;
        public const long VERSION_ESTIMATED_SYSTEM_DOWNTIME_END_TIME = 33008;
        public const long VERSION_ESTIMATED_SYSTEM_DOWNTIME_COMPLETION_DATE = 33009;
        public const long VERSION_NOTIFICATION_NOTE = 33010;
        public const long VERSION_ELEMENT_ICON_FILE_ID = 33011;
        public const long VERSION_RESULTS_AND_CONCLUSIONS_NOTE = 33012;
        public const long VERSION_ROLLBACK_REASON_NOTE = 33013;
        public const long VERSION_VALIDATION_STEPS = 33014;
        public const long VERSION_INVOLVED_OPERATORS = 33015;
        public const long VERSION_INSTALLATION_DATE = 33016;
        public const long VERSION_EARLY_POST_DEPLOYMENT_NOTICES = 33017;
        public const long VERSION_ELEMENT_TYPE_REFERENCE_TYPE_ID = 33018;
        public const long VERSION_ELEMENT_TYPE_FORBID_REFERENCE_VALUE = 33019;
        public const long VERSION_DML_PACKAGE_FILE_ID = 33020;
        public const long VERSION_DML_PACKAGE_FILE_TYPE = 33021;
        public const long VERSION_DML_PACKAGE_SOURCE_ID = 33022;

        #endregion

        #region METER_TYPE params

        public const long METER_TYPE_STARK_EXTERNAL_ID = 34001;
        public const long METER_TYPE_GASMETER_INDEX_DIALS = 34002;
        public const long METER_TYPE_GASMETER_UNIT = 34003;

        #endregion
        #region VIEW params

        public const long VIEW_ID_SOURCE_IMR_SERVER = 34501;
        public const long VIEW_INITIALIZE_ID_ROLE = 34502;
        public const long VIEW_IS_TREE_VIEW = 34503;
        public const long VIEW_ID_HIERARCHY_COLUMN = 34504;
        public const long VIEW_ID_HIERARCHY_PARENT_COLUMN = 34505;
        public const long VIEW_IS_SYNCHRONIZABLE = 34506;
        public const long VIEW_ID_DATABASE_TYPE = 34507;
        public const long VIEW_DATABASE_NAME = 34508;
        public const long VIEW_SIMPLE_COLUMN_NAMES = 34509;
        public const long VIEW_IMR_SERVER_WEBSERVICE_ADDRESS = 34510;
        public const long VIEW_IMR_SERVER_WEBSERVICE_BINDING = 34511;
        public const long VIEW_CURRENT_SESSION_GUID = 34512;

        #endregion
        #region VIEW_COLUMN params

        public const long VIEW_COLUMN_COMBINED_ID_DATA_TYPE = 34701;
        public const long VIEW_COLUMN_COMBINED_INDEX_NBR = 34702;
        public const long VIEW_COLUMN_COMBINED_SEPARATOR = 34703;

        public const long VIEW_COLUMN_ASSOCIATIVE_TABLE = 34704;
        public const long VIEW_COLUMN_ASSOCIATIVE_TABLE_ID_DATA_TYPE = 34705;
        public const long VIEW_COLUMN_ASSOCIATIVE_TABLE_INDEX = 34706;

        public const long VIEW_COLUMN_ALLOWED_VALUE = 34707;

        #endregion

        #region DATA read/write
        public const long DEVICE_CLOCK = 100009;    // Zegar urz�dzenia
        public const long ATG_PARAMS_LOW_PRODUCT_LEVEL_VALUE = 100025;    // Pr�g niskiego poziomu paliwa
        public const long ATG_PARAMS_DEADSTOCK_DISTR_LEVEL_VALUE = 100026;    // Pr�g krytcznego poziomu paliwa
        public const long ATG_PARAMS_QUERY_PERIOD = 100027;    // Cz�stotliwo�� odczytu
        public const long ATG_PARAMS_LINK_NUMBER = 100028;    // Numer portu (0-COM1, 1-COM2, 2-USB)
        public const long ATG_PARAMS_ATG_TYPE = 100029;    // Model sterownika
        public const long ATG_PARAMS_TANK_NUMBER = 100030;    // Numer zbiornika (1-16)
        public const long ATG_PARAMS_PRODUCT_CODE = 100031;    // Kod paliwa
        public const long ATG_PARAMS_HIGH_WATER_ALARM_ENABLE = 100032;    // Alarm wysoka woda
        public const long ATG_PARAMS_DEADSTOCK_ALARM_ENABLE = 100033;    // Alarm krytyczny poziom paliwa
        public const long ATG_PARAMS_LOW_PRODUCT_ALARM_ENABLE = 100034;    // Alarm niski poziom paliwa
        public const long ATG_PARAMS_HYSTERESIS = 100035;    // Histereza dla poziom�w alarmowych
        public const long ATG_PARAMS_HIGH_PRODUCT_LEVEL_VALUE = 102041; //Alarmowy wysoki poziom paliwa w litrach (Hi)
        public const long ATG_PARAMS_OVERFILL_PRODUCT_LEVEL_VALUE = 102042; //Alarmowy wysoki poziom paliwa w litrach (HiHi)
        public const long ATG_PARAMS_HIGH_WATER_LEVEL_VALUE = 102043;   //Alarmowy wysoki poziom wody w litrach
        public const long DEVICE_COM1_SPEED = 100037;    // Predkosc transmisji (300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600) - COM1
        public const long DEVICE_COM1_DATAFORMAT = 100038;    // Format transmisji (1 - 8N2, 2 - 8P1, 3 - 8N1, 4 - 7N2, 5 - 7P1, 6 - 7N1) - COM1
        public const long DEVICE_COM1_DATAPARITY = 100039;    // Kontrola transmisji (0 - odd, 1 - even, 2 - mark, 3 - space, 4 - none) - COM1
        public const long DEVICE_COM1_FLOWCONTROL_DCE_BY_DTE = 100040;    // Kontrola przeplywu danych z OKO do urzadzenia zewnetrznego (0 - none, 2 - RTS/CTS) - COM1
        public const long DEVICE_COM1_FLOWCONTROL_DTE_BY_DCE = 100041;    // Kontrola przeplywu danych z urzadzenia zewnetrznego do OKO (0 - none, 2 - RTS/CTS) - COM1
        public const long DEVICE_COM1_AUTORESET = 100042;    // Czy OKO ma sie restartowac w przypadku wystapienia bledu otwarcia linka - COM1
        public const long DEVICE_COM1_TIMEOUT = 100043;    // Maksymalny czas oczekiwania na dane - COM1
        public const long DEVICE_COM2_SPEED = 100045;    // Predkosc transmisji (300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600) - COM2
        public const long DEVICE_COM2_DATAFORMAT = 100046;    // Format transmisji (1 - 8N2, 2 - 8P1, 3 - 8N1, 4 - 7N2, 5 - 7P1, 6 - 7N1) - COM2
        public const long DEVICE_COM2_DATAPARITY = 100047;    // Kontrola transmisji (0 - odd, 1 - even, 2 - mark, 3 - space, 4 - none) - COM2
        public const long DEVICE_COM2_FLOWCONTROL_DCE_BY_DTE = 100048;    // Kontrola przeplywu danych z OKO do urzadzenia zewnetrznego (0 - none, 2 - RTS/CTS) - COM2
        public const long DEVICE_COM2_FLOWCONTROL_DTE_BY_DCE = 100049;    // Kontrola przeplywu danych z urzadzenia zewnetrznego do OKO (0 - none, 2 - RTS/CTS) - COM2
        public const long DEVICE_COM2_AUTORESET = 100050;    // Czy OKO ma sie restartowac w przypadku wystapienia bledu otwarcia linka - COM2
        public const long DEVICE_COM2_TIMEOUT = 100051;    // Maksymalny czas oczekiwania na dane - COM2
        public const long DEVICE_USB_SPEED = 100053;    // Predkosc transmisji (300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600) - USB
        public const long DEVICE_USB_DATAFORMAT = 100054;    // Format transmisji (1 - 8N2, 2 - 8P1, 3 - 8N1, 4 - 7N2, 5 - 7P1, 6 - 7N1) - USB
        public const long DEVICE_USB_DATAPARITY = 100055;    // Kontrola transmisji (0 - odd, 1 - even, 2 - mark, 3 - space, 4 - none) - USB
        public const long DEVICE_USB_FLOWCONTROL_DCE_BY_DTE = 100056;    // Kontrola przeplywu danych z OKO do urzadzenia zewnetrznego (0 - none, 2 - RTS/CTS) - USB
        public const long DEVICE_USB_FLOWCONTROL_DTE_BY_DCE = 100057;    // Kontrola przeplywu danych z urzadzenia zewnetrznego do OKO (0 - none, 2 - RTS/CTS) - USB
        public const long DEVICE_USB_AUTORESET = 100058;    // Czy OKO ma sie restartowac w przypadku wystapienia bledu otwarcia linka - USB
        public const long DEVICE_USB_TIMEOUT = 100059;    // Maksymalny czas oczekiwania na dane - USB
        public const long DEVICE_DISPLAY_TEXT1 = 100065;    // Pierwsza linia wy�wietlacza
        public const long DEVICE_DISPLAY_TEXT2 = 100066;    // Druga linia wy�wietlacza
        public const long DEVICE_EVENT_TRANSMISSION_LINK = 100067;    // Domy�lny kana� komunikacji dla zdarze� (0-NONE, 1-SMS, 2-GPRS, 3-ETH)
        public const long DEVICE_EVENT_PHONE = 100068;    // Numer zdarzeniowy telefonu dla SMS
        public const long DEVICE_EVENT_IP = 100069;    // Numer zdarzeniowy IP dla GPRS
        public const long DEVICE_EVENT_PORT = 100070;    // Numer zdarzeniowy portu dla GPRS
        public const long DEVICE_TEST_GPRS = 100074;    // Zdarzenie test GPRS
        public const long DEVICE_RADIO_ADDRESS = 100080;    // Adres radiowy urz�dzenia
        public const long ALEVEL_CONFIG_HIHI_LEVEL = 100081;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HI_LEVEL = 100082;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LO_LEVEL = 100083;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LOLO_LEVEL = 100084;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPREL_LEVEL = 100085;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPABS_LEVEL = 100086;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HIHI_EVENT_ENABLE = 100087;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HI_EVENT_ENABLE = 100088;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LO_EVENT_ENABLE = 100089;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LOLO_EVENT_ENABLE = 100090;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPREL_EVENT_ENABLE = 100091;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPABS_EVENT_ENABLE = 100092;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HIHI_EVENT_CONFIRM_REQUIRED = 100093;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HI_EVENT_CONFIRM_REQUIRED = 100094;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LO_EVENT_CONFIRM_REQUIRED = 100095;    // <Brak opisu>
        public const long ALEVEL_CONFIG_LOLO_EVENT_CONFIRM_REQUIRED = 100096;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPREL_EVENT_CONFIRM_REQUIRED = 100097;    // <Brak opisu>
        public const long ALEVEL_CONFIG_STEPABS_EVENT_CONFIRM_REQUIRED = 100098;    // <Brak opisu>
        public const long ALEVEL_CONFIG_BIAS = 100099;    // <Brak opisu>
        public const long ALEVEL_CONFIG_HYSTERESIS = 100100;    // <Brak opisu>
        public const long ALEVEL_CONFIG_EVENTS_ENABLE = 100423;    // <Brak opisu>        
        public const long ALEVEL_CONFIG_WL_EVENTS_ENABLE = 100424;    // <Brak opisu>
        public const long ALEVEL_VALUE_RAPID_EVENT = 100425;    // <Brak opisu>
        public const long ALEVEL_NVR = 100426;    // <Brak opisu>
        public const long ALEVEL_VALUE_VALID = 100427;    // <Brak opisu>
        public const long ALEVEL_CONFIG_NVR_UP_ENABLE = 100428;    // <Brak opisu>
        public const long ALEVEL_CONFIG_NVR_DOWN_ENABLE = 100429;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_VALID = 100430;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_EVENTS_ENABLE = 100431;    // <Brak opisu> 
        public const long ALEVEL_RAPID_EVENT_ACTIVE = 100432;    // <Brak opisu>
        public const long ALEVEL_NVR_DETECTED = 100433;    // <Brak opisu>  
        public const long DEVICE_GLOBAL_ENABLE = 100122;    // <Brak opisu>
        public const long DEVICE_ACTION_ALEVEL_ADDRESS = 100124;    // <Brak opisu>
        public const long DEVICE_ACTION_ALEVEL_ENABLE = 100125;    // <Brak opisu>
        public const long DEVICE_ACTION_CID_ENABLE = 100128;    // <Brak opisu>
        public const long DEVICE_TEST_IMR_ENABLE = 100129;    // <Brak opisu>
        public const long DEVICE_SIM_CARD_CHANGE_ENABLE = 100130;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_ENABLE = 100131;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_ENABLE = 100132;    // <Brak opisu>
        public const long DEVICE_EVENT_ERROR_IN_DEVICE_ENABLE = 100133;    // <Brak opisu>
        public const long DEVICE_EVENT_SERVICE_MENU_ENABLE = 100134;    // <Brak opisu>
        public const long DEVICE_EVENT_SCAN_GSM_ENABLE = 100135;    // <Brak opisu>
        public const long DEVICE_TEST_GPRS_ENABLE = 100136;    // <Brak opisu>
        public const long DEVICE_EVENT_MODEM_ON_ENABLE = 100137;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_FULL_ENABLE = 100138;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_ENABLE = 100139;    // <Brak opisu>
        public const long DEVICE_EVENT_CLOCK_ENABLE = 100140;    // <Brak opisu>
        public const long DEVICE_EVENT_FIRMWARE_ON_ENABLE = 100141;    // <Brak opisu>
        public const long DEVICE_EVENT_REFUEL_KNOWN_ENABLE = 100142;    // <Brak opisu>
        public const long DEVICE_EVENT_REFUEL_UNKNOWN_ENABLE = 100143;    // <Brak opisu>
        public const long DEVICE_EVENT_ALEVEL_ERROR_ENABLE = 100144;    // <Brak opisu>
        public const long DEVICE_EVENT_ALEVEL_BATTERY_LO_ENABLE = 100145;    // <Brak opisu>
        public const long DEVICE_CID = 100194;    // <Brak opisu>
        public const long DEVICE_BATTERY_SLEEP_TIME = 100197;    // <Brak opisu>
        public const long DEVICE_BATTERY_RADIO_TIME = 100198;    // <Brak opisu>
        public const long DEVICE_BATTERY_MODEM_TIME = 100199;    // <Brak opisu>
        public const long DEVICE_RTRS_ENABLE = 100205;    // <Brak opisu>
        public const long DEVICE_RTRS_PORT = 100206;    // <Brak opisu>
        public const long DEVICE_RTRS_MIN_PERIOD_TIME = 100207;    // <Brak opisu>
        public const long DEVICE_RTRS_SERVER = 100208;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_UP_EVENT_CONFIRM_REQUIRED = 100213;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_DOWN_EVENT_CONFIRM_REQUIRED = 100214;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_UP_EVENT_ENABLE = 100215;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_DOWN_EVENT_ENABLE = 100216;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_UP_EVENT = 100217;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_STATE_DOWN_EVENT = 100218;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_UP_EVENT_CONFIRM_REQUIRED = 100219;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_DOWN_EVENT_CONFIRM_REQUIRED = 100220;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_UP_EVENT_ENABLE = 100221;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_DOWN_EVENT_ENABLE = 100222;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_UP_EVENT = 100223;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN_DOWN_EVENT = 100224;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_UP_EVENT_CONFIRM_REQUIRED = 100225;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_DOWN_EVENT_CONFIRM_REQUIRED = 100226;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_UP_EVENT_ENABLE = 100227;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_DOWN_EVENT_ENABLE = 100228;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_UP_EVENT = 100229;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO_DOWN_EVENT = 100230;    // <Brak opisu>
        public const long DEVICE_EVENT_BACKUP_TRANSMISSION_LINK = 100231;    // <Brak opisu>
        public const long DEVICE_RADIO_FRAME_VALIDITY = 100232;    // <Brak opisu>
        public const long DEVICE_GPRS_ENABLE_MODE = 100233;    // <Brak opisu>
        public const long DEVICE_SMS_ENABLE_MODE = 100234;    // <Brak opisu>
        public const long DEVICE_PREFFERED_GSM_ROAMING_OPERATOR = 100235;    // <Brak opisu>
        public const long DEVICE_EVENT_REPEAT_TIME = 100236;    // <Brak opisu>
        public const long DEVICE_EVENT_REPEAT_COUNT = 100237;    // <Brak opisu>
        public const long DEVICE_KEEP_ALIVE_MODE = 100238;    // <Brak opisu>
        public const long DEVICE_KEEP_ALIVE_PERIOD = 100239;    // <Brak opisu>
        public const long DEVICE_KEEP_ALIVE_RESET_TIME = 100240;    // <Brak opisu>
        public const long DEVICE_KEEP_ALIVE_ALTERNATIVE_ADDRESS = 100241;    // <Brak opisu>
        public const long DEVICE_KEEP_ALIVE_ALTERNATIVE_PORT = 100242;    // <Brak opisu>
        public const long DEVICE_FRING_RESET_ADDRESS = 100243;    // <Brak opisu>
        public const long DEVICE_FRING_GPRS_RESET_ADDRESS = 100244;    // <Brak opisu>
        public const long DEVICE_RADIO_CALLBACK_ADDRESS = 100245;    // <Brak opisu>
        public const long DEVICE_AT_COMMAND = 100246;    // <Brak opisu>
        public const long DEVICE_AT_COMMAND_RESULT = 100247;    // <Brak opisu>
        public const long DEVICE_ACTION_ALEVEL_FINISHED = 100248;    // <Brak opisu>
        public const long DEVICE_ACTION_ALEVEL_STATUS = 100249;    // <Brak opisu>
        public const long DEVICE_ACTION_CID_FINISHED = 100250;    // <Brak opisu>
        public const long DEVICE_ACTION_CID_STATUS = 100251;    // <Brak opisu>
        public const long DEVICE_SIM_CARD_CHANGE_FINISHED = 100252;    // <Brak opisu>
        public const long DEVICE_SIM_CARD_CHANGE_STATUS = 100253;    // <Brak opisu>
        public const long DEVICE_EVENT_SCAN_GSM_FINISHED = 100254;    // <Brak opisu>
        public const long DEVICE_EVENT_SCAN_GSM_STATUS = 100255;    // <Brak opisu>
        public const long DEVICE_TEST_GPRS_FINISHED = 100256;    // <Brak opisu>
        public const long DEVICE_TEST_GPRS_STATUS = 100257;    // <Brak opisu>
        public const long DEVICE_TEST_IMR_FINISHED = 100258;    // <Brak opisu>
        public const long DEVICE_TEST_IMR_STATUS = 100259;    // <Brak opisu>
        public const long DEVICE_ERRORS_CONFIRMED = 100260;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HIHI_LEVEL = 100261;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HI_LEVEL = 100262;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LO_LEVEL = 100263;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LOLO_LEVEL = 100264;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPREL_LEVEL = 100265;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPABS_LEVEL = 100266;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HIHI_EVENT_ENABLE = 100267;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HI_EVENT_ENABLE = 100268;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LO_EVENT_ENABLE = 100269;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LOLO_EVENT_ENABLE = 100270;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPREL_EVENT_ENABLE = 100271;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPABS_EVENT_ENABLE = 100272;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HIHI_EVENT_CONFIRM_REQUIRED = 100273;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_HI_EVENT_CONFIRM_REQUIRED = 100274;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LO_EVENT_CONFIRM_REQUIRED = 100275;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_LOLO_EVENT_CONFIRM_REQUIRED = 100276;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPREL_EVENT_CONFIRM_REQUIRED = 100277;    // <Brak opisu>
        public const long ALEVEL_AVG_CONFIG_STEPABS_EVENT_CONFIRM_REQUIRED = 100278;    // <Brak opisu>
        public const long ALEVEL_CONFIG_WATER_HI_LEVEL = 102040;
        public const long ALEVEL_AVG_CONFIG_BIAS = 100279;    // <Brak opisu>
        public const long DEVICE_MENU_PREPAID_PRESENT = 100375;    // <Brak opisu>
        public const long DEVICE_MENU_LPG_PRESENT = 100376;    // <Brak opisu>
        public const long DEVICE_VALVE_PIN_CONSTANT = 100377;    // <Brak opisu>
        public const long DEVICE_ACTIVE_TIME = 100378;    // <Brak opisu>
        public const long DEVICE_EXTENDED_ACTIVE_TIME = 100379;    // <Brak opisu>
        public const long DEVICE_EXTENDED_MENU_PIN_NBR = 100392;    // <Brak opisu>
        public const long DEVICE_MAX_SAFE_FLOW_ALLOWED_MEASURE_TIME = 100395;
        public const long ALEVEL_VALUE_REFUEL_EVENT = 100396;
        public const long ALEVEL_VALUE_OUTFLOW_EVENT = 100397;
        public const long ALEVEL_CONFIG_REFUEL_LEVEL = 100398;
        public const long ALEVEL_CONFIG_OUTFLOW_LEVEL = 100399;
        public const long ALEVEL_CONFIG_REFUEL_EVENT_ENABLE = 100400;
        public const long ALEVEL_CONFIG_OUTFLOW_EVENT_ENABLE = 100401;
        public const long ALEVEL_CONFIG_IS_ALEVEL = 100402;
        public const long ALEVEL_CONFIG_IS_METER_LOW_WORD = 100403;

        public const long DEVICE_MODEM_PINS = 102308;

        public const long DEVICE_SCHEDULE_ACTIVATION_MINUTE = 100405;
        public const long DEVICE_SCHEDULE_ACTIVATION_HOUR = 100406;
        public const long DEVICE_SCHEDULE_ACTIVATION_DAY_OF_MONTH = 100407;
        public const long DEVICE_SCHEDULE_ACTIVATION_DAY_OF_WEEK = 100408;
        public const long DEVICE_SCHEDULE_READOUT_MINUTE = 100409;
        public const long DEVICE_SCHEDULE_READOUT_HOUR = 100410;
        public const long DEVICE_SCHEDULE_READOUT_DAY_OF_MONTH = 100411;
        public const long DEVICE_SCHEDULE_READOUT_DAY_OF_WEEK = 100412;
        public const long DEVICE_SCAN_GSM_PROVIDER_GSM_QUALITY = 100413;    // <Brak opisu>
        public const long DEVICE_SCAN_GSM_PROVIDER_MOBILE_NETWORK_CODE = 100414;    // <Brak opisu>

        public const long ANALOG_RADIO_ADDRESS = 100415;    // <Brak opisu>
        public const long MULTIREAD_RADIO_ADDRESS = 100416;    // <Brak opisu>

        public const long DEVICE_FIRMWARE_FLASH_BASE_ADDRESS = 100417;
        public const long DEVICE_FIRMWARE_FLASH_SERVER = 100418;
        public const long DEVICE_FIRMWARE_FLASH_LOGIN = 100419;
        public const long DEVICE_FIRMWARE_FLASH_PASSWORD = 100420;
        public const long DEVICE_FIRMWARE_FLASH_FILE = 100421;
        public const long DEVICE_FIRMWARE_FLASH_STATUS = 100422;
        public const long DEVICE_FIRMWARE_FLASH_SECTORS_COUNT = 100434;

        public const long DEVICE_R2S_MODE_ENABLED = 101270;
        public const long DEVICE_LISTENING_LINK_IN = 101272;
        public const long DEVICE_LISTENING_LINK_OUT = 101273;

        public const long DEVICE_PRE_ACQUISITION_WAIT_TIME = 101215;
        public const long DEVICE_ACQUISITION_AVERAGE_COUNT = 101216;
        public const long DEVICE_REFERENCE_TEMPERATURE_DENSITY = 101217;
        public const long DEVICE_TEMPERATURE_ESTIMATION_GAIN = 101218;
        public const long DEVICE_TEMPERATURE_ESTIMATION_COEFFICIENT = 101219;
        public const long DEVICE_MAX_ADC_VALUE = 101220;
        public const long DEVICE_MIN_ADC_VALUE = 101221;
        public const long DEVICE_SENSOR_CALIBRATION_PARAM_A = 101222;
        public const long DEVICE_SENSOR_CALIBRATION_PARAM_B = 101223;
        public const long DEVICE_PRESSURE_MEASURE_RANGE = 101224;
        public const long DEVICE_TEMPERATURE_OFFSET_TABLE = 101225;
        public const long REVISED_VOLUME = 101226;
        public const long REVISED_FUEL_HEIGHT = 101227;
        public const long TANK_PERCENTAGE_HEIGHT = 101228;
        public const long BATTERY_VOLUME_USAGE = 101229;
        public const long DEVICE_EXTREME_TEMPERATURE = 101230;
        public const long EXTREME_TEMPERATURE_TIMER_TIMESTAMP = 101231;
        public const long EXTREME_TEMPERATURE_TIMER_DURATION = 101232;
        public const long FUEL_DEPTH_RAW = 101233;
        public const long DEVICE_ACTION_CONFIGURATION_ID = 101234;
        public const long DEVICE_ACTION_CONFIGURATION_ID_ACK = 101236;
        public const long DEVICE_EVENT_SCAN_GSM_TIMESTAMP = 101235;
        public const long DEVICE_SIM_CARD_CHANGE_PIN_OK = 101246;

        public const long DEVICE_BATTERY_SLEEP_TIME_DENOMINATOR = 100807;    // <Brak opisu>
        public const long DEVICE_BATTERY_SLEEP_TIME_NUMERATOR = 100808;    // <Brak opisu>
        public const long DEVICE_BATTERY_RADIO_TIME_DENOMINATOR = 100809;    // <Brak opisu>
        public const long DEVICE_BATTERY_RADIO_TIME_NUMERATOR = 100810;    // <Brak opisu>
        public const long DEVICE_BATTERY_MODEM_TIME_DENOMINATOR = 100811;    // <Brak opisu>
        public const long DEVICE_BATTERY_MODEM_TIME_NUMERATOR = 100812;    // <Brak opisu>
        public const long DEVICE_BATTERY_NORMAL_LEVEL = 100813;    // <Brak opisu>
        public const long DEVICE_BATTERY_LOW_LEVEL = 100814;    // <Brak opisu>
        public const long DEVICE_BATTERY_CRITICAL_LEVEL = 100815;    // <Brak opisu>
        public const long DEVICE_BATTERY_MAX_LEVEL = 100816;    // <Brak opisu>
        public const long DEVICE_BATTERY_MAX_OFFSET = 100817;    // <Brak opisu>
        public const long DEVICE_ACTIVATION_DURATION = 100818;    // <Brak opisu>
        public const long DEVICE_ACTIVATION_EXTENSION_SEND = 100819;    // <Brak opisu>
        public const long DEVICE_ACTIVATION_EXTENSION_RECEIVE = 100820;    // <Brak opisu>
        public const long DEVICE_ACTIVATION_LOGGING_TIME = 100821;    // <Brak opisu>
        public const long DEVICE_SMS_VALIDITY_PERIOD = 100822;    // <Brak opisu>
        public const long DEVICE_ANALOG_DATA_ENABLE = 100823;    // <Brak opisu>
        public const long DEVICE_GPRS_SENT_PACKETS_COUNT = 100824;
        public const long DEVICE_GPRS_RECEIVED_PACKETS_COUNT = 100825;
        public const long DEVICE_GPRS_SENT_B_COUNT = 100826;
        public const long DEVICE_GPRS_RECEIVED_B_COUNT = 100827;
        public const long DEVICE_SYNCHRONIZE_MODEM_FROM_CSCA = 100828;
        public const long DEVICE_RADIO_SCAN_PERIOD = 100829;
        public const long DEVICE_RADIO_SCAN_TIME = 100830;

        public const long DEVICE_ARCHIVE_LATCH_START = 102998;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_LATCH_PERIOD = 102999;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_DATA = 103000;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_DATA_PERIODIC = 103002;    // <Brak opisu>
        public const long DEVICE_RAW_DATA_ID_SELECTOR = 103999;    // <Brak opisu>
        public const long DEVICE_RAW_DATA = 104000;    // <Brak opisu>
        public const long DEVICE_ATG_DATA_RAW = 104001;    // <Brak opisu>
        public const long DEVICE_ALEVEL_DATA_RAW = 104002;    // <Brak opisu>
        public const long DEVICE_MULTIREAD_DATA_RAW = 104003;    // <Brak opisu>
        public const long DEVICE_RAW_DATA_TABLE_SELECTOR = 104004;    // <Brak opisu>
        public const long DEVICE_EXTEND_MODEM_LIFE = 101247;
        public const long DEVICE_READ_TANK_DATA = 101248;
        public const long DEVICE_EXTEND_PIC_MODEM_LIFE = 101249;

        public const long DEVICE_ACTION_CONFIGURATION_PART1 = 101237;
        public const long DEVICE_ACTION_CONFIGURATION_PART2 = 101238;
        public const long DEVICE_ACTION_CONFIGURATION_PART3 = 101239;
        public const long DEVICE_ACTION_CONFIGURATION_PART4 = 101240;
        public const long DEVICE_ACTION_CONFIGURATION_ACK_PART1 = 101241;
        public const long DEVICE_ACTION_CONFIGURATION_ACK_PART2 = 101242;
        public const long DEVICE_ACTION_CONFIGURATION_ACK_PART3 = 101243;
        public const long DEVICE_ACTION_CONFIGURATION_ACK_PART4 = 101244;
        public const long DEVICE_ACTION_CONFIGURATION_SEND = 101245;

        public const long DEVICE_COUNTER_SYNCHRONISATION_TIME = 101261;
        public const long DEVICE_COUNTER_SYNCHRONISATION_TIME_MINUTE = 101262;
        public const long DEVICE_COUNTER_SYNCHRONISATION_TIME_HOUR = 101263;
        public const long DEVICE_COUNTER_SYNCHRONISATION_TIME_DAY_OF_MONTH = 101264;
        public const long DEVICE_COUNTER_SYNCHRONISATION_TIME_DAY_OF_WEEK = 101265;
        public const long DEVICE_COUNTER_SYNCHRONISATION_PERIOD = 101266;
        public const long DEVICE_RS485_MASTER_ADDRESS = 101267;
        public const long DEVICE_RS485_SLAVE_ADDRESS = 101268;
        public const long DEVICE_MASTER_MODE_ENABLED = 101269;
        public const long DEVICE_ANALOG_TYPE = 101274;

        public const long DEVICE_TANKING_LEVEL_INDICATOR = 101275;
        public const long DEVICE_TANKING_ENCODER_PERIOD = 101276;
        public const long DEVICE_TANKING_FRAME_PERIOD = 101277;
        public const long DEVICE_MAX_TANKING_TIME = 101278;
        public const long DEVICE_CHECK_TIME = 101279;
        public const long DEVICE_ENCODER_PERIOD = 101280;

        public const long DEVICE_TEMPERATURE_TABLE_VALUE = 101283;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET = 101284;
        public const long DEVICE_CONVERSION_ERROR = 101285;
        public const long DEVICE_FRAME_TYPE_ENABLED = 101286;
        public const long TEMPERATURE_ESTIMATED = 101287;
        public const long MODULE_ERROR_CODE = 101288;
        public const long PROGRAM_ID = 101300;
        public const long INPUT_1_IMPULSE_COUNTER = 101301;
        public const long INPUT_2_IMPULSE_COUNTER = 101302;

        public const long DEVICE_EXTENDED_MENU_PIN = 101314;
        public const long DEVICE_VALVE_PIN_NBR = 101315;
        public const long DEVICE_VALVE_PIN = 101316;
        public const long VALDRIVER_ERROR_MACHINE_STATE = 101317;
        public const long DEVICE_OPEN_VALVE_COUNT = 101318;
        public const long DEVICE_OPEN_VALVE_FAIL_COUNT = 101319;
        public const long DEVICE_CLOSE_VALVE_COUNT = 101320;
        public const long DEVICE_CLOSE_VALVE_FAIL_COUNT = 101321;
        public const long DEVICE_BATTERY_AVAL_STYLE_VOLTAGE_VALUE = 101322;
        public const long DEVICE_DIAGNOSTICS_READ_TIMESTAMP = 101323;
        public const long DEVICE_TIME_TO_SAVE = 101324;
        public const long DEVICE_TIME_TO_SAVE_BIT = 101325;
        public const long DEVICE_TIME_TO_REFRESH_BIT = 101326;
        public const long DEVICE_PREPAID_ENABLED = 101327;
        public const long DEVICE_PREPAID_UPDATE_AUTO_FETCH_ENABLED = 101328;
        public const long DEVICE_PREPAID_ALLOWED_DEBET_VOLUME = 101329;
        public const long DEVICE_PREPAID_ALLOWED_DEBET_SUM = 101330;
        public const long DEVICE_PREPAID_CLEARANCE_ON_SUM_ENABLED = 101331;
        public const long DEVICE_PREPAID_CUT_OF_ENABLED = 101332;
        public const long DEVICE_PREPAID_CUT_OF_ON_SUM_ENABLED = 101333;
        public const long VALDRIVER_LOGICAL_STATE = 101334;
        public const long DEVICE_VALVE_PIN_INFINITE_USAGE_ENABLED = 101335;
        public const long DEVICE_SCHEDULE_READOUT_SECOND = 101336;
        public const long DEVICE_SCHEDULE_READOUT_MONTH = 101337;
        public const long DEVICE_SCHEDULE_READOUT_YEAR = 101338;
        public const long DEVICE_PULSE_INPUT_LEAKAGE_IGNORE_TIME = 101370;
        public const long DEVICE_SCHEDULE_READOUT_FORCED_INDEX = 101371;
        public const long DEVICE_LATCH_ERROR_CLEAR_TIMESTAMP = 101372;

        public const long DEVICE_RETRANSMIT_DIRECT_FRAMES = 101382;
        public const long DEVICE_RETRANSMIT_INDIRECT_FRAMES = 101383;
        public const long DEVICE_FRAME_SEND_DELAY = 101384;
        public const long DEVICE_INSENSITIVE_DURATION = 101385;
        public const long DEVICE_FRAME_DISPERSION = 101386;

        public const long DEVICE_AIR_RESET_PERIOD = 101387;
        public const long MULTIREAD_RADIO_FRAME_LENGTH = 101388;
        public const long DEVICE_SCALE = 101389;

        public const long DEVICE_IMRLAN_FLUSH_ALL = 101390;
        public const long DEVICE_IMRLAN_CMD_STATUS = 101391;
        public const long DEVICE_IMRLAN_CMD_SEQUENCE_NBR = 101392;
        public const long DEVICE_IMRLAN_CMD_PROGRESS = 101393;
        public const long DEVICE_IMRLAN_ABORT_CMD_ID = 101394;
        public const long DEVICE_IMRLAN_SEND_TIMEOUT = 101396;
        public const long DEVICE_IMRLAN_RESPONSE_TIMEOUT = 101397;
        public const long DEVICE_IMRLAN_CMD_ADDRESS = 101547;
        public const long DEVICE_IMRLAN_CMD_ID = 101548;
        public const long DEVICE_IMRLAN_FLUSH_SEND_BUFFER = 101549;

        public const long MULTIREAD_RADIO_ADDRESS_TYPE = 101395;
        public const long DEVICE_BATTERY_REMAINING_DAYS = 101398;

        public const long DEVICE_EVENT_OPERATION_VALVE = 101552;
        public const long DEVICE_EVENT_OPERATION_VALVE_ENABLE = 101553;

        public const long DEVICE_REMAINING_MODEM_LIFE_TIME = 101554;
        public const long DEVICE_ARCHIVE_LATCH_TOLERANCE = 101555;

        public const long AWSR_FUEL_VOLUME = 101400;
        public const long AWSR_WATER_VOLUME = 101401;
        public const long AWSR_FUEL_NO_WATER_VOLUME = 101402;
        public const long AWSR_FUEL_REF_VOLUME = 101403;
        public const long AWSR_FUEL_WEIGHT = 101404;
        public const long AWSR_OPERATING_ULLAGE = 101405;
        public const long AWSR_FUEL_VOLUME_DISPENCED = 101406;
        public const long AWSR_FUEL_VOLUME_DELIVERED = 101407;
        public const long AWSR_FUEL_VOLUME_OUTCOME = 101408;
        public const long AWSR_FUEL_REF_VOLUME_DISPENCED = 101409;
        public const long AWSR_FUEL_REF_VOLUME_DELIVERED = 101410;
        public const long AWSR_FUEL_REF_VOLUME_OUTCOME = 101411;
        public const long AWSR_RECONCILIATION_STATUS = 101412;
        public const long AWSR_RECONCILIATION_CURRENT_ERROR = 101413;
        public const long AWSR_RECONCILIATION_CUMULATIVE_VARIANCE = 101414;
        public const long AWSR_RECONCILIATION_REF_STATUS = 101562;
        public const long AWSR_RECONCILIATION_REF_CURRENT_ERROR = 101563;
        public const long AWSR_RECONCILIATION_REF_CUMULATIVE_VARIANCE = 101564;
        public const long AWSR_CALIBRATION_RMSE_ERROR = 101415;
        public const long AWSR_NN_INPUT_WEIGHT_MATRIX = 101416;
        public const long AWSR_NN_LAYER_WEIGHT_MATRIX = 101417;
        public const long AWSR_NN_BIAS_VECTORS = 101418;
        public const long AWSR_NN_LEARNING_SET_MIN_HEIGHT = 101686;
        public const long AWSR_NN_LEARNING_SET_MAX_HEIGHT = 101687;
        public const long AWSR_NN_LEARNING_SET_MIN_VOLUME = 101688;
        public const long AWSR_NN_LEARNING_SET_MAX_VOLUME = 101689;

        public const long AWSR_RECONCILIATION_CURRENT_PER_ERROR = 101976;
        public const long AWSR_RECONCILIATION_CUMULATIVE_SALE = 101977;
        public const long AWSR_RECONCILIATION_CUMULATIVE_PER_VARIANCE = 101978;
        public const long AWSR_RECONCILIATION_REF_CURRENT_PER_ERROR = 101979;
        public const long AWSR_RECONCILIATION_REF_CUMULATIVE_SALE = 101980;
        public const long AWSR_RECONCILIATION_REF_CUMULATIVE_PER_VARIANCE = 101981;

        public const long AWSR_RECONCILIATION_SALES_CURRENT_ERROR = 102217;
        public const long AWSR_RECONCILIATION_SALES_REF_CURRENT_ERROR = 102218;
        public const long AWSR_RECONCILIATION_SALES_CURRENT_PER_ERROR = 102219;
        public const long AWSR_RECONCILIATION_SALES_REF_CURRENT_PER_ERROR = 102220;
        public const long AWSR_FUEL_RAW_REF_VOLUME_DISPENCED = 102221;
        public const long AWSR_RECONCILIATION_DATA_STATUS = 102228;
        public const long AWSR_RECONCILIATION_NOTES = 103015;
        public const long AWSR_FUEL_POS_VOLUME_DISPENCED = 103026;
        public const long AWSR_PUMPOUT_VOLUME = 103027;
        public const long AWSR_PUMPOUT_VALUE = 103028;

        public const long AWSR_IRC_SIR2_LEAK_RATE = 103029;
        public const long AWSR_IRC_SIR2_DECISION = 103030;
        public const long AWSR_NOZZLE_CALIBRATION_NAME = 103031;

        public const long ATTACHMENT_FILE_SIZE = 103032;
        public const long ATTACHMENT_FILE_URL = 103033;
        public const long ATTACHMENT_FILE_DESCR = 103034;
        public const long ATTACHMENT_FILE_CREATION_DATE = 103035;

        public const long AWSR_IRC_SIR2_OFFSET = 103036;
        public const long AWSR_IRC_SIR2_DECISION_DATE = 103037;

        public const long AWSR_DISPENSED_DISTR_OFFSET = 103039;
        public const long AWSR_DISPENSED_POS_OFFSET = 103040;
        public const long AWSR_OUTCOME_RETURN_OFFSET = 103041;
        public const long AWSR_OUTCOME_LOST_OFFSET = 103042;

        public const long AWSR_IRC_SIR2_OUTLIER_THRESHOLD = 103044;
        public const long AWSR_IRC_SIR2_INCONCLUSIVE_CAUSE = 103045;
        public const long AWSR_IRC_SIR2_LEAK_THRESHOLD = 103046;
        public const long AWSR_IRC_SIR2_METHOD_USED = 103047;
        public const long AWSR_IRC_SIR2_REPORT = 103048;
        public const long AWSR_IRC_SIR2_MDLR = 103049;

        public const long AWSR_RECONCILIATION_CONFIRMATION_TIME = 102997;
        public const long AWSR_RECONCILIATION_DAY = 103001;

        public const long NOZZLE_TOTAL_COUNTER = 101399;
        public const long NOZZLE_LAST_TOTAL_COUNTER = 102229;
        public const long NOZZLE_LAST_TOTAL_COUNTER_TIME = 102230;
        public const long NOZZLE_CALC_NUMBER = 101419;
        public const long NOZZLE_FUELING_POINT = 101420;
        public const long NOZZLE_LOGICAL_NUMBER = 101421;
        public const long NOZZLE_STATE = 101422;
        public const long TANK_QUERY_PERIOD = 101423;
        public const long TANK_ACTIVE = 101424;
        public const long TANK_LINK_NUMBER = 101425;
        public const long TANK_GROUPING_NUMBER = 101426;

        public const long AWSR_PROBLEM_CLASS = 101565;
        public const long AWSR_PROBLEM_PROBABILITY = 101566;
        public const long AWSR_PROBLEM_CRITICALITY = 101567;
        public const long AWSR_PROBLEM_CONFIRMATION_TIME = 101620;
        public const long AWSR_PROBLEM_FUEL_VOLUME = 101690;
        public const long AWSR_PROBLEM_NOTES = 102245;
        public const long AWSR_PROBLEM_OPERATOR_ID = 102258;
        public const long AWSR_PROBLEM_OPERATOR_DECISION = 102259;
        public const long AWSR_PROBLEM_OPERATOR_DECISION_DESCR = 102260;
        public const long AWSR_PROBLEM_OPERATOR_DECISION_TIME = 102261;
        public const long AWSR_PROBLEM_ALARM_ID = 102476;
        public const long AWSR_PROBLEM_ISSUE_ID = 102477;
        public const long AWSR_PROBLEM_FUEL_PERCENTAGE = 104125;
        public const long AWSR_PROBLEM_CONFIRMATION_OPERATOR_ID = 104256;

        public const long FUEL_MASS_IN_PIPING = 101891;
        public const long FUEL_THERMAL_TRANSMITTANCE_COEFF = 101887;
        public const long AWSR_FUEL_RAW_VOLUME_DISPENCED = 101888;
        public const long AWSR_FUEL_COMP_VOLUME_DISPENCED = 101889;
        public const long AWSR_NN_ENSEMBLE_BLOCK = 101890;
        public const long AWSR_FUEL_VOLUME_FROM_USAGE = 101949;
        public const long AWSR_FUEL_LEVEL_FROM_USAGE = 101950;

        public const long AWSR_FP_VERSION = 102660;
        public const long AWSR_FP_UPDATE_QUERY = 102661;
        public const long AWSR_FP_RESTART_NEEDED = 102662;
        public const long AWSR_FP_BACKUP_NEEDED = 102994;

        public const long AWSR_CALIBRATION_TABLE_TIME = 102992;
        public const long AWSR_CALIBRATION_TABLE_REPORTER = 104005;
        public const long AWSR_CURRENT_CALIBRATION_TABLE_TIME = 102993;

        public const long DEVICE_POWER_CONFIG_MODE = 101427;
        public const long DEVICE_POWER_CONFIG_BUCKET_SIZE = 101428;
        public const long DEVICE_POWER_CONFIG_SHUTDOWN_DELAY = 101429;
        public const long POWER_STATE_MODE = 101430;
        public const long POWER_BUCKET_SIZE = 101431;
        public const long POWER_SHUTDOWN_DELAY = 101432;

        public const long DEVICE_ORDER_NUMBER_NAME = 101433;
        public const long DEVICE_ORDER_NUMBER_FIRST_QUARTER = 101434;
        public const long DEVICE_ORDER_NUMBER_SECOND_QUARTER = 101435;
        public const long DEVICE_ORDER_NUMBER_THIRD_QUARTER = 101436;
        public const long DEVICE_SERIAL_NBR = 101437;
        public const long DEVICE_FIRMWARE_INFORMATION = 101438;
        public const long DEVICE_FIRMWARE_BUILD_DATE = 101439;

        public const long DEVICE_TIME_ZONE_MARK = 101478;

        public const long DEVICE_TANK_SCAN_VREF_P = 101479;
        public const long DEVICE_TANK_SCAN_VREF_M = 101480;
        public const long DEVICE_TANK_SCAN_PRE_ACQ_WAIT = 101481;
        public const long DEVICE_TANK_SCAN_RANGE_L = 101482;
        public const long DEVICE_TANK_SCAN_RANGE_H = 101483;

        //---
        public const long DEVICE_ARANGE_ADDRESS = 101484;
        public const long DEVICE_ARANGE_FRAME_PERIOD = 101485;
        public const long DEVICE_ARANGE_FRAME_ENABLED = 101486;

        public const long DEVICE_WL_MAX_WINDOW_SIZE_MULTIPLIER = 101487;
        public const long DEVICE_WL_SCAN_WINDOW_WIDTH = 101488;
        public const long DEVICE_WL_FRAME_MULTIPLIER = 101489;
        public const long DEVICE_WL_MAX_LEARNING_RETRIES = 101490;
        public const long DEVICE_WL_WAKEUP_OFFSET = 101491;
        public const long DEVICE_WL_ONE_TICK_CHARACTERS_COUNT = 101492;
        public const long DEVICE_WL_ENABLED = 101493;
        public const long DEVICE_WL_RETRANSMITTION_ENABLED = 101494;
        public const long DEVICE_WL_WIRELESS_LINK_FRAMES_ONLY = 101495;
        public const long DEVICE_WL_FORCE_LEARN_ON_LOST = 101496;
        public const long DEVICE_WL_IGNORE_RETRANSMITTED_FRAMES = 101497;
        public const long DEVICE_WL_EVENT_ALL_RETRIES_USED = 101498;
        public const long DEVICE_WL_EVENT_CONNECTION_LOST = 101499;

        public const long DEVICE_WL_LINK_LOST_UP_EVENT_CONFIRM_REQUIRED = 101568;
        public const long DEVICE_WL_LINK_LOST_DOWN_EVENT_CONFIRM_REQUIRED = 101569;
        public const long DEVICE_WL_LINK_LOST_UP_EVENT_ENABLED = 101570;
        public const long DEVICE_WL_LINK_LOST_DOWN_EVENT_ENABLED = 101571;
        public const long DEVICE_WL_LINK_LOST_EVENT_ENABLED = 101572;

        public const long DEVICE_WL_LINK_DEAD_UP_EVENT_CONFIRM_REQUIRED = 101573;
        public const long DEVICE_WL_LINK_DEAD_DOWN_EVENT_CONFIRM_REQUIRED = 101574;
        public const long DEVICE_WL_LINK_DEAD_UP_EVENT_ENABLED = 101575;
        public const long DEVICE_WL_LINK_DEAD_DOWN_EVENT_ENABLED = 101576;
        public const long DEVICE_WL_LINK_DEAD_EVENT_ENABLED = 101577;

        public const long DEVICE_WL_RESET = 101578;
        public const long DEVICE_WL_DEAD_RESET = 101590;

        public const long DEVICE_WL_LOST_EVENT_COUNT = 101579;
        public const long DEVICE_WL_DEAD_EVENT_COUNT = 101580;
        public const long DEVICE_WL_VALID_SCAN_COUNT = 101581; //DEVICE_WL_VALID_TRANSMITTED_FRAME_COUNT
        public const long DEVICE_WL_INVALID_SCAN_COUNT = 101582; //DEVICE_WL_INVALID_TRANSMITTED_FRAME_COUNT
        public const long DEVICE_WL_DEAD_RESET_COUNT = 101583;
        public const long DEVICE_RADIO_WAKEUP_COUNT = 101584; //DEVICE_RADIO_SCAN_COUNT
        public const long DEVICE_MODEM_WAKEUP_COUNT = 101585;

        public const long DEVICE_WL_LINK_DEAD_UP_EVENT = 101586;
        public const long DEVICE_WL_LINK_DEAD_DOWN_EVENT = 101587;
        public const long DEVICE_WL_LINK_LOST_UP_EVENT = 101588;
        public const long DEVICE_WL_LINK_LOST_DOWN_EVENT = 101589;

        public const long DEVICE_LINK_CABLE_ENABLED = 101500;
        public const long DEVICE_LINK_RADIO_ENABLED = 101558;
        public const long DEVICE_LINK_TRANSPARENT_CABLE_ENABLED = 101559;
        public const long DEVICE_LINK_TRANSAPRENT_RADIO_ENABLED = 101560;

        public const long DEVICE_PROCESSOR_IO_STATE = 101561;

        public const long AVERAGE_LEVEL = 101591;
        public const long MAXIMUM_LEVEL = 101592;
        public const long MINIMUM_LEVEL = 101593;
        public const long MEASURES_COUNT = 101594;
        public const long AVERAGE_AVG_LEVEL = 101609;
        public const long MAXIMUM_AVG_LEVEL = 101610;
        public const long MINIMUM_AVG_LEVEL = 101611;
        public const long MEASURES_AVG_COUNT = 101612;

        public const long DEVICE_EVENT_RAPID_UP_EVENT_CONFIRM_REQUIRED = 101595;
        public const long DEVICE_EVENT_RAPID_DOWN_EVENT_CONFIRM_REQUIRED = 101596;
        public const long DEVICE_EVENT_RAPID_UP_EVENT_ENABLE = 101597;
        public const long DEVICE_EVENT_RAPID_DOWN_EVENT_ENABLE = 101598;
        public const long DEVICE_EVENT_RAPID_ENABLE = 101599;

        public const long DEVICE_EVENT_RAPID_FRAME_PERIOD = 101603;
        public const long DEVICE_EVENT_RAPID_WINDOW_WIDTH = 101604;
        public const long DEVICE_EVENT_RAPID_TRIGGER_LEVEL = 101605;
        public const long DEVICE_EVENT_RAPID_HYSTERESIS = 101606;

        public const long ANALOG_ARCHIVE_LATCH_PERIOD = 101600;
        public const long ANALOG_ARCHIVE_LATCH_OFFSET = 101601;
        public const long ANALOG_ARCHIVE_DATA = 101602;
        public const long ANALOG_ARCHIVE_DATA_TIMESTAMP = 101607;
        public const long ANALOG_ARCHIVE_DATA_ROWS_COUNT = 101608;


        public const long DEVICE_MODEM_ON_EXCEPTION_WAKEUP_DELAY = 101613;
        public const long DEVICE_MODEM_ON_UNSUCCESSFUL_LAUNCH_WAKEUP_DELAY = 101614;
        public const long DEVICE_MODEM_MAXIMUM_SLEEPING_TIME = 101615;
        public const long DEVICE_MODEM_MAXIMUM_RUNNING_TIME = 101616;
        public const long DEVICE_MODEM_RUN_WATCHDOG_TIMEOUT = 101617;
        public const long DEVICE_MODEM_LAUNCH_WATCHDOG_TIMEOUT = 101618;
        public const long DEVICE_TEST_IMR_FIRST_INIT_DELAY = 101627;

        public const long ANALOG_DATA = 101619;

        public const long DEVICE_EVENT_RAPID_UP_EVENT = 101621;
        public const long DEVICE_EVENT_RAPID_DOWN_EVENT = 101622;
        public const long DEVICE_EVENT_RAPID_MINIMUM_VALUE = 101623;
        public const long DEVICE_EVENT_RAPID_MAXIMUM_VALUE = 101624;
        public const long DEVICE_EVENT_RAPID_MINIMUM_TIMESTAMP = 101625;
        public const long DEVICE_EVENT_RAPID_MAXIMUM_TIMESTAMP = 101626;
        public const long DEVICE_EVENT_RAPID_MINIMUM_AVG_VALUE = 101632;
        public const long DEVICE_EVENT_RAPID_MAXIMUM_AVG_VALUE = 101633;
        public const long DEVICE_EVENT_RAPID_MINIMUM_AVG_TIMESTAMP = 101634;
        public const long DEVICE_EVENT_RAPID_MAXIMUM_AVG_TIMESTAMP = 101635;

        public const long DEVICE_SELF_FRAME_PERIOD = 101628;
        public const long DEVICE_SELF_FRAME_ENABLE = 101629;
        public const long DEVICE_RADIO_433_FRAME_TYPE = 101630;

        public const long DEVICE_FORCED_GSM_ROAMING_OPERATOR = 101631;

        public const long DEVICE_WL_LINK_DEAD_STATE = 101636;
        public const long DEVICE_WL_LINK_LOST_STATE = 101637;

        public const long DEVICE_EVENT_RAPID_VALIDITY_WINDOWS_COUNT = 101638;

        public const long DEVICE_ARCHIVE_DATA_TIMESTAMP = 101639;
        public const long DEVICE_ARCHIVE_DATA_ROWS_COUNT = 101640;

        public const long DEVICE_WL_SCAN_WINDOW_MISS_EXTENSION = 101641;
        public const long DEVICE_ANALOG_TYPE_WL_ENABLED = 101642;
        public const long DEVICE_ANALOG_TYPE_RAW_ENABLED = 101643;

        public const long DEVICE_ARANGE_IMR_TEST_FRAME_PERIOD = 101644;
        public const long DEVICE_ARANGE_RECEIVE_ENABLED = 101645;
        public const long DEVICE_WL_LAST_STEPABS_LEVEL = 101646;
        public const long DEVICE_WL_PERIOD = 101647;
        public const long DEVICE_WL_TIME_TO_SCAN = 101648;
        public const long DEVICE_WL_LEARNING_RETRIES = 101649;
        public const long DEVICE_WL_LINK_DEAD_RETRIES = 101650;
        public const long DEVICE_WL_TIME_OFFSET = 101651;
        public const long DEVICE_WL_WINDOW_SIZE_MULTIPLIER = 101652;
        public const long DEVICE_WL_LAST_FRAME_NBR = 101653;
        public const long DEVICE_WL_LEARNING = 101654;
        public const long DEVICE_WL_ACTIVE_WINDOW = 101655;
        public const long DEVICE_WL_LEVEL_VALID = 101656;
        public const long DEVICE_WL_LEARNED = 101657;

        public const long DEVICE_ARANGE_RECEIVE_DURATION = 101658;

        public const long ANALOG_ARCHIVE_READOUT_ROWS_COUNT = 101659;
        public const long ANALOG_ARCHIVE_READOUT_VALIDITY_MARK_ENABLED = 101660;
        public const long ANALOG_ARCHIVE_READOUT_TIMESTAMP_ENABLED = 101661;
        public const long ANALOG_ARCHIVE_READOUT_TEMPERATURE_ENABLED = 101662;
        public const long ANALOG_ARCHIVE_READOUT_TANK_AVG_ENABLED = 101663;
        public const long ANALOG_ARCHIVE_READOUT_TANK_MIN_ENABLED = 101664;
        public const long ANALOG_ARCHIVE_READOUT_TANK_MAX_ENABLED = 101665;
        public const long ANALOG_ARCHIVE_READOUT_TANK_MEASUMRENTS_COUNT_ENABLED = 101666;
        public const long ANALOG_ARCHIVE_READOUT_AVERAGE_AVG_ENABLED = 101667;
        public const long ANALOG_ARCHIVE_READOUT_AVERAGE_MIN_ENABLED = 101668;
        public const long ANALOG_ARCHIVE_READOUT_AVERAGE_MAX_ENABLED = 101669;
        public const long ANALOG_ARCHIVE_READOUT_AVERAGE_MEASUMRENTS_COUNT_ENABLED = 101670;
        public const long ANALOG_ARCHIVE_READOUT_AVG_ENABLED = 101671;
        public const long ANALOG_ARCHIVE_READOUT_MIN_ENABLED = 101672;
        public const long ANALOG_ARCHIVE_READOUT_MAX_ENABLED = 101673;
        public const long ANALOG_ARCHIVE_READOUT_MEASUMRENTS_COUNT_ENABLED = 101674;

        public const long DEVICE_WL_IGNORE_FIRST_FRAME_EVENT = 101675;

        public const long DEVICE_PRESSURE_RANGE_ANTI_OVERFLOW_COEFFICIENT = 101676;

        public const long DEVICE_PULSE_INPUT_MAX_FLOW_CAPTURE_COUNT = 101691;
        public const long DEVICE_TEMPERATURE_READ_PERIOD = 101692;
        public const long DEVICE_TEMPERATURE_OFFSET = 101693;
        public const long DEVICE_ERROR_SENSOR_SCAN = 101694;
        public const long DEVICE_LATCH_ERROR_SENSOR_SCAN = 101695;
        public const long DEVICE_ERROR_USART_DATA = 101696;
        public const long DEVICE_LATCH_ERROR_USART_DATA = 101697;
        public const long DEVICE_PULSE_INPUT_ENABLED = 101698;
        public const long DEVICE_SABOTAGE_UP_EVENT_ENABLED = 101699;
        public const long DEVICE_SENSOR_ENABLED = 101700;
        public const long DEVICE_ACQUISITION_DELAY = 101701;
        public const long DEVICE_ACQUISITION_CUT = 101702;
        public const long DEVICE_USART_BAUD_RATE_ID = 101703;
        public const long DEVICE_USART_TYPE = 101704;
        public const long DEVICE_USART_WAKEUP_AFTER_QUERY_FRAME_TIME = 101705;
        public const long DEVICE_USART_SLEEP_TIME = 101706;
        public const long DEVICE_USART_QUERY_TIME = 101707;
        public const long DEVICE_USART_RESET_PERIOD = 101708;
        public const long DEVICE_USART_POWER_ON_ENABLED = 101709;
        public const long DEVICE_USART_ALWAYS_ON_ENABLED = 101710;
        public const long DEVICE_USART_QUERY_FRAME_ENABLED = 101711;
        public const long DEVICE_USART_INCOMING_DATA_WAKEUP_ENABLED = 101712;
        public const long DEVICE_USART_WAKEUP_AFTER_QUERY_FRAME_ENABLED = 101713;
        public const long DEVICE_USART_CYCLIC_WAKEUP_ENABLED = 101714;
        public const long DEVICE_USART_CYCLIC_RESET_ENABLED = 101715;
        public const long DEVICE_USART_SLEEP_AFTER_FRAME_ENABLED = 101716;
        public const long DEVICE_FRAME_BODY_DEF_OFFSET = 101717;
        public const long TRANSDUCER_NORMALIZED_VALUE = 101725;
        public const long DEVICE_MAX_PRESSURE_VALUE = 101726;
        public const long DEVICE_MAX_TRANSDUCER_NORMALIZED_VALUE = 101727;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_1 = 101728;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_1 = 101729;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_2 = 101730;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_2 = 101731;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_3 = 101732;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_3 = 101733;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_4 = 101734;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_4 = 101735;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_5 = 101736;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_5 = 101737;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_6 = 101738;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_6 = 101739;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_7 = 101740;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_7 = 101741;
        public const long DEVICE_TEMPERATURE_TABLE_OFFSET_8 = 101742;
        public const long DEVICE_TEMPERATURE_TABLE_VALUE_8 = 101743;
        public const long DEVICE_CONVERSION_TABLE_ARG_1 = 101744;
        public const long DEVICE_CONVERSION_TABLE_VAL_1 = 101745;
        public const long DEVICE_CONVERSION_TABLE_ARG_2 = 101746;
        public const long DEVICE_CONVERSION_TABLE_VAL_2 = 101747;
        public const long DEVICE_FRAME_BODY_DEF_LENGTH = 101748;

        public const long DEVICE_COM1_PING_COUNT = 101749;
        public const long DEVICE_COM1_PING_TIMEOUT = 101750;

        public const long AIR_PRESSURE = 101754;

        public const long ENERGY_READOUT_STATUS = 101755;
        public const long FLOW_IN_TEMPERATURE_READOUT_STATUS = 101756;
        public const long FLOW_OUT_TEMPERATURE_READOUT_STATUS = 101757;
        public const long CURRENT_FLOW_READOUT_STATUS = 101758;
        public const long ACTUATOR_POSITION = 101759;

        public const long DEVICE_AIR_WDT = 101760;
        public const long DEVICE_DISPLAY_ENERGY_ENABLED = 101761;
        public const long DEVICE_ENERGY_SLOPE = 101762;
        public const long DEVICE_ENERGY_OFFSET = 101763;

        public const long DEVICE_CONFIGURATION_CHANGE_COUNTER = 101764;
        public const long WORK_TIME_WITH_LOW_POWER = 101765;
        public const long WORK_TIME_WITH_HIGH_POWER = 101766;

        public const long DEVICE_POWER_CONFIG_LOW_INDEX = 101767;
        public const long DEVICE_POWER_CONFIG_LOW_SCALE = 101768;
        public const long DEVICE_POWER_CONFIG_LOW_OFFSET = 101769;
        public const long DEVICE_POWER_CONFIG_HIGH_INDEX = 101770;
        public const long DEVICE_POWER_CONFIG_HIGH_SCALE = 101771;
        public const long DEVICE_POWER_CONFIG_HIGH_OFFSET = 101772;

        public const long DEVICE_ASI_CONNECTION_ID = 101773;

        public const long DEVICE_ERROR_TRANSMISSION = 101774;
        public const long DEVICE_ERROR_MONTH_DIFF_CONFIDENCE = 101775;
        public const long DEVICE_ERROR_DAY_DIFF_CONFIDENCE = 101776;
        public const long DEVICE_ERROR_LOG_WRITE = 101777;
        public const long DEVICE_ERROR_COVER_OPEN = 101778;
        public const long DEVICE_LATCH_ERROR_TRANSMISSION = 101779;
        public const long DEVICE_LATCH_ERROR_MONTH_DIFF_CONFIDENCE = 101780;
        public const long DEVICE_LATCH_ERROR_DAY_DIFF_CONFIDENCE = 101781;
        public const long DEVICE_LATCH_ERROR_LOG_WRITE = 101782;
        public const long DEVICE_LATCH_ERROR_COVER_OPEN = 101783;
        public const long DEVICE_PROBE_PRESSURE_MIN = 102192;
        public const long DEVICE_PROBE_PRESSURE_MAX = 102193;


        public const long DEVICE_LOG_WRITE_COUNTER = 101784;

        public const long ALEVEL_CONFIG_ACTIVITY = 101785;
        public const long ALEVEL_CONFIG_TIMEOUT = 101786;

        public const long DEVICE_TEST_IMR_EXTENDED = 101883;

        public const long DEVICE_DIODE_RX_ENABLE = 101935;
        public const long DEVICE_BUZZER_RX_ENABLE = 101936;

        public const long ATG_CALCULATION_MODE = 101943;    //Numer funkcji przeliczajacej pomiary w IMR-ATG
        public const long ATG_TANK_DEVICE_ORDER = 101944;   //Kolejnosc urzadzen dla zbiornika w IMR-ATG
        public const long ATG_DEVICE_TYPE = 101985;         //Typ urzadzenia w IMR-ATG

        public const long DEVICE_MULTIREAD_ARCHIVE_TIME_OFFSET = 102274;
        public const long DEVICE_MULTIREAD_ARCHIVE_WINDOWS_COUNT = 102275;
        public const long DEVICE_MULTIREAD_ARCHIVE_TIME_RANGE = 102276;

        public const long DEVICE_VALDRIVER_COMMAND_CODE = 102296;
        public const long DEVICE_VALDRIVER_RETRY_LIMIT = 102297;
        public const long DEVICE_VALDRIVER_OPERATION_TIMEOUT = 102298;
        public const long DEVICE_VALDRIVER_OPERATION_ID = 102299;
        public const long DEVICE_VALDRIVER_LEAK_TEST_DURATION = 102301;
        public const long DEVICE_VALDRIVER_OPENING_RETRIES = 102302;
        public const long DEVICE_VALDRIVER_SIMPLE_PROCEDURES = 102304;

        public const long DEVICE_CLOCK_LOCAL_TIME = 102303;
        public const long DEVICE_IMRWAN2_ENCRYPTED_DATA_ONLY = 102337;
        public const long DEVICE_TEST_GSM_PENDING = 102338;
        public const long DEVICE_GSM_TEST_AFTER_SEAL = 102345;
        public const long DEVICE_GSM_TEST_MENU_REQUEST_ENABLED = 102403;
        public const long DEVICE_IMR_TEST_MENU_REQUEST_ENABLED = 102404;
        public const long DEVICE_FREQ_MENU_REQUEST_ENABLED = 102405;

        public const long DEVICE_COUNTER_SHIFT_DETECTED = 102371;
        public const long DEVICE_COUNTER_SHIFT_DETECTION_TIME = 102372;
        public const long LAST_GAS_METER_USAGE_BEFORE_SHIFT = 102373;

        public const long DEVICE_LCD_DRIVER_LP_SLEEP = 102375;
        public const long DEVICE_LCD_DRIVER_LP_RUN = 102376;
        public const long DEVICE_LCD_DRIVER_WFT = 102377;
        public const long DEVICE_LCD_DRIVER_LCDCST = 102378;
        public const long DEVICE_LCD_DRIVER_LRLAP = 102379;
        public const long DEVICE_LCD_DRIVER_LRLBP = 102380;
        public const long DEVICE_LCD_DRIVER_LRLAT = 102381;
        public const long DEVICE_LCD_DRIVER_LCDIRS = 102382;

        public const long DEVICE_VALDRIVER_CLOSE_ON_CLOSED_LAKAGE = 102384;

        public const long DEVICE_AUTO_SEND_TEST_IMR_RESULT = 102216;
        public const long DEVICE_FORCE_FREQ_AFTER_RESET = 102119;
        public const long DEVICE_MODEM_IMR_TEST_ENABLED = 102406;
        public const long DEVICE_MODEM_GSM_TEST_ENABLED = 102407;
        public const long DEVICE_MODEM_VALVE_REQUEST_ACTIVE = 102408;

        public const long DEVICE_QUERY_BLOCKAGE_ENABLED = 102409;
        public const long DEVICE_RESET_ON_NEW_QUERY = 102410;
        public const long DEVICE_MAX_QUERY_COUNT = 102411;
        public const long DEVICE_BLOCKAGE_PERIOD = 102412;
        public const long DEVICE_BLOCKAGE_DURATION = 102413;
        public const long DEVICE_DIAGNOSTICS_DATA = 102414;

        public const long DEVICE_BROWN_OUT_RESET_ENABLED = 102415;
        public const long DEVICE_LOW_VOLTAGE_DETECT_ENABLED = 102416;
        public const long DEVICE_LOW_VOLTAGE_DETECT_LEVEL = 102417;
        public const long DEVICE_CC1120_ENABLED = 102418;

        public const long DEVICE_LON_QUERY_PERIOD = 102419;
        public const long DEVICE_LON_RESPONSE_WAIT_TIMEOUT = 102420;
        public const long DEVICE_LON_ENABLED = 102421;

        public const long DEVICE_TEMPERATURE_CORRECTION_POLY_A = 102434;
        public const long DEVICE_TEMPERATURE_CORRECTION_POLY_B = 102435;
        public const long DEVICE_TEMPERATURE_CORRECTION_POLY_DIVIDER = 102436;
        public const long DEVICE_TEMPERATURE_CORRECTION_POLY_ENABLED = 102437;


        public const long DEVICE_RADIO_FREQ_POLY_A = 102447;
        public const long DEVICE_RADIO_FREQ_POLY_B = 102448;
        public const long DEVICE_RADIO_FREQ_POLY_C = 102449;
        public const long DEVICE_RADIO_FREQ_POLY_D = 102450;

        public const long DEVICE_FORCED_WALKBY_FRAMES_ENABLED = 102451;
        public const long DEVICE_SCRAMBLE_WALKBY_FRAMES_ENABLED = 102454;
        public const long EMERGENCY_CREDIT_AVALIABLE_AT = 102455;
        public const long PUBLIC_HOLIDAY_ALLOW_VALVE_CLOSE = 102456;
        public const long PUBLIC_HOLIDAY_SUSPEND = 102457;

        public const long PREPAID_CHARGE_VARIABLE_UPDATE_DATA_STEP = 102458;
        public const long PREPAID_CHARGE_FIXED_UPDATE_HOUR = 102459;
        public const long PREPAID_SUSPEND_DURATION_AFTER_MANUAL_OPEN = 102460;
        public const long PREPAID_MENU_VISIBILITY_EMERGENCY_CREDIT = 102461;
        public const long PREPAID_MENU_VISIBILITY_OWED = 102462;
        public const long PREPAID_MENU_VISIBILITY_RECOMMENDED_MONTHLY = 102463;
        public const long PREPAID_MENU_VISIBILITY_RECOMMENDED_WEEKLY = 102464;
        public const long PREPAID_MENU_VISIBILITY_RECOMMENDED_TOPUP_DATE = 102465;
        public const long PREPAID_MENU_VISIBILITY_UNIT_PRICE = 102466;
        public const long PREPAID_MENU_VISIBILITY_CREDIT_VOLUME = 102467;
        public const long PREPAID_ENABLED = 102468;
        public const long PREPAID_OPERATIONAL = 102469;
        public const long PREPAID_CHECK_CREDIT_ON_CLOSE = 102470;
        public const long PREPAID_ENABLE_PUBLIC_HOLIDAY_DEF = 102471;
        public const long PREPAID_SUSPEND_PREPAID_ON_MANUAL_VALVE_OPEN = 102472;
        public const long PREPAID_VALIDATE_TOP_UP = 102473;
        public const long CHARGE_PREPAID_FIXED_TOTAL = 102478;
        public const long PREPAID_ACTIVE = 102479;
        public const long PREAPID_RESUME_REASON = 102480;
        public const long PREAPID_RESUME_TIME = 102481;
        public const long PUBLIC_HOLIDAY_PREVENT_VALVE_CLOSE = 102482;

        public const long DEVICE_LCD_TYPE = 102474;
        public const long DEVICE_WMBUS_FREQUENCY_TEMPERATURE_OFFSET = 102475;
        public const long DEVICE_ASOL_CONFIG = 102487;
        public const long DEVICE_PREPAID_TEST_ACTIVE = 102490;
        public const long DEVICE_FREQ_MENU_MULTIPLE_REQUEST_ENABLED = 102491;
        public const long DEVICE_RADIO_SCAN_COUNT = 102492;
        public const long PRICE_PREPAID_FIXED_DAILY = 102494;
        public const long PREPAID_TEST_LOCK_DURATION = 102495;
        public const long PREPAID_ALLOW_SUSPEND = 102496;

        public const long DEVICE_ANALOG_CONFIG_FUNCTION_TYPE = 102502;
        public const long DEVICE_ANALOG_CONFIG_SOURCE = 102503;
        public const long DEVICE_REMOTE_ANALOG_BYTE_OFFSET = 102504;
        public const long DEVICE_REMOTE_ANALOG_NUMBER_OF_BYTES = 102505;
        public const long DEVICE_REMOTE_ANALOG_CONFIDENCE_VALID = 102506;
        public const long DEVICE_REMOTE_ANALOG_CONFIDENCE_BYTE_NUMBER = 102507;
        public const long DEVICE_REMOTE_ANALOG_CONFIDENCE_BIT_NUMBER = 102508;


        public const long DEVICE_FREQ_CONFIG_BASIC_ANALOG_DATA = 102527;
        public const long DEVICE_FREQ_CONFIG_EXTENDED_ANALOG_DATA = 102528;
        public const long DEVICE_FREQ_CONFIG_RAPID_DATA = 102529;
        public const long DEVICE_PREPAID_TOPUP_DATA = 102533;
        public const long DEVICE_PREPAID_LOG_DATA = 102534;
        public const long DEVICE_PREPAID_ARCHIVE_DATA_VALID_MARK = 102540;
        public const long DEVICE_PREPAID_LOG_DATA_VALID_MARK = 102541;
        public const long PREPAID_LOG_POSITION = 102543;
        public const long PREPAID_ARCHIVE_POSITION = 102544;
        public const long PREPAID_ARCHIVE_CURRENT_TOPUP = 102545;
        public const long PREPAID_ARCHIVE_CURRENT_FINISH_CREDIT = 102546;
        public const long DEVICE_WMBUS_CONFIG_DISTRIBUTOR_ID = 102547;
        public const long DEVICE_WMBUS_CONFIG_SYSTEM_ID = 102548;
        public const long DEVICE_WMBUS_CONFIG_AREA_ID = 102549;
        public const long DEVICE_FORCE_SCAN_ON_ANALOG_ACQUIRE = 102550;
        public const long DEVICE_ANALOG_ARCHIVE_COEFFICIENT = 102552;

        public const long DEVICE_DIRECT_MEMORY_TYPE_EEPROM = 102553;
        public const long DEVICE_DIRECT_MEMORY_TYPE_RAM = 102554;
        public const long DEVICE_DIRECT_MEMORY_TYPE_FLASH = 102555;
        public const long DEVICE_ACQUISITION_INTER_SCAN_DELAY = 102556;

        public const long ANALOG_MEASUREMENT_OUT_OF_RANGE = 102557;
        public const long ANALOG_OUT_OF_RANGE = 102558;
        public const long ANALOG_MEASUREMENT_MARGIN_RANGE = 102559;
        public const long ANALOG_AVG_MEASUREMENT_OUT_OF_RANGE = 102560;
        public const long ANALOG_AVG_OUT_OF_RANGE = 102561;
        public const long ANALOG_AVG_MEASUREMENT_MARGIN_RANGE = 102562;
        public const long ANALOG_AVG_TEMPERATURE_VALID = 102563;
        public const long ANALOG_AVG_BATTERY_VALID = 102564;
        public const long ANALOG_AVG_SIGNAL_VALID = 102565;

        public const long ANALOG_NOT_CONNECTED = 102566;
        public const long ANALOG_SHORTED = 102567;
        public const long DEVICE_GPS_BAUD_RATE = 102574;
        public const long DEVICE_CURRENT_APN_IP = 102575;
        public const long DEVICE_WMBUS_WALKBY_SLOT_SIZE = 102580;
        public const long DEVICE_WMBUS_WALKBY_CYCLE = 102581;
        public const long DEVICE_FULL_ARCHIVE_REQUEST = 102582;

        public const long DEVICE_UDI_MODULE_ENABLED = 102583;
        public const long DEVICE_UDI_EVENTS_ENABLED = 102584;
        public const long DEVICE_UDI_ENABLED = 102585;
        public const long DEVICE_UDI_ACTIVE_LOGIC = 102586;
        public const long DEVICE_UDI_FREQ_ENABLED = 102587;
        public const long DEVICE_UDI_UP_EVENT_ENABLED = 102588;
        public const long DEVICE_UDI_DOWN_EVENT_ENABLED = 102589;
        public const long DEVICE_UDI_TYPE = 102590;
        public const long DEVICE_UDI_WL_SLOT_LINKED = 102591;
        public const long DEVICE_UDI_BIT_OFFSET = 102592;
        public const long DEVICE_UDI_BYTE_OFFSET = 102593;
        public const long DEVICE_SACK_ITEM_SEND_DELAY = 102611;
        public const long DEVICE_SACK_MAX_LENGTH = 102612;
        public const long DEVICE_WMBUS_CONFIG_PASSWORD = 102613;
        public const long DEVICE_AT_COMMAND_CONTROL_BYTE = 102614;

        public const long DEVICE_CONVERSION_FACTOR = 102620;
        public const long GAS_PRESSURE_AVG = 102621;
        public const long GAS_TEMPERATURE_AVG = 102622;
        public const long DEVICE_SCHEDULE_READOUT_ENABLED = 102623;
        public const long CALORIFIC_VALUE = 102630;
        public const long DEVICE_COMPRESSIBILITY_FACTOR_RATIO_AT_MEASURMENT_CONDITIONS = 102631;
        public const long CONSUMPTION_DAILY_ACTUAL_AVG = 102634;

        public const long DEVICE_EVENT_TRANSMISSION_LINK_EVENT = 102638;
        public const long DEVICE_EVENT_TRANSMISSION_LINK_WAN2_CONFIRM_REQUEST = 102639;
        public const long DEVICE_FREQ_CONFIG_MULTIREAD_WINDOWS_COUNT = 102640;
        public const long DEVICE_FREQ_CONFIG_MULTIREAD_TIME_RANGE = 102641;

        public const long DEVICE_MODBUS_MASTER_ID_CONNECTION = 102644;
        public const long DEVICE_COMPRESSIBILITY_FACTOR_RATIO = 102646;
        public const long DEVICE_COMPRESSIBILITY_FACTOR_RATIO_AT_BASE_CONDITIONS = 102647;
        public const long DEVICE_CONVERSION_FACTOR_AVG = 102648;
        public const long DEVICE_COMPRESSIBILITY_FACTOR_RATIO_AVG = 102649;
        public const long ANALOG_NVR_LATCHED = 102650;
        public const long DEVICE_REGISTERS_MAP_VERSION = 102651;
        public const long DEVICE_SCHEDULE_READOUT_HOUR_START = 102652;
        public const long DEVICE_SCHEDULE_READOUT_HOUR_END = 102653;
        public const long WMBUS_SCAN_TOKEN = 102654;
        public const long GAS_PRESSURE_ABSOLUTE = 102659;
        public const long DEVICE_EVENT_RAPID_RISE_UP_ENABLE = 102663;
        public const long DEVICE_EVENT_RAPID_DROP_UP_ENABLE = 102664;
        public const long DEVICE_EVENT_RAPID_RISE_DOWN_ENABLE = 102665;
        public const long DEVICE_EVENT_RAPID_DROP_DOWN_ENABLE = 102666;
        public const long DEVICE_MODEM_CURRENT_QUERY_USED = 102667;
        public const long DEVICE_MODEM_CURRNET_BLOCKAGE_TIMEOUT = 102668;
        public const long DEVICE_CONFIRM_TIMEOUT = 102669;
        public const long DEVICE_CONFIRM_RETRIES_COUNT = 102670;
        public const long DEVICE_REMOTE_ANALOG_CONFIDENCE_BIT_LOGIC_ACTIVE = 102697;
        public const long WAN2_RECEIVED_VALID_PACKETS_WITH_RADIO_COUNT = 102701;
        public const long WAN2_RECEIVED_INVALID_PACKETS_WITH_RADIO_COUNT = 102702;
        public const long DEVICE_LCD_LATCH_WARNING_ICON_ENABLED = 102703;
        public const long DEVICE_DIAGNOSTICS_ARCHIVE_VALID_MARK = 102704;
        public const long UDI_READOUT_DATA_TYPE = 102705;
        public const long DEVICE_ANALOG_VALID_DURATION = 102706;
        public const long CONSUMPTION_MONTHLY_AVG = 102707;
        public const long GAS_DENSITY_RATIO = 102708;
        public const long DEVICE_LCD_ANALOG_INDEX_TO_SHOW = 102709;
        public const long DEVICE_LCD_SHOW_ANALOG_MEASUREMENT = 102710;
        public const long DEVICE_PREPAID_OFFSET_BEFORE_PUBLIC_HOLIDAY = 102711;
        public const long DEVICE_PREPAID_OFFSET_AFTER_PUBLIC_HOLIDAY = 102712;
        public const long DEVICE_PREPAID_MENU_VISIBILITY_VALVE_ATTEMPTS = 102713;
        public const long DEVICE_WMBUS_DESTINATION = 102716;
        public const long PREAPID_RESUME_TIME_ACTION = 102719;
        public const long PREAPID_RESUME_TIME_VALVE = 102720;
        public const long DEVICE_MBUS_BAUD_RATE = 102723;
        public const long DEVICE_MBUS_PRIMARY_ADDRESS = 102724;
        public const long DEVICE_MBUS_IDENTIFICATION_NUMBER = 102725;
        public const long DEVICE_MBUS_MANUFACTURER_ID = 102726;
        public const long DEVICE_MBUS_GENERATION = 102727;
        public const long DEVICE_MBUS_MEDIUM = 102728;
        public const long PREPAID_SEND_EXHAUSTED_TOPUP = 102733;
        public const long DEVICE_I2C_SENSOR_ADDRESS = 102734;
        public const long DEVICE_I2C_SENSOR_SLOPE = 102735;
        public const long DEVICE_I2C_SENSOR_SCAN_PERIOD = 102736;
        public const long DEVICE_MBUS_COUNTER_UNIT = 102747;
        public const long DEVICE_DIFF_PERIOD_VALUE = 102751;
        public const long DEVICE_DIFF_PERIOD_STEP = 102752;
        public const long DEVICE_DIFF_RESOLUTION = 102753;
        public const long DEVICE_DIFF_COUNT = 102754;

        public const long DEVICE_REGISTER_VALUE = 102757;
        public const long DEVICE_REGISTER_ADDRESS = 102758;
        public const long DEVICE_REGISTER_EXTENDED = 102759;
        public const long DEVICE_REGISTER_ENABLED = 102760;
        public const long DEVICE_RADIO_CHANNEL = 102763;
        public const long ACCOUNT_VALIDITY_DATETIME = 102764;
        public const long ACCOUNT_INFINITE_VALIDITY = 102769;
        public const long DEVICE_BATTERY_RADIO_FRAME_ENERGY = 102792;
        public const long DEVICE_BATTERY_CL_READOUT_ENERGY = 102770;
        public const long DEVICE_BATTERY_MAGNETO_CURRENT = 102771;
        public const long DEVICE_BATTERY_I2C_READOUT_ENERGY = 102772;
        public const long DEVICE_ACQUISITION_PERIOD_ON_RAPID = 102773;
        public const long DEVICE_CL_AVERAGING_ENABLED = 102774;
        public const long DEVICE_CL_NORMALIZATION_ENABLED = 102775;
        public const long DEVICE_CL_LINEAR_ENABLED = 102776;
        public const long DEVICE_CL_POLYLINE_ENABLED = 102777;
        public const long DEVICE_RADIO_433_FRAME_PERIOD_ON_REFUEL = 102778;
        public const long DEVICE_MAGNETO_REFUEL_DETECT_ENABLE = 102793;
        public const long DEVICE_MAGNETO_REFUEL_DETECT_LEVEL = 102794;
        public const long DEVICE_RADIO_433_ARCHIVE_FRAME_SAVE_SELECTOR = 102798;
        public const long DEVICE_MBUS_RESPONSE_DELAY = 102800;
        public const long DEVICE_MBUS_SLOT_COUNT = 102801;
        public const long DEVICE_MBUS_SLOT_SIZE = 102803;
        public const long DEVICE_ACQUISITION_RAPID_DETECT_LEVEL = 102806;
        public const long DEVICE_CL_RAPID_DETECT_ENABLED = 102807;
        public const long DEVICE_CL_RAPID_FUNCTION_TYPE = 102808;
        public const long DEVICE_RADIO_433_FRAME_DEFINITION = 102816;
        public const long DEVICE_UDI_PERIODICAL_LATCH_CLEAR = 102817;
        public const long DEVICE_UDI_TIME_TO_LATCH_CLEAR = 102818;
        public const long DEVICE_BATTERY_RADIO_BYTE_ENERGY = 102821;  //RW
        public const long DEVICE_BATTERY_CL_MAX_CURRENT = 102822;  // RW             [mA]

        public const long DEVICE_FREQ_CONFIG_COUNTER_DATA = 102824;
        public const long DEVICE_COUNTER_SCALER = 102825;

        public const long DEVICE_LOWER_PRESSURE_WARNING_LIMIT = 102833;
        public const long DEVICE_CURRENT_STATUS = 102834;
        public const long DEVICE_I2C_SENSOR_AUTO_CONFIGURE_ON_IMR_TEST = 102841;
        public const long DEVICE_RADIO_MODULE_CRC8_PRESENT = 102842;
        public const long DEVICE_REMOTE_ANALOG_BYTES_COUNT = 102843;
        public const long DEVICE_REMOTE_ANALOG_IS_FLOAT_VALUE = 102844;
        public const long DEVICE_REMOTE_ANALOG_FLOAT_SLOPE = 102845;

        public const long DEVICE_ANALOG_CONFIG_HIHI_UP_EVENT_ENABLE = 102846;
        public const long DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE = 102847;
        public const long DEVICE_ANALOG_CONFIG_HI_UP_EVENT_ENABLE = 102848;
        public const long DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE = 102849;
        public const long DEVICE_ANALOG_CONFIG_LO_UP_EVENT_ENABLE = 102850;
        public const long DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE = 102851;
        public const long DEVICE_ANALOG_CONFIG_LOLO_UP_EVENT_ENABLE = 102852;
        public const long DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE = 102853;
        public const long DEVICE_ANALOG_CONFIG_STEPREL_EMERGENCY_EVENT_ENABLE = 102854;

        public const long DEVICE_SABOTAGE_COUNTER_ENABLED = 102871;
        public const long DEVICE_COVER_OPEN_DELAY = 102872;
        public const long DEVICE_REMOVE_DELAY = 102873;
        public const long DEVICE_SABOTAGE_DELAY = 102874;
        public const long DEVICE_MAGNETO_DATA_VALIDITY_PERIOD = 102877;

        public const long DEVICE_FREQ_CONFIG_VOLUME_CORRECTOR_DAY_DATA = 102890;
        public const long DEVICE_FREQ_CONFIG_VOLUME_CORRECTOR_HOUR_DATA = 102891;
        public const long DEVICE_VOLUME_CORRECTOR_ENABLED = 102892;
        public const long DEVICE_MODBUS_ADDRESS = 102893;
        public const long DEVICE_DAY_REGISTER_ADDRESS = 102894;
        public const long DEVICE_DAY_REGISTER_COUNT = 102895;
        public const long DEVICE_HOUR_REGISTER_ADDRESS = 102896;
        public const long DEVICE_HOUR_REGISTER_COUNT = 102897;
        public const long DEVICE_VOLUME_CORRECTOR_RESPONSE_TIMEOUT = 102898;
        public const long DEVICE_VOLUME_CORRECTOR_RESEND_COUNT = 102899;
        public const long MODBUS_FRAME = 102900;
        public const long DEVICE_STATUS_REGISTER_COUNT = 102901;
        public const long DEVICE_STATUS_REGISTER_ADDRESS = 102902;
        public const long DEVICE_PIN_REGISTER_ADDRESS = 102903;
        public const long DEVICE_VOLUME_CORRECTOR_PIN = 102904;
        public const long DEVICE_FREQ_CONFIG_VOLUME_CORRECTOR_STATUS_DATA = 102905;
        public const long DEVICE_VOLUME_CORRECTOR_PRE_PIN_ENABLED = 102906;
        public const long DEVICE_VOLUME_CORRECTOR_DAY_REQUEST_ENABLED = 102907;
        public const long DEVICE_VOLUME_CORRECTOR_HOUR_REQUEST_ENABLED = 102908;
        public const long DEVICE_VOLUME_CORRECTOR_STATUS_REQUEST_ENABLED = 102909;

        public const long DEVICE_TEST_GSM_AFTER_GSM_REGISTRATION_FAILURE_ENABLED = 102918;
        public const long DEVICE_GSM_REGISTRATION_FAILURES_TRESHOLD = 102919;
        public const long DEVICE_MODEM_SHUTDOWN_AFTER_FREQ = 102934;

        public const long DEVICE_VOLUME_CORRECTOR_ALARM_ENABLED = 102935;
        public const long DEVICE_VOLUME_CORRECTOR_WARNING_ENABLED = 102936;
        public const long DEVICE_VOLUME_CORRECTOR_SYNCHRONIZE_TIME_ENABLED = 102937;
        public const long DEVICE_TIME_REGISTER_ADDRESS = 102938;
        public const long DEVICE_DELAY_AFTER_POWER_ON = 102939;

        public const long DEVICE_DRIVER_APULSE_X373_XXA4_SPECIAL_MODE = 102940;
        public const long DEVICE_DRIVER_PACKET_ID_PROCESS = 102942;

        public const long DEVICE_SMS_SENT_COUNT_PER_WAKEUP = 102944;
        public const long SIM_READY = 102945;
        public const long MODEM_IS_ACTIVE = 102946;
        public const long MODEM_IS_LOGGED = 102947;
        public const long DEVICE_RADIO_ACTIVE = 102948;
        public const long DEVICE_GPRS_CONNECTED = 102949;
        public const long DEVICE_MODEM_OPERATOR_CHANGE_COUNT = 102950;
        public const long DEVICE_RADIO_ACTIVED_FROM_SCHEDULE = 102951;
        public const long DEVICE_FTP_CONNECTED = 102952;
        public const long DEVICE_MODEM_FORCED_SHUTDOWN_DELAY = 102953;
        public const long MODEM_LAST_WAKEUP_TIME = 102954;
        public const long MODEM_LAST_LOGGING_DURATION = 102955;

        public const long DEVICE_RETRY_SEND_PACKET = 102964;
        public const long DEVICE_RETRY_SEND_PACKET_DURATION = 102965;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_WRITE_INDEX = 102966;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_READ_INDEX = 102967;
        public const long DEVICE_PACKET_ARCHIVE_INDEX_START_FOR_ARCHIVE_COMMAND = 102968;
        public const long DEVICE_PACKET_ARCHIVE_INDEX_END_FOR_ARCHIVE_COMMAND = 102969;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_SEND_RETRY = 102970;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_PACKET_TOTAL_PARTS = 102971;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_PACKET_PART = 102972;
        public const long DEVICE_PACKET_ARCHIVE_CURRENT_PACKET_PART_ID = 102973;
        public const long DEVICE_PACKET_ARCHIVE_START_TIME = 102974;
        public const long DEVICE_PACKET_ARCHIVE_END_TIME = 102975;
        public const long DEVICE_TEST_GSM_GPRS_TEST_RUNNING = 102976;

        public const long DEVICE_SACK_RADIO_ADDRESS_FILTER_MASK = 102977;
        public const long DEVICE_RTRS_PREPARE_PACKETS_OFFLINE = 102978;
        public const long DEVICE_MODEM_TCP_ENABLED = 102979;
        public const long ANALOG_LAST_VALID_VALUE = 102980;
        public const long DEVICE_MODEM_REQUEST_READ_PHONE_NUMBER_FROM_SIM = 102981;
        public const long DEVICE_FREQ_CONFIG_ANALOG_SUMMARY_DATA = 102982;

        public const long DEVICE_INCOMING_PORT = 102983;
        public const long DEVICE_SEND_SIM_CARD_CHANGE_INFO = 102984;

        public const long DEVICE_SIM_CARD_CHANGED = 102985;
        public const long DEVICE_SIM_CARD_IP = 102986;
        public const long DEVICE_MODEM_FORCED_SHUTDOWN_ENABLED = 102987;

        public const long DEVICE_IWD_CONFIRMATION_ENABLED = 102988;
        public const long DEVICE_IWD_PACKET_CONFIRM_RETRIES_COUNT = 102989;
        public const long DEVICE_IWD_PACKET_CONFIRM_DIVIDER = 102990;
        public const long DEVICE_IWD_SERVER_CONFIRM_TIMEOUT = 102991;

        public const long DEVICE_SITA_PHONE_NUMBER = 102995;
        public const long DEVICE_FACTORY_ID = 102996;

        public const long DEVICE_OMB_NUMBER = 103100;
        public const long DEVICE_GET_CUSTOM_OMB_TRANSMISSION_LINK = 103101;
        public const long DEVICE_GET_CUSTOM_OMB_WAKEUP_MODEM = 103102;
        public const long DEVICE_GET_CUSTOM_OMB_EXTEND_MODEM_DURATION = 103103;
        public const long DEVICE_REMOTE_I2C_PRESSURE_SENSOR_PRESENT = 103104;
        public const long DEVICE_AT_COMMAND_TIMEOUT = 103105;
        public const long DEVICE_AT_COMMAND_COMPLETED = 103106;
        public const long DEVICE_PROBE_PRESSURE_MIN_VALID = 103107;
        public const long DEVICE_PROBE_PRESSURE_MAX_VALID = 103108;
        public const long I2C_PROBE_ANALOG_ATTACHED = 103109;
        public const long I2C_PROBE_ANALOG_ATTACHED_NUMBER = 103110;
        public const long DEVICE_MODEM_EMERGENCY_WAKEUP_DELAY = 103111;

        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_ANALOG_MASK = 103112;
        public const long DEVICE_I2C_SENSOR_EXTREME_VALUES_CUT_COUNT = 103113;
        public const long DEVICE_I2C_SENSOR_MEASURES_COUNT = 103114;
        public const long DEVICE_I2C_SENSOR_MEASUREMENT_DELAY = 103115;
        public const long DEVICE_MODEM_TYPE_NAME = 103116;
        public const long DEVICE_FTP_FILE_OPENED = 103117;
        public const long DEVICE_MODEM_SHUTDOWN_DELAY = 103118;
        public const long MODEM_FIRMWARE_VERSION = 103119;

        public const long ANALOG_VALIDITY_SUSPENDED = 103120;
        public const long ALEVEL_CONFIG_AVG_BASED_EVENTS_ENABLE = 103121;
        public const long ANALOG_VALUE_VALID = 103130;
        public const long DEVICE_ANALOG_SOURCE = 103131;
        public const long ANALOG_CONFIG_VALIDITY_SUSPENDED = 103132;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_VALID_MARK = 103133;
        public const long DEVICE_SACK_RTRS_DATA = 103134;

        public const long DEVICE_BATTERY_KELLER_RS485_READOUT_ENERGY = 103135;
        public const long DEVICE_KELLER_RS485_READOUT_COUNT = 103136;
        public const long DEVICE_ARCHIVE_SENT_FRAMES_VALID_MARK = 103137;
        public const long DEVICE_KELLER_RS485_SENSOR_ADDRESS = 103138;
        public const long DEVICE_KELLER_RS485_SENSOR_IS_DOUBLE = 103139;
        public const long DEVICE_KELLER_RS485_SENSOR_QUERY_RETRIES = 103140;
        public const long DEVICE_KELLER_RS485_SENSOR_QUERY_TIMEOUT = 103141;
        public const long DEVICE_KELLER_RS485_READOUT_PERIOD = 103142;
        public const long DEVICE_KELLER_RS485_TRANSMITTER_POWER_UP_MODE_ENABLED = 103147;
        public const long DEVICE_KELLER_RS485_CURRENT_ADDRESS = 103156;
        public const long DEVICE_KELLER_RS485_NEW_ADDRESS = 103157;
        public const long DEVICE_SCHEDULE_READOUT_LOCAL_TIME = 103189;
        public const long DEVICE_SCHEDULE_COMMAND = 103190;
        public const long DEVICE_OBJECT_READ_ACCESS_LEVEL = 103194;
        public const long DEVICE_OBJECT_WRITE_ACCESS_LEVEL = 103192;
        public const long DEVICE_OUTPUT_PIN_E3 = 103195;
        public const long DEVICE_CC1120_BUSTER_ENABLED = 103196;

        public const long DEVICE_FREQ_CONFIG_FULL_DATA_ON_EVENTS = 103197;
        public const long DEVICE_I2C_SENSOR_VALUE_OFFSET = 103198;

        public const long DEVICE_TYPE_NAME = 103216;
        public const long DEVICE_STANDARD_PARAMETER_LIST_VERSION = 103217;
        public const long DEVICE_TIME_ZONE_OFFSET_WINTER = 103218;
        public const long DEVICE_TIME_ZONE_OFFSET_SUMMER = 103219;
        public const long CLOCK_SYNCHRONISATION_FROM_GSM = 103220;
        public const long REMOVE_COUNTER_VOLUME = 103221;
        public const long REMOVE_COUNTER_ENERGY = 103222;
        public const long COVER_OPEN_COUNTER_VOLUME = 103223;
        public const long COVER_OPEN_COUNTER_ENERGY = 103224;
        public const long SABOTAGE_COUNTER_VOLUME = 103225;
        public const long SABOTAGE_COUNTER_ENERGY = 103226;
        public const long DEVICE_BATTERY_PERCENTAGE_LEVEL = 103227;
        public const long DEVICE_EVENT_UDP_PORT = 103228;
        public const long DEVICE_EVENT_TCP_PORT = 103229;
        public const long DEVICE_MONTHLY_DATA_LATCH_DAY = 103230;
        public const long DEVICE_MONTHLY_AND_DAILY_DATA_LATCH_HOUR = 103231;
        public const long DEVICE_MONTHLY_AND_DAILY_DATA_LATCH_LOCAL_TIME = 103232;

        public const long DEVICE_UP_EVENT_MAX_HOUR_FLOW = 103259;
        public const long DEVICE_UP_EVENT_MAX_TEMPORAL_FLOW = 103260;
        public const long DEVICE_UP_EVENT_LOW_VOLTAGE_DETECTED = 103261;
        public const long DEVICE_UP_EVENT_BUTTON = 103262;
        public const long DEVICE_UP_EVENT_LARGE_CLOCK_SET = 103263;
        public const long DEVICE_UP_EVENT_VOLUME_SET = 103264;
        public const long DEVICE_UP_EVENT_ENERGY_SET = 103265;
        public const long DEVICE_UP_EVENT_INSTALLATION = 103266;
        public const long DEVICE_UP_EVENT_POWER_STATE = 103267;
        public const long DEVICE_DOWN_EVENT_MAX_HOUR_FLOW = 103268;
        public const long DEVICE_DOWN_EVENT_MAX_TEMPORAL_FLOW = 103269;
        public const long DEVICE_DOWN_EVENT_LOW_VOLTAGE_DETECTED = 103270;
        public const long DEVICE_DOWN_EVENT_BUTTON = 103271;
        public const long DEVICE_DOWN_EVENT_LARGE_CLOCK_SET = 103272;
        public const long DEVICE_DOWN_EVENT_VOLUME_SET = 103273;
        public const long DEVICE_DOWN_EVENT_ENERGY_SET = 103274;
        public const long DEVICE_DOWN_EVENT_POWER_STATE = 103275;
        public const long DEVICE_DOWN_EVENT_INSTALLATION = 103276;

        public const long DEVICE_LAST_LEVEL_MEASURE_TIME_BEFORE_REFUEL = 103279;

        public const long DEVICE_STATION_FRAME_USART_I2C_MODE = 103286;
        public const long DEVICE_STATION_FRAME_ENABLED = 103282;
        public const long DEVICE_STATION_FRAME_ACK_COUNT = 103283;
        public const long DEVICE_STATION_FRAME_NACK_TO_EVENT = 103284;
        public const long DEVICE_STATION_FRAME_TIMEOUT_TIMER = 103285;
        public const long DEVICE_WL_TANKING_WINDOW_WIDTH = 103291;
        public const long DEVICE_WL_TANKING_TIME_TO_SCAN = 103293;
        public const long DEVICE_WL_TANKING_ENABLED = 103287;
        public const long DEVICE_WL_RETRANSMITTED_FRAMES_ONLY = 103288;
        public const long DEVICE_TANKING_WINDOW = 103289;
        public const long DEVICE_TANK_VOLUME = 103290;
        public const long DEVICE_STATION_FRAME_RADIO_TRANSMISSION_ENABLED = 103292;

        public const long DEVICE_COVER_OPEN_INPUT_STATE = 103294;
        public const long DEVICE_REMOVE_INPUT_STATE = 103295;
        public const long DEVICE_SABOTAGE_CONTACT_INPUT_STATE = 103296;

        public const long DEVICE_VALDRIVER_LEAK_TEST_VOLUME = 103297;
        public const long DEVICE_PREPAID_TOPUP_END_TIME = 103298;
        public const long DEVICE_PREPAID_TOPUP_END_VOLUME = 103299;
        public const long DEVICE_PREPAID_TOPUP_END_ENERGY = 103300;
        public const long DEVICE_PREPAID_VALVE_CONTROL = 103301;
        public const long DEVICE_WPP_ACCESS_CONTROL_OBLIGATORY = 103302;
        public const long DEVICE_WPP_CRYPTOGRAPHY_OBLIGATORY = 103303;
        public const long DEVICE_TEMPERATURE_CALIBRATION_VALUE = 103304;
        public const long DEVICE_DIRECT_MEMORY_ALLOW_WRITE = 103305;
        public const long DEVICE_DIRECT_MEMORY_WRITE_SIZE = 103307;
        public const long DEVICE_WDP_IV_IN = 103308;

        public const long DEVICE_UP_EVENT_VALVE_OPEN_ACTIVATION = 103323;
        public const long DEVICE_UP_EVENT_VALVE_CLOSE_FAILED = 103324;
        public const long DEVICE_UP_EVENT_TOPUP_END = 103325;
        public const long DEVICE_DOWN_EVENT_VALVE_OPEN_ACTIVATION = 103326;
        public const long DEVICE_DOWN_EVENT_VALVE_CLOSE_FAILED = 103327;
        public const long DEVICE_DOWN_EVENT_TOPUP_END = 103328;

        public const long DEVICE_BATTERY_OPTO_CURRENT = 103330;
        public const long DEVICE_BATTERY_VALVE_CURRENT = 103331;
        public const long DEVICE_SELF_QUERY_ENCRYPTION_ENABLED = 103332;
        public const long DEVICE_VOLUME_OFFSET = 103333;
        public const long DEVICE_WDP_ARP = 103334;
        public const long DEVICE_LOG_SAVE_MODE = 103335;
        public const long DEVICE_UDI_TAMPER_SABOTAGE_SLOT_NUMBER = 103336;
        public const long DEVICE_UDI_TAMPER_REMOVE_SLOT_NUMBER = 103337;
        public const long DEVICE_UDI_TAMPER_OPEN_SLOT_NUMBER = 103338;
        public const long DEVICE_OPTO_ACTIVATION_ON_INIT = 103339;
        public const long DEVICE_OPTO_ACTIVATION_ON_DATA_RCV = 103340;
        public const long DEVICE_OPTO_ACTIVATION_ON_MENU_ACTION = 103341;
        public const long DEVICE_OPTO_DATAGRAM_INTERVAL = 103342;

        public const long DEVICE_STATION_FRAME_VALIDITY_TIME = 103350;

        public const long VOLUME_CORRECTED = 103400;
        public const long VOLUME_MEASURED_DURING_ERROR = 103401;
        public const long FLOW_MEASURED = 103402;
        public const long DAILY_MAXIMUM_TEMPORARY_FLOW_CORRECTED = 103403;
        public const long DEVICE_CURRENT_EVENT_INDEX = 103404;
        public const long DEVICE_EVENTS_QUEUE_COUNT = 103405;
        public const long METER_STATUS_EXTENDED_DESCRIPTION = 103406;
        public const long CARBON_MONOXIDE_CONTENT = 103407;
        public const long METHANE_CONTENT = 103408;
        public const long DEVICE_LCD_HIGH_BRIGHTNEST_DURATION = 103409;

        public const long ETHANE_CONTENT = 103410;
        public const long PROPANE_CONTENT = 103411;
        public const long HYDROGEN_SULFIDE_CONTENT = 103412;
        public const long WATER_CONTENT = 103413;
        public const long OXYGENE_CONTENT = 103414;
        public const long ISO_BUTANE_CONTENT = 103415;
        public const long N_BUTANE_CONTENT = 103416;
        public const long ISO_PENTANE_CONTENT = 103417;
        public const long N_PENTANE_CONTENT = 103418;
        public const long N_HEXANE_CONTENT = 103419;
        public const long N_HEPTANE_CONTENT = 103420;
        public const long N_OCTANE_CONTENT = 103421;
        public const long N_NONANE_CONTENT = 103422;
        public const long N_DECANE_CONTENT = 103423;
        public const long HELIUM_CONTENT = 103424;
        public const long ARGON_CONTENT = 103425;
        public const long DEVICE_ERROR_MAIN_POWER_DISABLED = 103426;
        public const long DEVICE_ERROR_EVENT_LOG_90_PERCENT_FULL = 103427;
        public const long DEVICE_ERROR_GENERAL = 103428;
        public const long DEVICE_ERROR_VOLUME_CORRECTOR_DISCONNECTED = 103429;
        public const long DEVICE_ERROR_EVENT_LOG_FULL = 103430;
        public const long DEVICE_ERROR_CLOCK_NOT_SYNCHRONIZED = 103431;
        public const long DEVICE_ERROR_GAS_TEMPERATURE_OUT_OF_RANGE = 103432;
        public const long DEVICE_ERROR_GAS_PRESSURE_OUT_OF_RANGE = 103433;
        public const long DEVICE_ERROR_VALVE_OPENED_LEAKAGE = 103434;
        public const long DEVICE_BATTERY_REMAINING_DAYS_MEASUREMENT_MODULE = 103435;
        public const long DEVICE_BATTERY_REMAINING_DAYS_CONVERSION_MODULE = 103436;
        public const long DEVICE_BATTERY_REMAINING_DAYS_DATA_RECORDING_MODULE = 103437;
        public const long DEVICE_BATTERY_REMAINING_DAYS_COMMUNICATION_MODULE = 103438;

        public const long DEVICE_GSM_BAND_900_POWER_LIMIT = 103439;
        public const long DEVICE_GSM_BAND_1800_POWER_LIMIT = 103440;
        public const long DEVICE_CONFIRM_WAKEUP_RETRIES_COUNT = 103441;
        public const long DEVICE_EXTENDED_MODEM_LOGS_ENABLED = 103442;
        public const long DEVICE_CONFIRM_ENABLED = 103443;
        public const long DEVICE_GPRS_SMS_LINK_ENABLED = 103444;
        public const long DEVICE_PACKET_CONFIRM_DIVIDER = 103445;
        public const long FUEL_LEVEL_AGGREGATED_ENHANCED = 103446;
        public const long DEVICE_TEMPERATURE_DISPLAY_TYPE = 103452;
        public const long DEVICE_BATTERY_NFC_CURRENT = 103453;
        public const long PREPAID_LOG_EVENT_TYPE = 103454;
        public const long DEVICE_LCD_CURRENCY_CODE = 103455;

        public const long DEVICE_SCHEDULE_ACTIVATION_MODE = 103456;
        public const long DEVICE_SCHEDULE_ACTIVATION_DELAY = 103457;
        public const long DEVICE_SCHEDULE_RECONNECT_RETRIES = 103458;
        public const long DEVICE_GENIUS_SLIM_IS_LOGGED = 103459;
        public const long DEVICE_NFC_USE_DEFAULT_URL = 103461;
        public const long DEVICE_FIRMWARE_FLASH_FTP_MODE = 103462;
        public const long DEVICE_FTP_SERVER_MODE = 103463;
        public const long DEVICE_NFC_DEFAULT_URL = 103464;

        public const long DEVICE_EVENT_FREQ_EXECUTION_DELAY = 103465;
        public const long DEVICE_PREDEFINED_GSM_SCAN_OPERATOR = 103467;
        public const long DEVICE_OVERWRITE_RESPONSE_LINK = 103469;
        public const long DEVICE_SMS_CONFIRM_ENABLED = 103470;
        public const long DEVICE_LCD_PULSE_ON_DURATION = 103471;
        public const long DEVICE_LCD_HIGHDRIVE = 103472;
        public const long DEVICE_LCD_DEADTIME = 103473;
        public const long DEVICE_LCD_CONTRAST = 103474;
        public const long DEVICE_LCD_SEGMENT_TIMEOUT = 103475;
        public const long DEVICE_LCD_SEGMENT_MASK = 103476;
        public const long DEVICE_NFC_URL_WRITE_TIMEOUT = 103503;
        public const long DEVICE_TANK_DIAMETER = 103504;
        public const long DEVICE_VOLUME_CORRECTOR_ARCHIVE_DATA_VALID_MARK = 103552;
        public const long DEVICE_VOLUME_CORRECTOR_PIN_CHECK_STATUS_ENABLED = 103553;
        public const long DEVICE_VOLUME_CORRECTOR_DEVICE_TYPE = 103554;
        public const long DEVICE_VOLUME_CORRECTOR_MEMORY_MAP_VERSION = 103508;
        public const long DEVICE_ALARM_STATUS_MASK = 103509;
        public const long DEVICE_READOUT_REGISTER_ADDRESS = 103510;
        public const long DEVICE_READOUT_REGISTER_COUNT = 103511;
        public const long DEVICE_VOLUME_CORRECTOR_DATA_TIMESTAMP = 103512;
        public const long DEVICE_VOLUME_CORRECTOR_VALUE_VALID = 103513;
        public const long MODBUS_READOUT_FRAME = 103514;
        public const long MODBUS_STATUS_FRAME = 103515;
        public const long MODBUS_TASK_FRAME = 103516;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_SEND_IMMEDIATELY = 103517;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_EXECUTED = 103518;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_TOO_LARGE_DATA = 103519;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_TIMEOUT = 103520;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_INDEX = 103521;
        public const long DEVICE_VOLUME_CORRECTOR_TASK_LENGTH = 103522;

        public const long DEVICE_LINK_IMRCABLE_SPEED = 103523;
        public const long DEVICE_DEFAULT_POWER_OUTPUT_STATE = 103524;
        public const long DEVICE_FRAME_RECEIVED_POWER_OUTPUT_STATE = 103525;
        public const long DEVICE_OUTGOING_SMS_ENABLED = 103526;
        public const long DEVICE_OUTGOING_UDP_ENABLED = 103527;
        public const long DEVICE_OUTGOING_TCP_ENABLED = 103528;
        public const long DEVICE_OUTGOING_FTP_ENABLED = 103529;
        public const long DEVICE_RESTRICTED_ACCESS_ADDRESS = 103530;

        public const long BASE_PRESSURE = 103531;
        public const long PRESSURE_EQUIVALENT = 103532;
        public const long BASE_TEMPERATURE = 103533;
        public const long DEVICE_WDP_SMS_ARP = 103534;
        public const long DEVICE_RETRY_SEND_DELAY = 103547;
        public const long DEVICE_TEST_GSM_GPRS_TEST_BODY = 103548;
        public const long DEVICE_COUNTERS_REGISTRATION_PERIOD = 103549;
        public const long DEVICE_SMS_CONFIRM_TIMEOUT = 103550;
        public const long DEVICE_VOLUME_CORRECTOR_RS485_ENABLED = 103551;

        public const long DEVICE_BATTERY_REMAINING_CAPACITY = 103562;
        public const long DEVICE_BATTERY_RUN_CURRENT = 103563;
        public const long DEVICE_ARCHIVE_REGISTER_ADDRESS = 103568;
        public const long DEVICE_ARCHIVE_REGISTER_COUNT = 103569;
        public const long DEVICE_IV_VERIFICATION_ENABLED = 103570;
        public const long DEVICE_LOG_COMMAND_ENABLE = 103571;
        public const long DEVICE_VOLUME_CORRECTOR_IDOM_PROTOCOL_ENABLED = 103573;
        public const long DEVICE_VOLUME_CORRECTOR_SEND_MODBUS_STATUS_ON_EVENT = 103574;
        public const long DEVICE_VOLUME_CORRECTOR_EVENT_PARITY_BIT = 103575;
        public const long DEVICE_VOLUME_CORRECTOR_SEND_IDOM_STATUS_ON_EVENT = 103576;
        public const long DEVICE_VOLUME_CORRECTOR_WINDOWS_EXTENTION_COUNT = 103577;
        public const long DEVICE_VOLUME_CORRECTOR_IDOM_OPEN_WINDOW_TIME = 103578;
        public const long DEVICE_VOLUME_CORRECTOR_IDOM_OPEN_WINDOW_TIMEOUT = 103579;
        public const long DEVICE_COUNTER_COEFFICIENT = 103583;
        public const long DEVICE_ARCHIVE_ITEMS_COUNT = 103585;
        public const long DEVICE_STATUS_MAX_ITEMS = 103586;
        public const long DEVICE_VOLUME_CORRECTOR_STATUS_FROM_LOG_ENABLED = 103587;
        public const long DEVICE_SYNCHRONIZE_LOCAL_TIME = 103588;
        public const long DEVICE_RTC_OFFSET = 103589;
        public const long DEVICE_DOWN_EVENT_RADIO = 103602;
        public const long DEVICE_UP_EVENT_RADIO = 103603;
        public const long DEVICE_VOLUME_CORRECTOR_LEARNING_TO_DEAD_COUNT = 103607;
        public const long DEVICE_VOLUME_CORRECTOR_TIME_DELTA = 103608;
        public const long DEVICE_VOLUME_CORRECTOR_SEND_MODBUS_ARCHIVE_ON_EVENT = 103609;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_EVENT_UP_ENABLED = 103610;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_EVENT_DOWN_ENABLED = 103611;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_DEAD_UP_ENABLED = 103612;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_DEAD_DOWN_ENABLED = 103613;
        public const long DEVICE_VOLUME_CORRECTOR_SEND_IDOM_DATA_ON_EVENT = 103614;
        public const long DEVICE_VOLUME_CORRECTOR_SEND_IDOM_ARCHIVE_ON_EVENT = 103615;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_STATE = 103616;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_DEAD_STATE = 103617;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_STATE_LATCHED = 103618;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_DEAD_STATE_LATCHED = 103619;
        public const long DEVICE_VOLUME_CORRECTOR_DAILY_ARCHIVE_DATA_VALID_MARK = 103620;
        public const long DEVICE_VOLUME_CORRECTOR_HOURLY_ARCHIVE_DATA_VALID_MARK = 103621;
        public const long DEVICE_VOLUME_CORRECTOR_STATUS_ARCHIVE_DATA_VALID_MARK = 103622;
        public const long DEVICE_VOLUME_CORRECTOR_IDOM_ARCHIVE_DATA_VALID_MARK = 103623;
        public const long DEVICE_HOUR_ITEMS_COUNT = 103624;
        public const long MODBUS_DAY_FRAME = 103625;
        public const long MODBUS_HOUR_FRAME = 103626;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_UP = 103627;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_LOST_DOWN = 103628;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_DEAD_UP = 103629;
        public const long DEVICE_VOLUME_CORRECTOR_LINK_DEAD_DOWN = 103630;
        public const long RESET_POR_COUNTER = 103631;
        public const long RESET_SFT_COUNTER = 103632;
        public const long ANALOG_SEND_READOUTS_TO_SLOT = 103633;
        public const long DEVICE_VOLUME_CORRECTOR_RS485_BAUD_RATE = 103634;
        public const long DEVICE_VOLUME_CORRECTOR_RS485_TRANSMISSION_MODE = 103635;
        public const long MEASURE_POINT_NAME = 103637;
        public const long DEVICE_PRESSURE_PROBE_KIND = 103638;
        public const long DEVICE_UP_EVENT_SERVICE_COUNT_MAX = 103642;
        public const long DEVICE_DOWN_EVENT_SERVICE_COUNT_MAX = 103643;
        public const long DEVICE_ANALOG_VALUE_OFFSET = 103644;
        public const long UDI_SEND_READOUTS_TO_SLOT = 103645;
        public const long DEVICE_DAYLIGHT_SAVING_TIME_ZONE_ID = 103646;
        public const long DEVICE_BATTERY_EVC_CURRENT = 103647;
        public const long DEVICE_RTC_CALIBRATION_REGISTER = 103666;
        public const long DEVICE_RADIO_DEFAULT_PROTOCOL_ID = 103668;
        public const long DEVICE_BUTTON_ENABLED = 103670;
        public const long WMBUS_SELF_FRAME_ATTACHMENT = 103671;
        public const long DEVICE_WDP_MR = 103672;
        public const long METER_STATUS_EVENT_START = 103673;
        public const long METER_STATUS_EVENT_END = 103674;
        public const long CLOCK_SYNCHRONISATION_SETTINGS = 103675;
        public const long DEVICE_WMBUS_RTRS_LINK = 103676;

        public const long DEVICE_CURRENT_ERROR = 103681;
        public const long DEVICE_PARAMETERS_CRC_CHECKSUM = 103682;
        public const long DEVICE_PRESSURE_PROBE_SN = 103683;
        public const long DEVICE_TEMPERATURE_PROBE_SN = 103684;
        public const long DEVICE_GAS_METER_SN = 103685;
        public const long DEVICE_ARCHIVE_DATA_PERIOD = 103686;
        public const long BAROMETRIC_PRESSURE = 103687;
        public const long DEVICE_SEA_LEVEL_ALTITUDE = 103688;
        public const long DEVICE_LATCHED_STATUS = 103689;

        public const long DEVICE_SACK_UNIQUE_CI_SEND_ALL = 103690;
        public const long DEVICE_SACK_UNIQUE_CI_SEND_NONE = 103691;
        public const long DEVICE_VALVE_TYPE = 103695;
        public const long DEVICE_WMBUS_SEND_TO_SACK = 103698;

        public const long DEVICE_REGULATOR_MODE = 103701;
        public const long DEVICE_REGULATOR_ADDRESS = 103702;
        public const long DEVICE_VOLUME_CORRECTOR_MODE = 103709;


        public const long TEMPERATURE_OUTSIDE = 103716;
        public const long TEMPERATURE_CENTRAL_HEATING_IN = 103717;
        public const long TEMPERATURE_CENTRAL_HEATING_SET = 103718;
        public const long TEMPERATURE_CENTRAL_HEATING_OUT = 103719;
        public const long TEMPEARTURE_DOMESTIC_HOT_WATER_IN = 103720;
        public const long TEMPERATURE_OUTSIDE_CHANGE_DELAYED = 103721;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_OUT = 103722;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_SET = 103723;
        public const long TEMPERATURE_OUTSIDE_DELAYED = 103724;
        public const long TEMPERATURE_STORAGE_CONTAINER_1 = 103725;
        public const long TEMPERATURE_STORAGE_CONTAINER_2 = 103726;
        public const long HEATING_CURVE_SLOPE = 103727;
        public const long HEATING_CURVE_BIAS = 103728;
        public const long TEMPERATURE_CENTRAL_HEATING_MAX = 103729;
        public const long TEMPERATURE_CENTRAL_HEATING_MIN = 103730;
        public const long PRESSURE_IN = 103731;
        public const long PRESSURE_OUT = 103732;
        public const long PRESSURE_DIFFERENCE = 103733;
        public const long DEVICE_MANUAL_VALVE_OPEN_SET_RATE = 103734;
        public const long DEVICE_VALVE_OPEN_RATE = 103735;
        public const long DEVICE_VALVE_CLOSE_RATE = 103736;
        public const long DEVICE_REGULATOR_WORKING_MODE = 103737;
        public const long DEVICE_PUMP_WORKING_MODE = 103738;
        public const long DEVICE_STATIONARY_FRAME_MENU_REQUEST_ENABLED = 103739;
        public const long DEVICE_CODE = 100840;
        public const long DEVICE_VOLUME_CORECTOR_CODE_REGISTER_ADDRESS = 103740;
        public const long DEVICE_VOLUME_CORECTOR_CODE = 103741;
        public const long DEVICE_WMBUS_DEFAULT_INTERFACE = 103742;
        public const long DEVICE_SLAVE_ADDRESS = 103743;
        public const long DEVICE_COUNTER_SOURCE = 103744;
        public const long DEVICE_COUNTER_COUNTING_ACTIVE = 103745;

        public const long DEVICE_VALVE_CENTRAL_HEATING_MANUAL_OPEN = 103746;
        public const long DEVICE_VALVE_CENTRAL_HEATING_MANUAL_CLOSE = 103747;
        public const long DEVICE_VALVE_CENTRAL_HEATING_AUTO_OPEN = 103748;
        public const long DEVICE_VALVE_CENTRAL_HEATING_AUTO_CLOSE = 103749;
        public const long DEVICE_VALVE_DOMESTIC_HOT_WATER_MANUAL_OPEN = 103750;
        public const long DEVICE_VALVE_DOMESTIC_HOT_WATER_MANUAL_CLOSE = 103751;
        public const long DEVICE_VALVE_DOMESTIC_HOT_WATER_AUTO_OPEN = 103752;
        public const long DEVICE_VALVE_DOMESTIC_HOT_WATER_AUTO_CLOSE = 103753;
        public const long DEVICE_PUMP_CENTRAL_HEATING_ENABLED = 103754;
        public const long DEVICE_PUMP_CENTRAL_HEATING_AUTO = 103755;
        public const long DEVICE_PUMP_DOMESTIC_HOT_WATER_ENABLED = 103756;
        public const long DEVICE_PUMP_DOMESTIC_HOT_WATER_AUTO = 103757;
        public const long SPECIFIC_GRAVITY = 103774;
        public const long DEVICE_REGULATOR_SEND_FREQ = 103782;
        public const long DEVICE_SYNCHRONIZE_MODEM_FROM_CSCA_MODE = 103783;

        public const long DEVICE_BATTERY_RECOVERY_PROCEDURE_BLOCK_COUNT = 103784;
        public const long DEVICE_BATTERY_RECOVERY_PROCEDURE_EXECUTE_COUNT = 103785;
        public const long DEVICE_BATTERY_RECOVERY_COUNT = 103786;
        public const long DEVICE_LAST_HIPOW_CUT_TIMESTAMP = 103787;
        public const long DEVICE_LAST_HIPOW_CUT_DURATION = 103788;
        public const long DEVICE_BATTERY_RECOVERY_MODEM_WAKEUP_DELAY = 103789;
        public const long DEVICE_BATTERY_RECOVERY_MIN_TEMPERATURE = 103790;
        public const long DEVICE_BATTERY_RECOVERY_MAX_TEMPERATURE = 103791;
        public const long DEVICE_BATTERY_RECOVERY_PERCENTAGE_LIMIT = 103792;
        public const long DEVICE_COMMANDER_COMMAND_EXECUTION_PERIOD = 103793;
        public const long DEVICE_COMMANDER_COMMAND_EXECUTION_COUNT = 103794;
        public const long DEVICE_COMMANDER_COMMAND_CODE = 103795;
        public const long DEVICE_COMMANDER_COMMAND_PARAM_1 = 103796;
        public const long DEVICE_COMMANDER_COMMAND_PARAM_2 = 103797;
        public const long DEVICE_COMMANDER_COMMAND_PARAM_3 = 103798;
        public const long STANDARD_OF_COMPRESSION = 103799;

        public const long PRESSURE_HI_EQUIVALENT = 103809;
        public const long PRESSURE_LO_EQUIVALENT = 103804;
        public const long FLOW_EQUIVALENT_CORRECTED_ACTIVE = 103805;
        public const long DEVICE_SEAL_OPEN_KIND = 103806;
        public const long MAXIMUM_FLOW_EQUIVALENT = 103807;
        public const long MINIMUM_FLOW_EQUIVALENT = 103808;
        public const long DEVICE_VALDRIVER_ACTION_ID = 103817;

        public const long GL_TOTAL_VOLUME = 104006;
        public const long GL_DELIVERY_LOADING_TEMPERATURE = 104007;
        public const long GL_DELIVERY_LOADING_DISCREPANCY = 104008;
        public const long GL_DELIVERY_TRANSPORT_TEMPERATURE = 104009;
        public const long GL_DELIVERY_TRANSPORT_DISCREPANCY = 104010;
        public const long GL_DELIVERY_TANK_CALIBRATION_MIN = 104011;
        public const long GL_DELIVERY_TANK_CALIBRATION_MAX = 104012;
        public const long GL_DELIVERY_DROP_DISCREPANCY = 104013;
        public const long GL_TANK_TEMPERATURE = 104014;
        public const long GL_TANK_DISCREPANCY = 104015;
        public const long GL_SALES_TEMPERATURE = 104016;
        public const long GL_TANK_SALES_CALIBRATION_MIN = 104017;
        public const long GL_TANK_SALES_CALIBRATION_MAX = 104018;
        public const long GL_SALES_TANK_DISCREPANCY = 104019;
        public const long GL_SALES_NOZZLES = 104020;
        public const long GL_SALES_POS = 104021;
        public const long AWSR_TEMINAL_FUEL_VIRT_VOLUME_DISPENSED = 104022;
        public const long AWSR_DISPENSED_FUEL_VIRT_VOLUME_DELIVERED = 104023;
        public const long AWSR_DISPENSED_FUEL_VIRT_TEMPERATURE = 104024;
        public const long GL_INPUT_DATA_ANOMALY = 104025;
        public const long GL_TC_START_VOLUME = 104035;
        public const long GL_TC_END_VOLUME = 104036;
        public const long GL_TC_MIN_VALUE = 104037;
        public const long GL_TC_MAX_VALUE = 104038;
        public const long GL_TC_LEARNING_TIME = 104039;

        public const long DEVICE_ARCHIVE_TYPE_NUMBER = 103820;
        public const long DEVICE_ARCHIVE_ENTRY_NUMBER = 103821;

        public const long DEVICE_EXTERNAL_USART_DATA_BITS_TYPE = 103830;
        public const long DEVICE_EXTERNAL_USART_STOP_BITS_TYPE = 103831;
        public const long DEVICE_EXTERNAL_USART_OPERATING_MODE = 103832;
        public const long DEVICE_EXTERNAL_USART_BAUD_RATE = 103833;

        public const long ROOM_TEMPERATURE_DAY_SETPOINT = 103835;
        public const long ROOM_TEMPERATURE_NIGHT_SETPOINT = 103836;
        public const long ROOM_TEMPERATURE = 103837;
        public const long TEMPERATURE_HOT_WATER_DAY_SETPOINT = 103838;
        public const long TEMPERATURE_HOT_WATER_NIGHT_SETPOINT = 103839;
        public const long SUMMER_CUT_OUT_TEMPERATURE = 103840;
        public const long MAX_ROOM_TEMPERATURE_INFLUENCE = 103841;
        public const long MIN_ROOM_TEMPERATURE_INFLUENCE = 103842;

        public const long DEVICE_DANFOSS_REQUEST_ADDRESS = 103852;
        public const long DANFOSS_TASK_FRAME = 103853;
        public const long WATER_METER_USAGE_VALUE = 103854;

        public const long DEVICE_LAST_WAKEUP_BATTERY_RECOVERY_COUNT = 103856;
        public const long DEVICE_BATTERY_RECOVERY_DURATION = 103857;
        public const long DEVICE_BATTERY_RECOVERY_MAX_EXECUTION_COUNT_PER_WAKEUP = 103858;

        public const long DEVICE_EXTERNAL_USART_ACTIVATION_TIME = 103859;
        public const long DEVICE_EXTERNAL_USART_WORK_DURATION_TIME = 103860;
        public const long DEVICE_BATTERY_EXTERNAL_USART_CURRENT = 103861;
        public const long DEVICE_DANFOSS_COMMAND_AFTER_REQUEST = 103862;

        public const long DEVICE_MODBUS_SLAVE_ADDRESS = 103863;
        public const long DEVICE_MODBUS_RECEIVING_TIME = 103864;
        public const long DEVICE_MODBUS_REQUEST_FUNCTION_CODE = 103865;
        public const long DEVICE_MODBUS_REQUEST_START_ADRESS = 103866;
        public const long DEVICE_MODBUS_REQUEST_QUANTITY_OF_DATA = 103867;
        public const long DEVICE_MODBUS_COMMAND_AFTER_REQUEST = 103869;

        public const long DEVICE_CIRCULATION_WATER_COUNTER_COEFFICIENT = 103870;
        public const long DEVICE_CHARGING_WATER_COUNTER_COEFFICIENT = 103871;
        public const long DEVICE_WATER_SUPPLY_WATER_COUNTER_COEFFICIENT = 103872;
        public const long DEVICE_NODE_WATER_COUNTER_COEFFICIENT = 103873;
        public const long DEVICE_DHW_VALVE_REGULATION_CYCLE = 103874;
        public const long DEVICE_CENTRAL_HEATING_VALVE_REGULATION_CYCLE = 103875;
        public const long DEVICE_LOADING_PUMP_REGULATION_CYCLE = 103876;
        public const long DEVICE_MIXING_VALVE_REGULATION_CYCLE = 103877;
        public const long DEVICE_CHARGING_PUMP_ACTIVATION_TIME = 103878;
        public const long DEVICE_PROGRAM_NUMBER = 103879;
        public const long TEMPERATURE_CENTRAL_HEATING_MAX_OUT = 103880;
        public const long TEMPERATURE_CENTRAL_HEATING_MIN_SET = 103881;
        public const long DEVICE_MIXING_VALVE_PULSE_TIME = 103882;
        public const long DEVICE_DHW_VALVE_PULSE_TIME = 103883;
        public const long DEVICE_CENTRAL_HEATING_PULSE_TIME = 103884;
        public const long DEVICE_LOADING_PUMP_PULSE_TIME = 103885;
        public const long DEVICE_TEMPERATURE_DEVIATION_OF_SECOND_DEGREE = 103886;
        public const long DEVICE_INITIAL_TEMPERATURE = 103887;
        public const long DEVICE_FULL_LOADING_CYCLE_DURATION = 103888;
        public const long DEVICE_MIXING_VALVE_OPENING_TIME = 103889;
        public const long DEVICE_MIXING_VALVE_CLOSING_TIME = 103890;
        public const long DEVICE_DHW_VALVE_OPENING_TIME = 103891;
        public const long DEVICE_DHW_VALVE_CLOSING_TIME = 103892;
        public const long DEVICE_CENTRAL_HEATING_VALVE_OPENING_TIME = 103893;
        public const long DEVICE_CENTRAL_HEATING_VALVE_CLOSING_TIME = 103894;
        public const long DEVICE_CHARGING_VALVE_OPENING_TIME = 103895;
        public const long DEVICE_CHARGING_VALVE_CLOSING_TIME = 103896;
        public const long DEVICE_NEW_CHARGING_ROUTINE_ACTIVE = 103897;
        public const long DEVICE_CHARGING_MANUAL_MODE_ACTIVE = 103898;
        public const long DEVICE_CHARGING_PUMP_MANUAL_SWITCH_ACTIVE = 103899;
        public const long TEMPERATURE_DHW_SET = 103900;
        public const long TEMPERATURE_CENTRAL_HEATING_CORRECTION = 103901;
        public const long CONSRANT_FLOW_SET_VALUE = 103902;
        public const long TEMPERATURE_CENTRAL_HEATING_OUT_WITH_ZERO_OUTSIDE = 103903;
        public const long ROOM_TEMPERATURE_ADDITIONAL = 103904;
        public const long TEMPERATURE_ON_DUTY = 103905;
        public const long DEVICE_CHARGING_FLOW_CORRECTION_VALUE = 103938;
        public const long DEVICE_CENTRAL_HEATING_VALVE_OPEN_TIME_DURING_START = 103937;
        public const long MODBUS_FRAME_DATA_FORMAT_ID = 103939;
        public const long DEVICE_INDUCTIVE_SENSOR_DETECTION_LEVEL = 103942;
        public const long DEVICE_INDUCTIVE_SENSOR_DETECTION_MIN_DIFF_LEVEL = 103943;
        public const long DEVICE_INDUCTIVE_SENSOR_MAX_SIGNAL_DYNAMICS = 103944;
        public const long DEVICE_INDUCTIVE_SENSOR_SIGNAL_NOISE_LEVEL = 103945;
        public const long DEVICE_INDUCTIVE_SENSOR_REMOVE_LEVEL = 103946;
        public const long DEVICE_INDUCTIVE_SENSOR_SENSOR_SAMPLES_PER_SECOND = 103947;

        public const long DEVICE_MODBUS_REQUEST_DATA_TYPE = 103967;
        public const long DEVICE_MODBUS_REQUEST_INDEX = 103968;
        public const long DEVICE_MODBUS_REQUEST_READ_FUNCTION_CODE = 103969;
        public const long DEVICE_MODBUS_REQUEST_WRITE_FUNCTION_CODE = 103970;
        public const long DEVICE_MODBUS_REQUEST_DATA_FORMAT_ID = 103971;
        public const long DEVICE_MODBUS_REQUEST_SLOPE = 103972;
        public const long DEVICE_MODBUS_REQUEST_BIAS = 103973;
        public const long DEVICE_EVENT_TRANSMISSION_DEFAULT_PROTOCOL_ID = 103974;
        public const long DEVICE_RADIO_ALARM_FRAME_POWER = 103975;
        public const long DEVICE_RADIO_ALARM_FRAME_PREAMBLE_LENGTH = 103976;
        public const long DEVICE_RADIO_MOON_MIN_PREAMBLE_LENGTH = 103977;
        public const long DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE = 103991;
        public const long DEVICE_INDUCTIVE_SENSOR_MEASUREMENTS_PERIOD = 103992;

        public const long DEVICE_TEST_IMR_RADIO_MODE = 104029;
        public const long DEVICE_IMRTEST_PARENT_SERIAL_NBR = 104030;
        public const long DEVICE_IMRTEST_RADIO_QUALITY = 104031;


        public const long DEVICE_H2O_METER_SLAVE_ADDRESS = 104033;
        public const long DEVICE_H2O_METER_SLAVE_ENABLED = 104034;
        public const long DEVICE_DOWN_EVENT_REVERSE_FLOW = 104040;
        public const long DEVICE_UP_EVENT_REVERSE_FLOW = 104041;

        public const long DEVICE_REGULATOR_CLOCK_YEAR = 104051;
        public const long DEVICE_REGULATOR_CLOCK_DAY_MONTH = 104052;
        public const long DEVICE_REGULATOR_CLOCK_HOUR_MINUTE = 104053;
        public const long TEMPERATURE_CENTRAL_HEATING_SET_DEVIATION = 104054;
        public const long HEATING_CURVE_TYPE = 104055;
        public const long LOWER_NIGHT_TEMPERATURE = 104056;
        public const long DAY_OUT_TEMPERATURE_LIMIT = 104057;
        public const long NIGHT_OUT_TEMPERATURE_LIMIT = 104058;
        public const long VALVE_PULSE_JUMP = 104059;
        public const long VALVE_PULSE_TIME = 104060;
        public const long TEMPERATURE_SET_POINT_TYPE = 104061;
        public const long TEMPERATURE_CONTROL_SUPPLY = 104062;
        public const long TEMPERATURE_DIFFERENCE_MIN = 104063;
        public const long DEVICE_EVENT_FRAME_RETRANSSMITION_RETRIES = 104067;

        public const long DEVICE_WMBUS_CRYPTOGRAPHY_KEY = 104083;

        public const long DIFF_SWITCH1 = 104085;
        public const long DIFF_SWITCH2 = 104086;
        public const long SLUM_SHUT1 = 104087;
        public const long SLUM_SHUT2 = 104088;
        public const long DEVICE_RESERVED_BIT = 104095;
        public const long DEVICE_WMBUS_ALARM_RELAY_MODE = 104096;
        public const long DEVICE_TEST_IMR_BLOCKAGE_ENABLED = 104097;
        public const long DEVICE_TEST_IMR_EXTENSION_BLOCKAGE_ENABLED = 104098;
        public const long DEVICE_TEST_IMR_MAX_COUNT = 104099;
        public const long DEVICE_TEST_IMR_PERIOD = 104100;
        public const long DEVICE_TEST_IMR_BLOCKAGE_DURATION = 104101;
        public const long DEVICE_RADIO_ALARM_FRAME_PREAMBLE_DIVIDED = 104102;
        public const long DEVICE_WMBUS_CRYPTOGRAPHY_KEY_ENCRYPTED = 104115;
        public const long DEVICE_MENU_LIMIT_ENABLED = 104116;
        public const long DEVICE_MENU_LIMIT_EXTENTION_ENABLED = 104117;
        public const long DEVICE_MENU_LIMIT_MAX_COUNT = 104118;
        public const long DEVICE_MENU_LIMIT_PERIOD = 104119;
        public const long DEVICE_MENU_LIMIT_BLOCKAGE_DURATION = 104120;
        public const long DEVICE_GPRS_ERROR_MODEM_RESET = 104123;
        public const long DEVICE_GPRS_SMS_MAX_RETIRES = 104124;
        public const long DEVICE_INDUCTIVE_SENSOR_LOGS_ENABLED = 104127;

        public const long DEVICE_MODEM_WAKEUP_DELAY = 104130;
        public const long DEVICE_TIME_MAX_DIFF = 104146;
        public const long DEVICE_PACKET_ARCHIVE_PENDING_ITEMS = 104147;

        public const long ANALOG_CONFIG_DATA_ID = 104153;
        public const long ANALOG_CONFIG_CONVERTION_TYPE = 104154;
        public const long ANALOG_CONFIG_HISTERESIS = 104155;
        public const long ANALOG_CONFIG_RAPID_RISE = 104156;
        public const long ANALOG_CONFIG_RAPID_DROP = 104157;
        public const long ANALOG_CONFIG_ARCHIVE_OPERATION = 104158;
        public const long ANALOG_CONFIG_OFFSET = 104159;
        public const long ANALOG_CONFIG_RAPID_PERIOD = 104160;

        public const long DEVICE_ANALOG_CONFIG_DATA_EXPIRED_UP_ENABLE = 104184;
        public const long DEVICE_ANALOG_CONFIG_DATA_EXPIRED_DOWN_ENABLE = 104185;
        public const long DEVICE_ANALOG_CONFIG_OUT_OF_RANGE_UP_ENABLE = 104186;
        public const long DEVICE_ANALOG_CONFIG_OUT_OF_RANGE_DOWN_ENABLE = 104187;

        public const long DEVICE_OPTO_BAUD_RATE = 104188;
        public const long DEVICE_NTP_SERVER_ADDRESS = 104193;
        public const long DEVICE_NTP_SERVER_PORT = 104194;
        public const long DEVICE_NTP_SERVER_SYNCHRONIZE_ENABLE = 104195;

        public const long DEVICE_VALDRIVER_CLOSE_ON_SABOTAGE_DELAY = 104196;
        public const long DEVICE_VALDRIVER_CLOSE_ON_COVER_OPEN_DELAY = 104197;

        public const long DEVICE_MAGNETIC_SENSOR_ENCODER_TO_LEVEL_TABLE = 104200;
        public const long DEVICE_MAGNETIC_SENSOR_MEASUREMENT_PERIOD = 104201;
        public const long DEVICE_MAGNETIC_SENSOR_TEMPERATURE_READOUT_PERIOD = 104202;
        public const long DEVICE_MAGNETIC_SENSOR_ANGLE_OFFSET = 104203;
        public const long DEVICE_MAGNETIC_SENSOR_TEMPERATURE_OFFSET = 104204;
        public const long DEVICE_MAGNETIC_SENSOR_TANKING_PERIOD = 104205;
        public const long DEVICE_MAGNETIC_SENSOR_TANKING_LEVEL = 104206;
        public const long DEVICE_MAGNETIC_SENSOR_EMPTING_LEVEL = 104207;
        public const long DEVICE_MAGNETIC_SENSOR_OUT_OF_RANGE_MIN_LEVEL = 104208;
        public const long DEVICE_MAGNETIC_SENSOR_OUT_OF_RANGE_MAX_LEVEL = 104209;

        public const long DEVICE_MODEM_AUTOMATIC_TIME_SYNCHRONIZE_ENABLED = 104212;
        public const long DEVICE_LOG_BLOB_LENGTH_LIMIT = 104213;

        public const long DEVICE_FTP_FIRMWARE_ENCRYPTED = 104217;
        public const long DEVICE_AVAILABLE_FIRMWARE_VERSION_ON_FTP = 104221;

        public const long DEVICE_MODEM_MAX_SLEEP_DURATION = 104222;

        public const long DEVICE_LCD_TIME_VERSION = 104223;

        public const long DEVICE_SMS_VALIDITY_PERIOD_HOURS = 104224;

        public const long DEVICE_WL_DEAD_DAYS_LEARN = 104229;
        public const long DEVICE_WL_MODE = 104230;
        public const long DEVICE_WL_LEARN_ON_LOST = 104231;
        public const long DEVICE_WL_LEARN_ON_DEAD = 104232;
        public const long DEVICE_WL_INTERFACE_ID = 104233;
        public const long DEVICE_WL_INTERFACE_PREPARE_TIME = 104234;
        public const long DEVICE_WL_INTERFACE_MODE = 104235;
        public const long DEVICE_WL_MISSED_TO_LOST = 104236;
        public const long DEVICE_WL_MISSED_TO_DEAD = 104237;
        public const long DEVICE_WL_MAX_WINDOW_SIZE = 104238;
        public const long DEVICE_WL_LEARN_TIME = 104239;

        public const long DEVICE_WL_RECEPTION_OFFSET = 104254;
        public const long DEVICE_LAN3_ENABLE = 104262;
        public const long WATER_COUNTER = 104265;
        public const long REVERSE_WATER_COUNTER = 104266;

        public const long ANALOG_CONFIG_READOUT_SELECT_CURRENT_DATA = 104268;
        public const long ANALOG_CONFIG_READOUT_SELECT_ARCHIVE_CURRENT = 104269;
        public const long ANALOG_CONFIG_READOUT_SELECT_ARCHIVE_AVG = 104270;
        public const long ANALOG_CONFIG_READOUT_SELECT_ARCHIVE_MAX = 104271;
        public const long ANALOG_CONFIG_READOUT_SELECT_ARCHIVE_MIN = 104272;
        public const long ANALOG_CONFIG_READOUT_SELECT_ARCHIVE_EVENTS = 104273;
        public const long ANALOG_QUERY = 104274;
        public const long ALEVEL_TEMPERATURE = 104275;

        public const long DEVICE_RADIO_SELF_FRAME_EXTENDED_DIVIDER = 104277;
        public const long DEVICE_RADIO_SELF_FRAME_PERIOD_DIVIDER = 104278;
        public const long DEVICE_RADIO_BIT_RATE_MODE = 104279;
        public const long DEVICE_WL_MIN_WINDOW_SIZE = 104291;

        public const long AVERAGE_OPERATIONAL_LEVEL = 104292;

        public const long DEVICE_SIM_ON_CHIP_ENABLED = 104298;
        public const long DEVICE_FORCED_UMTS_BAND = 104299;
        public const long DEVICE_FORCED_LTE_BAND = 104300;
        public const long DEVICE_INCOMING_CALL_BEHAVIOR = 104305;
        public const long DEVICE_GPRS_PDP_TYPE = 104306;
        public const long DEVICE_ACCESS_TECHNOLOGY_MASK = 104307;
        public const long DEVICE_ACCESS_TECHNOLOGY_FORCED = 104308;
        public const long DEVICE_SLAVE_LONG_ADDRESS = 104309;

        public const long INDUCTIVE_SENSOR_TEMPERATURE_COMPENSATION_ENABLED = 104350;
        public const long BATTERY_RADIO_FRAME_ENERGY = 104348;
        public const long INDUCTIVE_SENSOR_IMPULSES_TO_REVERSE_FLOW_ALARM = 104349;
        public const long BACKFLOW_COUNTER_TIMESTAMP = 104356;
        public const long BACKFLOW_COUNTER_DURATION = 104357;
        public const long BACKFLOW_COUNTER = 104358;
        public const long BACKFLOW_COUNTER_VOLUME = 104359;
        public const long DEVICE_BATTERY_MAGNETIC_SENSOR_ENERGY = 104366;
        public const long DEVICE_UDI_BACKFLOW_SLOT_NUMBER = 104367;
        public const long TEST_PROTOCOL_INTERFACE_REDIRECT = 104378;
        public const long DEVICE_FREQ_EVENT_CONTENT = 104379;
        public const long DEVICE_LCD_WARNING_ICON_CHECK_MASK = 104381;
        public const long DEVICE_GSM_TEST_MAX_DURATION = 104386;
        public const long DEVICE_POWER_HIPOW_EMERGENCY_TIMEOUT = 104399;
        public const long USER_STRUCTURE_1_DEF_OBJ_ID = 104417;
        public const long SECONDARY_ENERGY = 104419;
        public const long SECONDARY_DEVICE_COUNTER_LITER_MAX_VALUE = 104420;
        public const long SECONDARY_MAX_HOUR_FLOW = 104421;
        public const long DEVICE_FREQ_CYCLIC_CONTENT = 104423;        
        public const long DEVICE_DIFF_NEGATIVE_ACTIVE = 104424;
        public const long DEVICE_WEEKLY_DATA_LATCH_DAY_OF_WEEK = 104425;
        public const long DEVICE_MODEM_IDLE_CURRENT = 104428;
        public const long DEVICE_MODEM_LOGGING_CURRENT = 104429;
        public const long DEVICE_MODEM_CONTEXT_ACTIVATION_ENERGY = 104430;
        public const long DEVICE_MODEM_SEND_SMS_ENERGY = 104431;
        public const long DEVICE_MODEM_SEND_GPRS_BYTE_ENERGY = 104432;        
        public const long DEVICE_EXTERNAL_USART_ENABLE = 104433;
        public const long USER_STRUCTURE_2_DEF_OBJ_ID = 104438;
        public const long DEVICE_UP_EVENT_HIPOW = 104439;
        public const long DEVICE_DOWN_EVENT_HIPOW = 104440;
        #endregion
        #region DATA read only
        public const long FUEL_VOLUME = 100001;    // Obj�to�� paliwa
        public const long FUEL_VOLUME_APPROXIMATELY = 102203;    // Aproksymowane ci�nienie paliwa
        public const long FUEL_DEPTH = 100002;    // Wysoko�� paliwa
        public const long FUEL_TEMPERATURE = 100003;    // Temperatura paliwa
        public const long FUEL_ID = 100004;    // Id paliwa
        public const long OPERATING_ULLAGE = 100006;    // Wolna przestrze�
        public const long WATER_DEPTH = 100007;    // Wysoko�� wody
        public const long WATER_VOLUME = 100008;    // Obj�to�� wody
        public const long ATG_I_TIMESTAMP = 100010;    // Znacznik czasowy pomiaru
        public const long ATG_I_STATUS = 100011;    // Aktualny status komunikacji
        public const long ATG_I_PROGRESS = 100012;    // Aktualny post�p odpytania
        public const long ATG_I_CONFIDENCE = 100013;    // Wa�no�� pomiaru
        public const long TANK_NUMBER = 100014;    // Numer zbiornika
        public const long ULLAGE = 100015;    // Wolna obj�to��
        public const long WATER_HIGH_ALARM = 100016;    // Alarm wysoka woda
        public const long DEADSTOCK_ALARM = 100017;    // Alarm krytyczny pozim paliwa 
        public const long LOW_FUEL_LEVEL_ALARM = 100018;    // Alarm niski poziom paliwa
        public const long DEADSTOCK_DISTR_ALARM = 100019;    // Alarm krytyczny poziom paliwa
        public const long HIGH_FUEL_ALARM_LEVEL_VALUE = 100020;    // Bezpieczna pojemno�� zbiornika
        public const long STATUS = 100021;    // Status
        public const long ATG_UNIT = 100022;    // Jednostka
        public const long QUERY_TIMESTAMP = 100023;    // Znacznik czasowy
        public const long RADIO_QUALITY = 100024;    // Jako�� sygna�u radiowego
        public const long DEVICE_COM1_PRESENT = 100036;    // Czy link komunikacyjny jest dostepny w tej wersji urzadzenia - COM1
        public const long DEVICE_COM2_PRESENT = 100044;    // Czy link komunikacyjny jest dostepny w tej wersji urzadzenia - COM2
        public const long DEVICE_USB_PRESENT = 100052;    // Czy link komunikacyjny jest dostepny w tej wersji urzadzenia - USB
        public const long POWER_STATE = 100060;    // Zasilanie sieciowe aktywne
        public const long GSM_QUALITY = 100061;    // Jako�� sygna�u GSM (0-31)
        public const long DEVICE_SMS_SENT_COUNT = 100062;    // Ilo�� wys�anych SMS�w z urz�dzenia
        public const long DEVICE_SMS_RECEIVED_COUNT = 100504;    // Ilo�� odebranych SMS�w przez urz�dzenie
        public const long DEVICE_SIM_CARD_CHANGE_PIN = 100076;    // PIN z nowej kary SIM
        public const long DEVICE_SIM_CARD_CHANGE_SMS_CENTER = 100077;    // Centrum SMS nowej karty SIM
        public const long DEVICE_SIM_CARD_CHANGE_SERIAL_NUMBER = 100078;    // Numer seryjny nowej karty SIM
        public const long BATTERY_STATUS = 100101;    // <Brak opisu>
        public const long GAS_LEVEL_PERCENTAGE_VALUE = 100102;    // <Brak opisu>
        public const long ALEVEL_VALUE_HIHI_EVENT = 100103;    // <Brak opisu>
        public const long ALEVEL_VALUE_HI_EVENT = 100104;    // <Brak opisu>
        public const long ALEVEL_VALUE_LO_EVENT = 100105;    // <Brak opisu>
        public const long ALEVEL_VALUE_LOLO_EVENT = 100106;    // <Brak opisu>
        public const long ALEVEL_VALUE_STEPREL_EVENT = 100107;    // <Brak opisu>
        public const long ALEVEL_VALUE_STEPABS_EVENT = 100108;    // <Brak opisu>
        public const long ALEVEL_VALUE_UNCERTAINTY = 100109;    // <Brak opisu>
        public const long ALEVEL_VALUE_EVENTS_ENABLE = 100110;    // <Brak opisu>
        public const long TEMPERATURE = 100111;    // <Brak opisu>
        public const long ALEVEL_TEMPERATURE_UNCERTAINTY = 100112;    // <Brak opisu>
        public const long AVERAGE_PERCENTAGE_VALUE = 100113;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_HIHI_EVENT = 100114;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_HI_EVENT = 100115;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_LO_EVENT = 100116;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_LOLO_EVENT = 100117;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_STEPREL_EVENT = 100118;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_STEPABS_EVENT = 100119;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_UNCERTAINTY = 100120;    // <Brak opisu>
        public const long ALEVEL_AVG_VALUE_EVENTS_ENABLE = 100121;    // <Brak opisu>
        public const long DEVICE_ACTION_CID_NEW_CID = 100127;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LO = 100146;    // <Brak opisu>
        public const long DEVICE_EVENT_COVER_OPEN = 100147;    // <Brak opisu>
        public const long DEVICE_EVENT_ERROR_IN_DEVICE = 100148;    // <Brak opisu>
        public const long DEVICE_EVENT_SERVICE_MENU = 100149;    // <Brak opisu>
        public const long DEVICE_EVENT_SCAN_GSM = 100150;    // <Brak opisu>
        public const long DEVICE_EVENT_MODEM_ON = 100152;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_FULL = 100153;    // <Brak opisu>
        public const long DEVICE_EVENT_CLOCK = 100155;    // <Brak opisu>
        public const long DEVICE_EVENT_FIRMWARE_ON = 100156;    // <Brak opisu>
        public const long DEVICE_EVENT_REFUEL_KNOWN = 100157;    // <Brak opisu>
        public const long DEVICE_EVENT_REFUEL_UNKNOWN = 100158;    // <Brak opisu>
        public const long DEVICE_EVENT_ALEVEL_ERROR = 100159;    // <Brak opisu>
        public const long DEVICE_EVENT_ALEVEL_BATTERY_LO = 100160;    // <Brak opisu>
        public const long DEVICE_FIRMWARE_VERSION = 100161;    // <Brak opisu>
        public const long DEVICE_MODEM_RESET_COUNT = 100162;    // <Brak opisu>
        public const long DEVICE_PIC_RESET_COUNT = 100163;    // <Brak opisu>
        public const long GAS_METER_USAGE_VALUE = 100164;    // <Brak opisu>
        public const long APULSE_STATE = 100165;    // <Brak opisu>
        public const long VALDRIVER_STATE = 100166;    // <Brak opisu>
        public const long BATTERY_CHANGE_COUNTER = 100167;    // <Brak opisu>
        public const long BATTERY_STATE = 100168;    // <Brak opisu>
        public const long COVER_OPEN_COUNTER = 100169;    // <Brak opisu>
        public const long HARD_RESET_COUNTER = 100170;    // <Brak opisu>
        public const long BATTERY_PERCENTAGE_USAGE = 100172;    // <Brak opisu>
        public const long DEVICE_SEND_REASON = 100174;    // <Brak opisu>
        public const long DEVICE_LAST_SEND = 100175;    // <Brak opisu>
        public const long DEVICE_LAST_RESTART = 100176;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_APULSE = 100177;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_VALDRIVER = 100178;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_MODEM_COM = 100179;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_SIMCARD = 100180;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_MODEM_LOGIN = 100181;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_MODEM_GPRS = 100182;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_BATTERY_END = 100183;    // <Brak opisu>
        public const long DEVICE_OKO_ERROR_BATTERY_LO = 100184;    // <Brak opisu>
        public const long DEVICE_FRAM_SLOT = 100200;    // <Brak opisu>
        public const long DEVICE_EVENT_BATTERY_LOLO = 100390;
        public const long BATTERY_VOLTAGE_VALUE = 100391;
        public const long REFUEL_VALUE = 100393;
        public const long TEMPERATURE_MIN = 100435; // Temperatura minimalna ze stacji pogodowej
        public const long TEMPERATURE_MAX = 100436; // Temperatura maksymalna ze stacji pogodowej
        public const long TEMPERATURE_EQUIVALENT = 100437; // Temperatura do obliczen zuzycia
        public const long CLIMATE_TEMPERATURE_AVG = 100438;
        public const long CLIMATE_TEMPERATURE_EQUIVALENT = 100439;
        public const long CLIMATE_TEMPERATURE_MIN = 100440;
        public const long CLIMATE_TEMPERATURE_MAX = 100441;
        public const long FORECAST_TEMPERATURE_AVG = 100442;
        public const long FORECAST_TEMPERATURE_EQUIVALENT = 100443;
        public const long FORECAST_TEMPERATURE_MIN = 100444;
        public const long FORECAST_TEMPERATURE_MAX = 100445;

        public const long DEVICE_ERROR_RTC = 100600;
        public const long DEVICE_ERROR_COUNTER_COPY = 100601;
        public const long DEVICE_ERROR_MAX_TEMP = 100602;
        public const long DEVICE_ERROR_MIN_TEMP = 100603;
        public const long DEVICE_ERROR_EE_COUNTER = 100604;
        public const long DEVICE_ERROR_EE_CONFIG = 100605;
        public const long DEVICE_ERROR_EE_DIAGNOSTIC = 100606;
        public const long DEVICE_ERROR_EE_ARCHIVE = 100607;
        public const long DEVICE_ERROR_EE_ARCHIVE_FULL = 100608;
        public const long DEVICE_ERROR_ROM_UPDATE_FAILED = 100609;
        public const long DEVICE_ERROR_MAX_FLOW = 100610;
        public const long DEVICE_ERROR_BATTERY_LOW = 100611;
        public const long DEVICE_ERROR_SIM_CARD = 100612;
        public const long DEVICE_ERROR_SABOTAGE_CONTACT = 100613;
        public const long DEVICE_ERROR_SABOTAGE_SWITCH = 100614;
        public const long DEVICE_ERROR_IR_FLAG = 100615;
        public const long DEVICE_ERROR_VALDRIVER = 101339;
        public const long DEVICE_ERROR_REMOVE = 101340;
        public const long DEVICE_ERROR_EE_WRITE = 101751;
        public const long DEVICE_ERROR_SABOTAGE_DETECTED = 102252;

        public const long ENERGY = 100800;
        public const long VOLUME = 100801;
        public const long FACTORY_NUMBER = 100802;
        public const long CURRENT_POWER = 100803;
        public const long CURRENT_FLOW = 100804;
        public const long FLOW_IN_TEMPERATURE = 100805;
        public const long FLOW_OUT_TEMPERATURE = 100806;

        public const long COUNTER_FILTRATED = 101073;    // <Brak opisu>
        public const long COUNTER_NOT_FILTRATED = 101074;    // <Brak opisu>
        public const long SABOTAGE_COUNTER = 101075;    // <Brak opisu>
        public const long SABOTAGE_DURATION = 101076;    // <Brak opisu>
        public const long COUNTER_INPUT_STATE = 101077;    // <Brak opisu>
        public const long SABOTAGE_INPUT_STATE = 101078;    // <Brak opisu>
        public const long QUERY_STATUS = 101079;    // <Brak opisu>
        public const long QUERY_PROGRESS = 101080;    // <Brak opisu>
        public const long COUNTER_DECREMENTS = 101081;    // <Brak opisu>
        public const long VALVE_MOVE_COUNTER = 101082;    // <Brak opisu>
        public const long POWER_ON_RESET_COUNTER = 101083;    // <Brak opisu>
        public const long CONFIDENCE = 101084;    // <Brak opisu>
        public const long VOLUME_ON_ERROR = 101085;    // <Brak opisu>
        public const long MAX_HOUR_FLOW_FROM_LAST_ACCOUNT_PERIOD = 101086;    // <Brak opisu>
        public const long MAX_HOUR_FLOW_DAY = 101087;    // <Brak opisu>
        public const long MAX_HOUR_FLOW_HOUR = 101088;    // <Brak opisu>
        public const long VOLUME_LAST_MONTH = 101089;    // <Brak opisu>
        public const long DAY_OF_MONTH_SUMMARY_FROM_LAST_ACCOUNT_PERIOD = 101090;    // <Brak opisu>
        public const long WORK_TIME_WITH_ERROR = 101091;    // <Brak opisu>
        public const long DEVICE_ERROR_EE_IMRWAN2 = 101092;    // <Brak opisu>
        public const long EXTERNAL_MAGNETIC_FIELD_DETECTED = 101093;    // <Brak opisu>
        public const long DEVICE_OUT_OF_METER = 101094;    // <Brak opisu>
        public const long CHECKSUM_CRC = 101095;    // <Brak opisu>
        public const long CHECKSUM_LRC = 101096;    // <Brak opisu>
        public const long CLIENT_NUMBER = 101097;    // <Brak opisu>
        public const long WORK_TIME = 101098;    // <Brak opisu>
        //public const long WORK_TIME_WITH_ERROR = 101099;    // <Brak opisu> usuni�te z bazy, bo jest taki sam - ID_DATA_TYPE=101091
        public const long MAXIMUM_POWER = 101100;    // <Brak opisu>
        public const long MAXIMUM_FLOW = 101101;    // <Brak opisu>
        public const long TEMPERATURE_DIFFERENCE = 101102;    // <Brak opisu>
        public const long ENERGY_STATE_SAVED = 101103;    // <Brak opisu>
        public const long ADDIDITONAL_WATERMETER_1_VOLUME = 101104;    // <Brak opisu>
        public const long ADDIDITONAL_WATERMETER_2_VOLUME = 101105;    // <Brak opisu>
        public const long SECOND_MAIN_WATERMETER_VOLUME = 101106;    // <Brak opisu>
        public const long ERROR_CODE = 101107;    // <Brak opisu>
        public const long ENERGY_OVER_LIMIT = 101108;    // <Brak opisu>
        public const long LAST_MONTH_MAXIMUM_FLOW = 101109;    // <Brak opisu>
        public const long LAST_MONTH_MAXIMUM_POWER = 101110;    // <Brak opisu>
        public const long WATERMETER_SUPPLEMENT_VOLUME = 101111;    // <Brak opisu>
        public const long LITER_COUNTER = 101112;    // <Brak opisu>
        public const long MONTH_SUMMARY_METER_COUNTER = 101113;    // <Brak opisu>
        public const long PULSE_COUNTER = 101114;    // <Brak opisu>
        public const long REAL_TIME = 101115;    // <Brak opisu>
        public const long SABOTAGE_PULSE_COUNTER = 101116;    // <Brak opisu>
        public const long UNMOUNT_PULSE_COUNTER = 101117;    // <Brak opisu>
        public const long ENCODER_VALUE = 101118;    // <Brak opisu>
        public const long MEASURE_VALIDITY = 101119;    // <Brak opisu>
        public const long PROCESSING_ERROR = 101120;    // <Brak opisu>
        public const long BAD_ENCODER_VALUE = 101121;    // <Brak opisu>
        public const long PARITY = 101122;    // <Brak opisu>
        public const long BAD_MEASURE_COUNTER = 101123;    // <Brak opisu>
        public const long VOLUME_DIFFERENCE = 101124;
        public const long WATERMETER_VOLUME_DIFFERENCE = 101125;
        public const long DEVICE_RADIO_433_ENABLE = 101126;
        public const long DEVICE_RADIO_869_FRAME_PERIOD = 101127;
        public const long DEVICE_RADIO_869_ENABLE = 101128;
        public const long DEVICE_RADIO_CRYPTO_ENABLE = 101129;
        public const long DEVICE_RUN_IN_LOCAL_TIME = 101130;
        public const long DEVICE_DISPLAY_BLINKING_ENABLE = 101131;
        public const long DEVICE_DISPLAY_ON_TIME = 101132;
        public const long DEVICE_DISPLAY_OFF_TIME = 101133;
        public const long DEVICE_IRDA_AFTER_DATA_POWER_ON_TIME = 101134;
        public const long DEVICE_IRDA_ALLOWED_INVALID_INIT_COUNT = 101135;
        public const long DEVICE_IRDA_INVALID_INIT_IMPORTANCE_TIME = 101136;
        public const long DEVICE_IRDA_POWER_OFF_TIME = 101137;
        public const long DEVICE_IRDA_ON_AWAITING_TIME = 101138;
        public const long DEVICE_PULSE_INPUT_MAX_FLOW_CAPTURE_TIME = 101139;
        public const long DEVICE_PULSE_INPUT_MIN_INSTANT_FLOW = 101140;
        public const long DEVICE_PULSE_INPUT_MAX_INSTANT_FLOW = 101141;
        public const long DEVICE_COUNTER_M3_COEFFICIENT = 101142;
        public const long DEVICE_COUNTER_LAST_M3_COEFFICIENT = 101143;
        public const long DEVICE_COUNTER_M3_COEFFICIENT_SAVE_TIME_STAMP = 101144;
        public const long DEVICE_COUNTER_LITER_OFFSET = 101145;
        public const long DEVICE_COUNTER_LAST_LITER_OFFSET = 101146;
        public const long DEVICE_COUNTER_LITER_OFFSET_SAVE_TIME_STAMP = 101147;
        public const long DEVICE_COUNTER_LITER_MAX_VALUE = 101148;
        public const long DEVICE_ARCHIVE_DAYLONG_SUMMARY_OFFSET = 101149;
        public const long DEVICE_ARCHIVE_MONTHLONG_SUMMARY_DAY = 101150;
        public const long DEVICE_MIN_TEMPERATURE = 101151;
        public const long DEVICE_MAX_TEMPERATURE = 101152;
        public const long LIFE_TIMER_TIMESTAMP = 101153;
        public const long LIFE_TIMER_DURATION = 101154;
        public const long PRERUN_TIMER_TIMESTAMP = 101155;
        public const long PRERUN_TIMER_DURATION = 101156;
        public const long WORK_TIMER_TIMESTAMP = 101157;
        public const long WORK_TIMER_DURATION = 101158;
        public const long WORK_TIME_WITH_ERROR_TIMESTAMP = 101159;
        public const long WORK_TIME_WITH_ERROR_DURATION = 101160;
        public const long RADIO_TIMER_TIMESTAMP = 101161;
        public const long RADIO_TIMER_DURATION = 101162;
        public const long MODEM_TIMER_TIMESTAMP = 101163;
        public const long MODEM_TIMER_DURATION = 101164;
        public const long SABOTAGE_COUNTER_TIMESTAMP = 101165;
        public const long SABOTAGE_COUNTER_DURATION = 101166;
        public const long REMOVE_COUNTER = 101167;
        public const long REMOVE_COUNTER_TIMESTAMP = 101168;
        public const long REMOVE_COUNTER_DURATION = 101169;
        public const long POWER_ON_RESET_COUNTER_TIMESTAMP = 101170;
        public const long POWER_ON_RESET_COUNTER_DURATION = 101171;
        public const long RESET_WDT_COUNTER = 101172;
        public const long RESET_WDT_COUNTER_TIMESTAMP = 101173;
        public const long RESET_WDT_COUNTER_DURATION = 101174;
        public const long RESET_MCLR_COUNTER = 101175;
        public const long RESET_MCLR_COUNTER_TIMESTAMP = 101176;
        public const long RESET_MCLR_COUNTER_DURATION = 101177;
        public const long RESET_OTHER_COUNTER = 101178;
        public const long RESET_OTHER_COUNTER_TIMESTAMP = 101179;
        public const long RESET_OTHER_COUNTER_DURATION = 101180;
        public const long RESET_MODEM_COUNTER = 101181;
        public const long RESET_MODEM_COUNTER_TIMESTAMP = 101182;
        public const long RESET_MODEM_COUNTER_DURATION = 101183;
        public const long IRDA_STATE_VALID_INIT = 101184;
        public const long IRDA_STATE_INVALID_INIT = 101185;
        public const long IRDA_STATE_LAST_VALID_INIT_TIME = 101186;
        public const long IRDA_STATE_LAST_INVALID_INIT_TIME = 101187;
        public const long IRDA_STATE_DATA_COUNTER = 101188;
        public const long SABOTAGE_LITER_COUNTER = 101189;
        public const long MONTHLONG_SUMMARY_METER_COUNTER = 101190;
        public const long FSS_STATE = 101191;
        public const long DEVICE_LATCH_ERROR_RTC = 101192;
        public const long DEVICE_LATCH_ERROR_COUNTER_COPY = 101193;
        public const long DEVICE_LATCH_ERROR_MAX_TEMP = 101194;
        public const long DEVICE_LATCH_ERROR_MIN_TEMP = 101195;
        public const long DEVICE_LATCH_ERROR_EE_COUNTER = 101196;
        public const long DEVICE_LATCH_ERROR_EE_CONFIG = 101197;
        public const long DEVICE_LATCH_ERROR_EE_DIAGNOSTIC = 101198;
        public const long DEVICE_LATCH_ERROR_EE_ARCHIVE = 101199;
        public const long DEVICE_LATCH_ERROR_EE_ARCHIVE_FULL = 101200;
        public const long DEVICE_LATCH_ERROR_ROM_UPDATE_FAILED = 101201;
        public const long DEVICE_LATCH_ERROR_MAX_FLOW = 101202;
        public const long DEVICE_LATCH_ERROR_BATTERY_LOW = 101203;
        public const long DEVICE_LATCH_ERROR_SIM_CARD = 101204;
        public const long DEVICE_LATCH_ERROR_SABOTAGE_CONTACT = 101205;
        public const long DEVICE_LATCH_ERROR_SABOTAGE_SWITCH = 101206;
        public const long DEVICE_LATCH_ERROR_IR_FLAG = 101207;
        public const long DEVICE_LATCH_ERROR_EE_IMRWAN2 = 100616;
        public const long DEVICE_LATCH_ERROR_VALDRIVER = 101341;
        public const long DEVICE_LATCH_ERROR_REMOVE = 101342;
        public const long DEVICE_LATCH_ERROR_EE_WRITE = 101752;
        public const long IMRWAN2_CONTROL_RADIO_ADDRESS_IMAGE = 101208;
        public const long DEVICE_EE_COPY_IWP_IV_IN = 101209;
        public const long DEVICE_EE_COPY_IWP_IV_OUT = 101210;
        public const long DEVICE_HARDWARE_INFO = 101211;
        public const long DEVICE_RADIO_433_FRAME_PERIOD = 101212;
        public const long DEVICE_RADIO_433_FRAME_FAST_PERIOD = 101213;
        public const long DEVICE_RADIO_869_FRAME_FAST_PERIOD = 101214;
        public const long DEVICE_COMMAND_CODE = 101250;
        public const long DEVICE_COMMAND_EXECUTE = 101251;
        public const long DEVICE_COMMAND_PARAM = 101252;
        public const long DEVICE_COMMAND_RESULT = 101253;
        public const long ON_OFF_COUNTER = 101254;
        public const long TRANSMITTED_FRAMES_COUNTER = 101255;
        public const long DEVICES_TO_TRANSMIT_COUNTER = 101256;
        public const long LAST_TRANSMITTED_FRAME_TIME = 101257;
        public const long LAST_TRANSMITTED_RADIO_ADDRESS = 101258;
        public const long LAST_TRANSMITTED_SPECIAL_FRAME_TIME = 101259;
        public const long TRANSMITTED_SPECIAL_FRAMES_COUNTER = 101260;
        public const long DEVICE_SMS_SENT_TIMESTAMP = 101281;
        public const long DEVICE_SMS_SENT_STATUS = 101282;
        public const long RESERVE_BANK_ACTIVE = 101305;
        public const long LEFT_BANK_ACTIVE = 101306;
        public const long RIGHT_BANK_ACTIVE = 101307;
        public const long DEVICE_MODEM_TYPE = 101308;
        public const long DEVICE_GSM_PROVIDER_MOBILE_NETWORK_CODE = 101309;
        public const long DEVICE_GSM_PROVIDER_LAC = 101310;
        public const long DEVICE_GSM_PROVIDER_CI = 101311;
        public const long DEVICE_IN_ROAMING = 101312;
        public const long DEVICE_SIM_CARD_SERIAL_NUMBER = 101313;

        public const long DEVICE_ERROR_VALDRIVER_LIMIT_STOP_FAILED = 101347;
        public const long DEVICE_ERROR_EE_VALDRIVER = 101348;
        public const long DEVICE_ERROR_VALDRIVER_CLOSED_LEAKAGE = 101349;
        public const long DEVICE_ERROR_VALDRIVER_OPENED_LEAKAGE = 101350;
        public const long DEVICE_LATCH_ERROR_VALDRIVER_LIMIT_STOP_FAILED = 101351;
        public const long DEVICE_LATCH_ERROR_EE_VALDRIVER = 101352;
        public const long DEVICE_LATCH_ERROR_VALDRIVER_CLOSED_LEAKAGE = 101353;
        public const long DEVICE_LATCH_ERROR_VALDRIVER_OPENED_LEAKAGE = 101354;
        public const long DEVICE_ERROR_IMRLAN_CONTROL_FAILED = 101366;
        public const long DEVICE_ERROR_VALDRIVER_NOT_DETECTED = 101367;
        public const long DEVICE_LATCH_ERROR_IMRLAN_CONTROL_FAILED = 101368;
        public const long DEVICE_LATCH_ERROR_VALDRIVER_NOT_DETECTED = 101369;

        public const long ADDIDITONAL_WATERMETER_3_VOLUME = 101373;
        public const long ADDIDITONAL_WATERMETER_4_VOLUME = 101374;

        public const long LEAKAGE_COUNTER = 101375;
        public const long LEAKAGE_COUNTER_TIMESTAMP = 101376;
        public const long LITRE_COUNTER_INPUT_1 = 101378;
        public const long LITRE_COUNTER_INPUT_2 = 101379;
        public const long LITRE_COUNTER_DIFFERENCE = 101380;


        public const long CONSUMPTION_IN_TARIFF_OPTION = 101501;      //with index nbrs [0-2]

        public const long PREPAID_TOPUP_TOTAL = 101502;
        public const long CHARGE_PREPAID_VARIABLE_TOTAL = 101503;
        public const long EMERGENCY_CREDIT_TOPUP_TOTAL = 101504;
        public const long EMERGENCY_CREDIT_PAID_TOTAL = 101505;
        public const long RECOMMENDED_TOPUP = 101506;
        public const long RECOMMENDED_TOPUP_DATETIME = 101507;

        public const long TOTAL_VOLUME = 101508;

        public const long LAST_TOPUP_ID = 101509;
        public const long CURRENT_TARIFF_ID = 101510;
        public const long PREPAID_STATE = 101511;

        public const long TARIFF_NAME = 101512;
        public const long TARIFF_ID = 101513;
        public const long PRICE_POSTPAID_FIXED_DAILY = 101514;
        public const long TARIFF_SCHEME_NAME = 101515;
        public const long TARIFF_SCHEME_START_DAY_OF_WEEK = 101516;
        public const long TARIFF_SCHEME_START_HOUR = 101517;
        public const long TARIFF_SCHEME_END_DAY_OF_WEEK = 101518;
        public const long TARIFF_SCHEME_END_HOUR = 101519;
        public const long TARIFF_SCHEME_PRICE_PREPAID_VARIABLE = 101520;
        public const long TARIFF_SCHEME_PRICE_POSTPAID_VARIABLE = 101521;
        public const long TOPUP_DATETIME = 101522;
        public const long TOPUP_ID = 101523;
        //public const long TOPUP_OWED = 101524;
        public const long OWED_CHARGE_TOTAL = 101556;
        public const long OWED_PAID_TOTAL = 101557;
        public const long TOPUP_CHARGE_POSTPAID = 101525;
        public const long OWED_REPAYMENT_PERCENT = 101526;
        public const long EMERGENCY_CREDIT_REPAYMENT_PERCENT = 101527;
        public const long EMERGENCY_CREDIT_TOPUP_VALUE = 101528;
        public const long EMERGENCY_CREDIT_LIMIT = 101529;
        public const long CHARGE_POSTPAID_TOTAL = 101530;
        public const long CHARGE_FIXED_UPDATE_DATETIME = 101531;
        public const long OWED = 101532;
        public const long CONSUMPTION_DAILY_AVG = 101533;
        public const long CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT = 101534;
        public const long CONSUMPTION_WEEKLY_AVG = 101535;
        public const long CONSUMPTION_WEEKLY_AVG_WEIGHT_PERCENT = 101536;
        public const long CONSUMPTION_DAILY_MIN = 101537;
        public const long RECOMMENDED_TOPUP_DAYS = 101538;
        public const long RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS = 101539;
        public const long RECOMMENDED_TOPUP_GRANULATION = 101540;
        public const long AVG_UNIT_PRICE = 101541;
        public const long AVG_UNIT_PRICE_WEIGHT_PERCENT = 101542;
        public const long CHARGE_VARIABLE_UPDATE_INDEX = 101543;
        public const long PUBLIC_HOLIDAY_YEAR = 101544;
        public const long PUBLIC_HOLIDAY_MONTH = 101545;
        public const long PUBLIC_HOLIDAY_DAY_OF_MONTH = 101546;

        public const long PRICE_PREPAID_VARIABLE = 101440;
        public const long PRICE_POSTPAID_VARIABLE = 101441;
        public const long PREPAID_LAST_CREDIT_CHANGE = 101442;
        public const long PREPAID_LAST_CONSUMPTION_WEEKLY_AVG = 101443;
        public const long PREPAID_LAST_CONSUMPTION_DAILY_AVG = 101444;
        public const long PREPAID_TOTAL_TARIFF_CHANGE = 101445;
        public const long PREPAID_SCHEME_BASE = 101446;
        public const long PREPAID_SCHEME = 101447;
        public const long PREPAID_ARCHIVE_HOURLY_IDX = 101448;
        public const long PREPAID_ARCHIVE_DAILY_IDX = 101449;
        public const long LAST_APPLIED_EMERGENCY_CREDIT_DATETIME = 101450;

        public const long DEVICE_FAST_OEVE_FRAME_PERIOD = 101451;
        public const long DEVICE_FAST_OEVE_FRAME_ENABLE = 101452;
        public const long DEVICE_PREPAID_FRAME_PERIOD = 101453;
        public const long DEVICE_PREPAID_FRAME_ENABLE = 101454;
        public const long DEVICE_OFRAM_FRAME_PERIOD = 101455;
        public const long DEVICE_OFRAM_FRAME_ENABLE = 101456;
        public const long DEVICE_AIR_FRAME_PERIOD = 101457;
        public const long DEVICE_AIR_FRAME_ENABLE = 101458;
        public const long DEVICE_AIR_FAST_OEVE_FRAME_PERIOD = 101459;
        public const long DEVICE_AIR_FAST_OEVE_FRAME_ENABLE = 101460;
        public const long DEVICE_AIR_PREPAID_FRAME_PERIOD = 101461;
        public const long DEVICE_AIR_PREPAID_FRAME_ENABLE = 101462;
        public const long DEVICE_RADIO_433_WAKEUP_AWAITING_TIME = 101463;
        public const long DEVICE_RADIO_433_LISTENING_TIME = 101464;
        public const long DEVICE_AIR_WAKEUP_AWAITING_TIME = 101465;
        public const long DEVICE_AIR_LISTENING_TIME = 101466;
        public const long DEVICE_FRAME_DISPERSION_MODULO = 101467;

        public const long TRANSDUCER_CORRECTED_VALUE = 101468;

        public const long FUEL_LEVEL_APPROXIMATE = 101469;
        public const long FUEL_DENSITY_CORRECTED = 101470;
        public const long FUEL_LEVEL_AGGREGATED = 101471;
        public const long ULLAGE_AGGREGATED = 101472;
        public const long FUEL_CONSUMPTION = 101473;
        public const long FUEL_DENSITY_EXPECTED = 101474;
        public const long DELIVERY_VOLUME_APPROXIMATE_AGGREGATED = 101475;
        public const long FUEL_VOLUME_AGGREGATED = 101476;
        public const long FUEL_CONSUMPTION_AVG = 101477;

        public const long DEVICE_RETRANSMITTED_FRAMES_SMART_FILTER_ENABLED = 101476;

        public const long DEVICE_ERROR_BUTTON_SAFETY_ZONE = 101677;
        public const long DEVICE_ERROR_VALVE_MODULE_CONTROL_SAFETY_ZONE = 101678;
        public const long DEVICE_LATCH_ERROR_BUTTON_SAFETY_ZONE = 101679;
        public const long DEVICE_LATCH_ERROR_VALVE_MODULE_CONTROL_SAFETY_ZONE = 101680;

        public const long DEVICE_TIME_ZONE_OFFSET = 101681;
        public const long DEVICE_DAYLIGHT_SAVING_TIME_BEGIN = 101682;
        public const long DEVICE_DAYLIGHT_SAVING_TIME_END = 101683;
        public const long DEVICE_DAYLIGHT_SAVING_TIME_OFFSET = 101684;
        public const long DEVICE_DAYLIGHT_SAVING_TIME_ENABLED = 101685;

        public const long DEVICE_USART_DATA_VALIDITY_TIME = 101753;

        public const long DEVICE_ERROR_VALVE_IMRLAN_CONTROL_SAFETY_ZONE = 101787;
        public const long DEVICE_LATCH_ERROR_VALVE_IMRLAN_CONTROL_SAFETY_ZONE = 101788;
        public const long COVER_OPEN_COUNTER_TIMESTAMP = 101789;
        public const long COVER_OPEN_COUNTER_DURATION = 101790;
        public const long ENERGY_RAW = 101791;
        public const long DEVICE_ERROR_FLASH_LOG_WRITE = 101792;
        public const long DEVICE_ERROR_FLASH_MULTIREAD_WRITE = 101793;
        public const long DEVICE_ERROR_FLASH_ANALOG_ARCHIVE_WRITE = 101794;
        public const long DEVICE_ERROR_FLASH_COMMUNICATION = 101795;

        public const long DEVICE_BATTERY_CAPACITY = 101796;
        public const long DEVICE_BATTERY_MODEM_CURRENT = 101797;
        public const long DEVICE_BATTERY_RADIO_CURRENT = 101798;
        public const long DEVICE_BATTERY_SLEEP_CURRENT = 101799;
        public const long KEEP_ALIVE_ENABLED = 101800;
        public const long AVERAGE_TEMPERATURE = 101801;
        public const long LITER_COUNTER_END_DAY = 101802;

        public const long DEVICE_LATCH_ERROR_FLASH_LOG_WRITE = 101803;
        public const long DEVICE_LATCH_ERROR_FLASH_MULTIREAD_WRITE = 101804;
        public const long DEVICE_LATCH_ERROR_FLASH_ANALOG_ARCHIVE_WRITE = 101805;
        public const long DEVICE_LATCH_ERROR_FLASH_COMMUNICATION = 101806;


        public const long PULSE_COUNTER_DIFFERENCE = 101807;

        public const long DEVICE_ERROR_LOGGING = 101808;
        public const long DEVICE_LATCH_ERROR_LOGGING = 101809;
        public const long DEVICE_ERROR_MODEM = 101810;
        public const long DEVICE_LATCH_ERROR_MODEM = 101811;
        public const long DEVICE_ERROR_SACK = 102266;

        public const long DEVICE_TEST_GSM_CURRENT_ITEM_SCAN = 101812;
        public const long DEVICE_COUNTER_DM3_COEFFICIENT = 101813;
        public const long SABOTAGE_ACTIVE = 101814;
        public const long REMOVE_ACTIVE = 101815;
        public const long PULSE_COUNTER_FINAL = 101816;
        public const long MONTHLY_AVERAGE_TEMPERATURE = 101817;

        public const long DEVICE_MONTH_LATCH_ERROR_RTC = 101818;
        public const long DEVICE_MONTH_LATCH_ERROR_COUNTER_COPY = 101819;
        public const long DEVICE_MONTH_LATCH_ERROR_MAX_TEMP = 101820;
        public const long DEVICE_MONTH_LATCH_ERROR_MIN_TEMP = 101821;
        public const long DEVICE_MONTH_LATCH_ERROR_EE_COUNTER = 101822;
        public const long DEVICE_MONTH_LATCH_ERROR_EE_CONFIG = 101823;
        public const long DEVICE_MONTH_LATCH_ERROR_EE_DIAGNOSTIC = 101824;
        public const long DEVICE_MONTH_LATCH_ERROR_EE_DAYLONG_ARCHIVE = 101825;
        public const long DEVICE_MONTH_LATCH_ERROR_EE_MONTHLONG_ARCHIVE = 101826;
        public const long DEVICE_MONTH_LATCH_ERROR_SIM_CARD = 101827;
        public const long DEVICE_MONTH_ERROR_MODEM = 101828;
        public const long DEVICE_MONTH_LATCH_ERROR_MAX_FLOW = 101829;
        public const long DEVICE_MONTH_LATCH_ERROR_SABOTAGE_CONTACT = 101830;
        public const long DEVICE_MONTH_LATCH_ERROR_SABOTAGE_SWITCH = 101831;
        public const long DEVICE_MONTH_LATCH_ERROR_IR_FLAG = 101832;
        public const long DEVICE_MONTH_LATCH_ERROR_BATTERY_LOW = 101833;
        public const long DEVICE_MONTH_LATCH_ERROR_MONTH_DIFF_CONFIDENCE = 101834;
        public const long DEVICE_MONTH_LATCH_ERROR_DAY_DIFF_CONFIDENCE = 101835;
        public const long DEVICE_MONTH_LATCH_ERROR_COVER_OPEN = 101836;
        public const long DEVICE_MONTH_LATCH_ERROR_LOGGING = 101837;
        public const long DEVICE_MONTH_LATCH_ERROR_FLASH_LOG_WRITE = 101838;
        public const long DEVICE_MONTH_LATCH_ERROR_FLASH_MULTIREAD_WRITE = 101839;
        public const long DEVICE_MONTH_LATCH_ERROR_FLASH_ANALOG_ARCHIVE_WRITE = 101840;
        public const long DEVICE_MONTH_LATCH_ERROR_FLASH_COMMUNICATION = 101841;
        public const long DEVICE_MONTH_LATCH_ERROR_TRANSCEIVER = 102398;
        public const long DEVICE_MONTH_LATCH_ERROR_REMOVE = 102399;

        public const long DEVICE_MONTH_LATCH_ERROR_POWER_STATE = 102568;
        public const long DEVICE_MONTH_LATCH_ERROR_MODEM = 102569;
        public const long DEVICE_MONTH_LATCH_ERROR_EXTREME_TEMP = 102570;
        public const long DEVICE_MONTH_LATCH_ERROR_SACK = 102571;
        public const long DEVICE_MONTH_LATCH_ERROR_EEPROM = 102572;
        public const long DEVICE_MONTH_LATCH_COUNTER_ERROR = 102573;

        public const long DAILY_MAXIMUM_TEMPORARY_FLOW = 101842;
        public const long MONTHLY_MAXIMUM_TEMPORARY_FLOW = 101843;
        public const long DAILY_MAXIMUM_TEMPORARY_FLOW_TIME = 101844;
        public const long MONTHLY_MAXIMUM_TEMPORARY_FLOW_TIME = 101845;
        public const long MODEM_LOGGING_PENDING = 101846;
        public const long DEVICE_TIME_TO_WAKEUP = 101847;

        public const long RADIO_MODE_TIME = 101848;
        public const long MODEM_MODE_TIME = 101849;
        public const long SLEEP_MODE_TIME = 101850;

        public const long SIM_PIN_INCORRECT = 101851;
        public const long SIM_NOT_INSERTED = 101852;
        public const long SIM_WRONG = 101853;
        public const long SIM_FAILURE = 101854;
        public const long SIM_PUK_REQUIRED = 101855;

        public const long DEVICE_TEST_IMR_FINISH_CONDITION = 101856;
        public const long DEVICE_GSM_BAND = 101857;
        public const long MINIMUM_TEMPORARY_FLOW = 101858;
        public const long DEVICE_SMS_CME_ERROR = 101859;
        public const long DEVICE_SMS_CMS_ERROR = 102394;

        public const long DEVICE_ERROR_EE_DAYLONG_ARCHIVE = 101860;
        public const long DEVICE_ERROR_EE_MONTHLONG_ARCHIVE = 101861;
        public const long DEVICE_LATCH_ERROR_EE_DAYLONG_ARCHIVE = 101862;
        public const long DEVICE_LATCH_ERROR_EE_MONTHLONG_ARCHIVE = 101863;
        public const long DEVICE_TEST_IMR_PENDING = 101864;
        public const long MAXIMUM_TEMPORARY_FLOW = 101865;
        public const long MAXIMUM_TEMPORARY_FLOW_COUNT = 101866;

        public const long DEVICE_LCD_SLEEP_SCREEN_ENABLED = 101867;
        public const long INPUT_IMPULSE_COUNTER = 101868;

        public const long ATG_I_START_TIME = 101869;
        public const long ATG_I_END_TIME = 101870;

        public const long DEVICE_LITER_COUNTER_VALUE_VALID = 101871;
        public const long DEVICE_SCHEDULE_COMMAND_CODE = 101872;
        public const long DEVICE_SCHEDULE_COMMAND_EXECUTE = 101873;
        public const long DEVICE_SCHEDULE_COMMAND_PARAM = 101874;
        public const long DEVICE_PIC_MODEM_RESPONSE_TIMEOUT = 101875;
        public const long LITER_COUNTER_FINAL = 101876;
        public const long GAS_METER_USAGE_DIFFERENCE = 101877;
        public const long AUTO_GSM_BAND_ENABLED = 101878;

        public const long DEVICE_LOG_DATA = 101879;
        public const long DEVICE_QUERY_LOG_DATA = 101880;
        public const long DEVICE_SMS_UNSUCCESSFUL_SEND_TRY_TIMESTAMP = 101881;
        public const long DEVICE_SMS_UNSUCCESSFUL_SEND_TRY_STATUS = 101882;

        public const long ADDIDITONAL_WATERMETER_1_SLOPE = 101884;
        public const long ADDIDITONAL_WATERMETER_1_BIAS = 101885;
        public const long GAS_CONSUMPTION_HOURLY = 101886;

        public const long DEVICE_DIRECT_MEMORY_ACCESS_ADDRESS = 101892;
        public const long DEVICE_DIRECT_FLASH_ADDRESS = 101893;
        public const long DEVICE_DIRECT_DATA_LENGHT = 101894;
        public const long DEVICE_DIRECT_DATA = 101895;
        public const long DEVICE_DIRECT_EEPROM_ADDRESS = 101896;
        public const long DEVICE_DIRECT_EEPROM_DATA_LENGHT = 101897;
        public const long DEVICE_DIRECT_EEPROM_DATA = 101898;

        public const long DEVICE_UP_EVENT_MASK = 101899;
        public const long DEVICE_DOWN_EVENT_MASK = 101900;
        public const long DEVICE_EVENTS_LOGGING_ENABLE = 101901;

        public const long DEVICE_WL_LEARNING_TIME = 101902;
        public const long DEVICE_FORCED_GSM_BAND = 101903;

        public const long DEVICE_RADIO_MODULE_READOUT_DURATION = 101904;
        public const long DEVICE_RADIO_MODULE_READINGSINFO_OFFSET = 101905;
        public const long DEVICE_RADIO_MODULE_ADVANCED_LOGIC_ENABLED = 101906;
        public const long DEVICE_RADIO_MODULE_READOUT_PERIOD = 101907;

        public const long DEVICE_WL_SLOT_LINKED = 101908;

        public const long LEVEL1 = 101909;
        public const long LEVEL2 = 101910;
        public const long LEVEL3 = 101911;
        public const long LEVEL4 = 101912;
        public const long LEVEL5 = 101913;
        public const long LEVEL6 = 101914;
        public const long DEVICE_ANALOG_ARCHIVE1_DATA = 101915;
        public const long DEVICE_ANALOG_ARCHIVE2_DATA = 101916;
        public const long DEVICE_ANALOG_ARCHIVE6_DATA = 101917;

        public const long DEVICE_EVENT_RAPID_UP_ENABLE = 101918;
        public const long DEVICE_EVENT_RAPID_DOWN_ENABLE = 101919;

        public const long DEVICE_FREQ_CONFIG_OMB_CUSTOM_NUMBER = 101920;
        public const long DEVICE_FREQ_CONFIG_OMB_TABLE_NUMBER = 101921;
        public const long DEVICE_FREQ_CONFIG_OMB_TABLE_RECORDS = 101922;
        public const long DEVICE_FREQ_CONFIG_OMB_TABLE_TIME_OFFSET = 101923;

        public const long ALEVEL_CONFIG_ANALOG_FUNCTION_LINKED = 101924;
        public const long DEVICE_ANALOG_ARCHIVE_DATA_VALID_MARK = 101925;

        public const long IMR_TEST_MODE = 101926;
        public const long UTC_SYNCHRONIZE_OFFSET = 101927;

        public const long WALKEDBY_ID_OPERATOR = 101928;
        public const long WALKEDBY_LATITUDE = 101929;
        public const long WALKEDBY_LONGITUDE = 101930;

        public const long DEVICE_DIRECT_MEMORY_TYPE = 101931;
        public const long COMPACT_ANALOG_ARCHIVE_LATCH_PERIOD = 101932;
        public const long COMPACT_ANALOG_ARCHIVE_REGISTRY_COUNT = 101933;
        public const long COMPACT_ANALOG_ARCHIVE_ENABLE = 101934;

        public const long DEVICE_ANALOG_EVENTS_ACTIVE = 101945;

        public const long DEVICE_TEST_IMR_DURATION = 101946;
        public const long DEVICE_TEST_IMR_DURATION_EXTENSION = 101947;
        public const long DEVICE_TEST_IMR_PRERUNING_DURATION = 101948;

        public const long DEVICE_WMBUS_FREQUENCY_VALUE = 101951;
        public const long DEVICE_WMBUS_SELF_FRAME_DISPERSION = 101953;
        public const long DEVICE_WMBUS_LISTENING_PREAMBLE = 101954;
        public const long DEVICE_WMBUS_RESPONSE_PREAMBLE = 101955;
        public const long DEVICE_WMBUS_RESPONSE_DELAY = 101956;
        public const long DEVICE_WMBUS_SELF_FRAME_PREAMBLE = 101957;
        public const long DEVICE_WMBUS_SELF_FRAME_PERIOD = 101958;
        public const long DEVICE_WMBUS_UPSTREAM_TRANSMISSION_POWER = 101959;
        public const long DEVICE_WMBUS_LAT_TIME = 101960;
        public const long DEVICE_WMBUS_LAT_PREAMBLE = 101961;
        public const long DEVICE_WMBUS_MODE_VALUE = 101962;

        public const long DEVICE_SENT_RADIO_PACKETS_COUNT = 101963;
        public const long DEVICE_RECEIVED_RADIO_PACKETS_COUNT = 101964;
        public const long LITER_COUNTER_END_MONTH = 101965;

        public const long DEVICE_ERROR_TRANSCEIVER = 101966;
        public const long DEVICE_LATCH_ERROR_TRANSCEIVER = 101967;
        public const long DEVICE_DISPLAY_VOLUME_ENABLED = 101968;

        public const long DEVICE_SABOTAGE_DETECTION_ENABLED = 101969;
        public const long DEVICE_REMOVE_DETECTION_ENABLED = 101970;

        public const long DEVICE_SACK_REQUEST = 101971;
        public const long DEVICE_SACK_TIME_RANGE = 101972;
        public const long DEVICE_SACK_TIME_OFFSET = 101973;
        public const long DEVICE_KNOWN_NODES_REQUEST = 102246;
        public const long DEVICE_KNOWN_NODES_TIME_RANGE = 102253;
        public const long DEVICE_KNOWN_NODES_TIME_OFFSET = 102254;
        public const long DEVICE_SACK_SEARCH_TIME_RANGE = 102396;
        public const long DEVICE_SACK_SEARCH_TIME_OFFSET = 102397;


        public const long DEVICE_WMBUS_WALKBY_FRAME_PREAMBLE = 101974;
        public const long DEVICE_WMBUS_WALKBY_FRAME_PERIOD = 101975;

        public const long DEVICE_SEND_FREQ_AFTER_PUT_ENABLED = 101982;
        public const long DEVICE_BATTERY_CONSUMPTION_OFFSET = 101983;
        public const long DIFFERENCE_LATCH_HOUR = 101984;
        public const long DEVICE_DISPLAY_METER_ENABLED = 101986;
        public const long DEVICE_COVER_OPEN_DETECTION_ENABLED = 101987;
        public const long DEVICE_COVER_OPEN_ACTIVE_STATE = 101988;
        public const long DEVICE_GPRS_ACTIVE = 101989;

        public const long DEVICE_UP_EVENT_RTC = 101990;
        public const long DEVICE_UP_EVENT_COUNTER_COPY = 101991;
        public const long DEVICE_UP_EVENT_MAX_TEMP = 101992;
        public const long DEVICE_UP_EVENT_MIN_TEMP = 101993;
        public const long DEVICE_UP_EVENT_EE_COUNTER = 101994;
        public const long DEVICE_UP_EVENT_EE_CONFIG = 101995;
        public const long DEVICE_UP_EVENT_EE_DIAGNOSTIC = 101996;
        public const long DEVICE_UP_EVENT_EE_DAYLONG_ARCHIVE = 101997;
        public const long DEVICE_UP_EVENT_EE_MONTHLONG_ARCHIVE = 101998;
        public const long DEVICE_UP_EVENT_SIM_CARD = 101999;
        public const long DEVICE_UP_EVENT_MODEM = 102001;
        public const long DEVICE_UP_EVENT_MAX_FLOW = 102002;
        public const long DEVICE_UP_EVENT_SABOTAGE_CONTACT = 102003;
        public const long DEVICE_UP_EVENT_SABOTAGE_SWITCH = 102004;
        public const long DEVICE_UP_EVENT_IR_FLAG = 102005;
        public const long DEVICE_UP_EVENT_BATTERY_LOW = 102006;
        public const long DEVICE_UP_EVENT_MONTH_DIFF_CONFIDENCE = 102007;
        public const long DEVICE_UP_EVENT_DAY_DIFF_CONFIDENCE = 102008;
        public const long DEVICE_UP_EVENT_COVER_OPEN = 102009;
        public const long DEVICE_UP_EVENT_LOGGING = 102010;
        public const long DEVICE_UP_EVENT_FLASH_LOG_WRITE = 102011;
        public const long DEVICE_UP_EVENT_FLASH_MULTIREAD_WRITE = 102012;
        public const long DEVICE_UP_EVENT_FLASH_ANALOG_ARCHIVE_WRITE = 102013;
        public const long DEVICE_UP_EVENT_FLASH_COMMUNICATION = 102014;
        public const long DEVICE_UP_EVENT_TRANSCEIVER = 102400;

        public const long DEVICE_DOWN_EVENT_RTC = 102015;
        public const long DEVICE_DOWN_EVENT_COUNTER_COPY = 102016;
        public const long DEVICE_DOWN_EVENT_MAX_TEMP = 102017;
        public const long DEVICE_DOWN_EVENT_MIN_TEMP = 102018;
        public const long DEVICE_DOWN_EVENT_EE_COUNTER = 102019;
        public const long DEVICE_DOWN_EVENT_EE_CONFIG = 102020;
        public const long DEVICE_DOWN_EVENT_EE_DIAGNOSTIC = 102021;
        public const long DEVICE_DOWN_EVENT_EE_DAYLONG_ARCHIVE = 102022;
        public const long DEVICE_DOWN_EVENT_EE_MONTHLONG_ARCHIVE = 102023;
        public const long DEVICE_DOWN_EVENT_SIM_CARD = 102024;
        public const long DEVICE_DOWN_EVENT_MODEM = 102025;
        public const long DEVICE_DOWN_EVENT_MAX_FLOW = 102026;
        public const long DEVICE_DOWN_EVENT_SABOTAGE_CONTACT = 102027;
        public const long DEVICE_DOWN_EVENT_SABOTAGE_SWITCH = 102028;
        public const long DEVICE_DOWN_EVENT_IR_FLAG = 102029;
        public const long DEVICE_DOWN_EVENT_BATTERY_LOW = 102030;
        public const long DEVICE_DOWN_EVENT_MONTH_DIFF_CONFIDENCE = 102031;
        public const long DEVICE_DOWN_EVENT_DAY_DIFF_CONFIDENCE = 102032;
        public const long DEVICE_DOWN_EVENT_COVER_OPEN = 102033;
        public const long DEVICE_DOWN_EVENT_LOGGING = 102034;
        public const long DEVICE_DOWN_EVENT_FLASH_LOG_WRITE = 102035;
        public const long DEVICE_DOWN_EVENT_FLASH_MULTIREAD_WRITE = 102036;
        public const long DEVICE_DOWN_EVENT_FLASH_ANALOG_ARCHIVE_WRITE = 102037;
        public const long DEVICE_DOWN_EVENT_FLASH_COMMUNICATION = 102038;
        public const long DEVICE_DOWN_EVENT_TRANSCEIVER = 102401;

        public const long LITER_COUNTER_SET_VALUE = 102039;
        public const long DEVICE_POWER_CONFIG_FORCE_AC = 102045;
        public const long PROBE_RECEIVED_FRAMES_COUNT = 102046;
        public const long PROBE_MISSED_FRAMES_COUNT = 102047;
        public const long PROBE_RESET_COUNT = 102048;
        public const long DEVICE_SABOTAGE_ACTIVE_STATE = 102049;
        public const long DEVICE_USART_RESET_FRAME_COUNTERS_PERIOD = 102050;
        public const long WRONG_SAMPLE_COUNTER = 102052;
        public const long PROBE_PULSE_COUNTER = 102053;
        public const long PROBE_FLOATS_COUNT = 102054;
        public const long PROBE_FRAME_NUMBER = 102055;
        public const long PROBE_RECEIVE_EFFICIENCY = 102056;
        public const long USART_LAST_INTERRUPT_REQUEST = 102057;
        public const long DEVICE_USART_STATE = 102058;
        public const long MAGNETO_STATUS = 102059;
        public const long USART_DATA_VALIDITY = 102060;

        public const long DEVICE_GSM_BAND_900_1800_POWER_LIMIT = 102061;
        public const long DEVICE_GSM_BAND_1900_POWER_LIMIT = 102062;
        public const long DEVICE_GSM_BAND_850_POWER_LIMIT = 102063;

        public const long DEVICE_WMBUS_DOWNSTREAM_FRAME_PREAMBLE = 102064;
        public const long DEVICE_WMBUS_DOWNSTREAM_TRANSMISSION_POWER = 102065;
        public const long DEVICE_WMBUS_UPSTREAM_MAX_DELAYS_COUNT = 102066;
        public const long DEVICE_WMBUS_UPSTREAM_MAX_DELAY_DURATION = 102067;

        public const long DEVICE_FTP_SERVER_NAME = 102068;
        public const long DEVICE_FTP_SERVER_LOGIN = 102069;
        public const long DEVICE_FTP_SERVER_PASSWORD = 102070;
        public const long DEVICE_FTP_SERVER_PATH_FOR_PACKETS = 102071;

        public const long DEVICE_SACK_ENABLE = 102072;
        public const long DEVICE_SACK_SEND_INFO_AFTER_UPDATE = 102073;
        public const long DEVICE_SACK_SEND_MODE = 102074;

        public const long DEVICE_MULTIREAD_REQUEST = 102075;
        public const long DEVICE_MULTIREAD_TIME_RANGE = 102076;
        public const long DEVICE_MULTIREAD_WINDOWS_COUNT_OFFSET = 102077;

        public const long DEVICE_SACK_UNIQUE_TIME_OFFSET = 102255;
        public const long DEVICE_SACK_UNIQUE_TIME_RANGE = 102256;
        public const long DEVICE_SACK_UNIQUE_WINDOWS_COUNT = 102257;

        public const long DEVICE_SACK_ARCHIVE_TIME_OFFSET = 102286;
        public const long DEVICE_SACK_ARCHIVE_TYPE = 102287;
        public const long DEVICE_SACK_ARCHIVE_WINDOWS_COUNT = 102288;

        public const long DEVICE_WMBUS_MODE_DURATION = 102078;
        public const long DEVICE_WMBUS_AVERAGE_CURRENT = 102079;

        public const long DEVICE_MULTIHOP_CONFIG_ADDRESS = 102080;
        public const long DEVICE_MULTIHOP_CONFIG_NODES_RELAY_ON = 102081;
        public const long DEVICE_MULTIHOP_CONFIG_NODES_RELAY_ALL = 102082;
        public const long DEVICE_MULTIHOP_CONFIG_GATEWAY_RELAY_ON = 102083;
        public const long DEVICE_MULTIHOP_CONFIG_GATEWAY_RELAY_ALL = 102084;

        public const long DEVICE_WMBUS_WALK_BY_FRAMES_REQUEST = 102085;
        public const long DEVICE_WMBUS_WALK_BY_FRAMES_COUNT = 102086;
        public const long DEVICE_WMBUS_WALK_BY_FRAMES_TRANSMISSION_DELAY = 102087;
        public const long DEVICE_WMBUS_WALK_BY_FRAMES_WINDOW_PERIOD = 102088;
        public const long DEVICE_WMBUS_WALK_BY_FRAMES_WINDOW_DURATION = 102089;

        public const long DEVICE_SELF_FRAME_ENCRYPTION_ENABLED = 102090;
        public const long DEVICE_WALK_BY_FRAME_ENCRYPTION_ENABLED = 102091;
        public const long DEVICE_FREQ_TEMP_OFFSET_ENABLED = 102402;

        public const long DEVICE_SACK_INFO_FTP_SERVER_NAME = 102092;
        public const long DEVICE_SACK_INFO_FTP_SERVER_PATH = 102093;
        public const long DEVICE_SACK_INFO_FTP_FILE = 102094;

        public const long DEVICE_SYNCHRONIZE_MODEM_FROM_NITZ = 102095;

        public const long DEVICE_EVENT_RAPID_UP_AVG_EVENT = 102096;
        public const long DEVICE_EVENT_RAPID_DOWN_AVG_EVENT = 102097;
        public const long DEVICE_MULTIREAD_TIME_OFFSET = 102098;

        public const long DEVICE_BOOTLOADER_FIRMWARE_CONFIG = 102099;
        public const long DEVICE_BOOTLOADER_FIRMWARE_TYPE = 102100;
        public const long DEVICE_BOOTLOADER_BUILD_DATE = 102101;
        public const long BATTERY_VOLUME_USAGE_LATCHED = 102102;
        public const long DEVICE_ANALOG_MIN_VALUE = 102103;
        public const long DEVICE_ANALOG_MAX_VALUE = 102104;
        public const long DEVICE_CLEAR_STATUS_AFTER_FREQ = 102105;
        public const long DEVICE_ACTIVATION_MAX_DURATION = 102106;
        public const long DEVICE_DELIVERY_REPOROT_TIMEOUT_FOR_SYNCHRONISATION = 102107;
        public const long SECONDARY_GAS_METER_USAGE_VALUE = 102108;
        public const long SECONDARY_LITER_COUNTER = 102109;
        public const long SECONDARY_PULSE_COUNTER = 102110;

        public const long DEVICE_BOOTLOADER_VERSION = 102111;
        public const long DEVICE_EVENTS_DISABLE_TIMEOUT = 102112;
        public const long GPS_TIMER_TIMESTAMP = 102113;
        public const long GPS_TIMER_DURATION = 102114;
        public const long DEVICE_WAYNE_COMMAND_BLOCK_ID = 102115;
        public const long DEVICE_RTC_ERROR_COUNTER = 102116;
        public const long DEVICE_SACK_SEND_TIMEOUT = 102117;
        public const long DEVICE_MULTIDATAGRAM_MAX_SIZE = 102118;
        public const long DEVICE_GPRS_AUTO_CONNECT = 102120;
        public const long DEVICE_BATTERY_GPS_CURRENT = 102121;

        public const long ALEVEL_CONFIG_ARCHIVE_MASK_CURRENT_H = 102122;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_CURRENT_L = 102123;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_AVG_H = 102124;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_AVG_L = 102125;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MAX_H = 102126;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MAX_L = 102127;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MIN_H = 102128;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MIN_L = 102129;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MEASURES_COUNT_H = 102130;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_MEASURES_COUNT_L = 102131;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_BASE_VALUE = 102132;
        public const long ALEVEL_CONFIG_ARCHIVE_MASK_OPERATION_MULTIPLY = 102133;
        public const long DEVICE_GPS_ENABLED = 102134;
        public const long DEVICE_GPS_TIME_SYNCHRONISATION_ENABLED = 102135;
        public const long DEVICE_TIME_TO_GPS_SENDING_ENABLED = 102136;
        public const long DEVICE_GPS_SEEK_SIGNAL_TIMEOUT = 102137;
        public const long DEVICE_GPS_FIX_QUALITY = 102138;
        public const long DEVICE_GPS_ON = 102139;
        public const long DEVICE_GPS_LAST_COORDINATES_SENT = 102140;
        public const long DEVICE_GPS_MEASUREMENT_LATITUDE = 102141;
        public const long DEVICE_GPS_MEASUREMENT_LONGITUDE = 102142;
        public const long DEVICE_GPS_MEASUREMENT_ALTITUDE = 102143;
        public const long DEVICE_GPS_LATITUDE_POLE = 102144; //1-N,0-S
        public const long DEVICE_GPS_LONGITUDE_POLE = 102145;//1-E,0-W
        public const long DEVICE_GPS_LAST_FIX_DURATION = 102146;
        public const long DEVICE_GPS_LAST_FIX_TIMESTAMP = 102147;
        public const long DEVICE_GPS_POWER_ON_DURATION = 102148;//??
        public const long DEVICE_GPS_POWER_ON_COUNTER = 102149;//??
        public const long DEVICE_GPS_SATELLITES_IN_USE_COUNT = 102150;
        public const long DEVICE_EXTREME_ADC_VALUE_TOLERANCE = 102151;
        public const long DEVICE_ACQUISITION_PERIOD = 102152;
        public const long ADC_VALUE = 102153;
        public const long ADC_VALUE_ERROR = 102154;
        public const long ADC_POLYLINE_CONVERTED_VALUE = 102155;
        public const long ADC_NORMALIZED_VALUE = 102156;
        public const long ADC_LINEAR_CONVERTED_VALUE = 102157;
        public const long ADC_POLYLINE_CONVERTED_VALUE_VALID = 102158;
        public const long ADC_NORMALIZED_VALUE_VALID = 102159;
        public const long ADC_LINEAR_CONVERTED_VALUE_VALID = 102160;
        public const long DEVICE_POLYLINE_ELEMENT_X = 102161;
        public const long DEVICE_POLYLINE_ELEMENT_Y = 102162;
        public const long DEVICE_READ_SENSOR_DATA = 102163;
        public const long DEVICE_NORMALISATION_MAX_VALUE = 102164;
        public const long DEVICE_LINEAR_CONVERSION_PARAM_A_MULTIPLIER = 102165;
        public const long DEVICE_LINEAR_CONVERSION_PARAM_A_DIVIDER = 102166;
        public const long DEVICE_LINEAR_CONVERSION_PARAM_B = 102167;
        public const long ANALOG_READOUT_DATA_TYPE = 102168;
        public const long AVG_ANALOG_READOUT_DATA_TYPE = 102169;
        public const long MIN_ANALOG_READOUT_DATA_TYPE = 102170;
        public const long MAX_ANALOG_READOUT_DATA_TYPE = 102171;
        public const long TEMPERATURE_ATM = 102172;
        public const long PRESSURE_ATM = 102173;
        public const long PRESSURE = 102174;
        public const long ANALOG_VALUE = 102175;
        public const long ATM_VALUE_VALID = 102176;
        public const long DEVICE_ATM_SENSOR_ENABLED = 102177;
        public const long DEVICE_ATM_ACQUISITION_PERIOD = 102178;
        public const long ANALOG_TEMPERATURE_VALID = 102179;
        public const long ANALOG_BATTERY_VALID = 102180;
        public const long ANALOG_SIGNAL_VALID = 102181;
        public const long DEVICE_GPS_INIT_STATE = 102182;
        public const long ANALOG_VALUE_SLOPE = 102183;
        public const long DEVICE_GPS_COMMUNICATION_VALID = 102184;
        public const long DEVICE_GPS_MEASUREMENT_LATITUDE_DEGREES = 102185;
        public const long DEVICE_GPS_MEASUREMENT_LATITUDE_MINUTES = 102186;
        public const long DEVICE_GPS_MEASUREMENT_LATITUDE_SECONDS = 102187;
        public const long DEVICE_GPS_MEASUREMENT_LONGITUDE_DEGREES = 102188;
        public const long DEVICE_GPS_MEASUREMENT_LONGITUDE_MINUTES = 102189;
        public const long DEVICE_GPS_MEASUREMENT_LONGITUDE_SECONDS = 102190;
        public const long DEVICE_CL_ENABLE = 102191;
        public const long MAX_PRESSURE = 102194;
        public const long AVG_PRESSURE = 102195;
        public const long MAX_PRESSURE_ATM = 102196;
        public const long AVG_PRESSURE_ATM = 102197;
        public const long MAX_TEMPERATURE_ATM = 102198;
        public const long AVG_TEMPERATURE_ATM = 102199;
        public const long MIN_PRESSURE = 102200;
        public const long MIN_PRESSURE_ATM = 102201;
        public const long MIN_TEMPERATURE_ATM = 102202;
        public const long ANALOG_AVG_VALUE = 102204;
        public const long ANALOG_MAX_VALUE = 102205;
        public const long ANALOG_MIN_VALUE = 102206;
        public const long ARCHIVE_LAST_LATCH_COUNTER = 102207;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_REQUEST = 102208;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_VALUE_MULTIPLIER = 102209;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_VALUE_DIVIDER = 102210;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_ACQUIRE_DIVIDER = 102211;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_SLOT_LINKED = 102212;
        public const long DEVICE_COMPACT_ANALOG_ARCHIVE_VALID_MARK = 102213;
        public const long DEVICE_LOG_ARCHIVE_VALID_MARK = 102214;
        public const long DEVICE_COMPACT_ANALOG_ARCHIVE_DEFAULT_ITEMS_COUNT = 102215;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_VALUE_SLOPE = 102222;
        public const long DEVICE_ANALOG_LOG_ARCHIVE_CONFIG_READOUT_DATA_TYPE = 102223;
        public const long DEVICE_START_GPS_WITH_IMR_TEST = 102224;
        public const long DEVICE_SACK_SEND_TIME_RANGE = 102225;
        public const long DEVICE_SACK_SEND_TIMESTAMP_OFFSET = 102226;
        public const long DEVICE_SACK_SEND_RADIO_ADDRESS = 102227;
        public const long DEVICE_WMBUS_FREQUENCY_CORRECTION = 102336;
        public const long DEVICE_WMBUS_FREQUENCY_CORRECTION_0 = 102231;
        public const long DEVICE_WMBUS_FREQUENCY_CORRECTION_1 = 102232;
        public const long DEVICE_BUSTER_HGM_TX_ENBALE = 102233;
        public const long DEVICE_BUSTER_HGM_RX_ENBALE = 102234;
        public const long DEVICE_WMBUS_ATTEMPT_TIMEOUT = 102235;
        public const long DEVICE_WMBUS_ATTEMPT_COUNT = 102236;
        public const long WMBUS_FRAME = 102237;
        public const long WMBUS_FRAME_SEND_MODE = 102238;
        public const long DEVICE_WMBUS_CUSTOM_R2S_TIMEOUT = 102239;
        public const long DEVICE_WMBUS_CUSTOM_DOWNSTREAM_FRAME_PREAMBLE = 102240;
        public const long DEVICE_WMBUS_CUSTOM_DOWNSTREAM_TRANSMISSION_POWER = 102241;
        public const long DEVICE_FILTERED_SACK_FUNCTION_DEFAULT = 102242;
        public const long DEVICE_FILTERED_SACK_FUNCTION_FOR_OLD_FRAME_FORMAT = 102243;
        public const long DEVICE_FILTERED_SACK_FUNCTION_FOR_FRAME_TYPE = 102244;
        public const long DEVICE_FILTERED_SACK_FUNCTION_WMBUS_IW2_DATA_FRAME = 102247;
        public const long DEVICE_FILTERED_SACK_FUNCTION_WMBUS_CYCLIC_FRAME = 102248;
        public const long DEVICE_FILTERED_SACK_FUNCTION_WMBUS_WALKBY_FRAME = 102249;
        public const long DEVICE_FILTERED_SACK_FUNCTION_WMBUS_CRYPTED_CYCLIC_FRAME = 102250;
        public const long DEVICE_FILTERED_SACK_FUNCTION_WMBUS_CRYPTED_WALKBY_FRAME = 102251;
        public const long DEVICE_SACK_TIMESTAMP = 102262;
        public const long DEVICE_SACK_RADIO_QUALITY = 102263;
        public const long DEVICE_SACK_LENGTH = 102264;
        public const long DEVICE_SACK_DATA = 102265;
        public const long DEVICE_REMOVE_ACTIVE_STATE = 102267;
        public const long DEVICE_FILTERED_SACK_FUNCTION_FOR_OLD_FRAME_FORMAT_VIFE = 102395;

        public const long AWSR_CALIBRATION_PERFORMED = 102268;
        public const long AWSR_CALIBRATION_REF_CUMULATIVE_VARIANCE = 102269;
        public const long AWSR_CALIBRATION_REF_CUMULATIVE_PER_VARIANCE = 102270;

        public const long DEVICE_CUSTOM_WAN_PROTOCOL_SUPPORT = 102271;
        public const long LINK_QUALITY_INDICATION = 102272;
        public const long RECEIVED_SIGNAL_STRENGTH_INDICATION = 102273;

        public const long AWSR_CALIBRATION_RATING_ERROR = 102277;
        public const long AWSR_CALIBRATION_RATING_PER_ERROR = 102278;
        public const long AWSR_CALIBRATION_REF_RATING_ERROR = 102279;
        public const long AWSR_CALIBRATION_REF_RATING_PER_ERROR = 102280;
        public const long AWSR_CALIBRATION_RATING_TIME = 102281;
        public const long AWSR_CALIBRATION_RATING_FREQUENCY = 102282;
        public const long AWSR_PROBLEM_START_FUEL_HEIGHT = 102283;
        public const long AWSR_PROBLEM_END_FUEL_HEIGHT = 102284;
        public const long AWSR_PROBLEM_REF_RATING_PER_ERROR = 102285;
        public const long AWSR_PROBLEM_REF_RATING_ERROR = 102290;

        public const long AWSR_CALIBRATION_RATING_PERIOD = 103003;
        public const long AWSR_CALIBRATION_RATING_IS_EXACT = 103004;
        public const long AWSR_CALIBRATION_RATING_TH_PERC = 103005;
        public const long AWSR_CALIBRATION_RATING_SQUARED = 103006;
        public const long AWSR_CALIBRATION_RATING_MIN_WINDOW_SALE = 103007;
        public const long AWSR_CALIBRATION_RATING_TABLE_POINTS = 103008;
        public const long AWSR_CALIBRATION_RATING_THREADS_NUM = 103009;
        public const long AWSR_CALIBRATION_RATING_AGGREGATION_ID = 103010;
        public const long AWSR_CALIBRATION_RATING_LOCATION_STATE_IDS = 103011;
        public const long AWSR_CALIBRATION_RATING_CALIBRATION_TABLE_TIME = 103012;
        public const long AWSR_CALIBRATION_RATING_METER_PER_CYCLE = 103013;
        public const long AWSR_CALIBRATION_RATING_LOCATION_IDS = 103014;

        public const long AWSR_FP_EVENT_START_TIME = 103016;
        public const long AWSR_FP_EVENT_END_TIME = 103017;
        public const long AWSR_FP_EVENT_CLASS = 103018;
        public const long AWSR_FP_EVENT_VOLUME = 103019;
        public const long AWSR_FP_EVENT_NOZZLE_NUMBER = 103020;
        public const long AWSR_FP_EVENT_REASON = 103021;
        public const long AWSR_FP_EVENT_DISCHARGED = 103022;
        public const long AWSR_FP_EVENT_NOTES = 103023;
        public const long AWSR_FP_EVENT_STATUS = 103024;
        public const long AWSR_FP_EVENT_DISCHARGED_VOLUME = 103025;
        public const long AWSR_FP_EVENT_VOLUME_LOSS = 103038;
        public const long AWSR_FP_EVENT_ALL_FUEL_RETURNED = 103043;

        public const long LOG_EVENT_TYPE = 102289;
        public const long RESET_REASON = 102291;
        public const long PRESENTATION_COUNTER = 102292;
        public const long MODEM_MACHINE_STEP = 102293;
        public const long MODEM_STATE_BITS = 102294;
        public const long IMR_SERVER_RADIO_RESULT = 102295;
        public const long LOG_EVENT_DATA = 102452;

        public const long VALDRIVER_OPERATION_RESULT = 102300;

        public const long WIND_SPEED = 102305;
        public const long AIR_HUMIDITY = 102306;
        public const long LAUNCH_TIME = 102307;
        public const long WEATHER_FORECAST_SERVICE = 102309;

        public const long DEVICE_EXTREME_MIN_TEMPERATURE = 102310;
        public const long DEVICE_EXTREME_MAX_TEMPERATURE = 102311;
        public const long DEVICE_MODEM_MIN_TEMPERATURE = 102312;
        public const long DEVICE_MODEM_MAX_TEMPERATURE = 102313;
        public const long DEVICE_HISTERESIS_TEMPERATURE = 102314;
        public const long DEVICE_VALVE_MIN_TEMPERATURE = 102315;
        public const long DEVICE_VALVE_MAX_TEMPERATURE = 102316;
        public const long DEVICE_VALVE_PIN_CYCLIC = 102317;
        public const long DEVICE_EXTENDED_MENU_PIN_CONSTANT = 102318;
        public const long DEVICE_EXTENDED_MENU_PIN_CYCLIC = 102319;
        public const long DEVICE_LCD_TIME_TO_MAINSCREEN = 102320;
        public const long DEVICE_LCD_NUMERIC_SEGMENTS_DATA = 102321;
        public const long DEVICE_LCD_SYMBOL_SEGMENTS_DATA = 102322;

        public const long DEVICE_ERROR_POWER_STATE = 102323;
        public const long DEVICE_ERROR_EXTREME_TEMP = 102324;
        public const long DEVICE_ERROR_VALVE_CLOSED_LEAKAGE = 102325;
        public const long DEVICE_ERROR_VALVE_OPERATION_ERROR = 102326;
        public const long DEVICE_ERROR_VALVE_NOT_DETECTED = 102327;
        public const long DEVICE_COUNTER_ERROR = 102328;
        public const long DEVICE_ERROR_EEPROM = 102342;
        public const long DEVICE_LATCH_ERROR_EEPROM = 102343;
        public const long DEVICE_LATCH_ERROR_POWER_STATE = 102329;
        public const long DEVICE_LATCH_ERROR_EXTREME_TEMP = 102330;
        public const long DEVICE_LATCH_ERROR_SACK = 102331;
        public const long DEVICE_LATCH_ERROR_VALVE_CLOSED_LEAKAGE = 102332;
        public const long DEVICE_LATCH_ERROR_VALVE_OPERATION_ERROR = 102333;
        public const long DEVICE_LATCH_ERROR_VALVE_NOT_DETECTED = 102334;
        public const long DEVICE_LATCH_COUNTER_ERROR = 102335;
        public const long VALDRIVER_PHYSICAL_STATE = 102339;
        public const long VALDRIVER_ERROR_CODE = 102340;
        public const long VALDRIVER_COMMAND = 102341;
        public const long VALDRIVER_LAST_ERROR_CODE = 102344;
        public const long VALDRIVER_COMMAND_IS_RUNNING = 102346;
        public const long VALDRIVER_COMMAND_EXECUTE = 102347;
        public const long VALDRIVER_RUNNING_COMMAND = 102348;
        public const long VALDRIVER_EXTERNAL_COMMAND_DATA = 102349;
        public const long VALDRIVER_RETRIES_LEFT = 102350;
        public const long VALDRIVER_REPEAT_LEFT = 102351;
        public const long VALDRIVER_LOG_DATA = 102352;
        public const long DEVICE_UP_EVENT_REMOVE = 102353;
        public const long DEVICE_UP_EVENT_EXTREME_TEMP = 102354;
        public const long DEVICE_UP_EVENT_SACK = 102355;
        public const long DEVICE_UP_EVENT_EEPROM = 102356;
        public const long DEVICE_UP_EVENT_VALVE_CLOSED_LEAKAGE = 102357;
        public const long DEVICE_UP_EVENT_VALVE_OPERATION_ERROR = 102358;
        public const long DEVICE_UP_EVENT_VALVE_NOT_DETECTED = 102359;
        public const long DEVICE_UP_EVENT_COUNTER_ERROR = 102360;
        public const long DEVICE_DOWN_EVENT_REMOVE = 102361;
        public const long DEVICE_DOWN_EVENT_EXTREME_TEMP = 102362;
        public const long DEVICE_DOWN_EVENT_SACK = 102363;
        public const long DEVICE_DOWN_EVENT_EEPROM = 102364;
        public const long DEVICE_DOWN_EVENT_VALVE_CLOSED_LEAKAGE = 102365;
        public const long DEVICE_DOWN_EVENT_VALVE_OPERATION_ERROR = 102366;
        public const long DEVICE_DOWN_EVENT_VALVE_NOT_DETECTED = 102367;
        public const long DEVICE_DOWN_EVENT_COUNTER_ERROR = 102368;
        public const long VALDRIVER_HIGH_CURRENT_CUT_OFF_COUNT = 102369;
        public const long DEVICE_MODEM_HIGH_CURRENT_CUT_OFF_COUNT = 102370;
        public const long VALDRIVER_LOGICAL_ERROR_CODE = 102374;
        public const long VALDRIVER_CLOSE_ON_CLOSED_LAKAGE_COUNT = 102383;

        public const long DEVICE_LOCAL_TIME_ZONE_MARK = 102385;
        public const long DEVICE_LOCAL_TIME_ZONE_OFFSET = 102386;
        public const long RESET_BOR_COUNTER = 102387;
        public const long RESET_BOR_COUNTER_TIMESTAMP = 102388;
        public const long LVD_EVENT_COUNTER = 102389;
        public const long LVD_EVENT_COUNTER_TIMESTAMP = 102390;
        public const long MEMORY_INTEGRITY_ERROR_COUNTER = 102391;
        public const long MEMORY_INTEGRITY_ERROR_COUNTER_TIMESTAMP = 102392;
        public const long WEATHER_STATION_ID_LOCATION = 102393;

        public const long DEVICE_MINUTES_FROM_LAST_ENERGY_UPDATE = 102422;
        public const long DEVICE_MINUTES_FROM_LAST_VOLUME_UPDATE = 102423;
        public const long DEVICE_MINUTES_FROM_LAST_FLOW_IN_TEMPERATURE_UPDATE = 102424;
        public const long DEVICE_MINUTES_FROM_LAST_CURRENT_FLOW_UPDATE = 102425;
        public const long DEVICE_MINUTES_FROM_LAST_CURRENT_POWER_UPDATE = 102426;
        public const long DEVICE_MINUTES_FROM_LAST_WORK_TIME_UPDATE = 102427;
        public const long DEVICE_MINUTES_FROM_LAST_STATUS_UPDATE = 102428;
        public const long DEVICE_MINUTES_FROM_LAST_CLIENT_NUMBER_UPDATE = 102429;
        public const long DEVICE_MINUTES_FROM_LAST_ADDIDITONAL_WATERMETER_1_VOLUME_UPDATE = 102430;
        public const long DEVICE_MINUTES_FROM_LAST_ADDIDITONAL_WATERMETER_2_VOLUME_UPDATE = 102431;
        public const long DEVICE_MINUTES_FROM_LAST_FLOW_OUT_TEMPERATURE_UPDATE = 102432;
        public const long DEVICE_ERROR_LON_NOT_RESPONDING = 102433;
        public const long LOG_EVENT_TYPE_DESCR = 102440;

        public const long DEVICE_RECEIVED_BT_PACKETS_COUNT = 102441;
        public const long DEVICE_SEND_BT_PACKETS_COUNT = 102442;
        public const long DEVICE_ERROR_AIR = 102443;
        public const long DEVICE_ERROR_BLUETOOTH = 102444;
        public const long DEVICE_LATCH_ERROR_AIR = 102445;
        public const long DEVICE_LATCH_ERROR_BLUETOOTH = 102446;
        public const long DEVICE_SMS_RECEIVED_COUNT_CYCLIC = 102483;
        public const long DEVICE_GPRS_RECEIVED_PACKETS_COUNT_CYCLIC = 102484;
        public const long DAILY_MAXIMUM_TEMPORARY_FLOW_TIME_CYCLIC = 102485;
        public const long FREQ_REASON = 102486;
        public const long LAST_TOPUP_ID_S = 102488;
        public const long TARIFF_ID_S = 102489;

        public const long ACTION_PROFILE_PART_SEND_RETRIES = 102493;
        public const long DEVICE_SMS_SENT_ERROR_COUNT = 102497;
        public const long DEVICE_SMS_RETRIES_USED_COUNT = 102498;
        public const long DEVICE_LOGGING_ERROR_COUNT = 102499;
        public const long DEVICE_BLOCKAGE_EXECUTED_COUNT = 102500;
        public const long DEVICE_SMS_ALL_RETRIES_USED_COUNT = 102501;

        public const long ANALOG_AVG_READOUT_OFFSET = 102509;
        public const long ANALOG_READOUT_OFFSET = 102510;
        public const long ANALOG_AVG_NVR = 102511;
        public const long ANALOG_AVG_VALUE_RAPID_EVENT = 102512;
        public const long ANALOG_VALUE_HIHI_LATCHED_EVENT = 102513;
        public const long ANALOG_VALUE_HI_LATCHED_EVENT = 102514;
        public const long ANALOG_VALUE_LO_LATCHED_EVENT = 102515;
        public const long ANALOG_VALUE_LOLO_LATCHED_EVENT = 102516;
        public const long ANALOG_VALUE_STEPREL_LATCHED_EVENT = 102517;
        public const long ANALOG_VALUE_STEPABS_LATCHED_EVENT = 102518;
        public const long ANALOG_VALUE_RAPID_LATCHED_EVENT = 102519;
        public const long ANALOG_AVG_VALUE_HIHI_LATCHED_EVENT = 102520;
        public const long ANALOG_AVG_VALUE_HI_LATCHED_EVENT = 102521;
        public const long ANALOG_AVG_VALUE_LO_LATCHED_EVENT = 102522;
        public const long ANALOG_AVG_VALUE_LOLO_LATCHED_EVENT = 102523;
        public const long ANALOG_AVG_VALUE_STEPREL_LATCHED_EVENT = 102524;
        public const long ANALOG_AVG_VALUE_STEPABS_LATCHED_EVENT = 102525;
        public const long ANALOG_AVG_VALUE_RAPID_LATCHED_EVENT = 102526;

        public const long DEVICE_WL_LINK_ALL_RETRIES_USED = 102530;
        public const long DEVICE_WL_LINK_LOST_STATE_LATCHED = 102531;
        public const long DEVICE_WL_LINK_DEAD_STATE_LATCHED = 102532;
        public const long PREPAID_TOPUP_ARCHIVE_DATE_FINISH = 102535;
        public const long PREPAID_TOPUP_ARCHIVE_INDEX_START = 102536;
        public const long PREPAID_TOPUP_ARCHIVE_INDEX_FINISH = 102537;
        public const long PREPAID_TOPUP_ARCHIVE_OPERATION_TYPE = 102538;
        public const long EMERGENCY_CREDIT_BALANCE = 102539;
        public const long DEVICE_GPRS_ERROR_SESSION_COUNT = 102551;

        public const long WAN2_RECEIVED_VALID_PACKETS_WITH_OPTO_COUNT = 102576;
        public const long WAN2_RECEIVED_VALID_PACKETS_WITH_MODEM_COUNT = 102577;
        public const long WAN2_RECEIVED_INVALID_PACKETS_WITH_OPTO_COUNT = 102578;
        public const long WAN2_RECEIVED_INVALID_PACKETS_WITH_MODEM_COUNT = 102579;

        public const long UDI_STATE = 102594;
        public const long UDI_UP_EVENT = 102595;
        public const long UDI_DOWN_EVENT = 102596;
        public const long UDI_UP_EVENT_LATCHED = 102597;
        public const long UDI_DOWN_EVENT_LATCHED = 102598;
        public const long UDI_WL_LINK_ACTIVE = 102599;
        public const long UDI_UP_EVENT_DURATION = 102600;
        public const long UDI_UP_EVENT_COUNTER = 102601;

        public const long FIRMWARE_UPLOAD_VERSION = 102609;
        public const long FIRMWARE_UPLOAD_PARTS = 102610;
        public const long FIRMWARE_UPLOAD_PROGRESS = 102615;

        public const long TOTAL_VOLUME_MEASURED = 102616;
        public const long GAS_PRESSURE = 102617;
        public const long GAS_TEMPERATURE = 102618;
        public const long NITROGEN_CONTENT = 102624;
        public const long HYDROGEN_CONTENT = 102625;
        public const long CARBON_DIOXIDE_CONTENT = 102626;
        public const long GAS_DENSITY_AT_BASE_CONDITIONS = 102627;
        public const long FLOW_CORRECTED = 102628;

        public const long DEVICE_DIGITAL_EVENTS_ACTIVE = 102645;
        public const long VOLUME_MEASURED = 102655;

        public const long DEVICE_SACK_MULTIREAD_TIMESTAMP = 102673;
        public const long DEVICE_SACK_MULTIREAD_RADIO_QUALITY = 102674;
        public const long DEVICE_SACK_MULTIREAD_LENGTH = 102675;
        public const long DEVICE_SACK_MULTIREAD_DATA = 102676;
        public const long DEVICE_SACK_MULTIREAD_ADDRESS = 102677;

        public const long DEVICE_SACK_KNOWN_NODES_TIMESTAMP = 102689;
        public const long DEVICE_SACK_KNOWN_NODES_RADIO_QUALITY = 102690;
        public const long DEVICE_SACK_KNOWN_NODES_LENGTH = 102691;
        public const long DEVICE_SACK_KNOWN_NODES_HEADER_LENGTH = 102692;
        public const long DEVICE_SACK_KNOWN_NODES_HEADER_C_FIELD = 102693;
        public const long DEVICE_SACK_KNOWN_NODES_HEADER_ADDRESS = 102694;
        public const long DEVICE_SACK_KNOWN_NODES_CI = 102695;
        public const long DEVICE_SACK_KNOWN_NODES_DATA = 102696;

        public const long ULLAGE_AGGREGATED_ESTIMATED = 102698;
        public const long GAS_CONSUMPTION_DAILY = 102699;
        public const long FUEL_VOLUME_AGGREGATED_ESTIMATED = 102700;
        public const long TOPUP_COUNT = 102714;
        public const long PREPAID_SUSPEND_REASON = 102715;
        public const long PREPAID_TOPUP_VALUE = 102717;
        public const long LITER_COUNTER_ON_VALVE_ACTION = 102718;

        public const long DOOR_OPEN_SENSOR = 102721;
        public const long FLOODING_SENSOR = 102722;
        public const long PREPAID_TOPUP_ARCHIVE_DATE_RECEIVED = 102729;
        public const long PREPAID_TOPUP_ARCHIVE_DATE_CURRRENT = 102730;
        public const long PREPAID_TOPUP_ARCHIVE_INDEX_CURRENT = 102731;
        public const long CHARGE_PREPAID_TOTAL = 102732;

        public const long I2C_CONVERSION_DONE = 102739;
        public const long I2C_PRESSURE_VALID = 102740;
        public const long I2C_TEMPERATURE_VALID = 102741;
        public const long I2C_READY_TO_READ = 102742;
        public const long I2C_PRESSURE_READ = 102743;
        public const long I2C_TEMPERATURE_READ = 102744;
        public const long I2C_SENSOR_ADDRESS_SEARCH = 102745;
        public const long ADC_NOT_CONNECTED = 102748;
        public const long ADC_SHORTED = 102749;
        public const long ADC_MARGIN_RANGE = 102750;
        public const long ALEVEL_CORDIC_OVERFLOW_ACTIVE = 102761;
        public const long ALEVEL_LINEARITY_ALARM = 102762;
        public const long ACCOUNT_VALIDITY_DAYS_LEFT = 102765;
        public const long OUTDATED_PAYMENTS_TOTAL = 102766;

        public const long FUEL_LEVEL_AGGREGATED_PREDICTED = 102768;
        public const long MAGNETO_TIMER_DURATION = 102779;
        public const long DEVICE_CL_READOUT_COUNT = 102780;
        public const long DEVICE_I2C_READOUT_COUNT = 102781;
        public const long SCALED_PRESSURE = 102782;
        public const long SCALED_TEMPERATURE = 102783;
        public const long DEVICE_MAGNETO_RECEIVE_EFFICENCY = 102784;
        public const long DEVICE_MAGNETO_LAST_FRAME_NBR = 102785;
        public const long DEVICE_MAGNETO_LAST_PRODUCT_LEVEL = 102786;
        public const long DEVICE_MAGNETO_DATA_ERROR = 102787;
        public const long DEVICE_MAGNETO_REFUEL_ACTIVE = 102788;
        public const long ADC_AVARAGED_VALUE = 102789;
        public const long ADC_AVARAGED_VALUE_VALID = 102790;
        public const long ADC_MEASURES_COUNT_SINCE_LAST_FRAME = 102791;
        public const long ARCHIVE_RADIO_FRAME_LENGTH = 102796;
        public const long ARCHIVE_RADIO_FRAME_BODY = 102797;
        public const long LOG_EVENT_PUT_OMB = 102804;
        public const long GLOBAL_RESET_COUNTER = 102805;
        public const long ADC_RAPID_ACTIVE = 102809;
        public const long SAFETY_REGULATOR_ON_DIGITAL_INPUT_1_ACTIVE = 102810;
        public const long SAFETY_REGULATOR_ON_DIGITAL_INPUT_2_ACTIVE = 102811;
        public const long PROBE_FACTORY_NUMBER = 102815;

        public const long CL_WORK_MS_TIMER_DURATION = 102819; //    [ms] readonly
        public const long DEVICE_SENT_RADIO_BYTES_COUNT = 102820;  // readonly
        public const long DEVICE_RADIO_433_PACKETS_SENT_COUNT = 102823;
        public const long I2C_PROBE_SERIAL_NUMBER = 102835;
        public const long I2C_PROBE_SERIAL_NUMBER_READ = 102836;
        public const long I2C_PROBE_SERIAL_NUMBER_VALID = 102837;
        public const long I2C_PROBE_SOFTWARE_VERSION = 102838;
        public const long I2C_PROBE_SOFTWARE_VERSION_READ = 102839;
        public const long I2C_PROBE_SOFTWARE_VERSION_VALID = 102840;

        public const long ANALOG_VALUE_HIHI_UP_EVENT = 102855;
        public const long ANALOG_VALUE_HIHI_DOWN_EVENT = 102856;
        public const long ANALOG_VALUE_HI_UP_EVENT = 102857;
        public const long ANALOG_VALUE_HI_DOWN_EVENT = 102858;
        public const long ANALOG_VALUE_LO_UP_EVENT = 102859;
        public const long ANALOG_VALUE_LO_DOWN_EVENT = 102860;
        public const long ANALOG_VALUE_LOLO_UP_EVENT = 102861;
        public const long ANALOG_VALUE_LOLO_DOWN_EVENT = 102862;

        public const long ANALOG_VALUE_HIHI_UP_LATCHED_EVENT = 102863;
        public const long ANALOG_VALUE_HIHI_DOWN_LATCHED_EVENT = 102864;
        public const long ANALOG_VALUE_HI_UP_LATCHED_EVENT = 102865;
        public const long ANALOG_VALUE_HI_DOWN_LATCHED_EVENT = 102866;
        public const long ANALOG_VALUE_LO_UP_LATCHED_EVENT = 102867;
        public const long ANALOG_VALUE_LO_DOWN_LATCHED_EVENT = 102868;
        public const long ANALOG_VALUE_LOLO_UP_LATCHED_EVENT = 102869;
        public const long ANALOG_VALUE_LOLO_DOWN_LATCHED_EVENT = 102870;
        public const long I2C_PROBE_PRESSURE = 102875;
        public const long I2C_PROBE_TEMPERATURE = 102876;
        public const long DEVICE_MAGNETO_DATA_OUTDATED = 102878;
        public const long DEVICE_MAGNETO_TIME_SINCE_LAST_FRAME = 102879;
        public const long PROBE_RECEIVED_FRAMES_COUNT_GLOBAL = 102880;
        public const long PROBE_MISSED_FRAMES_COUNT_GLOBAL = 102881;

        public const long WATER_VERY_HIGH_ALARM = 102882;

        public const long METER_STATUS_DESCRIPTION = 102883;
        public const long DEVICE_GPRS_AVAILABILITY_MODE = 102884;
        public const long DEVICE_GPRS_AVAILABILITY_MODE_APPLY_DATE = 102885;
        public const long DEVICE_VOLUME_CORRECTOR_DATA_FREQ_TIMESTAMP = 102886;
        public const long DEVICE_REGISTER_COUNT = 102887;
        public const long DEVICE_REGISTER_DATA = 102888;
        public const long DEVICE_VOLUME_CORRECTOR_HOUR_DATA_FREQ_TIMESTAMP = 102889;

        public const long DEVICE_MODEM_REGISTRATION_FAIL_COUNT = 102910;

        public const long DEVICE_SCAN_GSM_PROVIDER_GSM_QUALITY_VALID = 102911;
        public const long DEVICE_SCAN_GSM_PROVIDER_GSM_REGISTRATION_ACCEPTED = 102912;
        public const long DEVICE_SCAN_GSM_PROVIDER_ROAMING_ACTIVE = 102913;
        public const long DEVICE_SCAN_GSM_PROVIDER_GPRS_REGISTRATION_ACCEPTED = 102914;
        public const long DEVICE_SCAN_GSM_PROVIDER_GPRS_CONNECTED_TO_IMR_SERVER = 102915;
        public const long DEVICE_SCAN_GSM_PROVIDER_GPRS_SENT_DATA_TO_IMR_SERVER = 102916;
        public const long DEVICE_SCAN_GSM_PROVIDER_GPRS_IMR_SERVER_CONFIRMATION_RECEIVED = 102917;
        public const long DEVICE_TEST_GSM_OPERATOR_LIST_VALID = 102920;
        public const long DEVICE_TEST_GSM_STARTING = 102921;
        public const long DEVICE_TEST_GSM_ERROR = 102922;

        public const long DEVICE_ACO_LEFT_BANK_PRIORITY = 102923;
        public const long DEVICE_ACO_RIGHT_BANK_PRIORITY = 102924;
        public const long DEVICE_ACO_LOW_PRESSURE = 102925;
        public const long DEVICE_ACO_LEFT_BANK_PRIORITY_CHANGE_DATE = 102926;
        public const long DEVICE_ACO_RIGHT_BANK_PRIORITY_CHANGE_DATE = 102927;
        public const long DEVICE_ACO_LOW_PRESSURE_CHANGE_DATE = 102928;
        public const long DEVICE_ACO_LEFT_BANK_CHANGE_COUNT = 102929;
        public const long DEVICE_ACO_RIGHT_BANK_CHANGE_COUNT = 102930;
        public const long DEVICE_ACO_LOW_PRESSURE_CHANGE_COUNT = 102931;
        public const long DEVICE_ACO_LOW_PRESSURE_ACTIVE_DURATION = 102932;
        public const long DEVICE_ACO_LOW_PRESSURE_NOT_ACTIVE_DURATION = 102933;

        public const long DEVICE_VOLUME_CORRECTOR_STATUS_DATA_TIMESTAMP = 102943;

        public const long DEVICE_ERROR_METER_DATA_WRONG_CRC = 102956;
        public const long DEVICE_ERROR_METER_DATA_TIMEOUT = 102957;
        public const long DEVICE_LATCH_ERROR_METER_DATA_WRONG_CRC = 102958;
        public const long DEVICE_LATCH_ERROR_METER_DATA_TIMEOUT = 102959;

        public const long DEVICE_GPRS_VALID_SESSION_COUNT = 102960;
        public const long DEVICE_BYTES_SENT_BY_SMS = 102961;
        public const long DEVICE_BYTES_SENT_BY_GPRS = 102962;
        public const long DEVICE_SMS_TO_SEND_SAVED_ON_SIM_COUNT = 102963;

        public const long PRESSURE_HIHI_LEVEL = 103122;
        public const long PRESSURE_HI_LEVEL = 103123;
        public const long PRESSURE_LO_LEVEL = 103124;
        public const long PRESSURE_LOLO_LEVEL = 103125;
        public const long PRESSURE_HIHI_EVENT_ENABLED = 103126;
        public const long PRESSURE_HI_EVENT_ENABLED = 103127;
        public const long PRESSURE_LO_EVENT_ENABLED = 103128;
        public const long PRESSURE_LOLO_EVENT_ENABLED = 103129;

        public const long DEVICE_KELLER_RS485_PROBE_PRESSURE = 103143;
        public const long DEVICE_KELLER_RS485_PROBE_TEMPERATURE = 103144;
        public const long DEVICE_KELLER_RS485_PROBE_SERIAL_NUMBER = 103145;
        public const long DEVICE_KELLER_RS485_PROBE_SOFTWARE_VERSION = 103146;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_CH0 = 103148;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_P1 = 103149;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_P2 = 103150;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_T = 103151;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_TOB1 = 103152;
        public const long DEVICE_KELLER_RS485_ERROR_CHANNEL_TOB2 = 103153;
        public const long DEVICE_KELLER_RS485_ERROR_CALCULATION_PROCESS = 103154;
        public const long DEVICE_KELLER_RS485_ERROR_CODE = 103155;

        public const long DEVICE_GPRS_SENT_ERROR_COUNT = 103158;
        public const long DEVICE_PACKET_ARCHIVE_PACKETS_SENT_COUNT = 103159;
        public const long DEVICE_PACKET_ARCHIVE_PACKETS_PROCESSED_ERROR_COUNT = 103160;
        public const long DEVICE_PACKET_ARCHIVE_PACKETS_SENT_ERROR_COUNT = 103161;
        public const long PERIOD_VOLUME_USAGE_COMPRESSED = 103185;
        public const long PERIOD_ENERGY_USAGE_COMPRESSED = 103186;
        public const long MONTH_VOLUME_USAGE_COMPRESSED = 103187;
        public const long MONTH_ENERGY_USAGE_COMPRESSED = 103188;

        public const long LAST_DAILY_LATCH_TIME = 103201;
        public const long LAST_DAILY_LATCH_VOLUME = 103202;
        public const long LAST_DAILY_LATCH_ENERGY = 103203;
        public const long COUNTERS_ARCHIVE_LAST_TIME = 103204;
        public const long COUNTERS_ARCHIVE_LAST_VOLUME = 103205;
        public const long COUNTERS_ARCHIVE_LAST_ENERGY = 103206;
        public const long HOURLY_FLOW = 103207;
        public const long TEMPORARY_FLOW_PERIOD_LATCHED = 103208;
        public const long TEMPORARY_FLOW_PERIOD_LATCHED_TIME_COMPRESSED = 103209;
        public const long HOURLY_FLOW_PERIOD_LATCHED = 103210;
        public const long HOURLY_FLOW_PERIOD_LATCHED_TIME_COMPRESSED = 103211;
        public const long TEMPORARY_FLOW_MONTHLY_LATCHED = 103212;
        public const long TEMPORARY_FLOW_MONTHLY_LATCHED_TIME_COMPRESSED = 103213;
        public const long HOURLY_FLOW_MONTHLY_LATCHED = 103214;
        public const long HOURLY_FLOW_MONTHLY_LATCHED_TIME_COMPRESSED = 103215;

        public const long FUEL_LEVEL_AGGREGATED_ESTIMATED = 103234;
        public const long DEVICE_ERROR_MAX_HOUR_FLOW = 103235;
        public const long DEVICE_ERROR_MAX_TEMPORAL_FLOW = 103236;
        public const long DEVICE_ERROR_LOW_VOLTAGE_DETECTED = 103237;
        public const long DEVICE_FLAG_BUTTON = 103238;
        public const long DEVICE_LARGE_CLOCK_SET = 103239;
        public const long DEVICE_VOLUME_SET = 103240;
        public const long DEVICE_ENERGY_SET = 103241;
        public const long DEVICE_INSTALLATION = 103242;
        public const long DEVICE_LATCH_ERROR_MAX_HOUR_FLOW = 103243;
        public const long DEVICE_LATCH_ERROR_MAX_TEMPORAL_FLOW = 103244;
        public const long DEVICE_LATCH_ERROR_LOW_VOLTAGE_DETECTED = 103245;
        public const long DEVICE_LATCH_FLAG_BUTTON = 103246;
        public const long DEVICE_LATCH_LARGE_CLOCK_SET = 103247;
        public const long DEVICE_LATCH_VOLUME_SET = 103248;
        public const long DEVICE_LATCH_ENERGY_SET = 103249;
        public const long DEVICE_LATCH_INSTALLATION = 103250;
        public const long DEVICE_MONTH_LATCH_ERROR_MAX_HOUR_FLOW = 103251;
        public const long DEVICE_MONTH_LATCH_ERROR_MAX_TEMPORAL_FLOW = 103252;
        public const long DEVICE_MONTH_LATCH_ERROR_LOW_VOLTAGE_DETECTED = 103253;
        public const long DEVICE_MONTH_LATCH_FLAG_BUTTON = 103254;
        public const long DEVICE_MONTH_LATCH_LARGE_CLOCK_SET = 103255;
        public const long DEVICE_MONTH_LATCH_VOLUME_SET = 103256;
        public const long DEVICE_MONTH_LATCH_ENERGY_SET = 103257;
        public const long DEVICE_MONTH_LATCH_INSTALLATION = 103258;

        public const long DEVICE_SCAN_GSM_COUNT = 103277;
        public const long DEVICE_GLOBAL_ENABLE_TEMPORARY_OFF_STATE = 103278;
        public const long HOURLY_USAGE_CURRENT = 103306;
        public const long DEVICE_STATUS_PERIODIC_LATCHED = 103309;
        public const long DEVICE_STATUS_MONTHLY_LATCHED = 103310;
        public const long DEVICE_VALVE_OPEN_ACTIVATION = 103311;
        public const long DEVICE_ERROR_VALVE_CLOSE_FAILED = 103312;
        public const long DEVICE_FLAG_TOPUP_END = 103313;
        public const long DEVICE_LATCH_VALVE_OPEN_ACTIVATION = 103314;
        public const long DEVICE_LATCH_ERROR_VALVE_CLOSE_FAILED = 103315;
        public const long DEVICE_LATCH_FLAG_TOPUP_END = 103316;
        public const long DEVICE_MONTH_LATCH_ERROR_VALVE_OPERATION_ERROR = 103317;
        public const long DEVICE_MONTH_LATCH_VALVE_OPEN_ACTIVATION = 103318;
        public const long DEVICE_MONTH_LATCH_ERROR_VALVE_CLOSED_LEAKAGE = 103319;
        public const long DEVICE_MONTH_LATCH_ERROR_VALVE_CLOSE_FAILED = 103320;
        public const long DEVICE_MONTH_LATCH_FLAG_TOPUP_END = 103321;
        public const long DEVICE_MONTH_LATCH_ERROR_VALVE_NOT_DETECTED = 103322;

        public const long DIAGNOSTICS_DIAGNOSTICS_MODULE = 103343;
        public const long DIAGNOSTICS_POWER_MODULE = 103344;
        public const long DIAGNOSTICS_TEMPERATURE_MODULE = 103345;
        public const long DIAGNOSTICS_DATALINKS_MODULE = 103346;
        public const long DIAGNOSTICS_VALDRIVER_MODULE = 103347;
        public const long DIAGNOSTICS_OPTO_MODULE = 103348;
        public const long DIAGNOSTICS_MODEM_MS_MODULE = 103349;
        public const long CREDIT_VOLUME = 103351;

        public const long DEVICE_IWD_RESENT_REQUEST_COUNT = 103352;
        public const long DEVICE_FTP_SENT_ERROR_COUNT = 103353;
        public const long DEVICE_FTP_SENT_OK_PACKETS_COUNT = 103354;
        public const long DEVICE_BYTES_RECEIVED_BY_GPRS = 103355;
        public const long OPTO_PACKETS_SENT_COUNT = 103356;
        public const long OPTO_VALID_PACKETS_RECEIVED_COUNT = 103357;
        public const long OPTO_INVALID_PACKETS_RECEIVED_COUNT = 103358;
        public const long DATALINKS_LOG_PACKETS_SENT_COUNT = 103359;
        public const long DATALINKS_LOG_PACKETS_RECEIVED_COUNT = 103360;
        public const long DATALINKS_LOG_BYTES_SENT_COUNT = 103361;
        public const long DATALINKS_LOG_BYTES_RECEIVED_COUNT = 103362;
        public const long DATALINKS_RADIO_PACKETS_SENT_COUNT = 103363;
        public const long DATALINKS_RADIO_PACKETS_RECEIVED_COUNT = 103364;
        public const long DATALINKS_RADIO_BYTES_SENT_COUNT = 103365;
        public const long DATALINKS_RADIO_BYTES_RECEIVED_COUNT = 103366;
        public const long DATALINKS_NFC_PACKETS_SENT_COUNT = 103367;
        public const long DATALINKS_NFC_PACKETS_RECEIVED_COUNT = 103368;
        public const long DATALINKS_NFC_BYTES_SENT_COUNT = 103369;
        public const long DATALINKS_NFC_BYTES_RECEIVED_COUNT = 103370;
        public const long DATALINKS_MODEM_FTP_PACKETS_SENT_COUNT = 103371;
        public const long DATALINKS_MODEM_FTP_PACKETS_RECEIVED_COUNT = 103372;
        public const long DATALINKS_MODEM_FTP_BYTES_SENT_COUNT = 103373;
        public const long DATALINKS_MODEM_FTP_BYTES_RECEIVED_COUNT = 103374;
        public const long DATALINKS_MODEM_TCP_PACKETS_SENT_COUNT = 103375;
        public const long DATALINKS_MODEM_TCP_PACKETS_RECEIVED_COUNT = 103376;
        public const long DATALINKS_MODEM_TCP_BYTES_SENT_COUNT = 103377;
        public const long DATALINKS_MODEM_TCP_BYTES_RECEIVED_COUNT = 103378;
        public const long DATALINKS_MODEM_UDP_PACKETS_SENT_COUNT = 103379;
        public const long DATALINKS_MODEM_UDP_PACKETS_RECEIVED_COUNT = 103380;
        public const long DATALINKS_MODEM_UDP_BYTES_SENT_COUNT = 103381;
        public const long DATALINKS_MODEM_UDP_BYTES_RECEIVED_COUNT = 103382;
        public const long DATALINKS_MODEM_SMS_PACKETS_SENT_COUNT = 103383;
        public const long DATALINKS_MODEM_SMS_PACKETS_RECEIVED_COUNT = 103384;
        public const long DATALINKS_MODEM_SMS_BYTES_SENT_COUNT = 103385;
        public const long DATALINKS_MODEM_SMS_BYTES_RECEIVED_COUNT = 103386;
        public const long DATALINKS_OPTO_PACKETS_SENT_COUNT = 103387;
        public const long DATALINKS_OPTO_PACKETS_RECEIVED_COUNT = 103388;
        public const long DATALINKS_OPTO_BYTES_SENT_COUNT = 103389;
        public const long DATALINKS_OPTO_BYTES_RECEIVED_COUNT = 103390;
        public const long OPTO_TIMER_DURATION = 103391;
        public const long HIPOW_TIMER_DURATION = 103392;
        public const long NFC_TIMER_DURATION = 103393;
        public const long VALDRIVER_TIMER_DURATION = 103394;
        public const long SLEEP_TIMER_DURATION = 103395;
        public const long EXTREME_TEMPERATURE_MODEM_TIMER_TIMESTAMP = 103396;
        public const long EXTREME_TEMPERATURE_MODEM_TIMER_DURATION = 103397;
        public const long EXTREME_TEMPERATURE_VALVE_TIMER_DURATION = 103398;
        public const long EXTREME_TEMPERATURE_VALVE_TIMER_TIMESTAMP = 103399;
        public const long DEVICE_OPEN_VALVE_TIMESTAMP = 103447;
        public const long DEVICE_CLOSE_VALVE_TIMESTAMP = 103448;
        public const long DEVICE_VALDRIVER_LATCHED_VOLUME = 103449;
        public const long DEVICE_VALDRIVER_LATCHED_ENERGY = 103450;
        public const long VALDRIVER_OPERATION_AVERAGE_CURRENT = 103451;
        public const long VALDRIVER_LEAK_TEST_FAIL_COUNT = 103460;
        public const long TOTAL_TABLE_INDEX = 103466;
        public const long EVENT_LOG_MODULE_ID = 103468;
        public const long CONFIGURATION_NVDATA_CHANGE_COUNTER = 103477;
        public const long CONFIGURATION_LAST_CHANGE_TIMESTAMP = 103478;
        public const long CONFIGURATION_REGISTERED_MODULES_COUNT = 103479;
        public const long CONFIGURATION_RESTORED_MODULES_COUNT = 103480;
        public const long CONFIGURATION_REGISTERED_TOTAL_SIZE = 103481;
        public const long CONFIGURATION_INCORRECT_CRC_MODULES_COUNT = 103482;
        public const long DIAGNOSTICS_CONFIG_MODULE = 103483;
        public const long CONFIGURATION_INTERNAL_CHANGE_COUNTER = 103484;
        public const long TEMPERATURE_EXTREME_DETECTED = 103485;
        public const long TEMPERATURE_EXTREME_MODEM_DETECTED = 103486;
        public const long TEMPERATURE_EXTREME_VALVE_DETECTED = 103487;

        public const long MAX_PRESSURE_HOUR_AGGREGATE = 103488;
        public const long MIN_PRESSURE_HOUR_AGGREGATE = 103489;
        public const long AVG_PRESSURE_HOUR_AGGREGATE = 103490;

        public const long DEVICE_FLAG_OPTO = 103491;
        public const long DEVICE_LATCH_FLAG_OPTO = 103492;
        public const long DEVICE_MONTH_LATCH_FLAG_OPTO = 103493;
        public const long DEVICE_DOWN_EVENT_FLAG_OPTO = 103494;
        public const long DEVICE_UP_EVENT_FLAG_OPTO = 103495;
        public const long DEVICE_ERROR_VALVE_LEAK_TEST_FAILED = 103496;
        public const long DEVICE_LATCH_ERROR_VALVE_LEAK_TEST_FAILED = 103497;
        public const long DEVICE_MONTH_LATCH_ERROR_VALVE_LEAK_TEST_FAILED = 103498;
        public const long DEVICE_DOWN_EVENT_VALVE_LEAK_TEST_FAILED = 103499;
        public const long DEVICE_UP_EVENT_VALVE_LEAK_TEST_FAILED = 103500;

        public const long VALVE_LEAK_TEST_TIME_LEFT = 103501;
        public const long EVENT_LOG_MODULE_NAME = 103502;

        public const long MAX_PRESSURE_DAY_AGGREGATE = 103505;
        public const long MIN_PRESSURE_DAY_AGGREGATE = 103506;
        public const long AVG_PRESSURE_DAY_AGGREGATE = 103507;

        public const long HARD_FAULT_COUNTER = 103535;
        public const long HARD_FAULT_COUNTER_TIMESTAMP = 103536;
        public const long BUS_FAULT_COUNTER = 103537;
        public const long BUS_FAULT_COUNTER_TIMESTAMP = 103538;
        public const long USAGE_FAULT_COUNTER = 103539;
        public const long USAGE_FAULT_COUNTER_TIMESTAMP = 103540;

        public const long SERIALIZED_DAILY_ARCHIVE = 103542;
        public const long SERIALIZED_MONTHLY_ARCHIVE = 103543;
        public const long SERIALIZED_ARCHIVE_CONFIGURATION = 103544;
        public const long SERIALIZED_COUNTER_CONFIGURATION = 103545;
        public const long SERIALIZED_METER_CONFIGURATION = 103546;

        public const long GAS_PRESSURE_ABSOLUTE_MAXIMUM = 103555;
        public const long GAS_PRESSURE_ABSOLUTE_MINIMUM = 103556;
        public const long GAS_TEMPERATURE_MAXIMUM = 103557;
        public const long GAS_TEMPERATURE_MINIMUM = 103558;
        public const long DAILY_MAXIMUM_TEMPORARY_FLOW_MEASURED_CONVENTIONAL = 103559;
        public const long DAILY_MINIMUM_TEMPORARY_FLOW_MEASURED_CONVENTIONAL = 103560;
        public const long DEVICE_BATTERY_MODEM_VOLTAGE = 103561;
        public const long RUN_TIME_UTILISATION = 103564;
        public const long SIM_PUK2_REQUIRED = 103565;
        public const long SIM_PIN2_REQUIRED = 103566;
        public const long MODEM_FULL_ACTIVE = 103567;
        public const long DEVICE_VOLUME_CORRECTOR_EVENT = 103572;
        public const long DEVICE_EVENT_VALUE = 103584;

        public const long FIRMWARE_VERSION_NUMBER = 103590;
        public const long DIAGNOSTICS_RADIO_MODULE = 103591;
        public const long DEVICE_WMBUS_MODE_TEMPORAL_VALUE = 103592;
        public const long DEVICE_WMBUS_MODE_TEMPORAL_TIME_REMAINING = 103593;
        public const long RADIO_BROADCAST_PACKETS_RECEIVED_COUNT = 103594;
        public const long RADIO_INVALID_PACKET_RECEIVED_COUNT = 103595;
        public const long RADIO_LAST_PACKET_SEND_TIME = 103596;
        public const long RADIO_LAST_PACKET_RECEIVED_TIME = 103597;
        public const long RADIO_KNOWN_NODES_ADDRESS = 103598;
        public const long RADIO_KNOWN_NODES_TIMESTAMP = 103599;
        public const long RADIO_KNOWN_NODES_QUALITY = 103600;
        public const long RADIO_KNOWN_NODES_FRAME_COUNT = 103601;
        public const long DEVICE_MONTH_LATCH_ERROR_RADIO = 103604;
        public const long DEVICE_LATCH_ERROR_RADIO = 103605;
        public const long DEVICE_ERROR_RADIO = 103606;
        public const long MAX_HOUR_FLOW_MONTH = 103636;
        public const long DEVICE_ERROR_SERVICE_COUNT_MAX = 103639;
        public const long DEVICE_LATCH_ERROR_SERVICE_COUNT_MAX = 103640;
        public const long DEVICE_MONTH_LATCH_ERROR_SERVICE_COUNT_MAX = 103641;
        public const long EVC_TIMER_TIMESTAMP = 103648;
        public const long EVC_TIMER_DURATION = 103649;

        public const long CONSUMPTION_HOURLY_MEASURED = 103650;
        public const long CONSUMPTION_HOURLY_CORRECTED = 103651;
        public const long UNCONVERTED_GAS_METER_COUNTER = 103652;
        public const long CONVERTED_GAS_METER_COUNTER = 103653;
        public const long MIN_FLOW_MEASURED = 103654;
        public const long MAX_FLOW_MEASURED = 103655;
        public const long MIN_FLOW_CORRECTED = 103656;
        public const long MAX_FLOW_CORRECTED = 103657;
        public const long AVG_FLOW_MEASURED = 103658;
        public const long AVG_FLOW_CORRECTED = 103659;
        public const long CONSUMPTION_DAILY_MEASURED = 103660;
        public const long CONSUMPTION_DAILY_CORRECTED = 103661;
        public const long DEVICE_EVENT_CODE = 103662;
        public const long DEVICE_EVENT_STATUS = 103663;
        public const long MODEM_VALID_PACKETS_RECEIVED_COUNT = 103664;
        public const long MODEM_INVALID_PACKETS_RECEIVED_COUNT = 103665;
        public const long ALEVEL_VALUE_REFUEL_LATCHED_EVENT = 103667;
        public const long RADIO_KNOWN_NODES_INTERFACE = 103669;
        public const long AVG_GAS_LEVEL_ON_STOCK = 103679;
        public const long VOLUME_CORRECTED_DURING_ERROR = 103680;
        public const long STATUS_CODE = 103694;
        public const long DEVICE_VOLUME_CORRECTOR_TIMEOUT_ERROR = 103699;
        public const long DEVICE_VOLUME_CORRECTOR_FRAME_ERROR = 103700;
        public const long REGULATOR_VALUE = 103703;
        public const long DEVICE_REGULATOR_INACTIVE_ERROR = 103704;
        public const long DEVICE_REGULATOR_TIMEOUT_ERROR = 103705;
        public const long DEVICE_REGULATOR_CRC_ERROR = 103706;
        public const long DEVICE_REGULATOR_FRAME_ERROR = 103707;
        public const long DEVICE_CONVERSION_FACTOR_DAILY_AVG = 103710;
        public const long DEVICE_COMPRESSIBILITY_FACTOR_RATIO_DAILY_AVG = 103711;
        public const long TOTAL_VOLUME_CORRECTED = 103712;
        public const long GAS_PRESSURE_DAILY_AVG = 103713;
        public const long GAS_TEMPERATURE_DAILY_AVG = 103714;

        public const long PRESSURE_CONVERSION_FACTOR = 103758;
        public const long TEMPERATURE_CONVERSION_FACTOR = 103759;
        public const long MAX_FLOW_MEASURED_DATE = 103760;
        public const long GAS_PRESSURE_ABSOLUTE_MAXIMUM_DATE = 103761;
        public const long GAS_PRESSURE_ABSOLUTE_MINIMUM_DATE = 103762;
        public const long GAS_TEMPERATURE_MAXIMUM_DATE = 103763;
        public const long GAS_TEMPERATURE_MINIMUM_DATE = 103764;
        public const long DEVICE_MOLAR_LIST = 103765;
        public const long DEVICE_INTERVAL_LOG_FULL = 103766;
        public const long DEVICE_DAILY_LOGGER_DATA_SET = 103767;
        public const long DEVICE_INTERVAL_LOGGER_DATA_SET = 103768;
        public const long DEVICE_EVENT_LOGGER_DATA_SET = 103769;
        public const long DEVICE_ALARM_LOGGER_DATA_SET = 103770;
        public const long FLOW_MEASURED_DAILY_AVG = 103771;
        public const long FLOW_CORRECTED_DAILY_AVG = 103772;
        public const long BATTERY_VOLTAGE_VALUE_DAILY_AVG = 103773;
        public const long UDI_UP_EVENT_COUNTER_CYCLIC = 103779;
        public const long TOTAL_VOLUME_MEASURED_CURRENT = 103780;
        public const long TOTAL_VOLUME_CORRECTED_CURRENT = 103781;
        public const long TOTAL_GAS_METER_USAGE_DIFFERENCE = 103800;
        public const long MAX_HOUR_FLOW_SECOND_MONTH = 103801;
        public const long DIFF_START_MONTH = 103802;
        public const long MAX_INSTANT_FLOW_SECOND_MONTH = 103803;
        public const long DATALINKS_CI_BYTES_RECEIVED_COUNT = 103810;
        public const long DATALINKS_CI_BYTES_SENT_COUNT = 103811;
        public const long DATALINKS_CI_PACKETS_RECEIVED_COUNT = 103812;
        public const long DATALINKS_CI_PACKETS_SENT_COUNT = 103813;
        public const long DEVICE_AC_POWER_UP_COUNT = 103814;
        public const long DEVICE_AC_POWER_DOWN_TIMESTAP = 103815;
        public const long DEVICE_AC_POWER_UP_TIMESTAP = 103816;
        public const long RADIO_TYPE_NAME = 103818;
        public const long DEVICE_ARCHIVE_REGISTER_DATA = 103819;

        public const long DEVICE_SACK_PROTOCOL_ID = 103834;
        public const long DAILY_VOLUME_MEASURED_DURING_ERROR = 103843;
        public const long DAILY_VOLUME_CORRECTED_DURING_ERROR = 103844;
        public const long HOURLY_VOLUME_MEASURED_DURING_ERROR = 103845;
        public const long HOURLY_VOLUME_CORRECTED_DURING_ERROR = 103846;

        public const long DEVICE_EVENT_DIGITAL_CHANNEL_NUMBER = 103847;
        public const long DEVICE_EVENT_ANALOG_CHANNEL_NUMBER = 103848;
        public const long DEVICE_EVENT_ANALOG_CHANNEL_NAME = 103849;
        public const long DEVICE_EVENT_LOW_THRESHOLD_EVENT = 103850;
        public const long DEVICE_EVENT_HIGH_THRESHOLD_EVENT = 103851;
        public const long DEVICE_DATA_SENT_TO_CLIENT_WEB_SERVICE = 103855;
        public const long EXTERNAL_USART_TIMER_DURATION = 103868;
        public const long DEVICE_CHARGING_FLOW = 103906;
        public const long DEVICE_WATER_SUPPLY_FLOW = 103907;
        public const long DEVICE_CIRCULATION_FLOW = 103908;
        public const long TEMPERATURE_CENTRAL_HEATING_RETURN = 103909;
        public const long WATER_SUPLY_RETURN_TEMPERATURE = 103910;
        public const long WATER_SUPLY_IN_TEMPERATURE = 103911;
        public const long TEMPERATURE_OF_SECOND_DEGREE = 103912;
        public const long TEMPERATURE_DWH_OUT = 103913;
        public const long TEMPERATURE_DWH_IN = 103914;
        public const long TEMPERATURE_BEFORE_FIRST_DEGREE = 103915;
        public const long DEVICE_MIXING_VALVE_OPENING_DEGREE = 103916;
        public const long DEVICE_DWH_VALVE_OPENING_DEGREE = 103917;
        public const long DEVICE_CENTRAL_HEATING_VALVE_OPENING_DEGREE = 103918;
        public const long DEVICE_SUMMER_AUTO_MODE_ACTIVE = 103919;
        public const long DEVICE_WINTER_AUTO_MODE_ACTIVE = 103920;
        public const long CHARGING_HOUR_FLOW = 103921;
        public const long CHARGING_FLOW_FROM_DIFFERENTIAL_PRESSURE = 103922;
        public const long CHARGING_FLOW_FROM_FIRST_DEGREE_TEMPERATURE = 103923;
        public const long CHARGING_FLOW_FROM_DWH_IN_TEMPERATURE = 103924;
        public const long CHARGING_CONSTANT_FLOW = 103925;
        public const long CHARGING_PUMP_ON = 103926;
        public const long RESERVE_PUMP_ON = 103927;
        public const long MAXIMUM_TEMPERATURE_OF_SECOND_DEGREE = 103928;
        public const long MINIMUM_TEMPERATURE_OF_SECOND_DEGREE = 103929;
        public const long TEMPERATURE_CENTRAL_HEATING_SET_WITH_ZERO_OUTSIDE = 103930;
        public const long CHARGING_REFERENCE_FLOW = 103931;
        public const long CHARGING_PUMP_CONTROL_PERCENTAGE = 103932;
        public const long WATCHDOG = 103933;

        public const long AWSR_SYNCHRONIZATION_TYPE = 103934;
        public const long PUMP_STATE = 103935;

        public const long INDUCTIVE_SENSOR_COUNTER_L1 = 103948;
        public const long INDUCTIVE_SENSOR_COUNTER_L2 = 103949;
        public const long INDUCTIVE_SENSOR_COUNTER_L3 = 103950;
        public const long INDUCTIVE_SENSOR_COUNTER = 103951;

        public const long DEVICE_ERROR_BACKFLOW = 103952;
        public const long DEVICE_ERROR_UNDERFLOW = 103953;
        public const long DEVICE_ERROR_OVERFLOW = 103954;
        public const long DEVICE_ERROR_SUBMARINE = 103955;
        public const long DEVICE_ERROR_LEAKAGE = 103956;
        public const long DEVICE_LATCH_ERROR_LEAKAGE = 103957;
        public const long DEVICE_ERROR_COUNTER_BLOCKED = 103958;

        public const long TOTAL_WATER_METER_USAGE_DIFFERENCE = 103961;
        public const long WATER_CONSUMPTION_15MINUTES = 103962;
        public const long WATER_CONSUMPTION_HOURLY = 103963;
        public const long WATER_CONSUMPTION_30MINUTES = 103964;
        public const long WATER_CONSUMPTION_DAILY = 103965;
        public const long WATER_METER_USAGE_DIFFERENCE = 103966;

        public const long DATALINKS_EXTERNAL_USART_BYTES_RECEIVED_COUNT = 103978;
        public const long DATALINKS_EXTERNAL_USART_BYTES_SENT_COUNT = 103979;
        public const long DATALINKS_EXTERNAL_USART_PACKETS_RECEIVED_COUNT = 103980;
        public const long DATALINKS_EXTERNAL_USART_PACKETS_SENT_COUNT = 103981;
        public const long DATALINKS_IMRCABLE_BYTES_RECEIVED_COUNT = 103982;
        public const long DATALINKS_IMRCABLE_BYTES_SENT_COUNT = 103983;
        public const long DATALINKS_IMRCABLE_PACKETS_RECEIVED_COUNT = 103984;
        public const long DATALINKS_IMRCABLE_PACKETS_SENT_COUNT = 103985;

        public const long GAS_LEVEL_PERCENTAGE_VALUE_ESTIMATED = 103986;
        public const long FUEL_VOLUME_ESTIMATED = 103987;
        public const long OPERATING_ULLAGE_ESTIMATED = 103988;
        public const long GAS_METER_USAGE_VALUE_ESTIMATED = 103989;
        public const long FUEL_CONSUMPTION_ESTIMATED = 103990;
        public const long RESET_SFT_COUNTER_TIMESTAMP = 103993;

        public const long DIAGNOSTICS_INDUCTIVE_SENSOR_MODULE = 103994;
        public const long INDUCTIVE_SENSOR_COUNTER_L1_L2 = 103995;
        public const long INDUCTIVE_SENSOR_COUNTER_L2_L3 = 103996;
        public const long INDUCTIVE_SENSOR_COUNTER_L1_L3 = 103997;
        public const long INDUCTIVE_SENSOR_COUNTER_L1_L2_L3 = 103998;

        public const long DEVICE_ERROR_REVERSE_FLOW = 104042;
        public const long DEVICE_LATCH_ERROR_REVERSE_FLOW = 104043;
        public const long DEVICE_MONTH_LATCH_ERROR_REVERSE_FLOW = 104044;

        public const long TEMPERATURE_NOT_AVAILABLE_FOR_DOWNLOAD = 104045;

        public const long LAST_MONTHLY_LATCH_TIME = 104048;
        public const long LAST_MONTHLY_LATCH_VOLUME = 104049;
        public const long YEAR_VOLUME_USAGE_COMPRESSED = 104050;


        public const long VOLUME_MEASURED_DIFFERENCE = 104064;
        public const long TOTAL_VOLUME_MEASURED_DIFFERENCE = 104065;
        public const long TOTAL_VOLUME_DIFFERENCE = 104066;

        public const long DEVICE_WMBUS_FIRMWARE_VERSION = 104072;
        public const long DEVICE_WMBUS_HARDWARE_VERSION = 104073;
        public const long FIRMWARE_WMBUS_UPLOAD_PART_NBR = 104074;
        public const long FIRMWARE_WMBUS_UPLOAD_PARTS = 104075;
        public const long WATER_TEMPORARY_FLOW = 104078;
        public const long FUEL_CONSUMPTION_DAILY_AVG = 104084;
        public const long FUEL_CONSUMPTION_LOST_BY_STOCK_OUT = 104089;
        public const long WAN3_FRAME = 104090;
        public const long PERIODIC_VOLUME_DATA_DEF_OBJ_ID = 104091;
        public const long MONTHLY_VOLUME_DATA_DEF_OBJ_ID = 104092;
        public const long PERIODIC_ARCHIVE_DEF_OBJ_ID = 104093;
        public const long MONTHLY_ARCHIVE_DEF_OBJ_ID = 104094;

        public const long DEVICE_LATCH_ERROR_BACKFLOW = 104103;
        public const long DEVICE_LATCH_ERROR_OVERFLOW = 104104;
        public const long DEVICE_LATCH_ERROR_UNDERFLOW = 104105;
        public const long DEVICE_LATCH_ERROR_NO_FLOW = 104106;
        public const long DEVICE_LATCH_ERROR_HIGH_LIGHT = 104107;
        public const long DEVICE_LATCH_ERROR_COMMAND = 104108;
        public const long DEVICE_LATCH_ERROR_ACCESS = 104109;
        public const long DEVICE_LATCH_ERROR_WORK_TIME = 104110;
        public const long DEVICE_LATCH_ERROR_RESET = 104111;
        public const long DEVICE_ERROR_NO_FLOW = 104112;
        public const long DEVICE_ERROR_HIGH_LIGHT = 104113;
        public const long DAILY_MAXIMUM_TEMPORARY_FLOW_CORRECTED_CONVENTIONAL = 104121;
        public const long DAILY_MINIMUM_TEMPORARY_FLOW_CORRECTED_CONVENTIONAL = 104122;
        public const long GAS_CONSUMPTION_LAST_24_HOURS = 104126;
        public const long DEVICE_RUN_MODE_ACTIVE = 104128;

        public const long DEVICE_LEDHMI_IMRTEST_REQUEST_COUNT = 104143;
        public const long DEVICE_LEDHMI_OPTO_REQUEST_COUNT = 104144;
        public const long DEVICE_LEDHMI_INTERRUPT_REQUEST_COUNT = 104145;

        public const long ANALOG_LAST_FREQ_VALUE = 104148;
        public const long ANALOG_LAST_VALID_VALUE_UPDATE = 104149;
        public const long ANALOG_LAST_VALUE_UPDATE = 104150;
        public const long ANALOG_EVENTS = 104151;
        public const long ANALOG_EVENTS_LATCHED = 104152;

        public const long ANALOG_INDEX = 104161;
        public const long RADIO_QUALITY_STRENGHT = 104162;
        public const long NEXT_PLANNED_DELIVERY_TIME = 104163;

        public const long ANALOG_DATA_EXPIRED_UP_EVENT = 104164;
        public const long ANALOG_DATA_EXPIRED_DOWN_EVENT = 104165;
        public const long ANALOG_OUT_OF_RANGE_UP_EVENT = 104166;
        public const long ANALOG_OUT_OF_RANGE_DOWN_EVENT = 104167;
        public const long ANALOG_CONFIG_NVR_UP_LATCHED_ENABLE = 104168;
        public const long ANALOG_CONFIG_NVR_DOWN_LATCHED_ENABLE = 104169;
        public const long ANALOG_DATA_EXPIRED_UP_LATCHED_EVENT = 104170;
        public const long ANALOG_DATA_EXPIRED_DOWN_LATCHED_EVENT = 104171;
        public const long ANALOG_OUT_OF_RANGE_UP_LATCHED_EVENT = 104172;
        public const long ANALOG_OUT_OF_RANGE_DOWN_LATCHED_EVENT = 104173;
        public const long DEVICE_EVENT_RAPID_RISE_UP_LATCHED_ENABLE = 104174;
        public const long DEVICE_EVENT_RAPID_DROP_UP_LATCHED_ENABLE = 104175;
        public const long DEVICE_EVENT_RAPID_RISE_DOWN_LATCHED_ENABLE = 104176;
        public const long DEVICE_EVENT_RAPID_DROP_DOWN_LATCHED_ENABLE = 104177;

        public const long ANALOG_VALUE_NVR_UP_EVENT = 104178;
        public const long ANALOG_VALUE_NVR_DOWN_EVENT = 104179;
        public const long ANALOG_VALUE_RAPID_RISE_UP_EVENT = 104180;
        public const long ANALOG_VALUE_RAPID_RISE_DOWN_EVENT = 104181;
        public const long ANALOG_VALUE_RAPID_DROP_UP_EVENT = 104182;
        public const long ANALOG_VALUE_RAPID_DROP_DOWN_EVENT = 104183;

        public const long STOCK_OUT_TO_NEXT_PLANNED_DELIVERY_TIMESPAN = 104189;

        public const long PACKET_ARCHIVE_CONNECTION_ID = 104190;
        public const long PACKET_ARCHIVE_TARGET_ADDRESS = 104191;
        public const long PACKET_ARCHIVE_ITEM_DATA = 104192;

        public const long DEVICE_SCAN_GSM_PROVIDER_CI = 104198;
        public const long DEVICE_SCAN_GSM_PROVIDER_LAC = 104199;

        public const long DIAGNOSTICS_MAGNETIC_SENSOR_MODULE = 104210;
        public const long WL_FRAME_SEQUENCE_NBR = 104211;

        public const long MAGNETIC_SENSOR_MEASUREMENT_OK = 104214;
        public const long MAGNETIC_SENSOR_MEASUREMENT_ERROR = 104215;

        public const long MAGNETIC_SENSOR_MEASUREMENT_VALIDITY = 104218;
        public const long MAGNETIC_SENSOR_ENCODER_VALUE = 104219;
        public const long MAGNETIC_SENSOR_LEVEL = 104220;

        public const long MODEM_LAST_IDLE_CHECK_TIMESTAMP = 104225;

        public const long DEVICE_WL_SLAVE_STATE = 104246;
        public const long DEVICE_WL_SLAVE_FRAME_DIV = 104247;
        public const long DEVICE_WL_SLAVE_WINDOW_SIZE = 104248;
        public const long DEVICE_WL_SLAVE_MISSED_FRAME_CURRENT_COUNT = 104249;
        public const long DEVICE_WL_SLAVE_MISSED_FRAME_MASTER_FAULT_COUNT = 104250;
        public const long DEVICE_WL_SLAVE_MISSED_FRAME_DEVICE_FAULT_COUNT = 104251;
        public const long DEVICE_WL_SLAVE_LOST_TIMESTAMP = 104252;
        public const long DEVICE_WL_SLAVE_DEAD_TIMESTAMP = 104253;
        public const long DEVICE_MAGNETIC_SENSOR_ANGLE = 104255;

        public const long ALEVEL_VALUE_OUTFLOW_COUNT = 104257;
        public const long ALEVEL_VALUE_REFUEL_COUNT = 104258;
        public const long ALEVEL_MEASUREMENT_OUT_OF_RANGE_COUNT = 104259;
        public const long ALEVEL_MEASUREMENT_ERROR_COUNT = 104260;
        public const long DEVICE_WL_SLAVE_FRAME_COUNT = 104261;
        public const long RADIO_KNOWN_NODES_PROTOCOL_ID = 104263;
        public const long ALEVEL_RADIO_QUALITY = 104267;

        public const long MAGNETIC_SENSOR_CHARACTERISTICS_DATA = 104280;
        public const long ANALOG_WAKEUP_EVENT_COUNT = 104281;
        public const long ANALOG_OUT_OF_RANGE_EVENT_COUNT = 104282;
        public const long ANALOG_DATA_EXPIRED_EVENT_COUNT = 104283;
        public const long ANALOG_NVR_EVENT_COUNT = 104284;

        public const long ANALOG_VALUE_RAPID_RISE_UP_LATCHED_EVENT = 104285;
        public const long ANALOG_VALUE_RAPID_RISE_DOWN_LATCHED_EVENT = 104286;
        public const long ANALOG_VALUE_RAPID_DROP_UP_LATCHED_EVENT = 104287;
        public const long ANALOG_VALUE_RAPID_DROP_DOWN_LATCHED_EVENT = 104288;
        public const long ANALOG_VALUE_NVR_UP_LATCHED_EVENT = 104289;
        public const long ANALOG_VALUE_NVR_DOWN_LATCHED_EVENT = 104290;
        public const long ALEVEL_MEASUREMENT_ERROR = 104293;

        public const long FUEL_CONSUMPTION_DECLARED = 104294;
        public const long FUEL_CONSUMPTION_PREDICTION_UNITED_PROFILE = 104295;
        public const long DEVICE_MODEM_ACCESS_TECHNOLOGY = 104301;
        public const long DEVICE_MODEM_ACCESS_TECHNOLOGY_NAME = 104302;
        public const long MODEM_SIGNAL_STRENGTH_LEVEL = 104303;
        public const long FIRMWARE_VERSION_NUMBER_MAJOR = 104304;

        public const long LAST_MONTHLY_LATCH_VOLUME_TOTAL = 104310;
        public const long YEAR_VOLUME_USAGE_COMPRESSED_TOTAL = 104311;
        public const long COUNTERS_ARCHIVE_LAST_VOLUME_TOTAL = 104312;
        public const long PERIOD_VOLUME_USAGE_COMPRESSED_TOTAL = 104313;
        public const long LAST_DAILY_LATCH_VOLUME_TOTAL = 104314;
        public const long MONTH_VOLUME_USAGE_COMPRESSED_TOTAL = 104315;


        public const long HIGH_LIGHT_EVENT_TIMESTAMP = 104317;
        public const long HIGH_LIGHT_EVENT_COUNTER = 104318;
        public const long HIGH_LIGHT_EVENT_DURATION = 104319;
        public const long RADIO_DISABLE_EVENT_TIMESTAMP = 104320;
        public const long RADIO_DISABLE_EVENT_COUNTER = 104321;
        public const long RADIO_DISABLE_EVENT_DURATION = 104322;
        public const long NO_FLOW_EVENT_TIMESTAMP = 104323;
        public const long NO_FLOW_EVENT_COUNTER = 104324;
        public const long NO_FLOW_EVENT_DURATION = 104325;
        public const long UNDERFLOW_EVENT_TIMESTAMP = 104326;
        public const long UNDERFLOW_EVENT_COUNTER = 104327;
        public const long UNDERFLOW_EVENT_VOLUME = 104328;
        public const long OVERFLOW_EVENT_TIMESTAMP = 104329;
        public const long OVERFLOW_EVENT_COUNTER = 104330;
        public const long OVERFLOW_EVENT_VOLUME = 104331;
        public const long BACKFLOW_EVENT_TIMESTAMP = 104332;
        public const long BACKFLOW_EVENT_COUNTER = 104333;
        public const long BACKFLOW_EVENT_VOLUME = 104334;
        public const long LEAKAGE_EVENT_VOLUME = 104335;
        public const long RESET_PROCESOR_COUNTER_TIMESTAMP = 104336;
        public const long COUNTER_EVENT_TIMESTAMP = 104337;
        public const long LOW_VOLTAGE_EVENT_TIMESTAMP = 104338;
        public const long BATTERY_LOW_EVENT_TIMESTAMP = 104339;
        public const long BATTERY_LOW_EVENT_DURATION = 104340;

        public const long GAS_LEVEL_PERCENTAGE_VALUE_PREDICTED = 104341;
        public const long FUEL_VOLUME_PREDICTED = 104342;
        public const long OPERATING_ULLAGE_PREDICTED = 104343;
        public const long GAS_METER_USAGE_VALUE_PREDICTED = 104344;
        public const long FUEL_CONSUMPTION_PREDICTED = 104345;
        public const long GAS_CONSUMPTION_HOURLY_PREDICTED = 104346;
        public const long FUEL_CONSUMPTION_DECLARED_DIFFERENCE = 104347;

        public const long TEMPEARTURE_DOMESTIC_HOT_WATER_OUT = 104350;
        public const long TEMPERATURE_CENTRAL_HEATING_SUMMER_TRESHOLD = 104351;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_HYSTERESIS = 104352;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_CONTAINER_1 = 104353;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_CORRECTION = 104354;
        public const long TEMPERATURE_DOMESTIC_HOT_WATER_MAX = 104355;


        public const long AWSR_RECONCILIATION_START_TIME = 104360;
        public const long AWSR_RECONCILIATION_END_TIME = 104361;
        public const long AWSR_RECONCILIATION_ID_AGGREGATION_TYPE = 104362;
        public const long AWSR_PROBLEM_START_TIME = 104363;
        public const long AWSR_PROBLEM_END_TIME = 104364;
        public const long AWSR_PROBLEM_ID_AGGREGATION_TYPE = 104365;

        public const long DEVICE_WL_SLAVE_MISSED_FRAME_COUNT = 104368;
        public const long DEVICE_WL_SLAVE_RECEPTION_EFFECTIVENESS = 104369;
        public const long DEVICE_WL_STATE = 104370;
        public const long DEVICE_WL_LINK_ACTIVE_STATE = 104371;

        public const long VOLUME_CORRECTED_ESTIMATED = 104372;
        public const long VOLUME_CORRECTED_PREDICTED = 104373;
        public const long AVG_GAS_LEVEL_PERCENTAGE_VALUE = 104374;
        public const long MAX_GAS_LEVEL_PERCENTAGE_VALUE = 104375;
        public const long RADIO_ID_PROTOCOL_MODE = 104376;
        public const long TEMPORARY_FLOW = 104377;
        public const long LCD_WARNING_ICON_ACTIVE = 104380;
        public const long CONFIGURATION_EE_WRITE_RETRIES_COUNT = 104382;
        public const long CONFIGURATION_EE_WRITE_FAIL_COUNT = 104383;
        public const long RADIO_TOTAL_TRANSMISSION_TIME = 104384;
        public const long RADIO_TOTAL_RECEIVE_TIME = 104385;

        public const long FIRMWARE_UPDATE_ERROR_CODE = 104400;
        public const long FIRMWARE_UPDATE_PROGRESS = 104401;
        public const long FTP_FIRMWARE_RETRIES_LEFT = 104402;
        public const long ALEVEL_BATTERY_VOLTAGE_VALUE = 104404;

        public const long GAS_TEMPERATURE_HOURLY_MINIMUM = 104408;
        public const long GAS_TEMPERATURE_HOURLY_MAXIMUM = 104409;
        public const long GAS_PRESSURE_ABSOLUTE_HOURLY_MINIMUM = 104410;
        public const long GAS_PRESSURE_ABSOLUTE_HOURLY_MAXIMUM = 104411;

        public const long SECONDARY_GAS_CONSUMPTION_HOURLY = 104412;
        public const long SECONDARY_GAS_CONSUMPTION_15MINUTES = 104413;
        public const long SECONDARY_GAS_CONSUMPTION_30MINUTES = 104414;
        public const long SECONDARY_GAS_CONSUMPTION_DAILY = 104415;

        public const long USER_STRUCTURE_1 = 104416;
        public const long DEVICE_SENT_ALARM_RADIO_PACKETS_COUNT = 104422; 
        public const long ALEVEL_BATTERY_PERCENTAGE_USAGE = 104426;     
        
        public const long DEVICE_CONTEXT_ACTIVATION_COUNT = 104427; 
        public const long GSM_TEST_EXECUTION_COUNT = 104434;
        public const long MODEM_RELOG_COUNT = 104435;
        public const long HIPOW_CUT_COUNT = 104436;
        public const long USER_STRUCTURE_2 = 104437;
        public const long DEVICE_ERROR_HIPOW = 104441;
        public const long DEVICE_LATCH_ERROR_HIPOW = 104442;
        public const long DEVICE_MONTH_LATCH_ERROR_HIPOW = 104443;
        
        public const long PERIOD_VOLUME_USAGE_LATCHED_COMPRESSED = 104444;
        public const long PERIOD_ENERGY_USAGE_LATCHED_COMPRESSED = 104445;
        public const long DEVICE_ERROR_TEMPORARY = 104448;
        public const long DEVICE_ERROR_PERMAMENT = 104449;
        #endregion
        #region DATA write only
        public const long DEVICE_SIM_CARD_PIN = 100063;    // PIN do karty SIM
        public const long DEVICE_SIM_CARD_SMS_CENTER = 100064;    // Centrum wiadomo�ci SMS
        public const long DEVICE_GPRS_APN = 100071;    // Adres APN dla GPRS
        public const long DEVICE_GPRS_USER = 100072;    // Nazwa u�ytkownika do logowania do GPRS
        public const long DEVICE_GPRS_PASSWORD = 100073;    // Has�o dost�pu do GPRS
        public const long DEVICE_TEST_IMR = 100075;    // Zdarzenie test IMR
        public const long DEVICE_CLOCK_OFFSET = 100185;    // <Brak opisu>
        public const long ATG_PARAMS_DEADSTOCK_DISTR_ALARM_ENABLE = 100190;    // <Brak opisu>
        public const long ATG_PARAMS_UP_EVENT_ENABLE = 100191;    // <Brak opisu>
        public const long ATG_PARAMS_DOWN_EVENT_ENABLE = 100192;    // <Brak opisu>
        public const long ATG_PARAMS_EVENT_ON_RESTART_ENABLE = 100193;    // <Brak opisu>
        public const long DEVICE_METER_RADIO_ADDRESS = 100196;    // <Brak opisu>
        public const long DEVICE_OPEN_VALVE = 100201;    // <Brak opisu>
        public const long DEVICE_CLOSE_VALVE = 100202;    // <Brak opisu>
        public const long DEVICE_OLAN_RADIO_ADDRESS = 100203;    // <Brak opisu>
        public const long DEVICE_GAS_METER_VALUE_OFFSET = 100212;    // <Brak opisu>
        public const long DEVICE_MOVE_VALVE_TYPE = 100286;    // <Brak opisu>
        public const long DEVICE_GSM_LOGGING_TIME = 100380;    // <Brak opisu>
        public const long DEVICE_RADIO_LISTEN_TIME = 100381;    // <Brak opisu>
        public const long DEVICE_INIT_CONFIRMATION_TIME = 100382;    // <Brak opisu>
        public const long DEVICE_WLSUP = 100383;    // <Brak opisu>
        public const long DEVICE_WLSDN = 100384;    // <Brak opisu>
        public const long DEVICE_WLINC = 100385;    // <Brak opisu>
        public const long DEVICE_WLDEC = 100386;    // <Brak opisu>
        public const long DEVICE_MAX_SMS_COUNT_PER_WAKEUP = 100387;    // <Brak opisu>
        public const long DEVICE_ETHERNET_ENABLE = 100388;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_SEND_ACTIVE = 100389;    // <Brak opisu>
        public const long DEVICE_RESET_VIA_RING = 100508;	// <Brak opisu>
        public const long DEVICE_SWITCH_TO_WAN1 = 109000;    // <Brak opisu>

        public const long DEVICE_LCD_SEGMENT = 101937;
        public const long AIR_FRAME = 101938;
        public const long AIR_FRAME_TX_DELAYED = 101939;
        public const long RS_FRAME = 101940;
        public const long CC1120_FRAME = 101941;
        public const long DEVICE_TRANSPARENT_MODE = 101942;

        public const long DEVICE_DIRECT_MEMORY_TYPE_CONFIG = 102438;
        public const long DEVICE_DIRECT_MEMORY_ACCESS_ADDRESS_CONFIG = 102439;

        public const long DEVICE_MULTICAST_RADIO_ADDRESS = 102453;
        public const long DEVICE_SCAN_GSM_REQUEST = 102542;
        public const long DEVICE_WMBUS_SUPER_PERIOD = 102603;
        public const long DEVICE_WMBUS_FRAMES_COUNT = 102604;
        public const long DEVICE_WMBUS_SLOT_WIDTH = 102605;
        public const long DEVICE_WMBUS_SLOT_COUNT = 102606;
        public const long DEVICE_WMBUS_SLOT_DELAY = 102607;
        public const long DEVICE_WMBUS_TRANSMISSION_TYPE = 102608;
        public const long DEVICE_SACK_ARCHIVE_LATEST_ITEMS_COUNT = 102635;
        public const long DEVICE_SACK_ARCHIVE_LATEST_TIME_OFFSET = 102636;
        public const long DEVICE_SACK_ARCHIVE_LATEST_TYPE = 102637;
        public const long DEVICE_DIRECT_FLASH_FIRMWARE_PACKET_NO = 102642;
        public const long DEVICE_DIRECT_FLASH_FIRMWARE_DATA = 102643;
        public const long ANALOG_ARCHIVE_MASK = 102657;
        public const long DEVICE_SACK_MULTIREAD_SEARCH_TIME_RANGE = 102671;
        public const long DEVICE_SACK_MULTIREAD_SEARCH_TIME_OFFSET = 102672;
        public const long DEVICE_SACK_MULTIREAD_WINDOWS_COUNT = 102678;
        public const long ANALOG_ARCHIVE_SEARCH_TIMESTAMP = 102686;
        public const long DEVICE_SACK_KNOWN_NODES_SEARCH_TIME_RANGE = 102687;
        public const long DEVICE_SACK_KNOWN_NODES_SEARCH_TIME_OFFSET = 102688;
        public const long DEVICE_I2C_CURRENT_ADDRESS = 102737;
        public const long DEVICE_I2C_NEW_ADDRESS = 102738;
        public const long DEVICE_DOMS_POS_PASSWORD = 102746;
        public const long DEVICE_MULTIREAD_ARCHIVE_TIMESTAMP = 102755;
        public const long DEVICE_MBUS_SELECT_SLAVE = 102756;
        public const long PREPAID_TOPUP_END = 102767;
        public const long DEVICE_ARCHIVE_RADIO_FRAME_DATA = 102795;
        public const long DEVICE_MBUS_BROADCAST_SLEEP = 102799;
        public const long ACTION_ID_ACTION_PARAM = 102802;
        public const long DEVICE_MBUS_MEDIUM_MASKED = 102812;
        public const long DEVICE_MBUS_GENERATION_MASKED = 102813;
        public const long DEVICE_MBUS_MANUFACTURER_ID_MASKED = 102814;
        public const long DEVICE_TABLE_OBJECT_ID = 103162;
        public const long DEVICE_TABLE_START_INDEX = 103163;
        public const long DEVICE_TABLE_END_INDEX = 103164;
        public const long DEVICE_TABLE_START_TIME = 103165;
        public const long DEVICE_TABLE_END_TIME = 103166;
        public const long DEVICE_TABLE_ROW_COUNT = 103167;
        public const long DEVICE_TABLE_START_TOTAL_INDEX = 103168;
        public const long DEVICE_TABLE_COLUMN_DATA_TYPE = 103169;
        public const long DEVICE_TABLE_COLUMN_DEFINITION = 103170;

        public const long DEVICE_TABLE_SACK_TIME_OFFSET = 103677;
        public const long DEVICE_TABLE_SACK_TIME_RANGE = 103678;
        public const long DEVICE_TABLE_SACK_UNIQUE_WINDOW_TIME_RANGE = 103692;
        public const long DEVICE_TABLE_SACK_UNIQUE_WINDOWS_COUNT = 103693;

        public const long DEVICE_PERIODIC_ARCHIVE_START_INDEX = 103171;
        public const long DEVICE_PERIODIC_ARCHIVE_END_INDEX = 103172;
        public const long DEVICE_PERIODIC_ARCHIVE_START_TIME = 103173;
        public const long DEVICE_PERIODIC_ARCHIVE_END_TIME = 103174;
        public const long DEVICE_PERIODIC_ARCHIVE_ROW_COUNT = 103175;
        public const long DEVICE_PERIODIC_ARCHIVE_START_TOTAL_INDEX = 103176;
        public const long DEVICE_PERIODIC_ARCHIVE_COLUMN_DATA_TYPE = 103177;

        public const long DEVICE_MONTHLY_ARCHIVE_START_INDEX = 103178;
        public const long DEVICE_MONTHLY_ARCHIVE_END_INDEX = 103179;
        public const long DEVICE_MONTHLY_ARCHIVE_START_TIME = 103180;
        public const long DEVICE_MONTHLY_ARCHIVE_END_TIME = 103181;
        public const long DEVICE_MONTHLY_ARCHIVE_ROW_COUNT = 103182;
        public const long DEVICE_MONTHLY_ARCHIVE_START_TOTAL_INDEX = 103183;
        public const long DEVICE_MONTHLY_ARCHIVE_COLUMN_DATA_TYPE = 103184;
        public const long DEVICE_OBJECT_ACCESS_PASSWORD = 103191;
        public const long DEVICE_OPERATOR_ACCESS_LEVEL = 103193;
        public const long DEVICE_COMMAND_VOLUME_OFFSET = 103199;
        public const long DEVICE_COMMAND_ENERGY_OFFSET = 103200;
        public const long ACTION_ID_LOCATION_LIST = 103233;

        public const long DEVICE_EVC_ARCHIVE_READ_ARCHIVE_TYPE = 103823;
        public const long DEVICE_EVC_ARCHIVE_READ_START_INDEX = 103824;
        public const long DEVICE_EVC_ARCHIVE_READ_ROW_COUNT = 103825;
        public const long PUMP_COMMAND = 103936;

        public const long DEVICE_WMBUS_COMMAND_CODE = 104068;
        public const long DEVICE_WMBUS_COMMAND_PARAM = 104069;
        public const long DEVICE_WMBUS_FIRMWARE_PACKET_NO = 104070;
        public const long DEVICE_WMBUS_FIRMWARE_DATA = 104071;
        public const long DEVICE_WMBUS_COMMAND_PARAM_1 = 104076;
        public const long DEVICE_WMBUS_COMMAND_PARAM_2 = 104077;
        public const long WMBUS_REMOTE_COMMAND_SERIAL_NBR = 104079;
        public const long WMBUS_REMOTE_COMMAND_CODE = 104080;
        public const long WMBUS_REMOTE_COMMAND_PARAM = 104081;
        public const long DEVICE_WMBUS_TIME_SYNCHRONIZATION = 104264;
        #endregion

        #region DATA archive only
        public const long DEVICE_FRAM_STATUS = 100186;    // <Brak opisu>
        public const long DEVICE_FRAM_IN_TIME = 100187;    // <Brak opisu>
        public const long DEVICE_FRAM_START_TIME = 100188;    // <Brak opisu>
        public const long DEVICE_FRAM_END_TIME = 100189;    // <Brak opisu>
        public const long GAS_METER_USAGE_DECREMENT_VALUE = 100209;    // <Brak opisu>
        public const long DEVICE_EVENT_FORBIDDEN_DECREMENTATION = 100210;    // <Brak opisu>
        public const long DEVICE_EVENT_POWER_ON_RESET = 100211;    // <Brak opisu>
        public const long GAS_USAGE = 100497;    // <Brak opisu>
        public const long AVG_GAS_USAGE = 100498;    // <Brak opisu>
        public const long DELTA_GAS_USAGE = 100499;    // <Brak opisu>
        public const long MIN_GAS_LEVEL_PERCENTAGE_VALUE = 100500;    // <Brak opisu>
        public const long HORIZ_GAS_USAGE = 100501;    // <Brak opisu>
        public const long HORIZ_GAS_USAGE_ERROR = 100502;    // <Brak opisu>
        public const long HORIZ_GAS_USAGE_ERROR_ACCEPTED = 100503;    // <Brak opisu>
        public const long TANK_PERCENTAGE_ULLAGE = 100505;    // <Brak opisu>
        public const long TANK_PERCENTAGE_USAGE = 100506;    // <Brak opisu>
        public const long AVG_TANK_PERCENTAGE_USAGE = 100507;    // <Brak opisu>
        public const long NEXT_LO_LEVEL_TIME = 100509;    // <Brak opisu>
        public const long NEXT_LOLO_LEVEL_TIME = 100510;    // <Brak opisu>
        public const long PREPAID_TOPUP_ARCHIVE_TOPUP_ID = 103541;
        public const long UDI_WARNING_LEVEL_EVENT = 103696;
        public const long UDI_ALARM_LEVEL_EVENT = 103697;
        #endregion
        #region DATA !!! NO read, NO write, NO archive !!!
        public const long FUEL_NAME = 100005;    // Nazwa paliwa
        public const long DEVICE_SIM_CARD_CHANGE = 100079;    // Zdarzenie zmiana karty SIM
        public const long DEVICE_ACTION_ALEVEL = 100123;    // <Brak opisu>
        public const long DEVICE_ACTION_CID = 100126;    // <Brak opisu>
        public const long DEVICE_SIM_CARD_CHANGE_ADDRESS = 100195;    // <Brak opisu>
        public const long LAST_MEASURE_TIME = 100204;    // <Brak opisu>
        public const long LAST_PACKET_TIME = 100394;    // <Brak opisu>
        public const long LAST_GAS_LEVEL_PERCENTAGE_VALUE_CHANGE_TIME = 100171;
        public const long DEVICE_CHWYTEN_TANKEN_ALEVEL_COUNT = 100404;
        public const long VOLUME_MANUAL_READOUT = 101072;    // <Brak opisu>
        public const long DEVICE_EXTERNAL_DATA = 102000;    // <Brak opisu>
        public const long METER_ID_DEVICE = 200001;    // <Brak opisu>
        public const long LOCATION_OLD_ID_LOCATION = 200033;    // <Brak opisu>
        public const long CREDIT = 102602;
        public const long ANALOG_COMPACT_ARCHIVE_DATA_TIMESTAMP = 102658;

        public const long SACK_PACKET_ID = 102679;
        public const long SACK_SESSION_ID = 102680;
        public const long SACK_IS_LAST = 102681;
        public const long DEVICE_EVENT_RAPID_RISE_UP_EVENT = 102682;
        public const long DEVICE_EVENT_RAPID_RISE_DOWN_EVENT = 102683;
        public const long DEVICE_EVENT_RAPID_DROP_UP_EVENT = 102684;
        public const long DEVICE_EVENT_RAPID_DROP_DOWN_EVENT = 102685;

        public const long UDI_READOUT_DATA_TYPE_UP_EVENT = 102826;
        public const long UDI_READOUT_DATA_TYPE_DOWN_EVENT = 102827;
        public const long UDI_READOUT_DATA_TYPE_UP_EVENT_LATCHED = 102828;
        public const long UDI_READOUT_DATA_TYPE_DOWN_EVENT_LATCHED = 102829;
        public const long UDI_READOUT_DATA_TYPE_WL_LINK_ACTIVE = 102830;
        public const long UDI_READOUT_DATA_TYPE_UP_EVENT_DURATION = 102831;
        public const long UDI_READOUT_DATA_TYPE_UP_EVENT_COUNTER = 102832;
        public const long LAST_FUEL_VOLUME_FOR_CONSUMPTION = 102941;

        public const long CONSUMPTION_TEMPERATURE_COEFFICIENT = 103280;
        public const long CONSUMPTION_CONSTANT_COEFFICIENT = 103281;
        public const long DEVICE_OBJECT_ID = 103329;
        public const long DEVICE_TABLE_END_TIME_OFFSET = 103580;
        public const long DEVICE_PERIODIC_ARCHIVE_END_TIME_OFFSET = 103581;
        public const long DEVICE_MONTHLY_ARCHIVE_END_TIME_OFFSET = 103582;
        public const long REGULATOR_READOUT_DATA_TYPE = 103708;
        public const long REGULATOR_READOUT_VALUE_SLOPE = 103715;

        public const long GAS_CONSUMPTION_15MINUTES = 103775;
        public const long GAS_CONSUMPTION_30MINUTES = 103776;
        public const long DEVICE_COUNT_GAS_CONSUMPTION_15MINUTES = 103777;
        public const long DEVICE_COUNT_GAS_CONSUMPTION_30MINUTES = 103778;

        public const long FUEL_VOLUME_FOR_SELL = 103822;
        public const long DEVICE_WMBUS_CUSTOM_SEND_RESPONSE_IMMEDIATELY = 103826;

        public const long CONSUMPTION_DAILY_AVG_SHORT_WINDOW = 103827;
        public const long CONSUMPTION_DAILY_AVG_LONG_WINDOW = 103828;
        public const long CONSUMPTION_DAILY_AVG_SHORT_TO_LONG_COEFFICIENT = 103829;
        public const long DEVICE_TABLE_MODBUS_DATA_TYPE_ID = 103940;
        public const long DEVICE_TABLE_MODBUS_DATA_TYPE_IDX = 103941;

        public const long DEVICE_COUNT_WATER_CONSUMPTION_15MINUTES = 103959;
        public const long DEVICE_COUNT_WATER_CONSUMPTION_30MINUTES = 103960;

        public const long WATER_CONSUMPTION_WEEKLY = 104026;
        public const long WATER_CONSUMPTION_MONTHLY = 104027;
        public const long WATER_CONSUMPTION = 104028;
        public const long DEVICE_RADIAN_ADDRESS = 104032;
        public const long DEVICE_ENCODER_TABLE_TYPE = 104046;
        public const long POWER_CALCULATED = 104047;

        public const long WEATHER_STATION_TEMPERATURE = 104082;
        public const long DEVICE_SENSITIVE_PARAMETERS_ENCRYPTED = 104114;
        public const long DEVICE_WAVENIS_ADDRESS = 104129;

        public const long DEVICE_CME_GPRS_ATTACH_CODE = 104131;
        public const long DEVICE_CME_SOCKET_DIAL_CODE = 104132;
        public const long DEVICE_CME_GPRS_SEND_CODE = 104133;
        public const long DEVICE_CMS_SMS_SEND_CODE = 104134;
        public const long DEVICE_CME_SIM_CODE = 104135;
        public const long DEVICE_FTP_OPEN_CODE = 104136;

        public const long DEVICE_CME_GPRS_ATTACH_TIMESTAMP = 104137;
        public const long DEVICE_CME_SOCKET_DIAL_TIMESTAMP = 104138;
        public const long DEVICE_CME_GPRS_SEND_TIMESTAMP = 104139;
        public const long DEVICE_CMS_SMS_SEND_TIMESTAMP = 104140;
        public const long DEVICE_CME_SIM_TIMESTAMP = 104141;
        public const long DEVICE_FTP_OPEN_TIMESTAMP = 104142;

        public const long DEVICE_NEW_FIRMWARE_AVAILABLE = 104216;

        public const long GAS_LEVEL_PERCENTAGE_VALUE_ESTIMATED_BY_CONSUMPTION = 104226;
        public const long FUEL_VOLUME_ESTIMATED_BY_CONSUMPTION = 104227;
        public const long OPERATING_ULLAGE_ESTIMATED_BY_CONSUMPTION = 104228;
        public const long FUEL_VOLUME_DECLARED = 104243;
        public const long GAS_LEVEL_PERCENTAGE_VALUE_DECLARED = 104244;

        public const long MAX_WATER_FLOW = 104240;
        public const long MIN_WATER_FLOW = 104241;
        public const long AVG_WATER_FLOW = 104242;
        public const long CORRECTED_FUEL_PRESSURE = 104276;

        public const long LAST_GAS_LEVEL_PERCENTAGE_VALUE_DECLARED_TIME = 104245;
        public const long LAST_FUEL_CONSUMPTION_ANALYZED_BY_PREDICTOR_UNITED = 104296;

        public const long FP_RESTART_REQUEST = 104297;
        public const long FPC_CONFIGURATION_REQUEST = 104316;

        public const long STATION_SYNCHRONIZATION_AGGREGATES_START_TIME = 104387;
        public const long STATION_SYNCHRONIZATION_AGGREGATES_END_TIME = 104388;
        public const long STATION_SYNCHRONIZATION_AGGREGATES_DAYS_COUNT = 104389;
        public const long STATION_SYNCHRONIZATION_TANK_MEASURES_START_TIME = 104390;
        public const long STATION_SYNCHRONIZATION_TANK_MEASURES_END_TIME = 104391;
        public const long STATION_SYNCHRONIZATION_TANK_MEASURES_DAYS_COUNT = 104392;
        public const long STATION_SYNCHRONIZATION_REFUELS_START_TIME = 104393;
        public const long STATION_SYNCHRONIZATION_REFUELS_END_TIME = 104394;
        public const long STATION_SYNCHRONIZATION_REFUELS_DAYS_COUNT = 104395;
        public const long STATION_SYNCHRONIZATION_RECONCILIATION_START_TIME = 104396;
        public const long STATION_SYNCHRONIZATION_RECONCILIATION_END_TIME = 104397;
        public const long STATION_SYNCHRONIZATION_RECONCILIATION_DAYS_COUNT = 104398;
        public const long STATION_SYNCHRONIZATION_DISPENSER_COUNTER_START_TIME = 104405;
        public const long STATION_SYNCHRONIZATION_DISPENSER_COUNTER_END_TIME = 104406;
        public const long STATION_SYNCHRONIZATION_DISPENSER_COUNTER_DAYS_COUNT = 104407;

        public const long ADDIDITONAL_WATERMETER_1_VOLUME_DIFFERENCE = 104403;
        public const long ANALOG_SEND_READOUTS_ON_INDEXES = 104418;
        public const long HOURLY_FLOW_PERIOD_LAST_LATCHED = 104446;
        public const long TEMPORARY_FLOW_PERIOD_LAST_LATCHED = 104447;
        #endregion
        #region DATA !!! REST !!! not categorized !!!
        #endregion
        #region DATA do usuniecia
        public const long DEVICE_STOPPER_SEND1_MINUTE = 101000;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND1_HOUR = 101001;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND1_DATE = 101002;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND1_DOW = 101003;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND2_MINUTE = 101004;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND2_HOUR = 101005;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND2_DATE = 101006;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND2_DOW = 101007;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND3_MINUTE = 101008;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND3_HOUR = 101009;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND3_DATE = 101010;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND3_DOW = 101011;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND4_MINUTE = 101012;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND4_HOUR = 101013;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND4_DATE = 101014;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND4_DOW = 101015;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND5_MINUTE = 101016;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND5_HOUR = 101017;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND5_DATE = 101018;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND5_DOW = 101019;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND6_MINUTE = 101020;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND6_HOUR = 101021;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND6_DATE = 101022;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND6_DOW = 101023;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND7_MINUTE = 101024;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND7_HOUR = 101025;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND7_DATE = 101026;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND7_DOW = 101027;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND8_MINUTE = 101028;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND8_HOUR = 101029;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND8_DATE = 101030;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND8_DOW = 101031;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND9_MINUTE = 101032;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND9_HOUR = 101033;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND9_DATE = 101034;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND9_DOW = 101035;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND10_MINUTE = 101036;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND10_HOUR = 101037;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND10_DATE = 101038;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND10_DOW = 101039;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND11_MINUTE = 101040;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND11_HOUR = 101041;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND11_DATE = 101042;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND11_DOW = 101043;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND12_MINUTE = 101044;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND12_HOUR = 101045;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND12_DATE = 101046;    // <Brak opisu>
        public const long DEVICE_STOPPER_SEND12_DOW = 101047;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE1_MINUTE = 101048;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE1_HOUR = 101049;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE1_DATE = 101050;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE2_MINUTE = 101051;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE2_HOUR = 101052;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE2_DATE = 101053;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE3_MINUTE = 101054;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE3_HOUR = 101055;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE3_DATE = 101056;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE4_MINUTE = 101057;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE4_HOUR = 101058;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE4_DATE = 101059;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE1_DOW = 101060;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE2_DOW = 101061;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE3_DOW = 101062;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE4_DOW = 101063;    // <Brak opisu>
        public const long DEVICE_STOPPER_WAKE5_MINUTE = 101064;
        public const long DEVICE_STOPPER_WAKE5_HOUR = 101065;
        public const long DEVICE_STOPPER_WAKE5_DATE = 101066;
        public const long DEVICE_STOPPER_WAKE5_DOW = 101067;
        public const long DEVICE_STOPPER_WAKE6_MINUTE = 101068;
        public const long DEVICE_STOPPER_WAKE6_HOUR = 101069;
        public const long DEVICE_STOPPER_WAKE6_DATE = 101070;
        public const long DEVICE_STOPPER_WAKE6_DOW = 101071;
        public const long DEVICE_TEMPERATURE_OFFSET_TABLE_0 = 110001;
        public const long DEVICE_TEMPERATURE_OFFSET_TABLE_1 = 110002;
        public const long METER_VOLUME_TABLE_0 = 110003;
        public const long METER_VOLUME_TABLE_1 = 110004;
        public const long METER_VOLUME_TABLE_2 = 110005;
        public const long METER_VOLUME_TABLE_3 = 110006;
        public const long METER_VOLUME_TABLE_4 = 110007;
        public const long METER_VOLUME_TABLE_5 = 110008;
        public const long METER_VOLUME_TABLE_6 = 110009;
        public const long METER_VOLUME_TABLE_7 = 110010;
        public const long METER_VOLUME_TABLE_8 = 110011;
        public const long METER_VOLUME_TABLE_9 = 110012;
        public const long DEVICE_AVAILABLE_RADIO1_SIGNAL_QUALITY = 100291;
        public const long DEVICE_AVAILABLE_RADIO1_ADDRESS = 100292;
        public const long DEVICE_AVAILABLE_RADIO1_LAST_FRAME_TIMESTAMP = 100293;
        public const long DEVICE_AVAILABLE_RADIO1_FRAME_COUNT = 100294;
        public const long DEVICE_AVAILABLE_RADIO2_SIGNAL_QUALITY = 100295;
        public const long DEVICE_AVAILABLE_RADIO2_ADDRESS = 100296;
        public const long DEVICE_AVAILABLE_RADIO2_LAST_FRAME_TIMESTAMP = 100297;
        public const long DEVICE_AVAILABLE_RADIO2_FRAME_COUNT = 100298;
        public const long DEVICE_AVAILABLE_RADIO3_SIGNAL_QUALITY = 100299;
        public const long DEVICE_AVAILABLE_RADIO3_ADDRESS = 100300;
        public const long DEVICE_AVAILABLE_RADIO3_LAST_FRAME_TIMESTAMP = 100301;
        public const long DEVICE_AVAILABLE_RADIO3_FRAME_COUNT = 100302;
        public const long DEVICE_AVAILABLE_RADIO4_SIGNAL_QUALITY = 100303;
        public const long DEVICE_AVAILABLE_RADIO4_ADDRESS = 100304;
        public const long DEVICE_AVAILABLE_RADIO4_LAST_FRAME_TIMESTAMP = 100305;
        public const long DEVICE_AVAILABLE_RADIO4_FRAME_COUNT = 100306;
        public const long DEVICE_AVAILABLE_RADIO5_SIGNAL_QUALITY = 100307;
        public const long DEVICE_AVAILABLE_RADIO5_ADDRESS = 100308;
        public const long DEVICE_AVAILABLE_RADIO5_LAST_FRAME_TIMESTAMP = 100309;
        public const long DEVICE_AVAILABLE_RADIO5_FRAME_COUNT = 100310;
        public const long DEVICE_AVAILABLE_RADIO6_SIGNAL_QUALITY = 100311;
        public const long DEVICE_AVAILABLE_RADIO6_ADDRESS = 100312;
        public const long DEVICE_AVAILABLE_RADIO6_LAST_FRAME_TIMESTAMP = 100313;
        public const long DEVICE_AVAILABLE_RADIO6_FRAME_COUNT = 100314;
        public const long DEVICE_AVAILABLE_RADIO7_SIGNAL_QUALITY = 100315;
        public const long DEVICE_AVAILABLE_RADIO7_ADDRESS = 100316;
        public const long DEVICE_AVAILABLE_RADIO7_LAST_FRAME_TIMESTAMP = 100317;
        public const long DEVICE_AVAILABLE_RADIO7_FRAME_COUNT = 100318;
        public const long DEVICE_AVAILABLE_RADIO8_SIGNAL_QUALITY = 100319;
        public const long DEVICE_AVAILABLE_RADIO8_ADDRESS = 100320;
        public const long DEVICE_AVAILABLE_RADIO8_LAST_FRAME_TIMESTAMP = 100321;
        public const long DEVICE_AVAILABLE_RADIO8_FRAME_COUNT = 100322;
        public const long DEVICE_AVAILABLE_RADIO9_SIGNAL_QUALITY = 100323;
        public const long DEVICE_AVAILABLE_RADIO9_ADDRESS = 100324;
        public const long DEVICE_AVAILABLE_RADIO9_LAST_FRAME_TIMESTAMP = 100325;
        public const long DEVICE_AVAILABLE_RADIO9_FRAME_COUNT = 100326;
        public const long DEVICE_AVAILABLE_RADIO10_SIGNAL_QUALITY = 100327;
        public const long DEVICE_AVAILABLE_RADIO10_ADDRESS = 100328;
        public const long DEVICE_AVAILABLE_RADIO10_LAST_FRAME_TIMESTAMP = 100329;
        public const long DEVICE_AVAILABLE_RADIO10_FRAME_COUNT = 100330;
        public const long DEVICE_AVAILABLE_RADIO11_SIGNAL_QUALITY = 100331;
        public const long DEVICE_AVAILABLE_RADIO11_ADDRESS = 100332;
        public const long DEVICE_AVAILABLE_RADIO11_LAST_FRAME_TIMESTAMP = 100333;
        public const long DEVICE_AVAILABLE_RADIO11_FRAME_COUNT = 100334;
        public const long DEVICE_AVAILABLE_RADIO12_SIGNAL_QUALITY = 100335;
        public const long DEVICE_AVAILABLE_RADIO12_ADDRESS = 100336;
        public const long DEVICE_AVAILABLE_RADIO12_LAST_FRAME_TIMESTAMP = 100337;
        public const long DEVICE_AVAILABLE_RADIO12_FRAME_COUNT = 100338;
        public const long DEVICE_AVAILABLE_RADIO13_SIGNAL_QUALITY = 100339;
        public const long DEVICE_AVAILABLE_RADIO13_ADDRESS = 100340;
        public const long DEVICE_AVAILABLE_RADIO13_LAST_FRAME_TIMESTAMP = 100341;
        public const long DEVICE_AVAILABLE_RADIO13_FRAME_COUNT = 100342;
        public const long DEVICE_AVAILABLE_RADIO14_SIGNAL_QUALITY = 100343;
        public const long DEVICE_AVAILABLE_RADIO14_ADDRESS = 100344;
        public const long DEVICE_AVAILABLE_RADIO14_LAST_FRAME_TIMESTAMP = 100345;
        public const long DEVICE_AVAILABLE_RADIO14_FRAME_COUNT = 100346;
        public const long DEVICE_AVAILABLE_RADIO15_SIGNAL_QUALITY = 100347;
        public const long DEVICE_AVAILABLE_RADIO15_ADDRESS = 100348;
        public const long DEVICE_AVAILABLE_RADIO15_LAST_FRAME_TIMESTAMP = 100349;
        public const long DEVICE_AVAILABLE_RADIO15_FRAME_COUNT = 100350;
        public const long DEVICE_AVAILABLE_RADIO16_SIGNAL_QUALITY = 100351;
        public const long DEVICE_AVAILABLE_RADIO16_ADDRESS = 100352;
        public const long DEVICE_AVAILABLE_RADIO16_LAST_FRAME_TIMESTAMP = 100353;
        public const long DEVICE_AVAILABLE_RADIO16_FRAME_COUNT = 100354;
        public const long DEVICE_AVAILABLE_RADIO17_SIGNAL_QUALITY = 100355;
        public const long DEVICE_AVAILABLE_RADIO17_ADDRESS = 100356;
        public const long DEVICE_AVAILABLE_RADIO17_LAST_FRAME_TIMESTAMP = 100357;
        public const long DEVICE_AVAILABLE_RADIO17_FRAME_COUNT = 100358;
        public const long DEVICE_AVAILABLE_RADIO18_SIGNAL_QUALITY = 100359;
        public const long DEVICE_AVAILABLE_RADIO18_ADDRESS = 100360;
        public const long DEVICE_AVAILABLE_RADIO18_LAST_FRAME_TIMESTAMP = 100361;
        public const long DEVICE_AVAILABLE_RADIO18_FRAME_COUNT = 100362;
        public const long DEVICE_AVAILABLE_RADIO19_SIGNAL_QUALITY = 100363;
        public const long DEVICE_AVAILABLE_RADIO19_ADDRESS = 100364;
        public const long DEVICE_AVAILABLE_RADIO19_LAST_FRAME_TIMESTAMP = 100365;
        public const long DEVICE_AVAILABLE_RADIO19_FRAME_COUNT = 100366;
        public const long DEVICE_AVAILABLE_RADIO20_SIGNAL_QUALITY = 100367;
        public const long DEVICE_AVAILABLE_RADIO20_ADDRESS = 100368;
        public const long DEVICE_AVAILABLE_RADIO20_LAST_FRAME_TIMESTAMP = 100369;
        public const long DEVICE_AVAILABLE_RADIO20_FRAME_COUNT = 100370;
        public const long DEVICE_AVAILABLE_RADIO21_SIGNAL_QUALITY = 100371;
        public const long DEVICE_AVAILABLE_RADIO21_ADDRESS = 100372;
        public const long DEVICE_AVAILABLE_RADIO21_LAST_FRAME_TIMESTAMP = 100373;
        public const long DEVICE_AVAILABLE_RADIO21_FRAME_COUNT = 100374;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER0_GSM_QUALITY = 100280;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER0_MOBILE_NETWORK_CODE = 100281;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER1_GSM_QUALITY = 100282;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER1_MOBILE_NETWORK_CODE = 100283;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER2_GSM_QUALITY = 100284;
        public const long DEVICE_EVENT_SCAN_GSM_PROVIDER2_MOBILE_NETWORK_CODE = 100285;
        #endregion

        //utworzone testowo, przy wersji finalnej zaimportowa� z serwera docelowego
        #region AMPLI datatypes
        public const long DEVICE_FRAME_PERIOD = 100832;    // <Brak opisu>
        public const long DEVICE_ENERGY_CONSUMPTION = 100833;    // <Brak opisu>
        public const long DEVICE_BATTERY_MODE = 100834;    // <Brak opisu>
        public const long DEVICE_WORK_TIME = 100835;    // <Brak opisu>
        public const long DEVICE_SLEEP_TIME = 100836;    // <Brak opisu>
        public const long DEVICE_IMRLAN_RX_TIME = 100837;    // <Brak opisu>
        public const long LINK_TX_DIRECT = 100844;    // <Brak opisu>
        public const long LINK_RX_DATA = 100845;    // <Brak opisu>
        public const long LINK_RX_TIMEOUT = 100846;    // <Brak opisu>
        public const long LINK_RX_MODE = 100847;    // <Brak opisu>
        public const long LINK_ID_REQUEST = 100848;    // <Brak opisu>
        public const long LINK_ACTIVITY = 100849;    // <Brak opisu>
        public const long LINK_ROUTE_OUTPUT = 100850;    // <Brak opisu>
        public const long LINK_FRAME_OUTPUT = 100851;    // <Brak opisu>
        public const long LINK_TX_FRAME_COUNTER = 100852;    // <Brak opisu>
        public const long LINK_RX_FRAME_COUNTER = 100853;    // <Brak opisu>
        public const long DEVICE_AIR_MODE = 100854;    // <Brak opisu>
        public const long DEVICE_AIR_MODE_REMAINING = 100855;    // <Brak opisu>
        public const long DEVICE_AIR_MODE1_TIMEOUT = 100856;    // <Brak opisu>
        public const long DEVICE_AIR_MODE1_POWER_MODE = 100857;    // <Brak opisu>
        public const long DEVICE_AIR_MODE1_TIMEREC_AS = 100858;    // <Brak opisu>
        public const long DEVICE_AIR_MODE1_TIMEPROBE = 100859;    // <Brak opisu>
        public const long DEVICE_AIR_MODE2_TIMEOUT = 100860;    // <Brak opisu>
        public const long DEVICE_AIR_MODE2_POWER_MODE = 100861;    // <Brak opisu>
        public const long DEVICE_AIR_MODE2_TIMEREC_AS = 100862;    // <Brak opisu>
        public const long DEVICE_AIR_MODE2_TIMEPROBE = 100863;    // <Brak opisu>
        public const long DEVICE_AIR_TX_R2S = 100864;    // <Brak opisu>
        public const long DEVICE_AIR_TX_DOWN_SET_POWER = 100865;    // <Brak opisu>
        public const long DEVICE_AIR_TX_DOWN_DEFAULT_POWER = 100866;    // <Brak opisu>
        public const long DEVICE_AIR_TX_UP_SET_POWER = 100867;    // <Brak opisu>
        public const long DEVICE_AIR_TX_UP_DEFAULT_POWER = 100868;    // <Brak opisu>
        public const long DEVICE_AIR_POWER_AMPLI_FRAME = 100869;    // <Brak opisu>
        public const long DEVICE_AIR_POWER_RETRANSMISSION_LO = 100870;    // <Brak opisu>
        public const long DEVICE_AIR_RESET = 100871;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_COUNT = 100872;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_ADDRESS = 100873;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_IN_QUALITY_LAST = 100874;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_OUT_QUALITY_LAST = 100875;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_IN_QUALITY_AVG = 100876;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_OUT_QUALITY_AVG = 100877;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_IN_COUNT = 100878;    // <Brak opisu>
        public const long DEVICE_AIR_CALLBACK_IN_PERCENT = 100879;    // <Brak opisu>
        public const long AIR_CONFIG_ADDRESS_MAN_CODE = 100881;    // <Brak opisu>
        public const long AIR_CONFIG_ADDRESS_IMR_ADDRESS = 100882;    // <Brak opisu>
        public const long AIR_CONFIG_ADDRESS_VERSION = 100883;    // <Brak opisu>
        public const long AIR_CONFIG_ADDRESS_DEVICE_TYPE = 100884;    // <Brak opisu>
        public const long AIR_CONFIG_CPOWER_MODE = 100885;    // <Brak opisu>
        public const long AIR_CONFIG_CPASS_DOWN_MODE = 100886;    // <Brak opisu>
        public const long AIR_CONFIG_USART_BAUD = 100887;    // <Brak opisu>
        public const long AIR_CONFIG_USART_STOPBITS = 100888;    // <Brak opisu>
        public const long AIR_CONFIG_USART_PARITY = 100889;    // <Brak opisu>
        public const long AIR_CONFIG_USART_XFEATURES_BYTE = 100890;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_FEATURES_BYTES = 100891;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_TIMEREC_AS = 100892;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_TIMEPROBE = 100893;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_PREAMBLE_PATTERN = 100894;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_PREAMBLE_TIMES = 100895;    // <Brak opisu>
        public const long AIR_CONFIG_WMBUS_DELIMITER = 100896;    // <Brak opisu>
        public const long AIR_CONFIG_USART_TIMEOUT = 100897;    // <Brak opisu>
        public const long AIR_CONFIG_RFFRAMEREC_TIMEOUT = 100898;    // <Brak opisu>
        public const long AIR_CONFIG_RADIO_CLOCK_TIMEOUT = 100899;    // <Brak opisu>
        public const long AIR_CONFIG_USART_TRANSMIT_TIMEOUT = 100900;    // <Brak opisu>
        public const long AIR_CONFIG_XPROCESS_PREPROCESS_FUNCTION = 100901;    // <Brak opisu>
        public const long AIR_CONFIG_XPROCESS_POSTPROCES_FUNCTION = 100902;    // <Brak opisu>
        public const long AIR_CONFIG_CC1020_CONFIG_BYTES = 100903;    // <Brak opisu>
        public const long AIR_CONFIG_CC1020_DOWNSEND_POWER = 100904;    // <Brak opisu>
        public const long AIR_CONFIG_CC1020_UPSEND_POWER = 100905;    // <Brak opisu>
        public const long AIR_CONFIG_CC1020_FREQ_DRIFT = 100906;    // <Brak opisu>
        public const long AIR_CONFIG_CC1020_RAMPING_TYPE = 100907;    // <Brak opisu>
        public const long AIR_CONFIG_RELAY_NODES_RELAYALL = 100909;    // <Brak opisu>
        public const long AIR_CONFIG_RELAY_NODES_ADDRESS = 100910;    // <Brak opisu>
        public const long AIR_CONFIG_RELAY_GATEWAYS_RELAYALL = 100912;    // <Brak opisu>
        public const long AIR_CONFIG_RELAY_GATEWAYS_ADDRESS = 100913;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_TRANSMIT_COUNTER = 100914;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_RECEIVE_COUNTER = 100915;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_USART_RCOUNTER = 100916;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_USART_TCOUNTER = 100917;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_EEPROM_RCOUNTER = 100918;    // <Brak opisu>
        public const long AIR_CONFIG_POWER_COUNTERS_EEPROM_WCOUNTER = 100919;    // <Brak opisu>
        public const long AIR_CONFIG_KNOWN_NODES_ELEMENTS = 100920;    // <Brak opisu>
        public const long AIR_CONFIG_KNOWN_NODES_ADDRESS = 100921;    // <Brak opisu>
        public const long AIR_CONFIG_KNOWN_NODES_QUALITY = 100922;    // <Brak opisu>
        public const long AIR_CONFIG_KNOWN_NODES_FRAME_COUNT = 101271;
        public const long DEVICE_COM1_DATA_BITS = 100923;    // <Brak opisu>
        public const long DEVICE_COM1_STOP_BITS = 100924;    // <Brak opisu>
        public const long WMBUS_ADDRESS = 100925;    // <Brak opisu>
        public const long IMRLAN_RTC_READ_INTERVAL = 100926;    // <Brak opisu>
        public const long IMRLAN_LEARNING_PERIOD = 100927;    // <Brak opisu>
        public const long IMRLAN_LEARNING_DURATION = 100928;    // <Brak opisu>
        public const long IMRLAN_TIME_SCAN_WINDOW = 100929;    // <Brak opisu>
        public const long DEVICE_LISTENING_ON = 100930;    // <Brak opisu>
        public const long DEVICE_LISTENING_PERIOD_COUNT = 100931;    // <Brak opisu>
        public const long DEVICE_DURATION_DEFAULT = 100932;    // <Brak opisu>
        public const long DEVICE_DURATION_LO = 100933;    // <Brak opisu>
        public const long DEVICE_FILTER_WILDCARD_ADDRESS = 100934;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_LATCH_INTERVAL = 100935;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_CLEAR = 100936;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_ON = 100937;    // <Brak opisu>
        public const long DEVICE_ARCHIVE_TIME = 100938;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_ADDRESS = 100939;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_LINK = 100940;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_COUNT = 100941;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_FRAMES_PERIOD = 100942;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_LAST_FRAME_TIME = 100943;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_AVG_QUALITY = 100944;    // <Brak opisu>
        public const long DEVICE_IMRSCAN_LAST_FRAME_QUALITY = 100945;    // <Brak opisu>
        public const long SIGNAL_LEVEL = 100946;
        public const long FUEL_PRESSURE = 100947;
        public const long FUEL_DENSITY = 100948;
        public const long PUMP_ACTION_DURATION = 100949;
        public const long LEAKAGE_ALARM = 100950;
        public const long HIGH_FUEL_LEVEL_ALARM = 100951;
        public const long ALARM = 100952;
        public const long ALARM_ACTIVE = 100953;
        public const long LEAKAGE_DOUBLE_WALL_ALARM = 100954;
        public const long FUEL_DENSITY_ALARM = 100955;
        public const long SIGNAL_LEVEL_DIFF = 102051;


        public const long AIR_CONFIG_PASS_SLOT_INTERVAL = 101550;
        public const long AIR_CONFIG_PASS_SLOT_NUMBER = 101551;
        #endregion

        #region WMBUS
        //bblach 27.09.2010 zmieni�em warto�ci bo k��ci�y si� z tymi w bazie
        //sprawdzi� czy te datatypy s� wog�le wykorzystywane i doda� do bazy
        public const long SEND_AMPLI_DATA = 999612;    // <Brak opisu>
        public const long AIR_NEXT_HOP = 999611;    // <Brak opisu>
        public const long WMBUS_ACKNOWLEDGE_ENABLED = 101377;
        #endregion
        #endregion

        #region Constructor
        public DataType(long IdDataType, string Name, string Description, string ExtDescription, Enums.DataTypeClass Class, DataTypeFormat Format, bool IsRemoteRead, bool IsRemoteWrite, bool? IsArchiveOnly, bool? IsEditable, int IdUnit, string UnitSymbol, Enums.ReferenceType ReferenceType)
        {
            this.idDataType = IdDataType;
            this.name = Name;
            this.description = Description;
            this.extDescription = ExtDescription;
            this.dtClass = Class;
            this.Format = Format;
            this.isArchiveOnly = IsArchiveOnly;
            this.isRemoteRead = IsRemoteRead;
            this.isRemoteWrite = IsRemoteWrite;
            this.idUnit = IdUnit;
            this.unitSymbol = UnitSymbol;
            this.referenceType = ReferenceType;
        }
        public DataType(long IdDataType, string Name, string Description, string ExtDescription, int ClassId, bool IsRemoteRead, bool IsRemoteWrite, bool? IsArchiveOnly, bool? IsEditable, int IdUnit, string UnitSymbol, int? ReferenceTypeId)
            : this(IdDataType, Name, Description, ExtDescription, Enums.DataTypeClass.Unknown, null, IsRemoteRead, IsRemoteWrite, IsArchiveOnly, IsEditable, IdUnit, UnitSymbol, Enums.ReferenceType.None)
        {
            if (Enum.IsDefined(typeof(Enums.DataTypeClass), ClassId))
            {
                this.dtClass = (Enums.DataTypeClass)ClassId;
            }

            if (ReferenceTypeId.HasValue && Enum.IsDefined(typeof(Enums.ReferenceType), ReferenceTypeId))
            {
                this.referenceType = (Enums.ReferenceType)ReferenceTypeId;
            }
        }

        public DataType(long IdDataType)
            : this(IdDataType, "", "", "", Enums.DataTypeClass.Unknown, null, false, false, null, null, 0, "", Enums.ReferenceType.None)
        {
        }
        public DataType(long IdDataType, string Name)
            : this(IdDataType, Name, Enums.DataTypeClass.Unknown)
        {
        }
        public DataType(long IdDataType, string Name, Enums.DataTypeClass Class)
            : this(IdDataType, Name, "", "", Class, null, false, false, null, null, 0, "", Enums.ReferenceType.None)
        {
        }
        #endregion

        #region GetName && GetId && GetIds
        internal static Dictionary<long, FieldInfo> dataTypeDict = null;
        private static bool GetDataTypeFiels()
        {
            if (dataTypeDict != null)
                return true;

            Type t = Type.GetType("IMR.Suite.Common.DataType");
            if (t == null)
                return false;

            FieldInfo[] dataTypeFields = t.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            if (dataTypeFields == null)
                return false;
            dataTypeDict = new Dictionary<long, FieldInfo>();
            //Foreach jest zasadny je�li istnieje mo�liwo��, �e w pliku pojawi� si� dubluj�ce si� identyfikatory DataType
            dataTypeFields.ToList().ForEach(d => dataTypeDict[(long)d.GetValue(null)] = d);

            return true;
        }
        public static string GetName(long idDataType)
        {
            try
            {
                if (!GetDataTypeFiels())
                    return UNKNOWN_DATA_TYPE;
#if !(WINDOWS_PHONE)
                FieldInfo fiItem = dataTypeDict.TryGetValue(idDataType);
                if (fiItem != null && !String.IsNullOrEmpty(fiItem.Name))
                    return fiItem.Name;
#else
                FieldInfo fiItem;
                if (dataTypeDict.TryGetValue(idDataType, out fiItem))
                    if (fiItem != null && !String.IsNullOrEmpty(fiItem.Name))
                        return fiItem.Name;
#endif

                //FieldInfo field = dataTypeFields.SingleOrDefault(f => (long)f.GetValue(null) == idDataType);
                //if (field != null)
                //    return field.Name;

                return UNKNOWN_DATA_TYPE;
            }
            catch
            {
                return UNKNOWN_DATA_TYPE;
            }
        }

        public static long GetId(string idDataTypeName)
        {
            try
            {
                if (!GetDataTypeFiels())
                    return -1;
                //FieldInfo field = dataTypeFields.SingleOrDefault(f => f.Name == idDataTypeName);
                //if (field != null)
                //    return Convert.ToInt64(field.GetValue(null));

                KeyValuePair<long, FieldInfo> field = dataTypeDict.SingleOrDefault(f => f.Value.Name == idDataTypeName);
                if (!field.Equals(default(KeyValuePair<long, FieldInfo>)))
                    return field.Key;

                return -1;
            }
            catch
            {
                return -1;
            }
        }

        public static List<Tuple<string, long>> GetIds(string idDataTypeName)
        {
            List<Tuple<string, long>> result = new List<Tuple<string, long>>();
            try
            {
                if (!GetDataTypeFiels())
                    return result;
                //List<FieldInfo> fields = dataTypeFields.Where(w => w.Name.Contains(idDataTypeName)).ToList();
                //if (fields != null && fields.Count > 0)
                //    return fields.Select(s => Tuple.Create<string, long>(s.Name, Convert.ToInt64(s.GetValue(null)))).ToList();

                result = dataTypeDict.Where(w => w.Value.Name.Contains(idDataTypeName)).Select(w => new Tuple<string, long>(w.Value.Name, w.Key)).ToList();
                if (result != null && result.Count > 0)
                    return result;

                return new List<Tuple<string, long>>(); ;
            }
            catch
            {
                return result;
            }
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is DataType)
                return this.idDataType == ((DataType)obj).idDataType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(DataType left, DataType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(DataType left, DataType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return idDataType.GetHashCode();
        }
        #endregion
        #region override ToString
        public override string ToString()
        {
            return String.Format("Name:{0}, IdDataType: {1}, Description: {2}, Class: {3}", Name, IdDataType, Description, Class.ToString());
        }
        #endregion

        //------------------------------------------------
        // te sa do AMPLI5501:
        public const long DEVICE_DIAGNOSTIC_DURATION = 101289;
        public const long DEVICE_ACTIVATION_PERIOD = 101290;
        public const long DEVICE_RADIO_SCAN_WINDOW_WIDTH = 101291;
        public const long DEVICE_RETRANSMITION_RETRIES = 101292;
        public const long DEVICE_RETRANSMITION_MULTIPLIER = 101293;
        public const long DEVICE_RETRANSMITION_OLD_RETRIES = 101294;
        public const long DEVICE_MODE_SCAN_FRAMES_COUNT = 101295;
        public const long DEVICE_WAKE_UP_PERIOD = 101296;
        public const long DEVICE_WAKE_UP_TIME = 101297;
        public const long DEVICE_WORK_MODE = 101298;
        public const long DEVICE_RUN_MODE = 101299;
        public const long LINK_RADIO433_TX_ENABLE = 101343;
        public const long LINK_RADIO433_RX_ENABLE = 101344;
        public const long LINK_CABLE_TX_ENABLE = 101345;
        public const long LINK_CABLE_RX_ENABLE = 101346;
        public const long LINK_CABLE_TX_DELAY = 101381;
        //-------------------------------------------------
        // a te do akcji krecenia zaworem:
        public const long DEVICE_MOVE_VALVE_SAFE_MODE_ENABLED = 101303;
        public const long DEVICE_MOVE_VALVE_REPEAT_COUNT = 101304;
        //-------------------------------------------------
        // do olana (ramka radiowa i archiwum)
        public const long DEVICE_ARCHIVE_DAYLONG_SUMMARY_DATA_VALID_MARK = 101355;
        public const long DEVICE_ARCHIVE_MONTHLONG_SUMMARY_DATA_VALID_MARK = 101356;
        public const long REMOVE_LITER_COUNTER = 101357;
        public const long LAST_MONTH_MAXIMUM_FLOW_M3_COEFFICIENT = 101358;
        public const long LAST_MONTH_MAXIMUM_FLOW_TIMESTAMP = 101359;
        public const long MAX_HOUR_FLOW_M3_COEFFICIENT = 101360;
        public const long MAX_HOUR_FLOW_TIMESTAMP = 101361;

        public const long GAS_METER_DISPLAY_VALUE = 101362;
        public const long MAXIMUM_FLOW_M3_COEFFICIENT = 101363;
        public const long MAXIMUM_FLOW_TIMESTAMP = 101364;
        public const long MAX_HOUR_FLOW = 101365;
    }
}