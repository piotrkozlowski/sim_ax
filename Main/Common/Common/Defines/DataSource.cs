﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
    #region DataSource
    [Serializable]
    public class DataSource
    {
        #region Members

        #region IdDataSource
        private long? idDataSource;

        public long? IdDataSource
        {
            get
            {
                return idDataSource;
            }
            set
            {
                idDataSource = value;
            }
        }
        #endregion        

        #region IdDataSourceType
        private Enums.DataSourceType idDataSourceType = Enums.DataSourceType.Unknown;

        public Enums.DataSourceType IdDataSourceType
        {
            get
            {
                return idDataSourceType;
            }
            set
            {
                idDataSourceType = value;
            }
        }
        #endregion

        #endregion

        #region Constructor
        public DataSource()
        {
        }

        public DataSource(long? IdDataSource, Enums.DataSourceType IdDataSourceType)
        {
            this.idDataSource = IdDataSource;
            this.idDataSourceType = IdDataSourceType;
        }      
        #endregion        
    }
    #endregion

    #region DataSourceDevice
    [Serializable]
    public class DataSourceDevice : DataSource
    {
        #region Members
        #region SerialNbr
        private SN serialNbr = null;

        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }            
        }
        #endregion

        #region Time
        private DateTime time;

        public DateTime Time
        {
            get
            {
                return time;
            }
        }
        #endregion

        #region PacketId
        private long packetId;
        public long PacketId
        {
            get { return packetId; }           
        }
        #endregion

        #region ActionId
        private long? actionId;
        public long? ActionId
        {
            get { return actionId; }
        }
        #endregion

      
        #endregion

        #region Constructor
        public DataSourceDevice()
        {
        }

        public DataSourceDevice(SN SerialNbr, DateTime Time, long PacketId, long? ActionId)
            :base(null,Enums.DataSourceType.OKO)
        {
            this.serialNbr = SerialNbr;
            this.time = Time;
            this.packetId = PacketId;
            if (ActionId.HasValue)
                this.actionId = ActionId.Value;
            else
                this.actionId = null;
        }      
        #endregion        
    }
    #endregion DataSourceDevice

    #region DataSourceWithData
    [Serializable]
    public class DataSourceWithData : DataSource
    {
        #region Members

        #region DataSourceData
        private List<DataCurrentValue> dataSourceData = new List<DataCurrentValue>();

        public List<DataCurrentValue> DataSourceData
        {
            get
            {
                return dataSourceData;
            }
            set
            {
                dataSourceData = DataSourceData;
            }
        }
        #endregion

        #region PacketId
        private long packetId;
        public long PacketId
        {
            get { return packetId; }
            set { packetId = value; }
        }
        #endregion

        #endregion

        #region Constructor
        public DataSourceWithData()
        {
        }

        public DataSourceWithData(long idDataSource, Enums.DataSourceType idDataSourceType, List<DataCurrentValue> dataSourceData)
        {
            this.IdDataSource = idDataSource;
            this.PacketId = idDataSource;
            this.IdDataSourceType = idDataSourceType;
            this.DataSourceData = dataSourceData;
        }

        public DataSourceWithData(List<DataCurrentValue> dataSourceData)
        {
            this.IdDataSource = (from item in dataSourceData select (long)item.IdDataSource).FirstOrDefault();
            this.PacketId = (from item in dataSourceData select (long)item.IdDataSource).FirstOrDefault();
            this.IdDataSourceType = (from item in dataSourceData select (Enums.DataSourceType)item.SourceType).FirstOrDefault();
            this.DataSourceData.AddRange(dataSourceData);
        }
        #endregion
    }
    #endregion DataSourceDevice
}
