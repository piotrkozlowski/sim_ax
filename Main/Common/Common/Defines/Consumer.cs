﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
    [Serializable]
    public class Consumer
    {
        public string Name;
        public string Surname;
        public string City;
        public string Address;
        public string ActorName;
        public int IdConsumer = 0;

        public Double CreditPp;
        public Double CreditEc;
        public long DeviceSN;

        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3}", Name, Surname, City, Address);
        }

        public Consumer() { }

        public Consumer(string Name, string Surname)
        {
            this.Name = Name;
            this.Surname = Surname;
        }
    }
}
