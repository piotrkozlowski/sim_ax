﻿using System;
using System.Net;
using System.Diagnostics;

namespace IMR.Suite.Common
{
	[Serializable]
	public class IPv6Address
	{
		#region internal Members
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		internal IPAddress Address;
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		internal IPAddress MaskedAddress;
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		internal ushort[] IPNumbers = new ushort[8];
		#endregion

		#region internal PrepareIPAddress
		internal void PrepareIPAddress()
		{
			byte[] ipBytes = new byte[16];
			for (int i = 0, j = 0; i < 8; i++)
			{
				ipBytes[j++] = (byte)(IPNumbers[i] >> 8);
				ipBytes[j++] = (byte)IPNumbers[i];
			}
			Address = new IPAddress(ipBytes);

			byte[] prefixBytes = new byte[16];
			int index = 0;
			for (index = 0; index < Prefix / 8; index++)
				prefixBytes[index] = 0xFF;
			if ((Prefix % 8) != 0)
				prefixBytes[index] = (byte)(-128 >> ((Prefix % 8) - 1));

			byte[] maskedIPBytes = new byte[16];
			for (int i = 0; i < 16; i++)
				maskedIPBytes[i] = (byte)(ipBytes[i] & prefixBytes[i]);
			MaskedAddress = new IPAddress(maskedIPBytes);
		}
		#endregion

		#region Constructors
		public IPv6Address()
		{
			IsUniqueLocal = true;
			LinkType = Enums.TransmissionDriverType.Unknown;
		}
		public IPv6Address(string ipString)
		{
			Parse(ipString);
		}
		public IPv6Address(string ipString, byte prefix)
		{
			this.prefix = prefix;
			Parse(ipString);
		}
		#endregion

		#region LinkType
		public Enums.TransmissionDriverType LinkType
		{
			get
			{
				return (Enums.TransmissionDriverType)IPNumbers[2];
			}
			set { IPNumbers[2] = (ushort)value; PrepareIPAddress(); }
		}
		#endregion
		#region MobileNetworkCode
		public ushort MobileNetworkCode
		{
			get
			{
				return IPNumbers[3];
			}
			set { IPNumbers[3] = value; PrepareIPAddress(); }
		}
		#endregion
		#region IDProvider
		public ushort IDProvider
		{
			get
			{
				if (IsUniqueLocal)
					return Convert.ToUInt16(IPNumbers[4].ToString("X"), 10);
				else
					return 0;
			}
			set
			{
				if (IsUniqueLocal)
				{
					IPNumbers[4] = Convert.ToUInt16(value.ToString(), 16);
					PrepareIPAddress();
				}
			}
		}
		#endregion
		#region IDTransmissionDistributor
		public ushort IDTransmissionDistributor
		{
			get
			{
				if (IsLinkLocal)
					return Convert.ToUInt16(IPNumbers[4].ToString("X"), 10);
				else
					return 0;
			}
			set
			{
				if (IsLinkLocal)
				{
					IPNumbers[4] = Convert.ToUInt16(value.ToString(), 16); ;
					PrepareIPAddress();
				}
			}
		}
		#endregion
		#region IDDistributor
		public ushort IDDistributor
		{
			get
			{
				return Convert.ToUInt16(IPNumbers[5].ToString("X"), 10);
			}
			set
			{
				IPNumbers[5] = Convert.ToUInt16(value.ToString(), 16); 
				PrepareIPAddress();
			}
		}
		#endregion
		#region SerialNumber
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private SN serialNbr = new SN();
		public SN SerialNbr
		{
			get
			{
				if (IsUniqueLocal)
					return serialNbr;
				else
					return null;
			}
			set
			{
				if (IsUniqueLocal)
				{
					serialNbr = value;
					byte[] bytes = BitConverter.GetBytes(IPAddress.NetworkToHostOrder((int)serialNbr.Value.Value));
					for (int i = 0; i < 2; i++)
						IPNumbers[6 + i] = (ushort)((bytes[i * 2] * 0x100) + bytes[(i * 2) + 1]);
					PrepareIPAddress();
				}
			}
		}
		#endregion
		#region IDServer
		public ushort? IDServer
		{
			get
			{
				if (IsLinkLocal)
					return Convert.ToUInt16(IPNumbers[6].ToString("X"), 10);
				return null;
			}
			set
			{
				if (IsLinkLocal && value.HasValue)
				{
					IPNumbers[6] = Convert.ToUInt16(value.Value.ToString(), 16);
					PrepareIPAddress();
				}
			}
		}
		#endregion
		#region IDDriver
		public ushort? IDDriver
		{
			get
			{
				if (IsLinkLocal)
					return Convert.ToUInt16(IPNumbers[7].ToString("X"), 10);
				return null;
			}
			set
			{
				if (IsLinkLocal && value.HasValue)
				{
					IPNumbers[7] = Convert.ToUInt16(value.Value.ToString(), 16);
					PrepareIPAddress();
				}
			}
		}
		#endregion

		#region Prefix
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		private byte prefix = 128;
		public byte Prefix
		{
			get { return prefix; }
			set { prefix = value; PrepareIPAddress(); }
		}
		#endregion

		#region IsLinkLocal
		public bool IsLinkLocal
		{
			// Link local addresses 
			// fe80::/10
			get { return (IPNumbers[0] & 0xffc0) == 0xfe80; }
			set
			{
				if (value)
					IPNumbers[0] |= 0xfe80;
				else
					IPNumbers[0] &= 0x017f; // negative 0xfe80;
				PrepareIPAddress();
			}
		}
		#endregion
		#region IsUniqueLocal
		public bool IsUniqueLocal
		{
			// Unique local addresses (ULA) 
			// fc00::/7
			get { return (this.IPNumbers[0] & 0xfe00) == 0xfc00; }
			set
			{
				if (value)
					IPNumbers[0] |= 0xfc00;
				else
					IPNumbers[0] &= 0x03ff; // negative 0xfc00;
				PrepareIPAddress();
			}
		}
		#endregion

		#region Equals
		//public bool Equals(IPv6Address comparand)
		//{
		//    return Address.Equals(comparand.Address);
		//}
		#endregion
		#region EqualsMasked
		public bool EqualsMasked(IPv6Address comparand)
		{
			return MaskedAddress.Equals(comparand.MaskedAddress);
		}
		#endregion
		#region Operators
		public override bool Equals(object obj)
		{
			if (obj is IPv6Address)
			{
				return this.Address.Equals(((IPv6Address)obj).Address);
			}
			if (obj == null)
			{
				return this.Address == null;
			}
			return base.Equals(obj);
		}
		public static bool operator ==(IPv6Address left, IPv6Address right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(IPv6Address left, IPv6Address right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}
		public override int GetHashCode()
		{
			return Address.GetHashCode();
		}
		#endregion

		#region Parse
		public void Parse(string ipString)
		{
			Address = IPAddress.Parse(ipString);
			byte[] bytes = Address.GetAddressBytes();
			for (int i = 0; i < 8; i++)
				IPNumbers[i] = (ushort)((bytes[i * 2] * 0x100) + bytes[(i * 2) + 1]);
			serialNbr = new SN((ulong)IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 12)));
			PrepareIPAddress();
		}
		#endregion

		#region Clone
		public IPv6Address Clone()
		{
			IPv6Address c = new IPv6Address();
			c.Address = this.Address;
			c.MaskedAddress = this.MaskedAddress;
			c.LinkType = this.LinkType;
			c.MobileNetworkCode = this.MobileNetworkCode;
			c.IDProvider = this.IDProvider;
			c.IDTransmissionDistributor = this.IDTransmissionDistributor;
			c.IDDistributor = this.IDDistributor;
			c.SerialNbr = this.SerialNbr;
			c.IDServer = this.IDServer;
			c.IDDriver = this.IDDriver;
			c.Prefix = this.Prefix;

			return c;
		}
		#endregion

		#region override ToString()
		public override string ToString()
		{
			return Address.ToString();
		}
		#endregion
	}
}
