using System;
using System.Collections.Generic;

namespace IMR.Suite.Common
{
    [Serializable]
	public class Location
    {
        #region Members

        public long IdLocation { get; set; }

        public Enums.LocationType Type { get; set; }
        public long? PatternIdLocation { get; set; }
        public string PatternName { get; set; }

        public int IdDistributor { get; set; }
        public int? IdConsumer { get; set; }
        
        public Enums.LocationState State { get; set; }

        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string CID { get; set; }
        public bool InKPI { get; set; }
        public bool AllowGrouping { get; set; } 

        #endregion
        
        #region Constructor
        public Location(long IdLocation, Enums.LocationType Type, long? PatternIdLocation, string PatternName, int IdDistributor, int? IdConsumer)
            : this()
        {
            this.IdLocation = IdLocation;
            this.Type = Type;
            this.PatternIdLocation = PatternIdLocation;
            this.PatternName = PatternName;
            this.IdDistributor = IdDistributor;
            this.IdConsumer = IdConsumer;
        }

        public Location(long IdLocation, int TypeId, long? PatternIdLocation, string PatternName, int IdDistributor, int? IdConsumer)
            : this(IdLocation, Enums.LocationType.Unknown, PatternIdLocation, PatternName, IdDistributor, IdConsumer)
        {
            if (Enum.IsDefined(typeof(Enums.LocationType), TypeId))
            {
                this.Type = (Enums.LocationType)TypeId;
            }
        }

        public Location()
        {
            this.State = Enums.LocationState.NEW;
            this.IdDistributor = -1;
            this.IdLocation = -1;
        }

        #endregion

        public override string ToString()
        {
            return String.Format("{0} {1} ({2})",City,Address, CID);
        }
    }

    public class LocationType
    {
        public int Id;
        public string Name;
        public string Descr;

        public LocationType()
        {
        }

        public LocationType(int Id, string Name, string Descr)
        {
            this.Id = Id;
            this.Name = Name;
            this.Descr = Descr;
        }

        public override string ToString()
        {
            if (Descr == "")
                return Name;
            return Descr;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(LocationType))
                return false;
            return ((obj as LocationType).Id == Id);
        }
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }

    public class LocationHierarchy
    {
        public long? IdTreeElemParent;
        public long IdTreeElem;
        public long IdTreeLocation;
        public string Name;

        public LocationHierarchy parent;
        public LocationHierarchy[] children;

        public override string ToString()
        {
            return Name;
        }

    }

    
}
