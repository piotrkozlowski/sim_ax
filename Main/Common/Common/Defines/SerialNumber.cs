﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace IMR.Suite.Common
{
	// Klasa nazwa jako SN z powodu aby wiekszosc 
	// definicji nie wygladala w ten sposob: public SerialNumber SerialNumber;
	[Serializable]
	public class SN
	{
		#region Value
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		internal ulong? _Value;
		public ulong? Value
		{
			get { return _Value; }
			set
			{
				_Value = value;
				if (!_Value.HasValue)
				{
					_DBValue = _Value;
				}
				else
				{
					if (_Value == ulong.MaxValue)
					{
						_DBValue = ulong.MaxValue;
						return;
					}

					try
					{
                        //_DBValue = ulong.Parse(_Value.Value.ToString("X"), System.Globalization.NumberStyles.Integer);
                        _DBValue = Convert.ToUInt64(_Value.Value.ToString("X"), 10);
                    }
                    catch (Exception)
					{
						_DBValue = null;
                        _Value = null;
					}
				}
			}
		}
		#endregion
		#region DBValue
#if !WindowsCE
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
#endif
		internal ulong? _DBValue;
		public ulong? DBValue
		{
			get { return _DBValue; }
			set
			{
				_DBValue = value;
				if (!_DBValue.HasValue)
				{
					_Value = _DBValue;
				}
				else
				{
					try
					{
						//_Value = ulong.Parse(_DBValue.Value.ToString(), System.Globalization.NumberStyles.HexNumber);
					    _Value = Convert.ToUInt64(_DBValue.Value.ToString(), 16);
					}
					catch (Exception)
					{
                        _DBValue = null;
						_Value = null;
					}
				}
			}
		}
		#endregion
		#region IsNull
		public static bool IsNull(SN value)
		{
			return value == null;
		}
		#endregion

		#region Any
		public static SN Any
		{
			get { return new SN(ulong.MaxValue); }
		}
		#endregion
		#region IsAny
		public bool IsAny
		{
			get { return (_Value.HasValue ? _Value.Value == ulong.MaxValue : false); }
		}
		#endregion

		#region Constructors
		public SN()
		{
			Value = 0;
		}
		public SN(ulong sn)
		{
			Value = sn;
		}
		public SN(ulong? sn)
		{
			Value = sn;
		}
		public SN(byte[] sn)
		{
            Value = Convert.ToUInt64(BitConverter.ToUInt32(sn, 0));
		}
		#endregion

        #region FromDBValue
        public static SN FromDBValue(ulong? DBValue)
        {
            SN result = new SN();
            result.DBValue = DBValue;
            return result;
        }
        #endregion

		#region Operators - wymagane
		public override bool Equals(object obj)
		{
			if (obj is SN)
			{
				return this._Value == ((SN)obj)._Value;
			}
            if (obj == null)
            {
                return this._Value == null;
            }
			return base.Equals(obj);
		}
		public static bool operator ==(SN left, SN right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(SN left, SN right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}
		public override int GetHashCode()
		{
			return _Value.GetHashCode();
		}
		#endregion
		#region Operators - na razie zablokowane aby dotychczasowe definicje ulong zamienic na SN
		/*
		#region SN = ulong
		public static implicit operator SN(ulong value)
		{
			return (new SN(value));
		}
		#endregion
		#region SN = ulong?
		public static implicit operator SN(ulong? value)
		{
			return (new SN(value));
		}
		#endregion
		#region ulong = SN
		public static implicit operator ulong(SN value)
		{
			if ((object)value == null)
				return ulong.MaxValue;
			return value.HasValue ? value.Value.Value : ulong.MaxValue;
		}
		#endregion
		#region ulong? = SN
		public static implicit operator ulong?(SN value)
		{
			if ((object)value == null)
				return null;
			return value.Value;
		}
		#endregion

		#region SN == ulong
		public static bool operator ==(SN sn1, ulong sn2)
		{
			if ((object)sn1 == null)
				return false;
			return sn1.HasValue ? sn1.Value == sn2 : false;
		}
		#endregion
		#region SN != ulong
		public static bool operator !=(SN sn1, ulong sn2)
		{
			if ((object)sn1 == null)
				return false;
			return sn1.HasValue ? sn1.Value != sn2 : false;
		}
		#endregion
		#region SN == ulong?
		public static bool operator ==(SN sn1, ulong? sn2)
		{
			if ((object)sn1 == null)
				return !sn2.HasValue;
			return sn1.Value == sn2;
		}
		#endregion
		#region SN != ulong?
		public static bool operator !=(SN sn1, ulong? sn2)
		{
			if ((object)sn1 == null)
				return sn2.HasValue;
			return sn1.Value != sn2;
		}
		#endregion
		*/

        public static explicit operator byte[](SN serialNumber)
        {
            return BitConverter.GetBytes(Convert.ToUInt32(serialNumber.Value.Value));
        }
		#endregion

		#region override ToString()
		public override string ToString()
		{
			if (!Value.HasValue)
				return "null";
			if (IsAny)
				return "Any";
			else
				return _Value.Value.ToString("X");
		}
		#endregion
	}
}
