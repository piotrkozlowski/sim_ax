using System;
//using System.Security.Principal;
using System.Collections.Generic;
#if !Android
using System.Drawing;
#endif

namespace IMR.Suite.Common 
{
    #region class Actor
    [Serializable]
	public class Actor
    {
        #region Members

        #region IdActor
        private int idActor = -1;

        public int IdActor
        {
            get
            {
                return idActor;
            }
        }
        #endregion

        #region Name
        private string name = "";

        public string Name
        {
            get
            {
                return name;
            }
        }
        #endregion
        #region Surname
        private string surname = "";

        public string Surname
        {
            get
            {
                return surname;
            }
        }
        #endregion

        #region City
        private string city = "";

        public string City
        {
            get
            {
                return city;
            }
        }
        #endregion
        #region Address
        private string address = "";

        public string Address
        {
            get
            {
                return address;
            }
        }
        #endregion
        #region PostCode
        private string postCode = "";

        public string PostCode
        {
            get
            {
                return postCode;
            }
        }
        #endregion
        #region Email
        private string email = "";

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        #endregion
        #region Mobile
        private string mobile = "";

        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                mobile = value;
            }
        }
        #endregion
        #region Phone
        private string phone = "";

        public string Phone
        {
            get
            {
                return phone;
            }
        }
        #endregion
        #region Avatar
#if !Android
        private Image avatar = null;
        public Image Avatar
        {
            get
            {
                return avatar;
            }
            set
            {
                avatar = value;
            }
        }
#endif
        #endregion
        #region ActorDescr
        private string actorDescr = "";

        public string ActorDescr
        {
            get
            {
                return actorDescr;
            }
        }
        #endregion

        #region Language
        protected Enums.Language language = Enums.Language.Unknown;

        public Enums.Language Language
        {
            get
            {
                return language;
            }
        }
        #endregion

        #endregion
        
        #region Constructor
        public Actor(int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, string ActorDescr, Enums.Language Language)
        {
            this.idActor = IdActor;
            this.name = Name;
            this.surname = Surname;
            this.city = City;
            this.address = Address;
            this.email = Email;
            this.mobile = Mobile;
            this.phone = Phone;
            this.actorDescr = ActorDescr;
            this.language = Language;
        }

        public Actor(int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, string ActorDescr, int LanguageId)
            : this(IdActor, Name, Surname, City, Address, Email, Mobile, Phone, ActorDescr, Enums.Language.Unknown)
        {
            if (Enum.IsDefined(typeof(Enums.Language), LanguageId))
            {
                this.language = (Enums.Language)LanguageId;
            }
        }
#if !Android
        public Actor(int IdActor, string Name, string Surname, string City, string Address, string PostCode, string Email, string Mobile, string Phone, string ActorDescr, int LanguageId, Image Avatar)
        {
            this.idActor = IdActor;
            this.name = Name;
            this.surname = Surname;
            this.city = City;
            this.address = Address;
            this.postCode = PostCode;
            this.email = Email;
            this.mobile = Mobile;
            this.phone = Phone;
            this.actorDescr = ActorDescr;
            this.language = (Enums.Language)LanguageId;
            this.avatar = Avatar;
        }
#endif
        #endregion
    }
    #endregion
    #region class Operator
    [Serializable]
    public class Operator: Actor
    {
        #region Members

        #region static Empty
        public static readonly Operator Empty = new Operator(-1,SN.Any,-1,"", "", true, -1, "", "", "", "", "", "", "", Enums.Language.Unknown, "");
        #endregion

        #region IdOperator
        private int idOperator = -1;

        public int IdOperator
        {
            get
            {
                return idOperator;
            }
        }
        #endregion

        #region SerialNbr
        private SN serialNbr = SN.Any;
        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }
        }
        #endregion 

        #region IdDistributor
        private int idDistributor = -1;

        public int IdDistributor
        {
            get
            {
                return idDistributor;
            }
        }
        #endregion
        #region Login
        private string login = "";

        public string Login
        {
            get
            {
                return login;
            }
        }
        #endregion
        #region Password
        private string passwd = string.Empty;

        public string Password
        {
            get
            {
                return passwd;
            }
        }
        #endregion

        #region OperatorDescr
        private string operatorDescr = "";

        public string OperatorDescr
        {
            get
            {
                return operatorDescr;
            }
        }
        #endregion

        #region IsBlocked
        private bool isBlocked = true;

        public bool IsBlocked
        {
            get
            {
                return isBlocked;
            }
        }
        #endregion

        #endregion

        #region Constructor
        public Operator(int IdOperator, SN SerialNbr, int IdDistributor, string Login, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, Enums.Language Language, string ActorDescr) 
            :base (IdActor, Name, Surname, City, Address, Email, Mobile, Phone, ActorDescr, Language)
        {
            this.idOperator = IdOperator;
            this.serialNbr = SerialNbr;
            this.idDistributor = IdDistributor;
            this.login = Login;
            this.operatorDescr = OperatorDescr;
            this.isBlocked = IsBlocked;
        }

        public Operator(int IdOperator, SN SerialNbr, int IdDistributor, string Login, string Passwd, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, Enums.Language Language, string ActorDescr)
            : base(IdActor, Name, Surname, City, Address, Email, Mobile, Phone, ActorDescr, Language)
        {
            this.idOperator = IdOperator;
            this.serialNbr = SerialNbr;
            this.idDistributor = IdDistributor;
            this.login = Login;
            this.passwd = Passwd;
            this.operatorDescr = OperatorDescr;
            this.isBlocked = IsBlocked;
        }

        public Operator(int IdOperator, int IdDistributor, string Login, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, Enums.Language Language, string ActorDescr)
            :this(IdOperator, SN.Any, IdDistributor,Login, OperatorDescr, IsBlocked, IdActor, Name, Surname, City, Address, Email, Mobile, Phone, Language, ActorDescr)
        {

        }

        public Operator(int IdOperator, SN SerialNbr, int IdDistributor, string Login, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, int LanguageId, string ActorDescr)
            : this(IdOperator, SerialNbr, IdDistributor, Login, OperatorDescr, IsBlocked, IdActor, Name, Surname, City, Address, Email, Mobile, Phone, Enums.Language.Unknown, ActorDescr)
        {
            if (Enum.IsDefined(typeof(Enums.Language), LanguageId))
            {
                base.language = (Enums.Language)LanguageId;
            }
        }

        public Operator(int IdOperator, int IdDistributor, string Login, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, int LanguageId, string ActorDescr)
			: this(IdOperator, SN.Any, IdDistributor, Login, OperatorDescr, IsBlocked, IdActor, Name, Surname, City, Address, Email, Mobile, Phone, LanguageId, ActorDescr)
        {


        }
#if !Android
        public Operator(int IdOperator, int IdDistributor, string Login, string OperatorDescr, bool IsBlocked, int IdActor, string Name, string Surname, string City, string Address, string Email, string Mobile, string Phone, int LanguageId, string ActorDescr, Image Avatar)
            : this(IdOperator, SN.Any, IdDistributor, Login, OperatorDescr, IsBlocked, IdActor, Name, Surname, City, Address, Email, Mobile, Phone, LanguageId, ActorDescr)
        {
            base.Avatar = Avatar;
        }
#endif
        #endregion

		#region ToString()
		public override string ToString()
		{
			return String.Format("{0} {1}", this.Name, this.Surname);
		}
		#endregion
	}
    #endregion

    #region class User
    public class User // : IPrincipal, IIdentity
    {
        public int LoginResult;
        public int? IdOperator { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string PhoneMobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public int IdDistributor { get; set; }
        public int? IdActor;
        public bool IsBlocked { get; set; }
        public int IdLanguage { get; set; }
       
        //public Language UserDefaultLanguage { get; set; }

        public List<Role> Roles { get; set; }
        public List<Activity> Activities { get; set; }

        public User()
        {
        }

        public User(int LoginResult)
        {
            this.LoginResult = LoginResult;
            this.IdOperator = 0;
            this.UserName = null;
            this.FirstName = null;
            this.Surname = null;
            this.City = null;
            this.PostalCode = null;
            this.Address = null;
            this.Phone = null;
            this.Email = null;
            //this.UserDefaultLanguage = new Language();
        }

        public User(int LoginResult, int IdOperator, string UserName, string FirstName, string Surname,
            string City, string PostalCode, string Address, string Phone, string Email, int IdDistributor)
        {
            this.LoginResult = LoginResult;
            this.IdOperator = IdOperator;
            this.UserName = UserName;
            this.FirstName = FirstName;
            this.Surname = Surname;
            this.City = City;
            this.PostalCode = PostalCode;
            this.Address = Address;
            this.Phone = Phone;
            this.Email = Email;
            this.IdDistributor = IdDistributor;
            //this.UserDefaultLanguage = UserDefaultLanguage;

            this.Roles = new List<Role>();
            this.Activities = new List<Activity>();
        }

        public User(User clone)
        {
            this.LoginResult = clone.LoginResult;
            this.IdOperator = clone.IdOperator;
            this.UserName = clone.UserName;
            this.FirstName = clone.FirstName;
            this.Surname = clone.Surname;
            this.City = clone.City;
            this.PostalCode = clone.PostalCode;
            this.Address = clone.Address;
            this.Phone = clone.Phone;
            this.PhoneMobile = clone.PhoneMobile;
            this.Email = clone.Email;
            this.Password = clone.Password;
            this.Description = clone.Description;
            this.IdDistributor = clone.IdDistributor;
            this.IdActor = clone.IdActor;
            this.IsBlocked = clone.IsBlocked;
            this.IdLanguage = clone.IdLanguage;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", this.FirstName, this.Surname);
        }

        public bool CanDo(int ActivityId)
        {
            foreach (Activity a in Activities)
            {
                if (a.ActivityID == ActivityId && a.Deny == false)
                    return true;
            }
            return false;
        }

        /*
        string IIdentity.AuthenticationType
        {
            get { return "SIMA"; }
        }

        bool IIdentity.IsAuthenticated
        {
            get { return true; }
        }

        string IIdentity.Name
        {
            get { return this.UserName; }
        }

        IIdentity IPrincipal.Identity
        {
            get { return this; }
        }

        bool IPrincipal.IsInRole(string role)
        {
            return true;
            //??
            //return role == this.UserRole.Name;
        }
        

        public static User Current
        {
            get { return System.Threading.Thread.CurrentPrincipal as User; }
            set { System.Threading.Thread.CurrentPrincipal = value; }
        }
         * */
    }
    #endregion
}
