﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Runtime.Serialization;

namespace IMR.Suite.Common
{
    #region DiagnosticCode
    [Serializable]
    public class DiagnosticCode
    {
        #region Members
        private static readonly string UNKNOWN_DIAGNOSTIC_CODE = "Unknown DiagnosticCode";
        #endregion

        #region GetName
        internal static System.Reflection.FieldInfo[] dataTypeFields = null;
        public static string GetName(long idCode)
        {
            try
            {
                if (dataTypeFields == null)
                {
                    Type t = Type.GetType("IMR.Suite.Common.DiagnosticCode");
                    if (t == null)
                        return UNKNOWN_DIAGNOSTIC_CODE;

                    dataTypeFields = t.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
                    if (dataTypeFields == null)
                        return UNKNOWN_DIAGNOSTIC_CODE;
                }

                FieldInfo field = dataTypeFields.SingleOrDefault(f => (long)f.GetValue(null) == idCode);
                if (field != null)
                    return field.Name;

                return UNKNOWN_DIAGNOSTIC_CODE;
            }
            catch
            {
                return UNKNOWN_DIAGNOSTIC_CODE;
            }
        }
        #endregion

        #region DiagnosticCodes
        public const long UNKNOWN_CODE = 1;//"Unknown code"
        public const long TRANSMISSION_DRIVER_MODEM_DISCONNECTED = 2;//"Transmission driver modem disconnected"
        public const long TRANSMISSION_DRIVER_MODEM_CONNECTED = 3;//"Transmission driver modem connected"
        public const long TRANSMISSION_DRIVER_MODEM_REMOVED = 4;//"Transmission driver modem removed"
        public const long TRANSMISSION_DRIVER_SIM_CARD_REMOVED = 5;//"Transmission driver sim card removed"
        public const long TRANSMISSION_DRIVER_SIM_CARD_INSERTED = 6;//"Transmission driver sim card inserted"
        public const long TRANSMISSION_DRIVER_NETWORK_DISCONNECTED = 7;//"Transmission driver network disconnected"
        public const long TRANSMISSION_DRIVER_NETWORK_CONNECTED = 8;//"Transmission driver network connected"
        public const long TRANSMISSION_DRIVER_MODEM_INITIALIZATION_ERROR = 9;//"Transmission driver modem initialization error"
        public const long TRANSMISSION_DRIVER_INITIALIZED_OK = 10;//"Transmission driver initialized ok"
        public const long TRANSMISSION_DRIVER_INITIALIZATION_ERROR = 11;//"Transmission driver initialization error"
        public const long TRANSMISSION_DRIVER_MODEM_CONNECTION_ERROR = 12;//"Transmission driver modem connection error"
        public const long TRANSMISSION_DRIVER_UDP_SERVER_INITIALIZED_OK = 13;//"Transmission driver UDP server initialized ok"
        public const long TRANSMISSION_DRIVER_UDP_SERVER_INITIALIZATION_ERROR = 14;//"Transmission driver UDP server initialization error"
        public const long TRANSMISSION_DRIVER_UDP_SERVER_STOPPED = 15;//"Transmission driver UDP server stopped"
        public const long TRANSMISSION_DRIVER_TCP_SERVER_INITIALIZED_OK = 16;//"Transmission driver TCP server initialized ok"
        public const long TRANSMISSION_DRIVER_TCP_SERVER_INITIALIZATION_ERROR = 17;//"Transmission driver TCP server initialization error"
        public const long TRANSMISSION_DRIVER_TCP_SERVER_STOPPED = 18;//"Transmission driver TCP server stopped"
        public const long TRANSMISSION_DRIVER_PACKET_SNIFFER_INITIALIZED_OK = 19;//"Transmission driver packet sniffer initialized ok"
        public const long TRANSMISSION_DRIVER_PACKET_SNIFFER_INITIALIZATION_ERROR = 20;//"Transmission driver packet sniffer initialization error"
        public const long TRANSMISSION_DRIVER_PACKET_SNIFFER_STOPPED = 21;//"Transmission driver packet sniffer stopped "
        public const long TRANSMISSION_DRIVER_FTP_SERVER_INITIALIZED_OK = 22;//"Transmission driver FTP server initialized ok"
        public const long TRANSMISSION_DRIVER_FTP_SERVER_INITIALIZATION_ERROR = 23;//"Transmission driver FTP server initialization error"
        public const long TRANSMISSION_DRIVER_FTP_SERVER_STOPPED = 24;//"Transmission driver FTP server stopped"
        public const long TRANSMISSION_DRIVER_STARTED = 25;//"Transmission driver started"
        public const long TRANSMISSION_DRIVER_STOPPED = 26;//"Transmission driver stopped"
        public const long TRANSMISSION_DRIVER_UCP_CLIENT_STOPPED = 27;//"Transmission driver UCP client stopped"
        public const long TRANSMISSION_DRIVER_UCP_CLIENT_TCP_SOCKET_ERROR = 28;//"Transmission driver UCP client TCP socket error"
        public const long TRANSMISSION_DRIVER_UCP_CLIENT_TCP_CLOSED_BY_REMOTE_SIDE = 29;//"Transmission driver UCP client TCP closed by remote side"
        public const long TRANSMISSION_DRIVER_UCP_CLIENT_CONNECTED = 30;//"Transmission driver UCP client connected"
        public const long TRANSMISSION_DRIVER_UCP_CLIENT_UNABLE_TO_AUTHENTICATE = 31;//"Transmission driver UCP client unable to authenticate"

        public const long CONTROLLER_NEW_VERSION_DETECTED = 200; //"Detected NEW version of IMR Modules"
        public const long CONTROLLER_OLD_VERSION_DETECTED = 201; //"Detected OLD version of IMR Modules"
        #endregion
    }
    #endregion


    #region class ModuleEventData
    [Serializable]
    public class ModuleEventData : ISerializable
    {
        #region Members
        #region Module
        private Enums.Module module;
        public Enums.Module Module
        {
            get { return module; }
        }
        #endregion
        #region Healthy
        private bool healthy;
        public bool Healthy
        {
            get { return healthy; }
        }
        #endregion
        #region Timestamp
        private DateTime timestamp;
        public DateTime Timestamp
        {
            get { return timestamp; }
        }
        #endregion
        #region Assembly
        private string assembly;
        public string Assembly
        {
            get { return assembly; }
        }
        #endregion
        #region Events
        private DataValue[] events = new DataValue[] { };
        public DataValue[] Events
        {
            get { return events; }
        }
        #endregion
        #endregion

        #region Constructor
        public ModuleEventData(DataValue singleEvent)
            : this(false, singleEvent) { }
        public ModuleEventData(bool healthy, DataValue singleEvent)
            : this(healthy, new DataValue[] { singleEvent }) { }
        public ModuleEventData(DataValue[] events)
            : this(false, events) { }
        public ModuleEventData(bool healthy, DataValue[] events)
            : this(healthy, null, DateTime.UtcNow, null, events) { }
        public ModuleEventData(bool healthy, Enums.Module module, DataValue[] events)
            : this(healthy, module, DateTime.UtcNow, null, events) { }
        public ModuleEventData(bool healthy, Enums.Module module, string assembly, DataValue[] events)
            : this(healthy, module, DateTime.UtcNow, assembly, events) { }
        private ModuleEventData(bool healthy, Enums.Module? module, DateTime timeStamp, string assembly, DataValue[] events)
        {
            this.healthy = healthy;
            this.assembly = assembly;
            this.events = events;
            this.timestamp = timeStamp;
            if (module != null)
                this.module = module.Value;
        }

        // The special constructor is used to deserialize values. 
        protected ModuleEventData(SerializationInfo info, StreamingContext context)
        {
            module = (Enums.Module)info.GetValue("module", typeof(int));
            healthy = (bool)info.GetValue("healthy", typeof(bool));
            timestamp = (DateTime)info.GetValue("timestamp", typeof(DateTime));
            assembly = (string)info.GetValue("assembly", typeof(string));
            events = (DataValue[])info.GetValue("events", typeof(DataValue[]));
        }
        #endregion

        #region GetObjectData
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("module", (int)module, typeof(int));
            info.AddValue("healthy", healthy, typeof(bool));
            info.AddValue("timestamp", timestamp, typeof(DateTime));
            info.AddValue("assembly", assembly, typeof(string));
            info.AddValue("events", events, typeof(DataValue[]));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }
        #endregion
        #region UpdateAssembly
        public void UpdateAssembly(string assembly)
        {
            this.assembly = assembly;
        }
        #endregion
        #region UpdateHealthy
        public void UpdateHealthy(bool healthy)
        {
            this.healthy = healthy;
        }
        #endregion
        #region UpdateModule
        public void UpdateModule(Enums.Module module)
        {
            this.module = module;
        }
        #endregion
        #region AddEventData
        public void AddEventData(DataValue dataValue)
        {
            AddRangeEventData(new DataValue[] { dataValue });
        }
        #endregion
        #region AddRangeEventData
        public void AddRangeEventData(DataValue[] dataValues)
        {
            events = events.Concat(dataValues).ToArray();
        }
        #endregion
    }
    #endregion
}