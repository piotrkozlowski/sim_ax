using System;
using System.Collections.Generic;
using System.Text;
#if !Android
using System.Data;
#endif
using System.Collections;

namespace IMR.Suite.Common
{
	#region AlarmEvent
    [Serializable]
    public class AlarmEvent
    {
        #region Members
       
        #region IdAlarmEvent
        private long? idAlarmEvent = null;

        public long? IdAlarmEvent
        {
            get
            {
                return idAlarmEvent;
            }
            set
            {
                idAlarmEvent = value;
            }
        }
        #endregion
        #region IdAlarmDef
        private long idAlarmDef = -1;

        public long IdAlarmDef
        {
            get
            {
                return idAlarmDef;
            }
        }
        #endregion
         
        #region Type
        private Enums.AlarmType type = Enums.AlarmType.Unknown;

        public Enums.AlarmType Type
        {
            get
            {
                return type;
            }
        }
        #endregion

        #region SerialNbr
        private SN serialNbr = null;

        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }
            set
            {
                serialNbr = value;
            }
        }
        #endregion
        #region IdMeter
        private long? idMeter = null;

        public long? IdMeter
        {
            get
            {
                return idMeter;
            }
            set
            {
                idMeter = value;
            }
        }
        #endregion
        #region IdLocation
        private long? idLocation = null;

        public long? IdLocation
        {
            get
            {
                return idLocation;
            }
            set
            {
                idLocation = value;
            }
        }
        #endregion 

        #region IdDataTypeAlarm
        private long idDataTypeAlarm = -1;
        public long IdDataTypeAlarm
        {
            get
            {
                return idDataTypeAlarm;
            }
        }
        #endregion
        #region IdDataTypeConfig
        private long? idDataTypeConfig = null;
        public long? IdDataTypeConfig
        {
            get
            {
                return idDataTypeConfig;
            }
        }
        #endregion

        #region IndexNbr
        private int indexNbr = 0;
        public int IndexNbr
        {
            get
            {
                return indexNbr;
            }
        }
        #endregion

        #region AlarmValue
        private object alarmValue = null;
        public object AlarmValue
        {
            get
            {
                return alarmValue;
            }
            set
            {
                alarmValue = value;
            }
        }
        #endregion
        #region SystemValue
        private object systemValue = null;
        public object SystemValue
        {
            get
            {
                return systemValue;
            }
            set
            {
                systemValue = value;
            }
        }
        #endregion        

        #region IdAlarmText
        private long? idAlarmText = null;
        public long? IdAlarmText
        {
            get
            {
                return idAlarmText;
            }
        }
        #endregion
        #region IdAlarmData
        private long? idAlarmData = null;
        public long? IdAlarmData
        {
            get
            {
                return idAlarmData;
            }
        }
        #endregion

        #region CheckLastAlarm
        private bool checkLastAlarm = true;
        public bool CheckLastAlarm
        {
            get
            {
                return checkLastAlarm;
            }
        }
        #endregion
        #region SuspensionTime
        private int? suspensionTime = null;
        public int? SuspensionTime
        {
            get
            {
                return suspensionTime;
            }
        }
        #endregion

        #region IsEnabled
        private bool isEnabled = true;
        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
        }
        #endregion

        #region IsConfirmable
        private bool isConfirmable = true;
        public bool IsConfirmable
        {
            get
            {
                return isConfirmable;
            }
        }
        #endregion
        #region ConfirmLevel
        private int confirmLevel = 1;
        public int ConfirmLevel
        {
            get
            {
                return confirmLevel;
            }
            set
            {
                confirmLevel = value;
            }
        }
        #endregion
        #region MaxConfirmLevel
        private int maxConfirmLevel = 1;
        public int MaxConfirmLevel
        {
            get
            {
                return maxConfirmLevel;
            }
            set
            {
                maxConfirmLevel = value;
            }
        }
        #endregion

        #region IdDataArch
        private long? idDataArch = null;
        public long? IdDataArch
        {
            get
            {
                return idDataArch;
            }
            set
            {
                idDataArch = value;
            }
        }
        #endregion
        #region RadioSerialNbr
        public SN RadioSerialNbr { get; set; }
        #endregion

        #region Alarms
        private List<Alarm> alarms = new List<Alarm>();
        public List<Alarm> Alarms
        {
            get
            {
                return alarms;
            }
            set
            {
                alarms = value;
            }
        }
        #endregion
        #region AlarmGroups
        private List<AlarmDefGroup> alarmGroups = new List<AlarmDefGroup>();
        public List<AlarmDefGroup> AlarmGroups
        {
            get
            {
                return alarmGroups;
            }
            set
            {
                alarmGroups = value;
            }
        }
        #endregion
        #region AlarmGroupsWithOperators
        private Dictionary<int, AlarmDefGroup> alarmGroupsWithOperators = new Dictionary<int, AlarmDefGroup>();
        public Dictionary<int, AlarmDefGroup> AlarmGroupsWithOperators
        {
            get
            {
                return alarmGroupsWithOperators;
            }
            set
            {
                alarmGroupsWithOperators = value;
            }
        }
        #endregion
        #region AlarmMessage
        private Dictionary<int, string> alarmMessage = new Dictionary<int, string>();
        public Dictionary<int, string> AlarmMessage
        {
            get
            {
                return alarmMessage;
            }
            set
            {
                alarmMessage = value;
            }
        }
        #endregion

        #region AlarmData
        private DataValue[] alarmData = null;
        public DataValue[] AlarmData
        {
            get { return alarmData; }
            set { alarmData = value; }
        }
        #endregion

        #region ActionToExecute
        private Common.Action action = null;
        public Common.Action Action
        {
            get { return action; }
            set { action = value; }
        }
        #endregion
        #endregion

        #region Constructor
        public AlarmEvent()
        { 
        }

        public AlarmEvent(Enums.AlarmType Type, SN SerialNbr, long? IdMeter, long? IdLocation, long IdAlarmDef, long IdDataTypeAlarm, long? IdDataTypeConfig, object SystemValue, bool IsEnabled, long? IdAlarmText, long? IdAlarmData, bool CheckLastAlarm, int? SuspensionTime, bool IsConfirmable)         
        {
            this.type = Type;
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
            this.idDataTypeAlarm = IdDataTypeAlarm;
            this.idDataTypeConfig = IdDataTypeConfig;
            this.systemValue = SystemValue;
            this.isEnabled = IsEnabled;
            this.idAlarmText = IdAlarmText;
            this.idAlarmData = IdAlarmData;
            this.idAlarmDef = IdAlarmDef;
            this.checkLastAlarm = CheckLastAlarm;
            this.suspensionTime = SuspensionTime;
            this.isConfirmable = IsConfirmable;
            this.indexNbr = 0;
        }

        public AlarmEvent(Enums.AlarmType Type, SN SerialNbr, long? IdMeter, long? IdLocation, long IdAlarmDef, long IdDataTypeAlarm, long? IdDataTypeConfig, object SystemValue, bool IsEnabled, long? IdAlarmText, long? IdAlarmData, bool CheckLastAlarm, int? SuspensionTime, bool IsConfirmable, int IndexNbr)
        {
            this.type = Type;
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
            this.idDataTypeAlarm = IdDataTypeAlarm;
            this.idDataTypeConfig = IdDataTypeConfig;
            this.systemValue = SystemValue;
            this.isEnabled = IsEnabled;
            this.idAlarmText = IdAlarmText;
            this.idAlarmData = IdAlarmData;
            this.idAlarmDef = IdAlarmDef;
            this.checkLastAlarm = CheckLastAlarm;
            this.suspensionTime = SuspensionTime;
            this.isConfirmable = IsConfirmable;
            this.indexNbr = IndexNbr;
        }

        public AlarmEvent(int TypeId, SN SerialNbr, long? IdMeter, long? IdLocation, long IdAlarmDef, long IdDataTypeAlarm, long? IdDataTypeConfig, object SystemValue, bool IsEnabled, long? IdAlarmText, long? IdAlarmData, bool CheckLastAlarm, int? SuspensionTime, bool IsConfirmable)
            : this(Enums.AlarmType.Unknown, SerialNbr, IdMeter, IdLocation, IdAlarmDef, IdDataTypeAlarm, IdDataTypeConfig, SystemValue, IsEnabled, IdAlarmText, IdAlarmData, CheckLastAlarm, SuspensionTime, IsConfirmable)
        {
            if (Enum.IsDefined(typeof(Enums.AlarmType), TypeId))
            {
                this.type = (Enums.AlarmType)TypeId;
            }
        }

        public AlarmEvent(int TypeId, SN SerialNbr, long? IdMeter, long? IdLocation, long IdAlarmDef, long IdDataTypeAlarm, long? IdDataTypeConfig, object SystemValue, bool IsEnabled, long? IdAlarmText, long? IdAlarmData, bool CheckLastAlarm, int? SuspensionTime, bool IsConfirmable, int IndexNbr)
            : this(Enums.AlarmType.Unknown, SerialNbr, IdMeter, IdLocation, IdAlarmDef, IdDataTypeAlarm, IdDataTypeConfig, SystemValue, IsEnabled, IdAlarmText, IdAlarmData, CheckLastAlarm, SuspensionTime, IsConfirmable, IndexNbr)
        {
            if (Enum.IsDefined(typeof(Enums.AlarmType), TypeId))
            {
                this.type = (Enums.AlarmType)TypeId;
            }
        }
        #endregion        
        
        #region AddAlarmToAlarmList
        public void AddAlarmToAlarmList(Alarm alarm)
        {
            this.alarms.Add(alarm);
        }
        #endregion
        #region RemoveAlarmFromAlarmList
        public void RemoveAlarmFromAlarmList(Alarm alarm)
        {
            this.alarms.Remove(alarm);
        }
        #endregion
        #region ClearAlarmList
        public void ClearAlarmList()
        {
            this.alarms.Clear();
        }
        #endregion
        #region GetMessageToSend
        public string GetMessageToSend()
        {
            string message = "";

            foreach (DataValue dataValue in alarmData)
            {
                if (dataValue.Type.IdDataType == DataType.ALARM_TEXT_MESSAGE_TO_SEND)
                {
                    message = dataValue.Value.ToString();
                    break;
                }
            }

            return message;
        }
        #endregion
        #region GetActionsToSend
        public List<long> GetActionsToSend()
        {
            long idActionDef;
            List<long> actionDefs = new List<long>();

            foreach (DataValue dataValue in alarmData)
            {                
                if (dataValue.Type.IdDataType == DataType.ALARM_ACTION_DEF_TO_SEND)
                {
#if !WindowsCE
                    if (long.TryParse(dataValue.Value.ToString(), out idActionDef))
                    {
                        actionDefs.Add(idActionDef);
                    }
#endif
                }
            }

            return actionDefs;
        }
        #endregion
    }
    #endregion AlarmEvent

    #region Alarm
    [Serializable]
    public class Alarm
    {
        #region Members
        
        #region IdAlarmEvent
        private long idAlarmEvent;

        public long IdAlarmEvent
        {
            get
            {
                return idAlarmEvent;
            }
        }
        #endregion
        #region IdAlarm
        private long idAlarm;

        public long IdAlarm
        {
            get
            {
                return idAlarm;
            }
        }
        #endregion

        #region Status
        private Enums.TransmissionStatus status = Enums.TransmissionStatus.Unknown;

        public Enums.TransmissionStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        #endregion 

        #region TransmissionType
        private Enums.TransmissionType transmissionType;

        public Enums.TransmissionType TransmissionType
        {
            get
            {
                return transmissionType;
            }
        }
        #endregion 

        #region IdAlarmGroup
        private int idAlarmGroup;

        public int IdAlarmGroup
        {
            get
            {
                return idAlarmGroup;
            }
        }
        #endregion
        #region IdOperator
        private long idOperator;

        public long IdOperator
        {
            get
            {
                return idOperator;
            }
        }
        #endregion
        #region OperatorSerialNbr
        private SN operatorSerialNbr = SN.Any;

        public SN OperatorSerialNbr
        {
            get
            {
                return operatorSerialNbr;
            }
        }
        #endregion

        #region ConfirmLevel
        private int confirmLevel;
        public int ConfirmLevel
        {
            get
            {
                return confirmLevel;
            }
            set
            {
                confirmLevel = value;
            }
        }
        #endregion

        #endregion Members

        #region Constructor
        public Alarm()
        {
        }
        public Alarm(long IdAlarmEvent, long IdAlarm, int IdAlarmGroup, int TransmissionType, Enums.TransmissionStatus Status, long IdOperator, SN OperatorSerialNbr, int ConfirmLevel)         
        {
            this.idAlarmEvent = IdAlarmEvent;
            this.idAlarm = IdAlarm;            
            this.status = Status;
            this.transmissionType = (Enums.TransmissionType)TransmissionType;
            this.idAlarmGroup = IdAlarmGroup;
            this.idOperator = IdOperator;
            this.operatorSerialNbr = OperatorSerialNbr;
            this.confirmLevel = ConfirmLevel;
        }

        public Alarm(long IdAlarmEvent, long IdAlarm, int IdAlarmGroup, int TransmissionType, long IdOperator, SN OperatorSerialNbr, int ConfirmLevel)
        {
            this.idAlarmEvent = IdAlarmEvent;
            this.idAlarm = IdAlarm;
            this.status = Enums.TransmissionStatus.New;
            this.transmissionType = (Enums.TransmissionType)TransmissionType;
            this.idAlarmGroup = IdAlarmGroup;
            this.idOperator = IdOperator;
            this.operatorSerialNbr = OperatorSerialNbr;
            this.confirmLevel = ConfirmLevel;
        }

        public Alarm(long IdAlarmEvent, long IdAlarm, int IdAlarmGroup, int TransmissionType, int Status, long IdOperator, SN OperatorSerialNbr, int ConfirmLevel)
            : this(IdAlarmEvent, IdAlarm, IdAlarmGroup, TransmissionType, Enums.TransmissionStatus.Unknown, IdOperator, OperatorSerialNbr, ConfirmLevel)
        {
            if (Enum.IsDefined(typeof(Enums.TransmissionStatus), Status))
            {
                this.status = (Enums.TransmissionStatus)Status;
            }
        }
        #endregion Constructor
    }
    #endregion Alarm

    #region AlarmGroup
    [Serializable]
    public class AlarmGroup
    {
        public int IdAlarmGroup;
        public string Name;
        public string Description;
        public int IdDistributor;

        public AlarmGroup(int idAlarmGroup, string name, string description, int idDistributor)
        {
            IdAlarmGroup = idAlarmGroup;
            Name = name;
            Description = description;
            IdDistributor = idDistributor;
        }
    }
    #endregion

    #region AlarmDefGroup
    [Serializable]
    public class AlarmDefGroup
    {
        #region Members
        #region IdAlarmGroup
        private int idAlarmGroup;

        public int IdAlarmGroup
        {
            get
            {
                return idAlarmGroup;
            }
        }
        #endregion

        #region ConfirmLevel
        private int confirmLevel;

        public int ConfirmLevel
        {
            get
            {
                return confirmLevel;
            }
        }
        #endregion
        #region ConfirmTimeout
        private int confirmTimeout;

        public int ConfirmTimeout
        {
            get
            {
                return confirmTimeout;
            }
        }
        #endregion

        #region TransmissionType
        private Enums.TransmissionType transmissionType;

        public Enums.TransmissionType TransmissionType
        {
            get
            {
                return transmissionType;
            }
        }
        #endregion

        #region IdDistributor
        private int? idDistributor = null;

        public int? IdDistributor
        {
            get
            {
                return idDistributor;
            }
            set
            {
                idDistributor = value;
            }
        }
        #endregion

        #region Operators
        private List<Operator> operators = new List<Operator>();
        public List<Operator> Operators
        {
            get
            {
                return operators;
            }
            set
            {
                operators = value;
            }
        }
        #endregion
        #endregion

        #region AlarmDefGroup
        public AlarmDefGroup(int IdAlarmGroup, int ConfirmLevel, int ConfirmTimeout, Enums.TransmissionType TransmissionType, int? IdDistributor)         
        {
            this.idAlarmGroup = IdAlarmGroup;
            this.confirmLevel = ConfirmLevel;
            this.confirmTimeout = ConfirmTimeout;
            this.transmissionType = TransmissionType;
            this.idDistributor = IdDistributor;
        }

        public AlarmDefGroup(int IdAlarmGroup, int ConfirmLevel, int ConfirmTimeout, int TransmissionType, int? IdDistributor)
        {
            this.idAlarmGroup = IdAlarmGroup;
            this.confirmLevel = ConfirmLevel;
            this.confirmTimeout = ConfirmTimeout;
            this.idDistributor = IdDistributor;

            if (Enum.IsDefined(typeof(Enums.TransmissionType), TransmissionType))
            {
                this.transmissionType = (Enums.TransmissionType)TransmissionType;
            }
        }
        #endregion Constructor
    }
    #endregion

    #region AlarmGroupOperator
    [Serializable]
    public class AlarmGroupOperator
    {
        public int IdAlarmGroup;
        public int? IdOperator;
        public long? IdDataType;
    }
    #endregion

    #region AlarmType
    [Serializable]
    public class AlarmType
    {
        public int IdAlarmType;
        public string Symbol;
        public string Descr;

        public AlarmType()
        {
        }

        public AlarmType(int id)
        {
            IdAlarmType = id;
        }

        public override string ToString()
        {
            return Symbol;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
                return false;
            return (obj as AlarmType).IdAlarmType == IdAlarmType;
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
    #endregion
    
    #region AlarmTextDataType
    public class AlarmTextDataType
    {
        public long IdAlarmText;
        public int ArgNbr;
        public long IdDataType;
        public string Name;
        public int IdLanguage;
        public int IdUnit;
    }
    #endregion

    #region AlarmText
    [Serializable]
    public class AlarmText
    {
        #region Members
        #region IdAlarmText
        private long idAlarmText;

        public long IdAlarmText
        {
            get
            {
                return idAlarmText;
            }
        }
        #endregion
        #region IdLanguage
        private int idLanguage;

        public int IdLanguage
        {
            get
            {
                return idLanguage;
            }
        }
        #endregion
        #region Name
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
        }
        #endregion
        #region AlarmDesc
        private string alarmDesc;

        public string AlarmDesc
        {
            get
            {
                return alarmDesc;
            }
        }
        #endregion
        #region IdDistributor
        private int? idDistributor;

        public int? IdDistributor
        {
            get
            {
                return idDistributor;
            }
        }
        #endregion
        #endregion
        #region Constructors
        public AlarmText()
        {
        }

        public AlarmText(long IdAlarmText, int IdLanguage, string Name, string AlarmDesc, int? IdDistributor)
        {
            this.idAlarmText = IdAlarmText;
            this.idLanguage = IdLanguage;
            this.name = Name;
            this.alarmDesc = AlarmDesc;
            this.idDistributor = IdDistributor;
        }
        #endregion
    }
    #endregion
}
