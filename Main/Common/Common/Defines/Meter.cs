using System;
using System.Reflection;

namespace IMR.Suite.Common
{
    [Serializable]
	public class Meter
    {

        #region IdMeter
        private long idMeter = -1;
        public long IdMeter
        {
            get
            {
                return idMeter;
            }
            set
            {
            	idMeter = value;
            }
        }
        #endregion
        #region Type
        private Enums.MeterType type = Enums.MeterType.Unknown;
        public Enums.MeterType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }

        }
        #endregion

        public string TypeName{get; set;}

        public string CurrentDevice { get; set; }

        public long? IdLocation { get; set; }

        #region PatternIdMeter
        private long? patternIdMeter = null;
        public long? PatternIdMeter
        {
            get
            {
                return patternIdMeter;
            }
            set
            {
                patternIdMeter = value;
            }
        }
        #endregion
        #region PatternName
        private string patternName = "";
        public string PatternName
        {
            get
            {
                return patternName;
            }
            set
            {
                patternName = value;
            }
        }
        #endregion

        public int TankNo {get; set;}

        public string TankName { get; set; }

        public string SerialNbr { get; set; }

        #region IdDistributor
        private int idDistributor = -1;
        public int IdDistributor
        {
            get
            {
                return idDistributor;
            }
            set
            {
                idDistributor = value;
            }
        }
        #endregion

        public Meter(long IdMeter, Enums.MeterType Type, long? PatternIdMeter, string PatternName, int IdDistributor)
        {
            this.idMeter = IdMeter;
            this.type = Type;
            this.patternIdMeter = PatternIdMeter;
            this.patternName = PatternName;
            this.idDistributor = IdDistributor;
        }

        public Meter(long IdMeter, int TypeId, long? PatternIdMeter, string PatternName, int IdDistributor)
            : this(IdMeter, Enums.MeterType.Unknown, PatternIdMeter, PatternName, IdDistributor)
        {
            if (Enum.IsDefined(typeof(Enums.MeterType), TypeId))
            {
                this.type = (Enums.MeterType)TypeId;
            }
        }

        public Meter()
        {
        }

        public Meter(Meter clone)
            : this()
        {
            FieldInfo[] myObjectFields = clone.GetType().GetFields(
            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo fi in myObjectFields)
            {
                fi.SetValue(this, fi.GetValue(clone));
            }
        }

        public Meter Clone()
        {
            return (Meter)this.MemberwiseClone();
        }
    }

    public class MeterType
    {
        public int Id;
        public string Name;
        public string Descr;

        public MeterType()
        {
        }

        public MeterType(int Id, string Name, string Descr)
        {
            this.Id = Id;
            this.Name = Name;
            this.Descr = Descr;
        }

        public override string ToString()
        {
            if (Descr == "")
                return Name;
            return Descr;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(MeterType))
                return false;
            return ((obj as MeterType).Id == Id);
        }
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
