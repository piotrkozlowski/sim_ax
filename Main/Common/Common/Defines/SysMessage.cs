﻿//!!!!!!!!!!!
//DAQ.Controller - without prolongate and  init on start, without conn to DB and new sysMsg_DevHier
//(CORE.Actions & DAQ.Controller && IMR.Suite.Common)
//#define NEW_DEVICE_CACHE
//!!!!!!!!!!!!!!
using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IMR.Suite.Common
{
	#region SysMessage
	[Serializable]
#if !WindowsCE && !Android
	[DataContract]
	[KnownType(typeof(SysMsg_UpdateData))]
	[KnownType(typeof(SysMsg_DeviceHierarchyChange))]
	[KnownType(typeof(SysMsg_SimCardChanged))]
	[KnownType(typeof(SysMsg_SubscribePacket))]
    [KnownType(typeof(SysMsg_ManageService))]
    [KnownType(typeof(SysMsg_DWTask))]
    [KnownType(typeof(SysMsg_DeviceTypeChange))]
    [KnownType(typeof(SysMsg_AddUpdateAddresses))]
#endif
    public class SysMessage
	{
		#region public SysMessageType Type
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SysMessageType Type;
		#endregion
		#region public Enums.Module Destination
#if !WindowsCE && !Android
		[DataMember]
#endif
        public Enums.Module Destination;
		#endregion
		#region public Enums.Module Source
#if !WindowsCE && !Android
		[DataMember]
#endif
        public Enums.Module Source;
		#endregion
		#region public bool RequestAck
#if !WindowsCE && !Android
		[DataMember]
#endif
        public bool RequestAck;
		#endregion
		#region public object Msg
#if !WindowsCE && !Android
		[DataMember]
#endif
        public object Msg;
		#endregion
	}
	#endregion
	#region SysMessageType
	[Serializable]
	public enum SysMessageType : int
	{
		UpdateData = 1,
        KillTaskForSerialNbr = 2,
        DumpModuleState = 3,
        DeviceHierarchyChange = 4,
		ForceInsertDataArch = 5,
		SimCardChanged = 6,
		ProcessModuleCommand = 7,
		SubscribePacket = 8,
        ManageService = 9,
        DWTask = 10,
        DeviceTypeUpdate = 11,
        AddUpdateAddresses = 12, // przesyłany do DAQ.Router i DAQ.Controller w celu update lub dodania adresów IP/Phone w cache modułów
        UpdateDeviceDriverData = 13,
        GetDeviceDriverData = 14,
    }
	#endregion

	#region SysMsg_UpdateData
	[Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class SysMsg_UpdateData
	{
		#region public SN SerialNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SN SerialNbr;
		#endregion
		#region public long? IdLocation
#if !WindowsCE && !Android
		[DataMember]
#endif
        public long? IdLocation;
		#endregion
		#region public long? IdMeter
#if !WindowsCE && !Android
		[DataMember]
#endif
        public long? IdMeter;
		#endregion
		#region public List<DataValue> Values
#if !WindowsCE && !Android
		[DataMember]
#endif
        public List<DataValue> Values;
		#endregion
		#region public bool SaveToDB
#if !WindowsCE && !Android
		[DataMember]
#endif
        public bool SaveToDB;
		#endregion
	}
	#endregion
	#region SysMsg_GetData
	[Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class SysMsg_GetData
	{
		#region public SN SerialNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SN SerialNbr;
		#endregion
		#region public long? IdLocation
#if !WindowsCE && !Android
		[DataMember]
#endif
        public long? IdLocation;
		#endregion
		#region public long? IdMeter
#if !WindowsCE && !Android
		[DataMember]
#endif
        public long? IdMeter;
		#endregion
		#region public List<DataValue> Values
#if !WindowsCE && !Android
		[DataMember]
#endif
        public List<DataValue> Values;
		#endregion
        #region public long? MsgSourceId
#if !WindowsCE && !Android
        [DataMember]
#endif
        public long? MsgSourceId;
        #endregion
	}
	#endregion
    #region SysMsg_DeviceHierarchyChange
    [Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class SysMsg_DeviceHierarchyChange
	{
		#region public SN SerialNbrParent
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SN SerialNbrParent;
		#endregion
		#region public SN SerialNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SN SerialNbr;
		#endregion
		#region public int SlotType
#if !WindowsCE && !Android
		[DataMember]
#endif
        public Enums.SlotType SlotType;
		#endregion
		#region public int SlotNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public int SlotNbr;
		#endregion
		#region public int ProtocolIn
#if !WindowsCE && !Android
		[DataMember]
#endif
        public Enums.Protocol ProtocolIn;
		#endregion
		#region public int ProtocolOut
#if !WindowsCE && !Android
		[DataMember]
#endif
        public Enums.Protocol ProtocolOut;
		#endregion
        #region public TimeSpan ReleaseTime
#if !WindowsCE && !Android
        [DataMember]
#endif
        public TimeSpan ReleaseTime;
		#endregion
#if  NEW_DEVICE_CACHE
        #region public ChangeAction ChangeAction
#if !WindowsCE && !Android
        [DataMember]
#endif
        //Insert = 1, ADD_DEVICE_HIERARCHY
        //Update = 2, GET_FROM_DB 
        //Delete = 3, REMOVE_DEVICE_HIERARCHY 
        public ChangeAction ChangeAction;
        #endregion
#endif
	}
    #endregion
	#region SysMsg_SimCardChanged
	[Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class SysMsg_SimCardChanged
	{
		#region public SN SerialNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public SN SerialNbr;
		#endregion
		#region public string OldPhoneNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public string OldPhoneNbr;
		#endregion
		#region public string NewPhoneNbr
#if !WindowsCE && !Android
		[DataMember]
#endif
        public string NewPhoneNbr;
		#endregion
		#region public string IP
#if !WindowsCE && !Android
		[DataMember]
#endif
        public string IP;
		#endregion
		#region public int Port
#if !WindowsCE && !Android
		[DataMember]
#endif
        public int Port;
		#endregion
		#region public bool SaveToDB
#if !WindowsCE && !Android
		[DataMember]
#endif
        public bool SaveToDB;
		#endregion
	}
	#endregion
	#region SysMsg_SubscribePacket
	[Serializable]
#if !WindowsCE && !Android
	[DataContract]
#endif
    public class SysMsg_SubscribePacket
	{
		#region public bool Subscribe - true:subscribe, false:unsubscribe
#if !WindowsCE && !Android
		[DataMember]
#endif
        /// True: subscribe
		/// False: unsubscribe
		public bool Subscribe;
		#endregion
		#region public SN SerialNbr
#if !WindowsCE && !Android 
		[DataMember]
#endif
        public SN SerialNbr;
		#endregion
		#region public DateTime? SubscribeTimeOut
#if !WindowsCE && !Android
		[DataMember]
#endif
        public DateTime? SubscribeTimeOut;
		#endregion
	}
	#endregion
    #region SysMsg_ManageService
    [Serializable]
#if !WindowsCE && !Android
    [DataContract]
#endif
    public class SysMsg_ManageService
    {
        #region public string ServiceName
#if !WindowsCE && !Android
        [DataMember]
#endif
        public string ServiceName;
        #endregion
        #region public int Command
        /// <summary>
        /// 1 - Start Service
        /// 2 - Stop Service
        /// 3 - Restart Service
        /// </summary>
#if !WindowsCE && !Android
        [DataMember]
#endif
        public int Command;
        #endregion
    }
    #endregion
    #region SysMsg_DWTask
    [Serializable]
#if !WindowsCE && !Android
    [DataContract]
#endif
    public class SysMsg_DWTask
    {
        #region public Enums.DWTaskType TaskType
#if !WindowsCE && !Android
        [DataMember]
#endif
        public Enums.DWTaskType TaskType;
        #endregion

        #region public List<long> IdLocation
#if !WindowsCE && !Android
        [DataMember]
#endif
        public List<long> IdLocation;
        #endregion

        #region public DateTime? StartTime
#if !WindowsCE && !Android
        [DataMember]
#endif
        public DateTime? StartTime;
        #endregion

        #region public Action Action
#if !WindowsCE && !Android
        [DataMember]
#endif
        public Action Action;
        #endregion


    }
    #endregion
    #region SysMsg_DeviceTypeChange
    [Serializable]
#if !WindowsCE && !Android
    [DataContract]
#endif
    public class SysMsg_DeviceTypeChange
    {
        #region public SN SerialNbr
#if !WindowsCE && !Android
        [DataMember]
#endif
        public SN SerialNbr;
        #endregion
        #region public int IdDeviceType
#if !WindowsCE && !Android
        [DataMember]
#endif
        public int IdDeviceType;
        #endregion
    }
    #endregion
    #region SysMsg_AddUpdateAddresses
    [Serializable]
#if !WindowsCE && !Android
    [DataContract]
#endif
    /// Przesyłany do DAQ.Router i DAQ.Controller w celu update lub dodania adresów IP/Phone w cache modułów
    public class SysMsg_AddUpdateAddresses
    {
        #region public SN SerialNbr
#if !WindowsCE && !Android
        [DataMember]
#endif
        public SN SerialNbr;
        #endregion
        #region public List<DataValue> Values
#if !WindowsCE && !Android
        [DataMember]
#endif
        public List<DataValue> Values;
        #endregion
        #region public bool OnlyUpdate
#if !WindowsCE && !Android
        [DataMember]
#endif
        public bool OnlyUpdate;
        #endregion
    }
    #endregion
}
