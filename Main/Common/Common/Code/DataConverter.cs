﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Security.Cryptography;

namespace IMR.Suite.Common
{
    /// <summary>
    /// Metody związane z konwersją danych.
    /// </summary>
    public class DataConverter
    {
        #region GetBytesFromBool
        public static byte[] GetBytesFromBool(bool ValueInBool)
        {
            byte[] result = new byte[1];

            if (ValueInBool)
            {
                return new byte[1] { 1 };
            }
            else
            {
                return new byte[1] { 0 };
            }
        }
        #endregion GetBytesFromBool
        #region GetBytesFromByte
        public static byte[] GetBytesFromByte(byte ValueInByte)
        {
            byte[] result = new byte[1];
            result[0] = ValueInByte;

            return result;
        }
        #endregion GetBytesFromByte
        #region GetBytesFromShort
        public static byte[] GetBytesFromShort(short ValueInShort)
        {
            return BitConverter.GetBytes(ValueInShort);
        }
        #endregion GetBytesFromShort
        #region GetBytesFromInt
        public static byte[] GetBytesFromInt(int ValueInInt)
        {
            return BitConverter.GetBytes(ValueInInt);
        }
        #endregion GetBytesFromInt
        #region GetBytesFromStringASCII
        public static byte[] GetBytesFromStringASCII(string ValueInStringASCII)
        {
            return GetBytesFromStringASCII(ValueInStringASCII, null);
        }
        public static byte[] GetBytesFromStringASCII(string ValueInStringASCII, byte? EndingByte)
        {
            try
            {
                System.Text.Encoding ascii = System.Text.Encoding.ASCII;

                byte[] asciiBytes = ascii.GetBytes(ValueInStringASCII);

                byte[] result = null;

                // na koncu podajemy dodatkowy bajt
                if (EndingByte.HasValue)
                {
                    result = new byte[asciiBytes.Length + 1];
                }
                else
                {
                    result = new byte[asciiBytes.Length];
                }

                for (int i = 0; i < asciiBytes.Length; i++)
                {
                    result[i] = asciiBytes[i];
                }

                // na koncu podajemy dodatkowy bajt
                if (EndingByte.HasValue)
                {
                    result[result.Length - 1] = EndingByte.Value;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in DataConverter.GetBytesFromStringASCII: " + ex.Message);
            }
        }
        #endregion GetBytesFromStringASCII
        #region GetBytesFromStringBCD
        public static byte[] GetBytesFromStringBCD(string ValueInStringBCD)
        {
            try
            {
                int bytesNumber = (int)Math.Ceiling(((double)ValueInStringBCD.Length / 2));
                byte[] result = new byte[bytesNumber];

                for (int i = 0; i < result.Length; i++)
                {
                    int left = int.Parse(ValueInStringBCD.Substring(i * 2, 1));

                    int right = 0xF;
                    if (ValueInStringBCD.Length > i * 2 + 1)
                    {
                        right = int.Parse(ValueInStringBCD.Substring(i * 2 + 1, 1));
                    }

                    result[i] = Convert.ToByte(left * 16 + right);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in DataConverter.GetBytesFromStringBCD: " + ex.Message);
            }
        }
        #endregion GetBytesFromStringBCD

        #region GetBoolFromByte
        public static bool GetBoolFromByte(byte ValueInByte)
        {
            if (ValueInByte == 0)
                return false;
            else
                return true;
        }
        #endregion GetBoolFromByte
        #region GetIntFromBytes
        public static int GetIntFromBytes(byte[] ValueInBytes)
        {
            switch (ValueInBytes.Length)
            {
                case 1:
                    return (int)ValueInBytes[0];
                case 2:
                    return (int)BitConverter.ToUInt16(ValueInBytes, 0);
                case 4:
                    return BitConverter.ToInt32(ValueInBytes, 0);
                default:
                    throw new Exception("Error in DataConverter.GetIntFromBytes.");
            }
        }
        #endregion GetIntFromBytes
        #region GetDoubleFromBytes
        public static double GetDoubleFromBytes(byte[] ValueInBytes)
        {
            return BitConverter.ToDouble(ValueInBytes, 0);
        }
        #endregion GetDoubleFromBytes
        #region GetStringASCIIFromBytes
        public static string GetStringASCIIFromBytes(byte[] ValueInBytes)
        {
            try
            {
                System.Text.Encoding ascii = System.Text.Encoding.ASCII;

                byte[] asciiBytes = new byte[0];

                // jesli ostatni bajt to 0
				if (ValueInBytes.Length > 1 && ValueInBytes[ValueInBytes.Length - 1] == 0)
                {
                    asciiBytes = new byte[ValueInBytes.Length - 1];
                }
                else
                {
                    asciiBytes = new byte[ValueInBytes.Length];
                }

                Buffer.BlockCopy(ValueInBytes, 0, asciiBytes, 0, asciiBytes.Length);

                return ascii.GetString(asciiBytes, 0, asciiBytes.Length);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in DataConverter.GetStringASCIIFromBytes: " + ex.Message);
            }
        }
        #endregion GetStringASCIIFromBytes
        #region GetStringBCDFromBytes
        public static string GetStringBCDFromBytes(byte[] ValueInBytes)
        {
            try
            {
                int charNumber = ValueInBytes.Length * 2;
                string result = "";

                for (int i = 0; i < ValueInBytes.Length; i++)
                {
                    int left = ValueInBytes[i] / 16;
                    int right = ValueInBytes[i] % 16;

                    if (right != 0xF)
                    {
                        result += left.ToString() + right.ToString();
                    }
                    else
                    {
                        result += left.ToString();
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in DataConverter.GetStringBCDFromBytes: " + ex.Message);
            }
        }
        #endregion GetStringBCDFromBytes
		#region GetStringHexFromBytes
		public static string GetStringHexFromBytes(byte[] ValueInBytes)
        {
            try
            {
                StringBuilder result = new StringBuilder();

                foreach (byte b in ValueInBytes)
                {
                    result.Append(Convert.ToString(b, 16).PadLeft(2, '0').ToUpper());
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in DataConverter.GetStringHexFromBytes: " + ex.Message);
            }
        }
        #endregion GetStringHexFromBytes
        
        #region EncodePassword
        public static string EncodePassword(string originalPassword)
        {
            string result = "";

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            byte[] encodedBytes = md5.ComputeHash(originalBytes);

            foreach (byte item in encodedBytes)
            {
                result += (item < 16) ? String.Format("0{0:x}", item) : item.ToString("x");
            }
            return result;
        }
        #endregion EncodePassword

        #region AlevelTemperature
        /// <summary>
        /// Specyficzny format dla temperatury (pomysł Bociana): wartosc na 7bitach a znak na 8 bicie
        /// </summary>
        /// <param name="bytes">dwa bajty z alevela, przy czym tylko młodszy ma znaczenie</param>
        /// <returns>signed short (Int16)</returns>
        public static object AlevelTemperature(byte[] bytes)
        {
            short result = -1;
            if ((bytes[0] & 0x80) == 0x80)
                result *= (short)(bytes[0] & 0x7F);
            else
                result = bytes[0];
            return result;
        }
        #endregion

		#region CalculateSlopeBiasScaling
		public static object CalculateSlopeBiasScaling(object value, double slope, double bias)
		{
			if (value is double) return (double)((double)value * slope + bias);
			if (value is float) return (float)((float)value * slope + bias);
			if (value is long) return (long)((long)value * slope + bias);
			if (value is ulong) return (ulong)((ulong)value * slope + bias);
			if (value is int) return (int)((int)value * slope + bias);
			if (value is uint) return (uint)((uint)value * slope + bias);
			if (value is short) return (short)((short)value * slope + bias);
			if (value is ushort) return (ushort)((ushort)value * slope + bias);
			if (value is char) return (char)((char)value * slope + bias);
			if (value is byte) return (byte)((byte)value * slope + bias);

			return value;
		}
		#endregion

        #region DoubleToBytes
        public static class DoubleToBytes
        {
            #region Short
            public static byte[] Short10(object v)
            {
                return Short(v, 10);
            }
            public static byte[] Short100(object v)
            {
                return Short(v, 100);
            }
            public static byte[] Short1000(object v)
            {
                return Short(v, 1000);
            }
            public static byte[] Short(object v, int multiplier)
            {
                return BitConverter.GetBytes((short)Math.Round(Convert.ToDouble(v) * multiplier));
            }

            public static object Short10(byte[] b)
            {
                return Short(b, 10);
            }
            public static object Short100(byte[] b)
            {
                return Short(b, 100);
            }
            public static object Short1000(byte[] b)
            {
                return Short(b, 1000);
            }
            public static object Short(byte[] b, int multiplier)
            {
                return (Double)(BitConverter.ToInt16(b, 0) / (Double)multiplier);
            }
            #endregion

            #region Ushort
            public static byte[] Ushort10(object v)
            {
                return Ushort(v, 10);
            }
            public static byte[] Ushort100(object v)
            {
                return Ushort(v, 100);
            }
            public static byte[] Ushort1000(object v)
            {
                return Ushort(v, 1000);
            }
            public static byte[] Ushort(object v, int multiplier)
            {
                return BitConverter.GetBytes((ushort)Math.Round(Convert.ToDouble(v) * multiplier));
            }

            public static object Ushort10(byte[] b)
            {
                return Ushort(b, 10);
            }
            public static object Ushort100(byte[] b)
            {
                return Ushort(b, 100);
            }
            public static object Ushort1000(byte[] b)
            {
                return Ushort(b, 1000);
            }
            public static object Ushort(byte[] b, int multiplier)
            {
                return (Double)(BitConverter.ToUInt16(b, 0) / (Double)multiplier);
            }
            #endregion

            #region Int
            public static byte[] Int10(object v)
            {
                return Int(v, 10);
            }
            public static byte[] Int100(object v)
            {
                return Int(v, 100);
            }
            public static byte[] Int1000(object v)
            {
                return Int(v, 1000);
            }
            public static byte[] Int(object v, int multiplier)
            {
                return BitConverter.GetBytes((int)Math.Round(Convert.ToDouble(v) * multiplier));
            }

            public static object Int10(byte[] b)
            {
                return Int(b, 10);
            }
            public static object Int100(byte[] b)
            {
                return Int(b, 100);
            }
            public static object Int1000(byte[] b)
            {
                return Int(b, 1000);
            }
            public static object Int(byte[] b, int multiplier)
            {
                return (Double)(BitConverter.ToInt32(b, 0) / (Double)multiplier);
            }
            #endregion

            #region Uint
            public static byte[] UInt10(object v)
            {
                return UInt(v, 10);
            }
            public static byte[] UInt100(object v)
            {
                return UInt(v, 100);
            }
            public static byte[] UInt1000(object v)
            {
                return UInt(v, 1000);
            }
            public static byte[] UInt(object v, int multiplier)
            {
                return BitConverter.GetBytes((uint)Math.Round(Convert.ToDouble(v) * multiplier));
            }

            public static object UInt10(byte[] b)
            {
                return UInt(b, 10);
            }
            public static object UInt100(byte[] b)
            {
                return UInt(b, 100);
            }
            public static object UInt1000(byte[] b)
            {
                return UInt(b, 1000);
            }
            public static object UInt(byte[] b, int multiplier)
            {
                return (Double)(BitConverter.ToUInt32(b, 0) / (Double)multiplier);
            }
            #endregion
        }
        #endregion
    }
}
