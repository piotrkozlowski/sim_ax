﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;

namespace IMR.Suite.Common.Code
{
    /// <summary>
    /// Enables access to objects across application domain boundaries.
    /// This type differs from <see cref="MarshalByRefObjectDisposable"/> by ensuring that the
    /// service lifetime is managed deterministically by the consumer.
    /// </summary>
    [Serializable]
    public abstract class MarshalByRefObjectDisposable : MarshalByRefObject, IDisposable
    {

        private bool disposed = false;

        ~MarshalByRefObjectDisposable()
        {
            Dispose(false);
        }

#if !WindowsCE
        public override object InitializeLifetimeService()
        {
            return null;
        }
        protected virtual IEnumerable<MarshalByRefObjectDisposable> NestedMarshalByRefObjectDisposables
        {
            get { yield break; }
        }
        private void Disconnect()
        {
            RemotingServices.Disconnect(this);

            foreach (var tmp in NestedMarshalByRefObjectDisposables)
                RemotingServices.Disconnect(tmp);
        }
#endif

        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to take this object off the finalization queue
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Clear();
                }

                // Call the appropriate methods to clean up unmanaged resources here.
                // If disposing is false, only the following code is executed.
#if !WindowsCE
                Disconnect();
#endif
                // Note disposing has been done.
                disposed = true;

            }
        }

        #region Clear
        public virtual void Clear() { }
        #endregion

    }
}
