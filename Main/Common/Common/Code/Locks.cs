using System;
using System.Threading;

namespace IMR.Suite.Common
{
	#region static class Locks
	public static class Locks
	{
		#region GetReadLock
#if !WindowsCE
		public static void GetReadLock(ReaderWriterLockSlim locks)
		{
			bool lockAcquired = false;
			while (!lockAcquired)
				lockAcquired = locks.TryEnterUpgradeableReadLock(1);
		}
#else
			public static void GetReadLock(Object locks) {}
#endif
		#endregion
		#region GetReadOnlyLock
#if !WindowsCE
		public static void GetReadOnlyLock(ReaderWriterLockSlim locks)
		{
			bool lockAcquired = false;
			while (!lockAcquired)
				lockAcquired = locks.TryEnterReadLock(1);
		}
#else
			public static void GetReadOnlyLock(Object locks) { }
#endif
		#endregion
		#region GetWriteLock
#if !WindowsCE
		public static void GetWriteLock(ReaderWriterLockSlim locks)
		{
			bool lockAcquired = false;
			while (!lockAcquired)
				lockAcquired = locks.TryEnterWriteLock(1);
		}
#else
			public static void GetWriteLock(Object locks) { }
#endif
		#endregion
		#region ReleaseReadOnlyLock
#if !WindowsCE
		public static void ReleaseReadOnlyLock(ReaderWriterLockSlim locks)
		{
			if (locks.IsReadLockHeld)
				locks.ExitReadLock();
		}
#else
			public static void ReleaseReadOnlyLock(Object locks) { }
#endif
		#endregion
		#region ReleaseReadLock
#if !WindowsCE
		public static void ReleaseReadLock(ReaderWriterLockSlim locks)
		{
			if (locks.IsUpgradeableReadLockHeld)
				locks.ExitUpgradeableReadLock();
		}
#else
			public static void ReleaseReadLock(Object locks) { }
#endif
		#endregion
		#region ReleaseWriteLock
#if !WindowsCE
		public static void ReleaseWriteLock(ReaderWriterLockSlim locks)
		{
			if (locks.IsWriteLockHeld)
				locks.ExitWriteLock();
		}
#else
			public static void ReleaseWriteLock(Object locks) { }
#endif
		#endregion
		#region ReleaseLock
#if !WindowsCE
		public static void ReleaseLock(ReaderWriterLockSlim locks)
		{
			ReleaseWriteLock(locks);
			ReleaseReadLock(locks);
			ReleaseReadOnlyLock(locks);
		}
#else
			public static void ReleaseLock(Object locks) { }
#endif
		#endregion
		#region GetLockInstance
#if !WindowsCE
		public static ReaderWriterLockSlim GetLockInstance()
		{
			return GetLockInstance(LockRecursionPolicy.SupportsRecursion);
		}
		public static ReaderWriterLockSlim GetLockInstance(LockRecursionPolicy recursionPolicy)
		{
			return new ReaderWriterLockSlim(recursionPolicy);
		}
#else
			public static Object GetLockInstance() { return new Object(); }
#endif
		#endregion
	}
	#endregion

	#region abstract class BaseLock
	public abstract class BaseLock : IDisposable
	{
#if !WindowsCE
		protected ReaderWriterLockSlim _Locks;
#else
			protected Object _Locks;
#endif
		protected bool _ThreadSafe;

		#region Ctor
#if !WindowsCE
		public BaseLock(ReaderWriterLockSlim locks)
#else
			public BaseLock(Object locks)
#endif
		{
			_Locks = locks;
			_ThreadSafe = true;
		}
#if !WindowsCE
		public BaseLock(ReaderWriterLockSlim locks, bool threadSafe)
#else
			public BaseLock(Object locks, bool threadSafe)
#endif
		{
			_Locks = locks;
			_ThreadSafe = threadSafe;
		}
		#endregion
		public abstract void Dispose();
	}
	#endregion

	#region class ReadLock
	public class ReadLock : BaseLock
	{
		#region Ctor
#if !WindowsCE
		public ReadLock(ReaderWriterLockSlim locks, bool threadSafe)
			: base(locks, threadSafe)
#else
			public ReadLock(Object locks, bool threadSafe) : base(locks, threadSafe)
#endif
		{
			if (this._ThreadSafe)
				Locks.GetReadLock(this._Locks);
		}
#if !WindowsCE
		public ReadLock(ReaderWriterLockSlim locks) : this(locks, true) { }
#else
			public ReadLock(Object locks) : this(locks, true) {}
#endif
		#endregion
		#region Dispose
		public override void Dispose()
		{
			if (this._ThreadSafe)
				Locks.ReleaseReadLock(this._Locks);
		}
		#endregion
	}
	#endregion
	#region class ReadOnlyLock
	public class ReadOnlyLock : BaseLock
	{
		#region Ctor
#if !WindowsCE
		public ReadOnlyLock(ReaderWriterLockSlim locks, bool threadSafe)
			: base(locks, threadSafe)
#else
			public ReadOnlyLock(Object locks, bool threadSafe) : base(locks, threadSafe)
#endif
		{
			if (this._ThreadSafe)
				Locks.GetReadOnlyLock(this._Locks);
		}
#if !WindowsCE
		public ReadOnlyLock(ReaderWriterLockSlim locks) : this(locks, true) { }
#else
			public ReadOnlyLock(Object locks) : this(locks, true) { }
#endif
		#endregion
		#region Dispose
		public override void Dispose()
		{
			if (this._ThreadSafe)
				Locks.ReleaseReadOnlyLock(this._Locks);
		}
		#endregion
	}
	#endregion
	#region class WriteLock
	public class WriteLock : BaseLock
	{
		#region Ctor
#if !WindowsCE
		public WriteLock(ReaderWriterLockSlim locks, bool threadSafe)
			: base(locks, threadSafe)
#else
			public WriteLock(Object locks, bool threadSafe) : base(locks, threadSafe)
#endif
		{
			if (this._ThreadSafe)
				Locks.GetWriteLock(this._Locks);
		}
#if !WindowsCE
		public WriteLock(ReaderWriterLockSlim locks) : this(locks, true) { }
#else
			public WriteLock(Object locks) : this(locks, true) { }
#endif
		#endregion
		#region Dispose
		public override void Dispose()
		{
			if (this._ThreadSafe)
				Locks.ReleaseWriteLock(this._Locks);
		}
		#endregion
	}
	#endregion
}