using System;
using System.Collections;
using System.Reflection;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Management;
using System.Threading;
using Microsoft.Win32;

namespace IMR.Suite.Common
{
	[global::System.ComponentModel.DesignTimeVisible(false)]
    public class dotService : global::System.ServiceProcess.ServiceBase
    {
		protected global::System.ServiceProcess.ServiceBase m_Srvc = null;
		private CmdLineArgs m_cmdLineArgs;
		private string m_sArguments = "";

		public bool m_bDebug = false;
		private static readonly ManualResetEvent StopDebugEvent = new ManualResetEvent(false);

		public string m_sServiceName = "dotService";
		public string m_sDisplayName = "dotService class";
		public ServiceStartMode m_StartType = ServiceStartMode.Automatic;
		public ServiceAccount m_Account = ServiceAccount.LocalSystem;
		public string m_sUserName = null;
		public string m_sPassword = null;
		public string[] m_sDependedOn = null;
		public string m_sDescription = null;
		public bool m_bInteractWithDesktop = false;

        #region ctor
        public dotService()
        {
            m_Srvc = this;
            AutoLog = false;
            ServiceName = m_sServiceName;
        }
        #endregion

		#region RegisterService
		public bool RegisterService(string[] args)
		{
			m_cmdLineArgs = new CmdLineArgs(args);

			// arguments for the service
			if(m_cmdLineArgs["a"] != null)
				m_sArguments = m_cmdLineArgs["a"].ToString();

			// login-account (only useful with -i)
			if(m_cmdLineArgs["l"] != null)
				m_sUserName = m_cmdLineArgs["l"].ToString();

			// password (only useful with -i)
			if(m_cmdLineArgs["p"] != null)
				m_sPassword = m_cmdLineArgs["p"].ToString();

			// install the service
			if(m_cmdLineArgs["i"] != null)
				return InstallService();

			// uninstall the service
			if(m_cmdLineArgs["u"] != null)
				return UnInstallService();

			// start the service
			if(m_cmdLineArgs["s"] != null)
				return StartService();

			// end the service
			if(m_cmdLineArgs["e"] != null)
				return StopService();

			// debug the service
			if(m_cmdLineArgs["d"] != null)
				return DebugService();

            global::System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new global::System.ServiceProcess.ServiceBase[] { m_Srvc };
            global::System.ServiceProcess.ServiceBase.Run(ServicesToRun);

			return true;
		}
		#endregion RegisterService
		#region InstallService
		private bool InstallService()
		{
            string basePath = Assembly.GetEntryAssembly().Location;
            GetServiceNameFromConfiguration(basePath);

            dotServiceInstaller serviceInstaller = new dotServiceInstaller(this);

			serviceInstaller.Context = new InstallContext(global::System.IO.Path.ChangeExtension(basePath, ".InstallLog"), global::System.Environment.GetCommandLineArgs());
			basePath += " " + m_sArguments;
			serviceInstaller.Context.Parameters["assemblypath"] = basePath;
			try
			{
				Hashtable hashtable = new Hashtable();
				serviceInstaller.Install(hashtable);
			}
			catch(Exception e)
			{
				Console.WriteLine("Service {0} could not be uninstall. Exception: {1}", m_sServiceName, e.Message);
			}
			return true;
		}
		#endregion InstallService
		#region UnInstallService
		private bool UnInstallService()
		{
            string basePath = Assembly.GetEntryAssembly().Location;
            GetServiceNameFromConfiguration(basePath);

            dotServiceInstaller serviceInstaller = new dotServiceInstaller(this);
			serviceInstaller.Context = new InstallContext(global::System.IO.Path.ChangeExtension(basePath, ".InstallLog"), global::System.Environment.GetCommandLineArgs());
			serviceInstaller.Context.Parameters["assemblypath"] = basePath;
			try
			{
				serviceInstaller.Uninstall(null);
			}
			catch(Exception e)
			{
				Console.WriteLine("Service {0} could not be uninstall. Exception: {1}", m_sServiceName, e.Message);
			}
			return true;
		}
		#endregion UnInstallService

		#region StartService
		private bool StartService()
		{
			ServiceController sc  = new ServiceController(m_sServiceName);
			if(sc.Status == ServiceControllerStatus.Stopped)
			{
				// Start the service if the current status is stopped.
				Console.WriteLine("Starting the {0} service...", m_sServiceName);
				try 
				{
					// Start the service, and wait until its status is "Running".
					sc.Start(CmdLineArgs.StringToArgs(m_sArguments));
					sc.WaitForStatus(ServiceControllerStatus.Running);

					// Display the current service status.
					Console.WriteLine("The {0} service status is now set to {1}.", m_sServiceName, sc.Status.ToString());
				}
				catch (InvalidOperationException e)
				{
					Console.WriteLine("Could not start the {0} service. Exception: {1}", m_sServiceName, e.Message);
				}
			}
			else
				Console.WriteLine("The {0} service status is now set to {1}.", m_sServiceName, sc.Status.ToString());

			return true;
		}
		#endregion StartService
		#region StopService
		private bool StopService()
		{
			ServiceController sc = new ServiceController(m_sServiceName);
			if(	(sc.Status != ServiceControllerStatus.Stopped) &&
				(sc.Status != ServiceControllerStatus.StopPending) )
			{
				try 
				{
					// Stop the service if its status is not set to "Stopped".
					Console.WriteLine("Stopping the {0} service...", m_sServiceName);
					sc.Stop();
					sc.WaitForStatus(ServiceControllerStatus.Stopped);

					// Display the current service status.
					Console.WriteLine("The {0} service status is now set to {1}.", 
						m_sServiceName,
						sc.Status.ToString());
				}
				catch (InvalidOperationException e)
				{
					Console.WriteLine("Could not stop the {0} service. Ecxeption: {1}", m_sServiceName, e.Message);
				}
			}
			else
				Console.WriteLine("The {0} service status is now set to {1}.", m_sServiceName, sc.Status.ToString());

			return true;
		}
		#endregion StopService
		#region DebugService
		private bool DebugService()
		{
			m_bDebug = true;

            GetServiceNameFromConfiguration(Assembly.GetEntryAssembly().Location);

            Console.WriteLine("Debugging {0}, Version={1}",  this.m_sServiceName, Assembly.GetEntryAssembly().GetName().Version.ToString());

			Console.CancelKeyPress += new ConsoleCancelEventHandler(CtrlCHandler);
			OnStart(CmdLineArgs.StringToArgs(m_sArguments));

			Thread.CurrentThread.Priority = ThreadPriority.Lowest;

			StopDebugEvent.WaitOne();

			Console.CancelKeyPress -= new ConsoleCancelEventHandler(CtrlCHandler);
			OnStop();

			return true;
		}
		#region CtrlCHandler
		protected static void CtrlCHandler(object sender, ConsoleCancelEventArgs args)
		{
			if (args.SpecialKey == ConsoleSpecialKey.ControlC)
			{
				Console.WriteLine("Stopping due to user input Ctrl+C");
                args.Cancel = true;
				StopDebugEvent.Set();
			}
		}
        #endregion
        #endregion DebugService

        #region GetServiceNameFromConfiguration
        public void GetServiceNameFromConfiguration(string servicePath)
        {
            #region Get ServiceName from *.exe.config if defined
            string configKey = GetConfigurationValue(servicePath, "ServiceName");
            if (!string.IsNullOrEmpty(configKey))
                m_sServiceName = configKey;
            #endregion
            #region Get ServiceDispalyName from *.exe.config if defined
            configKey = GetConfigurationValue(servicePath, "ServiceDisplayName");
            if (!string.IsNullOrEmpty(configKey))
                m_sDisplayName = configKey;
            #endregion
            #region Get ServiceDescription from *.exe.config if defined
            configKey = GetConfigurationValue(servicePath, "ServiceDescription");
            if (!string.IsNullOrEmpty(configKey))
                m_sDescription = configKey;
            #endregion
            #region Get ServiceDependedOn from *.exe.config if defined
            configKey = GetConfigurationValue(servicePath, "ServiceDependedOn");
            if (!string.IsNullOrEmpty(configKey))
                m_sDependedOn = configKey.Split(new char[] { ' ' });
            #endregion
        }
        #endregion
        #region private GetConfigurationValue
        private string GetConfigurationValue(string servicePath, string key)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(servicePath);
            if (config.AppSettings.Settings[key] != null)
                return config.AppSettings.Settings[key].Value;
            return string.Empty;
        }
        #endregion

        #region Stop
        public void Stop(int exitCode)
        {
            OnStop();
            global::System.Environment.Exit(exitCode);
        }
        #endregion

        #region override OnStart
        protected override void OnStart(string[] args)
        {
            global::System.IO.Directory.SetCurrentDirectory
                (AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
        }
        #endregion
        #region override OnStop
        protected override void OnStop()
        {
        }
        #endregion
        #region override OnShhutdown
        protected override void OnShutdown()
        {
            base.OnShutdown();
        }
        #endregion

		#region MarshalByRefObjectDisposable overrides
		public override object InitializeLifetimeService()
		{
			return null;
		}
        #endregion

        #region dotServiceInstaller
        public class dotServiceInstaller : global::System.Configuration.Install.Installer
        {
            private global::System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
            private global::System.ServiceProcess.ServiceInstaller serviceInstaller;
            public /*static*/ dotService m_Srvc = null;

            #region Constructor
            public dotServiceInstaller()
            {
                m_Srvc = new dotService();
            }
            public dotServiceInstaller(dotService srvc)
            {
                m_Srvc = srvc;
            }
            #endregion
            #region override Install
            public override void Install(IDictionary stateServer)
            {
                AddInstallres();

                //config is where I'll be putting service-specific configuration (Parameters)
                Microsoft.Win32.RegistryKey system, currentControlSet, services, service;
                try
                {
                    //Let the project installer do its job
                    base.Install(stateServer);

                    //Open the HKEY_LOCAL_MACHINE\SYSTEM key
                    system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
                    //Open CurrentControlSet
                    currentControlSet = system.OpenSubKey("CurrentControlSet");
                    //Go to the services key
                    services = currentControlSet.OpenSubKey("Services");
                    //Open the key for your service, and allow writing
                    service = services.OpenSubKey(m_Srvc.m_sServiceName, true);
                    //Add your service's description as a REG_SZ value named "Description"
                    //if (m_Srvc.m_sDescription != null)
                    //    service.SetValue("Description", m_Srvc.m_sDescription); // kiedy� w .Net nie by�o Description
                    //if (Environment.OSVersion.Version.Major >= 6)
                    //    service.SetValue("DelayedAutostart", 1, RegistryValueKind.DWord); // kiedy� w .Net nie by�o DelayedAutostart
                    //(Optional) Add some other information your service will be looking for...
                    // config = service.CreateSubKey("Parameters");
                }
                catch (Exception e)
                {
                    Console.WriteLine("An exception was thrown during serviceinstallation:\n" + e.ToString());
                }
            }
            #endregion
            #region override OnAfterInstall
            protected override void OnAfterInstall(IDictionary savedState)
            {
                base.OnAfterInstall(savedState);

                if (m_Srvc.m_bInteractWithDesktop)
                {
                    ManagementObject srvc = new ManagementObject("Win32_Service=" + "\"" + m_Srvc.m_sServiceName + "\"");
                    ManagementBaseObject inParams = srvc.GetMethodParameters("Change");

                    inParams["DesktopInteract"] = true; // Enable interactive mode (InteractWith Desktop)
                    ManagementBaseObject outParams = srvc.InvokeMethod("Change", inParams, null);
                    if (Convert.ToInt32(outParams.Properties["ReturnValue"].Value) != 0)
                        Console.WriteLine("Failed to set option DesktopInteract");
                }
            }
            #endregion
            #region override OnBeforeUninstall
            protected override void OnBeforeUninstall(IDictionary savedState)
            {
                ServiceController controller = new ServiceController(m_Srvc.m_sServiceName);
                try
                {
                    if (controller.Status == ServiceControllerStatus.Running || controller.Status == ServiceControllerStatus.Paused)
                    {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(15));
                        controller.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception encountered while stoping service:\n" + ex.ToString());
                }
                finally
                {
                    base.OnBeforeUninstall(savedState);
                }
            }
            #endregion
            #region override Uninstall
            public override void Uninstall(IDictionary stateServer)
            {
                AddInstallres();

                Microsoft.Win32.RegistryKey system, currentControlSet, services, service;
                try
                {
                    //Drill down to the service key and open it with write permission
                    system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
                    currentControlSet = system.OpenSubKey("CurrentControlSet");
                    services = currentControlSet.OpenSubKey("Services");
                    service = services.OpenSubKey(this.serviceInstaller.ServiceName, true);
                    //Delete any keys you created during installation (or that your service created)
                    // service.DeleteSubKeyTree("Parameters");
                    //...
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception encountered while uninstalling service:\n" + e.ToString());
                }
                finally
                {
                    //Let the project installer do its job
                    base.Uninstall(stateServer);
                }
            }
            #endregion

            #region private AddInstallres
            private void AddInstallres()
            {
                if (this.Context != null && this.Context.Parameters.ContainsKey("assemblypath"))
                    m_Srvc.GetServiceNameFromConfiguration(this.Context.Parameters["assemblypath"]);

                serviceProcessInstaller = new global::System.ServiceProcess.ServiceProcessInstaller();
                serviceProcessInstaller.Account = m_Srvc.m_Account;
                serviceProcessInstaller.Password = m_Srvc.m_sUserName;
                serviceProcessInstaller.Username = m_Srvc.m_sPassword;

                serviceInstaller = new global::System.ServiceProcess.ServiceInstaller();
                serviceInstaller.DisplayName = m_Srvc.m_sDisplayName;
                serviceInstaller.ServiceName = m_Srvc.m_sServiceName;
                serviceInstaller.Description = m_Srvc.m_sDescription;
                serviceInstaller.ServicesDependedOn = m_Srvc.m_sDependedOn;

                serviceInstaller.StartType = m_Srvc.m_StartType;
                serviceInstaller.DelayedAutoStart = true;

                Installers.AddRange(new global::System.Configuration.Install.Installer[] { serviceProcessInstaller, serviceInstaller });
            }
            #endregion
        }
        #endregion dotServiceInstaller 
    }

    #region CmdLineArgs
    public class CmdLineArgs
	{
		// Variables
		private StringDictionary Parameters;

		// Constructors
		public CmdLineArgs(string Args)
		{
			Extract ( StringToArgs(Args) );
		}

		public CmdLineArgs(string[] Args)
		{
			Extract(Args);
		}

		// Extract command line parameters and values stored in a string array
		private void Extract(string[] Args)
		{
			Parameters=new StringDictionary();
			Regex Spliter=new Regex(@"^([/-]|--){1}(?<name>\w+)([:=])?(?<value>.+)?$",RegexOptions.IgnoreCase|RegexOptions.Compiled);
			char[] TrimChars={'"','\''};
			string Parameter=null;
			Match Part;

			// Valid parameters forms:
			// {-,/,--}param{ ,=,:}((",')value(",'))
			// Examples: -param1 value1 --param2 /param3:"Test-:-work" /param4=happy -param5 '--=nice=--'
			foreach(string Arg in Args)
			{
				Part=Spliter.Match(Arg);
				if(!Part.Success)
				{
					// Found a value (for the last parameter found (space separator))
					if(Parameter!=null) Parameters[Parameter]=Arg.Trim(TrimChars);
				}
				else
				{
					// Matched a name, optionally with inline value
					Parameter=Part.Groups["name"].Value;
					Parameters.Add(Parameter,Part.Groups["value"].Value.Trim(TrimChars));
				}
			}
		}

		// Retrieve a parameter value if it exists
		public string this [string Param]
		{
			get
			{
				return(Parameters[Param]);
			}
		}

		public static string[] StringToArgs(string cmdLine) 
		{
			Regex Extractor=new Regex(@"(['""][^""]+['""])\s*|([^\s]+)\s*",RegexOptions.IgnoreCase|RegexOptions.Compiled);

			MatchCollection Matches = Extractor.Matches(cmdLine);
			string[] Parts = new string[Matches.Count];
			for(int i=0; i< Matches.Count; i++) 
				Parts[i] = Matches[i].Value;
			return Parts;
		}
	}
	#endregion CmdLineArgs
}
