﻿/*--------------------------------------------------------------------------
* AnonymousComparer - lambda compare selector for Linq
* ver 1.2.0.0 (Feb. 04th, 2010)
*
* created and maintained by neuecc <ils@neue.cc>
* licensed under Microsoft Public License(Ms-PL)
* http://neue.cc/
* http://linqcomparer.codeplex.com/
*--------------------------------------------------------------------------*/
// History
//
//2010-02-04 ver 1.2.0.0
//Add: IComparer<T> Create<T>(Func<T, T, int> compare)
//Fixed: compareKeySelector overload no throw if sequence contains null
//
//2010-01-26 ver 1.1.0.0
//Add: Full IEqualtyComparer overload
//
//2009-10-29 ver 1.0.0.0
//1st Release
#region Example
//
//1. Enumerable.Range -> (Right Click) -> Go To Definition -> Ctrl+A Ctrl+C (CopyAll Signature from Metadata)
//2. NewProject -> ConsoleApplication -> Run this code
//
//static data  = @"[paste signature here]";
//
//static void Main(string[] args)
//{
//	var result = data
//		.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
//		.Where(s => Regex.IsMatch(s, "public static.+IEqualityComparer"))
//		.Select(s => Regex.Replace(s, @"(<.+)(>\(this)", "$1,TCompareKey$2"))
//		.Select(s => Regex.Replace(s, @"IEqualityComparer<(.+?)>", "Func<$1,TCompareKey>"))
//		.Select(s => Regex.Replace(s, @"comparer", "compareKeySelector"))
//		.Select(s => s.Trim(' ', ';'))
//		.Select(s => new { Signature = s, Groups = Regex.Match(s, @"^(.+? ){3}(?<method>[^ ]+?)<.+?>\(this (?<args>.+)\)$").Groups })
//		.Select(a => new
//		{
//			a.Signature,
//			MethodName = a.Groups["method"].Value,
//			Args = a.Groups["args"].Value
//				.Split(new[] { ", " }, StringSplitOptions.None)
//				.Select(s => s.Split(' ')).Where(ar => ar.Length == 2).Select(ar => ar.Last())
//		})
//		.Select(a => string.Format("{1} {0} {{ return {2}.{3}({4}{5}); {0}}}{0}{0}",
//			Environment.NewLine,
//			a.Signature,
//			a.Args.First(),
//			a.MethodName,
//			string.Join(",", a.Args.Skip(1).TakeWhile(s => s != "compareKeySelector").ToArray()),
//			(a.Args.Count() == 2 ? "" : ",") + "AnonymousComparer.Create(compareKeySelector)"))
//		.Aggregate(new StringBuilder(), (sb, s) => sb.Append(s))
//		.ToString();
//}
#endregion

using System.Collections.Generic;

namespace System.Linq
{
    public static class AnonymousComparer
    {
        #region IComparer<T>

        /// <summary>Example:AnonymousComparer.Create&lt;int&gt;((x, y) => y - x)</summary>
        public static IComparer<T> Create<T>(Func<T, T, int> compare)
        {
            if (compare == null) throw new ArgumentNullException("compare");

            return new Comparer<T>(compare);
        }

        private class Comparer<T> : IComparer<T>
        {
            private readonly Func<T, T, int> compare;

            public Comparer(Func<T, T, int> compare)
            {
                this.compare = compare;
            }

            public int Compare(T x, T y)
            {
                return compare(x, y);
            }
        }

        #endregion

        #region IEqualityComparer<T>

        /// <summary>Example:AnonymousComparer.Create((MyClass mc) => mc.MyProperty)</summary>
        public static IEqualityComparer<T> Create<T, TKey>(Func<T, TKey> compareKeySelector)
        {
            if (compareKeySelector == null) throw new ArgumentNullException("compareKeySelector");

            return new EqualityComparer<T>(
                (x, y) =>
                {
                    if (object.ReferenceEquals(x, y)) return true;
                    if (x == null || y == null) return false;
                    return compareKeySelector(x).Equals(compareKeySelector(y));
                },
                obj =>
                {
                    if (obj == null) return 0;
                    return compareKeySelector(obj).GetHashCode();
                });
        }

        public static IEqualityComparer<T> Create<T>(Func<T, T, bool> equals, Func<T, int> getHashCode)
        {
            if (equals == null) throw new ArgumentNullException("equals");
            if (getHashCode == null) throw new ArgumentNullException("getHashCode");

            return new EqualityComparer<T>(equals, getHashCode);
        }

        private class EqualityComparer<T> : IEqualityComparer<T>
        {
            private readonly Func<T, T, bool> equals;
            private readonly Func<T, int> getHashCode;

            public EqualityComparer(Func<T, T, bool> equals, Func<T, int> getHashCode)
            {
                this.equals = equals;
                this.getHashCode = getHashCode;
            }

            public bool Equals(T x, T y)
            {
                return equals(x, y);
            }

            public int GetHashCode(T obj)
            {
                return getHashCode(obj);
            }
        }

        #endregion

        #region Extensions for LINQ Standard Query Operators

        // IComparer<T>

        public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TKey, int> compare)
        {
            return source.OrderBy(keySelector, AnonymousComparer.Create(compare));
        }

        public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TKey, int> compare)
        {
            return source.OrderByDescending(keySelector, AnonymousComparer.Create(compare));
        }

        public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TKey, int> compare)
        {
            return source.ThenBy(keySelector, AnonymousComparer.Create(compare));
        }

        public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TKey, int> compare)
        {
            return source.ThenByDescending(keySelector, AnonymousComparer.Create(compare));
        }

        // IEqualityComparer<T>

        public static bool Contains<TSource, TCompareKey>(this IEnumerable<TSource> source, TSource value, Func<TSource, TCompareKey> compareKeySelector)
        {
            return source.Contains(value, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TSource> Distinct<TSource, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TCompareKey> compareKeySelector)
        {
            return source.Distinct(AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TSource> Except<TSource, TCompareKey>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TCompareKey> compareKeySelector)
        {
            return first.Except(second, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.GroupBy(keySelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.GroupBy(keySelector, resultSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.GroupBy(keySelector, elementSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.GroupBy(keySelector, elementSelector, resultSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult, TCompareKey>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return outer.GroupJoin(inner, outerKeySelector, innerKeySelector, resultSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TSource> Intersect<TSource, TCompareKey>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TCompareKey> compareKeySelector)
        {
            return first.Intersect(second, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult, TCompareKey>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return outer.Join(inner, outerKeySelector, innerKeySelector, resultSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static bool SequenceEqual<TSource, TCompareKey>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TCompareKey> compareKeySelector)
        {
            return first.SequenceEqual(second, AnonymousComparer.Create(compareKeySelector));
        }

        /*
                public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TCompareKey> compareKeySelector)
                {
                    return source.ToDictionary(keySelector, AnonymousComparer.Create(compareKeySelector));
                }

                public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, TCompareKey> compareKeySelector)
                {
                    return source.ToDictionary(keySelector, elementSelector, AnonymousComparer.Create(compareKeySelector));
                }
        */

        public static ILookup<TKey, TSource> ToLookup<TSource, TKey, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.ToLookup(keySelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement, TCompareKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, TCompareKey> compareKeySelector)
        {
            return source.ToLookup(keySelector, elementSelector, AnonymousComparer.Create(compareKeySelector));
        }

        public static IEnumerable<TSource> Union<TSource, TCompareKey>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TCompareKey> compareKeySelector)
        {
            return first.Union(second, AnonymousComparer.Create(compareKeySelector));
        }
        #endregion

        #region Extensions for LINQ.Join(s) from http://www.codeproject.com/Articles/488643/LinQ-Extended-Joins
        public static IEnumerable<TResult> LeftJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
        {
            IEnumerable<TResult> _result = Enumerable.Empty<TResult>();

            _result = from s in source
                      join i in inner
                      on pk(s) equals fk(i) into joinData
                      from left in joinData.DefaultIfEmpty()
                      select result(s, left);

            return _result;
        }

        public static IEnumerable<TResult> RightJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
        {
            IEnumerable<TResult> _result = Enumerable.Empty<TResult>();

            _result = from i in inner
                      join s in source
                      on fk(i) equals pk(s) into joinData
                      from right in joinData.DefaultIfEmpty()
                      select result(right, i);

            return _result;
        }

        public static IEnumerable<TResult> FullOuterJoinJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
        {
            List<TSource> sourceList = source.ToList();
            List<TInner> innerList = inner.ToList();

            return sourceList.LeftJoin(innerList, pk, fk, result).Union(sourceList.RightJoin(innerList, pk, fk, result));
        }

        public static IEnumerable<TResult> LeftExcludingJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
            where TInner : class
            where TSource : class
        {
            IEnumerable<TResult> _result = Enumerable.Empty<TResult>();

            _result = from s in source
                      join i in inner
                      on pk(s) equals fk(i) into joinData
                      from left in joinData.DefaultIfEmpty()
                      where left == null
                      select result(s, left);

            return _result;
        }

        public static IEnumerable<TResult> RightExcludingJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
            where TInner : class
            where TSource : class
        {
            IEnumerable<TResult> _result = Enumerable.Empty<TResult>();

            _result = from i in inner
                      join s in source
                      on fk(i) equals pk(s) into joinData
                      from right in joinData.DefaultIfEmpty()
                      where right == null
                      select result(right, i);

            return _result;
        }

        public static IEnumerable<TResult> FulltExcludingJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result)
            where TInner : class
            where TSource : class
        {
            List<TSource> sourceList = source.ToList();
            List<TInner> innerList = inner.ToList();

            return sourceList.LeftExcludingJoin(innerList, pk, fk, result).Union(sourceList.RightExcludingJoin(innerList, pk, fk, result));
        }
        #endregion
    }
}