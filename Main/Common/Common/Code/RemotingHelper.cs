using System;
using System.Net;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.InteropServices;

namespace IMR.Suite.Common
{
	// Zmiany:
	// WERSJA	CZAS				KTO				OPIS ZMIAN
	// -------------------------------------------------------
	// 1.00		2004-02-18 22:40	TWagnerowski	Pierwsza wersja. Na podstawie http://www.ingorammer.com/RemotingFAQ/USEINTERFACESWITHCONFIGFILES.html
	// 1.01		2004-06-02 14:15	PSiodemak		Dodanie funkcji GetLocalIPs

	public class RemotingHelper 
	{
        #region Members
        private static bool _isInit;
        private static IDictionary _wellKnownTypes; 
        #endregion
        
        #region IP Helper Wrapper
		public enum MIB_TCP_STATE : uint
		{
			NO_ERROR	= 0,
			CLOSED		= 1,
			LISTEN		= 2,
			SYN_SENT	= 3,
			SYN_RCVD	= 4,
			ESTAB		= 5,
			FIN_WAIT1	= 6,
			FIN_WAIT2	= 7,
			CLOSE_WAIT	= 8,
			CLOSING		= 9,
			LAST_ACK	= 10,
			TIME_WAIT	= 11,
			DELETE_TCB	= 12
		}
		[StructLayout(LayoutKind.Sequential)]
			public struct MIB_TCPROW
		{
			[MarshalAs(UnmanagedType.U4)]	public uint dwState;  
			[MarshalAs(UnmanagedType.U4)]	public uint dwLocalAddr;
			[MarshalAs(UnmanagedType.U4)]	public uint dwLocalPort;
			[MarshalAs(UnmanagedType.U4)]	public uint dwRemoteAddr;
			[MarshalAs(UnmanagedType.U4)]	public uint dwRemotePort;
		}
		[StructLayout(LayoutKind.Sequential)]
			public class MIB_TCPTABLE 
		{
			[MarshalAs(UnmanagedType.U4)]			public int dwNumEntries;
			[ MarshalAs(UnmanagedType.ByValArray) ]	public MIB_TCPROW[] table;
		}
		[DllImport("iphlpapi.dll",SetLastError=true)]
		[return:MarshalAs(UnmanagedType.U4)]
		public static extern int GetTcpTable(IntPtr pTcpTable, [MarshalAs(UnmanagedType.U4)] out int pdwSize, bool bOrder);
		[DllImport("iphlpapi.dll",SetLastError=true)]
		[return:MarshalAs(UnmanagedType.U4)]
		public static extern int SetTcpEntry(IntPtr pTcpRow);
		#endregion IP Helper Wrapper

		#region CloseSocket
		public static void CloseSocket(int portToClose)
		{
			MIB_TCPTABLE connections = new MIB_TCPTABLE();
			
			int pdwSize = 0;
			// Make the call, getting the number of bytes.
			GetTcpTable(IntPtr.Zero, out pdwSize, true);
			// Now allocate the space in unmanaged memory.
			IntPtr ptrBuffer = Marshal.AllocCoTaskMem(pdwSize);
			// Make the call again.
			GetTcpTable(ptrBuffer, out pdwSize, true);

			// Get the number of structures.
			connections.dwNumEntries = Marshal.ReadInt32(ptrBuffer);
			// The buffer pointer.  Account for the int read already.
			IntPtr ptrPointer = (IntPtr)(ptrBuffer.ToInt32() + 4);

			// Allocate an array of structures.
			connections.table = new MIB_TCPROW[pdwSize];

			// Cycle through.
			for(int i = 0; i < connections.dwNumEntries; i++)
			{
				// Read the structure from the pointer.
				connections.table[i] = (MIB_TCPROW)Marshal.PtrToStructure(ptrPointer, typeof(MIB_TCPROW));

				// Increment the pointer.
				ptrPointer = (IntPtr)(ptrPointer.ToInt32() + Marshal.SizeOf(typeof(MIB_TCPROW)));
			}

			// Free the unmanaged memory.
			Marshal.FreeCoTaskMem(ptrBuffer);

			for(int i=0; i<connections.dwNumEntries;i++)
			{
				// Jesli bedzie koniecznosc to odkomentowac ponizsze linijki
				// Zamykanie LocalPort'ow
                if (IPAddress.HostToNetworkOrder((short)connections.table[i].dwLocalPort) == portToClose)
                {
                    connections.table[i].dwState = (uint)MIB_TCP_STATE.DELETE_TCB;
                    IntPtr ptrTcpRow = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(MIB_TCPROW)));
                    Marshal.StructureToPtr(connections.table[i], ptrTcpRow, false);
                    SetTcpEntry(ptrTcpRow);
                    Marshal.FreeCoTaskMem(ptrTcpRow);
                }
				if( (connections.table[i].dwRemoteAddr != 0) &&
					(IPAddress.HostToNetworkOrder((short)connections.table[i].dwRemotePort) == portToClose))
				{
					connections.table[i].dwState = (uint)MIB_TCP_STATE.DELETE_TCB;
					IntPtr ptrTcpRow = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(MIB_TCPROW)));
					Marshal.StructureToPtr(connections.table[i], ptrTcpRow, false);
					SetTcpEntry(ptrTcpRow);
					Marshal.FreeCoTaskMem(ptrTcpRow);
				}
			}
		}
		#endregion

        #region Configure
        public static object Configure(string configFileName, Type type)
        {
            object Server = null;
            WellKnownClientTypeEntry entry = RemotingConfiguration.IsWellKnownClientType(type);

            if (System.Object.ReferenceEquals(entry, null))
            {
                if (System.IO.File.Exists(configFileName))
                {
                    RemotingConfiguration.Configure(configFileName, false);
                    Server = GetObject(type);
                }
                else
                {
                    Server = null;
                }
            }
            else
            {
                Server = Common.RemotingHelper.GetObject(type);
            }
            return Server;
        }

        internal static Object GetObject(Type type)
        {
            if (!_isInit) InitTypeCache();
            WellKnownClientTypeEntry entr = (WellKnownClientTypeEntry)_wellKnownTypes[type];
            if (entr == null)
                throw new RemotingException("Type not found!");
            return Activator.GetObject(entr.ObjectType, entr.ObjectUrl);
        }
        internal static void InitTypeCache()
        {
            _isInit = true;
            _wellKnownTypes = new Hashtable();
            foreach (WellKnownClientTypeEntry entr in
                RemotingConfiguration.GetRegisteredWellKnownClientTypes())
            {
                if (entr.ObjectType == null)
                    throw new RemotingException("A configured type could not be found. Please check spelling");
                _wellKnownTypes.Add(entr.ObjectType, entr);
            }
        }

        internal static WellKnownServiceTypeEntry IsWellKnownServiceType(Type type)
        {
            foreach (WellKnownServiceTypeEntry entr in
                RemotingConfiguration.GetRegisteredWellKnownServiceTypes())
            {
                if (entr.TypeName == type.FullName)
                    return entr;
            }
            return null;
        } 
        #endregion
	}
}	
