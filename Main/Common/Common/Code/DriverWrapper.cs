﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Configuration;
using System.Runtime.Remoting.Lifetime;

using IMR.Suite.Common;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Common
{
	[Serializable]
	public class DriverWrapper<DriverType> : MarshalByRefObjectDisposable
	{
		public DriverType Driver = default(DriverType);
		private AppDomain Domain;

		private static Dictionary<string, Type[]> TypesInAssemblyDictionary = new Dictionary<string, Type[]>();
		private static Dictionary<string, Type> DerivedTypeDictionary = new Dictionary<string, Type>();

		private Type[] TypesInAssembly = null;
		private Type DerivedType = null;

		#region Constructor
		public DriverWrapper()
		{
		}
		public DriverWrapper(string DriverPath, object[] DriverConstructorArgs)
		{
			string assemblyPath = DriverPath;
			if (!System.IO.Path.IsPathRooted(assemblyPath))
				assemblyPath = AppDomain.CurrentDomain.BaseDirectory + assemblyPath;
			assemblyPath = Path.GetFullPath(assemblyPath);

			string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
			string DllPath = Path.GetDirectoryName(assemblyPath);
			if (DllPath.IndexOf(AppPath) == -1)
			{

				string dllDirectoryName = Path.Combine(AppPath, "Cache");
				DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
				if (!di.Exists)
				{
					di.Create();
					di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
				}

				string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(assemblyPath);
				try
				{
					File.Copy(assemblyPath, newAssemblyPath, true);
				}
				catch { }
				assemblyPath = newAssemblyPath;
				DllPath = Path.GetDirectoryName(assemblyPath);
			}

			AppDomainSetup ads = new AppDomainSetup();
			ads.ApplicationName = "DriverWrapper";
			ads.ApplicationBase = AppPath;
			ads.PrivateBinPath = new Uri(AppPath + "\\").MakeRelativeUri(new Uri(DllPath)).ToString();
			ads.PrivateBinPath += ";Drivers/Protocol";
			ads.ShadowCopyFiles = "true";

			Domain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(assemblyPath), null, ads);
			//Domain.ReflectionOnlyAssemblyResolve += new ResolveEventHandler(OnReflectionOnlyAssemblyResolve);

			if (!TypesInAssemblyDictionary.TryGetValue(assemblyPath, out TypesInAssembly))
			{
				TypesInAssembly = AssemblyWrapper.GetAssemblyTypes(assemblyPath);
				TypesInAssemblyDictionary.Add(assemblyPath, TypesInAssembly);
			}

			if (!DerivedTypeDictionary.TryGetValue(assemblyPath, out DerivedType))
			{
				DerivedType = GetDerivedType(TypesInAssembly, typeof(DriverType));
				DerivedTypeDictionary.Add(assemblyPath, DerivedType);
			}

			//Driver = (DriverType)Activator.CreateInstance(DerivedType, DriverConstructorArgs);
			DriverWrapper<DriverType> Wrapper = (DriverWrapper<DriverType>)Domain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(DriverWrapper<DriverType>).FullName);
			Driver = Wrapper.CreateInstance(DerivedType, DriverConstructorArgs);

		}
		#endregion
		#region Dispose
		public void Dispose()
		{            
			if (Driver != null)
			{
				try
				{
					AppDomain.Unload(Domain);
				}
				catch (Exception)
				{
				}
				finally
				{
					Driver = default(DriverType);
				}

			}
            base.Dispose();
		}
		#endregion

		#region private GetDerivedType
		private Type GetDerivedType(Type[] assemblyTypes, Type pluginType)
		{
			bool isPlugin = false;
			foreach (Type TypeInAssembly in assemblyTypes)
			{
				if (TypeInAssembly.IsClass && TypeInAssembly.IsPublic && !TypeInAssembly.IsAbstract)
				{
					Type baseClass = TypeInAssembly;
					do
					{
						isPlugin = pluginType.Equals(baseClass);
						baseClass = baseClass.BaseType;
					}
					while (baseClass != null && !isPlugin);

					if (isPlugin)
					{
						return TypeInAssembly;
					}
				}
			}
			return default(Type);
		}
		#endregion
		#region private CreateInstance
		private DriverType CreateInstance(Type type, object[] DriverConstructorArgs)
		{
			return (DriverType)Activator.CreateInstance(type, DriverConstructorArgs);
		}
		#endregion

		#region OnReflectionOnlyAssemblyResolve
		public static Assembly OnReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
		{
			return System.Reflection.Assembly.ReflectionOnlyLoad(args.Name);
		}
		#endregion
		#region MarshalByRefObjectDisposable overrides
		public override object InitializeLifetimeService()
		{
			return null;
		}
		#endregion
	}
	#region OldVersion
	/*
	[Serializable]
	public class DriverWrapper<DriverType> : MarshalByRefObjectDisposable
	{
		public DriverType Driver = default(DriverType);

		private AppDomain Domain;

		#region Constructor
		public DriverWrapper()
		{
		}
		public DriverWrapper(string DriverPath, object[] DriverConstructorArgs)
		{
			string assemblyPath = DriverPath;
			if (!System.IO.Path.IsPathRooted(assemblyPath))
				assemblyPath = AppDomain.CurrentDomain.BaseDirectory + assemblyPath;
			assemblyPath = Path.GetFullPath(assemblyPath);

			string AppPath = Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location));
			string DllPath = Path.GetDirectoryName(assemblyPath);
			if (DllPath.IndexOf(AppPath) == -1)
			{

				string dllDirectoryName = Path.Combine(AppPath, "Cache");
				DirectoryInfo di = new DirectoryInfo(dllDirectoryName);
				if (!di.Exists)
				{
					di.Create();
					di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
				}

				string newAssemblyPath = dllDirectoryName + "\\" + Path.GetFileName(assemblyPath);
				try
				{
					File.Copy(assemblyPath, newAssemblyPath, true);
				}
				catch { }
				assemblyPath = newAssemblyPath;
				DllPath = Path.GetDirectoryName(assemblyPath);
			}
			
			AppDomainSetup ads = new AppDomainSetup();
			ads.ApplicationName = "DriverWrapper";
			ads.ApplicationBase = AppPath;
			ads.PrivateBinPath = new Uri(AppPath + "\\").MakeRelativeUri(new Uri(DllPath)).ToString();
			ads.ShadowCopyFiles = "true";

			Domain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(DriverPath), null, ads);
			//Domain.ReflectionOnlyAssemblyResolve += new ResolveEventHandler(OnReflectionOnlyAssemblyResolve);
			//Domain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);

            DriverWrapper<DriverType> Wrapper = null;
            Wrapper = (DriverWrapper<DriverType>)Domain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(DriverWrapper<DriverType>).FullName);
            Driver = Wrapper.CreateInstance(DriverPath, DriverConstructorArgs);
		}
		#endregion
		#region Dispose
		public void Dispose()
		{
			if (Driver != null)
			{
				try
				{
					AppDomain.Unload(Domain);
				}
				catch (Exception ex)
				{
				}
				finally
				{
					Driver = default(DriverType);
				}

			}
		}
		#endregion

		#region private CreateInstance
		private DriverType CreateInstance(string DriverPath, object[] DriverConstructorArgs)
		{
			try
			{
				Type[] assemblyTypes = AssemblyWrapper.GetAssemblyTypes(DriverPath);

				bool isPlugin = false;
				foreach (Type TypeInAssembly in assemblyTypes)
				{
					if (TypeInAssembly.IsClass && TypeInAssembly.IsPublic && !TypeInAssembly.IsAbstract)
					{
						Type baseClass = TypeInAssembly;
						do
						{
							isPlugin = typeof(DriverType).Equals(baseClass);
							baseClass = baseClass.BaseType;
						}
						while (baseClass != null && !isPlugin);

						//jesli jest to typ <DriverType> to dodanie tego typu listy
						if (isPlugin)
						{
							return (DriverType)Activator.CreateInstance(TypeInAssembly, DriverConstructorArgs);
						}
					}
				}
			}
			catch (Exception ex)
			{
			}
			return default(DriverType);
		}
		#endregion

		#region OnUnhandledException
		public static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
		{
		}
		#endregion
		#region OnReflectionOnlyAssemblyResolve
		public static Assembly OnReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
		{
			return System.Reflection.Assembly.ReflectionOnlyLoad(args.Name);
		}
		#endregion

		#region MarshalByRefObjectDisposable overrides
		public override object InitializeLifetimeService()
		{
			return null;
		}
		#endregion
	}
*/
#endregion
}
