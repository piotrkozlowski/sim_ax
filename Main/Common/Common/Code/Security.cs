﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
    #region class Role
    public class Role
    {
        public uint RoleID { get; set; }
        public List<Activity> roleActivity { get; set; }

        public Role()
        {
            roleActivity = new List<Activity>();
        }

        public Role(uint RoleID)
        {
            roleActivity = new List<Activity>();
            this.RoleID = RoleID;
        }

        public void addActivity(Activity a)
        {
            roleActivity.Add(a);
        }
    }
    #endregion

    #region class Activity
    public class Activity
    {
        public uint ActivityID;
        public bool Deny;

        public Activity()
        {
        }

        public Activity(uint ActivityID, bool Deny)
        {
            this.ActivityID = ActivityID;
            this.Deny = Deny;
        }

        #region SQL query
        /*
        select 'public const int ' + 
        upper (a.[name]) + 
        space (45 - len (a.[name])) +
        ' = ' + cast (a.id_activity as varchar) + ';' +
        space (10 - len (a.id_activity)) +  
        '// '  + isnull(d.description, '<description>')
        from activity a left outer join descr d on a.id_descr = d.id_descr and id_language = 2
        order by a.id_module, a.name 
        */
        #endregion

        #region Zanim coś zmienisz koniecznie przeczytaj!!!
        // Legenda do nazewnictwa.
        // Przy dodawaniu nowych Activity proszę się trzymać reguł.
        // Activities są wspólne dla wszystkich aplikacji: WebIMR, SIMA, PrepaidManager, ServiceCentre i innych
        // LIST - wyświetlenie listy okreslonych obiektów oraz wyświetlenie szczegółów danego obiektu (bez możliwośći edycji) inaczej widok okreslonych elementow np. widok listy lokalizacji, widok szczegółowy lokalizacji
        // ADD - dodawania okreslonego obiektu np. dodawanie lokalizacji
        // EDIT - edycja określonego obiektu np. edycja lokalizacji (LOCATION_EDIT); możne być tez szczegółowo np. edycja uprawnień operatora OPERATOR_PERMISSIONS_EDIT;
        // DEL - usuwanie okreslonego obiektu np. usuwanie lokalizacji
        // OP (skrót od OPERATION) - uprawnienia do wykonywania okreslonej operacji np. uruchamianie akcji (ACTION_RUN_OP), możliwość ręcznego dodwania danych MEASUREMENTS_MANUAL_ADD_OP
        // ALLOWED - dopuszczalne elementy dla określonego obiektu, przeważnie skojarzone po ID_REFERENCE_TYPE np. dopuszczalne raporty (ALLOWED_REPORT), dopuszczalne typy akcji (ALLOWED_ACTION_TYPE)
        // Przykład 1:
        // Uprawnienia do uruchamiania akcji (GetData, PutData, ChangeCID); wyświetlania listy akcji.
        // Należy wykonfigurować następujące uprawnienia (Activity) w odpowiedniej roli (ROLE_ACTIVITIY) lub dla konkretnego operatora (OPERATOR_ACTIVITY)
        // ACTIVITY              |   ID_REFERENCE_TYPE       | REFERENCE_VALUE
        // -----------------------------------------------------------------------------
        // ACTION_RUN_OP         |   NULL                    | NULL   
        // ALLOWED_ACTION_TYPE   |   23                      | GetData
        // ALLOWED_ACTION_TYPE   |   23                      | PutData
        // ALLOWED_ACTION_TYPE   |   23                      | ChangeCID
        // ACTION_LIST           |   NULL                    | NULL
        // 
        // Przykład 2:
        // Uprawnienia do uruchamiania wszystkich akcji z wyjątkiem ChangeCID bez mozliwości przegladania
        // ACTIVITY              |   ID_REFERENCE_TYPE       | REFERENCE_VALUE   | DENY
        // -----------------------------------------------------------------------------
        // ACTION_RUN_OP         |   NULL                    | NULL              | false
        // ALLOWED_ACTION_TYPE   |   23                      | NULL              | false
        // ALLOWED_ACTION_TYPE   |   23                      | ChangeCID         | true 
        #endregion

        public const int LOGIN = 1;
        public const int DEVICE_LIST = 2;
        public const int DEVICE_DEL = 3;
        public const int DEVICE_EDIT = 4;
        public const int DEVICE_ADD = 5;

        public const int OPERATOR_LIST = 6;
        public const int OPERATOR_DEL = 7;
        public const int OPERATOR_ADD = 8;
        public const int OPERATOR_EDIT = 9;
        public const int OPERATOR_PERMISSIONS_EDIT = 10;
        //public const int INFORMATION = 10; // ???
		public const int OPERATOR_CAN_CHOOSE_CONFIGURATION = 11;
		public const int OPERATOR_CAN_CHANGE_CONFIGURATION = 12;
        public const int OPERATOR_EXPORT_OP = 13;
        public const int OPERATOR_IMPORT_OP = 14;
        public const int OPERATOR_PERMISSIONS_ROLE_CHOOSE = 15;
        public const int OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE = 16;
        public const int OPERATOR_PERMISSIONS_EFFECTIVE_PERMISSIONS = 17;
        public const int OPERATOR_LIST_ALL = 18;
        public const int OPERATOR_MANAGE_SOT = 19;
        public const int OPERATOR_IMRSC_SOT = 20;

        //2350-2399
        public const int OPERATOR_IMR_SERVER_EDIT = 2350;
        public const int OPERATOR_SHIPPING_LIST_NOTIFICATIONS_EDIT = 2351;
        public const int OPERATOR_CAN_ENABLE_SRT = 2352;
        public const int OPERATOR_RADIO_DEVICE_TESTER_NEW_EDIT = 2353;
        public const int OPERATOR_IMRSC_ISU = 2354;
        public const int OPERATOR_NOTIFICATIONS_LIST = 2355;
        public const int OPERATOR_PERFORMANCE_YELLOW_THRESHOLD_LIST = 2356;
        public const int OPERATOR_PACKAGE_NOTIFICATIONS_EDIT = 2357;
        public const int OPERATOR_GSM_API = 2358;
        public const int OPERATOR_WCF_BILLING = 2359;
        public const int OPERATOR_FITTER_NEWS_EDIT = 2360;
        public const int OPERATOR_FITTER_DEPOSITORY_EDIT = 2361;
        public const int OPERATOR_DEFAULT_PARAMETERS_EDIT = 2362;
        public const int OPERATOR_CAN_ADD_CUSTOM_ORGANIZATIONAL_UNIT = 2363;
        public const int OPERATOR_ADVANCED_DEFAULT_CONFIGURATION_OP = 2364;
        public const int OPERATOR_SMS_NOTIFICATIONS_LIST = 2365;
        public const int OPERATOR_ALL_ACTIVITIES_AVAILABLE_TO_ASSIGN = 2366;
        public const int ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES = 2367;
        public const int OPERATOR_SEND_NOTIFICATION_OP = 2368;
        public const int OPERATOR_ORANGE_PAYMENT = 2369;
        public const int OPERATOR_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 2370;
        public const int OPERATOR_ALLOW_SEND_PREPAID_ACTION = 2371;
        public const int OPERATOR_FORCE_QUEUE = 2372;
        public const int OPERATOR_DEFINE_CONSUMPTION_PROFILE = 2373;
        public const int OPERATOR_FP_PORTAL_ROLE_ADMIN = 2374;
        public const int OPERATOR_CAN_TOP_UP_ACCOUNT = 2375;
        public const int OPERATOR_LDAP_SYNCHRONIZATION_ACTION_TYPE = 2376;

        public const int METER_LIST = 21;
        public const int METER_ADD = 22;
        public const int METER_EDIT = 23;
        public const int METER_DEL = 24;
        public const int METER_ADVANCED_READING_CONFIG_OP = 25;
        public const int METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 26;
        public const int METER_CLUSTER_ASSIGNMENT_OP = 27;

        public const int LOCATION_LIST = 31;
        public const int LOCATION_ADD = 32;
        public const int LOCATION_DEL = 33;
        public const int LOCATION_EDIT = 34;
        public const int LOCATION_INSTALLATION_FORM_OP = 35;
        public const int LOCATION_RELATED_EDIT = 36;
        public const int LOCATION_ADD_FROM_FILE_SGM = 37;
        public const int LOCATION_PAREMETER_LIST = 38;
        public const int LOCATION_WITH_DEVICE_ALLOW_REMOVE = 39;

        public const int SETTINGS_LIST = 40;
        public const int ALLOWED_DISTRIBUTOR = 41; // do tegu numeru sa stare uprawnienia, nie bedziemy juz ich psuć
        public const int ALLOWED_ACTION_TYPE = 42;
        public const int ALLOWED_ROLE = 43;
        public const int ALLOWED_REPORT = 44;
        public const int ALLOWED_TASK_TYPE = 45;
        public const int ALLOWED_IMR_SERVER = 46;
        public const int ALLOWED_ACTIVITES = 47;
        public const int ALLOWED_LOCATION_STATE_TYPE = 48;
        public const int ALLOWED_LOCATION = 49;
        public const int SGM_OPERATOR_PERMISSIONS_FOR_MULTIPLE_ROLE_CHOOSE = 50;
        public const int SELECT_RDG_FROM_THE_LIST = 51;
        public const int ALLOWED_ISSUE_STATUS_EDIT = 52;
        public const int ALLOWED_ISSUE_TYPE = 53;

        public const int AREA_ADD = 54;
        public const int AREA_EDIT = 55;
        public const int AREA_DEL = 56;
        public const int AREA_LIST  = 57;
        public const int WAREHOUSE_EDIT = 58;
        public const int AREA_CLUSTER_EDIT = 59;
        public const int AREA_LOCATIONS_EDIT = 60;
        public const int AREA_SUPPLIER_HISTORY_VIEW = 61;
        public const int AREA_TARIFF_HISTORY_VIEW = 62;
        public const int AREA_INSTALLED_AUX_DEVICES_VIEW = 63;
        public const int ALLOWED_LOCATION_GROUP_LIST = 64;
        public const int ALLOWED_LOCATION_GROUP_EDIT = 65;
        public const int ALLOWED_DATA_TYPE = 66;
        public const int ALLOWED_EXISTING_ITEM_PARAM_EDIT = 67;
        public const int ALLOWED_HOST_NAME = 68;
        public const int SETTINGS_LAST_MEASURE_COLOR = 69;
        public const int ALLOWED_LOCATION_TYPE = 70;
        public const int TOOLS_LIST = 71;
        public const int ALLOWED_METER_TYPE = 72;

        //aż do 99

        //100 - 149
        public const int ROLE_LIST = 100;
        public const int ROLE_ADD = 101;
        public const int ROLE_EDIT = 102;
        public const int ROLE_DEL = 103;
        public const int ROLE_PERMISSIONS_EDIT = 104;

        //150 - 199
        public const int DATA_TYPE_LIST = 150;
        public const int DATA_TYPE_ADD = 151;
        public const int DATA_TYPE_EDIT = 152;
        public const int DATA_TYPE_DEL = 153;
        public const int DATA_TYPE_EXPORT_OP = 154;
        public const int DATA_TYPE_IMPORT_OP = 155;
        public const int DATA_FORMAT_GROUP_LIST = 156;
        public const int DATA_FORMAT_GROUP_ADD = 157;
        public const int DATA_FORMAT_GROUP_EDIT = 158;
        public const int DATA_FORMAT_GROUP_DEL = 159;

        //200 - 249
        public const int DATA_TYPE_GROUP_LIST = 200;
        public const int DATA_TYPE_GROUP_ADD = 201;
        public const int DATA_TYPE_GROUP_EDIT = 202;
        public const int DATA_TYPE_GROUP_DEL = 203;

        //250 - 299
        public const int RESOURCE_LIST = 250;
        public const int RESOURCE_ADD = 251;
        public const int RESOURCE_EDIT = 252;
        public const int RESOURCE_DEL = 253;
        public const int RESOURCE_EXPORT_OP = 254;
        public const int RESOURCE_IMPORT_OP = 255;

        //300 - 349
        public const int SIM_CARD_LIST = 300;
        public const int SIM_CARD_ADD = 301;
        public const int SIM_CARD_EDIT = 302;
        public const int SIM_CARD_DEL = 303;
		public const int SIM_CARD_IMPORT_OP = 304;
		public const int SIM_CARD_EXPORT_OP = 305;
        public const int SIM_CARD_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 306;

        //350 - 399
        public const int MEASUREMENTS_LIST = 350;
        public const int MEASUREMENTS_VALIDATE_OP = 351;
        public const int MEASUREMENTS_INVALIDATE_OP = 352;
        public const int MEASUREMENTS_MANUAL_ADD_OP = 353;
		public const int MEASUREMENTS_ONLINE_OP = 354;
        public const int MEASUREMENTS_IMPORT_REFUEL_OP = 355; //import pliku tankowania (np. shell w SC)
        public const int MEASUREMENTS_LIST_PREPAY = 356;
        public const int MEASUREMENTS_REPORTS = 357;
        public const int MEASUREMENT_EDIT_HIDDEN_UI_OBJECT_PROPERTIES = 358;

        //400 - 449
        public const int REPORTS_LIST = 400;
        public const int REPORT_CONFIGURE_OP = 401;
        public const int REPORT_PACKETS_LIST = 402;
        public const int REPORT_DEFINITION_LIST = 403;
        public const int REPORT_DEFINITION_ADD = 404;
        public const int REPORT_DEFINITION_EDIT = 405;
        public const int REPORT_DEFINITION_DEL = 406;
        public const int REPORT_EDIT = 407;
        public const int REPORT_DEL = 408;
        public const int REPORT_DETAILS_HIDDEN_UI_PROPERTIES = 409;
        public const int REPORT_ADD = 410;

        //450 - 499
        public const int ACTION_LIST = 450;
        public const int ACTION_RUN = 451;
        public const int ACTION_STOP = 452;
        public const int ACTION_CONFIGURE_OP = 453;
        public const int ACTION_TYPES_CONFIGURE_OP= 454;
        public const int ACTION_DEFS_CONFIGURE_OP = 455;
        public const int ACTION_STOP_WITH_ERROR = 456;
        public const int PROFILE_SENDER_LIST = 457;
        public const int ACTION_DEFS_IMPORT_OP = 458;
        public const int ACTION_DEFS_EXPORT_OP = 459;
        public const int ACTION_DEF_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 460;
        public const int ACTION_DEF_DETAILS_READONLY_UI_OBJECT_PROPERTIES = 461;
        public const int ACTION_DEFS_COPY = 462;
        public const int ALLOWED_ACTION_DEF = 463;
        public const int ACTION_DETAILS_HIDDEN_UI_PROPERTIES = 464;

        //500 - 549
        public const int DEVICE_HIERARCHY_LIST = 500;
        public const int DEVICE_HIERARCHY_EDIT = 501;
        public const int DEVICE_IMPORT_OP = 502;
        public const int DEVICE_EXPORT_OP = 503;
        public const int PACKET_LIST = 504; //uprawnienie do ogladania pakietów (odebranych/wyslanych)
		public const int DEVICE_ONSITE_OP = 505;
        public const int DEVICE_ADD_FROM_SERVICE_POOL = 506;
        public const int DEVICE_TYPES_REPORT_OP = 507;
        public const int DEVICE_INSTALLATION_PLAN_REPORT_OP = 508;
        public const int DEVICE_DEPOSITORY_REPORT_VIEW = 509;
        public const int DEVICE_DEPOSITORY_REPORT_VIEW_ADMIN = 510;
        public const int DEVICE_INSPECTION = 511;
        public const int DEVICE_ADD_SET = 512;
		public const int DEVICE_PAREMETERS_LIST = 513;
        public const int DEVICE_ADVANCED_HISTORY_LIST = 514;
        public const int DEVICE_DASHBOARD_VIEW = 515;
        public const int DEVICE_DASHBOARD_ADVANCED_VIEW = 516;
        public const int DEVICE_POSSIBLE_SIM_CARD_LIST = 517;
        public const int DEVICE_SIM_CARD_REPLACE_OP = 518;
        public const int DEVICE_CONFIGURATION_LIST = 519;
        public const int DEVICE_FIRMWARE_LIST = 520;
        public const int DEVICE_PROFILE_LIST = 521;
        public const int DEVICE_SET_LIST = 522;
        public const int DEVICE_SEND_TO_SERVER_OP = 523;
        public const int DEVICE_IMPORT_FROM_FILE_SGM = 524;
        public const int DEVICE_DASHBOARD_MAIN_CHART_VIEW = 525;
        public const int DEVICE_DASHBOARD_ADDITIONAL_CHART_VIEW = 526;
        public const int DEVICE_DIAGNOSTIC_DATA_VIEW = 527;
        public const int DEVICE_BULK_EDIT_HIDDEN_UI_OBJECT_PROPERTIES = 528;
        public const int DEVICE_DETAILS_READONLY_UI_OBJECT_PROPERTIES = 529;
        public const int DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 530;
        public const int PACKET_FREQ_REASON_LIST = 531;
        public const int ACCESS_TO_CRYPTOGRAPHIC_KEYS = 532;
        public const int DEVICE_CONNECTION_LIST = 533;
        public const int DEVICE_REWAREHOUSING_OP = 534;

        //550 - 599
        public const int ALARM_LIST = 550;
		public const int ALARM_ADD = 551;
		public const int ALARM_EDIT = 552;
		public const int ALARM_DEL = 553;
		public const int ALARM_CONFIGURE_OP = 554;
		public const int CONFIGURATION_PROFILE_MAP = 555;
        public const int CONFIGURATION_PROFILE = 556;
        public const int TEMPLATE_CONFIRM = 557;
        public const int ALARM_OPERATOR_EDIT = 558;
        public const int ALARM_DETAILS_HIDDEN_UI_PROPERTIES = 559;

        //600- 649 actor (osoba, np. jako kontakt do lokalizacji lub osoba zglaszajaca problem (issue)). 
        //Informacje podstawowe dla Operatora (nie istnieje operator bez aktora)
        public const int ACTOR_LIST = 600;
        public const int ACTOR_ADD = 601;
        public const int ACTOR_EDIT = 602;
        public const int ACTOR_DEL = 603;
        public const int ACTOR_MEMBERSHIP_EDIT = 604;
        public const int ACTOR_IMPORT_OP = 605;
        public const int ACTOR_DETAILS_HIDDEN_UI_PROPERTIES = 606;

        //650 - 699
        public const int ISSUE_LIST = 650;
        public const int ISSUE_ADD = 651;
        public const int ISSUE_EDIT = 652;
        public const int ISSUE_DEL = 653;
        public const int ISSUE_JCI_OP = 654;
        public const int ISSUE_ADD_FROM_FILE_OP = 655;
        public const int ISSUE_DISPLAY_IN_TEXT_OP = 656;
        public const int ISSUE_EQUIPMENT_OP = 657;//zgłoszenie dla urządzeń/meterów
        public const int ISSUE_ATTRIBUTES_EDIT = 658;
        public const int ISSUE_ATTRIBUTES_LIST = 659;
        public const int ISSUE_ATTRIBUTES_DEL = 660;
        public const int ISSUE_GENERATE_REPORT_OP = 661;
        public const int ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 662;
        public const int ALLOWED_ISSUE_STATUS_VIEW = 663;
        public const int ALLOWED_ISSUE_STATUS_CHANGE = 664;
        public const int ISSUE_ATTACHMENT_DELETE = 665;

        //700 - 749
        public const int TASK_LIST = 700;
        public const int TASK_ADD = 701;
        public const int TASK_EDIT = 702;
        public const int TASK_DEL = 703;
        public const int TASK_CLIENT_PAYMENT_LIST = 704;
        public const int TASK_CLIENT_PAYMENT_EDIT = 705;
        public const int TASK_FITTER_PAYMENT_LIST = 706;
        public const int TASK_FITTER_PAYMENT_EDIT = 707;
        public const int TASK_FITTER_FEEDBACK_EDIT = 708;
        public const int TASK_INSTALLATION_PROTOCOL_VIEW = 709;
        public const int TASK_WAREHOUSE_ORDER_VIEW = 710;
        public const int TASK_WAREHOUSE_ORDER_EDIT = 711;
        public const int TASK_ATTRIBUTES_EDIT = 712;
        public const int TASK_ATTRIBUTES_LIST = 713;
        public const int TASK_ATTRIBUTES_DEL = 714;
        public const int ALLOWED_SUPPLIER = 715;
        public const int TASK_ARRANGED_FITTER_FISIT_DATE_VIEW = 716;
        public const int TASK_REFUEL_AUDIT_MOVIE_VIEW = 717;
        public const int ALLOWED_TASK_ATTACHMENT_EXTENSION = 718;
        public const int TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 719;
        public const int ALLOWED_TASK_STATUS_VIEW = 720;
        public const int ALLOWED_TASK_STATUS_EDIT = 721;
        public const int ALLOWED_TASK_STATUS_CHANGE = 722;
        public const int TASK_DETAILS_READONLY_UI_OBJECT_PROPERTIES = 723;
        public const int TASK_ATTACHMENTS_EDIT = 724;
        public const int TASK_MULTIPLE_ADD = 725;
        public const int TASK_EXPORT = 726;
        public const int TASKS_MODULE_HIDDEN_UI_OBJECT_PROPERTIES = 727;
        public const int ALLOWED_TASK_GROUP = 728;
        public const int TASK_ATTACHMENT_DELETE = 729;
        public const int TASK_EXPORT_OP = 730;

        //750 - 799
        public const int ROUTE_LIST = 750;
        public const int ROUTE_ADD = 751;
        public const int ROUTE_EDIT = 752;
        public const int ROUTE_DEL = 753;
        public const int ROUTE_TMQ_EXPORT_OP = 754; //uprawnienie do generowania pliku xls w formacie TMQ
        public const int ROUTE_EXPORT_OP = 754; //uprawnienie do exportu trasy
        public const int ROUTE_POINT_DEVICES_EDIT = 756;
        public const int WALKED_BY_433_READOUTS_SGM_SUPPORT = 757;
        public const int ROUTE_DETAILS_HIDDEN_UI_PROPERTIES = 758;
        public const int ROUTE_IMPORT_OP = 759;

        //ROUTE_DEF 790 - 799
        public const int ROUTE_DEF_LIST = 790;
        public const int ROUTE_DEF_ADD = 791;
        public const int ROUTE_DEF_EDIT = 792;
        public const int ROUTE_DEF_DEL = 793;

        //800 - 849
        public const int LOCATION_IMPORT_OP = 800;
        public const int LOCATION_EXPORT_OP = 801;
		public const int LOCATION_MESH_OP = 802;
        public const int LOCATION_COLLECTOR_OP = 803; //uprawnienie do aplikacji pobierajacej informacje o lokalizacjach z serwerow klienckich (SC)
        public const int LOCATION_METER_ASSOCIATION_OP = 804;   //powiazanie lokalizacji z meterem pomijajac urzadzenie
        public const int LOCATION_DEVICE_ASSOCIATION_OP = 805;   //powiazanie lokalizacji z urządzeniem - aktywowanie przycisku Change dla Device set w SGM
        public const int LOCATION_STATE_CHANGE = 806;   //zmiana stanu lokalizacji - aktywowanie przycisku Change dla Location state w SGM
        public const int DISPLAY_CID_AS_MPRN = 807;
        public const int GROUPING_LOCATION_TYPE_LIST = 808;
        public const int GROUPING_LOCATION_TYPE_EDIT = 809;
        public const int LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 810;
        public const int LOCATION_WIZARD = 811;
        public const int LOCATION_INSTALLATION_DETAILS_READONLY_UI_OBJECT_PROPERTIES = 812;

        //850 - 899
        public const int DEVICE_TYPE_LIST = 850;
        public const int DEVICE_TYPE_ADD = 851;
        public const int DEVICE_TYPE_EDIT = 852;
        public const int DEVICE_TYPE_DEL = 853;
		public const int DEVICE_TYPE_GROUP_LIST = 854;
        public const int ALLOWED_DEVICE_TYPE_SETTINGS_EDIT = 855;
        public const int ALLOWED_DEVICE_TYPE_ALARMS_VIEW = 856;

        //900 - 924
        public const int SYSTEM_DRIVER_LIST = 900; //zawiera uprawnienie do DEVICE_DRIVER, TRASMISSION_DRIVER, PROTOCOL_DRIVER
        public const int SYSTEM_DRIVER_ADD = 901;
        public const int SYSTEM_DRIVER_EDIT = 902;
        public const int SYSTEM_DRIVER_DEL = 903;
        public const int SYSTEM_DRIVER_UPLOAD = 904;

        //925 - 949
        public const int SYSTEM_ROUTING_LIST = 925; //zawiera uprawnienie do ROUTE_TABLE, CONNECTION
        public const int SYSTEM_ROUTING_ADD = 926;
        public const int SYSTEM_ROUTING_EDIT = 927;
        public const int SYSTEM_ROUTING_DEL = 928;

        //950 - 999
        public const int METER_IMPORT_OP = 950;
        public const int METER_EXPORT_OP = 951;
        public const int METER_TYPE_LIST = 952;
        public const int METER_TYPE_GROUP_LIST = 953;
        public const int METER_TYPE_CLASS_LIST = 954;
        public const int METER_TYPE_DEVICETYPE_LIST = 955;
        public const int METER_TYPE_EDIT = 956;
        public const int METER_TYPE_GROUP_EDIT = 957;
        public const int METER_TYPE_CLASS_EDIT = 958;
        public const int METER_TYPE_DEVICETYPE_EDIT = 959;
        public const int METER_PARAMETER_LIST = 960;
        public const int METER_TYPE_GROUP_EXPORT_OP = 961;
        public const int METER_TYPE_GROUP_IMPORT_OP = 962;
        public const int METER_DECLARE_EXPECTED_CONSUMPTION = 963;

        //1000 - 1049 tarrifs
        public const int TARIFF_LIST = 1000;
        public const int TARIFF_ADD = 1001;
        public const int TARIFF_EDIT = 1002;
        public const int TARIFF_DEL = 1003;
        public const int LOCATION_TARIFF_VIEW = 1004;
        public const int LOCATION_TARIFF_EDIT = 1005;
        public const int LOCATION_TARIFF_HISTORY_VIEW = 1006;
        
        //1050 - 1099 consumers
        public const int CONSUMER_LIST = 1050;
        public const int CONSUMER_ADD = 1051;
        public const int CONSUMER_EDIT = 1052;
        public const int CONSUMER_DELETE = 1053;

        public const int CONSUMER_TYPE_LIST = 1060;
        public const int CONSUMER_TYPE_ADD = 1061;
        public const int CONSUMER_TYPE_EDIT = 1062;
        public const int CONSUMER_TYPE_DEL = 1063;

        public const int CONSUMER_FINANCIAL_STATE_VIEW = 1064;
        public const int CONSUMER_TRANSACTION_HISTORY_VIEW = 1065;
        public const int CONSUMER_CONTACT_HISTORY_VIEW = 1066;
        public const int CONSUMER_LOCATION_HISTORY_VIEW = 1067;
        public const int CONSUMER_SMS_ALERTS_VIEW = 1068;
        public const int CONSUMER_NOTIFICATIONS_VIEW = 1069;
        public const int CONSUMER_OPERATORS_VIEW = 1070;
        public const int CONSUMER_PAYMENT_MODULE_VIEW = 1071;
        public const int CONSUMER_NOTIFICATION_HISTORY_VIEW = 1072;
        public const int CONSUMER_CREDIT_LEVEL_VIEW = 1073;

        //1100 - 1149 consumer transactions
        public const int CONSUMER_TRANSACTION_LIST = 1100;
        public const int CONSUMER_TRANSACTION_ADD = 1101;
        public const int CONSUMER_TRANSACTION_ROLLBACK = 1102;
        public const int BILLING_DOCUMENTS_PANEL = 1103;

        //1150 - 1199 suppliers
        public const int SUPPLIER_LIST = 1150;
        public const int SUPPLIER_ADD = 1151;
        public const int SUPPLIER_EDIT = 1152;
        public const int SUPPLIER_DELETE = 1153;
         
        //1200 - 1249 product codes
        public const int PRODUCT_CODE_LIST = 1200;
        public const int PRODUCT_CODE_ADD = 1201;
        public const int PRODUCT_CODE_EDIT = 1202;

        //1250 - 1299 distributor
        public const int DISTRIBUTOR_LIST = 1250;
        public const int DISTRIBUTOR_ADD = 1251;
        public const int DISTRIBUTOR_EDIT = 1252;
        public const int DISTRIBUTOR_DENIED_IN_IMRSC_MANAGE_OPERATORS = 1253;
        public const int DISTRIBUTOR_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES = 1254;
        public const int DISTRIBUTOR_DEL = 1255;

        //1300 - 1349 article (towar, zaciagany z CDN)
        public const int ARTICLE_LIST = 1300;
        public const int ARTICLE_ADD = 1301;
        public const int ARTICLE_EDIT = 1302;
        public const int ARTICLE_DEL = 1303;

        //1350 - 1399 package
        public const int PACKAGE_LIST = 1350;
        public const int PACKAGE_ADD = 1351;
        public const int PACKAGE_EDIT = 1352;
        public const int PACKAGE_DEL = 1353;
        public const int PACKAGE_ORDER = 1354;
        public const int PACKAGE_ORDER_APPROVE = 1355;
        public const int PACKAGE_ORDER_COMPLEMENT = 1356;
        public const int PACKAGE_NEW_ORDER = 1357;
        public const int PACKAGE_ERRORS_REPORT_OP = 1358;
        public const int PACKAGE_MAGAZINE_REPORT_OP = 1359;
        public const int PACKAGE_DEFICIENCIES_REPORT_OP = 1360;
        public const int PACKAGE_FITTER_PACKAGE_APPROVE = 1361;//Akceptacja paczki w imieniu serwisanta

        //1400 - 1499 Delivery, Reconciliation
        public const int DELIVERY_ADVISOR_LIST = 1400;
        public const int DELIVERY_ORDER_LIST = 1401;
        public const int DETAIL_VIEW_LIST = 1402;
        public const int DELIVERY_BATCH_ADD = 1403;
        public const int DELIVERY_BATCH_EDIT = 1404;
        public const int DELIVERY_BATCH_DEL = 1405;
        public const int DELIVERY_BATCH_COMMIT_OP = 1406;
        public const int ALLOWED_DETAIL_VIEW = 1407;
        public const int LOCATION_DISTANCE_CALC_OP = 1408;
        public const int DELIVERY_TRUCK_LIST = 1409;
        public const int DELIVERY_TRUCK_ADD = 1410;
        public const int DELIVERY_TRUCK_EDIT = 1411;
        public const int DELIVERY_TRUCK_DEL = 1412;
        public const int FUEL_TERMINAL_LIST = 1413;
        public const int FUEL_TERMINAL_ADD = 1414;
        public const int FUEL_TERMINAL_EDIT = 1415;
        public const int FUEL_TERMINAL_DEL = 1416;
        public const int TRUCK_DRIVER_LIST = 1417;
        public const int TRUCK_DRIVER_ADD = 1418;
        public const int TRUCK_DRIVER_EDIT = 1419;
        public const int TRUCK_DRIVER_DEL = 1420;
        public const int DELIVERY_INETLLIGENCE_SIMULATE_OP = 1421;
        public const int DELIVERY_INETLLIGENCE_PLAN_OP = 1422;
        public const int DELIVERY_INTELLIGENCE_ROUTE_OP = 1423;
        public const int DELIVERY_INTELLIGENCE_CUSTOM_PREDICTION = 1424;
        public const int LOCATION_HISTORY_VIEW = 1425;
        public const int DELIVERY_INTELLIGENCE_ROUTE_VERIFICATION_OP = 1426;
        public const int DELIVERY_HISTORY_SIMULATE_OP = 1427;
        public const int DELIVERY_RECONCILIATION_LIST = 1428;
        public const int DELIVERY_ALLOWED_DATA_SOURCE_TYPE_VIEW = 1429;
        public const int DELIVERY_RECALCULATION_REFUEL_OP = 1430;
        public const int DELIVERY_RECALCULATION_TANKS_OP = 1431;
        public const int ALARMS_CONFIRMATION_ENABLED = 1432;
        public const int IRC_SIR2_LIST = 1433;
        public const int ANOMALY_DETECTION_LIST = 1434;
        public const int AUTOCALIBRATION_MODULE_LIST = 1435;
        public const int RECONCILIATION_STATUS_CHANGING_ENABLED = 1436;
        public const int STATION_ANALYSIS_PRIORITIZATION_LIST = 1437;
        public const int STATION_ANALYSIS_PRIORITIZATION_ADVANCED_SETTINGS = 1438;

        //1500 - 1549 Template/Pattern
        public const int TEMPLATE_LIST = 1500;
        public const int TEMPLATE_EDIT = 1501;
        public const int TEMPLATE_ADD = 1502;
        public const int TEMPLATE_LIST_ALL = 1503;
        public const int PATTERN_LIST = 1504;
        public const int PATTERN_EDIT = 1505;
        public const int PATTERN_ADD = 1506;
        public const int TEMPLATE_PATTERN_COPY = 1507;

        //1550 - 1559 Equipment pool
        public const int EQUIPMENT_POOL_LIST = 1550;
        public const int EQUIPMENT_POOL_EDIT = 1551;

        //1575 - 1599 System and Module Data
        public const int MODULE_DATA_LIST = 1575;
        public const int MODULE_DATA_EDIT = 1576;
        public const int MODULE_DATA_ADD  = 1577;
        public const int MODULE_DATA_DEL  = 1578;
        public const int SYSTEM_DATA_LIST = 1579;
        public const int SYSTEM_DATA_EDIT = 1580;
        public const int SYSTEM_DATA_ADD  = 1581;
        public const int SYSTEM_DATA_DEL  = 1582;
        public const int MODULE_LIST = 1583;
        public const int MODULE_ADD = 1584;
        public const int MODULE_EDIT = 1585;
        public const int COMMAND_CODE_TYPE_LIST = 1586;
        public const int COMMAND_CODE_TYPE_ADD = 1587;
        public const int COMMAND_CODE_TYPE_EDIT = 1588;
        public const int COMMAND_CODE_TYPE_DEL = 1589;
        public const int DB_AUDIT_LIST = 1590;
        public const int ALLOWED_MODULE = 1591;

        //1600 - 1649 Schedule
        public const int SCHEDULE_LIST = 1600;
        public const int SCHEDULE_ADD = 1601;
        public const int SCHEDULE_EDIT = 1602;
        public const int SCHEDULE_DEL = 1603;
        public const int SCHEDULE_ACTION_LIST = 1604;
        public const int SCHEDULE_ACTION_ADD = 1605;
        public const int SCHEDULE_ACTION_EDIT = 1606;
        public const int SCHEDULE_ACTION_DEL = 1607;

        //1650 - 1699
        public const int PROFILE_LIST = 1650;
        public const int PROFILE_ADD = 1651;
        public const int PROFILE_EDIT = 1652;
        public const int PROFILE_DEL = 1653;
        public const int PROFILE_IMPORT_OP = 1654;
        public const int PROFILE_EXPORT_OP  = 1655;
        public const int PROFILE_ACTION_LIST = 1656;
        public const int PROFILE_ACTION_RUN = 1657;
       
        //1700 - 1749 (SITA)
        public const int SITA_READOUT = 1700;
        public const int SITA_INSTALLATION = 1701;
        public const int SITA_SERVICE = 1702;
        public const int SITA_IMR_SCAN = 1703;
        public const int SITA_SETTINGS = 1704;
        public const int SITA_SERVICE_RESYNCHRONISATION = 1705;
        public const int SITA_SERVICE_REPROGRAMMING = 1706;
        public const int SITA_SERVICE_DOWNLOAD_ARCHIVES = 1707;
        public const int SITA_SERVICE_DOWNLOAD_LOG = 1708;
        public const int SITA_READOUT_METHOD_CONFIG = 1709;
        public const int SITA_FRAMES_PREVIEW = 1710;
        public const int SITA_SERVICE_MULTIHOP = 1711;
        public const int SITA_SERVICE_SEND_CONFIGURATION = 1712;
        public const int SITA_IMR_CLUSTER_SCAN = 1713;
        public const int SITA_IMR_CLUSTER_SCAN_VERIFICATION = 1714;
        public const int SITA_IMR_CLUSTER_SCAN_VERIFICATION_ADVANCED = 1715;
        public const int SITA_SETTINGS_ARANGE = 1716;
        public const int SITA_SETTINGS_RADIO = 1717;
        public const int SITA_SETTINGS_ADV_RADIO = 1718;
        public const int SITA_SERVICE_GMC = 1719;
        public const int SITA_SERVICE_CHANGE_BATTERY_GASMETER = 1720;
        public const int SITA_INSTALLATION_SETTINGS = 1721;
        public const int SITA_CLIENT_SIGNATURE_REQUIRED = 1722;
        public const int SITA_FITTER_SIGNATURE_REQUIRED = 1723;
        public const int SITA_ANALOG_ALARM_EDIT_ENABLE = 1724;
        public const int SITA_SERVICE_READ_DIAGNOSTICS = 1725;
        public const int SITA_SERVICE_SET_GAS_ENERGY = 1726;
        public const int SITA_SERVICE_REPROGRAMMING_BY_OPTOBT = 1727;
        public const int SITA_SERVICE_SIM_CARD_TEST = 1728;
        public const int SITA_SERVICE_VALVE_TEST = 1729;
        public const int SITA_READOUT_BROADCAST_ENABLED = 1730;
        public const int SITA_ROUTE_ALL_LOCATION_ENABLED = 1731;
        public const int SITA_TANK_LOCALIZATION_AVAILABLE = 1732;
        public const int SITA_SERVICE_REPLACE_OKO = 1733;
        public const int SITA_SERVICE_CHECK_DEVICE_STATE = 1734;
        public const int SITA_REPORT_ERROR_ENABLED = 1735;
        public const int SITA_SERVICE_PRESSURE_PREVIEW = 1736;
        public const int SITA_SERVICE_GSM_TEST = 1737;
        public const int SITA_CREATE_NEW_LOCATION = 1738;
        public const int SITA_UNALLOCATED_TASK_VIEW = 1739;
        public const int SITA_LPG_INSTALL_TELEMETRY_DEVICES = 1740;
        public const int SITA_LPG_INSTALL_OTHER_DEVICES = 1741;
        public const int SITA_ADDITIONAL_ACTION_DURING_SYNCHRONIZATION = 1742;
        public const int SITA_INSTALLATION_PROTOCOL_ENABLED = 1743;
        public const int SITA_SERVICE_PROBE_REMOVE = 1744;
        public const int SITA_SERVICE_PROBE_REPLACE = 1745;
        public const int SITA_ASSIGN_TASK_IN_BRANCH_ENABLED = 1746;
        public const int SITA_ASSIGN_ADAPTER_DURING_INSTALLATION_ENABLED = 1747;
        public const int SITA_ASSIGN_ADAPTER_DURING_INSTALLATION_REQUIRED = 1748;
        public const int SITA_SERVICE_FREQUENCY_CHANGE = 1749;        

        //1750 - 1799
        public const int SMS_ACTIONS_LIST = 1750;
        public const int SMS_ACTIONS_VIEW = 1751;
        public const int SMS_ACTION_EDIT = 1752;
        public const int SMS_ACTION_ADD = 1753;
        

        //1800 - 1849 service
        public const int SERVICE_LIST = 1800;
        public const int SERVICE_ADD = 1801;
        public const int SERVICE_EDIT = 1802;
        public const int SERVICE_DEL = 1803;

        //1850 - 1899 contract
        public const int CONTRACT_LIST = 1850;
        public const int CONTRACT_ADD = 1851;
        public const int CONTRACT_EDIT = 1852;
        public const int CONTRACT_DEL = 1853;

        //1900-1949 service package
        public const int SERVICE_PACKAGE_LIST = 1900;
        public const int SERVICE_PACKAGE_ADD = 1901;
        public const int SERVICE_PACKAGE_EDIT = 1902;
        public const int SERVICE_PACKAGE_DEL = 1903;

        //1950-1999 diagnostic action
        public const int DIAGNOSTIC_ACTION_LIST = 1950;
        public const int DIAGNOSTIC_ACTION_ADD = 1951;
        public const int DIAGNOSTIC_ACTION_EDIT = 1952;
        public const int DIAGNOSTIC_ACTION_DEL = 1953;
    
        //2050-2099 service list
        public const int SERVICE_LIST_LIST = 2050;
        public const int SERVICE_LIST_ADD = 2051;
        public const int SERVICE_LIST_EDIT = 2052;
        public const int SERVICE_LIST_DEL = 2053;
        public const int VIEW_THE_CURRENT_STATE_OF_THE_VALVE = 2054;
        public const int CONTROL_VALVE = 2055;
        public const int HISTORY_VALVE_CONTROL = 2056;
        public const int MANAGE_VALVE_ACTIONS_STATE = 2057;
        public const int SERVICE_LIST_DEVICE_TEST = 2058;
        public const int SERVICE_LIST_DEVICE_CUSTOMER_DAMAGE_MARK_OP = 2059;
        public const int SERVICE_LIST_DEVICE_REFURBISMENNT_MARK_OP = 2060;

        //2100-2149 shipping list
        public const int SHIPPING_LIST_LIST = 2100;
        public const int SHIPPING_LIST_ADD = 2101;
        public const int SHIPPING_LIST_EDIT = 2102;
        public const int SHIPPING_LIST_DEL = 2103;
        public const int SHIPPING_LIST_GENERATE_REPORT_OP = 2104;
        public const int SHIPPING_LIST_GENERATE_PROTOCOL_OP = 2105;

        //2150-2199 location hierarchy
        public const int LOCATION_HIERARCHY_LIST = 2150;
        public const int LOCATION_HIERARCHY_ADD = 2151;
        public const int LOCATION_HIERARCHY_EDIT = 2152;
        public const int LOCATION_HIERARCHY_DEL = 2153;

        //2200-2249
        public const int DEVICE_ORDER_NUMBER_LIST = 2200;
        public const int DEVICE_ORDER_NUMBER_ADD = 2201;
        public const int DEVICE_ORDER_NUMBER_EDIT = 2202;
        public const int DEVICE_ORDER_NUMBER_DEL = 2203;

        //2250-2299
        public const int CASHIER_NEW_PAYMENT = 2250;
        public const int CASHIER_TOPUP_HISTORY = 2251;
        public const int CASHIER_ENERGY_SUPPLIER_AND_TARIF = 2252;
        public const int CASHIER_CONSUMER_ACCOUNT = 2253;
        public const int CASHIER_CONSUMER_DETAILS = 2254;
        public const int CASHIER_CONSUMER_COMMENT = 2255;
        public const int CASHIER_METER_READINGS = 2256;
        public const int CASHIER_CHARGES = 2257;
        public const int CASHIER_CREDIT = 2258;
        public const int CASHIER_NEW_PAYMENT_HBC_RAR = 2259;
        public const int CASHIER_OLD_PANEL = 2260;
        public const int CASHIER_GENERATE_TOPUP_DEFAULT_CONFIRMATION = 2261;
        public const int CASHIER_GENERATE_TOPUP_AMANORA_CONFIRMATION = 2262;

        //2300 - 2349

        public const int DEPOSITORY_ELEMENT_LIST = 2300;
        public const int DEPOSITORY_ELEMENT_ADD = 2301;
        public const int DEPOSITORY_ELEMENT_EDIT = 2302;
        public const int DEPOSITORY_ELEMENT_DEL = 2303;

        //2350-2399 - dalszy ciąg Activity dla operatora (wpisy znajdują się pod starszymi wpisami dla Operatora)

        //2400-2449

        public const int PERFORMANCES_LIST = 2400;
        public const int PERFORMANCES_ADD = 2401;
        public const int PERFORMANCES_EDIT = 2402;
        public const int PERFORMANCES_DEL = 2403;

        //2450-2499

        public const int IMR_SERVER_LIST = 2450;
        public const int IMR_SERVER_ADD = 2451;
        public const int IMR_SERVER_EDIT = 2452;
        public const int IMR_SERVER_DEL = 2453;
        
        //2500-2549
        public const int FILE_LIST = 2500;
        public const int FILE_ADD = 2501;
        public const int FILE_EDIT = 2502;
        public const int FILE_DEL = 2503;
        public const int FILE_UNBLOCK = 2504;
        //2550-2599

        public const int FAULT_CODE_LIST = 2550;
        public const int FAULT_CODE_ADD = 2551;
        public const int FAULT_CODE_EDIT = 2552;
        public const int FAULT_CODE_DEL = 2553;
        public const int FAULT_CODE_ASSIGN_OP = 2554;
        public const int FAULT_CODE_DASHBOARD_VIEW = 2555;
        public const int FAULT_CODE_MINOR_VIEW = 2556;
        public const int FAULT_CODE_DASHBOARD_SUMMARY_VIEW = 2557;
        public const int FAULT_CODE_DASHBOARD_BIG_FAULT_VIEW = 2558;

        //2600-2699 fuelprime
        public const int FP_REPORT_ESCALATE_ALLOWED = 2600;
        public const int FP_REPORT_REPORT_ALLOWED = 2601;
        public const int FP_REPORT_CONFIRM_ALLOWED = 2602;

        //2700-2799 Configuration
        public const int CONFIGURATION_CONTROL_HIDDEN_UI_OBJECT_PROPERTIES = 2700;

        //2800-2819 SIMAX
        public const int SIMAX_DASHBOARD_VIEW = 2800;

        //2820-2899 WEBSIMAX
        public const int WEBSIMAX_DASHBOARD_VIEW = 2820;
        public const int WEBSIMAX_ALARM_SUMMARY_VIEW = 2821;
        public const int WEBSIMAX_MAIN_MENU_VIEW = 2822;


        //2900 - 2999 (SITA) - dalszy ciąg Activity dla SITA (wpisy znajdują się pod starszymi wpisami dla SITA)
        public const int SITA_INSTALLATION_COMPASS_VISIBLE = 2900;
        public const int SITA_SHOW_DEVICES_IN_RANGE = 2901;
        public const int SITA_INSTALLATION_BY_WIZARD_ALLOWED = 2902;
        public const int SITA_INSTALLATION_CUSTOM_IMPULSE_WEIGHT = 2903;
        public const int SITA_READOUT_STOPGO_ENABLED = 2904;
        public const int SITA_SCAN_KNOWN_NODES = 2905;
        public const int SITA_LPG_INSTALL_ARANGE = 2906;
        public const int SITA_LPG_ADD_NEW_TANK = 2907;
        public const int SITA_SHOW_EMPTY_AREA = 2908;
        public const int SITA_DIEHL_SCAN = 2909;
        public const int SITA_ARRANGED_FITTER_FISIT_DATE_ENABLE = 2910;
        public const int SITA_ATTACHMENT_GALLERY_ENABLE = 2911;
        public const int SITA_FINISH_TASK_AFTER_ACTION = 2912;
        public const int SITA_CREATE_LOCAL_TASK = 2913;
        public const int SITA_SHOW_CORE_DEVICE_INSTEAD_DEPOSITORY_ELEMENT_DEVICE = 2914;
        public const int SITA_READOUT_DAILY_ARCHIVE_ENABLED = 2915;
        public const int SITA_SERVICE_ACTION_ON_TOP = 2916;
        public const int SITA_SERVICE_PIN_MENU = 2917;
        public const int SITA_SERVICE_PIN_VALVE = 2918;
        public const int SITA_INSTALLATION_OKOI505_LC_DISABLED = 2919;
        public const int SITA_SERVICE_RADIO_QUALITY_TEST = 2920;
        public const int SITA_SERVICE_RADIO_QUALITY_TEST_AFTER_INSTALLATION = 2921;
        public const int SITA_TASK_AND_ROUTE_TAB_VIEW_ALLOWED = 2922;
        public const int SITA_INSTALLATION_PHOTO_REQUIRED = 2923;
        public const int SITA_REJECT_TASK_ENABLED = 2924;
        public const int SITA_INSTALLATION_DEVICE_LOCATION_PLACE = 2925;
        public const int SITA_SKIP_INSTALLATION_FORM = 2926;
        public const int SITA_INSTALLATION_GSM_TEST_RECOMMENDED = 2927;

        //3000-3049
        public const int MOBILE_NETWORK_CODE_LIST = 3000;
        public const int MOBILE_NETWORK_CODE_ADD = 3001;
        public const int MOBILE_NETWORK_CODE_EDIT = 3002;
        public const int MOBILE_NETWORK_CODE_DEL = 3003;

        //3050-3099 VERSION
        public const int VERSION_LIST = 3050;
        public const int VERSION_ADD = 3051;
        public const int VERSION_EDIT = 3052;
        public const int VERSION_DEL = 3053;
        public const int VERSION_CAN_REVERT_VERSION_STATE = 3054;
        public const int ALLOWED_VERSION_STATE_VIEW = 3055;
        public const int ALLOWED_VERSION_STATE_EDIT = 3056;
        public const int ALLOWED_VERSION_STATE_CHANGE = 3057;
        public const int ALLOWED_VERSION_REFERENCE_TYPE = 3058;

        //3100 - 3159 SESSION
        public const int SESSION_LIST = 3100;
        public const int SESSION_DATA_LIST = 3101;
        public const int SESSION_EVENT_LIST = 3102;
        public const int SESSION_EVENT_DATA_LIST = 3103;

        //3160 - 3199 ALARM_EVENT
        public const int ALARM_EVENT_LIST = 3160;
        public const int ALARM_EVENT_ADD = 3161;
        public const int ALARM_EVENT_EDIT = 3162;
        public const int ALARM_EVENT_DEL = 3163;


        #region WebIMR      od 2001 - te uprawnienia Kuba musi dopasować do tych z listy powyżej (usunac albo nadać odpowiednie nazwy)
        public const int GEN_ACTION_VALVE_ABORT_RUNNING = 2021;      // Permission to abort running 'Operation valve' action
        public const int GEN_ACTION_VALVE_HISTORY = 2014;           // Permission to view history of 'Operation valve' action
        public const int GEN_ACTION_VALVE_PERFORM = 2013;           // Permission to perform 'Operation valve' action
        public const int GEN_ACTION_VALVE_REMOVE_SCHEDULED = 2020;      // Permission to remove scheduled 'Operation valve' action
        public const int GEN_ACTION_VALVE_VIEW = 2012;      // Permission to view 'Operation valve' action
        public const int GEN_CHANGE_CID = 2028;      // Permission to execute 'Change CID' action on Device
        public const int GEN_SYNCH_COUNTER = 2031;      // Permission to synchronize impulse counter

        public const int GEN_CONSUMER_TANKS_VIEW = 2022;      // Permission to view tanks at location
        public const int GEN_LOCATIONS_VIEW = 2001;      // Permission to view main map with locations
        public const int GEN_GMAP_VIEW = 2030;      // Permission to view Google map with locations

        public const int GEN_STATION_MEASURES_VIEW = 2008;      // Permission to view station measures
        public const int GEN_STATIONS_REPORT_VIEW = 2003;      // Permission to view stations report
        public const int GEN_METERS_MANAGE = 2029;      // Permission to manage meters

        public const int LOC_VIEW_ON_MAP = 3001;      // Permission to view location on map

        // ->ALARM_LIST
        public const int GEN_ALARMS_VIEW = 2023;      // Permission to view alarms
        // ->ALARM_MANAGE
        public const int GEN_ALARMS_MANAGE = 2032;  //Permission to manage alarms

        public const int GEN_USER_CONFIRM = 2033;
        public const int GEN_DEMOKIT_ORDER = 2034;
        public const int GEN_MANAGE_ACCOUNT = 2035;
        public const int GEN_CHANGE_PASSWORD = 2036;
        public const int GEN_MANAGE_CONTENT = 2037;
        public const int GEN_PREVIEW_ORDERS = 2038;
        public const int GEN_MY_DEVICES = 2039;
        public const int GEN_MY_ORDERS = 2040;
        public const int GEN_LIVE_DEMO = 2041;
        public const int GEN_VISUALIZATION = 2042;
        public const int GEN_REPORT_ISSUE = 2043;
        public const int GEN_ISSUE_STATUS = 2044;
        // ->MEASUREMENTS_LIST
        public const int GEN_ARCHIVE_DATA_CHART_VIEW = 2027;      // Permission to view archive data chart
        // ->MEASUREMENTS_LIST
        public const int GEN_ARCHIVE_DATA_REPORT_VIEW = 2002;      // Permission to view archive data report
        
        // ->MEASUREMENTS_LIST
        public const int GEN_LATEST_DATA_REPORT_VIEW = 2011;      // Permission to view latest data report

        // ->LOCATION_ADD
        public const int GEN_LOCATIONS_CREATE = 2016;      // Permission to create locations
        // ???????????????
        public const int GEN_LOCATIONS_MANAGE = 2015;      // Permission to manage locations
        // ->LOCATION_EDIT
        public const int GEN_LOCATIONS_MODIFY = 2017;      // Permission to modify locations
        // ->LOCATION_DEL
        public const int GEN_LOCATIONS_REMOVE = 2018;      // Permission to remove locations
        // -> REPORTS_LIST
        public const int GEN_REPORTS_MANAGE = 2019;
        
        // ->MEASUREMENTS_MANUAL_ADD_OP
        public const int GEN_MEASURES_SEND = 2009;      // Permission to enter and send measures

        // -> InteliGAS data types
        public const int GEN_LOCATIONS_MODIFY_ATTRIBUTES = 2045;
        public const int GEN_LOCATIONS_MODIFY_ADDITIONAL_ATTRIBUTES = 2046;
        public const int GEN_CARDS_MANAGEMENT = 2047;
        public const int GEN_CONFIGURATION_VIEW = 2048;
        public const int GEN_SYMBOLS_SIZE_CONFIGURE = 2049;
        public const int GEN_LOCATION_ATTRIBUTES_MANAGEMENT = 3003;
        public const int GEN_LOCATION_ATTRIBUTE_CODE_MANAGEMENT = 3004;
        public const int GEN_MAP_VIEW = 3005;
        public const int GEN_IMR_INSTALLATIONS_VIEW = 3006;
        public const int GEN_SETS_VIEW = 3007;
        public const int GEN_EMPTY_LOCATIONS_VIEW = 3008;
        public const int GEN_TREE_VIEW = 3009;
        public const int GEN_COMMUNICATION = 3010;
        public const int GEN_MEASURE_SYMBOLS_VIEW = 3011;
        public const int GEN_MEASURES_REPORT = 3012;
        public const int GEN_VIEW_LAST_MEASURE = 3013;
        public const int GEN_CHANNEL_REPORT_VIEW = 3014;
        public const int GEN_INTELIGAS_LOCATIONS_VIEW = 3015;
        //public const int LOC_SEND_MEASURES = 3002;      // Permission to enter and send location measures

        //public const int GEN_PASSWORD_CHANGE = 2010;      // Permission to change operator password
        //public const int GEN_MANUAL_DOWNLOAD = 2006;      // Permission to download application manual
        //public const int GEN_MAINTENANCE_CONTACT_VIEW = 2005;      // Permission to view maintenance contact
        //public const int GEN_APPLICATION_INFO_VIEW = 2004;      // Permission to view application info
        //public const int GEN_REFUELS_IMPORT = 2026;      // Permission to import files with refuels
        //public const int GEN_REFUELS_MANAGE = 2025;      // Permission to manage refuels
        //public const int GEN_REFUELS_VIEW = 2024;      // Permission to view refuels
       
        //public const int GEN_VERSION_INFO_VIEW = 2007;      // Permission to view application version info

        #endregion

    }
    #endregion
}
