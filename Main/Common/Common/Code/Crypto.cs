﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace IMR.Suite.Common
{
    [Obsolete("Caution! This is not safe, should not be used for sensitive data.", false)]
	public class RSA
	{
        #region Const
        private const string PublicKey = "<RSAKeyValue><Modulus>3rUCOagVKP7WcKMr1/DDWJsxJomWHOFoab0cxaMf0PmN6Ck+2/Flg5Fnf77ZZyM26tpR+JMsmgDlc91I3xyXeW1gwwGfhKT5KqvFI8HdnXFYcpcTPx/ind0Gv696Jm2CSgsbHjxCq4H3aH9W8q5cc7hOuHGzmcmwCKaH28jT1sU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        private const string PrivateKey = "<RSAKeyValue><Modulus>3rUCOagVKP7WcKMr1/DDWJsxJomWHOFoab0cxaMf0PmN6Ck+2/Flg5Fnf77ZZyM26tpR+JMsmgDlc91I3xyXeW1gwwGfhKT5KqvFI8HdnXFYcpcTPx/ind0Gv696Jm2CSgsbHjxCq4H3aH9W8q5cc7hOuHGzmcmwCKaH28jT1sU=</Modulus><Exponent>AQAB</Exponent><P>9Y1Fa/Y0jlT28vqbUvZzJl80W8lXxd7CC+QSXh1+D17eTU3CNyodRw4t839yrvnQLr2StrY9lIfHeJn0LXafaQ==</P><Q>6C7lSPKhOdm372ho6mUGeQ1NnfrppoFc8XORf/3/BFgVH4OQMDxFO4ZYTITJFIC4+93fo4CVztN3revAwZhs/Q==</Q><DP>UCl9d2BaCfk84Vfk6zGfp/A+tJRHfkZZjO44/Nad98CdzBjNhZrPEgpUAAEkXHEbgJbM1a8q7VliSkHgUBNAuQ==</DP><DQ>c93bPDEgth7pRIcFPFuYBFbYMgtiCF6sRC4ZIRde6QsP138vOHMLVa4waFcnhZzLM53AmfZ0TBeJtPheY/4t/Q==</DQ><InverseQ>DXSlKJfmjdBY/PEFrXdyapxhQZTts8+C3u7SKKNaNSAI+yv1clQASemLAuIWLVVJSR/M1I1woAK5eFLAE2v+bA==</InverseQ><D>MzlrE5vnkhBbfnjpgresFttLSNPopSfrfjazQXotvUFJNJcYEDrL4KE3LxjUpHZjEU26APTJE3sVS7sgIcFPzX5Km4U5l6rNH//IA/vUeyyzmhp0qsH5R/lnK3zSyx0OOhQaYTnKmTe0pNlvywbng62oCH0qg+/RnK9Gz2zTaME=</D></RSAKeyValue>";
        #endregion


		#region static Encrypt
		public static string Encrypt(string strToEncript)
		{
			if (string.IsNullOrEmpty(strToEncript))
				return "";

			using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
			{
				RSACryptoServiceProvider.UseMachineKeyStore = true;
				provider.FromXmlString(PublicKey);

				byte[] bytes = provider.Encrypt(UnicodeEncoding.UTF8.GetBytes(strToEncript), false);
				return Convert.ToBase64String(bytes);
			}
		}
		#endregion

        #region static EncryptByteArray
        public static byte[] EncryptByteArray(string strToEncript)
        {
            if (string.IsNullOrEmpty(strToEncript))
                return null;

            using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
            {
                RSACryptoServiceProvider.UseMachineKeyStore = true;
                provider.FromXmlString(PublicKey);

                return provider.Encrypt(UnicodeEncoding.UTF8.GetBytes(strToEncript), false);
            }
        }
        public static byte[] EncryptByteArray(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return null;

            using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
            {
                RSACryptoServiceProvider.UseMachineKeyStore = true;
                provider.FromXmlString(PublicKey);

                return provider.Encrypt(bytes, false);
            }
        }
        #endregion

		#region static Decrypt
		public static string Decrypt(string strToDecript)
		{
			if (string.IsNullOrEmpty(strToDecript))
				return "";

			using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
			{
				RSACryptoServiceProvider.UseMachineKeyStore = true;
				provider.FromXmlString(PrivateKey);

				byte[] bytes = provider.Decrypt(Convert.FromBase64String(strToDecript), false);
				return System.Text.UnicodeEncoding.UTF8.GetString(bytes);
			}
		}
		#endregion
        #region DecryptByteArray
        public static byte[] DecryptByteArray(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return null;

            using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider())
            {
                RSACryptoServiceProvider.UseMachineKeyStore = true;
                provider.FromXmlString(PrivateKey);

                return provider.Decrypt(bytes, false);
            }
        }
        #endregion
	}
}
