﻿using System;
using System.Collections.Concurrent;
#if !Android
using System.Drawing;
#endif
using System.Globalization;
using System.Net;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;

namespace IMR.Suite.Common
{
    public static partial class Extensions
    {
        #region DateTime.TodayOrNext(DayOfWeek)
        public static DateTime TodayOrNext(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime startDate = date;
            while (startDate.DayOfWeek != dayOfWeek)
                startDate = startDate.AddDays(1.0);

            return startDate;
        }
        #endregion
        #region DateTime.Next(dayOfWeek)
        public static DateTime Next(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime startDate = date.AddDays(1.0);
            while (startDate.DayOfWeek != dayOfWeek)
                startDate = startDate.AddDays(1.0);

            return startDate;
        }
        #endregion
        #region DateTime.TodayOrLast(DayOfWeek)
        public static DateTime TodayOrLast(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime startDate = date;
            while (startDate.DayOfWeek != dayOfWeek)
                startDate = startDate.AddDays(-1.0);

            return startDate;
        }
        #endregion
        #region DateTime.Last(DayOfWeek)
        public static DateTime Last(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime startDate = date.AddDays(-1.0);
            while (startDate.DayOfWeek != dayOfWeek)
                startDate = startDate.AddDays(-1.0);

            return startDate;
        }
        #endregion
        #region DateTime.ToLocalTime(timeZoneId, localTimeOffset)
#if !WindowsCE
        public static DateTime ToLocalTime(this DateTime dateTime, int localTimeOffset)
        {
            return dateTime.ToLocalTime(null, localTimeOffset);
        }
        public static DateTime ToLocalTime(this DateTime dateTime, string timeZoneId)
        {
            return dateTime.ToLocalTime(timeZoneId, 0);
        }
        public static DateTime ToLocalTime(this DateTime dateTime, string timeZoneId, int localTimeOffset)
        {
            if (dateTime.Kind == DateTimeKind.Local)
                return dateTime;

            if (string.IsNullOrEmpty(timeZoneId))
            {
                return new DateTime(dateTime.Ticks, DateTimeKind.Local).AddMinutes(localTimeOffset);
            }
            else
            {
                try
                {
                    TimeZoneInfo localTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                    if (localTimeZoneInfo == TimeZoneInfo.Utc)
                        return new DateTime(dateTime.Ticks, DateTimeKind.Local);
                    DateTime result = TimeZoneInfo.ConvertTimeFromUtc(dateTime, localTimeZoneInfo);
                    return new DateTime(result.Ticks, DateTimeKind.Local);
                }
                catch
                {
                    return new DateTime(dateTime.Ticks, DateTimeKind.Local).AddMinutes(localTimeOffset);
                }
            }
        }
#endif
        #endregion
        #region DateTime.GetWeekOfMonth()
#if !WindowsCE
        public static int GetWeekOfMonth(this DateTime date)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

            while (date.Date.AddDays(1).DayOfWeek != System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                date = date.AddDays(1);

            return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;
        }
#endif
        #endregion
        #region DateTime.GetFirstDayOfWeek(weekNumber)
#if !WindowsCE
        public static DateTime GetFirstDayOfWeek(this DateTime date, int weekNumber)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

            int weekCount = 0;
            while (weekCount < weekNumber)
            {
                if (beginningOfMonth.DayOfWeek == System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                {
                    weekCount++;
                    if (weekCount == weekNumber)
                        break;
                }
                beginningOfMonth = beginningOfMonth.AddDays(1);
            }

            return beginningOfMonth;
        }
#endif
        #endregion
        #region DateTime.GetFirstDayOfWeek(dayInWeek)
#if !WindowsCE
        /// <summary>
        /// Returns the first day of the week that the specified
        /// date is in using the current culture. 
        /// </summary>
        public static DateTime GetFirstDayOfWeek(this DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
            return GetFirstDayOfWeek(dayInWeek, defaultCultureInfo);
        }
        /// <summary>
        /// Returns the first day of the week that the specified date 
        /// is in. 
        /// </summary>
        public static DateTime GetFirstDayOfWeek(this DateTime dayInWeek, CultureInfo cultureInfo)
        {
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }
#endif
        #endregion
        #region GetFirstDayOfQuarter
#if !WindowsCE
        /// <summary>
        /// Returns the first day of the quarter that the specified date is in.
        /// </summary>
        public static DateTime GetFirstDayOfQuarter(this DateTime date)
        {
            int month = date.Month;
            if (month % 3 != 1)
            {
                if (month > 10)
                    month = 10;
                else if (month > 7)
                    month = 7;
                else if (month > 4)
                    month = 4;
                else if (month > 1)
                    month = 1;
            }
            return new DateTime(date.Year, month, 1, 0, 0, 0);
        }
#endif
        #endregion

        #region IPEndPoint.Parse(string)
        public static IPEndPoint Parse(this IPEndPoint ipEndPoint, string ipEndPointString)
        {
            string[] values = ipEndPointString.Split(':');

            if (2 > values.Length)
            {
                throw new FormatException("Invalid endpoint format");
            }

            IPAddress ipAddress;
            string ipAddressString = string.Join(":", values.Take(values.Length - 1).ToArray());
#if !WindowsCE
            if (!IPAddress.TryParse(ipAddressString, out ipAddress))
            {
                throw new FormatException(string.Format("Invalid endpoint ipaddress '{0}'", ipAddressString));
            }
#else
            try
            {
                ipAddress = IPAddress.Parse(values[values.Length - 1]);
            }
            catch (Exception)
            {
                throw new FormatException(string.Format("Invalid endpoint ipaddress '{0}'", ipAddressString));
            }
#endif

            int port;
#if !WindowsCE
            if (!int.TryParse(values[values.Length - 1], out port) ||
                port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort)
            {
                throw new FormatException(string.Format("Invalid end point port '{0}'", values[values.Length - 1]));
            }
#else
            try
            {
                port = int.Parse(values[values.Length - 1]);
                if (port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort)
                    throw new FormatException(string.Format("Invalid end point port '{0}'", values[values.Length - 1]));
            }
            catch (Exception)
            {
                throw new FormatException(string.Format("Invalid end point port '{0}'", values[values.Length - 1]));
            }
#endif

            return new IPEndPoint(ipAddress, port);
        }
        #endregion

        #region Array.ToHexString
        public static string ToHexString(this Array bytes)
        {
            StringBuilder hex = new StringBuilder();
            foreach (byte b in bytes)
                hex.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return hex.ToString().ToUpper();
        }
        #endregion
        #region byte[].ToHexString()
        public static string ToHexString(this byte[] bytes)
        {
            return bytes.ToHexString(true);
        }
        public static string ToHexString(this byte[] bytes, bool withPrefix0x)
        {
            StringBuilder result = new StringBuilder();
            if (withPrefix0x)
                result.Append("0x");
            foreach (byte b in bytes)
                result.Append(string.Format("{0:X2}", b));
            return result.ToString();
        }
        #endregion
        #region byte[].ToBCDString()
        public static string ToBCDString(this byte[] bytes)
        {
            int charNumber = bytes.Length * 2;
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                int left = bytes[i] / 16;
                int right = bytes[i] % 16;

                if (right != 0xF)
                {
                    result.Append(left.ToString());
                    result.Append(right.ToString());
                }
                else
                {
                    result.Append(left.ToString());
                }
            }
            return result.ToString();
        }
        #endregion
        #region byte[].ToASCIIString()
        public static string ToASCIIString(this byte[] bytes)
        {
            byte[] asciiBytes = new byte[bytes[bytes.Length - 1] == 0 ? bytes.Length - 1 : bytes.Length];
            Buffer.BlockCopy(bytes, 0, asciiBytes, 0, asciiBytes.Length);
            return System.Text.Encoding.ASCII.GetString(asciiBytes, 0, asciiBytes.Length);
        }
        #endregion
        #region string.ToBytes()
        public static byte[] ToBytes(this string value)
        {
            string tmp = value.ToLower().Replace("0x", "");
            if (tmp.Length % 2 != 0) tmp += "F";
            byte[] bytes = new byte[tmp.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(tmp.Substring(i * 2, 2), 16);
            }
            return bytes;
        }
        #endregion

        #region Dictionary<TKey, List<TValue>>.Add()
        public static void Add<TKey, TValue>(this Dictionary<TKey, List<TValue>> dict, TKey key, TValue item)
        {
            if (dict.ContainsKey(key))
                dict[key].Add(item);
            else
                dict.Add(key, new List<TValue>() { item });
        }
        public static void Add<TKey, TValue>(this DictionarySync<TKey, List<TValue>> dict, TKey key, TValue item)
        {
            if (dict.ContainsKey(key))
                dict[key].Add(item);
            else
                dict.Add(key, new List<TValue>() { item });
        }
        public static void Add<TKey, TValue>(this CacheDictionary<TKey, List<TValue>> dict, TKey key, TValue item)
        {
            if (dict.ContainsKey(key))
                dict[key].Add(item);
            else
                dict.Add(key, new List<TValue>() { item });
        }
        public static void Add<TKey, TValue>(this ConcurrentDictionary<TKey, List<TValue>> dict, TKey key, TValue item)
        {
            if (dict.ContainsKey(key))
                dict[key].Add(item);
            else
                dict.TryAdd(key, new List<TValue>() { item });
        }
        #endregion
        #region Dictionary<TKey, <TValue>.TryGetValue(TKey key)
        public static TValue TryGetValue<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
        {
            TValue obj;
            if (dict.TryGetValue(key, out obj))
                return obj;

            return default(TValue);
        }
        #endregion

        #region Source.In(params)
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }
        #endregion
        #region StringBuilder.RemoveEnd(length)
        /// <summary>
        /// Removes the specified range of characters from the end of this instance
        /// </summary>
        /// <param name="stringBuilder">StringBuilder instance</param>
        /// <param name="length">The number of characters to remove</param>
        public static void RemoveEnd(this StringBuilder stringBuilder, int length)
        {
            if (stringBuilder != null && stringBuilder.Length >= length)
                stringBuilder.Remove(stringBuilder.Length - length, length);
        }
        #endregion
        #region string[].Cast<T>()
#if !WindowsCE && !Android
        public static T[] Cast<T>(this string[] array) where T : IConvertible
        {
            if (array.Length > 0)
            {
                T[] returnTable = new T[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    returnTable[i] = GenericConverter.Parse<T>(array[i]);
                }
                return returnTable;
            }
            return null;
        }
#endif
        #endregion
        #region string.Parse
        public static object Parse(this string valuesStr, Type type)
        {
            object value = null;
            try
            {
                System.TypeCode typeCode = System.Type.GetTypeCode(type);
                switch (typeCode)
                {
                    case TypeCode.Double:
                        value = Double.Parse(valuesStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                        break;
                    case TypeCode.Single:
                        value = Single.Parse(valuesStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                        break;
                    case TypeCode.Decimal:
                        value = Decimal.Parse(valuesStr.Replace(',', '.'), NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat);
                        break;

                    //Inne case?? być moze
                    default:
                        value = Convert.ChangeType(valuesStr, type, CultureInfo.InvariantCulture);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new FormatException("Parse from 'object' - <ExtensionMethod>", ex);
            }

            return value;
        }
        #endregion 

        #region IEnumerable<TKey>.Partition(partitionSize)
        public static IEnumerable<IEnumerable<TKey>> Partition<TKey>(this IEnumerable<TKey> items, int partitionSize)
        {
            if (partitionSize <= 0)
                throw new ArgumentOutOfRangeException("partitionSize");

            int innerListCounter = 0;
            int numberOfPackets = 0;
            foreach (var item in items)
            {
                innerListCounter++;
                if (innerListCounter == partitionSize)
                {
                    yield return items.Skip(numberOfPackets * partitionSize).Take(partitionSize);
                    innerListCounter = 0;
                    numberOfPackets++;
                }
            }

            if (innerListCounter > 0)
                yield return items.Skip(numberOfPackets * partitionSize);
        }
        #endregion
        #region IEnumerable<TKey>.Split(numberOfChunks)
        public static IEnumerable<IEnumerable<TKey>> Split<TKey>(this ICollection<TKey> items, int numberOfChunks)
        {
            if (numberOfChunks <= 0 || numberOfChunks > items.Count)
                throw new ArgumentOutOfRangeException("numberOfChunks");

            int sizePerPacket = items.Count / numberOfChunks;
            int extra = items.Count % numberOfChunks;

            for (int i = 0; i < numberOfChunks - extra; i++)
                yield return items.Skip(i * sizePerPacket).Take(sizePerPacket);

            int alreadyReturnedCount = (numberOfChunks - extra) * sizePerPacket;
            int toReturnCount = extra == 0 ? 0 : (items.Count - numberOfChunks) / extra + 1;
            for (int i = 0; i < extra; i++)
                yield return items.Skip(alreadyReturnedCount + i * toReturnCount).Take(toReturnCount);
        }
        #endregion

        #region MinBy
        //Na podstawie MoreLinq http://code.google.com/p/morelinq/wiki/OperatorsOverview
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            return source.MinBy<TSource, TKey>(selector, Comparer<TKey>.Default);
        }

        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            using (IEnumerator<TSource> sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence is empty");
                }
                TSource min = sourceIterator.Current;
                TKey minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    TSource candidate = sourceIterator.Current;
                    TKey candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }
        #endregion
        #region MaxBy
        //Na podstawie MoreLinq http://code.google.com/p/morelinq/wiki/OperatorsOverview
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            return source.MaxBy<TSource, TKey>(selector, Comparer<TKey>.Default);
        }
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            if (comparer == null) throw new ArgumentNullException("comparer");
            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence contains no elements");
                }
                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }
        #endregion

        #region SerializationInfo.GetFieldNames
        public static List<string> GetFieldNames(this System.Runtime.Serialization.SerializationInfo info)
        {
            List<string> namesList = new List<string>();
            System.Runtime.Serialization.SerializationInfoEnumerator e = info.GetEnumerator();
            while (e.MoveNext())
            {
                if (String.IsNullOrEmpty(e.Name))
                    break;
                namesList.Add(e.Name);
            }
            return namesList;
        }
        #endregion
        #region SerializationInfo.FieldExists
        public static bool FieldExists(this System.Runtime.Serialization.SerializationInfo info, string fieldName)
        {
            System.Runtime.Serialization.SerializationInfoEnumerator e = info.GetEnumerator();
            while (e.MoveNext())
            {
                if (String.IsNullOrEmpty(e.Name))
                    break;
                if (e.Name.Equals(fieldName))
                    return true;
            }
            return false;
        }
        #endregion
        #region SerializationInfo.TryGetValue
        public static object TryGetValue(this System.Runtime.Serialization.SerializationInfo info, string name, Type type, object defaultValue = null)
        {
            try 
            {
                return info.GetValue(name, type);
            }
            catch
            {

            }
            return defaultValue;
        }
        #endregion 

        #region Enums.Language.GetLanguage(cultureName)
        public static Enums.Language GetLanguage(this Enums.Language language, string cultureName)
        {
            language = Enums.Language.Default;
            switch (cultureName)
            {
                case "pl-PL":
                    language = Enums.Language.Polish;
                    break;
                case "en-GB":
                case "en-US":
                    language = Enums.Language.English;
                    break;
                case "ru-RU":
                    language = Enums.Language.Russian;
                    break;
                case "sr-Latn-RS":
                    language = Enums.Language.Serbian;
                    break;
                case "da-DK":
                    language = Enums.Language.Danish;
                    break;
                case "it-IT":
                    language = Enums.Language.Italian;
                    break;
                case "cs-CZ":
                    language = Enums.Language.Czech;
                    break;
                case "he-IL":
                    language = Enums.Language.Hebrew;
                    break;
            }
            return language;
        }
        #endregion

        #region ChangeColorBrightness
        #if !Android
        /// <summary>
        /// Creates color with corrected brightness.
        /// </summary>
        /// <param name="correctionFactor">The brightness correction factor. Must be between -1 and 1. 
        /// Negative values produce darker colors.</param>
        /// <returns>
        /// Corrected <see cref="Color"/> structure.
        /// </returns>
        public static Color ChangeColorBrightness(this Color color, float correction)
        {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;
            if (correction >= -1 && correction <= 1)
            {
                if (correction < 0)
                {
                    correction = 1 + correction;
                    red *= correction;
                    green *= correction;
                    blue *= correction;
                }
                else
                {
                    red = (255 - red) * correction + red;
                    green = (255 - green) * correction + green;
                    blue = (255 - blue) * correction + blue;
                }
            }
            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }
        #endif
        #endregion

        #region CreateColorImage
        #if !Android
        public static Image CreateColorRectangleImage(this Color color, int width, int height, int border = 0, Color? borderColor = null)
        {
            return CreateColorImage(color, width, height, border, borderColor, ImageShape.Rectangle);
        }
        public static Image CreateColorEllipseImage(this Color color, int width, int height, int border = 0, Color? borderColor = null)
        {
            return CreateColorImage(color, width, height, border, borderColor, ImageShape.Ellipse);
        }
        private enum ImageShape
        {
            Rectangle,
            Ellipse
        }
        private static Image CreateColorImage(Color color, int width, int height, int border, Color? borderColor, ImageShape imageShape)
        {
            Image img = new Bitmap(width, height);
            Graphics drawing = Graphics.FromImage(img);

            if (!borderColor.HasValue)
                borderColor = Color.Transparent;

            if (border < 0)
                border = 0;
            int widthWithBorder = width - (2 * border);
            int heightWithBorder = height - (2 * border);
            if (widthWithBorder < 0)
                widthWithBorder = 0;
            if (heightWithBorder < 0)
                heightWithBorder = 0;
            switch (imageShape)
            {
                case ImageShape.Rectangle:
                    if (border > 0)
                        drawing.FillRectangle(new SolidBrush(borderColor.Value), 0, 0, width, height);
                    drawing.FillRectangle(new SolidBrush(color), border, border, widthWithBorder, heightWithBorder);
                    break;
                case ImageShape.Ellipse:
                    if (border > 0)
                        drawing.FillEllipse(new SolidBrush(borderColor.Value), 0, 0, width, height);
                    drawing.FillEllipse(new SolidBrush(color), border, border, widthWithBorder, heightWithBorder);
                    break;
            }
            drawing.Save();
            drawing.Dispose();
            return img;
        }
#endif
        #endregion

        #region Enum extensions

        /// <summary>
        /// Zwraca ustawione flagi enuma.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetFlags<T>(this T value)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            if (typeof(T).GetCustomAttributes(typeof(FlagsAttribute), false).Length == 0)
            {
                throw new ArgumentException("T must be an enumerated type with [Flags] attribute");
            }

            Enum.GetValues(value.GetType());

            return GetFlags(value, Enum.GetValues(value.GetType()).Cast<T>().ToArray());
        }

        /// <summary>
        /// Zwraca ustawione flagi jako pojedyncze wartości.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetIndividualFlags<T>(this T value)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            return GetFlags(value, GetFlagValues<T>(value.GetType()).ToArray());
        }

        private static IEnumerable<T> GetFlags<T>(T value, T[] values)
        {
            long bits = Convert.ToInt64(value);
            List<T> results = new List<T>();

            for (int i = values.Length - 1; i >= 0; i--)
            {
                long mask = Convert.ToInt64(values[i]);

                if (i == 0 && mask == 0L)
                    break;

                if ((bits & mask) == mask)
                {
                    results.Add(values[i]);
                    bits -= mask;
                }
            }

            if (bits != 0L)
                return Enumerable.Empty<T>();

            if (Convert.ToInt64(value) != 0L)
                return results.Reverse<T>();

            if (bits == Convert.ToInt64(value) && values.Length > 0 && Convert.ToInt64(values[0]) == 0L)
                return values.Take(1);

            return Enumerable.Empty<T>();
        }

        private static IEnumerable<T> GetFlagValues<T>(Type enumType)
        {
            long flag = 0x1;

            foreach (var value in Enum.GetValues(enumType).Cast<T>())
            {
                long bits = Convert.ToInt64(value);

                if (bits == 0L)
                    continue;

                while (flag < bits)
                    flag <<= 1;

                if (flag == bits)
                    yield return value;
            }
        }

        #endregion Enum extensions

        #region Collection extensions

        public static bool IsNotEmpty<T>(this IEnumerable<T> source)
        {
            return source != null
                && source.Any(x => x != null);
        }

        public static bool IsNotEmpty(this ArrayList source)
        {
            return source != null
                && source.Count > 0;
        }

        public static bool IsNotEmpty<TKey, TValue>(this IDictionary<TKey, TValue> source)
        {
            return source != null
                && source.Keys.Any();
        }

        #endregion Collection extensions
    }
}
