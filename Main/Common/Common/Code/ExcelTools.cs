﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Xml;
using System.Xml.Schema;

namespace IMR.Suite.Common
{
    public class ExcelTools
    {
        public enum Version
        {
            /// <summary>
            /// 97-2003
            /// </summary>
            Excel8,
            /// <summary>
            /// 2007
            /// </summary>
            Excel12,
            /// <summary>
            /// 2010
            /// </summary>
            Excel14
        }

        #region LoadSchemaFromFile
        public static string[] LoadSchemaFromFile(string fileName, Version version)
        {
            string[] SheetNames = null;
            OleDbConnection conn = ReturnConnection(fileName, version, true);
            try
            {
                conn.Open();

                DataTable SchemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null });
                if (SchemaTable.Rows.Count > 0)
                {
                    SheetNames = new string[SchemaTable.Rows.Count];
                    int i = 0;
                    foreach (DataRow TmpRow in SchemaTable.Rows)
                    {
                        SheetNames[i] = TmpRow["TABLE_NAME"].ToString();
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return SheetNames;
        }
        #endregion

        #region ReturnConnection
        private static OleDbConnection ReturnConnection(string fileName, Version version, bool firstRowAsColumn)
        {
            if (firstRowAsColumn)
            {
                switch (version)
                {
                    case Version.Excel8:
                        return new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" +
                            "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5;" +
                            "Extended Properties=\"Excel 8.0;\"");
                    case Version.Excel12:
                        return new OleDbConnection(String.Format(
                            @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;""", fileName));
                    case Version.Excel14:
                        return new OleDbConnection(String.Format(
                            @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 14.0;""", fileName));
                }

            }
            else
            {
                switch (version)
                {
                    case Version.Excel8:
                        return new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" +
                            "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5;" +
                            "Extended Properties=\"Excel 8.0;HDR=NO\"");
                    case Version.Excel12:
                        return new OleDbConnection(String.Format(
                            @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;HDR=NO""", fileName));
                    case Version.Excel14:
                        return new OleDbConnection(String.Format(
                            @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 14.0;HDR=NO""", fileName));
                }

            }

            return null;
        }
        #endregion

        #region LoadSpecifiedSheet
        public static DataTable LoadSpecifiedSheet(string fileName, Version version, string sheetName)
        {
            OleDbConnection conn = ReturnConnection(fileName, version, true);
            DataTable SheetData = null;
            try
            {
                conn.Open();
                //retrieve datareader with data for that sheet			
                OleDbDataAdapter SheetAdapter = new OleDbDataAdapter("select * from [" + sheetName + "] where F1 <> NULL", conn);
                SheetData = new DataTable();
                SheetAdapter.Fill(SheetData);
            }
            catch
            {
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return SheetData;
        }
        #endregion
    }

    #region XmlValidator
    public class XmlValidator
    {
        #region Variables
        XmlReaderSettings xmlSettingsWorkOrder;
        XmlReaderSettings xmlSettingsWorkOrderStatusUpdate;
        private static bool isValid = false;
        public StringBuilder InvalidXmlFileReport = new StringBuilder();
        #endregion
        #region Constructor && ValidationEventHandler
        public XmlValidator()
        {
            xmlSettingsWorkOrder = new XmlReaderSettings();
            xmlSettingsWorkOrder.ValidationType = ValidationType.Schema;
            xmlSettingsWorkOrder.Schemas.Add(null, Path.Combine(Environment.CurrentDirectory + "\\JCI\\Mappings", "WODSchema.xsd"));
            xmlSettingsWorkOrder.ValidationEventHandler += new ValidationEventHandler(xmlReaderSettings_ValidationEventHandler);

            xmlSettingsWorkOrderStatusUpdate = new XmlReaderSettings();
            xmlSettingsWorkOrderStatusUpdate.ValidationType = ValidationType.Schema;
            xmlSettingsWorkOrderStatusUpdate.Schemas.Add(null, Path.Combine(Environment.CurrentDirectory + "\\JCI\\Mappings", "WOSUSchema.xsd"));
            xmlSettingsWorkOrderStatusUpdate.ValidationEventHandler += new ValidationEventHandler(xmlReaderSettings_ValidationEventHandler);
        }
        void xmlReaderSettings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            InvalidXmlFileReport.AppendFormat("Validation event:{0}\n", e.Message);
            isValid = false;
        }
        #endregion

        #region ValidateXmlFileScheme
        public bool ValidateXmlFileScheme(Enums.JCI.FILE_TYPE fileType, string filePath)
        {
            XmlReader reader = null;
            try
            {
                InvalidXmlFileReport = new StringBuilder();
                isValid = true;
                switch (fileType)
                {
                    case Enums.JCI.FILE_TYPE.WORK_ORDER:
                        reader = XmlReader.Create(filePath, xmlSettingsWorkOrder);
                        break;
                    case Enums.JCI.FILE_TYPE.WORT_ORDER_STATUS_UPDATE:
                        reader = XmlReader.Create(filePath, xmlSettingsWorkOrderStatusUpdate);
                        break;
                }
                while (reader.Read())
                {
                    //validation
                }
                return isValid;
            }
            catch (Exception GenExp)
            {
                InvalidXmlFileReport.AppendFormat("Exception: {0}\n", GenExp.Message);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
        #endregion
    }
    #endregion
}
