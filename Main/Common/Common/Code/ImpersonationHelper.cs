using System;
using System.Security.Principal;

namespace IMR.Suite.Common
{
    public class ImpersonationHelper : IDisposable
    {
        #region Members
        private readonly WindowsImpersonationContext ImpersonationContext;
        #endregion

        #region Ctor
        public ImpersonationHelper(string username, string domain, string password)
        {
            IntPtr token = IntPtr.Zero;
            IntPtr token_duplicate = IntPtr.Zero;

            if (NativeMethods.RevertToSelf())
            {
                if (NativeMethods.LogonUser(username, domain, password, NativeMethods.LOGON32_LOGON_INTERACTIVE, NativeMethods.LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                {
                    if (NativeMethods.DuplicateToken(token, 2, ref token_duplicate) != 0)
                    {
                        WindowsIdentity temp_windows_identity = new WindowsIdentity(token_duplicate);
                        ImpersonationContext = temp_windows_identity.Impersonate();

                        if (ImpersonationContext != null)
                        {
                            NativeMethods.CloseHandle(token);
                            NativeMethods.CloseHandle(token_duplicate);
                            return;
                        }
                    }
                }
            }

            if (token != IntPtr.Zero)
                NativeMethods.CloseHandle(token);

            if (token_duplicate != IntPtr.Zero)
                NativeMethods.CloseHandle(token_duplicate);

            throw new Exception(string.Format("Unable to impersonate as {0}\\{1}.", username, domain));
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            ImpersonationContext.Undo();
        }
        #endregion
    }

}
