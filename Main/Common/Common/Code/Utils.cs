﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IMR.Suite.Common
{
    /// <summary>
    /// Pomocnicze metody ogólne, których nie można przypisać do innej klasy w Common.
    /// </summary>
    public class Utils
    {
        #region IsProperIpWithPort
        public static bool IsProperIpWithPort(string IPPortToCheck, bool portRequired = false)
        {
            //if (!System.Net.IPAddress.TryParse(IPPortToCheck, out address))

            if (IPPortToCheck == null)
                return false;

            if (IPPortToCheck.Length < 7)
                return false;

            int colonIndex = IPPortToCheck.IndexOf(":");

            string ipWithoutPort = IPPortToCheck;

            if (portRequired && colonIndex < 0)
                return false; // wymagany port a go nie ma

            // podano port
            if (colonIndex >= 0)
            {
                string port = IPPortToCheck.Substring(colonIndex + 1, IPPortToCheck.Length - colonIndex - 1);

                UInt16 parseRes;

                // port moze byc liczba pomiedzy 0 - 65535
                if (!UInt16.TryParse(port, out parseRes))
                    return false;

                ipWithoutPort = IPPortToCheck.Substring(0, colonIndex);
            }

            string ipPattern = @"^([0-9]|[0-9][0-9]|0[0-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[0-9][0-9]|0[0-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";

            Regex regex = new Regex(ipPattern);

            if (!regex.IsMatch(ipWithoutPort))
                return false;

            return true;
        }
        #endregion IsProperIpWithPort
        #region IsProperPhoneNumber
        public static bool IsProperPhoneNumber(string PhoneNumberToCheck)
        {
            return IsProperPhoneNumber(PhoneNumberToCheck, null, null);
        }

        public static bool IsProperPhoneNumber(string PhoneNumberToCheck, uint? MinDigits, uint? MaxDigits)
        {
          
            if (PhoneNumberToCheck == null || PhoneNumberToCheck.Length == 0)
                return false;

            if (MinDigits.HasValue && PhoneNumberToCheck.Length < MinDigits)
                return false;

            if (MaxDigits.HasValue && PhoneNumberToCheck.Length > MaxDigits)
                return false;

            foreach (char c in PhoneNumberToCheck.ToCharArray())
            {
                // jesli znak nie jest cyfra
                if (!Char.IsDigit(c))
                    return false;
            }

            return true;
        }
        #endregion IsProperPhoneNumber
        #region ValueEquals
        public static bool ValueEquals<T>(T firstPrimitive, T secondPrimitive)
        {
            if (firstPrimitive == null && secondPrimitive == null)
                return true;

            if (firstPrimitive == null && secondPrimitive != null)
                return false;

            if (firstPrimitive != null && secondPrimitive == null)
                return false;
            return (bool)(firstPrimitive.GetType().InvokeMember("Equals", System.Reflection.BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstPrimitive.GetType()) }));
        }
        #endregion


    }

    #region EventArgs<T>
    public class EventArgs<T> : EventArgs
    {
        public T Value { get; private set; }
        public EventArgs(T value)
        {
            Value = value;
        }
    }
    #endregion

    #region class DebugExecutionTime
    public class DebugExecutionTime : IDisposable
    {
        List<Tuple<string, DateTime>> debugTimes = new List<Tuple<string, DateTime>>();
        string funcName = "UNKNOWN";
        int maxExecTime = 500;
        Guid guid = Guid.NewGuid();

        #region Ctor
        public DebugExecutionTime(string functionName, int maxExecutionTime)
        {
            this.funcName = functionName;
            this.maxExecTime = maxExecutionTime;
        }
        #endregion
        #region Add
        public void Add(string debugText, params object[] args)
        {
            debugTimes.Add(Tuple.Create<string, DateTime>(string.Format(debugText, args), DateTime.Now));
        }
        #endregion
        #region Dispose
        public void Dispose()
        {
            debugTimes.Add(Tuple.Create<string, DateTime>("END", DateTime.Now));

            long executionTime = (long)debugTimes.Last().Item2.Subtract(debugTimes.First().Item2).TotalMilliseconds;
            if (executionTime > maxExecTime)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.AppendLine();
                sb.AppendLine(string.Format("[{0}]Execution {1} Too LONG !!! --> {2} ms", guid.GetHashCode().ToString("X4"), funcName, executionTime));
                DateTime lastTime = debugTimes.First().Item2;
                foreach (Tuple<string, DateTime> kvp in debugTimes)
                {
                    double milisec = kvp.Item2.Subtract(lastTime).TotalMilliseconds;
                    if (milisec > 0)
                        sb.AppendLine(string.Format("[{0}]{1}:{2} --> {3} ms", guid.GetHashCode().ToString("X4"), funcName, kvp.Item1, milisec));
                    lastTime = kvp.Item2;
                }

                Logger.Add(new LogData(0, LogLevel.Error, sb.ToString()));
            }
        }
        #endregion
    }
    #endregion
    #region class TraceExecutionTime
    public class TraceExecutionTime : IDisposable
    {
        List<Tuple<string, DateTime>> traceTimes = new List<Tuple<string, DateTime>>();

        string entryMessage = "UNKNOWN";
        TimeSpan maxExecutionTime = TimeSpan.FromMilliseconds(500);
        string guid = string.Empty;
        LogData logDataExceeded = null;

        private TimeSpan executionTime = TimeSpan.Zero;
        public TimeSpan ExecutionTime { get { return executionTime; } }

        #region Ctor
        public TraceExecutionTime(string entryMessage, int maxExecutionTime, LogData logDataExceeded) :
            this(entryMessage, string.Empty, maxExecutionTime, logDataExceeded)
        { }
        public TraceExecutionTime(string entryMessage, string guid, int maxExecutionTime, LogData logDataExceeded) :
            this(entryMessage, guid, TimeSpan.FromMilliseconds(maxExecutionTime), logDataExceeded)
        { }
        public TraceExecutionTime(string entryMessage, string guid, TimeSpan maxExecutionTime, LogData logDataExceeded)
        {
            this.logDataExceeded = logDataExceeded;
            this.entryMessage = entryMessage;
            this.guid = guid;
            if (string.IsNullOrEmpty(this.guid))
                this.guid = Guid.NewGuid().GetHashCode().ToString("X4");
            this.maxExecutionTime = maxExecutionTime;

            traceTimes.Add(Tuple.Create<string, DateTime>(entryMessage, DateTime.Now));
        }
        #endregion
        #region Add
        public void Add(string debugText, params object[] args)
        {
            traceTimes.Add(Tuple.Create<string, DateTime>(string.Format(debugText, args), DateTime.Now));
            executionTime = traceTimes.Last().Item2.Subtract(traceTimes.First().Item2);
        }
        #endregion
        #region Dispose
        public void Dispose()
        {
            traceTimes.Add(Tuple.Create<string, DateTime>("END", DateTime.Now));

            executionTime = traceTimes.Last().Item2.Subtract(traceTimes.First().Item2);
            if (executionTime <= maxExecutionTime)
                return;

            if (logDataExceeded != null)
                Logger.Add(logDataExceeded, string.Format("[{0}][All={1:F0}ms]->START: {2}", guid, executionTime.TotalMilliseconds, entryMessage));

            DateTime lastTime = traceTimes.First().Item2;
            foreach (Tuple<string, DateTime> kvp in traceTimes.Take(traceTimes.Count))
            {
                long milisec = (long)kvp.Item2.Subtract(lastTime).TotalMilliseconds;
                if (milisec > 0 && logDataExceeded != null)
                    Logger.Add(logDataExceeded,string.Format("[{0}][{1:F0}ms]->{2}", guid, milisec, kvp.Item1));
                lastTime = kvp.Item2;
            }
        }
        #endregion
    }
    #endregion

}
