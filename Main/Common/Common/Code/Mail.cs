﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace IMR.Suite.Common
{
    public class Mail
    {
        public static bool SendMail(NetworkCredential credentials, MailAddress from, MailAddressCollection to, MailAddressCollection cc, string subject,
            string body, bool isBodyHTML, List<Attachment> attachments, MailPriority priority)
        {
            MailMessage m = new MailMessage();
            m.From = from;
            if (to != null)
            {
                foreach (var item in to)
                {
                    m.To.Add(item);
                }
            }
            if (cc != null)
            {
                foreach (var item in cc)
                {
                    m.CC.Add(item);
                }
            }
            if (attachments != null)
            {
                foreach (var item in attachments)
                {
                    m.Attachments.Add(item);
                }
            }
            m.Subject = subject;
            m.Body = body;
            m.IsBodyHtml = isBodyHTML;
            m.Priority = priority;
            SmtpClient client = new SmtpClient("ryba.aiut.com.pl") { UseDefaultCredentials = false, Credentials = credentials };

            try
            {
                client.Send(m);
                System.Diagnostics.Debug.WriteLine("Send mail - OK");
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Send mail - ERROR:" + ex.Message);
                return false;
            }
        }
    }
}
