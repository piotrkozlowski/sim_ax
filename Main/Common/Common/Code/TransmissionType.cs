﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
    public class TransmissionType
    {
        public int IdTransmissionType;
        public string Name;
        public string Descr;

        public TransmissionType()
        {
        }

        public TransmissionType(int Id)
        {
            IdTransmissionType = Id;
        }

        public override string ToString()
        {
            string str = Name;
            if (Descr != null && Descr != "")
                str += String.Format(" ({})", Descr);
            return str;
        }

        public override bool Equals(object obj)
        {
            return this.IdTransmissionType.Equals((obj as TransmissionType).IdTransmissionType);
        }

        public override int GetHashCode()
        {
            return IdTransmissionType;
        }
    }
}
