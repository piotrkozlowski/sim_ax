﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;

namespace IMR.Suite.Common
{
    #region BitHelper
    public class BitHelper
    {
        #region BytesRemove
        public static void BytesRemove(ref byte[] bytes, int index, int count)
        {
            if (bytes != null && bytes.Length > 0)
            {
                List<byte> tmpBytes = bytes.ToList();
                tmpBytes.RemoveRange(index, count);
                bytes = tmpBytes.ToArray();
            }
        }

        public static byte[] BytesRemove(byte[] bytes, int index, int count)
        {
            if (bytes == null || bytes.Length == 0)
                throw new ArgumentException();

            List<byte> tmpBytes = bytes.ToList();
            tmpBytes.RemoveRange(index, count);
            return tmpBytes.ToArray();
        }
        #endregion

        #region BytesInsert
        public static void BytesInsert(byte[] bytesSrc, ref byte[] bytesDest, int index)
        {
            if (bytesSrc != null && bytesSrc.Length > 0 &&
                bytesDest != null && bytesDest.Length > 0)
            {
                List<byte> bytesTmp = bytesDest.ToList();
                bytesTmp.InsertRange(index, bytesSrc);
                bytesDest = bytesTmp.ToArray();
            }
        }

        public static byte[] BytesInsert(byte[] bytesSrc, byte[] bytesDest, int index)
        {
            if (bytesSrc == null || bytesDest == null)
                throw new ArgumentException();

            List<byte> bytesTmp = bytesDest.ToList();
            bytesTmp.InsertRange(index, bytesSrc);
            return bytesTmp.ToArray();
        }
        #endregion

        #region Byte compare
        static public bool ByteCompare(byte[] b1, byte[] b2)
        {
            if (b1 == null && b2 == null)
                return true;

            if (b1 == null || b2 == null)
                return false;

            if (b1.Length != b2.Length)
                return false;

            for (int a = 0; a < b1.Length; a++)
                if (b1[a] != b2[a])
                    return false;

            return true;
        }
        #endregion

        #region ArrayToStringHex
        static public string ArrayToStringHex(byte[] Table)
        {
            if (Table == null)
                return null;

            StringBuilder sb = new StringBuilder();

            for (int a = 0; a < Table.Length; a++)
                sb.Append(String.Format("{0:X02}", Table[a]));

            return sb.ToString();
        }
        #endregion

        #region GetBytesFromHexString
        static public byte[] GetBytesFromHexString(string hexString)
        {
            if (hexString != null)
            {
                string newString = "";
                char c;
                // remove all none A-F, 0-9, characters
                for (int i = 0; i < hexString.Length; i++)
                {
                    c = hexString[i];
                    newString += c;
                }
                // if odd number of characters, discard last character
                if (newString.Length % 2 != 0)
                {
                    newString = newString.Substring(0, newString.Length - 1);
                }

                int byteLength = newString.Length / 2;
                byte[] bytes = new byte[byteLength];

                for (int i = 0; i < newString.Length / 2; i++)
                    bytes[i] = Convert.ToByte(newString.Substring(i * 2, 2), 16);

                return bytes;
            }
            else
                return null;
        }

        #endregion

        #region HexToInt
        public static byte HexToInt(char c)
        {
            return byte.Parse(c.ToString(), System.Globalization.NumberStyles.HexNumber);
        }
        #endregion

        #region FrameShortHex
        public static ushort FrameShortHex(byte[] Frame, ushort Offset)
        {
            //ushort res;

            int res = FrameByteHex(Frame, Offset) * 256 + FrameByteHex(Frame, (ushort)(Offset + 2));

            return (ushort)res;
        }
        #endregion

        #region FrameByteHex
        public static byte FrameByteHex(byte[] Frame, ushort Offset)
        {
            int tmp = HexToInt((char)Frame[Offset]) * 16 + HexToInt((char)Frame[Offset + 1]);

            return (byte)tmp;
        }
        #endregion

        #region FrameFloat
        public static float FrameFloat(byte[] Frame, int Offset)
        {
            byte[] t = new byte[4];

            t[0] = FrameByteHex(Frame, (ushort)(Offset + 6));
            t[1] = FrameByteHex(Frame, (ushort)(Offset + 4));
            t[2] = FrameByteHex(Frame, (ushort)(Offset + 2));
            t[3] = FrameByteHex(Frame, (ushort)(Offset));

            //przypadek szczegolny - zero
            if (t[0] == 0 && t[1] == 0 && t[2] == 0 && t[3] == 0)
                return 0;

            return BitConverter.ToSingle(t, 0);
        }
        #endregion

        #region StringToByte
        static public byte[] StringToByte(string s)
        {
            byte[] b = new byte[s.Length];

            for (int a = 0; a < s.Length; a++)
                b[a] = (byte)s[a];

            return b;
        }
        #endregion

        #region SetBitInByte
        /// <summary>
        /// Set specific bit in byte, position index is from 1
        /// </summary>
        /// <param name="currentByte"></param>
        /// <param name="bitValue"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        static public byte SetBitInByte(byte currentByte, bool bitValue, int position)
        {
            if (position > 8 || position < 1) return currentByte;

            if (bitValue)
            {
                currentByte |= Convert.ToByte(1 << position - 1);
                return currentByte;
            }
            else
            {
                currentByte &= unchecked((byte)(~(1 << (position-1)))); // 
                //currentByte = Convert.ToByte(currentByte & Convert.ToSByte(~(1 << (position - 1))));
                return currentByte;
            }
        }
        #endregion
        #region GetBitInByte
        /// <summary>
        /// Get specific bit in byte, position index is from 1
        /// </summary>
        /// <param name="currentByte"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        static public bool GetBitInByte(byte currentByte, int position)
        {
            if (position > 8 || position < 1) return false;
            byte positionByte = Convert.ToByte(1 << position - 1);
            byte resultByte = Convert.ToByte(((currentByte & positionByte) >> (position-1)) & 0x01);

            if (resultByte == 0x01)
                return true;
            else
                return false;
        }
        #endregion
    }
    #endregion

    public static class ByteHelper
    {
        public static int GetInt32(this byte[] byteTable, int index, int byteCount)
        {
            if (byteTable == null)
                throw new ArgumentNullException("byteTable");
            if (byteTable.Length == 0)
                throw new ArgumentException("Empty table", "byteTable");
            if (index >= byteTable.Length)
                throw new ArgumentOutOfRangeException("index");
            if (byteCount == 0)
                throw new ArgumentOutOfRangeException("byteCount");
            if (index + byteCount > byteTable.Length)
                throw new ArgumentOutOfRangeException("index; byteCount");

            byte[] bytes = new byte[byteCount];
            Buffer.BlockCopy(byteTable, (int)index, bytes, 0, (int)byteCount);
            return BitConverter.ToInt32(bytes, 0);
        }

        public static short GetInt16(this byte[] byteTable, int index, int byteCount)
        {
            if (byteTable == null)
                throw new ArgumentNullException("byteTable");
            if (byteTable.Length == 0)
                throw new ArgumentException("Empty table", "byteTable");
            if (index >= byteTable.Length)
                throw new ArgumentOutOfRangeException("index");
            if (byteCount == 0)
                throw new ArgumentOutOfRangeException("byteCount");
            if (index + byteCount > byteTable.Length)
                throw new ArgumentOutOfRangeException("index; byteCount");

            byte[] bytes = new byte[byteCount];
            Buffer.BlockCopy(byteTable, (int)index, bytes, 0, (int)byteCount);
            return BitConverter.ToInt16(bytes, 0);
        }
    }
}
