using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace IMR.Suite.Common
{
    #region globals event declaration
    #region UDPServer Events
    public delegate void UDPServerErrorEventHandler(UDPServer sender, UDPSocketExceptionEventArgs e);
    public delegate void UDPServerDataArrivalEventHandler(UDPServer sender, ReceiveUDPSocketDataEventArgs e);
    public delegate void UDPServerStoppedEventHandler(UDPServer sender, EventArgs e);
    public delegate void UDPServerStartedEventHandler(UDPServer sender, EventArgs e);
    public delegate void UDPServerSendCompletedEventHandler(UDPServer sender, EndPointEventArgs e);
    public delegate void UDPServerSendErrorEventHandler(UDPServer sender, EndPointEventArgs e, UDPSocketExceptionEventArgs ex);
    public delegate void UDPServerRemoteHostUnreachableEventHandler(UDPServer sender, EventArgs e);
    #endregion

    #region UDPSocketData Events
    public delegate void UDPSocketDataErrorEventHandler(UDPSocketData sender, UDPSocketExceptionEventArgs e);
    public delegate void UDPSocketDataSendCompletedEventHandler(UDPSocketData sender, EventArgs e);
    #endregion
    #endregion

    #region EventArgs objects
    #region EndPointEventArgs
    public class EndPointEventArgs : EventArgs
    {
        public readonly EndPoint RemoteHostEndPoint;
        public readonly object Tag;
        public EndPointEventArgs(EndPoint remoteHostEndPoint, object Tag)
        {
            this.RemoteHostEndPoint = remoteHostEndPoint;
            this.Tag = Tag;
        }
    }
    #endregion

    #region ReceiveUDPSocketDataEventArgs
    /// <summary>
    /// uzywane przez UDPServerDataArrivalEventHandler
    /// </summary>
    public class ReceiveUDPSocketDataEventArgs : EventArgs
    {
        /// <summary>
        /// dane w byte[]
        /// </summary>
        public readonly byte[] Buffer;
        /// <summary>
        /// czas odebrania danych
        /// </summary>
        public readonly DateTime Time;

        public readonly EndPoint RemoteHostEndPoint;

        public ReceiveUDPSocketDataEventArgs(byte[] buffer, DateTime time, EndPoint remoteHostEndPoint)
        {
            this.Buffer = buffer;
            this.Time = time;
            this.RemoteHostEndPoint = remoteHostEndPoint;
        }
    }
    #endregion

    #region UDPSocketExceptionEventArgs
    /// <summary>
    /// uzywany do wywlolania wyjatku
    /// </summary>
    public class UDPSocketExceptionEventArgs : EventArgs
    {
        public readonly Exception exception;
        public UDPSocketExceptionEventArgs(Exception e)
        {
            exception = e;
        }
    }
    #endregion
    #endregion

    #region UDPSendObject
    public class UDPSendObject
    {
        public readonly byte[] Buffer;
        public readonly EndPoint RemoteEndPoint;
        public readonly object Tag;

        public UDPSendObject(byte[] buffer, EndPoint remoteEndPoint, object Tag)
        {
            this.Buffer = buffer;
            this.RemoteEndPoint = remoteEndPoint;
            this.Tag = Tag;
        }
    }
    #endregion

    #region UDPServer
    public class UDPServer
    {
        #region Events
        public event UDPServerErrorEventHandler UDPServerErrorEvent;
        public event UDPServerDataArrivalEventHandler UDPServerDataArrivalEvent;
        public event UDPServerStoppedEventHandler UDPServerStoppedEvent;
        public event UDPServerStartedEventHandler UDPServerStartedEvent;
        public event UDPServerSendCompletedEventHandler UDPServerSendCompletedEvent;
        public event UDPServerSendErrorEventHandler UDPServerSendErrorEvent;
        public event UDPServerRemoteHostUnreachableEventHandler UDPServerRemoteHostUnreachableEvent;
        #endregion

        #region Consts
        private const int DEFAULT_PORT = 0;//11000;
        private const int DEFAULT_BUFFER_SIZE = 4096;
        #endregion

        #region Members

        #region public int LocalPort
        private int Port;
        public int LocalPort
        {
            get { return Port; }
        }
        #endregion
        #region public string LocalIP
        private string IP;
        public string LocalIP
        {
            get { return IP; }
        }
        #endregion
        private int BufferSize;
        private Socket SRVSocket;
        private System.Collections.Queue SendQueue;

        public bool Started;
        private System.Threading.ManualResetEvent EventMoreDataToSend = new System.Threading.ManualResetEvent(false);
        private System.Threading.AutoResetEvent EventAbort = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent EventUnlockedSendQueue = new System.Threading.AutoResetEvent(true);

        #endregion

        #region Constructors
        private void CommonConstructor()
        {
            this.Started = false;
            this.SendQueue = new System.Collections.Queue();
        }

        public UDPServer()
        {
            this.IP = "";
            this.Port = DEFAULT_PORT;
            this.BufferSize = DEFAULT_BUFFER_SIZE;
            this.CommonConstructor();
        }

        public UDPServer(int Port)
        {
            this.IP = "";
            this.Port = Port;
            this.BufferSize = DEFAULT_BUFFER_SIZE;
            this.CommonConstructor();
        }

        public UDPServer(string IP)
        {
            this.IP = IP;
            this.Port = DEFAULT_PORT;
            this.BufferSize = DEFAULT_BUFFER_SIZE;
            this.CommonConstructor();
        }

        public UDPServer(string IP, int Port)
        {
            this.IP = IP;
            this.Port = Port;
            this.BufferSize = DEFAULT_BUFFER_SIZE;
            this.CommonConstructor();
        }

        public UDPServer(int Port, int BufferSize)
        {
            this.Port = Port;
            this.BufferSize = BufferSize;
            this.CommonConstructor();
        }

        public UDPServer(string IP, int Port, int Buffer_size)
        {
            this.IP = IP;
            this.Port = Port;
            this.BufferSize = Buffer_size;
            this.CommonConstructor();
        }
        #endregion

        #region Methodes
        #region Start
        public void Start()
        {
            try
            {
                //stop server if it was runing
                if (Started)
                    Stop();

                // make local end point
                IPHostEntry localIPHostEntry;

                System.Net.IPAddress ipAddr = null;
                // resolve ip
                if (IP == null || IP == "" || IP == "Any")
                {
                    ipAddr = System.Net.IPAddress.Any;
                }
                else
                {
                    try
                    {
                        // don't resolve if ip is like xxx.yyy.uuu.vvv
                        ipAddr = System.Net.IPAddress.Parse(IP);
                    }
                    catch
                    {
                        try
                        {
                            // resolve
                            localIPHostEntry = System.Net.Dns.GetHostEntry(IP);
                        }
                        catch (Exception e)
                        {
                            UDPServerErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                            return;
                        }
                        foreach (IPAddress ipAddress in localIPHostEntry.AddressList)
                        {
                            if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                            {
                                ipAddr = ipAddress;
                                break;
                            }
                        }
                    }
                }
                // make end point
                System.Net.IPEndPoint ipep = new IPEndPoint(ipAddr, Port);
                EndPoint ep = (EndPoint)ipep;

                // create socket
                SRVSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                Started = true;

                // bind to create server
                SRVSocket.Bind(ep);
                Port = ((IPEndPoint)SRVSocket.LocalEndPoint).Port;// in case of null port given

                SRVSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 1024 * 1024 * 10);

                // clear send queue
                SendQueue.Clear();

                // start thread to send data
                ThreadStart myThreadStart = new ThreadStart(SendThread);
                Thread myThread = new Thread(myThreadStart);
                myThread.Start();

                // start thread to reveive deata
                StartReceiveThread();

                // send UDPServerStartedEvent
                if (UDPServerStartedEvent != null)
                    UDPServerStartedEvent(this, new EventArgs());
            }
            catch (Exception e)
            {
                if (e is SocketException)
                {
                    SocketException se = (SocketException)e;
                    if (se.ErrorCode == 10004) // close methode has been called
                        return;
                }
                // send UDPServerErrorEvent
                if (UDPServerErrorEvent != null)
                    UDPServerErrorEvent(this, new UDPSocketExceptionEventArgs(e));

                // close socket
                Stop(); // send UDPServerStoppedEvent
            }
        }
        #endregion

        #region Stop
        public void Stop()
        {
            try
            {
                EventAbort.Set();
                if (Started)
                {
                    Started = false;
                    SRVSocket.Close();
                    if (UDPServerStoppedEvent != null)
                        UDPServerStoppedEvent(this, new System.EventArgs());
                }
            }
            catch (Exception e)
            {
                if (UDPServerErrorEvent != null)
                    UDPServerErrorEvent(this, new UDPSocketExceptionEventArgs(e));
            }
        }
        #endregion

        #region StartReceiveThread
        private void StartReceiveThread()
        {
            ThreadStart myThreadStart = new ThreadStart(ReceiveThread);
            Thread myThread = new Thread(myThreadStart);
            myThread.Start();
        }
        #endregion

        #region ReceiveThread
        private void ReceiveThread()
        {
            if (!Started)// solve synchronisation troubles (when socket is closed in event_Socket_Server_Remote_Host_Unreachable)
                return;

            int bytes_received_size;
            //Declare IPEndPoint and EndPoint to capture the identity of the sending host
            IPEndPoint sender;
            EndPoint tempRemoteEP;
            // Creates a byte buffer to receive the message.
            byte[] buffer = new byte[BufferSize];
            // Creates an IpEndPoint to capture the identity of the sending host.
            sender = new IPEndPoint(IPAddress.Any, 0);
            tempRemoteEP = (EndPoint)sender;

            // wait until UDPServerDataArrivalEvent is set
            while (UDPServerDataArrivalEvent == null)
            {
                // wait 100 ms 
                if (EventAbort.WaitOne(100, false))
                    return;// if connection has been closed
            }

            SRVSocket.ReceiveTimeout = 30;
            try
            {
                while (true)
                {
                    //Receives datagram from a remote host.  This call blocks.
                    try
                    {
                        bytes_received_size = SRVSocket.ReceiveFrom(buffer, 0, BufferSize, SocketFlags.None, ref tempRemoteEP);
                    }
                    catch (SocketException) //TS: when reaches timeout defined above, prevents locking when waiting for data!
                    {
                        continue;                        
                    }
#if UTC
					DateTime time = DateTime.UtcNow;
#else
                    DateTime time = DateTime.Now;
#endif

                    byte[] sized_buffer = new byte[bytes_received_size];
                    System.Array.Copy(buffer, sized_buffer, bytes_received_size);
                    // send UDPServerDataArrivalEvent
                    if (UDPServerDataArrivalEvent != null)
#if (!DEBUG)
						try// avoid to close server on some programmer's error in the eventhandler function
#endif
                    {
                        UDPServerDataArrivalEvent(this, new ReceiveUDPSocketDataEventArgs(sized_buffer, time, tempRemoteEP));
                    }
#if (!DEBUG)
						catch{}
#endif
                }
            }
            catch (Exception e)
            {
                if (e is SocketException)
                {
                    SocketException se = (SocketException)e;
                    if (se.ErrorCode == 10004) // close methode as been called
                    {
                        return;
                    }
                    if (se.ErrorCode == 10054) // connection reset by peer this error don't close socket
                    {
                        StartReceiveThread(); // restart a receive loop
                        // send UDPServerRemoteHostUnreachableEvent
                        if (UDPServerRemoteHostUnreachableEvent != null)
                            UDPServerRemoteHostUnreachableEvent(this, null);
                        return;
                    }
                }
                // send UDPServerErrorEvent
                if (UDPServerErrorEvent != null)
                    UDPServerErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                // close socket
                Stop(); // send UDPServerStoppedEvent
            }
        }
        #endregion

        #region Send
        /// <summary>
        /// send data to remote host dequeueing send_queue
        /// </summary>
        private void SendThread()
        {
            UDPSendObject sendObject = null;

            try
            {
                byte[] buffer;
                System.Threading.WaitHandle[] WaitHandles = new System.Threading.WaitHandle[2];
                WaitHandles[0] = EventAbort;
                WaitHandles[1] = EventMoreDataToSend;
                int WaitResult;

                for (; ; )
                {
                    WaitResult = System.Threading.WaitHandle.WaitAny(WaitHandles);
                    if (WaitResult == 0)
                        // exit thread
                        return;

                    EventMoreDataToSend.Reset();
                    while (SendQueue.Count > 0) // while there's data to send
                    {
                        // dequeue data of send_queue
                        EventUnlockedSendQueue.WaitOne();
                        sendObject = (UDPSendObject)SendQueue.Dequeue();
                        EventUnlockedSendQueue.Set();
                        buffer = sendObject.Buffer;
                        SRVSocket.SendTo(buffer, 0, buffer.Length, SocketFlags.None, sendObject.RemoteEndPoint);
                        if (UDPServerSendCompletedEvent != null)
                            UDPServerSendCompletedEvent(this, new EndPointEventArgs(sendObject.RemoteEndPoint, sendObject.Tag));
                    }
                }
            }
            catch (Exception e)
            {
                if (UDPServerSendErrorEvent != null)
                    UDPServerSendErrorEvent(this, new EndPointEventArgs(sendObject.RemoteEndPoint, sendObject.Tag), new UDPSocketExceptionEventArgs(e));

                // clear send queue
                EventUnlockedSendQueue.WaitOne();
                SendQueue.Clear();
                EventUnlockedSendQueue.Set();
                // restart thread to send data
                ThreadStart myThreadStart = new ThreadStart(SendThread);
                Thread myThread = new Thread(myThreadStart);
                myThread.Start();
            }
        }

        /// <summary>
        /// used for sending bytes
        /// </summary>
        public void Send(byte[] buffer, EndPoint remoteEndPoint)
        {
            EventUnlockedSendQueue.WaitOne();
            SendQueue.Enqueue(new UDPSendObject(buffer, remoteEndPoint, null));
            EventUnlockedSendQueue.Set();
            EventMoreDataToSend.Set();
        }
        /// <summary>
        /// used for sending bytes with Tag
        /// </summary>
        public void Send(byte[] buffer, EndPoint remoteEndPoint, object Tag)
        {
            EventUnlockedSendQueue.WaitOne();
            SendQueue.Enqueue(new UDPSendObject(buffer, remoteEndPoint, Tag));
            EventUnlockedSendQueue.Set();
            EventMoreDataToSend.Set();
        }
        /// <summary>
        /// used for sending bytes
        /// launch thread for sending or enqueue data if a thread is allready used for sending
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(byte[] buffer, string ip, int port)
        {
            IPEndPoint remoteIPEndPoint;
            System.Net.IPAddress ipAddr;
            // Create endpoint
            try
            {
                // don't resolve if ip is like xxx.yyy.uuu.vvv
                ipAddr = System.Net.IPAddress.Parse(ip);
            }
            catch
            {// else resolve
                IPHostEntry remoteIPHostEntry;
                try
                {
                    remoteIPHostEntry = System.Net.Dns.GetHostEntry(ip);
                }
                catch (Exception e)
                {
                    UDPServerErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                    return;
                }
                ipAddr = remoteIPHostEntry.AddressList[0];
            }
            remoteIPEndPoint = new IPEndPoint(ipAddr, port);
            EndPoint remoteEndPoint = (EndPoint)remoteIPEndPoint;

            Send(buffer, remoteEndPoint);
        }
        /// <summary>
        /// used for sending string
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(string buffer, EndPoint remoteEndPoint)
        {
            Send(Encoding.Default.GetBytes(buffer), remoteEndPoint);
        }

        /// <summary>
        /// used for sending string
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(string buffer, string ip, int port)
        {
            Send(Encoding.Default.GetBytes(buffer), ip, port);
        }
        #endregion

        #endregion

    }
    #endregion

    #region UDPSocketData
    public class UDPSocketData
    {
        private const int TIMEOUT_SEND = 30000;//30s
        #region Events
        public event UDPSocketDataErrorEventHandler UDPSocketDataErrorEvent;
        public event UDPSocketDataSendCompletedEventHandler UDPSocketDataSendCompletedEvent;
        #endregion

        #region Members

        private Socket DataSocket = null;
        private System.Collections.Queue SendQueue;

        private int LocalPort;
        private EndPoint RemoteEndPoint;
        #region public int RemotePort
        public int RemotePort
        {
            get { return ((IPEndPoint)RemoteEndPoint).Port; }
        }
        #endregion
        #region public string RemoteHost
        public string RemoteHost
        {
            get { return ((IPEndPoint)RemoteEndPoint).Address.ToString(); }
        }
        #endregion

        #region AllowBroadcast
        private bool allowBroadcast = false;
        public bool AllowBroadcast
        {
            set
            {
                allowBroadcast = value;
                if (allowBroadcast)
                {
                    try // needs administrative rights
                    {
                        // enable broadcast
                        DataSocket.SetSocketOption(System.Net.Sockets.SocketOptionLevel.Socket, System.Net.Sockets.SocketOptionName.Broadcast, 1);
                    }
#if DEBUG
                    catch (Exception)
                    {
                        //System.Windows.Forms.MessageBox.Show(e.Message,"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
#else
					catch (Exception)
					{
#endif
                    }
                }
                else
                {
                    try // needs administrative rights
                    {
                        // disable broadcast
                        DataSocket.SetSocketOption(System.Net.Sockets.SocketOptionLevel.Socket, System.Net.Sockets.SocketOptionName.Broadcast, 0);
                    }
#if DEBUG
                    catch (Exception)
                    {
                        //System.Windows.Forms.MessageBox.Show(e.Message,"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
#else
					catch (Exception)
					{
#endif
                    }
                }
            }
            get
            {
                return allowBroadcast;
            }
        }
        #endregion

        private bool b_start_send_thread = true;

        private System.Threading.ManualResetEvent EventMoreDataToSend = new System.Threading.ManualResetEvent(false);
        private System.Threading.AutoResetEvent EventAbort = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent EventUnlockedSendQueue = new System.Threading.AutoResetEvent(true);

        #endregion

        #region Constructors

        private void CommonConstructor()
        {
            DataSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            // create new send queue
            SendQueue = new System.Collections.Queue();
        }

        private void CommonConstructor(int localPort)
        {
            CommonConstructor();
            try
            {
                this.LocalPort = localPort;
                IPEndPoint ipEndPoint = new IPEndPoint(System.Net.IPAddress.Any, LocalPort);

                DataSocket.Bind((EndPoint)ipEndPoint);
            }
            catch (Exception e)
            {
                if (UDPSocketDataErrorEvent != null)
                    UDPSocketDataErrorEvent(this, new UDPSocketExceptionEventArgs(e));
            }
        }

        public UDPSocketData(string remoteHost, int remotePort)
        {
            System.Net.IPAddress ipAddr;
            try
            {
                // don't resolve if ip is like xxx.yyy.uuu.vvv
                ipAddr = System.Net.IPAddress.Parse(remoteHost);
            }
            catch
            {
                IPHostEntry remoteIPHostEntry;
                try
                {
                    remoteIPHostEntry = System.Net.Dns.GetHostEntry(remoteHost);
                }
                catch (Exception e)
                {
                    if (UDPSocketDataErrorEvent != null)
                        UDPSocketDataErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                    return;
                }
                ipAddr = remoteIPHostEntry.AddressList[0];
            }
            IPEndPoint remoteIPendPoint = new IPEndPoint(ipAddr, remotePort);
            RemoteEndPoint = (EndPoint)remoteIPendPoint;

            CommonConstructor();
        }

        public UDPSocketData(IPEndPoint remoteIPEndPoint)
        {
            // Create endpoint
            RemoteEndPoint = (EndPoint)remoteIPEndPoint;
            CommonConstructor();
        }

        public UDPSocketData(EndPoint remoteEndPoint)
        {
            RemoteEndPoint = remoteEndPoint;
            CommonConstructor();
        }

        /// <summary>
        /// Warning: Errors are not catch
        /// </summary>
        /// <param name="localPort"></param>
        /// <param name="remoteHost"></param>
        /// <param name="remotePort"></param>
        public UDPSocketData(int localPort, string remoteHost, int remotePort)
        {
            // Create endpoint
            System.Net.IPAddress ipAddr;
            // Create endpoint
            try
            {
                // don't resolve if ip is like xxx.yyy.uuu.vvv
                ipAddr = System.Net.IPAddress.Parse(remoteHost);
            }
            catch
            {
                IPHostEntry remoteIPHostEntry;
                try
                {
                    remoteIPHostEntry = System.Net.Dns.GetHostEntry(remoteHost);
                }
                catch (Exception e)
                {
                    if (UDPSocketDataErrorEvent != null)
                        UDPSocketDataErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                    return;
                }
                ipAddr = remoteIPHostEntry.AddressList[0];
            }
            IPEndPoint remoteIPEndPoint = new IPEndPoint(ipAddr, remotePort);
            this.RemoteEndPoint = (EndPoint)remoteIPEndPoint;

            CommonConstructor(localPort);
        }

        /// <summary>
        /// Warning: Errors are not catch
        /// </summary>
        /// <param name="localPort"></param>
        /// <param name="remoteIPEndPoint"></param>
        public UDPSocketData(int localPort, IPEndPoint remoteIPEndPoint)
        {
            // Create endpoint
            this.RemoteEndPoint = (EndPoint)remoteIPEndPoint;
            CommonConstructor(localPort);
        }
        /// <summary>
        /// Warning: Errors are not catch
        /// </summary>
        /// <param name="localPort"></param>
        /// <param name="remoteEndPoint"></param>
        public UDPSocketData(int localPort, EndPoint remoteEndPoint)
        {
            this.RemoteEndPoint = remoteEndPoint;
            CommonConstructor(localPort);
        }

        #endregion

        #region Methodes

        #region Send
        private void SendThread()
        {
            try
            {
                byte[] buffer;
                System.Threading.WaitHandle[] WaitHandles = new System.Threading.WaitHandle[2];
                WaitHandles[0] = EventAbort;
                WaitHandles[1] = EventMoreDataToSend;
                int WaitResult;

                for (; ; )
                {
                    WaitResult = System.Threading.WaitHandle.WaitAny(WaitHandles, TIMEOUT_SEND, true);
                    switch (WaitResult)
                    {
                        case 0:
                        case System.Threading.WaitHandle.WaitTimeout:
                            this.b_start_send_thread = true;
                            // exit thread
                            return;
                    }
                    EventMoreDataToSend.Reset();
                    while (SendQueue.Count > 0) // while there's data to send
                    {
                        // dequeue data of send_queue
                        EventUnlockedSendQueue.WaitOne();
                        buffer = (byte[])SendQueue.Dequeue();
                        EventUnlockedSendQueue.Set();

                        DataSocket.SendTo(buffer, 0, buffer.Length, System.Net.Sockets.SocketFlags.DontRoute, RemoteEndPoint);

                        if (UDPSocketDataSendCompletedEvent != null)
                            UDPSocketDataSendCompletedEvent(this, new EventArgs());
                    }
                }
            }
            catch (Exception e)
            {
                if (UDPSocketDataErrorEvent != null)
                    UDPSocketDataErrorEvent(this, new UDPSocketExceptionEventArgs(e));
                // clear send queue
                EventUnlockedSendQueue.WaitOne();
                SendQueue.Clear();
                EventUnlockedSendQueue.Set();
                // restart thread to send data
                ThreadStart myThreadStart = new ThreadStart(SendThread);
                Thread myThread = new Thread(myThreadStart);
                myThread.Start();
            }
        }

        /// <summary>
        /// Send bytes
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(byte[] buffer)
        {
            if (this.b_start_send_thread)
            {
                this.b_start_send_thread = false;
                // start thread to send data
                ThreadStart myThreadStart = new ThreadStart(SendThread);
                Thread myThread = new Thread(myThreadStart);
                myThread.Start();
            }
            EventUnlockedSendQueue.WaitOne();
            SendQueue.Enqueue(buffer);
            EventUnlockedSendQueue.Set();
            EventMoreDataToSend.Set();
        }

        /// <summary>
        /// Send string
        /// </summary>
        /// <param name="buffer"></param>
        public void send(string buffer)
        {
            Send(Encoding.Default.GetBytes(buffer));
        }

        #endregion

        #endregion
    }
    #endregion
}