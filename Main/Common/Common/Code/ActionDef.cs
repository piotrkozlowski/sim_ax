﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Common
{
	[Serializable]
	public class ActionDef
	{
		public long Id;
		public SN SerialNbr;
		public long? IdMeter;
		public long? IdLocation;
		public long? IdDataType;
		public Enums.ActionType Type;
		public long IdActionData;
		public object Value;

		#region override ToString()
		public override string ToString()
		{
			return string.Format("ID:{0} [{1}] SN:{2} IdMeter:{3} IdLocation:{4} IdActionData:{5} IdDataType:{6} Value:{7}",
									Id, Type, SerialNbr, IdMeter, IdLocation, IdActionData, IdDataType, Value);
		}
		#endregion

		#region static Members
		[NonSerialized]
		private static List<ActionDef> ActionsDef = new List<ActionDef>();
		#endregion

		#region static FillActionDef
		public static void FillActionDef(List<ActionDef> actionsDef)
		{
			ActionsDef = actionsDef;
		}
		#endregion

		#region static GetActionsDef(SerialNbr)
		public static List<ActionDef> GetActionsDef(SN serialNbr)
		{
			return (from d in ActionsDef where d.SerialNbr == serialNbr select d).ToList<ActionDef>();
		}
		#endregion
		#region static GetActionsDef(ActionType, SerialNbr, DataType)
		public static List<ActionDef> GetActionsDef(Enums.ActionType Type, SN serialNbr, DataType dataType)
		{
			return (from d in GetActionsDef(serialNbr, dataType) where d.Type == Type select d).ToList<ActionDef>();
		}
		#endregion
		#region static GetActionsDef(ActionType, SerialNbr)
		public static List<ActionDef> GetActionsDef(Enums.ActionType Type, SN serialNbr)
		{
			return (from d in ActionsDef where d.Type == Type && d.SerialNbr == serialNbr select d).ToList<ActionDef>();
		}
		#endregion

		#region static GetActionsDef(SerialNbr, DataType)
		public static List<ActionDef> GetActionsDef(SN serialNbr, DataType dataType)
		{
			if (dataType == null)
			{
				return (from d in ActionsDef where d.IdDataType == null && d.SerialNbr == serialNbr select d).ToList<ActionDef>();
			}

			var actionDef_SerialNumber = (from d in ActionsDef
										  where d.IdDataType.HasValue &&
												d.SerialNbr == serialNbr
										  select d);

			var actionDef_AllDataType = (from d in ActionsDef
										 where d.IdDataType.HasValue &&
												d.SerialNbr == null &&
												!actionDef_SerialNumber.Any(e => e.IdDataType == d.IdDataType && e.Type == d.Type)
										 select d);

			var result = actionDef_SerialNumber.Union(actionDef_AllDataType).OrderBy(d => d.Id);;

			if (!dataType.IsAny)
				return (from d in result where d.IdDataType == dataType.IdDataType && d.SerialNbr == serialNbr select d).ToList<ActionDef>();

			return result.ToList<ActionDef>();
		}
		#endregion
	}
}
