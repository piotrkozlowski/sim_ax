﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMR.Suite.Common
{
    public class Schedule
    {
        #region Members

        #region static Empty
        public static readonly Schedule Empty = new Schedule(-1);
        #endregion

        #region Name
        private string name = "";
        public string Name
        {
            get { return name; }
        }
        #endregion
        #region Description
        private string description = "";
        public string Description
        {
            get { return description; }
        }
        #endregion

        #region IdSchedule
        private int idSchedule = 0;

        public int IdSchedule
        {
            get{return idSchedule;}
        }
        #endregion
        #region ScheduleType
        private Enums.ScheduleType scheduleType = Enums.ScheduleType.Unknown;

        public Enums.ScheduleType ScheduleType
        {
            get { return scheduleType; }
            set { scheduleType = value; }
        }
        #endregion
        #region ScheduleInterval
        private int scheduleInterval = 0;

        public int ScheduleInterval
        {
            get { return scheduleInterval; }
            set { scheduleInterval = value; }
        }
        #endregion

        #region SubdayType
        private Enums.ScheduleSubdayType subdayType = Enums.ScheduleSubdayType.Unknown;

        public Enums.ScheduleSubdayType SubdayType
        {
            get { return subdayType; }
            set { subdayType = value; }
        }
        #endregion
        #region SubdayInterval
        private int subdayInterval = 0;

        public int SubdayInterval
        {
            get { return subdayInterval; }
            set { subdayInterval = value; }
        }
        #endregion

        #region RelativeInterval
        private Enums.ScheduleRelativeInterval relativeInterval = Enums.ScheduleRelativeInterval.Unknown;

        public Enums.ScheduleRelativeInterval RelativeInterval
        {
            get { return relativeInterval; }
            set { relativeInterval = value; }
        }
        #endregion

        #region RecurrenceFactor
        private int recurrenceFactor = 0;

        public int RecurrenceFactor
        {
            get { return recurrenceFactor; }
            set { recurrenceFactor = value; }
        }
        #endregion

        #region StartDate
        // wazna jest tylko data, czas nie ma znaczenia
        private DateTime startDate = DateTime.MinValue;

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        #endregion
        #region EndDate
        // wazna jest tylko data, czas nie ma znaczenia
        private DateTime endDate = DateTime.MaxValue;

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        #endregion
        #region StartTime
        // wazny jest tylko czas, data nie ma znaczenia
        private TimeSpan startTime = TimeSpan.Zero;

        public TimeSpan StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        #endregion
        #region EndTime
        // wazny jest tylko czas, data nie ma znaczenia
        private TimeSpan endTime = TimeSpan.MaxValue;

        public TimeSpan EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
        #endregion
        #region TimesInUTC
        private bool timesInUTC = false;

        public bool TimesInUTC
        {
            get { return timesInUTC; }
            set { timesInUTC = value; }
        }
        #endregion

        #region LastRunDate
        // wazna jest tylko data, czas nie ma znaczenia
        private DateTime lastRunDate = DateTime.MinValue;

        public DateTime LastRunDate
        {
            get { return lastRunDate; }
            set { lastRunDate = value; }
        }
        #endregion

        #region IsActive
        private bool isActive = true;

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        #endregion

        #endregion

        #region Constructor

        public Schedule(int IdSchedule, string Name, string Description, Enums.ScheduleType ScheduleType, int ScheduleInterval, Enums.ScheduleSubdayType SubdayType, int SubdayInterval, Enums.ScheduleRelativeInterval RelativeInterval, int RecurrenceFactor, DateTime StartDate, DateTime EndDate, TimeSpan StartTime, TimeSpan EndTime, bool TimesInUTC, DateTime LastRunDate, bool IsActive) 
        {
            this.idSchedule = IdSchedule;
            this.name = Name;
            this.description = Description;
            this.scheduleType = ScheduleType;
            this.scheduleInterval = ScheduleInterval;
            this.subdayType = SubdayType;
            this.subdayInterval = SubdayInterval;
            this.relativeInterval = RelativeInterval;
            this.recurrenceFactor = RecurrenceFactor;
            this.startDate = StartDate;
            this.endDate = EndDate; 
            this.startTime = StartTime;
            this.endTime = EndTime;
            this.timesInUTC = TimesInUTC;
            // schedule jeszcze nie byl wykonany
            if (LastRunDate == DateTime.MinValue)
            {
                this.lastRunDate = StartDate;
            }
            // schedule juz byl wykonany
            else
            {
                this.lastRunDate = LastRunDate;
            }
            this.isActive = IsActive;
        }

        public Schedule(int IdSchedule)
            : this(IdSchedule, "", "", Enums.ScheduleType.Unknown, 0, Enums.ScheduleSubdayType.Unknown, 0, Enums.ScheduleRelativeInterval.Unknown, 0, DateTime.MinValue, DateTime.MinValue, TimeSpan.Zero, TimeSpan.Zero, false, DateTime.MinValue, false)
        {

        }


        public Schedule(int IdSchedule, string Name, string Description, int ScheduleType, int ScheduleInterval, int SubdayType, int SubdayInterval, int RelativeInterval, int RecurrenceFactor, DateTime StartDate, DateTime EndDate, TimeSpan StartTime, TimeSpan EndTime, bool TimesInUTC, DateTime LastRunDate, bool IsActive)
            : this(IdSchedule, Name, Description, Enums.ScheduleType.Unknown, ScheduleInterval, Enums.ScheduleSubdayType.Unknown, SubdayInterval, Enums.ScheduleRelativeInterval.Unknown, RecurrenceFactor, StartDate, EndDate, StartTime, EndTime, TimesInUTC, LastRunDate, IsActive)
        {
            if (Enum.IsDefined(typeof(Enums.ScheduleType), ScheduleType))
            {
                this.scheduleType = (Enums.ScheduleType)ScheduleType;
            }

            if (Enum.IsDefined(typeof(Enums.ScheduleSubdayType), SubdayType))
            {
                this.subdayType = (Enums.ScheduleSubdayType)SubdayType;
            }

            if (Enum.IsDefined(typeof(Enums.ScheduleRelativeInterval), RelativeInterval))
            {
                this.relativeInterval = (Enums.ScheduleRelativeInterval)RelativeInterval;
            }
        }
        #endregion

        #region GetNextRunDateTime
        /// <summary>
        /// Zwraca date i czas kolejnego uruchomienia schedula (w UTC).
        /// </summary>
        /// <param name="CurrentTime">Czas aktualny (UTC)</param>
        /// <returns>Data i czas lub null jesli schedule nie ma byc juz nigdy uruchomiony (lub cos sie nie udalo)</returns>
        private DateTime? GetNextRunDateTime(DateTime? CurrentTime)
        {
            DateTime now = DateTimer.GetUtcNowWithoutMs().DateTime;

            // jesli podano czas aktualny
            if (CurrentTime.HasValue)
                now = CurrentTime.Value;
            if (now > this.endDate)
                return null;

            // pozbywamy sie milisekund
            if (now.Millisecond > 0)
                now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);

            // data kolejnego odpalenia (czas nie ma znaczenia)
            DateTime? dayToRun = GetNextRunDate(now);
            // nie udalo sie pobrac 
            if (!dayToRun.HasValue)
                return null;

            // czas odpalenia w podanym dniu
            DateTime? dateTimeToRun = GetNextRunTime(now, dayToRun.Value);
            // nie udalo sie pobrac 
            if (!dateTimeToRun.HasValue)
                return null;

            // czasy ustawione w bazie jako lokalne
            if (!this.timesInUTC)
            {
                // raczej jest to zbedne, bo w GetNextRunTime juz zamieniam na Universal - ale nie powinno zaszkodzic
                dateTimeToRun = dateTimeToRun.Value.ToUniversalTime();
            }

            // kolejny dzien uruchomienia jest dzis i czas juz minal
            if (dateTimeToRun.Value < now)
            {
                // probujemy dla kolejnego dnia (dla jutra)
                dayToRun = GetNextRunDate(now.AddDays(1));
                // nie udalo sie pobrac
                if (!dayToRun.HasValue)
                    return null;

                // czas odpalenia w podanym dniu
                dateTimeToRun = GetNextRunTime(now, dayToRun.Value);

                // nie udalo sie pobrac
                if (!dateTimeToRun.HasValue)
                    return null;

                if (!this.timesInUTC)
                {
                    // czasy ustawione w bazie jako lokalne
                    dateTimeToRun = dateTimeToRun.Value.ToUniversalTime();
                }
            }

            // jesli czasy sa tak ustawione, ze nigdy sie nie wykona lub przekracza juz czas koncowy
            if (dateTimeToRun < now || dayToRun > this.endDate)
                return null;

            return dateTimeToRun;
        }

        public DateTime? GetNextRunDateTime()
        {
            return GetNextRunDateTime(null);
        }
        #endregion
        #region GetNextRunDate
        private DateTime? GetNextRunDate(DateTime CurrentTime)
        {
            DateTime tempTime;

            #region scheduleType
            switch (this.scheduleType)
            {
                #region Once
                case Enums.ScheduleType.Once:
                    {
                        return this.startDate;
                    }
                #endregion
                #region Daily
                case Enums.ScheduleType.Daily:
                    {
                        tempTime = new DateTime(this.lastRunDate.Year, this.lastRunDate.Month, this.lastRunDate.Day, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);

                        // dodajemy dni do daty poczatkowej az sie zrowna lub przekroczy czas aktualny
                        while (tempTime < CurrentTime)
                        {
                            tempTime = tempTime.AddDays(this.scheduleInterval);
                        }

                        return tempTime;
                    }
                #endregion
                #region Weekly
                case Enums.ScheduleType.Weekly:
                    {
                        int recWeeks = 1;

                        // jesli co n tygodni
                        if (this.recurrenceFactor > 1)
                        {
                            recWeeks = this.recurrenceFactor;
                        }

                        DateTime minTempTime = DateTime.MaxValue;

                        for (int weekDay = 0; weekDay <= 6; weekDay++)
                        {
                            int isWeekDay = this.scheduleInterval & (int)Math.Pow(2, weekDay);

                            if (isWeekDay == 0)
                                continue;

                            tempTime = new DateTime(this.lastRunDate.Year, this.lastRunDate.Month, this.lastRunDate.Day, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);

                            // dodajemy dni do daty poczatkowej az bedzie wybrany dzien tygodnia
                            while ((int)tempTime.DayOfWeek != weekDay)
                            {
                                tempTime = tempTime.AddDays(1);
                            }

                            // dodajemy tygodnie do daty poczatkowej az sie zrowna lub przekroczy czas aktualny
                            while (tempTime < CurrentTime)
                            {
                                tempTime = tempTime.AddDays(recWeeks * 7);
                            }

                            if (tempTime < minTempTime)
                                minTempTime = tempTime;
                        }

                        return minTempTime;
                    }
                #endregion
                #region Monthly
                case Enums.ScheduleType.Monthly:
                    {
                        try
                        {
                            tempTime = new DateTime(this.lastRunDate.Year, this.lastRunDate.Month, this.scheduleInterval, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);
                        }
                        // moglo sie nie udac stworzenie daty np. 31 lutego
                        catch
                        {
                            // ale na pewno uda sie dla kolejnego miesiaca
                            tempTime = new DateTime(this.lastRunDate.Year, this.lastRunDate.Month + 1, this.scheduleInterval, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);
                        }

                        int recMonths = 1;

                        // jesli co n miesiecy
                        if (this.recurrenceFactor > 1)
                        {
                            recMonths = this.recurrenceFactor;
                        }

                        // dodajemy miesiace do daty poczatkowej az sie zrowna lub przekroczy czas aktualny
                        while (tempTime < CurrentTime)
                        {
                            tempTime = tempTime.AddMonths(recMonths);
                        }

                        return tempTime;
                    }
                #endregion
                #region MonthlyRelative
                case Enums.ScheduleType.MonthlyRelative:
                    {
                        tempTime = new DateTime(this.lastRunDate.Year, this.lastRunDate.Month, 1, CurrentTime.Hour, CurrentTime.Minute, CurrentTime.Second);

                        int months = 1;

                        // jesli co n miesiecy
                        if (this.recurrenceFactor > 1)
                        {
                            months = this.recurrenceFactor;
                        }

                        DateTime? relativeDate = tempTime;

                        // dodajemy miesiace do daty poczatkowej az przekroczy czas aktualny
                        while (relativeDate < CurrentTime)
                        {
                            relativeDate = GetRelativeDateTime(tempTime, (Enums.ScheduleRelativeDay)this.scheduleInterval, this.relativeInterval);
                            tempTime = tempTime.AddMonths(months);
                        }

                        if (!relativeDate.HasValue)
                        {
                            return null;
                        }

                        // ponizsze juz dodane w GetRelativeDateTime6
                        //relativeDate = relativeDate.Value.AddHours(tempTime.Hour);
                        //relativeDate = relativeDate.Value.AddMinutes(tempTime.Minute);
                        //relativeDate = relativeDate.Value.AddSeconds(tempTime.Second);

                        return relativeDate;
                    }
                #endregion
                default:
                    {
                        return null;
                    }
            }
            #endregion
        }
        #endregion
        #region GetRelativeDate
        private DateTime? GetRelativeDate(DateTime Date, Enums.ScheduleRelativeDay RelativeDay, Enums.ScheduleRelativeInterval RelativeInterval)
        {
            int[] daysInWeekCountTotal = new int[7];
            int daysTotal = 0;
            int weekDaysTotal = 0;
            int weekendDaysTotal = 0;

            int[] daysInWeekCount = new int[7];
            int days = 0;
            int weekDays = 0;
            int weekendDays = 0;

            DateTime startCurrDate = new DateTime(Date.Year, Date.Month, 1, 0, 0, 0);
            DateTime currDate = startCurrDate;

            // po wszystkich dniach danego miesiaca - liczymy sumy
            for (int i = 0; i < 31; i++)
            {
                currDate = startCurrDate.AddDays(i);

                // przekroczony miesiac
                if (currDate.Month > Date.Month)
                    break;

                daysTotal++;
                daysInWeekCountTotal[(int)currDate.DayOfWeek]++;

                // jesli sobota lub niedziela
                if ((int)currDate.DayOfWeek == 6 || (int)currDate.DayOfWeek == 0)
                    weekendDaysTotal++;
                else
                    weekDaysTotal++;
            }


            // po wszystkich dniach danego miesiaca
            for (int i = 0; i < 31; i++)
            {
                currDate = startCurrDate.AddDays(i);

                // przekroczony miesiac
                if (currDate.Month > Date.Month)
                    break;

                days++; 
                daysInWeekCount[(int)currDate.DayOfWeek]++;

                // jesli sobota lub niedziela
                if ((int)currDate.DayOfWeek == 6 || (int)currDate.DayOfWeek == 0)
                    weekendDays++;
                else
                    weekDays++;

                bool dayCheck = false;

                switch (RelativeDay)
                {
                    #region Sunday
                    case Enums.ScheduleRelativeDay.Sunday:
                        {
                            if ((int)currDate.DayOfWeek == 0)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Monday
                    case Enums.ScheduleRelativeDay.Monday:
                        {
                            if ((int)currDate.DayOfWeek == 1)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Tuesday
                    case Enums.ScheduleRelativeDay.Tuesday:
                        {
                            if ((int)currDate.DayOfWeek == 2)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Wednesday
                    case Enums.ScheduleRelativeDay.Wednesday:
                        {
                            if ((int)currDate.DayOfWeek == 3)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Thursday
                    case Enums.ScheduleRelativeDay.Thursday:
                        {
                            if ((int)currDate.DayOfWeek == 4)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Friday
                    case Enums.ScheduleRelativeDay.Friday:
                        {
                            if ((int)currDate.DayOfWeek == 5)
                                dayCheck = true;
                            break;
                        }
                    #endregion
                    #region Saturday
                    case Enums.ScheduleRelativeDay.Saturday:
                        {
                            if ((int)currDate.DayOfWeek == 6)
                                dayCheck = true;
                            break;
                        }
                    #endregion

                    #region Day
                    case Enums.ScheduleRelativeDay.Day:
                        {
                            #region RelativeInterval
                            switch (RelativeInterval)
                            {
                                #region First
                                case Enums.ScheduleRelativeInterval.First:
                                    {
                                        if (days == 1)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Second
                                case Enums.ScheduleRelativeInterval.Second:
                                    {
                                        if (days == 2)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Third
                                case Enums.ScheduleRelativeInterval.Third:
                                    {
                                        if (days == 3)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Fourth
                                case Enums.ScheduleRelativeInterval.Fourth:
                                    {
                                        if (days == 4)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Last
                                case Enums.ScheduleRelativeInterval.Last:
                                    {
                                        if (days == daysTotal)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                            }
                            #endregion
                            break;
                        }
                    #endregion
                    #region WeekDay
                    case Enums.ScheduleRelativeDay.WeekDay:
                        {
                            #region RelativeInterval
                            switch (RelativeInterval)
                            {
                                #region First
                                case Enums.ScheduleRelativeInterval.First:
                                    {
                                        if (weekDays == 1)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Second
                                case Enums.ScheduleRelativeInterval.Second:
                                    {
                                        if (weekDays == 2)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Third
                                case Enums.ScheduleRelativeInterval.Third:
                                    {
                                        if (weekDays == 3)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Fourth
                                case Enums.ScheduleRelativeInterval.Fourth:
                                    {
                                        if (weekDays == 4)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Last
                                case Enums.ScheduleRelativeInterval.Last:
                                    {
                                        if (weekDays == weekDaysTotal)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                            }
                            #endregion
                            break;
                        }
                    #endregion
                    #region WeekendDay
                    case Enums.ScheduleRelativeDay.WeekendDay:
                        {
                            #region RelativeInterval
                            switch (RelativeInterval)
                            {
                                #region First
                                case Enums.ScheduleRelativeInterval.First:
                                    {
                                        if (weekendDays == 1)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Second
                                case Enums.ScheduleRelativeInterval.Second:
                                    {
                                        if (weekendDays == 2)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Third
                                case Enums.ScheduleRelativeInterval.Third:
                                    {
                                        if (weekendDays == 3)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Fourth
                                case Enums.ScheduleRelativeInterval.Fourth:
                                    {
                                        if (weekendDays == 4)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                                #region Last
                                case Enums.ScheduleRelativeInterval.Last:
                                    {
                                        if (weekendDays == weekendDaysTotal)
                                        {
                                            return currDate;
                                        }
                                        break;
                                    }
                                #endregion
                            }
                            #endregion
                            break;
                        }
                    #endregion
                }

                if (dayCheck)
                {
                    #region RelativeInterval
                    switch (RelativeInterval)
                    {
                        #region First
                        case Enums.ScheduleRelativeInterval.First:
                            {
                                if (daysInWeekCount[(int)currDate.DayOfWeek] == 1)
                                {
                                    return currDate;
                                }
                                break;
                            }
                        #endregion
                        #region Second
                        case Enums.ScheduleRelativeInterval.Second:
                            {
                                if (daysInWeekCount[(int)currDate.DayOfWeek] == 2)
                                {
                                    return currDate;
                                }
                                break;
                            }
                        #endregion
                        #region Third
                        case Enums.ScheduleRelativeInterval.Third:
                            {
                                if (daysInWeekCount[(int)currDate.DayOfWeek] == 3)
                                {
                                    return currDate;
                                }
                                break;
                            }
                        #endregion
                        #region Fourth
                        case Enums.ScheduleRelativeInterval.Fourth:
                            {
                                if (daysInWeekCount[(int)currDate.DayOfWeek] == 4)
                                {
                                    return currDate;
                                }
                                break;
                            }
                        #endregion
                        #region Last
                        case Enums.ScheduleRelativeInterval.Last:
                            {
                                if (daysInWeekCount[(int)currDate.DayOfWeek] == daysInWeekCountTotal[(int)currDate.DayOfWeek])
                                {
                                    return currDate;
                                }
                                break;
                            }
                        #endregion
                    }
                    #endregion
                }

            }

            return null;
        }
        #endregion
        #region GetRelativeDateTime
        private DateTime? GetRelativeDateTime(DateTime Date, Enums.ScheduleRelativeDay RelativeDay, Enums.ScheduleRelativeInterval RelativeInterval)
        {
            DateTime? relativeDate = GetRelativeDate(Date, RelativeDay, RelativeInterval);
            if (relativeDate.HasValue)
            {
                relativeDate = relativeDate.Value.AddHours(Date.Hour);
                relativeDate = relativeDate.Value.AddMinutes(Date.Minute);
                relativeDate = relativeDate.Value.AddSeconds(Date.Second);
            }

            return relativeDate;
        }
        #endregion

        #region GetNextRunTime
        private DateTime? GetNextRunTime(DateTime Now, DateTime RunDay)
        {
            // RunDay jest dniem dzisiejszym lub z przyszlosci

            DateTime tempStartTime;
            try
            {
                tempStartTime = RunDay.Date.Add(this.startTime);
            }
            catch (ArgumentOutOfRangeException)
            {
                tempStartTime = DateTime.MaxValue;
            }
            DateTime tempEndTime;
            try
            {
                tempEndTime = RunDay.Date.Add(this.endTime);
            }
            catch (ArgumentOutOfRangeException)
            {
                tempEndTime = DateTime.MaxValue;
            }

             // czasy ustawione w bazie jako lokalne
            if (!this.timesInUTC)
            {
                tempStartTime = tempStartTime.ToUniversalTime();
                tempEndTime = tempEndTime.ToUniversalTime();
            }

            #region subdayType
           
            switch (this.subdayType)
            {
                #region SpecifiedTime
                case Enums.ScheduleSubdayType.SpecifiedTime:
                    {
                        return tempStartTime;
                    }
                #endregion
                #region Seconds
                case Enums.ScheduleSubdayType.Seconds:
                    {
                        int recSeconds = 1;
                        if (this.subdayInterval > 1)
                        {
                            recSeconds = this.subdayInterval;
                        }

                        // dodajemy sekundy az az sie zrowna lub przekroczy czas koncowy lub czas aktualny
                        while (tempStartTime < tempEndTime.AddSeconds(-recSeconds) && tempStartTime < Now)
                        {
                            tempStartTime = tempStartTime.AddSeconds(recSeconds);
                        }

                        return tempStartTime;
                    }
                #endregion
                #region Minutes
                case Enums.ScheduleSubdayType.Minutes:
                    {
                        int recMinutes = 1;
                        if (this.subdayInterval > 1)
                        {
                            recMinutes = this.subdayInterval;
                        }

                        // dodajemy minuty az az sie zrowna lub przekroczy czas koncowy lub czas aktualny
                        while (tempStartTime < tempEndTime.AddMinutes(-recMinutes) && tempStartTime < Now)
                        {
                            tempStartTime = tempStartTime.AddMinutes(recMinutes);
                        }

                        return tempStartTime;
                    }
                #endregion
                #region Hours
                case Enums.ScheduleSubdayType.Hours:
                    {
                        int recHours = 1;
                        if (this.subdayInterval > 1)
                        {
                            recHours = this.subdayInterval;
                        }

                        // dodajemy godziny az az sie zrowna lub przekroczy czas koncowy lub czas aktualny
                        while (tempStartTime < tempEndTime.AddHours(-recHours) && tempStartTime < Now)
                        {
                            tempStartTime = tempStartTime.AddHours(recHours);
                        }

                        return tempStartTime;
                    }
                #endregion
                default:
                    {
                        return null;
                    }
            }
            #endregion
        }
        #endregion
          
        #region GetRemainingTime
        /// <summary>
        /// Zwraca czas pozostaly do kolejnego uruchomienia schedula
        /// </summary>
        /// <param name="CurrentTime">Czas aktualny (UTC)</param>
        /// <returns>Przedzial casowy lub null jesli schedule juz nie ma byc uruchomiony (lub cos sie nie udalo)</returns>
        public TimeSpan? GetRemainingTime(DateTime? CurrentTime)
        {
            DateTime now = DateTimer.GetUtcNowWithoutMs().DateTime;

            // jesli podano czas aktualny
            if (CurrentTime.HasValue)
            {
                now = CurrentTime.Value;
            }

            DateTime? nextRunTime = GetNextRunDateTime(now);

            // czas wykonania juz minal
            if (!nextRunTime.HasValue || nextRunTime < now)
                return null;

            // dodatkowe sprawdzenie
            if (nextRunTime > this.endDate)
                return null;
            
            return nextRunTime.Value - now;
        }

        public TimeSpan? GetRemainingTime()
        {
            return GetRemainingTime(null);
        }
        #endregion
    }
}
