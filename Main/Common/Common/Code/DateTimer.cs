using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Common.Drivers.Device;

namespace IMR.Suite.Common
{
    /// <summary>
    /// Metody zwi�zane z dat�, czasem i ich konwersj�.
    /// </summary>
    public class DateTimer
    {
        #region GetUtcNowWithoutMs
        public static DateTimeOffset GetUtcNowWithoutMs()
        {
            DateTimeOffset now = DateTimeOffset.UtcNow;

            return new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, TimeSpan.Zero);
        }
        #endregion GetNowMsUTC

        #region GetMsUtcFromDtLocal
        /// <summary>
        /// Function transforms date and time from local time in datetime format into UTC in miliseconds.
        /// </summary>
        /// <param name="DtLocalDateTime">Local time in datetime</param>
        /// <returns>UTC in miliseconds</returns>
        public static long GetMsUtcFromDtLocal(DateTime DtLocalDateTime)
        {
            DateTime baseDateTime = DateTime.Parse("1970-01-01 00:00:00+0", System.Globalization.DateTimeFormatInfo.InvariantInfo);

            TimeSpan timeSpan = DtLocalDateTime.ToUniversalTime() - baseDateTime.ToUniversalTime();

            return (long)Math.Round((double)timeSpan.TotalMilliseconds);

        }

        /// <summary>
        /// Function transforms date and time from local time in datetime format into UTC in miliseconds.
        /// </summary>
        /// <param name="DtLocalDateTime">Local time in datetime nullable</param>
        /// <returns>UTC in miliseconds</returns>
        public static long? GetMsUtcFromDtLocal(DateTime? DtLocalDateTime)
        {
            if (DtLocalDateTime.HasValue)
                return GetMsUtcFromDtLocal(DtLocalDateTime);
            else
                return null;
        }
        #endregion
        #region GetMsUtcFromDtUtc
        /// <summary>
        /// Function transforms date and time from UTC in datetime format into UTC in miliseconds.
        /// </summary>
        /// <param name="DtUtcDateTime">UTC in datetime</param>
        /// <returns>UTC in miliseconds</returns>
        public static long GetMsUtcFromDtUtc(DateTime DtUtcDateTime)
        {
            DateTime baseDateTime = DateTime.Parse("1970-01-01 00:00:00+0", System.Globalization.DateTimeFormatInfo.InvariantInfo);

            TimeSpan timeSpan = DtUtcDateTime - baseDateTime.ToUniversalTime();

            return (long)Math.Round((double)timeSpan.TotalMilliseconds);
        }

        /// <summary>
        /// Function transforms date and time from UTC in datetime format into UTC in miliseconds.
        /// </summary>
        /// <param name="DtUtcDateTime">UTC in datetime nullable</param>
        /// <returns>UTC in miliseconds</returns>
        public static long? GetMsUtcFromDtUtc(DateTime? DtUtcDateTime)
        {
            if (DtUtcDateTime.HasValue)
                return GetMsUtcFromDtUtc(DtUtcDateTime.Value);
            else
                return null;
        }
        #endregion
        #region GetSUtcFromDtUtc
        /// <summary>
        /// Function transforms date and time from UTC in datetime format into UTC in seconds.
        /// </summary>
        /// <param name="DtUtcDateTime">UTC in datetime</param>
        /// <returns>UTC in seconds</returns>
        public static int GetSUtcFromDtUtc(DateTime DtUtcDateTime)
        {
            DateTime baseDateTime = DateTime.Parse("1970-01-01 00:00:00+0", System.Globalization.DateTimeFormatInfo.InvariantInfo);

            TimeSpan timeSpan = DtUtcDateTime - baseDateTime.ToUniversalTime();

            return (int)Math.Round((double)timeSpan.TotalSeconds);
        }
        #endregion

        #region GetDtLocalFromMsUtc
        /// <summary>
        /// Function transform date and time from UTC in miliseconds into local time in datetime format.
        /// </summary>
        /// <param name="MsUtcDateTime">UTC in miliseconds</param>
        /// <returns>Local time in datetime</returns>
        public static DateTime GetDtLocalFromMsUtc(long MsUtcDateTime)
        {
            DateTime baseDateTime = DateTime.Parse("1970-01-01 00:00:00+0", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            baseDateTime = baseDateTime.ToUniversalTime();

            return baseDateTime.AddMilliseconds(MsUtcDateTime).ToLocalTime();
        }

        /// <summary>
        /// Function transform date and time from UTC in miliseconds into local time in datetime format.
        /// </summary>
        /// <param name="MsUtcDateTime">UTC in miliseconds</param>
        /// <returns>Local time in nullable datetime</returns>
        public static DateTime? GetDtLocalFromMsUtc(long? MsUtcDateTime)
        {
            return MsUtcDateTime.HasValue ? (DateTime?)GetDtLocalFromMsUtc(MsUtcDateTime.Value) : null;
        }

        #endregion
        #region GetDtUtcFromMsUtc
        /// <summary>
        /// Function transform date and time from UTC in miliseconds into UTC time in datetime format.
        /// </summary>
        /// <param name="MsUtcDateTime">UTC in miliseconds</param>
        /// <returns>UTC time in datetime</returns>
        public static DateTime GetDtUtcFromMsUtc(long MsUtcDateTime)
        {
            DateTime baseDateTime = DateTime.Parse("1970-01-01 00:00:00+0", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            baseDateTime = baseDateTime.ToUniversalTime();

            return baseDateTime.AddMilliseconds(MsUtcDateTime);
        }

        /// <summary>
        /// Function transform date and time from UTC in miliseconds into UTC time in datetime format.
        /// </summary>
        /// <param name="MsUtcDateTime">UTC in miliseconds</param>
        /// <returns>UTC time in nullable datetime</returns>
        public static DateTime? GetDtUtcFromMsUtc(long? MsUtcDateTime)
        {
            return MsUtcDateTime.HasValue ? (DateTime?)GetDtUtcFromMsUtc(MsUtcDateTime.Value) : null;
        }
        #endregion

        #region GetHUtcFromHLocal
        public static int? GetHUtcFromHLocal(int? hour)
        {
            if (hour.HasValue)
                return DateTime.Today.AddHours(hour.Value).ToUniversalTime().Hour;
            else
                return null;
        }
        #endregion
        #region GetHLocalFromHUTC
        public static int? GetHLocalFromHUTC(int? hour)
        {
            if (hour.HasValue)
            {
                DateTime utc = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, hour.Value, 0, 0);
                return utc.ToLocalTime().Hour;
            }
            else
                return null;
        }
        #endregion

        #region GetEpochFromDateTime
        public static long GetEpochFromDateTime(DateTime time)
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((time - epochStart).TotalSeconds);
        }
        #endregion
        #region GetDateTimeFromEpoch
        public static DateTime GetDateTimeFromEpoch(long epochValue)
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epochStart.AddSeconds(Convert.ToDouble(epochValue));
        }
        #endregion

        #region GetCurrentTimeInDeviceLocalTime
        public static DateTime GetCurrentTimeInDeviceLocalTime(DeviceInfo deviceInfo)
        {
            return ConvertUTCToDeviceLocalTime(DateTime.UtcNow, deviceInfo);
        }
        public static DateTime GetCurrentTimeInDeviceLocalTime(List<DataValue> deviceDriverData)
        {
            return ConvertUTCToDeviceLocalTime(DateTime.UtcNow, deviceDriverData);
        }
        #endregion

        #region ConvertDeviceLocalTimeToUTC
        public static DateTime ConvertDeviceLocalTimeToUTC(DateTime deviceTime, DeviceInfo deviceInfo)
        {
            if (deviceInfo == null)
                return deviceTime;
            return ConvertDeviceLocalTimeToUTC(deviceTime, deviceInfo.DeviceDriverData);
        }
        public static DateTime ConvertDeviceLocalTimeToUTC(DateTime deviceTime, List<DataValue> deviceDriverData)
        {
            if (deviceDriverData == null)
                return deviceTime;
            DateTime result = DateTime.SpecifyKind(deviceTime, DateTimeKind.Unspecified);
            string daylightTimeZoneId = deviceDriverData.GetValueOrDefault<string>(DataType.DEVICE_DAYLIGHT_SAVING_TIME_ZONE_ID, String.Empty);

            if (!String.IsNullOrEmpty(daylightTimeZoneId))
            {
                TimeZoneInfo[] allTimeZoneInfos = TimeZoneInfo.GetSystemTimeZones().ToArray();
                TimeZoneInfo currentTimeZoneInfo = allTimeZoneInfos.FirstOrDefault(t => daylightTimeZoneId.Equals(t.Id));
                if (currentTimeZoneInfo != null)
                {
                    result = TimeZoneInfo.ConvertTimeToUtc(result, currentTimeZoneInfo);
                }
            }
            else
                return deviceTime;

            return result;
        }
        #endregion
        #region ConvertUTCToDeviceLocalTime
        public static DateTime ConvertUTCToDeviceLocalTime(DateTime deviceTime, DeviceInfo deviceInfo)
        {
            if (deviceInfo == null)
                return deviceTime;
            return ConvertUTCToDeviceLocalTime(deviceTime, deviceInfo.DeviceDriverData);
        }
        public static DateTime ConvertUTCToDeviceLocalTime(DateTime deviceTime, List<DataValue> deviceDriverData)
        {
            if (deviceDriverData == null)
                return deviceTime;
            DateTime result = DateTime.SpecifyKind(deviceTime, DateTimeKind.Unspecified);
            string daylightTimeZoneId = deviceDriverData.GetValueOrDefault<string>(DataType.DEVICE_DAYLIGHT_SAVING_TIME_ZONE_ID, String.Empty);

            if (!String.IsNullOrEmpty(daylightTimeZoneId))
            {
                TimeZoneInfo[] allTimeZoneInfos = TimeZoneInfo.GetSystemTimeZones().ToArray();
                TimeZoneInfo currentTimeZoneInfo = allTimeZoneInfos.FirstOrDefault(t => daylightTimeZoneId.Equals(t.Id));
                if (currentTimeZoneInfo != null)
                {
                    result = TimeZoneInfo.ConvertTimeFromUtc(result, currentTimeZoneInfo);
                }
            }
            else
                return deviceTime;

            return result;
        }
        #endregion

        #region ConvertLocalTimeToUTC
        public static DateTime ConvertLocalTimeToUTC(DateTime time, string timeZoneId)
        {
            DateTime result = DateTime.SpecifyKind(time, DateTimeKind.Unspecified);

            if (!String.IsNullOrEmpty(timeZoneId))
            {
                TimeZoneInfo[] allTimeZoneInfos = TimeZoneInfo.GetSystemTimeZones().ToArray();
                TimeZoneInfo currentTimeZoneInfo = allTimeZoneInfos.FirstOrDefault(t => timeZoneId.Equals(t.Id));
                if (currentTimeZoneInfo != null)
                {
                    result = TimeZoneInfo.ConvertTimeToUtc(result, currentTimeZoneInfo);
                }
            }
            else
                return time;

            return result;
        }
        #endregion
        #region ConvertUTCToLocalTime
        public static DateTime ConvertUTCToLocalTime(DateTime time, string timeZoneId)
        {
            DateTime result = DateTime.SpecifyKind(time, DateTimeKind.Unspecified);

            if (!String.IsNullOrEmpty(timeZoneId))
            {
                TimeZoneInfo[] allTimeZoneInfos = TimeZoneInfo.GetSystemTimeZones().ToArray();
                TimeZoneInfo currentTimeZoneInfo = allTimeZoneInfos.FirstOrDefault(t => timeZoneId.Equals(t.Id));
                if (currentTimeZoneInfo != null)
                {
                    result = TimeZoneInfo.ConvertTimeFromUtc(result, currentTimeZoneInfo);
                }
            }
            else
                return time;

            return result;
        }
        #endregion

    }
}
