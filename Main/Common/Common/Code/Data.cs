using System;
using System.Collections.Generic;
using System.Text;
using IMR.Suite.Common;
using System.Linq;

namespace IMR.Suite.Common
{
    [Serializable]
    public class Data
    {
        #region Members
        
        #region SerialNbr
        private SN serialNbr = null;
        public SN SerialNbr
        {
            get
            {
                return serialNbr;
            }
        }
        #endregion 
        #region IdMeter
        private long? idMeter = null;
        public long? IdMeter
        {
            get
            {
                return idMeter;
            }
        }
        #endregion
        #region IdLocation
        private long? idLocation = null;
        public long? IdLocation
        {
            get
            {
                return idLocation;
            }
        }
        #endregion

        #region IdOperator
        private long? idOperator = null;
        public long? IdOperator
        {
            get
            {
                return idOperator;
            }
        }
        #endregion

        #region DataValues
        /// <summary>
        /// Dane np. konkretnego urzadzenia/lokalizacji/licznika/akcji/operatora
        /// </summary>
        public Dictionary<long, List<DataValue>> DataValues = null;
        #endregion
        #region DataTypes
        /// <summary> 
        /// Slownik typow danych dla konkretnego urzadzenia/lokalizacji/licznika/akcji
        /// </summary>
        public Dictionary<long, DataType> DataTypes = null;
        #endregion

        #region HasDataValues
        public bool HasDataValues
        {
            get
            {
                return (this.DataValues != null && this.DataValues.Count > 0);
            }
        }
        #endregion
        #region HasDataTypes
        public bool HasDataTypes
        {
            get
            {
                return (this.DataTypes != null && this.DataTypes.Count > 0);
            }
        }
        #endregion
        
        #endregion

        #region Constructor
        public Data(SN SerialNbr, long? IdMeter, long? IdLocation)
        {
            this.serialNbr = SerialNbr;
            this.idMeter = IdMeter;
            this.idLocation = IdLocation;
        }

        public Data()  
            :this (null,null,null)
        {
        }

        public static Data ForDevice(SN SerialNbr)
        {
            Data result = new Data(SerialNbr, null, null);
            return result;
        }

        public static Data ForMeter(long IdMeter)
        {
            Data result = new Data(null, IdMeter, null); 
            return result;
        }

        public static Data ForLocation(long IdLocation)
        {
            Data result = new Data(null, null, IdLocation);
            return result;
        }

        public static Data ForOperator(int IdOperator)
        {
            Data result = new Data();
            result.idOperator = IdOperator;
            return result;
        }
        #endregion

        #region GetDataValues
        public DataValue[] GetDataValues(long[] DataTypes)
        {
            List<DataValue> result = new List<DataValue>();

            foreach (long dataType in DataTypes)
            {
                if (DataValues.ContainsKey(dataType))
                {
                    foreach (DataValue value in DataValues[dataType])
                    {
                        result.Add(value);
                    }
                }
            }

            return result.ToArray();
        }

        public DataValue[] GetDataValues(long DataType)
        {
            if (DataValues.ContainsKey(DataType))
                return DataValues[DataType].ToArray();
            else
                return new DataValue[0];
        }
        #endregion
        #region GetOneDataValue
        public DataValue GetOneDataValue(long DataType)
        {
            DataValue[] dataValues = GetDataValues(DataType);

            if (dataValues != null && dataValues.Length > 0)
                return dataValues[0];
            else
                return null;
        }
        #endregion

        #region GetDataValue
        public bool GetDataValue(long DataType, bool DefaultValue)
        {
            DataValue dataValue = GetOneDataValue(DataType);
            if (dataValue == null || dataValue.Value == null)
                return DefaultValue;
            else
                return Convert.ToBoolean(dataValue.Value);
        }
        public int GetDataValue(long DataType, int DefaultValue)
        {
            DataValue dataValue = GetOneDataValue(DataType);
            if (dataValue == null || dataValue.Value == null)
                return DefaultValue;
            else
                return Convert.ToInt32(dataValue.Value);
        }
        public long GetDataValue(long DataType, long DefaultValue)
        {
            DataValue dataValue = GetOneDataValue(DataType);
            if (dataValue == null || dataValue.Value == null)
                return DefaultValue;
            else
                return Convert.ToInt64(dataValue.Value);
        }
        public string GetDataValue(long DataType, string DefaultValue)
        {
            DataValue dataValue = GetOneDataValue(DataType);
            if (dataValue == null || dataValue.Value == null)
               return DefaultValue;
            else
                return dataValue.Value.ToString();
        }
        #endregion
         
        #region static FillTypesInValues
        public static void FillTypesInValues(Dictionary<long, DataType> Types, ref Dictionary<long, List<DataValue>> Values)
        {
            // jesli nie czym lub co wypelniac
            if (Types == null || Values == null || Types.Count == 0 || Values.Count == 0)
                return;

            foreach (long type in Values.Keys)
            {
                for (int i = 0; i < Values[type].Count; i++)
                {
                    Values[type][i].Type = Types[type];
                }
            }
        }

        public static void FillTypesInValues(Dictionary<long, DataType> Types, ref DataValue[] Values)
        {
            // jesli nie czym lub co wypelniac
            if (Types == null || Values == null || Types.Count == 0 || Values.Length == 0)
                return;

            for (int i = 0; i < Values.Length; i++)
            {
                Values[i].Type = Types[Values[i].Type.IdDataType];
            }
        }


		public static void FillTypesInValues(Dictionary<long, DataType> Types, ref DataMeasureValue[] Values)
		{
			// jesli nie czym lub co wypelniac
			if (Types == null || Values == null || Types.Count == 0 || Values.Length == 0)
				return;

			foreach (DataMeasureValue dv in Values)
				dv.Type = Types[dv.Type.IdDataType];
		}
		
		public static void FillTypesInValues(Dictionary<long, DataType> Types, ref List<DataMeasureValue> Values)
		{
			// jesli nie czym lub co wypelniac
			if (Types == null || Values == null || Types.Count == 0 || Values.Count == 0)
				return;

			foreach (DataMeasureValue dv in Values)
				dv.Type = Types[dv.Type.IdDataType];
		}

		public static void FillTypesInValues(Dictionary<long, DataType> Types, ref List<DataCurrentValue> Values)
		{
			// jesli nie czym lub co wypelniac
			if (Types == null || Values == null || Types.Count == 0 || Values.Count == 0)
				return;

			foreach (DataValue dv in Values)
				dv.Type = Types[dv.Type.IdDataType];
		}

		public static void FillTypesInValues(Dictionary<long, DataType> Types, ref List<DataArchValue> Values)
		{
			// jesli nie czym lub co wypelniac
			if (Types == null || Values == null || Types.Count == 0 || Values.Count == 0)
				return;

			foreach (DataValue dv in Values)
				dv.Type = Types[dv.Type.IdDataType];
		}
        public static void FillTypesInValues(Dictionary<long, DataType> Types, ref ReportDataType[] Values)
        {
            // jesli nie czym lub co wypelniac
            if (Types == null || Values == null || Types.Count == 0 || Values.Length == 0)
                return;

            for (int i = 0; i < Values.Length; i++)
            {
                Values[i].DataType = Types[Values[i].DataType.IdDataType];
            }
        }
        #endregion
        #region static CreateValuesDictionary
        public static Dictionary<long, List<DataValue>> CreateValuesDictionary(DataValue[] DataValues)
        {
            Dictionary<long, List<DataValue>> result = new Dictionary<long, List<DataValue>>();

            foreach (long idDataType in (from p in DataValues select p.Type.IdDataType).Distinct())
            	result.Add(idDataType, (from d in DataValues where d.Type.IdDataType == idDataType select d).ToList<DataValue>());

            return result;

            #region //old
            // !!!TW!!! To samo mozna zrobic tez tak:
			//
			//foreach (long idDataType in (from p in DataValues select p.Type.IdDataType).Distinct())
			//	result.Add(idDataType, (from d in DataValues where d.Type.IdDataType == idDataType select d).ToList<DataValue>());
			//
			// lub tak:
			//
			//foreach (long idDataType in DataValues.Select(p => p.Type.IdDataType).Distinct())
			//	result.Add(idDataType, DataValues.Where(d => d.Type.IdDataType == idDataType).Select(d => d).ToList<DataValue>());

            //long idDataType;
            //List<DataValue> dataValueList;

            //foreach (DataValue dataValue in DataValues)
            //{
            //    idDataType = dataValue.Type.IdDataType;

            //    if (result.ContainsKey(idDataType))
            //    {
            //        result[idDataType].Add(dataValue);
            //    }
            //    else
            //    {
            //        dataValueList = new List<DataValue>();
            //        dataValueList.Add(dataValue);
            //        result.Add(idDataType, dataValueList);
            //    }
            //}
            #endregion
        }
        #endregion
        #region static CompareValues
        /// <summary>
        /// Compares object values
        /// </summary>
        /// <param name="FirstValue"></param>
        /// <param name="SecondValue"></param>
        /// <param name="DataTypeClass"></param>
        /// <returns>
        /// Less than zero if FirstValue is less than SecondValue
        /// Zero if FirstValue is equal to SecondValue
        /// Greater than zero if FirstValue is greater than SecondValue
        /// </returns>
        public static int? CompareValues(object FirstValue, object SecondValue, Enums.DataTypeClass DataTypeClass)
        {
            try
            {
                switch (DataTypeClass)
                {
                    case Enums.DataTypeClass.Unknown:
                        return null;
                    case Enums.DataTypeClass.Integer:
                        return (Convert.ToInt64(FirstValue)).CompareTo(Convert.ToInt64(SecondValue));
                    case Enums.DataTypeClass.Real:
                        return (Convert.ToDouble(FirstValue)).CompareTo(Convert.ToDouble(SecondValue));
                    case Enums.DataTypeClass.Decimal:
                        return (Convert.ToDecimal(FirstValue)).CompareTo(Convert.ToDecimal(SecondValue));
                    case Enums.DataTypeClass.Text:
                        return string.Compare(FirstValue.ToString(), SecondValue.ToString());
                    case Enums.DataTypeClass.Date:
                    case Enums.DataTypeClass.Time:
                    case Enums.DataTypeClass.Datetime:
                        return DateTime.Compare(Convert.ToDateTime(FirstValue), Convert.ToDateTime(SecondValue));
                    case Enums.DataTypeClass.Boolean:
                        return (Convert.ToBoolean(FirstValue)).CompareTo(Convert.ToBoolean(SecondValue));
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return null;
            }
        }

        public static int? CompareValues(object FirstValue, object SecondValue, int IdDataTypeClass)
        {
            if (Enum.IsDefined(typeof(Enums.DataTypeClass), IdDataTypeClass))
            {
                return CompareValues(FirstValue, SecondValue, (Enums.DataTypeClass)IdDataTypeClass);
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
