using System;

namespace IMR.Suite.Common
{
	/// <summary>
	/// A null implementation of the <see cref="IPerformanceCountersManager"/>.
	/// This is a demonstration of the null pattern, which saves from checking from null each time.
	/// </summary>
	public class NullPerformanceCountersManager : IPerformanceCountersManager
	{
		public void StartWatch(string counterName) { }

		public void StopWatch(string counterName) { }

		public void UpdateCounter(string counterName, long value) { }

		public void Increment(string counterName) { }

		public void IncrementBy(string counterName, long value) { }

		public long GetElapsedMilliseconds(string counterName) { return 0; }

		public void ResetCounterValue(string counterName) { }

		public void IncrementCounterValueBy(string counterName, long value) { }

		public void UpdateCounter(string counterName) { }

		public void Dispose() { }
	}
}