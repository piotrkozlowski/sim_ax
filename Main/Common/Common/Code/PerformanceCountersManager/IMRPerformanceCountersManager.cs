using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace IMR.Suite.Common
{
	// To jest wykorzystywane przez niekt�re modu�y jako podlinkowany plik *.cs
	// Do Common'a nie jest kompilowane.
	public class IMRPerformanceCountersManager : PerformanceCountersManagerBase
	{
		public const string CategoryName = "IMR Server";
		public const string CategoryHelp = "IMR Server counters";

		#region Counters
		public const string PacketsInCounterName = "Packets In";
		public const string PacketsInPerSecondCounterName = "Packets In /sec";
		public const string PacketInAvgTimeCounterName = "Avg. Packet In process time";
		public const string PacketInAvgTimeBaseCounterName = "Avg. Packet In process time base";

		public const string PacketsOutCounterName = "Packets Out";
		public const string PacketsOutPerSecondCounterName = "Packets Out /sec";
		public const string PacketOutAvgTimeCounterName = "Avg. Packet Out process time";
		public const string PacketOutAvgTimeBaseCounterName = "Avg. Packet Out process time base";

		public const string AckPacketsInCounterName = "Ack Packets In";
		public const string AckPacketsInPerSecondCounterName = "Ack Packets In /sec";
		public const string AckPacketInAvgTimeCounterName = "Avg. Ack Packet In process time";
		public const string AckPacketInAvgTimeBaseCounterName = "Avg. Ack Packet In process time base";

		public const string QueuePacketsInCounterName = "Queue Packets In";
		public const string QueuePacketsInPerSecondCounterName = "Queue Packets In /sec";
		public const string QueuePacketInAvgTimeCounterName = "Queue Avg. Packet In process time";
		public const string QueuePacketInAvgTimeBaseCounterName = "Queue Avg. Packet In process time base";

		#endregion

		#region enum IMRCounterType
		[Flags]
		public enum IMRCounterType
		{
			PacketsIn,
			PacketsOut,
			AckPackets,
			QueuePacketsIn,
			QueuePacketsOut,
			QueueAckPackets,
		}
		#endregion

		#region ctor
		public IMRPerformanceCountersManager(string instanceName, IMRCounterType imrCountersType)
		{
//            if (PerformanceCounterCategory.Exists(CategoryName))
//                PerformanceCounterCategory.Delete(CategoryName);

			#region Install Counters if not exists
			var counters = new CounterCreationDataCollection();

			#region IMRCounterType.PacketsIn
			counters.Add(new CounterCreationData(PacketsInCounterName, "Number of input pakets.", PerformanceCounterType.NumberOfItems64));
			counters.Add(new CounterCreationData(PacketsInPerSecondCounterName, "Number of input packets per second.", PerformanceCounterType.RateOfCountsPerSecond32));
			counters.Add(new CounterCreationData(PacketInAvgTimeCounterName, "The average time [milisecond] it takes to process a input packet.", PerformanceCounterType.AverageCount64));
			counters.Add(new CounterCreationData(PacketInAvgTimeBaseCounterName, "The average time [milisecond] it takes to process a input packet.", PerformanceCounterType.AverageBase));
			#endregion
			#region IMRCounterType.PacketsOut
			counters.Add(new CounterCreationData(PacketsOutCounterName, "Number of output pakets.", PerformanceCounterType.NumberOfItems64));
			counters.Add(new CounterCreationData(PacketsOutPerSecondCounterName, "Number of output packets per second.", PerformanceCounterType.RateOfCountsPerSecond32));
			counters.Add(new CounterCreationData(PacketOutAvgTimeCounterName, "The average time [milisecond] it takes to process a output packet.", PerformanceCounterType.AverageCount64));
			counters.Add(new CounterCreationData(PacketOutAvgTimeBaseCounterName, "The average time [milisecond] it takes to process a output packet.", PerformanceCounterType.AverageBase));
			#endregion
			#region AckPackets
			counters.Add(new CounterCreationData(AckPacketsInCounterName, "Number of input Ack pakets.", PerformanceCounterType.NumberOfItems64));
			counters.Add(new CounterCreationData(AckPacketsInPerSecondCounterName, "Number of input Ack packets per second.", PerformanceCounterType.RateOfCountsPerSecond32));
			counters.Add(new CounterCreationData(AckPacketInAvgTimeCounterName, "The average time [milisecond] it takes to process a input Ack packet.", PerformanceCounterType.AverageCount64));
			counters.Add(new CounterCreationData(AckPacketInAvgTimeBaseCounterName, "The average time [milisecond] it takes to process a input Ack packet.", PerformanceCounterType.AverageBase));
			#endregion
			#region QueuePacketsIn
			counters.Add(new CounterCreationData(QueuePacketsInCounterName, "Number of input pakets in queue.", PerformanceCounterType.NumberOfItems64));
			counters.Add(new CounterCreationData(QueuePacketsInPerSecondCounterName, "Number of input packets in queue per second.", PerformanceCounterType.RateOfCountsPerSecond32));
			counters.Add(new CounterCreationData(QueuePacketInAvgTimeCounterName, "The average time [milisecond] it takes to process a input packets from queue.", PerformanceCounterType.AverageCount64));
			counters.Add(new CounterCreationData(QueuePacketInAvgTimeBaseCounterName, "The average time [milisecond] it takes to process a input packets from queue.", PerformanceCounterType.AverageBase));
			#endregion

            // !!! 
            // Na niekt�rych maszynach 'Exists(CategoryName)' wywo�uje Exception
            // Pomaga wykonanie rebuilda PerformanceCounters za pomoc� wsadowej komendy: lodctr.exe /r
            // !!!
            if (PerformanceCounterCategory.Exists(CategoryName))
            {
                InstanceDataCollectionCollection preservedValues = GetPreservedValues();
                PerformanceCounterCategory.Delete(CategoryName);
                PerformanceCounterCategory.Create(CategoryName, CategoryHelp, PerformanceCounterCategoryType.MultiInstance, counters);
                SetPreservedValues(preservedValues);
            }
            else
            {
                PerformanceCounterCategory.Create(CategoryName, CategoryHelp, PerformanceCounterCategoryType.MultiInstance, counters);
            }

            #endregion
            #region Create instance of counters
            #region IMRCounterType.PacketsIn
            if ((imrCountersType & IMRCounterType.PacketsIn) == IMRCounterType.PacketsIn)
            {
                PerformanceCounters.Add(PacketsInCounterName, CreateWritablePerformanceCounter(CategoryName, PacketsInCounterName, instanceName));
                PerformanceCounters.Add(PacketsInPerSecondCounterName, CreateWritablePerformanceCounter(CategoryName, PacketsInPerSecondCounterName, instanceName));
                PerformanceCounters.Add(PacketInAvgTimeCounterName, CreateWritablePerformanceCounter(CategoryName, PacketInAvgTimeCounterName, instanceName));
                PerformanceCounters.Add(PacketInAvgTimeBaseCounterName, CreateWritablePerformanceCounter(CategoryName, PacketInAvgTimeBaseCounterName, instanceName));
            }
            #endregion
            #region IMRCounterType.PacketsOut
            if ((imrCountersType & IMRCounterType.PacketsOut) == IMRCounterType.PacketsOut)
            {
                PerformanceCounters.Add(PacketsOutCounterName, CreateWritablePerformanceCounter(CategoryName, PacketsOutCounterName, instanceName));
                PerformanceCounters.Add(PacketsOutPerSecondCounterName, CreateWritablePerformanceCounter(CategoryName, PacketsOutPerSecondCounterName, instanceName));
                PerformanceCounters.Add(PacketOutAvgTimeCounterName, CreateWritablePerformanceCounter(CategoryName, PacketOutAvgTimeCounterName, instanceName));
                PerformanceCounters.Add(PacketOutAvgTimeBaseCounterName, CreateWritablePerformanceCounter(CategoryName, PacketOutAvgTimeBaseCounterName, instanceName));
            }
            #endregion
            #region IMRCounterType.AckPackets
            if ((imrCountersType & IMRCounterType.AckPackets) == IMRCounterType.AckPackets)
            {
                PerformanceCounters.Add(AckPacketsInCounterName, CreateWritablePerformanceCounter(CategoryName, AckPacketsInCounterName, instanceName));
                PerformanceCounters.Add(AckPacketsInPerSecondCounterName, CreateWritablePerformanceCounter(CategoryName, AckPacketsInPerSecondCounterName, instanceName));
                PerformanceCounters.Add(AckPacketInAvgTimeCounterName, CreateWritablePerformanceCounter(CategoryName, AckPacketInAvgTimeCounterName, instanceName));
                PerformanceCounters.Add(AckPacketInAvgTimeBaseCounterName, CreateWritablePerformanceCounter(CategoryName, AckPacketInAvgTimeBaseCounterName, instanceName));
            }
            #endregion
            #region IMRCounterType.QueuePacketsIn
            if ((imrCountersType & IMRCounterType.QueuePacketsIn) == IMRCounterType.QueuePacketsIn)
            {
                PerformanceCounters.Add(QueuePacketsInCounterName, CreateWritablePerformanceCounter(CategoryName, QueuePacketsInCounterName, instanceName));
                PerformanceCounters.Add(QueuePacketsInPerSecondCounterName, CreateWritablePerformanceCounter(CategoryName, QueuePacketsInPerSecondCounterName, instanceName));
                PerformanceCounters.Add(QueuePacketInAvgTimeCounterName, CreateWritablePerformanceCounter(CategoryName, QueuePacketInAvgTimeCounterName, instanceName));
                PerformanceCounters.Add(QueuePacketInAvgTimeBaseCounterName, CreateWritablePerformanceCounter(CategoryName, QueuePacketInAvgTimeBaseCounterName, instanceName));
            }
            #endregion
            #endregion
		}
		#endregion

		#region private GetPreservedValues
		private InstanceDataCollectionCollection GetPreservedValues()
		{
			InstanceDataCollectionCollection preservedValues = new InstanceDataCollectionCollection();
			
			if (!PerformanceCounterCategory.Exists(CategoryName))
				return preservedValues;

			PerformanceCounterCategory imrPerformanceCategory = new PerformanceCounterCategory(CategoryName);
			preservedValues = imrPerformanceCategory.ReadCategory();

			return preservedValues;
		}
		#endregion
		#region private SetPreservedValues
		private void SetPreservedValues(InstanceDataCollectionCollection preservedValues)
		{
			foreach (InstanceDataCollection iDataCol in preservedValues.Values)
			{
				foreach (InstanceData iData in iDataCol.Values)
				{
					PerformanceCounter performanceCounter = new PerformanceCounter(CategoryName, iDataCol.CounterName, iData.InstanceName, false);
					performanceCounter.RawValue = iData.RawValue;
				}
			}
		}
		#endregion

		#region class PacketsIn
		public class PacketsIn : IDisposable
		{
			IPerformanceCountersManager manager;
			Stopwatch stopWatch = new Stopwatch();

			public PacketsIn(IPerformanceCountersManager performanceCountersManager)
			{
				stopWatch.Start();
				manager = performanceCountersManager;
			}

			public void Dispose()
			{
				stopWatch.Stop();
				manager.IncrementBy(IMRPerformanceCountersManager.PacketInAvgTimeCounterName, stopWatch.ElapsedMilliseconds);
				manager.Increment(IMRPerformanceCountersManager.PacketInAvgTimeBaseCounterName);
				manager.Increment(IMRPerformanceCountersManager.PacketsInCounterName);
				manager.Increment(IMRPerformanceCountersManager.PacketsInPerSecondCounterName);
			}
		}
		#endregion
		#region class PacketsOut
		public class PacketsOut : IDisposable
		{
			IPerformanceCountersManager manager;
			Stopwatch stopWatch = new Stopwatch();

			public PacketsOut(IPerformanceCountersManager performanceCountersManager)
			{
				stopWatch.Start();
				manager = performanceCountersManager;
			}

			public void Dispose()
			{
				stopWatch.Stop();
				manager.IncrementBy(IMRPerformanceCountersManager.PacketOutAvgTimeCounterName, stopWatch.ElapsedMilliseconds);
				manager.Increment(IMRPerformanceCountersManager.PacketOutAvgTimeBaseCounterName);
				manager.Increment(IMRPerformanceCountersManager.PacketsOutCounterName);
				manager.Increment(IMRPerformanceCountersManager.PacketsOutPerSecondCounterName);
			}
		}
		#endregion
		#region class AckPacketsIn
		public class AckPacketsIn : IDisposable
		{
			IPerformanceCountersManager manager;
			Stopwatch stopWatch = new Stopwatch();

			public AckPacketsIn(IPerformanceCountersManager performanceCountersManager)
			{
				stopWatch.Start();
				manager = performanceCountersManager;
			}

			public void Dispose()
			{
				stopWatch.Stop();
				manager.IncrementBy(IMRPerformanceCountersManager.AckPacketInAvgTimeCounterName, stopWatch.ElapsedMilliseconds);
				manager.Increment(IMRPerformanceCountersManager.AckPacketInAvgTimeBaseCounterName);
				manager.Increment(IMRPerformanceCountersManager.AckPacketsInCounterName);
				manager.Increment(IMRPerformanceCountersManager.AckPacketsInPerSecondCounterName);
			}
		}
		#endregion
		#region class QueuePacketsIn
		public class QueuePacketsIn : IDisposable
		{
			IPerformanceCountersManager manager;
			Stopwatch stopWatch = new Stopwatch();

			public QueuePacketsIn(IPerformanceCountersManager performanceCountersManager)
			{
				stopWatch.Start();
				manager = performanceCountersManager;
			}

			public void Dispose()
			{
				stopWatch.Stop();
				manager.IncrementBy(IMRPerformanceCountersManager.QueuePacketInAvgTimeCounterName, stopWatch.ElapsedMilliseconds);
				manager.Increment(IMRPerformanceCountersManager.QueuePacketInAvgTimeBaseCounterName);
			}
			public void IncrementBy(long value)
			{
				manager.IncrementBy(IMRPerformanceCountersManager.QueuePacketsInCounterName, value);
				manager.IncrementBy(IMRPerformanceCountersManager.QueuePacketsInPerSecondCounterName, value);
			}
		}
		#endregion
	}
}