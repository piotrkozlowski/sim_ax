using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace IMR.Suite.Common
{
	#region interface IPerformanceCountersManager
	public interface IPerformanceCountersManager : IDisposable
	{
		/// <summary>
		/// Starts the stopwatch for the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		void StartWatch(string counterName);

		/// <summary>
		/// Stops the stopwatch for the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		void StopWatch(string counterName);

		/// <summary>
		/// Updates the performance counter by the given value, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		void UpdateCounter(string counterName, long value);

		/// <summary>
		/// Increments the specified counter value by 1, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		void Increment(string counterName);

		/// <summary>
		/// Increments the specified counter value by the given value, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		void IncrementBy(string counterName, long value);

		/// <summary>
		/// Gets the elapsed milliseconds of the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <returns>If stopwatch exists for the counter returns elapsed milliseconds of the counter, 0 otherwise</returns>
		long GetElapsedMilliseconds(string counterName);

		/// <summary>
		/// Resets the counter value to zero.
		/// This does not change the actual performance counter, only its internal representation.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		void ResetCounterValue(string counterName);

		/// <summary>
		/// Increment the counter value by the given value.
		/// This does not change the actual performance counter, only its internal representation.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		void IncrementCounterValueBy(string counterName, long value);

		/// <summary>
		/// Change the value of the counter according to its internal value.
		/// You should use this after calling <see cref="PerformanceCountersManagerBase.IncrementCounterValueBy"/> when you want to actually
		/// change the counter value.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		void UpdateCounter(string counterName);
	}
	#endregion

	#region class PerformanceCountersManagerBase
	/// <summary>
	/// A base class for performance counters.
	/// </summary>
	public class PerformanceCountersManagerBase : IPerformanceCountersManager
	{
		#region Members
		private bool disposed;
		private Dictionary<string, PerformanceCounter> performanceCounters = new Dictionary<string, PerformanceCounter>();
		private Dictionary<string, Stopwatch> stopWatches = new Dictionary<string, Stopwatch>();
		private Dictionary<string, long> counterValues = new Dictionary<string, long>();
		#endregion

		#region Properties
		protected Dictionary<string, PerformanceCounter> PerformanceCounters
		{
			get { return performanceCounters; }
		}
		#endregion

		#region StartWatch
		/// <summary>
		/// Starts the stopwatch for the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		public void StartWatch(string counterName)
		{
			if (!stopWatches.ContainsKey(counterName))
				stopWatches[counterName] = new Stopwatch();

			stopWatches[counterName].Reset();
			stopWatches[counterName].Start();
		}
		#endregion
		#region StopWatch
		/// <summary>
		/// Stops the stopwatch for the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		public void StopWatch(string counterName)
		{
			if (!stopWatches.ContainsKey(counterName))
				return;

			stopWatches[counterName].Stop();
			//UpdateCounter(counterName, stopWatches[counterName].ElapsedMilliseconds);
			IncrementBy(counterName, stopWatches[counterName].ElapsedMilliseconds);
		}
		#endregion
		#region UpdateCounter
		/// <summary>
		/// Updates the performance counter by the given value, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		public void UpdateCounter(string counterName, long value)
		{
			if (!performanceCounters.ContainsKey(counterName))
				return;

			performanceCounters[counterName].RawValue = value;
		}
		#endregion
		#region Increment
		/// <summary>
		/// Increments the specified counter value by 1, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		public void Increment(string counterName)
		{
			if (!performanceCounters.ContainsKey(counterName))
				return;

			performanceCounters[counterName].Increment();
		}
		#endregion
		#region IncrementBy
		/// <summary>
		/// Increments the specified counter value by the given value, if the counter exists.
		/// If the counter does not exist - return
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		public void IncrementBy(string counterName, long value)
		{
			if (!performanceCounters.ContainsKey(counterName))
				return;

			performanceCounters[counterName].IncrementBy(value);
		}
		#endregion
		#region GetElapsedMilliseconds
		/// <summary>
		/// Gets the elapsed milliseconds of the given counter.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <returns>If stopwatch exists for the counter returns elapsed milliseconds of the counter, 0 otherwise</returns>
		public long GetElapsedMilliseconds(string counterName)
		{
			return !stopWatches.ContainsKey(counterName) ? 0 : stopWatches[counterName].ElapsedMilliseconds;
		}
		#endregion
		#region ResetCounterValue
		/// <summary>
		/// Resets the counter value to zero.
		/// This does not change the actual performance counter, only its internal representation.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		public void ResetCounterValue(string counterName)
		{
			counterValues[counterName] = 0;
		}
		#endregion
		#region IncrementCounterValueBy
		/// <summary>
		/// Increment the counter value by the given value.
		/// This does not change the actual performance counter, only its internal representation.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		/// <param name="value">The value.</param>
		public void IncrementCounterValueBy(string counterName, long value)
		{
			if (!counterValues.ContainsKey(counterName))
				return;

			counterValues[counterName] += value;
		}
		#endregion
		#region UpdateCounter
		/// <summary>
		/// Change the value of the counter according to its internal value.
		/// You should use this after calling <see cref="IncrementCounterValueBy"/> when you want to actually
		/// change the counter value.
		/// </summary>
		/// <param name="counterName">Name of the counter.</param>
		public void UpdateCounter(string counterName)
		{
			if (!counterValues.ContainsKey(counterName))
				return;

			UpdateCounter(counterName, counterValues[counterName]);
		}
		#endregion

		#region CreateWritablePerformanceCounter
		protected static PerformanceCounter CreateWritablePerformanceCounter(string category, string counterName, string instance)
		{
			return new PerformanceCounter(category, counterName, instance, false) { RawValue = 0 };
		}
		#endregion
		#region Dispose
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
				return;

			if (disposing)
			{
				foreach (PerformanceCounter counter in performanceCounters.Values)
				{
					counter.RemoveInstance();
					counter.Close();
					counter.Dispose();
				}
			}

			disposed = true;
		}
		#endregion
		#region dtor
		~PerformanceCountersManagerBase()
		{
			Dispose(false);
		}
		#endregion
	}
	#endregion
}