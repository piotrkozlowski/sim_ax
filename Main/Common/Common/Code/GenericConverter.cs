﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace IMR.Suite.Common
{
    public class GenericConverter
    {
        public static T Parse<T>(object sourceValue) where T : IConvertible
		{
            return Parse<T>(sourceValue, default(T));
        }

        public static T Parse<T>(object sourceValue, T defaultResult) where T : IConvertible
        {
            if (sourceValue == null)
                return defaultResult;
            try
            {
                return (T)Convert.ChangeType(sourceValue, typeof(T));
            }
            catch (Exception)
            {
                return defaultResult;
            }
        }

        public static T Parse<T>(object sourceValue, IFormatProvider provider) where T : IConvertible
        {
            return Parse<T>(sourceValue, provider, default(T));
        }

        public static T Parse<T>(object sourceValue, IFormatProvider provider, T defaultResult) where T : IConvertible
        {
            if (sourceValue == null || provider == null)
                return defaultResult;
            try
            {
                return (T)Convert.ChangeType(sourceValue, typeof(T), provider);
            }
            catch (Exception)
            {
                return defaultResult;
            }
        }

        public static Nullable<T> ParseNullable<T>(object sourceValue) where T : struct, IConvertible
        {
            if (sourceValue == null)
                return null;
            try
            {
                return (T)Convert.ChangeType(sourceValue, typeof(T));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Nullable<T> ParseNullable<T>(object sourceValue, IFormatProvider provider) where T : struct, IConvertible
        {
            if (sourceValue == null || provider == null)
                return null;
            try
            {
                return (T)Convert.ChangeType(sourceValue, typeof(T), provider);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
