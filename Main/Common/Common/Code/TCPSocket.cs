#define USE_TASK

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace IMR.Suite.Common
{
    #region Enum WSA_ERROR - Windows Sockets Error Codes
    public enum WSA_ERROR : int
    {
        WSAEINTR = 10004,	// Interrupted function call. 
        WSAEACCES = 10013,	// Permission denied. 
        WSAEFAULT = 10014,	// Bad address. 
        WSAEINVAL = 10022,	// Invalid argument. 
        WSAEMFILE = 10024,	// Too many open files. 
        WSAEWOULDBLOCK = 10035,	// Resource temporarily unavailable. 
        WSAEINPROGRESS = 10036,	// Operation now in progress. 
        WSAEALREADY = 10037,	// Operation already in progress. 
        WSAENOTSOCK = 10038,	// Socket operation on nonsocket. 
        WSAEDESTADDRREQ = 10039,	// Destination address required. 
        WSAEMSGSIZE = 10040,	// Message too long. 
        WSAEPROTOTYPE = 10041,	// Protocol wrong type for socket. 
        WSAENOPROTOOPT = 10042,	// Bad protocol option. 
        WSAEPROTONOSUPPORT = 10043,	// Protocol not supported. 
        WSAESOCKTNOSUPPORT = 10044,	// Socket type not supported. 
        WSAEOPNOTSUPP = 10045,	// Operation not supported. 
        WSAEPFNOSUPPORT = 10046,	// Protocol family not supported. 
        WSAEAFNOSUPPORT = 10047,	// Address family not supported by protocol family. 
        WSAEADDRINUSE = 10048,	// Address already in use. 
        WSAEADDRNOTAVAIL = 10049,	// Cannot assign requested address. 
        WSAENETDOWN = 10050,	// Network is down. 
        WSAENETUNREACH = 10051,	// Network is unreachable. 
        WSAENETRESET = 10052,	// Network dropped connection on reset. 
        WSAECONNABORTED = 10053,	// Software caused connection abort. 
        WSAECONNRESET = 10054,	// Connection reset by peer. 
        WSAENOBUFS = 10055,	// No buffer space available. 
        WSAEISCONN = 10056,	// Socket is already connected. 
        WSAENOTCONN = 10057,	// Socket is not connected. 
        WSAESHUTDOWN = 10058,	// Cannot send after socket shutdown. 
        WSAETIMEDOUT = 10060,	// Connection timed out. 
        WSAECONNREFUSED = 10061,	// Connection refused. 
        WSAEHOSTDOWN = 10064,	// Host is down. 
        WSAEHOSTUNREACH = 10065,	// No route to host. 
        WSAEPROCLIM = 10067,	// Too many processes. 
        WSASYSNOTREADY = 10091,	// Network subsystem is unavailable. 
        WSAVERNOTSUPPORTED = 10092,	// Winsock.dll version out of range. 
        WSANOTINITIALISED = 10093,	// Successful WSAStartup not yet performed. 
        WSAEDISCON = 10101,	// Graceful shutdown in progress. 
        WSATYPE_NOT_FOUND = 10109,	// Class type not found. 
        WSAHOST_NOT_FOUND = 11001,	// Host not found. 
        WSATRY_AGAIN = 11002,	// Nonauthoritative host not found. 
        WSANO_RECOVERY = 11003,	// This is a nonrecoverable error. 
        WSANO_DATA = 11004		// Valid name, no data record of requested type.
    }
    #endregion

    #region Globals Event declaration
    #region TCPServer Events
    public delegate void TCPServerErrorEventHandler(TCPServer sender, TCPSocketExceptionEventArgs e);
    public delegate void TCPServerNewConnectionEventHandler(TCPServer sender, TCPSocketEventArgs s);
    public delegate void TCPServerStoppedEventHandler(TCPServer sender, EventArgs e);
    public delegate void TCPServerStartedEventHandler(TCPServer sender, EventArgs e);
    #endregion
    #region TCPSocket Events
    public delegate void TCPSocketErrorEventHandler(TCPSocket sender, TCPSocketExceptionEventArgs e);
    public delegate void TCPSocketDataArrivalEventHandler(TCPSocket sender, TCPSocketReceiveEventArgs e);
    public delegate void TCPSocketClosedByRemoteSideEventHandler(TCPSocket sender, EventArgs e);
    public delegate void TCPSocketConnectedToRemoteHostEventHandler(TCPSocket sender, EventArgs e);
    public delegate void TCPSocketSendProgressEventHandler(TCPSocket sender, TCPSocketSendProgressEventArgs e);
    public delegate void TCPSocketSendCompletedEventHandler(TCPSocket sender, TCPSocketSendCompletedEventArgs e);
    #endregion
    #endregion

    #region EventArgs objects
    #region TCPSocketSendProgressEventArgs
    /// <summary>
    /// uzywany przez TCPSocketSendProgressEventHandler
    /// </summary>
    public class TCPSocketSendProgressEventArgs : EventArgs
    {
        /// <summary>
        /// liczba wyslanych bajtow
        /// </summary>
        public readonly int SentBytes;
        /// <summary>
        /// liczba bajtow jeszcze do wyslania
        /// </summary>
        public readonly int RemainingBytes;
        /// <summary>
        /// liczba wszystkich bajtow (SentBytes + RemainingBytes)
        /// </summary>
        public readonly int TotalBytes;
        public TCPSocketSendProgressEventArgs(int sentBytes, int remainingBytes, int totalBytes)
        {
            SentBytes = sentBytes;
            RemainingBytes = remainingBytes;
            TotalBytes = totalBytes;
        }
    }
    #endregion
    #region TCPSocketReceiveEventArgs
    /// <summary>
    /// uzywany przez TCPSocketDataArrivalEventHandler
    /// </summary>
    public class TCPSocketReceiveEventArgs : EventArgs
    {
        /// <summary>
        /// data in byte[] </summary>
        public readonly byte[] Buffer;
        /// <summary>
        /// data length
        /// </summary>
        public readonly int BufferSize;
        public TCPSocketReceiveEventArgs(byte[] data)
        {
            Buffer = data;
            BufferSize = data.Length;
        }
    }
    #endregion
    #region TCPSocketSendCompletedEventArgs
    /// <summary>
    /// uzywany przez TCPSocketSendCompletedEvent
    /// </summary>
    public class TCPSocketSendCompletedEventArgs : EventArgs
    {
        public readonly TCPSendObject SendObject;

        public TCPSocketSendCompletedEventArgs(TCPSendObject sendObject)
        {
            SendObject = sendObject;
        }
    }
    #endregion
    #region TCPSocketExceptionEventArgs
    /// <summary>
    /// uzywany do wywlolania wyjatku
    /// </summary>
    public class TCPSocketExceptionEventArgs : EventArgs
    {
        public readonly Exception exception;

        public TCPSocketExceptionEventArgs(Exception e)
        {
            exception = e;
        }
    }
    #endregion
    #region TCPSocketEventArgs
    /// <summary>
    /// uzywany przez TCPSeverNewConnectionEvent
    /// </summary>
    public class TCPSocketEventArgs : EventArgs
    {
        public Socket socket;
        public TCPSocketEventArgs(Socket s)
        {
            socket = s;
        }
    }
    #endregion
    #endregion

    #region TCPSendObject
    public class TCPSendObject
    {
        public readonly byte[] Buffer;
        public readonly object Tag;

        public TCPSendObject(byte[] buffer, object Tag)
        {
            this.Buffer = buffer;
            this.Tag = Tag;
        }
    }
    #endregion

    #region class TCPServer
    public class TCPServer
    {
        #region Events
        public event TCPServerErrorEventHandler TCPServerErrorEvent;
        public event TCPServerNewConnectionEventHandler TCPServerNewConnectionEvent;
        public event TCPServerStoppedEventHandler TCPServerStoppedEvent;
        public event TCPServerStartedEventHandler TCPServerStartedEvent;
        #endregion

        #region Const
        private const int DEFAULT_PORT = 6000;
        private const int DEFAULT_MAX_LENGTH_QUEUE_FOR_PENDING_CONNECTIONS = 100;
        #endregion

        #region Members
        #region Port
        private int port;
        public int Port
        {
            get { return port; }
            set { port = value; }
        }
        #endregion
        #region IP
        private string ip;
        public string IP
        {
            get { return ip; }
            set { ip = value; }
        }
        #endregion
        #region MaxLengthQueueForPendingConnections
        private int maxLengthQueueForPendingConnections;
        public int MaxLengthQueueForPendingConnections
        {
            get { return maxLengthQueueForPendingConnections; }
            set { maxLengthQueueForPendingConnections = value; }
        }
        #endregion
        #region Started
        private bool started = false;
        public bool Started
        {
            get { return started; }
            set { started = value; }
        }
        #endregion

        private Socket srvSocket;
        #endregion

        #region Constructors
        private void CommonInitialization()
        {
            MaxLengthQueueForPendingConnections = DEFAULT_MAX_LENGTH_QUEUE_FOR_PENDING_CONNECTIONS;
        }

        public TCPServer()
        {
            Port = DEFAULT_PORT;
            CommonInitialization();
        }

        public TCPServer(int port)
        {
            Port = port;
            CommonInitialization();
        }

        public TCPServer(string ip)
        {
            IP = ip;
            CommonInitialization();
        }

        public TCPServer(string ip, int port)
        {
            IP = ip;
            Port = port;
            CommonInitialization();
        }
        #endregion

        #region Methodes
        #region Start
        public void Start()
        {
            Thread tcpThread = new Thread(new ThreadStart(ListenThread));
            tcpThread.Start();
        }
        #endregion
        #region Stop
        public void Stop()
        {
            try
            {
                if (srvSocket != null)
                    srvSocket.Close();
                started = false;
            }
            catch (Exception e)
            {
                if (TCPServerErrorEvent != null)
                    TCPServerErrorEvent(this, new TCPSocketExceptionEventArgs(e));
            }
        }
        #endregion

        #region ListenThread
        private void ListenThread()
        {
            try
            {
                IPHostEntry iphe;
                IPAddress ipaddr;
                IPEndPoint localep;

                //stop server if it was runing
                Stop();

                // resolve ip
                if (IP == "" || IP == null)
                {
                    ipaddr = System.Net.IPAddress.Any;
                }
                else
                {
                    try
                    {
                        // don't resolve if ip is like xxx.yyy.uuu.vvv
                        ipaddr = System.Net.IPAddress.Parse(IP);
                    }
                    catch
                    {
                        // else resolve
                        try
                        {
                            iphe = System.Net.Dns.GetHostEntry(IP);
                        }
                        catch (Exception e)
                        {
                            if (TCPServerErrorEvent != null)
                                TCPServerErrorEvent(this, new TCPSocketExceptionEventArgs(e));
                            return;
                        }
                        ipaddr = iphe.AddressList[0];
                    }
                }
                // make local end point
                localep = new IPEndPoint(ipaddr, Port);
                // create socket
                srvSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                srvSocket.Bind(localep);
                Port = ((IPEndPoint)srvSocket.LocalEndPoint).Port;// in case of null port given
                srvSocket.Listen(MaxLengthQueueForPendingConnections);

                started = true;
                // send TCPServerStartedEvent
                if (TCPServerStartedEvent != null)
                    TCPServerStartedEvent(this, new EventArgs());
                // accept connections
                Socket mySocket;
                while (true)
                {
                    mySocket = srvSocket.Accept();//blocking

                    // send event_Socket_Server_New_Connection
                    if (TCPServerNewConnectionEvent != null)
#if (!DEBUG)
						try// avoid to close server on some programmer's error in the eventhandler function
#endif
                    {
                        TCPServerNewConnectionEvent(this, new TCPSocketEventArgs(mySocket));
                    }
#if (!DEBUG)
						catch{}
#endif
                    
                }
            }
            catch (Exception e)
            {
                started = false;
                if (e is SocketException)
                {
                    SocketException se;
                    se = (SocketException)e;
                    if (se.ErrorCode == (int)WSA_ERROR.WSAEINTR)// accept canceled because of socket closing
                    {
                        if (TCPServerStoppedEvent != null)
                            TCPServerStoppedEvent(this, new EventArgs());
                        return;
                    }
                }
                // send event TCPServerError
                if (TCPServerErrorEvent != null)
                    TCPServerErrorEvent(this, new TCPSocketExceptionEventArgs(e));
                // close socket
                Stop();
                // send event TCPServerStopped
                if (TCPServerStoppedEvent != null)
                    TCPServerStoppedEvent(this, new EventArgs());
            }
        }
        #endregion
        #endregion
    }
    #endregion

    #region class TCPSocket
    public class TCPSocket
    {
        #region Events
        public event TCPSocketErrorEventHandler TCPSocketErrorEvent;
        public event TCPSocketClosedByRemoteSideEventHandler TCPSocketClosedByRemoteSideEvent;
        public event TCPSocketConnectedToRemoteHostEventHandler TCPSocketConnectedToRemoteHostEvent;
        public event TCPSocketSendProgressEventHandler TCPSocketSendProgressEvent;
        public event TCPSocketSendCompletedEventHandler TCPSocketSendCompletedEvent;
        public event TCPSocketDataArrivalEventHandler TCPSocketDataArrivalEvent;
        #endregion

        #region Const
        public const int DEFAULT_BUFFER_SIZE = 4096;
        #endregion

        #region Members
        private Socket tcpSocket;
        #region public	LocalIPEndPoint
        public IPEndPoint LocalIPEndPoint
        {
            get { return (IPEndPoint)tcpSocket.LocalEndPoint; }
        }
        #endregion
        private int LocalPort;
        #region public	RemoteIPEndPoint
        private IPEndPoint remoteIPEndPoint;
        public IPEndPoint RemoteIPEndPoint
        {
            get { return remoteIPEndPoint; }
        }
        #endregion
        #region public RemoteIP
        private string remoteIP;
        public string RemoteIP
        {
            get { return remoteIP; }
        }
        #endregion
        #region public RemotePort
        private int remotePort;
        public ushort RemotePort
        {
            get { return (ushort)remotePort; }
        }
        #endregion
        private int BufferSize;
        private bool LocallyClosed = false;
        #region public Connected
        public bool Connected
        {
            get { return tcpSocket.Connected; }
        }
        #endregion
        private bool bCancelSend = false;
        private System.Collections.Queue SendQueue;

        private ManualResetEvent MoreDataToSendEvent = new ManualResetEvent(false);
        private ManualResetEvent CloseEvent = new ManualResetEvent(false);
        private AutoResetEvent UnLockedSendQueueEvent = new AutoResetEvent(true);
#if !USE_TASK
        private Thread rcvThread = null;
        private Thread sendThread = null;
#endif
        #endregion

        #region Properties
        private int? maxDataSize = null;
        public int? MaxDataSize
        {
            get
            {
                return maxDataSize;
            }
            set
            {
                maxDataSize = value;
            }
        }
        #endregion

        #region Constructors
        #region konstruktory do uzycia w celu polaczenia ze zdalnym IP
        public TCPSocket()
        {
            BufferSize = DEFAULT_BUFFER_SIZE;
            LocalPort = 0;
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            CommonInitialization();
        }

        public TCPSocket(int bufferSize)
        {
            BufferSize = bufferSize;
            LocalPort = 0;
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            CommonInitialization();
        }

        public TCPSocket(int bufferSize, int localPort)
        {
            BufferSize = bufferSize;
            LocalPort = localPort;
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            CommonInitialization();
        }

        private void CommonInitialization()
        {
            // kolejka dla wysylanych komunikatow
            SendQueue = new System.Collections.Queue();

            //SetKeepAlive(tcpSocket, 10000, 10000);
        }
        #endregion

        #region konstrukotry do uzycia po 'accept'
        public TCPSocket(Socket socket)
        {
            BufferSize = DEFAULT_BUFFER_SIZE;
            CommonInitializationWithConnectedSocket(socket);
        }

        public TCPSocket(Socket socket, int bufferSize)
        {
            BufferSize = bufferSize;
            CommonInitializationWithConnectedSocket(socket);
        }

        private void CommonInitializationWithConnectedSocket(Socket socket)
        {
            tcpSocket = socket;
            remoteIPEndPoint = (IPEndPoint)tcpSocket.RemoteEndPoint;
            remoteIP = remoteIPEndPoint.Address.ToString();
            remotePort = (ushort)remoteIPEndPoint.Port;

#if USE_TASK
            System.Threading.Tasks.Task.Factory.StartNew(() => SendThread(), CancellationToken.None, System.Threading.Tasks.TaskCreationOptions.LongRunning, System.Threading.Tasks.TaskScheduler.Default);
            System.Threading.Tasks.Task.Factory.StartNew(() => ReceiveThread(), CancellationToken.None, System.Threading.Tasks.TaskCreationOptions.LongRunning, System.Threading.Tasks.TaskScheduler.Default);
#else
            sendThread = new Thread(new ThreadStart(SendThread));
            sendThread.Start();

            rcvThread = new Thread(new ThreadStart(ReceiveThread));
            rcvThread.Start();
#endif
            CommonInitialization();
        }
        #endregion
        #endregion

        #region Methodes
        #region Close
        AutoResetEvent CloseUnLockedEvent = new AutoResetEvent(true);
        public void Close()
        {
            try
            {
                CloseUnLockedEvent.WaitOne();
                CloseUnLockedEvent.Reset();
                if (!LocallyClosed)
                {
                    LocallyClosed = true;
                    if (tcpSocket != null)
                    {
                        CloseEvent.Set();

                        if (tcpSocket.Connected)
                        {
                            //RemotingHelper.CloseSocket(((IPEndPoint)tcpSocket.LocalEndPoint).Port);
                            tcpSocket.Shutdown(SocketShutdown.Both);
                        }

                        tcpSocket.Close();
                    }
                }
                CloseUnLockedEvent.Set();
            }
            catch (Exception e)
            {
                CloseUnLockedEvent.Set();// przed dead lockiem
                if (TCPSocketErrorEvent != null)
                    TCPSocketErrorEvent(this, new TCPSocketExceptionEventArgs(e));
            }
        }
        #endregion

        #region Reconnect
        /// <summary>
        /// Nie uzywac po 'accept': polaczenie juz wtedy jest
        /// </summary>
        public void Reconnect()
        {
            try
            {
                if (!LocallyClosed)
                    Close();
                // tworzymy nowy socket
                tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                FirstConnect(remoteIP, remotePort);
            }
            catch (Exception e)
            {
                if (TCPSocketErrorEvent != null)
                    TCPSocketErrorEvent(this, new TCPSocketExceptionEventArgs(e));
            }
        }
        #endregion

        #region Connect
        /// <summary>
        /// Nie uzywac po 'accept': polaczenie juz wtedy jest
        /// </summary>
        public void Connect(string ip, int port)
        {
            if (LocallyClosed)
            {
                remoteIP = ip;
                remotePort = port;
                Reconnect();
                return;
            }
            FirstConnect(ip, port);
        }
        private void FirstConnect(string ip, int port)
        {
            CloseEvent.Reset();
            remoteIP = ip;
            remotePort = port;
            IPEndPoint ipEndPoint = new IPEndPoint(System.Net.IPAddress.Any, LocalPort);
            tcpSocket.Bind(ipEndPoint);
            //uruchamiamy watek ConnectThread() (ConnectThread uruchamia watek ReceiveThread() po polaczeniu
            Thread connectThread = new Thread(new ThreadStart(ConnectThread));
            connectThread.Start();
        }
        #endregion

        #region ConnectThread
        /// <summary>
        /// laczy do zdalnego hosta i przygotowywuje socket do odbioru danych
        /// wysyla event: TcpSocketConnectedToRemoteHostEvent
        /// </summary>
        private void ConnectThread()
        {
            try
            {
                IPHostEntry ipHostEntry;
                IPAddress ipAddress;
                IPEndPoint remoteEndPoint;
                // tworzymy end point
                try
                {
                    // jesli ip podane w postaci: xxx.yyy.uuu.vvv to nie rozwiazujemy nazwy DNS
                    ipAddress = System.Net.IPAddress.Parse(remoteIP);
                }
                catch
                {
                    // w przeciwnym wypadku razwiazujemy nazwe DNS
                    ipHostEntry = System.Net.Dns.GetHostEntry(remoteIP);
                    ipAddress = ipHostEntry.AddressList[0];
                }
                remoteEndPoint = new IPEndPoint(ipAddress, remotePort);
                remoteIPEndPoint = remoteEndPoint;

                // laczenie
                tcpSocket.Connect(remoteIPEndPoint);

                // czyscimy kolejke danych do wyslania
                SendQueue.Clear();

                // uruchamiamy watek do wysylania danych
                Thread sendThread = new Thread(new ThreadStart(SendThread));
                sendThread.Start();

                // wysylamy event TCPSocketConnectedToRemoteHostevent
                if (TCPSocketConnectedToRemoteHostEvent != null)
                    TCPSocketConnectedToRemoteHostEvent(this, new EventArgs());

                // NIE TWORZYMY nowego watka poniewaz jestesmy w watku ConnectThread 
                // ktory po porawnym polaczeniu staje sie watkiem odbierajacym
                ReceiveThread();
            }
            catch (Exception e)
            {
                if (TCPSocketErrorEvent != null)
                    TCPSocketErrorEvent(this, new TCPSocketExceptionEventArgs(e));
                Close();
            }
        }
        #endregion

        #region CancelSend
        public void CancelSend()
        {
            bCancelSend = true;
        }
        #endregion

        #region SendThread
        /// <summary>
        /// wysyla dane do zdalnego hosta z kolejki SendQueue
        /// wysyla event: TCPSocketSendProgressEvent
        /// wysyla event: TCPSocketSendCompletedEvent
        /// </summary>
        private void SendThread()
        {
            TCPSendObject sendObject = null;

            try
            {
                byte[] local_buffer;
                int bufsize;
                int nbbytesent;
                int nbbyteremaining;
                int data_send_size;
                int cpt;
                bool bSentCompleted = true;

                System.Threading.WaitHandle[] waitHandles = new System.Threading.WaitHandle[] { CloseEvent, MoreDataToSendEvent };

                while (true)
                {
                    int waitResult = System.Threading.WaitHandle.WaitAny(waitHandles);
                    if (waitResult == 0)
                        // CloseEvent - exit thread
                        return;

                    MoreDataToSendEvent.Reset();
                    while (SendQueue.Count > 0) // tak dlugo az w kolejce sa dane do wyslania
                    {
                        // z kolejki pobieramy dane do wyslania
                        UnLockedSendQueueEvent.WaitOne();
                        sendObject = (TCPSendObject)SendQueue.Dequeue();
                        UnLockedSendQueueEvent.Set();
                        local_buffer = sendObject.Buffer;

                        bufsize = local_buffer.Length;

                        nbbytesent = 0;
                        nbbyteremaining = bufsize;
                        data_send_size = 0;
                        cpt = 0;

                        while (nbbyteremaining > BufferSize)
                        {
                            if (bCancelSend)
                            {
                                UnLockedSendQueueEvent.WaitOne();
                                SendQueue.Clear();
                                UnLockedSendQueueEvent.Set();

                                bCancelSend = false;
                                nbbyteremaining = 0;
                                bSentCompleted = false;
                                break;
                            }

                            if (LocallyClosed) return;
                            if (!tcpSocket.Connected) return;

                            // send a buffer with length=buffer_size
                            data_send_size = tcpSocket.Send(local_buffer, cpt * BufferSize, BufferSize, SocketFlags.None);
                            nbbytesent += data_send_size;
                            nbbyteremaining -= data_send_size;
                            cpt++;

                            //wysylamy event TCPSocketSendProgressEvent
                            if (TCPSocketSendProgressEvent != null)
                                TCPSocketSendProgressEvent(this, new TCPSocketSendProgressEventArgs(nbbytesent, nbbyteremaining, bufsize));
                        }
                        if (nbbyteremaining > 0)//nbbyteremaining < buffer_size we should send a buffer with length=nbbyteremaining
                        {
                            // send the end of the data
                            if (!tcpSocket.Connected) return;
                            data_send_size = tcpSocket.Send(local_buffer, cpt * BufferSize, nbbyteremaining, SocketFlags.None);
                            nbbytesent += data_send_size;
                            nbbyteremaining -= data_send_size;
                            //wysylamy event TCPSocketSendProgressEvent
                            if (TCPSocketSendProgressEvent != null)
                                TCPSocketSendProgressEvent(this, new TCPSocketSendProgressEventArgs(nbbytesent, nbbyteremaining, bufsize));
                        }
                        // jesli wszystko juz wyslane
                        if (bSentCompleted)
                        {
                            // wysylamy event TCPSocketSendCompletedEvent
                            if (TCPSocketSendCompletedEvent != null)
                                TCPSocketSendCompletedEvent(this, new TCPSocketSendCompletedEventArgs(sendObject));
                        }
                    }
                    bSentCompleted = true;
                }
            }
            catch (Exception e)
            {
                // usuwamy wszystkie dane do wyslania
                UnLockedSendQueueEvent.WaitOne();
                SendQueue.Clear();
                UnLockedSendQueueEvent.Set();

                // restart watka do wysylania danych
                Thread sendThread = new Thread(new ThreadStart(SendThread));
                sendThread.Start();

                if (LocallyClosed)
                    return;

                if (e is SocketException)
                {
                    SocketException se = (SocketException)e;
                    if (se.ErrorCode == (int)WSA_ERROR.WSAEINTR) // wywolano metode Close
                        return;

                    Close();
                    if (se.ErrorCode == (int)WSA_ERROR.WSAECONNABORTED) // socket zamkniety przez zdalny host
                    {
                        if (TCPSocketClosedByRemoteSideEvent != null)
                            this.TCPSocketClosedByRemoteSideEvent(this, new EventArgs());
                        return;
                    }
                }
                if (TCPSocketErrorEvent != null)
                    TCPSocketErrorEvent(this, new TCPSocketExceptionEventArgs(e));
            }
        }

        /// <summary>
        /// used for sending bytes
        /// launch thread for sending or enqueue data if a thread is allready used for sending
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(byte[] buffer)
        {
            if (buffer == null) return;
            if (buffer.Length == 0) return;

            UnLockedSendQueueEvent.WaitOne();
            SendQueue.Enqueue(new TCPSendObject(buffer, null));
            UnLockedSendQueueEvent.Set();
            MoreDataToSendEvent.Set();
        }
        /// <summary>
        /// used for sending bytes with object Tag
        /// launch thread for sending or enqueue data if a thread is allready used for sending
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(byte[] buffer, object tag)
        {
            if (buffer == null) return;
            if (buffer.Length == 0) return;

            UnLockedSendQueueEvent.WaitOne();
            SendQueue.Enqueue(new TCPSendObject(buffer, tag));
            UnLockedSendQueueEvent.Set();
            MoreDataToSendEvent.Set();
        }
        /// <summary>
        /// used for sending string
        /// launch thread for sending or enqueue data if a thread is allready used for sending
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(string buffer)
        {
            if (buffer == null) return;
            if (buffer.Length == 0) return;
            byte[] sendBytes;
            sendBytes = Encoding.Default.GetBytes(buffer);
            Send(sendBytes);
        }
        #endregion

        #region ReceiveThread
        /// <summary>
        ///  watek odbierajacy dane
        ///  wysyla event: TcpSocketDataArrivalEvent po kazdym odebraniu danych
        ///  wysyla event: TcpSocketClosedByRemoteSide i zamyka 'socket' jesli 'remoteside' rozlaczyla sie
        /// </summary>
        private void ReceiveThread()
        {
            try
            {
                LocallyClosed = false;

                int bufferSize    = DEFAULT_BUFFER_SIZE;
                byte[] buffer     = new byte[DEFAULT_BUFFER_SIZE];
                int bytesReceived = 0;

                while (true)
                {
                    if (TCPSocketDataArrivalEvent == null)
                    {
                        Thread.Sleep(10);
                        continue;
                    }

                    if (tcpSocket.Poll(-1, SelectMode.SelectRead))
                    {
                        if (tcpSocket.Available == 0)
                            break;

                        bytesReceived = 0;
                        // resize buffer if available size exceeds the allocated receive buffer
                        if (tcpSocket.Available > bufferSize)
                        {
                            Array.Resize(ref buffer, tcpSocket.Available);
                            bufferSize = tcpSocket.Available;
                        }
                        // do recieve
                        bytesReceived = tcpSocket.Receive(buffer, 0, bufferSize, SocketFlags.None);

                        // callback any listeners with fresh data here
                        if (bytesReceived > 0)
                        {
                            if (TCPSocketDataArrivalEvent != null)
#if (!DEBUG)
							try// avoid to close server on some programmer's error in the eventhandler function
#endif
                            {
                                byte[] eventBuffer = new byte[bytesReceived];
                                Buffer.BlockCopy(buffer, 0, eventBuffer, 0, bytesReceived);
                                TCPSocketDataArrivalEvent(this, new TCPSocketReceiveEventArgs(eventBuffer));
                            }
#if (!DEBUG)
							catch{}
#endif

                        }
                    }
                }
                #region // Inna wersja
                /*
                byte[] RecvBytes;
                int tmp_recvbytes;
                int available_size = 0;
                int old_available_size;
                LocallyClosed = false; 

                do
                {

                    RecvBytes = new byte[maxDataSize.HasValue ? Math.Min(maxDataSize.Value, available_size) : available_size];
                    if (!tcpSocket.Connected) return;
                    // receive data
                    tmp_recvbytes = tcpSocket.Receive(RecvBytes, RecvBytes.Length, SocketFlags.None);//blocking

                    // old_available_size contain the amount of data recieved by the last receive
                    old_available_size = available_size;
                    // get the amount of data that remain to receive
                    available_size = tcpSocket.Available;

                    if (tmp_recvbytes > 0)
                    {
                        if (TCPSocketDataArrivalEvent != null)
#if (!DEBUG)
							try// avoid to close server on some programmer's error in the eventhandler function
#endif
                        {
                            TCPSocketDataArrivalEvent(this, new TCPSocketReceiveEventArgs(RecvBytes));
                        }
#if (!DEBUG)
							catch{}
#endif
                    }

                }
                while (available_size != 0 || old_available_size != 0);// while socket not closed by remote side
                */
                #endregion

                // socket closed by remote side
                // send event_Socket_Data_Closed_by_Remote_Side
                if (!LocallyClosed)
                {
                    if (TCPSocketClosedByRemoteSideEvent != null)
                        TCPSocketClosedByRemoteSideEvent(this, new EventArgs());
                    Close();
                }
            }
            catch (Exception e)
            {
                if (e is SocketException)
                {
                    SocketException se = (SocketException)e;
                    if (se.ErrorCode == (int)WSA_ERROR.WSAEINTR) // close methode has been called
                        return;

                    if (se.ErrorCode == (int)WSA_ERROR.WSAECONNABORTED ||
                        se.ErrorCode == (int)WSA_ERROR.WSAECONNRESET) // socket closed by remote side
                    {
                        if (LocallyClosed) return;
                        if (TCPSocketClosedByRemoteSideEvent != null)
                            TCPSocketClosedByRemoteSideEvent(this, new EventArgs());
                        return;
                    }

                }
                // if methode of data socket called during it's closing
                if (e is System.ObjectDisposedException) return;

                Close();
                if (TCPSocketErrorEvent != null)
                    TCPSocketErrorEvent(this, new TCPSocketExceptionEventArgs(e));
            }
        }
        #endregion

        #region private SetKeepAlive
        private bool SetKeepAlive(Socket sock, ulong time, ulong interval)
        {
            const int bytesperlong = 4;
            const int bitsperbyte = 8;

            try
            {
                // resulting structure
                byte[] SIO_KEEPALIVE_VALS = new byte[3 * bytesperlong];

                // array to hold input values
                ulong[] input = new ulong[3];

                // put input arguments in input array
                if (time == 0 || interval == 0) // enable disable keep-alive
                    input[0] = (0UL); // off
                else
                    input[0] = (1UL); // on

                input[1] = (time); // time millis
                input[2] = (interval); // interval millis

                // pack input into byte struct
                for (int i = 0; i < input.Length; i++)
                {
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 3] = (byte)(input[i] >> ((bytesperlong - 1) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 2] = (byte)(input[i] >> ((bytesperlong - 2) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 1] = (byte)(input[i] >> ((bytesperlong - 3) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 0] = (byte)(input[i] >> ((bytesperlong - 4) * bitsperbyte) & 0xff);
                }
                // create bytestruct for result (bytes pending on server socket)
                byte[] result = BitConverter.GetBytes(0);
                // write SIO_VALS to Socket IOControl
                sock.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, 1);
                sock.IOControl(IOControlCode.KeepAliveValues, SIO_KEEPALIVE_VALS, result);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        #endregion
        #endregion
    }
    #endregion
}