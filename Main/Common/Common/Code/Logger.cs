﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace IMR.Suite.Common
{
    #region LogData
    [Serializable]
    public class LogData
    {
        public uint EventID;
        public LogLevel LogLevel;
        public string Message;

        public LogData(uint EventID, LogLevel LogLevel, string Message)
        {
            this.EventID = EventID;
            this.LogLevel = LogLevel;
            this.Message = Message;
        }
    }
    #endregion

    #region enum LogLevel
    public enum LogLevel : byte
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5,
        Off = 6,
    }
    #endregion


#if WindowsCE 
    public class Logger : NLog.Logger
    {
        private static NLog.Logger logger = null;

    #region Constructor
        public Logger()
        {
            logger = NLog.LogManager.GetLogger("WinCE");
        }
    #endregion
    #region Add
        public static void Add(LogData EventID, params object[] args)
        {
            Add(Enums.Module.Unknown, EventID, args);
        }
        public static void Add(Enums.Module Module, LogData EventID, params object[] args)
        {
            if (logger == null)
                logger = NLog.LogManager.GetLogger("WinCE");
            string LoggerName = logger.Name;

            NLog.LogEventInfo lei = new NLog.LogEventInfo(NLog.LogLevel.FromOrdinal((int)EventID.LogLevel), LoggerName, null, EventID.Message, args);

            // set the per-log context data
            // this data can be retrieved using ${event-context:EventID}
            lei.Context["ModuleID"] = (int)Module;
            lei.Context["EventID"] = EventID.EventID;
            lei.Context["FuncName"] = LoggerName;

            logger.Log(typeof(Logger), lei);
        }
    #endregion
    #region IsDebugEnabled
        public new static bool IsDebugEnabled
        {
            get { return logger != null ? logger.IsDebugEnabled : false; }
        }
    #endregion
    #region IsErrorEnabled
        public new static bool IsErrorEnabled
        {
            get { return logger != null ? logger.IsErrorEnabled : false; }
        }
    #endregion
    #region IsFatalEnabled
        public new static bool IsFatalEnabled
        {
            get { return logger != null ? logger.IsFatalEnabled : false; }
        }
    #endregion
    #region IsInfoEnabled
        public new static bool IsInfoEnabled
        {
            get { return logger != null ? logger.IsInfoEnabled : false; }
        }
    #endregion
    #region IsTraceEnabled
        public new static bool IsTraceEnabled
        {
            get { return logger != null ? logger.IsTraceEnabled : false; }
        }
    #endregion
    #region IsWarnEnabled
        public new static bool IsWarnEnabled
        {
            get { return logger != null ? logger.IsWarnEnabled : false; }
        }
    #endregion

    #region IsEnabled
        public static bool IsEnabled(LogLevel level)
        {
            return logger != null ? logger.IsEnabled(NLog.LogLevel.FromOrdinal((int)level)) : false;
        }
    #endregion
    }
#elif Android
    public class Logger/* : NLog.Logger*/
    {
        public static void Add(LogData EventID, params object[] args)
        {
            Add(Enums.Module.Unknown, EventID, args);
        }

        public static void Add(Enums.Module Module, LogData EventID, params object[] args)
        {
        }

    }
#else
    public class Logger : NLog.Logger
    {
        private static NLog.Logger logger = null;

        #region Constructor
        public Logger()
        {
            logger = NLog.LogManager.GetCurrentClassLogger();
        }
        #endregion

        #region IsDebugEnabled
        public new static bool IsDebugEnabled
        {
            get { return logger != null ? logger.IsDebugEnabled : false; }
        }
        #endregion
        #region IsErrorEnabled
        public new static bool IsErrorEnabled
        {
            get { return logger != null ? logger.IsErrorEnabled : false; }
        }
        #endregion
        #region IsFatalEnabled
        public new static bool IsFatalEnabled
        {
            get { return logger != null ? logger.IsFatalEnabled : false; }
        }
        #endregion
        #region IsInfoEnabled
        public new static bool IsInfoEnabled
        {
            get { return logger != null ? logger.IsInfoEnabled : false; }
        }
        #endregion
        #region IsTraceEnabled
        public new static bool IsTraceEnabled
        {
            get { return logger != null ? logger.IsTraceEnabled : false; }
        }
        #endregion
        #region IsWarnEnabled
        public new static bool IsWarnEnabled
        {
            get { return logger != null ? logger.IsWarnEnabled : false; }
        }
        #endregion

        #region IsEnabled
        public static bool IsEnabled(LogLevel level)
        {
            return logger != null ? logger.IsEnabled(NLog.LogLevel.FromOrdinal((int)level)) : false;
        }
        #endregion

        #region Add
        //po przejściu na Framerowk >= 4.5 będzie można użyć CallerMemberName i ni trzeba bedzie 'latać' po stosie
        //public static void Add(LogData EventID, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "", params object[] args)
        public static void Add(LogData EventID, params object[] args)
        {
            Add(Enums.Module.Unknown, EventID, args);
        }
        //po przejściu na Framerowk >= 4.5 będzie można użyć CallerMemberName i ni trzeba bedzie 'latać' po stosie
        //public static void Add(Enums.Module Module, LogData EventID, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "", params object[] args)
        public static void Add(Enums.Module Module, LogData EventID, params object[] args)
        {
            if (logger == null)
                logger = NLog.LogManager.GetCurrentClassLogger();

            bool bDoNotAddLog = true;
            if (EventID.LogLevel == LogLevel.Trace && IsTraceEnabled) bDoNotAddLog = false;
            if (EventID.LogLevel == LogLevel.Debug && IsDebugEnabled) bDoNotAddLog = false;
            if (EventID.LogLevel == LogLevel.Info && IsInfoEnabled) bDoNotAddLog = false;
            if (EventID.LogLevel == LogLevel.Warn && IsWarnEnabled) bDoNotAddLog = false;
            if (EventID.LogLevel == LogLevel.Error && IsErrorEnabled) bDoNotAddLog = false;
            if (EventID.LogLevel == LogLevel.Fatal && IsFatalEnabled) bDoNotAddLog = false;

            if (bDoNotAddLog)
                return;

            logger.Log(typeof(Logger),  GetLogEventInfo(Module, EventID, args));
        }
        #endregion

        #region GetLogMessage
        public static string GetLogMessage(NLog.Targets.Target target, Enums.Module Module, LogData EventID, params object[] args)
        {
            return ((NLog.Targets.TargetWithLayout)target).Layout.Render(GetLogEventInfo(Module, EventID, args));
        }
        #endregion

        #region private GetLogEventInfo
        public static NLog.LogEventInfo GetLogEventInfo(Enums.Module Module, LogData EventID, params object[] args)
        {
            string LoggerName = string.Empty;

            StackTrace stackTrace = null;
            StackFrame[] stackFrames = null;
            try
            {
                int funcNameStackIndex = 0;
                stackTrace = new StackTrace(0, false);
                if (stackTrace != null)
                {
                    if ((stackFrames = stackTrace.GetFrames()) != null)
                    {
                        System.Reflection.MethodBase method;
                        Type declaringType;
                        string methodName = string.Empty, reflectedTypeName = string.Empty, declaringTypeName = string.Empty;

                        bool bFuncFound = false;
                        while (!bFuncFound && stackTrace.FrameCount >= funcNameStackIndex)
                        {
                            method = stackFrames[funcNameStackIndex].GetMethod();
                            methodName = method.Name;
                            declaringType = method.DeclaringType;
                            reflectedTypeName = method.ReflectedType.FullName;
                            declaringTypeName = declaringType.Name;

                            if (declaringType.IsDefined(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), false))
                            {
                                funcNameStackIndex++;
                                continue;
                            }

                            if (reflectedTypeName.Contains("Logger") ||reflectedTypeName.Contains("TraceExecutionTime"))
                            {
                                funcNameStackIndex++;
                                continue;
                            }

                            if (methodName.Equals("Log") || methodName.Equals("AddLog"))
                            {
                                funcNameStackIndex++;
                                continue;
                            }
                            bFuncFound = true;
                        }

                        int idx = methodName.LastIndexOf('.');
                        if (idx != -1)
                            methodName = methodName.Substring(idx + 1);

                        idx = reflectedTypeName.LastIndexOf('.');
                        if (idx != -1)
                            reflectedTypeName = reflectedTypeName.Substring(idx + 1);

                        idx = declaringTypeName.LastIndexOf('.');
                        if (idx != -1)
                            declaringTypeName = declaringTypeName.Substring(idx + 1);

                        //LoggerName = string.Concat(declaringTypeName, ".", methodName);
                        LoggerName = string.Concat(reflectedTypeName, ".", methodName);
                    }
                }
            }
            catch (Exception)
            {
                LoggerName = string.Empty;
            }

            NLog.LogEventInfo lei = new NLog.LogEventInfo(NLog.LogLevel.FromOrdinal((int)EventID.LogLevel), LoggerName, null, EventID.Message, args);

            // set the per-log context data
            // this data can be retrieved using ${event-context:EventID}
            lei.Properties["ModuleID"] = (int)Module;
            lei.Properties["EventID"] = EventID.EventID;
            lei.Properties["FuncName"] = LoggerName;
            lei.Properties["DbHost"] = IMRLog.DbHost;
            lei.Properties["DbDataBase"] = IMRLog.DbDataBase;
            lei.Properties["DbUserName"] = IMRLog.DbUserName;
            lei.Properties["DbPassword"] = IMRLog.DbPassword;

            return lei;
        }
        #endregion
    }
#endif

    public class IMRLog
    {
        public static LogLevel MinLogLevel = LogLevel.Error;
        public static string DbHost = "localhost";
        public static string DbDataBase = "IMR_Suite_DW";
        public static string DbUserName;
        public static string DbPassword;

        public static void AddToLog(LogData EventData, params object[] Parameters)
        {
            AddToLog(Enums.Module.Unknown, EventData, Parameters);
        }

        public static void AddToLog(Enums.Module module, LogData EventData, params object[] Parameters)
        {
            AddToLog(module, true, EventData, Parameters);
        }

        public static void AddToLog(Enums.Module module, bool useDefaultLogLevel, LogData EventData, params object[] Parameters)
        {
            if (!useDefaultLogLevel || EventData.LogLevel >= MinLogLevel)
                //Logger.Add(DbHost, DbDataBase, DbUserName, DbPassword, module, EventData, Parameters);
                Logger.Add(module, EventData, Parameters);

            if (EventData.LogLevel > LogLevel.Info)
            {
#if DEBUG
                try
                {
                    System.Diagnostics.Debug.WriteLine(String.Format(EventData.Message, Parameters));
                }
                catch
                {
                }
#endif
            }
        }
    }
}
