﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using IMR.Suite.Common.Code;

namespace IMR.Suite.Common
{
    [Serializable]
    public class AssemblyWrapper : MarshalByRefObjectDisposable
    {
        #region Properties
		#region ExecutingAssembly
		public static Assembly ExecutingAssembly
		{
			get
			{
				Assembly assembly = null;
				assembly = Assembly.GetEntryAssembly();
				if (assembly == null)
					assembly = Assembly.GetExecutingAssembly(); //bblach zaleczenie dla aplikacji webowych poniewaz Assembly.GetEntryAssembly() zwraca null dla ASP.NET

				return assembly;
			}
		}
		#endregion

		#region Name
        public static string Name
        {
            get
            {
				if (ExecutingAssembly != null) //jeśli juz naprawde nie znajdziemy assembly nie chcemy tutaj dostac wyjątku
					return new AssemblyName(ExecutingAssembly.FullName).Name;
                return null;
            }
        }
        #endregion
        #region Version
        public static string Version
        {
            get
            {
				if (ExecutingAssembly != null)//jeśli juz naprawde nie znajdziemy assembly nie chcemy tutaj dostac wyjątku
					return new AssemblyName(ExecutingAssembly.FullName).Version.ToString();
                return null;
            }
        }
        #endregion
        #region NameAndVersion
        public static string NameAndVersion
        {
            get
            {
				if (ExecutingAssembly != null)//jeśli juz naprawde nie znajdziemy assembly nie chcemy tutaj dostac wyjątku
                {
					AssemblyName assemblyName = new AssemblyName(ExecutingAssembly.FullName);
                    return string.Format("{0}, {1}", assemblyName.Name, assemblyName.Version.ToString());
                }
                return null;
            }
        }
        #endregion

		#region Title
		public static string Title
		{
			get
			{
				if (ExecutingAssembly == null)
					return "";

				// Get all Title attributes on this assembly
				object[] attributes = ExecutingAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				// If there is at least one Title attribute
				if (attributes.Length > 0)
				{
					// Select the first one
					AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
					// If it is not an empty string, return it
					if (titleAttribute.Title != "")
						return titleAttribute.Title;
				}
				// If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
				return System.IO.Path.GetFileNameWithoutExtension(ExecutingAssembly.CodeBase);
			}
		}
		#endregion
		#region Company
		public static string Company
		{
			get
			{
				if (ExecutingAssembly == null)
					return "";

				// Get all Company attributes on this assembly
				object[] attributes = ExecutingAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				// If there aren't any Company attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Company attribute, return its value
				return ((AssemblyCompanyAttribute)attributes[0]).Company;
			}
		}
		#endregion
		#region Description
		public static string Description
		{
			get
			{
				if (ExecutingAssembly == null)
					return "";

				// Get all Description attributes on this assembly
				object[] attributes = ExecutingAssembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				// If there aren't any Description attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Description attribute, return its value
				return ((AssemblyDescriptionAttribute)attributes[0]).Description;
			}
		}
		#endregion
		#region Copyright
		public static string Copyright
		{
			get
			{
				if (ExecutingAssembly == null)
					return "";

				// Get all Copyright attributes on this assembly
				object[] attributes = ExecutingAssembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				// If there aren't any Copyright attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Copyright attribute, return its value
				return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
			}
		}
		#endregion
		#region Product
		public static string Product
		{
			get
			{
				if (ExecutingAssembly == null)
					return "";

				// Get all Product attributes on this assembly
				object[] attributes = ExecutingAssembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				// If there aren't any Product attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Product attribute, return its value
				return ((AssemblyProductAttribute)attributes[0]).Product;
			}
        }
        #endregion
        #endregion

        #region static LoadAssembly
        /// <summary>
        /// WAŻNE: jeśli DLLka jest ładowana z innego katalogu niż wywołujący EXE to należy obsłużyć eventy:  <para/>
        ///        1. forReflectionOnly == true => AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += XXXXX <para/>
        ///        2. forReflectionOnly == false => AppDomain.CurrentDomain.AssemblyResolve += XXXXX <para/>
        /// </summary>
        public static Assembly LoadAssembly(string filePath, bool forReflectionOnly = false)
        {
            Assembly assembly = null;
            try
            {
                AssemblyName assemblyNameToLoad = AssemblyName.GetAssemblyName(filePath);

                if (forReflectionOnly)
                {
                    // sprawdzamy czy jest już załadowana ta wersja
                    Assembly[] alreadyLoaded = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies();
                    if (alreadyLoaded.Length > 0)
                    {
                        assembly = alreadyLoaded.FirstOrDefault(w => w.GetName().FullName == assemblyNameToLoad.FullName);
                        if (assembly != null)
                            return assembly; // jest już załadowana, dokłanie ta wersja
                    }

                    // nie jest załadowana, więc ładujemy ale
                    // !!! UWAGA !!!!
                    // !!! ładowanie przez 'ręczne' wczytanie pliku jako talicy bajtów ponieważ tylko w ten sposób Framework nie blokuje pliku (AccessDenied przy zmianie, usunięciu) !!!
                    assembly = Assembly.ReflectionOnlyLoad(File.ReadAllBytes(filePath));
                }
                else
                {
                    // sprawdzamy czy jest już załadowana ta wersja
                    Assembly[] alreadyLoaded = AppDomain.CurrentDomain.GetAssemblies();
                    if (alreadyLoaded.Length > 0)
                    {
                        assembly = alreadyLoaded.FirstOrDefault(w => w.GetName().FullName == assemblyNameToLoad.FullName);
                        if (assembly != null)
                            return assembly; // jest już załadowana, dokłanie ta wersja
                    }
                    // nie jest załadowana, więc ładujemy ale
                    // !!! UWAGA !!!!
                    // !!! ładowanie przez 'ręczne' wczytanie pliku jako talicy bajtów ponieważ tylko w ten sposób Framework nie blokuje pliku (AccessDenied przy zmianie, usunięciu) !!!
                    assembly = Assembly.Load(File.ReadAllBytes(filePath));
                }
            }
            catch { }
            return assembly;
        }


        #endregion
        #region static GetAssemblyTypes
        public static Type[] GetAssemblyTypes(string filePath, bool forReflectionOnly = false)
        {
            // Gdyby był VS2015 to by było tylko tyle: 
            // return AssemblyWrapper.LoadAssembly(filePath, forReflectionOnly)?.GetTypes();
            
            // A tak trzeba więcej :)
            Assembly assembly = AssemblyWrapper.LoadAssembly(filePath, forReflectionOnly);
            if (assembly != null)
                return assembly.GetTypes();
            return null;
        }
        #endregion
        #region static GetAssemblyName
        public static AssemblyName GetAssemblyName(string filePath)
        {
            // Tak chyba jest znacznie szybciej i prościej niż to co zakomentowane niżej
            return AssemblyName.GetAssemblyName(filePath);
            #region prev Version
            /*
            System.Reflection.AssemblyName assemblyName = null;
            AppDomain appDomain = null;

            FileStream fileStreamAssembly = null;
            BinaryReader binaryReaderAssembly = null;

            try
            {
                appDomain = AppDomain.CreateDomain("LoadAssemblyForReflectionOnly");

                fileStreamAssembly = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                binaryReaderAssembly = new BinaryReader(fileStreamAssembly);
                byte[] bytes = binaryReaderAssembly.ReadBytes((int)fileStreamAssembly.Length);
                Assembly assembly = AppDomain.CurrentDomain.Load(bytes);

                assemblyName = assembly.GetName();
            }
            finally
            {
                if (binaryReaderAssembly != null)
                    binaryReaderAssembly.Close();
                if (fileStreamAssembly != null)
                {
                    fileStreamAssembly.Close();
                    fileStreamAssembly.Dispose();
                }

                if (appDomain != null)
                    AppDomain.Unload(appDomain);
            }
            return assemblyName;
            */
            #endregion
        }
        #endregion
     }

    #region Old Version
    /*
	[Serializable]
	public class AssemblyWrapper : MarshalByRefObjectDisposable
	{
		#region Properties
		#region Name
		public static string Name
		{
			get { return new AssemblyName(Assembly.GetEntryAssembly().FullName).Name; }
		}
		#endregion
		#region Version
		public static string Version
		{
			get { return new AssemblyName(Assembly.GetEntryAssembly().FullName).Version.ToString(); }
		}
		#endregion
		#region NameAndVersion
		public static string NameAndVersion
		{
			get
			{
				AssemblyName assembly = new AssemblyName(Assembly.GetEntryAssembly().FullName);
				return string.Format("{0}, {1}", assembly.Name, assembly.Version.ToString());
			}
		}
		#endregion
		#endregion


		#region static GetAssemblyTypes
		public static Type[] GetAssemblyTypes(string filePath)
		{
			Type[] assemblyTypes = null;
			AppDomain appDomain = null;
			try
			{
				appDomain = AppDomain.CreateDomain("LoadAssemblyForReflectionOnly");

				appDomain.ReflectionOnlyAssemblyResolve += new ResolveEventHandler(OnReflectionOnlyAssemblyResolve);
				AssemblyWrapper assemblyWrapper = (AssemblyWrapper)appDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(AssemblyWrapper).FullName);
				assemblyWrapper.LoadAssemblyTypes(filePath);
				assemblyTypes = assemblyWrapper.assemblyTypes;
			}
			catch (Exception ex)
			{
			}
			finally
			{
				if (appDomain != null)
					AppDomain.Unload(appDomain);
			}
			return assemblyTypes;
		}
		#endregion
		#region static GetAssemblyName
		public static AssemblyName GetAssemblyName(string filePath)
		{
			System.Reflection.AssemblyName assemblyName = null;
			AppDomain appDomain = null;
			try
			{
				appDomain = AppDomain.CreateDomain("LoadAssemblyForReflectionOnly");
				AssemblyWrapper assemblyWrapper = (AssemblyWrapper)appDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().CodeBase, typeof(AssemblyWrapper).FullName);
				assemblyWrapper.LoadAssemblyName(filePath);
				assemblyName = assemblyWrapper.assemblyName;
			}
			finally
			{
				if (appDomain != null)
					AppDomain.Unload(appDomain);
			}
			return assemblyName;
		}
		#endregion

		#region LoadAssemblyTypes
		public Type[] assemblyTypes = null;
		public void LoadAssemblyTypes(string filePath)
		{
			FileStream fileStreamAssembly = null;
			BinaryReader binaryReaderAssembly = null;
			try
			{
				fileStreamAssembly = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				binaryReaderAssembly = new BinaryReader(fileStreamAssembly);
				byte[] bytes = binaryReaderAssembly.ReadBytes((int)fileStreamAssembly.Length);
				Assembly assembly = Assembly.ReflectionOnlyLoad(bytes);
				assemblyTypes = assembly.GetTypes();
			}
			catch { }
			finally
			{
				if (binaryReaderAssembly != null)
					binaryReaderAssembly.Close();
				if (fileStreamAssembly != null)
				{
					fileStreamAssembly.Close();
					fileStreamAssembly.Dispose();
				}
			}
		}
		#endregion
		#region LoadAssemblyName
		public AssemblyName assemblyName = null;
		public void LoadAssemblyName(string filePath)
		{
			FileStream fileStreamAssembly = null;
			BinaryReader binaryReaderAssembly = null;
			try
			{
				fileStreamAssembly = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				binaryReaderAssembly = new BinaryReader(fileStreamAssembly);
				byte[] bytes = binaryReaderAssembly.ReadBytes((int)fileStreamAssembly.Length);
				Assembly assembly = Assembly.ReflectionOnlyLoad(bytes);
				assemblyName = assembly.GetName();
			}
			catch { }
			finally
			{
				if (binaryReaderAssembly != null)
					binaryReaderAssembly.Close();
				if (fileStreamAssembly != null)
				{
					fileStreamAssembly.Close();
					fileStreamAssembly.Dispose();
				}
			}
		}
		#endregion

		#region OnReflectionOnlyAssemblyResolve
		static Assembly OnReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
		{
			return System.Reflection.Assembly.ReflectionOnlyLoad(args.Name);
		}
		#endregion
	}
*/
    #endregion
}
