﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using IMR.Suite.Common;
using System.Web;

namespace IMR.Suite.Common
{
    public class FTPClient
    {
        #region Variables & Properties
        public bool TestMode = false;
        private string ftpServerAddress;

        public delegate void UploadProgressDelegate(long AllBytes, long UploadedBytes);
        public UploadProgressDelegate UploadProgress;

        public string FtpServerAddress
        {
            get
            {
                return ftpServerAddress;
            }
            set
            {
                ftpServerAddress = value;
            }
        }
        private string ftpLogin;
        public string FtpLogin
        {
            get
            {
                return ftpLogin;
            }
            set
            {
                ftpLogin = value;
            }
        }
        private string ftpPassword;
        public string FtpPassword
        {
            get
            {
                return ftpPassword;
            }
            set
            {
                ftpPassword = value;
            }
        }
        //private string TEMP_DIR = Consts.Directory.TEMP;
        //public string DIRECTORY_IN
        //{
        //    get
        //    {
        //        if (TestMode)
        //            return Consts.Directory.JCI_IN_TEST;
        //        else
        //            return Consts.Directory.JCI_IN;
        //    }
        //}
        //public string DIRECTORY_OUT
        //{
        //    get
        //    {
        //        if (TestMode)
        //            return Consts.Directory.JCI_OUT_TEST;
        //        else
        //            return Consts.Directory.JCI_OUT;
        //    }
        //}
        //private Uri FTP_URI_DIR_IN = null;
        //private Uri FTP_URI_DIR_OUT = null;

        public DateTime LastUploadDateTime = DateTime.Now;
        #endregion
        #region Constructor
        public FTPClient()
        {

        }
        #endregion

        #region Initalize
        public void Initialize(string ftpServerAddress, string ftpLogin, string ftpPassword)
        {
            Initialize(ftpServerAddress, ftpLogin, ftpPassword, false);
        }

        public void Initialize(string ftpServerAddress, string ftpLogin, string ftpPassword, bool testMode)
        {
            this.FtpServerAddress = ftpServerAddress;
            this.FtpLogin = ftpLogin;
            this.FtpPassword = ftpPassword;
            this.TestMode = testMode;

            //if (String.IsNullOrEmpty(UriPrefix))
            //{
            //    FTP_URI_DIR_IN = new Uri(String.Format("ftp://{0}/{1}/", FtpServerAddress, DIRECTORY_IN));
            //    FTP_URI_DIR_OUT = new Uri(String.Format("ftp://{0}/{1}/", FtpServerAddress, DIRECTORY_OUT));
            //}
            //else
            //{
            //    FTP_URI_DIR_IN = new Uri(String.Format("ftp://{0}/{1}/{2}/", FtpServerAddress, UriPrefix, DIRECTORY_IN));
            //    FTP_URI_DIR_OUT = new Uri(String.Format("ftp://{0}/{1}/{2}/", FtpServerAddress, UriPrefix, DIRECTORY_OUT));
            //}
        }

        #endregion

        #region GetFileList
        public List<string> GetFileList(string directory)
        {
            List<string> resultList = new List<string>();
            WebResponse response = null;
            StreamReader reader = null;
            try
            {
                response = CreateFtpWebRequest(GetFtpDirectoryUri(directory), WebRequestMethods.Ftp.ListDirectory).GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    resultList.Add(Path.GetFileName(line));
                    line = reader.ReadLine();
                }
                return resultList;
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFileList :" + ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (response != null)
                    response.Close();
            }
        }
        #endregion
        #region DownloadFile
        /// <summary>
        /// Download's the specified file to the TEMP_DIR directory
        /// </summary>
        /// <param name="fileName">File name to download</param>
        /// <returns>File path in TEMP_DIR if the download succeed, otherwise NULL</returns>
        public string DownloadFile(string tempDir, string directory, string fileName)
        {
            WebResponse response = null;
            FileStream outputStream = null;
            Stream ftpStream = null;
            try
            {
                string path = Path.Combine(tempDir, fileName);
                outputStream = new FileStream(path, FileMode.Create);

                response = CreateFtpWebRequest(GetFtpFileUri(directory, EncodeWithoutSpace(fileName)), WebRequestMethods.Ftp.DownloadFile).GetResponse();
                ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }
                return path;
            }
            catch (Exception ex)
            {
                Console.WriteLine("DownloadFile:" + ex.Message);
                throw ex;
            }
            finally
            {
                if (ftpStream != null)
                    ftpStream.Close();
                if (outputStream != null)
                    outputStream.Close();
                if (response != null)
                    response.Close();
            }
        }

        /// <summary>
        /// Download's the specified file as byte array
        /// </summary>
        /// <param name="fileName">File name to download</param>
        /// <returns>File as byte array, otherwise NULL</returns>
        public byte[] DownloadFile(string directory, string fileName)
        {
            WebResponse response = null;
            Stream ftpStream = null;
            try
            {

                response = CreateFtpWebRequest(GetFtpFileUri(directory, EncodeWithoutSpace(fileName)), WebRequestMethods.Ftp.DownloadFile).GetResponse();
                ftpStream = response.GetResponseStream();

                using (MemoryStream ms = new MemoryStream())
                {
                    ftpStream.CopyTo(ms);
                    return ms.ToArray();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("DownloadFile:" + ex.Message);
                throw ex;
            }
            finally
            {
                if (ftpStream != null)
                    ftpStream.Close();
                if (response != null)
                    response.Close();
            }

        }
        #endregion

        #region UploadFile
        /// <summary>
        /// Uploads the file to the FTP server
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <returns>True if upload succeed, otherwise False</returns>
        public bool UploadFile(string directory, string filePath)
        {
            return UploadFile(directory, filePath, false);
        }

        /// <summary>
        /// Uploads the file to the FTP server
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <param name="directory">Create directory if not exists</param>
        /// <returns>True if upload succeed, otherwise False</returns>
        public bool UploadFile(string directory, string filePath, bool createDirectory)
        {
            FileInfo fileInf = new FileInfo(filePath);
            if (fileInf.Exists)
            {
                #region Check if directory Exists
                if (createDirectory)
                {
                    try
                    {
                        List<string> filesForIssue = GetFileList(directory);
                    }
                    catch (WebException)
                    {
                        MakeDirectory(directory);
                    }
                    catch (Exception)
                    {
                    }
                }
                #endregion
                FtpWebRequest reqFTP = CreateFtpWebRequest(GetFtpFileUri(directory, EncodeWithoutSpace(fileInf.Name)), WebRequestMethods.Ftp.UploadFile);
                reqFTP.ContentLength = fileInf.Length;

                // The buffer size is set to 2kb
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];
                int contentLen;

                // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
                FileStream fs = null;
                Stream strm = null;
                try
                {
                    fs = fileInf.OpenRead();

                    // Stream to which the file to be upload is written
                    strm = reqFTP.GetRequestStream();

                    // Read from the file stream 2kb at a time
                    contentLen = fs.Read(buff, 0, buffLength);

                    long UploadedBytes = (fs.Length < buffLength) ? fs.Length : buffLength;

                    if (UploadProgress != null)
                        UploadProgress(fs.Length, UploadedBytes);

                    // Till Stream content ends
                    while (contentLen != 0)
                    {
                        // Write Content from the file stream to the FTP Upload Stream
                        strm.Write(buff, 0, contentLen);
                        contentLen = fs.Read(buff, 0, buffLength);
                        UploadedBytes += buffLength;

                        if (UploadedBytes > fs.Length)
                            UploadedBytes = fs.Length;

                        if (UploadProgress != null)
                            UploadProgress(fs.Length, UploadedBytes);
                    }
                    LastUploadDateTime = DateTime.Now;
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("UploadFile:" + ex.Message);
                    throw ex;
                }
                finally
                {
                    // Close the file stream and the Request Stream
                    if (strm != null)
                        strm.Close();
                    if (fs != null)
                        fs.Close();
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Uploads the file to the FTP server
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <param name="directory">Create directory if not exists</param>
        /// <returns>True if upload succeed, otherwise False</returns>
        public bool UploadFile(string directory, ref Stream fileStream, string name, bool createDirectory)
        {
            if (fileStream != null)
            {
                #region Check if directory Exists
                if (createDirectory)
                {
                    try
                    {
                        List<string> filesForIssue = GetFileList(directory);
                    }
                    catch (WebException)
                    {
                        MakeDirectory(directory);
                    }
                    catch (Exception)
                    {
                    }
                }
                #endregion
                FtpWebRequest reqFTP = CreateFtpWebRequest(GetFtpFileUri(directory, EncodeWithoutSpace(name)), WebRequestMethods.Ftp.UploadFile);
                reqFTP.ContentLength = fileStream.Length;

                // The buffer size is set to 2kb
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];
                int contentLen;

                // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
                //FileStream fs = null;
                Stream strm = null;
                try
                {
                    //fs = fileInf.OpenRead();

                    // Stream to which the file to be upload is written
                    strm = reqFTP.GetRequestStream();

                    // Read from the file stream 2kb at a time
                    contentLen = fileStream.Read(buff, 0, buffLength);

                    long UploadedBytes = (fileStream.Length < buffLength) ? fileStream.Length : buffLength;

                    if (UploadProgress != null)
                        UploadProgress(fileStream.Length, UploadedBytes);

                    // Till Stream content ends
                    while (contentLen != 0)
                    {
                        // Write Content from the file stream to the FTP Upload Stream
                        strm.Write(buff, 0, contentLen);
                        contentLen = fileStream.Read(buff, 0, buffLength);
                        UploadedBytes += buffLength;

                        if (UploadedBytes > fileStream.Length)
                            UploadedBytes = fileStream.Length;

                        if (UploadProgress != null)
                            UploadProgress(fileStream.Length, UploadedBytes);
                    }
                    LastUploadDateTime = DateTime.Now;
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("UploadFile:" + ex.Message);
                    throw ex;
                }
                finally
                {
                    // Close the file stream and the Request Stream
                    if (strm != null)
                        strm.Close();
                    if (fileStream != null)
                        fileStream.Close();
                }
            }
            else
                return false;
        }

        #endregion
        #region DeleteFile
        /// <summary>
        /// Deletes the file on the FTP Server
        /// </summary>
        /// <param name="fileName">file name to delete</param>
        /// <returns>True if delete succeed, otherwise False</returns>
        public bool DeleteFile(string directoryPath, string fileName)
        {
            Stream datastream = null;
            StreamReader sr = null;
            WebResponse response = null;
            try
            {
                string result = String.Empty;
                response = CreateFtpWebRequest(GetFtpFileUri(directoryPath, EncodeWithoutSpace(fileName)), WebRequestMethods.Ftp.DeleteFile).GetResponse();
                datastream = response.GetResponseStream();
                sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("DeleteFile:" + ex.Message);
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (datastream != null)
                    datastream.Close();
                if (response != null)
                    response.Close();
            }
        }
        #endregion
        #region MakeDirectory
        public bool MakeDirectory(string directoryPath)
        {
            Stream datastream = null;
            StreamReader sr = null;
            WebResponse response = null;
            try
            {
                string result = String.Empty;
                response = CreateFtpWebRequest(GetFtpDirectoryUri(directoryPath), WebRequestMethods.Ftp.MakeDirectory).GetResponse();
                datastream = response.GetResponseStream();
                sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("MakeDirectory:" + ex.Message);
                throw ex;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (datastream != null)
                    datastream.Close();
                if (response != null)
                    response.Close();
            }
        }
        #endregion
        #region CheckIfDirExists
        /// <summary>
        /// Return true if remote directory exists.
        /// </summary>
        /// <param name="directoryPath">Remote path for check</param>
        /// <returns></returns>
        public bool CheckIfDirectoryExists(string directoryPath)
        {
            WebResponse response = null;
            try
            {
                response = CreateFtpWebRequest(GetFtpDirectoryUri(directoryPath), WebRequestMethods.Ftp.GetDateTimestamp).GetResponse();
            }
            catch (WebException ex)
            {
                FtpWebResponse Response = (FtpWebResponse)ex.Response;
                if (Response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
                else return true;
            }
            return true;
        }
        #endregion

        #region CreateFtpWebRequest
        private FtpWebRequest CreateFtpWebRequest(Uri uri, string method)
        {
            FtpWebRequest reqFTP;
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(uri);
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(FtpLogin, FtpPassword);
            reqFTP.Method = method;
            reqFTP.KeepAlive = false;
            return reqFTP;
        }
        #endregion
        #region GetFtpFileUri
        public Uri GetFtpFileUri(string directory, string fileName)
        {
            //if (String.IsNullOrEmpty(uriPrefix))
            //{
            //    return new Uri(String.Format("ftp://{0}/{1}/{2}", FtpServerAddress, directory, fileName));
            //}
            //else
            //{
            //    return new Uri(String.Format("ftp://{0}/{1}/{2}/{3}", FtpServerAddress, uriPrefix, directory, fileName));
            //}
            return new Uri(String.Format("ftp://{0}{1}/{2}", FtpServerAddress, !String.IsNullOrEmpty(directory) ? ("/" + directory.Trim('/')) : String.Empty, fileName.TrimStart('/')));
        }
        #endregion
        #region GetFtpDirectoryUri
        public Uri GetFtpDirectoryUri(string directory)
        {
            //if (String.IsNullOrEmpty(uriPrefix))
            //{
            //    return new Uri(String.Format("ftp://{0}/{1}/{2}", FtpServerAddress, directory, fileName));
            //}
            //else
            //{
            //    return new Uri(String.Format("ftp://{0}/{1}/{2}/{3}", FtpServerAddress, uriPrefix, directory, fileName));
            //}
            return new Uri(String.Format("ftp://{0}/{1}", FtpServerAddress.TrimEnd('/'), directory.TrimStart('/')));
        }
        #endregion

        #region EncodeWithoutSpace
        private string EncodeWithoutSpace(string fileName)
        {
            return (HttpUtility.UrlEncode(fileName)).Replace('+', ' ');
        }
        #endregion
    }

    public static class FTPSettings
    {
        public static bool Enabled { get; set; }
        public static string Address { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }
    }
}
