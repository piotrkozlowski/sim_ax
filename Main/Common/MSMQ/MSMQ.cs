﻿#define USE_TRANSACTION	// UWAGA: zmiana sposobu z tranzakcją lub bez wymaga STWORZENIEA kolejek od nowa !!!
#define SEND_BULK

using System;
using System.Diagnostics;
using System.Configuration;
using System.Messaging;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace IMR.Suite.Common.MSMQ
{
	[Serializable]
	public class MSMQ
	{
		private MessageQueue Queue = null;
        private NLog.Logger logger = null;
        private const string MSMQ_REGISTRY_KEY = @"SOFTWARE\Microsoft\MSMQ\Setup";

        #region Path
        public string Path { get; private set; }
		#endregion

        #region Properties
        public bool Transactional { get { return Queue != null ? Queue.Transactional : false; } }
        #endregion

        #region IsInstalled
        public static bool IsInstalled()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(MSMQ_REGISTRY_KEY);
                if (key == null)
                    return false;

                object value = (object)key.GetValue("msmq_Core");
                if (value == null || Convert.ToInt32(value) != 1)
                    return false;
            }
            catch
            {
                return false;
            }

            return true;
        }
        #endregion
        #region IsDefinitionExists
        public static bool IsDefinitionExists(string QueueNameOrPath)
        {
            QueueElement QueueParameters;

            try
            {
                QueueSection Queues = (QueueSection)ConfigurationManager.GetSection("QueueSection");
                QueueParameters = Queues.Queues[QueueNameOrPath];

                return QueueParameters != null;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region ReceiveCompletedArgs
        [Serializable]
		public class ReceiveCompletedArgs : EventArgs
		{
			public readonly object Message;

			public ReceiveCompletedArgs(object Message)
			{
				this.Message = Message;
			}
		}
		#endregion
		#region ReceiveCompletedEvent
		public delegate void ReceiveCompletedDelegate(object sender, ReceiveCompletedArgs e);
		private ReceiveCompletedDelegate ReceiveCompletedMethod;
		public event ReceiveCompletedDelegate ReceiveCompletedEvent
		{
			add
			{
				ReceiveCompletedMethod = value;
				Queue.PeekCompleted += OnPeekCompleted;
                Queue.BeginPeek();
			}
			remove
			{
				ReceiveCompletedMethod = null;
				Queue.PeekCompleted -= OnPeekCompleted;
			}
		}
		#endregion

		#region Constructor
        public MSMQ(string QueueNameOrPath, bool FromPath, bool Transactional)
        {
            if (FromPath)
            {
                Path = QueueNameOrPath;
            }
            else
            {
                QueueElement QueueParameters;

                try
                {
                    QueueSection Queues = (QueueSection)ConfigurationManager.GetSection("QueueSection");
                    QueueParameters = Queues.Queues[QueueNameOrPath];

                    #region //From mapped configuration file
                    // Map the new configuration file
                    //ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                    //configFileMap.ExeConfigFilename = ConfigFilePath;
                    //// Get the mapped configuration file
                    //Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
                    //QueueSection Queues = (QueueSection)configuration.GetSection("QueueSection");
                    //QueueParameters = Queues.Queues[QueueName];
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("Queue '{0}' entry error or entry missing in configuration file: {1}", QueueNameOrPath, ex.Message));
                }

                Path = QueueParameters.Path;
            }

            Queue = new MessageQueue(Path);

            #region Sprawdzenie czy 'QueueParameters.Path' to path do lokalnego komputera
            System.Net.IPHostEntry localIPs = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            if ((Queue.MachineName == System.Environment.MachineName) ||
                (Queue.MachineName == System.Net.Dns.GetHostName()) ||
                (Queue.MachineName == localIPs.HostName) ||
                (Queue.MachineName == "localhost") ||
                (Queue.MachineName == "127.0.0.1"))
            {
                Queue.Path = @".\" + Queue.QueueName;
            }
            else
            {
                foreach (System.Net.IPAddress ip in localIPs.AddressList)
                {
                    if (Queue.MachineName == ip.ToString())
                    {
                        Queue.Path = @".\" + Queue.QueueName;
                        break;
                    }
                }
            }
            #endregion

            if (Queue.MachineName == ".")
            {
                #region Dla lokalnej kolejki sprawdzenie istnienia owej kolejki i ewentualne jej utworzenie
				if (MessageQueue.Exists(Queue.Path))
				{
                    if (Queue.Transactional != Transactional)
                        MessageQueue.Delete(Queue.Path);
				}
				if (!MessageQueue.Exists(Queue.Path))
				{
#if USE_TRANSACTION
                    Queue = MessageQueue.Create(Queue.Path, Transactional);
					//Queue.DefaultPropertiesToSend.Recoverable = true;
#else
					Queue = MessageQueue.Create(Queue.Path, false);
#endif

                    string everyone = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null).Translate(typeof(System.Security.Principal.NTAccount)).ToString();
					string administrators = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.BuiltinAdministratorsSid, null).Translate(typeof(System.Security.Principal.NTAccount)).ToString();
					string anonymous = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.AnonymousSid, null).Translate(typeof(System.Security.Principal.NTAccount)).ToString();

					Queue.SetPermissions(everyone/*@".\\Everyone"*/, MessageQueueAccessRights.GenericRead | MessageQueueAccessRights.PeekMessage | MessageQueueAccessRights.GenericWrite);
					Queue.SetPermissions(administrators/*@".\\Administrators"*/, MessageQueueAccessRights.FullControl);
					Queue.SetPermissions(anonymous/*@"ANONYMOUS LOGON"*/, MessageQueueAccessRights.WriteMessage);
				}
                #endregion
            }
            else
            {
                #region Dla zdalnej kolejki sprawdznie w jakiej postaci podany jest RemotePath (Name or IP)
                System.Net.IPAddress ipAddress;
                if (System.Net.IPAddress.TryParse(Queue.MachineName, out ipAddress))
                {
                    Queue.Path = "FormatName:DIRECT=TCP:" + Path;
                }
                else
                    Queue.Path = "FormatName:DIRECT=OS:" + Path;
                #endregion
            }

            Queue.Formatter = new BinaryMessageFormatter();
        }

        public MSMQ(string QueueName)
#if USE_TRANSACTION
            : this(QueueName, false, true)
#else
            : this(QueueName, false, false)
#endif
        {
        }

        public MSMQ(string QueueName, bool FromPath)
#if USE_TRANSACTION
            : this(QueueName, FromPath, true)
#else
            : this(QueueName, FromPath, false)
#endif
        {
        }
        
        public static MSMQ FromName(string QueueName)
        {
#if USE_TRANSACTION
            return new MSMQ(QueueName, false, true);
#else
            return new MSMQ(QueueName, false, false);
#endif
        }
        
        public static MSMQ FromPath(string QueuePath)
        {
#if USE_TRANSACTION
            return new MSMQ(QueuePath, true, true);
#else
            return new MSMQ(QueuePath, true, false);
#endif
        }

		#endregion
		#region Destructor
		~MSMQ()
		{
			if (Queue != null)
			{
				//Queue.PeekCompleted -= OnPeekCompleted;
				Queue.Close();
				Queue.Dispose();
			}
		}
		#endregion

		#region Send
		public void Send(object objectToSend)
		{
			Send(objectToSend, TimeSpan.FromDays(7));
		}
		public void Send(object objectToSend, TimeSpan timeout)
		{
#if USE_TRANSACTION
			MessageQueueTransaction transaction = new MessageQueueTransaction();
#endif

			try
			{
#if USE_TRANSACTION
				transaction.Begin();
#endif

				Message msg = new Message(objectToSend, new BinaryMessageFormatter());
				// Moze bede jeszcze potrzebne inne ustawienia typu:
				//msg.Priority = MessagePriority.Highest;
				//msg.TimeToBeReceived = TimeSpan.FromSeconds(5);
				//itd.

                if (timeout > TimeSpan.Zero)
                {
                    msg.TimeToBeReceived = timeout;
                }
#if USE_TRANSACTION
				Queue.Send(msg, objectToSend.GetType().Name, transaction);
#else
				Queue.Send(msg, objectToSend.GetType().Name);
#endif


#if USE_TRANSACTION
				transaction.Commit();
#endif
			}
			catch (MessageQueueException)
			{
#if USE_TRANSACTION
				transaction.Abort();
#endif
				throw;
			}
			finally
			{
#if USE_TRANSACTION
				transaction.Dispose();
#endif
			}
		}
		public void Send(object[] objectsToSend)
		{
			Send(objectsToSend, TimeSpan.FromDays(7));
		}
#if SEND_BULK
		public void Send(object[] objectsToSend, TimeSpan timeout)
		{
#if USE_TRANSACTION
			MessageQueueTransaction transaction = new MessageQueueTransaction();
#endif

			try
			{
#if USE_TRANSACTION
				transaction.Begin();
#endif

				Message msg = new Message(objectsToSend, new BinaryMessageFormatter());
				if (timeout > TimeSpan.Zero)
					msg.TimeToBeReceived = timeout;
#if USE_TRANSACTION
					Queue.Send(msg, objectsToSend.GetType().Name, transaction);
#else
					Queue.Send(msg, objectsToSend.GetType().Name);
#endif

#if USE_TRANSACTION
				transaction.Commit();
#endif
			}
			catch (MessageQueueException)
			{
#if USE_TRANSACTION
				transaction.Abort();
#endif
				throw;
			}
			finally
			{
#if USE_TRANSACTION
				transaction.Dispose();
#endif
			}
		}
#else
		public void Send(object[] objectsToSend, TimeSpan timeout)
		{
#if USE_TRANSACTION
			MessageQueueTransaction transaction = new MessageQueueTransaction();
#endif

			try
			{
#if USE_TRANSACTION
				transaction.Begin();
#endif

				foreach (object toSend in objectsToSend)
				{
					Message msg = new Message(toSend, new BinaryMessageFormatter());
					if (timeout > TimeSpan.Zero)
						msg.TimeToBeReceived = timeout;
#if USE_TRANSACTION
					Queue.Send(msg, toSend.GetType().Name, transaction);
#else
					Queue.Send(msg, toSend.GetType().Name);
#endif
				}

#if USE_TRANSACTION
				transaction.Commit();
#endif
			}
			catch (MessageQueueException)
			{
#if USE_TRANSACTION
				transaction.Abort();
#endif
				throw;
			}
			finally
			{
#if USE_TRANSACTION
				transaction.Dispose();
#endif
			}
		}
#endif
		#endregion
        #region Receive
        public object Receive()
        {
            return Receive(TimeSpan.FromDays(7));
        }
        public object Receive(TimeSpan timeout)
        {
#if USE_TRANSACTION
            MessageQueueTransaction transaction = new MessageQueueTransaction();
#endif

            try
            {
#if USE_TRANSACTION
                transaction.Begin();
#endif
                Message msg = new Message();
#if USE_TRANSACTION
                if (timeout > TimeSpan.Zero)
                {
                    msg = Queue.Receive(timeout, transaction);
                }
                else
                    msg =  Queue.Receive(transaction);
#else
				if (timeout > TimeSpan.Zero)
                {
                    msg = Queue.Receive(timeout);
                }
                else
                    msg =  Queue.Receive();
#endif


#if USE_TRANSACTION
                transaction.Commit();
                return (object)msg.Body;
#endif
            }
            catch (MessageQueueException)
            {
#if USE_TRANSACTION
                transaction.Abort();
#endif
                throw;
            }
            finally
            {
#if USE_TRANSACTION
                transaction.Dispose();
#endif
            }
        }
 
        #endregion
		#region OnPeekCompleted
		void OnPeekCompleted(object sender, PeekCompletedEventArgs e)
		{
			try
			{
				// Connect to the queue.
				MessageQueue queue = (MessageQueue)sender;
				// End the asynchronous peek operation.
				Message msg = queue.EndPeek(e.AsyncResult);

				if (ReceiveCompletedMethod != null)
				{
					MessageEnumerator enumerator = queue.GetMessageEnumerator2();

					while (enumerator.MoveNext())
					{
#if USE_TRANSACTION
						MessageQueueTransaction transaction = new MessageQueueTransaction();
#endif
						try
						{
#if USE_TRANSACTION
                            if (queue.Transactional) 
                                transaction.Begin();
#endif

							msg = enumerator.Current;
							if (msg.BodyType == 0x300)	// to 0x300 wzialem z refactoringu klasy System.Messaging
							{							// wpisywane jest na "sztywno" w BinaryMessageFormatter.Write()
								// internal const short VT_BINARY_OBJECT = 0x300;
#if USE_TRANSACTION
                                if (queue.Transactional)
                                    enumerator.RemoveCurrent(transaction);
                                else
                                    enumerator.RemoveCurrent();
#else
								enumerator.RemoveCurrent();
#endif
								ReceiveCompletedMethod(sender, new ReceiveCompletedArgs(msg.Body));
							}
							else
							{
								enumerator.RemoveCurrent();
								using (System.IO.StreamReader streamReader = new System.IO.StreamReader(msg.BodyStream))
								{
									ReceiveCompletedMethod(sender, new ReceiveCompletedArgs(streamReader.ReadToEnd()));
								}
							}
#if USE_TRANSACTION
                            if (queue.Transactional)
                                transaction.Commit();
#endif
						}
						catch (MessageQueueException mqex)
						{
#if USE_TRANSACTION
                            if (queue.Transactional)
                                transaction.Abort();
#endif
                            if (logger == null)
                                logger = NLog.LogManager.GetCurrentClassLogger();
                            logger.FatalException("MessageQueueException", mqex);
                        }
                        catch (SerializationException sex)
						{
#if USE_TRANSACTION
                            if (queue.Transactional)
                                transaction.Commit();
#endif
                            if (logger == null)
                                logger = NLog.LogManager.GetCurrentClassLogger();
                            logger.Fatal("MSMQ SerializationException: {0}", sex);

                            //throw new SerializationException("MSMQ SerializationException", sex);
                        }
						finally
						{
#if USE_TRANSACTION
                            if (queue.Transactional)
                                transaction.Dispose();
#endif
							enumerator.Reset();
						}
					}
				}

				// Restart the asynchronous receive operation.
				queue.BeginPeek();
			}
			catch (MessageQueueException ex)
			{
                if ((uint)ex.MessageQueueErrorCode == 0xC0000120)
                // Asynchronous Peek Thread sent STATUS_CANCELLED.
                    return;
				
                // Handle sources of MessageQueueException.
				throw;
			}
			return;
		}
		#endregion
        #region SetPermissions
        public void SetPermissions(string User, MessageQueueAccessRights Rights)
        {
            Queue.SetPermissions(User, Rights);
        }
        #endregion

        #region GetCount
        public uint GetCount()
        {
            return Queue.GetCount();            
        }
        #endregion

        #region Purge

        /// <summary>
        /// Usuwa wszystkie wiadomości z kolejki.
        /// </summary>
        public void Purge()
        {
            Queue.Purge();
        }

        #endregion Purge

        #region //OnReceiveComleted
        /*
		void OnReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
		{
			try
			{
				// Connect to the queue.
				MessageQueue queue = (MessageQueue)sender;

				// End the asynchronous receive operation.
				Message msg = queue.EndReceive(e.AsyncResult);

				if (ReceiveCompletedEvent != null)
				{
					ReceiveCompletedEvent(this, new ReceiveCompletedArgs(e.Message.Body));
				}

				// Restart the asynchronous receive operation.
				queue.BeginReceive();
			}
			catch (MessageQueueException)
			{
				// Handle sources of MessageQueueException.
				throw;
			}
			return;
		}
		*/
		#endregion
		#region // Proby
		/*
		public Type ReceiveType { get; set; }
		private Cursor cursor;
		public MSMQ(string receiveQueueName, Type receiveObjectType, bool local)
		{
			this.ReceiveType = receiveObjectType;

			MessageQueue queue;

			if (local == true && MessageQueue.Exists(receiveQueueName) == false)
			{
				queue = MessageQueue.Create(receiveQueueName, true);
			}
			else
			{
				queue = new MessageQueue(receiveQueueName);
			}
			queue.Formatter = new BinaryMessageFormatter();
			queue.MessageReadPropertyFilter.Id = true;
			cursor = queue.CreateCursor();

			queue.PeekCompleted += Queue_PeekCompleted;
			queue.BeginPeek();

			Queue = queue;
		}
		private void Queue_PeekCompleted(object sender, PeekCompletedEventArgs e)
		{
			MessageQueue queue = (MessageQueue)sender;
			queue.EndPeek(e.AsyncResult);

			Message[] msgs = queue.GetAllMessages();

			foreach (Message message in msgs)
			{
				if (message.Body.GetType() == ReceiveType)
				{
					MessageQueueTransaction transaction = new MessageQueueTransaction();
					try
					{
						transaction.Begin();
						Message recmsg = queue.ReceiveById(message.Id, transaction);
						transaction.Commit();

						if (ReceiveCompletedEvent != null)
						{
//							ReceiveCompletedEvent(recmsg.Body);
						}
					}
					catch (MessageQueueException)
					{
						transaction.Abort();
					}
					finally
					{
						transaction.Dispose();
					}
				}
			}

			Message msg = null;

			while (true)
			{
				try
				{
					if (msg == null)
						msg = queue.Peek(new TimeSpan(0, 0, 1), cursor, PeekAction.Current);
					else
						msg = queue.Peek(new TimeSpan(1), cursor, PeekAction.Next);
					if (msg.Body.GetType() == ReceiveType)
					{
						MessageQueueTransaction transaction = new MessageQueueTransaction();
						try
						{
							transaction.Begin();
							Message message = queue.ReceiveById(msg.Id, transaction);
							transaction.Commit();

							if (ReceiveCompletedEvent != null)
							{
//								ReceiveCompletedEvent(this, new EventArgs(message.Body);
							}
						}
						catch (MessageQueueException)
						{
							// cursor nie wraca !!!
							transaction.Abort();
						}
						finally
						{
							transaction.Dispose();
						}
					}
				}
				catch (MessageQueueException mqe)
				{
					if (mqe.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
						throw mqe;
					else
						msg = null;
				}
			}
		}

		protected Message PeekWithoutTimeout(MessageQueue q, Cursor cursor, PeekAction action)
		{
			Message ret = null;
			try
			{
				ret = q.Peek(new TimeSpan(1), cursor, action);
			}
			catch (MessageQueueException mqe)
			{
				if (!mqe.Message.ToLower().Contains("timeout"))
				{
					throw;
				}
			}
			return ret;
		}

		protected int GetMessageCount(MessageQueue q)
		{
			int count = 0;
			Cursor cursor = q.CreateCursor();

			Message m = PeekWithoutTimeout(q, cursor, PeekAction.Current);
			if (m != null)
			{
				count = 1;
				while ((m = PeekWithoutTimeout(q, cursor, PeekAction.Next)) != null)
				{
					count++;
				}
			}
			return count;
		}
		*/
		#endregion
       
	}

    #region class MessageQueueExtensions
    public static class MessageQueueExtensions
    {
        [DllImport("mqrt.dll")]
        private static extern int MQMgmtGetInfo([MarshalAs(UnmanagedType.BStr)]string computerName, [MarshalAs(UnmanagedType.BStr)]string objectName, ref MQMGMTPROPS mgmtProps);

        private const byte VT_NULL = 1;
        private const byte VT_UI4 = 19;
        private const int PROPID_MGMT_QUEUE_MESSAGE_COUNT = 7;

        //size must be 16
        [StructLayout(LayoutKind.Sequential)]
        private struct MQPROPVariant
        {
            public byte vt;       //0
            public byte spacer;   //1
            public short spacer2; //2
            public int spacer3;   //4
            public uint ulVal;    //8
            public int spacer4;   //12
        }

        //size must be 16 in x86 and 28 in x64
        [StructLayout(LayoutKind.Sequential)]
        private struct MQMGMTPROPS
        {
            public uint cProp;
            public IntPtr aPropID;
            public IntPtr aPropVar;
            public IntPtr status;
        }

        public static uint GetCount(this MessageQueue queue)
        {
            return GetCount(queue.Path);
        }

        private static uint GetCount(string path)
        {


         MQMGMTPROPS props = new MQMGMTPROPS { cProp = 1 };
         try
         {
             props.aPropID = Marshal.AllocHGlobal(sizeof(int));
             Marshal.WriteInt32(props.aPropID, PROPID_MGMT_QUEUE_MESSAGE_COUNT);

             props.aPropVar = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(MQPROPVariant)));
             Marshal.StructureToPtr(new MQPROPVariant { vt = VT_NULL }, props.aPropVar, false);

             props.status = Marshal.AllocHGlobal(sizeof(int));
             Marshal.WriteInt32(props.status, 0);

             string localPath = "FormatName:DIRECT=OS:" + path.Replace(".\\", Environment.MachineName + "\\");
             
             int result = MQMgmtGetInfo(null, "queue=" + localPath.Replace("FormatName:", ""), ref props);

             if (result != 0 || Marshal.ReadInt32(props.status) != 0)
             {
                 return 0;
             }

             MQPROPVariant propVar = (MQPROPVariant)Marshal.PtrToStructure(props.aPropVar, typeof(MQPROPVariant));
             if (propVar.vt != VT_UI4)
             {
                 return 0;
             }
             else
             {
                 return propVar.ulVal;
             }
         }
         finally
         {
             Marshal.FreeHGlobal(props.aPropID);
             Marshal.FreeHGlobal(props.aPropVar);
             Marshal.FreeHGlobal(props.status);
         }
        }
    }
    #endregion
}
