﻿using System;
using System.Configuration;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace IMR.Suite.Common.MSMQ
{
	#region QueueSection
	public class QueueSection : ConfigurationSection
	{
		[ConfigurationProperty("Queues", IsDefaultCollection = false),
		 ConfigurationCollection(typeof(QueueCollection), AddItemName = "Queue", ClearItemsName = "clearQueue", RemoveItemName = "removeQueue")]
		public QueueCollection Queues
		{
			get { return this["Queues"] as QueueCollection; }
		}
	}
	#endregion
	#region QueueElement
	public class QueueElement : ConfigurationElement
	{
		[ConfigurationProperty("Name", IsRequired = true)]
		public string Name
		{
			get { return this["Name"] as string; }
		}
		[ConfigurationProperty("Path", IsRequired = true)]
		public string Path
		{
			get { return this["Path"] as string; }
		}
	}
	#endregion
	#region QueueCollection
	public class QueueCollection : ConfigurationElementCollection
	{
		public QueueElement this[int index]
		{
			get { return base.BaseGet(index) as QueueElement; }
			set
			{
				if (base.BaseGet(index) != null)
					base.BaseRemoveAt(index);
				this.BaseAdd(index, value);
			}
		}
		public new QueueElement this[string key]
		{
			get { return base.BaseGet(key) as QueueElement; }
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new QueueElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((QueueElement)element).Name;
		}
	}
	#endregion}
}
