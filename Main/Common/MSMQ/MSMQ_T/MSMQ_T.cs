﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.Common.MSMQ;

namespace MSMQ_T
{
	public class MSMQ_T
	{
		static void Main(string[] args)
		{
			QueueTestClientServer test = new QueueTestClientServer();
			test.Run();
		}
	}

	#region QueueTestClientServer
	public class QueueTestClientServer
	{
		bool ServerMode = true;
		MSMQ queueRead;
		MSMQ queueSend;
		C1 c1 = new C1 { M1 = 1, M2 = "C1.1" };
		Dictionary<C1, string> dict = new System.Collections.Generic.Dictionary<C1, string>();
		C1 c2 = new C1 { M1 = 2, M2 = "C1.2" };

		#region Run
		public void Run()
		{
			dict.Add(c2, "C1.2");

			queueRead = new MSMQ("QueueRead");
			queueSend = new MSMQ("QueueSend");


			while (true)
			{
				Console.WriteLine();
				Console.WriteLine("1. Server");
				Console.WriteLine("2. Client");
				Console.WriteLine("Q. Quit");
				Console.WriteLine();
				ConsoleKeyInfo cki = Console.ReadKey(true);
				if (cki.KeyChar == 'q' || cki.KeyChar == 'Q')
					return;

				if (cki.KeyChar == '1')
				{
					ServerMode = true;
					queueRead.ReceiveCompletedEvent += OnReceiveQueue;
					Console.WriteLine("Server Mode. Waiting for message(s)....");
					Console.WriteLine();

					Console.ReadKey(true);
				}
				else if (cki.KeyChar == '2')
				{
					ServerMode = false;
					queueRead.ReceiveCompletedEvent += OnReceiveQueue;

					Console.WriteLine("Client Mode. Sending message(s)....");
					Console.WriteLine();
					queueSend.Send(c1);
					queueSend.Send(new C1 { M1 = 3, M2 = "C1.3" });
					queueSend.Send(c2);
					queueSend.Send(new C2 { M1 = 4, M2 = DateTime.Now});
					//for (int i = 5; i < 25; i++)
					//	queueSend.Send(new C1 { M1 = i, M2 = string.Format("C1.{0}",i) });
					Console.WriteLine("Sent");
					Console.WriteLine("Waiting for response....");
					Console.WriteLine();
					Console.ReadKey(true);
					queueRead.ReceiveCompletedEvent -= OnReceiveQueue;
				}
			}
		}
		#endregion
		#region OnReceiveQueue
		private void OnReceiveQueue(object sender, MSMQ.ReceiveCompletedArgs e)
		{
			if (ServerMode)
			{
				Console.WriteLine("Received object of type: {0}, sending response...", e.Message.GetType());
				queueSend.Send(e.Message);
			}
			else
			{
				Console.WriteLine();
				Console.WriteLine("Received object of type: {0}", e.Message.GetType());
				if (e.Message is C1)
				{
					Console.WriteLine("C1.M1={0}, C1.M2={1}", ((C1)e.Message).M1, ((C1)e.Message).M2);
					if (c1 == e.Message)
						Console.WriteLine("    Object equal to local c1 (1st method: c1 == e.Message)");

					C1 localC1 = (C1)e.Message;
					if (c1 == localC1)
						Console.WriteLine("    Object equal to local c1 (2nd method: c1 == (C1)e.Message)");

					if (dict.ContainsKey((C1)e.Message))
						Console.WriteLine("    Object equal to local c2 and exists in dictionary");
				}
				if (e.Message is C2)
				{
					Console.WriteLine("C2.M1={0}, C2.M2={1}", ((C2)e.Message).M1, ((C2)e.Message).M2);
					C2 localC2 = (C2)e.Message;
					if (c2 == localC2)
						Console.WriteLine("    Object equal to local c2 (2nd method: c2 == (C2)e.Message)");
				}
			}
		}
		#endregion
	}
	#endregion

	#region Class C1 with overloading operators
	[Serializable]
	public class C1
	{
		public int M1;
		public string M2;

		private readonly Guid Guid = Guid.NewGuid();

		public static bool operator ==(C1 left, object right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);

		}

		public static bool operator !=(C1 left, object right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}    

		public override bool Equals(object obj)
		{
			if (!(obj is C1)) return false;
			return this.GetHashCode() == obj.GetHashCode();
		}

		public override int GetHashCode()
		{
			return Guid.GetHashCode();
		}

	}
	#endregion
	#region Class C2 without overloading
	[Serializable]
	public class C2
	{
		public int M1;
		public DateTime M2;
	}
	#endregion
}
