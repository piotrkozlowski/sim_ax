﻿var path = require('path');
var webpack = require('webpack');

var CopyWebpackPlugin = require('copy-webpack-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var multi = require("multi-loader");

var config = require('./config.js')

var plugins = [
    new webpack.ResolverPlugin(
        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('.bower.json', ['main'])
    ),
    new CopyWebpackPlugin([
        {
            from: './css',
            to: 'css',
            ignore: ['img/*']
        }, {
            from: './images',
            to: 'images'
        },
        {
            from: './config.js',
            to: 'config.js'
        }
    ]),
    new ProgressBarPlugin()
]

var sassLoaders = []
config.THEMES.forEach(function (theme) {
    var plugin = new ExtractTextPlugin('css/style-' + theme.name + '.css')
    plugins.push(plugin)

    var themeConfig = require('./themes/' + theme.name + '.js')
    var query = []
    for (var key in themeConfig) {
        query.push(key + '=' + themeConfig[key])
    }
    var replaceString = query.join('&')
    sassLoaders.push(plugin.extract('style', 'multiple-replace?' + replaceString + '!css?sourceMap!sass?sourceMap'))
})

var loaders = [
    { test: /\.ts$/, loader: 'ts', exclude: /node_modules/ },
    { test: /\.html$/, loader: 'raw' },
    { test: /\.css$/, loaders: ['style', 'css?sourceMap'] },
    { test: /\.js$/, loader: 'script', exclude: /node_modules/ },
    { test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file?publicPath=../&name=images/[name].[ext]' },
    { include: /\.json$/, loaders: ["json-loader"] },
    { test: /\.scss$/, loader: multi(sassLoaders) },
    { test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'file?publicPath=../&name=fonts/[name].[ext]' }
]

module.exports = {
    context: __dirname,
    entry: {
        app: './build/entry.ts',
    },
    output: {
        filename: 'js/bundle.js',
        path: __dirname + '/build/client/'
    },
    resolve: {
        root: [path.join(__dirname, "node_modules"), path.join(__dirname, "bower_components")],
        extensions: ['', '.ts', '.js', '.json']
    },
    devtool: "#source-map",
    plugins: plugins,
    module: {
        loaders: loaders,
    }
}