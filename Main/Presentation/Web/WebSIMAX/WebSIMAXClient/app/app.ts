﻿import 'angular'

require('./app.scss')
require('../js/angular-gridster.js')
require('./ie11.js')

require('slick-carousel/slick/slick.scss')
require('slick-carousel/slick/slick-theme.scss')

import dashboardModule from './modules/dashboard/dashboard.module'
import loginModule from './modules/login/login.module'
import locationsModule from './modules/locations/locations.module'
import devicesModule from './modules/devices/devices.module'
import deviceConnectionsModule from './modules/deviceconnections/deviceconnections.module'
import metersModule from './modules/meters/meters.module'
import issuesModule from './modules/issues/issues.module'
import tasksModule from './modules/tasks/tasks.module'
import usersModule from './modules/users/users.module'
import alarmsModule from './modules/alarms/alarms.module'
import distributorsModule from './modules/distributors/distributors.module'
import reportsModule from './modules/reports/reports.module'
import routesModule from './modules/routes/routes.module'
import simcardsModule from './modules/simcards/simcards.module'
import contactsModule from './modules/contacts/contacts.module'
import actionsModule from './modules/actions/actions.module'
import { Api } from  './modules/api/api.module'
import { Permissions } from './helpers/permissions'
import { CommonHelper } from './helpers/common-helper'
import { UserConfigHelper } from './helpers/user-config-helper'
import { LogoModel } from './modules/api/api.models'
import 'angular-material-icons'
import 'angular-slick-carousel/src/slick'

import { PopupManager } from './helpers/popup-manager'

export var simax = angular.module('simax', ['ngMdIcons', 'slickCarousel', loginModule, locationsModule, devicesModule, dashboardModule, metersModule, issuesModule, tasksModule, usersModule, alarmsModule, distributorsModule, routesModule, reportsModule, contactsModule, simcardsModule,  actionsModule, deviceConnectionsModule])
var resourcesDownloaded = false

export class ActiveDistributorsInfo {
    count = 0
    multiple: boolean
    singleDistributor: string
    proccessDone: boolean
}

simax.run(['$animate', ($animate: ng.animate.IAnimateService) => {
    $animate.enabled(false)
}])

simax.controller("appController", ['$rootScope', '$scope', '$timeout', '$interval', 'i18n', '$state', 'api', ($rootScope: ng.IRootScopeService, $scope: ng.IScope, $timeout: ng.ITimeoutService, $interval: ng.IIntervalService, i18n, $state: angular.ui.IStateService, api: Api) => {
    var popupManager = PopupManager.getInstance()

    var hash = location.href.split("#")[1]

    $scope['theme'] = window['config'].DEFAULT_THEME

    $rootScope['getActiveDistributorsNumber'] = () => {
        api.distributor.getAllDistributors(null).then(result => {
            $rootScope['activeDistributors'] = new ActiveDistributorsInfo()
            result.values.forEach(value => {
                if (value.isenabled) $rootScope['activeDistributors'].count++
            })
            if ($rootScope['activeDistributors'].count > 1) {
                $rootScope['activeDistributors'].multiple = true
            }
            else {
                $rootScope['activeDistributors'].singleDistributor = result.values[0].name
            }
            $rootScope['activeDistributors'].proccessDone = true
        })
    }

    $scope['checkLocalStorage'] = (appVersion) => {
        var lastAppVersion = CommonHelper.getValueFromLocalStorage('APP_VERSION')
        if (lastAppVersion != appVersion) {
            localStorage.clear()
            CommonHelper.setValueToLocalStorage('APP_VERSION', appVersion)
        }
        $scope['APP_VERSION'] = appVersion
        $scope['localStorageChecked'] = true
    }

    var getLogoFromApi = (user) => {
        var model = new LogoModel()
        model.id = user.id
        model.jwt = api.login.getToken()

        $.ajax({
            url: window['config'].API_URL + 'api/app/logo',
            type: "POST",
            data: JSON.stringify(model),
            contentType: "application/json; charset=utf-8",
            success: (result) => {
                $rootScope['logo'] = result
                CommonHelper.setValueToLocalStorage('APP_LOGO', result)
            }
        })
    }

    var initData = () => {
        var user = api.login.getUser()
        if (user.permissions) {
            Permissions.setPermissions(user.permissions)
        }
        if (['login', 'logout'].indexOf($state.current.name) < 0) {
            var previousState = UserConfigHelper.getUserConfig(UserConfigHelper.StateBeforeAuthRejection, 'home')
            $state.go(previousState)
        }

        Permissions.runIfHasPermissions([Permissions.ALARM_LIST], () => {
            var refreshAlarms = () => {
                $timeout(() => {
                    $scope.$root.$broadcast('disableLoadingIndicator')
                    api.alarm.getAlarmsSummary(user.id).then(response => {
                        $scope.$root.$broadcast('enableLoadingIndicator')
                        if (response) {
                            $rootScope['alarmsCount'] = response.result.length
                            $rootScope['alarmTiles'] = response.result
                        }
                    })
                })
            }
            refreshAlarms()
            $interval(refreshAlarms, 60000 * 3)
        })

        var storagedLogo = CommonHelper.getValueFromLocalStorage('APP_LOGO')
        if (storagedLogo) {
            if (storagedLogo != 'undefined')
                $rootScope['logo'] = storagedLogo
        }
        else {
            getLogoFromApi(user)
        }

        $interval(() => {
            $scope.$broadcast('disableLoadingIndicator')
            UserConfigHelper.saveSettingsToDB(api, 1000).then(() => {
                $scope.$broadcast('enableLoadingIndicator')
            })
        }, 60000)

        $rootScope['getActiveDistributorsNumber']()
    }

    if (!hash || hash.indexOf('/reset') != 0) {
        if (!api.login.isLogged() && $state.current.name != 'login') $state.go('login')
        else {
            initData()
        }
    }

    $scope['rtlState'] = window['config']['RTLSTATE']
    DevExpress.rtlEnabled = $scope['rtlState']

    $scope['resourcesDownloaded'] = false
    $rootScope['i18n'] = i18n
    $rootScope['hasPermissions'] = Permissions.hasPermissionsResolved
    $rootScope['state'] = $state

    $scope['openedPopupCount'] = 0

    $scope.$watch(() => {
        return angular.element('body > .details-popup').length
    }, (val) => {
        $scope['openedPopupCount'] = val
    })

    $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams, options) => {
        if (['logout','login','reset','home'].indexOf(toState.name) < 0) {
            UserConfigHelper.setUserConfig(UserConfigHelper.StateBeforeAuthRejection, toState.name)
        }
    })

    $rootScope.$on('$stateNotFound', (event, unfoundState, fromState, fromParams) => {
        $state.go('home')
    })

    $rootScope.$on('onLogged', () => {
        initData()
    })

    $timeout(() => {
        $rootScope.$broadcast("showLoadingIndicator")
    }, 0)    

    var resourcesInterval = setInterval(() => {
        if (resourcesDownloaded) {
            $scope['resourcesDownloaded'] = true
            $rootScope.$broadcast("hideLoadingIndicator")
            if (!$rootScope.$$phase) $rootScope.$apply()
            clearInterval(resourcesInterval)
        }
    }, 100)
}])

simax.config(['$urlRouterProvider', '$stateProvider', '$locationProvider', 'i18nProvider', ($urlRouterProvider: ng.ui.IUrlRouterProvider, $stateProvider: angular.ui.IStateProvider, $locationProvider: ng.ILocationProvider, i18nProvider) => {
    var locale = CommonHelper.getValueFromLocalStorage('LOCALE')
    if (!locale) locale = 'pl-PL'    

    var numdersLocalizeJSON
    window['Globalize'].load(
            require("cldr-data/supplemental/likelySubtags"),            
            require("cldr-data/supplemental/numberingSystems"),
            require("cldr-data/supplemental/plurals"),
            require("cldr-data/supplemental/ordinals"),            
            require("cldr-data/supplemental/currencyData"),            
            require("cldr-data/supplemental/timeData"),
            require("cldr-data/supplemental/weekData")            
    );
    
    if (locale == 'pl-PL') {
        numdersLocalizeJSON = require("cldr-data/main/pl/numbers")
        numdersLocalizeJSON.main.pl.numbers["symbols-numberSystem-latn"].group = "\r"
        window['Globalize'].load(  
            numdersLocalizeJSON,                  
            require("cldr-data/main/pl/currencies"),
            require("cldr-data/main/pl/ca-gregorian"),
            require("cldr-data/main/pl/timeZoneNames"),
            require("cldr-data/main/pl/dateFields"),
            require("cldr-data/main/pl/units")
        );        
    }
    if (locale == 'en-GB') {
        numdersLocalizeJSON = require("cldr-data/main/en-GB/numbers")
        numdersLocalizeJSON.main["en-GB"].numbers["symbols-numberSystem-latn"].group = "\r"
        window['Globalize'].load(
            numdersLocalizeJSON,            
            require("cldr-data/main/en-GB/currencies"),
            require("cldr-data/main/en-GB/ca-gregorian"),
            require("cldr-data/main/en-GB/timeZoneNames"),
            require("cldr-data/main/en-GB/dateFields"),
            require("cldr-data/main/en-GB/units")
        );
    }
    window['Globalize'].locale(locale)       
    
    i18nProvider.setLanguage(locale)
    $.ajax(window['config'].API_URL + 'api/app/' + locale + '/resources', {
        method: 'GET',
        contentType: 'application/json; charset=utf-8',
        success: (data) => {
            i18nProvider.setResources(JSON.parse(data))
            resourcesDownloaded = true
        }
    })

    $stateProvider.state('home', {
        url: '/',
        controller: ['$scope', '$state', ($scope: ng.IScope, $state: angular.ui.IStateService) => {
            $scope.$watchCollection(() => {
                return $state.get()
            }, (states: ng.ui.IState[]) => {
                var visibleStates = states.filter(state => {
                    if (!state.abstract && state.name != 'login' && state.name != 'logout' && state.name != 'reset' && state.name != 'home') {
                        return true
                    }
                    return false
                })

                if (visibleStates.length > 0) {
                    $state.go(visibleStates[0])
                }
            })
        }],
        template: '<layout active="home"><br><br><br><h4 class="text-center">{{$root.i18n("Common", "NoPermissions")}}</h4></layout>',
    })
    $urlRouterProvider.when('', '/')
    $locationProvider.hashPrefix('');
}])