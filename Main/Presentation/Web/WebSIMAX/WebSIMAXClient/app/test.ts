﻿import { ApiProvider } from './modules/api/api.module'
import { simax } from './app'

simax.config(['apiProvider', (apiProvider: ApiProvider) => {
    apiProvider.setFakeServices(true)
}])