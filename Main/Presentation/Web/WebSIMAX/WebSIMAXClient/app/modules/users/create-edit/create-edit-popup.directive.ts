﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { EventsNames } from '../../../helpers/events-names'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

interface CreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    editPasswordButtonVisible: boolean
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class UserCreateEditDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        id: "="
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: CreateEditScope, $q, api: Api, localDictionary: LocalDictionaryFactory) => {
        
        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Common', 'Apply'),
            type: PopupType.USER_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submit(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean) => {
            var sections = []
            sections.push($q.all([localDictionary.getActorEditFields(), localDictionary.getActorRequiredFields()]).then(([columnsEdit, required]) => {
                return {
                    name: i18n('Common', 'PersonalDetails'),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }))
            sections.push($q.all([localDictionary.getUserEditFields(), localDictionary.getUserRequiredFields()]).then(([columnsEdit, required]) => {
                return {
                    name: i18n('Common', 'User'),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }))

            return $q.all(sections).then(dict => {
                $scope.dictionary = dict
            })
        }
        $scope.$on(EventsNames.OPEN_USER_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            loadColumns(args && args[0]).then(() => {
                if (args && args[0]) {
                    $scope.options.title = i18n('Popup', 'UserEdit')
                    api.users.getUserToEdit(args[0]).then(result => {
                        $scope.editedItem = result.fields
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0], args[1])
                    })
                    return
                } else {
                    $scope.editedItem = null
                    $scope.options.title = i18n('Popup', 'UserAdd')
                }
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[1])
            })
        })

        var submit = (fields: FieldNameValueModel[], id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.users.saveUser(formModel).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'Failed to save the user'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'UserSaved'))
                    CommonHelper.setValueToLocalStorage('userSaved', true)
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, result)
                }
            }).catch(err => {
                alert(err)
            })

        }

    }]
    template = require("./create-edit-popup.tpl.html")
    transclude = true
}