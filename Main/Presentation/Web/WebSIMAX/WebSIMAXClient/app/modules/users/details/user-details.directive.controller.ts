﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FieldNameValueModel, GridRequestModel, GridRequestReferenceModel, NotificationModel, NotificationEntity, SecurityChangeModel, SecurityActivityModel, OperatorModel } from '../../api/api.models'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'
import { EventsNames } from '../../../helpers/events-names'

interface CommonData {
    details: any
    actorDetails: any
}

interface EditReference {
    editReferenceButtonDisabled: boolean

    popover: DevExpress.ui.dxPopover
    popoverOptions: DevExpress.ui.dxPopoverOptions
    popoverVisible: boolean

    popoverGrid: GridData

    isUnknownReferenceTable: boolean
}

interface SecurityData {
    tabsOptions: DevExpress.ui.dxTabsOptions
    selectedTabIndex: number
    selectedTab: () => string
    multiViewOptions: DevExpress.ui.dxMultiViewOptions

    roles: any
    activities: any
    allActivities: any

    roleColumns: ColumnModel[]
    activityColumns: ColumnModel[]

    rolesData: any[]
    activitiesData: any[]

    modules: ColumnModel[]

    submitRoles: (added: any[], removed: any[]) => ng.IPromise<boolean>
    submitActivities: (added: any[], removed: any[]) => ng.IPromise<boolean>

    selectBoxOptions: DevExpress.ui.dxSelectBoxOptions

    editReference: EditReference
}

interface NotificationsData {
    tabsOptions: DevExpress.ui.dxTabsOptions
    selectedTabIndex: number
    selectedTab: any
    multiViewOptions: DevExpress.ui.dxMultiViewOptions

    distributors: ColumnModel[]

    task: any
    issue: any
    workOrder: any
    location: any
}

interface UsersDetailsScope extends DetailsScope {
    id: number
    editId: number
    itemIds: number[]
    details: any
    itemObjects: any[]
    popupTitle: string
    options: DetailsPopupOptions
    common: CommonData
    security: SecurityData
    notifications: NotificationsData

    roles: GridData
    activities: GridData
    allActivities: GridData

    showTabs: boolean

    confirmDelete: () => void
}

export class UserDetailsDirectiveController extends Details<UsersDetailsScope> {
    private permissions: any[]
    private securityGridDeferred: ng.IDeferred<void>

    constructor($scope: UsersDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.UserPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'UserDetails'),
            openEventName: EventsNames.OPEN_USER_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_USER_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.OPERATOR_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.requestsStates = {}

        $scope.popupFor = CommonHelper.user
        $scope.common = <CommonData>{}
        $scope.security = <SecurityData>{}

        $scope.editId = $scope.$id

        this.securityGridDeferred = $q.defer<void>()

        var securityTabs = []

        jQuery.when.apply(jQuery,
            [Permissions.OPERATOR_EDIT, Permissions.OPERATOR_NOTIFICATIONS_LIST, Permissions.OPERATOR_EFFECTIVE_PERMISSIONS, Permissions.OPERATOR_PERMISSIONS_ROLE_CHOOSE, Permissions.OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            var [OPERATOR_EDIT, OPERATOR_NOTIFICATIONS_LIST, OPERATOR_EFFECTIVE_PERMISSIONS, OPERATOR_PERMISSIONS_ROLE_CHOOSE, OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE] = permissions

            if (OPERATOR_EFFECTIVE_PERMISSIONS)
                securityTabs.push({ text: i18n('Common', 'EffectivePermissions'), name: 'effective' })
            if (OPERATOR_PERMISSIONS_ROLE_CHOOSE)
                securityTabs.push({ text: i18n('Common', 'Roles'), name: 'roles' })
            if (OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE)
                securityTabs.push({ text: i18n('Common', 'CustomPermissions'), name: 'custom' })
        })

        //common
        $scope.common = <CommonData>{}

        var updateAllActivitiesGrid = (activities: any[]) => {
            $scope.allActivities.grid.option("dataSource").store().clear()
            $scope.allActivities.grid.refresh()
            activities.forEach(value => {
                $scope.allActivities.grid.option("dataSource").store().insert(value).then(() => {
                    $scope.allActivities.grid.refresh()
                })
            })
        }
        
        this.configureTabs(this.getTabs())

        $scope.$on(EventsNames.OPEN_USER_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_USER_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var user: OperatorModel = args[0]
            if (user.id == $scope.modelId) {
                this.updateData(user)
            }
        })

        //security
        $scope.security.editReference = <EditReference>{}
        $scope.security.editReference.popoverGrid = <GridData>{}
        $scope.security.selectedTabIndex = this.getSelectedTabFromConfig(UserConfigHelper.UserPopupSecurityTabActive)

        $scope.security.rolesData = []
        $scope.security.activitiesData = []

        $scope.roles = <GridData>{}
        $scope.activities = <GridData>{}

        api.app.getRoles($scope.requestsStates.getRoles).then(roles => {
            $scope.security.rolesData = roles.values
            $scope.roles.showGrid = true
        })

        api.app.getActivities($scope.requestsStates.getActivities).then(activities => {
            $scope.security.activitiesData = activities.values
            $scope.activities.showGrid = true
        })

        $scope.security.tabsOptions = {
            dataSource: securityTabs,
            bindingOptions: {
                selectedIndex: 'security.selectedTabIndex'
            },
            onInitialized: e => {
                e.element.css({ "max-width": 600 })
            },
        }

        $scope.security.selectedTab = () => {
            return securityTabs[$scope.security.selectedTabIndex].name
        }

        $scope.$watch('security.selectedTabIndex', (value) => {
            UserConfigHelper.setUserConfig(UserConfigHelper.UserPopupSecurityTabActive, value)
        })

        $scope.security.editReference.popoverOptions = {
            width: 600,
            height: 500,
            onInitialized: (e) => {
                $scope.security.editReference.popover = e.component
            },
            target: '.edit-reference-button',
            bindingOptions: {
                visible: 'security.editReference.popoverVisible'
            },
            onHiding: () => {
                $scope.security.editReference.popoverVisible = false
                $scope.security.editReference.isUnknownReferenceTable = true
            }
        }

        // submit activities
        $scope.security.submitActivities = (added, removed) => {
            var model = new SecurityChangeModel()
            model.added = added.map(a => a['id'])
            model.removed = removed.map(r => r['id'])
            model.operatorId = $scope.modelId

            return api.users.submitActivities(model).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'SecurityEditError'), 'error', 5000)
                    return false
                }
                else return true
            })
        }

        // submit roles
        $scope.security.submitRoles = (added, removed) => {
            var model = new SecurityChangeModel()
            model.added = added.map(a => a['id'])
            model.removed = removed.map(r => r['id'])
            model.operatorId = $scope.modelId

            return api.users.submitRoles(model).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'SecurityEditError'), 'error', 5000)
                    return false
                }
                else return true
            })
        }

        var onModuleChanged = (e) => {
            $scope.allActivities.grid.beginCustomLoading('Loading')
            var value = e.value
            if (!value) {
                value = e.component.option('value')
            }
            updateAllActivitiesGrid($scope.security.allActivities.values.filter(activity => {
                return value != 0 ? activity.moduleId == value : true
            }))
            $scope.allActivities.grid.endCustomLoading()

            if (e.value)
                UserConfigHelper.setUserConfig(UserConfigHelper.SecurityModuleId, e.value)
        }

        $scope.security.selectBoxOptions = {
            width: 300,
            height: 35,
            bindingOptions: {
                dataSource: 'security.modules'
            },
            displayExpr: 'caption',
            valueExpr: 'id',
            searchEnabled: true,
            onValueChanged: onModuleChanged,
            onInitialized: (e) => {
                var component: DevExpress.ui.dxSelectBox = e.component
                component.option('value', UserConfigHelper.getUserConfig(UserConfigHelper.SecurityModuleId, 1))
                this.securityGridDeferred.promise.then(() => {
                    onModuleChanged(e)
                })
            }
        }

        //notifications
        $scope.notifications = <NotificationsData>{}
        var notificationsTabs = [
            { text: i18n('Common', 'Notifications2'), name: 'task' },
            { text: i18n('Common', 'Works'), name: 'workorder' },
            { text: i18n('Common', 'Tracks'), name: 'issue' },
            { text: i18n('Common', 'Locations'), name: 'location' }
        ]

        $scope.notifications.selectedTabIndex = this.getSelectedTabFromConfig(UserConfigHelper.UserPopupNotificationsTabActive)

        $scope.notifications.selectedTab = () => {
            return notificationsTabs[$scope.notifications.selectedTabIndex].name
        }

        $scope.notifications.tabsOptions = {
            dataSource: notificationsTabs,
            bindingOptions: {
                selectedIndex: 'notifications.selectedTabIndex'
            },
            onInitialized: e => {
                e.element.css({ "max-width": 700 })
            },
        }

        $scope.$watch('notifications.selectedTabIndex', (value) => {
            UserConfigHelper.setUserConfig(UserConfigHelper.UserPopupNotificationsTabActive, value)
        })

        $scope.notifications.multiViewOptions = {
            dataSource: {
                store: notificationsTabs.map(t => <any>{ name: t.name })
            },
            bindingOptions: {
                selectedIndex: 'notifications.selectedTabIndex'
            },
            height: '100%',
            onSelectionChanged: (e) => {
                if (e.addedItems && e.addedItems[0]) {

                }
            }
        }

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteUser'))
        }

        $scope.confirmDelete = () => {
            api.users.deleteUser($scope.modelId).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'UserDeleteError'), 'error', 4000)
                    return
                }
                $scope.$root.$broadcast(EventsNames.USER_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'UserDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'UserDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.requestsStates.getUser = {}
        this.$scope.showTabs = false
        this.$scope.roles = <GridData>{}
        this.$scope.activities = <GridData>{}
        this.$scope.allActivities = <GridData>{}
        this.$scope.notifications.issue = []
        this.$scope.notifications.workOrder = []
        this.$scope.notifications.task = []
        this.$scope.notifications.location = []
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.users.getUser(id, this.$scope.requestsStates.getUser).then(result => {
            this.$scope.popupTitle = result.name
            this.updateData(result)

            this.$scope.showTabs = true

        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: OperatorModel) {
        this.$scope.roles = <GridData>{}
        this.$scope.activities = <GridData>{}
        this.$scope.allActivities = <GridData>{}

        this.localDictionary.getModules().then(modules => {
            this.$scope.security.modules = []
            modules.forEach(module => {
                this.$scope.security.modules.push(module)
            })
            return modules
        }).then((modules) => {
            var columns = []
            Permissions.hasPermissions([Permissions.ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES]).then(ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES => {
                if (ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES) {
                    var editReferenceObjectColumn: ColumnModel = <any>{
                        id: 1001,
                        idName: 'editReferenceObject',
                        cellTemplate: (container, options) => {
                            if (options.data.referenceType != "" && !options.data.isNew) {
                                let buttonOptions = {
                                    hint: i18n('Popup', 'EditReference'),
                                    icon: 'edit',
                                    onClick: (e) => {
                                        this.$scope.security.editReference.popover.option('target', e.element[0])
                                        this.$scope.security.editReference.popoverVisible = !this.$scope.security.editReference.popoverVisible
                                        if (this.$scope.security.editReference.popoverVisible)
                                            this.getData(options.data)
                                    }
                                }

                                let button = $('<div class="edit-reference-button"></div>')
                                button.dxButton(buttonOptions)
                                button.appendTo(container)
                            }
                        },
                        name: '',
                        referenceTypeId: null,
                        sortDesc: false,
                        type: 0,
                        typeName: '',
                        unitId: null,
                        unitName: '',
                        where: null,
                        width: 70,
                        isEditable: false,
                        alignment: 'center',
                        allowSorting: false
                    }
                    columns.push(editReferenceObjectColumn)
                }

                this.insertEntityData(result.roles.values, 'getRoleColumns', "", 'roles')
                this.insertEntityData(result.allActivities.values.filter(a => a.idOperatorActivity), 'getActivityColumns', "", 'activities', columns)
                this.insertEntityData(result.allActivities.values.filter(a => a.moduleId == UserConfigHelper.getUserConfig(UserConfigHelper.SecurityModuleId, 1)), 'getActivityColumns', "", 'allActivities').then(() => {
                    this.securityGridDeferred.resolve()
                })
            })
        })

        this.$scope.notifications.task = result.notificationTasks.values
        this.$scope.notifications.issue = result.notificationIssues.values
        this.$scope.notifications.location = result.notificationLocations.values
        this.$scope.notifications.workOrder = result.notificationWorkOrders.values


        this.localDictionary.getUserDisplayFields().then(userFields => {
            this.localDictionary.getUserActorDisplayFields().then(actorFields => {

                this.$scope.security.roles = result.roles
                this.$scope.security.activities = result.activities
                this.$scope.security.allActivities = result.allActivities

                this.$scope.common.details = result ? Details.filterFields(result.fields, userFields) : []
                this.$scope.common.actorDetails = result ? Details.filterFields(result.actorFields, actorFields) : []

                var addColumnNamesToDetails = (editFields, fields, details) => {
                    this.localDictionary[editFields]().then(columns => {
                        var allColumns = columns.concat(fields)
                        details.forEach(prop => {
                            var suitedColumn
                            allColumns.some(column => {
                                if (column.id == prop.id) {
                                    suitedColumn = column
                                    return true
                                }
                                return false
                            })
                            prop.name = suitedColumn ? suitedColumn.name : ""
                        })
                    })
                }
                addColumnNamesToDetails("getActorEditFields", actorFields, this.$scope.common.actorDetails)
                addColumnNamesToDetails("getUserEditFields", userFields, this.$scope.common.details)
            })
        })

        this.localDictionary.getAllDistributors().then(distributors => {
            this.$scope.notifications.distributors = distributors
        })
    }

    getTabs() {
        var tabs = [
            { template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: "common" }
        ]

        var [OPERATOR_EDIT, OPERATOR_NOTIFICATIONS_LIST, OPERATOR_EFFECTIVE_PERMISSIONS, OPERATOR_PERMISSIONS_ROLE_CHOOSE, OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE] = this.permissions

        if (OPERATOR_EFFECTIVE_PERMISSIONS || OPERATOR_PERMISSIONS_ROLE_CHOOSE || OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE)
            tabs.push({ template: CommonHelper.getIconForDetails("SECURITY", '#000000', '18', i18n('Common', 'Security')), name: "security" })

        if (OPERATOR_NOTIFICATIONS_LIST)
            tabs.push({ template: CommonHelper.getIconForDetails("NOTIFICATIONS", '#000000', '18', i18n('Common', 'Notifications')), name: "notifications" })

        return tabs
    }

    getAccordions() {
        var accordions: DetailsAccordion[] = []

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "PersonalDetails"),
            canEdit: true,
            name: 'actor',
            showStatus: false,
            editMethod: () => {
                this.$scope.$root.$broadcast(EventsNames.OPEN_USER_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId, 0)
            },
            visible: true
        })

        items.push({
            title: i18n("Common", "User"),
            canEdit: true,
            name: 'params',
            showStatus: false,
            editMethod: () => {
                this.$scope.$root.$broadcast(EventsNames.OPEN_USER_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId, 1)
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.UserAccordionsSettings, 0, items, true, (component: DevExpress.ui.dxAccordion) => {
            this.$timeout(() => {
                component.expandItem(1)
            })
        }))

        return accordions
    }

    // getting data to grid in popover (editting reference object)
    getData(activity: any) {
        this.$scope.security.editReference.isUnknownReferenceTable = false

        if (activity.referenceTypeId) {
            var type = activity.referenceType.replace('ID_', '')
            var dictionaryMethodName = 'get' + type.charAt(0).toUpperCase() + type.slice(1).toLowerCase().trim() + 'DefaultColumns'

            if (this.localDictionary[dictionaryMethodName]) {
                this.localDictionary[dictionaryMethodName]().then(columns => {
                    this.createReferenceGrid(activity, columns, true)
                })
            }
            else {
                var nameColumn = new ColumnModel()
                nameColumn.id = 1
                nameColumn.idName = 'name'
                nameColumn.type = 3
                nameColumn.typeName = 'string'
                nameColumn.caption = i18n('Common', 'Name')

                this.createReferenceGrid(activity, [nameColumn], false)
            }
        }
        else {
            this.$scope.security.editReference.isUnknownReferenceTable = true
        }
    }

    createReferenceGrid(activity, columns, columnsFromWS: boolean = false) {
        this.$scope.security.editReference.popoverGrid = <GridData>{}
        this.$scope.security.editReference.popoverGrid.showGrid = false
        var refGridCustomGridOptions = this.setRefGridCustomGridOptions(activity, columns)
        this.$scope.security.editReference.popoverGrid.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, this.$scope, null, (request) => {
            this.$scope.requestsStates.getReferenceObjectList = {}
            return this.api.app.getReferenceObjectList(request, this.$scope.requestsStates.getReferenceObjectList)
        }, refGridCustomGridOptions)
        this.$timeout(() => {
            if (this.$scope.security.editReference.popoverGrid) {
                this.$scope.security.editReference.popoverGrid.grid.option(this.$scope.security.editReference.popoverGrid.options)
                this.$scope.security.editReference.popoverGrid.grid.refresh()
            }
        }, 200)
    }

    setRefGridCustomGridOptions(activity, columns) {
        var refGridCustomGridOptions = {
            idReferenceType: activity.referenceTypeId,
            name: 'ReferenceTypeGrid',
            defaultColumnsDictionary: columns,
            columnsDictionary: columns,
            columnFixing: {
                enabled: true
            },
            onInitialized: (e) => {
                this.$scope.security.editReference.popoverGrid.grid = e.component
            },
            onDataReady: () => {
                this.$scope.security.editReference.popoverGrid.grid.option('pager.visible', true)
                this.$scope.security.editReference.popoverGrid.showGrid = true
            },
            onRowDoubleClick: e => {
                this.$scope.security.editReference.popoverVisible = false

                var model = new SecurityActivityModel()
                model.operatorId = this.$scope.modelId
                model.id = activity.id
                model.referenceObjectId = e.key.id

                this.api.users.changeReferenceObject(model).then(result => {
                    if (!result) {
                        CommonHelper.openInfoPopup(i18n('Popup', 'ChangeReferenceError'), 'error', 5000)
                    }
                    else {
                        this.$scope.activities.grid.option("dataSource").store().clear()
                        this.$scope.activities.grid.refresh()
                        result.allActivities.values.filter(a => a.idOperatorActivity).forEach(a => {
                            this.$scope.activities.grid.option("dataSource").store().insert(a).then(() => {
                                this.$scope.activities.grid.refresh()
                            })
                            this.$scope.security.editReference.popoverGrid.showGrid = true
                        })
                    }
                })
            },
            allowColumnReordering: false,
            selection: { mode: 'single' },
            pager: {
                showPageSizeSelector: false,
                allowedPageSizes: [100],
                showInfo: true,
                infoText: i18n('Grid', 'PagerTextShort'),
                visible: false
            }
        }
        return refGridCustomGridOptions
    }
}