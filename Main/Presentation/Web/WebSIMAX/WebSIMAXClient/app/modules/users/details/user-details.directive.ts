﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { UserDetailsDirectiveController } from './user-details.directive.controller'

require('./user-details.scss')

export class UserDetailsDirective {
    scope = {
        id: '='
    }
    restrict = "E"
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new UserDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require('./user-details.tpl.html')
}