﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel, EntityModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'

interface UsersScope extends ScopeBaseGrid {
    dataGridOptions: DevExpress.ui.dxDataGridOptions
    dataGrid: DevExpress.ui.dxDataGrid
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
    showDetails(e: any): void

    gridId: number
}

export class UsersController extends BaseGridController {

    constructor($scope: UsersScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "users",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_USER_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_USER_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['"+Permissions.OPERATOR_EDIT+"']",
                import: "['"+Permissions.OPERATOR_IMPORT+"']",
                add: "['" + Permissions.OPERATOR_ADD + "']",
                export: "['" + Permissions.OPERATOR_EXPORT + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.OPERATOR_ADD, Permissions.OPERATOR_EDIT, Permissions.OPERATOR_EXPORT, Permissions.OPERATOR_IMPORT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                permissions[2] = false //export not implemented yet
                this.setSettingsListOptions(permissions, EventsNames.OPEN_USER_CREATE_EDIT_POPUP)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.getValueFromLocalStorage('userSaved')
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('userSaved')
            }
        })

        $scope.$on(EventsNames.USER_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getUserEditFields',
            popupName: i18n('Popup', 'ImportUsers'),
            requestMethodType: 'users'
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_USER_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }
    }

    getUsers = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.users.getUsers(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getUserColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getUserColumns().then(columns => {
            this.localDictionary.getUserDefaultColumns().then(defaultColumns => {
                var customOptions = {
                    name: "UsersGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    },
                    height: '100%'
                }
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getUsers, customOptions, true, this.localDictionary, Permissions.OPERATOR_EXPORT, this.$compile)
                this.$scope.dataGridOptions = gridOptions

                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
                this.$scope.dataGrid.refresh()
            })
        })
    }
}