﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'

import { UsersController } from './users.controller'
import { UserDetailsDirective } from './details/user-details.directive'
import { UserCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.users'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('UsersController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new UsersController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('userDetails', [() => new UserDetailsDirective()])
module.directive('userCreateEdit', [() => new UserCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.OPERATOR_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['USERS'], 'users', 'Users', 'users', 5)
        $stateProvider.state('users', {
            url: '/users',
            controller: 'UsersController',
            template: require('./users.tpl.html')
        })
    })
}])

export default moduleName