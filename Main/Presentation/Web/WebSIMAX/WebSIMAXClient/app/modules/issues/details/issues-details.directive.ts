﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { IssuesDetailsDirectiveController } from './issues-details.directive.controller'

require('./issues-details.scss')

export class IssuesDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new IssuesDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./issues-details.tpl.html")
}