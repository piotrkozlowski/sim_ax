﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper.ts'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'

interface IssuesScope extends ScopeBaseGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
}

export class IssuesController extends BaseGridController {
    constructor($scope: IssuesScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "issues",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_ISSUES_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_ISSUES_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['" + Permissions.ISSUE_EDIT + "']",
                add: "['" + Permissions.ISSUE_ADD + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.ISSUE_ADD, Permissions.ISSUE_EDIT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                permissions[2] = true //export permission not implemented yet
                permissions[3] = true //import permission not implemented yet
                this.setSettingsListOptions(permissions, EventsNames.OPEN_ISSUES_CREATE_EDIT_POPUP)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.setValueToLocalStorage('issueSaved', true)
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('issueSaved')
            }
        })

        $scope.$on(EventsNames.ISSUE_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getIssueEditFields',
            popupName: i18n('Popup', 'ImportIssues'),
            requestMethodType: 'issue'
        }


        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_ISSUES_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }
    }

    getIssues = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.issue.getIssues(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getIssuesColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getIssuesColumns().then(columns => {
            this.localDictionary.getIssuesDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "IssuesGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getIssues, customOptions, true, this.localDictionary, Permissions.METER_EXPORT, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
            })
        })
    }
}



