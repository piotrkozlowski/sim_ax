﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

require('./create-edit-popup.scss')

interface CreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class IssuesCreateEditDirective implements ng.IDirective {
    constructor() {
        
    }

    scope = {
        id: "="
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: CreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Common', 'Apply'),
            type: PopupType.ISSUE_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitIssue(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean) => {
            var sections = []
            return $q.all([localDictionary.getIssueEditFields(), localDictionary.getIssueRequiredFields()]).then(([columnsEdit, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }
        $scope.$on(EventsNames.OPEN_ISSUES_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            loadColumns(args && args[0]).then(() => {
                if (args && args[0]) {
                    $scope.options.title = i18n('Popup', 'IssueEdit')
                    if (typeof args[0] == "number") {
                        api.issue.getIssueToEdit(args[0]).then(result => {
                            $scope.editedItem = result.fields
                            $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                        })
                        return
                    } else {
                        $scope.editedItem = args[0]
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                    }
                } else {
                    $scope.editedItem = null
                    $scope.options.title = i18n('Popup', 'IssueAdd')
                }
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem)
            })
        })        

        var submitIssue = (fields, id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.issue.createIssue(formModel).then(issue => {
                if (!issue) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'IssueSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'IssueSaved'))
                    CommonHelper.setValueToLocalStorage('issueSaved', true)
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, issue)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
    }]
    template = require("./create-edit-popup.tpl.html")
    transclude = true
}