﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { IssuesCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { IssuesDetailsDirective } from './details/issues-details.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { IssuesController } from './issues.controller'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.issues'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('IssuesController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new IssuesController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('issuesDetails', [() => new IssuesDetailsDirective()])
module.directive('issuesCreateEdit', [() => new IssuesCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.ISSUE_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['ISSUES'], 'issues', 'Issues', 'issues', 6)
        $stateProvider.state('issues', {
            url: '/issues',
            controller: 'IssuesController',
            template: require('./issues.tpl.html')
        })
    })
}])

export default moduleName