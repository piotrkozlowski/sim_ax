﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { IssueModel, FormValuesModel, FileModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonDataGridOptions } from '../../common/common.data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    history: any
    attachments: any[]
    tasks: any[]
    equipment: any[]
}

interface IssuesDetailsScope extends DetailsScope {
    id: number
    editId: number
    itemObjects: any[]

    popupTitle: string
    common: CommonData

    issuesDetailsPopupId: number
    attachments: GridData
    tasks: GridData
    equipment: GridData
    attachmentsBridge: GridBridge
    equipmentBridge: GridBridge
    tasksBridge: GridBridge

    showTabs: boolean

    confirmDelete: () => void
}

export class IssuesDetailsDirectiveController extends Details<IssuesDetailsScope> {
    private issueId: number
    private permissions: any[]

    constructor($scope: IssuesDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.IssuePopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'IssueDetails'),
            openEventName: EventsNames.OPEN_ISSUES_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_ISSUES_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.ISSUE_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.issue
        $scope.common = <CommonData>{}
        $scope.editId = $scope.$id

        $scope.attachments = <GridData>{}
        $scope.tasks = <GridData>{}
        $scope.equipment = <GridData>{}

        jQuery.when.apply(jQuery,
            [Permissions.ISSUE_PARAMETERS, Permissions.ISSUE_ATTACHMENTS, Permissions.ISSUE_HISTORY, Permissions.ISSUE_TASKS, Permissions.ISSUE_EQUIPMENT, Permissions.ISSUE_EDIT]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteIssue'))
        }

        this.createEventEditForGridEntities(EventsNames.CLOSE_GRID_ATTACHMENTS_EDIT_POPUP + $scope.detailsPopupId + "attachments", 'attachments')

        $scope.$on(EventsNames.OPEN_ISSUES_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var issue: IssueModel = args[0]
            if (issue.id == this.issueId) {
                this.updateData(issue)
            }
        })

        $scope.$on(EventsNames.CLOSE_ISSUES_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        $scope.$on(EventsNames.DOWNLOAD_ISSUE_ATTACHMENT + $scope.detailsPopupId, (event: ng.IAngularEvent, ...args: any[]) => {
            api.issue.checkIfAttachmentExists(this.issueId, args[0]).then(message => {
                if (message.toUpperCase() == 'OK') {
                    CommonHelper.setJWTCookie(api.login.getToken())
                    window.open(window['config'].API_URL + 'api/app/issue/' + this.issueId + '/attachments/' + args[0], '_self');
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDownloadError') + ': ' + message, 'error', 5000)
                }
            })
        })

        $scope.confirmDelete = () => {
            api.issue.deleteIssue($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.ISSUE_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'IssueDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'IssueDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getIssue = {}
        this.$scope.requestsStates.getIssueEquipment = {}
        this.$scope.attachments = <GridData>{}
        this.$scope.equipment = <GridData>{}
        this.$scope.tasks = <GridData>{}
        this.$scope.common.attachments = []
        this.$scope.common.tasks = []
        this.$scope.common.equipment = []
        this.$scope.common.history = []
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.issue.getIssue(id, this.$scope.requestsStates.getIssue).then(result => {
            this.issueId = result.id
            this.$scope.popupTitle = result.name

            this.$scope.showTabs = true

            this.updateData(result)

            if (result.idLocation != null && result.idLocation != 0) {
                this.api.issue.getEquipment(result.idLocation, this.$scope.requestsStates.getIssueEquipment).then(equipment => {
                    this.$scope.common.equipment = equipment.result
                    this.insertEntityData(this.$scope.common.equipment, 'getLocationEquipmentColumns', null, 'equipment')
                })
            }
            else {
                this.$scope.common.equipment = []
                this.insertEntityData(this.$scope.common.equipment, 'getLocationEquipmentColumns', null, 'equipment')
            }
        }).catch(reason => this.logger.log(reason))

        this.buildBridges()

        this.$scope.requestsStates.getIssueAttachments = {}

        this.api.issue.getAttachments(id, this.$scope.requestsStates.getIssueAttachments).then(result => {
            var attachments = result.result
            this.updateAttachments(attachments)
            this.$scope.common.attachments = attachments
            this.$scope.attachmentsBridge.items = attachments
            this.$scope.attachmentsBridge.title = i18n('Popup', 'AttachmentsEdit') + " (" + this.$scope.common.attachments.length + ")"
        })
    }

    updateData(result: IssueModel) {
        var tasks = result && result.tasks ? result.tasks.map(task => CommonHelper.objectFromEntityModel(task)) : []
        tasks = tasks.map(task => {
            task['id'] = task[CommonHelper.idField]
            return task
        })
        this.insertEntityData(tasks, 'getIssueTaskDefaultColumns', EventsNames.OPEN_TASKS_DETAILS_POPUP, 'tasks')

        this.$scope.common.history = result.history

        this.localDictionary.getIssueDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
            this.$scope.common.tasks = tasks
            this.$scope.tasksBridge.items = this.$scope.common.tasks
            this.$scope.tasksBridge.title = i18n('Popup', 'TasksEdit') + " (" + this.$scope.common.tasks.length + ")"
        })
    }

    getTabs() {
        var [ISSUE_PARAMETERS, ISSUE_ATTACHMENTS, ISSUE_HISTORY, ISSUE_TASKS, ISSUE_EQUIPMENT] = this.permissions

        var tabs = []

        if (ISSUE_PARAMETERS || ISSUE_ATTACHMENTS || ISSUE_TASKS || ISSUE_EQUIPMENT) tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: 'common' })
        if (ISSUE_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: 'history' })

        return tabs
    }

    getAccordions() {
        var [ISSUE_PARAMETERS, ISSUE_ATTACHMENTS, ISSUE_HISTORY, ISSUE_TASKS, ISSUE_EQUIPMENT, ISSUE_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (ISSUE_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: ISSUE_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (ISSUE_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_ISSUES_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.IssueDetailsAccordions, 0, items))
        }

        if (ISSUE_ATTACHMENTS || ISSUE_TASKS || ISSUE_EQUIPMENT) {
            var items: DetailsAccordionSection[] = []

            if (ISSUE_ATTACHMENTS)
                items.push({
                    title: i18n("Common", "Attachments"),
                    canEdit: ISSUE_EDIT,
                    name: 'attachments',
                    showStatus: true,
                    editMethod: () => {
                        if (ISSUE_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_ATTACHMENTS_EDIT_POPUP + this.$scope.detailsPopupId + "attachments")
                    },
                    status: () => {
                        return i18n("Common", "AddedCount") + this.$scope.common.attachments.length
                    },
                    visible: true
                })

            if (ISSUE_TASKS)
                items.push({
                    title: i18n("Common", "Tasks"),
                    canEdit: ISSUE_EDIT,
                    name: 'tasks',
                    showStatus: true,
                    editMethod: () => {
                        if (ISSUE_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "tasks")
                    },
                    status: () => {
                        return i18n('Common', 'AssignedCount') + this.$scope.common.tasks.length
                    },
                    visible: true
                })

            if (ISSUE_EQUIPMENT)
                items.push({
                    title: i18n("Common", "Equipment"),
                    canEdit: false,
                    name: 'equipment',
                    showStatus: true,
                    status: () => {
                        return i18n('Common', 'InstalledCount') + this.$scope.common.equipment.length
                    },
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.IssueDetailsAccordions, 1, items))
        }

        return accordions
    }

    buildBridges() {
        this.$scope.attachmentsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'AttachmentsEdit') + " (" + this.$scope.common.attachments.length +")",
            items: [],
            popupId: this.$scope.detailsPopupId + 'attachments',
            dictionaryMethodName: 'getAttachmentColumns',
            dataMethodName: 'getAttachments',
            dataScope: 'issue',
            onAdd: (added: FileModel) => {
                added.parentEntityId = this.$scope.modelId
                return this.saveAttachment(added)
            },
            onDelete: (removed: number[]) => {
                var model = new FormValuesModel
                model.id = this.$scope.modelId
                model.values = removed
                return this.deleteAttachments(model)
            }
        }

        this.$scope.tasksBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'TasksEdit') + " (" + this.$scope.common.tasks.length + ")",
            items: [],
            popupId: this.$scope.detailsPopupId + 'tasks',
            dictionaryMethodName: 'getIssueTaskDefaultColumns',
            dataMethodName: 'getTasks',
            dataScope: 'task',
            onSubmit: (added, removed) => {
                var formModel = new FormValuesModel
                added = added.map(m => m[CommonHelper.idField])
                removed = removed.map(m => m[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.values = this.$scope.common.tasks.map(m => m[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveTasks(formModel)
            }
        }
    }

    updateAttachments(result) {
        this.insertEntityData(result, 'getAttachmentColumns', EventsNames.DOWNLOAD_ISSUE_ATTACHMENT, 'attachments')
    }

    saveAttachment(attachment: FileModel) {
        var deferred = this.$q.defer()
        this.api.issue.saveAttachment(attachment).then(response => {
            if (!response.fileModel) {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaveError') + ': ' + response.status, 'error', 5000, 500)
                deferred.reject()
            }
            else {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaved'))
                deferred.resolve(response.fileModel)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaveError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }

    deleteAttachments(model: FormValuesModel) {
        var deferred = this.$q.defer<number[]>()
        this.api.issue.deleteAttachments(model).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleteError'), 'error', 5000)
                deferred.reject()
            }
            else {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleted'))
                deferred.resolve(response.result)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleteError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }


    saveTasks(formModel: FormValuesModel) {
        var deferred = this.$q.defer()
        this.api.issue.saveIssue(formModel).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'IssueSaveError'), 'error', 5000)
                deferred.reject()
            }
            else {
                CommonHelper.openInfoPopup(i18n('Popup', 'IssueSaved'))
                this.updateData(response)
                deferred.resolve(true)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'IssueSaveError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }
}