﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api, LocalDictionaryFactory } from  '../api/api.module'
import { default as commonModule, LoggerService } from  '../common/common.module'
import { UserConfigHelper } from '../../helpers/user-config-helper'

import { LoginController } from './login.controller'

require('./login.scss')

var moduleName = 'simax.login'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', commonModule, apiModule])

module.controller('LoginController', ['$scope', '$state', '$location', 'api', 'logger', 'localDictionary', ($scope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new LoginController($scope, $state, $location, api, logger, localDictionary)])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('login', {
        url: '/login',
        controller: 'LoginController',
        template: require('./login.tpl.html')
    }).state('logout', {
        url: '/logout',
        controller: ['$scope', '$state', 'api', ($scope: ng.IScope, $state: angular.ui.IStateService, api: Api) => {
            UserConfigHelper.saveSettingsToDB(api, 0).then(() => {
                api.login.logout()
                    .finally(() => {
                        $scope.$root['hidePage'] = true
                        $state.go('login').then(() => {
                            location.reload()
                        })
                    })
            })
        }]
    }).state('reset', {
        url: '/reset',
        controller: 'LoginController',
        template: require('./login.tpl.html')
    })
}])

export default moduleName