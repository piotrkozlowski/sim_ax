﻿import { LoginController, LoginScope } from './login.controller'

describe('loginController', () => {
    var _$scope: LoginScope, _$state: angular.ui.IStateService

    beforeEach(angular.mock.module('simax.login'))
    beforeEach(inject(($controller: ng.IControllerService, $rootScope: ng.IRootScopeService, $state: angular.ui.IStateService, _api_, _logger_) => {
        _$scope = <LoginScope>$rootScope.$new()
        _$state = $state
        $controller('LoginController', {
            $scope: _$scope,
            $state: $state,
            api: _api_,
            logger: _logger_
        })
    }))

    it('should login', () => {
        var spy = sinon.spy(_$state, 'go')

        _$scope.loginModel = { user: 'TestUser', password: 'TestUser', locale: 'pl-PL' }
        _$scope.logIn()

        sinon.assert.calledOnce(spy)
        sinon.assert.calledWith(spy, 'locations')
        chai.assert.isTrue(true)
    })
})

