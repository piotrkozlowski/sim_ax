﻿import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { LoginModel, PasswordResetModel, PasswordResetEmailModel } from '../api/api.models'
import { UserConfigHelper } from '../../helpers/user-config-helper'
import { i18n } from '../common/i18n/common.i18n'
import { CommonHelper } from '../../helpers/common-helper'

export interface LoginScope extends ng.IScope {
    date: Date
    showError: string
    showInfo: string
    loginModel: LoginModel
    logIn(): ng.IPromise<void>
    loginForm: ng.IFormController
    setLocale(locale: string)
    passwordReset()
    sendEmail()
    isReset: boolean
    canReset: boolean
    keyPressed(keyEvent: any)
    password: string
    password2: string
}

export class LoginController {
    constructor($scope: LoginScope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        $scope['i18n'] = i18n

        var sessionGuid = $location.search().sessionGuid

        if ($state.current.name == 'reset') {
            $scope.canReset = true
            if (!sessionGuid) $state.go('login')
        }

        var getLocale = () => {
            var locale = CommonHelper.getValueFromLocalStorage('LOCALE')
            if (!locale) locale = 'pl-PL'
            return locale
        }

        $scope.keyPressed = (keyEvent) => {
            if (keyEvent.keyCode === 13) {
                if ($scope.password != undefined && $scope.password.length > 0 && $scope.password2 != undefined && $scope.password2.length > 0) {
                    $scope.passwordReset()
                }
                else if ($scope.loginModel.user.length > 0 && $scope.loginModel.password == undefined) {
                    $scope.sendEmail()
                }
            }
        }

        $scope.date = new Date()
        $scope.logIn = () => {
            if ($scope.loginForm.$invalid) {
                $scope.showError = i18n('Login', 'LoginErrorText')
            } else {
                $scope.loginModel.locale = getLocale()
                return api.login.login($scope.loginModel).then(result => {
                    localDictionary.refreshDictionaries().then(() => {
                        var previousState = UserConfigHelper.getUserConfig(UserConfigHelper.StateBeforeAuthRejection, 'dashboard')
                        $state.go(previousState)
                        $scope.$root.$broadcast('onLogged')
                    }).catch(dictReason => {
                        CommonHelper.openInfoPopup(i18n("Login", "LoginDictException"), "error", 3000)
                    })
                }).catch(reason => {
                    CommonHelper.openInfoPopup(i18n("Login", "LoginException"), "error")
                    $scope.showError = i18n('Login', 'LoginErrorText')
                })
            }
        }

        $scope.setLocale = (locale) => {
            CommonHelper.setValueToLocalStorage('LOCALE', locale)
            window.location.reload()
        }

        $scope.sendEmail = () => {
            var model = new PasswordResetEmailModel()
            model.user = $scope.loginModel.user
            model.locale = getLocale()

            api.password.reset(model).then(result => {
                if (result == 'OK') {
                    $scope.showError = null
                    $scope.showInfo = i18n('Login', 'PasswordResetEmail')
                    $scope.isReset = false
                } else {
                    $scope.showError = result
                }
                
            }).catch(() => {
                $scope.showError = i18n('Login', 'PasswordResetEmailError')
            })
        }

        $scope.passwordReset = () => {
            if ($scope.password == $scope.password2) {
                var model = new PasswordResetModel()

                model.guid = sessionGuid
                model.password = $scope.password
                model.locale = getLocale()

                api.password.change(model).then(result => {
                    if (result.status == 'OK') {
                        $scope.isReset = false
                        $scope.canReset = false
                        $scope.showError = null
                        $scope.showInfo = result.message
                    } else {
                        $scope.showError = result.message
                    }
                })
            } else {
                $scope.showError = i18n('Login', 'PasswordResetError')
            }
        }
    }
}

