﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'
import { BaseGridGridsterController, ScopeBaseGridsterGrid } from '../common/common.base.grid.gridster.controller'

interface MetersScope extends ScopeBaseGridsterGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
    popupId: number
}

export class MetersController extends BaseGridGridsterController {

    constructor($scope: MetersScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api, {
                ids: [],
                measurmentIdField: "meterId",
                measurmentIdsField: "meterIds",
                methodForDisplayFields: "getMeterDisplayFields",
                methodForMeasurments: "getMeasurementsForLocation",
                methodForMeasurmentsFields: "getMeasurementsForMeterFields",
                methodForPreviewFields: "getMeterPreviewFields",
                methodName: "getMeter",
                serviceName: "meter",
                filterMethodName: "getMeters"
            }, 'METERS')

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "meters",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_METER_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_METER_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onExportClick: () => {
                this.exportXls('meters')
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['"+Permissions.METER_EDIT+"']",
                import: "['"+Permissions.METER_IMPORT+"']",
                add: "['" + Permissions.METER_ADD + "']",
                export: "['" + Permissions.METER_EXPORT + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        this.addButtons(buttonsOptions)
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.METER_ADD, Permissions.METER_EDIT, Permissions.METER_EXPORT, Permissions.METER_IMPORT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                this.setSettingsListOptions(permissions, EventsNames.OPEN_METER_CREATE_EDIT_POPUP, 'meters', this.showPreviewDetails)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.getValueFromLocalStorage('meterSaved')
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('meterSaved')
            }
        })

        $scope.$on(EventsNames.METER_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getMeterEditFields',
            popupName: i18n('Popup', 'ImportMeters'),
            requestMethodType: 'meter'
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_METER_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }

        $scope.getItems = () => {
            return jQuery.when.apply(jQuery,
                [Permissions.DEVICE_LIST, Permissions.METER_LIST, Permissions.TASK_LIST, Permissions.ISSUE_LIST, Permissions.SIM_CARD_LIST, Permissions.ACTION_LIST]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [DEVICE_LIST, METER_LIST, TASK_LIST, ISSUE_LIST, SIM_CARD_LIST, ACTION_LIST] = permissions
                var items = []
                if (DEVICE_LIST) items.push({
                    template: CommonHelper.getIconForDetails("DEVICES", '#000000', '18', i18n('Grid', 'ShowDevices'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdDevice) }
                })
                if (METER_LIST) items.push({
                    template: CommonHelper.getIconForDetails("LOCATIONS", '#000000', '18', i18n('Grid', 'ShowLocations'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdLocation) }
                })
                if (TASK_LIST) items.push({
                    template: CommonHelper.getIconForDetails("TASKS", '#000000', '18', i18n('Grid', 'ShowTasks'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdTask) }
                })
                if (ISSUE_LIST) items.push({
                    template: CommonHelper.getIconForDetails("ISSUES", '#000000', '18', i18n('Grid', 'ShowIssues'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdIssue) }
                })
                if (SIM_CARD_LIST) items.push({
                    template: CommonHelper.getIconForDetails("SIM_CARD", '#000000', '18', i18n('Grid', 'ShowSimCards'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdSimCard) }
                })
                if (ACTION_LIST) items.push({
                    template: CommonHelper.getIconForDetails("RECENT_ACTIONS", '#000000', '18', i18n('Grid', 'ShowRecentActions'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdAction) }
                })
                return items
            })
        }

    }

    getMeters = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.meter.getMeters(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getMeterColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getMeterColumns().then(columns => {
            this.localDictionary.getMeterDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "MetersGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onDataReady: this.onDataReady,
                    onSelectionChanged: this.onSelectionChanged,
                    height: '100%',
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getMeters, customOptions, true, this.localDictionary, null, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
            })
        })
    }
}

