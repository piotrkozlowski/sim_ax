﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { MeterDetailsDirectiveController } from './meter-details.directive.controller'

export class MeterDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', 'api', 'logger', '$timeout', 'localDictionary', ($scope, $q: ng.IQService, api: Api, logger: LoggerService, $timeout: ng.ITimeoutService, localDictionary: LocalDictionaryFactory) => {
        return new MeterDetailsDirectiveController($scope, $q, api, logger, $timeout, localDictionary)
    }]
    template = require('./meter-details.tpl.html')
}