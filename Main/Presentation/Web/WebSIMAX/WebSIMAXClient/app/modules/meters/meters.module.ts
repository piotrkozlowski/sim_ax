﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { MetersController } from './meters.controller'
import { MeterCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { MeterDetailsDirective } from './details/meter-details.directive'
import { Permissions } from '../../helpers/permissions'
import { GroupsEditDirective } from '../common/groups-edit/groups-edit.directive'

var moduleName = 'simax.meters'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('MetersController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new MetersController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('meterCreateEdit', [() => new MeterCreateEditDirective()])
module.directive('meterDetails', [() => new MeterDetailsDirective()])
module.directive('alarmGroupsEdit', [() => new GroupsEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.METER_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['METERS'], 'meters', 'Meters', 'meters', 4)
        $stateProvider.state('meters', {
            url: '/meters',
            controller: 'MetersController',
            template: require('./meters.tpl.html')
        })
    })
}])

export default moduleName