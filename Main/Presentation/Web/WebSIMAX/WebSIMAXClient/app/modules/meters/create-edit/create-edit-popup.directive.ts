﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

interface CreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean

    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class MeterCreateEditDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        id: "="
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: CreateEditScope, $q, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Common', 'Apply'),
            type: PopupType.METER_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitMeter(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean, referenceId: number) => {
            return $q.all([localDictionary.getMeterEditFields(referenceId), localDictionary.getMeterDisplayFields(referenceId), localDictionary.getMeterRequiredFields(referenceId)]).then(([columnsEdit, columnsDisplay, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }
        $scope.$on(EventsNames.OPEN_METER_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            if (args && args[0]) {
                $scope.options.title = i18n('Popup', 'MeterEdit')

                api.meter.getMeterToEdit(args[0]).then(result => {
                    loadColumns(args && args[0], result.referenceId).then(() => {
                        $scope.editedItem = result.fields
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                    })
                })
                return
            } else {
                $scope.editedItem = null
                $scope.options.title = i18n('Popup', 'MeterAdd')
            }

            loadColumns(args && args[0], 0).then(() => {
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem)
            })
        })

        var submitMeter = (fields: FieldNameValueModel[], id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.meter.saveMeter(formModel).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'MeterSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'MeterSaved'))
                    CommonHelper.setValueToLocalStorage('meterSaved', true)
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, result)
                }
            }).catch(err => {
                alert(err)
            })
            
        }

    }]
    template = require("./create-edit-popup.tpl.html")
    transclude = true
}