﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { MeterModel, FormValuesModel, SchemaModel, LocationFormValuesModel, GroupModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { ConnectedObject } from '../../common/connected-object/connected-object.directive'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'
import { GroupsBridge } from '../../common/groups-edit/groups-edit.directive'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'


interface CommonData {
    details: any
    devices: any[]
    alarmDefs: any[]
    alarmGroups: GroupModel[]
}

interface MeterDetailsScope extends DetailsScope {
    id: number
    editId: number
    meterDetailsPopupId: number
    itemObjects: any[]
    details: any
    popupTitle: string

    common: CommonData
    devices: GridData
    alarmDefs: GridData
    alarmGroups: GridData
    measurementsBridge: MeasurementsBridge
    deviceBridge: GridBridge
    alarmGroupsBridge: GroupsBridge
    gridParams: GridData

    connectedObject: ConnectedObject

    confirmDelete: () => void

    //TODO: import accurate models, these are not the ones we're looking for
    submitAlarmDefs: (changed: any[]) => ng.IPromise<MeterModel[]>
    submitAlarmGroups: (changed: any[]) => ng.IPromise<MeterModel[]>

    showTabs: boolean

    schemas: SchemaModel[]
    loadSchemas: (id: number) => void
}

export class MeterDetailsDirectiveController extends Details<MeterDetailsScope> {
    private meterId: number
    private permissions: any[]
    private referenceId: number

    private gridParamsGridPromise: ng.IPromise<void>

    constructor($scope: MeterDetailsScope, $q: ng.IQService, private api: Api, logger: LoggerService, $timeout: ng.ITimeoutService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.MeterPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'MeterDetails'),
            openEventName: EventsNames.OPEN_METER_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_METER_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.METER_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.meter
        $scope.editId = $scope.$id
        $scope.common = <CommonData>{}

        $scope.alarmDefs = <GridData>{}
        $scope.alarmGroups = <GridData>{}
        $scope.gridParams = <GridData>{}

        $scope.connectedObject = {
            connectedObjectTitle: i18n('Common', 'CurrentDevice'),
            connectedObjectName: "",
            connectedObjectVisibility: false,
            connectedObjectId: 0,
            connectedObjectMethod: 'device'
        }

        // $scope.$watchCollection("common.alarmDefs", value =>{
        //     this.updateGridEntities("AlarmDefs")
        // })
        // $scope.$watchCollection("common.alarmGroups", value => {
        //     this.updateGridEntities("AlarmGroups")
        // })

        jQuery.when.apply(jQuery, [Permissions.MEASUREMENTS_LIST, Permissions.METER_HISTORY, Permissions.METER_PARAMETERS, Permissions.METER_DEVICES, Permissions.METER_GENERAL_SCHEMA, Permissions.METER_EDIT, Permissions.METER_ALARM_DEFINITIONS, Permissions.METER_ALARM_GROUPS].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteMeter'))
        }

        this.createEventEditForGridEntities(EventsNames.CLOSE_GRID_EDIT_POPUP + $scope.detailsPopupId + "devices", 'devices')

        //common
        $scope.common = <CommonData>{}
        //devices
        $scope.devices = <GridData>{}

        $scope.$on(EventsNames.OPEN_METER_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_METER_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var meter: MeterModel = args[0]
            if (meter.id == $scope.modelId) {
                this.updateData(meter)
            }
        })

        this.createEventEditForGridEntities(EventsNames.CLOSE_GROUPS_EDIT_POPUP + $scope.detailsPopupId, 'alarmGroups')

        //submitting alarm definitions and groups (no idea how it works)
        // $scope.submitAlarmDefs = (changed: any[]) => {
        //     var editModel = new MeterEditModel()
        //     editModel.id = $scope.modelId
        //     editModel.meters = []
        //     changed.forEach(ch => {
        //         editModel.meters.push(<MeterHierarchyModel>ch)
        //     })

        //     return api.meter.saveMeter(editModel).then(result => {
        //         if(!result && !result.result)
        //             CommonHelper.openInfoPopup(i18n('Popup', 'EditingError'), 'error', 5000)
        //         return result.result
        //     })
        // }

        $scope.confirmDelete = () => {
            api.meter.deleteMeter($scope.modelId).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'MeterDeleteError'), 'error', 4000)
                    return
                }
                $scope.$root.$broadcast(EventsNames.METER_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'MeterDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'MeterDeleteError'), 'error', 4000)
            })
        }

        $scope.loadSchemas = (id: number) => {
            this.loadSchemas(id)
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.requestsStates.getMeter = {}
        this.$scope.showTabs = false
        this.$scope.devices = <GridData>{}
        this.$scope.alarmDefs = <GridData>{}
        this.$scope.alarmGroups.showGrid = false
        this.$scope.schemas = null
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        //promise for getting a meter
        var meterPromise = this.api.meter.getMeter(this.$scope.modelId, this.$scope.requestsStates.getMeter).then(meterResult => {
            this.meterId = meterResult ? meterResult.id : null;
            this.referenceId = meterResult.referenceId
            this.updateData(meterResult)
            this.$scope.gridParams.showGrid = true
            this.$scope.popupTitle = meterResult.name
            this.$scope.alarmGroups.showGrid = true
        }).catch(reason => this.logger.log(reason))

        // //TODO: implement getAlarmGroups in meter service
        // var alarmGroupsPromise = this.api.meter.getAlarmGroups(this.$scope.modelId, this.$scope.requestsStates.getAlarmGroups).then(alarmGroupsResult => {
        //     this.$scope.common.alarmGroups = alarmGroupsResult.result
        //     this.insertEntityData(alarmGroupsResult.result, 'getAlarmHierarchyColumns', EventsNames.OPEN_ALARMS_DETAILS_POPUP, 'alarmGroups')
        // }).catch(reason => this.logger.log(reason))

        // //TODO: implement getAlarmDefs in meter service
        // var alarmDefsPromise = this.api.meter.getAlarmDefs(this.$scope.modelId, this.$scope.requestsStates.getAlarmDefs).then(alarmDefsResult => {
        //     this.$scope.common.alarmDefs = alarmDefsResult.result
        //     this.insertEntityData(alarmDefsResult.result, 'getAlarmHierarchyColumns', EventsNames.OPEN_ALARMS_DETAILS_POPUP, 'alarmDefs')
        // }).catch(reason => this.logger.log(reason))

        //not sure if this is even needed
        this.$q.all([meterPromise//, alarmDefsPromise, alarmGroupsPromise
        ]).then(() => {
            this.buildBridges()
            this.$scope.showTabs = true
        })
    }

    updateData(result: MeterModel) {
        var devices = result && result.devices ? result.devices.map(device => CommonHelper.objectFromEntityModel(device)) : []
        this.insertEntityData(devices, 'getMeterDeviceColumns', EventsNames.OPEN_DEVICES_DETAILS_POPUP, 'devices')

        this.loadAlarmGroups().then(() => {
            if (result) {
                this.$scope.alarmGroups.options.dataSource.store().clear()
                result.alarmGroups.forEach(element => {
                    this.$scope.alarmGroups.options.dataSource.store().insert(element)
                })
                this.$scope.alarmGroups.grid.refresh()
            }
        })

        this.localDictionary.getMeterDisplayFields(result.referenceId).then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
            this.$scope.common.alarmGroups = result ? result.alarmGroups : []
        })
        this.$scope.common.devices = devices

        if (result.currentDeviceId != null) {
            this.$scope.connectedObject.connectedObjectName = result.currentDeviceName
            this.$scope.connectedObject.connectedObjectVisibility = true
            this.$scope.connectedObject.connectedObjectId = result.currentDeviceId
        }
        else {
            this.$scope.connectedObject.connectedObjectVisibility = false
        }

        this.buildBridges()

        this.$scope.showTabs = true
    }

    getTabs(addSchemaTab: boolean = false) {
        var [MEASUREMENTS_LIST, METER_HISTORY, METER_PARAMETERS, METER_DEVICES, METER_GENERAL_SCHEMA, METER_ALARM_DEFINITIONS, METER_ALARM_GROUPS] = this.permissions
        var tabs = []

        if ([METER_PARAMETERS, METER_DEVICES].indexOf(true) > -1 || METER_GENERAL_SCHEMA && addSchemaTab) tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: "common" })
        if (MEASUREMENTS_LIST) tabs.push({ template: CommonHelper.getIconForDetails("MEASUREMENTS", '#000000', '18', i18n('Common', 'Measurements')), name: 'measurements' })
        if (METER_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: "history" })

        return tabs
    }

    getAccordions() {
        var [MEASUREMENTS_LIST, METER_HISTORY, METER_PARAMETERS, METER_DEVICES, METER_GENERAL_SCHEMA, METER_EDIT, METER_ALARM_GROUPS, METER_ALARM_DEFINITIONS] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (METER_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: METER_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (METER_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_METER_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.MeterDetailsAccordions, 0, items))
        }

        if (METER_DEVICES || METER_ALARM_GROUPS || METER_ALARM_DEFINITIONS) {
            var items: DetailsAccordionSection[] = []

            if (METER_DEVICES)
                items.push({
                    title: i18n("Common", "Devices"),
                    canEdit: METER_EDIT,
                    name: 'devices',
                    showStatus: true,
                    editMethod: () => {
                        if (METER_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "devices")
                    },
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.devices ? this.$scope.common.devices.length : 0)
                    },
                    visible: true
                })

            if (METER_ALARM_DEFINITIONS)
                items.push({
                    title: i18n("Common", "AlarmDefs"),
                    canEdit: METER_EDIT,
                    name: 'alarmDefs',
                    showStatus: true,
                    editMethod: () => {
                        if (METER_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "alarmDefs")
                    },
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.alarmDefs ? this.$scope.common.alarmDefs.length : 0)
                    },
                    visible: true
                })

           if (METER_ALARM_GROUPS)
                items.push({
                    title: i18n("Common", "AlarmGroups"),
                    canEdit: METER_EDIT,
                    name: 'alarmGroups',
                    showStatus: true,
                    editMethod: () => {
                        if (METER_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "alarmGroups")
                    },
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.alarmGroups ? this.$scope.common.alarmGroups.length : 0)
                    },
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.MeterDetailsAccordions, 1, items))

        }

        return accordions
    }

    loadSchemas(id) {
        var [MEASUREMENTS_LIST, METER_HISTORY, METER_PARAMETERS, METER_DEVICES, METER_ALARM_DEFINITIONS, METER_ALARM_GROUPS, METER_GENERAL_SCHEMA] = this.permissions

        if (METER_GENERAL_SCHEMA) {
            this.api.meter.getSchemas(id).then(result => {
                this.$scope.schemas = result.result
                // update tabs
                if (result.result.length > 0) {
                    if (!([METER_PARAMETERS, METER_DEVICES, METER_ALARM_DEFINITIONS, METER_ALARM_GROUPS].indexOf(true) > -1)) {
                        this.updateTabsPanel(this.getTabs(true))
                    }
                }
            })
        }
    }

    buildBridges() {
        this.$scope.measurementsBridge = {
            id: this.$scope.modelId,
            latestDataMethodName: 'getLatestMeasurementsForMeter',
            dataMethodName: 'getMeasurementsForMeter',
            requestProperty: 'meterIds',
            meters: [{
                id: this.$scope.modelId,
                name: this.$scope.popupTitle,
                referenceId: this.referenceId
            }],
            groups: [{
                id: this.$scope.modelId,
                referenceId: this.referenceId,
                name: this.$scope.popupTitle
            }]
        }

        this.$scope.alarmGroupsBridge = {
            id: this.$scope.modelId,
            assigned: this.$scope.common.alarmGroups,
            popupId: this.$scope.detailsPopupId,
            onSubmit: (added, removed) => {
                var formModel = new LocationFormValuesModel()
                formModel.id = this.$scope.modelId
                formModel.groups = this.$scope.common.alarmGroups.map(g => g.id).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }

        //TODO: check if needed to be rewritten
        this.$scope.deviceBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'DevicesEdit') + " (" + this.$scope.common.devices.length + ")",
            items: this.$scope.common.devices,
            popupId: this.$scope.detailsPopupId + 'devices',
            dictionaryMethodName: 'getMeterDeviceColumns',
            dataMethodName: 'getDevices',
            dataScope: 'device',
            onSubmit: (added, removed) => {
                var formModel = new FormValuesModel()
                added = added.map(m => m[CommonHelper.idField])
                removed = removed.map(m => m[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.values = this.$scope.common.devices.map(m => m[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }
    }

    saveFormModel(formModel: FormValuesModel) {
        var deferred = this.$q.defer()
        this.api.meter.saveMeter(formModel).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'MeterSaveError'), 'error', 5000)
                deferred.reject()
            } else {
                CommonHelper.openInfoPopup(i18n('Popup', 'MeterSaved'))
                this.updateData(response)
                deferred.resolve(true)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'MeterSaveError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }

    loadAlarmGroups() {
        var defer = this.$q.defer<void>()

        if (this.$scope.alarmGroups.grid) {
            defer.resolve()
        } else {
            var customGroupsGridOptions = {
                name: "MeterDetails",
                onInitialized: e => {
                    this.$scope.alarmGroups.grid = e.component
                    defer.resolve()
                },
                columns: [
                    {
                        dataField: "name",
                        caption: i18n('Popup', 'AssignedGroups'),
                        selectedFilterOperation: 'contains'
                    }, {
                        dataField: "id",
                        visible: false
                    }
                ]
            }
            this.$scope.alarmGroups.options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, this.$scope, this.logger, null, customGroupsGridOptions)
        }

        return defer.promise
    }
}