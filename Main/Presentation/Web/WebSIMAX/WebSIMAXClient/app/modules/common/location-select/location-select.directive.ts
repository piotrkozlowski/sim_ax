﻿import 'angular'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'

require('./location-select.scss')

var lastTextButtonEvent = null

class LocationData {
    protected _name = null
    _latitude = null
    _longitude = null

    get name() {
        return this._name ? this._name : i18n('Popup', 'LocationNothingSelected')
    }

    set name(str) {
        this._name = str
    }

    get latitude() {
        return this._latitude
    }

    set latitude(value: number) {
        this._latitude = angular.isNumber(value) ? parseFloat(value.toFixed(4)) : null
    }

    get longitude() {
        return this._longitude
    }

    set longitude(value: number) {
        this._longitude = angular.isNumber(value) ? parseFloat(value.toFixed(4)) : null
    }
}

class CurrentLocationData extends LocationData {
    markers = []
    center = [51.9189046, 19.1343787]
    mapDisabled: boolean = false
    mapZoom: number = 5

    isChanged: boolean = false

    autoCompleteService = new window['google'].maps.places.AutocompleteService()
    geocoder = new window['google'].maps.Geocoder()

    constructor(private $q: ng.IQService, private $http: ng.IHttpService, public mapObject, private $timeout: ng.ITimeoutService) {
        super()
    }

    get name() {
        return this._name ? this._name : null
    }

    set name(str) {
        this._name = str
    }

    autoCompleteResults = []

    autoCompleteDataSource: DevExpress.data.DataSource = new DevExpress.data.DataSource({
        store: new DevExpress.data.CustomStore({
            load: () => {
                var deferred = this.$q.defer()
                if (this._name && this._name != "") {
                    this.autoCompleteService.getQueryPredictions({ input: this._name }, (predictions, status) => {
                        this.autoCompleteResults = []
                        if (predictions)
                            predictions.forEach(prediction => {
                                this.autoCompleteResults.push(prediction)
                            })
                        deferred.resolve(this.autoCompleteResults)
                    })
                } else {
                    this.autoCompleteResults = []
                    deferred.resolve(this.autoCompleteResults)
                }
                return deferred.promise
            }
        })
    })

    autoCompleteOptions: DevExpress.ui.dxAutocompleteOptions = {
        dataSource: this.autoCompleteDataSource,
        placeholder: i18n('Popup', 'InsertAddress'),
        onValueChanged: (e) => {
            this._name = e.value
        },
        onItemClick: (e) => {
            this.selectLocationByPredictionObject(e.itemData)
        },
        valueExpr: 'description',
        bindingOptions: {
            value: "currentLocation.name"
        }
    }

    latitudeOptions: DevExpress.ui.dxNumberBoxOptions = {
        min: -90,
        max: 90,
        width: '100%',
        placeholder: i18n('Popup', 'InsertLatitude'),
        bindingOptions: {
            value: "currentLocation.latitude"
        },
        onEnterKey: this.updateLocationFromMemory.bind(this),
        onValueChanged: () => {
            this.isChanged = true
        },
        onFocusIn: () => {
            this.isChanged = false
        },
        onFocusOut: () => {
            if (this.isChanged)
                this.updateLocationFromMemory()
        }
    }

    longitudeOptions: DevExpress.ui.dxNumberBoxOptions = {
        min: -180,
        max: 180,
        width: '100%',
        placeholder: i18n('Popup', 'InsertLongitude'),
        bindingOptions: {
            value: "currentLocation.longitude"
        },
        onEnterKey: this.updateLocationFromMemory.bind(this),
        onValueChanged: () => {
            this.isChanged = true
        },
        onFocusIn: () => {
            this.isChanged = false
        },
        onFocusOut: () => {
            if (this.isChanged)
                this.updateLocationFromMemory()
        }
    }

    updateLocationFromMemory() {
        this.$timeout(() => {
            if (angular.isNumber(this.latitude) && angular.isNumber(this.longitude))
                this.updateLocation(this.latitude, this.longitude, true)
        }, 0)
    }

    updateLocation(latitude: number, longitude: number, centerView: boolean = false) {
        this.mapDisabled = true
        this.latitude = latitude
        this.longitude = longitude
        this.fetchPlaceAddress().then(() => {
            this.$timeout(() => {
                this.updateMarker(latitude, longitude)
                this.mapDisabled = false
                if (centerView) {
                    this.center = [this.latitude, this.longitude]
                    this.mapZoom = 12
                }
            }, 100)
        })
    }

    updateLocationButtonText(latitude: number, longitude: number) {
        this.latitude = latitude
        this.longitude = longitude
        return this.fetchPlaceAddress()
    }

    fetchPlaceAddress() {
        var deferred = this.$q.defer()
        if (angular.isNumber(this.latitude) && angular.isNumber(this.longitude)) {
            this.geocoder.geocode({
                location: { lat: this.latitude, lng: this.longitude }
            }, (result, status) => {
                if (result && result.length > 0)
                    this.name = result[0].formatted_address
                else {
                    this.name = this.latitude + ', ' + this.longitude
                }
                deferred.resolve()
            })
        } else {
            deferred.resolve()
        }
        return deferred.promise
    }

    selectLocationByPredictionObject = (prediction) => {
        this.geocoder.geocode({
            placeId: prediction.place_id
        }, result => {
            if (!result) return
            this.latitude = result[0].geometry.location.lat()
            this.longitude = result[0].geometry.location.lng()
            this.updateMarker(this.latitude, this.longitude)
            this.center = [this.latitude, this.longitude]
            this.mapZoom = 12
        })
    }

    updateMarker = (lat: number, lng: number) => {
        this.removeMarkers()
        
        this.mapObject.component.addMarker({
            location: [lat, lng]
        })
    }

    removeMarkers = () => {
        if (this.markers.length) {
            var i = this.markers.length
            while (i--) {
                this.mapObject.component.removeMarker(i)
            }
        }
    }

    reset() {
        this.latitude = null
        this.longitude = null
        this.name = null
        this.center = [51.9189046, 19.1343787]
        this.mapZoom = 5
        this.removeMarkers()
    }
}

interface LocationSelectScope extends ng.IScope {
    bindedLatitude: number
    bindedLongitude: number
    selectedLocation: LocationData
    currentLocation: CurrentLocationData

    popupOptions: DevExpress.ui.dxPopupOptions
    locationSelectButtonOptions: DevExpress.ui.dxButtonOptions
    submitLocationButtonOptions: DevExpress.ui.dxButtonOptions
    cancelButtonOptions: DevExpress.ui.dxButtonOptions
    popupVisible: boolean
    hidePopup(): void

    mapOptions: DevExpress.ui.dxMapOptions
    mapObject: any

    onInit(): void
}

export class LocationSelectDirective implements ng.IDirective {
    constructor() {

    }
    scope = {
        'loadCoords': '&coords',
        'callback': '&callback'
    }
    restrict = 'E'
    controller = ['$scope', '$q', '$timeout', '$http', 'logger', ($scope: LocationSelectScope, $q: ng.IQService, $timeout: ng.ITimeoutService, $http: ng.IHttpService, logger: LoggerService) => {
        $scope.selectedLocation = new LocationData
        $scope.currentLocation = new CurrentLocationData($q, $http, $scope.mapObject, $timeout)
        $scope.popupVisible = false
        $scope.mapObject = null

        var showPopup = () => {
            $scope.popupVisible = true
            setLocationFromAttribute()
        }

        $scope.hidePopup = () => {
            $scope.popupVisible = false
        }

        $scope.popupOptions = {
            bindingOptions: {
                visible: 'popupVisible'
            },
            title: i18n('Popup', 'SelectingLocation'),
            showTitle: true
        }

        $scope.mapOptions = {
            width: "100%",
            height: "100%",
            provider: "google",
            controls: true,
            center: [51.9189046, 19.1343787],
            autoAdjust: false,
            onClick: (e) => {
                $scope.currentLocation.updateLocation(e.location.lat, e.location.lng)
            },
            bindingOptions: {
                markers: 'currentLocation.markers',
                center: 'currentLocation.center',
                disabled: 'currentLocation.mapDisabled',
                zoom: 'currentLocation.mapZoom'
            },
            onReady: object => {
                $scope.mapObject = $scope.currentLocation.mapObject = object
                setLocationFromAttribute()
            }
        }

        var selectLocation = () => {
            $scope.selectedLocation.name = $scope.currentLocation.name
            $scope.selectedLocation.latitude = $scope.currentLocation.latitude
            $scope.selectedLocation.longitude = $scope.currentLocation.longitude
            $scope['callback']()($scope.selectedLocation.latitude, $scope.selectedLocation.longitude)
            $scope.hidePopup()
        }

        var cancel = () => {
            $scope.hidePopup()
        }

        var setLocationFromAttribute = () => {
            var coords = $scope['loadCoords'] ? $scope['loadCoords']() : null
            if ($scope.mapObject && coords) {
                $scope.currentLocation.updateLocation(coords.latitude, coords.longitude, true)
            } else {
                $scope.currentLocation.reset()
            }
        }

        $scope.locationSelectButtonOptions = {
            template: CommonHelper.getIcon('ADD_LOCATION'),
            hint: i18n('Popup', 'SelectLocation'),
            onClick: showPopup
        }

        $scope.submitLocationButtonOptions = {
            text: i18n('Common', 'Select'),
            onClick: selectLocation
        }

        $scope.cancelButtonOptions = {
            text: i18n('Common', 'Cancel'),
            onClick: cancel
        }

        // DX and angular hack :)
        if (lastTextButtonEvent) lastTextButtonEvent()
        lastTextButtonEvent = $scope.$on(EventsNames.UPDATE_COORDINATES_BUTTON_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            if (args[0] && angular.isNumber(args[0].latitude) && angular.isNumber(args[0].longitude)) {
                $scope.currentLocation.updateLocationButtonText(args[0].latitude, args[0].longitude).then(() => {
                    $scope.selectedLocation.name = $scope.currentLocation.name
                    $scope.selectedLocation.latitude = $scope.currentLocation.latitude
                    $scope.selectedLocation.longitude = $scope.currentLocation.longitude
                })
            } else {
                $scope.selectedLocation.name = undefined
            }
        })
        
    }]
    template = require("./location-select.tpl.html")
    replace = true
}