﻿import 'angular'
import { EventsNames } from '../../../helpers/events-names'
import { i18n } from '../i18n/common.i18n'

require('./confirmation-popup.scss')

interface ConfirmationScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    toolbarOptions: DevExpress.ui.dxToolbarOptions

    id: number
    popupVisible: boolean
    title: string
    confirm: () => void
    confirmQuestion: string
    yes: string
    no: string
    confirmNo: () => void
}

export class ConfirmationDirective implements ng.IDirective {
    constructor() {

    }

    scope = {

        id: "=",
        confirm: '=', 
        confirmNo: '=',       
        title: '@title',
        yes: '@yes',
        no: '@no'
    }

    restrict = 'E'
    controller = ['$scope', '$rootScope', ($scope: ConfirmationScope, $rootScope: ng.IRootScopeService) => {
        $scope.popupVisible = false

        $scope.popupOptions = {
            maxWidth: 400,
            height: 200,
            bindingOptions: {
                visible: 'popupVisible',
                title: 'title'
            },
            showCloseButton: false
        }

        $scope.toolbarOptions = {
            items: [
                {
                    widget: 'dxButton',
                    options: {
                        text: $scope.yes ? $scope.yes : i18n('Common', 'Yes'),
                        onClick: () => {
                            $scope.popupVisible = false
                            $scope.confirm()
                        }
                    }
                },
                {
                    widget: 'dxButton',
                    options: {
                        text: $scope.no ? $scope.no : i18n('Common', 'No'),
                        onClick: () => {
                            $scope.popupVisible = false
                            if ($scope.confirmNo)
                                $scope.confirmNo()
                            $rootScope.$broadcast(EventsNames.CLOSE_CONFIRMATION_POPUP, null)
                        }
                    }
                }
            ]
        }

        $scope.$on(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.confirmQuestion = (args && args[0]) ? args[0] : i18n('Popup', 'ConfirmDeleteLocation')
            $scope.popupVisible = true
        })
    }]
    template = require("./confirmation-popup.tpl.html")
}