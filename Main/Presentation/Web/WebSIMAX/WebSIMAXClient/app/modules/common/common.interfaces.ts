﻿import { ColumnModel } from '../api/api.models'

export interface GridData {
    options: DevExpress.ui.dxDataGridOptions
    grid: DevExpress.ui.dxDataGrid
    showGrid?: boolean
    columnsCount?: number
}

export interface TabsData {
    visible: boolean
    selectedTab: number
    selectedTabName: string
    tabsOptions: DevExpress.ui.dxTabsOptions
}

export interface GridBridge {
    id: number
    title: string
    items: any[]
    popupId: number | string,
    dictionaryMethodName: string
    dataMethodName: string
    dataScope: string
    columns?: ColumnModel[]
    data?: any

    onDelete?: (removed: number[]) => ng.IPromise<any[]>
    onAdd?: (added: any) => ng.IPromise<any>
    onSubmit?: (added: any[], removed: any[], inGrid: any[]) => ng.IPromise<boolean>
}