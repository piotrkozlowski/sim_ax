﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { ColumnModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common.extended-data-grid-options'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { i18n } from '../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import * as moment from 'moment'

require('./history.scss')

interface HistoryScope extends CommonScope {
    gridOptions: DevExpress.ui.dxDataGridOptions
    grid: DevExpress.ui.dxDataGrid
    showGrid: boolean
    source: any
    loadingIndicatorOptions: DevExpress.ui.dxLoadIndicatorOptions
}

export class HistoryDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        "source": "=source"
    }

    restrict = 'E'
    controller = ['$scope', '$q', '$timeout', 'logger', 'api', 'localDictionary', ($scope: HistoryScope, $q: ng.IQService, $timeout: ng.ITimeoutService, logger: LoggerService, api: Api, localDictionary: LocalDictionaryFactory) => {

        var getColDict = () => {
            var colDict = [{
                dataField: "eventDate",
                dataType: 'date',
                cssClass: 'history-column',
                caption: i18n("Common", "Date"),
                customizeText: (options) => {
                    return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                }
            },
                {
                    dataField: "eventType",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "EventType")
                },
                {
                    dataField: "description",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Description")
                }]
            return colDict
        }

        var getIssueHistoryColDict = () => {
            var colDict = [
                {
                    dataField: "startDate",
                    dataType: 'date',
                    caption: i18n("Common", "StartTime"),
                    cssClass: 'history-column',
                    customizeText: (options) => {
                        return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    }
                },
                {
                    dataField: "endDate",
                    dataType: 'date',
                    caption: i18n("Common", "EndTime"),
                    cssClass: 'history-column',
                    customizeText: (options) => {
                        return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    }
                },
                {
                    dataField: "issueStatusDesc",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Status")
                },
                {
                    dataField: "operatorDesc",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Operator")
                },
                {
                    dataField: "notes",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Notes")
                },
                {
                    dataField: "shortDescr",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Description")
                },
                {
                    dataField: "priorityDesc",
                    dataType: 'string',
                    cssClass: 'history-column',
                    caption: i18n("Common", "Priority")
                }
            ]
            return colDict
        }

        var dataRequest = () => {
            if ($scope.source.data) {
                var defer = $q.defer()
                defer.resolve($scope.source.data)
                return defer.promise
            } else return api.history[$scope.source.dataMethodName]($scope.source.id).then(result => {
                return result.result
            })
        }

        var setCustomOptions = (colDict, deferred, deferredData) => {
            var customGridOptions = {
                name: "History",
                columns: colDict,
                onInitialized: e => {
                    $scope.grid = e.component
                    deferred.resolve()
                },
                onDataReady: () => {
                    deferredData.resolve()
                },
                onContentReady: () => {
                    $scope.showGrid = true
                    $timeout(() => { if ($scope.grid) $scope.grid.updateDimensions() }, 0)
                },
                masterDetail: {
                    enabled: true,
                    template: "detail",
                    autoExpandAll: true
                },
                onCellPrepared: e => {
                    if (e.rowType === "data" && e.column.command === "expand") {
                        if (!e.data.longDescription) {
                            e.component.collapseRow(e.key)
                        }
                        e.cellElement.removeClass("dx-datagrid-expand")
                        e.cellElement.empty()
                    }
                },
                pager: {
                    showPageSizeSelector: false,
                    allowedPageSizes: [100],
                    showInfo: true,
                    infoText: i18n('Grid', 'StandardPagerText'),
                    visible: true
                }
            }
            return customGridOptions
        }

        var loadHistory = () => {
            var deferred = $q.defer()
            var deferredData = $q.defer()
            $scope.showGrid = false          
            if ($scope.source.dataMethodName == "getHistoryForIssue") {
                var colDict = getIssueHistoryColDict()
            }
            else {
                var colDict = getColDict()  
            }
            var customGridOptions = setCustomOptions(colDict, deferred, deferredData)
            $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger,
                dataRequest, customGridOptions, false)
            return $q.all([deferred.promise, deferredData.promise])
        }
        loadHistory()

        $scope.loadingIndicatorOptions = {
            width: 40,
            height: 40
        }

        $scope.$watch('source.id', (value) => {
            if ($scope.gridOptions && (<any>$scope.gridOptions).loadData) {
                $scope.showGrid = false;
                (<any>$scope.gridOptions).loadData().then((data) => {
                    if ($scope.grid) {
                        delete $scope.gridOptions.columns
                        $scope.grid.option($scope.gridOptions)
                        $scope.grid.refresh()
                        
                        $timeout(() => {
                            $scope.grid.updateDimensions()
                            $scope.showGrid = true
                        }, 0)
                    }
                })
            }
        })
    }]
    template = require("./history.tpl.html")
    replace = true
}