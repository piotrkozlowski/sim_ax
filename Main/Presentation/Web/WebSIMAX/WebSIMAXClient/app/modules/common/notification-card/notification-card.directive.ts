﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { i18n } from '../i18n/common.i18n'
import { GridData, GridBridge } from '../common.interfaces'
import { ColumnModel, NotificationModel, NotificationEntity, OperatorModel } from '../../api/api.models'
import { Permissions } from '../../../helpers/permissions'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { Details, DetailsScope } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonScope } from '../common.extended-data-grid-options'

require('./notification-card.scss')

interface NotificationCardSource {
    operatorId: number
    notificationType: string
    notifications: any
    distributors: any
}

interface NotificationCardScope extends CommonScope {
    source: NotificationCardSource

    notificationGrid: GridData

    applyButtonOptions: DevExpress.ui.dxButtonOptions
    tagBoxOptions: DevExpress.ui.dxTagBoxOptions

    setNotifications: NotificationEntity[]
    resetNotifications: NotificationEntity[]

    isDisabledApplyButton: boolean
}

export class NotificationCardDirective implements ng.IDirective {
    constructor() {
        
    }
    scope = {
        source: '='
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', '$timeout', '$http', 'logger', ($scope: NotificationCardScope, $q: ng.IQService, api: Api, $timeout: ng.ITimeoutService, $http: ng.IHttpService, logger: LoggerService) => {
        $scope.notificationGrid = <GridData>{}
        $scope.isDisabledApplyButton = true
        $scope.setNotifications = []
        $scope.resetNotifications = []

        $scope.$watch('source.notifications', (notifications) => {
            $scope.notificationGrid = <GridData>{}
            if (notifications && (<any>notifications).length > 0) 
                assignOptionsToNotificationGrid($scope.notificationGrid, notifications)
        })

        $scope.$watch('source.distributors', (distributors) => {
            if (distributors)
                $scope.tagBoxOptions = loadSelectBox(<any>distributors, $scope.source.notificationType)
        })

        var loadSelectBox = (distributors: ColumnModel[], type: string) => {
            return <any>{
                dataSource: new DevExpress.data.ArrayStore({
                    data: distributors,
                    key: 'id'
                }),
                deferRendering: false,
                searchEnabled: true,
                placeholder: i18n("Common", "SelectPlaceholder"),
                displayExpr: 'caption',
                valueExpr: 'caption',
                noDataText: i18n("Common", "NoDataTypesText"),
                hideSelectedItems: true,
                onSelectionChanged: (e) => {
                    if(e.addedItems.length > 0 || e.removedItems.length > 0)
                        $scope.isDisabledApplyButton = false
                }
            }
        }

        var loadGrid = (gridData, values) => {
            if (gridData.grid) {
                gridData.grid.option("dataSource").store().clear()
                values.forEach(value => {
                    gridData.grid.option("dataSource").store().insert({
                        action: i18n('Notification', value[1]),
                        notificationByEmail: value[2],
                        notificationBySms: value[3],
                        id: value['notificationId']
                    })
                })
                gridData.grid.refresh()
            }
        }

        var changeNotification = (id: number, isEmailValueChanged: boolean, isSet: boolean) => {
            $scope.isDisabledApplyButton = false

            var entity = new NotificationEntity()
            entity.id = id
            entity.isEmailValue = isEmailValueChanged

            if (isSet) {
                var isAlreadyInReset = false
                for (var i = 0; i < $scope.resetNotifications.length; i++) {
                    if ($scope.resetNotifications[i].id == id) {
                        $scope.resetNotifications.splice(i)
                        isAlreadyInReset = true
                        break
                    }
                }
                if (!isAlreadyInReset)
                    $scope.setNotifications.push(entity)
            }
            else {
                var isAlreadyInSet = false
                for (var i = 0; i < $scope.setNotifications.length; i++) {
                    if ($scope.setNotifications[i].id == id) {
                        $scope.setNotifications.splice(i)
                        isAlreadyInSet = true
                        break
                    }
                }
                if (!isAlreadyInSet)
                    $scope.resetNotifications.push(entity)
            }
        }

        var assignOptionsToNotificationGrid = (gridData: GridData, values) => {
            var defer = $.Deferred()
            gridData.showGrid = false
            var customGridOptions = 
                Permissions.hasPermissions([Permissions.OPERATOR_NOTIFICATIONS_EDIT]).then(OPERATOR_NOTIFICATIONS_EDIT => {
                    var customGridOptions = {
                        columns: [
                            {
                                dataField: 'action',
                                caption: i18n('Notification', 'Action'),
                                allowEditing: false
                            }, {
                                dataField: 'notificationByEmail',
                                caption: i18n('Notification', 'NotificationByEmail'),
                                allowEditing: OPERATOR_NOTIFICATIONS_EDIT,
                                trueText: "Prawda",
                                falseText: "Fałsz"
                            }, {
                                dataField: 'notificationBySms',
                                caption: i18n('Notification', 'NotificationBySms'),
                                allowEditing: OPERATOR_NOTIFICATIONS_EDIT,
                                trueText: "Prawda",
                                falseText: "Fałsz"
                            }
                        ],
                        onInitialized: e => {
                            gridData.grid = e.component
                            loadGrid(gridData, values)
                            defer.resolve()
                        },
                        onDataReady: e => {
                            loadGrid(gridData, values)
                            gridData.showGrid = true
                        },
                        onRowUpdated: (e) => {
                            var isEmailChanged = [undefined, null].indexOf(e.data.notificationByEmail) < 0
                            var isSet = isEmailChanged ? e.key.notificationByEmail : e.key.notificationBySms
                            changeNotification(e.key.id, isEmailChanged, isSet)
                        }
                    }
                    gridData.options = new CommonExtendedDataGridOptions(CommonHelper.createEditGrid, $scope, logger, null, customGridOptions)
            })

            return defer.promise()
        }

        $scope.applyButtonOptions = {
            text: i18n('Common', 'Apply'),
            bindingOptions: {
                disabled: 'isDisabledApplyButton'
            },
            onClick: e => {
                var tagBox = angular.element('.tag-box').dxTagBox('instance')
                if (tagBox) {
                    var model = new NotificationModel()
                    model.operatorId = $scope.source.operatorId
                    model.type = $scope.source.notificationType
                    model.distributorIds = tagBox.option('selectedItems').map(distributor => {
                        return distributor.id
                    })
                    model.set = $scope.setNotifications
                    model.reset = $scope.resetNotifications
                    api.users.changeNotification(model).then(result => {
                        if (!result) {
                            CommonHelper.openInfoPopup(i18n('Popup', 'NotificationChangeError'), 'error', 5000)
                        }
                        else {
                            var values = []
                            switch ($scope.source.notificationType) {
                                case 'task':
                                    values = result.notificationTasks.values
                                    break
                                case 'issue':
                                    values = result.notificationIssues.values
                                    break
                                case 'location':
                                    values = result.notificationLocations.values
                                    break
                                case 'workorder':
                                    values = result.notificationWorkOrders.values
                                    break
                            }
                            loadGrid($scope.notificationGrid, values)
                            $scope.isDisabledApplyButton = true
                            $scope.setNotifications = []
                            $scope.resetNotifications = []
                        }
                    })
                }
            }
        }
    }]
    template = require("./notification-card.tpl.html")
    replace = true
}