﻿import 'angular'
import { i18n } from './../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'

require("./map.scss")

declare var google: any
declare var MarkerClusterer: any
declare var OverlappingMarkerSpiderfier: any

class MarkerDictionary {
    marker: any
    id: number
    type: number
    parentId: number
    childIds: number[] = []
}

export interface MapScope extends ng.IScope {

    mapOptions: DevExpress.ui.dxMapOptions
    mapdata: any[]
    selectedrowsdata: any[]
    mapCenter: any
    mapZoom: number
    mapComponent: DevExpress.ui.dxMap,
    googleMap: any
    markersDictionary: MarkerDictionary[]
    googleMapDeffered: JQueryDeferred<any>
    mapCluster: any
    mapSpiderifier: any


    isClustering: boolean
    zeroMarkers: boolean
    isMapFullscreen: boolean
    isRowClicked: boolean


    //helper methods
    checkLatitude: (Lat: any) => boolean
    checkLongitude: (Lon: any) => boolean
    mapClusterOptions: () => any
    isParentMarkerAdded: (parentPos: any) => boolean

    //marker methods
    createTooltip: (name: string) => any
    createMarker: (pos: any, iconSrc: string, dataRow: any, name: string, markerType?: number) => any
    createMarkerDictionary: (marker: any, id: number, type?: number, parentId?: number, childIds?: number[]) => any
    showMarkers: () => void
    uncolorMarkers: (oldMarkersDict: any[]) => void
    colorMarkers: () => void
    parentMarkerClicked: (row: any) => void
    markerClicked: (row: any) => void

    //map methods
    zoomCenter: () => void
    zoomIn: () => void
    updateMapMarkers: (data: any[]) => void
    resizeMap: () => void
    closeMap: () => void

}


export class MapDirective implements ng.IDirective {

    private oldTooltip: any
    private newTooltip: any
    private oldSelectedMarkers: any[] = []

    constructor() { }

    scope = {
        mapdata: '='
    }

    link = ($scope, element, $attrs) => {
        $scope.$watchCollection("mapdata", value => {
            $scope.mapdata = value
            $scope.googleMapDeffered.promise().then(() => {
                $scope.updateMapMarkers(value)
            })
        })
    }

    restrict = 'E'
    template = require("./map.tpl.html")

    controller = ['$rootScope', '$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($rootScope: ng.IRootScopeService, $scope: MapScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        //map settings
        $scope.mapCenter = [51.9189046, 19.1343787]
        $scope.mapZoom = 5
        $scope.googleMapDeffered = $.Deferred<any>()
        $scope.isClustering = true
        $scope.isMapFullscreen = false
        $scope.mapOptions = {
            width: '100%',
            height: '100%',
            provider: "google",
            controls: true,
            autoAdjust: true,
            bindingOptions: {
                center: 'mapCenter',
                zoom: 'mapZoom'
            },
            onInitialized: e => {
                $scope.mapComponent = e.component
            },
            onReady: e => {
                $scope.googleMap = e.originalMap
                $scope.googleMapDeffered.resolveWith($scope.googleMap)
                $scope.updateMapMarkers($scope.mapdata)
            }
        }
        //resize map event from outer controllers
        $scope.$on(EventsNames.REFRESH_MAP_SIZE, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.mapComponent.repaint()
        })
        //zoom on marker from selected grid row event
        $scope.$on(EventsNames.GRIDSTER_ROW_CLICKED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.selectedrowsdata = args[0]
            $scope.uncolorMarkers(this.oldSelectedMarkers)
            if ($scope.selectedrowsdata && $scope.selectedrowsdata.length === 0) {
                $scope.isRowClicked = false
                $scope.zoomCenter()
            } else {
                $scope.zoomIn()
                $scope.isRowClicked = true
                $scope.colorMarkers()
            }

        })

        //helper methods
        //checking position
        $scope.checkLatitude = (Lat: any) => {
            return angular.isNumber(Lat) && (Lat >= -90) && (Lat <= 90)
        }

        $scope.checkLongitude = (Lon: any) => {
            return angular.isNumber(Lon) && (Lon >= -180) && (Lon <= 180)
        }
        //map clustering options
        $scope.mapClusterOptions = () => {
            return {
                zoomOnClick: true,
                gridSize: 30,
                maxZoom: 14,
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            }
        }

        $scope.isParentMarkerAdded = (parentPos: any) => {
            return ($scope.markersDictionary.map(markerDictionary => {
                return markerDictionary.marker
            }).filter(mark => {
                return mark.getPosition().lng().toFixed(5) == parentPos.lng.toFixed(5) && mark.getPosition().lat().toFixed(5) == parentPos.lat.toFixed(5)
            }).length >= 1)
        }
        //marker methods
        //creating tooltip
        $scope.createTooltip = (name: string) => {
            return new google.maps.InfoWindow({
                content: name,
                pixelOffset: new google.maps.Size(0, -15)
            })
        }
        //creating marker
        $scope.createMarker = (pos: any, iconSrc: string, dataRow: any, name: string, markerType: number = 0) => {
            var marker = new google.maps.Marker({
                position: pos,
                map: $scope.googleMap,
                icon: iconSrc

            })
            var infoTooltip = $scope.createTooltip(name)
            marker.addListener('click', (e) => {
                if (this.oldTooltip)
                    this.oldTooltip.close()
                if (markerType === 0) {
                    $scope.markerClicked(dataRow)
                }
                else {
                    $scope.parentMarkerClicked(dataRow)
                }
                this.newTooltip = infoTooltip
                infoTooltip.setPosition(e.latLng)
                infoTooltip.open($scope.googleMap)
                this.oldTooltip = this.newTooltip
            })

            return marker
        }
        //createing marker dictionary
        $scope.createMarkerDictionary = (marker: any, id: number, type: number = 0, parentId: number = null, childIds: number[] = []) => {
            var markerDictionary = new MarkerDictionary()
            markerDictionary.marker = marker
            markerDictionary.id = id
            markerDictionary.type = type
            markerDictionary.parentId = parentId
            markerDictionary.childIds = childIds

            return markerDictionary
        }
        //showing/hiding clusters
        $scope.showMarkers = () => {
            $scope.isClustering = !$scope.isClustering
            $timeout(() => {
                $scope.updateMapMarkers($scope.mapdata)
                $scope.mapComponent.repaint()
            }, 0)
        }
        //reseting colors of markers
        $scope.uncolorMarkers = (oldMarkersDictionary: any[]) => {
            if (oldMarkersDictionary.length > 0) {
                oldMarkersDictionary.map(oldMarkerDict => {
                    if (oldMarkerDict.type == CommonHelper.parentMarkerId) {
                        oldMarkerDict.marker.setIcon('images/red_hub.png')
                    } else {
                        oldMarkerDict.marker.setIcon('images/red_radio.png')
                    }
                })
                oldMarkersDictionary = []
            }
        }
        //coloring selected rows' markers
        $scope.colorMarkers = () => {
            $scope.markersDictionary.forEach(markerDict => {
                $scope.selectedrowsdata.forEach(row => {
                    if (row[CommonHelper.serialNumberField] === markerDict.id || row[CommonHelper.idField] === markerDict.id) {
                        this.oldSelectedMarkers.push(markerDict)
                        markerDict.marker.setIcon('images/green_radio.png')
                    }
                    if (row[CommonHelper.serialNumberParentField] === markerDict.id) {
                        this.oldSelectedMarkers.push(markerDict)
                        markerDict.marker.setIcon('images/green_hub.png')
                    }
                })
            })
        }

        $scope.parentMarkerClicked = (row: any) => {
            $scope.uncolorMarkers(this.oldSelectedMarkers)
            var id = row[CommonHelper.serialNumberParentField]
            $scope.markersDictionary.forEach(markerDict => {
                if (markerDict.parentId === id || markerDict.id === id) {
                    this.oldSelectedMarkers.push(markerDict)
                    if (markerDict.type === CommonHelper.deviceMarkerId)
                        markerDict.marker.setIcon('images/green_radio.png')
                    else
                        markerDict.marker.setIcon('images/green_hub.png')
                }
            })
            $rootScope.$broadcast(EventsNames.MARKER_CLICKED, { row: row, parent: true })
        }

        $scope.markerClicked = (row: any) => {
            $scope.uncolorMarkers(this.oldSelectedMarkers)
            var id = row[CommonHelper.serialNumberField] != null ? row[CommonHelper.serialNumberField] : row[CommonHelper.idField] == null ? null : row[CommonHelper.idField]
            var parentId = row[CommonHelper.serialNumberParentField]
            $scope.markersDictionary.forEach(markerDict => {
                if (row[CommonHelper.serialNumberField] === markerDict.id || row[CommonHelper.idField] === markerDict.id) {
                    this.oldSelectedMarkers.push(markerDict)
                    markerDict.marker.setIcon('images/green_radio.png')
                }
                if (row[CommonHelper.serialNumberParentField] === markerDict.id) {
                    this.oldSelectedMarkers.push(markerDict)
                    markerDict.marker.setIcon('images/green_hub.png')
                }
            })
            $rootScope.$broadcast(EventsNames.MARKER_CLICKED, { row: row, parent: false })
        }

        //map methods
        //zoom on all markers
        $scope.zoomCenter = () => {
            var markerBounds = new google.maps.LatLngBounds()
            for (var i in $scope.markersDictionary) {
                markerBounds.extend($scope.markersDictionary[i].marker.getPosition())
            }
            $timeout(() => {
                $scope.googleMap.fitBounds(markerBounds)
                $scope.googleMap.panToBounds(markerBounds)
            }, 0)
        }
        //zoom on selected markers
        $scope.zoomIn = () => {
            var selectedRowsPosition = new google.maps.LatLng()
            var markerBounds = new google.maps.LatLngBounds()
            $scope.selectedrowsdata.forEach(row => {
                if ($scope.checkLongitude(row.deviceLon) && $scope.checkLatitude(row.deviceLat)) {
                    markerBounds.extend(new google.maps.LatLng(row.deviceLat, row.deviceLon))
                    if ($scope.checkLongitude(row.parentLon) && $scope.checkLatitude(row.parentLat))
                        markerBounds.extend(new google.maps.LatLng(row.parentLat, row.parentLon))
                }
                else if ($scope.checkLatitude(row.latitude) && $scope.checkLongitude(row.longitude)) {
                    markerBounds.extend(new google.maps.LatLng(row.latitude, row.longitude))
                }
            })
            $timeout(() => {
                $scope.googleMap.fitBounds(markerBounds)
                $scope.googleMap.panToBounds(markerBounds)
            }, 0)
        }
        //updating markers
        $scope.updateMapMarkers = (data: any) => {

            if (data) {
                $scope.mapSpiderifier = new OverlappingMarkerSpiderfier($scope.googleMap, {
                    nearbyDistance: 3,
                    keepSpiderfied: true
                })
                $scope.zeroMarkers = false
                if ($scope.mapCluster) {
                    $scope.mapCluster.clearMarkers()
                    $scope.mapCluster.resetViewport()
                }
                $scope.markersDictionary = []

                var id: number
                var parentId: number
                var pos: any
                var parentPos: any

                data.forEach(dataRow => {
                    if (dataRow.hasOwnProperty('latitude') || dataRow.hasOwnProperty('longitude')) {
                        var lat = dataRow['latitude']
                        var lon = dataRow['longitude']
                        id = dataRow[CommonHelper.idField]
                    } else {
                        var deviceLat = dataRow['deviceLat']
                        var deviceLon = dataRow['deviceLon']
                        var parentLat = dataRow['parentLat']
                        var parentLon = dataRow['parentLon']
                        id = dataRow[CommonHelper.serialNumberField]
                        parentId = dataRow[CommonHelper.serialNumberParentField]
                    }
                    if ($scope.checkLatitude(lat) && $scope.checkLongitude(lon)) {
                        pos = { lat: lat, lng: lon }
                    }
                    else if ($scope.checkLatitude(deviceLat) && $scope.checkLongitude(deviceLon)) {
                        pos = { lat: deviceLat, lng: deviceLon }
                    }

                    if ($scope.checkLatitude(parentLat) && $scope.checkLongitude(parentLon)) {
                        parentPos = { lat: parentLat, lng: parentLon }
                    }
                    if (pos) {
                        var name = dataRow[CommonHelper.serialNumberField] != null ? dataRow[CommonHelper.serialNumberField].toString() : dataRow['name']
                        var mapMarker = $scope.createMarker(pos, 'images/red_radio.png', dataRow, name)
                        var markerDictionary = $scope.createMarkerDictionary(mapMarker, id, CommonHelper.deviceMarkerId, parentId)
                        $scope.markersDictionary.push(markerDictionary)
                        $scope.mapSpiderifier.addMarker(mapMarker)
                    }
                    if (parentPos && !$scope.isParentMarkerAdded(parentPos)) {
                        var name = dataRow[CommonHelper.serialNumberParentField].toString()
                        var mapMarker = $scope.createMarker(parentPos, 'images/red_hub.png', dataRow, name, CommonHelper.parentMarkerId)
                        var markerDictionaryParent = $scope.createMarkerDictionary(mapMarker, dataRow[CommonHelper.serialNumberParentField], CommonHelper.parentMarkerId, null)
                        $scope.markersDictionary.push(markerDictionaryParent)
                        $scope.mapSpiderifier.addMarker(mapMarker)
                    }
                })

                if ($scope.isClustering) {
                    $scope.mapCluster = new MarkerClusterer($scope.googleMap, $scope.markersDictionary.map(markerDict => {
                        return markerDict.marker
                    }), $scope.mapClusterOptions())
                }

                $scope.zoomCenter()
                if ($scope.isRowClicked) {
                    $scope.colorMarkers()
                }
            }
            else {
                $scope.zeroMarkers = true
            }
        }

        $scope.resizeMap = () => {
            $scope.isMapFullscreen = !$scope.isMapFullscreen
            $timeout(() => { $scope.mapComponent.repaint() }, 100)
        }

        $scope.closeMap = () => {
            $rootScope.$broadcast(EventsNames.CLOSE_MAP)
        }
    }]
}