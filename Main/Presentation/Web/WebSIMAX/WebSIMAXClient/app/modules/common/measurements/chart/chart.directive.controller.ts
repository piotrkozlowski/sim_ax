﻿import 'angular'
import { LoggerService } from  '../../common.module'
import { EventsNames } from '../../../../helpers/events-names'
import { i18n } from '../../i18n/common.i18n'
import { UserConfigHelper } from '../../../../helpers/user-config-helper'
import { MeasurementDataType } from '../measurements.controller'
import { CommonHelper } from '../../../../helpers/common-helper'
import * as moment from 'moment'

export interface MeasurementsChartBridge {
    data: any[]
    filterId: number
    selectedTypes: MeasurementDataType[]
    tickInterval: any,
    dataTypesChartStorageName?: string
    updateChart?: boolean
    clearChart?: boolean
}

interface VisibleType {
    id: number,
    state: boolean,
    axisName: string,
    seriesName: string
}

interface MeasurementsChartScope extends ng.IScope {
    chartOptions: DevExpress.viz.charts.dxChartOptions
    noSeriesSelected: boolean
    isLegendVisible: boolean
    isRotated: boolean
    source: MeasurementsChartBridge

    devices?: any[]
    meters?: any[]
    groups?: any[]

    showAllSeries: boolean

    legendButtonOptions: DevExpress.ui.dxButtonOptions
    rotateButtonOptions: DevExpress.ui.dxButtonOptions
}

export class ChartDirectiveController {
    chart: DevExpress.viz.charts.dxChart
    selectedTypes: MeasurementDataType[]
    visibleTypes: VisibleType[]
    data: any[]
    chartStorageName: string
    showAllSeries: boolean

    legendButton: DevExpress.ui.dxButton
    rotateButton: DevExpress.ui.dxButton

    constructor(private $scope: MeasurementsChartScope, private $filter: ng.IFilterService, private $q: ng.IQService, private $timeout: ng.ITimeoutService, private logger: LoggerService) {
        var dataExists = false
        $scope.noSeriesSelected = true
        $scope.isLegendVisible = UserConfigHelper.getUserConfig(UserConfigHelper.IsLegendVisible, false)
        $scope.isRotated = UserConfigHelper.getUserConfig(UserConfigHelper.IsRotated, false)

        $scope.$watch(() => {
            if (this.visibleTypes) return this.visibleTypes.every(vt => !vt.state)
            else return false
        }, noSeriesSelected => {
            $scope.noSeriesSelected = noSeriesSelected
        })

        $scope.$on(EventsNames.UPDATE_CHART, () => {
            this.refreshChart()
        })

        $scope.$watchCollection("source", (source: MeasurementsChartBridge) => {
            if (source) {
                if (this.chart && source.data) {
                    this.updateChart(source.data, source.selectedTypes, source.filterId, $scope.source.tickInterval, $scope.groups, $scope.devices, $scope.meters, source.dataTypesChartStorageName)
                }
                if (source.updateChart) {
                    $timeout(() => {
                        this.refreshChart()
                    }, 500)
                    source.updateChart = false
                }
                if (source.clearChart) {
                    $timeout(() => {
                        this.chart.option('dataSource', [])
                        this.refreshChart()
                    }, 0)
                    source.clearChart = false
                }
            }
        })

        var onLegendClickTimeout
        $scope.chartOptions = {
            onInitialized: e => {
                $scope['showChart'] = false
                this.chart = e.component
                this.updateChart($scope.source.data, $scope.source.selectedTypes, $scope.source.filterId, $scope.source.tickInterval, $scope.groups, $scope.devices, $scope.meters, $scope.source.dataTypesChartStorageName)
            },
            height: '100%',
            containerBackgroundColor: window['config'].BACKGROUND_COLOR_CHART,
            dataSource: [],
            bindingOptions: {
                rotated: 'isRotated'
            },
            scrollingMode: 'all',
            zoomingMode: 'all',
            useAggregation: false,
            argumentAxis: {
                argumentType: 'datetime',
                tickInterval: { hours: 2 },
                grid: {
                    visible: true
                },
                tick: {
                    visible: true,
                    length: 10,
                    color: 'blue',
                    width: 2
                },
                label: {
                    customizeText: (options) => {
                        return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    }
                }
            },
            valueAxis: [
                {
                    name: "number",
                    valueType: "numeric"
                },
                {
                    name: "date",
                    valueType: "string",
                    label: {
                        customizeText: (options) => {
                            return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                        }
                    }
                }
            ],
            tooltip: {
                enabled: true,
                customizeTooltip: (point: any) => {
                    var pointObject: DevExpress.viz.charts.ChartPoint = point.point
                    var unitName = (<any>pointObject.series.tag).unitName
                    var tag = point.point.tag
                    var color = point.point.getColor()
                    var value = point.valueText
                    var argument = moment(new Date(point.argument)).format(i18n("Common", "DateTimeFormatMomentJS"))
                    if ((<any>pointObject.series.tag).isDate) {
                        value = moment(new Date(point.value)).format(i18n("Common", "DateTimeFormatMomentJS"))
                    }
                    return {
                        html: "<strong><span style='color:" + color + ";'>" + point.seriesName + ":</span> " + value + " " + this.encloseUnit(unitName) + "</strong><br/>" + argument + (tag ? "<br/>" + tag : ''),
                    }
                },
                zIndex: 10000
            },
            commonSeriesSettings: {
                tagField: "info",
                argumentField: "date",
                ignoreEmptyPoints: true,
                axis: "number",
                point: {
                    size: 4,
                    hoverStyle: {
                        size: 3
                    }
                }
            },
            dataPrepareSettings: {
                checkTypeForAllData: true
            },
            legend: {
                visible: $scope.isLegendVisible,
                orientation: 'horizontal',
                horizontalAlignment: 'center',
                itemTextPosition: 'right',
                rowItemSpacing: 2,
                columnItemSpacing: 4,
                columnCount: 10,
                customizeText: (seriesInfo) => {
                    if (!this.chart || !seriesInfo) return ''
                    var series = null
                    try {
                        series = this.chart.getSeriesByPos(seriesInfo.seriesIndex)
                    } catch (e) { }
                    if (series == null) return ''
                    var seriesTag: any = series.tag
                    var text = seriesInfo.seriesName.substring(0, 20) + " " + this.encloseUnit(seriesTag.unitName)
                    return text
                },
                customizeHint: function () {
                    return this.seriesName
                }
            },
            onLegendClick: e => {
                if (onLegendClickTimeout) clearTimeout(onLegendClickTimeout)
                let currentSeries: DevExpress.viz.charts.BaseSeries = e.component.getSeriesByPos(e.target.index);
                let visibleType: VisibleType
                this.visibleTypes.some(vt => {
                    if (vt.id == currentSeries.tag["typeId"]) {
                        visibleType = vt
                        return true
                    }
                    return false
                })

                if (visibleType) {
                    if (currentSeries.isVisible()) {
                        visibleType.state = false
                        currentSeries.hide()
                    } else {
                        visibleType.state = true
                        currentSeries.show()
                    }
                }

                onLegendClickTimeout = setTimeout(() => {
                    this.updateValueAxes(true)
                    UserConfigHelper.setUserConfig($scope.source.dataTypesChartStorageName, this.visibleTypes)
                }, 500)
            },
            animation: {
                enabled: false
            }
        }

        $scope.legendButtonOptions = {
            template: CommonHelper.getIcon('LEGEND'),
            hint: i18n('Common', 'Legend'),
            onInitialized: e => {
                this.legendButton = e.component
            },
            onClick: () => {
                $scope.isLegendVisible = !$scope.isLegendVisible
                UserConfigHelper.setUserConfig(UserConfigHelper.IsLegendVisible, $scope.isLegendVisible)
                this.chart.option('legend.visible', $scope.isLegendVisible)
            }
        }

        $scope.rotateButtonOptions = {
            template: CommonHelper.getIcon('ROTATE'),
            hint: i18n('Common', 'Rotate'),
            onInitialized: e => {
                this.rotateButton = e.component
            },
            onClick: () => {
                $scope.isRotated = !$scope.isRotated
                UserConfigHelper.setUserConfig(UserConfigHelper.IsRotated, $scope.isRotated)
            }
        }
    }

    private setupButtons(result: any) {
        if (!result || result.length == 0) {
            this.legendButton.option('disabled', true)
            this.rotateButton.option('disabled', true)
        }
        else {
            this.legendButton.option('disabled', false)
            this.rotateButton.option('disabled', false)
        }
    }

    private setupTypes(types: MeasurementDataType[], result: any) {
        this.selectedTypes = types

        var mapTypes = (types: MeasurementDataType[]): any[] => {
            return types.map(type => {
                return {
                    id: type.id,
                    state: false,
                    axisName: type.unitType.toString(),
                    seriesName: type.name
                }
            })
        }

        var selectFirst = () => {
            if (result && result.length > 0) {
                var first = result[0].dataType
                this.visibleTypes.forEach(vt => {
                    if (vt.id == first) {
                        vt.state = true
                    }
                })
            }
        }

        var selectExisting = () => {
            if (result && result.length > 0) {
                var resultIds = result.map(r => { return r.dataType })
                this.visibleTypes = this.visibleTypes.filter(vt =>
                    resultIds.some(ri => ri == vt.id)
                )
            }
            else {
                this.visibleTypes = []
            }
        }

        var refreshTypes = () => {
            this.visibleTypes = mapTypes(types)
            selectFirst()
        }

        if (!this.visibleTypes) {
            this.visibleTypes = UserConfigHelper.getUserConfig(this.chartStorageName, undefined)
            if (!this.visibleTypes) {
                refreshTypes()
                return
            }
        }

        var newTypes = mapTypes(types)
        var newTypesIds = newTypes.map(t => t.id)
        if (!newTypes.every(nt => !this.visibleTypes.every(vt => !(nt.id == vt.id && nt.seriesName == vt.seriesName)))) {
            var lastTypes = this.visibleTypes
            refreshTypes()
            this.visibleTypes.forEach(t => {
                lastTypes.forEach(lt => {
                    if (t.id == lt.id) t.state = lt.state
                })
            })
        }

        selectExisting()
    }

    private getValueAxes(types: MeasurementDataType[]): DevExpress.viz.charts.ChartValueAxis[] {
        let axes = <DevExpress.viz.charts.ChartValueAxis[]>[]
        let counter = 0
        types.forEach((type) => {
            if (!axes[type.unitType]) {
                axes[type.unitType] = {
                    name: type.unitType.toString(),
                    valueType: "numeric",
                    title: this.encloseUnit(type.unitName),
                    position: counter++ % 2 ? "right" : "left",
                    visible: true,
                    showZero: false
                }
                if (type['isDate']) {
                    axes[type.unitType].valueType = "string"
                    axes[type.unitType].label = {
                        customizeText: (options) => {
                            return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                        }
                    }
                }
            }
        })

        return axes.filter(a => <any>a)
    }

    private setValueAxesTitles(axes: DevExpress.viz.charts.ChartValueAxis[]): DevExpress.viz.charts.ChartValueAxis[] {
        let counter = 0
        axes.forEach(axis => {
            let selectedType: MeasurementDataType
            this.selectedTypes.some(st => {
                if (st.unitType.toString() == axis.name) {
                    selectedType = st
                    return true
                }
                return false
            })
            if (this.visibleTypes) {
                let axisVisibleTypes = this.visibleTypes.filter(vt => vt.axisName == selectedType.unitType.toString())
                if (axisVisibleTypes.every(vt => !vt.state)) {
                    axis.title = ""
                } else {
                    axis.title = this.encloseUnit(selectedType.unitName)
                    axis.position = counter++ % 2 ? "right" : "left"
                }
            }
        })
        return axes
    }

    private encloseUnit(unit) {
        return unit.trim() ? "[" + unit + "]" : ""
    }

    private updateValueAxes(onlyTitles: boolean = false) {
        this.chart.option("valueAxis", onlyTitles ? this.setValueAxesTitles(this.chart.option("valueAxis")) : this.setValueAxesTitles(this.getValueAxes(this.selectedTypes)))
        if (this.chart['series'])
            this.chart.getAllSeries().forEach(series => {
                if (this.visibleTypes) {
                    this.visibleTypes.some(vt => {
                        if (vt.seriesName == series.name) {
                            if (vt.state != series.isVisible()) {
                                vt.state ? series.show() : series.hide()
                            }
                            return true
                        }
                        return false
                    })
                }
            })

        this.updateChartSeries(this.selectedTypes)
    }

    private updateChartSeries(selectTypes: MeasurementDataType[]) {
        selectTypes = selectTypes.filter(st => this.visibleTypes.some(vt => vt.id == st.id))
        var series = selectTypes.map(type => {
            var isVisible = this.showAllSeries || (this.visibleTypes && this.visibleTypes.some(vt => vt.id == type.id && vt.state))
            return {
                name: type.name,
                valueField: type.name,
                tag: <any>{
                    unitType: type.unitType,
                    unitName: type.unitName,
                    typeId: type.id,
                    isDate: type['isDate']
                },
                type: type['chartType'],
                visible: isVisible,
                axis: type.unitType.toString()
            }
        })

        if (series.length > 0)
            this.chart.option({
                series: series
            })
    }

    private updateChart(result = [], selectedTypes, filterId, tickInterval, groups: any[], devices: any[], meters: any[], chartTypesStorage?: any) {
        this.chartStorageName = chartTypesStorage
        this.chart.beginUpdate()

        this.setupTypes(selectedTypes, result)
        this.updateValueAxes()
        var points = []
        this.data = []
        result.forEach(record => {
            var id = null
            switch (filterId) {
                case 1:
                    id = record.locationId
                    break
                case 2:
                    id = record.serialNumber
                    break
                case 3:
                    id = record.meterId
                    break
            }
            var pointName = record.date + ' ' + id
            if (!points[pointName]) {
                var info = ''
                switch (filterId) {
                    case 1:
                        var title = record.locationId
                        if (groups && groups.length > 0) title = groups.filter(g => g[CommonHelper.idField] == record.locationId).map(g => g.name)[0]
                        if (!title) title = record.locationId
                        info = i18n('Common', 'Location') + ': ' + title
                        break
                    case 2:
                        var title = record.serialNumber
                        if (devices && devices.length > 0) title = devices.filter(d => d[CommonHelper.idField] == record.serialNumber).map(d => d.name)[0]
                        if (!title) title = record.serialNumber
                        info = i18n('Common', 'Device') + ': ' + title
                        break
                    case 3:
                        var title = record.meterId
                        if (meters && meters.length > 0) title = meters.filter(m => m[CommonHelper.idField] == record.meterId).map(m => m.name)[0]
                        if (!title) title = record.meterId
                        info = i18n('Common', 'Meter') + ': ' + title
                        break
                }
                points[pointName] = {
                    date: new Date(record.date),
                    info: info
                }
            }
            var field
            if (selectedTypes.some(value => {
                if (value.id == record.dataType) {
                    field = value.name
                    return true
                }
                return false
            })) {
                points[pointName][field] = record.value
            }
        })

        for (var i in points) {
            this.data.push(points[i])
        }

        this.updateChartSeries(selectedTypes)
        this.chart.option({
            argumentAxis: {
                tickInterval: tickInterval
            },
            dataSource: this.data
        })
        this.chart.render({ animate: false })
        this.chart.endUpdate()

        this.setupButtons(result)
    }

    private refreshChart() {
        if (this.chart) {
            try {
                this.chart.render()
            } catch (e) {
            }
        }
    }
}