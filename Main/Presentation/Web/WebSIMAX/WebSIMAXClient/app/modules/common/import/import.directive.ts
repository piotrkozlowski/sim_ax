﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FileModel, FileColumnModel, FormValuesModel, FieldNameValueModel, LocationFormValuesModel, ImportDataModel, ImportStatusModel, ImportProgressModel, SimpleValueModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridBridge } from '../../common/common.interfaces'
import { i18n } from './../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'
import { ImportStatuses } from '../../../helpers/import-statuses'
import { FileUploaderOptions } from '../file-uploader/file-uploader.directive'
import * as _ from 'underscore'

require('./import.scss')

export interface ImportOptions {
    dictionaryMethodName: string
    requestMethodType: string
    popupName: string
}

interface ImportScope extends CommonScope {
    popoverVisible: boolean
    gridPopupVisible: boolean
    importPopupVisible: boolean
    
    gridPopupOptions: DevExpress.ui.dxPopupOptions
    importPopupOptions: DevExpress.ui.dxPopupOptions
    popoverOptions: DevExpress.ui.dxPopoverOptions
    popover: DevExpress.ui.dxPopover
    
    
    initButtonOptions: DevExpress.ui.dxButtonOptions
    startButtonOptions: DevExpress.ui.dxButtonOptions
    stopButtonOptions: DevExpress.ui.dxButtonOptions
    summaryToastOptions: DevExpress.ui.dxToastOptions
    summaryToast: DevExpress.ui.dxToast
    importGridOptions: DevExpress.ui.dxDataGridOptions
    importGrid: DevExpress.ui.dxDataGrid
    importProgressOptions: DevExpress.ui.dxProgressBarOptions

    fileUploaderOptions: FileUploaderOptions
    
    showGrid: boolean

    guid: string
    isValidated: boolean
    isStarted: boolean
    isSuccedded: boolean
    uploadedPercent: number

    isInitButtonDisabled: () => boolean
    isStartButtonDisabled: () => boolean

    columns: ColumnModel[]
    options: ImportOptions

    lastStatus: ImportProgressModel
}

export class ImportDirective implements ng.IDirective {

    hideSettingsPopover: () => void

    constructor() {
    }

    restrict = 'E'
    scope = {
        options: '='
    }

    template = require("./import.tpl.html")

    controller = ['$rootScope', '$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($rootScope: ng.IRootScopeService, $scope: ImportScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {

        var entities = []
        var timeout

        
        $scope.popoverVisible = false
        $scope.gridPopupVisible = false
        $scope.fileUploaderOptions = <FileUploaderOptions>{
            acceptValue: 'xls,xlsx,csv',
            uploaderVisible: true,
            onReadEnd: (reader, fileName) => {
                $scope.showGrid = false
                localDictionary[$scope.options.dictionaryMethodName]().then(columns => {
                    var fileModel = new FileColumnModel()
                    fileModel.fileBytes = reader.result
                    fileModel.physicalFileName = fileName
                    fileModel.columns = columns
                    showPopup()
                    createGrid()
                    api.app.importExcelFile(fileModel).then(result => {
                        updateGrid(result.values)
                        $scope.showGrid = true
                    }).catch(rej => {
                        $scope.showGrid = true
                        CommonHelper.openInfoPopup(i18n('Popup', 'ServerErrorImport'), 'error', 5000)
                    })
                })
            },
            onSubmitClick: () => {
                updateGrid([])
                if (this.hideSettingsPopover) {
                    this.hideSettingsPopover()
                }
            }
        }
        
        $scope.uploadedPercent = 0
        $scope.importPopupVisible = false
        $scope.gridPopupOptions = {
            showTitle: true,
            showCloseButton: true,
            title: $scope.options.popupName + ' (xls, xlsx, csv)',
            fullScreen: true,
            bindingOptions: {
                visible: 'gridPopupVisible'
            },
            onHidden: (e) => {
                resetVariables()
                $scope.isSuccedded = false
                $scope.uploadedPercent = 0
            }
        }

        $scope.importPopupOptions = {
            width: 400,
            height: 300,
            showTitle: true,
            title: $scope.options.popupName + ' (xls, xlsx, csv)',
            visible: true,
            bindingOptions: {
                visible: 'importPopupVisible'
            },
            onHiding: () => {
                $scope.importPopupVisible = false
            },
            onShowing: () => {
                //if ($scope.uploader) $scope.uploader.reset()
                $scope.fileUploaderOptions.uploaderVisible = true
            }
        }

        $scope.popoverOptions = {
            width: 400,
            height: 300,
            target: '#importButton',
            showTitle: true,
            title: $scope.options.popupName + ' (xls, xlsx, csv)',
            bindingOptions: {
                visible: 'popoverVisible'
            },
            onHiding: () => {
                $scope.popoverVisible = false
            },
            onShowing: () => {
                //if ($scope.uploader) $scope.uploader.reset()
                $scope.fileUploaderOptions.uploaderVisible = true
            },
            onInitialized: (e) => {
                $scope.popover = e.component
            }
        }

        $scope.$on(EventsNames.OPEN_IMPORT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            if (args[0]) {
                $scope.importPopupVisible = !$scope.importPopupVisible
            }
            else {
                $scope.popoverVisible = !$scope.popoverVisible
            }
            if (args[1])
                this.hideSettingsPopover = args[1]
        })

        $scope.$on(EventsNames.CLOSE_IMPORT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.popoverVisible = false
            $scope.importPopupVisible = false
        })

        $scope.summaryToastOptions = {
            type: 'info',
            contentTemplate: 'content',
            width: 500,
            position: 'bottom center',
            displayTime: 5000,
            visible: false,
            onInitialized: e => {
                $scope.summaryToast = e.component
            }
        }

        

        $scope.importProgressOptions = {
            visible: true,
            statusFormat: (ratio, value) => {
                return i18n('Popup', 'Progress') + ': ' + (ratio* 100) + '%'
            },
            bindingOptions: {
                value: 'uploadedPercent'
            },
            value: 0,
            min: 0,
            max: 100
        }

        $scope.isInitButtonDisabled = () => {
            return $scope.isValidated || $scope.isSuccedded;
        }

        $scope.isStartButtonDisabled = () => {
            return !$scope.isValidated || $scope.isStarted
        }

        

        var resetVariables = () => {
            $scope.isValidated = false
            $scope.isStarted = false
        }

        var initEntities = () => {
            entities = []
            var completedHeaders = false

            var dataModel = new ImportDataModel()
            dataModel.headers = []
            dataModel.data = []

            $scope.importGridOptions.dataSource.store()._array.forEach((entity, index) => {

                if (!entity.success) {
                    var dataTable = []

                    _.mapObject(entity, (value, key) => {
                        if (!isNaN(+key) && _.find($scope.columns, col => {
                            if (col.id == +key)
                                return true
                            return false
                        })) {
                            if (!completedHeaders) {
                                dataModel.headers.push(key)
                            }

                            dataTable.push(value)
                        }
                    })

                    completedHeaders = true
                    dataModel.data.push(dataTable)
                    entities.push(entity)
                }
            })

            api[$scope.options.requestMethodType]['initImport'](dataModel).then(res => {
                if (!res)
                    CommonHelper.openInfoPopup(i18n('Popup', 'PluginNotConfigurated'), 'error', 5000)
                $scope.guid = res.guid
                var infoText = ""
                res.statuses.forEach(info => {
                    if (info.statusId == ImportStatuses.NotValid || info.statusId == ImportStatuses.Failed) {
                        infoText += info.description
                    }
                })
                if (infoText.length > 0) {
                    CommonHelper.openInfoPopup(infoText, 'warning', 3000)
                }
                else if (!res.guid) {

                }
                else {
                    $scope.isValidated = true
                    CommonHelper.openInfoPopup(i18n('Popup', 'ImportDataValidated'))
                }

            }).catch(rej => {
                console.log(rej)
            })
        }

        var getProgress = () => {
            api[$scope.options.requestMethodType]['importProgress']($scope.guid).then((res: ImportProgressModel) => {
                $scope.lastStatus = res
                $scope.uploadedPercent = Math.round(res.proceedItemsCount / res.itemsCount * 100) || 0
                switch (res.statusId) {
                    case ImportStatuses.Failed:
                        clearTimeout(timeout)
                        getSummary()
                        break
                    case ImportStatuses.NotValid:
                        resetVariables()
                        break
                    case ImportStatuses.NotStarted:
                        break
                    case ImportStatuses.InProgress:
                        if ($scope.isStarted)
                            timeout = setTimeout(getProgress, 500)
                        break
                    case ImportStatuses.Success:
                        $scope.isSuccedded = true
                        resetVariables()
                        getSummary()
                        break
                }
            })
        }

        var getSummary = () => {
            $scope.summaryToast.show()

            var setSuccess = (index: number, val: boolean) => {
                entities[index].success = val
                entities[index].error = !val
            }

            api[$scope.options.requestMethodType]['importSummary']($scope.guid).then((result: ImportStatusModel) => {
                result.statuses.forEach((stat, index) => {
                    switch (stat.statusId) {
                        case ImportStatuses.NotValid:
                            entities[index].errorInfo = stat.description.split('\r\n')
                            setSuccess(index, false)
                            break
                        case ImportStatuses.Success:
                            setSuccess(index, true)
                            break
                    }
                })
                $scope.importGrid.refresh()
            })
        }

        var saveEntities = () => {
            $scope.isStarted = true
            api[$scope.options.requestMethodType]['startImport']($scope.guid).then(res => {
                getProgress()
            })
        }

        var stopImporting = () => {
            api[$scope.options.requestMethodType]['stopImport']($scope.guid).then(res => {
                resetVariables()
                getSummary()
            })
        }

        $scope.initButtonOptions = {
            text: i18n('Common', 'Validate'),
            bindingOptions: {
                disabled: 'isInitButtonDisabled()'
            },
            onClick: initEntities
        }

        $scope.startButtonOptions = {
            text: i18n('Popup', 'Start'),
            bindingOptions: {
                disabled: 'isStartButtonDisabled()'
            },
            onClick: saveEntities
        }

        $scope.stopButtonOptions = {
            text: i18n('Popup', 'Stop'),
            bindingOptions: {
                disabled: '!isStarted'
            },
            onClick: stopImporting
        }

        var createGrid = () => {
            localDictionary[$scope.options.dictionaryMethodName](0).then(columns => {
                $scope.columns = columns
                var dxColumns = []
                columns.forEach(column => {
                    var caption
                    var dataType = 'object'
                    if (column.type == CommonHelper.string) dataType = 'string'
                    else if ((column.type == CommonHelper.integer || column.type == CommonHelper.number) && !column.referenceTypeId) dataType = 'number'
                    else if (column.type == CommonHelper.boolean) dataType = 'boolean'
                    else if (column.type == CommonHelper.date) dataType = 'date'
                    else dataType = 'string'
                    var dataField = column.id.toString()
                    if (column.idName) dataField = column.idName.charAt(0).toLowerCase() + column.idName.slice(1)
                    if (column.unitName) {
                        caption = column.caption + "[" + column.unitName + "]"
                    }
                    else {
                        caption = column.caption
                    }

                    var dxColumn = <DevExpress.ui.dxDataGridColumn>{
                        dataField: dataField,
                        dataType: dataType,
                        caption: caption,
                        allowEditing: true,
                        visible: true,
                        falseText: i18n('Common', 'False'),
                        trueText: i18n('Common', 'True'),
                        filterOperations: undefined
                    }

                    if (column.referenceTypeId) {
                        localDictionary.getReferenceTable(column.referenceTypeId).then(table => {
                            if (!table) {
                                dxColumn.dataType = 'string'
                                return
                            }
                            dxColumn.cellTemplate = (container: JQuery, options) => {
                                var val = _.find(table, (entity) => {
                                    if (options.value) {
                                        if (entity.id == options.value)
                                            return true
                                        if (entity.caption == options.value) {
                                            return true
                                        }
                                    }
                                    return false
                                })

                                if (val)
                                    container.html(val.caption)
                            },
                            dxColumn.editCellTemplate = (container: JQuery, options) => {
                                var val = _.find(table, (entity) => {
                                    if (options.value) {
                                        if (entity.id == options.value)
                                            return true
                                        if (entity.caption == options.value)
                                            return true
                                    }
                                    return false
                                })

                                let selectBoxOptions = <DevExpress.ui.dxSelectBoxOptions>{
                                    dataSource: table,
                                    pagingEnabled: true,
                                    onSelectionChanged: (e) => {
                                        options.setValue(e.selectedItem.id)
                                    },
                                    valueExpr: 'id',
                                    displayExpr: 'caption',
                                    searchEnabled: true,
                                    value: val? val.id : "",
                                    displayValue: val ? val.caption : ""
                                }
                                var selectBox = document.createElement('div')
                                container.get(0).appendChild(selectBox)
                                $(selectBox).dxSelectBox(selectBoxOptions)
                            }
                            dxColumn.lookup = {
                                dataSource: table, 
                                valueExpr: 'id',
                                displayExpr: 'caption'
                            }
                        })
                    }

                    dxColumns.push(dxColumn)
                })

                var customImportGridOptions = {
                    name: 'ImportGrid',
                    columns: dxColumns,
                    onInitialized: e => {
                        $scope.importGrid = e.component
                    },
                    editing: {
                        mode: 'row',
                        allowUpdating: true,
                        allowDeleting: true,
                        texts: {
                            cancelRowChanges: i18n('Common', 'Cancel'),
                            confirmDeleteMessage: i18n('Popup', 'ConfirmDeleteRow'),
                            deleteRow: i18n('Common', 'Delete'),
                            editRow: i18n('Common', 'Edit'),
                            saveRowChanges: i18n('Common', 'Save')
                        }
                    },
                    masterDetail: {
                        enabled: true,
                        template: "errors",
                        autoExpandAll: true
                    },
                    onRowPrepared: e => {
                        if (e.rowType == "data") {
                            var row = $(e.rowElement)
                            if (e.data.success) {
                                
                                row.css('background-color', '#02E6A1')
                                row.children('td').css('background-color', 'inherit')
                            }
                            else if (e.data.error) {
                                row.css('background-color', '#F7484E')
                                row.children('td').css('background-color', 'inherit')
                            }
                        }
                    }
                }
                $scope.importGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, null, customImportGridOptions)
            })
        }

        var showPopup = () => {
            $scope.popoverVisible = false
            $scope.importPopupVisible = false
            $scope.gridPopupVisible = true
        }

        var updateGrid = (values) => {
            if (!$scope.importGridOptions)
                return
            $scope.importGridOptions.dataSource.store().clear()
            values.forEach(value => {
                $scope.importGridOptions.dataSource.store().insert(value)
            })
            $scope.importGrid.option($scope.importGridOptions)
            $scope.importGrid.refresh()
        }
    }]
}