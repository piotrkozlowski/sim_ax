﻿export class EventsNames {
    CLOSE_DETAILS_POPUP = 'CLOSE_DETAILS_POPUP'

    OPEN_LOCATION_DETAILS_POPUP = 'event:openDetailsLocationPopup'
    CLOSE_LOCATION_DETAILS_POPUP = 'event:closeDetailsLocationPopup'

    OPEN_GROUPS_EDIT_POPUP = 'event:openGroupsEditPopup'
    CLOSE_GROUPS_EDIT_POPUP = 'event:closeGroupsEditPopup'
    SAVE_GROUPS = 'event:saveGroups'
    OPEN_GRID_EDIT_POPUP = 'event:openGridEditPopup'
    OPEN_GRID_ATTACHMENTS_EDIT_POPUP = 'event:openGridAttachmentsEditPopup'
    CLOSE_GRID_EDIT_POPUP = 'event:closeGridEditPopup'
    CLOSE_GRID_ATTACHMENTS_EDIT_POPUP = 'event:closeGridAttachmentsEditPopup'

    OPEN_CREATE_EDIT_POPUP = 'event:openCreateEditPopup'
    CLOSE_CREATE_EDIT_POPUP = 'event:closeCreateEditPopup'
    OPEN_IMPORT_POPUP = 'event:openImportPopup'
    CLOSE_IMPORT_POPUP = 'event:closeImportPopup'
    OPEN_LOCATION_CREATE_EDIT_POPUP = 'event:openLocationCreateEditPopup'
    LOCATION_DELETED = 'event:locationDeleted'
    UPDATE_CHART = 'event:updateChartForLocationPreview'
    UPDATE_GRID_DETAILS = 'event:updateShortGridDetails'

    REFRESH_MAP_SIZE = 'event:refreshMapSize'
    CLOSE_MAP = 'event:closeSecondGridsterRow'
    GRIDSTER_ROW_CLICKED = 'event:gridsterRowClicked'

    //OPEN_COORDINATES_POPUP = 'event:openCoordinatesPopup'
    UPDATE_COORDINATES_BUTTON_POPUP = 'event:updateCoordinatesButtonPopup'

    // delete popup events
    OPEN_CONFIRMATION_POPUP = 'event:openConfirmationPopup'
    CLOSE_CONFIRMATION_POPUP = 'event:closeConfirmationPopup'
    CONFIRM_DELETION_LOCATION = 'event:confirmDeletionLocation'
    OPEN_COORDINATES_POPUP = 'event:openCoordinatesPopup'
    CLOSE_COORDINATES_POPUP = 'event:closeCoordinatesPopup'

    //DEVICES
    OPEN_DEVICES_DETAILS_POPUP = 'event:showDevicesDetailsPopup'
    CLOSE_DEVICES_DETAILS_POPUP = 'event:hideDevicesDetailsPopup'
    OPEN_DEVICES_CREATE_EDIT_POPUP = 'event:openDevicesCreateEditPopup'
    CLOSE_DEVICES_CREATE_EDIT_POPUP = 'event:closeDevicesCreateEditPopup'
    OPEN_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:openDevicesDeleteConfirmationPopup'
    CLOSE_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:closeDevicesDeleteConfirmationPopup'
    CONFIRM_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:confirmDevicesDeleteConfirmationPopup'
    DEVICE_DELETED = 'event:deviceDeleted'

    //DEVICECONNECTIONS
    OPEN_DEVICECONNECTIONS_DETAILS_POPUP = 'event:showDeviceConnectionsDetailsPopus'

    //TASKS
    OPEN_TASKS_DETAILS_POPUP = 'event:showTasksDetailsPopup'
    CLOSE_TASKS_DETAILS_POPUP = 'event:hideTasksDetailsPopup'
    OPEN_TASKS_CREATE_EDIT_POPUP = 'event:openTasksCreateEditPopup'
    CLOSE_TASKS_CREATE_EDIT_POPUP = 'event:closeTasksCreateEditPopup'
    OPEN_TASKS_DELETE_CONFIRMATION_POPUP = 'event:openTasksDeleteConfirmationPopup'
    CLOSE_TASKS_DELETE_CONFIRMATION_POPUP = 'event:closeTasksDeleteConfirmationPopup'
    CONFIRM_TASKS_DELETE_CONFIRMATION_POPUP = 'event:confirmTasksDeleteConfirmationPopup'
    DOWNLOAD_TASK_ATTACHMENT = 'event:downloadTaskAttachment'
    TASK_DELETED = 'event:taskDeleted'
    OPEN_TASKS_STATUS_CHANGE_POPUP = 'event:openTasksStatusChangePopup'
    CLOSE_TASKS_STATUS_CHANGE_POPUP = 'event:closeTasksStatusChangePopup'

    //ALARMS
    OPEN_ALARMS_DETAILS_POPUP = 'event:showAlarmsDetailsPopup'
    CLOSE_ALARMS_DETAILS_POPUP = 'event:hideAlarmsDetailsPopup'
    OPEN_ALARMS_CREATE_EDIT_POPUP = 'event:openAlarmsCreateEditPopup'
    CLOSE_ALARMS_CREATE_EDIT_POPUP = 'event:closeAlarmsCreateEditPopup'
    OPEN_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:openAlarmsDeleteConfirmationPopup'
    CLOSE_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:closeAlarmsDeleteConfirmationPopup'
    CONFIRM_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:confirmAlarmsDeleteConfirmationPopup'
    ALARM_DELETED = 'event:alarmDeleted'
    ALARM_APPROVED = 'event:alarmApproved'

    //ISSUE
    OPEN_ISSUES_DETAILS_POPUP = 'event:showIssuesDetailsPopup'
    CLOSE_ISSUES_DETAILS_POPUP = 'event:hideIssuesDetailsPopup'
    OPEN_ISSUES_CREATE_EDIT_POPUP = 'event:openIssuesCreateEditPopup'
    CLOSE_ISSUES_CREATE_EDIT_POPUP = 'event:closeIssuesCreateEditPopup'
    OPEN_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:openIssuesDeleteConfirmationPopup'
    CLOSE_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:closeIssuesDeleteConfirmationPopup'
    CONFIRM_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:confirmIssuesDeleteConfirmationPopup'
    DOWNLOAD_ISSUE_ATTACHMENT = 'event:downloadIssueAttachment'
    ISSUE_DELETED = 'event:issueDeleted'

    //METER
    OPEN_METER_CREATE_EDIT_POPUP = 'event:openMeterCreateEditPopup'
    OPEN_METER_DETAILS_POPUP = 'event:openMeterDetailsPopup'
    CLOSE_METER_DETAILS_POPUP = 'event:closeMeterDetailsPopup'
    METER_DELETED = 'event:meterDeleted'

    //USER
    OPEN_USER_CREATE_EDIT_POPUP = 'event:openUserCreateEdit'
    OPEN_USER_DETAILS_POPUP = 'event:openUserDetailsPopup'
    CLOSE_USER_DETAILS_POPUP = 'event:closeUserDetailsPopup'
    USER_DELETED = 'event:userDeleted'

    OPEN_PASSWORD_CHANGE_POPUP = 'event:openPasswordChangePopup'

    // distributors
    OPEN_DISTRIBUTORS_DETAILS_POPUP = 'event:showDistributorsDetailsPopup'
    CLOSE_DISTRIBUTORS_DETAILS_POPUP = 'event:hideDistributorsDetailsPopup'
    OPEN_DISTRIBUTORS_CREATE_EDIT_POPUP = 'event:openDistributorsCreateEditPopup'
    CLOSE_DISTRIBUTORS_CREATE_EDIT_POPUP = 'event:closeDistributorsCreateEditPopup'
    OPEN_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:openDistributorsDeleteConfirmationPopup'
    CLOSE_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:closeDistributorsDeleteConfirmationPopup'
    CONFIRM_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:confirmDistributorsDeleteConfirmationPopup'
    CHANGED_ALLOWED_DISTRIBUTORS = 'event:changedAllowedDistributors'
    DISTRIBUTOR_DELETED = 'event:distributorDeleted'

    OPENED_POPUP = 'event:incrementPopupCount'
    CLOSED_POPUP = 'event:decrementPopupCount'

    //READING ROUTES    
    OPEN_ROUTES_DETAILS_POPUP = 'event:showRoutesDetailsPopup'
    CLOSE_ROUTES_DETAILS_POPUP = 'event:hideRoutesDetailsPopup'
    OPEN_ROUTES_CREATE_EDIT_POPUP = 'event:openRoutesCreateEditPopup'
    CLOSE_ROUTES_CREATE_EDIT_POPUP = 'event:closeRoutesCreateEditPopup'
    OPEN_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:openRoutesDeleteConfirmationPopup'
    CLOSE_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:closeRoutesDeleteConfirmationPopup'
    CONFIRM_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:confirmRoutesDeleteConfirmationPopup'
    ROUTE_DELETED = 'event:routeDeleted'
    ROUTE_POINT_DRAG_AND_DROP = 'event:routePointDragAndDrop'

    //SIM CARDS    
    OPEN_SIM_CARDS_DETAILS_POPUP = 'event:showSimcardDetailsPopup'
    CLOSE_SIM_CARDS_DETAILS_POPUP = 'event:hideSimcardDetailsPopup'
    OPEN_SIM_CARDS_CREATE_EDIT_POPUP = 'event:openSimcardCreateEditPopup'
    CLOSE_SIM_CARDS_CREATE_EDIT_POPUP = 'event:closeSimcardCreateEditPopup'
    OPEN_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:openSimcardDeleteConfirmationPopup'
    CLOSE_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:closeSimcardDeleteConfirmationPopup'
    CONFIRM_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:confirmSimcardDeleteConfirmationPopup'
    SIM_CARDS_DELETED = 'event:simcardDeleted'

    //contacts    
    OPEN_CONTACT_DETAILS_POPUP = 'event:showContactDetailsPopup'
    CLOSE_CONTACT_DETAILS_POPUP = 'event:hideContactDetailsPopup'
    OPEN_CONTACT_CREATE_EDIT_POPUP = 'event:openContactCreateEditPopup'
    CLOSE_CONTACT_CREATE_EDIT_POPUP = 'event:closeContactCreateEditPopup'
    OPEN_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:openContactDeleteConfirmationPopup'
    CLOSE_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:closeContactDeleteConfirmationPopup'
    CONFIRM_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:confirmContactDeleteConfirmationPopup'
    CONTACT_DELETED = 'event:contactDeleted'

    // actions
    OPEN_ACTION_CREATE_EDIT_POPUP = 'event:openActionCreateEditPopup'
    OPEN_ACTION_DETAILS_POPUP = 'event:openActionDetailsPopup'
    CLOSE_ACTION_DETAILS_POPUP = 'event:closeActionDetailsPopup'
    ACTION_DELETED = 'event:actionDeleted'

    STOP_MEASUREMENTS_LOADING = 'event:stopMeasurementsLoading'
}