﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonExtendedDataGridOptions } from '../common.extended-data-grid-options'
import { CommonScope } from '../common.extended-data-grid-options'
import { Api } from  '../../api/api.module'

require('./alarms-menu.scss')

interface AlarmsMenuScope extends CommonScope {
    options: any
    menuPopoverOptions: DevExpress.ui.dxPopoverOptions
    refreshButtonOptions: DevExpress.ui.dxButtonOptions
    alarmsMenuGridOptions: DevExpress.ui.dxDataGridOptions
    showGrid: boolean
}

export class AlarmsMenuDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        options: '='
    }

    controller = ['$scope', 'api', ($scope: AlarmsMenuScope, api: Api) => {
        var gridDeferred = $.Deferred<DevExpress.ui.dxDataGrid>()

        var loadGrid = (values) => {
            gridDeferred.then(grid => {
                grid.option("dataSource").store().clear()
                var store = grid.option("dataSource").store()
                values.forEach(value => {
                    var text = value.text + ' (' + value.refdata.length + ')'
                    store.insert({
                        name: text,
                        refdata: value.refdata,
                    })
                })

                grid.refresh()
                $scope.showGrid = true
            })
        }

        $scope.menuPopoverOptions = {
            width: 400,
            height: 500,
            target: '.alarm-button',
            bindingOptions: {
                visible: 'options.popoverVisible'
            },
            onHiding: () => {
                $scope.options.popoverVisible = false
            },
            onShown: () => {
                loadGrid($scope.$root['alarmTiles'])
            }
        }

        var customOptions = {
            columns: [
                {
                    dataField: 'name',
                    caption: i18n('Common', 'Name'),
                    allowEditing: false,
                    dataType: 'string'
                }
            ],
            onInitialized: e => {
                gridDeferred.resolve(e.component)
            },
            onCellPrepared: e => {
                if (e.column.command === "expand") {
                    if (e.rowType === "data" && (!e.data.refdata || e.data.refdata.length == 0)) {
                        e.component.collapseRow(e.key)
                    }
                    e.cellElement.removeClass("dx-datagrid-expand")
                    e.cellElement.empty()
                } else if (e.rowType == "detail") {
                    e.cellElement.addClass("alarm-grid-row-cell")
                }
            },
            masterDetail: {
                enabled: true,
                template: "detail",
                autoExpandAll: true
            }
        }
        $scope.alarmsMenuGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, null, null, customOptions)

        $scope.refreshButtonOptions = {
            onClick: () => {
                $scope.showGrid = false
                var user = api.login.getUser()
                api.alarm.getAlarmsSummary(user.id).then(result => {
                    $scope.$root['alarmsCount'] = result.result.length
                    $scope.$root['alarmTiles'] = result.result
                    loadGrid(result)
                })
            },
            template: CommonHelper.getIcon('REFRESH'),
            hint: i18n("Grid", "Refresh")
        }

    }]
    template = require("./alarms-menu.tpl.html")
    replace = true
}