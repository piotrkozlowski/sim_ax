﻿import { ColumnModel, UserModel, PasswordResetEmailModel, FieldNameValueModel } from '../../api/api.models'
import { CommonHelper } from './../../../helpers/common-helper'
import { PopupType } from './../../../helpers/popup-manager'
import { EventsNames } from '../../../helpers/events-names'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { i18n } from '../i18n/common.i18n'
import { CommonExtendedDataGridOptions } from '../common.extended-data-grid-options'
import { PopupManager } from '../../../helpers/popup-manager'
import { CommonScope } from '../common.extended-data-grid-options'
import * as moment from 'moment'

export class CreateEditPopupOptions {
    title: string = i18n('Common', 'Edit')
    saveButtonText: string = i18n('Common', 'Apply')
    type: PopupType = PopupType.LOCATION_POPUP
    onSubmit: (fields: FieldNameValueModel[], id?: number) => ng.IPromise<any>
}

interface CreateEditScope extends CommonScope {
    // popup
    popupOptions: DevExpress.ui.dxPopupOptions
    resetPasswordButtonOptions: DevExpress.ui.dxButtonOptions
    changePasswordButtonOptions: DevExpress.ui.dxButtonOptions
    popupVisible: boolean
    isEditMode: boolean
    isUserDetails: boolean
    isLoggedUserDetails: boolean
    userPassword: string
    loggedUser: UserModel
    modelId: number
    tabs: DevExpress.ui.dxTabPanel
    tabsOptions: DevExpress.ui.dxTabsOptions
    tabsVisible: boolean;
    tabItems: any[]
    panels: any
    dateFormat: string
    selectedTab: number

    passwordResetPopupId: number
    distributorChangePopupId: number

    toolbarOptions: DevExpress.ui.dxToolbarOptions

    sendEmail: () => void
    options: CreateEditPopupOptions

    isLoggedUser: boolean

    openConfirmationPopup()
    confirm()
    confirmNo()
    yes: string
    no: string

    // location type
    getCoords(): any
    onLocationSelected(lat: number, lng: number): void

    // types / lists
    typeListsNames: string[]
}

export class CreateEditPopupController {
    referenceTableCount: number = 0
    referenceTypeFieldName: string = 'referenceTypeField'
    specificParametersGrid: DevExpress.ui.dxDataGrid
    result: any

    tabs: DevExpress.ui.dxTabs
    entityId: number = null


    // select boxes and events for distributor
    distributorFormInstance: DevExpress.ui.dxSelectBox
    distributorGridFormInstance: DevExpress.ui.dxSelectBox
    distributorChangedEvent: any = null
    distributorGridChangedEvent: any = null
    hideDistributorChangePopup: boolean = false

    constructor(private $scope: CreateEditScope, private $q: ng.IQService, private $timeout: ng.ITimeoutService, api: Api, private localDictionary: LocalDictionaryFactory) {

        $scope.passwordResetPopupId = $scope.$id
        $scope.distributorChangePopupId = new Date().getTime()
        $scope.loggedUser = api.login.getUser()
        //popup
        $scope.dateFormat = i18n("Common", "DateFormatDX")
        $scope.popupVisible = false

        $scope.popupOptions = {
            bindingOptions: {
                visible: 'popupVisible',
                title: 'options.title'
            },
            maxWidth: 800,
            showTitle: true,
            closeOnBackButton: true,
            closeOnOutsideClick: true,
            onHiding: () => { }
        }
        $scope.openConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.distributorChangePopupId, i18n('Popup', 'ConfirmDystrybutorChange'))
        }

        $scope.confirm = () => {
            this.distributorChangedEvent = null
            this.distributorGridChangedEvent = null
        }
        $scope.confirmNo = () => {
            if (this.distributorChangedEvent) {
                this.distributorFormInstance.option('value', this.distributorChangedEvent.previousValue)
                this.distributorChangedEvent = null
            } else if (this.distributorGridChangedEvent) {
                this.distributorGridFormInstance.option('value', this.distributorGridChangedEvent.previousValue)
                this.distributorGridChangedEvent = null
            }
        }

        //tabs
        $scope.tabs = <any>{}
        $scope.selectedTab = -1
        $scope.tabItems = []
        $scope.tabsOptions = {
            bindingOptions: {
                visible: 'tabsVisible',
                items: 'tabItems',
                selectedIndex: 'selectedTab'
            },
            onInitialized: e => {
                this.tabs = e.component
                this.updateTabs($scope, $scope.panels)
            }
        }

        $scope.$watch('tabItems', (value: any[]) => {
            $scope.tabsVisible = value.length > 1
        })

        $scope.$watch('selectedTab', value => {
            if ($scope.selectedTab < 0) {
                $timeout(() => { $scope.selectedTab = 0 })
            }

            if ($scope.panels) {
                $scope.panels.forEach(panel => {
                    if (panel.grid) {
                        $timeout(() => {
                            panel.grid.repaint()
                        })
                    }
                })
            }
        })

        var getLocale = () => {
            var locale = CommonHelper.getValueFromLocalStorage('LOCALE')
            if (!locale) locale = 'pl-PL'
            return locale
        }

        $scope.sendEmail = () => {
            var model = new PasswordResetEmailModel()
            api.users.getUser($scope.modelId).then(result => {
                result.fields.forEach(field => {
                    if (field.id == CommonHelper.userColumnLogin)
                        model.user = field.value
                })
            }).then(result => {
                model.locale = getLocale()
                api.password.reset(model).then(result => {
                    if (result == 'OK') {
                        CommonHelper.openInfoPopup(i18n('Popup', 'ResetEmailSent'))
                    } else {
                        CommonHelper.openInfoPopup(i18n('Popup', 'ResetError'), 'error')
                    }

                }).catch(() => {
                    CommonHelper.openInfoPopup(i18n('Popup', 'ResetError'), 'error')
                })
            })
        }

        var openChangePassword = () => {
            $scope.$root.$broadcast(EventsNames.OPEN_PASSWORD_CHANGE_POPUP, $scope.loggedUser.id, [{
                id: $scope.loggedUser.id,
                name: $scope.loggedUser.username
            }])
        }

        var openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.passwordResetPopupId, i18n('Popup', 'ConfirmResetPassword'))
        }

        $scope.resetPasswordButtonOptions = {
            text: i18n("Common", "ResetPassword"),
            onClick: () => {
                openDeleteConfirmationPopup()
            }
        }

        $scope.changePasswordButtonOptions = {
            text: i18n("Common", "ChangePassword"),
            onClick: () => {
                openChangePassword()
            }
        }

        $scope.toolbarOptions = {
            items: [
                {
                    location: 'after',
                    widget: 'dxButton',
                    options: {
                        text: $scope.options.saveButtonText,
                        onClick: () => {
                            var promises = []
                            $scope.panels.forEach(section => {
                                if (section.grid) {
                                    var deferred = $q.defer()
                                    section.grid.saveEditData()
                                    section.grid.refresh().then(() => {
                                        deferred.resolve()
                                    })
                                    promises.push(deferred.promise)
                                }
                            })
                            if (promises.length) {
                                $q.all(promises).then(() => {
                                    $timeout(() => {
                                        this.submit()
                                    }, 0)
                                })
                            } else {
                                this.submit()
                            }
                        }
                    }
                }
            ]
        }

        // location select callbacks
        $scope.getCoords = () => {
            return this.getCoords()
        }

        $scope.onLocationSelected = (lat: number, lng: number) => {
            this.onLocationSelected(lat, lng)
        }

        // open
        $scope.$on(EventsNames.OPEN_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.popupVisible = true
            $scope.isEditMode = true
            var sections = args[0]            
            var initial_data = args[1]
            $scope.modelId = args[2]
            var selectedTab = args[3]
            $scope.isLoggedUser = $scope.modelId == $scope.loggedUser.id
            $timeout(() => {
                if (selectedTab) {
                    $scope.selectedTab = selectedTab
                }
            }, 500)

            this.entityId = $scope.modelId

            if (!initial_data) {
                initial_data = []
                $scope.isEditMode = false
            }
            this.updateTabs($scope, sections)
            $scope.panels = []
            var mappedData = {}
            initial_data.forEach(data => {
                mappedData[data.id] = data
            })

            sections.forEach(section => {
                switch (section.type) {
                    default:
                    case "form":
                        this.createForm(section, mappedData)
                        break;
                    case "grid":
                        this.createGrid(section, mappedData)
                        break;
                }
                $scope.panels.push(section)
            })
            $timeout(() => {
                $scope.$root.$broadcast(EventsNames.UPDATE_COORDINATES_BUTTON_POPUP, $scope.getCoords())
            }, 0)
        })

        // close
        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.popupVisible = false
            this.distributorChangedEvent = null
        })
    }

    updateTabs($scope: CreateEditScope, sections: any[]) {
        $scope.tabItems = sections.map(section => {
            return {
                template: (itemData, itemIndex, itemElement) => {
                    this.$scope['showSectionError' + itemIndex] = false
                    return section.name + '<ng-md-icon ng-show="showSectionError' + itemIndex + '" icon="' + window['config']['ICONS']['REQUIRED_TAB'] + '" style="fill: #ea4444; vertical-align: middle;" size="20"></ng-md-icon>'
                }
            }
        })
    }

    createForm(section, data) {        
        section.formData = {}
        section.items = []
        section.onFieldChanged = (e) => {
            if (e.dataField) {
                if (!isNaN(parseFloat(e.value)))
                    section.formData[e.dataField] = parseFloat(e.value)
                else
                    section.formData[e.dataField] = e.value

                this.trackChanges(section, e.dataField, e.value)
            }
            else {
                var value = (e.selectedItem) ? e.selectedItem.id : ''
                section.formData[e.model.dxTemplateModel.dataField] = value
                this.trackChanges(section, e.model.dxTemplateModel.dataField, value)
            }
        }

        section.onDateFieldChanged = (e) => {
            this.trackChanges(section, e.model.dxTemplateModel.dataField, e.value)
        }

        section.onFieldValueChanged = (e) => {
            if (this.$scope.options.type == PopupType.DEVICE_POPUP && !this.hideDistributorChangePopup) {
                if (e.model.dxTemplateModel.dataField == CommonHelper.distributorColumnId) {
                    if (e.previousValue != '') {
                        if (!this.distributorChangedEvent) {
                            this.distributorChangedEvent = e
                            this.$scope.openConfirmationPopup()
                        }
                    }
                }
            }
        }
        section.initializedHandler = (e) => {
            if (e.model.dxTemplateModel.dataField == CommonHelper.distributorColumnId) {
                this.distributorFormInstance = e.component
            }
        }
        section.options = <DevExpress.ui.dxFormOptions>{
            onFieldDataChanged: (e: any) => {
                this.trackChanges(section, e.dataField, e.value)
            },
            requiredMark: '*',
            showRequiredMark: true,
            labelLocation: 'top',
            colCount: 3,
            width: '100%',
            height: '100%',
            scrollingEnabled: true,
            bindingOptions: {
                formData: 'panel.formData',
                items: 'panel.items'
            },
            onInitialized: e => {
                section.form = e.component               
                this.loadForm(section, data)
            }
        }
        if (section.form) {            
            this.loadForm(section, data)
        }
    }

    isIdColumn(column) {
        switch (this.$scope.options.type) {
            case PopupType.LOCATION_POPUP:
                if (column.id == CommonHelper.locationColumnId)
                    return true
                break
            case PopupType.DEVICE_POPUP:
                break
            case PopupType.METER_POPUP:
                if (column.id == CommonHelper.meterColumnId)
                    return true
                break
            case PopupType.USER_POPUP:
                if ([CommonHelper.userColumnId, CommonHelper.actorColumnId].indexOf(column.id) > -1)
                    return true
                break
            case PopupType.ISSUE_POPUP:
                if (column.id == CommonHelper.issuesColumnId)
                    return true
                break
            case PopupType.TASK_POPUP:
                if (column.id == CommonHelper.tasksColumnId)
                    return true
                break
            case PopupType.DISTRIBUTOR_POPUP:
                if (column.id == CommonHelper.distributorColumnId)
                    return true
                break
            case PopupType.DISTRIBUTOR_POPUP:
                if (column.id == CommonHelper.alarmsColumnId)
                    return true
                break
            case PopupType.ROUTE_POPUP:
                if (column.id == CommonHelper.routeId)
                    return true
                break
        }
        return false
    }

    loadForm(section, data) {
        section.form.beginUpdate()
        section.items = []
        section.formData = {}
        var locationSelectedButtonAdded = false
        var items = section.items        

        for (var i = 0; i < section.columns.length; ++i) {
            var column = section.columns[i]
            if (this.isIdColumn(column)) {
                if (!data[column.id]) data[column.id] = new ColumnModel()
                section.formData[column.id] = data[column.id].value
                continue
            }

            var visible = true
            if (column.id == CommonHelper.locationLatitude || column.id == CommonHelper.locationLongitude) {
                visible = false
                if (!locationSelectedButtonAdded) {
                    items.push({
                        visible: true,
                        label: {
                            text: i18n('Common', 'Location')
                        },
                        colSpan: 2,
                        template: '<location-select coords="::getCoords()" callback="::onLocationSelected"></location-select>'
                    })
                    locationSelectedButtonAdded = true
                }
            }
            if (column.id != CommonHelper.userColumnPassword) {                
                items.push({
                    dataField: column.id,
                    visible: visible,                    
                    label: {
                        text: column.caption.split(" ").map(word => word.substring(0, 24)).join(" ")
                    },
                    validationRules: [{
                        type: 'pattern',
                        pattern: ''
                    }]
                })                
            }
            if (column.id == CommonHelper.userColumnPassword && !this.$scope.isLoggedUser) {
                var dataType = data[column.id]
                if (dataType) {
                    this.$scope.userPassword = dataType.value
                    items.push({
                        visible: true,
                        label: {
                            text: column.caption.split(" ").map(word => word.substring(0, 24)).join(" ")
                        },
                        colSpan: 1,
                        template: '<div class="location-select"><div class="bordered"><span>{{userPassword}}</span><div class="select-button" dx-button="resetPasswordButtonOptions"></div></div></div>'
                    })
                }
            }
            if (column.id == CommonHelper.userColumnPassword && this.$scope.isLoggedUser) {
                var dataType = data[column.id]
                if (dataType) {
                    this.$scope.userPassword = dataType.value
                    items.push({
                        visible: true,
                        label: {
                            text: column.caption.split(" ").map(word => word.substring(0, 24)).join(" ")
                        },
                        colSpan: 1,
                        template: '<div class="location-select"><div class="bordered"><span>{{userPassword}}</span><div class="select-button" dx-button="changePasswordButtonOptions"></div></div></div>'
                    })
                }
            }

            if (section.required) {
                for (var j = 0; j < section.required.length; j++) {
                    var req = section.required[j]
                    if (req.id == column.id && column.type != CommonHelper.boolean) {
                        items[items.length - 1].validationRules.push({
                            type: 'required',
                            message: i18n('Common', 'RequiredField')
                        })
                        break
                    }
                }
            }
            this.addValidationRules(section, column, items[items.length - 1])
            // set values
            section.formData[column.id] = this.getFieldValue(data, column)            
        }        
        section.form.endUpdate()
    }

    onEditorPreparing(e, section, data) {
        var onDateChange
        if (e.value && e.row.data.value instanceof Date) {
            e.cancel = true
            e.editorElement.dxDateBox(<DevExpress.ui.dxDateBoxOptions>{
                value: e.row.data.value,
                width: "100%",
                type: "datetime",
                displayFormat: {
                    formatter: (value) => {
                        return moment(value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    },
                    parser: (value) => {
                        return moment(value, i18n("Common", "DateTimeFormatMomentJS")).toDate()
                    }
                },
                onValueChanged: onDateChange = de => {
                    if (de.value)
                        e.setValue(de.value)
                },
                onEnterKey: onDateChange,
                applyButtonText: i18n('Common', 'Done'),
                cancelButtonText: i18n('Common', 'Cancel'),
                onFocusOut: onDateChange
            })
        }
        if (e.row) {
            if (typeof (e.row.data.value) == 'boolean') {
                e.cancel = true
                e.editorElement.dxSelectBox(<DevExpress.ui.dxSelectBoxOptions>{
                    value: e.row.data.value,
                    width: '100%',
                    dataSource: [{ id: false, value: false }, { id: true, value: true }],
                    valueExpr: 'id',
                    displayExpr: 'value',
                    onValueChanged: (select) => {
                        e.setValue(select.value)
                    }
                })
            }
            if (e.row.data.index == CommonHelper.distributorColumnId) {
                var referenceType
                section.columns.forEach(column => {
                    if (column.id == CommonHelper.distributorColumnId) referenceType = column.referenceTypeId
                })
                e.cancel = true
                this.localDictionary.getReferenceTable(referenceType).then(table => {
                    if (table) {
                        e.editorElement.dxSelectBox(<DevExpress.ui.dxSelectBoxOptions>{
                            value: e.row.data.value,
                            displayValue: this.getValue(e.row.data.value, table),
                            width: '100%',
                            dataSource: table,
                            valueExpr: 'id',
                            displayExpr: 'caption',
                            noDataText: i18n('Common', 'NoData'),
                            onValueChanged: (select) => {
                                if (this.$scope.options.type == PopupType.DEVICE_POPUP) {
                                    if (e.previousValue != '') {
                                        if (!this.distributorGridChangedEvent) {
                                            this.distributorGridChangedEvent = e
                                            this.$scope.openConfirmationPopup()
                                        }
                                    }
                                }
                                if(select.value) e.setValue(select.value)
                            },
                            onInitialized: e => {
                                this.distributorGridFormInstance = e.component
                            }
                        })
                    }
                })
            }
        }
    }

    onCellPrepared(options, section, data) {        
        if (options.data) {
            if (options.data.index == CommonHelper.distributorColumnId && options.column.caption == "Value") {
                var referenceType
                section.columns.forEach(column => {
                    if (column.id == CommonHelper.distributorColumnId) referenceType = column.referenceTypeId
                })
                this.localDictionary.getReferenceTable(referenceType).then(table => {
                    if (table) {
                        table.forEach(data => {
                            if (data.id == options.data.value) {
                                if (options.cellElement.html().indexOf('<div class="dx-highlight-outline dx-pointer-events-target">') < 0)
                                    options.cellElement.html(data.caption)
                            }
                        })
                    }
                })
            }
        }
    }

    createCustomOptions(section, data) {
        return {
            columns: [
                {
                    dataField: "description",
                    caption: i18n("Common", "Description"),
                    allowEditing: false
                },
                {
                    dataField: "index",
                    caption: i18n("Common", "Index"),
                    allowEditing: false
                },
                {
                    dataField: "value",
                    caption: i18n("Common", "Value"),
                    allowEditing: true,
                    calculateCellValue: function (rowData: any) {
                        if (rowData.value instanceof Date) {
                            return CommonHelper.cellFormatForDifferentValues({
                                value: rowData.value
                            })
                        }
                        return this.defaultCalculateCellValue(rowData)
                    }
                }
            ],
            onInitialized: e => {
                section.grid = e.component
                this.loadGrid(section, data)
            },
            onRowUpdated: (e) => {
                this.trackChanges(section, e.key.index, e.key.value)
            },
            onEditorPreparing: (e) => this.onEditorPreparing(e, section, data),
            onCellPrepared: (options) => this.onCellPrepared(options, section, data)
        }
    }

    createGrid(section, data) {
        var customOptions = this.createCustomOptions(section, data)
        section.options = new CommonExtendedDataGridOptions(CommonHelper.createEditGrid, this.$scope, null, null, customOptions)
        if (section.grid) {
            this.loadGrid(section, data)
        }
    }

    loadGrid(section, data) {
        section.grid.option("dataSource").store().clear()
        var filterIds = section.columns.map(field => field.id)
        section.columns.forEach(column => {
            section.grid.option("dataSource").store().insert({
                description: column.caption,
                index: column.id,
                value: this.getFieldValue(data, column)
            })
        })

        section.grid.refresh()
    }

    getFieldValue(data, column) {
        var field = data[column.id]
        if (field) {
            if (field.typeId == CommonHelper.date) {
                if (field.value != null) {
                    return new Date(field.value)
                }
                else {
                    return new Date()
                }
            } else if (field.typeId == CommonHelper.boolean) {
                return !!field.value
            } else {
                if (column.id == CommonHelper.priorityColumnId) {
                    if (field.value == null) {
                        return 1
                    }
                }
                return field.value
            }
        } else {
            if (column.type == CommonHelper.date) {
                return new Date()
            }
            else if (column.type == CommonHelper.boolean) {
                return false
            }
            if (column.id == CommonHelper.priorityColumnId) {
                return 1
            }
            return ""
        }
    }

    trackChanges(section, index, value) {
        this.$scope.panels.forEach(panel => {
            if (panel != section) {
                if (panel.form) {
                    if (index == CommonHelper.distributorColumnId && this.distributorFormInstance) {
                        this.hideDistributorChangePopup = true
                        this.distributorFormInstance.option('value', value)
                        this.hideDistributorChangePopup = false
                    }
                    panel.formData[index] = value
                }
                if (panel.grid) {
                    panel.grid.option("dataSource").items().every(item => {
                        if (item.index == index) {
                            item.value = value
                        }
                        return true
                    })
                }
            }
        })
    }

    submit() {
        var sections: any[] = this.$scope.panels

        var isValid = true
        sections.forEach((section, index) => {
            this.$scope['showSectionError' + index] = false
            if (section.form && !section.form.validate().isValid) {
                section.showRequiredError = true
                this.$scope['showSectionError' + index] = true
                isValid = false
            }
            return true
        })

        if (isValid) {
            var unique = {}
            var fields: FieldNameValueModel[] = []
            sections.forEach(section => {
                if (section.form) {
                    Object.keys(section.formData).forEach(property => {
                        var id = parseInt(property)
                        if (!unique[id]) {
                            var nameValue = new FieldNameValueModel()
                            nameValue.nameId = id
                            nameValue.value = section.formData[property]
                            fields.push(nameValue)
                            unique[id] = true
                        }
                    })
                }
                if (section.grid) {
                    section.grid.option("dataSource").items().forEach(item => {
                        var id = parseInt(item.index)
                        if (!unique[id]) {
                            var nameValue = new FieldNameValueModel()
                            nameValue.nameId = id
                            nameValue.value = item.value
                            fields.push(nameValue)
                            unique[id] = true
                        }
                    })
                }
            })

            this.$scope.$root.$broadcast('showLoadingIndicator')
            this.$scope.options.onSubmit(fields, this.entityId).finally(() => {
                this.$scope.$root.$broadcast('hideLoadingIndicator')
            })
        }
        else {
            CommonHelper.openInfoPopup(i18n('Common', 'IncorrectData'), 'error', 5000)
            angular.element('.dx-invalid').css('height', '36px')
        }

    }

    addValidationRules(section, column: ColumnModel, item = null) {
        item.editorOptions = {}

        if (!column.isEditable && this.$scope.isEditMode)
            item.editorOptions.readOnly = true

        switch (column.type) {
            case CommonHelper.integer:
                var referenceType = column.referenceTypeId
                if (!referenceType) {
                    if (item) {
                        item.validationRules.push({
                            type: 'pattern',
                            pattern: '^[0-9]{0,}$',
                            message: i18n('Common', 'IntegerField')
                        })
                        if (column.format.numberMaxValue) {
                            item.validationRules.push({
                                type: 'range',
                                max: column.format.numberMaxValue,
                                message: i18n('Common', 'NumberMaxValue') + ' ' + column.format.numberMaxValue
                            })
                        }
                        if (column.format.numberMinValue) {
                            item.validationRules.push({
                                type: 'range',
                                min: column.format.numberMinValue,
                                message: i18n('Common', 'NumberMinValue') + ' ' + column.format.numberMinValue
                            })
                        }
                    }
                    break
                } else {
                    this.localDictionary.getReferenceTable(referenceType).then(table => {
                        if (table) {
                            this.referenceTableCount += 1
                            this.$scope['referenceTable' + this.referenceTableCount] = table
                            var selectText = i18n("Common", "Select")
                            var noDataText = i18n('Common', 'NoData')
                            item.template = '<div id="table' + column.id + '" dx-select-box="{dataSource: referenceTable' + this.referenceTableCount + ', pagingEnabled:true, placeholder:' + "'" + selectText + "'" + ', onSelectionChanged: panel.onFieldChanged, onValueChanged: panel.onFieldValueChanged, onInitialized: panel.initializedHandler, value: panel.formData[' + column.id + '], valueExpr: \'id\', searchEnabled: true, displayExpr: \'caption\', noDataText: ' + "'" + noDataText + "'" + ' }"></div>'
                        }
                    })
                }
                break
            case CommonHelper.string:
                section.formData[column.id] = ""
                if (column.id == CommonHelper.userColumnPassword) {
                    item.editorOptions.mode = 'password'
                }
                if (item) {
                    if (column.format.textMinLength) {
                        item.validationRules.push({
                            type: 'stringLength',
                            min: column.format.textMinLength,
                            message: i18n('Common', 'MinNumberOfCharacters') + ' ' + column.format.textMinLength
                        })
                    }
                    if (column.format.textMaxLength) {
                        item.validationRules.push({
                            type: 'stringLength',
                            max: column.format.textMaxLength,
                            message: i18n('Common', 'MaxNumberOfCharacters') + ' ' + column.format.textMaxLength
                        })
                    }
                }
                break
            case CommonHelper.date:
                section.formData[column.id] = new Date()
                item.template = '<div dx-date-box="{ displayFormat: dateFormat, bindingOptions: { value: \'panel.formData[' + column.id + ']\' }, onValueChanged: panel.onDateFieldChanged }"></div>'
                break
            case CommonHelper.number:
                if (item) {
                    item.validationRules.push({
                        type: 'numeric',
                        message: i18n('Common', 'NumericField')
                    })
                    if (column.format.numberMaxValue) {
                        item.validationRules.push({
                            type: 'range',
                            max: column.format.numberMaxValue,
                            message: i18n('Common', 'NumberMaxValue') + ' ' + column.format.numberMaxValue
                        })
                    }
                    if (column.format.numberMinValue) {
                        item.validationRules.push({
                            type: 'range',
                            min: column.format.numberMinValue,
                            message: i18n('Common', 'NumberMinValue') + ' ' + column.format.numberMinValue
                        })
                    }
                    if (column.format.numberMinPrecision && column.format.numberMaxPrecision) {
                        item.validationRules.push({
                            type: 'pattern',
                            pattern: '^\\d+\\.\\d{' + column.format.numberMinPrecision + ',' + column.format.numberMaxPrecision + '}$',
                            message: i18n('Common', 'NumberPrecisionRange') + ' ' + column.format.numberMinPrecision + '-' + column.format.numberMaxPrecision
                        })
                    }
                    else if (column.format.numberMaxPrecision) {
                        item.validationRules.push({
                            type: 'pattern',
                            pattern: '^\\d+(\\.\\d{1,' + column.format.numberMaxPrecision + '})?$',
                            message: i18n('Common', 'NumberMaxPrecision') + ' ' + column.format.numberMaxPrecision
                        })
                    }
                    else if (column.format.numberMinPrecision) {
                        item.validationRules.push({
                            type: 'pattern',
                            pattern: '^\\d+\\.\\d{' + column.format.numberMinPrecision + ',}$',
                            message: i18n('Common', 'NumberMinPrecision') + ' ' + column.format.numberMinPrecision
                        })
                    }
                }
                break
            case CommonHelper.boolean:
                section.formData[column.id] = false
                if (item) {
                    item.validationRules[0] = {
                        type: 'pattern',
                        pattern: ''
                    }
                }
                break
            default:
                break
        }
    }

    getCoords() {
        var latitude
        var longitude
        this.$scope.panels.forEach(panel => {
            if (!panel.formData) return
            var lat = panel.formData[CommonHelper.locationLatitude]
            var lng = panel.formData[CommonHelper.locationLongitude]
            if (lat && lng) {
                latitude = lat
                longitude = lng
            }
        })
        if (latitude && longitude) {
            return {
                latitude: latitude,
                longitude: longitude
            }
        }
        return null
    }

    onLocationSelected(lat: number, lng: number) {
        this.$scope.panels.forEach(panel => {
            if (panel.formData[CommonHelper.locationLatitude] != undefined && panel.formData[CommonHelper.locationLongitude] != undefined) {
                panel.formData[CommonHelper.locationLatitude] = lat
                panel.formData[CommonHelper.locationLongitude] = lng
            }
        })
    }

    getValue(id: number, table) {
        var result
        table.forEach(data => {
            if (data.id == id) {
                result = data.caption
            }
        })
        return result
    }

}