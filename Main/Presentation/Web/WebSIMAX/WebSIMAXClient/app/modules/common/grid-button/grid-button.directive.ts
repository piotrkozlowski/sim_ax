﻿import 'angular'
import { Api } from  '../../api/api.module'
import { ColumnModel, GridRequestModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from './../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'

require('./grid-button.scss')

export interface GridButtonBridge {
    dataRequest(request): ng.IPromise<any>
    columnRequest(): ng.IPromise<any>
    onSelect(e): void
    enabled: boolean
    name: string
}

interface GridButtonScope extends CommonScope {
    popoverOptions: DevExpress.ui.dxPopoverOptions
    popoverVisible: boolean
    togglePopover(): void

    popoverGridOptions: DevExpress.ui.dxDataGridOptions
    popoverGrid: DevExpress.ui.dxDataGrid
    
    buttonOptions: DevExpress.ui.dxButtonOptions
    buttonDisabled: boolean

    serialNumber: number
    bridge: GridButtonBridge
}

export class GridButtonDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        'bridge': '=bridge'
    }
    restrict = 'E'

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', ($scope: GridButtonScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService) => {
        $scope.popoverVisible = false
        $scope.popoverOptions = {
            width: 600,
            height: 500,
            target: '#popover_'+$scope.$id,
            bindingOptions: {
                visible: 'popoverVisible'
            },
            onHiding: () => {
                $scope.popoverVisible = false
            }
        }

        $scope.togglePopover = () => {
            $scope.popoverVisible = !$scope.popoverVisible
        }

        var getData = () => {
            $scope.bridge.columnRequest().then(defaultColumns => {

                var customOptions = {
                    name: $scope.bridge.name,
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: defaultColumns,
                    onRowDoubleClick: e => {
                        $scope.bridge.onSelect(e)
                        $scope.popoverGrid.clearSelection()
                        $scope.togglePopover()
                    },
                    onDataReady: () => {
                        $scope['showGrid'] = true
                        $timeout(() => {
                            $scope.popoverGrid.updateDimensions()
                        }, 0)
                    },
                    selection: { mode: 'single' },
                    allowColumnReordering: false
                }

                $scope.popoverGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, $scope.bridge.dataRequest, customOptions)
                $timeout(() => {
                    $scope.popoverGrid.option($scope.popoverGridOptions)
                }, 0)
            })
        }

        $scope.popoverGridOptions = {
            onInitialized: (e) => {
                $scope.popoverGrid = e.component
                getData()
            }
        }

        $scope.buttonDisabled = true
        $scope.$watch(() => {
            return !$scope.bridge.enabled
        }, (value: boolean) => {
            $scope.buttonDisabled = value
        })
        $scope.buttonOptions = {
            activeStateEnabled: false,
            text: i18n("Common", "SelectDevice"),
            onClick: () => {
                $scope.togglePopover()
            },
            bindingOptions: {
                disabled: 'buttonDisabled'
            }
        }
    }]

    template = require("./grid-button.tpl.html")
    replace = true
}