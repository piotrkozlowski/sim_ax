﻿import 'angular'
import { Api } from  '../../api/api.module'
import { ColumnModel, GridRequestModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'
require('./grid-select.scss')

export interface GridSelectBridge {
    value: any
    elements: any[]
    data?: any[]
    dataRequest(request): ng.IPromise<any>
    columnRequest(): ng.IPromise<any>
    onSelect(e): void
    onDoubleRowClick(e): void
    name: string

}

interface GridSelectScope extends CommonScope {
    popoverOptions: DevExpress.ui.dxPopoverOptions
    popoverVisible: boolean
    togglePopover(): void

    popoverGridOptions: DevExpress.ui.dxDataGridOptions
    popoverGrid: DevExpress.ui.dxDataGrid
    columnsCount: number

    textBoxOptions: DevExpress.ui.dxTextBoxOptions
    textBoxButtonOptions: DevExpress.ui.dxButtonOptions

    serialNumber: number
    bridge: GridSelectBridge
}

export class GridSelectDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        'bridge': '=bridge'
    }
    restrict = 'E'

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', ($scope: GridSelectScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService) => {

        $scope.popoverVisible = false
        $scope.popoverOptions = {
            width: 600,
            height: 500,
            target: '#popover_'+$scope.$id,
            bindingOptions: {
                visible: 'popoverVisible'
            },
            onHiding: () => {
                $scope.popoverVisible = false
            }
        }

        $scope.togglePopover = () => {
            $scope.popoverVisible = !$scope.popoverVisible
        }

        $scope.popoverGridOptions = {
            onInitialized: (e) => {
                $scope.popoverGrid = e.component
                getData()
            }
        }

        var setCustomGridOptions = (defaultColumns) => {
            var customGridOptions = {
                name: $scope.bridge.name,
                defaultColumnsDictionary: defaultColumns,
                columnsDictionary: defaultColumns,
                onRowClick: e => {
                    $scope.bridge.onSelect(e)
                },
                onRowDoubleClick: e => {
                    $scope.bridge.onDoubleRowClick(e)
                    $scope.togglePopover()
                },
                onDataReady: () => {
                   $scope.popoverGrid.updateDimensions()
                },
                onContentReady: () => {
                    $scope.popoverGrid.option('pager.visible', true)
                },
                allowColumnReordering: false,
                pager: {
                    showPageSizeSelector: false,
                    allowedPageSizes: [100],
                    showInfo: true,
                    infoText: i18n('Grid', 'PagerTextShort'),
                    visible: false
                }
            }
            return customGridOptions
        }

        var getData = () => {
            $scope.bridge.columnRequest().then(defaultColumns => {
                $scope.columnsCount = defaultColumns.length
                var customGridOptions = setCustomGridOptions(defaultColumns)
                $scope.popoverGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger,
                    $scope.bridge.dataRequest, customGridOptions)
                if ($scope.popoverGrid) {
                    $scope.popoverGrid.option($scope.popoverGridOptions)
                    if ($scope.bridge.data) {
                        $timeout(() => {
                            $scope.popoverGrid.option('dataSource', $scope.bridge.data)
                        })
                    }
                }                
            })
        }


        $scope.textBoxOptions = {
            width: '100%',
            placeholder: i18n('Common','Select...'),
            readOnly: true,
            bindingOptions: {
                value: 'bridge.value'
            },
            onFocusIn: () => {
                $scope.popoverVisible = true
            }
        }
        $scope.textBoxButtonOptions = {
            activeStateEnabled: false,
            template: CommonHelper.getIcon('ARROW_DROP_UP'),
            onClick: () => {
                $scope.togglePopover()
            }
        }
    }]

    template = require("./grid-select.tpl.html")
    replace = true
}