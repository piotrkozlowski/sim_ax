﻿import 'angular'
import { EventsNames } from '../../../helpers/events-names'
import { UserModel, GridModel, FormValuesModel } from '../../api/api.models'
import { i18n } from '../../common/i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { ActiveDistributorsInfo } from '../header/common.header.directive'
require('./user-menu.scss')
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { Permissions } from '../../../helpers/permissions'
import { GridData } from '../../common/common.interfaces'
import { CommonScope } from '../common.extended-data-grid-options'

interface UserMenuScope extends CommonScope {
    user: UserModel
    options: any
    openUserDetails(): void
    openChangePassword(): void
    distributors: ActiveDistributorsInfo
    isDisabledSubmit: boolean
    logoutIcon: string
    menuPopoverOptions: DevExpress.ui.dxPopoverOptions
    menuPopover: DevExpress.ui.dxPopover

    showNoPermission: boolean
    userDetailsPopupId: number
    showUserDetails: boolean

    myAccountButtonOptions: DevExpress.ui.dxButtonOptions
    signOutButtonOptions: DevExpress.ui.dxButtonOptions
    submitButtonOptions: DevExpress.ui.dxButtonOptions
    changePasswordButtonOptions: DevExpress.ui.dxButtonOptions

    grid: GridData
    allDistributors: any[]

    onInit: () => void
}

export class UserMenuDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        user: '=',
        options: '=',
        distributors: '=activeDistributors'
    }
    controller = ['$scope', '$timeout', 'api', 'localDictionary', ($scope: UserMenuScope, $timeout: ng.ITimeoutService, api: Api, localDictionary: LocalDictionaryFactory) => {
        $scope.grid = <GridData>{}
        $scope.isDisabledSubmit = true
        $scope.logoutIcon = window['config']['ICONS']['LOGOUT']
        $scope.user.avatar = $scope.user.avatar ? "data:image/png;base64," + $scope.user.avatar : null

        $scope.userDetailsPopupId = $scope.$id

        $scope.openUserDetails = () => {
            $scope.showUserDetails = true
            $timeout(() => {
                $scope.$root.$broadcast(EventsNames.OPEN_USER_DETAILS_POPUP + $scope.userDetailsPopupId, $scope.user.id, [{
                    id: $scope.user.id,
                    name: $scope.user.username
                }])
            }, 0)

            $scope.options.popoverVisible = false
        }

        $scope.openChangePassword = () => {
            $scope.$root.$broadcast(EventsNames.OPEN_PASSWORD_CHANGE_POPUP, $scope.user.id, [{
                id: $scope.user.id,
                name: $scope.user.username
            }])

            $scope.options.popoverVisible = false
        }

        var loadGrid = (values) => {
            if ($scope.grid.grid) {
                $scope.grid.grid.option("dataSource").store().clear()
                values.forEach(value => {
                    $scope.grid.grid.option("dataSource").store().insert({
                        name: value['name'],
                        isEnabled: value['isenabled'],
                        id: value['distributorid']
                    })
                })

                $scope.grid.grid.refresh()
            }
        }

        var submitDistributors = () => {
            var distributorsIds = []
            $scope.grid.showGrid = false

            $scope.grid.grid.option("dataSource").store()._array.forEach(dist => {
                if (dist.isEnabled) {
                    distributorsIds.push(dist.id)
                }
            })

            var model = new FormValuesModel()
            model.values = distributorsIds

            api.users.changeAllowedDistributors(model).then(result => {
                if (!result) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'ChangeAllowedDistributorsError'), 'error', 5000)
                    return
                }

                if (!$scope.distributors)
                    $scope.distributors = <ActiveDistributorsInfo>{}

                $scope.distributors.count = result.result.length
                if ($scope.distributors.count > 1) {
                    $scope.distributors.multiple = true
                }
                else {
                    $scope.distributors.multiple = false
                    $scope.grid.grid.option('dataSource').store()._array.forEach(dist => {
                        if (dist.id == result.result[0]) {
                            $scope.distributors.singleDistributor = dist.name
                        }
                    })
                }

                $scope.isDisabledSubmit = true
                localDictionary.refreshDictionaries(true).then(result => {
                    CommonHelper.openInfoPopup(i18n('Popup', 'ChangeAllowedDistributorsSuccess'))
                    $scope.$root.$broadcast(EventsNames.CHANGED_ALLOWED_DISTRIBUTORS)
                    $scope.grid.showGrid = true
                })
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'ChangeAllowedDistributorsError'), 'error', 5000)
            })
        }

        $scope.menuPopoverOptions = {
            width: 400,
            height: 500,
            onInitialized: (e) => {
                $scope.menuPopover = e.component
            },
            target: '.menu-button',
            bindingOptions: {
                visible: 'options.popoverVisible'
            },
            onHiding: () => {
                $scope.options.popoverVisible = false
            }
        }

        $scope.myAccountButtonOptions = {
            text: i18n('Common', 'MyAccount'),
            onClick: $scope.openUserDetails,
            height: 36
        }

        $scope.signOutButtonOptions = {
            text: i18n('Common', 'SignOut'),
            icon: 'icon-logout',
            height: 36,
            template: 'signOutButton'
        }

        $scope.submitButtonOptions = {
            text: i18n('Common', 'Apply'),
            height: 36,
            onClick: submitDistributors,
            bindingOptions: {
                disabled: 'isDisabledSubmit'
            }
        }

        $scope.changePasswordButtonOptions = {
            hint: i18n('Popup', 'ChangePassword'),
            template: CommonHelper.getIcon('PASSWORD'),
            onClick: $scope.openChangePassword,
            height: 36
        }

        var customGridOptions = {
            columns: [
                {
                    dataField: 'name',
                    caption: i18n('Common', 'Distributor'),
                    allowEditing: false,
                    dataType: 'string'
                },
                {
                    dataField: 'isEnabled',
                    caption: i18n('Common', 'Status'),
                    allowEditing: true,
                    dataType: 'boolean',
                    falseText: i18n('Common', 'Inactive'),
                    trueText: i18n('Common', 'Active'),
                }
            ],
            onInitialized: e => {
                $scope.grid.grid = e.component
                $scope.$root.$broadcast('disableLoadingIndicator')
                //Permissions.runIfHasPermissions([Permissions.DISTRIBUTOR_LIST], () => {
                    api.distributor.getAllDistributors(null).then(result => {
                        $scope.allDistributors = result.values
                        loadGrid($scope.allDistributors)
                        $scope.grid.showGrid = true
                        $scope.$root.$broadcast('enableLoadingIndicator')
                    })
                //})
            },
            onRowUpdated: (e) => {
                $scope.isDisabledSubmit = false
            },
            editing: {
                allowUpdating: true,
                mode: "cell"
            }
        }
        $scope.grid.options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, null, null, customGridOptions)
        /*Permissions.hasPermissions([Permissions.DISTRIBUTOR_LIST]).then(hasPermission => {
            if (!hasPermission) {
                $scope.showNoPermission = true
            }
        })*/
    }]
    template = require("./user-menu.tpl.html")
    replace = true
}