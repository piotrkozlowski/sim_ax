﻿import 'angular'
import { LoggerService } from  '../../common.module'
import { ChartDirectiveController } from './chart.directive.controller'

export class MeasurementsChartDirective implements ng.IDirective {
    scope = {
        "source": "=",
        "groups": "=",
        "devices": "=",
        "meters": "=",
        "showAllSeries": "="
    }
    controller = ['$scope', '$filter', '$q', '$timeout', 'logger',
        ($scope, $filter: ng.IFilterService, $q: ng.IQService, $timeout: ng.ITimeoutService, logger: LoggerService) =>
            new ChartDirectiveController($scope, $filter, $q, $timeout, logger)
    ]
    template = require("./chart.tpl.html")
    restrict = 'E'
    replace = true
}