﻿interface ILoggerService {
    log(error: any)
}

export class LoggerService implements ILoggerService {
    log(error: any) {
        if (error && error.data && error.data.Message) alert(error.data.Message)
        else if (typeof error === 'string') alert(error)
        else console.log(error)
    }
}