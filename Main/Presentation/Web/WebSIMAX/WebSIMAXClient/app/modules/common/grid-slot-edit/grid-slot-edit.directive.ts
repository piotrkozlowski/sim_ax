﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, DeviceHierarchyModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridButtonBridge } from '../../common/grid-button/grid-button.directive'
import { i18n } from './../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'

require('./grid-slot-edit.scss')

interface GridSlotBridge {
    id: number
    title: string
    items: any[]
    popupId: number,
    dictionarySlotMethodName: string
    dictionaryMethodName: string
    dataMethodName: string
    dataScope: string
    toStringColumn: number
    keyColumn: number

    onSubmit?: (changed: any[]) => ng.IPromise<DeviceHierarchyModel[]>
}

interface GridSlotEditScope extends CommonScope {
    gridOptions: DevExpress.ui.dxDataGridOptions
    grid: DevExpress.ui.dxDataGrid
    rowSelected: boolean
    bridge: GridButtonBridge
    saveButtonDisabled: boolean

    toggleStateButtonIcon(): string
    toggleState(): void
    switchState: boolean
    stateSwitch: DevExpress.ui.dxSwitch
    stateSwitchOptions: DevExpress.ui.dxSwitchOptions
    removeButtonOptions: DevExpress.ui.dxButtonOptions
    saveButtonOptions: DevExpress.ui.dxButtonOptions

    source: GridSlotBridge
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean

    changed: any[]
}

export class GridSlotEditDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        source: '='
    }
    template = require("./grid-slot-edit.tpl.html")

    link = (scope, element, attrs) => {
        scope.$watch("source", value => {
            scope.source = value
        })
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope: GridSlotEditScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        $scope.popupVisible = false
        $scope.popupOptions = {
            showTitle: true,
            showCloseButton: true,
            title: $scope.source.title,
            bindingOptions: {
                visible: 'popupVisible'
            },
            onHiding: () => {
                $scope.$root.$broadcast(EventsNames.CLOSE_GRID_EDIT_POPUP + $scope.source.popupId)
            }
        }

        $scope.$on(EventsNames.OPEN_GRID_EDIT_POPUP + $scope.source.popupId, (event: ng.IAngularEvent, ...args: any[]) => {
            if ($scope.grid) {
                $scope.saveButtonDisabled = true
                $scope.grid.repaint()
            }
            load().then(() => {
                $scope.gridOptions.dataSource.store().clear()
                $scope.source.items.forEach(element => {
                    $scope.gridOptions.dataSource.store().insert(element)
                })
                $scope.grid.option($scope.gridOptions)
                $scope.grid.refresh()
            })
            $scope.popupVisible = true
        })

        $scope.switchState = null

        $scope.stateSwitchOptions = {
            height: '100%',
            onInitialized: e => {
                $scope.stateSwitch = e.component
            },
            onValueChanged: (e) => {
                if ($scope.grid && e.jQueryEvent) {
                    var rowData: any[] = $scope.grid.getSelectedRowsData()
                    if (rowData.length) {
                        rowData[0].active = e.value
                        $scope.saveButtonDisabled = false
                        rowData[0].isChanged = true

                        var isAlreadyInChanged = false

                        if (!$scope.changed)
                            $scope.changed = []

                        $scope.changed.forEach(ch => {
                            if (ch.id == rowData[0].id) {
                                ch.active = e.value
                                isAlreadyInChanged = true
                            }
                        })

                        if (!isAlreadyInChanged)
                            $scope.changed.push(rowData[0])

                        $timeout(() => {
                            $scope.grid.clearSelection()
                        })
                    }
                }
            },
            bindingOptions: {
                disabled: '!rowSelected',
                value: 'switchState'
            }
        }

        var clearSlot = () => {
            if ($scope.grid) {
                var entity = (<any>$scope.grid.getSelectedRowsData()[0])
                entity.device = null;
                entity.deviceId = null
                entity.isChanged = true
                $scope.saveButtonDisabled = false

                var isAlreadyInChanged = false

                if (!$scope.changed)
                    $scope.changed = []

                $scope.changed.forEach(ch => {
                    if (ch.id == entity.id) {
                        ch.device = null
                        ch.deviceId = null
                        isAlreadyInChanged = true
                    }
                })

                if (!isAlreadyInChanged)
                    $scope.changed.push(entity)

                $timeout(() => {
                    $scope.grid.clearSelection()
                })
            }
        }

        $scope.$watch(() => {
            if ($scope.grid) {
                return <any[]>$scope.grid.getSelectedRowsData()
            }
        }, (rowsData: any[]) => {
            if (rowsData && rowsData[0]) {
                $timeout(() => {
                    $scope.switchState = (rowsData[0].active || false)
                    $scope.stateSwitch.option("value", $scope.switchState)
                    $scope.stateSwitch.repaint()
                })
            }
        })

        $scope.rowSelected = false

        var isSlotSelected = () => {
            return $scope.grid ? $scope.grid.getSelectedRowKeys().length != 0 : false
        }

        $scope.saveButtonDisabled = true

        var saveChanges = () => {
            if ($scope.source.onSubmit) {
                $scope.source.onSubmit($scope.changed).then(result => {
                    if (result) {
                        $scope.source.items.length = 0
                        $scope.grid.option("dataSource").items().forEach(item => {
                            if (item.isChanged) delete item.isChanged
                            $scope.source.items.push(item)
                        })
                        $scope.saveButtonDisabled = true
                    }
                })
            }
        }

        $scope.$watch(isSlotSelected, (value) => {
            $scope.rowSelected = $scope.bridge.enabled = value
        })

        var setCustomGridOptions = (defaultColumns, deferred) => {
            var customGridOptions = {
                columnsDictionary: defaultColumns,
                onInitialized: e => {
                    $scope.grid = e.component
                    deferred.resolve()
                },
                selection: {
                    mode: 'single'
                },
                onRowPrepared: (e: any) => {
                    if (e.rowType == "data") {
                        if (e.data.isChanged) {
                            $(e.rowElement).css('font-style', 'italic')
                        }
                    }
                }
            }
            return customGridOptions
        }

        var load = () => {
            var deferred = $q.defer()
            localDictionary[$scope.source.dictionarySlotMethodName]().then(defaultColumns => {
                var customGridOptions = setCustomGridOptions(defaultColumns, deferred)
                $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger, null, customGridOptions)
            })
            if ($scope.grid) {
                deferred.resolve()
            }
            return deferred.promise
        }

        $scope.bridge = {
            onSelect: (e) => {
                $timeout(() => {
                    var entity = (<any>$scope.grid.getSelectedRowsData()[0])
                    entity.device = e.data[$scope.source.toStringColumn]
                    entity.deviceId = e.data[$scope.source.keyColumn]
                    entity.isChanged = true
                    $scope.saveButtonDisabled = false

                    var isAlreadyInChanged = false

                    if (!$scope.changed)
                        $scope.changed = []

                    $scope.changed.forEach(ch => {
                        if (ch.id == entity.id) {
                            ch.device = entity.device
                            ch.deviceId = entity.deviceId
                            isAlreadyInChanged = true
                        }
                    })

                    if (!isAlreadyInChanged)
                        $scope.changed.push(entity)

                    $timeout(() => {
                        $scope.grid.clearSelection()
                    })
                })
            },
            dataRequest: (request): ng.IPromise<any> => {
                return api[$scope.source.dataScope][$scope.source.dataMethodName](request)
            },
            columnRequest: (): ng.IPromise<any> => {
                return localDictionary[$scope.source.dictionaryMethodName]()
            },
            enabled: $scope.rowSelected,
            name: null
        }

        $scope.removeButtonOptions = {
            onClick: clearSlot,
            template: CommonHelper.getIcon('REMOVE'),
            bindingOptions: {
                disabled: '!rowSelected'
            }
        }

        $scope.saveButtonOptions = {
            text: i18n('Common', 'Apply'),
            onClick: saveChanges,
            bindingOptions: {
                disabled: 'saveButtonDisabled'
            }
        }

        $scope.$watchCollection('source.items', (items: any[]) => {
            if ($scope.grid) {
                $scope.grid.option("dataSource").store().clear()
                items.forEach((item) => {
                    $scope.grid.option("dataSource").store().insert(item)
                })
                $scope.grid.refresh()
            }
        })
    }]
}