﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { CommonScope } from '../../Common/common.extended-data-grid-options'

require('./groups-edit.scss')

export interface GroupsBridge {
    id: number
    assigned: any[],
    popupId: number,
    onSubmit: (added: any[], removed: any[]) => ng.IPromise<boolean>
}

interface GroupsData {
    transferAvailableGroups(): void
    transferAssignedGroups(): void
    allowCustomLoading: boolean
}

interface ChangesState {
    availableItems: any[]
    assignedItems: any[]
    assigned: any[]
    retracted: any[]
}

interface GroupsEditScope extends CommonScope {
    availableGroupsGridOptions: DevExpress.ui.dxDataGridOptions
    assignedGroupsGridOptions: DevExpress.ui.dxDataGridOptions
    availableGroupsGrid: DevExpress.ui.dxDataGrid
    assignedGroupsGrid: DevExpress.ui.dxDataGrid

    source: GroupsBridge
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    popupTitle: string

    submitButtonOptions: DevExpress.ui.dxButtonOptions
    transferBackButtonOptions: DevExpress.ui.dxButtonOptions
    transferForwardButtonOptions: DevExpress.ui.dxButtonOptions
    groups: GroupsData

    assigned: any[]
    retracted: any[]
    noChanges: boolean
    oldId: number

    hideGroupsGrid: boolean
    loadPanelMessage: string
}

export class GroupsEditDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        source: '='
    }
    template = require("./groups-edit.tpl.html")

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope: GroupsEditScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        $scope.popupVisible = false
        $scope.hideGroupsGrid = false
        $scope.loadPanelMessage = i18n('Popup', 'Saving')

        var isRequestCancelled = false
        $scope.popupOptions = {
            showTitle: true,
            showCloseButton: true,
            bindingOptions: {
                visible: 'popupVisible',
                title: 'popupTitle'
            },
            onHiding: () => {
                if (!requestState.request.$resolved) {
                    isRequestCancelled = true
                    requestState.request.$cancelRequest()
                }
                else {
                    stageChanges(true)
                }
                $scope.$root.$broadcast(EventsNames.CLOSE_GROUPS_EDIT_POPUP + $scope.source.popupId)
            }
        }
        var stageChanges = (backward) => {
            var stage = () => {                
                $scope.hideGroupsGrid = true
                var availableDataSource = $scope.availableGroupsGrid.option("dataSource")
                var assignedDataSource = $scope.assignedGroupsGrid.option("dataSource")
                var apply = (store) => {
                    store.createQuery().filter((backward ? 'isRemoved' : 'isNew'), '=', true).toArray().forEach(element => {
                        element[(backward ? 'isRemoved' : 'isNew')] = false
                    })

                    store.createQuery().filter((backward ? 'isNew' : 'isRemoved'), '=', true).toArray().forEach(element => {
                        store.remove(element)
                    })
                }
                apply(availableDataSource.store())
                apply(assignedDataSource.store())
                $scope.assigned.length = 0
                $scope.retracted.length = 0
                $scope.availableGroupsGrid.refresh()
                $scope.assignedGroupsGrid.refresh()                
                $scope.hideGroupsGrid = false
            }
            if (!backward) {                
                $scope.hideGroupsGrid = true
                $scope.source.onSubmit($scope.assigned.map(g => g.id), $scope.retracted.map(g => g.id)).then(stage)
            }
            else
                stage()
        }

        $scope.$watch(() => {
            return !($scope.assigned.length + $scope.retracted.length)
        }, value => {
            $scope.noChanges = value
            })

        $scope.$watch('source.id', (sourceId: number) => {
        })

        $scope.$on(EventsNames.OPEN_GROUPS_EDIT_POPUP + $scope.source.popupId, (event: ng.IAngularEvent, ...args: any[]) => {            
            if ($scope.oldId && $scope.source.id != $scope.oldId || isRequestCancelled) {
                $scope.availableGroupsGridOptions = null
                $scope.assignedGroupsGridOptions = null
                isRequestCancelled = false
            }
            $scope.groups.allowCustomLoading = true
            if ($scope.availableGroupsGrid)
                $scope.availableGroupsGrid.repaint()
            loadGroups().then(() => {
                $scope.availableGroupsGrid.option($scope.availableGroupsGridOptions)
                $timeout(() => {
                    var promises = []
                    var store = $scope.availableGroupsGrid.option("dataSource").store()
                    $scope.source.assigned.forEach(element => {
                        var deferred = $q.defer()
                        promises.push(deferred.promise)
                        var elementToRemove = store.createQuery().filter('id', '=', element.id).toArray()[0]
                        store.remove(elementToRemove).then(deferred.resolve)
                    })
                    $scope.assignedGroupsGrid.option($scope.assignedGroupsGridOptions)
                    $q.all(promises).then(() => {
                        $timeout(() => {
                            $scope.availableGroupsGrid.refresh()
                            $scope.assignedGroupsGrid.refresh()
                        }, 0)
                    })
                }, 0)
            })
            $scope.popupVisible = true
            $scope.popupTitle = i18n('Popup', 'GroupsEdit') + " (" + $scope.assigned.length + ")"
            $scope.oldId = $scope.source.id
        })

        $scope.groups = <GroupsData>{
            allowCustomLoading: true
        }

        $scope.assigned = []
        $scope.retracted = []
        $scope.popupTitle = i18n('Popup', 'GroupsEdit') + " (" + $scope.assigned.length + ")"
        let requestState: any = {}
        var availableGroupsDataRequest = () => {
            return api.group.getLocationGroups(requestState).then(result => {
                return result.result
            })
        }
        var assignedGroupsDataRequest = () => {
            var deferred = $q.defer()
            deferred.resolve($scope.source.assigned.slice())
            return deferred.promise
        }

        var onRowPrepared = (e: any) => {
            if (e.rowType == "data") {
                if (e.data.isNew) {
                    $(e.rowElement).css('font-style', 'italic')
                }
                else if (e.data.isRemoved) {
                    $(e.rowElement).css('text-decoration', 'line-through')
                }
            }
        }

        var setAvailableGroupsGridCustomOptions = (deferredAvailable, deferredAvailableData) => {
            var availableGroupsGridCustomOptions = {
                columns: [
                    {
                        dataField: "name",
                        caption: i18n('Popup', 'AvailableGroups'),
                        selectedFilterOperation: 'contains'
                    }, {
                        dataField: "id",
                        visible: false
                    }
                ],
                onInitialized: e => {
                    $scope.availableGroupsGrid = e.component
                    if ($scope.groups.allowCustomLoading)
                        $scope.availableGroupsGrid.beginCustomLoading(null)
                    deferredAvailable.resolve()
                },
                onDataReady: () => {
                    $scope.groups.allowCustomLoading = false
                    if ($scope.availableGroupsGrid)
                        $scope.availableGroupsGrid.endCustomLoading()
                    deferredAvailableData.resolve()
                },
                loadPanel: {
                    showPane: false,                    
                    text:''
                },
                onRowDoubleClick: openGroup,
                onRowPrepared: onRowPrepared
            }
            return availableGroupsGridCustomOptions
        }

        var setAssignedGroupsCustomGridOptions = (deferredAssigned) => {
            var assignedGroupsCustomGridOptions = {
                onInitialized: e => {
                    $scope.assignedGroupsGrid = e.component
                    if ($scope.groups.allowCustomLoading)
                        $scope.availableGroupsGrid.beginCustomLoading(null)
                    deferredAssigned.resolve()
                },
                columns: [
                    {
                        dataField: "name",
                        caption: i18n('Popup', 'AssignedGroups'),
                        selectedFilterOperation: 'contains'
                    }, {
                        dataField: "id",
                        visible: false
                    }
                ],
                onRowDoubleClick: openGroup,
                onRowPrepared: onRowPrepared,
                onDataReady: () => {
                    if ($scope.availableGroupsGrid)
                        $scope.availableGroupsGrid.endCustomLoading()
                }
            }
            return assignedGroupsCustomGridOptions
        }

        var openGroup = (e) => {
            $scope.$root.$broadcast(EventsNames.OPEN_LOCATION_DETAILS_POPUP + $scope.source.popupId, e.data.id)
        }

        var loadGroups = () => {
            var deferredAvailable = $.Deferred()
            var deferredAvailableData = $.Deferred()
            var deferredAssigned = $.Deferred()
            $timeout(() => {              
                var availableGroupsGridCustomOptions = setAvailableGroupsGridCustomOptions(deferredAvailable, deferredAvailableData)
                $scope.availableGroupsGridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger,
                    availableGroupsDataRequest, availableGroupsGridCustomOptions)

                var assignedGroupsGridCustomOptions = setAssignedGroupsCustomGridOptions(deferredAssigned)
                $scope.assignedGroupsGridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger,
                    assignedGroupsDataRequest, assignedGroupsGridCustomOptions)                
            }, 0)
            return $.when(deferredAvailable.promise(), deferredAvailableData.promise(), deferredAssigned.promise())
        }

        var transferData = (keys: any[], added: any[], removed: any[], from: DevExpress.data.Store, to: DevExpress.data.Store): JQueryPromise<any> => {
            var deferred = $.Deferred()
            keys.forEach(element => {
                if (!element.isRemoved) {
                    if (element.isNew) {
                        var index
                        removed.every((el, i) => {
                            if (el.id == element.id) {
                                index = i
                                return false
                            }
                            return true
                        })
                        removed.splice(index, 1)
                        from.remove(element).then((e) => {
                            (<any>to).createQuery().filter('id', '=', e.id).toArray()[0].isRemoved = false
                            deferred.resolve()
                        })
                    } else {
                        added.push(element)
                        var newElement = (<any>Object).assign({}, element)
                        newElement.isNew = true
                        element.isRemoved = true
                        to.insert(newElement).then((e) => {
                            deferred.resolve()
                        })
                    }
                }
            })
            return deferred.promise()
        }

        $scope.groups.transferAvailableGroups = () => {
            var availableDataSource = $scope.availableGroupsGrid.option("dataSource")
            var assignedDataSource = $scope.assignedGroupsGrid.option("dataSource")
            var selectedGroups = $scope.availableGroupsGrid.getSelectedRowsData()
            transferData(selectedGroups, $scope.assigned, $scope.retracted, availableDataSource.store(), assignedDataSource.store()).then(() => {
                $scope.availableGroupsGrid.refresh()
                $scope.assignedGroupsGrid.refresh()
            })
            $scope.availableGroupsGrid.deselectAll()
        }

        $scope.groups.transferAssignedGroups = () => {
            var availableDataSource = $scope.availableGroupsGrid.option("dataSource")
            var assignedDataSource = $scope.assignedGroupsGrid.option("dataSource")
            var selectedGroups = $scope.assignedGroupsGrid.getSelectedRowsData()
            transferData(selectedGroups, $scope.retracted, $scope.assigned, assignedDataSource.store(), availableDataSource.store()).then(() => {
                $scope.availableGroupsGrid.refresh()
                $scope.assignedGroupsGrid.refresh()
            })
            $scope.assignedGroupsGrid.deselectAll()
        }

        var submitButton: DevExpress.ui.dxButton
        $scope.submitButtonOptions = {
            text: i18n('Common', 'Apply'),
            bindingOptions: {
                disabled: 'noChanges'
            },
            onInitialized: e => {
                submitButton = e.component
            },
            onClick: stageChanges.bind(this, false)
        }

        $scope.transferBackButtonOptions = {
            template: CommonHelper.getIcon('ARROW_BACK'),
            onClick: $scope.groups.transferAssignedGroups
        }

        $scope.transferForwardButtonOptions = {
            template: CommonHelper.getIcon('ARROW_FORWARD'),
            onClick: $scope.groups.transferAvailableGroups
        }
    }]
}