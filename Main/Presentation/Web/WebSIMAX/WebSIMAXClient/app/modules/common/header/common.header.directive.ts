﻿import 'angular'
import { EventsNames } from '../../../helpers/events-names'
import { UserModel, ModelAlarm } from '../../api/api.models'
import { i18n } from '../../common/i18n/common.i18n'

require('./header.scss')
import { Api } from  '../../api/api.module'
import { CommonHelper } from '../../../helpers/common-helper'

export class ActiveDistributorsInfo {
    count = 0
    multiple: boolean
    singleDistributor: string
    proccessDone: boolean
}

interface HeaderScope extends ng.IScope {
    user: UserModel
    openUserDetails(): void
    logoutIcon: string
    distributorsIcon: string
    userIcon: string
    alarmIcon: string

    userMenuOptions: any
    alarmsMenuOptions: any

    myAccountButtonOptions: DevExpress.ui.dxButtonOptions
    signOutButtonOptions: DevExpress.ui.dxButtonOptions
}

export class HeaderDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    link = (scope: ng.IScope, element: JQuery) => {
        scope['setLocale'] = (locale) => {
            CommonHelper.setValueToLocalStorage('LOCALE', locale)
            location.href = '#/logout'
            window.location.reload()
        }
    }
    controller = ['$scope', '$interval', '$state', 'api', ($scope: HeaderScope, $interval: ng.IIntervalService, $state: angular.ui.IStateService, api: Api) => {
        $scope.user = api.login.getUser();

        $scope.logoutIcon = window['config']['ICONS']['LOGOUT']
        $scope.userIcon = window['config']['ICONS']['USER']
        $scope.distributorsIcon = window['config']['ICONS']['DISTRIBUTORS']
        $scope.alarmIcon = window['config']['ICONS']['ALARMS']

        $interval(() => {
            if ($scope.alarmIcon == window['config']['ICONS']['ALARMS'] && $scope.$root['alarmsCount'] > 0) $scope.alarmIcon = window['config']['ICONS']['ALARMS_ACTIVE']
            else $scope.alarmIcon = window['config']['ICONS']['ALARMS']
        }, 500)

        if ($scope.user) {
            $scope.user.avatar = $scope.user.avatar ? "data:image/png;base64," + $scope.user.avatar : null
            $scope.userMenuOptions = {
                target: '.menu-button',
                popoverVisible: false
            }
            $scope.alarmsMenuOptions = {
                popoverVisible: false
            }
        }

        $scope['showDashboard'] = () => {
            $state.go('dashboard')
            $scope.$root.$broadcast(EventsNames.CLOSE_DETAILS_POPUP)
        }

        // chrome fix
        $scope.$on(EventsNames.OPENED_POPUP, () => {
            var sel = angular.element(".navbar-logo")[0]
            sel.style.display = 'none';
            sel.offsetHeight;
            sel.style.display = 'block';
        })

        $scope.$on(EventsNames.CLOSED_POPUP, () => {
            var sel = angular.element(".navbar-logo")[0]
            sel.style.display = 'none';
            sel.offsetHeight;
            sel.style.display = 'block';
        })
        // end of chrome fix
    }]
    template = require("./header.tpl.html")
    replace = true
}