﻿import 'angular'
import { LoggerService } from  '../common.module'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
require('./date-picker.scss')
import * as moment from 'moment'

interface CustomDatePeriod {
    id: any
    title: string
}

export interface DatePickerBridge {
    fromDate: Date
    toDate: Date
    customPeriods?: CustomDatePeriod[]
    callback(customPeriod: CustomDatePeriod): void
    isSmallScreen?: boolean
}

interface DatePickerScope extends ng.IScope {
    source: DatePickerBridge
    fromDateBoxOptions: DevExpress.ui.dxDateBoxOptions
    toDateBoxOptions: DevExpress.ui.dxDateBoxOptions
    fromDateBox: DevExpress.ui.dxDateBox
    toDateBox: DevExpress.ui.dxDateBox
    periodButtonOptions: DevExpress.ui.dxButtonOptions
    smScreenPeriodButtonOptions: DevExpress.ui.dxButtonOptions
    prevButtonOptions: DevExpress.ui.dxButtonOptions
    nextButtonOptions: DevExpress.ui.dxButtonOptions

    isCustom: boolean
    hidePickers: boolean

    periodPopoverVisible: boolean
    periodPopoverOptions: DevExpress.ui.dxPopoverOptions
    periodPopover: DevExpress.ui.dxPopover
    periodListOptions: DevExpress.ui.dxListOptions
    selectedPeriod: string
    periods: {
        day: string, week: string, month: string, year: string, custom: string
    }
}

export class DatePickerDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        "source": "="
    }

    restrict = 'E'
    controller = ['$scope', '$q', '$timeout', 'logger', ($scope: DatePickerScope, $q: ng.IQService, $timeout: ng.ITimeoutService, logger: LoggerService) => {

        var date = new Date()
        date.setDate(date.getDate() - 6)
        var fromDate = UserConfigHelper.getUserConfig(UserConfigHelper.FromDate, date)
        $scope.source.fromDate = moment(fromDate).toDate()

        var periods = {
            day: i18n("Common", "Day"),
            week: i18n("Common", "Week"),
            month: i18n("Common", "Month"),
            year: i18n("Common", "Year"),
            custom: i18n("Common", "Custom")
        }

        var periodItems = [periods.day, periods.week, periods.month, periods.year, periods.custom]
        $scope.periods = periods

        $scope.fromDateBoxOptions = <any>{
            acceptCustomValue: true,
            type: 'date',
            max: new Date(),
            displayFormat: i18n("Common", "DateFormatDX"),
            onInitialized: e => {
                $scope.fromDateBox = e.component
            },
            dateOutOfRangeMessage: i18n('Common', 'ValueIsOutOfRange'),
            onValueChanged: object => {
                var fromDate = moment(object.value).toDate()
                fromDate.setHours(0, 0, 0, 0)
                $scope.source.fromDate = fromDate
                $scope.toDateBox.option("min", fromDate)
                if (fromDate > $scope.source.toDate) {
                    var toDate = moment(fromDate).toDate()
                    $scope.source.toDate = toDate = fromDate
                }
                if ($scope.selectedPeriod != periods.custom) {
                    var toDate = moment(fromDate).toDate()
                    switch ($scope.selectedPeriod) {
                        case periods.week:
                            toDate.setDate(toDate.getDate() + 6)
                            break;
                        case periods.month:
                            toDate.setMonth(toDate.getMonth() + 1)
                            break;
                        case periods.year:
                            toDate.setFullYear(toDate.getFullYear() + 1)
                            break;
                    }
                }
                if (toDate) {
                    toDate.setHours(23, 59, 59, 999)
                    $scope.source.toDate = toDate
                    $scope.toDateBox.option("value", toDate)
                }
                $scope.source.callback(getPeriod($scope.selectedPeriod))
                UserConfigHelper.setUserConfig(UserConfigHelper.FromDate, $scope.source.fromDate)
            },
            value: $scope.source.fromDate
        }


        var toDate = UserConfigHelper.getUserConfig(UserConfigHelper.ToDate, new Date())        
        $scope.source.toDate = moment(toDate).toDate()

        if ($scope.source.fromDate > $scope.source.toDate) {
            $scope.source.toDate.setDate($scope.source.fromDate.getDate() + 6)
            if ($scope.source.toDate > new Date()) {
                $scope.source.toDate.setTime(new Date().getTime())
                $scope.source.fromDate.setDate($scope.source.toDate.getDate() - 6)
            }
            $scope.source.toDate.setHours(23, 59, 59, 999)
        }

        $scope.toDateBoxOptions = <any>{
            acceptCustomValue: true,
            type: 'date',
            max: new Date(),
            displayFormat: i18n("Common", "DateFormatDX"),
            onInitialized: e => {
                $scope.toDateBox = e.component
                e.component.option("min", $scope.source.fromDate)
            },
            dateOutOfRangeMessage: i18n('Common', 'ValueIsOutOfRange'),
            onValueChanged: object => {
                var toDate = moment(object.value).toDate()
                toDate.setHours(23, 59, 59, 999)
                $scope.source.toDate = toDate
                if ($scope.selectedPeriod != periods.custom) {
                    var fromDate = moment(toDate).toDate()
                    switch ($scope.selectedPeriod) {
                        case periods.week:
                            fromDate.setDate(fromDate.getDate() - 6)
                            break;
                        case periods.month:
                            fromDate.setMonth(fromDate.getMonth() - 1)
                            break;
                        case periods.year:
                            fromDate.setFullYear(fromDate.getFullYear() - 1)
                            break;
                    }
                    fromDate.setHours(0, 0, 0, 0)
                    $scope.source.fromDate = fromDate
                    $scope.fromDateBox.option("value", fromDate)
                }
                $scope.source.callback(getPeriod($scope.selectedPeriod))
                UserConfigHelper.setUserConfig(UserConfigHelper.ToDate, $scope.source.toDate)
            },
            value: $scope.source.toDate
        }

        $scope.prevButtonOptions = {
            template: CommonHelper.getIcon('PREV'),
            hint: i18n("Grid", "PreviousTimePeriod"),
            onClick: e => {
                var fromDate = $scope.source.fromDate
                if (!periods.custom)
                    var toDate = $scope.source.toDate = new Date(fromDate.valueOf())
                else
                    var toDate = $scope.source.toDate
                slidePeriod(fromDate, toDate, -1)
            }
        }

        $scope.nextButtonOptions = {
            template: CommonHelper.getIcon('NEXT'),
            hint: i18n("Grid", "NextTimePeriod"),
            onClick: e => {
                var fromDate = $scope.source.fromDate
                var toDate = $scope.source.toDate
                slidePeriod(fromDate, toDate, 1)
            }
        }

        var togglePopover = () => {
            $scope.periodPopoverVisible = !$scope.periodPopoverVisible
        }

        $scope.periodButtonOptions = {
            template: CommonHelper.getIcon('DATE_RANGE'),
            hint: i18n("Grid", "Period"),
            onClick: () => {
                $scope.periodPopover.option("target", '#period-button-' + $scope.$id)
                $scope.periodPopover.option("position", "bottom")
                togglePopover()
            }
        }

        $scope.smScreenPeriodButtonOptions = {
            text: i18n("Common", "ChooseRange"),
            onClick: () => {
                $scope.periodPopover.option("target", '#sm-screen-period-button-' + $scope.$id)
                $scope.periodPopover.option("position","left")
                togglePopover()
            }
        }

        $scope.periodPopoverVisible = false


        $scope.periodPopoverOptions = {
            target: '#period-button-' + $scope.$id,
            width: 100,
            bindingOptions: {
                visible: 'periodPopoverVisible'
            },
            onInitialized: e => {
                $scope.periodPopover = e.component
            }, 
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            }
        }

        var getPeriod = (selectedPeriodName) => {
            for (let key in periods) {
                if (periods[key] == selectedPeriodName) return {
                    id: key, title: selectedPeriodName
                }
            }
            return {
                id: "day",
                title: periods.day
            }
        }

        if ($scope.source.customPeriods)
            $scope.source.customPeriods.forEach((option, index) => {
                periods[option.id] = option.title
                periodItems.splice(index, 0, option.title)
            })

        $scope.selectedPeriod = periods.custom
        var selectedPeriodIndex = periodItems.indexOf($scope.selectedPeriod)

        $scope.periodListOptions = {
            items: periodItems.map(o => { return { text: o } }),
            onItemClick: e => {
                $scope.periodPopoverVisible = false
                e.component.option("items")[selectedPeriodIndex].disabled = false
                e.itemData.disabled = true
                $scope.selectedPeriod = e.itemData.text
                selectedPeriodIndex = e.itemIndex

                if ($scope.selectedPeriod != periods.custom) {
                    if (selectedPeriodIndex < $scope.source.customPeriods.length) {
                        $scope.hidePickers = true
                        $scope.source.callback($scope.source.customPeriods[selectedPeriodIndex])
                        return
                    } else {
                        $scope.source.fromDate = moment($scope.source.fromDate).toDate()
                        $scope.source.fromDate.setHours(0, 0, 0, 0)
                        var fromDate = $scope.source.fromDate
                        var toDate = $scope.source.toDate = moment($scope.source.toDate).toDate()
                        switch ($scope.selectedPeriod) {
                            case periods.day:
                                toDate.setDate(fromDate.getDate())
                                break;
                            case periods.week:
                                fromDate.setDate(toDate.getDate() - 6)
                                break;
                            case periods.month:
                                fromDate.setMonth(toDate.getMonth() - 1)
                                break;
                            case periods.year:
                                fromDate.setFullYear(toDate.getFullYear() - 1)
                                break;
                        }
                        toDate = moment(toDate).toDate()
                        toDate.setHours(23, 59, 59, 999)
                        $scope.fromDateBox.option("value", fromDate)
                        $scope.toDateBox.option("value", toDate)
                    }
                }
                $scope.hidePickers = false
                $scope.source.callback(getPeriod($scope.selectedPeriod))
            },
            onItemRendered: e => {
                if (e.itemData.text == $scope.selectedPeriod)
                    e.itemData.disabled = true
            }
        }

        var slidePeriod = (fromDate, toDate, multiply: number) => {
            switch ($scope.selectedPeriod) {
                case periods.day:
                    fromDate.setDate(fromDate.getDate() + 1 * multiply)
                    toDate = $scope.source.toDate = new Date(fromDate.valueOf())
                    toDate.setDate(fromDate.getDate())
                    break;
                case periods.week:
                    fromDate.setDate(fromDate.getDate() + 6 * multiply)
                    toDate = $scope.source.toDate = new Date(fromDate.valueOf())
                    toDate.setDate(fromDate.getDate() + 6)
                    break;
                case periods.month:
                    fromDate.setMonth(fromDate.getMonth() + 1 * multiply)
                    toDate = $scope.source.toDate = new Date(fromDate.valueOf())
                    toDate.setMonth(fromDate.getMonth() + 1)
                    break;
                case periods.year:
                    fromDate.setFullYear(fromDate.getFullYear() + 1 * multiply)
                    toDate = $scope.source.toDate = new Date(fromDate.valueOf())
                    toDate.setFullYear(fromDate.getFullYear() + 1)
                    break;
                case periods.custom:
                    var diff = toDate.getTime() - fromDate.getTime()
                    if (diff) {
                        fromDate.setTime(fromDate.getTime() + (diff * multiply))
                        toDate.setTime(toDate.getTime() + (diff * multiply))
                    }
            }

            toDate = moment(toDate).toDate()
            toDate.setHours(23, 59, 59, 999)

            $scope.fromDateBox.option("value", fromDate)
            $scope.toDateBox.option("min", new Date(fromDate.valueOf()))
            $scope.toDateBox.option("value", toDate)

            $timeout(() => {
                $scope.fromDateBox.repaint()
                $scope.toDateBox.repaint()
            }, 0)
            $scope.source.callback(getPeriod($scope.selectedPeriod))
        }
    }]

    template = require("./date-picker.tpl.html")
}