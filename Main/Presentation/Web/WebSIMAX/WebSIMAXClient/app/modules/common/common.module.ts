﻿import 'angular'

import { LayoutDirective } from './layout/common.layout.directive'
import { HeightFillDirective } from './common.height-fill.directive'
import { GridHeightFillDirective } from './common.grid-height-fill.directive'
import { HeaderDirective } from './header/common.header.directive'
import { LoadingIndicatorDirective } from './loading-indicator/common.loading-indicator.directive'
import { SidebarMenuDirective } from './sidebar-menu/common.sidebar-menu.directive'
import { LoggerService } from './common.logger.service'
import { CreateEditPopupDirective } from './create-edit-popup/create-edit-popup.directive'
import { LocationSelectDirective } from './location-select/location-select.directive'
import { ConfirmationDirective } from './confirmation-popup/confirmation-popup.directive'
import { GridSelectDirective } from './grid-select/grid-select.directive'
import { GridButtonDirective } from './grid-button/grid-button.directive'
import { MeasurementsDirective } from './measurements/measurements.directive'
import { MeasurementsChartDirective } from './measurements/chart/chart.directive'
import { ConnectedObjectDirective } from './connected-object/connected-object.directive'
import { HistoryDirective } from './history/history.directive'
import { I18NProvider } from './i18n/common.i18n.provider'
import { GridEditDirective } from './grid-edit/grid-edit.directive'
import { GridSlotEditDirective } from './grid-slot-edit/grid-slot-edit.directive'
import { DetailsPopupDirective } from './details-popup/details-popup.directive'
import { DatePickerDirective } from './date-picker/date-picker.directive'
import { DetailsParametersDirective } from './details-parameters/details-parameters.directive'
import { ToolbarPopoverDirective } from './toolbar-popover/toolbar-popover.directive'
import { UserMenuDirective } from './user-menu/user-menu.directive'
import { AlarmsMenuDirective} from './alarms-menu/alarms-menu.directive'
import { GridAttachmentsEditDirective } from './grid-attachments-edit/grid-attachments-edit.directive'
import { NotificationCardDirective } from './notification-card/notification-card.directive'
import {ChangePasswordDirective} from './change-password/change-password.directive'
import { ImportDirective } from './import/import.directive'
import { GridDetailsDirective } from './grid-details/grid-details.directive'
import { SchemaDirective } from './schema/schema.directive'
import { GalleryDirective } from './gallery/gallery.directive'
import { MapDirective }  from './map/map.directive'
import { FileUploaderDirective } from './file-uploader/file-uploader.directive'
import { CustomFilterDirective } from './custom-filter/custom-filter.directive'

var moduleName = 'simax.common'
var module = angular.module(moduleName, [])

// directives
module.directive('layout', [() => new LayoutDirective()])
module.directive('heightFill', ['$window', ($window: ng.IWindowService) => new HeightFillDirective($window)])
module.directive('gridHeightFill', ['$timeout', ($timeout: ng.ITimeoutService) => new GridHeightFillDirective($timeout)])
module.directive('header', [() => new HeaderDirective()])
module.directive('loadingIndicator', ['$http', ($http: ng.IHttpService) => new LoadingIndicatorDirective($http)])
module.directive('sidebarMenu', [() => new SidebarMenuDirective()])
module.directive('createEditPopup', [() => new CreateEditPopupDirective()])
module.directive('locationSelect', [() => new LocationSelectDirective()])
module.directive('confirmation', [() => new ConfirmationDirective()])
module.directive('gridSelect', [() => new GridSelectDirective()])
module.directive('gridButton', [() => new GridButtonDirective()])
module.directive('measurements', [() => new MeasurementsDirective()])
module.directive('measurementsChart', [() => new MeasurementsChartDirective()])
module.directive('connectedObject', [() => new ConnectedObjectDirective()])
module.directive('history', [() => new HistoryDirective()])
module.directive('gridEdit', [() => new GridEditDirective()])
module.directive('gridSlotEdit', [() => new GridSlotEditDirective()])
module.directive('detailsPopup', [() => new DetailsPopupDirective()])
module.directive('datePicker', [() => new DatePickerDirective()])
module.directive('detailsParameters', [() => new DetailsParametersDirective()])
module.directive('toolbarPopover', [() => new ToolbarPopoverDirective()])
module.directive('userMenu', [() => new UserMenuDirective()])
module.directive('alarmsMenu', [() => new AlarmsMenuDirective()])
module.directive('gridAttachmentsEdit', [() => new GridAttachmentsEditDirective()])
module.directive('notificationCard', [() => new NotificationCardDirective()])
module.directive('changePassword', [() => new ChangePasswordDirective()])
module.directive('import', [() => new ImportDirective()])
module.directive('gridDetails', [() => new GridDetailsDirective()])
module.directive('schema', [() => new SchemaDirective()])
module.directive('gallery', [() => new GalleryDirective()])
module.directive('map', [() => new MapDirective()])
module.directive('fileUploader', [() => new FileUploaderDirective()])
module.directive('customFilter', [() => new CustomFilterDirective()])

// services
module.service('logger', [() => new LoggerService()])

// providers
module.provider('i18n', [() => new I18NProvider()])

export default moduleName
export { LoggerService }