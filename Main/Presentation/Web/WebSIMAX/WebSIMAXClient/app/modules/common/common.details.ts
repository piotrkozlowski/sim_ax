﻿import { i18n } from './i18n/common.i18n'
import { GridData, TabsData } from './common.interfaces'
import { UserConfigHelper } from '../../helpers/user-config-helper'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { Api, LocalDictionaryFactory } from  '../api/api.module'
import { LoggerService } from './common.logger.service'
import { ColumnModel, FieldModel } from '../api/api.models'
import { PopupManager } from '../../helpers/popup-manager'
import { CommonHelper } from '../../helpers/common-helper'
import * as moment from 'moment'
import { CommonScope } from './common.extended-data-grid-options'
import { DetailsPopupOptions } from './details-popup/details-popup.directive'

export interface DetailsAccordionSection {
    title: string
    canEdit: boolean
    name: string
    status?: () => string
    showStatus: boolean
    editMethod?: () => void
    visible: boolean
    selected?: boolean
    onHidden?: () => void
}

export class DetailsAccordion {
    private options: DevExpress.ui.dxAccordion
    private instance: DevExpress.ui.dxAccordion
    private selectedItems: any[] = []

    private accordionSettingForHidden: string
    private height: string = '0'
    private updateItemsTimeout

    public needRefresh: (() => void) = null
    public visible: boolean
    public hideContent: boolean = false

    constructor(private $scope, private $timeout: ng.ITimeoutService, public accordionId: string, position: number, public items: DetailsAccordionSection[], private multiple: boolean = false, onInitialized: ((component: DevExpress.ui.dxAccordion) => void) = null) {
        var accordionSetting = accordionId + position
        var selectedIndexes = UserConfigHelper.getUserConfig(accordionSetting, [0])
        if (!selectedIndexes.indexOf) selectedIndexes = [0]
        this.accordionSettingForHidden = accordionSetting + 'hidden'

        var hiddenItems: string[] = UserConfigHelper.getUserConfig(this.accordionSettingForHidden, [])
        var visibleItems = items.filter((item, index) => {
            if (hiddenItems.indexOf(item.name) == -1) {
                item.visible = true
                if (selectedIndexes.indexOf(index) > -1) {
                    item.selected = true
                    this.selectedItems.push(item)
                }
                return true
            }
            item.visible = false
            if (item.onHidden) item.onHidden()
            return false
        })

        if (visibleItems.length == 1) {
            visibleItems[0].selected = true
        }

        this.options = <any>{
            dataSource: visibleItems,
            selectedItems: this.selectedItems,
            itemTemplate: 'panel',
            multiple: multiple,
            height: () => {
                return angular.element(window).width() >= 768 ? '100%' : (400 * this.items.filter(item => item.visible).length) + 'px'
            },
            animationDuration: 300,
            onSelectionChanged: e => {
                var addedItems: DetailsAccordionSection[] = e.addedItems
                addedItems.forEach(item => {
                    item.selected = true
                })
                var removedItems: DetailsAccordionSection[] = e.removedItems
                removedItems.forEach(item => {
                    if (item) {
                        item.selected = false
                    }
                })

                var selectedIndex = this.items.map((item, index) => { return { selected: item.selected, index: index } }).filter(i => i.selected).map(i => i.index)
                this.selectedItems = this.items.filter(i => i.selected)
                UserConfigHelper.setUserConfig(accordionSetting, selectedIndex)

                this.hideContent = true
                $timeout(() => {
                    this.hideContent = false
                }, 500)
            },
            onInitialized: e => {
                this.instance = e.component
                if (onInitialized) onInitialized(e.component)
                this.selectedItems = this.items.filter(i => i.selected)
                $timeout(() => {
                    this.instance.option('selectedItems', this.selectedItems)
                }, 500)
            }
        }

        this.visible = visibleItems.length > 0
    }

    private hidePromise: JQueryPromise<void>
    private hideSync(name: string) {
        var hideDeferred = $.Deferred<void>()
        if (!this.hidePromise) {
            this.hidePromise = hideDeferred.promise().then(() => {
                this.hidePromise = null
            })
        }

        var itemPromise: JQueryPromise<void> = null
        var items = this.items.filter((item, index) => {
            if (name == item.name) {
                if (this.multiple) {
                    if (item.selected) {
                        this.items.every((item2, i) => {
                            if (item2.visible && !item2.selected) {
                                itemPromise = this.instance.expandItem(i).then(() => {
                                    item2.selected = true
                                })
                                return false
                            }
                            return true
                        })

                        var collapse = () => {
                            if (this.items.filter(i => i.visible).length == 1) {
                                this.hideItem(item)
                                return null
                            } else {
                                return this.instance.collapseItem(index).then(() => {
                                    this.hideItem(item)
                                })
                            }
                        }

                        if (itemPromise) {
                            itemPromise = itemPromise.then(() => {
                                return collapse()
                            })
                        } else {
                            itemPromise = collapse()
                        }

                        return false
                    }
                }
                this.hideItem(item)
            }
            return item.visible
        })

        var up = () => {
            if (items.length == 0) {
                if (this.visible && this.needRefresh) this.needRefresh()
                this.visible = false
                this.options['dataSource'] = []
                this.instance.option({ 'dataSource': [] })
                this.items.forEach((v, i) => {
                    if (i == 0) {
                        this.instance.expandItem(i)
                    } else {
                        this.instance.collapseItem(i)
                    }
                })
                this.save()
            }
            else this.updateItems(items)
            hideDeferred.resolve()
        }

        if (itemPromise) {
            itemPromise.then(() => {
                up()
            })
        } else {
            up()
        }
    }

    public hide(name: string) {
        if (this.hidePromise) {
            this.hidePromise.then(() => {
                this.hideSync(name)
            })
        } else {
            this.hideSync(name)
        }
    }

    private hideItem(item: DetailsAccordionSection) {
        if (item.visible && item.onHidden) {
            item.onHidden()
        }
        item.visible = false
        item.selected = false
    }

    public show(name: string) {
        var items = this.items.filter(item => {
            if (name == item.name) {
                item.visible = true
                if (this.items.filter(i => i.selected).length == 0) {
                    item.selected = true
                }
            }
            return item.visible
        })
        if (!this.visible && this.needRefresh) this.needRefresh()
        this.visible = true
        this.updateItems(items)
    }

    public update(names: string[]) {
        this.items.forEach(item => {
            if (names.indexOf(item.name) > -1) {
                if (!item.visible) this.show(item.name)
            } else {
                if (item.visible) this.hide(item.name)
            }
        })
    }

    private itemsNew(items: DetailsAccordionSection[]) {
        var lastItems: DetailsAccordionSection[] = this.options['dataSource']
        if (lastItems.length != items.length) return true
        return !items.every(item => {
            return !lastItems.every(lastItem => !(item.name == lastItem.name))
        })
    }

    private updateItems(items: DetailsAccordionSection[]) {
        if (this.updateItemsTimeout) clearTimeout(this.updateItemsTimeout)
        this.updateItemsTimeout = setTimeout(() => {
            if (this.itemsNew(items)) {
                this.options['dataSource'] = items
                if (this.instance) {
                    var selectedIndex = this.instance.option('selectedIndex')
                    if (selectedIndex >= items.length) selectedIndex = items.length - 1
                    else if (selectedIndex <= -1) selectedIndex = 0

                    var selectedItems = this.items.filter(item => {
                        if (item.visible) item.selected = true
                        return item.visible
                    })
                    try {
                        this.instance.option({
                            'dataSource': items,
                            'selectedItems': selectedItems
                        })
                    } catch (e) { }
                }
                this.save()
            }
        }, 200)
    }

    private save() {
        UserConfigHelper.setUserConfig(this.accordionSettingForHidden, this.items.filter(item => !item.visible).map(item => item.name))
    }
}
 
export interface DetailsScope extends CommonScope {
    accordionHeight: string

    modelId: number
    itemObjects: any[]
    popupFor: string
    options: DetailsPopupOptions

    visiblePopup: boolean
    popup: DevExpress.ui.dxPopup
    popupTitle: string

    editClick(e)
    detailsPopupId: number
    i18n: any
    tabs: TabsData

    accordions: DetailsAccordion[]
    accordionsButtonVisible: boolean
    anyAccordionVisible: () => void

    openDeleteConfirmationPopup()
    requestsStates: any
    closeAccordionButtonOptions(name: string, column: number)

    isLoading: boolean
}

export abstract class Details<T extends DetailsScope> implements ng.IDirective {
    private popupManager = PopupManager.getInstance()
    private tabsPanel: DevExpress.ui.dxTabPanel
    private tabs: { template: string, name: string }[]
    protected accordionsNameToShow: string
    protected mapWidget: DevExpress.ui.dxMap
    protected mapWidgetDeferred: JQueryDeferred<DevExpress.ui.dxMap>

    constructor(protected $scope: T, protected $timeout: ng.ITimeoutService, protected $q: ng.IQService, protected logger: LoggerService, protected localDictionary: LocalDictionaryFactory, private tabsId: string) {
        $scope.options = <DetailsPopupOptions>{}
        $scope.visiblePopup = false
        $scope.closeAccordionButtonOptions = (name: string, column: number) => {
            return {
                template: CommonHelper.getIcon('CLOSE'),
                onClick: (e) => {
                    e.jQueryEvent.stopPropagation()
                    this.hideAccordion(this.$scope, name, column)
                }
            }
        }
        $scope.detailsPopupId = new Date().getTime()
        $scope.i18n = i18n
        $scope.tabs = <TabsData>{}

        $scope.anyAccordionVisible = () => {
            return $scope.accordions.filter(accordion => accordion.visible).length != 0
        }

        $scope.tabs.selectedTab = this.getSelectedTabFromConfig(tabsId)
        if (!$scope.tabs.selectedTab) {
            $scope.tabs.selectedTab = 0
        }

        // accordion edit button
        $scope.editClick = e => {
            if (e.model.section.editMethod)
                e.model.section.editMethod()
            e.jQueryEvent.stopPropagation()
        }

        $scope.$watch('modelId', val => {
            $scope.options.modelId = val
        })

        $scope.$watchCollection('itemObjects', val => {
            $scope.options.items = <any>val
        })

        $scope.$watch('popupTitle', val => {
            $scope.options.contentTitle = <string>val
        })

        $scope.$watch('visiblePopup', val => {
            $scope.options.visiblePopup = <boolean>val
        })

        $scope.$watch(() => {
            return angular.element(window).width() < 768
        }, (isMobile) => {
            this.$scope.accordions = []
            $timeout(() => {
                var accordions = this.getAccordions()
                if (isMobile && accordions.length > 0) {
                    var items = []
                    accordions.forEach(accordion => {
                        items.push(...accordion.items)
                    })

                    var accordion = new DetailsAccordion($scope, $timeout, accordions[0].accordionId + 'mobile', 0, items, true)
                    accordions = [accordion]
                }
                this.$scope.accordions = accordions
            })
        })

        $scope.$on(EventsNames.CLOSE_DETAILS_POPUP, () => {
            this.hidePopup()
        })
    }

    getAccordionData() {
        return [].concat.apply([], this.$scope.accordions.map(accordion => accordion.items.map(item => { return { name: item.title, value: item.name } })))
    }

    getAccordionSelection() {
        return [].concat.apply([], this.$scope.accordions.map(accordion => accordion.items.filter(item => item.visible).map(item => item.name)))
    }

    updateAccordions(names: any[]) {
        this.$scope.accordions.forEach(accordion => {
            accordion.update(names)
        })
    }

    showPopup() {
        this.$scope.visiblePopup = true
        this.$scope.$emit(EventsNames.OPENED_POPUP)
    }

    hidePopup() {
        this.$scope.visiblePopup = false
        this.$scope.$emit(EventsNames.CLOSED_POPUP)
    }

    getSelectedTabName() {
        return this.tabs[this.$scope.tabs.selectedTab] ? this.tabs[this.$scope.tabs.selectedTab].name : ''
    }

    configureTabs(tabs: { template: string, name: string }[]) {
        if (this.$scope.tabs.selectedTab >= tabs.length) this.$scope.tabs.selectedTab = tabs.length - 1
        this.tabs = tabs
        this.$scope.tabs.selectedTabName = this.getSelectedTabName()
        this.$scope.tabs.visible = this.tabs.length > 1
        this.$scope.tabs.tabsOptions = {
            dataSource: tabs,
            bindingOptions: {
                selectedIndex: 'tabs.selectedTab',
                visible: 'tabs.visible'
            },
            onSelectionChanged: (e) => {
                this.$scope.isLoading = true
                this.$timeout(() => {
                    UserConfigHelper.setUserConfig(this.tabsId, this.$scope.tabs.selectedTab)
                    this.$scope.tabs.selectedTabName = this.getSelectedTabName()

                    if (this.accordionsNameToShow && this.$scope.tabs.selectedTabName) {
                        this.$scope.accordionsButtonVisible = this.accordionsNameToShow == this.$scope.tabs.selectedTabName
                    }
                    else {
                        this.$scope.accordionsButtonVisible = false
                    }
                    this.$scope.isLoading = false
                }, 100)

                e.component.repaint()
            },
            onInitialized: e => {
                this.tabsPanel = e.component
            }
        }
    }

    updateTabsPanel(tabs: { template: string, name: string }[]) {
        this.tabs = tabs
        if (this.tabsPanel) {
            var lastSelectedItem = this.tabsPanel.option('selectedIndex')
            this.tabsPanel.option('dataSource', tabs)
            this.tabsPanel.repaint()
            this.tabsPanel.selectItem(lastSelectedItem)
        }
    }

    hideAccordion($scope: DetailsScope, name: string, column: number) {
        // fix problem with map widget
        if (name == 'maps') this.mapWidget = null
        // end of fix

        var detailsAccordion = this.$scope.accordions[column]
        if (detailsAccordion) detailsAccordion.hide(name)
    }

    showAccordion($scope: DetailsScope, name: string, column: number) {
        var detailsAccordion = this.$scope.accordions[column]
        if (detailsAccordion) detailsAccordion.show(name)
    }

    getSelectedTabFromConfig(tabName: string) {
        return UserConfigHelper.getUserConfig(tabName, 0)
    }

    updateGridEntities(gridName: string) {
        if (this.$scope[gridName].grid) {
            this.$scope[gridName].showGrid = false
            var store = this.$scope[gridName].grid.option("dataSource").store()
            store.clear()
            this.$scope['common'][gridName].forEach(element => {
                store.insert(element)
            })
            this.$scope[gridName].grid.refresh()
            
            this.$scope[gridName].showGrid = true
        }
    }

    createEventEditForGridEntities(eventName: string, gridName: string) {
        this.$scope.$on(eventName, (...args: any[]) => {
            this.updateGridEntities(gridName)
        })
    }

    loadEntityData(columnsName: string, openEventName: string, gridDataName: string, additionalColumns?: ColumnModel[]) {
        var deferred = this.$q.defer()
        this.$scope[gridDataName].showGrid = false
        this.localDictionary[columnsName]().then(defaultColumns => {
            this.$scope[gridDataName].columnsCount = defaultColumns.length

            var allColumns = []
            defaultColumns.forEach(col => {
                allColumns.push(col)
            })

            if (additionalColumns) {
                additionalColumns.forEach(col => {
                    if (col.position) allColumns.splice(col.position, 0, col)
                    else allColumns.push(col)
                })
            }
            this.$scope.showDetails = openEventName == null ? null : (e) => {
                var object = {
                    id: e.data[CommonHelper.idField]
                }

                this.$scope.$broadcast(openEventName + this.$scope['detailsPopupId'], e.data[CommonHelper.idField], [object])
            }
            var customGridOptions = {
                name: gridDataName,
                onInitialized: e => {
                    this.$scope[gridDataName].grid = e.component
                    deferred.resolve()
                },
                columnsDictionary: allColumns
            }
            this.$scope[gridDataName].options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, this.$scope, this.logger, null, customGridOptions)
            if (this.$scope[gridDataName].grid) {
                this.$scope[gridDataName].grid.option(this.$scope[gridDataName].options)
                deferred.resolve()
            }
        })
        return deferred.promise
    }

    insertEntityData(data: any[], columnsName: string, openEventName: string, gridDataName: string, additionalColumns?: ColumnModel[]) {
        return this.loadEntityData(columnsName, openEventName, gridDataName, additionalColumns).then(() => {
            data.forEach(d => {
                this.$scope[gridDataName].options.dataSource.store().insert(d)
            })
            this.$scope[gridDataName].grid.option(this.$scope[gridDataName].options)
            this.$scope[gridDataName].grid.refresh()
            this.$scope[gridDataName].showGrid = true
        })
    }

    static filterFields(fields: FieldModel[], displayFields: ColumnModel[]) {
        var existingFieldIds = fields.map((field) => field.id)
        var result: FieldModel[] = []
        displayFields.forEach(fieldModel => {
            var index = existingFieldIds.indexOf(fieldModel.id)
            var filteredField: ColumnModel
            if (index > -1) {
                fields[index].typeId = fieldModel.type
                fields[index].type = fieldModel.typeName
                fields[index].caption = fieldModel.caption
                fields[index].unitName = fieldModel.unitName
                result.push(fields[index])
            }
        })
        return result
    }

    mapUpdateTimeout
    protected mapUpdate() {
        if (this.mapUpdateTimeout) clearTimeout(this.mapUpdateTimeout)
        this.mapUpdateTimeout = setTimeout(() => {
            this.$scope.$broadcast(EventsNames.REFRESH_MAP_SIZE)
        }, 500)
    }

    abstract resetData()
    abstract loadData(id: number, objects: any[])
    abstract updateData(result: any)
    abstract getTabs()
    abstract getAccordions(): DetailsAccordion[]
}