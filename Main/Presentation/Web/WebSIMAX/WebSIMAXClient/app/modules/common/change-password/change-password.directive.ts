﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { PasswordResetModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { PopupType } from './../../../helpers/popup-manager'
import { PasswordResetResponseModel } from './../../api/api.models'

require('./change-password.scss')

interface ChangePasswordScope extends ng.IScope {
    id: number

    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean    
        
    formOptions: DevExpress.ui.dxFormOptions
    formItems: any
    formData: any
    formInstance: any

    cancelBtnOptions: DevExpress.ui.dxButtonOptions
    saveBtnOptions: DevExpress.ui.dxButtonOptions

    showError: any
}

export class ChangePasswordDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = true

    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: ChangePasswordScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {
        $scope.popupVisible = false  
        $scope.formInstance = {}      

        $scope.popupOptions = {
            bindingOptions: {
                visible: 'popupVisible',
                title: 'popupOptions.title'
            },
            maxWidth: 400,
            height: 'auto',
            showTitle: true,
            closeOnBackButton: true,
            closeOnOutsideClick: true,
            onHidden: () => { onPopupHidden() }            
        }

        var onPopupHidden = () => {
            $scope.formInstance.resetValues()
            $scope.showError = null
        }
       
        $scope.cancelBtnOptions = {
            text: i18n('Common', 'Cancel'),
            onClick: () => { $scope.popupVisible = false },
            height: 36
        }

        $scope.saveBtnOptions = {
            text: i18n('Common', 'Apply'),
            type: 'submit',
            onClick: () => {                
                submitPasswordChange($scope.formData)                
            },
            height: 36
        }

        $scope.$on(EventsNames.OPEN_PASSWORD_CHANGE_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {            

            $scope.popupVisible = true 
            $scope.popupOptions.title = i18n('Popup', 'ChangePassword')

            $scope.formItems = [                
                {
                    dataField: "OldPassword",
                    label: {
                        text: i18n('Common', 'OldPassword')
                    },
                    isRequired: true,
                    editorOptions: {
                        mode: 'password',
                    },
                    validationRules: [
                        requiredField()
                    ]
                },
                {
                    dataField: "NewPassword",
                    label: {
                        text: i18n('Common', 'NewPassword')
                    },
                    isRequired: true,
                    editorOptions: {
                        mode: 'password',
                    },
                    validationRules: [
                        requiredField()                    
                    ]
                },
                {
                    dataField: "ConfirmNewPassword",
                    label: {
                        text: i18n('Common', 'ConfirmNewPassword')
                    },
                    isRequired: true,
                    editorOptions: {
                        mode: 'password',
                    },
                    validationRules: [
                        requiredField(),
                        compareField()
                    ]
                }                
            ] 
                        
            $scope.formData = []            

            if (!$scope.formOptions) {
                $scope.formOptions = {
                    formData: $scope.formData,
                    items: $scope.formItems,
                    onInitialized: initializedHandler,
                    onOptionChanged: optionChangedHandler
                }
            }
            else {
                $scope.formInstance.option('formData', $scope.formData)
            }
        })        

        var comparisonTargetNew = () => {
            if ($scope.formData['NewPassword']) {
                return $scope.formData['NewPassword']
            }
        }

        var initializedHandler = (e) => {
            $scope.formInstance = e.component
        }

        var optionChangedHandler = (e) => {
            $scope.formInstance = e.component
            $scope.formInstance.repaint()
        }

        var submitPasswordChange = (fields) => {
            var result = $scope.formInstance.validate();
            if (result.isValid) { 
                var oldPassword = fields['OldPassword']
                var newPassword = fields['NewPassword']
                var confirmNewPassword = fields['ConfirmNewPassword']

                if (newPassword === confirmNewPassword) {
                    //send oldPass and newPass to service
                    var model = new PasswordResetModel()
                    model.password = newPassword
                    model.oldPassword = oldPassword
                    api.users.changePassword(model).then(request => {
                        if (request.status != 'OK') {                         
                            $scope.showError = request.message
                        }
                        else if (request.status == 'OK' && (!request.hasErrorCode || request.hasErrorCode <= 0)) {
                            CommonHelper.openInfoPopup(request.message)
                            $scope.popupVisible = false                            
                        }
                    }).catch(err => {
                        console.log(err)
                    })                    
                }                
            }
        }

        var requiredField = () => {
            return {
                type: "required",
                message: i18n('Common', 'RequiredField')
            }
        }

        var minLenghtField = () => {
            return {
                type: 'stringLength',
                min: 8,
                message: i18n('Common', 'Require8symbols')
            }
        }

        var compareField = () => {
            return {
                type: "compare",
                message: i18n('Common', 'PasswordsDontMatch'),
                comparisonTarget: comparisonTargetNew
            }
        }
    }]
    template = require("./change-password.tpl.html")
    transclude = true
}