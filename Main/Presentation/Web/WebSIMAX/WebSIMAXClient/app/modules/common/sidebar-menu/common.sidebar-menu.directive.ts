﻿import 'angular'

import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from  '../../../helpers/common-helper'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { Api } from  '../../api/api.module'

require('./sidebar-menu.scss')

window['MenuItems'] = []

export class MenuItem {
    constructor(public icon: string, public name: string, public resource: string, public state: string, public order: number) {
    }

    static addMenuItem(icon: string, name: string, resource: string, state: string, order: number) {
        window['MenuItems'].push(new MenuItem(icon, name, resource, state, order))
    }
}

export class SidebarMenuDirective implements ng.IDirective {
    constructor() {}

    scope = {
        active: '@active',
        small: '='
    }

    restrict = 'E'

    controller = ['$scope', '$timeout', '$state', 'api', ($scope: ng.IScope, $timeout: ng.ITimeoutService, $state: angular.ui.IStateService, api: Api) => {
        $scope.$watch(() => {
            return window['MenuItems']
        }, (items: MenuItem[]) => {
            $scope['items'] = items.sort((a, b) => {
                return ((a.order < b.order) ? -1 : ((a.order > b.order) ? 1 : 0));
            })
            $scope['i18n'] = i18n
            if (window['config']['RTLSTATE'])
                $scope['arrowIcon'] = 'icon-arrow-right'
            else
                $scope['arrowIcon'] = 'icon-arrow-left'
            })

        var goTimeout: ng.IPromise<void>
        $scope['goToPage'] = (item) => {
            if (item.name != $scope['active']) {
                $scope['active'] = item.name
                $scope.$root.$broadcast('moduleChanging')
                if (goTimeout) $timeout.cancel(goTimeout)
                goTimeout = $timeout(() => {
                    $scope.$root.$broadcast('disableLoadingIndicator')
                    UserConfigHelper.saveSettingsToDB(api, 0).then(() => {
                        $state.go(item.state).then(() => {
                            $scope.$root.$broadcast('enableLoadingIndicator')
                        })
                    })
                }, 400)
            }
        }
    }]

    template = require("./sidebar-menu.tpl.html")
    replace = true
}