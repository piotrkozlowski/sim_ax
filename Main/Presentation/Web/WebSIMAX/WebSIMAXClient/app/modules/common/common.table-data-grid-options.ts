﻿import { i18n } from './i18n/common.i18n'

export class CommonTableDataGridOptions implements DevExpress.ui.dxDataGridOptions {
    noDataText = i18n('Common', 'NoData')
    height = '100%'
    sorting: { mode: 'multiple' }
    pager = {
        showPageSizeSelector: false,
        allowedPageSizes: [10],
        showInfo: true,
        infoText: i18n('Grid', 'PagerTextShort'),
        visible: false
    }
    paging = {
        pageSize: 10,
        enabled: true
    }
}