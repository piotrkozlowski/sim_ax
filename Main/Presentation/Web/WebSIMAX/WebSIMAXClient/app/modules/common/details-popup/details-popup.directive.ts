﻿import 'angular'
import { LoggerService } from  '../common.module'
import { EventsNames } from '../../../helpers/events-names'
import { i18n } from '../i18n/common.i18n'
import { DetailsScope } from '../common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { PopupManager } from '../../../helpers/popup-manager'
import * as _ from 'underscore'

require('./details-popup.scss')

interface DetailsPopupScope extends DetailsScope {
    deleteMethod(): void
    hidePopup(clickedCloseButton: boolean): void
    popupName: string

    popupOptions: DevExpress.ui.dxPopupOptions
    closeButtonOptions: DevExpress.ui.dxButtonOptions
    prevButtonOptions: DevExpress.ui.dxButtonOptions
    nextButtonOptions: DevExpress.ui.dxButtonOptions
    deleteButtonOptions: DevExpress.ui.dxButtonOptions
    accordionsButtonOptions: DevExpress.ui.dxButtonOptions

    accordionPopoverOptions: DevExpress.ui.dxPopoverOptions
    accordionPopoverVisible: boolean
    accordionIsUnknownReferenceTable: boolean

    accordionGrid: DevExpress.ui.dxDataGrid
    accordionGridOptions: DevExpress.ui.dxDataGridOptions

    nextEntityId(): number
    prevEntityId(): number
    groupsEditSource: any

    getOptions(): DetailsPopupOptions

    actionPopoverVisible: boolean
    actionPopoverOptions: DevExpress.ui.dxPopoverOptions
    actionButtonVisible: boolean
    actionButtonOptions: DevExpress.ui.dxButtonOptions
    actions: DevExpress.ui.dxButtonOptions[]

    scrollViewOptions: DevExpress.ui.dxScrollViewOptions
}

export interface DetailsPopupOptions {
    id: number
    deleteMethod: any
    delHasPermissions: string
    title: string
    contentTitle: string
    openEventName: string
    closeEventName: string
    visiblePopup: boolean
    modelId: any
    items: any[]
    popupId: string
    getAccordionDataMethod: any
    updateAccordionMethod: any
    getAccordionSelectionMethod: any
    actions: DevExpress.ui.dxButtonOptions[]
}

export class DetailsPopupDirective implements ng.IDirective {

    restrict = 'E'

    controller = ['$scope', '$rootScope', '$attrs', '$q', '$timeout', 'logger', ($scope: DetailsPopupScope, $rootScope: ng.IRootScopeService, $attrs: ng.IAttributes, $q: ng.IQService, $timeout: ng.ITimeoutService, logger: LoggerService) => {
        $scope.requestsStates = {}
        var popupManager = PopupManager.getInstance()
        $scope.accordionsButtonVisible = true

        $scope.getOptions = () => {
            return $scope[$attrs['options']] ? $scope[$attrs['options']] : <DetailsPopupOptions>{}
        }

        $scope.scrollViewOptions = {
            disabled: false,
        }

        $scope.popupOptions = {
            fullScreen: false,
            height: () => {
                return 'calc(100% - 33px)'
            },
            width: '100%',
            showCloseButton: true,
            dragEnabled: false,
            bindingOptions: {
                visible: 'getOptions().visiblePopup'
            },
            onInitialized: (e) => {
                $scope.popup = e.component
            },
            onShown: (e) => {
                $scope.$emit(EventsNames.OPENED_POPUP)
                popupManager.add($scope.popup)
            },
            onHiding: (e) => {
                if ($scope.getOptions().closeEventName) {
                    $scope.$emit($scope.getOptions().closeEventName + $scope.getOptions().id.toString())
                }
            },
            onHidden: (e) => {
                $scope.$emit(EventsNames.CLOSED_POPUP)
                popupManager.deleteAndFocusLastPopup()
                _.mapObject($scope.requestsStates, (val, key) => {
                    if (val.request && !val.request.$resolved)
                        val.request.$cancelRequest()
                })
            },
            position: {
                at: 'top',
                my: 'top'
            }
        }

        $scope.nextEntityId = (): number => {
            if (!$scope.getOptions().items) return null
            var itemIds = $scope.getOptions().items.map(item => item.id)
            var index = itemIds.indexOf($scope.getOptions().modelId)

            popupManager.focusOnCurrent()

            return itemIds[index + 1] || null
        }

        $scope.prevEntityId = (): number => {
            if (!$scope.getOptions().items) return null
            var itemIds = $scope.getOptions().items.map(item => item.id)
            var index = itemIds.indexOf($scope.getOptions().modelId)

            popupManager.focusOnCurrent()

            return itemIds[index - 1] || null
        }

        var nextEntity = () => {
            $scope.$emit(EventsNames.CLOSED_POPUP)
            var nextId = $scope.nextEntityId()
            if (nextId)
                $scope.$root.$broadcast($scope.getOptions().openEventName + $scope.getOptions().id.toString(), nextId, $scope.getOptions().items)
        }

        var prevEntity = () => {
            $scope.$emit(EventsNames.CLOSED_POPUP)
            var prevId = $scope.prevEntityId()
            if (prevId)
                $scope.$root.$broadcast($scope.getOptions().openEventName + $scope.getOptions().id.toString(), prevId, $scope.getOptions().items)
        }

        var closeDetails = (clickedCloseButton) => {
            $scope.getOptions().visiblePopup = false

            if ($scope.getOptions().closeEventName) {
                $scope.$emit($scope.getOptions().closeEventName + $scope.getOptions().id.toString())
            }

            $scope.$broadcast(EventsNames.STOP_MEASUREMENTS_LOADING)
        }

        $scope.closeButtonOptions = {
            template: CommonHelper.getIcon('CLOSE'),
            onClick: closeDetails
        }

        $scope.prevButtonOptions = {
            template: CommonHelper.getIcon('PREV'),
            onClick: prevEntity,
            bindingOptions: {
                disabled: '!prevEntityId()'
            }
        }

        $scope.nextButtonOptions = {
            template: CommonHelper.getIcon('NEXT'),
            onClick: nextEntity,
            bindingOptions: {
                disabled: '!nextEntityId()'
            }
        }

        $scope.deleteButtonOptions = {
            template: CommonHelper.getIcon('DELETE'),
            onClick: $scope.getOptions().deleteMethod
        }

        $scope.accordionPopoverVisible = false
        $scope.accordionIsUnknownReferenceTable = true

        var accordionPopover: DevExpress.ui.dxPopover
        $scope.accordionsButtonOptions = {
            template: CommonHelper.getIcon('ACCORDION'),
            onInitialized: () => {
                if (!$scope.getOptions().getAccordionDataMethod) {
                    $scope.accordionsButtonVisible = false
                }
            },
            onClick: (e) => {               
                $scope.accordionPopoverVisible = !$scope.accordionPopoverVisible
                if ($scope.accordionPopoverVisible) {
                    accordionPopover.show()
                    getAccordionGridData()
                }
                else accordionPopover.hide()
            },
            hint: i18n('Popup', 'SelectedAccordions')
        }

        $scope.accordionPopoverOptions = {
            width: 300,
            onInitialized: (e) => {
                accordionPopover = e.component
            },
            target: '.edit-accordions-button',
            bindingOptions: {
                visible: 'accordionPopoverVisible'
            },
            onHiding: () => {
                $scope.accordionPopoverVisible = false
            },
            onHidden: () => {
                $scope.accordionIsUnknownReferenceTable = true
            }
        }

        var getAccordionGridData = () => {
            var columns = [
                {
                    dataField: "name",
                    caption: i18n('Common', 'Name'),
                    dataType: "string"
                }
            ]

            var data = $scope.getOptions().getAccordionDataMethod()
            var selection = $scope.getOptions().getAccordionSelectionMethod()

            var dataSourceOptions = {
                store: new DevExpress.data.ArrayStore({
                    data: data,
                    key: 'value'
                })
            }
            var dataSource = new DevExpress.data.DataSource(dataSourceOptions)

            var accorionOnSelectionChangedTimeout
            var customGridOptions = {
                onInitialized: (e) => {
                    $scope.accordionGrid = e.component
                },
                columns: columns,
                showColumnHeaders: false,
                showColumnLines: false,
                showRowLines: false,
                onSelectionChanged: (e) => {
                    if (accorionOnSelectionChangedTimeout) clearTimeout(accorionOnSelectionChangedTimeout)
                    accorionOnSelectionChangedTimeout = setTimeout(() => {
                        $scope.getOptions().updateAccordionMethod(e.selectedRowsData.map(val => val.value))
                    }, 500)
                },
                filterRow: {
                    visible: false
                },
                selectedRowKeys: selection,
                selection: { mode: 'multiple' },
                dataSource: dataSource
            }
            $scope.accordionGridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger, null, customGridOptions, false)
            $scope.accordionGridOptions.height = undefined

            if (data && data.length > 0) {
                $scope.accordionIsUnknownReferenceTable = false
            }
            else {
                $scope.accordionIsUnknownReferenceTable = true
            }
        }

        $scope.actions = $scope.getOptions().actions
        if ($scope.actions) {
            var actionPopover: DevExpress.ui.dxPopover
            $scope.actionPopoverOptions = {
                width: 200,
                onInitialized: (e) => {
                    actionPopover = e.component
                },
                target: '#detailsPopupActionsButton',
                bindingOptions: {
                    visible: 'actionPopoverVisible'
                },
                onHiding: () => {
                    $scope.actionPopoverVisible = false
                }
            }
            $scope.actionButtonOptions = {
                template: CommonHelper.getIcon('DETAILS_POPUP_ACTIONS_BUTTON'),
                onClick: (e) => {
                    $scope.actionPopoverVisible = !$scope.actionPopoverVisible
                    if ($scope.actionPopoverVisible) actionPopover.show()
                    else actionPopover.hide()
                }
            }
            
            $scope.actions.forEach(a => {
                // add popover hide
                a['actionOnClick'] = a.onClick
                a.onClick = () => {
                    a['actionOnClick']()
                    $scope.actionPopoverVisible = false
                    actionPopover.hide()
                }
            })
            $scope.actionButtonVisible = $scope.actions.length > 0
        }
    }]

    template = require("./details-popup.tpl.html")
    replace = true
    transclude = true
}