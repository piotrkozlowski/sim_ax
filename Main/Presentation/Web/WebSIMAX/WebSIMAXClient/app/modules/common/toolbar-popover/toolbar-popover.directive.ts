﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridSelectBridge } from '../../common/grid-select/grid-select.directive'
import { GridBridge } from '../../common/common.interfaces'
import { i18n } from '../i18n/common.i18n'

require('./toolbar-popover.scss')

interface ToolbarPopoverScope extends ng.IScope {
    source: GridBridge
    bridge: GridSelectBridge

    grid: DevExpress.ui.dxDataGrid
    addButtonOptions: DevExpress.ui.dxButtonOptions
    removeButtonOptions: DevExpress.ui.dxButtonOptions
    submitButtonOptions: DevExpress.ui.dxButtonOptions
    addButtonDisabled: boolean
    removeButtonDisabled: boolean
    submitButtonDisabled: boolean

    selectionButtonOptions: DevExpress.ui.dxButtonOptions
    selectionPopoverVisible: boolean
    selectionPopoverOptions: DevExpress.ui.dxPopoverOptions
    selectionListOptions: DevExpress.ui.dxListOptions
    selectionPopover: DevExpress.ui.dxPopover 

    added: any[]
    removed: any[]

    submitChanges(): void
}

export class ToolbarPopoverDirective implements ng.IDirective {
    constructor() { }

    scope = {
        source: '=',
        grid: '='
    }

    restrict = 'E'

    link = (scope, element, attrs) => {
        scope.$watch("source", value => {
            scope.source = value
        })
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', '$filter', ($scope: ToolbarPopoverScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, $filter) => {

        $scope.added = []
        $scope.removed = []

        $scope.bridge = {
            value: null,
            elements: [],

            data: $scope.source.data,
            onSelect: (e) => {
                $timeout(() => {
                    var selectedData: any[] = e.component.getSelectedRowsData()
                    var value = ''
                    selectedData.forEach((item, index) => {
                        if (index > 0) value += ','
                        value += item[CommonHelper.nameField]
                    })
                    $scope.bridge.value = value
                    $scope.bridge.elements = selectedData
                }, 0)
            },
            onDoubleRowClick: (e) => {
                $timeout(() => {
                    $scope.bridge.value = ''
                    $scope.bridge.elements = [e.data]
                    add()
                }, 0)
            },
            dataRequest: ($scope.source.dataScope && $scope.source.dataMethodName) ? (request): ng.IPromise<any> => {
                return api[$scope.source.dataScope][$scope.source.dataMethodName](request)
            } : null,
            columnRequest: (): ng.IPromise<any> => {
                return localDictionary[$scope.source.dictionaryMethodName]()
            },
            name: null
        }

        $scope.$watch(() => {
            return !($scope.added.length + $scope.removed.length)
        }, value => {
            $scope.submitButtonDisabled = value
        })

        $scope.$watch('grid', grid => {
            if (grid) {
                (<any>grid).option({
                    onRowPrepared: (e: any) => {
                        if (e.rowType == "data") {
                            if (e.data.isNew) {
                                $(e.rowElement).css('font-style', 'italic')
                            }
                            else if (e.data.isRemoved) {
                                $(e.rowElement).css('text-decoration', 'line-through')
                            }
                            e.rowElement.addClass('myRow').data('keyValue', e.key);
                        }
                    }
                })
            }
        })

        var add = () => {
            if ($scope.bridge.elements.length > 0) {
                var items = $scope.grid.option('dataSource').items()
                $scope.bridge.elements.forEach((e) => {
                    var element = {}
                    for (var property in e) {
                        element[property] = e[property]
                    }

                    var checkIfExist = (item) => {
                        return item['id'] == element[CommonHelper.idField]
                    }
                    if (!items.some(checkIfExist)) {
                        var newElement = (<any>Object).assign({}, element)
                        $scope.added.push(element)
                        newElement.isNew = true
                        $scope.grid.option("dataSource").store().insert(newElement)
                    }
                    if ($scope.removed.some(checkIfExist)) {
                        for (var i = 0; i < $scope.removed.length; i++) {
                            if ($scope.removed[i].id === element[CommonHelper.idField]) {
                                $scope.grid.byKey($scope.removed[i]).then(el => {
                                    el.isRemoved = false
                                    $scope.grid.refresh()
                                })
                                $scope.removed.splice(i, 1);
                                break
                            }
                        }
                    }
                    $scope.grid.refresh()
                })
            }

            $timeout(() => {
                $scope.bridge.value = null
                $scope.bridge.elements = []
            }, 0)
        }

        var remove = () => {
            var selected = $scope.grid.getSelectedRowKeys()
            if (selected.length) {
                selected.forEach(element => {
                    if (!(<any>element).isNew) {
                        if ((<any>element).isRemoved) {
                            for (var i = 0; i < $scope.removed.length; i++) {
                                if ($scope.removed[i].id === element['id']) {
                                    $scope.removed.splice($scope.removed.indexOf(element), 1);
                                    (<any>element).isRemoved = false
                                    break
                                }
                            }
                        } else {
                            (<any>element).isRemoved = true
                            $scope.removed.push(element)
                        }
                        $scope.grid.refresh()
                    }
                    else {
                        $scope.grid.option("dataSource").store().remove(element).then(() => {
                            $scope.grid.refresh()
                        })
                        $scope.added.splice($scope.added.indexOf(element), 1)
                    }
                })
            }
        }

        var submitChanges = () => {
            if ($scope.source.onSubmit) {
                $scope.$root.$broadcast('showLoadingIndicator')

                var dataSource: DevExpress.data.DataSource = $scope.grid.option("dataSource")
                var items = dataSource.store()['_array']
                
                var nowInGrid = $filter('orderBy')(items, 'VisibleIndex')

                $scope.source.onSubmit($scope.added, $scope.removed, nowInGrid).then(result => {
                    if (result) {
                        $scope.grid.option("dataSource").store().createQuery().filter('isNew', '=', true).toArray().forEach(element => {
                            element['isNew'] = false
                        })

                        $scope.grid.option("dataSource").store().createQuery().filter('isRemoved', '=', true).toArray().forEach(element => {
                            $scope.grid.option("dataSource").store().remove(element)
                        })
                        $scope.grid.refresh()

                        $scope.added = [];
                        $scope.removed = [];

                        $scope.submitButtonDisabled = true
                    }
                    $scope.$root.$broadcast('hideLoadingIndicator')
                }).catch(() => {
                    $scope.$root.$broadcast('hideLoadingIndicator')
                })
            }
        }

        $scope.addButtonDisabled = true
        $scope.removeButtonDisabled = true
        $scope.submitButtonDisabled = true

        var isNewSelected = () => {
            return $scope.bridge.elements.length > 0
        }
        var isInstalledSelected = () => {
            return $scope.grid ? $scope.grid.getSelectedRowKeys().length != 0 : false
        }

        $scope.submitButtonOptions = {
            text: i18n('Common', 'Apply'),
            onClick: submitChanges,
            bindingOptions: {
                disabled: 'submitButtonDisabled'
            }
        }

        $scope.addButtonOptions = {
            template: CommonHelper.getIcon('ADD'),
            onClick: add,
            bindingOptions: {
                disabled: 'addButtonDisabled'
            }
        }

        $scope.removeButtonOptions = {
            template: CommonHelper.getIcon('REMOVE'),
            onClick: remove,
            bindingOptions: {
                disabled: 'removeButtonDisabled'
            }
        }

        $scope.selectionButtonOptions = {
            template: CommonHelper.getIcon('SELECTION'),
            onClick: () => {
                $scope.selectionPopover.option('target', '#toolbar-popover-selection-button')
                $scope.selectionPopover.option('position', 'top')
                $scope.selectionPopoverVisible = !$scope.selectionPopoverVisible
            }
        }

        $scope.selectionPopover = undefined

        $scope.selectionPopoverOptions = {
            target: '#toolbar-popover-selection-button',
            width: 100,
            bindingOptions: {
                visible: 'selectionPopoverVisible'
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            },
            onInitialized: e => {
                $scope.selectionPopover = e.component
            }
        }

        $scope.selectionPopoverVisible = false

        var selectionOptions = {
            all: i18n("Grid", "SelectionAll"),
            none: i18n("Grid", "SelectionNone"),
            invert: i18n("Grid", "SelectionInvert")
        }

        $scope.selectionListOptions = {
            items: [selectionOptions.all, selectionOptions.invert, selectionOptions.none],
            onItemClick: e => {
                $scope.selectionPopoverVisible = false
                $scope.grid.beginUpdate()
                switch (e.itemData) {
                    case selectionOptions.all:
                        $scope.grid.selectRows($scope.grid.option('dataSource').items(), true)
                        break
                    case selectionOptions.none:
                        $scope.grid.clearSelection()
                        break
                    case selectionOptions.invert:
                        var selected = $scope.grid.getSelectedRowKeys().slice()
                        var deselectKeys = []
                        var selectKeys = []
                        $scope.grid.option('dataSource').items().forEach(key => {
                            if ($scope.grid.isRowSelected(key)) {
                                deselectKeys.push(key)
                            } else {
                                selectKeys.push(key)
                            }
                        })
                        $scope.grid.deselectRows(deselectKeys)
                        $scope.grid.selectRows(selectKeys, true)
                        break
                }
                $scope.grid.endUpdate()
            }
        }

        $scope.$watch(isNewSelected, (value) => {
            $scope.addButtonDisabled = !value
        })
        $scope.$watch(isInstalledSelected, (value) => {
            $scope.removeButtonDisabled = !value
        })

        $scope.$on(EventsNames.ROUTE_POINT_DRAG_AND_DROP + $scope.source.id, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.submitButtonDisabled = false
        })

    }]

    template = require('./toolbar-popover.tpl.html')
}