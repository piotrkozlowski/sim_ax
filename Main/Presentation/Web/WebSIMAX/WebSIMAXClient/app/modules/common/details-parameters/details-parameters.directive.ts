﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FieldModel } from '../../api/api.models'
import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { EventsNames } from '../../../helpers/events-names'
import { Permissions } from '../../../helpers/permissions'

require('./details-parameters.scss')

interface DetailsParametersScope extends ng.IScope {
    fields: any[]
    modelId: number
    popupFor: string
    parseDate(value: any): string

    CommonHelper: CommonHelper
    DistributorDetails(field: FieldModel)
    DeviceDetails(field: FieldModel)
    MeterDetails(field: FieldModel)
    RouteDetails(field: FieldModel)
    UserDetails(field: FieldModel)

    scrollViewOptions: DevExpress.ui.dxScrollViewOptions

    detailsPopupId: number
    showParams: boolean

    addDistributorDetails: boolean
    addDeviceDetails: boolean
    addMeterDetails: boolean
    addRouteDetails: boolean
    addUserDetails: boolean
}

export class DetailsParametersDirective implements ng.IDirective {
    constructor() { }

    restrict = 'E'
    scope = {
        fields: '=',
        modelId: '=',
        popupFor: '='
    }
    template = require("./details-parameters.tpl.html")

    link = (scope: ng.IScope, element: JQuery) => {

    }

    controller = ['$scope', '$element', 'api', 'logger', 'localDictionary', '$timeout', ($scope: DetailsParametersScope, $element: JQuery, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, $timeout: ng.ITimeoutService) => {
        $scope.CommonHelper = CommonHelper
        $scope.detailsPopupId = new Date().getTime()
        $scope.parseDate = (value) => {
            return CommonHelper.cellFormatForDifferentValues({
                value: value
            })
        }
        $scope.$watch('modelId', val => {
            $scope.showParams = false
        })

        $scope.$watch('fields', val => {
            $scope.showParams = true
            $timeout(() => {
                var height = 0
                $element.find('.field').each((i, e) => {
                    height += angular.element(e).outerHeight()
                })
                var parentHeight = $element.parent().height()
                if (parentHeight - height > 0) $element.height(height)
            })
        })
                
        $scope.scrollViewOptions = {
            height: '100%'
        }
        var openDetails = (field, eventName: string, typeName: string) => {
            if (!$scope[typeName]) {
                $scope[typeName] = true
                $timeout(() => {
                    openDetails(field, eventName, typeName)
                })
                return
            }
            if (!field.valueReferenceId) field.valueReferenceId = field.value
            var itemIds = [{ id: field.valueReferenceId, title: field.value }]
            if (angular.isNumber(field.valueReferenceId) && field.valueReferenceId > 0)
                $scope.$broadcast(eventName + $scope.detailsPopupId, field.valueReferenceId, itemIds)
            else CommonHelper.openInfoPopup("Problem with reference id", "error")
        }

        $scope.DistributorDetails = (field) => {
            openDetails(field, EventsNames.OPEN_DISTRIBUTORS_DETAILS_POPUP, 'addDistributorDetails')
        }
        $scope.DeviceDetails = (field) => {
            openDetails(field, EventsNames.OPEN_DEVICES_DETAILS_POPUP, 'addDeviceDetails')
        }
        $scope.MeterDetails = (field) => {
            openDetails(field, EventsNames.OPEN_METER_DETAILS_POPUP, 'addMeterDetails')
        }
        $scope.RouteDetails = (field) => {
            openDetails(field, EventsNames.OPEN_ROUTES_DETAILS_POPUP, 'addRouteDetails')           
        }
        $scope.UserDetails = (field) => {
            openDetails(field, EventsNames.OPEN_USER_DETAILS_POPUP, 'addUserDetails')
        }
    }]

    transclude = true
}