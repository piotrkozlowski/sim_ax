﻿import 'angular'

export class GridHeightFillDirective implements ng.IDirective {
    constructor(private $timeout: ng.ITimeoutService) { }

    restrict = 'A'
    link = (scope: ng.IScope, element: JQuery) => {
        scope.$watch(() => {
            var gridElement = element.find('.dx-datagrid')
            return element.height() - gridElement.height()
        }, (result) => {
            if (result != 0) {
                this.$timeout(() => {
                    if (element && element.dxDataGrid()) {
                        var grid: DevExpress.ui.dxDataGrid = element.dxDataGrid('instance')
                        grid.updateDimensions()
                    }
                    if (!scope.$$phase) scope.$digest()
                }, 500)
            }
        })
    }
}