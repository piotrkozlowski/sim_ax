﻿import { i18n } from './i18n/common.i18n'
import { CommonHelper } from '../../helpers/common-helper'
import { UserConfigHelper } from '../../helpers/user-config-helper'
import { BaseGridController, ScopeBaseGrid } from './common.base.grid.controller'
import { ButtonConfig } from './common.grid-buttons-options'
import { GridDetailsSettings } from './grid-details/grid-details.directive'
import { Api } from  './../api/api.module'
import { EventsNames } from '../../helpers/events-names'

export interface ScopeBaseGridsterGrid extends ScopeBaseGrid {
    // gridster
    gridsterOpts: any
    gridsterGridOpts: any
    gridsterSecondRowOpts: any

    isSecondRowVisible: boolean
    isLandscape: boolean
    closeSecondRow()

    // settings for controls in second row
    isDetailsVisible: boolean

    // popover with positions list
    positionPopoverOptions: DevExpress.ui.dxPopoverOptions
    positionListOptions: DevExpress.ui.dxListOptions
    // details
    detailsButtonText: string
    showDetails(e: any): void
    gridDetailsSettings: GridDetailsSettings

    
    // map directive data
    isMapVisible: boolean
    mapButtonText: string
    mapData: any[]
    rowsData: any[]

    // pin/unpin grid
    pinSwitchOptions: DevExpress.ui.dxButtonOptions
    returnButtonOptions: DevExpress.ui.dxButtonOptions
    isGridShown: boolean
    onReturnButtonMouseOver: () => void
    onReturnButtonMouseOut: () => void
    onFocus: () => void
    returnButtonStyle
    switchButtonStyle
}

export class BaseGridGridsterController extends BaseGridController {
    private GRIDSTER_SETTINGS = 'GRIDSTER_SETTINGS'
    private IS_SECOND_ROW_VISIBLE = 'IS_SECOND_ROW_VISIBLE'
    private SECOND_ROW_VISIBLE_TYPE = 'SECOND_ROW_VISIBLE_TYPE'
    protected SECOND_ROW_POSITION = 'SECOND_ROW_POSITION'
    private selectedRowsData: any[]
    private isMarkerClicked: boolean

    gridPositions = {
        left: "OnTheLeft",
        right: "OnTheRight",
        top: "OnTheTop",
        bottom: "OnTheBottom",
        hide: "Hide"
    }

    private pinButton: DevExpress.ui.dxButton
    private isGridPinned: boolean

    // data for maps
    private actualData: any[]

    constructor(protected $scope: ScopeBaseGridsterGrid, protected $timeout: ng.ITimeoutService, protected $compile: ng.ICompileService, drawGrid: () => void, protected api: Api, gridDetailsSettings: GridDetailsSettings, name: string, private addMap: boolean = false, private showShortDetails: boolean = true) {
        super($scope, $timeout, $compile, drawGrid, api)

        this.GRIDSTER_SETTINGS += name
        this.IS_SECOND_ROW_VISIBLE += name
        this.SECOND_ROW_VISIBLE_TYPE += name
        this.SECOND_ROW_POSITION += name

        this.isMarkerClicked = false

        $scope.gridDetailsSettings = gridDetailsSettings


        $scope.gridsterOpts = {
            columns: 4, // the width of the grid, in columns
            pushing: false,
            swapping: false,
            floating: false,
            outerMargin: false, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 200, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 4,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: 4, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: 4, // maximum row height of an item
            resizable: {
                enabled: false
            },
            draggable: {
                enabled: false
            }
        }

        var positionList = [this.gridPositions.left, this.gridPositions.right, this.gridPositions.top, this.gridPositions.bottom, this.gridPositions.hide]
        $scope.positionListOptions = {
            items: positionList.map(key => i18n("Grid", key)),
            onItemClick: e => {
                this.hideGridsterPopover()
                this.$scope.hideSettingsPopover()
                this.setGridsterPosition(positionList[e.itemIndex])
            }
        }
        $scope.positionPopoverOptions = {
            width: 160,
            visible: false,
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            },
            onInitialized: e => {
                $scope.positionPopover = e.component 
            }
        }

        $scope.$on(EventsNames.CLOSE_MAP, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.closeSecondRow()
        })

        $scope.$on(EventsNames.MARKER_CLICKED, (event: ng.IAngularEvent, ...args: any[]) => {
            if (args[0].parent === false) {
                var rows = args[0].row
            } else {
                var rowId = args[0].row[CommonHelper.serialNumberParentField]
                var allRows = this.$scope.dataGrid.option("dataSource").items()
                rows = allRows.filter(row => {
                    if (row[CommonHelper.serialNumberParentField] === rowId)
                        return true
                })
            }
            
            this.isMarkerClicked = true
            this.$scope.dataGrid.selectRows(rows, false)
        })


        $scope.closeSecondRow = () => {
            this.hideSecondRow()
            if (!$scope.isGridShown) {
                $scope.isGridShown = true
            }
        }

        var isSecondRowVisible = UserConfigHelper.getUserConfig(this.IS_SECOND_ROW_VISIBLE, false)
        if (isSecondRowVisible) this.showSecondRow()
        else this.hideSecondRow()

        var visibleType = UserConfigHelper.getUserConfig(this.SECOND_ROW_VISIBLE_TYPE)
        if (visibleType == 'map') {
            $scope.isMapVisible = true
            $scope.isDetailsVisible = false
            $scope.detailsButtonText = i18n("Grid", "ShowPreview")
            $scope.mapButtonText = i18n("Grid", "SetMapPosition")
        } else if (visibleType == 'details') {
            $scope.isMapVisible = false
            $scope.isDetailsVisible = true
            $scope.mapButtonText = i18n("Grid", "ShowMap")
            $scope.detailsButtonText = i18n("Grid", "SetPreviewPosition")
        }
        if (!isSecondRowVisible) {
            $scope.detailsButtonText = i18n("Grid", "ShowPreview")
            $scope.mapButtonText = i18n("Grid", "ShowMap")
        }

        $scope.$watch(() => {
            return angular.element('.content').innerHeight()
        }, (contentHeight) => {
            $scope.gridsterOpts.rowHeight = (contentHeight - 10) / 4
            var windowWidth = angular.element(window).width()
            if ($scope.gridsterOpts.rowHeight < 150 && windowWidth >= 768) $scope.gridsterOpts.rowHeight = 150
            else if (windowWidth < 768) $scope.gridsterOpts.rowHeight = 300

            $timeout(() => {
                if ($scope.dataGrid) $scope.dataGrid.updateDimensions()
            })
        })

        $scope.$watch(() => {
            return angular.element(window).width()
        }, (windowWidth) => {
            if (windowWidth < 768) $scope.gridsterOpts.isMobile = true
            else $scope.gridsterOpts.isMobile = false
        })

        //pinned grid
        this.isGridPinned = true
        $scope.isGridShown = true

        $scope.returnButtonStyle = { position: 'absolute', left: '5px', top: '5px', 'z-index': '1000', opacity: '0.05' }
        $scope.switchButtonStyle = { position: 'absolute', left: '5px', top: '5px', 'z-index': '1000', opacity: '1' }

        $scope.onReturnButtonMouseOver = () => {
            if (!this.isGridPinned) $scope.returnButtonStyle = { position: 'absolute', left: '5px', top: '5px', 'z-index': '1000', opacity: '1' }
        }
        $scope.onReturnButtonMouseOut = () => {
            if (!this.isGridPinned) $scope.returnButtonStyle = { position: 'absolute', left: '5px', top: '5px', 'z-index': '1000', opacity: '0.05' }
        }
        $scope.onFocus = () => {
            if ($scope.isGridShown) {
                if (!this.isGridPinned) {
                    $scope.isGridShown = false
                    $scope.gridsterSecondRowOpts = { sizeX: 4, sizeY: 4, row: 0, col: 0 }
                    $timeout(() => {
                        this.resizeSecondRow()
                    }, 1000)
                }
            }
        }
        $scope.returnButtonOptions = {
            template: CommonHelper.getIcon('SETTINGS'),
            onClick: () => {
                if (this.isGridPinned) {
                    $scope.isGridShown = false
                } else {
                    $scope.isGridShown = true
                }
                $timeout(() => {
                    var secondRowPosition = UserConfigHelper.getUserConfig(this.SECOND_ROW_POSITION, this.gridPositions.bottom)
                    this.setGridsterPosition(secondRowPosition)
                })
            },
            hint: i18n('Common', 'ShowGrid')
        }
        $scope.pinSwitchOptions = {
            template: CommonHelper.getIcon('KB_ARROW_DOWN'),
            hint: 'Pin',
            onClick: () => {
                if (this.isGridPinned) {
                    this.isGridPinned = false
                    $scope.pinSwitchOptions.template = CommonHelper.getIcon('KB_ARROW_LEFT')
                    this.pinButton.option({ template: CommonHelper.getIcon('KB_ARROW_LEFT') })
                }
                else {
                    this.isGridPinned = true
                    $scope.pinSwitchOptions.template = CommonHelper.getIcon('KB_ARROW_DOWN')
                    this.pinButton.option({ template: CommonHelper.getIcon('KB_ARROW_DOWN') })
                }
            },
            onInitialized: (e) => { this.pinButton = e.component }
        }
        // end of pinned grid
    }

    protected setGridsterPosition = (position) => {
        if (position == this.gridPositions.hide)
            this.hideSecondRow()
        if (this.$scope.isSecondRowVisible) {
            UserConfigHelper.setUserConfig(this.SECOND_ROW_POSITION, position)
            var isLandscape = true
            var gridsterGridOpts, gridsterSecondRowOpts
            switch (position) {
                case this.gridPositions.left:
                    gridsterGridOpts = { sizeX: 2, sizeY: 4, row: 0, col: 2 }
                    gridsterSecondRowOpts = { sizeX: 2, sizeY: 4, row: 0, col: 0 }
                    isLandscape = false
                    break
                case this.gridPositions.right:
                    gridsterGridOpts = { sizeX: 2, sizeY: 4, row: 0, col: 0 }
                    gridsterSecondRowOpts = { sizeX: 2, sizeY: 4, row: 0, col: 2 }
                    isLandscape = false
                    break
                case this.gridPositions.top:
                    gridsterGridOpts = { sizeX: 4, sizeY: 2, row: 2, col: 0 }
                    gridsterSecondRowOpts = { sizeX: 4, sizeY: 2, row: 0, col: 0 }
                    break
                case this.gridPositions.bottom:
                    gridsterGridOpts = { sizeX: 4, sizeY: 2, row: 0, col: 0 }
                    gridsterSecondRowOpts = { sizeX: 4, sizeY: 2, row: 2, col: 0 }
                    break
            }
            this.$scope.gridsterGridOpts = gridsterGridOpts
            this.$scope.gridsterSecondRowOpts = gridsterSecondRowOpts
            this.$timeout(() => {
                this.$scope.isLandscape = isLandscape
                this.$timeout(() => {
                    this.$scope.dataGrid.updateDimensions()
                    this.resizeSecondRow()
                }, 0)
            }, 500)
        }
    }

    private showPopover = (target?: string) => {
        var component = angular.element('#SecondRowPositionPopover').dxPopover('instance')
        if (target) component.option('target', target)
        component.option('visible', true)
    }

    protected onSelectionChanged = (e) => {
        this.$scope.rowsData = e.selectedRowsData
        this.selectedRowsData = this.$scope.dataGrid.getSelectedRowsData()
        
        var ids = this.selectedRowsData.map(s => {
            return s[CommonHelper.idField]
        })
        if (ids.length > 10) {
            CommonHelper.openInfoPopup(i18n('Grid', 'Max10Locations'), 'info', 5000)
            ids = ids.slice(0, 10)
        }
        this.$scope.gridDetailsSettings.ids = ids
        if (!this.$scope.$$phase) this.$scope.$digest()
        if (!this.isMarkerClicked) {
            this.$scope.$broadcast(EventsNames.GRIDSTER_ROW_CLICKED, e.selectedRowsData)
        } else {
            this.isMarkerClicked = false
        }
        
    }

    private showSecondRow = () => {
        this.$scope.isSecondRowVisible = true
        UserConfigHelper.setUserConfig(this.IS_SECOND_ROW_VISIBLE, true)
        var secondRowPosition = UserConfigHelper.getUserConfig(this.SECOND_ROW_POSITION, this.gridPositions.bottom)
        this.setGridsterPosition(secondRowPosition)
        this.$timeout(() => {
            if (this.$scope.dataGrid.totalCount() > 0 && this.$scope.dataGrid.getSelectedRowsData().length < 1) {
                this.$scope.dataGrid.selectRowsByIndexes([0])
            }
            this.$scope.dataGrid.updateDimensions()
        }, 500)
    }

    private hideSecondRow = () => {
        if (this.addMap) {
            this.$scope.mapButtonText = i18n("Grid", "ShowMap")
        }

        this.$scope.detailsButtonText = i18n("Grid", "ShowPreview")
        this.$scope.isSecondRowVisible = false
        UserConfigHelper.setUserConfig(this.IS_SECOND_ROW_VISIBLE, false)
        this.$scope.gridsterGridOpts = { sizeX: 4, sizeY: 4, row: 0, col: 0 }
        this.$timeout(() => {
            this.$scope.dataGrid.updateDimensions()
        }, 500)
    }

    protected resizeSecondRow = () => {
        if (this.addMap && this.$scope.isSecondRowVisible && this.$scope.isMapVisible)
            this.$scope.$broadcast(EventsNames.REFRESH_MAP_SIZE)
        if (this.$scope.isDetailsVisible && this.$scope.isSecondRowVisible) {
            this.$scope.$broadcast(EventsNames.UPDATE_CHART)
            this.$scope.$broadcast(EventsNames.UPDATE_GRID_DETAILS)
        }
    }

    protected showPreviewDetails = (target = '#DetailsButton') => {
        if (this.$scope.isSecondRowVisible && this.$scope.isDetailsVisible) {
            this.showPopover(target)
        }
        else if (!this.$scope.isSecondRowVisible) {
            this.$scope.mapButtonText = i18n("Grid", "ShowMap")
            this.$scope.detailsButtonText = i18n("Grid", "SetPreviewPosition")
            this.showSecondRow()
            this.$scope.hideSettingsPopover()
        }
        this.$scope.isMapVisible = false
        this.$scope.isDetailsVisible = true
        UserConfigHelper.setUserConfig(this.SECOND_ROW_VISIBLE_TYPE, 'details')
    }

    protected showMapDetails = (target = '#MapButton') => {
        if (this.$scope.isSecondRowVisible && this.$scope.isMapVisible) {
            this.showPopover(target)
        }
        else if (!this.$scope.isSecondRowVisible) {
            this.$scope.detailsButtonText = i18n("Grid", "ShowPreview")
            this.$scope.mapButtonText = i18n("Grid", "SetMapPosition")
            this.showSecondRow()
            this.$scope.hideSettingsPopover()
        }
        this.$scope.isMapVisible = true
        this.$scope.isDetailsVisible = false
        UserConfigHelper.setUserConfig(this.SECOND_ROW_VISIBLE_TYPE, 'map')
    }

    protected addButtons = (buttonsOptions: ButtonConfig[]) => {
        // map
        if (this.addMap) {
            buttonsOptions.push({
                name: 'MapButton',
                options: {
                    onClick: () => {
                        this.$scope.positionPopover.option('position', 'bottom')
                        this.showMapDetails()
                    },
                    template: CommonHelper.getIcon('MAP'),
                    bindingOptions: {
                        hint: 'mapButtonText'
                    }
                },
                idSelector: 'MapButton'
            })
        }
        // end of map
        // details
        if (this.showShortDetails) {
            buttonsOptions.push({
                name: 'DetailsButton',
                options: {
                    onClick: () => {
                        this.$scope.positionPopover.option('position', 'bottom')
                        this.showPreviewDetails()
                    },
                    template: CommonHelper.getIcon('DETAILS_PREVIEW'),
                    bindingOptions: {
                        hint: 'detailsButtonText'
                    }
                },
                idSelector: 'DetailsButton'
            })
        }
        this.settingsList.showPreview = ["ShowPreview", "DETAILS_PREVIEW"]
        this.settingsList.showMap = ["ShowMap", "MAP"]
        // end of details
    }
    
    protected onDataReady = (data: any[]) => {
        if (this.addMap) {
            this.$scope.mapData = data
            this.actualData = data
        }
        this.$scope.showGrid = true

        this.$scope.$watch(() => {
            return angular.element('.dx-datagrid').width()
        }, (gridWidth) => {
            if (gridWidth < 220) {
                angular.element('.dx-datagrid-search-panel').addClass('dx-datagrid-search-panel-narrow')
            } else {
                angular.element('.dx-datagrid-search-panel').removeClass('dx-datagrid-search-panel-narrow')
            }
        })
    }
}
