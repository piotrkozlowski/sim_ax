﻿import { i18n } from './../i18n/common.i18n'

require('./file-uploader.scss')

export interface FileUploaderOptions {
    acceptValue: string
    uploaderVisible: boolean
    readAsText?: boolean
    onReadEnd?: (reader, fileName) => void
    onSubmitClick?: () => void
}

interface FileUploaderScope extends ng.IScope {
    uploaderOptions: DevExpress.ui.dxFileUploaderOptions
    uploader: DevExpress.ui.dxFileUploader
    cancelFileButtonOptions: DevExpress.ui.dxButtonOptions
    submitFileButtonOptions: DevExpress.ui.dxButtonOptions

    file: File
    
    submitButtonDisabled: boolean

    options: FileUploaderOptions
}

export class FileUploaderDirective implements ng.IDirective {
    constructor() { }

    restrict = 'E'

    template = require('./file-uploader.tpl.html')

    scope = {
        options: '='
    }

    controller = ['$scope', '$timeout', ($scope: FileUploaderScope, $timeout: ng.ITimeoutService) => {
        $scope.submitButtonDisabled = true

        $scope.uploaderOptions = {

            selectButtonText: i18n('Common', 'SelectFile'),
            labelText: i18n('Common', 'DropFileHere'),
            width: '100%',
            height: '100%',
            readyToUploadMessage: i18n('Common', 'ReadyToUpload'),
            uploadMode: 'useForm',
            uploadedMessage: i18n('Common', 'Uploaded'),
            uploadFailedMessage: i18n('Common', 'UploadFailed'),
            showFileList: false,
            bindingOptions: {
                accept: 'options.acceptValue'
            },
            onInitialized: e => {
                $scope.uploader = e.component
            },
            onValueChanged: (e) => {
                if (e.value && e.value.length) {
                    $scope.options.uploaderVisible = false
                    $scope.file = e.value[0]
                    $scope.submitButtonDisabled = false
                }
                else {
                    $scope.options.uploaderVisible = true
                }
            }
        }

        $scope.cancelFileButtonOptions = {
            text: i18n("Common", "Cancel"),
            onClick: () => {
                if ($scope.uploader) $scope.uploader.reset()
                $scope.file = null
                $scope.options.uploaderVisible = true
            }
        }

        $scope.submitFileButtonOptions = {
            text: i18n("Common", "Upload"),
            bindingOptions: {
                disabled: 'submitButtonDisabled'
            },
            onClick: () => {
                if ($scope.options.onSubmitClick)
                    $scope.options.onSubmitClick()
                
                var reader = new FileReader()
                reader.onloadend = (e: any) => {
                    if ($scope.options.onReadEnd)
                        $scope.options.onReadEnd(reader, $scope.file.name)

                    $scope.submitButtonDisabled = true
                }
                if ($scope.options.readAsText)
                    reader.readAsText($scope.file)
                else
                    reader.readAsDataURL($scope.file)

            }
        }
    }]

    replace = true
}