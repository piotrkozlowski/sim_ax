﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FileModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridBridge } from '../../common/common.interfaces'
import { i18n } from './../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'

require('./grid-attachments-edit.scss')


interface AttachmentsEditScope extends CommonScope {
    gridOptions: DevExpress.ui.dxDataGridOptions
    uploaderOptions: DevExpress.ui.dxFileUploaderOptions
    uploader: DevExpress.ui.dxFileUploader
    formOptions: DevExpress.ui.dxFormOptions
    form: DevExpress.ui.dxForm
    file: File
    cancelFileButton: DevExpress.ui.dxButtonOptions
    submitFileButton: DevExpress.ui.dxButtonOptions
    deleteButtonOptions: DevExpress.ui.dxButtonOptions
    attachFileButtonOptions: DevExpress.ui.dxButtonOptions

    popoverOptions: DevExpress.ui.dxPopoverOptions
    buttonOptions: DevExpress.ui.dxButtonOptions
    grid: DevExpress.ui.dxDataGrid
    popoverVisible: boolean
    uploaderVisible: boolean
    selectedItemsIds: number[]

    attachmentData: any
    source: GridBridge
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    isRowSelected: () => boolean
    deleteButtonDisabled: boolean
    deleteFile: (ids: number[]) => void
}


export class GridAttachmentsEditDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        source: '='
    }
    template = require("./grid-attachments-edit.tpl.html")

    link = (scope, element, attrs) => {
        scope.$watch("source", value => {
            scope.source = value
        })
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope: AttachmentsEditScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.popoverVisible = false
        $scope.uploaderVisible = true

        $scope.popoverOptions = {
            width: 600,
            height: 250,
            target: '#popover_' + $scope.$id,
            bindingOptions: {
                visible: 'popoverVisible'
            },
            onHiding: () => {
                $scope.popoverVisible = false
            },
            onShowing: () => {
                if ($scope.uploader) $scope.uploader.reset()
                if ($scope.form) $scope.form.resetValues()
            }
        }

        $scope.popupOptions = {
            showTitle: true,
            showCloseButton: true,
            title: $scope.source.title,
            bindingOptions: {
                visible: 'popupVisible'
            },
            onHiding: () => {
                $scope.$root.$broadcast(EventsNames.CLOSE_GRID_ATTACHMENTS_EDIT_POPUP + $scope.source.popupId)
            }
        }

        $scope.uploaderOptions = {

            selectButtonText: i18n('Common', 'SelectFile'),
            labelText: i18n('Common', 'DropFileHere'),
            width: '100%',
            height: '100%',
            readyToUploadMessage: i18n('Common', 'ReadyToUpload'),
            uploadMode: 'useForm',
            uploadedMessage: i18n('Common', 'Uploaded'),
            uploadFailedMessage: i18n('Common', 'UploadFailed'),
            showFileList: false,
            onInitialized: e => {
                $scope.uploader = e.component
            },
            onValueChanged: (e) => {
                if (e.value && e.value.length) {
                    $scope.uploaderVisible = false
                    $scope.file = e.value[0]
                }
                else {
                    $scope.uploaderVisible = true
                }
            }           
        }


        var formItems = [
            {
                dataField: 'Description',
                label: { text: i18n('Popup','Description') },
                visible: true,
                editorType: 'dxTextArea',
                cssClass: 'text-area-description'
            }
        ]

        $scope.formOptions = {
            colCount: 1,
            height: 100,
            labelLocation: 'top',
            items: formItems,
            bindingOptions: {
                formData: 'attachmentData'
            },
            onInitialized: e => {
                $scope.form = e.component
            }
        }

        $scope.attachmentData = {
            Description: ""
        }

        $scope.$on(EventsNames.OPEN_GRID_ATTACHMENTS_EDIT_POPUP + $scope.source.popupId, (event: ng.IAngularEvent, ...args: any[]) => {


            $scope.deleteButtonDisabled = true
            load().then(() => {
                if ($scope.source.items) {
                    $scope.source.items.forEach(element => {
                        $scope.gridOptions.dataSource.store().insert(element)
                    })
                    $scope.grid.option($scope.gridOptions)
                }
            })

            $scope.popupVisible = true
        })

        var load = () => {
            var deferred = $q.defer()

            localDictionary[$scope.source.dictionaryMethodName]().then(defaultColumns => {
                var columns = defaultColumns.map((value) => {

                    var column = {
                        dataField: value.id,
                        caption: value.caption,
                        dataType: CommonHelper.getDataType(value)
                    }
                    if (value.type == CommonHelper.date) column['format'] = i18n("Common", "DateFormatDX")

                    return column
                })

                var customGridOptions = {
                    name: "Attachment",
                    columns: columns,
                    onInitialized: e => {
                        $scope.grid = e.component
                        deferred.resolve()
                    },
                    onRowClick: (e) => {
                        $scope.deleteButtonDisabled = false
                        var selectedItems = e.component.getSelectedRowsData()
                        $scope.selectedItemsIds = selectedItems.map(item => item.id)
                    }
                }
                $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger, null, customGridOptions)
            })
            return deferred.promise
        }

        var addFile = (attachment) => {
            if ($scope.source.onAdd) {
                $scope.source.onAdd(attachment).then(result => {
                    if (result) {
                        $scope.source.items.push(result)
                        $scope.uploader.reset()
                        $scope.form.resetValues()
                    }
                })
            }
        }

        $scope.deleteFile = (ids: number[]) => {
            if ($scope.source.onDelete) {
                $scope.source.onDelete(ids).then(result => {
                    if (result) {
                        result.forEach(id => {
                            for (var i = 0; i < $scope.source.items.length; i++) {
                                var item = $scope.source.items[i]

                                if (item.id == id) {
                                    $scope.source.items.splice(i, 1)
                                    break
                                }
                            }
                        })
                    }
                })
            }
        }


        var togglePopover = () => {
            $scope.popoverVisible = !$scope.popoverVisible
        }

        var isRowSelected = () => {
            return $scope.grid ? $scope.grid.getSelectedRowKeys().length != 0 : false
        }

        $scope.cancelFileButton = {
            text: i18n("Common", "Cancel"),
            onClick: () => {
                if ($scope.uploader) $scope.uploader.reset()
                $scope.file = null
                $scope.uploaderVisible = true
            }
        }

        $scope.submitFileButton = {
            text: i18n("Common", "Upload"),
            onClick: () => {
                var file = new FileModel()
                file.description = $scope.attachmentData.Description
                file.size = $scope.file.size
                file.physicalFileName = $scope.file.name
                var reader = new FileReader()
                reader.onload = (e: any) => {
                    file.fileBytes = e.target.result
                    addFile(file)
                }
                reader.readAsDataURL($scope.file)

            }
        }

        $scope.deleteButtonOptions = {
            text: i18n('Common', 'Delete'),
            onClick: () => {
                $scope.deleteFile($scope.selectedItemsIds)
            },
            bindingOptions: {
                disabled: 'deleteButtonDisabled'
            }
        }

        $scope.attachFileButtonOptions = {
            activeStateEnabled: false,
            template: CommonHelper.getIcon('ATTACH_FILE'),
            hint: i18n('Popup', 'UploadAttachment'),
            onClick: togglePopover,
            bindingOptions: {
                disabled: 'buttonDisabled'
            }
        }

        $scope.$watch(isRowSelected, (value) => {
            $scope.deleteButtonDisabled = !value
        })


        $scope.$watchCollection('source.items', (items: any[]) => {
            if ($scope.grid) {
                $scope.grid.option("dataSource").store().clear()
                items.forEach((item) => {
                    $scope.grid.option("dataSource").store().insert(item)
                })
                $scope.grid.refresh()
            }
        })
    }]
}