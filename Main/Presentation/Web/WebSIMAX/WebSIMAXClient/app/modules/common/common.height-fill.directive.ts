﻿import 'angular'

export class HeightFillDirective implements ng.IDirective {
    constructor(private window: ng.IWindowService) {

    }

    scope = {
        subtract: '@heightFillSubtract',
        disableOn: '@heightFillDisableOn',
        disable: '@heightFillDisable'
    }

    restrict = 'A'
    link = (scope: ng.IScope, element: JQuery) => {
        var updateHeight = () => {
            var subtract = 0
            if (scope['subtract']) subtract = scope['subtract']

            var disable = typeof scope['disable'] == 'boolean' ? scope['disable'] : false

            if (!disable && scope['disableOn'] && scope['disableOn'] >= this.window.innerWidth) {
                element.height('auto')
            } else {
                element.height(this.window.innerHeight - subtract)
            }            
        }
        updateHeight()
        scope.$watch('subtract', updateHeight)
        angular.element(this.window).bind('resize', updateHeight)
    }
}