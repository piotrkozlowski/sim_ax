﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { MeasurementsController } from './measurements.controller' 

require('./measurements.scss')


export class MeasurementsDirective implements ng.IDirective {
    scope = {
        "hideGridster": '=?',
        "addGrid": '=?',
        "isGridInCommon": '=?',
        "source": "=source"
    }
    restrict = 'E'
    controller = ['$scope', '$element', '$q', '$timeout', 'logger', 'api', 'localDictionary',
        ($scope, $element: ng.IRootElementService, $q: ng.IQService, $timeout: ng.ITimeoutService, logger: LoggerService, api: Api, localDictionary: LocalDictionaryFactory) =>
            new MeasurementsController($scope, $element, $q, $timeout, logger, api, localDictionary)
    ]
    template = require("./measurements.tpl.html")
    replace = true
}