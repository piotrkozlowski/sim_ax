﻿import 'angular'

import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { SchemaModel, SchemaElement, SchemaElementApperance, FieldNameValueModel, FormValuesModel, ColumnModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common.grid-buttons-options'
import { i18n } from '../i18n/common.i18n'
import { Permissions } from '../../../helpers/permissions'
import { BaseGridController, ScopeBaseGrid } from '../common.base.grid.controller'

require('./schema.scss')

interface SchemaSource {
    id?: number
    data?: SchemaModel
    isReadOnly: boolean
}

interface ImgPosition {
    top: number
    left: number
}

interface SchemaScope extends ng.IScope {
    source: SchemaSource
    schemaModel: SchemaModel
    isLoaded: boolean
    originalHeight: number
    originalWidth: number
    imgPosition: ImgPosition

    scaleX: number
    scaleY: number

    formOptions: DevExpress.ui.dxFormOptions
    form: DevExpress.ui.dxForm
    formData: any
    items: any[]

    applyButtonOptions: DevExpress.ui.dxButtonOptions
    isApplyButtonDisabled: boolean

    units: ColumnModel[]
}

export class SchemaDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        source: '='
    }
    template = require("./schema.tpl.html")

    controller = ['$scope', '$element', '$q', '$timeout', 'api', 'logger', 'localDictionary', '$window', ($scope: SchemaScope, $element: JQuery, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, $window: ng.IWindowService) => {
        $scope.items = []
        $scope.formData = {}
        $scope.isApplyButtonDisabled = true

        var units = localDictionary.getUnits().then(result => {
            $scope.units = result
        })

        var lastResizeTimeout
        var resize = () => {
            if (lastResizeTimeout) $timeout.cancel(lastResizeTimeout)
            lastResizeTimeout = $timeout(() => {
                var actualImg = $element.find('.schema')

                $scope.scaleX = actualImg.width() / $scope.originalWidth
                $scope.scaleY = actualImg.height() / $scope.originalHeight
                $scope.imgPosition = actualImg.position()
                var number = 0
                if ($scope.schemaModel && $scope.schemaModel.schemaConfiguration) {
                    $scope.schemaModel.schemaConfiguration.forEach(sc => {
                        var item = $scope.items[number]
                        if (item) {
                            item.editorOptions.height = sc.height * $scope.scaleY + 2
                            item.editorOptions.width = sc.width * $scope.scaleX + 2
                        }

                        updateInput(number++, sc)
                    })
                    $scope.form.option('items', $scope.items)
                }
            }, 200)
        }

        $scope.$watch(() => {
            return $element.width()
        }, (width) => {
            resize()
        })

        var getImageSize = (byteArray) => {
            var defer = $q.defer()

            var img = new Image()

            img.onload = () => {
                $scope.originalHeight = img.height;
                $scope.originalWidth = img.width;

                var actualImg = $element.find('.schema')
                $scope.scaleX = actualImg.width() / $scope.originalWidth
                $scope.scaleY = actualImg.height() / $scope.originalHeight
                $scope.imgPosition = actualImg.position()

                defer.resolve()
            }

            img.src = 'data:image/png;base64,' + byteArray

            return defer.promise
        }

        var updateSchema = (schema: SchemaModel) => {
            $scope.schemaModel = schema

            if ($scope.schemaModel.schema)
                getImageSize($scope.schemaModel.schema).then(() => {
                    if ($scope.schemaModel.schemaConfiguration) {
                        $scope.schemaModel.schemaConfiguration.forEach(sc => {
                            addToForm(sc)
                        })
                    }
                    $element.find('.form-container').dxForm('instance').repaint()
                    $timeout(() => {
                        $scope.isLoaded = true
                    }, 1000)
                })
            else
                $scope.isLoaded = true
        }

        var loadSchema = () => {
            $scope.isLoaded = false

            if ($scope.source.id) {
                api.location.getSchema($scope.source.id).then(result => {
                    updateSchema(result)
                })
            } else if ($scope.source.data) {
                units.then(() => {
                    updateSchema($scope.source.data)
                })
            }
        }
        loadSchema()

        $scope.formOptions = {
            bindingOptions: {
                formData: 'formData',
                items: 'items'
            },
            onInitialized: (e) => {
                $scope.form = e.component
            }
        }

        $scope.applyButtonOptions = {
            text: i18n('Common', 'Apply'),
            bindingOptions: {
                disabled: 'isApplyButtonDisabled'
            },
            onClick: (e) => {
                var model = <FormValuesModel>{}
                model.fields = []
                model.id = $scope.source.id

                Object.keys($scope.formData).forEach(prop => {
                    var field = new FieldNameValueModel()
                    field.nameId = parseInt(prop)
                    field.value = $scope.formData[prop]

                    model.fields.push(field)
                })

                api.location.saveLocation(model).then(result => {
                    if (!result) {
                        CommonHelper.openInfoPopup(i18n('Popup', 'SchemaSaveError'), 'error', 5000)
                    }
                    else {
                        CommonHelper.openInfoPopup(i18n('Popup', 'SchemaSaved'))
                        $scope.isApplyButtonDisabled = true
                    }
                })
            }
        }

        var num = 0
        var addToForm = (item: SchemaElement) => {
            $scope.formData[item.idDataType] = item.value

            addValidationRules(item, num++)
        }

        var updateInput = (number: number, item: SchemaElement) => {
            $timeout(() => {
                if ($scope.imgPosition) {
                    var inputBox = $element.find('.input-box' + '-' + number)

                    inputBox.css({
                        position: 'absolute',
                        top: $scope.imgPosition.top + item.y * $scope.scaleY + 'px',
                        left: $scope.imgPosition.left + item.x * $scope.scaleX + 'px',
                        'font-size': item.height * $scope.scaleY + 'px'
                    })

                    inputBox.find('input').css({
                        'font-size': (item.height * 2 / 3) * $scope.scaleY + 'px',
                        padding: 0,
                        position: 'absolute',
                        top: 0,
                        left: '2px'
                    })
                }
            })
        }

        var changeBackgroundColor = (input: JQuery, color: string) => {
            input.css('background-color', color)
            input.find('input').css('background-color', color)
        }

        var checkCondition = (elementApperance: SchemaElementApperance, input: JQuery, currentValue: any, isChanged: boolean = false) => {
            switch (elementApperance.condition) {
                case CommonHelper.greaterThan:
                    if (currentValue > elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
                case CommonHelper.greaterOrEqual:
                    if (currentValue >= elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
                case CommonHelper.lowerThan:
                    if (currentValue < elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
                case CommonHelper.lowerOrEqual:
                    if (currentValue <= elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
                case CommonHelper.equal:
                    if (currentValue == elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
                case CommonHelper.notEqual:
                    if (currentValue != elementApperance.constraint1) {
                        changeBackgroundColor(input, elementApperance.color)
                        return true
                    }
                    else if (!isChanged) {
                        changeBackgroundColor(input, '#fff')
                    }
                    break
            }
            return false
        }

        var createItem = (editorType: string, number: number, item: SchemaElement, callback: (input) => void) => {
            localDictionary.getIdUnitByIdDataType(item.idDataType).then(unitId => {
                var unitName = ''
                if (item.isUnitVisible) {
                    for (var i = 0; i < $scope.units.length; i++) {
                        var unit = $scope.units[i]
                        if (unit.id == unitId) {
                            unitName = unit.caption
                            break
                        }
                    }
                }

                var input = {
                    cssClass: 'input-box input-box' + '-' + number,
                    dataField: item.idDataType,
                    editorType: editorType,
                    editorOptions: {
                        width: item.width * $scope.scaleX + 2,
                        height: item.height * $scope.scaleY + 2,
                        disabled: !item.isEditable,
                        value: item.value,
                        onChange: (e) => {
                            $scope.isApplyButtonDisabled = false
                            var input = $element.find('.input-box' + '-' + number).find('.dx-texteditor-container')

                            var isChanged = false
                            item.elementsApperance.forEach(ea => {
                                isChanged = checkCondition(ea, input, e.model.formData[item.idDataType], isChanged)
                            })

                            $scope.formData = e.model.formData
                        }
                    },
                    label: <any>{}
                }

                var unitText = ''
                if (unitName != '') unitText = ' [' + unitName + ']'

                if ($scope.source.isReadOnly) {
                    var text = ''

                    if (item.description) {
                        if (item.value)
                            text = item.description + unitText + ': ' + item.value
                        else
                            text = item.description + unitText
                    } else if (item.value) {
                        text = item.value + unitText
                    } else {
                        text = unitText
                    }
                    input['template'] = '<label>' + text + '</label>'
                    input.label.visible = false
                } else {
                    if (item.description) {
                        input.label = {
                            location: 'left',
                            text: item.description + ' ' + unitText,
                            showColon: true,
                            visible: true
                        }
                    } else {
                        input.label = {
                            location: 'right',
                            text: unitText,
                            showColon: false,
                            visible: true
                        }
                    }
                }

                callback(input)
            })
        }

        var addValidationRules = (item: SchemaElement, number: number) => {
            switch (item.type) {
                case CommonHelper.integer:
                    createItem('dxNumberBox', number, item, input => {
                        input['validationRules'] = []
                        input['validationRules'].push({
                            type: 'pattern',
                            pattern: '^[0-9]{0,}$',
                            message: i18n('Common', 'IntegerField')
                        })
                        $scope.items.push(input)
                    })
                    
                    break
                case CommonHelper.number:
                    var it = createItem('dxNumberBox', number, item, input => {
                        input['validationRules'] = []
                        input['validationRules'].push({
                            type: 'numeric',
                            message: i18n('Common', 'NumericField')
                        })
                        $scope.items.push(input)
                    })
                    break
                case CommonHelper.string:
                default:
                    createItem('dxTextBox', number, item, input => {
                        $scope.items.push(input)
                    })
                    break
            }
            updateInput(number, item)
        }        
    }]

    replace = true
}