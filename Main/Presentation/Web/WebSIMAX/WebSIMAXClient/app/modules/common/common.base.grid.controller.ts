﻿import { i18n } from './i18n/common.i18n'
import { EntityModel, ColumnModel, SimpleValueModel, ContextListModel } from '../api/api.models'
import { ImportOptions } from './import/import.directive'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonScope } from './common.extended-data-grid-options'
import { CommonExtendedDataGridOptions } from './common.extended-data-grid-options'
import { Api } from  './../api/api.module'
import { ButtonsStates } from './common.grid-buttons-options'
import { EventsNames } from '../../helpers/events-names'

export interface ScopeBaseGrid extends CommonScope {
    dataGridOptions: DevExpress.ui.dxDataGridOptions
    loadingIndicatorOptions: DevExpress.ui.dxLoadIndicatorOptions
    columnsCount: number
    importOptions: ImportOptions
    popupId: number
    filterBoxOptions?: DevExpress.ui.dxTagBoxOptions
    filterBox?: DevExpress.ui.dxTagBox
    isFilterBoxVisible?: boolean
    clearFilterButtonOptions?: DevExpress.ui.dxButtonOptions
    clearFilterButton?: DevExpress.ui.dxButton
    barContainerWidth
    buttonsBarWidth: number
    popoverListButtonOptions
    isSettingsListPopoverVisible: boolean
    settingsListPopoverOptions: DevExpress.ui.dxPopoverOptions
    settingsListOptions: DevExpress.ui.dxListOptions
    settingsListPopover: DevExpress.ui.dxPopover
    settingsList: DevExpress.ui.dxList
    permissions: any[]
    isPopoverListButtonClicked: boolean
    positionPopover: DevExpress.ui.dxPopover
    buttonsStates: ButtonsStates
    hideSettingsPopover: () => void
    showButtons: boolean
    customFilterSource: any
}

export class BaseGridController {
    gridOptions: CommonExtendedDataGridOptions

    settingsList: any = {
        show: ["ShowDetails", "DETAILS"],
        add: ["Add", "ADD"],
        edit: ["Edit", "EDIT"],
        refresh: ["Refresh", "REFRESH"],
        selection: ["Selection", "SELECTION"],
        columnFixing: ["ColumnFixing", "FORMAT_INCREASE"],
        columnChooser: ["ColumnChooser", "COLUMN_CHOOSER"],
        import: ["Import", "IMPORT"],
        export: ["Export", "EXPORT"],
        generateReport: ["ReportGenerate", "REPORT_GENERATE"],
        customFilter: ["CustomFilter","FILTER"]
    }

    settings: any[]
    settingsListArray: any[]
    constructor(protected $scope: ScopeBaseGrid, protected $timeout: ng.ITimeoutService, protected $compile: ng.ICompileService, drawGrid: () => void, protected api: Api) {
        this.$scope.showGrid = false
        $scope.popupId = new Date().getTime()
        $scope.gridId = $scope.$id

        $scope.$on('moduleChanging', () => {
            this.$scope.showGrid = false
        })

        $scope.dataGridOptions = {
            onInitialized: (e) => {
                $scope.dataGrid = e.component
                $scope.dataGrid.option('loadPanel.text', i18n('Grid', 'Loading'))
                drawGrid()
            },
            height: '100%'
        }

        var headerWatch: () => void = null
        $scope.$on('customButtonsGridAdded', (event, header: JQuery) => {
            var headerWidth = header.width()
            if (headerWatch) headerWatch()
            headerWatch = $scope.$watch(() => {
                var toolbar = angular.element(this.$scope.dataGrid.element).find('.dx-toolbar-items-container')
                var toolbarWidth = toolbar.width()
                var buttons = toolbar.find('.dx-toolbar-button')
                return toolbarWidth - headerWidth - angular.element(buttons.last()).width()
            }, (width) => {
                this.checkWidth(width)
            })
        })

        $scope.$watch('isFilterBoxVisible', (isFilterBoxVisible) => {
            if ($scope.dataGrid) {
                if (isFilterBoxVisible) {
                    $scope.dataGrid.option('height', 'calc(100% - 40px)')
                } else {
                    $scope.dataGrid.option('height', '100%')
                }
            }
        })

        $scope.clearFilterButtonOptions = {
            icon: 'clear',
            onInitialized: (e) => {
                $scope.clearFilterButton = e.component
            },
            onClick: (e) => {
                $scope.dataGrid.clearFilter()
                $scope.filterBox.reset()
            }
        }

        $scope.filterBoxOptions = {
            readOnly: true,
            onInitialized: (e) => {
                e.component.option("multiline", false)
                $scope.filterBox = e.component
            },

            showClearButton: false,
            placeholder: i18n("Grid", "NoActiveFilters"),
            disabled: true
        }

        $scope.$on(EventsNames.CHANGED_ALLOWED_DISTRIBUTORS, (event: ng.IAngularEvent) => {
            $scope.showGrid = false
            $scope.dataGrid.refresh()
        })

        $scope.loadingIndicatorOptions = {
            width: 40,
            height: 40
        }

        DevExpress.ui.dxDataGrid.defaultOptions({
            options: {
                noDataText: ''
            }
        })

        $scope.isLargeScreen = true
        $scope.isSettingsListPopoverVisible = false

        $scope.$watch(() => {
            return $scope.disabled
        }, val => {
            var elements = angular.element('.listItemTemplate').each((index, el) => {
                var instance = angular.element(el).dxButton('instance')
                if (instance)
                    instance.option('disabled', val)
            })
        })

        $scope.hideSettingsPopover = () => {
            if (this.$scope.settingsListPopover)
                this.$scope.settingsListPopover.option("visible", false)
            this.$scope.isSettingsListPopoverVisible = false
        }

        $scope.settingsListPopoverOptions = {
            target: '#popoverListButton',
            width: 250,
            bindingOptions: {
                visible: 'isSettingsListPopoverVisible'
            },
            onInitialized: e => {
                $scope.settingsListPopover = e.component
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            },
            onHidden: () => {
                this.$scope.hideSettingsPopover()
            }
        }

        $scope.isFilterBoxVisible = false

        $scope.filterPopoverOptions = {
            height: 250,
            width: 700,
            target: "#filterButton",
            bindingOptions: {
                visible: 'isFilterPopoverVisible'
            },
            onInitialized: (e) => {
                this.$scope.filterPopover = e.component
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'border-radius': 'inherit'
                })
            },
            onHidden: () => {
                this.$scope.isFilterPopoverVisible = false
            }
        }

        $scope.filterListPopupOptions = {
            height: 300,
            width: 300,
            bindingOptions: {
                visible: 'isFilterListPopupVisible'
            },
            onHidden: () => {
                this.$scope.isFilterListPopupVisible = false
            }
        }

        $scope.filterFormPopupOptions = {
            height: 300,
            width: 300,
            bindingOptions: {
                visible: 'isFilterFormPopupVisible'
            },
            onHidden: () => {
                this.$scope.isFilterFormPopupVisible = false
            },
            onShowing: () => {
                this.$scope.filterForm.resetValues()
                this.$scope.filterForm.option("items", this.$scope.formItems)
                this.$scope.filterForm.repaint()
            },
            onInitialized: e => {
                $scope.filterFormPopup = e.component
            }
        }
    }

    protected refreshFilters(columns?: ColumnModel[]) {
        this.$timeout(() => {
            var filters = this.gridOptions.getWhereClause(this.$scope.dataGrid.getCombinedFilter()).items

            this.$scope.filterBox.option("value", filters.map((item) => {
                    var column = columns.filter(col => { return col.name.toUpperCase() == item.id.toUpperCase() })
                    if (column.length > 0)
                        return item.where.replace(new RegExp(column[0].name.toUpperCase(), 'g'), column[0].caption)
                    return item.where
                })
            )

            this.$scope.isFilterBoxVisible = filters.length > 0
            this.$scope.filterBox.repaint()
        }, 0)
    }

    protected getContextList(destinationType: number) {
        if (this.$scope.dataGrid) {
            var selectedRows = this.$scope.dataGrid.getSelectedRowsData()
            var ids = selectedRows.map(selectedRow => { return selectedRow[CommonHelper.idField] })
            var contextListModel = new ContextListModel()
            contextListModel.sourceType = CommonHelper.IdLocation
            contextListModel.destinationType = destinationType
            contextListModel.referenceValues = ids
            this.api.app.getContextList(contextListModel).then(result => {
                //method in WS not implemented yet
            })
            //switch (destinationType) {
            //    case CommonHelper.IdDevice:
            //        this.$state.go('devices')
            //        break
            //    case CommonHelper.IdMeter:
            //        this.$state.go('meters')
            //        break
            //    case CommonHelper.IdTask:
            //        this.$state.go('tasks')
            //        break
            //    case CommonHelper.IdIssue:
            //        this.$state.go('issues')
            //        break
            //    case CommonHelper.IdSimCard:
            //        //TODO
            //        break
            //    case CommonHelper.IdAction:
            //        //TODO
            //        break
            //}

        }
    }

    private pushBasicSettingsListItems = (permissions) => {
        var [ADD, EDIT, EXPORT, IMPORT, REPORT_GENERATE] = permissions
        var selectedRowsData = this.$scope.dataGrid.getSelectedRowsData()
        this.$scope.disabled = selectedRowsData.length == 0
        var items = []
        this.settingsListArray = []
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.columnFixing[1], '#000000', '18', i18n('Grid', this.settingsList.columnFixing[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.columnFixing)
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.show[1], '#000000', '18', i18n('Grid', this.settingsList.show[0]), 'context-menu-row'), disabled: this.$scope.disabled
        })
        this.settingsListArray.push(this.settingsList.show)
        if (ADD) {
            items.push({
                template: CommonHelper.getIconForDetails(this.settingsList.add[1], '#000000', '18', i18n('Grid', this.settingsList.add[0]), 'context-menu-row')
            })
            this.settingsListArray.push(this.settingsList.add)
        }
        if (EDIT) {
            items.push({
                template: CommonHelper.getIconForDetails(this.settingsList.edit[1], '#000000', '18', i18n('Grid', this.settingsList.edit[0]), 'context-menu-row'), disabled: this.$scope.disabled
            })
            this.settingsListArray.push(this.settingsList.edit)
        }
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.selection[1], '#000000', '18', i18n('Grid', this.settingsList.selection[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.selection)
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.refresh[1], '#000000', '18', i18n('Grid', this.settingsList.refresh[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.refresh)
        if (IMPORT) {
            items.push({
                template: CommonHelper.getIconForDetails(this.settingsList.import[1], '#000000', '18', i18n('Grid', this.settingsList.import[0]), 'context-menu-row')
            })
            this.settingsListArray.push(this.settingsList.import)
        }
        if (EXPORT) {
            items.push({
                template: CommonHelper.getIconForDetails(this.settingsList.export[1], '#000000', '18', i18n('Grid', this.settingsList.export[0]), 'context-menu-row')
            })
            this.settingsListArray.push(this.settingsList.export)
        }        
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.columnChooser[1], '#000000', '18', i18n('Grid', this.settingsList.columnChooser[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.columnChooser)
        if (REPORT_GENERATE) {
            items.push({
                template: CommonHelper.getIconForDetails(this.settingsList.generateReport[1], '#000000', '18', i18n('Grid', this.settingsList.generateReport[0]), 'context-menu-row'), disabled: this.$scope.disabled
            })
            this.settingsListArray.push(this.settingsList.generateReport)
        }  
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.customFilter[1], '#000000', '18', i18n('Grid', this.settingsList.customFilter[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.customFilter)  
        return items
    }

    private pushMapItem = (items: any[]) => {
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.showMap[1], '#000000', '18', i18n('Grid', this.settingsList.showMap[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.showMap)
        return items
    }

    private pushDetailsPreviewItem = (items: any[]) => {
        items.push({
            template: CommonHelper.getIconForDetails(this.settingsList.showPreview[1], '#000000', '18', i18n('Grid', this.settingsList.showPreview[0]), 'context-menu-row')
        })
        this.settingsListArray.push(this.settingsList.showPreview)
        return items
    }

    protected setSettingsListOptions = (permissions, eventName, moduleName?, showPreviewDetails?, showMapDetails?) => {
        var items = this.pushBasicSettingsListItems(permissions)
        if (showMapDetails)
            items = this.pushMapItem(items)
        if (showPreviewDetails)
            items = this.pushDetailsPreviewItem(items)
        this.settings = items
        this.$scope.settingsListOptions = {
            items: this.settings.map(setting => setting.disabled),
            itemTemplate: (...args) => {
                this.createItemTemplate(args, eventName, moduleName, showPreviewDetails, showMapDetails)
            },
            onInitialized: e => {
                this.$scope.settingsList = e.component
            }
        }
    }

    protected onPopoverListButtonClick = () => {
        this.$scope.isSettingsListPopoverVisible = !this.$scope.isSettingsListPopoverVisible
        if (this.$scope.isSettingsListPopoverVisible)
            this.$scope.settingsListPopover.show()
        else
            this.$scope.settingsListPopover.hide()
        this.$scope.isPopoverListButtonClicked = true
    }

    protected hideGridsterPopover() {
        var component = angular.element('#SecondRowPositionPopover').dxPopover('instance')
        component.option('visible', false)
    }

    private executeAction = (action, index, eventName, moduleName, showPreviewDetails?, showMapDetails?) => {
        switch (action) {
            case this.settingsList.showPreview:
                this.$scope.positionPopover.option('position', 'left')
                if (this.$scope.positionPopover.option('visible'))
                    this.hideGridsterPopover()
                else
                    showPreviewDetails('#listItemTemplate' + index)
                break
            case this.settingsList.showMap:
                this.$scope.positionPopover.option('position', 'left')
                if (this.$scope.positionPopover.option('visible'))
                    this.hideGridsterPopover()
                else
                    showMapDetails('#listItemTemplate' + index)
                break
            case this.settingsList.columnFixing:
                this.$scope.hideSettingsPopover()
                this.changeColumnFixing(this.$scope.dataGrid)
                break
            case this.settingsList.show:
                this.$scope.showDetails({ data: this.$scope.dataGrid.getSelectedRowsData()[0] })
                this.$scope.hideSettingsPopover()
                break
            case this.settingsList.add:
                this.$scope.hideSettingsPopover()
                if (eventName)
                    this.$scope.$broadcast(eventName + this.$scope.popupId, null)
                break
            case this.settingsList.edit:
                this.$scope.hideSettingsPopover()
                if (eventName)
                    this.$scope.$broadcast(eventName + this.$scope.popupId, this.$scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
                break
            case this.settingsList.selection:
                this.$scope.buttonsStates.selectionPopover.option('target', '#listItemTemplate' + index)
                this.$scope.buttonsStates.selectionPopover.option('position', 'left')
                this.$scope.buttonsStates.selectionPopoverVisible = !this.$scope.buttonsStates.selectionPopoverVisible
                break
            case this.settingsList.refresh:
                this.$scope.hideSettingsPopover()
                this.$scope.dataGrid.refresh()
                break
            case this.settingsList.import:
                this.$scope.hideSettingsPopover()
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP, CommonHelper.importInPopup, this.$scope.hideSettingsPopover)
                break
            case this.settingsList.export:
                this.$scope.hideSettingsPopover()
                this.exportXls('locations')
                break
            case this.settingsList.columnChooser:
                this.$scope.hideSettingsPopover()
                this.$scope.dataGrid.showColumnChooser()
                break
            case this.settingsList.generateReport:
                this.$scope.hideSettingsPopover()
                this.$scope.$broadcast(EventsNames.REPORT_GENERATE, this.$scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])                    
                break
            case this.settingsList.customFilter:
                this.$scope.hideSettingsPopover()
                this.$scope.isFilterListPopupVisible = true
        }
    }

    updateDataSource(result: EntityModel, grid: DevExpress.ui.dxDataGrid, gridOptions: DevExpress.ui.dxDataGridOptions) {
        var items = gridOptions.dataSource._items

        if (!result)
            return false

        for (var i = 0; i < items.length; i++) {
            var item = items[i]

            if (item[CommonHelper.idField] == result.id) {
                result.fields.forEach(f => {
                    var value = f.value
                    if (f.value == 'True')
                        value = true
                    else if (f.value == 'False')
                        value = false
                    item[f.id] = value
                })
                return true
            }
        }

        grid.option(gridOptions)
        grid.refresh()

        return false
    }

    changeColumnFixing(grid: DevExpress.ui.dxDataGrid) {
        var columnFixing = grid.option('columnFixing')
        var current = columnFixing ? columnFixing.enabled : false

        var columns = []

        for (var i = 0; i < grid.columnCount(); i++) {
            columns.push(grid.columnOption(i))
        }

        grid.option({
            columns: columns,
            columnAutoWidth: !current,
            columnFixing: {
                enabled: !current
            }
        })
    }

    protected afterFilteringGridHeightFix = (component: DevExpress.ui.dxDataGrid) => {
        let totalHeight = angular.element(component.element).find('.dx-scrollable-content').height()
        let rowsCount = this.$scope.dataGrid.totalCount()
        var rows = angular.element(component.element).find('.dx-datagrid-table tr')
        let totalRowsHeight = rowsCount * rows.height()
        let lastRowHeight = totalHeight - totalRowsHeight
        if (lastRowHeight > 0 && rowsCount > 0) {
            rows.last().height(lastRowHeight - 4)
        }
    }

    private checkWidth = (width: number) => {
        this.$timeout(() => {
            if (width <= 0) {
                this.$scope.isLargeScreen = false
            }
            else {
                this.$scope.hideSettingsPopover()
                this.$scope.isLargeScreen = true
            }
            if (this.$scope.positionPopover && this.$scope.positionPopover.option("visible")) {
                this.$scope.positionPopover.hide()
            }
            if (this.$scope.buttonsStates.selectionPopover && this.$scope.buttonsStates.selectionPopover.option("visible")) {
                this.$scope.buttonsStates.selectionPopover.hide()
            }
            if (this.$scope.filterPopover && this.$scope.filterPopover.option("visible")) {
                this.$scope.filterPopover.hide()
            }
        }, 0)
    }

    protected exportXls = (moduleName: string) => {
        if (this.$scope.dataGrid) {
            var gridOptions = <CommonExtendedDataGridOptions>this.$scope.dataGridOptions
            var req = gridOptions.getRequest(0, 0, true)
            this.api.app.saveRequestModel(req).then(token => {
                if (token) {
                    CommonHelper.setJWTCookie(this.api.login.getToken())
                    window.open(window['config'].API_URL + 'api/app/' + moduleName + '/export/' + token, '_self');
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', moduleName + 'ExportError'), 'error', 5000)
                }
            })
        }
    }

    protected createItemTemplate = (args, eventName, moduleName, showPreviewDetails?, showMapDetails?) => {
        var generateTemplate = (index) => {
            var onButtonClick = () => {
                this.executeAction(this.settingsListArray[args[index]], args[index], eventName, moduleName, showPreviewDetails, showMapDetails)
            }
            var el: JQuery
            if (this.settings[args[index]].disabled) {
                args[index + 1].append('<div class="listItemTemplate" id="listItemTemplate' + args[index] + '"></div>')
                el = angular.element('#listItemTemplate' + args[index])
                el.dxButton(<DevExpress.ui.dxButtonOptions>{
                    template: this.$compile(this.settings[args[index]].template)(this.$scope),
                    disabled: this.$scope.disabled,
                    onClick: e => {
                        onButtonClick()
                    }
                })
            }
            else {
                args[index + 1].append('<div id="listItemTemplate' + args[index] + '"></div>')
                el = angular.element('#listItemTemplate' + args[index])
                el.dxButton(<DevExpress.ui.dxButtonOptions>{
                    template: this.$compile(this.settings[args[index]].template)(this.$scope),
                    onClick: e => {
                        onButtonClick()
                    }
                })
            }
            el.css({
                padding: '5px 10px',
                border: 0,
                'border-radius': 0,
                margin: 0,
                width: "100%"
            })
            el.parent().css({
                padding: 0,
                margin: 0
            })
        }
        if (args[2]) {
            generateTemplate(1)
        }
        else {
            generateTemplate(0)
        }
    }
}