﻿import 'angular'
import { Api } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { ColumnModel } from '../../api/api.models'
import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { EventsNames } from '../../../helpers/events-names'

require('./connected-object.scss')

export interface ConnectedObject {
    connectedObjectId: number
    connectedObjectName: string
    connectedObjectVisibility: boolean
    connectedObjectTitle: string
    connectedObjectMethod: string
}

interface ConnectedObjectScope extends ng.IScope {
    connection: ConnectedObject
}

export class ConnectedObjectDirective implements ng.IDirective {
    constructor() {
    }

    restrict = 'E'
    scope = {
        connection: '='
    }
    controller = ['$scope', ($scope: ng.IScope) => {
        $scope['deviceDetails'] = (id, title) => {
            var itemIds = [{ id: id, title: title }]
            $scope.$broadcast(EventsNames.OPEN_DEVICES_DETAILS_POPUP + $scope.$id, id, itemIds)
        }
        $scope['meterDetails'] = (id, title) => {
            var itemIds = [{ id: id, title: title }]
            $scope.$broadcast(EventsNames.OPEN_METER_DETAILS_POPUP + $scope.$id, id, itemIds)
        }
    }]
    template = require("./connected-object.tpl.html")
}