﻿import { CommonHelper } from '../../helpers/common-helper'
import { i18n } from './i18n/common.i18n'
import { EventsNames } from '../../helpers/events-names'
import { ScopeBaseGrid } from './common.base.grid.controller'

export interface ButtonConfig {
    name: string
    idSelector?: string
    options: DevExpress.ui.dxButtonOptions,
    customAppendix?: string
}

export interface ButtonsStates {
    editDisabled: boolean
    showDisabled: boolean
    selectionPopoverVisible: boolean
    selectionPopoverOptions: DevExpress.ui.dxPopoverOptions
    selectionListOptions: DevExpress.ui.dxListOptions
    selectionPopover: DevExpress.ui.dxPopover    
}

interface Options {
    scopeName: string
    getDataGrid: () => DevExpress.ui.dxDataGrid
    onShowClick?: (e) => void
    onAddClick?: (e) => void
    onEditClick?: (e) => void
    onRefreshClick?: (e) => void
    onImportClick?: (e) => void
    onExportClick?: (e) => void
    onColumnFixingClick?: (e) => void
    onSaveColumnSettingsClick?: (e) => void
    onLoadColumnSettingsClick ?:(e) => void
    onPopoverListButtonClick?: () => void
    onGenerateReportClick?: () => void
    permissions?: {
        show?: string,
        add?: string,
        edit?: string,
        import?: string
        export?: string
        generateReport?: string
    }
    hideEditButton?: boolean,
    hideAddButton?: boolean,
    hideGenerateReportButton?: boolean
}

export class CommonGridButtonsOptions {
    buttonsStates: ButtonsStates
    buttonsOptions: ButtonConfig[]
    popoverListButtonOptions: ButtonConfig
    testOptions: Options
    selectionPopover: DevExpress.ui.dxPopover
    get dataGrid() {
        return this.options.getDataGrid()
    }

    constructor(private $scope: ScopeBaseGrid, private options: Options) {

        var selectionOptions = {
            all: i18n("Grid", "SelectionAll"),
            none: i18n("Grid", "SelectionNone"),
            invert: i18n("Grid", "SelectionInvert")
        }

        this.buttonsStates = {
            editDisabled: true,
            showDisabled: true,
            selectionPopoverVisible: false,
            selectionPopoverOptions: {
                target: '#' + options.scopeName + '-selection-button',
                width: 100,
                bindingOptions: {
                    visible: 'buttonsStates.selectionPopoverVisible'
                },
                onShowing: e => {
                    e.component.content().css({
                        'padding': 0,
                        'text-align': 'center',
                        'border-radius': 'inherit'
                    })
                },
                onInitialized: e => {
                    this.buttonsStates.selectionPopover = e.component
                }
            },
            selectionPopover: undefined,
            selectionListOptions: {
                items: [selectionOptions.all, selectionOptions.invert, selectionOptions.none],
                onItemClick: e => {
                    this.$scope.hideSettingsPopover()
                    this.buttonsStates.selectionPopoverVisible = false
                    this.dataGrid.beginUpdate()
                    switch (e.itemData) {
                        case selectionOptions.all:
                            this.dataGrid.selectRows(this.dataGrid.option('dataSource').items(), true)
                            break
                        case selectionOptions.none:
                            this.dataGrid.clearSelection()
                            break
                        case selectionOptions.invert:
                            var selected = this.dataGrid.getSelectedRowKeys().slice()
                            var deselectKeys = []
                            var selectKeys = []
                            this.dataGrid.option('dataSource').items().forEach(key => {
                                if (this.dataGrid.isRowSelected(key)) {
                                    deselectKeys.push(key)
                                } else {
                                    selectKeys.push(key)
                                }
                            })
                            this.dataGrid.deselectRows(deselectKeys)
                            this.dataGrid.selectRows(selectKeys, true)
                            break
                    }
                    this.dataGrid.endUpdate()
                }
            }
        }

        this.buttonsOptions = []

        var columnFixingButton = {
            name: "columnFixingButton",
            options: {
                onClick: options.onColumnFixingClick,
                template: CommonHelper.getIcon('FORMAT_INCREASE'),
                hint: i18n('Grid', 'ColumnFixing')
            }
        }
        this.buttonsOptions.push(columnFixingButton)

        var showButton = {
            name: "showOptions",
            options: {
                onClick: options.onShowClick,
                template: CommonHelper.getIcon('DETAILS'),
                hint: i18n("Grid", "ShowDetails"),
                bindingOptions: {
                    disabled: 'buttonsStates.editDisabled'
                }
            }
        }
        if (options.permissions && options.permissions.show)
            showButton['permissions'] = options.permissions.show
        this.buttonsOptions.push(showButton)

        var addButton = {
            name: "addOptions",
            options: {
                onClick: options.onAddClick,
                template: CommonHelper.getIcon('ADD'),
                hint: i18n("Grid", "Add")
            }
        }
        if (options.permissions && options.permissions.add)
            addButton['permissions'] = options.permissions.add
        if (options.hideAddButton !== true)
            this.buttonsOptions.push(addButton)

        var editButton = {
            name: "editOptions",
            options: {
                onClick: options.onEditClick,
                template: CommonHelper.getIcon('EDIT'),
                hint: i18n("Grid", "Edit"),
                bindingOptions: {
                    disabled: 'buttonsStates.editDisabled'
                }
            }
        }
        if (options.permissions && options.permissions.edit)
            editButton['permissions'] = options.permissions.edit
        if (options.hideEditButton !== true)
            this.buttonsOptions.push(editButton)

        var selectOptions = {
            name: "selectionOptions",
            options: {
                onClick: () => {
                    this.buttonsStates.selectionPopover.option('target', '#' + options.scopeName + '-selection-button')
                    this.buttonsStates.selectionPopover.option('position', 'bottom')
                    this.buttonsStates.selectionPopoverVisible = !this.buttonsStates.selectionPopoverVisible
                },
                hint: i18n("Grid", "Selection"),
                template: CommonHelper.getIcon('SELECTION'),
                rtlEnabled: true
            },
            idSelector: options.scopeName + "-selection-button",
            customAppendix: '<div dx-popover="buttonsStates.selectionPopoverOptions"><div dx-list="buttonsStates.selectionListOptions"></div></div>'
        }
        this.buttonsOptions.push(selectOptions)

        var refreshButton = {
            name: "refreshOptions",
            options: {
                onClick: options.onRefreshClick,
                template: CommonHelper.getIcon('REFRESH'),
                hint: i18n("Grid", "Refresh")
            }
        }
        this.buttonsOptions.push(refreshButton)

        var importButton = {
            name: "importOptions",
            options: {
                onClick: options.onImportClick,
                hint: i18n("Grid", "Import"),
                template: CommonHelper.getIcon('IMPORT'),
            },
            idSelector: 'importButton'
        }

        if (options.permissions && options.permissions.import)
            importButton['permissions'] = options.permissions.import
        this.buttonsOptions.push(importButton)

        var exportButton = {
            name: "exportOptions",
            options: {
                onClick: options.onExportClick,
                hint: i18n("Grid", "Export"),
                template: CommonHelper.getIcon('EXPORT'),
            }
        }
        if (options.permissions && options.permissions.export)
            exportButton['permissions'] = options.permissions.export
        this.buttonsOptions.push(exportButton)
        var columnChooserButton = {
            name: "columnChooserButton",
            options: {
                onClick: () => {
                    this.$scope.dataGrid.showColumnChooser()
                },
                hint: i18n("Grid", "ColumnChooser"),
                template: CommonHelper.getIcon('COLUMN_CHOOSER')
            }
        }

        this.buttonsOptions.push(columnChooserButton)

        /*
        var saveColumnSettings = {
            name: "saveColumnSettings",
            options: {
                onClick: options.onSaveColumnSettingsClick,
                hint: i18n("Grid", "SaveColumnSettings"),
                template: CommonHelper.getIcon('IMPORT'),
            },
            idSelector: 'saveColumnSettingsButton'
        }
        this.buttonsOptions.push(saveColumnSettings)
        */

        var filterButton = {
            name: "filterButton",
            options: {
                onClick: () => {
                    this.$scope.isFilterPopoverVisible = !this.$scope.isFilterPopoverVisible
                    if (this.$scope.isFilterPopoverVisible) {
                        this.$scope.filterPopover.show()
                    }
                    else {
                        this.$scope.filterPopover.hide()
                    }
                },
                hint: i18n("Grid", "CustomFiltering"),
                template: CommonHelper.getIcon('FILTER')
            },
            idSelector: 'filterButton'
        }
        this.buttonsOptions.push(filterButton)
        this.buttonsOptions.reverse()

        this.popoverListButtonOptions = {
            name: "popoverListButtonOptions",
            options: {
                onClick: options.onPopoverListButtonClick,
                template: CommonHelper.getIcon('SHOW_OPTIONS')
            },
            idSelector: 'popoverListButton'
        }

        var generateReportButton = {
            name: "generateReportOptions",
            options: {
                onClick: options.onGenerateReportClick,
                template: CommonHelper.getIcon('REPORT_GENERATE'),
                hint: i18n("Grid", "GenerateReport"),
                bindingOptions: {
                    disabled: 'buttonsStates.editDisabled'
                }
            }
        }
        if (options.permissions && options.permissions.generateReport) {
            generateReportButton['permissions'] = options.permissions.generateReport
            this.buttonsOptions.push(generateReportButton)
        }

        $scope.$watch(() => {
            return (!this.dataGrid || this.dataGrid.getSelectedRowKeys().length != 1)
        }, value => {
            this.buttonsStates.editDisabled = value
            this.buttonsStates.showDisabled = value
            })
    }
}