import { UserModel } from '../../api/api.models'
import { CommonHelper } from '../../../helpers/common-helper'

var translate = (collection: string, word: string) => {
    var user: UserModel = CommonHelper.getValueFromLocalStorage('USER'), idDistributor
    if (user) idDistributor = user.idDistributor

    var resource = global['resource']
    word = word.toLowerCase()
    var distributorCollection = idDistributor ? 'Distributor' + idDistributor : undefined
    if (resource && distributorCollection && resource[distributorCollection] && resource[distributorCollection][word]) return resource[distributorCollection][word]
    else if (resource && resource[collection] && resource[collection][word]) return resource[collection][word]
    else return word
}

export var i18n = translate