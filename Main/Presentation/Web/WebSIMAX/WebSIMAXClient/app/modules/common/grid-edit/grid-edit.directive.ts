﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, ObjectReferenceModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridSelectBridge } from '../../common/grid-select/grid-select.directive'
import { GridBridge } from '../../common/common.interfaces'
import { CommonScope } from '../common.extended-data-grid-options'
import { i18n} from '../i18n/common.i18n'
import { KeydownDispatcher } from '../../../helpers/keydown-dispatcher'

require('./grid-edit.scss')

interface EditReference {
    editReferenceButtonDisabled: boolean

    popover: DevExpress.ui.dxPopover
    popoverOptions: DevExpress.ui.dxPopoverOptions
    popoverVisible: boolean

    popoverGrid: DevExpress.ui.dxDataGrid
    popoverGridOptions: DevExpress.ui.dxDataGridOptions

    popoverObjectId: number

    isUnknownReferenceTable: boolean
}

interface GroupsEditScope extends CommonScope {
    gridOptions: DevExpress.ui.dxDataGridOptions
    grid: DevExpress.ui.dxDataGrid
    columnsCount: number

    source: GridBridge
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean

    editReference: EditReference
}

export class GridEditDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        source: '='
    }
    template = require("./grid-edit.tpl.html")

    link = (scope, element, attrs) => {
        scope.$watch("source", value => {
            scope.source = value
        })
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope: GroupsEditScope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        $scope.popupVisible = false
        $scope.popupOptions = {
            showTitle: true,
            showCloseButton: true,
            bindingOptions: {
                visible: 'popupVisible',
                title: 'source.title'
            },
            onHiding: () => {
                $scope.$root.$broadcast(EventsNames.CLOSE_GRID_EDIT_POPUP + $scope.source.popupId)
                KeydownDispatcher.getInstance().removeListener(KeydownDispatcher.crtlA, CommonHelper.crtlAGridEditFunc)
            }
        }

        var dataSource
        $scope.$on(EventsNames.OPEN_GRID_EDIT_POPUP + $scope.source.popupId, (event: ng.IAngularEvent, ...args: any[]) => {
            load(args).then(() => {
                if ($scope.source.items) {
                    $scope.source.items.forEach((element, index) => {
                        element["VisibleIndex"] = index
                        $scope.gridOptions.dataSource.store().insert(element)
                    })
                    
                    $scope.grid.option($scope.gridOptions)  
                    $scope.grid.refresh()   
                    dataSource = $scope.gridOptions.dataSource.store()               
                }
            })
            $scope.popupVisible = true
        })

        var createEditReferenceObjectColumn = (hint: string, columnsName: string, serviceName: string, methodName: string, srcServiceName: string, srcMethodName: string, gridName: string) => {
            var editReferenceObjectColumn: ColumnModel = <any>{
                idName: 'editReferenceObject',
                cellTemplate: (container, options) => {
                    if (options.data.referenceType != "" && !options.data.isNew) {
                        let buttonOptions = {
                            hint: hint,
                            icon: 'edit',
                            onClick: (e) => {
                                $scope.editReference.popover.option('target', e.element[0])
                                $scope.editReference.popoverVisible = !$scope.editReference.popoverVisible
                                $scope.editReference.popoverObjectId = options.data[CommonHelper.idField]
                                getData(columnsName, serviceName, methodName, srcServiceName, srcMethodName, gridName)
                            }
                        }

                        let button = $('<div class="edit-reference-button"></div>')
                        button.dxButton(buttonOptions)
                        button.appendTo(container)
                    }
                },
                name: '',
                referenceTypeId: null,
                sortDesc: false,
                type: 0,
                typeName: '',
                unitId: null,
                unitName: '',
                where: null,
                width: 70,
                isEditable: false,
                alignment: 'center',
                allowSorting: false
            }
            return editReferenceObjectColumn
        }

        var getColumns = (): ng.IPromise<any> => {
            return localDictionary[$scope.source.dictionaryMethodName]().then(defaultColumns => {
                $scope.columnsCount = defaultColumns.length
                var columns = defaultColumns.map((value) => {
                    var column = {
                        dataField: value.name.toLocaleLowerCase(),
                        caption: value.caption,
                        dataType: CommonHelper.getDataType(value)
                    }
                    return column
                })
                return columns
            })
        }

        var getMetersColumns = () => {
            return getColumns().then(columns => {
                var editReferenceObjectColumn = createEditReferenceObjectColumn(i18n('Popup', 'ConnectDevice'), 'getDevicesDefaultColumns', 'device', 'getDevices', 'meter', 'changeCurrentDevice', 'DevicesJoinGrid')
                columns.push(editReferenceObjectColumn)
                return columns
            })
        }

        var getDevicesColumns = () => {
            return getColumns().then(columns => {
                var editReferenceObjectColumn = createEditReferenceObjectColumn(i18n('Popup', 'ConnectMeter'), 'getMeterDefaultColumns', 'meter', 'getMeters', 'device', 'changeCurrentMeter', 'MetersJoinGrid')
                columns.push(editReferenceObjectColumn)
                return columns
            })
        }

        var getRoutePointsColumns = () => {
            return getColumns().then(columns => {
                columns.forEach(column => {
                    column.allowSorting = false
                })
                columns.push({
                    dataField: "VisibleIndex",
                    caption: "VisibleIndex",
                    allowSorting: false,
                    visible: false,
                    sortIndex: 0, sortOrder: 'asc'
                })
                return columns
            }) 
        }

        var initializeGrid = (columns, deferred) => {
            var customOptions = {
                columns: columns,
                onInitialized: e => {
                    $scope.grid = e.component
                    deferred.resolve()
                }
            }
            $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, null, customOptions)
        }

        var initalizeRoutePointsGrid = (columns, deferred) => {
            var customOptions = {
                columns: columns,                
                onInitialized: e => {
                    $scope.grid = e.component
                    deferred.resolve()
                },
                onContentReady: (e) => {
                    initDragging(e);
                }
            }
            $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, null, customOptions)
        }

        var load = (args: any = null) => {
            var deferred = $q.defer()
            var func = []
            func[CommonHelper.dispatcherIndex] = CommonHelper.crtlAGridEditFunc
            func[CommonHelper.dispatcherFunction] = (e) => {
                $scope.grid.selectAll()
            }
            if (args == null || args.length == 0) {
                getColumns().then(columns => {
                    initializeGrid(columns, deferred)
                    KeydownDispatcher.getInstance().addListener(KeydownDispatcher.crtlA, func)
                })

            }
            else {
                switch (args[0])
                {
                    case "locationDevices":
                        getDevicesColumns().then(columns => {
                            initializeGrid(columns, deferred)
                            //KeydownDispatcher.getInstance().addListener(KeydownDispatcher.crtlA, func)
                        })
                        break
                    case "locationMeters":
                        getMetersColumns().then(columns => {
                            initializeGrid(columns, deferred)
                            //KeydownDispatcher.getInstance().addListener(KeydownDispatcher.crtlA, func)
                        })
                        break
                        
                    case "routePoints":
                        getRoutePointsColumns().then(columns => {
                            initalizeRoutePointsGrid(columns, deferred)
                            //KeydownDispatcher.getInstance().addListener(KeydownDispatcher.crtlA, func)
                        })
                        break
                }
                KeydownDispatcher.getInstance().addListener(KeydownDispatcher.crtlA, func)
            }
            return deferred.promise
        }

        //editReference
        $scope.editReference = <EditReference>{}
        $scope.editReference.popoverOptions = {
            width: 600,
            height: 500,
            onInitialized: (e) => {
                $scope.editReference.popover = e.component
            },
            target: '.edit-reference-button',
            bindingOptions: {
                visible: 'editReference.popoverVisible'
            },
            onHiding: () => {
                $scope.editReference.popoverVisible = false
                $scope.editReference.isUnknownReferenceTable = true
            }
        }

        var createReferenceGrid = (apiReq, gridName, onRowClick, columns, columnsFromWS: boolean = false) => {
            var customGridOptions = {
                name: gridName,
                defaultColumnsDictionary: columns,
                columnsDictionary: columns,
                onRowDoubleClick: onRowClick,
                onInitialized: (e) => {
                    $scope.editReference.popoverGrid = e.component
                },
                allowColumnReordering: false,
                selection: { mode: 'single' }
            }
            $scope.editReference.popoverGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, null, apiReq, customGridOptions)
            $timeout(() => {
                if ($scope.editReference.popoverGrid) {
                    $scope.editReference.popoverGrid.option($scope.editReference.popoverGridOptions)
                    $scope.editReference.popoverGrid.refresh()
                }
            }, 200)
        }

        // getting data to grid in popover (editting reference object)
        var getData = (columnsName: string, serviceName: string, methodName: string, srcServiceName: string, srcMethodName: string, gridName: string) => {
            $scope.editReference.isUnknownReferenceTable = false

            localDictionary[columnsName]().then(columns => {
                var apiReq = (request) => {
                    return api[serviceName][methodName](request)
                }

                var onClickFn = (e: any) => {
                    $scope.editReference.popoverVisible = false

                    var model = new ObjectReferenceModel()
                    model.id = $scope.editReference.popoverObjectId
                    model.referenceObjectId = e.data[CommonHelper.idField]

                    api[srcServiceName][srcMethodName](model).then(result => {
                        if (!result) {
                            CommonHelper.openInfoPopup(i18n('Popup', 'ChangeReferenceError'), 'error', 5000)
                        }
                    })
                }
                createReferenceGrid(apiReq, gridName, onClickFn, columns, true)
            })
        }


        //end edit reference

        $scope.$watchCollection('source.items', (items: any[]) => {
            if ($scope.grid) {
                $scope.grid.option("dataSource").store().clear()
                items.forEach((item) => {
                    $scope.grid.option("dataSource").store().insert(item)
                })
                $scope.grid.refresh()
            }
        })

        var initDragging = (e) => {
            var $gridElement = e.element
            $gridElement.find('.myRow').draggable({
                helper: 'clone',
                start: function (event, ui) {
                    var $originalRow = $(this),
                        $clonedRow = ui.helper;
                    var $originalRowCells = $originalRow.children(),
                        $clonedRowCells = $clonedRow.children();
                    for (var i = 0; i < $originalRowCells.length; i++)
                        $($clonedRowCells.get(i)).width($($originalRowCells.get(i)).width());
                    $clonedRow
                        .width($originalRow.width())
                        .addClass('drag-helper');
                }
            });
            $gridElement.find('.myRow').droppable({
                drop: function (event, ui) {
                    var draggingRowKey = ui.draggable.data('keyValue');
                    var targetRowKey = $(this).data('keyValue');
                    var draggingIndex = null,
                        targetIndex = null;

                    var dataItems = null
                    dataSource.load().done(function (data) {
                        dataItems = data;
                    });
                    for (var dataIndex = 0; dataIndex < dataItems.length; dataIndex++) {
                        if (dataItems[dataIndex].VisibleIndex == null)
                            dataItems[dataIndex].VisibleIndex = dataIndex
                    }

                    dataSource.byKey(draggingRowKey).done(function (item) {
                        draggingIndex = item.VisibleIndex;
                    });
                    dataSource.byKey(targetRowKey).done(function (item) {
                        targetIndex = item.VisibleIndex;
                    });
                    var draggingDirection = (targetIndex < draggingIndex) ? 1 : -1;
                    
                    for (var dataIndex = 0; dataIndex < dataItems.length; dataIndex++) {                        
                        if ((dataItems[dataIndex].VisibleIndex > Math.min(targetIndex, draggingIndex)) && (dataItems[dataIndex].VisibleIndex < Math.max(targetIndex, draggingIndex))) {
                            dataItems[dataIndex].VisibleIndex += draggingDirection;
                        }
                    }
                    dataSource.update(draggingRowKey, {
                        VisibleIndex: targetIndex
                    });
                    dataSource.update(targetRowKey, {
                        VisibleIndex: targetIndex + draggingDirection
                    });                    
                    $scope.grid.refresh()
                    
                    //MAKE SUBMIT BTN ACTIVE 
                    $scope.$broadcast(EventsNames.ROUTE_POINT_DRAG_AND_DROP + $scope.source.id)
                }
            });
        }
    }]
}