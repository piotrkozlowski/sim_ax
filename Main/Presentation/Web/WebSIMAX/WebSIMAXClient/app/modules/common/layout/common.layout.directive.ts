﻿import 'angular'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { Permissions } from '../../../helpers/permissions'

require('./layout.scss')

export class LayoutDirective implements ng.IDirective {
    constructor() {
    }

    scope = {
        active: '@active',
        heightFillDisable: '=?'
    }

    controller = ['$scope', ($scope: ng.IScope) => {
        Permissions.runIfHasPermissions([Permissions.WEBSIMAX_MAIN_MENU_VIEW], () => {
            $scope['smallMenu'] = UserConfigHelper.getUserConfig(UserConfigHelper.SmallMenuVisibility, false)

            if ($('html').width() < 768)
                $scope['smallMenu'] = true


            $scope.$watch(() => {
                return window['MenuItems']
            }, val => {
                $scope['isSidebarMenuVisible'] = val.length > 1
            })


            $scope.$watch('smallMenu', () => {
                UserConfigHelper.setUserConfig(UserConfigHelper.SmallMenuVisibility, $scope['smallMenu'])
            })

            $scope['toggleMenu'] = () => {
                $scope['smallMenu'] = !$scope['smallMenu']
            }
        })
    }]

    restrict = 'E'
    template = require("./layout.tpl.html")
    transclude = true
}