﻿import 'angular'

require('./loading-indicator.scss')

interface LoadingIndicatorScope extends ng.IScope {
    visiblePopup: boolean
    popupOptions: DevExpress.ui.dxPopupOptions
}

export class LoadingIndicatorDirective implements ng.IDirective {
    constructor(private $http: ng.IHttpService) {

    }

    enabled: boolean = true
    show: boolean = true
    enabledCounter: number = 0
    showedCounter: number = 0
    
    isLoading = () => {
        return this.$http.pendingRequests.length > 0 && this.enabled || this.show
    }

    restrict = 'E'
    link = (scope: LoadingIndicatorScope, element: JQuery) => {
        scope.visiblePopup = true

        scope.$on('disableLoadingIndicator', () => {
            if (--this.enabledCounter < 1)
                this.enabled = false;
        })

        scope.$on('enableLoadingIndicator', () => {
            if (++this.enabledCounter > 0)
                this.enabled = true;
        })

        scope.$on('hideLoadingIndicator', (event) => {
            if (--this.showedCounter < 1)
                this.show = false;
        })

        scope.$on('showLoadingIndicator', (event?) => {
            if (++this.showedCounter > 0)
                this.show = true;
        })
    }
    controller = ['$scope', '$timeout', ($scope: LoadingIndicatorScope, $timeout: ng.ITimeoutService) => {
        var lastVisiblePromise

        $scope.popupOptions = {
            width: 100,
            height: 100,
            showTitle: false,
            dragEnabled: false,
            closeOnOutsideClick: false,
            visible: true,
            onContentReady: (e) => {
                $scope.$watch(this.isLoading, (state) => {
                    if (lastVisiblePromise)
                        lastVisiblePromise = lastVisiblePromise.then(() => {
                            return e.component.toggle(state)
                        })
                    else lastVisiblePromise = e.component.toggle(state)
                })
            }
        }
    }]
    template = require("./loading-indicator.tpl.html")
    replace = true
}