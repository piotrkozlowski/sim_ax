﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { Details } from '../common.details'
import { CommonExtendedDataGridOptions } from '../common.extended-data-grid-options'
import { MeasurementsChartBridge } from '../measurements/chart/chart.directive.controller'
import { MeasurementsController, MeasurementDataType } from '../measurements/measurements.controller'
import { ColumnModel, MeasurementsRequestModel, SchemaModel, GridRequestModel, GridModel } from '../../api/api.models'
import { CommonHelper } from '../../../helpers/common-helper'
import { CommonScope } from '../common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { EventsNames } from '../../../helpers/events-names'
import { RequestManager } from '../../../helpers/request-manager'
var moment = require('moment-timezone')

require('./grid-details.scss')

export interface GridDetailsSettings {
    ids: number[]

    methodForPreviewFields: string
    methodForDisplayFields: string
    methodForMeasurmentsFields: string

    methodForMeasurments: string
    measurmentIdsField: string
    measurmentIdField: string

    serviceName: string
    methodName: string

    schemaMethodName?: string
    filterMethodName?: string
}

interface GridDetailsScope extends CommonScope {
    settings: GridDetailsSettings
    details: any[]
    gridOptions: DevExpress.ui.dxDataGridOptions
    chartBridge: MeasurementsChartBridge
    tooltipNames: any[]
    showLoading: boolean
    schemas: SchemaModel[]
    filterSwitchOptions: DevExpress.ui.dxSwitchOptions
    useFilter: boolean
    hideFilter: boolean
    schemaGalleryOptions: DevExpress.ui.dxGalleryOptions
}

export class GridDetailsDirective implements ng.IDirective {
    requestManager = RequestManager.getInstance()

    constructor() {

    }

    restrict = 'E'
    scope = {
        settings: '='
    }
    template = require("./grid-details.tpl.html")

    controller = ['$scope', '$element', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope: GridDetailsScope, $element: JQuery, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        $scope.useFilter = true
        $scope.hideFilter = true

        $scope.filterSwitchOptions = {
            offText: i18n('Common', 'DownloadOneByOne'),
            onText: i18n('Common', 'DownloadByFilter'),
            width: '100%',
            bindingOptions: {
                value: 'useFilter'
            },
            onValueChanged: (e) => {
                $scope.useFilter = e.value
            }
        }

        var setCustomOptions = (displayFields, gridDeferred) => {
            var customOptions = {
                name: "GridDetails",
                columnsDictionary: displayFields,
                onInitialized: (e) => {
                    var grid: DevExpress.ui.dxDataGrid = e.component
                    gridDeferred.resolve(grid)
                },
                allowColumnReordering: false,
                filterRow: { visible: false },
                columnAutoWidth: true,
                columnFixing: { enabled: true }
            }
            return customOptions
        }

        var downloadMeasurements = (measurementsFields, ids, objectsPromise, gridDeferred, schemasPromise: ng.IPromise<void>[]) => {
            $scope.$root.$broadcast('disableLoadingIndicator')
            var measurementsTypes = MeasurementsController.getTypesFromColumns(measurementsFields)
            var req = new MeasurementsRequestModel()
            req.columns = <any>measurementsTypes
            req[$scope.settings.measurmentIdsField] = ids
            var date = new Date()
            date.setDate(date.getDate() - 7)
            req.dateFrom = date
            req.dateTo = new Date()

            var momentFrom = moment(req.dateFrom)
            var momentTo = moment(req.dateTo)
            req.dateFrom = momentFrom.add(momentFrom.utcOffset(), 'minutes').toDate()
            req.dateTo = momentTo.add(momentTo.utcOffset(), 'minutes').toDate()

            api.measurement[$scope.settings.methodForMeasurments](req).then((data: any[]) => {
                var chartData = MeasurementsController.getChartData(data, measurementsTypes, $scope.settings.measurmentIdField, $scope.tooltipNames)

                $scope.$root.$broadcast('enableLoadingIndicator')

                objectsPromise.then(() => {
                    $scope.chartBridge = null
                    $timeout(() => {
                        $scope.showLoading = false
                        $scope.chartBridge = {
                            data: chartData.data,
                            selectedTypes: chartData.types,
                            filterId: 1,
                            tickInterval: { hours: 2 }
                        }
                        if (gridDeferred) {
                            gridDeferred.then(grid => {
                                grid.updateDimensions()
                            })
                        }

                        $q.all(schemasPromise).then(() => {
                            //$scope.$root.$broadcast('enableLoadingIndicator')
                        })
                    }, 0)
                })
            })
        }

        var initializeGrid = (columns, gridDeferred) => {
            var customGridOptions = setCustomOptions(columns, gridDeferred)
            $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, $scope, logger, null, customGridOptions)

            return gridDeferred
        }

        var gridDeferred: JQueryDeferred<DevExpress.ui.dxDataGrid> = null

        var updateDetailsTimeout
        var updateDetails = (ids: number[]) => {
            if (updateDetailsTimeout) clearTimeout(updateDetailsTimeout)
            updateDetailsTimeout = setTimeout(() => {
                if (!ids || ids.length == 0) {
                    return
                }
                $scope.$root.$broadcast('disableLoadingIndicator')
                $scope.showLoading = true
                $scope.gridOptions = null
                $timeout(() => {
                    gridDeferred = null
                    if (ids.length > 1 && $scope.useFilter && $scope.settings.filterMethodName) {
                        gridDeferred = $.Deferred<DevExpress.ui.dxDataGrid>()
                        var userId = api.login.getUser().id

                        localDictionary[$scope.settings.methodForPreviewFields](userId).then(previewFields => {
                            if (previewFields.length > 0)
                                return previewFields
                            else
                                return localDictionary[$scope.settings.methodForDisplayFields](userId)
                        }).then(columns => {
                            var objectPromise = api[$scope.settings.serviceName][$scope.settings.filterMethodName](<GridRequestModel>{
                                ids: ids,
                                pageNumber: 1,
                                pageSize: ids.length,
                                columns: columns
                            }).then((gridModel: GridModel) => {
                                $scope.details = gridModel.values
                                $scope.tooltipNames = []

                                return initializeGrid(columns, gridDeferred).then(grid => {
                                    var dataStore: DevExpress.data.DataSource = grid.option('dataSource')
                                    dataStore.items().forEach((item) => {
                                        dataStore.store().remove(item)
                                    })
                                    gridModel.values.forEach(value => {
                                        dataStore.store().insert(value)
                                        $scope.tooltipNames.push({
                                            id: [value.id],
                                            name: value.name
                                        })
                                    })
                                })
                            }).then(() => {
                                return <ng.IPromise<ColumnModel[]>>localDictionary[$scope.settings.methodForMeasurmentsFields](userId)
                            }).then(measurementFields => {
                                $scope.$root.$broadcast('enableLoadingIndicator')
                                downloadMeasurements(measurementFields, ids, objectPromise, gridDeferred, null)
                            })
                        })
                    }
                    else {
                        $scope.schemas = []
                        $scope.schemaGalleryOptions = null

                        var schemasPromises: ng.IPromise<void>[] = []

                        var objectsPromise = $q.all(ids.map(id => {
                            return <ng.IPromise<{ fields: any[], id: number, name: string, referenceId: string }>>api[$scope.settings.serviceName][$scope.settings.methodName](id)
                        })).then(objects => {
                            // measurments
                            $q.all(objects.map(o => {
                                if (ids.length == 1 && $scope.settings.schemaMethodName) {
                                    if (this.requestManager.requestStates.getSchemas && this.requestManager.requestStates.getSchemas.request && !this.requestManager.requestStates.getSchemas.request.$resolved) {
                                        this.requestManager.requestStates.getSchemas.request.$cancelRequest()
                                        this.requestManager.requestStates.getSchemas.cancelled = true
                                    }
                                    if (!this.requestManager.requestStates.getSchemas)
                                        this.requestManager.requestStates.getSchemas = {}
                                    var schemaPromise = <ng.IPromise<void>>api[$scope.settings.serviceName][$scope.settings.schemaMethodName](o.id, this.requestManager.requestStates.getSchemas).then(response => {
                                        response.result.forEach(schema => $scope.schemas.push(schema))
                                        if ($scope.schemas.length > 1)
                                            $scope.schemaGalleryOptions = {
                                                items: $scope.schemas,
                                                height: '100%',
                                                width: '100%'
                                            }
                                    })
                                    schemasPromises.push(schemaPromise)
                                }
                                return <ng.IPromise<ColumnModel[]>>localDictionary[$scope.settings.methodForMeasurmentsFields](o.referenceId)
                            })).then(measurementsFieldsArray => {
                                var measurementsFields: ColumnModel[] = []
                                var addedIds = []
                                measurementsFieldsArray.forEach(df => {
                                    df.forEach(f => {
                                        if (addedIds.indexOf(f.name) == -1) {
                                            addedIds.push(f.name)
                                            measurementsFields.push(f)
                                        }
                                    })
                                })

                                downloadMeasurements(measurementsFields, ids, objectsPromise, gridDeferred, schemasPromises)
                            })
                            return objects
                        }).then(objects => {
                            return $q.all(objects.map(r => {
                                return <ng.IPromise<ColumnModel[]>>localDictionary[$scope.settings.methodForPreviewFields](r.referenceId).then((displayFields) => {
                                    if (displayFields.length > 0) {
                                        return displayFields;
                                    } else
                                        return <ng.IPromise<ColumnModel[]>>localDictionary[$scope.settings.methodForDisplayFields](r.referenceId);
                                })
                            })).then(arrayDisplayFields => {
                                var displayFields: ColumnModel[] = []
                                var addedIds = []
                                arrayDisplayFields.forEach(df => {
                                    df.forEach(f => {
                                        if (addedIds.indexOf(f.name) == -1) {
                                            addedIds.push(f.name)
                                            displayFields.push(f)
                                        }
                                    })
                                })
                                return { displayFields: displayFields, objects: objects }
                            })
                        }).then(result => {
                            $scope.details = []
                            $scope.tooltipNames = []

                            result.objects.forEach(obj => {
                                var details = result ? Details.filterFields(obj.fields, result.displayFields) : []
                                $scope.details.push(details)
                                $scope.tooltipNames.push({
                                    id: [obj.id],
                                    name: obj.name
                                })
                            })

                            return result.displayFields
                        }).then(displayFields => {
                            if ($scope.details.length > 1) {
                                initializeGrid(displayFields, gridDeferred).then(grid => {
                                    var dataStore: DevExpress.data.DataSource = grid.option('dataSource')
                                    dataStore.items().forEach((item) => {
                                        dataStore.store().remove(item)
                                    })
                                    $scope.details.forEach((detail: any[]) => {
                                        var row = {}
                                        displayFields.forEach(field => {
                                            row[field.name.toLowerCase()] = null
                                        })

                                        detail.forEach(d => {
                                            var value = d.value
                                            if (d.type == "Real" && typeof value == "string") {
                                                value = +value.replace(',', '.')
                                            }
                                            row[d.name.toLowerCase()] = value
                                        })
                                        dataStore.store().insert(row)
                                    })
                                    $timeout(() => {
                                        dataStore.reload()
                                    }, 0)
                                })
                            }
                        }).catch(reason => logger.log(reason))
                    }
                }, 0)
            }, 500)
        }

        $scope.$watch('useFilter', val => {
            if ($scope.gridOptions)
                updateDetails($scope.settings.ids)
        })

        $scope.$watchCollection('settings.ids', (ids: number[]) => {
            updateDetails(ids)
        })

        var updateGridDetailsTimeout
        $scope.$on(EventsNames.UPDATE_GRID_DETAILS, () => {
            if (updateGridDetailsTimeout) $timeout.cancel(updateGridDetailsTimeout)
            updateGridDetailsTimeout = $timeout(() => {
                if (gridDeferred) {
                    gridDeferred.then(grid => {
                        grid.updateDimensions()
                    })
                }
            }, 200)
        })
    }]
}