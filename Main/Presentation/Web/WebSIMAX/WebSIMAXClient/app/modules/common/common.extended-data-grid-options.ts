﻿import { ColumnModel, GridModel, GridRequestModel, GridRequestReferenceModel } from '../api/api.models'
import { LoggerService } from  '../common/common.module'
import { i18n } from './i18n/common.i18n'
import { CommonDataGridOptions, CustomOptions } from './common.data-grid-options'
import { LocalDictionaryFactory } from  './../api/api.module'
import {ScopeBaseGrid } from './common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { CommonHelper } from '../../helpers/common-helper'

export interface CommonScope extends ng.IScope{
    showDetails(e: any): void
    gridId: number
    getItems(): any
    dataGrid: DevExpress.ui.dxDataGrid
    showGrid: boolean
    disabled: boolean
    //main grid custom filter options
    isFilterPopoverVisible: boolean
    isFilterListPopupVisible: boolean
    isFilterFormPopupVisible: boolean
    filterPopover: DevExpress.ui.dxPopover 
    filterPopoverOptions: DevExpress.ui.dxPopoverOptions
    filterListPopupOptions: DevExpress.ui.dxPopupOptions
    filterFormPopupOptions: DevExpress.ui.dxPopupOptions
    filterFormPopup: DevExpress.ui.dxPopup
    filterFormOptions: DevExpress.ui.dxFormOptions
    filterForm: DevExpress.ui.dxForm
    filterData: any
    filterListOptions: DevExpress.ui.dxListOptions
    submitFilterButtonOptions: DevExpress.ui.dxButtonOptions
    resetFilterButtonOptions: DevExpress.ui.dxButtonOptions
    isLargeScreen: boolean
    formItems: any[]
}

export class CommonExtendedDataGridOptions extends CommonDataGridOptions {
    constructor(private gridType: string, $scope: CommonScope, logger: LoggerService, request: (request?: GridRequestReferenceModel) => ng.IPromise<any>, customOptions?: CustomOptions, loadData: boolean = true, localDictionary: LocalDictionaryFactory = null, private exportPermission?: string, private $compile?: ng.ICompileService) {
        super($scope, logger, request, customOptions, loadData, localDictionary)

        switch (gridType) {
            case CommonHelper.mainGrid:
                customOptions.isSmallLoadingIndicator = true
                this.customOptions.loadPanel = {
                    enabled: true
                }
                customOptions.export = {
                    enabled: false
                }    
                customOptions.pager = {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 25, 50, 100],
                    showInfo: true,
                    infoText: i18n('Grid', 'PagerText'),
                    visible: true
                }
                customOptions.addCustomFilters = true
                customOptions.buttons = {
                    enabled: true,
                    gridSelector: "#gridContainer_" + this.$scope.gridId,
                    optionsScope: "buttonsOptions",
                    compile: this.$compile
                }


                customOptions.scrolling = {
                    mode: 'standard'
                }
                customOptions.onRowDoubleClick = (e) => {
                    if (customOptions.onSelectionChanged && typeof customOptions.onSelectionChanged === 'function') customOptions.onSelectionChanged(e)
                    this.$scope.showDetails(e)
                }
                customOptions.onContextMenuPreparing = e => {
                    if (e.row && e.row.rowType === 'data') {
                        if (this.$scope.getItems) {
                            this.$scope.getItems().then(items => {
                                if (items.length == 0) {
                                    return
                                }
                                e.items = items
                            })
                        }
                    }
                }
                if (!customOptions.onDataReady) {
                    customOptions.onDataReady = () => {
                        this.$scope.showGrid = true
                    }
                }
                super.callClassConstructor()
                break

            case CommonHelper.paginatedSmallGrid:
                customOptions.isSmallLoadingIndicator = true            
                customOptions.scrolling = {
                    mode: 'standard'
                }
                if (!customOptions.pager) {
                    customOptions.pager = {
                        showPageSizeSelector: false,
                        allowedPageSizes: [100],
                        showInfo: true,
                        infoText: i18n('Grid', 'PagerTextShort'),
                        visible: true
                    }
                }
                if (!customOptions.editing) {
                    customOptions.editing = {
                        allowUpdating: false
                    }
                }
                this.setCommonSmallGridOptions()
                super.callClassConstructor()
                break

            case CommonHelper.smallGrid:
                customOptions.isSmallLoadingIndicator = true         
                customOptions.onRowDoubleClick = this.$scope.showDetails
                customOptions.scrolling = {
                    mode: 'virtual'
                }
                if (!customOptions.editing) {
                    customOptions.editing = {
                        allowUpdating: false
                    }
                }
                this.setCommonSmallGridOptions()
                super.callClassConstructor()
                break
            case CommonHelper.createEditGrid:
                customOptions.isSmallLoadingIndicator = true      
                this.setCommonSmallGridOptions()
                super.callClassConstructor()
        }
    }

    setCommonSmallGridOptions() {
        this.customOptions.export = {
            enabled: false
        }
        this.customOptions.searchPanel = {
            visible: false
        }
        this.customOptions.width = '100%'
    }
}
