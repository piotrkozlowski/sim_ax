﻿require('./custom-filter.scss')

export class CustomFilterDirective implements ng.IDirective {
    constructor() { }

    restrict = 'E'
    template = require("./custom-filter.tpl.html")
}