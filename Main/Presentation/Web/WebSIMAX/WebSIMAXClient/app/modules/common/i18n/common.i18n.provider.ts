﻿import { i18n } from './common.i18n'

export class I18NProvider {
    private language = 'PL'

    $get = [() => {
        return i18n
    }]

    setLanguage(language: string) {
        this.language = language.split('-')[0].toUpperCase()
        global['language'] = this.language
    }

    setResources(resource) {
        global['resource'] = resource
    }
}