﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../common.module'
import { ColumnModel, MeasurementsRequestModel, MeasurementModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { DatePickerBridge } from '../date-picker/date-picker.directive'
import { MeasurementsChartBridge } from './chart/chart.directive.controller'
import { i18n } from '../i18n/common.i18n'
import { CommonScope } from '../common.extended-data-grid-options'
var moment = require('moment-timezone')

export interface MeasurementsBridge {
    id: number
    latestDataMethodName: string
    dataMethodName: string
    requestProperty: string
    locations?: any[]
    devices?: any[]
    meters?: any[]
    groups?: any[]
    data?: () => MeasurementModel[]
    dataCallback?: (data: MeasurementModel[]) => void
    chartBridgeCallback?: (chartBridge: MeasurementsChartBridge) => void
    showMeasurements?: boolean
}

export interface MeasurementDataType {
    id: number
    name: string
    unitType: number
    unitName: string
}

interface MeasurementsScope extends CommonScope {
    typeTagsOptions: DevExpress.ui.dxTagBoxOptions
    deviceTagsOptions: DevExpress.ui.dxTagBoxOptions
    meterTagsOptions: DevExpress.ui.dxTagBoxOptions
    groupTagsOptions: DevExpress.ui.dxTagBoxOptions
    preferencesButtonOptions: DevExpress.ui.dxButtonOptions
    refreshButtonOptions: DevExpress.ui.dxButtonOptions
    settingsListRefreshButtonOptions: DevExpress.ui.dxButtonOptions
    exitRefreshButtonOptions: DevExpress.ui.dxButtonOptions
    settingsListExitButtonOptions: DevExpress.ui.dxButtonOptions
    positioningButtonOptions: DevExpress.ui.dxButtonOptions
    exportGridButtonOptions: DevExpress.ui.dxButtonOptions
    settingsListButtonOptions: DevExpress.ui.dxButtonOptions
    loadingIndicatorOptions: DevExpress.ui.dxLoadIndicatorOptions
    settingsListOptions: DevExpress.ui.dxListOptions
    settingsListPopoverOptions: DevExpress.ui.dxPopoverOptions
    datesPopupOptions: DevExpress.ui.dxPopupOptions
    gridTypePopoverOptions: DevExpress.ui.dxPopoverOptions
    gridTypeListOptions: DevExpress.ui.dxListOptions
    positioningPopoverOptions: DevExpress.ui.dxPopoverOptions
    positioningListOptions: DevExpress.ui.dxListOptions
    selectFilterOptions: DevExpress.ui.dxSelectBoxOptions

    //gridster
    isFullScreenGrid: boolean
    isFullScreenChart: boolean
    gridsterOpts: any
    gridsterGridOpts: any
    gridsterChartOpts: any

    filter: number

    datePickerBridge: DatePickerBridge
    selectedDevices: any[]
    selectedMeters: any[]
    selectedGroups: any[]
    chartHintText: string

    chartBridge: MeasurementsChartBridge

    source: MeasurementsBridge

    positioningPopoverVisible: boolean
    gridTypePopoverVisible: boolean
    isButtonsPanelVisible: boolean
    isSettingsListPopoverVisible: boolean
    datesPopoverVisible: boolean
    isRefreshing: boolean
    showFilterOptions: boolean
    isGridInCommon: boolean

    gridOptions: CommonExtendedDataGridOptions
}

export class MeasurementsController {
    private static POSITIONS = {
        left: "ChartOnTheLeft",
        right: "ChartOnTheRight",
        top: "ChartOnTheTop",
        bottom: "ChartOnTheBottom",
        fullScreenChart: "FullScreenChart",
        fullScreenGrid: "FullScreenGrid"
    }

    private static IS_FULLSCREEN_GRID = 'IS_FULLSCREEN_GRID'
    private static IS_FULLSCREEN_CHART = 'IS_FULLSCREEN_CHART'
    private static CHART_POSITION = 'CHART_POSITION'

    private GRID_TYPES = {
        IN_COLUMNS: { template: CommonHelper.getTemplateforListRow(i18n('Grid', 'DataTypeColumns'), 'context-menu-row'), id: "data-type-columns" },
        IN_ROWS: { template: CommonHelper.getTemplateforListRow(i18n('Grid', 'DataTypeRows'), 'context-menu-row'), id: "data-type-rows" }
    }

    private data: MeasurementModel[]
    private dataTypeFields: ColumnModel[]

    private gridDeferred = $.Deferred<DevExpress.ui.dxDataGrid>()
    private typeTagsDeferred = $.Deferred<DevExpress.ui.dxTagBox>()

    private gridColumnsFromTypes = [
        {
            dataField: "dataType",
            caption: i18n("Common", "DataType"),
            calculateCellValue: row => {
                var caption
                if (this.dataTypeFields.some(value => {
                    if (value.id == row.dataType) {
                        caption = value.caption
                        return true
                    }
                    return false
                }))
                    return caption
            }
        },
        {
            dataField: "date",
            caption: i18n("Common", "Date"),
            dataType: "date",
            sortOrder: 'desc',
            customizeText: (options) => {
                return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
            },
            editorOptions: {
                displayFormat: i18n("Common", "DateFormatDX")
            }
        }, {
            dataField: "unit",
            caption: i18n("Common", "Unit"),
            calculateDisplayValue: rowData => {
                var index = this.selectedTypes.map(type => type.id).indexOf(rowData.dataType)
                return (index > -1 ? this.selectedTypes[index].unitName : "")
            },
            alignment: 'right'
        }, {
            dataField: "value",
            caption: i18n("Common", "Value"),
            dataType: "string",
            customizeText: CommonHelper.cellFormatForDifferentValues,
            alignment: 'right'
        }
    ]

    private dataTypesChartStorageName: string = 'defaultChartDataTypes'
    private settings = {
        itemsPosition: { template: CommonHelper.getIconForDetails('SIZE', '#000000', '18', i18n('Grid', 'ItemsPosition'), 'context-menu-row', "items-position-row") },
        gridLayout: { template: CommonHelper.getIconForDetails('REORDER', '#000000', '18', i18n('Grid', 'GridLayout'), 'context-menu-row', "grid-layout-row") },
        dates: { template: CommonHelper.getIconForDetails('DATE_RANGE', '#000000', '18', i18n('Grid', 'DateRange'), 'context-menu-row', "dates-row") },
        refresh: { template: '<div><div class="settings-refresh-button" ng-hide="isRefreshing" dx-button="settingsListRefreshButtonOptions"></div><div class="settings-refresh-button settings-exit-button" ng-show="isRefreshing" dx-button="settingsListExitButtonOptions"></div></div>' },
        export: { template: CommonHelper.getIconForDetails('EXPORT', '#000000', '18', i18n('Grid', 'Export'), 'context-menu-row', "export-row") }
    }
    private gridTypePopover: DevExpress.ui.dxPopover
    private positioningPopover: DevExpress.ui.dxPopover
    private loadLatest: boolean

    private selectedTypes: MeasurementDataType[]
    private selectedPeriodName: string

    private buttonsBarWidth: number = NaN
    private loadDataTimeout
    private lastSelectedTypes: MeasurementDataType[] = []
    private selectedGridType: any

    private lastSelectedFilter: number = 0
    private lastSelectedIds: any[] = []
    private disableTypesOnChange: boolean = false

    private requestState: any = {}

    constructor(private $scope: MeasurementsScope, private $element: ng.IRootElementService, private $q: ng.IQService, private $timeout: ng.ITimeoutService, private logger: LoggerService, private api: Api, private localDictionary: LocalDictionaryFactory) {
        var positionList = [MeasurementsController.POSITIONS.left, MeasurementsController.POSITIONS.right, MeasurementsController.POSITIONS.top, MeasurementsController.POSITIONS.bottom, MeasurementsController.POSITIONS.fullScreenChart, MeasurementsController.POSITIONS.fullScreenGrid]

        var gridTypesItems = [this.GRID_TYPES.IN_COLUMNS, this.GRID_TYPES.IN_ROWS]
        this.selectedGridType = UserConfigHelper.getUserConfig(UserConfigHelper.MeasurementsGridType, this.GRID_TYPES.IN_ROWS.id)

        $scope.isFullScreenGrid = UserConfigHelper.getUserConfig(MeasurementsController.IS_FULLSCREEN_GRID, false)
        $scope.isFullScreenChart = UserConfigHelper.getUserConfig(MeasurementsController.IS_FULLSCREEN_CHART, false)
        $scope.datesPopoverVisible = false
        $scope.positioningPopoverVisible = false
        $scope.isRefreshing = false

        $scope.gridsterOpts = {
            rowHeight: '100%',
            columns: 4, // the width of the grid, in columns
            pushing: true,
            swapping: true,
            floating: true,
            outerMargin: false, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 200, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 4,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: 4, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: 4, // maximum row height of an item
            resizable: {
                enabled: false
            },
            draggable: {
                enabled: false
            }
        }
        $scope.gridsterGridOpts = { sizeX: 2, sizeY: 4, row: 0, col: 2 }
        $scope.gridsterChartOpts = { sizeX: 2, sizeY: 4, row: 0, col: 0 }

        $scope.loadingIndicatorOptions = {
            width: 40,
            height: 40
        }

        $scope.datePickerBridge = {
            fromDate: null,
            toDate: null,
            callback: (selectedPeriod) => { this.loadMeasurementsFromPeriod(selectedPeriod) },
            customPeriods: [
                {
                    id: "latest",
                    title: i18n('Common', 'Latest')
                }
            ],
            isSmallScreen: false
        }

        var filterOptions = []
        if ($scope.source.groups || $scope.source.requestProperty == "locationIds")
            filterOptions.push({
                name: i18n('Common', 'CurrentLocation'),
                type: 1
            })
        if ($scope.source.devices || $scope.source.requestProperty == "deviceIds")
            filterOptions.push({
                name: i18n('Common', 'Devices'),
                type: 2
            })
        if ($scope.source.meters || $scope.source.requestProperty == "meterIds")
            filterOptions.push({
                name: i18n('Common', 'Meters'),
                type: 3
            })

        if (filterOptions.length == 0)
            alert('This error should not occur. Please contact with admin.')

        $scope.filter = UserConfigHelper.getUserConfig(CommonHelper.filterSelection + $scope.source.requestProperty, filterOptions[0].type)
        this.updateChartStorage($scope.filter)
        $scope.selectFilterOptions = {
            dataSource: filterOptions,
            valueExpr: 'type',
            displayExpr: 'name',
            onInitialized: e => {
                var selectBox: DevExpress.ui.dxSelectBox = e.component
                $timeout(() => {
                    selectBox.option('onSelectionChanged', e => {
                        $scope.filter = e.selectedItem.type
                        this.updateChartStorage($scope.filter)
                        UserConfigHelper.setUserConfig(CommonHelper.filterSelection + $scope.source.requestProperty, $scope.filter)
                        this.loadMeasurements(true, 1000)
                    })
                })
            }
        }
        $scope.showFilterOptions = filterOptions.length > 1

        var customGridOptions = {
            name: "Measurements",
            columns: [],
            export: {
                enabled: true
            },
            onInitialized: e => {
                this.gridDeferred.resolve(e.component)
            },
            paging: {
                pageSize: 50,
                enabled: true
            }
        }

        $scope.gridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, null, customGridOptions)

        $scope.chartBridge = {
            data: void 0,
            selectedTypes: [],
            filterId: $scope.filter,
            tickInterval: { hours: 2 },
            dataTypesChartStorageName: this.dataTypesChartStorageName,
            updateChart: false,
            clearChart: false
        }
        if ($scope.source.chartBridgeCallback) $scope.source.chartBridgeCallback($scope.chartBridge)

        //tags
        $scope.typeTagsOptions = <any>{
            onInitialized: e => {
                this.typeTagsDeferred.resolve(e.component)
            },
            deferRendering: false,
            searchEnabled: true,
            placeholder: i18n("Common", "SelectPlaceholder"),
            displayExpr: 'name',
            valueExpr: 'name',
            multiline: false,
            noDataText: i18n("Common", "NoDataTypesText"),
            hideSelectedItems: true,
            onValueChanged: e => {
                if (!this.disableTypesOnChange) {
                    this.selectedTypes = e.component.option('selectedItems')

                    var storageName = ""
                    switch (this.$scope.filter) {
                        default:
                        case 1:
                            storageName = "locationDataTypes"
                            break;
                        case 2:
                            storageName = "deviceDataTypes"
                            break;
                        case 3:
                            storageName = "meterDataTypes"
                            break;
                    }

                    UserConfigHelper.setUserConfig(storageName, this.selectedTypes.map(type => type.id))
                    if (this.selectedGridType == this.GRID_TYPES.IN_COLUMNS.id) this.updateGridColumnsDataTypeColumns()

                    if (this.canRefreshMeasurments(this.selectedTypes)) {
                        this.$scope.chartBridge.data = void 0
                        this.loadMeasurements(true, 1000)
                    } else {
                        this.loadMeasurements(false, 1000)
                    }
                }
            }
        }

        $scope.deviceTagsOptions = <any>{
            width: '100%',
            multiline: true
        }

        $scope.meterTagsOptions = <any>{
            width: '100%',
            multiline: true
        }

        $scope.groupTagsOptions = <any>{
            width: '100%',
            multiline: true
        }

        $scope.gridTypePopoverVisible = false

        $scope.refreshButtonOptions = {
            template: CommonHelper.getIcon('REFRESH'),
            hint: i18n("Common", "Refresh"),
            onClick: () => { this.refresh() }
        }

        $scope.settingsListRefreshButtonOptions = {
            template: CommonHelper.getIconForDetails('REFRESH', '#000000', '18', i18n('Grid', 'Refresh'), 'refresh-button-row', null),
            hint: i18n("Common", "Refresh"),
            onClick: () => { this.refresh() }
        }

        $scope.settingsListExitButtonOptions = {
            template: CommonHelper.getIconForDetails('CLOSE', '#000000', '18', i18n('Common', 'CancelLoading'), 'exit-button-row', null),
            hint: i18n("Grid", "Close"),
            onClick: () => {
                $scope.isSettingsListPopoverVisible = false
                this.exitRefresh()
            }
        }

        $scope.exportGridButtonOptions = {
            template: CommonHelper.getIcon('EXPORT'),
            hint: i18n("Common", "Export"),
            onClick: () => {
                this.gridDeferred.promise().then(grid => {
                    grid.exportToExcel(false)
                })
            }
        }

        $scope.exitRefreshButtonOptions = {
            template: CommonHelper.getIcon('CLOSE'),
            hint: i18n("Common", "CancelLoading"),
            onClick: () => { this.exitRefresh() }
        }

        $scope.preferencesButtonOptions = {
            template: CommonHelper.getIcon('REORDER'),
            hint: i18n("Grid", "Preferences"),
            onClick: () => {
                this.gridTypePopover.option("target", '#grid-button-' + $scope.$id)
                this.gridTypePopover.option("position", "bottom")
                this.toggleGridTypePopover()
            }
        }

        $scope.positioningButtonOptions = {
            template: CommonHelper.getIcon('SIZE'),
            bindingOptions: {
                hint: 'chartHintText'
            },
            visible: !$scope.isGridInCommon,
            onClick: () => {
                this.positioningPopover.option("target", '#position-button-' + $scope.$id)
                this.positioningPopover.option("position", "bottom")
                $scope.positioningPopoverVisible = !$scope.positioningPopoverVisible
            }
        }

        $scope.settingsListButtonOptions = {
            template: CommonHelper.getIcon('SHOW_OPTIONS'),
            hint: i18n("Grid", "ShowOptions"),
            onClick: () => {
                $scope.isSettingsListPopoverVisible = !$scope.isSettingsListPopoverVisible
            }
        }

        $scope.settingsListPopoverOptions = {
            target: '#popoverSettingsListButton',
            width: 200,
            bindingOptions: {
                visible: 'isSettingsListPopoverVisible'
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            },
            onHidden: () => {
                $scope.isSettingsListPopoverVisible = false
            }
        }


        var settingsList = [
            this.settings.gridLayout, this.settings.dates, this.settings.refresh, this.settings.export
        ]
        if (!$scope.isGridInCommon) {
            settingsList.unshift(this.settings.itemsPosition)
        }

        $scope.settingsListOptions = {
            items: settingsList,
            onItemClick: e => {
                this.executeAction(settingsList[e.itemIndex])
            },
            onContentReady: () => {
                var refreshButton = angular.element('.settings-refresh-button')
                refreshButton.parent().parent().css({
                    padding: '0px 0px'
                })
                var exitRefreshButton = angular.element('.settings-exit-button')
                exitRefreshButton.parent().parent().css({
                    padding: '0px 0px'
                })
                angular.element('.refresh-button-row').children().first().css({
                    "margin-bottom": "5px"
                })
                angular.element('.exit-button-row').children().first().css({
                    "margin-bottom": "5px"
                })
            }
        }

        $scope.datesPopupOptions = {
            maxWidth: 250,
            maxHeight: 250,
            bindingOptions: {
                visible: 'datesPopoverVisible'
            },
            onHidden: () => {
                $scope.datesPopoverVisible = false
            }
        }

        $scope.gridTypePopoverOptions = {
            target: '#grid-button-' + $scope.$id,
            width: 200,
            bindingOptions: {
                visible: 'gridTypePopoverVisible'
            },
            onInitialized: e => {
                this.gridTypePopover = e.component
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            }
        }

        $scope.positioningPopoverOptions = {
            target: '#position-button-' + $scope.$id,
            width: 160,
            bindingOptions: {
                visible: 'positioningPopoverVisible'
            },
            onInitialized: e => {
                this.positioningPopover = e.component
            },
            onShowing: e => {
                e.component.content().css({
                    'padding': 0,
                    'text-align': 'center',
                    'border-radius': 'inherit'
                })
            }
        }

        $scope.positioningListOptions = {
            items: positionList.map(key => i18n("Grid", key)),
            onItemClick: e => {
                $scope.isSettingsListPopoverVisible = false
                $scope.positioningPopoverVisible = false
                this.setPosition(positionList[e.itemIndex])
            }
        }

        $scope.gridTypeListOptions = {
            items: gridTypesItems,
            onItemClick: e => {
                $scope.isSettingsListPopoverVisible = false
                $scope.gridTypePopoverVisible = false
                e.component.option("items").forEach(item => { item.disabled = false })
                e.itemData.disabled = true
                this.selectedGridType = e.itemData.id
                UserConfigHelper.setUserConfig(UserConfigHelper.MeasurementsGridType, this.selectedGridType)
                //gridColumns
                this.gridDeferred.promise().then(grid => {
                    grid.beginCustomLoading(null)
                    $timeout(() => {
                        switch (this.selectedGridType) {
                            case this.GRID_TYPES.IN_ROWS.id: {
                                this.updateGridColumnsDataTypeRows()
                                break
                            }
                            case this.GRID_TYPES.IN_COLUMNS.id: {
                                this.updateGridColumnsDataTypeColumns()
                                break
                            }
                        }
                        this.loadMeasurements(false).then(() => {
                            grid.endCustomLoading()
                        })
                    })
                })
            },
            onItemRendered: e => {
                if (e.itemData.id == this.selectedGridType)
                    e.itemData.disabled = true
            }
        }

        if ($scope.isFullScreenGrid) {
            this.gridToFullScreen()
        }
        else if ($scope.isFullScreenChart) {
            this.chartToFullScreen()
        }
        else {
            this.showBoth()
        }

        $scope.$watch(() => {
            if (!$scope.isButtonsPanelVisible) return NaN
            return $element.find('.measurements-buttons-panel').width()
        }, width => {
            if (!isNaN(width)) {
                this.buttonsBarWidth = width + 200 // 200 for filter
            }
        })

        $scope.$watch(() => {
            if (this.buttonsBarWidth == NaN) return NaN
            return $element.find('.measurements-controls').width()
        }, (barContainerWidth) => {
            if (!isNaN(barContainerWidth)) {
                if (barContainerWidth - this.buttonsBarWidth <= 0) {
                    $scope.isButtonsPanelVisible = false
                    $scope.datePickerBridge.isSmallScreen = true
                }
                else {
                    $scope.isSettingsListPopoverVisible = false
                    $scope.isButtonsPanelVisible = true
                    $scope.datePickerBridge.isSmallScreen = false
                    $scope.datesPopoverVisible = false
                }
                $scope.gridTypePopoverVisible = false
                $scope.positioningPopoverVisible = false
            }
        })

        $scope.$watch(() => {
            return angular.element('.gridster-custom-container').innerHeight()
        }, (contentHeight) => {
            $scope.gridsterOpts.rowHeight = (contentHeight) / 4
            var windowWidth = angular.element(window).width()

            if ($scope.gridsterOpts.rowHeight < 100 && windowWidth >= 768) $scope.gridsterOpts.rowHeight = 100
            else if (windowWidth < 768) $scope.gridsterOpts.rowHeight = 200
        })

        $scope.$watch(() => {
            return angular.element(window).width()
        }, (windowWidth) => {
            if (windowWidth < 768) $scope.gridsterOpts.isMobile = true
            else $scope.gridsterOpts.isMobile = false
        })

        var groupsDefer = $q.defer<void>()
        $scope.$watchCollection('source.groups', (result: any[]) => {
            if (!result) result = []
            this.updateTagBox('groupTags', 'groupTagsOptions', 'selectedGroups', result)
            groupsDefer.resolve()
        })

        var devicesDefer = $q.defer<void>()
        $scope.$watchCollection('source.devices', (result: any[]) => {
            if (!result) result = []
            var devices = result.map(device => {
                if (device.id && device.name && device.referenceId)
                    return device
                else
                    return {
                        id: device[CommonHelper.idField],
                        name: device[CommonHelper.nameField],
                        referenceId: device.referenceId
                    }
            })
            this.updateTagBox('deviceTags', 'deviceTagsOptions', 'selectedDevices', devices)
            devicesDefer.resolve()
        })

        var metersDefer = $q.defer<void>()
        $scope.$watchCollection('source.meters', (result: any[]) => {
            if (!result) result = []
            var meters = result.map(meter => {
                if (meter.id && meter.name && meter.referenceId)
                    return meter
                else
                    return {
                        id: meter[CommonHelper.idField],
                        name: meter[CommonHelper.nameField],
                        referenceId: meter.referenceId
                    }
            })
            this.updateTagBox('meterTags', 'meterTagsOptions', 'selectedMeters', meters)
            metersDefer.resolve()
        })

        $q.all([groupsDefer.promise, devicesDefer.promise, metersDefer.promise]).then(() => {
            $scope.$watch('source.id', (sourceId: number) => {
                var data = $scope.source.data ? $scope.source.data() : null
                if (data) {
                    this.loadTypes().then(() => {
                        this.data = data

                        var actualData = this.data
                        if (this.selectedGridType == this.GRID_TYPES.IN_COLUMNS.id) {
                            actualData = this.changeDataToRows(this.data)
                        }

                        this.updateGrid(actualData)
                        this.updateChart(this.data)

                        this.$timeout(() => {
                            $scope.source.showMeasurements = true
                        }, 0)
                    })
                } else {
                    this.loadMeasurements()
                }
            })
        })

        $scope.$on(EventsNames.STOP_MEASUREMENTS_LOADING, () => {
            this.cancel()
        })
    }

    private cancel(): boolean {
        var result = this.requestState.request && !this.requestState.request.$resolved
        if (this.requestState && result) {
            this.requestState.request.$cancelRequest()
            this.requestState.cancelled = true
        }
        return result
    }

    private updateChartStorage(filter: number) {
        switch (filter) {
            default:
            case 1:
                this.dataTypesChartStorageName = 'locationChartDataTypes'
                break;
            case 2:
                this.dataTypesChartStorageName = 'deviceChartDataTypes'
                break;
            case 3:
                this.dataTypesChartStorageName = 'meterChartDataTypes'
                break;
        }
    }

    private changeDataToRows(data: any[]) {
        var newData = {}
        data.forEach(d => {
            if (newData[d.date] == undefined) {
                newData[d.date] = {
                    date: d.date
                }
            }
            newData[d.date][d.dataType.toString()] = d.value
        })
        var array = []
        for (var d in newData) {
            array.push(newData[d])
        }

        return array
    }

    private updateData(reload: boolean = true, timeout: number = 0) {
        var defer = this.$q.defer<void>()
        this.$scope.source.showMeasurements = !reload
        if (this.loadDataTimeout) this.$timeout.cancel(this.loadDataTimeout)
        this.loadDataTimeout = this.$timeout(() => {
            this.loadTypes().then(() => {
                if (reload) {
                    this.lastSelectedTypes = this.selectedTypes
                    if (!this.lastSelectedTypes) {
                        this.$scope.source.showMeasurements = true
                        defer.resolve()
                        return
                    }

                    var dictionaries = {}

                    this.lastSelectedTypes.forEach(t => {
                        dictionaries[t.id] = t.unitName
                    })

                    this.$scope.isRefreshing = true
                    this.getData().then(data => {
                        this.$scope.isRefreshing = false

                        if (!data) data = []
                        data.forEach(d => {
                            var unit = dictionaries[d.dataType]
                            if (unit)
                                d['unit'] = unit
                        })
                        this.data = data
                        if (this.$scope.source.dataCallback) this.$scope.source.dataCallback(this.data)

                        var actualData = data
                        if (this.selectedGridType == this.GRID_TYPES.IN_COLUMNS.id) {
                            actualData = this.changeDataToRows(data)
                        }

                        this.updateGrid(actualData)
                        this.updateChart(this.data)

                        this.$timeout(() => {
                            this.$scope.source.showMeasurements = true
                            defer.resolve()
                        }, 0)
                    }).catch(e => {
                        defer.reject()
                    })
                } else {
                    var removedTypes: number[] = this.lastSelectedTypes.filter(lastSelectedType => {
                        return this.selectedTypes.every(selectedType => !(selectedType.id == lastSelectedType.id))
                    }).map(type => type.id)

                    if (this.selectedGridType == this.GRID_TYPES.IN_COLUMNS.id) {
                        var data = this.changeDataToRows(this.data)
                        this.updateGrid(data)

                        this.$scope.chartBridge.updateChart = false
                        this.$scope.chartBridge.selectedTypes = this.$scope.chartBridge.selectedTypes.filter(type => this.selectedTypes.some(sType => sType.id == type['lastId'] || sType.id == type.id))
                    } else if (this.selectedGridType == this.GRID_TYPES.IN_ROWS.id) {
                        this.data = this.data.filter(d => {
                            return removedTypes.every(removedType => !(removedType == d.dataType))
                        })
                        if (this.$scope.source.dataCallback) this.$scope.source.dataCallback(this.data)
                        this.updateGrid(this.data)
                        if (removedTypes.length > 0)
                            this.updateChart(this.data)
                    }

                    this.lastSelectedTypes = this.selectedTypes
                    this.$timeout(() => {
                        this.$scope.source.showMeasurements = true
                    }, 0)
                    defer.resolve()
                }
            })
        }, timeout)
        return defer.promise
    }

    private updateGrid(data) {
        this.gridDeferred.promise().then(grid => {
            var dataSource = new DevExpress.data.DataSource({
                store: new DevExpress.data.ArrayStore(data)
            })
            dataSource.pageSize(100)
            grid.option('dataSource', dataSource)
            grid.refresh()
        })
    }

    private updateChart(data) {
        var period
        switch (this.selectedPeriodName) {
            default:
            case "latest":
            case "day":
                period = { hours: 2 }
                break
            case "week":
                period = { days: 1 }
                break
            case "month":
                period = { weeks: 1 }
                break
            case "years":
                period = { months: 1 }
                break
        }

        var idFieldName: string, entityName: string
        switch (this.$scope.filter) {
            default:
            case 1:
                idFieldName = 'locationId'
                entityName = 'groups'
                break;
            case 2:
                idFieldName = 'serialNumber'
                entityName = 'devices'
                break;
            case 3:
                idFieldName = 'meterId'
                entityName = 'meters'
                break;
        }

        var tooltips = this.$scope.source[entityName].map(obj => {
            return {
                id: [obj.id],
                name: obj.name
            }
        })

        var chartData = MeasurementsController.getChartData(data, this.selectedTypes, idFieldName, tooltips)

        this.$scope.chartBridge = null
        this.$timeout(() => {
            this.$scope.chartBridge = {
                data: chartData.data,
                selectedTypes: chartData.types,
                filterId: this.$scope.filter,
                tickInterval: period,
                dataTypesChartStorageName: this.dataTypesChartStorageName,
                updateChart: false,
                clearChart: false
            }
            if (this.$scope.source.chartBridgeCallback) this.$scope.source.chartBridgeCallback(this.$scope.chartBridge)
        })
    }

    private loadTypes() {
        var defer = this.$q.defer()
        var ids = []
        var method: (referenceValue: number) => ng.IPromise<ColumnModel[]>
        var storageName = ""
        switch (this.$scope.filter) {
            default:
            case 1:
                method = this.localDictionary.getMeasurementsForLocationFields
                storageName = "locationDataTypes"
                ids = this.$scope.selectedGroups.map(object => object.referenceId)
                break;
            case 2:
                method = this.localDictionary.getMeasurementsForDeviceFields
                storageName = "deviceDataTypes"
                ids = this.$scope.selectedDevices.map(object => object.referenceId)
                break;
            case 3:
                method = this.localDictionary.getMeasurementsForMeterFields
                storageName = "meterDataTypes"
                ids = this.$scope.selectedMeters.map(object => object.referenceId)
                break;
        }
        var refresh = this.lastSelectedFilter != this.$scope.filter
        if (!refresh) refresh = this.lastSelectedIds.length != ids.length
        if (!refresh) {
            refresh = !ids.every(id => this.lastSelectedIds.indexOf(id) > -1)
        }
        if (refresh) {
            this.lastSelectedFilter = this.$scope.filter
            this.lastSelectedIds = ids

            this.$q.all(ids.map(id => {
                return method.call(this.localDictionary, id)
            })).then((result: ColumnModel[][]) => {
                var fields: ColumnModel[] = []
                var addedIds = []
                result.forEach(df => {
                    df.forEach(f => {
                        if (addedIds.indexOf(f.name) == -1) {
                            addedIds.push(f.name)
                            fields.push(f)
                        }
                    })
                })
                this.dataTypeFields = fields

                var types = MeasurementsController.getTypesFromColumns(fields)
                var savedIds = UserConfigHelper.getUserConfig(storageName, types.map(type => type.id)).filter(id => {
                    return types.some(t => t.id == id)
                })
                this.selectedTypes = types.filter(type => savedIds.some(id => type.id == id))

                var typeTagsOptions = {
                    dataSource: new DevExpress.data.ArrayStore({
                        data: types,
                        key: 'id'
                    }),
                    value: this.selectedTypes.map(value => {
                        return value.name
                    })
                }
                this.typeTagsDeferred.promise().then(typeTags => {
                    this.disableTypesOnChange = true
                    typeTags.option(typeTagsOptions)
                    this.$timeout(() => {
                        this.disableTypesOnChange = false
                    }, 100)
                })

                this.gridDeferred.then(() => {
                    this.refreshGridColumns()
                })
                defer.resolve()
            })
        } else {
            defer.resolve()
        }
        return defer.promise
    }

    private updateGridOptions(options: any) {
        this.gridDeferred.promise().then(grid => {
            grid.option(options)
        })
    }

    private getAdditionalColumn(dataField: string, caption: string, listName: string) {
        return {
            dataField: dataField,
            caption: i18n('Common', caption),
            dataType: 'string',
            calculateCellValue: row => {
                var name = row[dataField]
                if (!name) name = ''
                this.$scope[listName].some(e => {
                    if (e.id == row[dataField]) {
                        name = e.name
                        return true
                    }
                    return false
                })
                return name
            }
        }
    }

    private getAdditionalColumns(columns: any[]) {
        var result = []
        switch (this.$scope.filter) {
            default:
            case 1:
                if (this.$scope.selectedGroups.length > 1) {
                    result.push(this.getAdditionalColumn('locationId', 'Location', 'selectedGroups'))
                }
                result.push(this.getAdditionalColumn('serialNumber', 'Device', 'selectedDevices'))
                result.push(this.getAdditionalColumn('meterId', 'Meter', 'selectedMeters'))
                break;
            case 2:
                if (this.$scope.selectedDevices.length > 1) {
                    result.push(this.getAdditionalColumn('serialNumber', 'Device', 'selectedDevices'))
                }
                result.push(this.getAdditionalColumn('meterId', 'Meter', 'selectedMeters'))
                break;
            case 3:
                if (this.$scope.selectedMeters.length > 1) {
                    result.push(this.getAdditionalColumn('meterId', 'Meter', 'selectedMeters'))
                }
                result.push(this.getAdditionalColumn('serialNumber', 'Device', 'selectedDevices'))
                break;
        }
        result.push(...columns)
        return result
    }

    private updateGridColumnsDataTypeRows() {
        var columns = this.getAdditionalColumns(this.gridColumnsFromTypes)

        this.updateGridOptions({
            columns: columns,
            columnAutoWidth: false,
            columnFixing: {
                enabled: false
            }
        })
    }

    private updateGridColumnsDataTypeColumns() {
        var gridColumns = [{
            dataField: "date",
            caption: i18n("Common", "Date"),
            dataType: "date",
            sortOrder: 'desc',
            customizeText: (options) => {
                return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
            },
            editorOptions: {
                displayFormat: i18n("Common", "DateFormatDX")
            }
        }]
        this.selectedTypes.forEach((type) => {
            var unit = ''
            if (type.unitName != '') unit = ' [' + type.unitName + ']'
            var column: any = {
                dataField: type.id.toString(),
                dataType: "string",
                caption: type.name + unit,
                customizeText: CommonHelper.cellFormatForDifferentValues,
                alignment: 'right'
            }
            gridColumns.push(column)
        })
        this.updateGridOptions({
            columns: gridColumns,
            columnAutoWidth: true,
            columnFixing: {
                enabled: true
            }
        })
    }

    private canRefreshMeasurments(selectedTypes: MeasurementDataType[]) {
        if (selectedTypes.length > this.lastSelectedTypes.length) return true
        return !selectedTypes.every(selectedType => {
            return !this.lastSelectedTypes.every(lastSelectedType => !(selectedType.id == lastSelectedType.id))
        })
    }

    private loadMeasurements(reload: boolean = true, timeout: number = 0) {
        return this.updateData(reload, timeout)
    }

    private loadMeasurementsFromPeriod(selectedPeriod) {
        this.selectedPeriodName = selectedPeriod.id
        this.loadLatest = selectedPeriod.id == "latest"
        this.updateData(true, 1000)
    }

    private refreshGridColumns() {
        if (this.selectedGridType == this.GRID_TYPES.IN_COLUMNS.id) {
            this.updateGridColumnsDataTypeColumns()
        } else {
            this.updateGridColumnsDataTypeRows()
        }
        this.$timeout(() => {
            this.gridDeferred.promise().then(grid => {
                grid.refresh()
                grid.updateDimensions()
            })
        })
    }

    private getRequest(): MeasurementsRequestModel {
        var req = new MeasurementsRequestModel()
        req.columns = <any>this.selectedTypes
        switch (this.$scope.filter) {
            default:
            case 1:
                req.locationIds = this.$scope.selectedGroups.map(object => object.id)
                break;
            case 2:
                req.deviceIds = this.$scope.selectedDevices.map(object => object.id)
                break;
            case 3:
                req.meterIds = this.$scope.selectedMeters.map(object => object.id)
                break;
        }

        var fromDate = this.$scope.datePickerBridge.fromDate
        var toDate = this.$scope.datePickerBridge.toDate

        req.dateFrom = moment(fromDate).toDate()
        req.dateTo = moment(toDate).toDate()

        var momentFrom = moment(fromDate)
        var momentTo = moment(toDate)

        req.dateFrom = momentFrom.add(momentFrom.utcOffset(), 'minutes').toDate()
        req.dateTo = momentTo.add(momentTo.utcOffset(), 'minutes').toDate()

        return req
    }

    private updateTagBox(tagsName: string, tagsNameOptions: string, name: string, data: any[]) {
        this.$scope[name] = data
        var visible = data.length > 1
        if (!this[tagsName]) {
            this.$scope[tagsNameOptions] = <any>{
                dataSource: new DevExpress.data.ArrayStore({
                    data: data,
                    key: 'id'
                }),
                deferRendering: false,
                value: data.map(value => {
                    return value.name
                }),
                searchEnabled: true,
                placeholder: i18n("Common", "SelectPlaceholder"),
                displayExpr: 'name',
                valueExpr: 'name',
                multiline: false,
                noDataText: i18n("Common", "NoDataTypesText"),
                hideSelectedItems: true,
                onValueChanged: e => {
                    this.$scope[name] = e.component.option('selectedItems')
                    this.loadMeasurements()
                },
                onInitialized: e => {
                    this[tagsName] = e.component
                },
                visible: visible
            }
        } else {
            this[tagsName].reset()
            this[tagsName].option({
                dataSource: new DevExpress.data.ArrayStore({
                    data: data,
                    key: 'id'
                }),
                value: data.map(value => {
                    return value.name
                }),
                visible: visible
            })
        }
    }

    private toggleGridTypePopover() {
        this.$scope.gridTypePopoverVisible = !this.$scope.gridTypePopoverVisible
    }

    private refresh() {
        this.$scope.chartBridge.updateChart = false
        this.$scope.chartBridge.data = void 0

        this.loadMeasurements().then(() => {
            this.$scope.isSettingsListPopoverVisible = false
        })
    }

    private exitRefresh() {
        if (this.cancel()) {
            this.$timeout(() => {
                this.gridDeferred.promise().then(grid => {
                    grid.option('dataSource', [])
                    grid.refresh()
                })
                this.$scope.chartBridge.clearChart = true
            }, 0)
            this.$timeout(() => {
                this.$scope.source.showMeasurements = true
            }, 100)
            this.$scope.isRefreshing = false
        }
    }

    private executeAction(settings) {
        switch (settings) {
            case this.settings.itemsPosition:
                this.positioningPopover.option("target", "#items-position-row")
                this.positioningPopover.option("position", "right")
                this.$scope.positioningPopoverVisible = true
                break
            case this.settings.gridLayout:
                this.gridTypePopover.option("target", "#grid-layout-row")
                this.gridTypePopover.option("position", "right")
                this.$scope.gridTypePopoverVisible = true
                break
            case this.settings.dates:
                this.$scope.datesPopoverVisible = !this.$scope.datesPopoverVisible
                break
            case this.settings.refresh:
                this.refresh()
                break
            case this.settings.export:
                this.$scope.isSettingsListPopoverVisible = false
                this.gridDeferred.promise().then(grid => {
                    grid.exportToExcel(false)
                })
                break
        }
    }

    private setPosition(position) {
        if (position == MeasurementsController.POSITIONS.fullScreenGrid) this.gridToFullScreen()
        else if (position == MeasurementsController.POSITIONS.fullScreenChart) this.chartToFullScreen()
        else {
            UserConfigHelper.setUserConfig(MeasurementsController.CHART_POSITION, position)
            this.$scope.isFullScreenChart = false
            this.$scope.isFullScreenGrid = false
            UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_GRID, false)
            UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_CHART, false)
            switch (position) {
                case MeasurementsController.POSITIONS.left:
                    this.$scope.gridsterGridOpts = { sizeX: 2, sizeY: 4, row: 0, col: 2 }
                    this.$scope.gridsterChartOpts = { sizeX: 2, sizeY: 4, row: 0, col: 0 }
                    break
                case MeasurementsController.POSITIONS.right:
                    this.$scope.gridsterGridOpts = { sizeX: 2, sizeY: 4, row: 0, col: 0 }
                    this.$scope.gridsterChartOpts = { sizeX: 2, sizeY: 4, row: 0, col: 2 }
                    break
                case MeasurementsController.POSITIONS.top:
                    this.$scope.gridsterGridOpts = { sizeX: 4, sizeY: 2, row: 2, col: 0 }
                    this.$scope.gridsterChartOpts = { sizeX: 4, sizeY: 2, row: 0, col: 0 }
                    break
                case MeasurementsController.POSITIONS.bottom:
                    this.$scope.gridsterGridOpts = { sizeX: 4, sizeY: 2, row: 0, col: 0 }
                    this.$scope.gridsterChartOpts = { sizeX: 4, sizeY: 2, row: 2, col: 0 }
                    break
            }
            this.$timeout(() => {
                this.gridDeferred.promise().then(grid => {
                    grid.updateDimensions()
                })
                if (this.$scope.chartBridge)
                    this.$scope.chartBridge.updateChart = true
            }, 200)
        }
    }

    private gridToFullScreen() {
        this.$scope.isFullScreenGrid = true
        this.$scope.isFullScreenChart = false
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_GRID, true)
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_CHART, false)
        this.$scope.gridsterGridOpts = { sizeX: 4, sizeY: 4, row: 0, col: 0 }
        this.$timeout(() => {
            this.gridDeferred.promise().then(grid => {
                grid.updateDimensions()
            })
        }, 0)
    }

    private chartToFullScreen() {
        this.$scope.isFullScreenChart = true
        this.$scope.isFullScreenGrid = false
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_GRID, false)
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_CHART, true)
        this.$scope.gridsterChartOpts = { sizeX: 4, sizeY: 4, row: 0, col: 0 }
        this.$timeout(() => {
            this.$scope.chartBridge.updateChart = true
        }, 0)
    }

    private showBoth() {
        this.$scope.isFullScreenGrid = false
        this.$scope.isFullScreenChart = false
        this.$scope.chartHintText = i18n("Common", "ChangePosition")
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_GRID, false)
        UserConfigHelper.setUserConfig(MeasurementsController.IS_FULLSCREEN_CHART, false)
        var chartPosition = UserConfigHelper.getUserConfig(MeasurementsController.CHART_POSITION, MeasurementsController.POSITIONS.right)
        this.setPosition(chartPosition)
    }

    private getData(): ng.IPromise<MeasurementModel[]> {
        if (this.cancel()) {
            this.gridDeferred.promise().then(grid => {
                grid.option('dataSource', [])
            })
        }
        var req = this.getRequest()
        var methodName = this.loadLatest ? this.$scope.source.latestDataMethodName : this.$scope.source.dataMethodName
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.measurement[methodName](req, this.requestState).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            return result
        })
    }

    static getTypesFromColumns(columns: ColumnModel[]) {
        var types: MeasurementDataType[] = []
        columns.forEach((element, index) => {
            types[index] = {
                id: element.id,
                name: element.caption,
                unitType: element.unitId,
                unitName: element.unitName
            }
            types[index]['chartType'] = element.chartType
            if (element.type == CommonHelper.date) {
                types[index]['isDate'] = true
            }
        })
        return types
    }

    static getChartData(data: any[], types: any[], idFieldName: string, tooltipNames: any[]) {
        var newTypes = []
        var newData = []
        var existingDataTypes = {}

        if (tooltipNames && tooltipNames.length > 1) {
            var dataByEntity = {}
            data.forEach(d => {
                if (!dataByEntity[d[idFieldName]]) dataByEntity[d[idFieldName]] = []
                dataByEntity[d[idFieldName]].push(d)
                existingDataTypes[d.dataType + '-' + d[idFieldName]] = true
            })

            types.forEach(type => {
                for (var index in dataByEntity) {
                    var values: any[] = dataByEntity[index]
                    var tooltipName = tooltipNames.filter(tN => tN.id == index)[0]
                    if (tooltipName) tooltipName = tooltipName.name
                    else tooltipName = index
                    if (existingDataTypes[type.id + '-' + index]) {
                        var newType = {
                            id: type.id + '-' + index,
                            name: tooltipName + ': ' + type.name,
                            unitName: type.unitName,
                            unitType: type.unitType
                        }
                        newType['lastId'] = type.id
                        newType['chartType'] = type.chartType
                        newTypes.push(newType)
                    }
                }
            })

            for (var index in dataByEntity) {
                var values: any[] = dataByEntity[index]
                values.forEach(value => {
                    var d = angular.copy(value)
                    d.dataType = value.dataType + '-' + index
                    newData.push(d)
                })
            }
        } else {
            newData = data

            data.forEach(d => {
                existingDataTypes[d.dataType] = true
            })
            types.forEach(type => {
                if (existingDataTypes[type.id]) {
                    newTypes.push(type)
                }
            })
        }

        return {
            data: newData,
            types: newTypes
        }
    }
}