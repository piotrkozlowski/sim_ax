﻿import { ColumnModel, GridModel, GridRequestReferenceModel } from '../api/api.models'
import { LoggerService } from  '../common/common.module'
import { i18n } from './i18n/common.i18n'
import { CommonHelper } from '../../helpers/common-helper'
import { UserConfigHelper } from '../../helpers/user-config-helper'
import * as moment from 'moment'
import * as _ from 'underscore'
import { LocalDictionaryFactory } from  './../api/api.module'
import { CommonScope } from './common.extended-data-grid-options'

interface ButtonsSettings {
    enabled: boolean
    gridSelector: string,
    optionsScope: string
    compile: ng.ICompileService
}

export interface CustomOptions {
    name?: string
    buttons?: ButtonsSettings
    defaultColumnsDictionary?: ColumnModel[]
    columnsDictionary?: ColumnModel[]
    idReferenceType?: number

    columns?: DevExpress.ui.dxDataGridColumn[]

    onDataReady?(data): void
    onContentReady?(e): void
    onRowClick?(e): void
    onRowDoubleClick?(e): void
    onRowPrepared?(e): void
    onInitialized?(e): void
    onContextMenuPreparing?(e): void
    onSelectionChanged?(e): void
    onRowUpdated?(e): void
    onEditorPreparing?(e): void
    onCellPrepared?(options): void

    addCustomFilters?: boolean
    showShortDetails?: boolean
    showColumnLines?: boolean
    showRowLines?: boolean
    showColumnHeaders?: boolean
    isSmallLoadingIndicator?: boolean
    allowColumnReordering?: boolean
    columnAutoWidth?: boolean

    export?: any
    scrolling?: any
    pager?: any
    paging?: any
    width?: string
    height?: string
    editing?: any
    columnChooser?: any
    searchPanel?: any
    filterRow?: any
    selectedRowKeys?: any
    selection?: any
    dataSource?: any
    masterDetail?: any
    columnFixing?: any
    loadPanel?: any
}

export class CommonDataGridOptions implements DevExpress.ui.dxDataGridOptions {

    columns: DevExpress.ui.dxDataGridColumn[]
    data: any
    dataSource: DevExpress.data.DataSource
    dataSourceOptions: DevExpress.data.DataSourceOptions
    defaultColumns: ColumnModel[]
    referenceTables: any
    component: DevExpress.ui.dxDataGrid
    gridElement: JQuery
    filterData: any
    sortData: any
    filteredColumn: string
    customWhereClause: { where: string, item: { id: string, where: string } }
    constructor(protected $scope: CommonScope, protected logger: LoggerService, protected request: (request?: GridRequestReferenceModel) => ng.IPromise<any>, protected customOptions?: CustomOptions, protected canLoadData: boolean = true, protected localDictionary: LocalDictionaryFactory = null) {
    }

    callClassConstructor() {
        if (this.request && this.request.length) {
            // Load options and data source
            var loadOptions: DevExpress.data.LoadOptions = {}
            loadOptions.sort = []
            var customStore: DevExpress.data.CustomStoreOptions = {}
            customStore.load = (options: DevExpress.data.LoadOptions) => {
                var deferred = $.Deferred()
                if (!angular.isUndefined(options.skip)) loadOptions.skip = options.skip
                if (!angular.isUndefined(options.take)) loadOptions.take = options.take
                if (!angular.isUndefined(options.sort) && angular.isArray(options.sort)) loadOptions.sort = options.sort
                if (!angular.isUndefined(loadOptions.skip) && !angular.isUndefined(loadOptions.take) && !angular.isUndefined(loadOptions.sort) && angular.isArray(loadOptions.sort)) {
                    this.filterData = options.filter
                    this.sortData = options.sort
                    this.getData((loadOptions.skip / loadOptions.take) + 1, loadOptions.take, data => {
                        if (data) {
                            this.data = data.values
                            if (this.customOptions.onDataReady) {
                                this.customOptions.onDataReady(this.data)
                            }
                            deferred.resolve(data.values, { totalCount: data.totalCount })
                        } else {
                            deferred.resolve([], { totalCount: 0 })
                        }
                    })
                } else deferred.resolve([], { totalCount: 0 })

                return deferred.promise()
            }
            var dataSource = new DevExpress.data.DataSource(customStore)
            dataSource.pageSize(100)
            this.dataSource = dataSource

            this.$scope['filterPopoverVisible'] = false

            var removedIndicator = false


            // Columns
            this.customOptions.defaultColumnsDictionary.forEach(defaultColumn => {
                var exist = false
                this.customOptions.columnsDictionary.forEach(column => {
                    if (defaultColumn.id == column.id) exist = true
                })
                if (!exist) this.customOptions.columnsDictionary.push(defaultColumn)
            })
            this.columns = this.getDxColumns(this.customOptions.columnsDictionary, this.customOptions.defaultColumnsDictionary)
        } else {
            if (this.request) {
                if (this.canLoadData)
                    this.loadData().then(() => {
                        if (this.customOptions.onDataReady) {
                            this.customOptions.onDataReady(this.data)
                        }
                    })
            } else {
                this.dataSourceOptions = {
                    store: new DevExpress.data.ArrayStore([])
                }
                var dataSource = new DevExpress.data.DataSource(this.dataSourceOptions)
                dataSource.pageSize(100)
                this.dataSource = dataSource
                if (this.customOptions.onDataReady) {
                    this.customOptions.onDataReady(this.data)
                }
            }

            var removedIndicator = false

            if (this.customOptions.columns)
                this.columns = this.customOptions.columns
            else if (this.customOptions.columnsDictionary)
                this.columns = this.getDxColumns(this.customOptions.columnsDictionary)
        }
        this.masterDetail.enabled = this.customOptions.showShortDetails == true
        this.setCustomOptions()
        this.setCustomFilterOptions()
    }

    setCustomFilterOptions() {
        this.$scope.formItems = [
            {
                dataField: 'filters',
                label: { text: i18n('Popup', 'FilterBy') + ' - ' + i18n('Popup', 'NoColumnSelected') },
                visible: true,
                editorType: 'dxTextArea',
                cssClass: 'text-area-filter'
            }
        ]
        this.$scope.filterFormOptions = {
            colCount: 1,
            height: '100%',
            labelLocation: 'top',
            items: this.$scope.formItems,
            onInitialized: e => {
                this.$scope.filterForm = e.component
            }
        }
        this.$scope.resetFilterButtonOptions = {
            text: i18n("Popup", "ResetFilter"),
            onClick: () => {
                this.$scope.filterForm.resetValues()
                this.customWhereClause = null
                this.component.refresh()
            }
        }
        this.$scope.submitFilterButtonOptions = {
            text: i18n("Popup", "Filter"),
            onClick: () => {
                var form = this.$scope.filterForm.option('formData')
                if (this.filteredColumn && form.filters) {
                    this.filteredColumn = this.filteredColumn.toUpperCase()
                    var filters = form.filters.split('\n')
                    var where = ""
                    filters.forEach(filter => {
                        if (!where.length) {
                            where = where + "( [" + this.filteredColumn + "]" + " like " + "'%" + filter + "%'"
                        }
                        else {
                            where = where + " or " + "[" + this.filteredColumn + "]" + " like " + "'%" + filter + "%'"
                        }
                    })
                    where = where + " )"
                    var item = { id: this.filteredColumn, where: where }
                    this.customWhereClause = { where: where,  item: item }
                    this.component.refresh()
                }
                if (this.$scope.isLargeScreen) {
                    this.$scope.isFilterPopoverVisible = false
                    this.$scope.filterPopover.hide()
                }
                else {
                    this.$scope.isFilterFormPopupVisible = false
                    this.$scope.filterFormPopup.hide()
                }
            }
        }

        if (this.customOptions.columnsDictionary) {
            this.$scope.filterListOptions = {
                items: this.customOptions.columnsDictionary.map(col => { return col.caption }),
                onItemClick: (e) => {
                    this.$scope.formItems[0].label = { text: i18n('Popup', 'FilterBy') + ' - ' + this.customOptions.columnsDictionary[e.itemIndex].caption }
                    if (!this.$scope.isLargeScreen) {
                        this.$scope.isFilterListPopupVisible = false
                        this.$scope.isFilterFormPopupVisible = true
                        this.$scope.filterFormPopup.show()
                    }
                    else {
                        this.$scope.filterForm.option("items", this.$scope.formItems)
                        this.$scope.filterForm.repaint()
                    }
                    this.filteredColumn = this.customOptions.columnsDictionary[e.itemIndex].name       
                }
            }
        }
    }

    getDxColumns(columns: ColumnModel[], visibleColumns?: ColumnModel[]): DevExpress.ui.dxDataGridColumn[] {
        this.defaultColumns = (<any>Object).assign({}, columns)
        var dxColumns: DevExpress.ui.dxDataGridColumn[] = []
        columns.forEach(column => {
            var visible = true
            if (visibleColumns) {
                visible = false
                visibleColumns.forEach(c => {
                    if (c.id == column.id) visible = true
                })
            }
            var dataType = 'object'
            if (column.type == CommonHelper.string) dataType = 'string'
            else if ((column.type == CommonHelper.integer || column.type == CommonHelper.number) && !column.referenceTypeId) dataType = 'number'
            else if (column.type == CommonHelper.boolean) dataType = 'boolean'
            else if (column.type == CommonHelper.date) dataType = 'date'
            else dataType = 'string'

            var dataField = column.id.toString()
            if (column.name)
                dataField = column.name.toLowerCase()

            if (column.idName) dataField = column.idName.toLowerCase()
            if (column.unitName) {
                var caption = column.caption + " [" + column.unitName + "]"
            }
            else {
                var caption = column.caption
            }

            var dxColumn: DevExpress.ui.dxDataGridColumn = {
                dataField: dataField,
                dataType: dataType,
                caption: caption,
                allowEditing: false,
                visible: visible,
                falseText: i18n('Common', 'False'),
                trueText: i18n('Common', 'True')
            }

            var filterOperations = ['=', '<>']
            if (dataType == 'string' && this.customOptions.addCustomFilters) {
                filterOperations.push('contains', 'notcontains', 'startswith', 'endswith', 'empty', 'notempty')

                var referenceTablePromise = null
                if (column.referenceTypeId && this.localDictionary) {
                    this.referenceTables = {}
                    referenceTablePromise = this.localDictionary.getReferenceTable(column.referenceTypeId).then(table => {
                        this.referenceTables[column.referenceTypeId] = table
                        if (table && table.length > 0)
                            filterOperations.push('choose')
                    })

                    dxColumn.calculateFilterExpression = (filterValues, selectedFilterOperation) => {
                        var filterExpression = []
                        if (selectedFilterOperation == 'choose') {
                            for (var i = 0; i < filterValues.length; i++) {
                                var value = angular.isString(filterValues[i]) ? filterValues[i] : filterValues[i].name
                                var filterExpr = [dxColumn.dataField, selectedFilterOperation, value]
                                if (i > 0) {
                                    filterExpression.push('or')
                                }
                                filterExpression.push(filterExpr)
                            }
                        }
                        else {
                            filterExpression.push([dxColumn.dataField, selectedFilterOperation || 'contains', filterValues])
                        }
                        return filterExpression.length > 0 ? filterExpression : null
                    }
                }
            }
            else if (dataType == 'date' || dataType == 'number') {
                filterOperations.push('<', '>', '<=', '>=', 'between')
            }

            if (dataType == 'string' && (column.type == CommonHelper.integer || column.type == CommonHelper.number)) {
                filterOperations.push('<', '>', '<=', '>=', 'between')
            }

            dxColumn.filterOperations = filterOperations.length > 0 ? filterOperations : undefined

            if (column.cellTemplate) {
                dxColumn.cellTemplate = column.cellTemplate
                dxColumn.allowSearch = false
                dxColumn.allowFiltering = false
            }

            if (column.type == CommonHelper.integer || column.type == CommonHelper.number) {
                dxColumn.customizeText = (data) => {
                    return window['DevExpress'].localization.number.format(data.value)
                }
            }

            if (column.width)
                dxColumn.width = column.width

            if (column.alignment)
                dxColumn.alignment = column.alignment

            dxColumn.editorOptions = {
                onInitialized: e => {
                    if (e.component._options.placeholder == "Start")
                        e.component._options.placeholder = i18n("Common", "Start")
                    else if (e.component._options.placeholder == "End")
                        e.component._options.placeholder = i18n("Common", "End")
                }
            }

            if (column.type == CommonHelper.date) {
                dxColumn.customizeText = (options) => {
                    if (options.value)
                        return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    return ''
                }
                dxColumn.editorOptions['displayFormat'] = i18n("Common", "DateFormatDX")
            }

            dxColumns.push(dxColumn)
        })

        var gridSettings: any = UserConfigHelper.getUserConfig('SAVED_GRID_' + this.customOptions.name, null)
        if (gridSettings) {
            var savedColumns: any[] = gridSettings.columns
            dxColumns.forEach(column => {
                savedColumns.forEach(sColumn => {
                    if (column.dataField == sColumn.dataField)
                        column.visible = sColumn.visible
                })
            })
        }
        return dxColumns
    }

    loadData = () => {
        var deferred = $.Deferred();
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        this.request().then((result) => {
            if (!result) {
                deferred.reject()
                return
            }
            this.data = result
            this.dataSourceOptions = {
                store: new DevExpress.data.ArrayStore(result)
            }
            var dataSource = new DevExpress.data.DataSource(this.dataSourceOptions)
            dataSource.pageSize(100)
            this.dataSource = dataSource
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            deferred.resolve(this.data)
        }).catch(reject => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            deferred.reject()
        })
        return deferred.promise()
    }

    onInitialized = e => {
        if (this.customOptions.onInitialized) {
            this.customOptions.onInitialized(e)
        }

        this.component = e.component
        this.gridElement = e.element
        this.gridElement.parent().css('position', 'relative')
    }

    hackForCustomFilters = false
    hackForColumnChooser = false

    lastVisibleColumns: any[]
    onChangeColumnsTimeout

    showColumnLines = this.customOptions.showColumnLines == undefined ? true : this.customOptions.showColumnLines
    showRowLines = this.customOptions.showRowLines == undefined ? false : this.customOptions.showRowLines
    showColumnHeaders = this.customOptions.showColumnHeaders == undefined ? true : this.customOptions.showColumnHeaders

    onContentReady = e => {
        var component: DevExpress.ui.dxDataGrid = e.component
        this.component = e.component
        if (e.element.find('.custom-grid-button').length == 0 && this.customOptions && this.customOptions.buttons && this.customOptions.buttons.enabled) {
            this.addButtons(this.customOptions.buttons.gridSelector)
        }
        if (this.customOptions.onContentReady)
            this.customOptions.onContentReady(e)

        if (this.customOptions.showShortDetails) {
            var elementsToRemove = $(e.element).find('.dx-command-expand')
            elementsToRemove.closest('table').find('> colgroup > col:first-child').width(0)
            elementsToRemove.width(0)
        }

        if (!this.hackForColumnChooser) {
            this.hackForColumnChooser = true
            this.$scope.$watch(() => {
                return component['_views'].columnChooserView && component['_views'].columnChooserView._columnChooserList
            }, (result) => {
                if (result) {
                    component['_views'].columnChooserView._columnChooserList._setFocusedItem = () => {
                        // problem with column chooser
                    }
                }
            })
        }

        // hack for custom filters
        if (!this.hackForCustomFilters && this.customOptions.addCustomFilters) {
            var lastGetFilterOperationMenuItems: (() => any[]) = e.component._views.columnHeadersView._getFilterOperationMenuItems
            e.component._views.columnHeadersView._getFilterOperationMenuItems = (column) => {
                var filters: any[] = lastGetFilterOperationMenuItems.call(e.component._views.columnHeadersView, column)
                filters.forEach(filter => {
                    if (filter.name == 'notempty') {
                        filter.text = i18n('Common', 'NotEmpty')
                        filter.icon = 'comment'
                    } else if (filter.name == 'empty') {
                        filter.text = i18n('Common', 'Empty')
                        filter.icon = 'close'
                    } else if (filter.name == 'choose') {
                        filter.text = i18n('Common', 'Select')
                        filter.icon = 'check'
                    }
                })
                return filters
            }

            var lastUpdateFilterOperationChooser: (() => any[]) = e.component._views.columnHeadersView._updateFilterOperationChooser
            e.component._views.columnHeadersView._updateFilterOperationChooser = ($menu, column, $editorContainer) => {
                lastUpdateFilterOperationChooser.call(e.component._views.columnHeadersView, $menu, column, $editorContainer)
                var dxIcons: JQuery = $menu.find('dx-icon')

                var isCustom = false
                dxIcons.each((i, dxIcon) => {
                    if (column.selectedFilterOperation == 'empty') {
                        $(dxIcon).parent().empty().append('<i class="dx-icon dx-icon-close ng-scope"></i>')
                        isCustom = true
                    } else if (column.selectedFilterOperation == 'notempty') {
                        $(dxIcon).parent().empty().append('<i class="dx-icon dx-icon-comment ng-scope"></i>')
                        isCustom = true
                    } else if (column.selectedFilterOperation == 'choose') {
                        $(dxIcon).parent().empty().append('<i class="dx-icon dx-icon-check ng-scope"></i>')
                        isCustom = true
                    }
                    
                })
                var input = $editorContainer.find('.dx-texteditor-input')


                var filters = this.getFilters(component, column)

                var containerCopy = null
                if (isCustom) {
                    var selectedFilters = []
                    if (column.selectedFilterOperation == 'choose') {
                        var div = '<div id="filterButton' + column.dataField + '" class="filter-button"></div>'
                        $editorContainer.append(div)
                        $editorContainer.children('.filter-button').dxButton(<DevExpress.ui.dxButtonOptions>{
                            text: i18n('Common', 'SelectPlaceholder'),
                            onClick: e => {
                                this.$scope['filterPopoverVisible'] = !this.$scope['filterPopoverVisible']
                                if (this.$scope['filterPopoverVisible']) {
                                    if ($editorContainer.children('.filter-popover').length == 0)
                                        $editorContainer.append('<div class="filter-popover"></div>')
                                    $editorContainer.children('.filter-popover').dxPopover(<DevExpress.ui.dxPopoverOptions>{
                                        target: '#filterButton' + column.dataField,
                                        width: 400,
                                        bindingOptions: {
                                            visible: 'filterPopoverVisible'
                                        },
                                        visible: true,
                                        onHiding: e => {
                                            this.$scope['filterPopoverVisible'] = false
                                        },
                                        contentTemplate: contentElement => {
                                            if (contentElement.children('.tag-box').length == 0)
                                                contentElement.append('<div class="tag-box"></div>')
                                            contentElement.children('.tag-box').dxTagBox(<DevExpress.ui.dxTagBoxOptions>{
                                                dataSource: this.referenceTables[_.find(this.defaultColumns, col => {
                                                    if (col.name.toLocaleLowerCase() == column.dataField)
                                                        return true
                                                    return false
                                                }).referenceTypeId],
                                                displayExpr: e => {
                                                    return e ? e.caption : null
                                                },
                                                displayValue: 'caption',
                                                value: selectedFilters || [],
                                                visible: true,
                                                pagingEnabled: true,
                                                searchEnabled: true,
                                                noDataText: i18n("Common", "NoDataTypesText"),
                                                placeholder: i18n("Common", "SelectPlaceholder"),
                                                onValueChanged: el => {
                                                    var filters = this.getFilters(component, column)

                                                    if (el.value) {
                                                        if (filters.length > 0 && el.value.length > 0) filters.push('and')
                                                        selectedFilters = []
                                                        el.value.forEach((val, index) => {
                                                            selectedFilters.push(val)
                                                            var filter = [column.dataField, column.selectedFilterOperation, val.id]
                                                            filters.push(filter)
                                                            if (index < el.value.length - 1)
                                                                filters.push('or')
                                                        })
                                                    }
                                                    if (filters.length == 0)
                                                        component.filter(null)
                                                    else
                                                        component.filter(filters)
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    } else if (column.selectedFilterOperation) {
                        $editorContainer.children('.filter-button').remove()
                        var filter = [column.dataField, column.selectedFilterOperation, ' ']
                        if (filters.length > 0) filters.push('and')
                        filters.push(filter)
                        input.css('opacity', 0)
                    }
                } else {
                    $editorContainer.children('.filter-button').remove()
                    input.css('opacity', 1)
                }

                if (filters.length == 0)
                    component.filter(null)
                else
                    component.filter(filters)
            }
            this.hackForCustomFilters = true
        }
        // end of hack for custom filters
        if (this.customOptions.showShortDetails) {
            var visibleColumns: any[] = e.component._controllers.columns._visibleColumns[0]
            if (!angular.equals(visibleColumns, this.lastVisibleColumns)) {
                if (this.onChangeColumnsTimeout) clearTimeout(this.onChangeColumnsTimeout)
                this.onChangeColumnsTimeout = setTimeout(() => {
                    this.lastVisibleColumns = visibleColumns
                    e.component.collapseAll(-1)
                    e.component.deselectAll()
                }, 100)
            }
        }
    }

    getFilters(component: DevExpress.ui.dxDataGrid, column) {
        var lastF: any = component.filter()
        var newFilters = []
        if (lastF) {
            lastF.forEach(lf => {
                if (lf != 'and' && lf.length == 3) {
                    var lfArray: any[] = lf
                    if (!(column.dataField == lfArray[0])) {
                        if (newFilters.length > 0 && newFilters[newFilters.length - 1][0] != lfArray[0]) newFilters.push('and')
                        else if (newFilters.length > 0 && newFilters[newFilters.length - 1][0] == lfArray[0]) newFilters.push('or')
                        newFilters.push(lfArray)
                    }
                }
            })
        }
        return newFilters
    }

    addButtons(selector) {
        var buttonsSettings: ButtonsSettings = this.customOptions.buttons
        var header = angular.element(selector).find('.dx-datagrid-header-panel .dx-toolbar > .dx-toolbar-items-container > .dx-toolbar-after')
        this.$scope[buttonsSettings.optionsScope].forEach((element, index) => {
            header.prepend(buttonsSettings.compile('<div ng-show="isLargeScreen"' + (element.permissions ? 'ng-if="$root.hasPermissions(' + element.permissions + ')"' : '') + ' class="custom-grid-button dx-toolbar-button dx-toolbar-menu-container"><div class="dx-item-content dx-toolbar-item-content"><div id="' + (element.idSelector ? element.idSelector : '') + '" dx-button="' + buttonsSettings.optionsScope + '[' + index + '].options"></div></div></div>' + (element.customAppendix ? element.customAppendix : ''))(this.$scope))
        })
        header.prepend(buttonsSettings.compile('<div ng-hide="isLargeScreen" class="custom-grid-button dx-toolbar-button dx-toolbar-menu-container"><div class="dx-item-content dx-toolbar-item-content"><div id="{{popoverListButtonOptions.idSelector}}" dx-button="popoverListButtonOptions.options"></div></div></div>')(this.$scope))
        this.$scope.$broadcast('customButtonsGridAdded', header)
    }

    getWhereClause(filter: any): { where: string, items: { id: string, where: string }[] } {
        var strings = [], items = []
        if (angular.isArray(filter)) {
            var array: any[] = filter
            var isDate = array.length == 3 && Object.prototype.toString.call(array[2]) === '[object Date]'
            var isBool = array.length == 3 && Object.prototype.toString.call(array[2]) === '[object Boolean]'
            if (array.length == 3 && angular.isString(array[0]) && angular.isString(array[1]) && (angular.isString(array[2]) || angular.isNumber(array[2]) || typeof array[2] == "boolean" || isDate)) {
                var columnId: string = array[0]
                var operator: string = array[1]
                var value: (string | number) = array[2]
                if (isDate) value = moment(value).format('YYYY-MM-DD HH:mm:ss')
                if (isBool) value = value.toString() == 'true' ? 1 : 0

                columnId = columnId.toUpperCase()

                var where = "[" + columnId + "]"
                if (operator == "contains") {
                    where += " like '%" + value + "%'"
                } else if (operator == "notcontains") {
                    where += " not like '%" + value + "%'"
                } else if (operator == "startswith") {
                    where += " like '" + value + "%'"
                } else if (operator == "endswith") {
                    where += " like '%" + value + "'"
                } else if (operator == "empty") {
                    where = "(" + where + " is null or " + where + " = ''" + ")"
                } else if (operator == "notempty") {
                    where = "(" + where + " is not null and " + where + " != ''" + ")"
                } else if (operator == "choose") {
                    where += " like '" + value + "'"
                } else {
                    if (angular.isNumber(value))
                        where += " " + operator + " " + value
                    else
                        where += " " + operator + " '" + value + "'"
                }
                strings.push(where)
                items.push({ id: columnId, where: where })
            } else {
                strings.push('(')
                array.forEach(value => {
                    var innerWhere = this.getWhereClause(value)
                    if (innerWhere.where != "") strings.push(innerWhere.where)
                    if (innerWhere.items.length > 0) items.push(...innerWhere.items)
                })
                strings.push(')')
            }
        } else if (angular.isString(filter)) {
            strings.push(filter)
        }
        return {
            where: strings.join(' '),
            items: items
        }
    }

    getRequest(pageSize: number = 0, pageNumber: number = 0, onlyVisibleColumns: boolean = false, customWhereClause = null) {
        var searchText;
        if (this.component)
            searchText = this.component.option('searchPanel').text

        var columns = []
        if (onlyVisibleColumns && this.component) {
            var visibleColumns = []
            for (var i = 0; i < this.component.columnCount(); i++) {
                var col = <any>this.component.columnOption(i)
                col.filterValue = null
                if (col.visible) {
                    var column = _.find(this.customOptions.columnsDictionary, (element) => {
                        return element.name == col.dataField
                    })
                    visibleColumns.push(column)
                }
            }
            columns = visibleColumns
        }
        else {
            columns = this.customOptions.columnsDictionary
        }

        var whereClause = this.getWhereClause(this.filterData)
        if (this.customWhereClause && this.customWhereClause.where.length) {
            whereClause.items.push(this.customWhereClause.item)
            if (whereClause.where.length) {
                whereClause.where = whereClause.where + " and "
            }
            whereClause.where = whereClause.where + this.customWhereClause.where
        }

        columns.forEach(c => {
            c.sortDesc = null
            c.where = null
            whereClause.items.forEach((whereCl, index) => {
                if (c.name == whereCl.id.toLowerCase()) {
                    if (!c.where) c.where = [whereCl.where]
                    else c.where.push(whereCl.where)
                }
            })
        })

        if (angular.isArray(this.sortData)) {
            var sortArray: any[] = this.sortData
            sortArray.forEach(s => {
                var id = s.selector
                columns.forEach(c => {
                    if (c.name == id.toLowerCase()) c.sortDesc = s.desc
                })
            })
        }

        var req = new GridRequestReferenceModel()
        req.idReferenceType = this.customOptions.idReferenceType
        req.columns = columns
        req.pageSize = pageSize
        req.pageNumber = pageNumber
        req.where = whereClause.where
        return req
    }

    getData = (pageNumber: number, pageSize: number, callback: (result: GridModel) => void = null, whereClause = null) => {
        var req = this.getRequest(pageSize, pageNumber, false, whereClause)
        this.request(req).then(result => {
            if (callback) callback(result)
        }).catch(reason => this.logger.log(reason))
    }

    clickTimer: any
    lastRowCLickedId: number

    columnFixing = {
        enabled: false,
        texts: {
            fix: i18n('Grid', 'Fix'),
            leftPosition: i18n('Grid', 'LeftPosition'),
            rightPosition: i18n('Grid', 'RightPosition'),
            unfix: i18n('Grid', 'Unfix')
        }
    }

    noDataText = i18n('Common', 'NoData')

    columnChooser = {
        enabled: false,
        emptyPanelText: i18n('Grid', 'ColumnChooserText'),
        title: i18n('Grid', 'ColumnChooserTitle'),
        mode: 'select'
    }

    stateStoring = {
        enabled: this.customOptions.name != null,
        type: "custom",
        storageKey: this.customOptions.name,
        customLoad: () => UserConfigHelper.getUserConfig('SAVED_GRID_' + this.customOptions.name, null),
        customSave: (gridState) => UserConfigHelper.setUserConfig('SAVED_GRID_' + this.customOptions.name, gridState)
    }

    editing = {
        mode: 'cell',
        allowUpdating: true
    }

    hoverStateEnabled = true
    rowAlternationEnabled = true
    allowColumnReordering = true
    allowColumnResizing = true
    height = '100%'
    sorting = {
        mode: 'multiple',
        ascendingText: i18n('Grid', 'SortAscending'),
        clearText: i18n('Grid', 'ClearSorting'),
        descendingText: i18n('Grid', 'SortDescending')
    }

    groupPanel = {
        visible: false,
        emptyPanelText: i18n('Grid', 'GroupPanelText')
    }

    loadPanel = {
        text: i18n('Grid', 'Loading'),
        enabled: false
    }
    paging = {
        pageSize: 100,
        enabled: true
    }
    export = {
        enabled: true,
        allowExportSelectedData: true,
        texts: {
            exportAll: i18n('Grid', 'ExportAll'),
            exportSelectedRows: i18n('Grid', 'ExportSelectedRows'),
            exportTo: i18n('Grid', 'ExportFileName')
        }
    }

    filterRow = {
        visible: true,
        showOperationChooser: true,
        operationDescriptions: <any>{ '=': i18n('Common', 'Equal'), '<>': i18n('Common', 'NotEqual'), '<': i18n('Common', 'LessThan'), '<=': i18n('Common', 'LessThanOrEqual'), '>': i18n('Common', 'GreaterThan'), '>=': i18n('Common', 'GreaterThanOrEqual'), 'startswith': i18n('Common', 'StartsWith'), 'contains': i18n('Common', 'Contains'), 'notcontains': i18n('Common', 'NotContains'), 'endswith': i18n('Common', 'EndsWith'), 'between': i18n('Common', 'Between') },
        resetOperationText: i18n('Common', 'Reset'),
        showAllText: i18n('Common', 'All')
    }
    searchPanel = {
        visible: true,
        placeholder: i18n('Common', 'Search'),
        searchVisibleColumnsOnly: true
    }

    pager = {
        visible: 'auto',
        showPageSizeSelector: false,
        showNavigationButtons: false,
        showInfo: false,
        allowedPageSizes: 'auto'
    }

    width = undefined
    columnAutoWidth = false
    selection = {
        mode: 'multiple',
        showCheckBoxesMode: 'none'
    }

    selectedRowKeys = []

    lastExpandedKey: any

    setCustomOptions() {
        if (this.customOptions.editing)
            this.editing = this.customOptions.editing
        if (this.customOptions.export)
            this.export['enabled'] = this.customOptions.export['enabled']
        if (this.customOptions.searchPanel)
            this.searchPanel = this.customOptions.searchPanel
        if (this.customOptions.pager)
            this.pager = this.customOptions.pager
        if (this.customOptions.width)
            this.width = this.customOptions.width
        if (this.customOptions.height)
            this.height = this.customOptions.height
        if (this.customOptions.filterRow)
            this.filterRow = this.customOptions.filterRow
        if (this.customOptions.selectedRowKeys)
            this.selectedRowKeys = this.customOptions.selectedRowKeys
        if (this.customOptions.selection)
            this.selection = this.customOptions.selection
        if (this.customOptions.dataSource)
            this.dataSource = this.customOptions.dataSource
        if (this.customOptions.allowColumnReordering)
            this.allowColumnReordering = this.customOptions.allowColumnReordering
        if (this.customOptions.masterDetail)
            this.masterDetail = this.customOptions.masterDetail
        if (this.customOptions.paging)
            this.paging = this.customOptions.paging
        if (this.customOptions.columnAutoWidth)
            this.columnAutoWidth = this.customOptions.columnAutoWidth
        if (this.customOptions.columnFixing)
            this.columnFixing = this.customOptions.columnFixing
        if (this.customOptions.loadPanel)
            this.loadPanel = this.customOptions.loadPanel
        if (this.customOptions.scrolling)
            this['scrolling'] = this.customOptions.scrolling
    }

    onRowUpdated = (e) => {
        if (this.customOptions.onRowUpdated) {
            this.customOptions.onRowUpdated(e)
        }
    }

    onEditorPreparing = (e) => {
        if (this.customOptions.onEditorPreparing) {
            this.customOptions.onEditorPreparing(e)
        }
    }

    onRowClick = (e) => {
        if (e.rowType == 'data') {
            if (this.clickTimer && this.lastRowCLickedId === e.rowIndex) {
                clearTimeout(this.clickTimer)
                this.clickTimer = null
                this.lastRowCLickedId = e.rowIndex
                if (this.customOptions.onRowDoubleClick)
                    this.customOptions.onRowDoubleClick(e)
                if (this.onSelectionChangedTimeout) clearTimeout(this.onSelectionChangedTimeout)
            } else {
                this.clickTimer = setTimeout(() => {
                    if (this.customOptions.onRowClick) {
                        this.customOptions.onRowClick(e)
                    }
                    this.clickTimer = null
                }, 250)
            }
            this.lastRowCLickedId = e.rowIndex
        }
    }

    onRowPrepared = (e) => {
        if (this.customOptions.onRowPrepared) {
            this.customOptions.onRowPrepared(e)
        }
    }

    masterDetail = {
        autoExpandAll: false,
        enabled: false,
        template: (container, info) => {
            $('<div class="short-details">').appendTo(container)
        }
    }

    onCellPrepared = (e) => {
        if (this.customOptions.onCellPrepared) {
            this.customOptions.onCellPrepared(e)
        }
    }

    onSelectionChangedTimeout
    onSelectionChanged = (e) => {
        if (this.$scope.dataGrid) {
            this.$scope.disabled = this.$scope.dataGrid.getSelectedRowsData().length == 0
        }
        if (this.onSelectionChangedTimeout) clearTimeout(this.onSelectionChangedTimeout)
        this.onSelectionChangedTimeout = setTimeout(() => {
            if (this.customOptions.onSelectionChanged) {
                this.customOptions.onSelectionChanged(e)
            }
        }, 500)
    }

    onContextMenuPreparing = (e) => {
        var data = e.component.getSelectedRowsData()
        if (data.length == 0)
            return
        else if (this.customOptions.onContextMenuPreparing) {
            this.customOptions.onContextMenuPreparing(e)
        }
    }

    private rollbackSearchPanelText = (searchText: string) => {
        if (angular.isArray(this.filterData)) {
            for (var i in this.filterData) {
                var x = this.filterData[i]

                if (angular.isArray(x)) {
                    for (var j in x) {
                        var y = x[j]

                        if (angular.isArray(y)) {
                            for (var k in y) {
                                var z = y[k]

                                if (y.length == 3) {
                                    y[2] = searchText
                                }
                                else if (angular.isArray(z) && z.length == 3) {
                                    z[2] = searchText
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}