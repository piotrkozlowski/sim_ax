﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FieldModel } from '../../api/api.models'
import { i18n } from '../i18n/common.i18n'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'

require('./gallery.scss')

interface GalleryScope extends ng.IScope {
    schemas: any[]
    isDxGallery: boolean
    schemaGalleryOptions: DevExpress.ui.dxGalleryOptions
    slickOptions: any
    isLoaded: boolean
    id: number
}

export class GalleryDirective implements ng.IDirective {
    constructor() { }

    restrict = 'E'
    scope = {
        schemas: '='
    }
    template = require("./gallery.tpl.html")

    link = (scope: ng.IScope, element: JQuery) => {

    }

    controller = ['$scope', '$element', 'api', 'logger', 'localDictionary', '$timeout', ($scope: GalleryScope, $element: JQuery, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, $timeout: ng.ITimeoutService) => {
        $scope.id = new Date().getTime()
        $scope.isDxGallery = window['config']['GALLERY_TYPE'] == 'dx-gallery'
        $scope.schemaGalleryOptions = {
            height: '100%',
            width: '100%',
            items: $scope.schemas
        }
        
        $scope.slickOptions = {
            autoplay: false,
            enabled: true,
            dots: true,
            method: {},
            event: {}
        }
        
        $scope.$watch(() => {
            return angular.element('.schemas' + $scope.id).width()
        }, (val) => {
            slickUpdate()
        })

        var slickUpdate = () => {
            $scope.isLoaded = false
            $timeout(() => {
                $scope.isLoaded = true
            })
        }
    }]
}