﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel } from '../../api/api.models'
import { LoggerService } from  '../common.module'
import { CreateEditPopupController, CreateEditPopupOptions } from './create-edit-popup.controller'

require('./create-edit-popup.scss')

export class CreateEditPopupDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'E'
    scope = {
        options: '=',
        editPassword: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'localDictionary', ($scope, $q, $timeout: ng.ITimeoutService, api: Api, localDictionary: LocalDictionaryFactory) => new CreateEditPopupController($scope, $q, $timeout, api, localDictionary)]
    template = require("./create-edit-popup.tpl.html")
    transclude = true
}

export { CreateEditPopupOptions }