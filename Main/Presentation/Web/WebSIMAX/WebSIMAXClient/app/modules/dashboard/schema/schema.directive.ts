﻿import 'angular'

require('./schema.scss')

export class SchemaDirective implements ng.IDirective {
    constructor() {

    }
    scope = {
        data: '=',
        itemTitle: '='
    }
    restrict = 'E'
    template = require("./schema.tpl.html")
    transclude = true
    replace = true
}