﻿import 'angular'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { EventsNames } from '../../../helpers/events-names'

require('./tile.scss')

interface TileScope extends ng.IScope {
    id: number
    data: {
        referenceType: number,
        referenceData: number[]
        icon: string
        iconName: string
        iconVisible: boolean
        iconExtension: string
        backgroundImage: string
        backgroundName: string
        backgroundImageExtension: string,
        borderRadius: number[]
        shadowColor: string
        shadowHorizontalLenght: number
        shadowVerticalLenght: number
        shadowBlurRadius: number
        shadowSpreadRadius: number
    }
    action: () => void
    iconType: string
    style: any,
    backgroundImage: string
    borderRadius: string
    boxShadow: string
}

export class TileDirective implements ng.IDirective {
    constructor() {
        
    }
    scope = {
        data: '=',
        itemTitle: '='
    }

    controller = ['$scope', '$timeout', '$state', ($scope: TileScope, $timeout: ng.ITimeoutService, $state: angular.ui.IStateService) => {
        $scope.action = null

        var extensionRegex = /(?:\.([^.]+))?$/

        if ($scope.data.iconVisible && $scope.data.icon) {
            if ($scope.data.iconExtension == "Unknown" && $scope.data.iconName) {
                var ext = extensionRegex.exec($scope.data.iconName)[1]
                if (ext && ext.toLowerCase() == 'svg') $scope.iconType = 'svg+xml'
            } else if ($scope.data.iconExtension == "Png") {
                $scope.iconType = 'png'
            } else if ($scope.data.iconExtension == "Svg") {
                $scope.iconType = 'svg+xml'
            } else {
                $scope.iconType = 'png'
            }
        }

        if ($scope.data.backgroundImage) {
            if ($scope.data.backgroundImageExtension == "Unknown" && $scope.data.backgroundName) {
                var ext = extensionRegex.exec($scope.data.backgroundName)[1]
                if (ext && ext.toLowerCase() == 'svg') $scope.backgroundImage = "url('data:image/svg+xml;base64," + $scope.data.backgroundImage + "')"
            } else if ($scope.data.iconExtension == "Png") {
                $scope.backgroundImage = "url('data:image/png;base64," + $scope.data.backgroundImage + "')"
            } else if ($scope.data.iconExtension == "Svg") {
                $scope.backgroundImage = "url('data:image/svg+xml;base64," + $scope.data.backgroundImage + "')"
            } else {
                $scope.backgroundImage = "url('data:image/png;base64," + $scope.data.backgroundImage + "')"
            }
        } else {
            $scope.backgroundImage = "none"
        }

        if ($scope.data.borderRadius.length > 0) {
            $scope.borderRadius = $scope.data.borderRadius.map(r => r + 'px').join(' ')
        } else {
            $scope.borderRadius = '10px'
        }

        // box shadow
        var boxShadow = ''

        if ($scope.data.shadowHorizontalLenght) boxShadow += $scope.data.shadowHorizontalLenght + 'px '
        else boxShadow += '10px '

        if ($scope.data.shadowVerticalLenght) boxShadow += $scope.data.shadowVerticalLenght + 'px '
        else boxShadow += '10px '

        if ($scope.data.shadowBlurRadius) boxShadow += $scope.data.shadowBlurRadius + 'px '
        else boxShadow += '20px '

        if ($scope.data.shadowSpreadRadius) boxShadow += $scope.data.shadowSpreadRadius + 'px '
        else boxShadow += '-10px '

        if ($scope.data.shadowColor) boxShadow += 'rgba(' + $scope.data.shadowColor + ')'
        else boxShadow += 'rgba(0,0,0,0.8)'

        $scope.boxShadow = boxShadow
        // end of box shadow

        if ($scope.data.referenceType == CommonHelper.IdLocation && $scope.data.referenceData) {
            this.initDetails($scope, $timeout, $state, 'locations', 'showLocationDetails', [Permissions.LOCATION_LIST], EventsNames.OPEN_LOCATION_DETAILS_POPUP)
        } else if ($scope.data.referenceType == CommonHelper.IdAlarmEvent && $scope.data.referenceData) {
            this.initDetails($scope, $timeout, $state, 'alarms', 'showAlarmDetails', [Permissions.ALARM_LIST], EventsNames.OPEN_ALARMS_DETAILS_POPUP)
        }
    }]

    restrict = 'E'
    template = require("./tile.tpl.html")
    transclude = true
    replace = true

    initDetails($scope: TileScope, $timeout: ng.ITimeoutService, $state: angular.ui.IStateService, detailsName: string, showDetailsName: string, permissions: string[], eventName: string) {
        Permissions.runIfHasPermissions(permissions, () => {
            var referenceData: any[] = $scope.data.referenceData

            if (referenceData.length == 1) {
                $scope.id = $scope.$id

                $scope.action = () => {
                    $scope[showDetailsName] = true
                    $timeout(() => {
                        $scope.$broadcast(eventName + $scope.id, referenceData[0], [])
                    }, 0)
                }
            } else if (referenceData.length > 1) {
                $scope.action = () => {
                    $state.go(detailsName, { ids: referenceData })
                }
            }
        })
    }
}