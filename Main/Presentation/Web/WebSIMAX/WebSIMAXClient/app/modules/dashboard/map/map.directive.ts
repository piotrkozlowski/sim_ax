﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'

declare var google: any

export class MapDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        data: '=',
        itemTitle: '=',
        fitToScreen: '=?'
    }
        
    restrict = 'E'
    link = (scope: ng.IScope, element: JQuery) => {
        var addPolygons = (map) => {
            var polygons = scope['data'].polygons
            polygons.forEach(polygon => {
                var coords = polygon.coords.map(c => {
                    return {
                        lat: c.latitude,
                        lng: c.longitude
                    }
                })

                var infoTooltip = new google.maps.InfoWindow({
                    content: polygon.tooltip,
                })

                var newPolygon = new google.maps.Polygon({
                    paths: coords,
                    strokeColor: polygon.lineColor,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: polygon.fillColor,
                    fillOpacity: 0.35
                })

                newPolygon.addListener('click', (e) => {
                    infoTooltip.setPosition(e.latLng)
                    infoTooltip.open(map)
                })

                newPolygon.setMap(map)
            })
        }

        var addCircles = (map) => {
            var ranges = scope['data'].ranges
            ranges.forEach(range => {
                var coords = {
                    lat: range.coords.latitude,
                    lng: range.coords.longitude
                }

                var infoTooltip = new google.maps.InfoWindow({
                    content: range.tooltip,
                    position: coords
                })

                var newCircle = new google.maps.Circle({
                    center: coords,
                    strokeColor: range.lineColor,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: range.fillColor,
                    fillOpacity: 0.35,
                    radius: range.radius * 1000,
                    map: map
                })

                newCircle.addListener('click', () => {
                    infoTooltip.open(map)
                })
            })
        }

        scope['options'] = {
            width: "100%",
            height: scope['fitToScreen'] ? '100%' : 350,
            provider: "google",
            controls: true,
            autoAdjust: true,
            zoom: 20,
            onInitialized: e => {
                var map: DevExpress.ui.dxMap = e.component
                scope.$watch('data', (data: any) => {
                    map.option('zoom', 20)
                    map.option('markers', [])

                    scope['showMap'] = true
                    var cntr = 0;
                    var chunkSize = 100

                    scope['isLoading'] = true
                    var next = () => {
                        if (cntr < data.markers.length) {
                            setTimeout(() => {
                                var markers = []

                                for (var i = cntr; i < cntr + chunkSize; i++) {
                                    if (i == data.markers.length) break;

                                    var marker = data.markers[i]
                                    var m = {
                                        location: {
                                            lat: marker.coords.latitude,
                                            lng: marker.coords.longitude
                                        }
                                    }
                                    if (marker.tooltip) {
                                        m['tooltip'] = { text: marker.tooltip }
                                    }
                                    if (marker.icon && marker.icon != '')
                                        m['iconSrc'] = 'data:image/*;base64,' + marker.icon
                                    else if (marker.color && marker.color != '') {
                                        var rgb = (<string>marker.color).split(', ')
                                        if (rgb.length == 3) {
                                            var hex = rgb.map(c => {
                                                var h = parseInt(c).toString(16)
                                                return (h.length == 1) ? '0' + h : h
                                            }).join('')
                                            if (hex.length == 6)
                                                m['iconSrc'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + hex
                                        }
                                    } else
                                        m['iconSrc'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569'

                                    markers.push(m)
                                    map.addMarker(m)
                                }
                                cntr += chunkSize
                                map.addMarker(markers).then(next)
                            }, 1)
                        } else {
                            scope['showMap'] = true
                            scope['isLoading'] = false
                        }
                    }
                    next()
                })
            },
            onReady: e => {
                var map = e.originalMap
                if (scope['data']) {
                    addPolygons(map)
                    addCircles(map)
                }
            }
        }        
    }
    template = require("./map.tpl.html")
    transclude = true
    replace = true
}