﻿import 'angular'

export class MeterDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        data: '=',
        itemTitle: '='
    }

    restrict = 'E'
    link = (scope: ng.IScope, element: JQuery) => {
        var data = scope['data']
        scope['options'] = {
            geometry: {
                startAngle: 220,
                endAngle: -40
            },
            scale: {
                startValue: data.min,
                endValue: data.max,
                tick: {
                    visible: true,
                    color: 'black'
                },
                minorTick: {
                    visible: true,
                    color: 'black'
                }
            },
            value: data.value,
            valueIndicator: {
                type: 'triangleNeedle',
                color: 'black',
                width: 6,
                spindleGapSize: 0,
                spindleSize: 0,
                offset: 1
            },
            rangeContainer: {
                backgroundColor: 'red',
                ranges: [
                    {
                        startValue: data.max - data.max * 0.1,
                        endValue: data.max,
                        color: 'yellow'
                    }
                ]
            },
            size: {
                height: 150
            },
            title: {
                text: scope['itemTitle'],
                font: {
                    size: 14,
                    weight: 600
                }
            },
            onInitialized: e => {
                var gauge: DevExpress.viz.gauges.dxCircularGauge = e.component
                scope.$watch('data.value', (value: number) => {
                    gauge.value(value)
                })
            }
        }
    }
    template = require("./meter.tpl.html")
    transclude = true
    replace = true
}