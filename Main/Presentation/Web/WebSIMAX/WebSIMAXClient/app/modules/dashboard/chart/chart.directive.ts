import 'angular'
import * as moment from 'moment'
import { i18n } from '../../common/i18n/common.i18n'

export class ChartDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        data: '=',
        itemTitle: '=',
        fitToScreen: '=?'
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    getDataSource(series: any[]) {
        var dataSource = []
        for (var i = 0; i < series.length; ++i) {
            for (var j = 0; j < series[i].seriesPoints.length; ++j) {
                var existData = null

                var xValue = series[i].seriesPoints[j].xValue
                if (series[i].xTypeName == "System.DateTime") {
                    xValue = moment(xValue).toDate()//.format(i18n("Common", "DateTimeFormatMomentJS"))
                }
                
                var yValue = series[i].seriesPoints[j].yValue
                if (!isNaN(yValue)) {
                    yValue = parseFloat(yValue)
                } else if (series[i].yTypeName == "System.DateTime") {
                    yValue = moment(yValue).format(i18n("Common", "DateTimeFormatMomentJS"))
                } 

                dataSource.forEach((d) => {
                    if (d.day == xValue) existData = d
                })
                if (existData) {
                    existData['value' + i] = yValue
                } else {
                    var newData = { day: xValue }
                    newData['value' + i] = yValue
                    dataSource.push(newData)
                }
            }
        }
        return dataSource
    }

    
    restrict = 'E'
    link = (scope: ng.IScope, element: JQuery) => {
        var colors = ['red', 'blue', '#008000', '#6600cc', 'green', 'yellow', 'purple']
        var data = scope['data']
        var hasZeroValue = false

        var series = []

        for (var i = 0; i < data.series.length; ++i) {
            if (data.series[i]['value' + i] == 0)
                hasZeroValue = true

            series.push({
                argumentField: "day",
                valueField: "value" + i,
                name: data.series[i].seriesName,
                type: data.series[i].seriesType.toLowerCase(),
                color: colors[i],
                point: { visible: false }
            })
        }

        scope['options'] = {
            dataSource: new DevExpress.data.DataSource({
                load: () => {
                    var deferred = $.Deferred()
                    var dataSource = this.getDataSource(scope['data'].series)
                    deferred.resolve(dataSource, { totalCount: dataSource.length })
                    return deferred.promise()
                }
            }),
            series: series,
            size: {
                height: scope['fitToScreen'] ? '100%' : 350
            },
            title: {
                text: scope['itemTitle'],
                font: {
                    size: 20,
                    weight: 600
                }
            },
            legend: {
                border: {
                    visible: true,
                    color: '#000'
                }
            },
            argumentAxis: {
                grid: { visible: true },
                title: data.series[0] ? data.series[0].xCaption : 'Title X',
                valueType: 'datetime',
                label: {
                    customizeText: (options) => {
                        return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                    }
                }
            },
            valueAxis: {
                grid: { visible: true },
                visible: true,
                title: data.series[0] ? data.series[0].yCaption : 'Title Y',
                min: hasZeroValue ? 0 : undefined
            },
            tooltip: {
                enabled: true,
                customizeTooltip: (point: any) => {
                    let color = point.point.getColor()       
                    var value = point.valueText
                    var argument = moment(new Date(point.argument)).format(i18n("Common", "DateTimeFormatMomentJS"))
                    return {
                        html: "<strong><span style='color:'" + color + ";'>" + point.seriesName + ": </span>" + value + " " + "</strong><br/>" + argument
                    }
                }
            },
            onInitialized: e => {
                var map: DevExpress.viz.charts.dxChart = e.component
                scope.$watchCollection('data', (data) => {
                    map.option('dataSource').load()
                })
            }
        }
    }
    template = require("./chart.tpl.html")
    transclude = true
    replace = true
}