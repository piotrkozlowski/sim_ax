﻿import 'angular'

import { CommonTableDataGridOptions } from '../../common/common.table-data-grid-options'
import { CommonExtendedDataGridOptions, CommonScope } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'

interface TableScope extends CommonScope {
    options: DevExpress.ui.dxDataGridOptions
    datagrid: DevExpress.ui.dxDataGrid
    fitToScreen: boolean
}

export class TableDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        data: '=',
        itemTitle: '=',
        fitToScreen: '=?'
    }

    restrict = 'E'
    link = (scope: TableScope, element: JQuery, $timeout: ng.ITimeoutService) => {
        var data = scope['data']

        scope.$watchCollection('data.dataSource', (dataSource) => {
            data.dataSource = dataSource
        })

        if (data && data.columns && data.dataSource) {
            var customOptions = {
                dataSource: new DevExpress.data.DataSource({
                    load: (options: DevExpress.data.LoadOptions) => {
                        var deferred = $.Deferred()
                        var dataSorted = data.dataSource
                        var sortArray: any[] = <any>options.sort
                        if (sortArray && sortArray[0]) {
                            var selector = sortArray[0].selector
                            dataSorted = dataSorted.sort((a, b) => {
                                if (a[selector] == b[selector]) return 0
                                return (sortArray[0].desc ? a[selector] < b[selector] : a[selector] > b[selector]) ? 1 : -1
                            })
                        }
                        deferred.resolve(dataSorted.slice(options.skip, options.take * ((options.skip / options.take) + 1)), { totalCount: data.dataSource.length })
                        return deferred.promise()
                    }
                }),
                columns: data.columns,
                onInitialized: (e) => { scope.datagrid = e.component },
                onContentReady: () => {
                    scope.datagrid.option('pager.visible', true)
                    scope.datagrid.updateDimensions()
                },
                height: scope.fitToScreen ? '100%' : '350px'
            }
               
            scope.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, scope, null, null, customOptions)
        }
    }
    template = require("./table.tpl.html")
    transclude = true
    replace = true
}