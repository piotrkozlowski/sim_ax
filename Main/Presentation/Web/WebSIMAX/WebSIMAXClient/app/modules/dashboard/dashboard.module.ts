﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  '../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  '../api/api.module'
import { i18n } from '../common/i18n/common.i18n'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

require('./dashboard.scss')

import { DashboardController } from './dashboard.controller'

import { ChartDirective } from './chart/chart.directive'
import { MeterDirective } from './meter/meter.directive'
import { TableDirective } from './table/table.directive'
import { TileDirective } from './tile/tile.directive'
import { MapDirective } from './map/map.directive'
import { SchemaDirective } from './schema/schema.directive'

var moduleName = 'simax.dashboard'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('DashboardController', ['$scope', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new DashboardController($scope, $timeout, api, logger, localDictionary)])

module.directive('itemChart', [() => new ChartDirective()])
module.directive('itemMeter', [() => new MeterDirective()])
module.directive('itemTable', [() => new TableDirective()])
module.directive('itemTile', [() => new TileDirective()])
module.directive('itemMap', [() => new MapDirective()])
module.directive('itemSchema', [() => new SchemaDirective()])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.DASHBOARD_VIEW], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['DASHBOARD'], 'home', 'Home', 'dashboard', 1)
        $stateProvider.state('dashboard', {
            url: '/dashboard',
            controller: 'DashboardController',
            template: require('./dashboard.tpl.html')
        })
    })
}])

export default moduleName