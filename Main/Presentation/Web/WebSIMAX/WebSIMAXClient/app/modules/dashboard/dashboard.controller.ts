﻿import 'angular'

import { DashboardModel, DashboardRequestModel } from  '../api/api.models'
import { Api, LocalDictionaryFactory } from  '../api/api.module'
import { LoggerService } from  '../common/common.module'

interface DashboardScope extends ng.IScope {
    model: DashboardModel
    rowHeight: string
    dashboardStyle: any
}

export class DashboardController {
    constructor(private $scope: DashboardScope, private $timeout: ng.ITimeoutService, private api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        api.dashboard.getDashboardConfig().then(result => {
            $scope.model = result

            $scope.dashboardStyle = {}
            if (result.maxWidth) {
                $scope.dashboardStyle['max-width'] = result.maxWidth
                $scope.dashboardStyle['margin-left'] = 'auto'
                $scope.dashboardStyle['margin-right'] = 'auto'
            }
            if (result.width)
                $scope.dashboardStyle['width'] = result.width

            if (result.maxHeight)
                $scope.dashboardStyle['max-height'] = result.maxHeight

            if (result.fitToScreen)
                $scope.rowHeight = (100 / result.rows.length) + '%'
            else
                $scope.rowHeight = 'auto'

            this.updateItems()
        })

        angular.element(window).resize(() => {
            this.refreshHeight()
        })
    }

    refreshHeight = () => {
        var dashboardFitToScreen = angular.element('.dashboard.fit-to-screen .dashboard-tile')
        if (dashboardFitToScreen.length > 0) {
            dashboardFitToScreen.height('calc(100% - 20px)')
        } else {
            angular.element('.dashboard .dashboard-row').each((index, row) => {
                var maxHeight = 0
                angular.element(row).find('.dashboard-tile').height('auto').each((i, e) => {
                    var height = $(e).height()
                    if (height > maxHeight) maxHeight = height
                }).height(maxHeight)

                if (!this.$scope['model'].fitToScreen) {
                    var maxHeightColumn = 0
                    angular.element(row).find('.column').height('auto').each((i, e) => {
                        var height = $(e).height()
                        if (height > maxHeightColumn) maxHeightColumn = height
                    }).height(maxHeightColumn)
                } else {
                    angular.element(row).find('.column').height('100%')
                }
            })
        }
    }

    updateView() {
        if (this.$scope.$root) {
            this.$timeout(() => {
                this.refreshHeight()
            }, 0)
        }
    }

    updateItems() {
        if (!this.$scope.model) return
        var refreshCount = 0
        this.$scope.model.rows.forEach(row => {
            row.columns.forEach(column => {
                var refreshFunction = () => {
                    if (this.$scope.$root) {
                        var req = new DashboardRequestModel();
                        req.groupId = column.groupId
                        req.ids = column.items.map(i => i.id)

                        refreshCount++
                        this.$scope.$root.$broadcast('disableLoadingIndicator')
                        this.api.dashboard.getDashbordItems(req).then(r => {
                            if (r) {
                                r.result.forEach(obj => {
                                    column.items.forEach(item => {
                                        if (obj['id'] == item.id) {
                                            item.data = obj
                                        }
                                    })
                                })
                                refreshCount--
                                if (refreshCount == 0) {
                                    if (this.$scope.$root) {
                                        this.updateView()
                                    }
                                }

                                this.$timeout(() => {
                                    refreshFunction()
                                }, column.refreshPeriod * 1000)

                                this.updateView()
                            }
                            this.$scope.$root.$broadcast('enableLoadingIndicator')
                        })
                    }
                }

                this.$timeout(() => {
                    refreshFunction()
                }, 0)
            })
        })
    }
}