﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from './../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { PopupType } from './../../../helpers/popup-manager'

require('./contacts-create-edit.scss')

interface ContactCreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class ContactCreateEditDirective implements ng.IDirective {
    constructor() {  
              
    }

    restrict = 'E'
    scope = {
        id: "="
    }

    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: ContactCreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Popup', 'SaveContact'),
            type: PopupType.CONTACT_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitContact(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean) => {
            return $q.all([localDictionary.getContactEditFields(), localDictionary.getContactDisplayFields(), localDictionary.getContactRequiredFields()]).then(([columnsEdit, columnsDisplay, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }

        $scope.$on(EventsNames.OPEN_CONTACT_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {            
            loadColumns(args && args[0]).then(() => {
                if (args && args[0]) {
                    $scope.options.title = i18n('Popup', 'ContactEdit')

                    api.contact.getContactToEdit(args[0]).then(result => {
                        $scope.editedItem = result.fields
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                    })
                    return
                } else {
                    $scope.editedItem = null
                    $scope.options.title = i18n('Popup', 'ContactAdd')
                }
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem)
            })
        })

        var submitContact = (fields: FieldNameValueModel[], id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.contact.createContact(formModel).then(contact => {
                if (!contact) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'ContactSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'ContactSaved'))
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, contact)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
    }]
    template = require("./contacts-create-edit.tpl.html")
    transclude = true
}