﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { ContactCreateEditDirective } from './create-edit/contacts-create-edit.directive'
import { ContactDetailsDirective } from './details/contacts-details.directive'
import { i18n } from '../common/i18n/common.i18n'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

import { ContactsController } from './contacts.controller'

var moduleName = 'simax.contacts'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('ContactsController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new ContactsController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('contactDetails', [() => new ContactDetailsDirective()])
module.directive('contactCreateEdit', [() => new ContactCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.CONTACT_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['CONTACTS'], 'contacts', 'Contacts', 'contacts', 10)
        $stateProvider.state('contacts', {
            url: '/contacts',
            controller: 'ContactsController',
            template: require('./contacts.tpl.html')
        })
    })
}])

export default moduleName