﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ContactModel, EntityModel, ContactFormValuesModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    locations: any[]
    groups: any[]
}

interface ContactDetailsScope extends DetailsScope {
    id: number
    editId: number
    modelId: number
    itemObjects: any[]
    popupTitle: string

    common: CommonData

    // accordion    
    locationsGridParams: GridData
    locationsBridge: GridBridge

    groupsGridParams: GridData
    groupsBridge: GridBridge

    showTabs: boolean

    confirmDelete()
}

export class ContactDetailsDirectiveController extends Details<ContactDetailsScope> {
    private contactId: number
    private permissions: any[]
    private currentLocations: EntityModel[] = []

    private locationsGridPromise: ng.IPromise<void>
    private groupsGridPromise: ng.IPromise<void>

    constructor($scope: ContactDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.ContactPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'ContactDetails'),
            openEventName: EventsNames.OPEN_CONTACT_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_CONTACT_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.CONTACT_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.contact
        $scope.editId = $scope.$id
        $scope.common = <CommonData>{}

        $scope.locationsGridParams = <GridData>{}
        $scope.groupsGridParams = <GridData>{}

        jQuery.when.apply(jQuery, [Permissions.CONTACT_EDIT, Permissions.CONTACT_GROUPS_EDIT, Permissions.CONTACT_LOCATIONS_EDIT].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteContact'))
        }

        this.loadLocationsGrid()
        this.loadGroupsGrid()

        // popup open
        $scope.$on(EventsNames.OPEN_CONTACT_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var contact: ContactModel = args[0]
            if (contact.id == this.contactId) {
                this.updateData(contact)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_CONTACT_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.contact.deleteContact($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.CONTACT_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'ContactDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'ContactDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.groupsGridParams.showGrid = false
        this.$scope.locationsGridParams.showGrid = false
        this.$scope.requestsStates.getContact = {}
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.contact.getContact(id, this.$scope.requestsStates.getContact).then(contactResult => {
            this.contactId = contactResult ? contactResult.id : null;
            this.$scope.showTabs = true
            this.$scope.popupTitle = contactResult.name

            this.updateData(contactResult)
        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: ContactModel) {
        //left column
        this.localDictionary.getContactDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })

        //center column            
        this.groupsGridPromise.then(() => {
            this.$scope.groupsGridParams.options.dataSource.store().clear()
            if (result)
                result.groups.forEach(element => {
                    this.$scope.groupsGridParams.options.dataSource.store().insert(element)
                })
            this.$scope.groupsGridParams.showGrid = true
            this.$scope.groupsGridParams.grid.refresh()
        })

        //groups bridge
        var groups = result && result.groups ? result.groups : []
        this.$scope.common.groups = groups
        this.buildGroupsBridges()

        //right column
        this.locationsGridPromise.then(() => {
            this.$scope.locationsGridParams.options.dataSource.store().clear()
            if (result)
                result.locations.forEach(element => {
                    var location = {}
                    element.fields.forEach(f => {
                        location[f.id] = f.value
                    })
                    this.$scope.locationsGridParams.options.dataSource.store().insert(location)
                })
            this.$scope.locationsGridParams.showGrid = true
            this.$scope.locationsGridParams.grid.refresh()
        })

        //locations bridge
        var locations = result && result.locations ? result.locations.map(location => CommonHelper.objectFromEntityModel(location)) : []
        this.$scope.common.locations = locations
        this.buildLocationsBridges()
    }

    getTabs() {
        var tabs = []

        tabs.push({ text: i18n('Common', 'CommonX'), icon: "home", name: 'common' })

        return tabs
    }

    getAccordions() {
        var [CONTACT_EDIT, CONTACT_GROUPS_EDIT, CONTACT_LOCATIONS_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "BasicParameters"),
            canEdit: CONTACT_EDIT,
            name: 'params',
            showStatus: false,
            editMethod: () => {
                if (CONTACT_EDIT)
                    this.$scope.$root.$broadcast(EventsNames.OPEN_CONTACT_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)                
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.ContactDetailsAccordions, 0, items))

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "Groups"),
            canEdit: CONTACT_GROUPS_EDIT,
            name: 'groups',
            showStatus: false,
            editMethod: () => {
                if (CONTACT_GROUPS_EDIT)
                    this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + 'group')
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.ContactDetailsAccordions, 1, items))

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "Locations"),
            canEdit: CONTACT_LOCATIONS_EDIT,
            name: 'locations',
            showStatus: false,
            editMethod: () => {
                if (CONTACT_LOCATIONS_EDIT)
                    this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + 'location')
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.ContactDetailsAccordions, 2, items))

        return accordions
    }

    loadLocationsGrid() {
        this.localDictionary.getContactLocationColumns().then(displayFields => {
            this.$scope.locationsGridParams.columnsCount = displayFields.length

            var defer = this.$q.defer<void>()
            var customLocationGridOptions =
                {
                    columnsDictionary: displayFields,
                    defaultColumnsDictionary: displayFields,
                    onInitialized: e => {
                        this.$scope.locationsGridParams.grid = e.component
                        defer.resolve()
                    }
                }
            this.$scope.locationsGridParams.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, this.$scope, this.logger, null, customLocationGridOptions)
            this.locationsGridPromise = defer.promise
        })
    }

    loadGroupsGrid() {
        this.localDictionary.getContactGroupsColumns().then(displayFields => {
            var defer = this.$q.defer<void>()
            var commonGroupsGridOptions = {
                columnsDictionary: displayFields,
                defaultColumnsDictionary: displayFields,
                onInitialized: e => {
                    this.$scope.groupsGridParams.grid = e.component
                    defer.resolve()
                }
            }
            this.$scope.groupsGridParams.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, this.$scope, this.logger, null, commonGroupsGridOptions)
            this.groupsGridPromise = defer.promise
        })
    }

    buildGroupsBridges() {
        this.$scope.groupsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'GroupsEdit') + " (" + this.$scope.common.groups.length + ")",
            items: this.$scope.common.groups,
            popupId: this.$scope.detailsPopupId + 'group',
            dictionaryMethodName: 'getContactGroupsColumns',
            dataMethodName: 'getContactGroups',
            dataScope: 'contact',
            onSubmit: (added, removed) => {
                var formModel = new ContactFormValuesModel()
                added = added.map(d => d[CommonHelper.idField])
                removed = removed.map(d => d[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.groups = this.$scope.common.groups.map(d => d[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }
    }

    buildLocationsBridges() {
        this.$scope.locationsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'ContactEdit') + " (" + this.$scope.common.locations.length + ")",
            items: this.$scope.common.locations,
            popupId: this.$scope.detailsPopupId + 'location',
            dictionaryMethodName: 'getContactLocationColumnsEdit',
            dataMethodName: 'getLocations',
            dataScope: 'location',
            onSubmit: (added, removed) => {
                var formModel = new ContactFormValuesModel()
                added = added.map(d => d[CommonHelper.idField])
                removed = removed.map(d => d[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.locations = this.$scope.common.locations.map(d => d[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }
    }

    saveFormModel(formModel: ContactFormValuesModel) {
        var defer = this.$q.defer()
        this.api.contact.createContact(formModel).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaveError'), 'error', 5000)
                defer.reject()
            } else {
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaved'))
                this.updateData(response)
                defer.resolve(true)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaveError'), 'error', 5000)
            defer.reject()
        })
        return defer.promise;
    }
}