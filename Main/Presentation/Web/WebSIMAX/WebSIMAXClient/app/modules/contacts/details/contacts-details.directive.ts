﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ContactDetailsDirectiveController } from './contacts-details.directive.controller'

export class ContactDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new ContactDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./contacts-details.tpl.html")
}