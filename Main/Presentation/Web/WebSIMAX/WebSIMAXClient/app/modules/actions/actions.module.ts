﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { ActionsController } from './actions.controller'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.actions'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('ActionsController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new ActionsController($scope, $compile, $timeout, api, logger, localDictionary)])


module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.ACTION_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['ACTIONS'], 'actions', 'Actions', 'actions', 13)
        $stateProvider.state('actions', {
            url: '/actions',
            controller: 'ActionsController',
            template: require('./actions.tpl.html')
        })
    })
}])

export default moduleName