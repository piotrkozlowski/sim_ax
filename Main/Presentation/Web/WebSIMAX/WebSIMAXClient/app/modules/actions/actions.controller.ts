﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonDataGridOptions } from '../common/common.data-grid-options'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { Permissions } from '../../helpers/permissions'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { i18n } from '../common/i18n/common.i18n'

require('./actions.scss')

interface ActionsScope extends ScopeBaseGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
    actionDetailsPopupId: number

    defaultColumns: ColumnModel[]
    columns: ColumnModel[]
}

export class ActionsController extends BaseGridController {
    constructor(public $scope: ActionsScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        $scope.actionDetailsPopupId = new Date().getTime()

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "actions",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.showDetails($scope.dataGrid.getSelectedRowsData()[0])
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_ACTION_CREATE_EDIT_POPUP + $scope.actionDetailsPopupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.actionColumnId])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['" + Permissions.ACTION_EDIT + "']",
                add: "['" + Permissions.ACTION_EDIT + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.ACTION_EDIT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...editPermission) => {
                var permissions = [false, editPermission, true, true]
                this.setSettingsListOptions(permissions, EventsNames.OPEN_ACTION_CREATE_EDIT_POPUP)
            })
        }, 0)

        $scope.$on(EventsNames.ACTION_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })
    }

    getActions(request) {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.actions.getActions(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            for (var i = result.values.length - 1; i >= 0; i--) {
                var val = result.values[i]
                if (val['act_10123_0']) {
                    for (var j = i - 1; j >= 0; j--) {
                        var valj = result.values[j]
                        if (valj.id == val['act_10123_0']) {
                            if (!valj.innerGridData) valj.innerGridData = []

                            valj.innerGridData.push(val)
                            break
                        }
                    }
                    result.values.splice(i, 1)
                }
            }
            this.localDictionary.getActionColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getActionColumns().then(columns => {
            this.localDictionary.getActionDefaultColumns().then(defaultColumns => {
                this.$scope.defaultColumns = defaultColumns
                this.$scope.columns = columns
                this.$scope.columnsCount = defaultColumns ? defaultColumns.length : 0
                var customOptions = {
                    name: "ActionsGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    masterDetail: {
                        enabled: true,
                        template: (container, info) => {
                            this.createTemplateForInnerDataGrid(container, info, this)
                        }
                    },
                    onCellPrepared: (e) => {
                        if (e.rowType == 'data' && e.column.command == 'expand' && !e.data.innerGridData) {
                            e.cellElement.removeClass("dx-datagrid-expand")
                            e.cellElement.empty()
                        }
                    },
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }

                // Data grid options
                var dataGridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger, (request) => {
                    return this.getActions(request)
                }, customOptions, true, this.localDictionary, null, this.$compile)
                this.$scope.dataGridOptions = dataGridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = dataGridOptions
                this.$scope.dataGrid.refresh()
            })
        })
    }

    createTemplateForInnerDataGrid(container: JQuery, info, self) {
        container.find('#dataGrid' + info.data.id).remove()
        $('<div id="dataGrid' + info.data.id + '">').appendTo(container)

        var columns = []
        var visibleColumns = []

        for (var i = 0; i < self.$scope.dataGrid.columnCount(); i++) {
            var newCol = self.$scope.dataGrid.columnOption(i)
            newCol.filterValue = null
            columns.push(newCol)
            if (newCol.visible)
                visibleColumns.push(newCol)
        }
        
        container.find('#dataGrid' + info.data.id).dxDataGrid(new CommonExtendedDataGridOptions(CommonHelper.smallGrid, self.$scope, self.logger, null, {
            onInitialized: (e) => {
                (<JQuery>e.element).parent().parent().find('.dx-datagrid-group-space').remove()

                var cell = $('.dx-master-detail-cell')

                cell.attr('colSpan', visibleColumns.length + 1)
            },
            dataSource: info.data.innerGridData,
            columns: columns,
            filterRow: {
                visible: false
            },
            masterDetail: {
                enabled: true,
                template: (container, info) => {
                    self.createTemplateForInnerDataGrid(container, info, self)
                }
            },
            onCellPrepared: (e) => {
                if (e.rowType == 'data' && e.column.command == 'expand' && !e.data.innerGridData) {
                    e.cellElement.removeClass("dx-datagrid-expand")
                    e.cellElement.empty()
                }
            }
        }, false))
    }

    showDetails(e) {
        var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
            return {
                id: item[CommonHelper.actionColumnId]
            }
        })
        this.$scope.$broadcast(EventsNames.OPEN_ACTION_DETAILS_POPUP + this.$scope.actionDetailsPopupId, e[CommonHelper.actionColumnId], itemIds)
    }
}

