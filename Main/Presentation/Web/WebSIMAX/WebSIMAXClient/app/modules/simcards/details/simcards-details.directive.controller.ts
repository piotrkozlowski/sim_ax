﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { EntityModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { GridSelectBridge } from '../../common/grid-select/grid-select.directive'
import { i18n } from '../../common/i18n/common.i18n'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    locations: any[]
}

interface SimcardDetailsScope extends DetailsScope {
    id: number
    editId: number
    modelId: number
    itemObjects: any[]
    popupTitle: string

    common: CommonData

    showTabs: boolean

    confirmDelete()
}

export class SimcardDetailsDirectiveController extends Details<SimcardDetailsScope> {
    private permissions: any[]
    private simcardId: number

    constructor($scope: SimcardDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.SimcardPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'SimcardDetails'),
            openEventName: EventsNames.OPEN_SIM_CARDS_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_SIM_CARDS_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.SIM_CARD_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.simcard
        $scope.editId = $scope.$id

        $scope.common = <CommonData>{}

        jQuery.when.apply(jQuery, [Permissions.SIM_CARD_LIST, Permissions.SIM_CARD_HISTORY, Permissions.SIM_CARD_EDIT].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteSimcard'))
        }

        // popup open
        $scope.$on(EventsNames.OPEN_SIM_CARDS_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var simcard: EntityModel = args[0]
            if (simcard.id == this.simcardId) {
                this.updateData(simcard)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_SIM_CARDS_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.simcard.deleteSimcard($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.SIM_CARDS_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'SimcardDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'SimcardDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getSimcard = {}
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.simcard.getSimcard(id, this.$scope.requestsStates.getSimcard).then(simcardResult => {
            this.simcardId = simcardResult ? simcardResult.id : null;
            this.$scope.popupTitle = simcardResult.name
            this.$scope.showTabs = true

            this.updateData(simcardResult)
        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: EntityModel) {
        this.localDictionary.getSimcardsDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })
    }

    getTabs() {
        var tabs = []
        tabs.push({ text: i18n('Common', 'CommonX'), icon: "home", name: 'common' })
        return tabs
    }

    getAccordions() {
        var [SIM_CARD_LIST, SIM_CARD_HISTORY, SIM_CARD_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "BasicParameters"),
            canEdit: SIM_CARD_EDIT,
            name: 'params',
            showStatus: false,
            editMethod: () => {
                if (SIM_CARD_EDIT)
                    this.$scope.$root.$broadcast(EventsNames.OPEN_SIM_CARDS_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.SimcardDetailsAccordions, 0, items))

        if (SIM_CARD_HISTORY) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "History"),
                canEdit: false,
                name: 'history',
                showStatus: false,
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.SimcardDetailsAccordions, 1, items))
        }

        return accordions
    }
}