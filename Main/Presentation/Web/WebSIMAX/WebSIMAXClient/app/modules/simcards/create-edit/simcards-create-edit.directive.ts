﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { PopupType } from './../../../helpers/popup-manager'

require('./simcards-create-edit.scss')

interface SimcardCreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class SimcardCreateEditDirective implements ng.IDirective {
    constructor() {  
              
    }

    restrict = 'E'
    scope = {
        id: "="
    }

    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: SimcardCreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Popup', 'SaveSimcard'),
            type: PopupType.SIM_CARD_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitSimcard(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean) => {
            return $q.all([localDictionary.getSimcardsEditFields(), localDictionary.getSimcardsDisplayFields(), localDictionary.getSimcardsRequiredFields()]).then(([columnsEdit, columnsDisplay, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }

        $scope.$on(EventsNames.OPEN_SIM_CARDS_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {            
            loadColumns(args && args[0]).then(() => {
                if (args && args[0]) {
                    $scope.options.title = i18n('Popup', 'SimcardEdit')

                    api.simcard.getSimcardToEdit(args[0]).then(result => {
                        $scope.editedItem = result.fields
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                    })
                    return
                } else {
                    $scope.editedItem = null
                    $scope.options.title = i18n('Popup', 'SimcardAdd')
                }
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem)
            })
        })

        var submitSimcard = (fields: FieldNameValueModel[], id: number) => {
            var formModel = new FormValuesModel()
            formModel.fields = fields
            formModel.id = id

            return api.simcard.createSimcard(formModel).then(simcard => {
                if (!simcard) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'SimcardSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'SimcardSaved'))
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, simcard)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
    }]
    template = require("./simcards-create-edit.tpl.html")
    transclude = true
}