﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { SimcardCreateEditDirective } from './create-edit/simcards-create-edit.directive'
import { SimcardDetailsDirective } from './details/simcards-details.directive'
import { i18n } from '../common/i18n/common.i18n'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

import { SimcardsController } from './simcards.controller'

var moduleName = 'simax.simcards'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('SimcardsController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new SimcardsController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('simcardsDetails', [() => new SimcardDetailsDirective()])
module.directive('simcardsCreateEdit', [() => new SimcardCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.SIM_CARD_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['SIM_CARD'], 'simcards', 'Simcards', 'simcards', 9)
        $stateProvider.state('simcards', {
            url: '/simcards',
            controller: 'SimcardsController',
            template: require('./simcards.tpl.html')
        })
    })
}])

export default moduleName