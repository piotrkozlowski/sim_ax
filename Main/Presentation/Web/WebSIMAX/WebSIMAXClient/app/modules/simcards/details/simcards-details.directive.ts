﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { SimcardDetailsDirectiveController } from './simcards-details.directive.controller'

export class SimcardDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new SimcardDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./simcards-details.tpl.html")
}