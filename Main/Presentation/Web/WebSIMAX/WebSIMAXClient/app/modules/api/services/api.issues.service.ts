﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, DeviceHierarchyModel, IssueModel, SaveFileModel, FileModel, LocationEquipmentModel, FormValuesModel, ResponseListModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IIssuesService {
    getIssues(req: GridRequestModel, state?: any): ng.IPromise<GridModel> // TODO
    createIssue(req): ng.IPromise<any>
    getIssue(id, state?: any): ng.IPromise<IssueModel>
    getIssueToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteIssue(id): ng.IPromise<ResponseDeleteModel>
    saveIssue(req): ng.IPromise<IssueModel>
    checkIfAttachmentExists(id, attachmentId): ng.IPromise<string>
    getAttachments(id, state?: any): ng.IPromise<ResponseListModel<FileModel>>
    getEquipment(id, state?: any): ng.IPromise<ResponseListModel<LocationEquipmentModel>>
    saveAttachment(attachment: FileModel): ng.IPromise<SaveFileModel>
    deleteAttachments(model: FormValuesModel): ng.IPromise<ResponseListModel<number>>
}

export class IssuesService implements IIssuesService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getIssues(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.issues.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getIssue(id, state: any = {}) {
        var get = this.apiResource.issues.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getIssueToEdit(id, state: any = {}) {
        var getIssueToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.issues['getIssueToEdit']
        state.request = getIssueToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => {
            CommonHelper.goToLoginPage(this.apiResource, rej.status)
            return null
        })
    }

    saveIssue(req) {
        return this.apiResource.issues.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createIssue(req) {
        return this.apiResource.issues.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => {
            CommonHelper.goToLoginPage(this.apiResource, rej.status)
        })
    }

    deleteIssue(id) {
        return this.apiResource.issues.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    checkIfAttachmentExists(id, attachmentId) {
        var checkIfExists: ng.resource.IResourceMethod<ng.resource.IResource<string>> = this.apiResource.issues['checkIfAttachmentExists']
        return checkIfExists({ id: id, attachmentId: attachmentId }).$promise.then(r => {
                var result = ""
                var rJSON = (<any>r).toJSON()
                for (var i in rJSON) {
                    result += rJSON[i]
                }
                return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAttachments(id, state: any = {}) {
        var getAttachments: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<FileModel>>> = this.apiResource.issues['getAttachments']
        state.request = getAttachments({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getEquipment(id, state: any = {}) {
        var getEquipment: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<LocationEquipmentModel>>> = this.apiResource.issues['getEquipment']
        state.request = getEquipment({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveAttachment(attachment: FileModel) {
        var saveAttachment: ng.resource.IResourceMethod<ng.resource.IResource<SaveFileModel>> = this.apiResource.issues['saveAttachment']
        return saveAttachment(attachment).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteAttachments(model: FormValuesModel) {
        var deleteAttachment: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<number>>> = this.apiResource.issues['deleteAttachment']
        return deleteAttachment(model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeIssuesService implements IIssuesService {
    constructor(private $q: ng.IQService) {

    }

    getIssues(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createIssue(req) {
        return this.$q.resolve({})
    }

    getIssue(id) {
        return this.$q.resolve({})
    }

    getIssueToEdit(id) {
        return this.$q.resolve({})
    }

    getAttachments(id) {
        return this.$q.resolve({})
    }

    getEquipment(id) {
        return this.$q.resolve({})
    }

    saveIssue(req) {
        return this.$q.resolve({})
    }

    deleteIssue(id) {
        return this.$q.resolve({})
    }

    checkIfAttachmentExists(id, attachmentId) {
        return this.$q.resolve({})
    }

    saveAttachment(attachment: FileModel) {
        return this.$q.resolve({})
    }

    deleteAttachments(model) {
        return this.$q.resolve({})
    }
}