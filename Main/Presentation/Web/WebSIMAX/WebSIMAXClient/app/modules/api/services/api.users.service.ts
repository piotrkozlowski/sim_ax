﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, LocationModel, OperatorModel, FieldNameValueModel, NotificationModel, SecurityActivityModel, SecurityChangeModel, ResponseDeleteModel, FormValuesModel, PasswordResetModel, PasswordResetResponseModel, ResponseListModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IUsersService {
    getUsers(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getUser(id: number, state?: any): ng.IPromise<OperatorModel>
    getUserToEdit(id: number, state?: any): ng.IPromise<EntityModel>
    saveUser(req): ng.IPromise<OperatorModel>
    deleteUser(id: number): ng.IPromise<ResponseDeleteModel>
    submitActivities(model: SecurityChangeModel): ng.IPromise<OperatorModel>
    submitRoles(model: SecurityChangeModel): ng.IPromise<OperatorModel>
    changeNotification(model: NotificationModel): ng.IPromise<OperatorModel>
    changeReferenceObject(model: SecurityActivityModel): ng.IPromise<OperatorModel>
    changeAllowedDistributors(model: FormValuesModel): ng.IPromise<ResponseListModel<number>>
    changePassword(model: PasswordResetModel): ng.IPromise<PasswordResetResponseModel>
}

export class UsersService implements IUsersService {
    constructor(private apiResource: ApiResourceFactory) { }

    getUsers(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.users.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getUser(id: number, state: any = {}) {
        var get = this.apiResource.users.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getUserToEdit(id: number, state: any = {}) {
        var getUserToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.users['getUserToEdit']
        state.request = getUserToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveUser(req) {
        return this.apiResource.users.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteUser(id: number) {
        return this.apiResource.users.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    submitActivities(model: SecurityChangeModel) {
        return this.apiResource.users['submitActivities'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    submitRoles(model: SecurityChangeModel) {
        return this.apiResource.users['submitRoles'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changeNotification(model: NotificationModel) {
        return this.apiResource.users['changeNotification'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changeReferenceObject(model: SecurityActivityModel) {
        return this.apiResource.users['changeReferenceObject'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changeAllowedDistributors(model: FormValuesModel) {
        return this.apiResource.users['changeAllowedDistributors'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        })
            .catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changePassword(model: PasswordResetModel): ng.IPromise<PasswordResetResponseModel> {
        return this.apiResource.users['changePassword'](model).$promise.then(r => {
            CommonHelper.checkResultError(r)
            return (<any>r).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeUsersService implements IUsersService {
    constructor(private $q: ng.IQService) { }

    getUsers(req: GridRequestModel) {
        return this.$q.resolve(new GridModel())
    }

    getUser(id: number) {
        return this.$q.resolve(new EntityModel())
    }

    getUserToEdit(id: number) {
        return this.$q.resolve({})
    }

    saveUser(req) {
        return this.$q.resolve({})
    }

    deleteUser(id: number) {
        return this.$q.resolve({})
    }

    submitActivities(model: SecurityChangeModel) {
        return this.$q.resolve({})
    }

    submitRoles(model: SecurityChangeModel) {
        return this.$q.resolve({})
    }

    changeNotification(model: NotificationModel) {
        return this.$q.resolve({})
    }

    changeReferenceObject(model: SecurityActivityModel) {
        return this.$q.resolve({})
    }

    changeAllowedDistributors(model: FormValuesModel) {
        return this.$q.resolve({})
    }

    changePassword(model: PasswordResetModel) {
        return this.$q.resolve({})
    }
}