﻿﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, DistributorModel, EntityModel, ResponseDeleteModel } from '../api.models'
import { LoginService } from './api.login.service'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IDistributorsService {
    getDistributors(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getAllDistributors(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    createDistributor(req): ng.IPromise<DistributorModel>
    getDistributor(id, state?: any): ng.IPromise<DistributorModel>
    getDistributorToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteDistributor(id): ng.IPromise<ResponseDeleteModel>
}

export class DistributorsService implements IDistributorsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getDistributors(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.distributors.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAllDistributors(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.distributors['getAllDistributors']
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getDistributor(id, state: any = {}) {
        var get = this.apiResource.distributors.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getDistributorToEdit(id, state: any = {}) {
        var getDistributorToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.distributors['getDistributorToEdit']
        state.request = getDistributorToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createDistributor(req) {
        return this.apiResource.distributors.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteDistributor(id) {
        return this.apiResource.distributors.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeDistributorsService implements IDistributorsService {
    constructor(private $q: ng.IQService) {

    }

    getDistributors(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getAllDistributors(req: GridRequestModel) {
        return this.$q.resolve(new GridModel())
    }

    createDistributor(req) {
        return this.$q.resolve({})
    }

    getDistributor(id) {
        return this.$q.resolve({})
    }

    getDistributorToEdit(id) {
        return this.$q.resolve({})
    }

    deleteDistributor(id) {
        return this.$q.resolve({})
    }
}