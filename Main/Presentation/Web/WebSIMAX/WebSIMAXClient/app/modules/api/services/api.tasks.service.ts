﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, DeviceHierarchyModel, TaskModel, FileModel, SaveFileModel, LocationEquipmentModel, FormValuesModel, TaskStatusModel, ResponseDeleteModel, TaskStatusChangeModel, ResponseListModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface ITasksService {
    getTasks(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    createTask(req): ng.IPromise<any>
    deleteTask(id): ng.IPromise<ResponseDeleteModel>
    getTask(id, state?: any): ng.IPromise<TaskModel>
    getTaskToEdit(id, state?: any): ng.IPromise<EntityModel>
    getAttachments(id, state?: any): ng.IPromise<ResponseListModel<FileModel>>
    getEquipment(id, state?: any): ng.IPromise<ResponseListModel<LocationEquipmentModel>>
    checkIfAttachmentExists(id, attachmentId): ng.IPromise<string>
    saveAttachment(attachment: FileModel): ng.IPromise<SaveFileModel>
    deleteAttachments(model: FormValuesModel): ng.IPromise<ResponseListModel<number>>
    getAvailableStatuses(ids: number[], state?: any): ng.IPromise<ResponseListModel<TaskStatusModel>>
    saveStatuses(model: TaskStatusChangeModel): ng.IPromise<ResponseListModel<TaskModel>>
}

export class TasksService implements ITasksService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getTasks(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.tasks.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getTask(id, state: any = {}) {
        var get = this.apiResource.tasks.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getTaskToEdit(id, state: any = {}) {
        var getTaskToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.tasks['getTaskToEdit']
        state.request = getTaskToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => {
            CommonHelper.goToLoginPage(this.apiResource, rej.status)
            return null
        })
    }

    createTask(req) {
        return this.apiResource.tasks.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => {
            CommonHelper.goToLoginPage(this.apiResource, rej.status)
        })
    }

    deleteTask(id) {
        return this.apiResource.tasks.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAttachments(id, state: any = {}) {
        var getAttachments: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<FileModel>>> = this.apiResource.tasks['getAttachments']
        state.request = getAttachments({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getEquipment(id, state: any = {}) {
        var getEquipment: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<LocationEquipmentModel>>> = this.apiResource.tasks['getEquipment']
        state.request = getEquipment({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    checkIfAttachmentExists(id, attachmentId) {
        var checkIfExists: ng.resource.IResourceMethod<ng.resource.IResource<string>> = this.apiResource.tasks['checkIfAttachmentExists']
        return checkIfExists({ id: id, attachmentId: attachmentId }).$promise.then(r => {
            var result = ""
            var rJSON = (<any>r).toJSON()
            for (var i in rJSON) {
                result += rJSON[i]
            }
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveAttachment(attachment: FileModel) {
        var saveAttachment: ng.resource.IResourceMethod<ng.resource.IResource<SaveFileModel>> = this.apiResource.tasks['saveAttachment']
        return saveAttachment(attachment).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteAttachments(model: FormValuesModel) {
        var deleteAttachment: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<number>>> = this.apiResource.tasks['deleteAttachment']
        return deleteAttachment(model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAvailableStatuses(ids: number[], state: any = {}) {
        var get = this.apiResource.tasks['getAvailableStatuses']
        state.request = get(ids)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveStatuses(model: TaskStatusChangeModel) {
        var save = this.apiResource.tasks['saveTaskStatuses']
        return save(model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeTasksService implements ITasksService {
    constructor(private $q: ng.IQService) {

    }

    getTasks(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createTask(req) {
        return this.$q.resolve({})
    }

    getTask(id) {
        return this.$q.resolve({})
    }

    deleteTask(id) {
        return this.$q.resolve({})
    }

    getTaskToEdit(id) {
        return this.$q.resolve({})
    }

    getAttachments(id) {
        return this.$q.resolve({})
    }

    getEquipment(id) {
        return this.$q.resolve({})
    }

    checkIfAttachmentExists(id, attachmentId) {
        return this.$q.resolve({})
    }

    saveAttachment(attachment: FileModel) {
        return this.$q.resolve({})
    }

    deleteAttachments(model) {
        return this.$q.resolve({})
    }

    getAvailableStatuses(ids: number[]) {
        return this.$q.resolve({})
    }

    saveStatuses(model: TaskStatusChangeModel) {
        return this.$q.resolve({})
    }
}