﻿import { ApiResourceFactory } from '../api.resource.factory'
import { MeasurementsRequestModel, EntityModel, MeasurementModel, SimpleValueModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IMeasurementsService {
    getMeasurementsForLocation(req, state?: any): ng.IPromise<MeasurementModel[]>
    getLatestMeasurementsForLocation(req, state?: any): ng.IPromise<MeasurementModel[]>
    getMeasurementsForDevice(req, state?: any): ng.IPromise<MeasurementModel[]>
    getLatestMeasurementsForDevice(req, state?: any): ng.IPromise<MeasurementModel[]>
    getMeasurementsForMeter(req, state?: any): ng.IPromise<MeasurementModel[]>
    getLatestMeasurementsForMeter(req, state?: any): ng.IPromise<MeasurementModel[]>
}

export class MeasurementsService implements IMeasurementsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    decodeMeasurments(simpleVM: SimpleValueModel<string>) {
        var base64 = CommonHelper.Base64.decode(simpleVM.value)
        var byteNumbers = new Array(base64.length);
        for (var i = 0; i < base64.length; i++) {
            byteNumbers[i] = base64.charCodeAt(i);
        }

        var msgpack = require("msgpack-lite")
        var data: any[] = msgpack.decode(byteNumbers)

        var result = data.map((d: any[]) => {
            var isDate = d[2]

            var m = new MeasurementModel()
            m.dataType = d[0]
            m.date = new Date(d[1])
            m.locationId = d[3]
            m.meterId = d[4]
            m.serialNumber = d[5]
            m.value = isDate ? new Date(d[6]) : d[6]

            return m
        })

        return result
    }

    getMeasurementsForLocation(req: MeasurementsRequestModel, state:any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getMeasurementsForLocation']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getLatestMeasurementsForLocation(req: MeasurementsRequestModel, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getLatestMeasurementsForLocation']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getMeasurementsForDevice(req: MeasurementsRequestModel, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getMeasurementsForDevice']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getLatestMeasurementsForDevice(req: MeasurementsRequestModel, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getLatestMeasurementsForDevice']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getMeasurementsForMeter(req: MeasurementsRequestModel, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getMeasurementsForMeter']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getLatestMeasurementsForMeter(req: MeasurementsRequestModel, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<any>> = this.apiResource.measurements['getLatestMeasurementsForMeter']
        state.request = get(req)
        return state.request.$promise.then(this.decodeMeasurments).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }
}


export class FakeMeasurementsService implements IMeasurementsService {
    constructor(private $q: ng.IQService) {

    }

    getMeasurementsForLocation(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }

    getLatestMeasurementsForLocation(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }

    getMeasurementsForDevice(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }

    getLatestMeasurementsForDevice(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }

    getMeasurementsForMeter(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }

    getLatestMeasurementsForMeter(req: MeasurementsRequestModel = null) {
        return this.$q.resolve([])
    }
}