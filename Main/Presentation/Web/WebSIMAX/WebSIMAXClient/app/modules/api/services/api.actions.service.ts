﻿import { ApiResourceFactory } from '../api.resource.factory'
import { MeasurementsRequestModel, EntityModel, GridModel, GridRequestModel, GridRequestReferenceModel, FileModel, ActionModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IActionsService {
    getActions(req: GridRequestReferenceModel, state?: any): ng.IPromise<GridModel>
    createAction(req): ng.IPromise<ActionModel>
    getAction(id, state?: any): ng.IPromise<ActionModel>
    getActionToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteAction(id): ng.IPromise<ResponseDeleteModel>
}

export class ActionsService implements IActionsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getActions(req: GridRequestReferenceModel, state: any = {}) {
        var get = this.apiResource.actions.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createAction(req) {
        var save = this.apiResource.actions.save
        return save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAction(id, state?: any) {
        var get = this.apiResource.actions.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getActionToEdit(id, state?: any) {
        var get = this.apiResource.actions['getActionToEdit']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteAction(id) {
        var del = this.apiResource.actions.delete
        return del({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeActionsService implements IActionsService {
    constructor(private $q: ng.IQService) {

    }

    getActions(req: GridRequestReferenceModel) {
        return this.$q.resolve({})
    }

    createAction(req) {
        return this.$q.resolve({})
    }

    getAction(id, state?: any) {
        return this.$q.resolve({})
    }

    getActionToEdit(id, state?: any) {
        return this.$q.resolve({})
    }

    deleteAction(id) {
        return this.$q.resolve({})
    }
}