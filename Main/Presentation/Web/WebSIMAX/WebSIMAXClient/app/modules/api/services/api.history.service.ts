﻿import { ApiResourceFactory } from '../api.resource.factory'
import { HistoryModel, ResponseListModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IHistoryService {
    getHistoryForLocation(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
    getHistoryForDevice(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
    getHistoryForMeter(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
    getHistoryForIssue(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
    getHistoryForTask(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
    getHistoryForAlarm(id: number, state?: any): ng.IPromise<ResponseListModel<HistoryModel>>
}

export class HistoryService implements IHistoryService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getHistoryForLocation(id: number, state: any = {}) {
        var get = this.apiResource.history['getLocationHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForDevice(id: number, state: any = {}) {
        var get = this.apiResource.history['getDeviceHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForMeter(id: number, state: any = {}) {
        var get = this.apiResource.history['getMeterHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForIssue(id: number, state: any = {}) {
        var get = this.apiResource.history['getIssueHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForTask(id: number, state: any = {}) {
        var get = this.apiResource.history['getTaskHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForAlarm(id: number, state: any = {}) {
        var get = this.apiResource.history['getAlarmHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getHistoryForSimcard(id: number, state: any = {}) {
        var get = this.apiResource.history['getSimcardHistory']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeHistoryService implements IHistoryService {
    constructor(private $q: ng.IQService) {

    }

    getHistoryForLocation(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForDevice(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForMeter(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForIssue(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForTask(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForAlarm(id: number) {
        return this.$q.resolve({})
    }

    getHistoryForSimcard(id: number) {
        return this.$q.resolve({})
    }
}