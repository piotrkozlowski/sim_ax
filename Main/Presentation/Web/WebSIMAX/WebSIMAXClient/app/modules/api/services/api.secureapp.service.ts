﻿import { ApiResourceFactory } from '../api.resource.factory'
import { OperatorSettingsModel  } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface ISecureAppService {
    getOperatorSettings(req , state?: any): ng.IPromise<OperatorSettingsModel>
    saveOperatorSettings(req, state?: any): ng.IPromise<OperatorSettingsModel>
}

export class SecureAppService implements ISecureAppService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getOperatorSettings(id, state: any = {}) {
        var getOperatorSettings: ng.resource.IResourceMethod<ng.resource.IResource<OperatorSettingsModel>> = this.apiResource.secureapp['getOperatorSettings']
        state.request = getOperatorSettings(id)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveOperatorSettings(req, state: any = {}) {
        var saveOperatorSettings: ng.resource.IResourceMethod<ng.resource.IResource<OperatorSettingsModel>> = this.apiResource.secureapp['saveOperatorSettings']
        state.request = saveOperatorSettings(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }    
}


export class FakeSecureAppService implements ISecureAppService {
    constructor(private $q: ng.IQService) {

    }

    getOperatorSettings(req = null) {
        return this.$q.resolve(new OperatorSettingsModel())
    }

    saveOperatorSettings(req = null) {
        return this.$q.resolve(new OperatorSettingsModel())
    }
}