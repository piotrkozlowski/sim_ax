﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, SimpleValueModel, ModelAlarm, ResponseListModel} from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IAlarmsService {
    getAlarms(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getAlarm(id, state?: any): ng.IPromise<ModelAlarm>
    getAlarmsSummary(id, state?: any): ng.IPromise<ResponseListModel<Object>>
    confirmAlarms(ids: number[], state?: any): ng.IPromise<GridModel>
}

export class AlarmsService implements IAlarmsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getAlarms(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.alarms.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAlarm(id, state: any = {}) {
        var get = this.apiResource.alarms.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getAlarmsSummary(id, state: any = {}) {
        var get = this.apiResource.alarms['getAlarmSummary']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    confirmAlarms(ids: number[], state: any = {}) {
        var method = this.apiResource.alarms['confirmAlarms']
        state.request = method(ids)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}  

export class FakeAlarmsService implements IAlarmsService {
    constructor(private $q: ng.IQService) {

    }

    getAlarms(req: GridRequestModel = null, state: any = {}) {
        return this.$q.resolve(new GridModel())
    }

    getAlarm(id, state: any = {}) {
        return this.$q.resolve({})
    }

    getAlarmsSummary(id, state: any = {}) {
        return this.$q.resolve({})
    }

    confirmAlarms(ids: number[], state: any = {}) {
        return this.$q.resolve(new GridModel())
    }
}