﻿import { ApiResourceFactory } from '../api.resource.factory'
import { DashboardModel, DashboardRequestModel, ResponseListModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IDashboardService {
    getDashboardConfig(): ng.IPromise<DashboardModel>
    getDashbordItems(request: DashboardRequestModel): ng.IPromise<ResponseListModel<Object>>
}

export class DashboardService implements IDashboardService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getDashboardConfig() {
        return this.apiResource.dashboard.get().$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getDashbordItems(request: DashboardRequestModel) {
        return this.apiResource.dashboard.query(request).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeDashboardService implements IDashboardService {
    constructor(private $q: ng.IQService) {

    }

    getDashboardConfig() {
        return null
    }

    getDashbordItems(request: DashboardRequestModel) {
        return null
    }
}