﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, RouteModel, EntityModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'
import { IImportService } from './api.app.service'

export interface IRoutesService extends IImportService {
    getRoutes(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getRoutePoints(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getRouteOperators(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    createRoute(req): ng.IPromise<RouteModel>
    getRoute(id, state?: any): ng.IPromise<RouteModel>
    getRouteToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteRoute(id): ng.IPromise<ResponseDeleteModel>
}

export class RoutesService implements IRoutesService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getRoutes(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.routes.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getRoutePoints(req: GridRequestModel, state: any = {}) {
        var getRoutePoints: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.routes['getRoutePoints']
        state.request = getRoutePoints(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
    getRouteOperators(req: GridRequestModel, state: any = {}) {
        var getRouteOperators: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.routes['getRouteOperators']
        state.request = getRouteOperators(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getRoute(id, state: any = {}) {
        var get = this.apiResource.routes.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getRouteToEdit(id, state: any = {}) {
        var getRouteToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.routes['getRouteToEdit']
        state.request = getRouteToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
    
    createRoute(req) {
        return this.apiResource.routes.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteRoute(id) {
        return this.apiResource.routes.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    initImport(model: GridModel) {
        return this.apiResource.routes['importInit'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    startImport(guid: string) {
        return this.apiResource.routes['importStart']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    stopImport(guid: string) {
        return this.apiResource.routes['importStop']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importProgress(guid: string) {
        return this.apiResource.routes['importProgress']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importSummary(guid: string) {
        return this.apiResource.routes['importSummary']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeRoutesService implements IRoutesService {
    constructor(private $q: ng.IQService) {

    }

    getRoutes(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getRoutePoints(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getRouteOperators(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createRoute(req) {
        return this.$q.resolve({})
    }

    getRoute(id) {
        return this.$q.resolve({})
    }

    getRouteToEdit(id) {
        return this.$q.resolve({})
    }

    deleteRoute(id) {
        return this.$q.resolve({})
    }

    initImport(model: GridModel) {
        return this.$q.resolve({})
    }

    startImport(guid: string) {
        return this.$q.resolve({})
    }

    stopImport(guid: string) {
        return this.$q.resolve({})
    }

    importProgress(guid: string) {
        return this.$q.resolve({})
    }

    importSummary(guid: string) {
        return this.$q.resolve({})
    }
}