﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface ISimcardsService {
    getSimcards(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    createSimcard(req): ng.IPromise<EntityModel>
    getSimcard(id, state?: any): ng.IPromise<EntityModel>
    getSimcardToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteSimcard(id): ng.IPromise<ResponseDeleteModel>
}

export class SimcardsService implements ISimcardsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getSimcards(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.simcards.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getSimcard(id, state: any = {}) {
        var get = this.apiResource.simcards.get        
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getSimcardToEdit(id, state: any = {}) {
        var getSimcardToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.simcards['getSimcardToEdit']
        state.request = getSimcardToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createSimcard(req) {
        return this.apiResource.simcards.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteSimcard(id) {
        return this.apiResource.simcards.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    initImport(model: GridModel) {
        return this.apiResource.simcards['importInit'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    startImport(guid: string) {
        return this.apiResource.simcards['importStart']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    stopImport(guid: string) {
        return this.apiResource.simcards['importStop']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importProgress(guid: string) {
        return this.apiResource.simcards['importProgress']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importSummary(guid: string) {
        return this.apiResource.simcards['importSummary']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeSimcardsService implements ISimcardsService {
    constructor(private $q: ng.IQService) {

    }

    getSimcards(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createSimcard(req) {
        return this.$q.resolve({})
    }

    getSimcard(id) {
        return this.$q.resolve({})
    }

    getSimcardToEdit(id) {
        return this.$q.resolve({})
    }

    deleteSimcard(id) {
        return this.$q.resolve({})
    }
}