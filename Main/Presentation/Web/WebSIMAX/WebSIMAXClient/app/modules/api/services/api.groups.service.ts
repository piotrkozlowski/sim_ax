﻿﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GroupModel, ResponseListModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IGroupsService {
    getLocationGroups(state?: any): ng.IPromise<ResponseListModel<GroupModel>>
}

export class GroupsService implements IGroupsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getLocationGroups(state: any = {}): ng.IPromise<ResponseListModel<GroupModel>> {
        var getLocationGroups: ng.resource.IResourceMethod<ng.resource.IResource<ResponseListModel<GroupModel>>> = this.apiResource.groups['getLocationGroups']
        state.request = getLocationGroups()
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeGroupsService implements IGroupsService {
    constructor(private $q: ng.IQService) {

    }

    getLocationGroups(): ng.IPromise<ResponseListModel<GroupModel>> {
        return this.$q.resolve({})
    }
}