﻿import { ApiResourceFactory } from '../api.resource.factory'
import { MeasurementsRequestModel, EntityModel, GridModel, GridRequestModel, GridRequestReferenceModel, ImportProgressModel, FileModel, FileColumnModel, ContextListModel, SimpleValueModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IImportService {
    initImport(model: GridModel): ng.IPromise<any>
    startImport(guid: string): ng.IPromise<any>
    stopImport(guid: string): ng.IPromise<any>
    importProgress(guid: string): ng.IPromise<ImportProgressModel>
    importSummary(guid: string): ng.IPromise<any>
}

export interface IAppService {
    getActivities(state?: any): ng.IPromise<GridModel>
    getRoles(state?: any): ng.IPromise<GridModel>
    getReferenceObjectList(req: GridRequestReferenceModel, state?: any): ng.IPromise<GridModel>
    importExcelFile(model: FileColumnModel): ng.IPromise<GridModel>
    getContextList(contextListModel: ContextListModel): ng.IPromise<any[]>
    saveRequestModel(request: GridRequestModel): ng.IPromise<string>
}

export class AppService implements IAppService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    saveRequestModel(request: GridRequestModel): ng.IPromise<string> {
        var saveRequestModel: ng.resource.IResourceMethod<ng.resource.IResource<SimpleValueModel<string>>> = this.apiResource.app['saveReqModel']
        return saveRequestModel(request).$promise.then((result: SimpleValueModel<string>) => {
            return result.value
        })
    }

    getActivities(state: any = {}) {
        var get = this.apiResource.app['getActivities']
        state.request = get()
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getRoles(state: any = {}) {
        var get = this.apiResource.app['getRoles']
        state.request = get()
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getReferenceObjectList(req: GridRequestReferenceModel, state: any = {}) {
        var get = this.apiResource.app['getReferenceObjectList']
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importExcelFile(model: FileColumnModel) {
        return this.apiResource.app['importExcelFile'](model).$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getContextList(contextListModel: ContextListModel) {
        var getContext: ng.resource.IResourceMethod<ng.resource.IResource<any[]>> = this.apiResource.app['getContextList']
        return getContext(contextListModel).$promise.then(result => {
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeAppService implements IAppService {
    constructor(private $q: ng.IQService) {

    }

    getActivities() {
        return this.$q.resolve({})
    }

    getRoles() {
        return this.$q.resolve({})
    }

    getReferenceObjectList(req: GridRequestReferenceModel) {
        return this.$q.resolve({})
    }

    importExcelFile(model: FileModel) {
        return this.$q.resolve({})
    }

    getContextList(contextListModel: ContextListModel) {
        return this.$q.resolve({})
    }
    saveRequestModel(request: GridRequestModel) {
        return this.$q.resolve({})
    }
}