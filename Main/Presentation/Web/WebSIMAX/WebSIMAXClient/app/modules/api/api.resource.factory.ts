﻿import { UserModel, GroupModel, DashboardModel, EntityModel, HistoryModel } from './api.models' 

var API_URL = window['config'].API_URL

interface IApiResource<T> extends ng.resource.IResourceClass<ng.resource.IResource<T>> {
}

export class ApiResourceFactory {

    public login: IApiResource<UserModel>
    public locations: IApiResource<any>
    public devices: IApiResource<any>
    public dictionaries: IApiResource<any>
    public distributors: IApiResource<any>
    public groups: IApiResource<GroupModel>
    public dashboard: IApiResource<DashboardModel>
    public measurements: IApiResource<any>
    public meters: IApiResource<any>
    public users: IApiResource<EntityModel>
    public history: IApiResource<HistoryModel>
    public issues: IApiResource<any>
    public tasks: IApiResource<any>
    public app: IApiResource<any>
    public routes: IApiResource<any>
    public password: IApiResource<any>
    public alarms: IApiResource<any>
    public reports: IApiResource<any>
    public simcards: IApiResource<any>
    public contacts: IApiResource<any>
    public actions: IApiResource<any>
    public deviceconnections: IApiResource<any>
    public secureapp: IApiResource<any>

    constructor($resource: ng.resource.IResourceService) {
        this.login = $resource(API_URL + 'api/login')
        this.locations = $resource(API_URL + 'api/locations/:id/:method/:guid/:type', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getLocationToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'getSchema': { method: 'GET', params: { method: 'schema' }, cancellable: true },
            'getSchemas': { method: 'GET', params: { method: 'schemas' }, isArray: false, cancellable: true },
            'get': { method: 'GET', cancellable: true },
            'importInit': { method: 'POST', params: { method: 'import', type: 'init' } },
            'importStart': { method: 'GET', params: { method: 'import', type: 'start' } },
            'importStop': { method: 'GET', params: { method: 'import', type: 'stop' } },
            'importProgress': { method: 'GET', params: { method: 'import', type: 'progress' } },
            'importSummary': { method: 'GET', params: { method: 'import', type: 'summary' } }
        })
        this.devices = $resource(API_URL + 'api/devices/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getDeviceToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'getParentDevices': { method: 'GET', isArray: false, params: { method: 'parent' }, cancellable: true },
            'getChildDevices': { method: 'GET', isArray: false, params: { method: 'child' }, cancellable: true },
            'saveChildDevices': { method: 'PUT', isArray: false, params: { method: 'child' } },
            'delete': { method: 'DELETE', isArray: false },
            'changeCurrentMeter': { method: 'POST', isArray: false, params: { method: 'meter' } },
            'get': { method: 'GET', cancellable: true },
            'getSchemas': { method: 'GET', params: { method: 'schemas' }, isArray: false, cancellable: true }
        })
        this.deviceconnections = $resource(API_URL + 'api/deviceconnections/:id/:method', {}, {
            'query': {
                method: 'POST', isArray: false, timeout: 72000000, cancellable: true
            }
        })
        this.routes = $resource(API_URL + 'api/routes/:id/:method/:guid/:type', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getRoutePoints': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true, params: { method: 'locations' } },
            'getRouteOperators': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true, params: { method: 'operators' } },
            'getRouteToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'get': { method: 'GET', cancellable: true },
            'importInit': { method: 'POST', params: { method: 'import', type: 'init' } },
            'importStart': { method: 'GET', params: { method: 'import', type: 'start' } },
            'importStop': { method: 'GET', params: { method: 'import', type: 'stop' } },
            'importProgress': { method: 'GET', params: { method: 'import', type: 'progress' } },
            'importSummary': { method: 'GET', params: { method: 'import', type: 'summary' } }
        })
        this.dictionaries = $resource(API_URL + 'api/dictionary/:method', {}, {
            'get': { method: 'GET', params: { method: '' }, isArray: false, cancellable: true },
            'getVersion': { method: 'GET', params: { method: 'version' }, cancellable: true }
        })
        this.distributors = $resource(API_URL + 'api/distributors/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getDistributorToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false }, 
            'delete': { method: 'DELETE', isArray: false },
            'getAllDistributors': { method: 'POST', isArray: false, params: { method: 'All' }, cancellable: true },
            'get': { method: 'GET', cancellable: true }
        })
        this.groups = $resource(API_URL + 'api/groups/:method', {}, {
            'getLocationGroups': { method: 'GET', params: { method: 'locations' }, isArray: false, cancellable: true },
            'get': { method: 'GET', cancellable: true }
        })
        this.dashboard = $resource(API_URL + 'api/dashboard', {}, {
            'query': { method: 'POST', isArray: false },
            'get': { method: 'GET', cancellable: true }
        })
        this.measurements = $resource(API_URL + 'api/measurements/:method', {}, {
            'getMeasurementsForLocation': { method: 'POST', params: { method: 'measurements-for-location' }, isArray: false, cancellable: true },
            'getLatestMeasurementsForLocation': { method: 'POST', params: { method: 'latest-measurements-for-location' }, isArray: false, cancellable: true },
            'getMeasurementsForDevice': { method: 'POST', params: { method: 'measurements-for-device' }, isArray: false, cancellable: true },
            'getLatestMeasurementsForDevice': { method: 'POST', params: { method: 'latest-measurements-for-device' }, isArray: false, cancellable: true },
            'getMeasurementsForMeter': { method: 'POST', params: { method: 'measurements-for-meter' }, isArray: false, cancellable: true },
            'getLatestMeasurementsForMeter': { method: 'POST', params: { method: 'latest-measurements-for-meter' }, isArray: false, cancellable: true }
        })
        this.meters = $resource(API_URL + 'api/meters/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getMeterToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'changeCurrentDevice': { method: 'POST', isArray: false, params: { method: 'device' } },
            'get': { method: 'GET', cancellable: true },
            'getSchemas': { method: 'GET', params: { method: 'schemas' }, isArray: false, cancellable: true }
        })
        this.users = $resource(API_URL + 'api/users/:id/:method/:action', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getUserToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'submitActivities': { method: 'PUT', isArray: false, params: { method: 'activity', action: 'change' } },
            'submitRoles': { method: 'PUT', isArray: false, params: { method: 'role', action: 'change' } },
            'changeNotification': { method: 'POST', isArray: false, params: { method: 'notification', action: 'change' } },
            'changeReferenceObject': { method: 'POST', isArray: false, params: { method: 'activity', action: 'reference' } },
            'changeAllowedDistributors': { method: 'POST', isArray: false, params: { method: 'distributors', action: 'change' } },
            'changePassword': { method: 'POST', isArray: false, params: { method: 'password', action: 'change' } },
            'get': { method: 'GET', cancellable: true }
        })

        this.issues = $resource(API_URL + 'api/issues/:id/:method/:attachmentId', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getIssueToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'getAttachments': { method: 'GET', isArray: false, params: { method: 'Attachments' }, cancellable: true },
            'getTasks': { method: 'GET', isArray: false, params: { method: 'Tasks' }, cancellable: true },
            'getEquipment': { method: 'GET', isArray: false, params: { method: 'Equipment' }, cancellable: true }, 
            'delete': { method: 'DELETE', isArray: false },
            'saveAttachment': { method: 'POST', isArray: false, params: { method: 'Attachments' } },
            'deleteAttachment': { method: 'PUT', isArray: false, params: { method: 'Attachments' } },
            'checkIfAttachmentExists': { method: 'GET', isArray: false, params: { method: 'CheckIfAttachmentExists' } },
            'get': { method: 'GET', cancellable: true }
        })
        this.tasks = $resource(API_URL + 'api/tasks/:id/:method/:attachmentId', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getTaskToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'getAttachments': { method: 'GET', isArray: false, params: { method: 'Attachments' }, cancellable: true },
            'getEquipment': { method: 'GET', isArray: false, params: { method: 'Equipment' }, cancellable: true },
            'saveAttachment': { method: 'POST', isArray: false, params: { method: 'Attachments' } },
            'deleteAttachment': { method: 'PUT', isArray: false, params: { method: 'Attachments' } },
            'checkIfAttachmentExists': { method: 'GET', isArray: false, params: { method: 'CheckIfAttachmentExists' } },
            'get': { method: 'GET', cancellable: true },
            'getAvailableStatuses': { method: 'POST', isArray: false, params: { method: 'AvailableStatuses' } },
            'saveTaskStatuses': { method: 'PUT', isArray: false, params: { method: 'SaveTaskStatuses' } }
        })

        this.alarms = $resource(API_URL + 'api/alarms/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getAlarmToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'getAlarmSummary': { method: 'GET', isArray: false, params: { method: 'summary' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'get': { method: 'GET', cancellable: true },
            'confirmAlarms': { method: 'POST', isArray: false, params: { method: 'confirmAlarms' }, timeout: 72000000, cancellable: true },
        })

        this.history = $resource(API_URL + 'api/history/:method/:id', {}, {
            'getLocationHistory': { method: 'GET', isArray: false, params: { method: 'location' }, cancellable: true },
            'getDeviceHistory': { method: 'GET', isArray: false, params: { method: 'Device' }, cancellable: true },
            'getMeterHistory': { method: 'GET', isArray: false, params: { method: 'Meter' }, cancellable: true },
            'getIssueHistory': { method: 'GET', isArray: false, params: { method: 'Issue' }, cancellable: true },
            'getTaskHistory': { method: 'GET', isArray: false, params: { method: 'Task' }, cancellable: true },
            'getSimcardHistory': { method: 'GET', isArray: false, params: { method: 'Simcard', cancellable: true  } }
        })

        this.app = $resource(API_URL + 'api/app/:method', {}, {
            'getRoles': { method: 'POST', isArray: false, params: { method: 'roles' }, cancellable: true },
            'getActivities': { method: 'POST', isArray: false, params: { method: 'activities' }, cancellable: true },
            'getReferenceObjectList': { method: 'POST', isArray: false, params: { method: 'reference' }, cancellable: true },
            'importExcelFile': { method: 'POST', isArray: false, params: { method: 'excel' } },
            'get': { method: 'GET', cancellable: true },
            'getContextList': { method: 'POST', isArray: false, params: { method: 'contextlist' }, timeout: 72000000, cancellable: true },
            'saveReqModel': { method: 'POST', isArray: false, params: { method: 'requestmodel' }, timeout: 72000000, cancellable: true }
        })

        this.password = $resource(API_URL + 'api/password', {}, {
            'get': { method: 'POST', isArray: false },
            'save': { method: 'PUT', isArray: false }
        })

        this.simcards = $resource(API_URL + 'api/simcards/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getSimcardToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'get': { method: 'GET', cancellable: true },
            'importInit': { method: 'POST', params: { method: 'import', type: 'init' } },
            'importStart': { method: 'GET', params: { method: 'import', type: 'start' } },
            'importStop': { method: 'GET', params: { method: 'import', type: 'stop' } },
            'importProgress': { method: 'GET', params: { method: 'import', type: 'progress' } },
            'importSummary': { method: 'GET', params: { method: 'import', type: 'summary' } }
        })

        this.reports = $resource(API_URL + 'api/reports/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'get': { method: 'GET', cancellable: true },
            'getReportToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'generateReport': { method: 'GET', params: { method: 'generate' }, cancellable: true },
        })

        this.contacts = $resource(API_URL + 'api/contacts/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getContactGroups': { method: 'POST', params: { method: 'groups' }, cancellable: true },
            'getContactToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'get': { method: 'GET', cancellable: true }
        })

        this.actions = $resource(API_URL + 'api/actions/:id/:method', {}, {
            'query': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true },
            'getActionToEdit': { method: 'GET', params: { method: 'edit' }, cancellable: true },
            'save': { method: 'PUT', isArray: false },
            'delete': { method: 'DELETE', isArray: false },
            'get': { method: 'GET', cancellable: true }
        })

        this.secureapp = $resource(API_URL + 'api/secureapp/:id/:method', {}, {            
            'getOperatorSettings': { method: 'GET', isArray: false, timeout: 72000000, cancellable: true, params: { method: 'get' } },
            'saveOperatorSettings': { method: 'POST', isArray: false, timeout: 72000000, cancellable: true, params: { method: 'save' } },            
        })
    }
}