﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, LocationModel, MeterModel, ObjectReferenceModel, SchemaModel, ResponseListModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IMetersService {
    getMeters(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getMeter(id: number, state?: any): ng.IPromise<MeterModel>
    getMeterToEdit(id: number, state?: any): ng.IPromise<EntityModel>
    // getAlarmGroups(id: number, state?: any): ng.IPromise<EntityModel>
    // getAlarmDefs(id:number, state?: any): ng.IPromise<EntityModel>
    saveMeter(req): ng.IPromise<MeterModel>
    deleteMeter(id): ng.IPromise<ResponseDeleteModel>
    changeCurrentDevice(model: ObjectReferenceModel): ng.IPromise<MeterModel>
    getSchemas(id, state?: any): ng.IPromise<ResponseListModel<SchemaModel>>
}

export class MetersService implements IMetersService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getMeters(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.meters.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getMeter(id: number, state: any = {}) {
        var get = this.apiResource.meters.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getMeterToEdit(id: number, state: any = {}) {
        var getMeterToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.meters['getMeterToEdit']
        state.request = getMeterToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    // getAlarmGroups(id: number, state: any = {}) {
    //     var getAlarmGroups: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.meters['getAlarmGroups']
    //     state.request = getAlarmGroups({id: id})
    //     return state.request.$promise.then(result => {
    //         CommonHelper.checkResultError(result)
    //         return(<any>result).toJSON()
    //     }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    // }

    // getAlarmDefs(id: number, state: any = {}) {
    //     var getAlarmDefs: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.meters['getAlarmDefs']
    //     state.request = getAlarmDefs({id: id})
    //     return state.request.$promise.then(result => {
    //         CommonHelper.checkResultError(result)
    //         return (<any>result).toJSON()
    //     }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    // }

    saveMeter(req) {
        return this.apiResource.meters.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteMeter(id) {
        return this.apiResource.meters.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changeCurrentDevice(model: ObjectReferenceModel) {
        return this.apiResource.meters['changeCurrentDevice'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getSchemas(id, state: any = {}) {
        var get = this.apiResource.meters['getSchemas']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeMetersService implements IMetersService {
    constructor(private $q: ng.IQService) {

    }

    getMeters(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getMeter(id: number) {
        return this.$q.resolve(new MeterModel())
    }

    getMeterToEdit(id: number) {
        return this.$q.resolve(new EntityModel())
    }

    saveMeter(req) {
        return this.$q.resolve({})
    }

    deleteMeter(id) {
        return this.$q.resolve({})
    }

    changeCurrentDevice(model: ObjectReferenceModel) {
        return this.$q.resolve(new MeterModel())
    }

    getSchemas(id) {
        return this.$q.resolve({})
    }
}