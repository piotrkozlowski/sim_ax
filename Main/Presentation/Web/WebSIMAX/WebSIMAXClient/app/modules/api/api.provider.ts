﻿import { ApiResourceFactory } from './api.resource.factory'
import { ILocationsService, LocationsService, FakeLocationsService} from './services/api.locations.service'
import { IDevicesService, DevicesService, FakeDevicesService} from './services/api.devices.service'
import { ILoginService, LoginService, FakeLoginService } from './services/api.login.service'
import { IDictionariesService, DictionariesService, FakeDictionariesService } from './services/api.dictionary.service'
import { IGroupsService, GroupsService, FakeGroupsService } from './services/api.groups.service'
import { IDashboardService, DashboardService, FakeDashboardService } from './services/api.dashboard.service'
import { IMeasurementsService, MeasurementsService, FakeMeasurementsService } from './services/api.measurements.service'
import { IMetersService, MetersService, FakeMetersService } from './services/api.meters.service'
import { IUsersService, UsersService, FakeUsersService } from './services/api.users.service'
import { IHistoryService, HistoryService, FakeHistoryService } from './services/api.history.service'
import { IIssuesService, IssuesService, FakeIssuesService} from './services/api.issues.service'
import { ITasksService, TasksService, FakeTasksService} from './services/api.tasks.service'
import { IAlarmsService, AlarmsService, FakeAlarmsService} from './services/api.alarms.service'
import { IDistributorsService, DistributorsService, FakeDistributorsService} from './services/api.distributors.service'
import { IAppService, AppService, FakeAppService} from './services/api.app.service'
import { IRoutesService, RoutesService, FakeRoutesService} from './services/api.routes.service'
import { IPasswordService, PasswordService, FakePasswordService } from './services/api.password.service'
import { IReportsService, ReportsService, FakeReportsService } from './services/api.reports.service'
import { ISimcardsService, SimcardsService, FakeSimcardsService} from './services/api.simcards.service'
import { IContactsService, ContactsService, FakeContactsService} from './services/api.contacts.service'
import { IActionsService, ActionsService, FakeActionsService } from './services/api.actions.service'
import { ISecureAppService, SecureAppService, FakeSecureAppService } from './services/api.secureapp.service'
import { IDeviceConnectionsService, DeviceConnectionsService, FakeDeviceConnectionsService } from './services/api.deviceconnections.service'

export class Api {
    constructor(public location: ILocationsService,
        public login: ILoginService,
        public dictionary: IDictionariesService,
        public device: IDevicesService,
        public group: IGroupsService,
        public dashboard: IDashboardService,
        public measurement: IMeasurementsService,
        public meter: IMetersService,
        public history: IHistoryService,
        public issue: IIssuesService,
        public task: ITasksService,
        public users: IUsersService,
        public distributor: IDistributorsService,
        public app: IAppService,
        public password: IPasswordService,
        public route: IRoutesService,
        public alarm: IAlarmsService,
        public report: IReportsService,
        public contact: IContactsService,
        public simcard: ISimcardsService,
        public actions: IActionsService,
        public deviceconnections: IDeviceConnectionsService,
        public secureapp: ISecureAppService) {
    }
}

export class ApiProvider {
    private isFakeServices = false

    $get = ['$q', 'apiResource', function ($q: ng.IQService, apiResource: ApiResourceFactory) {
        if (this.isFakeServices)
            return new Api(
                new FakeLocationsService($q),
                new FakeLoginService($q),
                new FakeDictionariesService($q),
                new FakeDevicesService($q),
                new FakeGroupsService($q),
                new FakeDashboardService($q),
                new FakeMeasurementsService($q),
                new FakeMetersService($q),
                new FakeHistoryService($q),
                new FakeIssuesService($q),
                new FakeTasksService($q),
                new FakeUsersService($q),
                new FakeDistributorsService($q),
                new FakeAppService($q),
                new FakePasswordService($q),
                new FakeRoutesService($q),
                new FakeAlarmsService($q),
                new FakeReportsService($q),
                new FakeContactsService($q),
                new FakeSimcardsService($q),
                new FakeActionsService($q),
                new FakeDeviceConnectionsService($q),
                new FakeSecureAppService($q))
        else
            return new Api(
                new LocationsService(apiResource),
                new LoginService(apiResource),
                new DictionariesService(apiResource),
                new DevicesService(apiResource),
                new GroupsService(apiResource),
                new DashboardService(apiResource),
                new MeasurementsService(apiResource),
                new MetersService(apiResource),
                new HistoryService(apiResource),
                new IssuesService(apiResource),
                new TasksService(apiResource),
                new UsersService(apiResource),
                new DistributorsService(apiResource),
                new AppService(apiResource),
                new PasswordService(apiResource),
                new RoutesService(apiResource),
                new AlarmsService(apiResource),
                new ReportsService(apiResource),
                new ContactsService(apiResource),
                new SimcardsService(apiResource),
                new ActionsService(apiResource),
                new DeviceConnectionsService(apiResource),
                new SecureAppService(apiResource))
    }]

    setFakeServices(isFake) {
        this.isFakeServices = isFake
    }
}