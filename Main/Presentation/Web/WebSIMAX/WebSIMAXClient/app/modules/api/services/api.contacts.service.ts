﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, ContactModel, EntityModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IContactsService {
    getContacts(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getContactGroups(req: GridRequestModel): ng.IPromise<GridModel>
    createContact(req): ng.IPromise<ContactModel>
    getContact(id, state?: any): ng.IPromise<ContactModel>
    getContactToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteContact(id): ng.IPromise<ResponseDeleteModel>
}

export class ContactsService implements IContactsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getContacts(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.contacts.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getContactGroups(req: GridRequestModel) {
        var getContactGroups: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.contacts['getContactGroups']
        return getContactGroups({ request: req }).$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
    
    getContact(id, state: any = {}) {
        var get = this.apiResource.contacts.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getContactToEdit(id, state: any = {}) {
        var get: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.contacts['getContactToEdit']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createContact(req) {
        return this.apiResource.contacts.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteContact(id) {
        return this.apiResource.contacts.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeContactsService implements IContactsService {
    constructor(private $q: ng.IQService) {

    }

    getContacts(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getContactGroups(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createContact(req) {
        return this.$q.resolve({})
    }

    getContact(id) {
        return this.$q.resolve({})
    }

    getContactToEdit(id) {
        return this.$q.resolve({})
    }

    deleteContact(id) {
        return this.$q.resolve({})
    }
}