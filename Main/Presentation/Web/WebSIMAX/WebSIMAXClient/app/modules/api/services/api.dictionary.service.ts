﻿﻿import { ApiResourceFactory } from '../api.resource.factory'
import { SimpleValueModel, DictionaryModel, ColumnModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IDictionariesService {
    getVersion(): ng.IPromise<SimpleValueModel<number>>
    getDictionaries(): ng.IPromise<string>
}

export class DictionariesService implements IDictionariesService {
    constructor(private apiResource: ApiResourceFactory) {
        
    }

    public getVersion(): ng.IPromise<SimpleValueModel<number>> {
        var getVersion: ng.resource.IResourceMethod<ng.resource.IResource<SimpleValueModel<number>>> = this.apiResource.dictionaries['getVersion']
        return getVersion().$promise.catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    public getDictionaries(): ng.IPromise<string> {
        return this.apiResource.dictionaries.get().$promise.then((result: SimpleValueModel<string>) => {
            return result.value
        }).catch(rej => {
            return CommonHelper.handleRejection(this.apiResource, rej.status)
        })
    }
}

export class FakeDictionariesService implements IDictionariesService {
    constructor(private $q: ng.IQService) {

    }

    public getVersion(): ng.IPromise<SimpleValueModel<number>> {
        var svm = new SimpleValueModel<number>();
        svm.value = 0;
        return this.$q.resolve(svm);
    }

    public getDictionaries(): ng.IPromise<string> {
        return this.$q.resolve("");
    }
}