
export class ColumnModel {
	alignment: string;
	caption: string;
	cellTemplate: string;
	chartType: string;
	editCellTemplate: any;
	format: FormatModel;
	id: number;
	idName: string;
	isEditable: boolean;
	name: string;
	position: number;
	referenceTypeId: number;
	sortDesc: boolean;
	type: number;
	typeName: string;
	unitId: number;
	unitName: string;
	where: string[];
	width: number;
}
export class ContextListModel {
	destinationType: number;
	referenceValues: any[];
	sourceType: number;
}
export class DashboardColumnModel {
	borderVisible: boolean;
	groupId: number;
	items: DashboardItemModel[];
	refreshPeriod: number;
	size: number;
	title: string;
}
export class DashboardItemModel {
	data: any;
	id: number;
	size: number;
	title: string;
	type: number;
}
export class DashboardRequestModel {
	groupId: number;
	ids: number[];
}
export class DashboardRowModel {
	columns: DashboardColumnModel[];
}
export class DataTypeGroupModel {
	idDataTypeGroup: number;
	idParentGroup: number;
	idReferenceType: number;
	name: string;
	referenceValue: any;
}
export class DataTypeModel {
	caption: string;
	format: FormatModel;
	isEditable: boolean;
	referenceTypeId: number;
	type: number;
	typeName: string;
	unitId: number;
	unitName: string;
}
export class DeviceHierarchyModel {
	active: boolean;
	device: string;
	deviceId: number;
	hasErrorCode: number;
	id: number;
	protocolIn: string;
	protocolOut: string;
	referenceId: number;
	slotNbr: number;
	slotType: string;
}
export class DevicesEditModel {
	devices: DeviceHierarchyModel[];
	id: number;
}
export class DictionaryModel {
	dataTypeGroups: DataTypeGroupModel[];
	dataTypeInGroups: any;
	dataTypes: any;
	distributors: ColumnModel[];
	modules: ColumnModel[];
	references: {[key: number]:  ColumnModel[]};
	units: ColumnModel[];
	views: any;
}
export class EntityModel {
	detailsFields: FieldModel[];
	fields: FieldModel[];
	hasErrorCode: number;
	id: number;
	name: string;
	referenceId: number;
}
export class FieldModel {
	caption: string;
	format: FormatModel;
	id: number;
	index: number;
	isEditable: boolean;
	name: string;
	type: string;
	typeId: number;
	unitName: string;
	value: any;
	valueReferenceId: any;
}
export class FieldNameValueModel {
	nameId: number;
	value: any;
}
export class FileModel {
	description: string;
	fileBytes: string;
	id: number;
	insertDate: Date;
	isBlocked: boolean;
	lastDownloaded: Date;
	parentEntityId: number;
	physicalFileName: string;
	size: number;
	version: string;
}
export class FormatModel {
	dateTimeFormat: string;
	isRequired: boolean;
	numberMaxPrecision: number;
	numberMaxScale: number;
	numberMaxValue: number;
	numberMinPrecision: number;
	numberMinScale: number;
	numberMinValue: number;
	textMaxLength: number;
	textMinLength: number;
}
export class FormValuesModel {
	fields: FieldNameValueModel[];
	id: number;
	values: number[];
}
export class GeneratedReportModel {
	id: number;
	reportType: number;
	resultString: string;
}
export class GridModel {
	hasErrorCode: number;
	totalCount: number;
	values: any[];
}
export class GridRequestModel {
	columns: ColumnModel[];
	ids: number[];
	pageNumber: number;
	pageSize: number;
	where: string;
}
export class GroupModel {
	builtin: boolean;
	distributor: string;
	hasErrorCode: number;
	id: number;
	name: string;
	referenceId: number;
}
export class HistoryIssueModel {
	deadline: Date;
	endDate: Date;
	hasErrorCode: number;
	id: number;
	idIssue: number;
	idIssueStatus: number;
	idOperator: number;
	idPriority: number;
	issueStatusDesc: string;
	notes: string;
	operatorDesc: string;
	priorityDesc: string;
	shortDescr: string;
	startDate: Date;
}
export class HistoryModel {
	description: string;
	eventDate: Date;
	eventObject: any;
	eventType: string;
	hasErrorCode: number;
	longDescription: string;
}
export class HistoryTaskModel {
	endDate: Date;
	hasErrorCode: number;
	id: number;
	idOperator: number;
	idTask: number;
	idTaskStatus: number;
	notes: string;
	operatorDesc: string;
	startDate: Date;
	taskStatusDesc: string;
}
export class ImportDataModel {
	data: any[][];
	headers: string[];
}
export class ImportProgressModel {
	errorCount: number;
	hasErrorCode: number;
	itemsCount: number;
	proceedItemsCount: number;
	status: string;
	statusId: number;
	successCount: number;
	warningCount: number;
}
export class ImportStatusModel {
	guid: string;
	hasErrorCode: number;
	statuses: SingleStatusModel[];
}
export class LocationEquipmentModel {
	device: string;
	endTime: Date;
	meter: string;
	startTime: Date;
}
export class LoginModel {
	locale: string;
	password: string;
	user: string;
}
export class LogoModel {
	id: number;
	jwt: string;
}
export class MeasurementModel {
	dataType: number;
	date: any;
	isDate: boolean;
	locationId: number;
	meterId: number;
	serialNumber: number;
	value: any;
}
export class MeasurementsRequestModel {
	columns: ColumnModel[];
	dateFrom: Date;
	dateTo: Date;
	deviceIds: number[];
	locationIds: number[];
	meterIds: number[];
}
export class NotificationEntity {
	id: number;
	isEmailValue: boolean;
}
export class NotificationModel {
	distributorIds: number[];
	operatorId: number;
	referenceObjectId: number;
	referenceType: string;
	reset: NotificationEntity[];
	set: NotificationEntity[];
	type: string;
}
export class ObjectReferenceModel {
	id: number;
	referenceObjectId: number;
}
export class OperatorSettingsModel {
	idOperator: number;
	settings: string;
}
export class PasswordResetEmailModel {
	locale: string;
	user: string;
}
export class PasswordResetModel {
	guid: string;
	locale: string;
	oldPassword: string;
	password: string;
}
export class PasswordResetResponseModel {
	hasErrorCode: number;
	message: string;
	status: string;
}
export class PointModel {
	idLocation: number;
	idTask: number;
}
export class ReferenceTypeModel {
	referenceTypeId: number;
	tableName: number;
}
export class ResponseDeleteModel {
	hasErrorCode: number;
	result: boolean;
}
export class ResponseListModel<T> {
	hasErrorCode: number;
	result: T[];
}
export class SaveFileModel {
	fileModel: FileModel;
	hasErrorCode: number;
	status: string;
}
export class SchemaElement {
	description: string;
	elementsApperance: SchemaElementApperance[];
	height: number;
	idDataType: number;
	isEditable: boolean;
	isUnitVisible: boolean;
	type: number;
	value: any;
	width: number;
	x: number;
	y: number;
}
export class SchemaElementApperance {
	color: string;
	condition: string;
	constraint1: any;
	constraint2: any;
	image: number[];
}
export class SchemaModel {
	hasErrorCode: number;
	schema: number[];
	schemaConfiguration: SchemaElement[];
}
export class SecurityChangeModel {
	added: number[];
	operatorId: number;
	removed: number[];
}
export class SecurityRoleModel {
	description: string;
	id: number;
	moduleName: string;
	name: string;
	valueString: string;
}
export class SimpleValueModel<T> {
	value: T;
}
export class SingleStatusModel {
	description: string;
	key: any;
	status: string;
	statusId: number;
}
export class TaskStatusChangeModel {
	statusId: number;
	taskIds: number[];
}
export class TaskStatusModel {
	id: number;
	name: string;
}
export class UserModel {
	avatar: string;
	id: number;
	idDistributor: number;
	permissions: string[];
	token: string;
	username: string;
}
export class ViewColumnModel {
	id: number;
	name: string;
}
export class KeyValuePair<TKey, TValue> {
	key: TKey;
	value: TValue;
}

// File is generated :)
export class ActionModel extends EntityModel {
}
export class ContactFormValuesModel extends FormValuesModel {
	groups: number[];
	locations: number[];
}
export class ContactModel extends EntityModel {
	groups: GroupModel[];
	locations: EntityModel[];
}
export class DashboardModel extends EntityModel {
	fitToScreen: boolean;
	keepAspectRatio: boolean;
	margin: number[];
	maxHeight: number;
	maxWidth: number;
	padding: number[];
	rows: DashboardRowModel[];
	useMargin: boolean;
	usePadding: boolean;
	width: number;
}
export class DeviceConnectionModel extends EntityModel {
	deviceLat: number;
	deviceLon: number;
	parentLat: number;
	parentLon: number;
	serialNbr: number;
	serialNbrCol: string;
	serialNbrParent: number;
	serialNbrParentCol: string;
}
export class DeviceModel extends EntityModel {
	currentMeterId: number;
	currentMeterName: string;
	locations: GroupModel[];
	meters: EntityModel[];
}
export class DistributorModel extends EntityModel {
}
export class FileColumnModel extends FileModel {
	columns: ColumnModel[];
}
export class GridRequestReferenceModel extends GridRequestModel {
	idReferenceType: number;
}
export class IssueModel extends EntityModel {
	history: HistoryIssueModel[];
	idLocation: number;
	tasks: TaskModel[];
}
export class LocationFormValuesModel extends FormValuesModel {
	devices: number[];
	groups: number[];
	meters: number[];
}
export class LocationModel extends EntityModel {
	allowGrouping: boolean;
	devices: DeviceModel[];
	groups: GroupModel[];
	history: HistoryModel[];
	latitude: number;
	locationId: number;
	longitude: number;
	meters: MeterModel[];
}
export class MeterModel extends EntityModel {
	alarmGroups: GroupModel[];
	allowGrouping: boolean;
	currentDeviceId: number;
	currentDeviceName: string;
	devices: EntityModel[];
}
export class ModelAlarm extends EntityModel {
	latitude: number;
	longitude: number;
}
export class OperatorModel extends EntityModel {
	activities: GridModel;
	actorFields: FieldModel[];
	allActivities: GridModel;
	notificationIssues: GridModel;
	notificationLocations: GridModel;
	notificationTasks: GridModel;
	notificationWorkOrders: GridModel;
	roles: GridModel;
}
export class ReportModel extends EntityModel {
}
export class RouteFormValuesModel extends FormValuesModel {
	locations: PointModel[];
	operators: number[];
	removedOperators: number[];
	removedTasks: number[];
}
export class RouteModel extends EntityModel {
	locations: EntityModel[];
	operators: EntityModel[];
}
export class SecurityActivityModel extends ObjectReferenceModel {
	operatorId: number;
}
export class TaskModel extends EntityModel {
	idLocation: number;
	taskString: string;
}