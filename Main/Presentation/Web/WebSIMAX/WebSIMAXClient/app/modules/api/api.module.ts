﻿import 'angular'

import { default as commonModule, LoggerService } from  './../common/common.module'

import { Api, ApiProvider } from './api.provider'
import { ApiResourceFactory } from './api.resource.factory'
import { LocalDictionaryFactory } from './api.local-dictionary.factory'
import { ApiHttpRequestInterceptorFactory } from './api.http-request-interceptor.factory'

var moduleName = 'simax.api'
var module = angular.module(moduleName, [require('angular-resource'), commonModule])

module.factory('apiResource', ['$resource', ($resource: ng.resource.IResourceService) => new ApiResourceFactory($resource)])
module.factory('httpRequestInterceptor', ['$injector', ($injector: ng.auto.IInjectorService) => new ApiHttpRequestInterceptorFactory($injector)])
module.factory('localDictionary', ['api', 'logger', (api: Api, logger: LoggerService) => new LocalDictionaryFactory(api, logger)])
module.provider('api', [() => new ApiProvider()])

module.config(['$httpProvider', ($httpProvider: ng.IHttpProvider) => {
    $httpProvider.interceptors.push('httpRequestInterceptor')
}])

export default moduleName
export { Api, ApiProvider, LocalDictionaryFactory }