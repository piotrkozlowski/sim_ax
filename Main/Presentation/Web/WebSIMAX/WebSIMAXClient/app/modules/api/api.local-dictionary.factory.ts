﻿import { Api } from  './api.module'
import { DictionaryModel, ColumnModel, SecurityRoleModel, DataTypeModel, DataTypeGroupModel, FormatModel, ViewColumnModel, OperatorSettingsModel } from './api.models'
import { LoggerService } from '../common/common.logger.service'
import { i18n } from '../common/i18n/common.i18n'
import { CommonHelper } from '../../helpers/common-helper'
import { UserConfigHelper } from '../../helpers/user-config-helper'

export class LocalDictionaryFactory {
    private dictionaries: DictionaryModel = null
    private promise: ng.IPromise<DictionaryModel>

    constructor(private api: Api, private logger: LoggerService) {
        
    }

    private arrayToColumnModel(array: any[]) {
        var result = new ColumnModel()
        result.alignment = array[0]
        result.caption = array[1]
        result.cellTemplate = array[2]
        result.chartType = array[3]
        result.editCellTemplate = array[4]
        result.format = array[5]
        result.id = array[6]
        result.idName = array[7]
        result.isEditable = array[8]
        result.name = array[9]
        result.position = array[10]
        result.referenceTypeId = array[11]
        result.sortDesc = array[12]
        result.type = array[13]
        result.typeName = array[14]
        result.unitId = array[15]
        result.unitName = array[16]
        result.where = array[17]
        result.width = array[18]
        return result
    }

    private decodeDictionaries(decoded: string): DictionaryModel {
        var base64 = CommonHelper.Base64.decode(decoded)
        var byteNumbers = new Array(base64.length);
        for (let i = 0; i < base64.length; i++) {
            byteNumbers[i] = base64.charCodeAt(i);
        }

        var msgpack = require("msgpack-lite")
        var data: any[] = msgpack.decode(byteNumbers)

        var result = new DictionaryModel()

        var dataTypeGroups: any[][] = data[0]
        result.dataTypeGroups = dataTypeGroups.map(dtg => {
            var r = new DataTypeGroupModel()
            r.idDataTypeGroup = dtg[0]
            r.idParentGroup = dtg[1]
            r.idReferenceType = dtg[2]
            r.name = dtg[3]
            r.referenceValue = dtg[4]
            return r
        })

        var dataTypeInGroups: any[][] = data[1]
        result.dataTypeInGroups = {}
        for (let i in dataTypeInGroups) {
            result.dataTypeInGroups[i] = dataTypeInGroups[i]
        }

        var dataTypes = data[2]
        result.dataTypes = {}
        for (let i in dataTypes) {
            var dataType = dataTypes[i]
            var r = new DataTypeModel()
            r.caption = dataType[0]

            var format = dataType[1]
            var f = new FormatModel()
            f.dateTimeFormat = format[0]
            f.isRequired = format[1]
            f.numberMaxPrecision = format[2]
            f.numberMaxScale = format[3]
            f.numberMaxValue = format[4]
            f.numberMinPrecision = format[5]
            f.numberMinScale = format[6]
            f.numberMinValue = format[7]
            f.textMaxLength = format[8]
            f.textMinLength = format[9]

            r.format = f
            r.isEditable = dataType[2]
            r.referenceTypeId = dataType[3]
            r.type = dataType[4]
            r.typeName = dataType[5]
            r.unitId = dataType[6]
            r.unitName = dataType[7]

            result.dataTypes[i] = r
        }

        result.distributors = data[3].map(d => this.arrayToColumnModel(d))
        result.modules = data[4].map(d => this.arrayToColumnModel(d))

        var references = data[5]
        result.references = {}
        for (let i in references) {
            result.references[i] = references[i].map(d => this.arrayToColumnModel(d))
        }

        result.units = data[6].map(d => this.arrayToColumnModel(d))

        var views = data[7]
        result.views = {}
        for (let i in views) {
            var columns = views[i]
            result.views[i] = columns.map(column => {
                var c = new ViewColumnModel()
                c.id = column[0]
                c.name = column[1]
                return c
            })
        }

        return result
    }

    private getDictionariesInfo() {
        var dictionaryInfo: { dictionary: string, locale: string, version: number, user: number } = CommonHelper.getValueFromLocalStorage('DICTIONARY')
        return dictionaryInfo
    }

    private getDictionaries(): DictionaryModel {
        return this.dictionaries
    }

    private saveDictionaries(dictionary: string, locale: string, version: number, user: number) {
        CommonHelper.setValueToLocalStorage('DICTIONARY', { dictionary: dictionary, locale: locale, version: version, user: user })
    }

    private getPromise(): ng.IPromise<DictionaryModel> {
        if (this.promise) return this.promise
        else return this.refreshDictionaries()
    }

    private getFieldFromDictionary(name: string): ng.IPromise<any> {
        return this.getPromise().then(dictionaries => {
            return this.getDictionaries()[name]
        })
    }

    private getDataTypeInGroup(dataTypeGroups: DataTypeGroupModel[], dataTypesInGroup: {}, dataTypeGroupName: string, referenceType: number = null, referenceValue: number = null): number[]  {
        var result = []

        var group: DataTypeGroupModel = null

        if (referenceType == null || referenceValue == null) {
            var groups = dataTypeGroups.filter(g => g.name == dataTypeGroupName && g.idReferenceType == null && g.referenceValue == null)
            if (groups.length > 0) group = groups[0]
        } else {
            var groups = dataTypeGroups.filter(g => g.name == dataTypeGroupName && g.idReferenceType == referenceType && g.referenceValue == referenceValue)
            if (groups.length == 0) {
                groups = dataTypeGroups.filter(g => g.name == dataTypeGroupName && g.idReferenceType == null && g.referenceValue == null)
                if (groups.length > 0) group = groups[0]
            } else {
                group = groups[0]
            }
        }

        if (group == null) return result

        var dataTypeInGroup: number[] = dataTypesInGroup[group.idDataTypeGroup]
        if (!dataTypeInGroup) return result;
        else if (!group.idParentGroup)
            return dataTypeInGroup

        var dataTypeInParentGroup: number[] = dataTypesInGroup[group.idParentGroup]

        if (!dataTypeInParentGroup) return dataTypeInGroup

        dataTypeInGroup.forEach(item => {
            var index = -1
            dataTypeInParentGroup.some((d, i) => {
                if (d == item) {
                    index = i
                    return true
                }
                return false
            })
            if (index > -1) {
                dataTypeInParentGroup[index] = item
            } else {
                dataTypeInParentGroup.push(item)
            }
        })

        return dataTypeInParentGroup;
    }

    private getDictionary(model: string, referenceType: number = null, referenceValue: number = null): ng.IPromise<ColumnModel[]> {
        return this.getPromise().then(dictionaries => {            
            var result: ColumnModel[] = []

            var dataTypeInGroupIds = this.getDataTypeInGroup(dictionaries.dataTypeGroups, dictionaries.dataTypeInGroups, model, referenceType, referenceValue)
            dataTypeInGroupIds.forEach(id => {
                var dataType: DataTypeModel = dictionaries.dataTypes[id]
                if (dataType) {
                    var column = new ColumnModel()
                    column.id = id
                    column.caption = dataType.caption
                    column.name = id.toString()
                    column.type = dataType.type
                    column.typeName = dataType.typeName
                    column.referenceTypeId = dataType.referenceTypeId
                    column.isEditable = dataType.isEditable
                    column.format = dataType.format
                    column.unitId = dataType.unitId
                    column.unitName = dataType.unitName

                    result.push(column)
                }
            })
            return result
        })
    }

    private getColumnsWithNames(columns: ColumnModel[], idReferenceType: number): ng.IPromise<ColumnModel[]> {
        return this.getPromise().then(dictionaries => {
            var viewColumns = dictionaries.views[idReferenceType]

            if (!viewColumns) return columns
            else {
                var result: ColumnModel[] = []
                viewColumns.forEach(vc => {
                    columns.forEach(c => {
                        if (vc.id == c.id) {
                            var column = new ColumnModel()

                            column.id = c.id
                            column.caption = c.caption
                            column.name = vc.name.toLowerCase()
                            column.type = c.type
                            column.typeName = c.typeName
                            column.referenceTypeId = c.referenceTypeId
                            column.isEditable = c.isEditable
                            column.format = c.format
                            column.unitId = c.unitId
                            column.unitName = c.unitName

                            result.push(column)
                        }
                    })
                })
                return result
            }
        })
    }

    //private get
    public getReferenceTable(referenceId: number): ng.IPromise<ColumnModel[]> {
        return this.getFieldFromDictionary('references').then(references => {
            return references[referenceId]
        })
    }

    // Location
    public getLocationColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    public getLocationDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    public getLocationDeviceDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_DEVICE_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getLocationMeterDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_METER_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdMeter)
        })
    }

    public getLocationDisplayFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_PANEL_PARAMS_TO_DISPLAY', CommonHelper.IdDistributor, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    public getLocationEditFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        if (referenceValue == null) {
            referenceValue = this.api.login.getUser().idDistributor
        }
        return this.getDictionary('WEBSIMAX_LOCATION_PANEL_PARAMS_TO_EDIT', CommonHelper.IdDistributor, referenceValue)
    }

    public getLocationRequiredFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        if (referenceValue == null) {
            referenceValue = this.api.login.getUser().idDistributor
        }
        return this.getDictionary('WEBSIMAX_LOCATION_PARAMS_REQUIRED', CommonHelper.IdDistributor, referenceValue).then(columns => {
            return columns
        })
    }

    public getLocationGridDisplayFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_GRID_PARAMS_TO_DISPLAY', CommonHelper.IdDistributor, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    public getLocationPreviewFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_LOCATION_PREVIEW_PANEL_PARAMS', CommonHelper.IdDistributor, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    // Device Connections

    public getDeviceConnectionsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_CONNECTION_GRID_PARAMS', CommonHelper.IdDeviceConnection, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDeviceConnection)
        })
    }

    public getDeviceConnectionsDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_CONNECTION_DEFAULT_GRID_PARAMS', CommonHelper.IdDeviceConnection, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDeviceConnection)
        })
    }

    public getDeviceConnectionsPreviewFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        alert("KOWAAAAAL")
        return this.getDictionary('WEBSIMAX_DEVICE_CONNECTION_PREVIEW_PANEL_PARAMS', CommonHelper.IdDeviceConnection, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDeviceConnection)
        })
    }

    
    // Device
    public getDevicesHierarchyColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: 'Device', caption: i18n('Common', 'Device') })
        columns.push({ id: 2, idName: 'SlotNbr', caption: i18n('Common', 'SlotNbr') })
        columns.push({ id: 3, idName: 'SlotType', caption: i18n('Common', 'SlotType') })
        columns.push({ id: 4, idName: 'ProtocolIn', caption: i18n('Common', 'ProtocolIn') })
        columns.push({ id: 5, idName: 'ProtocolOut', caption: i18n('Common', 'ProtocolOut') })
        columns.push({ id: 6, idName: 'Active', caption: i18n('Common', 'Active'), type: 7 })

        deferred.resolve(columns)
        
        return deferred.promise()
    }

    public getDevicesColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getDevicesDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getDeviceDisplayFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_PANEL_PARAMS_TO_DISPLAY', CommonHelper.IdDeviceType, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getDeviceEditFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_PANEL_PARAMS_TO_EDIT', CommonHelper.IdDeviceType, referenceValue)
    }

    public getDeviceRequiredFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_PARAMS_REQUIRED', CommonHelper.IdDeviceType, referenceValue)
    }
    
    public getDeviceGridDisplayFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_GRID_PARAMS_TO_DISPLAY', CommonHelper.IdDeviceType, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getDeviceGridEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_GRID_PARAMS_TO_EDIT')
    }

    public getDevicePreviewFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DEVICE_PREVIEW_PANEL_PARAMS', CommonHelper.IdDeviceType, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    //Route
    public getRoutesColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdRoute)
        })
    }
    public getRoutesDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_DEFAULT_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdRoute)
        })
    }
    public getRouteDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdRoute)
        })
    }
    public getRouteEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_PANEL_PARAMS_TO_EDIT')
    }
    public getRouteRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_PARAMS_REQUIRED')
    }

    public getRoutePointColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_LOCATION_GRID_PARAMS')
    }  
    public getRoutePointColumnsEdit(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ROUTE_LOCATION_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }    
    public getRouteOperatorsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getPromise().then(() => {
            var columns = []
            columns.push({ id: 1, idName: 'name', name: 'Name', caption: i18n('Common', 'Operator') })
            return columns
        })
    }
    public getRouteTrackingColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: 'Time', caption: i18n('Common', 'Time') })
        columns.push({ id: 2, idName: 'EventType', caption: i18n('Common', 'EventType') })

        deferred.resolve(columns)

        return deferred.promise()
    }

    // Meter
    public getMeterColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdMeter)
        })
    }

    public getMeterDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdMeter)
        })
    }

    public getMeterDeviceColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_DEVICE_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    public getMeterDisplayFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_PANEL_PARAMS_TO_DISPLAY', CommonHelper.IdMeterTypeClass, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdMeter)
        })
    }

    public getMeterEditFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_PANEL_PARAMS_TO_EDIT', CommonHelper.IdMeterTypeClass, referenceValue)
    }

    public getMeterRequiredFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_PARAMS_REQUIRED', CommonHelper.IdMeterTypeClass, referenceValue)
    }

    public getMeterPreviewFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_METER_PREVIEW_PANEL_PARAMS', CommonHelper.IdDeviceType, referenceValue).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDevice)
        })
    }

    // Measurement
    private getChartTypes() {
        return this.getDictionary('WEBSIMAX_DATA_TYPE_CHART_TYPE', CommonHelper.IdChartType, CommonHelper.ChartTypeLine).then(chartDataTypesLine => {
            return this.getDictionary('WEBSIMAX_DATA_TYPE_CHART_TYPE', CommonHelper.IdChartType, CommonHelper.ChartTypeBar).then(chartDataTypesBar => {
                return [chartDataTypesLine, chartDataTypesBar]
            })
        })
    }

    private updateFieldsByChartTypes(chartTypes: ColumnModel[][], fields: ColumnModel[]) {
        var [chartDataTypesLine, chartDataTypesBar] = chartTypes

        fields.forEach(f => {
            chartDataTypesLine.forEach(c => {
                if (f.id == c.id)
                    f.chartType = 'line'
            })
            chartDataTypesBar.forEach(c => {
                if (f.id == c.id)
                    f.chartType = 'bar'
            })
        })

        return fields
    }

    public getMeasurementsForLocationFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getChartTypes().then(result => {
            return this.getDictionary('WEBSIMAX_LOCATION_MEASUREMENTS', CommonHelper.IdDistributor, referenceValue).then(fields => {
                return this.updateFieldsByChartTypes(result, fields)
            })
        })        
    }

    public getMeasurementsForDeviceFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getChartTypes().then(result => {
            return this.getDictionary('WEBSIMAX_DEVICE_MEASUREMENTS', CommonHelper.IdDeviceType, referenceValue).then(fields => {
                return this.updateFieldsByChartTypes(result, fields)
            })
        })   
    }

    public getMeasurementsForMeterFields(referenceValue: number): ng.IPromise<ColumnModel[]> {
        return this.getChartTypes().then(result => {
            return this.getDictionary('WEBSIMAX_METER_MEASUREMENTS', CommonHelper.IdMeterTypeClass, referenceValue).then(fields => {
                return this.updateFieldsByChartTypes(result, fields)
            })
        })   
    }

    // User
    public getUserColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_OPERATOR_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdOperator)
        })
    }

    public getUserDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_OPERATOR_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdOperator)
        })
    }

    public getUserDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdOperator)
        })
    }

    public getUserActorDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdOperator)
        })
    }

    public getUserEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_OPERATOR_PANEL_PARAMS_TO_EDIT')
    }

    public getUserRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_OPERATOR_PARAMS_REQUIRED')
    }

    // Actor
    public getActorDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdActor)
        })
    }

    public getActorEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PANEL_PARAMS_TO_EDIT')
    }

    public getActorRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PARAMS_REQUIRED')
    }

    // Issues
    public getIssuesColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdIssue)
        })
    }

    public getIssuesDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdIssue)
        })
    }

    public getIssueTaskDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_TASK_GRID_PARAMS')
    }

    public getIssueDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdIssue)
        })
    }

    public getIssueEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_PANEL_PARAMS_TO_EDIT')
    }

    public getIssueRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ISSUE_PARAMS_REQUIRED')
    }

    // Tasks
    public getTasksColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_TASK_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdTask)
        })
    }

    public getTasksDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_TASK_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdTask)
        })
    }

    public getTaskDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_TASK_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdTask)
        })
    }

    public getTaskEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_TASK_PANEL_PARAMS_TO_EDIT')
    }

    public getTaskRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_TASK_PARAMS_REQUIRED')
    }

    //Alarms
    public getAlarmsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ALARM_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdAlarmEvent)
        })
    }

    public getAlarmsDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ALARM_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdAlarmEvent)
        })
    }

    public getAlarmDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ALARM_PANEL_PARAMS_TO_DISPLAY', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdAlarmEvent)
        })
    }

    public getAlarmEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ALARM_PANEL_PARAMS_TO_EDIT', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor)
    }

    public getAlarmRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ALARM_PARAMS_REQUIRED')
    }

    // Distributors
    public getDistributorDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DISTRIBUTOR_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDistributor)
        })
    }

    public getDistributorColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DISTRIBUTOR_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor).then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDistributor)
        })
    }

    public getDistributorDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdDistributor)
        })
    }

    public getDistributorEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DISTRIBUTOR_PANEL_PARAMS_TO_EDIT')
    }

    public getDistributorRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_DISTRIBUTOR_PARAMS_REQUIRED')
    }

    //Contact
    public getContactColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor)
    }
    public getContactDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor)
    }
    public getContactGroupsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getPromise().then(() => {
            var columns = []
            columns.push({ id: 1, idName: "name", name: 'Name', caption: i18n('Common', 'Name') })
            columns.push({ id: 2, idName: "distributor", name: 'Distributor', caption: i18n('Common', 'Distributor') })
            columns.push({ id: 3, idName: "builtin", name: 'BuiltIn', caption: i18n('Common', 'BuiltIn'), type: 7 })
                       
            return columns
        })     
    }
    public getContactDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PANEL_PARAMS_TO_DISPLAY')
    }
    public getContactEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PANEL_PARAMS_TO_EDIT')
    }
    public getContactRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_PARAMS_REQUIRED')
    }
    public getContactLocationColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary ('WEBSIMAX_ACTOR_LOCATION_GRID_PARAMS')
    }
    public getContactLocationColumnsEdit(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTOR_LOCATION_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdLocation)
        })
    }

    //Sim cards
    public getSimcardsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_SIM_CARD_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdSimCard)
        })        
    }
    public getSimcardsDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_SIM_CARD_DEFAULT_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdSimCard)
        })
    }
    public getSimcardsDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdSimCard)
        })
    }
    public getSimcardsEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_SIM_CARD_PANEL_PARAMS_TO_EDIT').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdSimCard)
        })
    }
    public getSimcardsRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_SIM_CARD_PARAMS_REQUIRED').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdSimCard)
        })
    }

    // Actions
    public getActionColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTION_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor)
    }
    public getActionDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_ACTION_DEFAULT_GRID_PARAMS', CommonHelper.IdDistributor, this.api.login.getUser().idDistributor)
    }

    // Reports
    public getReportsColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_REPORT_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdReport)
        })
    }

    public getReportsDefaultColumns(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_REPORT_DEFAULT_GRID_PARAMS').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdReport)
        })
    }

    public getReportsDisplayFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_REPORT_PANEL_PARAMS_TO_DISPLAY').then(columns => {
            return this.getColumnsWithNames(columns, CommonHelper.IdReport)
        })
    }

    public getReportsEditFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_REPORT_PANEL_PARAMS_TO_EDIT')
    }

    public getReportsRequiredFields(): ng.IPromise<ColumnModel[]> {
        return this.getDictionary('WEBSIMAX_REPORT_PARAMS_REQUIRED ')
    }

    // Others
    public getActivityColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: "Description", typeName: "Text", caption: i18n('Popup', 'Description') })
        columns.push({ id: 2, idName: "Allow", typeName: "Text", caption: i18n('Popup', 'Allow'), type: 7 })
        columns.push({ id: 3, idName: "ReferenceType", typeName: "Text", caption: i18n('Popup', 'ReferenceType') })
        columns.push({ id: 4, idName: "ReferenceObject", typeName: "Text", caption: i18n('Popup', 'ReferenceObject') })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getAttachmentColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: "physicalFileName", caption: i18n('Common', 'PhysicalFileName') })
        columns.push({ id: "size", caption: i18n('Common', 'Size'), unitName: 'Mb', type: 1 })
        columns.push({ id: "description", caption: i18n('Common', 'Description') })
        columns.push({ id: "insertDate", caption: i18n('Common', 'InsertDate'), type: 6 })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getLocationEquipmentColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: "Device", caption: i18n('Common', 'Device') })
        columns.push({ id: 2, idName: "Meter", caption: i18n('Common', 'Meter') })
        columns.push({ id: 3, idName: "StartTime", caption: i18n('Common', 'StartTime'), type: 6 })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getTaskForIssueColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: "TaskString", caption: i18n('Common', 'TaskString') })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getRoleColumns(): JQueryPromise<ColumnModel[]> {
        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: "Name", typeName: "Text", caption: i18n('Popup', 'Name') })
        columns.push({ id: 2, idName: "Description", typeName: "Text", caption: i18n('Popup', 'Description') })
        columns.push({ id: 3, idName: "ModuleName", typeName: "Text", caption: i18n('Popup', 'ModuleName') })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getNotificationColumns(): JQueryPromise<ColumnModel[]> {

        var deferred = $.Deferred<ColumnModel[]>()

        var columns = []
        columns.push({ id: 1, idName: "Action", typeName: "Text", caption: i18n('Popup', 'Action') })
        columns.push({ id: 2, idName: "NotificationByEmail", typeName: "Boolean", caption: i18n('Popup', 'NotificationByEmail') })
        columns.push({ id: 3, idName: "NotificationBySms", typeName: "Boolean", caption: i18n('Popup', 'NotificationBySms') })

        deferred.resolve(columns)

        return deferred.promise()
    }

    public getModules(): ng.IPromise<ColumnModel[]> {
        return this.getFieldFromDictionary('modules')
    }

    public getAllDistributors(): ng.IPromise<ColumnModel[]> {
        return this.getFieldFromDictionary('distributors')
    }

    public getUnits(): ng.IPromise<ColumnModel[]> {
        return this.getFieldFromDictionary('units')
    }

    public getIdUnitByIdDataType(id: number) {
        return this.getPromise().then(dictionaries => {
            var dataType = dictionaries.dataTypes[id]
            console.log(dataType)
            if (dataType) return dataType.unitId
            return null
        })
    }

    public refreshDictionaries(forceRefresh?: boolean): ng.IPromise<DictionaryModel> {
        var dictionariesInfo = this.getDictionariesInfo()

        var promise = this.api.dictionary.getVersion().then(version => {
            var locale = CommonHelper.getValueFromLocalStorage('LOCALE')
            var userId = this.api.login.getUser().id
            if (!dictionariesInfo || locale != dictionariesInfo.locale || dictionariesInfo.version != version.value || userId != dictionariesInfo.user || forceRefresh) {
                return UserConfigHelper.loadSettingsFromDb(this.api, userId)
                    .then(() => this.api.dictionary.getDictionaries())
                    .then(encodedDictionary => {
                        var dictionaries = this.decodeDictionaries(encodedDictionary)
                        if (dictionaries != null) {
                            this.saveDictionaries(encodedDictionary, locale, version.value, userId)                         
                            return dictionaries
                        }
                        throw new Error("Can't download dictionaries")
                    })
            } else return this.decodeDictionaries(dictionariesInfo.dictionary)
        }).then(dictionaries => {
            this.dictionaries = dictionaries
            return dictionaries
        })

        promise.catch(reason => { this.logger.log(reason) })

        this.promise = promise
        return promise
    }    
}