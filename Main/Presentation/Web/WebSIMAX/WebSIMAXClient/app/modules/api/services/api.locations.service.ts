﻿﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, LocationModel, EntityModel, SchemaModel, ImportProgressModel, ResponseListModel, ResponseDeleteModel, ImportStatusModel } from '../api.models'
import { LoginService } from './api.login.service'
import { IImportService } from './api.app.service'
import { CommonHelper } from '../../../helpers/common-helper'

export interface ILocationsService extends IImportService {
    getLocations(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    saveLocation(req): ng.IPromise<LocationModel>
    getLocation(id, state?: any): ng.IPromise<LocationModel>
    getLocationToEdit(id, state?: any): ng.IPromise<EntityModel>
    deleteLocation(id): ng.IPromise<ResponseDeleteModel>
    getSchema(id, state?: any): ng.IPromise<SchemaModel>
    getSchemas(id, state?: any): ng.IPromise<ResponseListModel<SchemaModel>>
}

export class LocationsService implements ILocationsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getLocations(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.locations.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getLocation(id, state: any = {}) {
        var get = this.apiResource.locations.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getLocationToEdit(id, state: any = {}) {
        var getLocationToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.locations['getLocationToEdit']
        state.request = getLocationToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveLocation(req) {
        return this.apiResource.locations.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteLocation(id) {
        return this.apiResource.locations.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getSchema(id, state: any = {}) {
        var get = this.apiResource.locations['getSchema']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    getSchemas(id, state: any = {}) {
        var get = this.apiResource.locations['getSchemas']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status, state))
    }

    initImport(model: GridModel) {
        return this.apiResource.locations['importInit'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    startImport(guid: string) {
        return this.apiResource.locations['importStart']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    stopImport(guid: string) {
        return this.apiResource.locations['importStop']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importProgress(guid: string) {
        return this.apiResource.locations['importProgress']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    importSummary(guid: string) {
        return this.apiResource.locations['importSummary']({ guid: guid }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeLocationsService implements ILocationsService {
    constructor(private $q: ng.IQService) {

    }

    getLocations(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    saveLocation(req) {
        return this.$q.resolve({})
    }

    getLocation(id) {
        return this.$q.resolve({})
    }

    getLocationToEdit(id) {
        return this.$q.resolve({})
    }

    deleteLocation(id) {
        return this.$q.resolve({})
    }

    getSchema(id) {
        return this.$q.resolve({})
    }

    getSchemas(id) {
        return this.$q.resolve({})
    }

    initImport(model: GridModel) {
        return this.$q.resolve({})
    }

    startImport(guid: string) {
        return this.$q.resolve({})
    }

    stopImport(guid: string) {
        return this.$q.resolve({})
    }

    importProgress(guid: string) {
        return this.$q.resolve({})
    }

    importSummary(guid: string) {
        return this.$q.resolve({})
    }
}