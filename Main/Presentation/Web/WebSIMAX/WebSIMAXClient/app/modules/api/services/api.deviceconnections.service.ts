﻿﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, LocationModel, EntityModel, SchemaModel } from '../api.models'
import { LoginService } from './api.login.service'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IDeviceConnectionsService {
    getDeviceConnections(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
}

export class DeviceConnectionsService implements IDeviceConnectionsService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getDeviceConnections(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.deviceconnections.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result);
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeDeviceConnectionsService implements IDeviceConnectionsService {
    constructor(private $q: ng.IQService) {

    }

    getDeviceConnections(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }
}