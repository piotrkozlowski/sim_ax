﻿import { ApiResourceFactory } from '../api.resource.factory'
import { PasswordResetModel, PasswordResetEmailModel, PasswordResetResponseModel, SimpleValueModel } from '../api.models'

export interface IPasswordService {
    reset(model: PasswordResetEmailModel): ng.IPromise<string>
    change(model: PasswordResetModel): ng.IPromise<PasswordResetResponseModel>
}

export class PasswordService implements IPasswordService {
    constructor(private apiResource: ApiResourceFactory) {
        
    }

    public reset(model: PasswordResetEmailModel): ng.IPromise<string> {
        return this.apiResource.password.get(model).$promise.then((result: SimpleValueModel<string>) => {
            return result.value
        })
    }

    public change(model: PasswordResetModel): ng.IPromise<PasswordResetResponseModel> {
        return this.apiResource.password.save(model).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }
}

export class FakePasswordService implements IPasswordService {
    constructor(private $q: ng.IQService) {

    }

    public reset(model: PasswordResetEmailModel): ng.IPromise<string> {
        return this.$q.resolve("")
    }

    public change(model: PasswordResetModel): ng.IPromise<PasswordResetResponseModel> {
        return this.$q.resolve({})
    }
}