﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, DeviceModel, EntityModel, DeviceHierarchyModel, DevicesEditModel, ObjectReferenceModel, SchemaModel, ResponseListModel, ResponseDeleteModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IDevicesService {
    getDevices(req: GridRequestModel, state?: any): ng.IPromise<GridModel> // TODO
    createDevice(req): ng.IPromise<DeviceModel>
    getDevice(id, state?: any): ng.IPromise<DeviceModel>
    getDeviceToEdit(id, state?: any): ng.IPromise<EntityModel>
    getParentDevices(id, state?: any): ng.IPromise<ResponseListModel<DeviceHierarchyModel>>
    getChildDevices(id, state?: any): ng.IPromise<ResponseListModel<DeviceHierarchyModel>>
    saveChildDevices(editModel: DevicesEditModel): ng.IPromise<ResponseListModel<DeviceHierarchyModel>>
    deleteDevice(id): ng.IPromise<ResponseDeleteModel>
    changeCurrentMeter(model: ObjectReferenceModel): ng.IPromise<DeviceModel>
    getSchemas(id, state?: any): ng.IPromise<ResponseListModel<SchemaModel>>
}

export class DevicesService implements IDevicesService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getDevices(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.devices.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getDevice(id, state: any = {}) {
        var get = this.apiResource.devices.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getDeviceToEdit(id, state: any = {}) {
        var getDeviceToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.devices['getDeviceToEdit']
        state.request = getDeviceToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getParentDevices(id, state: any = {}) {
        var getParentDevices: ng.resource.IResourceMethod<ng.resource.IResource<DeviceHierarchyModel[]>> = this.apiResource.devices['getParentDevices']
        state.request = getParentDevices({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getChildDevices(id, state: any = {}) {
        var getChildDevices: ng.resource.IResourceMethod<ng.resource.IResource<DeviceHierarchyModel[]>> = this.apiResource.devices['getChildDevices']
        state.request = getChildDevices({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    saveChildDevices(editModel: DevicesEditModel) {
        return this.apiResource.devices['saveChildDevices'](editModel).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createDevice(req) {
        return this.apiResource.devices.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteDevice(id) {
        return this.apiResource.devices.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    changeCurrentMeter(model: ObjectReferenceModel) {
        return this.apiResource.devices['changeCurrentMeter'](model).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getSchemas(id, state: any = {}) {
        var get = this.apiResource.devices['getSchemas']
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return result
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}


export class FakeDevicesService implements IDevicesService {
    constructor(private $q: ng.IQService) {

    }

    getDevices(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    createDevice(req) {
        return this.$q.resolve({})
    }

    getDevice(id) {
        return this.$q.resolve({})
    }

    getDeviceToEdit(id) {
        return this.$q.resolve({})
    }

    getParentDevices(id) {
        return this.$q.resolve({})
    }

    getChildDevices(id) {
        return this.$q.resolve({})
    }

    saveChildDevices(editModel: DevicesEditModel) {
        return this.$q.resolve({})
    }

    deleteDevice(id) {
        return this.$q.resolve({})
    }

    changeCurrentMeter(model: ObjectReferenceModel) {
        return this.$q.resolve({})
    }

    getSchemas(id) {
        return this.$q.resolve({})
    }
}