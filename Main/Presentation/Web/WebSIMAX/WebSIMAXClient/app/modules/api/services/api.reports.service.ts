﻿import { ApiResourceFactory } from '../api.resource.factory'
import { GridModel, GridRequestModel, EntityModel, ReportModel, ResponseDeleteModel, GeneratedReportModel } from '../api.models'
import { CommonHelper } from '../../../helpers/common-helper'

export interface IReportsService {
    getReports(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getAllReports(req: GridRequestModel, state?: any): ng.IPromise<GridModel>
    getReport(id, state?: any): ng.IPromise<any>
    getReportToEdit(id, state?: any): ng.IPromise<EntityModel>
    createReport(req): ng.IPromise<ReportModel>
    deleteReport(id): ng.IPromise<ResponseDeleteModel>
    generateReport(id): ng.IPromise<GeneratedReportModel>
}

export class ReportsService implements IReportsService {
    constructor(private apiResource: ApiResourceFactory) {

    }
    getReports(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.reports.query
        state.request = get(req)
        return state.request.$promise.then(result => {
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
    
    getAllReports(req: GridRequestModel, state: any = {}) {
        var get = this.apiResource.reports['getAllReports']
        state.request = get(req)
        return state.request.$promise.then(result => {
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getReport(id, state: any = {}) {
        var get = this.apiResource.reports.get
        state.request = get({ id: id })
        return state.request.$promise.then(result => {
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    getReportToEdit(id, state: any = {}) {
        var getReportToEdit: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.reports['getReportToEdit']
        state.request = getReportToEdit({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    createReport(req) {
        return this.apiResource.reports.save(req).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    deleteReport(id) {
        return this.apiResource.reports.delete({ id: id }).$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }

    generateReport(id, state: any = {}) {
        var generateReport: ng.resource.IResourceMethod<ng.resource.IResource<EntityModel>> = this.apiResource.reports['generateReport']
        state.request = generateReport({ id: id })
        return state.request.$promise.then(result => {
            CommonHelper.checkResultError(result)
            return (<any>result).toJSON()
        }).catch(rej => CommonHelper.handleRejection(this.apiResource, rej.status))
    }
}

export class FakeReportsService implements IReportsService {
    constructor(private $q: ng.IQService) {

    }
    getReports(req: GridRequestModel = null) {
        return this.$q.resolve(new GridModel())
    }

    getAllReports(req: GridRequestModel) {
        return this.$q.resolve(new GridModel())
    }

    getReport(id) {
        return this.$q.resolve({})
    }

    getReportToEdit(id) {
        return this.$q.resolve({})
    }

    createReport(req) {
        return this.$q.resolve({})
    }

    deleteReport(id) {
        return this.$q.resolve({})
    }

    generateReport(id) {
        return this.$q.resolve({})
    }
}