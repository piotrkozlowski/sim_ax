﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { ContextListModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { i18n } from '../common/i18n/common.i18n'
import { Permissions } from '../../helpers/permissions'
import { BaseGridGridsterController, ScopeBaseGridsterGrid } from '../common/common.base.grid.gridster.controller'

declare var google: any

interface DeviceconnetionsScope extends ScopeBaseGridsterGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
    showRadioDeviceDetails: any
}

export class DeviceConnectionsController extends BaseGridGridsterController {
    constructor($scope: DeviceconnetionsScope, private $state: angular.ui.IStateService, $timeout: ng.ITimeoutService, protected $compile: ng.ICompileService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            // fake grid...
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api,
            {
                ids: [],
                measurmentIdField: "deviceConnetcionId",
                measurmentIdsField: "deviceConnetcionIds",
                methodForDisplayFields: "getDeviceConnectionsPreviewFields",
                methodForMeasurments: "getMeasurementsForDeviceConnections",
                methodForMeasurmentsFields: "getMeasurementsForDeviceConnectionsFields",
                methodForPreviewFields: "getDeviceConnectionsPreviewFields",
                methodName: "getDeviceConnections",
                serviceName: "deviceconnections",
                filterMethodName: "getDeviceConnections"
        }, 'DEVICECONNECTIONS', true, false)

        $scope.importOptions = {
            dictionaryMethodName: '',
            popupName: i18n('Popup', 'ImportLocations'),
            requestMethodType: 'deviceconnections'
        }

        $scope.getItems = () => {
            return jQuery.when.apply(jQuery,
                [Permissions.DEVICE_LIST]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [DEVICE_LIST] = permissions
                var items = []
                if (DEVICE_LIST) {
                    items.push({
                        template: CommonHelper.getIconForDetails("DETAILS", '#000000', '18', i18n('Common', 'HubDetails'), 'deviceconnections-context-menu-row'), onItemClick: () => {
                            this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
                        }
                    })

                    items.push({
                        template: CommonHelper.getIconForDetails("DETAILS", '#000000', '18', i18n('Common', 'RadioDeviceDetails'), 'deviceconnections-context-menu-row'), onItemClick: () => {
                            $scope.showRadioDeviceDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
                        }
                    })
                }
                
                items.push({
                    template: CommonHelper.getIconForDetails("DETAILS_PREVIEW", '#000000', '18', i18n('Common', 'ShowHubs'), 'deviceconnections-context-menu-row'), onItemClick: () => {
                        this.$scope.dataGrid.filter([this.getSerialNbrColumnName($scope.dataGrid.getSelectedRowsData()[0]), '=', this.getSerialNbr($scope.dataGrid.getSelectedRowsData()[0])])
                    }
                })

                items.push({
                    template: CommonHelper.getIconForDetails("DETAILS_PREVIEW", '#000000', '18', i18n('Common', 'ShowRadioDevices'), 'deviceconnections-context-menu-row'), onItemClick: () => {
                        this.$scope.dataGrid.filter([this.getSerialNbrParentColumnName($scope.dataGrid.getSelectedRowsData()[0]), '=', this.getSerialNbrParent($scope.dataGrid.getSelectedRowsData()[0])])
                    }
                })
                return items
            })
        }


        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "deviceconnections",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                show: "['" + Permissions.DEVICE_LIST + "']",
                edit: "['" + Permissions.DEVICE_EDIT + "']",
                import: "['" + Permissions.DEVICE_IMPORT + "']",
                add: "['" + Permissions.DEVICE_ADD + "']",
                export: "['" + Permissions.DEVICE_EXPORT + "']"
            },
            hideEditButton: true,
            hideAddButton: true
        })        

        $scope.buttonsStates = buttonsStates
        this.addButtons(buttonsOptions)
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            this.setSettingsListOptions([false, false, false, false], null, 'deviceConnections', null, this.showMapDetails)
        }, 0)

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_DEVICES_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }

        $scope.showRadioDeviceDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item['serialNbr']
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_DEVICES_DETAILS_POPUP + $scope.popupId, e.data['serialNbr'], itemIds)
        }
    }

    getSerialNbrColumnName = (column) => {
        return column.serialNbrCol
    }

    getSerialNbr = (column) => {
        return column.serialNbr
    }

    getSerialNbrParentColumnName = (column) => {
        return column.serialNbrParentCol
    }

    getSerialNbrParent = (column) => {
        return column.serialNbrParent
    }


    getDeviceconnections = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.deviceconnections.getDeviceConnections(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getDeviceConnectionsColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getDeviceConnectionsColumns().then(columns => {
            this.localDictionary.getDeviceConnectionsDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "DeviceConnectionsGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onSelectionChanged: this.onSelectionChanged,
                    onDataReady: this.onDataReady,
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }

                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getDeviceconnections, customOptions, true, this.localDictionary, null, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
            })
        })
    }
}