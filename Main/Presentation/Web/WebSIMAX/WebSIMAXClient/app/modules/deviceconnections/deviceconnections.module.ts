﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

require('./deviceconnections.scss')

import { DeviceConnectionsController } from './deviceconnections.controller'
import { Permissions } from '../../helpers/permissions'


var moduleName = 'simax.deviceconnections'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule, 'gridster'])

module.controller('DeviceConnectionsController', ['$scope', '$state', '$timeout', '$compile', 'api', 'logger', 'localDictionary', ($scope, $state: angular.ui.IStateService, $timeout, $compile: ng.ICompileService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new DeviceConnectionsController($scope, $state, $timeout, $compile, api, logger, localDictionary)])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.DEVICE_CONNECTION_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['DEVICECONNECTIONS'], 'deviceconnections', 'DeviceConnections', 'deviceconnections', 14)
        $stateProvider.state('deviceconnections', {
            url: '/deviceconnections',
            controller: 'DeviceConnectionsController',
            template: require('./deviceconnections.tpl.html'),
        })
    })
}])

export default moduleName