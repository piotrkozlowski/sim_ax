﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { DevicesDetailsDirectiveController } from './devices-details.directive.controller'

require('./devices-details.scss')

export class DevicesDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new DevicesDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./devices-details.tpl.html")
}