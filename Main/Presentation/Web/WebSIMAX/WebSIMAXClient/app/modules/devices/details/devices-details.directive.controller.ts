﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { DeviceModel, DeviceHierarchyModel, DevicesEditModel, SchemaModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { ConnectedObject } from '../../common/connected-object/connected-object.directive'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    parentDevices: any[]
    childDevices: any[]
}

interface DevicesDetailsScope extends DetailsScope {
    id: number
    editId: number

    common: CommonData

    // accordion
    parentDevices: GridData
    childDevices: GridData
    gridParams: GridData
    measurementsBridge: MeasurementsBridge

    connectedObject: ConnectedObject

    confirmDelete()

    submitParentDevices: (changed: any[]) => ng.IPromise<DeviceHierarchyModel[]>
    submitChildDevices: (changed: any[]) => ng.IPromise<DeviceHierarchyModel[]>

    showTabs: boolean

    schemas: SchemaModel[]
    loadSchemas: (id: number) => void
}

export class DevicesDetailsDirectiveController extends Details<DevicesDetailsScope> {
    private deviceId: number
    private permissions: any[]
    private referenceId: number

    private gridParamsGridPromise: ng.IPromise<void>

    constructor($scope: DevicesDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.DevicePopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'DeviceDetails'),
            openEventName: EventsNames.OPEN_DEVICES_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_DEVICES_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.DEVICE_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.device
        $scope.editId = $scope.$id
        $scope.common = <CommonData>{}
        $scope.common.parentDevices = []
        $scope.common.childDevices = []

        $scope.parentDevices = <GridData>{}
        $scope.childDevices = <GridData>{}
        $scope.gridParams = <GridData>{}

        $scope.connectedObject = {
            connectedObjectTitle: i18n('Common', 'CurrentMeter'),
            connectedObjectName: "",
            connectedObjectVisibility: false,
            connectedObjectId: 0,
            connectedObjectMethod: 'meter'
        }

        this.loadParamsGrid()

        $scope.$watchCollection("common.childDevices", value => {
            this.updateGridEntities("childDevices")
        })
        $scope.$watchCollection("common.parentDevices", value => {
            this.updateGridEntities("parentDevices")
        })

        jQuery.when.apply(jQuery,
            [Permissions.MEASUREMENTS_LIST, Permissions.DEVICE_HISTORY, Permissions.DEVICE_PARENT_DEVICES, Permissions.DEVICE_CHILD_DEVICES, Permissions.DEVICE_GENERAL_SCHEMA, Permissions.DEVICE_PARAMETERS, Permissions.DEVICE_EDIT, Permissions.DEVICE_EXTENDED_PARAMETERS, Permissions.DEVICE_MEASUREMENT]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteDevice'))
        }

        // popup open
        $scope.$on(EventsNames.OPEN_DEVICES_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        // saving parent and child devices
        $scope.submitChildDevices = (changed: any[]) => {
            var editModel = new DevicesEditModel()
            editModel.id = $scope.modelId
            editModel.devices = []
            changed.forEach(ch => {
                editModel.devices.push(<DeviceHierarchyModel>ch)
            })

            return api.device.saveChildDevices(editModel).then(result => {
                if (!result && !result.result)
                    CommonHelper.openInfoPopup(i18n('Popup', 'EditingError'), 'error', 5000)
                return result.result
            })
        }

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var device: DeviceModel = args[0]
            if (device.id == this.deviceId) {
                this.updateData(device)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_DEVICES_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.device.deleteDevice($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.DEVICE_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'DeviceDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'DeviceDeleteError'), 'error', 4000)
            })
        }

        $scope.loadSchemas = (id: number) => {
            this.loadSchemas(id)
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.schemas = null
        this.$scope.parentDevices = <GridData>{}
        this.$scope.childDevices = <GridData>{}
        this.$scope.requestsStates.getDevice = {}
        this.$scope.requestsStates.getChildDevices = {}
        this.$scope.requestsStates.getParentDevices = {}
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        var devicePromise = this.api.device.getDevice(this.$scope.modelId, this.$scope.requestsStates.getDevice).then(deviceResult => {
            this.deviceId = deviceResult ? deviceResult.id : null;
            this.referenceId = deviceResult.referenceId

            this.updateData(deviceResult)

            this.$scope.gridParams.showGrid = true
            this.$scope.popupTitle = deviceResult.name
        }).catch(reason => this.logger.log(reason))

        var childDevicesPromise = this.api.device.getChildDevices(this.$scope.modelId, this.$scope.requestsStates.getChildDevices).then(childResult => {
            this.$scope.common.childDevices = childResult.result
            this.insertEntityData(childResult.result, 'getDevicesHierarchyColumns', EventsNames.OPEN_DEVICES_DETAILS_POPUP, 'childDevices')
        }).catch(reason => this.logger.log(reason))

        this.api.device.getParentDevices(this.$scope.modelId, this.$scope.requestsStates.getParentDevices).then(parentResult => {
            this.$scope.common.parentDevices = parentResult.result
            this.insertEntityData(parentResult.result, 'getDevicesHierarchyColumns', EventsNames.OPEN_DEVICES_DETAILS_POPUP, 'parentDevices')
        }).catch(reason => this.logger.log(reason))

        this.$q.all([devicePromise, childDevicesPromise]).then(() => {
            this.buildBridges()
            this.$scope.showTabs = true
        })
    }

    updateData(result: DeviceModel) {
        this.localDictionary.getDeviceDisplayFields(result.referenceId).then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })

        this.localDictionary.getDeviceGridDisplayFields(result.referenceId).then(displayFields => {
            this.gridParamsGridPromise.then(() => {
                this.$scope.gridParams.grid.option("dataSource").store().clear()
                Details.filterFields(result.detailsFields, displayFields).forEach((field) => {
                    this.$scope.gridParams.grid.option("dataSource").store().insert({
                        description: field.caption,
                        index: field.index,
                        value: field.value,
                        typeId: field.typeId
                    })
                })
                this.$scope.gridParams.grid.refresh()
            })
        })

        if (result.currentMeterId != null) {
            this.$scope.connectedObject.connectedObjectName = result.currentMeterName
            this.$scope.connectedObject.connectedObjectVisibility = true
            this.$scope.connectedObject.connectedObjectId = result.currentMeterId
        }
        else {
            this.$scope.connectedObject.connectedObjectVisibility = false
        }
    }

    getTabs() {
        var [MEASUREMENTS_LIST, DEVICE_HISTORY, DEVICE_PARENT_DEVICES, DEVICE_CHILD_DEVICES, DEVICE_GENERAL_SCHEMA, DEVICE_PARAMETERS, DEVICE_EDIT, DEVICE_EXTENDED_PARAMETERS, DEVICE_MEASUREMENT] = this.permissions
        var tabs = []

        tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: 'common' })
        if (DEVICE_MEASUREMENT) tabs.push({ template: CommonHelper.getIconForDetails("MEASUREMENTS", '#000000', '18', i18n('Common', 'Measurements')), name: 'measurements' })
        if (DEVICE_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: 'history' })

        return tabs
    }

    getAccordions() {
        var [MEASUREMENTS_LIST, DEVICE_HISTORY, DEVICE_PARENT_DEVICES, DEVICE_CHILD_DEVICES, DEVICE_GENERAL_SCHEMA, DEVICE_PARAMETERS, DEVICE_EDIT, DEVICE_EXTENDED_PARAMETERS, DEVICE_MEASUREMENT] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (DEVICE_PARAMETERS || DEVICE_GENERAL_SCHEMA) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "BasicParameters"),
                canEdit: DEVICE_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (DEVICE_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_DEVICES_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId, 0)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.DeviceDetailsAccordions, 0, items))
        }

        if (DEVICE_EXTENDED_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "SpecificParameters"),
                canEdit: DEVICE_EDIT,
                name: 'specifics',
                showStatus: false,
                editMethod: () => {
                    if (DEVICE_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_DEVICES_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId, 1)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.DeviceDetailsAccordions, 1, items))
        }

        if (DEVICE_PARENT_DEVICES || DEVICE_CHILD_DEVICES) {
            var items: DetailsAccordionSection[] = []

            if (DEVICE_PARENT_DEVICES)
                items.push({
                    title: i18n("Common", "ParentDevices"),
                    canEdit: false,
                    name: 'parentDevices',
                    showStatus: true,
                    status: () => {
                        return i18n("Common", "InstalledCount") + (this.$scope.common.parentDevices ? this.$scope.common.parentDevices.length : 0)
                    },
                    visible: true
                })

            if (DEVICE_CHILD_DEVICES)
                items.push({
                    title: i18n("Common", "ChildDevices"),
                    canEdit: DEVICE_EDIT,
                    name: 'childDevices',
                    editMethod: () => {
                        if (DEVICE_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "childDevices")
                    },
                    showStatus: true,
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.childDevices ? this.$scope.common.childDevices.length : 0)
                    },
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.DeviceDetailsAccordions, 2, items))
        }

        return accordions
    }

    loadSchemas(id) {
        var [MEASUREMENTS_LIST, DEVICE_HISTORY, DEVICE_PARENT_DEVICES, DEVICE_CHILD_DEVICES, DEVICE_GENERAL_SCHEMA] = this.permissions

        if (DEVICE_GENERAL_SCHEMA) {
            this.api.device.getSchemas(id).then(result => {
                this.$scope.schemas = result.result
            })
        }
    }

    buildBridges() {
        if (this.$scope.common.childDevices == null) this.$scope.common.childDevices = []
        this.$scope.measurementsBridge = {
            id: this.$scope.modelId,
            latestDataMethodName: 'getLatestMeasurementsForDevice',
            dataMethodName: 'getMeasurementsForDevice',
            requestProperty: 'deviceIds',
            devices: [{
                id: this.$scope.modelId,
                name: this.$scope.popupTitle,
                referenceId: this.referenceId
            }].concat(this.$scope.common.childDevices.filter((device: DeviceHierarchyModel) => {
                return device.device && device.slotNbr != 0
            }).map((device: DeviceHierarchyModel) => {
                return {
                    id: device.deviceId,
                    name: device.device,
                    referenceId: device.referenceId
                }
            }))
        }
    }

    loadParamsGrid() {
        var defer = this.$q.defer<void>()

        var customOptions = {
            name: "DevicesDetails",
            columns: [
                {
                    dataField: "description",
                    caption: i18n("Common", "Description")
                },
                {
                    dataField: "index",
                    caption: i18n("Common", "Index")
                },
                {
                    dataField: "value",
                    caption: i18n("Common", "Value"),
                    dataType: "string",
                    calculateDisplayValue: (rowData) => {
                        return CommonHelper.cellFormatForDifferentValues(rowData)
                    }
                }
            ],
            onInitialized: e => {
                this.$scope.gridParams.grid = e.component
                defer.resolve()
            },
            onContentReady: e => {
                this.$scope.gridParams.showGrid = true
            }
        }

        this.$scope.gridParams.options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, this.$scope, this.logger, null, customOptions)
        this.$scope.gridParams.options.columnAutoWidth = true

        this.gridParamsGridPromise = defer.promise
    }
}