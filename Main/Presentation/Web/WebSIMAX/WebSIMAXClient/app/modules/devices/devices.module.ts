﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { DevicesCreateEditDirective } from './create-edit/devices-create-edit.directive'
import { DevicesDetailsDirective } from './details/devices-details.directive'
import { i18n } from '../common/i18n/common.i18n'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

import { DevicesController } from './devices.controller'

var moduleName = 'simax.devices'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('DevicesController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new DevicesController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('devicesDetails', [() => new DevicesDetailsDirective()])
module.directive('devicesCreateEdit', [() => new DevicesCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.DEVICE_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['DEVICES'], 'devices', 'Devices', 'devices', 3)
        $stateProvider.state('devices', {
            url: '/devices',
            controller: 'DevicesController',
            template: require('./devices.tpl.html')
        })
    })
}])

export default moduleName