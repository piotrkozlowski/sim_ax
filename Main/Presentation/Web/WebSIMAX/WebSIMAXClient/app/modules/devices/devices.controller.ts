﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { Permissions } from '../../helpers/permissions'
import { BaseGridGridsterController, ScopeBaseGridsterGrid } from '../common/common.base.grid.gridster.controller'
import { ImportOptions } from '../common/import/import.directive'
import { i18n } from '../common/i18n/common.i18n'

interface DevicesScope extends ScopeBaseGridsterGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
}

export class DevicesController extends BaseGridGridsterController {
    constructor($scope: DevicesScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api, {
                ids: [],
                measurmentIdField: "serialNumber",
                measurmentIdsField: "deviceIds",
                methodForDisplayFields: "getDeviceDisplayFields",
                methodForMeasurments: "getMeasurementsForLocation",
                methodForMeasurmentsFields: "getMeasurementsForDeviceFields",
                methodForPreviewFields: "getDevicePreviewFields",
                methodName: "getDevice",
                serviceName: "device",
                filterMethodName: "getDevices"
            }, 'DEVICES')        

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "devices",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_DEVICES_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_DEVICES_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onExportClick: () => {
                this.exportXls('devices')
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['"+Permissions.DEVICE_EDIT+"']",
                import: "['"+Permissions.DEVICE_IMPORT+"']",
                add: "['" + Permissions.DEVICE_ADD + "']",
                export: "['" + Permissions.DEVICE_EXPORT + "']"
            }
        })
 
        $scope.buttonsStates = buttonsStates
        this.addButtons(buttonsOptions)
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.DEVICE_ADD, Permissions.DEVICE_EDIT, Permissions.DEVICE_EXPORT, Permissions.DEVICE_IMPORT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                this.setSettingsListOptions(permissions, EventsNames.OPEN_TASKS_CREATE_EDIT_POPUP, 'devices', this.showPreviewDetails)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.getValueFromLocalStorage('deviceSaved')
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('deviceSaved')
            }
        })

        $scope.$on(EventsNames.DEVICE_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getDeviceEditFields',
            popupName: i18n('Popup', 'ImportDevices'),
            requestMethodType: 'device'
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_DEVICES_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }

        $scope.getItems = () => {
            return jQuery.when.apply(jQuery,
                [Permissions.LOCATION_LIST, Permissions.METER_LIST, Permissions.TASK_LIST, Permissions.ISSUE_LIST, Permissions.SIM_CARD_LIST, Permissions.ACTION_LIST]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [LOCATION_LIST, METER_LIST, TASK_LIST, ISSUE_LIST, SIM_CARD_LIST, ACTION_LIST] = permissions
                var items = []
                if (LOCATION_LIST) items.push({
                    template: CommonHelper.getIconForDetails("LOCATIONS", '#000000', '18', i18n('Grid', 'ShowLocations'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdLocation) }
                })
                if (METER_LIST) items.push({
                    template: CommonHelper.getIconForDetails("METERS", '#000000', '18', i18n('Grid', 'ShowMeters'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdMeter) }
                })
                if (TASK_LIST) items.push({
                    template: CommonHelper.getIconForDetails("TASKS", '#000000', '18', i18n('Grid', 'ShowTasks'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdTask) }
                })
                if (ISSUE_LIST) items.push({
                    template: CommonHelper.getIconForDetails("ISSUES", '#000000', '18', i18n('Grid', 'ShowIssues'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdIssue) }
                })
                if (SIM_CARD_LIST) items.push({
                    template: CommonHelper.getIconForDetails("SIM_CARD", '#000000', '18', i18n('Grid', 'ShowSimCards'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdSimCard) }
                })
                if (ACTION_LIST) items.push({
                    template: CommonHelper.getIconForDetails("RECENT_ACTIONS", '#000000', '18', i18n('Grid', 'ShowRecentActions'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdAction) }
                })
                return items
            })
        }

    }

    getDevices = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.device.getDevices(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getDevicesColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getDevicesColumns().then(columns => {
            this.localDictionary.getDevicesDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "DevicesGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onDataReady: this.onDataReady,
                    onSelectionChanged: this.onSelectionChanged,
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getDevices, customOptions, true, this.localDictionary, null, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
            })
        })
    }
}

