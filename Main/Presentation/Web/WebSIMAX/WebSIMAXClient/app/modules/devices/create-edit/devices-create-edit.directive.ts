﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

require('./devices-create-edit.scss')

interface DevicesCreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class DevicesCreateEditDirective implements ng.IDirective {
    constructor() {
        
    }

    restrict = 'E'
    scope = {
        id: "="
    }

    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: DevicesCreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Common','Apply'),
            type: PopupType.DEVICE_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitDevice(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean, referenceId: number) => {
            var sections = []
            sections.push($q.all([localDictionary.getDeviceEditFields(referenceId), localDictionary.getDeviceRequiredFields(referenceId)]).then(([columnsEdit, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }))
            sections.push(localDictionary.getDeviceGridDisplayFields(referenceId).then(fields => {
                return {
                    name: i18n("Common", "Specifics"),
                    type: "grid",
                    columns: fields
                }
            }))
            return $q.all(sections).then(dict => {
                $scope.dictionary = dict
            })
        }

        $scope.$on(EventsNames.OPEN_DEVICES_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            if (args && args[0] != null) {
                $scope.options.title = i18n('Popup', 'DeviceEdit')
                if (typeof args[0] == "number") {
                    api.device.getDeviceToEdit(args[0]).then(result => {
                        loadColumns(args && args[0], result.referenceId).then(() => {
                            $scope.editedItem = result.fields
                            $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0], args[1])
                        })
                    })
                    return
                } else {
                    $scope.editedItem = args[0]
                }
            } else {
                $scope.editedItem = null
                $scope.options.title = i18n('Popup', 'DeviceAdd')
            }
            loadColumns(args && args[0], 0).then(() => {
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[1])
            })
        })

        var submitDevice = (fields, id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.device.createDevice(formModel).then(device => {
                if (!device) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'DeviceSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'DeviceSaved'))
                    CommonHelper.setValueToLocalStorage('deviceSaved', true)
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, device)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
    }]
    template = require("./devices-create-edit.tpl.html")
    transclude = true
}