﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { AlarmsDetailsDirective } from './details/alarms-details.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

require('./alarms.scss')

import { AlarmsController } from './alarms.controller'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.alarms'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule, 'gridster'])

module.controller('AlarmsController', ['$scope', '$state', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $state: ng.ui.IStateService, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new AlarmsController($scope, $state, $compile, $timeout, api, logger, localDictionary)])

module.directive('alarmsDetails', [() => new AlarmsDetailsDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.ALARM_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['ALARMS'], 'alarms', 'Alarms', 'alarms', 9)
        $stateProvider.state('alarms', {
            url: '/alarms',
            controller: 'AlarmsController',
            template: require('./alarms.tpl.html'),
            params: { ids: null }
        })
    })
}])

export default moduleName