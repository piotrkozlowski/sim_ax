﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'

interface AlarmsScope extends ScopeBaseGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
}

export class AlarmsController extends BaseGridController {
    constructor($scope: AlarmsScope, private $state: angular.ui.IStateService, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "alarms",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                add: "['" + Permissions.ALARM_ADD + "']",
                edit: "['" + Permissions.ALARM_EDIT + "']",
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.ALARM_ADD, Permissions.ALARM_EDIT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                permissions[2] = true//export permissions not implemented
                permissions[3] = true//import permissions not implemented
                this.setSettingsListOptions(permissions, EventsNames.OPEN_ALARMS_CREATE_EDIT_POPUP)
            })
        }, 0)

        $scope.importOptions = {
            dictionaryMethodName: 'getActorEditFields',
            popupName: i18n('Popup', 'ImportActors'),
            requestMethodType: 'actor'
        }

        $scope.getItems = () => {
            return jQuery.when.apply(jQuery,
                [Permissions.ALARMS_CONFIRMATION_ENABLED]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [ALARMS_CONFIRMATION_ENABLED] = permissions
                var items = []
                if (ALARMS_CONFIRMATION_ENABLED) items.push({
                    template: CommonHelper.getIconForDetails("ALARM_DONE", '#000000', '18', i18n('Common', 'Submit'), 'context-menu-row'), onItemClick: () => {
                        var selectedRows: any[] = this.$scope.dataGrid.getSelectedRowsData()
                        var ids = selectedRows.map(r => r.id)
                        api.alarm.confirmAlarms(ids).then(result => {
                            if (ids.length == 1) CommonHelper.openInfoPopup(i18n('Common', 'AlarmApproved'))
                            else CommonHelper.openInfoPopup(i18n('Common', 'AlarmsApproved'))
                            $scope.dataGrid.refresh()
                        })
                    }
                })
                return items
            })
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_ALARMS_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }

        $scope.$on(EventsNames.ALARM_APPROVED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })
    }

    getAlarms = (request) => {
        if (this.$state.params['ids'] && this.$state.params['ids'].length > 1) {
            request.ids = this.$state.params['ids']
        }

        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.alarm.getAlarms(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getAlarmsColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getAlarmsColumns().then(columns => {
            this.localDictionary.getAlarmsDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "AlarmsGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }
                var dataGridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getAlarms, customOptions, true, this.localDictionary, null, this.$compile)
                this.$scope.dataGridOptions = dataGridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = dataGridOptions
            })
        })
    }
}



