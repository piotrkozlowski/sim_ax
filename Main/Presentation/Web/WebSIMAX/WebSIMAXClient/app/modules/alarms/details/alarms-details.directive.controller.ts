﻿import 'angular'

import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ModelAlarm } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { GridSelectBridge } from '../../common/grid-select/grid-select.directive'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
}

interface AlarmsDetailsScope extends DetailsScope {
    id: number
    itemObjects: any[]
    entityName: string
    visiblePopup: boolean
    modelId: number

    popupTitle: string
    common: CommonData

    gridParams: GridData

    mapData: any[]

    showTabs: boolean

    confirmDelete()
    editButtonOptions: DevExpress.ui.dxButtonOptions
}

export class AlarmsDetailsDirectiveController extends Details<AlarmsDetailsScope> {
    private permissions: any[]

    constructor($scope: AlarmsDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.AlarmPopupTabActive)

        var actions: DevExpress.ui.dxButtonOptions[];

        jQuery.when.apply(jQuery,
            [Permissions.ALARMS_CONFIRMATION_ENABLED]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            var [ALARMS_CONFIRMATION_ENABLED] = permissions
            if (ALARMS_CONFIRMATION_ENABLED) {
                actions = [{
                    template: CommonHelper.getIconWithText(i18n('Common', 'Submit'), 'ALARM_DONE'),
                    onClick: (e) => {
                        api.alarm.confirmAlarms([$scope.modelId]).then(result => {
                            CommonHelper.openInfoPopup(i18n('Common', 'AlarmApproved'))
                            this.loadData($scope.modelId, $scope.itemObjects)
                            $scope.$root.$broadcast(EventsNames.ALARM_APPROVED)
                        })
                    },
                    width: '100%'
                }]
            }
        })

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'AlarmDetails'),
            openEventName: EventsNames.OPEN_ALARMS_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_ALARMS_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.ALARM_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) },
            actions: actions
        }
        $scope.mapData = []

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.alarm
        $scope.common = <CommonData>{}

        jQuery.when.apply(jQuery,
            [Permissions.ALARM_PARAMETERS, Permissions.ALARM_HISTORY]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.$on(EventsNames.OPEN_ALARMS_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_ALARMS_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        $scope.$on(EventsNames.CLOSE_MAP, (event: ng.IAngularEvent, ...args: any[]) => {
            //todo:
            this.hideAccordion($scope, "maps", 1)
        })

        $scope.confirmDelete = () => {
            // TODO
            alert('deletion not implemented')
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getAlarm = {}
    }

    loadData(id: number, objects: any[]) {
        this.resetData()
        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.alarm.getAlarm(this.$scope.modelId, this.$scope.requestsStates.getAlarm).then(result => {
            
            this.$scope.popupTitle = result.name
            this.$scope.showTabs = true

            this.updateData(result)

        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: ModelAlarm) {
        this.localDictionary.getAlarmDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })

        this.$scope.mapData = []
        this.$scope.mapData.push(result)
    }

    getTabs() {
        var [ALARM_PARAMETERS, ALARM_HISTORY] = this.permissions

        var tabs = []

        if (ALARM_PARAMETERS) tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: 'common' })
        if (ALARM_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: 'history' })

        return tabs
    }

    getAccordions() {
        var [ALARM_PARAMETERS, ALARM_HISTORY] = this.permissions
        var accordions: DetailsAccordion[] = []

        var items: DetailsAccordionSection[] = []
        items.push({
            title: i18n("Common", "Parameters"),
            canEdit: false,
            name: 'params',
            showStatus: false,
            visible: true
        })
        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.AlarmAccordionsSettings, 0, items))

        var items: DetailsAccordionSection[] = []
        items.push({
            title: i18n("Common", "Map"),
            canEdit: false,
            name: 'map',
            showStatus: false,
            visible: true
        })
        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.AlarmAccordionsSettings, 1, items))

        return accordions
    }
}