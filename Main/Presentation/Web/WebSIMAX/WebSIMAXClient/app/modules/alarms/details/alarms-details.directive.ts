﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { AlarmsDetailsDirectiveController } from './alarms-details.directive.controller'

require('./alarms-details.scss')

export class AlarmsDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary',
        ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
            return new AlarmsDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
        }
    ]
    template = require("./alarms-details.tpl.html")
}