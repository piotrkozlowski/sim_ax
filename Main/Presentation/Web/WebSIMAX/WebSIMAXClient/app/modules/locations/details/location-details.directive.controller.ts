﻿import 'angular'
import { LocationModel, GroupModel, LocationFormValuesModel, SchemaModel, MeasurementModel } from '../../api/api.models'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { MeasurementsChartBridge } from '../../common/measurements/chart/chart.directive.controller'
import { GroupsBridge } from '../../common/groups-edit/groups-edit.directive'
import { Permissions } from '../../../helpers/permissions'
import { CommonHelper } from '../../../helpers/common-helper'
import { RequestManager } from '../../../helpers/request-manager'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    groups: GroupModel[]
    devices: any[]
    meters: any[]
}

interface LocationDetailsScope extends DetailsScope {
    id: number
    editId: number

    itemObjects: any[]

    visibleMap: boolean

    popupTitle: string

    common: CommonData

    // accordion
    groups: GridData
    devices: GridData
    meters: GridData
    specifics: GridData
    groupsBridge: GroupsBridge
    devicesBridge: GridBridge
    metersBridge: GridBridge

    confirmDelete()

    showTabs: boolean

    schemas: SchemaModel[]
    loadSchemas: (id: number) => void

    measurementsBridge: MeasurementsBridge
    measurementsChartBridge: MeasurementsChartBridge
    measurementsGridOptions: DevExpress.ui.dxDataGridOptions
    measurementsShow: () => void
    measurementsChartShow: () => void
    needFakeMeasurements: boolean

    mapData: any[]
}

export class LocationDetailsDirectiveController extends Details<LocationDetailsScope> {
    private referenceId: number
    private permissions: any[]

    private measurementShowed = false

    private requestManager = RequestManager.getInstance()

    private specificsGridPromise: ng.IPromise<void>
    private measurementsData: MeasurementModel[]

    constructor($scope: LocationDetailsScope, $q: ng.IQService, $element: JQuery, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.LocationPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'LocationDetails'),
            openEventName: EventsNames.OPEN_LOCATION_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_LOCATION_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.LOCATION_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }
        this.$scope.mapData = []
        this.accordionsNameToShow = 'common'        

        $scope.popupFor = CommonHelper.location
        $scope.common = <CommonData>{}

        $scope.editId = $scope.$id

        $scope.groups = <GridData>{}
        $scope.devices = <GridData>{}
        $scope.meters = <GridData>{}
        $scope.specifics = <GridData>{}
        
        $scope.showDetails = (e) => {
            var object = {
                id: e.data.id,
                name: e.data.name
            }
            $scope.$broadcast(EventsNames.OPEN_LOCATION_DETAILS_POPUP + $scope.detailsPopupId, e.data.id, [object])
        }

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteLocation'))
        }

        this.loadSpecifics()

        jQuery.when.apply(jQuery,
            [Permissions.LOCATION_PARAMETERS, Permissions.MEASUREMENTS_LIST, Permissions.LOCATION_HISTORY, Permissions.LOCATION_GROUPS, Permissions.LOCATION_INSTALLED_DEVICES, Permissions.LOCATION_INSTALLED_METERS, Permissions.LOCATION_SCHEMA, Permissions.LOCATION_GENERAL_SCHEMA, Permissions.LOCATION_EXTENDED_PARAMETERS, Permissions.LOCATION_MAP, Permissions.LOCATION_EDIT, Permissions.LOCATION_MEASUREMENT, Permissions.LOCATION_MEASUREMENT_PANEL]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs(false))
        })

        $scope.$on(EventsNames.OPEN_LOCATION_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var location: LocationModel = args[0]
            if (location.id == $scope.modelId) {
                this.updateData(location)
            }
        })

        this.createEventEditForGridEntities(EventsNames.CLOSE_GROUPS_EDIT_POPUP + $scope.detailsPopupId, 'groups')
        this.createEventEditForGridEntities(EventsNames.CLOSE_GRID_EDIT_POPUP + $scope.detailsPopupId + "devices", 'devices')
        this.createEventEditForGridEntities(EventsNames.CLOSE_GRID_EDIT_POPUP + $scope.detailsPopupId + "meters", 'meters')

        // close
        $scope.$on(EventsNames.CLOSE_LOCATION_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        $scope.$on(EventsNames.CLOSE_MAP, (event: ng.IAngularEvent, ...args: any[]) => {
            this.hideAccordion($scope, "maps", 2)
        })

        // delete
        $scope.confirmDelete = () => {
            api.location.deleteLocation($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.LOCATION_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationDeleteError'), 'error', 4000)
            })
        }

        $scope.measurementsShow = () => {
            $scope.needFakeMeasurements = false
            this.measurementShowed = true
        }

        $scope.measurementsChartShow = () => {
            $timeout(() => {
                if (!this.measurementShowed) {
                    // We need fake measurements
                    $scope.needFakeMeasurements = true
                } else {
                    // We don't need fake measurements
                    $scope.needFakeMeasurements = false
                }
            })
        }

        $scope.loadSchemas = (id: number) => {
            this.loadSchemas(id)
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.requestsStates.getLocation = {}
        this.$scope.requestsStates.getSchemas = {}
        this.$scope.showTabs = false
        this.$scope.schemas = null
        this.$scope.devices.showGrid = false
        this.$scope.groups.showGrid = false
        this.$scope.meters.showGrid = false
        this.$scope.specifics.showGrid = false
        this.measurementsData = null
        this.measurementShowed = false
        this.$scope.measurementsChartBridge = null
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.location.getLocation(id, this.$scope.requestsStates.getLocation).then(result => {
            this.$scope.mapData = []
            this.$scope.mapData.push(result)


            this.$scope.popupTitle = result.name
            this.referenceId = result.referenceId

            this.updateData(result)

            this.$scope.groups.showGrid = true
        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: LocationModel) {
        this.loadGroups().then(() => {
            if (result) {
                this.$scope.groups.options.dataSource.store().clear()
                result.groups.forEach(element => {
                    this.$scope.groups.options.dataSource.store().insert(element)
                })
                this.$scope.groups.grid.refresh()
            }
        })

        var devices = result && result.devices ? result.devices.map(device => CommonHelper.objectFromEntityModel(device)) : []
        var meters = result && result.meters ? result.meters.map(meter => CommonHelper.objectFromEntityModel(meter)) : []

        this.insertEntityData(devices, 'getLocationDeviceDefaultColumns', EventsNames.OPEN_DEVICES_DETAILS_POPUP, 'devices', null)
        this.insertEntityData(meters, 'getLocationMeterDefaultColumns', EventsNames.OPEN_METER_DETAILS_POPUP, 'meters', null)

        this.localDictionary.getLocationDisplayFields(result.referenceId).then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
            this.$scope.common.groups = result ? result.groups : []
            this.$scope.common.devices = devices
            this.$scope.common.meters = meters

            this.$scope.showTabs = true

            this.localDictionary.getLocationGridDisplayFields(result.referenceId).then(gridDisplayFields => {
                this.specificsGridPromise.then(() => {
                    this.$scope.specifics.grid.option("dataSource").store().clear()
                    Details.filterFields(result.detailsFields, displayFields).forEach((field) => {
                        this.$scope.specifics.grid.option("dataSource").store().insert({
                            description: field.caption,
                            index: field.index,
                            value: field.value,
                            typeId: field.typeId
                        })
                    })
                    this.$scope.specifics.grid.refresh()
                    this.$scope.specifics.showGrid = true
                })
            })

            this.updateBridges()
        })
    }

    getTabs(addSchemaAccordion: boolean = false) {
        var [LOCATION_PARAMETERS, MEASUREMENTS_LIST, LOCATION_HISTORY, LOCATION_GROUPS, LOCATION_INSTALLED_DEVICES, LOCATION_INSTALLED_METERS, LOCATION_SCHEMA, LOCATION_GENERAL_SCHEMA, LOCATION_EXTENDED_PARAMETERS, LOCATION_MAP, LOCATION_EDIT, LOCATION_MEASUREMENT, LOCATION_MEASUREMENT_PANEL] = this.permissions
        var tabs = []

        if ((LOCATION_PARAMETERS || LOCATION_GROUPS || LOCATION_INSTALLED_DEVICES || LOCATION_INSTALLED_METERS) || LOCATION_MEASUREMENT || LOCATION_GENERAL_SCHEMA && addSchemaAccordion) {
            tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: 'common' })
        }
        if (LOCATION_MEASUREMENT_PANEL) tabs.push({ template: CommonHelper.getIconForDetails("MEASUREMENTS", '#000000', '18', i18n('Common', 'Measurements')), name: 'measurements' })
        if (LOCATION_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: 'history' })
        if (LOCATION_SCHEMA) tabs.push({ template: CommonHelper.getIconForDetails("SCHEMA", '#000000', '18', i18n('Common', 'Schema')), name: 'schema' })

        return tabs
    }

    getAccordions() {
        var [LOCATION_PARAMETERS, MEASUREMENTS_LIST, LOCATION_HISTORY, LOCATION_GROUPS, LOCATION_INSTALLED_DEVICES, LOCATION_INSTALLED_METERS, LOCATION_SCHEMA, LOCATION_GENERAL_SCHEMA, LOCATION_EXTENDED_PARAMETERS, LOCATION_MAP, LOCATION_EDIT, LOCATION_MEASUREMENT, LOCATION_MEASUREMENT_PANEL] = this.permissions

        var accordions: DetailsAccordion[] = []
        var firstAccordion: DetailsAccordion = null

        if (LOCATION_PARAMETERS || LOCATION_SCHEMA || LOCATION_MEASUREMENT) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: LOCATION_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (LOCATION_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_LOCATION_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            if (LOCATION_MEASUREMENT)
                items.push({
                    title: i18n("Common", "Measurements"),
                    canEdit: false,
                    name: 'measurements',
                    showStatus: false,
                    visible: true
                })

            firstAccordion = new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.LocationAccordionsSettings, 0, items, true, (component: DevExpress.ui.dxAccordion) => {
                this.$timeout(() => {
                    // TODO: expand all
                    //component.expandItem(1)
                })
            })
            firstAccordion.needRefresh = () => {
                this.refreshDetails()
            }
            accordions.push(firstAccordion)
        }

        if (LOCATION_GROUPS || LOCATION_INSTALLED_DEVICES || LOCATION_INSTALLED_METERS || LOCATION_MEASUREMENT) {
            var items: DetailsAccordionSection[] = []

            if (LOCATION_GROUPS)
                items.push({
                    title: i18n("Common", "Groups"),
                    canEdit: LOCATION_EDIT,
                    name: 'groups',
                    showStatus: true,
                    editMethod: () => {
                        if (LOCATION_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GROUPS_EDIT_POPUP + this.$scope.detailsPopupId)
                    },
                    status: () => {
                        return i18n('Common', 'AssignedCount') + (this.$scope.common.groups ? this.$scope.common.groups.length : 0)
                    },
                    visible: true
                })

            if (LOCATION_INSTALLED_DEVICES)
                items.push({
                    title: i18n("Common", "Devices"),
                    canEdit: LOCATION_EDIT,
                    name: 'devices',
                    showStatus: true,
                    editMethod: () => {
                        if (LOCATION_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "devices", "locationDevices")
                    },
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.devices ? this.$scope.common.devices.length : 0)
                    },
                    visible: true
                })

            if (LOCATION_INSTALLED_METERS)
                items.push({
                    title: i18n("Common", "Meters"),
                    canEdit: LOCATION_EDIT,
                    name: 'meters',
                    showStatus: true,
                    editMethod: () => {
                        if (LOCATION_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + "meters", "locationMeters")
                    },
                    status: () => {
                        return i18n('Common', 'InstalledCount') + (this.$scope.common.meters ? this.$scope.common.meters.length : 0)
                    },
                    visible: true
                })

            if (LOCATION_MEASUREMENT)
                items.push({
                    title: i18n("Common", "MeasurmentsDetails"),
                    canEdit: false,
                    name: 'measurements-table',
                    showStatus: false,
                    visible: true
                })

            var accordion = new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.LocationAccordionsSettings, 1, items)
            accordion.needRefresh = () => {
                this.refreshDetails()
            }
            accordions.push(accordion)
        }

        if (LOCATION_MAP || LOCATION_EXTENDED_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            if (LOCATION_MAP)
                items.push({
                    title: i18n("Common", "Map"),
                    canEdit: false,
                    name: 'maps',
                    showStatus: false,
                    visible: true
                })

            if (LOCATION_EXTENDED_PARAMETERS)
                items.push({
                    title: i18n("Common", "SpecificParameters"),
                    canEdit: false,
                    name: 'specifics',
                    showStatus: false,
                    visible: true
                })

            var accordion = new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.LocationAccordionsSettings, 2, items)
            accordion.needRefresh = () => {
                this.refreshDetails()
            }
            accordions.push(accordion)
        }

        return accordions
    }

    refreshDetails() {
        this.mapUpdate()
        if (this.$scope.measurementsChartBridge)
            this.$scope.measurementsChartBridge.updateChart = true
    }

    updateBridges() {
        this.$scope.groupsBridge = {
            id: this.$scope.modelId,
            assigned: this.$scope.common.groups,
            popupId: this.$scope.detailsPopupId,
            onSubmit: (added, removed) => {
                var formModel = new LocationFormValuesModel()
                formModel.id = this.$scope.modelId
                formModel.groups = this.$scope.common.groups.map(g => g.id).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }

        this.$scope.devicesBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'DevicesEdit') + " (" + this.$scope.common.devices.length + ")",
            items: this.$scope.common.devices,
            popupId: this.$scope.detailsPopupId + 'devices',
            dictionaryMethodName: 'getLocationDeviceDefaultColumns',
            dataMethodName: 'getDevices',
            dataScope: 'device',
            onSubmit: (added, removed) => {
                var formModel = new LocationFormValuesModel()
                added = added.map(d => d[CommonHelper.idField])
                removed = removed.map(d => d[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.devices = this.$scope.common.devices.map(d => d[CommonHelper.idField]).filter(serial => removed.indexOf(serial) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }

        this.$scope.metersBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'MetersEdit') + " (" + this.$scope.common.meters.length + ")",
            items: this.$scope.common.meters,
            popupId: this.$scope.detailsPopupId + 'meters',
            dictionaryMethodName: 'getLocationMeterDefaultColumns',
            dataMethodName: 'getMeters',
            dataScope: 'meter',
            onSubmit: (added, removed) => {
                var formModel = new LocationFormValuesModel()
                added = added.map(m => m[CommonHelper.idField])
                removed = removed.map(m => m[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.meters = this.$scope.common.meters.map(m => m[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1).concat(added)
                return this.saveFormModel(formModel)
            }
        }

        this.$scope.measurementsBridge = {
            id: this.$scope.modelId,
            latestDataMethodName: 'getLatestMeasurementsForLocation',
            dataMethodName: 'getMeasurementsForLocation',
            requestProperty: 'locationIds',
            devices: this.$scope.common.devices.map(d => {
                return d
            }),
            meters: this.$scope.common.meters.map(m => {
                return m
            }),
            groups: [{
                id: this.$scope.modelId,
                referenceId: this.referenceId,
                name: this.$scope.popupTitle
            }],
            data: () => {
                return this.measurementsData
            },
            dataCallback: data => {
                this.measurementsData = data
            },
            chartBridgeCallback: chartBridge => {
                this.$scope.measurementsChartBridge = chartBridge
            }
        }
    }

    saveFormModel(formModel: LocationFormValuesModel) {
        var defer = this.$q.defer()
        this.api.location.saveLocation(formModel).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaveError'), 'error', 5000)
                defer.reject()
            } else {
                CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaved'))
                this.updateData(response)
                defer.resolve(true)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'LocationSaveError'), 'error', 5000)
            defer.reject()
        })
        return defer.promise;
    }

    loadGroups() {
        var defer = this.$q.defer<void>()

        if (this.$scope.groups.grid) {
            defer.resolve()
        } else {
            var customGroupsGridOptions = {
                name: "LocationDetails",
                onInitialized: e => {
                    this.$scope.groups.grid = e.component
                    defer.resolve()
                },
                columns: [
                    {
                        dataField: "name",
                        caption: i18n('Popup', 'AssignedGroups'),
                        selectedFilterOperation: 'contains'
                    }, {
                        dataField: "id",
                        visible: false
                    }
                ]
            }
            this.$scope.groups.options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, this.$scope, this.logger, null, customGroupsGridOptions)
        }

        return defer.promise
    }

    loadSpecifics() {
        var defer = this.$q.defer<void>()
        var customSpecificsGridOptions = {
            name: "Specifics",
            columns: [
                {
                    dataField: "description",
                    caption: i18n("Common", "Description")
                },
                {
                    dataField: "index",
                    caption: i18n("Common", "Index")
                },
                {
                    dataField: "value",
                    caption: i18n("Common", "Value"),
                    dataType: "string",
                    calculateDisplayValue: (rowData) => {
                        return CommonHelper.cellFormatForDifferentValues(rowData)
                    }
                }
            ],
            onInitialized: e => {
                this.$scope.specifics.grid = e.component
                defer.resolve()
            }
        }
        this.$scope.specifics.options = new CommonExtendedDataGridOptions(CommonHelper.smallGrid, this.$scope, this.logger, null, customSpecificsGridOptions)
        this.specificsGridPromise = defer.promise
    }

    loadSchemas(id) {
        var [LOCATION_PARAMETERS, MEASUREMENTS_LIST, LOCATION_HISTORY, LOCATION_GROUPS, LOCATION_INSTALLED_DEVICES, LOCATION_INSTALLED_METERS, LOCATION_SCHEMA, LOCATION_GENERAL_SCHEMA, LOCATION_EXTENDED_PARAMETERS, LOCATION_MAP, LOCATION_EDIT] = this.permissions

        if (LOCATION_GENERAL_SCHEMA) {
            if (this.requestManager.requestStates.getSchemasDetails && this.requestManager.requestStates.getSchemasDetails.request && !this.requestManager.requestStates.getSchemasDetails.request.$resolved) {
                this.requestManager.requestStates.getSchemasDetails.request.$cancelRequest()
                this.requestManager.requestStates.getSchemasDetails.cancelled = true
            }
            if (!this.requestManager.requestStates.getSchemasDetails)
                this.requestManager.requestStates.getSchemasDetails = {}
            this.api.location.getSchemas(id, this.requestManager.requestStates.getSchemasDetails).then(response => {
                this.$scope.schemas = response.result
                // update tabs
                if (response.result.length > 0) {
                    if (!(LOCATION_PARAMETERS || LOCATION_GROUPS || LOCATION_INSTALLED_DEVICES || LOCATION_INSTALLED_METERS || LOCATION_EXTENDED_PARAMETERS || LOCATION_MAP)) {
                        this.updateTabsPanel(this.getTabs(true))
                    }
                }
            })
        }
    }
}