﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { LocationDetailsDirective } from './details/location-details.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

require('./locations.scss')

import { LocationsController } from './locations.controller'
import { LocationCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { GroupsEditDirective } from '../common/groups-edit/groups-edit.directive'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.locations'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule, 'gridster'])

module.controller('LocationsController', ['$scope', '$state', '$timeout', '$compile', 'api', 'logger', 'localDictionary', ($scope, $state: angular.ui.IStateService, $timeout, $compile: ng.ICompileService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new LocationsController($scope, $state, $timeout, $compile, api, logger, localDictionary)])

module.directive('locationCreateEdit', [() => new LocationCreateEditDirective()])
module.directive('locationDetails', [() => new LocationDetailsDirective()])
module.directive('groupsEdit', [() => new GroupsEditDirective()])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.LOCATION_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['LOCATIONS'], 'locations', 'Locations', 'locations', 2)
        $stateProvider.state('locations', {
            url: '/locations',
            controller: 'LocationsController',
            template: require('./locations.tpl.html'),
            params: { ids: null }
        })
    })
}])

export default moduleName