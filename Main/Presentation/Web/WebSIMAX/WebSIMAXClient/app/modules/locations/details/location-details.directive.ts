﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { LocationDetailsDirectiveController } from './location-details.directive.controller'

require('./location-details.scss')

export class LocationDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }

    controller = ['$scope', '$q', '$element', '$timeout', 'api', 'logger', 'localDictionary',
        ($scope, $q: ng.IQService, $element: JQuery, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
            return new LocationDetailsDirectiveController($scope, $q, $element, $timeout, api, logger, localDictionary)
        }
    ]

    template = require("./location-details.tpl.html")
}