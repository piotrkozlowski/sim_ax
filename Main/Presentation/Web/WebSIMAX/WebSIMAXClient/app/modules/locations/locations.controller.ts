﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { ContextListModel, GridRequestReferenceModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper'
import { CommonGridButtonsOptions, ButtonConfig } from '../common/common.grid-buttons-options'
import { i18n } from '../common/i18n/common.i18n'
import { Permissions } from '../../helpers/permissions'
import { BaseGridGridsterController, ScopeBaseGridsterGrid } from '../common/common.base.grid.gridster.controller'


interface LocationsScope extends ScopeBaseGridsterGrid {
    buttonsOptions: ButtonConfig[]
}

export class LocationsController extends BaseGridGridsterController {
    constructor($scope: LocationsScope, private $state: angular.ui.IStateService, $timeout: ng.ITimeoutService, protected $compile: ng.ICompileService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            // fake grid...
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api,
            {
                ids: [],
                measurmentIdField: "locationId",
                measurmentIdsField: "locationIds",
                methodForDisplayFields: "getLocationDisplayFields",
                methodForMeasurments: "getMeasurementsForLocation",
                methodForMeasurmentsFields: "getMeasurementsForLocationFields",
                methodForPreviewFields: "getLocationPreviewFields",
                methodName: "getLocation",
                serviceName: "location",
                schemaMethodName: "getSchemas",
                filterMethodName: "getLocations"
            }, 'LOCATIONS', true)

        $scope.importOptions = {
            dictionaryMethodName: 'getLocationEditFields',
            popupName: i18n('Popup', 'ImportLocations'),
            requestMethodType: 'location'
        }
   

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "locations",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_LOCATION_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_LOCATION_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onExportClick: () => {
                this.exportXls('locations')
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['" + Permissions.LOCATION_EDIT + "']",
                import: "['" + Permissions.LOCATION_IMPORT + "']",
                add: "['" + Permissions.LOCATION_ADD + "']",
                export: "['" + Permissions.LOCATION_EXPORT + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        this.addButtons(buttonsOptions)
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.LOCATION_ADD, Permissions.LOCATION_EDIT, Permissions.LOCATION_EXPORT, Permissions.LOCATION_IMPORT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                this.setSettingsListOptions(permissions, EventsNames.OPEN_LOCATION_CREATE_EDIT_POPUP, 'locations', this.showPreviewDetails, this.showMapDetails)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.getValueFromLocalStorage('issueSaved')
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('issueSaved')
            }
        })
       
        $scope.$on(EventsNames.LOCATION_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_LOCATION_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }

        $scope.getItems = () => {
            return jQuery.when.apply(jQuery,
                [Permissions.DEVICE_LIST, Permissions.METER_LIST, Permissions.TASK_LIST, Permissions.ISSUE_LIST, Permissions.SIM_CARD_LIST, Permissions.ACTION_LIST]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [DEVICE_LIST, METER_LIST, TASK_LIST, ISSUE_LIST, SIM_CARD_LIST, ACTION_LIST] = permissions
                var items = []
                if (DEVICE_LIST) items.push({
                    template: CommonHelper.getIconForDetails("DEVICES", '#000000', '18', i18n('Grid', 'ShowDevices'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdDevice) }
                })
                if (METER_LIST) items.push({
                    template: CommonHelper.getIconForDetails("METERS", '#000000', '18', i18n('Grid', 'ShowMeters'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdMeter) }
                })
                if (TASK_LIST) items.push({
                    template: CommonHelper.getIconForDetails("TASKS", '#000000', '18', i18n('Grid', 'ShowTasks'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdTask) }
                })
                if (ISSUE_LIST) items.push({
                    template: CommonHelper.getIconForDetails("ISSUES", '#000000', '18', i18n('Grid', 'ShowIssues'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdIssue) }
                })
                if (SIM_CARD_LIST) items.push({
                    template: CommonHelper.getIconForDetails("SIM_CARD", '#000000', '18', i18n('Grid', 'ShowSimCards'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdSimCard) }
                })
                if (ACTION_LIST) items.push({
                    template: CommonHelper.getIconForDetails("RECENT_ACTIONS", '#000000', '18', i18n('Grid', 'ShowRecentActions'), 'context-menu-row'), onItemClick: () => { this.getContextList(CommonHelper.IdAction) }
                })
                return items
            })
        }
    }

    getLocations = (request: GridRequestReferenceModel) => {
        if (this.$state.params['ids'] && this.$state.params['ids'].length > 1) {
            request.ids = this.$state.params['ids']
        }

        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.location.getLocations(request).then(result => {
            this.localDictionary.getLocationColumns().then(cols => {
                this.refreshFilters(cols)
                this.$scope.$root.$broadcast('enableLoadingIndicator')
            })
            return result
        })
    }

    drawGrid() {
        this.localDictionary.getLocationColumns().then(columns => {
            this.localDictionary.getLocationDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "LocationsGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onDataReady: this.onDataReady,
                    onSelectionChanged: this.onSelectionChanged,
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    },
                    height: "100%"
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger, this.getLocations, customOptions, true, this.localDictionary, null, this.$compile)
                    
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)

                this.gridOptions = gridOptions
            })
        })
    }
}

