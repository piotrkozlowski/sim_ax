﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { RouteModel, EntityModel, RouteFormValuesModel, GridRequestReferenceModel, GridModel, PointModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

declare var google

interface CommonData {
    details: any
    locations: any[]
    operators: any[]
}

interface RoutesDetailsScope extends DetailsScope {
    id: number
    editId: number
    modelId: number
    itemObjects: any[]
    popupTitle: string
    tasksDetailsPopupId: number

    mapOptions: DevExpress.ui.dxMapOptions
    visibleMap: boolean


    common: CommonData

    showTabs: boolean

    // accordion    
    routePoints: GridData
    pointsBridge: GridBridge

    routeOperators: GridData
    operatorsBridge: GridBridge

    confirmDelete()

    mapData: any[]
    rowsData: any[]

}

export class RoutesDetailsDirectiveController extends Details<RoutesDetailsScope> {
    private routeId: number
    private permissions: any[]
    private currentLocations: EntityModel[] = []
    private routePointsGridOnInitializedPromise: ng.IPromise<void>
    private routeOperatorsGridOnInitializedPromise: ng.IPromise<void>
    private isMarkerClicked: boolean

    private tasks: EntityModel[]

    constructor($scope: RoutesDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.RoutePopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'RouteDetails'),
            openEventName: EventsNames.OPEN_ROUTES_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_ROUTES_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.ROUTE_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.route
        $scope.editId = $scope.$id

        $scope.common = <CommonData>{}

        $scope.tasksDetailsPopupId = new Date().getTime()

        jQuery.when.apply(jQuery, [Permissions.ROUTE_PARAMETERS, Permissions.ROUTE_POINTS, Permissions.ROUTE_ASSIGNED_OPERATOR, Permissions.ROUTE_MAP, Permissions.ROUTE_EDIT].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteRoute'))
        }

        this.initRoutePointsGrid()
        this.initRouteOperatorsGrid()

        // popup open
        $scope.$on(EventsNames.OPEN_ROUTES_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })
        //map marker clicked
        $scope.$on(EventsNames.MARKER_CLICKED, (event: ng.IAngularEvent, ...args: any[]) => {
            this.routePointsGridOnInitializedPromise.then(() => {
                var rows = args[0].row
                this.isMarkerClicked = true
                this.$scope.routePoints.grid.selectRows(rows, false)
            })
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var route: RouteModel = args[0]
            if (route.id == this.routeId) {
                this.updateData(route)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_ROUTES_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.route.deleteRoute($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.ROUTE_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'RouteDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'RouteDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getRoute = {}
        this.$scope.common.locations = []
        this.$scope.common.operators = []
        this.$scope.routePoints.showGrid = false
        this.$scope.routeOperators.showGrid = false
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.route.getRoute(this.$scope.modelId, this.$scope.requestsStates.getRoute).then(routeResult => {
            this.$scope.popupTitle = routeResult.name
            this.$scope.showTabs = true
            this.routeId = routeResult ? routeResult.id : null;
            this.updateData(routeResult)
        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: RouteModel) {
        this.$scope.common.locations = result.locations
        this.$scope.common.operators = result.operators
        this.currentLocations = result.locations
        this.$scope.mapData = []

        //left column
        this.localDictionary.getRouteDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })

        //center column             
        this.routePointsGridOnInitializedPromise.then(() => {

            this.$scope.routePoints.options.dataSource.store().clear()
            if (result)
                result.locations.forEach(element => {
                    var location = {}
                    element.fields.forEach(f => {
                        location[f.name.toLowerCase()] = f.value
                    })
                    location[CommonHelper.nameField] = element.name
                    location[CommonHelper.idField] = element.id
                    this.$scope.routePoints.options.dataSource.store().insert(location)

                    this.$scope.mapData.push(location)
                })
            this.$scope.routePoints.grid.refresh()
            this.$scope.routePoints.showGrid = true
        })

        this.routeOperatorsGridOnInitializedPromise.then(() => {
            this.$scope.routeOperators.options.dataSource.store().clear()
            if (result)
                result.operators.forEach(element => {
                    var op = {}
                    op[CommonHelper.idField] = element.id
                    op[CommonHelper.nameField] = element.name
                    this.$scope.routeOperators.options.dataSource.store().insert(op)
                })
            this.$scope.routeOperators.grid.refresh()
            this.$scope.routeOperators.showGrid = true
        })


        //points bridge
        this.tasks = result.locations
        var locations = result.locations ? result.locations.map(location => {
            var object = {}
            if (location.id) object['id'] = location.id
            if (location.name) object['name'] = location.name
            location.fields.forEach(field => {
                object[field.name] = field.value
            })
            return object
        }) : []

        this.getRoutePoints(locations)
        this.buildOperatorsBridge()
    }

    getTabs() {
        var tabs = []

        tabs.push({ text: i18n('Common', 'CommonX'), icon: "home", name: 'common' })
        tabs.push({ text: i18n('Popup', 'Tracking'), icon: "runner", name: 'tracking' })

        return tabs
    }

    getAccordions() {
        var [ROUTE_PARAMETERS, ROUTE_POINTS, ROUTE_ASSIGNED_OPERATOR, ROUTE_MAP, ROUTE_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (ROUTE_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: ROUTE_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (ROUTE_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_ROUTES_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.RouteDetailsAccordions, 0, items))
        }

        if (ROUTE_POINTS || ROUTE_ASSIGNED_OPERATOR) {
            var items: DetailsAccordionSection[] = []

            if (ROUTE_POINTS)
                items.push({
                    title: i18n("Common", "Points"),
                    canEdit: ROUTE_EDIT,
                    name: 'points',
                    showStatus: true,
                    editMethod: () => {
                        if (ROUTE_EDIT)
                            this.updatePointsInEdit().then(() => {
                                this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + 'points', "routePoints")
                            })
                    },
                    status: () => {
                        return i18n("Common", "Records") + ": " + (this.tasks ? this.tasks.length : 0)
                    },
                    visible: true
                })

            if (ROUTE_ASSIGNED_OPERATOR)
                items.push({
                    title: i18n("Common", "Operators"),
                    canEdit: ROUTE_EDIT,
                    name: 'operators',
                    showStatus: true,
                    editMethod: () => {
                        if (ROUTE_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_EDIT_POPUP + this.$scope.detailsPopupId + 'operators')
                    },
                    status: () => {
                        return i18n('Common', 'Records') + ": " + this.$scope.common.operators.length
                    },
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.RouteDetailsAccordions, 1, items))
        }

        if (ROUTE_MAP) {
            var items: DetailsAccordionSection[] = []

            if (ROUTE_MAP)
                items.push({
                    title: i18n("Common", "Map"),
                    canEdit: false,
                    name: 'maps',
                    showStatus: false,
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.RouteDetailsAccordions, 2, items))
        }

        return accordions
    }

    updatePointsInEdit() {
        return this.localDictionary.getRoutePointColumnsEdit().then(displayFields => {
            var request = new GridRequestReferenceModel()
            request.columns = displayFields
            request.where = this.tasks.map(l => l.id).join()
            request.pageSize = this.tasks.length
            return this.api.route.getRoutePoints(request).then((result: GridModel) => {
                var orderedLocations = []
                this.tasks.forEach(loc => {
                    result.values.forEach(value => {
                        if (loc.id == value.id) {
                            orderedLocations.push(value)
                            return
                        }
                    })
                })
                this.$scope.common.locations = orderedLocations
                this.$scope.pointsBridge.items = this.$scope.common.locations
                this.$scope.pointsBridge.title = i18n('Popup', 'PointsEdit') + " (" + this.$scope.common.locations.length + ")"
            })
        })
    }

    isContain(value: number, obj: any[], field: any) {
        for (var i = 0; i < obj.length; i++) {
            if (obj[i][field] == value) return true
        }
        return false
    }

    getRoutePoints(locations: any[]) {
        this.buildPointsBridge()
    }

    buildPointsBridge() {
        this.$scope.pointsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'PointsEdit') + " (" + this.$scope.common.locations.length + ")",
            items: this.$scope.common.locations,
            popupId: this.$scope.detailsPopupId + 'points',
            dictionaryMethodName: 'getRoutePointColumnsEdit',
            dataMethodName: 'getLocations',
            dataScope: 'location',
            onSubmit: (added, removed, inGrid) => {
                var formModel = new RouteFormValuesModel()
                added = added.map(d => d[CommonHelper.idField])
                removed = removed.map(d => d[CommonHelper.idField])
                formModel.id = this.$scope.modelId

                var allLocations = inGrid.map(d => d[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1)

                formModel.removedTasks = this.tasks.filter(task => {
                    return allLocations.indexOf(task.id) == -1
                }).map(task => task.referenceId)

                formModel.locations = allLocations.map(idLocation => {
                    var point = new PointModel()
                    point.idLocation = idLocation
                    this.tasks.every(t => {
                        if (t.id == idLocation) {
                            point.idTask = t.referenceId
                            return false
                        }
                        return true
                    })

                    return point
                })

                return this.saveFormModel(formModel).then(result => {
                    this.updatePointsInEdit()
                    return result
                })
            }
        }
    }

    saveFormModel(formModel: RouteFormValuesModel) {
        var deferred = this.$q.defer()
        this.api.route.createRoute(formModel).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'RouteSaveError'), 'error', 5000)
                deferred.reject()
            } else {
                CommonHelper.openInfoPopup(i18n('Popup', 'RouteSaved'))
                this.updateData(response)
                deferred.resolve(true)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'RouteSaveError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }

    removeAllPoints() {

        if (this.mapWidget && this.$scope.mapOptions) {
            this.mapWidget.option('zoom', 21)
            this.$scope.mapOptions.zoom = 21
            this.$scope.mapOptions.autoAdjust = true
            this.mapWidget.option('autoAdjust', true)
            this.mapWidget.option({ markers: [] })
            this.$scope.mapOptions.markers = []
        }

    }

    buildOperatorsBridge() {
        this.$scope.operatorsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'OperatorsEdit') + " (" + this.$scope.common.operators.length + ")",
            items: this.$scope.common.operators,
            popupId: this.$scope.detailsPopupId + 'operators',
            dictionaryMethodName: 'getRouteOperatorsColumns',
            dataMethodName: 'getRouteOperators',
            dataScope: 'route',
            onSubmit: (added, removed, inGrid) => {
                var formModel = new RouteFormValuesModel()
                added = added.map(d => d[CommonHelper.idField])
                removed = removed.map(d => d[CommonHelper.idField])
                formModel.id = this.$scope.modelId
                formModel.operators = inGrid.map(d => d[CommonHelper.idField]).filter(id => removed.indexOf(id) == -1)
                formModel.removedOperators = removed
                return this.saveFormModel(formModel)
            }
        }
    }

    showTaskDetails(e) {
        var itemIds = this.$scope.routePoints.options.dataSource.items().map(item => {
            return {
                id: item[CommonHelper.tasksColumnId]
            }
        })
        this.$scope.$broadcast(EventsNames.OPEN_TASKS_DETAILS_POPUP + this.$scope.tasksDetailsPopupId, e.data[CommonHelper.tasksColumnId], itemIds)
    }


    initRoutePointsGrid() {
        this.$scope.routePoints = <GridData>{}

        var defer = this.$q.defer<void>()
        this.routePointsGridOnInitializedPromise = defer.promise

        this.localDictionary.getRoutePointColumns().then(columns => {
            this.$scope.routePoints.columnsCount = columns.length

            var customGridOptions = {
                name: "RoutesPointsDetails",
                columnsDictionary: columns,
                defaultColumnsDictionary: columns,
                onInitialized: e => {
                    this.$scope.routePoints.grid = e.component
                    defer.resolve()
                },
                onSelectionChanged: e => {
                    if (!this.isMarkerClicked) {
                        this.$scope.$broadcast(EventsNames.GRIDSTER_ROW_CLICKED, e.selectedRowsData)
                    } else {
                        this.isMarkerClicked = false
                    }
                },
                onRowDoubleClick: e => {
                    this.showTaskDetails(e)
                }
            }
            this.$scope.routePoints.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, this.$scope, this.logger, null, customGridOptions)
        })

    }

    initRouteOperatorsGrid() {
        this.$scope.routeOperators = <GridData>{}

        var defer = this.$q.defer<void>()
        this.routeOperatorsGridOnInitializedPromise = defer.promise

        this.localDictionary.getRouteOperatorsColumns().then(columns => {
            this.$scope.routeOperators.columnsCount = columns.length
            var customGridOptions = {
                name: "RoutesOperatorsDetails",
                columnsDictionary: columns,
                defaultColumnsDictionary: columns,
                onInitialized: e => {
                    this.$scope.routeOperators.grid = e.component
                    defer.resolve()
                }
            }
            this.$scope.routeOperators.options = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, this.$scope, this.logger, null, customGridOptions)
        })
    }
}