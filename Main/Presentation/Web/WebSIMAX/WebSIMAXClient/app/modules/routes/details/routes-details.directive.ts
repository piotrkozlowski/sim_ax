﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { RoutesDetailsDirectiveController } from './routes-details.directive.controller'

export class RoutesDetailsDirective {    
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new RoutesDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./routes-details.tpl.html")
}