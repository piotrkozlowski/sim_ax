﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, FieldNameValueModel, GridRequestModel, GridRequestReferenceModel, NotificationModel, NotificationEntity, SecurityChangeModel, SecurityActivityModel, OperatorModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonExtendedDataGridOptions, CommonScope } from '../../common/common.extended-data-grid-options'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'
import { FileUploaderOptions } from '../../common/file-uploader/file-uploader.directive'
import { TrackEvents } from '../../../helpers/track-events'
import * as _ from 'underscore'

declare var google: any

interface DeviceOnMap {
    idLocation: number
    latitude: number
    longitude: number
    serialNbr: number
    marker?: any
    stepNumber: number
}

interface DeviceReading {
    text: string
    time: any
    stepNumber: number
}

interface TrackStep {
    latitude: number
    longitude: number
    stepNumber: number
    marker?: any
    deviceReadings: DeviceReading[]
}

interface DeviceMeasurement {
    serialNbr: number
    stepNumber: number
}

export interface TrackingScope extends CommonScope {
    startButtonOptions: DevExpress.ui.dxButtonOptions
    pauseButtonOptions: DevExpress.ui.dxButtonOptions
    prevButtonOptions: DevExpress.ui.dxButtonOptions
    nextButtonOptions: DevExpress.ui.dxButtonOptions
    firstButtonOptions: DevExpress.ui.dxButtonOptions
    lastButtonOptions: DevExpress.ui.dxButtonOptions
    resetButtonOptions: DevExpress.ui.dxButtonOptions
    trackingGridOptions: DevExpress.ui.dxDataGridOptions
    timeSelectBoxOptions: DevExpress.ui.dxSelectBoxOptions
    
    mapOptions: DevExpress.ui.dxMapOptions
    map: DevExpress.ui.dxMap

    fileUploaderOptions: FileUploaderOptions

    sliderOptions: DevExpress.ui.dxSliderOptions
    slider: DevExpress.ui.dxSlider

    showMap: boolean
    mapZoom: number
    mapCenter: number[]
    file: any
    readedText: string
    selectedRow: number

    gridData: any[]

    isStartButtonDisabled: boolean
    isPauseButtonDisabled: boolean
    isPrevButtonDisabled: boolean
    isNextButtonDisabled: boolean
    isFirstButtonDisabled: boolean
    isLastButtonDisabled: boolean

    selectedInterval: number
}

export class TrackingDirectiveController {
    interval: any
    map: any
    infoTooltip: any
    trackPolyline: any

    devicesOnMap: DeviceOnMap[] = []
    trackOnMap: TrackStep[] = []
    receivedMeasurementsFromDevices: DeviceMeasurement[] = []
    receivedCallsFromDevices: DeviceMeasurement[] = []

    constructor(private $scope: TrackingScope, private $q: ng.IQService, private $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        this.resetVariables()
        $scope.selectedInterval = 2000

        $scope.$watch('selectedRow', () => {
            this.selectRow($scope.selectedRow)
            if (this.$scope.slider) {
                this.$scope.slider.option('value', $scope.selectedRow)
            }
        })

        $scope.fileUploaderOptions = {
            acceptValue: 'txt',
            uploaderVisible: true,
            readAsText: true,
            onReadEnd: (reader, fileName) => {
                $scope.readedText = reader.result
                this.loadFile()
                localDictionary.getRouteTrackingColumns().then(columns => {
                    var customTrackingGridOptions = {
                        name: 'TrackingGrid',
                        columnsDictionary: columns,
                        onInitialized: e => {
                            $scope.dataGrid = e.component
                        },
                        dataSource: {
                            store: {
                                type: 'array',
                                key: 'id',
                                data: $scope.gridData
                            }
                        },
                        selection: {
                            mode: 'single'
                        },
                        onSelectionChanged: (e) => {
                            var scrollable = e.component.getView('rowsView')._scrollable
                            var selectedRowElements = e.component.element().find('tr.dx-selection')
                            scrollable.scrollToElement(selectedRowElements)
                            this.selectionChanged(e)
                        },
                        onRowClick: (e) => {
                            if (this.interval)
                                clearInterval(this.interval)
                            this.resetVariables()
                        },
                        columnAutoWidth: false,
                        masterDetail: {
                            enabled: true,
                            template: "detail",
                            autoExpandAll: true
                        }
                    }

                    $scope.trackingGridOptions = new CommonExtendedDataGridOptions(CommonHelper.paginatedSmallGrid, $scope, logger, null, customTrackingGridOptions, false, localDictionary)
                })
            }
        }
        
        $scope.mapZoom = 5
        $scope.mapCenter = [51.9189046, 19.1343787]

        $scope.mapOptions = {
            provider: 'google',
            height: '100%',
            width: '100%',
            type: 'roadmap',
            bindingOptions: {
                zoom: 'mapZoom',
                center: 'mapCenter'
            },
            autoAdjust: true,
            onInitialized: (e) => {
                this.$scope.map = e.component
            },
            onReady: (e) => {
                this.map = e.originalMap
                this.updateMarkers()
                $timeout(() => {
                    this.zoomCenter()
                })
            }
        }

        $scope.timeSelectBoxOptions = {
            dataSource: [
                {
                    value: 1000,
                    text: '1s'
                },
                {
                    value: 2000,
                    text: '2s'
                },
                {
                    value: 5000,
                    text: '5s'
                },
                {
                    value: 10000,
                    text: '10s'
                },
                {
                    value: 20000,
                    text: '20s'
                }
            ],
            value: $scope.selectedInterval,
            valueExpr: 'value',
            displayExpr: 'text',
            width: 80,
            bindingOptions: {
                disabled: 'isStartButtonDisabled'
            },
            onValueChanged: e => {
                $scope.selectedInterval = e.value
            }
        }

        $scope.startButtonOptions = {
            template: CommonHelper.getIcon('PLAY_BUTTON'),
            bindingOptions: {
                disabled: 'isStartButtonDisabled'
            },
            onClick: () => {
                this.start()
            }
        }

        $scope.pauseButtonOptions = {
            template: CommonHelper.getIcon('PAUSE_BUTTON'),
            bindingOptions: {
                disabled: 'isPauseButtonDisabled'
            },
            onClick: () => {
                this.pause()
            }
        }

        $scope.prevButtonOptions = {
            template: CommonHelper.getIcon('PREV_BUTTON'),
            bindingOptions: {
                disabled: 'isPrevButtonDisabled'
            },
            onClick: () => {
                this.prev()
            }
        }

        $scope.nextButtonOptions = {
            template: CommonHelper.getIcon('NEXT_BUTTON'),
            bindingOptions: {
                disabled: 'isNextButtonDisabled'
            },
            onClick: () => {
                this.next()
            }
        }

        $scope.firstButtonOptions = {
            template: CommonHelper.getIcon('FIRST_BUTTON'),
            bindingOptions: {
                disabled: 'isFirstButtonDisabled'
            },
            onClick: () => {
                this.first()
            }
        }

        $scope.lastButtonOptions = {
            template: CommonHelper.getIcon('LAST_BUTTON'),
            bindingOptions: {
                disabled: 'isLastButtonDisabled'
            },
            onClick: () => {
                this.last()
            }
        }

        $scope.resetButtonOptions = {
            template: CommonHelper.getIconWithText(i18n('Popup', 'Reset'), 'RENEW_BUTTON'),
            onClick: () => {
                this.$scope.file = null
                this.$scope.readedText = null
                this.$scope.fileUploaderOptions.uploaderVisible = true
                this.devicesOnMap.forEach(device => {
                    device.marker.setVisible(false)
                })
                this.devicesOnMap = []
            }
        }
    }

    private zoomCenter() {
        var markerBounds = new google.maps.LatLngBounds();
        if (this.devicesOnMap.length == 1) {
            this.map.setCenter(this.devicesOnMap[0].marker.getPosition());
            this.map.setZoom(10)
        } else {
            for (var i in this.devicesOnMap) {
                markerBounds.extend(new google.maps.LatLng(this.devicesOnMap[i].latitude, this.devicesOnMap[i].longitude))
            }
            this.map.fitBounds(markerBounds);
        }
    }

    loadFile() {
        var lines = this.$scope.readedText.split(/\r\n|\n/)
        this.$scope.gridData = []

        var index = 0
        lines.forEach(line => {
            index = this.validateLine(line, index)
        })

        this.$scope.sliderOptions = {
            min: 0,
            max: index-1,
            value: 0,
            onValueChanged: e => {
                this.$scope.selectedRow = e.value
            },
            onInitialized: e => {
                this.$scope.slider = e.component
            }
        }
    }

    private validateLine(line: string, index: number) {
        var timeRegex = /\d{1,2}:\d{1,2}:\d{1,2}/g
        var time = timeRegex.exec(line)

        var eventRegex = /\[EventID: (\d+)\]/g
        var event = eventRegex.exec(line)

        var lineRegex = /\[Line: \d+\] /g
        var foundLine = lineRegex.exec(line)
        var eventInfo = line.substring(lineRegex.lastIndex || 0 + 1)
        

        if (time && event && eventInfo && [0, TrackEvents.ExecuteMethod, TrackEvents.Read, TrackEvents.Write].indexOf(parseInt(event[1])) < 0) {
            var gridRow = {
                id: index,
                time: time[0],
                eventtype: parseInt(event[1]),
                eventinfo: this.replaceNumbersInString(eventInfo)
            }

            this.checkEventId(parseInt(event[1]), eventInfo, gridRow, index)
            this.$scope.gridData.push(gridRow)

            return ++index
        }
        return index
    }

    private checkEventId(eventId: number, eventInfo: string, row: any, index: number) {
        if ([TrackEvents.Processed, TrackEvents.FrameProcessed, TrackEvents.LocationDescrEnd, TrackEvents.ReciveDiagnostic, TrackEvents.ReadDiagnostic].indexOf(eventId) > -1) { // frames
            var regex = /SERIAL_NBR=(\d+)/g

            var serialNbr = regex.exec(eventInfo)
            if (serialNbr) {
                var data = <DeviceMeasurement>{
                    serialNbr: parseInt(serialNbr[1]),
                    stepNumber: index
                };

                (<any>Object).assign(row, data)
            }
        }
        else if ([TrackEvents.LocationDescrStart].indexOf(eventId) > -1) { // new location point
            const latRegex = /LOCATION_LATITUDE=(\d+(\.|,)?\d*)/g
            const lngRegex = /LOCATION_LONGITUDE=(\d+(\.|,)?\d*)/g
            const serialNbrRegex = /SERIAL_NBR=(\d+)/g
            const idLocationRegex = /ID_LOCATION=(\d+)/g

            var lat = latRegex.exec(eventInfo)
            var lng = lngRegex.exec(eventInfo)
            var serialNbr = serialNbrRegex.exec(eventInfo)
            var idLocation = idLocationRegex.exec(eventInfo)

            lat[1] = lat[1].replace(',', '.')
            lng[1] = lng[1].replace(',', '.')

            var latitude = parseFloat(lat[1])
            var longitude = parseFloat(lng[1])

            if (latitude < -90 || latitude > 90)
                latitude = 0
            if (longitude < -180 || longitude > 180)
                longitude = 0

            var deviceData = <DeviceOnMap>{
                idLocation: parseInt(idLocation[1]),
                latitude: latitude,
                longitude: longitude,
                serialNbr: parseFloat(serialNbr[1]),
                stepNumber: index
            }

            this.devicesOnMap.push(deviceData);
            (<any>Object).assign(row, deviceData)
        }
        else if (eventId === TrackEvents.CurGPS) { // GPS new location or information about error
            const latRegex = /LATITUDE: (\d+(\.|,)?\d*)/g
            const lngRegex = /LONGITUDE: (\d+(\.|,)?\d*)/g

            var lat = latRegex.exec(eventInfo)
            var lng = lngRegex.exec(eventInfo)

            if (lat && lng) {
                lat[1] = lat[1].replace(',', '.')
                lng[1] = lng[1].replace(',', '.')

                var marker = new google.maps.Marker({
                    icon: 'https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_red.png',
                    position: {
                        lat: parseFloat(lat[1]),
                        lng: parseFloat(lng[1])
                    }
                })

                var trackStep = <TrackStep>{
                    latitude: parseFloat(lat[1]),
                    longitude: parseFloat(lng[1]),
                    stepNumber: index,
                    marker: marker,
                    deviceReadings: []
                }

                this.trackOnMap.push(trackStep);
                (<any>Object).assign(row, trackStep)
            }
        }
        else if ([119,120,121,122,123,124,125,126,127,128,129,130,131,132,141,142,143,144,145,146,147,148,149,150,151,152].indexOf(eventId) > -1) { // Device informations
            if (this.trackOnMap && this.trackOnMap.length > 0) {
                var lastStep = this.trackOnMap[this.trackOnMap.length - 1]

                lastStep.deviceReadings.push({
                    stepNumber: index,
                    text: this.replaceNumbersInString(eventInfo),
                    time: ''
                })
            }
        }
        else if (eventId === TrackEvents.ReadoutRecived) { // Device measurements
            const serialNbrRegex = /SERIAL_NBR=(\d+)/g

            var serialNbr = serialNbrRegex.exec(eventInfo)

            if (serialNbr) {
                var data = <DeviceMeasurement>{
                    serialNbr: parseInt(serialNbr[1]),
                    stepNumber: index
                }

                this.receivedMeasurementsFromDevices.push(data);
                (<any>Object).assign(row, data)
            }
        }
        else if (eventId === TrackEvents.ReciveWMBUS) { // Device ready calls
            const serialNbrRegex = /SERIAL_NBR=(\d+)/g

            var serialNbr = serialNbrRegex.exec(eventInfo)

            if (serialNbr) {
                var data = <DeviceMeasurement>{
                    serialNbr: parseInt(serialNbr[1]),
                    stepNumber: index
                }

                this.receivedCallsFromDevices.push(data);
                (<any>Object).assign(row, data)
            }
        }
    }

    private resetVariables() {
        this.$scope.isFirstButtonDisabled = false
        this.$scope.isStartButtonDisabled = false
        this.$scope.isPauseButtonDisabled = true
        this.$scope.isLastButtonDisabled = false
        this.$scope.isPrevButtonDisabled = false
        this.$scope.isNextButtonDisabled = false
    }

    private updateMarkers() {
        if (this.map) {
            this.devicesOnMap.forEach(device => {
                device.marker = new google.maps.Marker({
                    position: {
                        lat: device.latitude,
                        lng: device.longitude
                    },
                    icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                    map: this.map
                })
            })
            this.$scope.showMap = true
        }
    }

    private selectionChanged(e) {
        var row = e.selectedRowsData[0]

        if (!row)
            return
        this.$scope.selectedRow = row.id

        // hiding previous dxToast
        CommonHelper.closeInfoPopup()

        // Closing existing tooltip
        if (this.infoTooltip)
            this.infoTooltip.close()

        this.checkGPSLocation(row.id)   // Checking gps location
        this.resetMapMarkers()          // Reset markers
        this.highlightDevices(row.id, this.receivedCallsFromDevices, 'orange')          // Highlight all devices which made call
        this.highlightDevices(row.id, this.receivedMeasurementsFromDevices, 'green')    // Highlight all devices with measurements as far
        this.prepareSummaryTooltips(row.id)     // Summary tooltips for GPS locations

        if ([TrackEvents.Processed, TrackEvents.FrameProcessed, TrackEvents.ReadoutRecived, TrackEvents.ReciveWMBUS, TrackEvents.ReciveDiagnostic, TrackEvents.ReadDiagnostic].indexOf(row.eventtype) > -1) {
            var selectedDevice = _.find(this.devicesOnMap, device => {
                return device.serialNbr == row.serialNbr
            })
            if (selectedDevice) {
                this.openInfoTooltip(selectedDevice.marker, row.eventinfo)
            }
            else {
                this.showToast(row.eventinfo, 'info')
            }
        }
        else if ([TrackEvents.LocationDescrStart, TrackEvents.LocationDescrEnd].indexOf(row.eventtype) > -1) {  // Clicked row with adding new device location
            var selectedDevice = _.find(this.devicesOnMap, (device) => {
                return device.serialNbr == row.serialNbr
            })

            this.openInfoTooltip(selectedDevice.marker, row.eventinfo)
        }
        else {
            this.showToast(row.eventinfo, 'info')
        }

    }

    private replaceNumbersInString(text: string): string {
        var regex = /((\d+(\.|,)?\d*)[,:\/]*)+/g
        var locationNameRegex = /LOCATION_NAME=((\w*\s?)*);/g
        var frameRegex = /FRAME=(\w*)/g
        var changedText = text

        // finding location name
        var locationNameResult = locationNameRegex.exec(text)

        if (locationNameResult) {
            changedText = changedText.replace(locationNameResult[1], '{location_name}')
        }

        changedText = changedText.split(' ').join('_')

        // finding frame
        var frameResult = frameRegex.exec(text)

        if (frameResult) {
            changedText = changedText.replace(frameResult[1], '{frame}')
        }

        var index = 0

        // finding all numbers
        var result = changedText.match(regex)
        
        // if no numbers in string return default text
        if (result) {
            var charIndex = 0 // index in string of last replaced number
            result.forEach(number => {
                changedText = changedText.substring(0, charIndex) + changedText.substring(charIndex).replace(number, '{0_' + index + '}')
                charIndex = changedText.indexOf('{0_' + index + '}') + 3 + index.toString().length
                index++
            })
        }

        // downloaded text from api
        var textFromApi = i18n('Tracking', changedText)

        if (result) {
            index = 0
            // replacing numbers
            result.forEach(number => {
                textFromApi = textFromApi.replace('{0_' + index++ + '}', number)
            })
        }

        if (frameResult) {
            textFromApi = textFromApi.replace('{frame}', frameResult[1])
        }

        if (locationNameResult) {
            textFromApi = textFromApi.replace('{location_name}', locationNameResult[1])
        }

        textFromApi = textFromApi.split('_').join(' ')

        return textFromApi
    }

    private openInfoTooltip(entity, text: string) {
        this.infoTooltip = new google.maps.InfoWindow({
            content: text.split(';').join('<br/>')
        })

        this.infoTooltip.open(this.map, entity)
    }

    private highlightMarker(marker, markerColor?: string) {

        if (marker) {
            // set different color of marker (green or orange)
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/' + (markerColor || 'green') + '-dot.png')
            // highlight marker
            marker.setZIndex(100)
        }
    }

    private showToast(text: string, type: string) {
        CommonHelper.openInfoPopup(text, type, this.$scope.selectedInterval, undefined, {
            position: {
                at: 'bottom right',
                my: 'bottom right',
                offset: '-10 -10'
            }
        })
    }

    checkGPSLocation(stepNumber: number) {
        // Removing existing polyline
        if (this.trackPolyline)
            this.trackPolyline.setMap(null)
        if (this.trackOnMap && this.trackOnMap.length > 0) {
            this.trackOnMap.forEach(step => step.marker.setMap(null))
        }

        var latLngArray = []
        var lastStepIndex

        for (var i = 0; i < this.trackOnMap.length; i++) {
            var step = this.trackOnMap[i]
            if (step.stepNumber <= stepNumber) {
                latLngArray.push({
                    lat: step.latitude,
                    lng: step.longitude
                })

                // set blue dot as an icon for marker
                step.marker.setIcon('https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_blue.png')
                step.marker.setMap(this.map)
            }

            lastStepIndex = i
            
            if (step.stepNumber >= stepNumber) {
                if (step.stepNumber > stepNumber)
                    lastStepIndex = i - 1
                break
            }
        }

        if (latLngArray.length > 0) {
            this.trackPolyline = new google.maps.Polyline({
                path: latLngArray,
                geodesic: true,
                strokeColor: '#0000FF',
                strokeOpacity: 1.0,
                strokeWeight: 2
            })

            var lastStep = this.trackOnMap[lastStepIndex]
            if (lastStep)
                lastStep.marker.setIcon({
                    url: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    scaledSize: new google.maps.Size(16, 16)
                })

            this.trackPolyline.setMap(this.map)
        }
    }

    highlightDevices(stepNumber: number, devicesList: any[], markerColor?: string) {
        _.forEach(devicesList, device => {
            if (device.stepNumber <= stepNumber) {
                var currentDevice = _.find(this.devicesOnMap, dev => {
                    return dev.serialNbr == device.serialNbr
                })

                if (currentDevice) {
                    this.highlightMarker(currentDevice.marker, markerColor)
                }
            }
        })
    }

    resetMapMarkers() {
        _.forEach(this.devicesOnMap, (device) => {
            // setting default red color for marker
            if (device.marker) {
                device.marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png')
                device.marker.setZIndex(1)
            }
        })
    }

    prepareSummaryTooltips(stepNumber: number) {
        if (this.trackOnMap && this.trackOnMap.length > 0) {
            _.forEach(this.trackOnMap, (step) => {
                var text = '<p>'

                // calculating text for tooltip
                _.forEach(step.deviceReadings, (reading) => {
                    if (reading.stepNumber <= stepNumber) {
                        text += reading.text + '<br/>'
                    }
                })

                // remove previous listeners
                google.maps.event.clearListeners(step.marker, 'click')

                // add new click listener
                step.marker.addListener('click', (event) => {
                    this.openInfoTooltip(step.marker, text)
                })
            })
        }
    }

    selectRow(rowKey: number) {
        if (!this.$scope.dataGrid)
            return
        var pageSize = this.$scope.dataGrid.pageSize()
        var page = Math.floor(rowKey / pageSize)

        this.$scope.dataGrid.pageIndex(page)
        this.$scope.dataGrid.selectRows([rowKey], false)
    }

    start() {
        this.$scope.isStartButtonDisabled = true
        this.$scope.isPauseButtonDisabled = false
        this.interval = setInterval(() => {
            this.next()
        }, this.$scope.selectedInterval)
    }

    pause() {
        this.$scope.isStartButtonDisabled = false
        this.$scope.isPauseButtonDisabled = true
        clearInterval(this.interval)
    }

    prev() {
        this.$scope.isNextButtonDisabled = false
        this.$scope.isLastButtonDisabled = false
        this.$scope.isStartButtonDisabled = false

        var selectedRows = this.$scope.dataGrid.getSelectedRowKeys()[0]
        var pageSize = this.$scope.dataGrid.pageSize()
        if (selectedRows) {
            if (<number>selectedRows % 100 > 0)
                this.$scope.dataGrid.selectRows([<number>selectedRows - 1], false)
            else {
                this.$scope.dataGrid.pageIndex(this.$scope.dataGrid.pageIndex() - 1)
                this.$scope.dataGrid.selectRows([<number>selectedRows - 1], false)
            }

            if (<number>selectedRows == 1 && this.$scope.dataGrid.pageIndex() == 0) {
                this.$scope.isPrevButtonDisabled = true
                this.$scope.isFirstButtonDisabled = true
            }
        }
    }

    next() {
        this.$scope.isPrevButtonDisabled = false
        this.$scope.isFirstButtonDisabled = false

        var selectedRows = this.$scope.dataGrid.getSelectedRowKeys()[0]
        var pageSize = this.$scope.dataGrid.pageSize()
        
        if ([null, undefined].indexOf(selectedRows) < 0) {
            if (<number>selectedRows % 100 < pageSize - 1)
                this.$scope.dataGrid.selectRows([<number>selectedRows + 1], false)
            else {
                this.$scope.dataGrid.pageIndex(this.$scope.dataGrid.pageIndex() + 1)
                this.$scope.dataGrid.selectRows([<number>selectedRows + 1], false)
            }

            var pageCount = Math.ceil(this.$scope.dataGrid.totalCount() / pageSize)
            
            if (<number>selectedRows == this.$scope.dataGrid.totalCount() - 1) {
                this.$scope.isNextButtonDisabled = true
                this.$scope.isLastButtonDisabled = true
                this.$scope.isStartButtonDisabled = true
                this.$scope.isPauseButtonDisabled = true

                clearInterval(this.interval)
            }
        }
        else {
            this.$scope.dataGrid.selectRowsByIndexes([0])
        }
    }

    first() {
        this.$scope.isPrevButtonDisabled = true
        this.$scope.isFirstButtonDisabled = true
        this.$scope.isNextButtonDisabled = false
        this.$scope.isLastButtonDisabled = false
        this.$scope.isStartButtonDisabled = false

        this.$scope.dataGrid.pageIndex(0)
        this.$scope.dataGrid.selectRows([0], false)
    }

    last() {
        this.$scope.isNextButtonDisabled = true
        this.$scope.isLastButtonDisabled = true
        this.$scope.isPrevButtonDisabled = false
        this.$scope.isFirstButtonDisabled = false
        this.$scope.isStartButtonDisabled = true

        var pageSize = this.$scope.dataGrid.pageSize()
        var pageCount = Math.ceil(this.$scope.dataGrid.totalCount() / pageSize)

        this.$scope.dataGrid.pageIndex(pageCount - 1)
        var lastPageCount = this.$scope.dataGrid.totalCount() % pageSize
        this.$scope.dataGrid.selectRows([this.$scope.dataGrid.totalCount() - 1], false)
    }
}