﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { TrackingDirectiveController } from './tracking.directive.controller'

require('./tracking.scss')

export class TrackingDirective {
    scope = {
        id: '='
    }
    restrict = "E"
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new TrackingDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require('./tracking.tpl.html')
}