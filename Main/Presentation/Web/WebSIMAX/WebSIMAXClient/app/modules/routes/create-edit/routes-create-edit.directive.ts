﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { PopupType } from './../../../helpers/popup-manager'

require('./routes-create-edit.scss')

interface RoutesCreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class RoutesCreateEditDirective implements ng.IDirective {
    constructor() {  
              
    }

    restrict = 'E'
    scope = {
        id: "="
    }

    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: RoutesCreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Popup', 'SaveRoute'),
            type: PopupType.ROUTE_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitRoute(fields, id)
            }
        }
        var loadColumns = (isEdit: boolean) => {
            return $q.all([localDictionary.getRouteEditFields(), localDictionary.getRouteDisplayFields(), localDictionary.getRouteRequiredFields()]).then(([columnsEdit, columnsDisplay, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }

        $scope.$on(EventsNames.OPEN_ROUTES_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {            
            loadColumns(args && args[0]).then(() => {
                if (args && args[0]) {
                    $scope.options.title = i18n('Popup', 'RouteEdit')

                    api.route.getRouteToEdit(args[0]).then(result => {
                        $scope.editedItem = result.fields
                        $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, args[0])
                    })
                    return
                } else {
                    $scope.editedItem = null
                    $scope.options.title = i18n('Popup', 'RouteAdd')
                }
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem)
            })
        })

        var submitRoute = (fields: FieldNameValueModel[], id: number) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.route.createRoute(formModel).then(route => {
                if (!route) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'RouteSaveError'), 'error', 5000)
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'RouteSaved'))
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, route)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
    }]
    template = require("./routes-create-edit.tpl.html")
    transclude = true
}