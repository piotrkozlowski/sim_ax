﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { RoutesCreateEditDirective } from './create-edit/routes-create-edit.directive'
import { RoutesDetailsDirective } from './details/routes-details.directive'
import { TrackingDirective } from './tracking/tracking.directive'
import { i18n } from '../common/i18n/common.i18n'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'
import { Permissions } from '../../helpers/permissions'

import { RoutesController } from './routes.controller'

var moduleName = 'simax.routes'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('RoutesController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new RoutesController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('routesDetails', [() => new RoutesDetailsDirective()])
module.directive('routesCreateEdit', [() => new RoutesCreateEditDirective()])
module.directive('tracking', [() => new TrackingDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.ROUTE_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['ROUTES'], 'routes', 'Routes', 'routes', 9)
        $stateProvider.state('routes', {
            url: '/routes',
            controller: 'RoutesController',
            template: require('./routes.tpl.html')
        })
    })
}])

export default moduleName