﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ColumnModel, ReportModel, EntityModel, FormValuesModel, FileModel } from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { GridSelectBridge } from '../../common/grid-select/grid-select.directive'
import { CommonExtendedDataGridOptions } from '../../common/common.extended-data-grid-options'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { CommonHelper } from '../../../helpers/common-helper'
import { Permissions } from '../../../helpers/permissions'
import { MeasurementsBridge } from '../../common/measurements/measurements.controller'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

require('./reports-details.scss')


interface CommonData {
    details: any
}

interface ReportDetailsScope extends DetailsScope {
    id: number
    editId: number
    itemObjects: any[]

    popupTitle: string
    common: CommonData

    showTabs: boolean

    reportsDetailsPopupId: number

    confirmDelete: () => void
    editButtonOptions: DevExpress.ui.dxButtonOptions

    //closing accordings
    closeParamsButtonOptions: DevExpress.ui.dxButtonOptions
}

export class ReportDetailsDirectiveController extends Details<ReportDetailsScope> {
    private reportId: number
    private permissions: any[]

    constructor($scope: ReportDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.ReportPopupTabActive)

        var actions: DevExpress.ui.dxButtonOptions[];

        jQuery.when.apply(jQuery,
            [Permissions.REPORT_GENERATE]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            var [REPORT_GENERATE] = permissions
            if (REPORT_GENERATE) {
                actions = [{
                    template: CommonHelper.getIconWithText(i18n('Grid', 'GenerateReport'), 'REPORT_GENERATE'),
                    onClick: (e) => {
                        $scope.$root.$broadcast(EventsNames.REPORT_GENERATE, $scope.modelId)                        
                    },
                    width: '100%'
                }]
            }
        })

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'ReportDetails'),
            openEventName: EventsNames.OPEN_REPORTS_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_REPORTS_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.REPORT_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) },
            actions: actions
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.report
        $scope.editId = $scope.$id

        $scope.common = <CommonData>{}        

        jQuery.when.apply(jQuery, [Permissions.REPORT_PARAMETERS, Permissions.REPORT_EDIT].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })        

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteReport'))
        }

        // popup open
        $scope.$on(EventsNames.OPEN_REPORTS_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var report: ReportModel = args[0]
            if (report.id == this.reportId) {
                this.updateData(report)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_REPORTS_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.report.deleteReport($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.REPORTS_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'ReportDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'ReportDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getReport = {}   
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.report.getReport(this.$scope.modelId, this.$scope.requestsStates.getReport).then(reportResult => {
            this.$scope.popupTitle = reportResult.name            
            this.reportId = reportResult ? reportResult.id : null;
            this.updateData(reportResult)
        }).catch(reason => {
            this.logger.log(reason)            
        }).finally(() => { this.$scope.showTabs = true })    
    }

    updateData(result: ReportModel) {
        //left column
        this.localDictionary.getReportsDisplayFields().then(displayFields => {
        this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })
    }

    getTabs() {
        var tabs = []

        tabs.push({ text: i18n('Common', 'CommonX'), icon: "home", name: 'common' })

        return tabs
    }

    getAccordions() {
        var [REPORT_PARAMETERS, REPORT_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (REPORT_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: REPORT_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (REPORT_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_REPORTS_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.ReportDetailsAccordions, 0, items))
        }

        return accordions
    }
}