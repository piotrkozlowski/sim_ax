﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { ReportsDetailsDirective } from './details/reports-details.directive'
import { ReportCreateEditDirective } from './create-edit/report-create-edit.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { ReportsController } from './reports.controller'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.reports'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('ReportsController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, Event) => new ReportsController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('reportsDetails', [() => new ReportsDetailsDirective()])
module.directive('reportCreateEdit', [() => new ReportCreateEditDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.REPORTS_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['REPORTS'], 'reports', 'Reports', 'reports', 11)
        $stateProvider.state('reports', {
            url: '/reports',
            controller: 'ReportsController',
            template: require('./reports.tpl.html')
        })
    })
}])

export default moduleName