﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel, GeneratedReportModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper.ts'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'

interface ReportsScope extends ScopeBaseGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
}

export class ReportsController extends BaseGridController {
    constructor($scope: ReportsScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "reports",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_REPORTS_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_REPORTS_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            onGenerateReportClick: () => {                
                this.$scope.$broadcast(EventsNames.REPORT_GENERATE, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },

            permissions: {
                edit: "['" + Permissions.REPORT_EDIT + "']",
                add: "['" + Permissions.REPORT_ADD + "']",
                export: "['" + Permissions.REPORTS_EXPORT + "']",
                import: "['" + Permissions.REPORTS_IMPORT + "']",
                generateReport: "['" + Permissions.REPORT_GENERATE + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.REPORT_ADD, Permissions.REPORT_EDIT, Permissions.REPORTS_EXPORT, Permissions.REPORTS_IMPORT, Permissions.REPORT_GENERATE]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {                
                this.setSettingsListOptions(permissions, EventsNames.OPEN_REPORTS_CREATE_EDIT_POPUP)                
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {            
            $scope.dataGrid.refresh()
        })

        $scope.$on(EventsNames.REPORT_GENERATE, (event: ng.IAngularEvent, ...args: any[]) => {                   
            this.api.report.generateReport(args[0]).then((result: GeneratedReportModel) => {                
                if (result.resultString.toLowerCase() != false.toString()) {
                    CommonHelper.openInfoPopup(i18n('Common', 'ReportGenerated'))
                    if (result.reportType == CommonHelper.reportType.serverReportingServices) {                        
                        window.open(
                            result.resultString,
                            '_blank'
                        );
                    }
                }
                else CommonHelper.openInfoPopup(i18n('Common', 'ReportGenerateProblem'), 'error', 5000)
            })
        })

        $scope.$on(EventsNames.REPORTS_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getReportsEditFields',
            popupName: i18n('Popup', 'ImportReports'),
            requestMethodType: 'report'
            //requestMethodName: 'createReport'                                               
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_REPORTS_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }
        
        $scope.getItems = () => {
            return jQuery.when.apply(jQuery, [Permissions.REPORT_GENERATE]
                .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                var [REPORT_GENERATE] = permissions
                var items = []
                if (REPORT_GENERATE) 
                    items.push({
                        template: CommonHelper.getIconForDetails("REPORT_GENERATE", '#000000', '18', i18n('Grid', 'GenerateReport'), 'context-menu-row'), onItemClick: () => { this.$scope.$broadcast(EventsNames.REPORT_GENERATE, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField]) }
                    })                    
                return items
            })
        }
    }

    getReports = (request) => {        
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.report.getReports(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')            
            this.localDictionary.getReportsColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })        
    }

    showReportPopup = () => {
        var selectedRows = this.$scope.dataGrid.getSelectedRowsData()
        var statusIds = []
        var ids = []

        selectedRows.forEach(row => {
            ids.push(row['id'])
            statusIds.push(row['statusId'])
        })

        this.$scope.$broadcast(EventsNames.OPEN_REPORT_STATUS_CHANGE_POPUP, ids, statusIds)
    }

    drawGrid() {
        this.localDictionary.getReportsColumns().then(columns => {
            this.localDictionary.getReportsDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "ReportsGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    height: "100%",
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    }
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getReports, customOptions, true, this.localDictionary, Permissions.REPORTS_EXPORT, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
                this.$scope.dataGrid.refresh()
            })
        })
    }
}