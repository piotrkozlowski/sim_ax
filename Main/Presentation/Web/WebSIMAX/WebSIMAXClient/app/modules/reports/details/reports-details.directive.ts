﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { ReportDetailsDirectiveController } from './reports-details.directive.controller'

export class ReportsDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new ReportDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./reports-details.tpl.html")
}