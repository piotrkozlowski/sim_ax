﻿import 'angular'

import { Api, LocalDictionaryFactory } from  './../api/api.module'
import { LoggerService } from  './../common/common.module'
import { GridModel, GridRequestModel, ColumnModel } from '../api/api.models'
import { EventsNames } from '../../helpers/events-names'
import { CommonExtendedDataGridOptions } from '../common/common.extended-data-grid-options'
import { CommonHelper } from '../../helpers/common-helper.ts'
import { CommonGridButtonsOptions, ButtonConfig, ButtonsStates } from '../common/common.grid-buttons-options'
import { BaseGridController, ScopeBaseGrid } from '../common/common.base.grid.controller'
import { Permissions } from '../../helpers/permissions'
import { i18n } from '../common/i18n/common.i18n'

interface TasksScope extends ScopeBaseGrid {
    buttonsOptions: ButtonConfig[]
    buttonsStates: ButtonsStates
}

export class TasksController extends BaseGridController {
    constructor($scope: TasksScope, protected $compile: ng.ICompileService, $timeout: ng.ITimeoutService, protected api: Api, private logger: LoggerService, private localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $compile, () => {
            this.$scope.dataGridOptions = {
                height: '100%'
            }
            $timeout(() => {
                this.drawGrid()
            }, 1000)
        }, api)

        var { buttonsStates, buttonsOptions, popoverListButtonOptions } = new CommonGridButtonsOptions($scope, {
            scopeName: "tasks",
            getDataGrid: () => $scope.dataGrid,
            onShowClick: () => {
                this.$scope.showDetails({ data: $scope.dataGrid.getSelectedRowsData()[0] })
            },
            onAddClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_TASKS_CREATE_EDIT_POPUP + $scope.popupId, null)
            },
            onEditClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_TASKS_CREATE_EDIT_POPUP + $scope.popupId, $scope.dataGrid.getSelectedRowsData()[0][CommonHelper.idField])
            },
            onRefreshClick: () => {
                $scope.dataGrid.refresh()
            },
            onColumnFixingClick: () => {
                this.changeColumnFixing($scope.dataGrid)
            },
            onImportClick: () => {
                this.$scope.$broadcast(EventsNames.OPEN_IMPORT_POPUP)
            },
            onExportClick: () => {
                this.exportXls('tasks')
            },
            onPopoverListButtonClick: () => {
                this.onPopoverListButtonClick()
            },
            permissions: {
                edit: "['" + Permissions.TASK_EDIT + "']",
                add: "['" + Permissions.TASK_ADD + "']",
                export: "['" + Permissions.TASK_EXPORT + "']",
                import: "['" + Permissions.TASK_IMPORT + "']"
            }
        })

        $scope.buttonsStates = buttonsStates
        $scope.buttonsOptions = buttonsOptions
        $scope.popoverListButtonOptions = popoverListButtonOptions

        $timeout(() => {
            jQuery.when.apply(jQuery,
                [Permissions.TASK_ADD, Permissions.TASK_EDIT, Permissions.OPERATOR_EXPORT, Permissions.OPERATOR_IMPORT]
                    .map(p => Permissions.hasPermissions([p]))
            ).then((...permissions) => {
                permissions[2] = false //export not implemented yet
                this.setSettingsListOptions(permissions, EventsNames.OPEN_TASKS_CREATE_EDIT_POPUP)
            })
        }, 0)

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var isSaved = CommonHelper.getValueFromLocalStorage('taskSaved')
            if (isSaved) {
                if (!this.updateDataSource(args[0], this.$scope.dataGrid, this.$scope.dataGridOptions))
                    $scope.dataGrid.refresh()
                CommonHelper.removeValueFromLocalStorage('taskSaved')
            }
        })

        $scope.$on(EventsNames.TASK_DELETED, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.dataGrid.refresh()
        })

        $scope.importOptions = {
            dictionaryMethodName: 'getTaskEditFields',
            popupName: i18n('Popup', 'ImportTasks'),
            requestMethodType: 'task'
        }

        $scope.showDetails = (e) => {
            var itemIds = this.$scope.dataGrid.option("dataSource").items().map(item => {
                return {
                    id: item[CommonHelper.idField]
                }
            })
            this.$scope.$broadcast(EventsNames.OPEN_TASKS_DETAILS_POPUP + $scope.popupId, e.data[CommonHelper.idField], itemIds)
        }


        $scope.getItems = () => {
            return jQuery.when.apply(jQuery, [Permissions.ALLOWED_TASK_STATUS_EDIT].map(p => Permissions.hasPermissions([p]))).then((...permissions) => {
                var [ALLOWED_TASK_STATUS_EDIT] = permissions

                var items = []
                if (ALLOWED_TASK_STATUS_EDIT)
                    items.push({
                        template: CommonHelper.getIconForDetails("STATUS", '#000000', '18', i18n('Grid', 'ChangeTaskStatus'), 'context-menu-row'), onItemClick: () => { this.showStatusPopup() }
                    })
                return items
            })
        }
    }

    getTasks = (request) => {
        this.$scope.$root.$broadcast('disableLoadingIndicator')
        return this.api.task.getTasks(request).then(result => {
            this.$scope.$root.$broadcast('enableLoadingIndicator')
            this.localDictionary.getTasksColumns().then(cols => {
                this.refreshFilters(cols)
            })
            return result
        })
    }

    showStatusPopup = () => {
        var selectedRows = this.$scope.dataGrid.getSelectedRowsData()
        var statusIds = []
        var ids = []

        selectedRows.forEach(row => {
            ids.push(row['id'])
            statusIds.push(row['statusId'])
        })

        this.$scope.$broadcast(EventsNames.OPEN_TASKS_STATUS_CHANGE_POPUP, ids, statusIds)
    }

    drawGrid() {
        this.localDictionary.getTasksColumns().then(columns => {
            this.localDictionary.getTasksDefaultColumns().then(defaultColumns => {
                var columnsCount = defaultColumns.length
                if (columnsCount) {
                    this.$scope.columnsCount = columnsCount
                }
                else {
                    this.$scope.columnsCount = 0
                }
                var customOptions = {
                    name: "TasksGrid",
                    defaultColumnsDictionary: defaultColumns,
                    columnsDictionary: columns,
                    onContentReady: e => {
                        this.afterFilteringGridHeightFix(e)
                    },
                    height: '100%'
                }
                var gridOptions = new CommonExtendedDataGridOptions(CommonHelper.mainGrid, this.$scope, this.logger,
                    this.getTasks, customOptions, true, this.localDictionary, Permissions.TASK_EXPORT, this.$compile)
                this.$scope.dataGridOptions = gridOptions
                this.$scope.dataGrid.option(this.$scope.dataGridOptions)
                this.gridOptions = gridOptions
                this.$scope.dataGrid.refresh()
            })
        })
    }
}



