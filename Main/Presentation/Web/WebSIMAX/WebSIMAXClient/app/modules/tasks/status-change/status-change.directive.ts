﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel, TaskStatusModel, TaskStatusChangeModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

require('./status-change.scss')

interface StatusChangeScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupTitle: string
    popupVisible: boolean
    ids: number[]

    options: CreateEditPopupOptions
    dictionary: any
    toolbarOptions: DevExpress.ui.dxToolbarOptions

    selectBoxOptions: DevExpress.ui.dxSelectBoxOptions
    statuses: TaskStatusModel[]
    selected: any
}

export class StatusChangeDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: StatusChangeScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false

        $scope.popupOptions = {
            bindingOptions: {
                visible: 'popupVisible',
                title: 'popupTitle'
            },
            maxWidth: 400,
            maxHeight: 200,
            showTitle: true,
            closeOnBackButton: true,
            closeOnOutsideClick: true,
            onHiding: () => { }
        }

        $scope.selectBoxOptions = {
            displayExpr: 'name',
            valueExpr: 'id',
            bindingOptions: {
                dataSource: 'statuses',
                value: 'selected'
            },
            noDataText: i18n('Common', 'NoData'),
            placeholder: i18n('Common', 'Select')
        }

        $scope.toolbarOptions = {
            items: [
                {
                    location: 'after',
                    widget: 'dxButton',
                    options: {
                        text: i18n('Common', 'Apply'),
                        onClick: () => {
                            submitTaskStatus()
                        }
                    }
                },
                {
                    location: 'after',
                    widget: 'dxButton',
                    options: {
                        text: i18n('Common', 'Cancel'),
                        onClick: () => {
                            $scope.popupVisible = false
                        }
                    }
                }
            ]
        }

        $scope.$watch('ids', val => {
            $scope.popupTitle = i18n('Popup', 'TaskStatusChange') + ' (' + ($scope.ids ? $scope.ids.length : 0) + ')'
        })

        $scope.$on(EventsNames.OPEN_TASKS_STATUS_CHANGE_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            $scope.ids = args[0]
            var statusIds = args[1]

            api.task.getAvailableStatuses(statusIds).then(res => {
                if (res && res.result) {
                    $scope.statuses = res.result
                    $scope.popupVisible = true
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Grid', 'TaskStatusesError'), 'error', 4000)
                }
            })
        })

        var submitTaskStatus = () => {
            if ($scope.selected) {
                var model = new TaskStatusChangeModel()
                model.statusId = $scope.selected
                model.taskIds = $scope.ids
                api.task.saveStatuses(model).then(result => {
                    if (result && result.result) {
                        CommonHelper.openInfoPopup(i18n('Popup', 'TaskStatusesUpdated'))
                        CommonHelper.setValueToLocalStorage('taskSaved', true)
                        $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP)
                        $scope.popupVisible = false
                    }
                    else {
                        CommonHelper.openInfoPopup(i18n('Popup', 'TaskStatusUpdateError'), 'error', 5000)
                    }
                })
            }
            else {
                CommonHelper.openInfoPopup(i18n('Common', 'NothingSelected'), 'warning', 2000)
            }
        }

    }]
    template = require("./status-change.tpl.html")
    transclude = true
}