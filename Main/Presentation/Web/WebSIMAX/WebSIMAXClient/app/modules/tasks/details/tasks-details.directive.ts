﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { TasksDetailsDirectiveController } from './tasks-details.directive.controller'

require('./tasks-details.scss')

export class TasksDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }
    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new TasksDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]
    template = require("./tasks-details.tpl.html")
}