﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { TasksCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { TasksDetailsDirective } from './details/tasks-details.directive'
import { StatusChangeDirective } from './status-change/status-change.directive'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { TasksController } from './tasks.controller'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.tasks'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('TasksController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => new TasksController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('tasksDetails', [() => new TasksDetailsDirective()])
module.directive('tasksCreateEdit', [() => new TasksCreateEditDirective()])
module.directive('statusChange', [() => new StatusChangeDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.TASK_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['TASKS'], 'tasks', 'Tasks', 'tasks', 7)
        $stateProvider.state('tasks', {
            url: '/tasks',
            controller: 'TasksController',
            template: require('./tasks.tpl.html')
        })
    })
}])


export default moduleName