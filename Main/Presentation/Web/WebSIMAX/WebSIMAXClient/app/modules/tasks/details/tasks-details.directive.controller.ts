﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { TaskModel, FormValuesModel, FileModel } from '../../api/api.models'
import { CommonHelper } from '../../../helpers/common-helper'
import { EventsNames } from '../../../helpers/events-names'
import { UserConfigHelper } from './../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { GridData, GridBridge } from '../../common/common.interfaces'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
    attachments: any[]
    equipment: any[]
}

interface TasksDetailsScope extends DetailsScope {
    id: number
    itemObjects: any[]

    popupTitle: string
    common: CommonData
    // accordion
    attachments: GridData
    equipment: GridData
    attachmentsBridge: GridBridge
    equipmentBridge: GridBridge

    showTabs: boolean

    confirmDelete()
    editId: number
}

export class TasksDetailsDirectiveController extends Details<TasksDetailsScope> {
    private taskId: number
    private permissions: any[]

    constructor($scope: TasksDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.TaskPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'TaskDetails'),
            openEventName: EventsNames.OPEN_TASKS_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_TASKS_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.TASK_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.task
        $scope.common = <CommonData>{}
        $scope.editId = $scope.$id

        $scope.attachments = <GridData>{}
        $scope.equipment = <GridData>{}

        jQuery.when.apply(jQuery,
            [Permissions.TASK_PARAMETERS, Permissions.TASK_ATTACHMENTS, Permissions.TASK_HISTORY, Permissions.TASK_EQUIPMENT, Permissions.TASK_EDIT]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteTask'))
        }

        this.createEventEditForGridEntities(EventsNames.CLOSE_GRID_ATTACHMENTS_EDIT_POPUP + $scope.detailsPopupId + "attachments", 'attachments')

        $scope.$on(EventsNames.OPEN_TASKS_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var task: TaskModel = args[0]
            if (task && task.id == this.taskId) {
                this.updateData(task)
            }
        })

        $scope.$on(EventsNames.CLOSE_TASKS_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })
        $scope.$on(EventsNames.DOWNLOAD_TASK_ATTACHMENT + $scope.detailsPopupId, (event: ng.IAngularEvent, ...args: any[]) => {
            api.task.checkIfAttachmentExists(this.taskId, args[0]).then(message => {
                if (message.toUpperCase() == 'OK') {
                    CommonHelper.setJWTCookie(api.login.getToken())
                    window.open(window['config'].API_URL + 'api/app/task/' + this.taskId + '/attachments/' + args[0], '_self');
                }
                else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDownloadError') + ': ' + message, 'error', 5000)
                }
            })
        })

        $scope.confirmDelete = () => {
            api.task.deleteTask($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.TASK_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'TaskDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'TaskDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getTask = {}
        this.$scope.requestsStates.getTaskEquipment = {}
        this.$scope.requestsStates.getTaskAttachments = {}
        this.$scope.equipment = <GridData>{}
        this.$scope.attachments = <GridData>{}
        this.$scope.common.attachments = []
        this.$scope.common.equipment = []
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.task.getTask(id, this.$scope.requestsStates.getTask).then(result => {
            this.taskId = result.id
            this.$scope.popupTitle = result.name
            this.updateData(result)

            this.$scope.showTabs = true

            if (result.idLocation != null && result.idLocation != 0) {
                this.api.task.getEquipment(result.idLocation, this.$scope.requestsStates.getTaskEquipment).then(equipment => {
                    this.$scope.common.equipment = equipment.result
                    this.insertEntityData(this.$scope.common.equipment, 'getLocationEquipmentColumns', null, 'equipment')
                })
            }
            else {
                this.$scope.common.equipment = []
                this.insertEntityData(this.$scope.common.equipment, 'getLocationEquipmentColumns', null, 'equipment')
            }

        }).catch(reason => this.logger.log(reason))

        this.buildBridges()

        this.api.task.getAttachments(id, this.$scope.requestsStates.getTaskAttachments).then(result => {
            var attachments = result.result
                this.updateAttachments(attachments)
                this.$scope.common.attachments = attachments
                this.$scope.attachmentsBridge.items = attachments
                this.$scope.attachmentsBridge.title = i18n('Popup', 'AttachmentsEdit') + " (" + this.$scope.common.attachments.length + ")"

        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: TaskModel) {
        this.localDictionary.getTaskDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })
    }

    getTabs() {
        var [TASK_PARAMETERS, TASK_ATTACHMENTS, TASK_HISTORY, TASK_EQUIPMENT] = this.permissions

        var tabs = []

        if (TASK_PARAMETERS || TASK_ATTACHMENTS || TASK_EQUIPMENT) tabs.push({ template: CommonHelper.getIconForDetails("COMMON_DETAILS", '#000000', '18', i18n('Common', 'CommonX')), name: 'common' })
        if (TASK_HISTORY) tabs.push({ template: CommonHelper.getIconForDetails("HISTORY", '#000000', '18', i18n('Common', 'History')), name: 'history' })

        return tabs
    }

    getAccordions() {
        var [TASK_PARAMETERS, TASK_ATTACHMENTS, TASK_HISTORY, TASK_EQUIPMENT, TASK_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        if (TASK_PARAMETERS) {
            var items: DetailsAccordionSection[] = []

            items.push({
                title: i18n("Common", "Parameters"),
                canEdit: TASK_EDIT,
                name: 'params',
                showStatus: false,
                editMethod: () => {
                    if (TASK_EDIT)
                        this.$scope.$root.$broadcast(EventsNames.OPEN_TASKS_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
                },
                visible: true
            })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.TaskAccordionsSettings, 0, items))
        }

        if (TASK_ATTACHMENTS || TASK_EQUIPMENT) {
            var items: DetailsAccordionSection[] = []

            if (TASK_ATTACHMENTS)
                items.push({
                    title: i18n("Common", "Attachments"),
                    canEdit: TASK_EDIT,
                    name: 'attachments',
                    showStatus: true,
                    editMethod: () => {
                        if (TASK_EDIT)
                            this.$scope.$broadcast(EventsNames.OPEN_GRID_ATTACHMENTS_EDIT_POPUP + this.$scope.detailsPopupId + 'attachments')
                    },
                    status: () => {
                        return i18n("Common", "InstalledCount") + this.$scope.common.attachments.length
                    },
                    visible: true
                })

            if (TASK_EQUIPMENT)
                items.push({
                    title: i18n("Common", "Equipment"),
                    canEdit: false,
                    name: 'equipment',
                    showStatus: true,
                    status: () => {
                        return i18n('Common', 'InstalledCount') + this.$scope.common.equipment.length
                    },
                    visible: true
                })

            accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.TaskAccordionsSettings, 1, items))
        }        

        return accordions
    }

    updateAttachments(result) {
        this.insertEntityData(result, 'getAttachmentColumns', EventsNames.DOWNLOAD_TASK_ATTACHMENT, 'attachments')
    }

    buildBridges() {
        this.$scope.attachmentsBridge = {
            id: this.$scope.modelId,
            title: i18n('Popup', 'AttachmentsEdit')  + " (" + this.$scope.common.attachments.length + ")",
            items: [],
            popupId: this.$scope.detailsPopupId + 'attachments',
            dictionaryMethodName: 'getAttachmentColumns',
            dataMethodName: 'getAttachments',
            dataScope: 'task',
            onAdd: (added: FileModel) => {
                added.parentEntityId = this.$scope.modelId
                return this.saveAttachment(added)
            },
            onDelete: (removed: number[]) => {
                var model = new FormValuesModel
                model.id = this.$scope.modelId
                model.values = removed
                return this.deleteAttachments(model)
            }
        }
    }

    saveAttachment(attachment: FileModel) {
        var deferred = this.$q.defer()
        this.api.task.saveAttachment(attachment).then(response => {
            if (!response.fileModel) {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaveError') + ': ' + response.status, 'error', 5000, 500)
                deferred.reject()
            }
            else {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaved'))
                deferred.resolve(response.fileModel)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentSaveError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }


    deleteAttachments(model: FormValuesModel) {
        var deferred = this.$q.defer<number[]>()
        this.api.task.deleteAttachments(model).then(response => {
            if (!response) {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleteError'), 'error', 5000)
                deferred.reject()
            }
            else {
                CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleted'))
                deferred.resolve(response.result)
            }
        }).catch(err => {
            CommonHelper.openInfoPopup(i18n('Popup', 'AttachmentDeleteError'), 'error', 5000)
            deferred.reject()
        })
        return deferred.promise;
    }
}