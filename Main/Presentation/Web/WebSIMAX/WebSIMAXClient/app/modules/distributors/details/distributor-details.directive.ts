﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { DistributorDetailsDirectiveController } from './distributor-details.directive.controller'

export class DistributorDetailsDirective {
    restrict = 'E'
    scope = {
        id: '='
    }

    controller = ['$scope', '$q', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $q: ng.IQService, $timeout: ng.ITimeoutService, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) => {
        return new DistributorDetailsDirectiveController($scope, $q, $timeout, api, logger, localDictionary)
    }]

    template = require("./distributor-details.tpl.html")
}