﻿import 'angular'
import 'angular-ui-router'

import { default as commonModule, LoggerService } from  './../common/common.module'
import { default as apiModule, Api, LocalDictionaryFactory } from  './../api/api.module'
import { MenuItem } from '../common/sidebar-menu/common.sidebar-menu.directive'

import { DistributorsController } from './distributors.controller'
import { DistributorCreateEditDirective } from './create-edit/create-edit-popup.directive'
import { DistributorDetailsDirective } from './details/distributor-details.directive'
import { Permissions } from '../../helpers/permissions'

var moduleName = 'simax.distributors'
var module = angular.module(moduleName, [require('angular-animate'), require('angular-sanitize'), 'dx', 'ui.router', commonModule, apiModule])

module.controller('DistributorsController', ['$scope', '$compile', '$timeout', 'api', 'logger', 'localDictionary', ($scope, $compile: ng.ICompileService, $timeout, api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory, Event) => new DistributorsController($scope, $compile, $timeout, api, logger, localDictionary)])

module.directive('distributorCreateEdit', [() => new DistributorCreateEditDirective()])
module.directive('distributorDetails', [() => new DistributorDetailsDirective()])

module.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
    Permissions.runIfHasPermissions([Permissions.DISTRIBUTOR_LIST], () => {
        MenuItem.addMenuItem(window['config']['ICONS']['DISTRIBUTORS'], 'distributors', 'Distributors', 'distributors', 8)
        $stateProvider.state('distributors', {
            url: '/distributors',
            controller: 'DistributorsController',
            template: require('./distributors.tpl.html')
        })
    })
}])

export default moduleName