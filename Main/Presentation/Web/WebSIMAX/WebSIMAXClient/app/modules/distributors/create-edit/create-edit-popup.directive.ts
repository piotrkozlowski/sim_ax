﻿import 'angular'
import { Api, LocalDictionaryFactory } from  './../../api/api.module'
import { ColumnModel, FieldNameValueModel, FormValuesModel } from '../../api/api.models'
import { LoggerService } from  '../../common/common.module'
import { EventsNames } from '../../../helpers/events-names'
import { CreateEditPopupOptions } from '../../common/create-edit-popup/create-edit-popup.directive.ts'
import { CommonHelper } from '../../../helpers/common-helper'
import { PopupType } from '../../../helpers/popup-manager'
import { i18n } from '../../common/i18n/common.i18n'

interface CreateEditScope extends ng.IScope {
    popupOptions: DevExpress.ui.dxPopupOptions
    popupVisible: boolean
    
    options: CreateEditPopupOptions
    dictionary: any
    editedItem: any
    id: number
}

export class DistributorCreateEditDirective implements ng.IDirective {
    constructor() {

    }

    scope = {
        id: "="
    }
    restrict = 'E'
    controller = ['$scope', '$q', 'api', 'localDictionary', ($scope: CreateEditScope, $q: ng.IQService, api: Api, localDictionary: LocalDictionaryFactory) => {

        $scope.popupVisible = false
        $scope.options = {
            title: "",
            saveButtonText: i18n('Common', 'Apply'),
            type: PopupType.DISTRIBUTOR_POPUP,
            onSubmit: (fields: FieldNameValueModel[], id: number) => {
                return submitDistributor(id, fields)
            }
        }
        var loadColumns = (isEdit: boolean, referenceId: number) => {
            return $q.all([localDictionary.getDistributorEditFields(), localDictionary.getDistributorRequiredFields()]).then(([columnsEdit, required]) => {
                return {
                    name: i18n("Common", "Basics"),
                    type: "form",
                    columns: columnsEdit,
                    required: required
                }
            }).then(dict => {
                $scope.dictionary = [dict]
            })
        }
        $scope.$on(EventsNames.OPEN_DISTRIBUTORS_CREATE_EDIT_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            var [modelId] = args

            if (modelId) {
                $scope.options.title = i18n('Popup', 'DistributorEdit')
                if (typeof modelId == "number") {
                    api.distributor.getDistributorToEdit(modelId).then(result => {
                        loadColumns(modelId, result.referenceId).then(() => {
                            $scope.editedItem = result.fields
                            $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, modelId)
                        })
                    })
                    return
                } else {
                    $scope.editedItem = modelId
                }
            } else {
                $scope.editedItem = null
                $scope.options.title = i18n('Popup', 'DistributorAdd')
            }
            loadColumns(modelId, null).then(() => {
                $scope.$broadcast(EventsNames.OPEN_CREATE_EDIT_POPUP, $scope.dictionary, $scope.editedItem, modelId)
            })
        })

        var submitDistributor = (id: number, fields: FieldNameValueModel[]) => {
            var formModel = new FormValuesModel()
            formModel.id = id
            formModel.fields = fields

            return api.distributor.createDistributor(formModel).then(loc => {
                if (!loc) {
                    CommonHelper.openInfoPopup(i18n('Popup', 'DistributorSaveError'), 'error', 5000)
                } else {
                    CommonHelper.openInfoPopup(i18n('Popup', 'DistributorSaved'))
                    CommonHelper.setValueToLocalStorage('distributorSaved', true)
                    $scope.$root.$broadcast(EventsNames.CLOSE_CREATE_EDIT_POPUP, loc)
                }
            }).catch(err => {
                CommonHelper.openInfoPopup(i18n('Popup', 'DistributorSaveError'), 'error', 5000)
                console.log(err)
            })
        }

    }]
    template = require("./create-edit-popup.tpl.html")
    transclude = true
}