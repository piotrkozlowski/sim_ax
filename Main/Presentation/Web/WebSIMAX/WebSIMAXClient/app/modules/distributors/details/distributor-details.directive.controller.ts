﻿import 'angular'
import { Api, LocalDictionaryFactory } from  '../../api/api.module'
import { LoggerService } from  '../../common/common.module'
import { DistributorModel} from '../../api/api.models'
import { EventsNames } from '../../../helpers/events-names'
import { CommonHelper } from '../../../helpers/common-helper'
import { UserConfigHelper } from '../../../helpers/user-config-helper'
import { i18n } from '../../common/i18n/common.i18n'
import { Details, DetailsScope, DetailsAccordion, DetailsAccordionSection } from '../../common/common.details'
import { Permissions } from '../../../helpers/permissions'
import { DetailsPopupOptions } from '../../common/details-popup/details-popup.directive'

interface CommonData {
    details: any
}

interface DistributorDetailsScope extends DetailsScope {
    id: number
    editId: number
    itemObjects: any[]
    popupTitle: string

    common: CommonData

    showTabs: boolean

    confirmDelete()
}

export class DistributorDetailsDirectiveController extends Details<DistributorDetailsScope> {
    private permissions: any[]

    constructor($scope: DistributorDetailsScope, $q: ng.IQService, $timeout: ng.ITimeoutService, private api: Api, logger: LoggerService, localDictionary: LocalDictionaryFactory) {
        super($scope, $timeout, $q, logger, localDictionary, UserConfigHelper.DistributorPopupTabActive)

        $scope.options = <DetailsPopupOptions>{
            title: i18n('Popup', 'DistributorDetails'),
            openEventName: EventsNames.OPEN_DISTRIBUTORS_DETAILS_POPUP,
            closeEventName: EventsNames.CLOSE_DISTRIBUTORS_DETAILS_POPUP,
            deleteMethod: () => { return $scope.openDeleteConfirmationPopup() },
            delHasPermissions: Permissions.DISTRIBUTOR_DEL,
            id: $scope.id,
            getAccordionDataMethod: () => { return this.getAccordionData() },
            getAccordionSelectionMethod: () => { return this.getAccordionSelection() },
            updateAccordionMethod: (names: any[]) => { return this.updateAccordions(names) }
        }

        this.accordionsNameToShow = 'common'

        $scope.popupFor = CommonHelper.distributor
        $scope.common = <CommonData>{}
        $scope.editId = $scope.$id

        jQuery.when.apply(jQuery,
            [Permissions.DISTRIBUTOR_EDIT]
                .map(p => Permissions.hasPermissions([p]))
        ).then((...permissions) => {
            this.permissions = permissions
            this.configureTabs(this.getTabs())
        })

        $scope.openDeleteConfirmationPopup = () => {
            $scope.$broadcast(EventsNames.OPEN_CONFIRMATION_POPUP + $scope.id, i18n('Popup', 'ConfirmDeleteDistributor'))
        }

        $scope.$on(EventsNames.OPEN_DISTRIBUTORS_DETAILS_POPUP + $scope.id, (event: ng.IAngularEvent, ...args: any[]) => {
            this.loadData(args[0], args[1])
        })

        $scope.$on(EventsNames.CLOSE_CREATE_EDIT_POPUP, (event: ng.IAngularEvent, ...args: any[]) => {
            var distributor: DistributorModel = args[0]
            if (distributor.id == $scope.modelId) {
                this.updateData(distributor)
            }
        })

        // close
        $scope.$on(EventsNames.CLOSE_DISTRIBUTORS_DETAILS_POPUP + $scope.id, () => {
            this.hidePopup()
        })

        // delete
        $scope.confirmDelete = () => {
            api.distributor.deleteDistributor($scope.modelId).then(result => {
                $scope.$root.$broadcast(EventsNames.DISTRIBUTOR_DELETED)
                this.hidePopup()
                CommonHelper.openInfoPopup(i18n('Popup', 'DistributorDeleted'))
            }).catch(rej => {
                CommonHelper.openInfoPopup(i18n('Popup', 'DistributorDeleteError'), 'error', 4000)
            })
        }
    }

    resetData() {
        this.$scope.popupTitle = ''
        this.$scope.showTabs = false
        this.$scope.requestsStates.getDistributor = {}
    }

    loadData(id: number, objects: any[]) {
        this.resetData()

        this.$scope.modelId = id
        this.$scope.itemObjects = objects

        this.showPopup()

        this.api.distributor.getDistributor(id, this.$scope.requestsStates.getDistributor).then(result => {
            this.$scope.popupTitle = result.name

            this.$scope.showTabs = true

            this.updateData(result)
        }).catch(reason => this.logger.log(reason))
    }

    updateData(result: DistributorModel) {
        this.localDictionary.getDistributorDisplayFields().then(displayFields => {
            this.$scope.common.details = result ? Details.filterFields(result.fields, displayFields) : []
        })
    }

    getTabs() {
        var tabs = []
        tabs.push({ text: i18n('Common', 'CommonX'), icon: "home", name: 'common' })
        return tabs
    }

    getAccordions() {
        var [DISTRIBUTOR_EDIT] = this.permissions

        var accordions: DetailsAccordion[] = []

        var items: DetailsAccordionSection[] = []

        items.push({
            title: i18n("Common", "Parameters"),
            canEdit: DISTRIBUTOR_EDIT,
            name: 'params',
            showStatus: false,
            editMethod: () => {
                if (DISTRIBUTOR_EDIT)
                    this.$scope.$root.$broadcast(EventsNames.OPEN_DISTRIBUTORS_CREATE_EDIT_POPUP + this.$scope.editId, this.$scope.modelId)
            },
            visible: true
        })

        accordions.push(new DetailsAccordion(this.$scope, this.$timeout, UserConfigHelper.DistributorDetailsAccordions, 0, items))

        return accordions
    }
}