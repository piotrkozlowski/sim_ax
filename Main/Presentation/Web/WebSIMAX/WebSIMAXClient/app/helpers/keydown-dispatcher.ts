﻿import { CommonHelper } from './common-helper'

export class KeydownDispatcher {
    static instance: KeydownDispatcher
    static keyA: number = 65
    static crtlA: string = 'crtlA'
    private listenersTable: any = {}
    constructor() {// not to be used from outside
        window.addEventListener('keydown', this.dispatch.bind(this))
    }



    static getInstance() {
        if (!this.instance) {
            this.instance = new KeydownDispatcher()
        }
        return this.instance
    }

    addListener(shortCut: string, func: any) {
        if (!this.listenersTable[shortCut]) {
            this.listenersTable[shortCut] = [];
        }
        this.listenersTable[shortCut].push(func)
    }

    removeListener(shortCut: string, funcIndex: number) {
        this.listenersTable[shortCut] = this.listenersTable[shortCut].filter(f => {
            return f[CommonHelper.dispatcherIndex] != funcIndex
        })
    }

    private dispatch(e) {
        if (e.ctrlKey) {
            if (e.keyCode == KeydownDispatcher.keyA) {
                this.listenersTable[KeydownDispatcher.crtlA].map(func => {
                    e.preventDefault()
                    func[CommonHelper.dispatcherFunction]()
                })
            }
        }
    }

}