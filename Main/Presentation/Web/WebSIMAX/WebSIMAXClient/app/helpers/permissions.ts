﻿export class Permissions {
    private static deferred: JQueryDeferred<string[]> = $.Deferred<string[]>().done(values => {
        Permissions.values = values
    })
    private static values: string[]
    private static resolved: boolean = false

    private static checkPermissions(permissions: string[], userPermissions: string[]): boolean {
        var hasPermissions = true
        permissions.forEach(p => {
            hasPermissions = userPermissions.some(up => p == up)
        })
        return hasPermissions
    }

    private static permissionsResolved(): boolean {
        if (!Permissions.resolved) {
            if (Permissions.deferred.state() == 'resolved') {
                Permissions.resolved = true
                return true
            }
        } else return true
        return false
    }

    public static setPermissions(permissions: string[]) {
        Permissions.deferred.resolve(permissions)
    }

    public static hasPermissionsResolved(permissions: string[]): boolean {
        if (Permissions.permissionsResolved()) {
            return Permissions.checkPermissions(permissions, Permissions.values)
        } else return false
    }

    public static hasPermissions(permissions: string[]): JQueryPromise<boolean> {
        return Permissions.deferred.promise().then(userPermissions => {
            return Permissions.checkPermissions(permissions, userPermissions)
        })
    }

    public static runIfHasPermissions(permissions: string[], run: () => void) {
        Permissions.hasPermissions(permissions).then(hasPermissions => {
            if (hasPermissions) run()
        })
    }

    public static refresh() {
        Permissions.deferred = $.Deferred()
        Permissions.deferred.done(values => {
            Permissions.values = values
        })
    }

    static WEBSIMAX_MAIN_MENU_VIEW = "WEBSIMAX_MAIN_MENU_VIEW"

    static DASHBOARD_VIEW = "DASHBOARD_VIEW"

    static ALARM_LIST = "ALARM_LIST"
    static ALARM_ADD = "ALARM_ADD"
    static ALARM_EDIT = "ALARM_EDIT"
    static ALARM_DEL = "ALARM_DEL"
    static ALARMS_CONFIRMATION_ENABLED = "ALARMS_CONFIRMATION_ENABLED"
    static ALARM_PARAMETERS  = "ALARM_PARAMETERS"
    static ALARM_MAP = "ALARM_MAP"
    static ALARM_HISTORY = "ALARM_HISTORY"

    static DISTRIBUTOR_LIST = "DISTRIBUTOR_LIST"
    static DISTRIBUTOR_EDIT = "DISTRIBUTOR_EDIT"
    static DISTRIBUTOR_ADD = "DISTRIBUTOR_ADD"
    static DISTRIBUTOR_DEL = "DISTRIBUTOR_DEL"

    static MEASUREMENTS_LIST = "MEASUREMENTS_LIST"

    static LOCATION_LIST = "LOCATION_LIST"
    static LOCATION_EDIT = "LOCATION_EDIT"
    static LOCATION_ADD = "LOCATION_ADD"
    static LOCATION_DEL = "LOCATION_DEL"
    static LOCATION_EXPORT = "LOCATION_EXPORT"
    static LOCATION_IMPORT = "LOCATION_IMPORT"
    static LOCATION_PARAMETERS = "LOCATION_PARAMETERS"
    static LOCATION_HISTORY = "LOCATION_HISTORY"
    static LOCATION_GROUPS = "LOCATION_GROUPS"
    static LOCATION_INSTALLED_DEVICES = "LOCATION_INSTALLED_DEVICES"
    static LOCATION_INSTALLED_METERS = "LOCATION_INSTALLED_METERS"
    static LOCATION_SCHEMA = "LOCATION_SCHEMA"
    static LOCATION_GENERAL_SCHEMA = "LOCATION_GENERAL_SCHEMA"
    static LOCATION_EXTENDED_PARAMETERS = "LOCATION_EXTENDED_PARAMETERS"
    static LOCATION_MAP = "LOCATION_MAP"
    static LOCATION_MEASUREMENT = "LOCATION_MEASUREMENT"
    static LOCATION_MEASUREMENT_PANEL = "LOCATION_MEASUREMENT_PANEL"

    static DEVICE_LIST = "DEVICE_LIST"
    static DEVICE_EDIT = "DEVICE_EDIT"
    static DEVICE_ADD = "DEVICE_ADD"
    static DEVICE_DEL = "DEVICE_DEL"
    static DEVICE_EXPORT = "DEVICE_EXPORT"
    static DEVICE_IMPORT = "DEVICE_IMPORT"
    static DEVICE_PARAMETERS = "DEVICE_PARAMETERS"
    static DEVICE_HISTORY = "DEVICE_HISTORY"
    static DEVICE_EXTENDED_PARAMETERS = "DEVICE_EXTENDED_PARAMETERS"
    static DEVICE_PARENT_DEVICES = "DEVICE_PARENT_DEVICES"
    static DEVICE_CHILD_DEVICES = "DEVICE_CHILD_DEVICES"
    static DEVICE_GENERAL_SCHEMA = "DEVICE_GENERAL_SCHEMA"
    static DEVICE_MEASUREMENT = "DEVICE_MEASUREMENT"

    static DEVICE_CONNECTION_LIST = "DEVICE_CONNECTION_LIST"

    static METER_LIST = "METER_LIST"
    static METER_EDIT = "METER_EDIT"
    static METER_ADD = "METER_ADD"
    static METER_DEL = "METER_DEL"
    static METER_EXPORT = "METER_EXPORT"
    static METER_IMPORT = "METER_IMPORT"
    static METER_PARAMETERS = "METER_PARAMETERS"
    static METER_HISTORY = "METER_HISTORY"
    static METER_DEVICES = "METER_DEVICES"
    static METER_GENERAL_SCHEMA = "METER_GENERAL_SCHEMA"
    static METER_ALARM_GROUPS = "METER_ALARM_GROUPS"
    static METER_ALARM_DEFINITIONS = "METER_ALARM_DEFINITIONS"

    static OPERATOR_LIST = "OPERATOR_LIST"
    static OPERATOR_EDIT = "OPERATOR_EDIT"
    static OPERATOR_ADD = "OPERATOR_ADD"
    static OPERATOR_DEL = "OPERATOR_DEL"
    static OPERATOR_EXPORT = "OPERATOR_EXPORT"
    static OPERATOR_IMPORT = "OPERATOR_IMPORT"
    static OPERATOR_NOTIFICATIONS_LIST = "OPERATOR_NOTIFICATIONS_LIST"
    static OPERATOR_NOTIFICATIONS_EDIT = "OPERATOR_NOTIFICATIONS_EDIT"
    static OPERATOR_PERMISSIONS_EDIT = "OPERATOR_PERMISSIONS_EDIT"
    static OPERATOR_EFFECTIVE_PERMISSIONS = "OPERATOR_EFFECTIVE_PERMISSIONS"
    static ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES = "ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES" //edycja ról i uprawnień niestandardowych
    static OPERATOR_PERMISSIONS_ROLE_CHOOSE = "OPERATOR_PERMISSIONS_ROLE_CHOOSE"  //widok ról
    static OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE = "OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE" //widok uprawnień niestandardowych

    static ISSUE_LIST = "ISSUE_LIST"
    static ISSUE_EDIT = "ISSUE_EDIT"
    static ISSUE_ADD = "ISSUE_ADD"
    static ISSUE_DEL = "ISSUE_DEL"
    static ISSUE_PARAMETERS = "ISSUE_PARAMETERS"
    static ISSUE_HISTORY = "ISSUE_HISTORY"
    static ISSUE_TASKS = "ISSUE_TASKS"
    static ISSUE_ATTACHMENTS = "ISSUE_ATTACHMENTS"
    static ISSUE_EQUIPMENT = "ISSUE_EQUIPMENT"

    static TASK_LIST = "TASK_LIST"
    static TASK_EDIT = "TASK_EDIT"
    static TASK_ADD = "TASK_ADD"
    static TASK_DEL = "TASK_DEL"
    static TASK_EXPORT = "TASK_EXPORT"
    static TASK_IMPORT = "TASK_IMPORT"
    static TASK_PARAMETERS = "TASK_PARAMETERS"
    static TASK_ATTACHMENTS = "TASK_ATTACHMENTS"
    static TASK_HISTORY = "TASK_HISTORY"
    static TASK_EQUIPMENT = "TASK_EQUIPMENT"
    static ALLOWED_TASK_STATUS_EDIT = "ALLOWED_TASK_STATUS_EDIT"

    static ROUTE_LIST = "ROUTE_LIST"
    static ROUTE_EDIT = "ROUTE_EDIT"
    static ROUTE_ADD = "ROUTE_ADD"
    static ROUTE_DEL = "ROUTE_DEL"
    static ROUTE_EXPORT = "ROUTE_EXPORT"
    static ROUTE_IMPORT = "ROUTE_IMPORT"
    static ROUTE_POINTS = "ROUTE_POINTS"
    static ROUTE_ASSIGNED_OPERATOR = "ROUTE_ASSIGNED_OPERATOR"
    static ROUTE_PARAMETERS = "ROUTE_PARAMETERS"
    static ROUTE_MAP = "ROUTE_MAP"

    static REPORTS_LIST = "REPORTS_LIST"
    static REPORT_ADD = "REPORT_ADD"
    static REPORT_EDIT = "REPORT_EDIT"
    static REPORTS_EXPORT = "REPORTS_EXPORT"
    static REPORTS_IMPORT = "REPORTS_IMPORT"
    static REPORTS_MAP = "REPORTS_MAP"
    static REPORT_PARAMETERS = "REPORT_PARAMETERS"
    static REPORT_DEL = "REPORT_DEL"
    static REPORT_HISTORY = "REPORT_HISTORY"
    static REPORT_GENERATE = "REPORT_GENERATE"

    static SIM_CARD_LIST = "SIM_CARD_LIST"
    static SIM_CARD_ADD = "SIM_CARD_ADD"
    static SIM_CARD_EDIT = "SIM_CARD_EDIT"
    static SIM_CARD_DEL = "SIM_CARD_DEL"
    static SIM_CARD_HISTORY = "SIM_CARD_HISTORY"
    static SIM_CARD_EXPORT_OP = "SIM_CARD_EXPORT_OP"
    static SIM_CARD_IMPORT_OP = "SIM_CARD_IMPORT_OP"

    static CONTACT_LIST = "CONTACT_LIST"
    static CONTACT_EDIT = "CONTACT_EDIT"
    static CONTACT_GROUPS_EDIT = "CONTACT_GROUPS_EDIT"
    static CONTACT_LOCATIONS_EDIT = "CONTACT_LOCATIONS_EDIT"
    static CONTACT_ADD = "CONTACT_ADD"
    static CONTACT_DEL = "CONTACT_DEL"
    static CONTACT_EXPORT = "CONTACT_EXPORT"
    static CONTACT_IMPORT = "CONTACT_IMPORT"

    static ACTION_LIST = "ACTION_LIST"
    static ACTION_EDIT = "ACTION_EDIT"
    static ACTION_DEL = "ACTION_DEL"
    static ACTION_PARAMETERS = "ACTION_PARAMETERS"
}