﻿import { OperatorSettingsModel  } from '../modules/api/api.models'
import { Api } from  '../modules/api/api.module'
import { CommonHelper } from '../helpers/common-helper'

export class UserConfigHelper {
    private static saveTimer
    private static USER_CONFIG = CommonHelper.domain + 'USER_CONFIG'

    private static getStorage(): {} {
        var json = localStorage.getItem(this.USER_CONFIG)
        var storage = json ? JSON.parse(json) : {}
        return storage
    }

    private static saveStorage(storage: {}) {
        localStorage.setItem(this.USER_CONFIG, JSON.stringify(storage))
    }
    
    static setUserConfig(name: string, value: any) {
        var storage = this.getStorage()
        if (value != null) {
            storage[name] = value
        } else {
            delete storage[name]
        }
        this.saveStorage(storage)
    }

    static getUserConfig<T>(name: string, defaultValue?: T): T {
        var json = localStorage.getItem(this.USER_CONFIG);
        var storage = json ? JSON.parse(json) : {}
        var result = storage[name]
        if (result) {
            return result
        }
        return defaultValue
    }

    static saveSettingsToDB(api: Api, time: number = 5000) {
        var deferred = $.Deferred<void>()
        if (this.saveTimer) clearTimeout(this.saveTimer)
        this.saveTimer = setTimeout(() => {
            var user = api.login.getUser()
            if (user) {
                var storage = localStorage.getItem(this.USER_CONFIG)
                var request: OperatorSettingsModel = {
                    idOperator: user.id,
                    settings: storage
                }
                api.secureapp.saveOperatorSettings(request).then(() => {
                    deferred.resolve()
                })
            }
        }, time)
        return deferred.promise()
    }

    static loadSettingsFromDb(api: Api, id: number) {
        return api.secureapp.getOperatorSettings({ id: id }).then(operatorSettings => {
            if (operatorSettings && operatorSettings.settings) {
                localStorage.setItem(this.USER_CONFIG, operatorSettings.settings)
            }
        })
    }
    
    static StateBeforeAuthRejection = "StateBeforeAuthRejection"
    static LocationPopupTabActive = "LocationPopupTabActive"
    static LocationAccordionsSettings = "LocationAccordionsSettings"
    static DevicePopupTabActive = "DevicePopupTabActive"
    static MeterPopupTabActive = "MeterPopupTabActive"
    static TaskPopupTabActive = "TaskPopupTabActive"
    static TaskAccordionsSettings = "TaskAccordionsSettings"
    static IssuePopupTabActive = "IssuePopupTabActive"
    static UserPopupTabActive = "UserPopupTabActive"
    static UserPopupSecurityTabActive = "UserPopupSecurityTabActive"
    static UserPopupNotificationsTabActive = "UserPopupNotificationsTabActive"
    static UserAccordionsSettings = "UserAccordionsSettings"
    static DistributorPopupTabActive = "DistributorPopupTabActive"
    static SecurityModuleId = "SecurityModuleId"
    static MeasurementsGridType = "MeasurementsGridType"
    static RoutePopupTabActive = "RoutePopupTabActive" 
    static AlarmPopupTabActive = "AlarmPopupTabActive" 
    static AlarmAccordionsSettings = "AlarmAccordionsSettings"
    static SmallMenuVisibility = "smallMenu"
    static ToDate = "ToDate"
    static FromDate = "FromDate"
    static IsLegendVisible = "IsLegendVisible"
    static IsRotated = "IsRotated"
    static ContactPopupTabActive = "ContactPopupTabActive"
    static SimcardPopupTabActive = "SimcardPopupTabActive"
    static SimcardDetailsAccordions = "simcardDetailsAccordions"
    static DeviceDetailsAccordions = "deviceDetailsAccordions"
    static taskDetailsAccordions = "taskDetailsAccordions"
    static IssueDetailsAccordions = "IssueDetailsAccordions"
    static DistributorDetailsAccordions = "DistributorDetailsAccordions"   
    static RouteDetailsAccordions = "RouteDetailsAccordions"
    static ContactDetailsAccordions = "ContactDetailsAccordions"
    static MeterDetailsAccordions = "MeterDetailsAccordions"
    static ReportAccordionBellowActive = "reportAccordionBellowActive"
    static ReportDetailsAccordions = "reportDetailsAccordions"
    static ReportPopupTabActive = "reportPopupTabActive"
}