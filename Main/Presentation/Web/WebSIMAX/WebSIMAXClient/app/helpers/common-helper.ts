﻿import { LoginService } from '../modules/api/services/api.login.service'
import { UserConfigHelper } from './user-config-helper'
import { EntityModel, ColumnModel, GridModel } from '../modules/api/api.models'
import { i18n } from '../modules/common/i18n/common.i18n'
import * as moment from 'moment'

var base64: any = {}
base64.PADCHAR = '=';
base64.ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

base64.makeDOMException = function () {
    // sadly in FF,Safari,Chrome you can't make a DOMException
    var e, tmp;

    try {
        return new DOMException();
    } catch (ex) {
        // not available, just passback a duck-typed equiv
        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error
        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error/prototype
        var ex = new Error("DOM Exception 5");

        // ex.number and ex.description is IE-specific.
        ex.code = ex.number = 5;
        ex.name = ex.description = "INVALID_CHARACTER_ERR";

        // Safari/Chrome output format
        ex.toString = function () { return 'Error: ' + ex.name + ': ' + ex.message; };
        return ex;
    }
}

base64.getbyte64 = function (s, i) {
    // This is oddly fast, except on Chrome/V8.
    //  Minimal or no improvement in performance by using a
    //   object with properties mapping chars to value (eg. 'A': 0)
    var idx = base64.ALPHA.indexOf(s.charAt(i));
    if (idx === -1) {
        throw base64.makeDOMException();
    }
    return idx;
}

base64.decode = function (s) {
    // convert to string
    s = '' + s;
    var getbyte64 = base64.getbyte64;
    var pads, i, b10;
    var imax = s.length
    if (imax === 0) {
        return s;
    }

    if (imax % 4 !== 0) {
        throw base64.makeDOMException();
    }

    pads = 0
    if (s.charAt(imax - 1) === base64.PADCHAR) {
        pads = 1;
        if (s.charAt(imax - 2) === base64.PADCHAR) {
            pads = 2;
        }
        // either way, we want to ignore this last block
        imax -= 4;
    }

    var x = [];
    for (i = 0; i < imax; i += 4) {
        b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12) |
            (getbyte64(s, i + 2) << 6) | getbyte64(s, i + 3);
        x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff, b10 & 0xff));
    }

    switch (pads) {
        case 1:
            b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12) | (getbyte64(s, i + 2) << 6);
            x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff));
            break;
        case 2:
            b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12);
            x.push(String.fromCharCode(b10 >> 16));
            break;
    }
    return x.join('');
}

base64.getbyte = function (s, i) {
    var x = s.charCodeAt(i);
    if (x > 255) {
        throw base64.makeDOMException();
    }
    return x;
}

base64.encode = function (s) {
    if (arguments.length !== 1) {
        throw new SyntaxError("Not enough arguments");
    }
    var padchar = base64.PADCHAR;
    var alpha = base64.ALPHA;
    var getbyte = base64.getbyte;

    var i, b10;
    var x = [];

    // convert to string
    s = '' + s;

    var imax = s.length - s.length % 3;

    if (s.length === 0) {
        return s;
    }
    for (i = 0; i < imax; i += 3) {
        b10 = (getbyte(s, i) << 16) | (getbyte(s, i + 1) << 8) | getbyte(s, i + 2);
        x.push(alpha.charAt(b10 >> 18));
        x.push(alpha.charAt((b10 >> 12) & 0x3F));
        x.push(alpha.charAt((b10 >> 6) & 0x3f));
        x.push(alpha.charAt(b10 & 0x3f));
    }
    switch (s.length - imax) {
        case 1:
            b10 = getbyte(s, i) << 16;
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +
                padchar + padchar);
            break;
        case 2:
            b10 = (getbyte(s, i) << 16) | (getbyte(s, i + 1) << 8);
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +
                alpha.charAt((b10 >> 6) & 0x3f) + padchar);
            break;
    }
    return x.join('');
}

export class CommonHelper {
    static Base64 = base64
    static toast: DevExpress.ui.dxToast

    static handleRejection(apiResourceFactory, statusCode: number, state: any = null) {
        if (statusCode == 503) {
            this.openInfoPopup(i18n("Common", "WebServiceException"), "error")
        }

        if ((statusCode == -1 && !state) || (statusCode == -1 && state && !state.cancelled)) {
            this.openInfoPopup(i18n("Common", "ServerException"), "error")
        }

        this.goToLoginPage(apiResourceFactory, statusCode)

        return null
    }

    static goToLoginPage(apiResourceFactory, statusCode: number) {
        if (statusCode == 401 || statusCode == 503) {
            var bodyElement = angular.element("body")
            var scope: ng.IScope = bodyElement ? bodyElement.scope() : null
            if (scope) {
                let state = scope.$root["state"]

                if (state && ['login', 'logout', 'reset', 'home'].indexOf(state.current.name) < 0)
                    UserConfigHelper.setUserConfig(UserConfigHelper.StateBeforeAuthRejection, state.current.name)

                scope.$broadcast("showLoadingIndicator", true)
                scope.$root['hidePage'] = true
            }
            CommonHelper.removeValueFromLocalStorage('USER')
            location.href = '#/login'
            window.location.reload()
        }
    }

    static openInfoPopup(message: string = "", type: string = "success", displayTime: number = 2000, width: number = 300, additionalOptions?: DevExpress.ui.dxToastOptions) {
        if (this.toast)
            this.toast.hide()

        var toastOptions: DevExpress.ui.dxToastOptions = {
            type: type,
            message: message,
            displayTime: displayTime,
            width: width,
            onInitialized: e => {
                this.toast = e.component
            }
        };

        (<any>Object).assign(toastOptions, additionalOptions)
        DevExpress.ui.notify(toastOptions)
    }

    static closeInfoPopup() {
        if (this.toast)
            this.toast.hide()
    }

    static createLoadingIndicator(parentEl: JQuery) {
        var div = '<div id="loadingIndicatorGrid" style="display: -ms-flexbox; flex-direction: -ms-column;"></div>'
        var loadingIndicator = '<div id="indicator"></div>'
        parentEl.append(div)
        parentEl.find('#loadingIndicatorGrid').css({
            position: 'absolute',
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            'z-index': 999,
            background: '#fff',
            display: 'flex',
            'flex-direction': 'column',
            'align-items': 'center',
            'justify-content': 'center'
        }).append(loadingIndicator)
        parentEl.find('#indicator').dxLoadIndicator({
            width: 40,
            height: 40
        })
    }

    static removeLoadingIndicator(parentEl: JQuery) {
        parentEl.find('#loadingIndicatorGrid').remove()
    }

    static objectFromEntityModel(model: EntityModel) {
        var object = {}
        if (model.id) object['id'] = model.id
        if (model.name) object['name'] = model.name
        if (model.referenceId) object['referenceId'] = model.referenceId
        model.fields.forEach(field => {
            object[field.name.toLowerCase()] = field.value
        })
        return object
    }

    static getRequestObject(id: number) {
        var obj = {}
        if (id == 6)
            obj = CommonHelper.prepareRequestObject('device', 'getDevices', 'getDevicesDefaultColumns')
        else if (id == 7)
            obj = CommonHelper.prepareRequestObject('meter', 'getMeters', 'getMeterDefaultColumns')
        else if (id == 8)
            obj = CommonHelper.prepareRequestObject('location', 'getLocations', 'getLocationDefaultColumns')
        else if (id == 11)
            obj = CommonHelper.prepareRequestObject('distributor', 'getDistributors', 'getDistributorDefaultColumns')
        else
            obj = CommonHelper.prepareRequestObject(null, null, null)
        return obj
    }

    static getDataType(column: ColumnModel) {
        switch (column.type) {
            case CommonHelper.integer:
            case CommonHelper.number:
                if (column.referenceTypeId) return 'string'
                return 'number'
            case CommonHelper.date:
                return 'date'
            case CommonHelper.boolean:
                return 'boolean'
            default:
                return 'string'
        }
    }

    static cellFormatForDifferentValues(options) {
        if (options.typeId) {
            switch (options.typeId) {
                case CommonHelper.number:
                case CommonHelper.integer:
                    return options.value.toString()
                case CommonHelper.string:
                    return options.value
                case CommonHelper.date:
                    return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
                case CommonHelper.boolean:
                    if (options.value.toString().toLowerCase() == 'true') {
                        return i18n('Common', 'True')
                    } else {
                        return i18n('Common', 'False')
                    }
            }
        } else {
            if (moment(options.value, moment.ISO_8601, true).isValid()) {
                return moment(options.value).format(i18n("Common", "DateTimeFormatMomentJS"))
            } else if (angular.isNumber(options.value)) {
                return window['DevExpress'].localization.number.format(options.value)
            } else if (angular.isUndefined(options.value)) {
                return ''
            } else if (options.value == null) {
                return ''
            } else if (options.value.toString() == 'true') {
                return i18n('Common', 'True')
            } else if (options.value.toString() == 'false') {
                return i18n('Common', 'False')
            } else {
                return options.value.toString()
            }
        }
    }

    static setJWTCookie(jwt: string) {
        var d = new Date();
        d.setTime(d.getTime() + (5 * 60000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "jwt=" + jwt + ";" + expires + ";path=/"
    }

    private static prepareRequestObject(dataScope: string, dataMethodName: string, dictionaryMethodName: string) {
        return {
            dataScope: dataScope,
            dataMethodName: dataMethodName,
            dictionaryMethodName: dictionaryMethodName
        }
    }

    static getIcon(icon: string, fill: string = '#000000', size: string = '18') {
        return '<ng-md-icon icon="' + window['config']['ICONS'][icon] + '" style="fill: ' + fill + '" size="' + size + '"></ng-md-icon>'
    }

    static getIconWithText(text: string, icon: string, fill: string = '#000000', size: string = '18') {
        return '<ng-md-icon icon="' + window['config']['ICONS'][icon] + '" style="fill: ' + fill + '" size="' + size + '"></ng-md-icon><span>' + text + '</span>'
    }

    static getIconForDetails(icon: string, fill: string = '#000000', size: string = '18', text: string = null, className: string = '', id="") {
        return "<div id=" + id + " class='row " + className  + "'><div class='icon-row'><ng-md-icon icon=" + window['config']['ICONS'][icon] + " style=fill: " + fill + " size=" + size + "></ng-md-icon></div><span>" + " " + text + "</span></div>"
    }

    static getTemplateforListRow(text: string = null, className: string = '', id = "") {
        return "<div id=" + id + " class='row " + className + "'><div class='icon-row'></div><span>" + " " + text + "</span></div>"
    }

    static checkResultError(result: any) {
        if (result.hasErrorCode && result.hasErrorCode > 0) {
            switch (result.hasErrorCode) {
                case 1:
                    this.openInfoPopup(i18n("Common", "GridError"), "error", 5000)
                    break

                case 6:
                    this.openInfoPopup(i18n("Common", "InsufficientPermissionsError"), "error", 5000)
                    break

                default:
                    this.openInfoPopup(i18n("Common", "GridError"), "error", 5000)
                    break
            }
        }
    }

    static reportType = {
        jobReport: 7,
        serverReportingServices: 8
    }

    static date = 6
    static string = 3
    static number = 2
    static boolean = 7
    static integer = 1

    static greaterThan = '>'
    static lowerThan = '<'
    static greaterOrEqual = '>='
    static lowerOrEqual = '<='
    static equal = '=='
    static notEqual = '!='

    static idField = 'id'
    static nameField = 'name'
    static serialNumberField = 'serialNbr'
    static serialNumberParentField = 'serialNbrParent'

    static locationColumnId = 10002
    static locationLatitude = 7
    static locationLongitude = 8

    static meterColumnId = 10003

    static issuesColumnId = 10033

    static tasksColumnId = 10031

    static alarmsColumnId = 10090
    static alarmsName = 10108

    static userColumnId = 10032
    static actorColumnId = 10045
    static userColumnLogin = 10047
    static userColumnPassword = 10049
    static contactGroupId = 10125

    static distributorColumnId = 10005

    static priorityColumnId = 10065

    static routeId = 10095
    
    static simcardId = 28029

    static actionColumnId = 0
    static gridDateColumnId = 10104

    // Enums.ReferenceType.XXX (backend)
    static IdLocation = 8
    static IdAlarm = 88
    static IdAlarmEvent = 114
    static IdDistributor = 11
    static IdAction = 102
    static IdActor = 73
    static IdRoute = 20
    static IdOperator = 1
    static IdSimCard = 70
    static IdDevice = 6
    static IdDeviceConnection = 112
    static IdDeviceType = 14
    static IdMeter = 7
    static IdTask = 66
    static IdIssue = 65
    static IdMeterTypeClass = 51
    static IdChartType = 93
    static ChartTypeLine = 10
    static ChartTypeBar = 0
    static IdReport = 3 

    static alarm = 'alarm'
    static device = 'device'
    static distributor = 'distributor'
    static issue = 'issue'
    static location = 'location'
    static meter = 'meter'
    static route = 'route'
    static task = 'task'
    static user = 'user'
    static simcard = 'simcard'
    static contact = 'contact'
    static report = 'report'

    static mainGrid = 'mainGrid'
    static paginatedSmallGrid = 'paginatedSmallGrid'
    static smallGrid = 'smallGrid'
    static createEditGrid = 'createEditGrid'


    static deviceMarkerId = 0
    static parentMarkerId = 1
    static normalMarkerId = -1
    static importInPopup = true

    static filterSelection = "filterSelection"

    static dispatcherIndex = 0
    static dispatcherFunction = 1

    //dispatcher function indices - values cannot duplicate !!!
    static crtlAGridEditFunc = 0 

    static domain = location.pathname.replace(/\\/g, '').replace('/', '')

    static setValueToLocalStorage(name: string, value: any) {
        localStorage.setItem(CommonHelper.domain + name, JSON.stringify(value))
    }

    static getValueFromLocalStorage(name: string) {
        var json = localStorage.getItem(CommonHelper.domain + name)
        if (json) {
            try {
                return JSON.parse(json)
            } catch (e) {
                console.log(e.message)
                return null
            }
        }
        return null
    }

    static removeValueFromLocalStorage(name: string) {
        localStorage.removeItem(CommonHelper.domain + name)
    }
}