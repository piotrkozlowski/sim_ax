﻿export class PopupManager {
    private static stack = []

    private static instance: PopupManager = new PopupManager()

    constructor() {
        if (PopupManager.instance)
            throw new Error("Error: Instantiation failed: Use PopupManager.getInstance()")
        PopupManager.instance = this
    }

    public static getInstance(): PopupManager {
        return PopupManager.instance
    }

    add(popup: DevExpress.ui.dxPopup) {
        PopupManager.stack.push(popup)
    }

    deleteAndFocusLastPopup() {
        PopupManager.stack.pop()
        if (PopupManager.stack.length > 0) {
            var lastPopup = <DevExpress.ui.dxPopup>PopupManager.stack[PopupManager.stack.length - 1]
            lastPopup.focus()
        }
    }

    focusOnCurrent() {
        if (PopupManager.stack.length > 0) {
            var popup = <DevExpress.ui.dxPopup>PopupManager.stack[PopupManager.stack.length - 1]
        }
    }

    getStack() {
        return PopupManager.stack
    }
}

export class PopupType {
    static LOCATION_POPUP = "LOCATION"
    static METER_POPUP = "METER"
    static ALARM_POPUP = "ALARM"
    static DEVICE_POPUP = "DEVICE"
    static DISTRIBUTOR_POPUP = "DISTRIBUTOR"
    static ISSUE_POPUP = "ISSUE"
    static TASK_POPUP = "TASK"
    static USER_POPUP = "USER"
    static ROUTE_POPUP = "ROUTE"
    static SIM_CARD_POPUP = "SIMCARD"
    static CONTACT_POPUP = "CONTACT"
    static REPORT_POPUP = "REPORT"
}