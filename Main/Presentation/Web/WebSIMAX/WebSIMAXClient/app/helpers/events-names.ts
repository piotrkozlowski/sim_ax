﻿export class EventsNames {
    static CLOSE_DETAILS_POPUP = 'CLOSE_DETAILS_POPUP'

    static OPEN_LOCATION_DETAILS_POPUP = 'event:openDetailsLocationPopup'
    static CLOSE_LOCATION_DETAILS_POPUP = 'event:closeDetailsLocationPopup'

    static OPEN_GROUPS_EDIT_POPUP = 'event:openGroupsEditPopup'
    static CLOSE_GROUPS_EDIT_POPUP = 'event:closeGroupsEditPopup'
    static SAVE_GROUPS = 'event:saveGroups'
    static OPEN_GRID_EDIT_POPUP = 'event:openGridEditPopup'
    static OPEN_GRID_ATTACHMENTS_EDIT_POPUP = 'event:openGridAttachmentsEditPopup'
    static CLOSE_GRID_EDIT_POPUP = 'event:closeGridEditPopup'
    static CLOSE_GRID_ATTACHMENTS_EDIT_POPUP = 'event:closeGridAttachmentsEditPopup'

    static OPEN_CREATE_EDIT_POPUP = 'event:openCreateEditPopup'
    static CLOSE_CREATE_EDIT_POPUP = 'event:closeCreateEditPopup'
    static OPEN_IMPORT_POPUP = 'event:openImportPopup'
    static CLOSE_IMPORT_POPUP = 'event:closeImportPopup'
    static OPEN_LOCATION_CREATE_EDIT_POPUP = 'event:openLocationCreateEditPopup'
    static LOCATION_DELETED = 'event:locationDeleted'
    static UPDATE_CHART = 'event:updateChartForLocationPreview'
    static UPDATE_GRID_DETAILS = 'event:updateShortGridDetails'
    static OPEN_SAVE_COLUMN_SETTINGS = 'event:openSaveColumnSetting'
    static CLOSE_SAVE_COLUMN_SETTINGS = 'event:closeSaveColumnSettings'
    static OPEN_LOAD_COLUMN_SETTINGS = 'event:openLoadColumnSettings'
    static CLOSE_LOAD_COLUMN_SETTINGS = 'event:closeLoadColumnSettings'

    static REFRESH_MAP_SIZE = 'event:refreshMapSize'
    static CLOSE_MAP = 'event:closeSecondGridsterRow'
    static GRIDSTER_ROW_CLICKED = 'event:gridsterRowClicked'
    static MARKER_CLICKED = 'event:markerClicked'

    //OPEN_COORDINATES_POPUP = 'event:openCoordinatesPopup'
    static UPDATE_COORDINATES_BUTTON_POPUP = 'event:updateCoordinatesButtonPopup'

    // delete popup events
    static OPEN_CONFIRMATION_POPUP = 'event:openConfirmationPopup'
    static CLOSE_CONFIRMATION_POPUP = 'event:closeConfirmationPopup'
    static CONFIRM_DELETION_LOCATION = 'event:confirmDeletionLocation'
    static OPEN_COORDINATES_POPUP = 'event:openCoordinatesPopup'
    static CLOSE_COORDINATES_POPUP = 'event:closeCoordinatesPopup'

    //DEVICES
    static OPEN_DEVICES_DETAILS_POPUP = 'event:showDevicesDetailsPopup'
    static CLOSE_DEVICES_DETAILS_POPUP = 'event:hideDevicesDetailsPopup'
    static OPEN_DEVICES_CREATE_EDIT_POPUP = 'event:openDevicesCreateEditPopup'
    static CLOSE_DEVICES_CREATE_EDIT_POPUP = 'event:closeDevicesCreateEditPopup'
    static OPEN_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:openDevicesDeleteConfirmationPopup'
    static CLOSE_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:closeDevicesDeleteConfirmationPopup'
    static CONFIRM_DEVICES_DELETE_CONFIRMATION_POPUP = 'event:confirmDevicesDeleteConfirmationPopup'
    static DEVICE_DELETED = 'event:deviceDeleted'

    //DEVICECONNECTIONS
    static OPEN_DEVICECONNECTIONS_DETAILS_POPUP = 'event:showDeviceConnectionsDetailsPopus'

    //TASKS
    static OPEN_TASKS_DETAILS_POPUP = 'event:showTasksDetailsPopup'
    static CLOSE_TASKS_DETAILS_POPUP = 'event:hideTasksDetailsPopup'
    static OPEN_TASKS_CREATE_EDIT_POPUP = 'event:openTasksCreateEditPopup'
    static CLOSE_TASKS_CREATE_EDIT_POPUP = 'event:closeTasksCreateEditPopup'
    static OPEN_TASKS_DELETE_CONFIRMATION_POPUP = 'event:openTasksDeleteConfirmationPopup'
    static CLOSE_TASKS_DELETE_CONFIRMATION_POPUP = 'event:closeTasksDeleteConfirmationPopup'
    static CONFIRM_TASKS_DELETE_CONFIRMATION_POPUP = 'event:confirmTasksDeleteConfirmationPopup'
    static DOWNLOAD_TASK_ATTACHMENT = 'event:downloadTaskAttachment'
    static TASK_DELETED = 'event:taskDeleted'
    static OPEN_TASKS_STATUS_CHANGE_POPUP = 'event:openTasksStatusChangePopup'
    static CLOSE_TASKS_STATUS_CHANGE_POPUP = 'event:closeTasksStatusChangePopup'

    //ALARMS
    static OPEN_ALARMS_DETAILS_POPUP = 'event:showAlarmsDetailsPopup'
    static CLOSE_ALARMS_DETAILS_POPUP = 'event:hideAlarmsDetailsPopup'
    static OPEN_ALARMS_CREATE_EDIT_POPUP = 'event:openAlarmsCreateEditPopup'
    static CLOSE_ALARMS_CREATE_EDIT_POPUP = 'event:closeAlarmsCreateEditPopup'
    static OPEN_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:openAlarmsDeleteConfirmationPopup'
    static CLOSE_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:closeAlarmsDeleteConfirmationPopup'
    static CONFIRM_ALARMS_DELETE_CONFIRMATION_POPUP = 'event:confirmAlarmsDeleteConfirmationPopup'
    static ALARM_DELETED = 'event:alarmDeleted'
    static ALARM_APPROVED = 'event:alarmApproved'

    //ISSUE
    static OPEN_ISSUES_DETAILS_POPUP = 'event:showIssuesDetailsPopup'
    static CLOSE_ISSUES_DETAILS_POPUP = 'event:hideIssuesDetailsPopup'
    static OPEN_ISSUES_CREATE_EDIT_POPUP = 'event:openIssuesCreateEditPopup'
    static CLOSE_ISSUES_CREATE_EDIT_POPUP = 'event:closeIssuesCreateEditPopup'
    static OPEN_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:openIssuesDeleteConfirmationPopup'
    static CLOSE_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:closeIssuesDeleteConfirmationPopup'
    static CONFIRM_ISSUES_DELETE_CONFIRMATION_POPUP = 'event:confirmIssuesDeleteConfirmationPopup'
    static DOWNLOAD_ISSUE_ATTACHMENT = 'event:downloadIssueAttachment'
    static ISSUE_DELETED = 'event:issueDeleted'

    //METER
    static OPEN_METER_CREATE_EDIT_POPUP = 'event:openMeterCreateEditPopup'
    static OPEN_METER_DETAILS_POPUP = 'event:openMeterDetailsPopup'
    static CLOSE_METER_DETAILS_POPUP = 'event:closeMeterDetailsPopup'
    static METER_DELETED = 'event:meterDeleted'

    //USER
    static OPEN_USER_CREATE_EDIT_POPUP = 'event:openUserCreateEdit'
    static OPEN_USER_DETAILS_POPUP = 'event:openUserDetailsPopup'
    static CLOSE_USER_DETAILS_POPUP = 'event:closeUserDetailsPopup'
    static USER_DELETED = 'event:userDeleted'

    static OPEN_PASSWORD_CHANGE_POPUP = 'event:openPasswordChangePopup'

    // distributors
    static OPEN_DISTRIBUTORS_DETAILS_POPUP = 'event:showDistributorsDetailsPopup'
    static CLOSE_DISTRIBUTORS_DETAILS_POPUP = 'event:hideDistributorsDetailsPopup'
    static OPEN_DISTRIBUTORS_CREATE_EDIT_POPUP = 'event:openDistributorsCreateEditPopup'
    static CLOSE_DISTRIBUTORS_CREATE_EDIT_POPUP = 'event:closeDistributorsCreateEditPopup'
    static OPEN_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:openDistributorsDeleteConfirmationPopup'
    static CLOSE_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:closeDistributorsDeleteConfirmationPopup'
    static CONFIRM_DISTRIBUTORS_DELETE_CONFIRMATION_POPUP = 'event:confirmDistributorsDeleteConfirmationPopup'
    static CHANGED_ALLOWED_DISTRIBUTORS = 'event:changedAllowedDistributors'
    static DISTRIBUTOR_DELETED = 'event:distributorDeleted'

    static OPENED_POPUP = 'event:incrementPopupCount'
    static CLOSED_POPUP = 'event:decrementPopupCount'

    //READING ROUTES    
    static OPEN_ROUTES_DETAILS_POPUP = 'event:showRoutesDetailsPopup'
    static CLOSE_ROUTES_DETAILS_POPUP = 'event:hideRoutesDetailsPopup'
    static OPEN_ROUTES_CREATE_EDIT_POPUP = 'event:openRoutesCreateEditPopup'
    static CLOSE_ROUTES_CREATE_EDIT_POPUP = 'event:closeRoutesCreateEditPopup'
    static OPEN_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:openRoutesDeleteConfirmationPopup'
    static CLOSE_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:closeRoutesDeleteConfirmationPopup'
    static CONFIRM_ROUTES_DELETE_CONFIRMATION_POPUP = 'event:confirmRoutesDeleteConfirmationPopup'
    static ROUTE_DELETED = 'event:routeDeleted'
    static ROUTE_POINT_DRAG_AND_DROP = 'event:routePointDragAndDrop'

    //SIM CARDS    
    static OPEN_SIM_CARDS_DETAILS_POPUP = 'event:showSimcardDetailsPopup'
    static CLOSE_SIM_CARDS_DETAILS_POPUP = 'event:hideSimcardDetailsPopup'
    static OPEN_SIM_CARDS_CREATE_EDIT_POPUP = 'event:openSimcardCreateEditPopup'
    static CLOSE_SIM_CARDS_CREATE_EDIT_POPUP = 'event:closeSimcardCreateEditPopup'
    static OPEN_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:openSimcardDeleteConfirmationPopup'
    static CLOSE_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:closeSimcardDeleteConfirmationPopup'
    static CONFIRM_SIM_CARDS_DELETE_CONFIRMATION_POPUP = 'event:confirmSimcardDeleteConfirmationPopup'
    static SIM_CARDS_DELETED = 'event:simcardDeleted'

    //contacts    
    static OPEN_CONTACT_DETAILS_POPUP = 'event:showContactDetailsPopup'
    static CLOSE_CONTACT_DETAILS_POPUP = 'event:hideContactDetailsPopup'
    static OPEN_CONTACT_CREATE_EDIT_POPUP = 'event:openContactCreateEditPopup'
    static CLOSE_CONTACT_CREATE_EDIT_POPUP = 'event:closeContactCreateEditPopup'
    static OPEN_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:openContactDeleteConfirmationPopup'
    static CLOSE_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:closeContactDeleteConfirmationPopup'
    static CONFIRM_CONTACT_DELETE_CONFIRMATION_POPUP = 'event:confirmContactDeleteConfirmationPopup'
    static CONTACT_DELETED = 'event:contactDeleted'

    static OPEN_REPORTS_DETAILS_POPUP = 'event:showReportsDetailsPopup'
    static CLOSE_REPORTS_DETAILS_POPUP = 'event:hideReportsDetailsPopup'
    static OPEN_REPORTS_CREATE_EDIT_POPUP = 'event:openReportsCreateEditPopup'
    static CLOSE_REPORTS_CREATE_EDIT_POPUP = 'event:closeReportsCreateEditPopup'
    static OPEN_REPORTS_DELETE_CONFIRMATION_POPUP = 'event:openReportsDeleteConfirmationPopup'
    static CLOSE_REPORTS_DELETE_CONFIRMATION_POPUP = 'event:closeReportsDeleteConfirmationPopup'
    static CONFIRM_REPORTS_DELETE_CONFIRMATION_POPUP = 'event:confirmReportsDeleteConfirmationPopup'
    static CHANGED_REPORTS_DISTRIBUTORS = 'event:changedAllowedReports'
    static REPORTS_DELETED = 'event:deportsDeleted'
    static OPEN_REPORT_STATUS_CHANGE_POPUP = 'event:openReportStatusChangePopup'
    static REPORT_GENERATE = 'event:openReportGenerate'

    // actions
    static OPEN_ACTION_CREATE_EDIT_POPUP = 'event:openActionCreateEditPopup'
    static OPEN_ACTION_DETAILS_POPUP = 'event:openActionDetailsPopup'
    static CLOSE_ACTION_DETAILS_POPUP = 'event:closeActionDetailsPopup'
    static ACTION_DELETED = 'event:actionDeleted'

    static STOP_MEASUREMENTS_LOADING = 'event:stopMeasurementsLoading'
}