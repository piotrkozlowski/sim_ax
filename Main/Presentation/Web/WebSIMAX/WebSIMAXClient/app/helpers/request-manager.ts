﻿export class RequestManager {
    public requestStates: any = {}

    private static instance: RequestManager = new RequestManager()

    constructor() {
        if (RequestManager.instance)
            throw new Error("Error: Instantiation failed: Use RequestManager.getInstance()")
        RequestManager.instance = this
    }

    public static getInstance(): RequestManager {
        return RequestManager.instance
    }
}