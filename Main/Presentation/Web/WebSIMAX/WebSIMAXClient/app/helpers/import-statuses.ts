﻿export class ImportStatuses {
    static NotStarted = 1
    static Success = 2
    static Failed = 3
    static InProgress = 4
    static Validated = 5
    static NotValid = 6
    static Loaded = 7
}