﻿export class TrackEvents {
    //Wywołanie metody
    static ExecuteMethod = 1
    //Podstawowe komunikaty bluetooth
    static Write = 10
    static Read = 11
    static ReadSN = 12
    static Processed = 13
    static ARANGE = 14
    static EmptyValue = 15
    static EmptyReadout = 16
    static FrameProcessed = 17
    static Exception = 18
    static ReadoutRecived = 19
    //Interpracja ramki bez kluczy szyfrujących
    static NoEncryptionReciveFrame = 20
    static NoEncryptionNoBody = 21
    static NoEncryptionReciveYearFrame = 22
    static NoEncryptionReciveOneMonthFrame = 23
    static NoEncryptionReciveTwoMonthFrame = 24
    static NoEncryptionReciveThreeMonthFrame = 25
    //Informacje o ARANGE
    static ARANGEType = 100
    static ARANGEBateryInfo = 101
    static ReadARANGEType = 102
    static ReadARANGEBattery = 103
    //Informacje o preambule
    static SetPreamble = 104
    static SetPreambleError = 105
    static SetPreambleSummary = 106
    //Rozpoczęcie Stop&Go
    static Start = 107
    //Opis lokalizacji na start
    static LocationDescrStart = 108
    //Opis lokalizacji na koniec
    static LocationDescrEnd = 109
    //Współrzędne GPS
    static CurGPS = 110
    //Parametry komendy
    static CommandParams = 111
    //Wysłanie ramki budzącej
    static WakeUp = 112
    static WakeUpTryAgain = 113
    static WakeUpError = 114
    //Wysłanie ramki skanowania
    static WMBUSSCAN = 115
    static WMBUSSCANPutOK = 116
    static WMBUSSCANTryAgain = 117
    static WMBUSSCANError = 118
    //Podsumowanie skanowania
    static ScanSummary01 = 119
    static ScanSummary02 = 120
    static ScanSummary03 = 121
    static ScanSummary04 = 122
    static ScanSummary05 = 123
    static ScanSummary06 = 124
    static ScanSummary07 = 125
    static ScanSummary08 = 126
    static ScanSummary09 = 127
    static ScanSummary10 = 128
    static ScanSummary11 = 129
    static ScanSummary12 = 130
    static ScanSummary13 = 131
    static ScanSummary14 = 132
    //Wysyłanie ramki odczytu obchodoznego
    static ReadReadout = 133
    static ReadReadoutPutOK = 134
    static ReadReadoutTryAgain = 135
    static ReadReadoutError = 136
    //Wysyłane akcje do urządzenia
    static SetTime = 137
    static SendAdditionalPacket = 138
    static DeleteLatch = 139
    static ReadDiagnostic = 140
    //Podsumowanie odczytu obchodzonego
    static ReadoutSummary01 = 141
    static ReadoutSummary02 = 142
    static ReadoutSummary03 = 143
    static ReadoutSummary04 = 144
    static ReadoutSummary05 = 145
    static ReadoutSummary06 = 146
    static ReadoutSummary07 = 147
    static ReadoutSummary08 = 148
    static ReadoutSummary09 = 149
    static ReadoutSummary10 = 150
    static ReadoutSummary11 = 151
    static ReadoutSummary12 = 152
    //Wysłanie ramki usypiającej
    static SleepDevice = 153
    static SleepDevicePutOK = 154
    static SleepDeviceTryAgain = 155
    static SleepDeviceError = 156
    //Podsumowanie obiegu
    static CycleInfo = 157
    static CycleInfo01 = 158
    static CycleInfo02 = 159
    static CycleInfo03 = 160
    static CycleInfo04 = 161
    static CycleInfo05 = 162
    static CycleInfo06 = 163
    static CycleInfo07 = 164
    static CycleInfo08 = 165
    //Interpretacja skanowania urządzeń
    static ReciveWMBUS = 166
    //Interpretacja dodatkowych akcji podejmowanych podczas odczytu obchodzonego
    static ReciveAdditionalPacket = 167
    //Interpretacja otrzymania diagnostyki
    static ReciveDiagnostic = 168
    static ReciveDiagnosticEmptyValue = 169
    static ReciveDiagnosticError = 170
    //Wyliczenie slotu skanowania
    static ARANGERange = 171
    static ARANGERangeScan = 172
    //Zapis zatrzasku 
    static SaveTaskStepLatch = 173
}