var gulp = require("gulp")
var clean = require('gulp-clean')
var connect = require('gulp-connect')
var fs = require("fs")
var glob = require("glob")
var webpack = require("webpack")
var server = require('karma').Server
var ts = require("gulp-typescript")
var mkdirp = require('mkdirp')
var getDirName = require('path').dirname;
var replace = require('gulp-string-replace')

var webpackProgress = function (err, stats, next) {
    if (err) {
        //console.log("ERROR: " + err)
        return
    }
    var jsonStats = stats.toJson()
    if (jsonStats.errors.length > 0) {
        console.log("ERROR: " + jsonStats.errors[0])
        return
    }
    if (jsonStats.warnings.length > 0)
        console.log("WARNING: " + jsonStats.warnings)

    console.log(new Date().toTimeString().split(' ')[0], "FINISH")

    if (next) next()
}

var writeFile = function (path, contents, cb, next) {
    mkdirp(getDirName(path), function (err) {
        if (err) return cb(err)
        fs.writeFile(path, contents, cb, next)
    })
}

// App tasks
gulp.task('app:clean', function () {
    return gulp.src('./build/*', { read: false })
		.pipe(clean())
})

gulp.task('app:build:entry', ['app:clean'], next => {
    var destPath = "./build/entry.ts"
    if (! fs.existsSync('./build')) fs.st
    if (fs.existsSync(destPath)) fs.unlinkSync(destPath)
    glob("./app/**/*.ts", (err, files) => {
        var requires = files.filter(f => f.indexOf('.spec.ts') === -1 && f.indexOf('test.ts') === -1).map(f => "require('." + f + "')").reverse()
        var requireFile = requires.join("\r\n")
        writeFile(destPath, requireFile, {
            encoding: "utf-8"
        }, next)
    })
})

gulp.task('app:build:index.html', ['app:build:entry'], function (done) {
    var moment = require('moment')
    var version = moment().format('YYYY.MM.DD.HHmm')
    return gulp.src(["./index.html"])
        .pipe(replace('@VERSION@', version))
        .pipe(gulp.dest('./build/client'))
})

gulp.task('app:build:webpack', ['app:build:index.html'], function (done) {
    var compiler = webpack(require('./webpack.config'))
    compiler.run((err, stats) => webpackProgress(err, stats, done))
})

gulp.task('app:watch:webpack', ['app:build:index.html'], function () {
    var compiler = webpack(require('./webpack.config'))
    compiler.watch({}, (err, stats) => webpackProgress(err, stats))
})

gulp.task('app:server', function () {
    connect.server({
        root: './build/client',
        port: 51981
    })
})
// End of app tasks

// wwwroot tasks
gulp.task('wwwroot:clean', function () {
    return gulp.src('../WebSIMAX/wwwroot/*', { read: false })
        .pipe(clean({ force: true }))
})

gulp.task('wwwroot:copy', ['app:build:webpack', 'wwwroot:clean'], function () {
    return gulp.src('./build/client/**/*.*')
        .pipe(gulp.dest('../WebSIMAX/wwwroot'))
})
// End of tasks

// Test tasks
gulp.task('test:clean', function () {
    return gulp.src('Test/*', { read: false })
		.pipe(clean())
})

gulp.task('test:build:entry', ['test:clean'], next => {
    var destPath = "Test/entry.ts"
    if (fs.existsSync(destPath)) fs.unlinkSync(destPath)
    glob("./app/**/*.ts", (err, files) => {
        var requires = files.filter(f => f.indexOf('.spec.ts') > -1 ).map(f => "require('./" + f + "')")
        requires.push('require("angular-mocks")')
        requires.push('require("../app/entry.ts")')
        requires = requires.reverse()
        var requireFile = requires.join("\r\n")
        writeFile(destPath, requireFile, {
            encoding: "utf-8"
        }, next)
    })
})

gulp.task('test:build:webpack', ['app:build:webpack', 'test:build:entry'], function (done) {
    var compiler = webpack(require('./spec.webpack.config'))
    return compiler.run((err, stats) => webpackProgress(err, stats, done))
})

gulp.task('test:run', ['test:build:webpack'], function (done) {
    new server({
        configFile: __dirname + './karma.conf.js',
        singleRun: true
    }, done).start()
})
// End of test tasks
