﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using System.Threading;
using System.Globalization;
using TypeLite;
using TypeLite.Net4;
using System.Text.RegularExpressions;

namespace SIMAX
{
    public class Program
    {
        public static void Main(string[] args)
        {

#if DEBUG
            GenerateModelsForTypeScript();
#endif
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        // Warn: for developers only!
        public static void GenerateModelsForTypeScript()
        {
            var ts = TypeScript.Definitions()
                .ForLoadedAssemblies()
                .WithMemberFormatter((identifier) =>
                   Char.ToLower(identifier.Name[0]) + identifier.Name.Substring(1)
                )
                .WithModuleNameFormatter((identifier) => identifier.Name = "SIMAX");

            var tsOutput = ts.Generate(TsGeneratorOutput.Properties);
            var tsOutputLines = tsOutput.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            tsOutputLines[0] = "// File is generated :)";
            tsOutputLines.RemoveAt(tsOutputLines.Count - 1);
            var clearOutput = String.Join(Environment.NewLine, tsOutputLines.ToArray()).Replace("\t\t", "_").Replace("\t", "").Replace("_", "\t").Replace("interface", "export class").Replace("SIMAX.", "").Replace("}\r\ndeclare module SIMAX {\r\n", "");
            clearOutput = Regex.Replace(clearOutput, 
                @"KeyValuePair\<(?<k>[^\,]+),(?<v>[^\,]+)\>\[\];", 
                m => "{[key: " + m.Groups["k"].Value + "]: " + m.Groups["v"].Value + "};", 
                RegexOptions.Multiline);

            var clearOutputLines = clearOutput.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            var clearOutputDictionary = new Dictionary<string, List<string>>();
            var classLine = clearOutputLines[0];
            var stringLines = new List<string>();
            clearOutputLines.ForEach(l =>
            {
                if (l.Contains("export"))
                {
                    classLine = l;
                    stringLines.Add(l);
                }
                else if (l.Equals("}"))
                {
                    stringLines.Add(l);
                    clearOutputDictionary.Add(classLine, stringLines);
                    stringLines = new List<string>();
                }
                else
                {
                    stringLines.Add(l);
                }
            });

            var baseClasses = "";
            var extendedClasses = "";

            foreach (KeyValuePair<string, List<string>> entry in clearOutputDictionary)
            {
                if (entry.Key.Contains("extends"))
                {
                    extendedClasses += Environment.NewLine + String.Join(Environment.NewLine, entry.Value.ToArray());
                }
                else
                {
                    baseClasses += Environment.NewLine + String.Join(Environment.NewLine, entry.Value.ToArray());
                }
            }

            File.WriteAllText(Directory.GetCurrentDirectory() + @"..\..\WebSIMAXClient\app\modules\api\api.models.ts", baseClasses + Environment.NewLine + extendedClasses);
        }
    }
}
