﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIMAX.Resources.Notification {
    using System;
    using System.Reflection;
    
    
    /// <summary>
    ///    A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Notification {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal Notification() {
        }
        
        /// <summary>
        ///    Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebSIMAX.Resources.Notification.Notification", typeof(Notification).GetTypeInfo().Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///    Overrides the current thread's CurrentUICulture property for all
        ///    resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Action {
            get {
                return ResourceManager.GetString("Action", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Assigned {
            get {
                return ResourceManager.GetString("Assigned", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Change {
            get {
                return ResourceManager.GetString("Change", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string None {
            get {
                return ResourceManager.GetString("None", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Note_Change {
            get {
                return ResourceManager.GetString("Note_Change", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string NotificationByEmail {
            get {
                return ResourceManager.GetString("NotificationByEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string NotificationBySms {
            get {
                return ResourceManager.GetString("NotificationBySms", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_APPROVED {
            get {
                return ResourceManager.GetString("State_APPROVED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_DELETED {
            get {
                return ResourceManager.GetString("State_DELETED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_INTERVENTION {
            get {
                return ResourceManager.GetString("State_INTERVENTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_NEW {
            get {
                return ResourceManager.GetString("State_NEW", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_OPERATIONAL {
            get {
                return ResourceManager.GetString("State_OPERATIONAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_PENDING {
            get {
                return ResourceManager.GetString("State_PENDING", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_SCHEDULED {
            get {
                return ResourceManager.GetString("State_SCHEDULED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_SUSPENDED {
            get {
                return ResourceManager.GetString("State_SUSPENDED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string State_UNUSED {
            get {
                return ResourceManager.GetString("State_UNUSED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Accepted {
            get {
                return ResourceManager.GetString("Status_Accepted", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Canceled {
            get {
                return ResourceManager.GetString("Status_Canceled", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Finished {
            get {
                return ResourceManager.GetString("Status_Finished", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Finished_succesfully {
            get {
                return ResourceManager.GetString("Status_Finished_succesfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Finished_with_error {
            get {
                return ResourceManager.GetString("Status_Finished_with_error", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_In_progress {
            get {
                return ResourceManager.GetString("Status_In_progress", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_InProgress {
            get {
                return ResourceManager.GetString("Status_InProgress", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_NEW {
            get {
                return ResourceManager.GetString("Status_NEW", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Not_Started {
            get {
                return ResourceManager.GetString("Status_Not_Started", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Planned {
            get {
                return ResourceManager.GetString("Status_Planned", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_QUEUED {
            get {
                return ResourceManager.GetString("Status_QUEUED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_RealizationNotPossible {
            get {
                return ResourceManager.GetString("Status_RealizationNotPossible", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Rejected {
            get {
                return ResourceManager.GetString("Status_Rejected", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_REOPENED {
            get {
                return ResourceManager.GetString("Status_REOPENED", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Suspended {
            get {
                return ResourceManager.GetString("Status_Suspended", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_Verified {
            get {
                return ResourceManager.GetString("Status_Verified", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_WAITING_FOR_RESPONSE {
            get {
                return ResourceManager.GetString("Status_WAITING_FOR_RESPONSE", resourceCulture);
            }
        }
        
        /// <summary>
        ///    Looks up a localized string similar to .
        /// </summary>
        public static string Status_WaitingForAcceptance {
            get {
                return ResourceManager.GetString("Status_WaitingForAcceptance", resourceCulture);
            }
        }
    }
}
