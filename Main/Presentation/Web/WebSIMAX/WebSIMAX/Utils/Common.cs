﻿using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace SIMAX.Utils
{
    public static class Common
    {
        public static readonly string LOGGED_USER = "LOGGED_USER";

        public static dynamic DictionaryToObject(Dictionary<string, object> dict, bool toLower)
        {
            IDictionary<string, object> eo = new ExpandoObject() as IDictionary<string, object>;
            if (dict == null) return eo;
            foreach (KeyValuePair<string, object> kvp in dict)
            {
                var key = kvp.Key;//.Replace("_0", "");
                if (toLower) key = key.ToLower();
                if (kvp.Value == null) eo.Add(new KeyValuePair<string, object>(key, null));
                else eo.Add(key, kvp.Value);
            }
            return eo;
        }

        public static dynamic DictionaryToObject(Dictionary<string, object> dict, string text = null, long id = 0)
        {
            IDictionary<string, object> eo = Common.DictionaryToObject(dict, true);
            if (text != null)
                eo.Add("Name", text);
            if (id != 0)
                eo.Add("Id", id);
            return eo;
        }

        public static dynamic DictionaryToObject(Dictionary<string, object> dict, OpTask opTask)
        {
            IDictionary<string, object> eo = Common.DictionaryToObject(dict, true);
            eo.Add("Name", opTask.ToString());
            eo.Add("Id", opTask.IdTask);
            eo.Add("StatusId", opTask.IdTaskStatus);
            return eo;
        }

        public static string ColorToHEX(System.Drawing.Color color)
        {
            return "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }

        public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
}
