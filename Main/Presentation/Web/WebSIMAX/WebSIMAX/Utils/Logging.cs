﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel;
using Microsoft.AspNetCore.Mvc.Controllers;
using IMR.Suite.Common;
using NLog;

namespace SIMAX.Utils
{
    public class Logging : ActionFilterAttribute, IClientMessageInspector
    {
        public Logging()
        {
            IMRLog.MinLogLevel = IMR.Suite.Common.LogLevel.Debug;
        }

        private static string Format = "[{0}]-Exit function {1}. Duration: {2}[s].";

        public class EventID
        {
            // EventIDs -> 2002000-2004999
            public class API
            {
                public static readonly LogData EnterFunction = new LogData(2002000, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Enter function.");
                public static readonly LogData ExitFunction = new LogData(2002001, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Exit function. Duration: {1}[s].");
            }

            public class WS
            {
                public static readonly LogData EnterFunction = new LogData(2002010, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Enter function.");
                public static readonly LogData ExitFunction = new LogData(2002011, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Exit function. Duration: {1}[s].");
            }

            public class ERRORS
            {
                public static readonly LogData ViewsProblem = new LogData(2002020, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Problem with views. Views length = 0");
                public static readonly LogData FileExtensionProblem = new LogData(2002021, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Problem with file extension: {1}.");
                public static readonly LogData Exception = new LogData(2002022, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Exception occurred: {1}.");
                public static readonly LogData ActiveViewSessionError = new LogData(2002023, IMR.Suite.Common.LogLevel.Debug, "[OpxGetActiveView]-Error in session: {0}");
                public static readonly LogData ViewsProblem2 = new LogData(2002024, IMR.Suite.Common.LogLevel.Debug, "[{0}]-Problem with views. IdReferenceType already exists: {1}.");
            }

            public class INFO
            {
                public static readonly LogData Measurments = new LogData(2002030, IMR.Suite.Common.LogLevel.Debug, "[{0}]-The packing time of the measurements: {1}[s].");
                public static readonly LogData Login = new LogData(2002030, IMR.Suite.Common.LogLevel.Info, "[Login]-Attempt to log in: {0}");
            }

            public class DICTIONARY
            {
                public static readonly LogData GetActiveViewsLog = new LogData(2002100, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDictionariesLog = new LogData(2002101, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDictVersionLog = new LogData(2002102, IMR.Suite.Common.LogLevel.Debug, Format);
            }

            public class COMMON 
            {
                public static readonly LogData GetContextListLog = new LogData(2002200, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetRefObjectListLog = new LogData(2002201, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetAllowedListLog = new LogData(2002202, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class ACTIONS
            {
                public static readonly LogData GetActionLog = new LogData(2002300, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterActionLog = new LogData(2002301, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveActionLog = new LogData(2002302, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class ALARMS
            {
                public static readonly LogData FilterAlarmLog = new LogData(2002400, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetAlarmSummaryLog = new LogData(2002401, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetAlarmLog = new LogData(2002402, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ConfirmAlarmLog = new LogData(2002403, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class TASKS
            {
                public static readonly LogData GetTaskAttachmentLog = new LogData(2002500, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetTaskLog = new LogData(2002501, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterTaskLog = new LogData(2002502, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveTaskLog = new LogData(2002503, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveTaskAttachmentLog = new LogData(2002504, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteTaskAttachmentLog = new LogData(2002505, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ExportTaskLog = new LogData(2002506, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetTaskStatusChangeListLog = new LogData(2002507, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ChangeTaskStatusLog = new LogData(2002508, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetTaskHistoryLog = new LogData(2002509, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class ISSUES
            {
                public static readonly LogData GetIssueAttachmentLog = new LogData(2002600, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetIssueLog = new LogData(2002601, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterIssueLog = new LogData(2002602, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveIssueLog = new LogData(2002603, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveIssueAttachmentLog = new LogData(2002604, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteIssueAttachmentLog = new LogData(2002605, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class LOCATIONS
            {
                public static readonly LogData ExportLocationLog = new LogData(2002700, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterLocationLog = new LogData(2002701, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetLocationHistoryLog = new LogData(2002702, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetLocationEquipmentLog = new LogData(2002703, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetLocationLog = new LogData(2002704, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveLocationLog = new LogData(2002705, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetLocationSchemaLog = new LogData(2002706, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationInitializeLog = new LogData(2002707, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationValidateDataLog = new LogData(2002708, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationStartImport = new LogData(2002709, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationStopImport = new LogData(2002710, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationProgress = new LogData(2002711, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportLocationSummary = new LogData(2002712, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class METERS
            {
                public static readonly LogData ExportMeterLog = new LogData(2002800, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetMeterLog = new LogData(2002801, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetMeterHistoryLog = new LogData(2002802, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterMeterLog = new LogData(2002803, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveMeterLog = new LogData(2002804, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteMeterLog = new LogData(2002805, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetMeterSchemaLog = new LogData(2002806, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class DEVICES
            {
                public static readonly LogData ExportDeviceLog = new LogData(2002900, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterDeviceLog = new LogData(2002901, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetParentDevicesLog = new LogData(2002902, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetChildDevicesLog = new LogData(2002903, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveChildDevicesLog = new LogData(2002904, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveParentDevicesLog = new LogData(2002905, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDeviceLog = new LogData(2002906, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveDeviceLog = new LogData(2002907, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDeviceHistoryLog = new LogData(2002908, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDeviceSchemaLog = new LogData(2002909, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class USERS
            {
                public static readonly LogData GetOperatorSettingsLog = new LogData(2003000, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterActorLog = new LogData(2003001, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetActorLog = new LogData(2003002, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveActorLog = new LogData(2003003, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetOperatorDashboardConfLog = new LogData(2003004, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetOperatorDashboardDataLog = new LogData(2003005, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ResetOperatorPasswordLog = new LogData(2003006, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ChangeOperatorPasswordLog = new LogData(2003007, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterOperatorLog = new LogData(2003008, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetOperatorLog = new LogData(2003009, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveOperatorLog = new LogData(2003010, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteOperatorLog = new LogData(2003011, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class DISTRIBUTORS
            {
                public static readonly LogData FilterDistributorLog = new LogData(2003100, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetDistributorLog = new LogData(2003101, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveDistributorLog = new LogData(2003102, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteDistributorLog = new LogData(2003103, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class MEASUREMENTS
            {
                public static readonly LogData FilterMeasurementsLog = new LogData(2003200, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData FilterLatestMeasurementsLog = new LogData(2003201, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class REPORTS
            {
                public static readonly LogData FilterReportsLog = new LogData(2003300, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetReportLog = new LogData(2003301, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveReportLog = new LogData(2003302, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteReportLog = new LogData(2003303, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GenerateReportLog = new LogData(2003304, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class ROUTES
            {
                public static readonly LogData FilterRoutesLog = new LogData(2003400, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetRoutesLog = new LogData(2003401, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveRouteLog = new LogData(2003402, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ExportRoutesLog = new LogData(2003403, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteInitializeLog = new LogData(2003404, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteValidateDataLog = new LogData(2003405, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteStartImport = new LogData(2003406, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteStopImport = new LogData(2003407, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteProgress = new LogData(2003408, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportRouteSummary = new LogData(2003409, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class SESSION
            {
                public static readonly LogData InitSessionLog = new LogData(2003500, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData InitDBCollectorLog = new LogData(2003501, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData LoginLog = new LogData(2003502, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData InitDBCollectorPropertiesLog = new LogData(2003503, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetLoggedOperatorLog = new LogData(2003504, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData LogoutLog = new LogData(2003505, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class CONTACTS
            {
                public static readonly LogData ActorGroupFilterLog = new LogData(2003600, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class DEVICECONNECTIONS
            {
                public static readonly LogData DeviceConntectionFilterLog = new LogData(2003700, IMR.Suite.Common.LogLevel.Debug, Format);
            }
            public class SIMCARDS
            {
                public static readonly LogData GetSimcardHistoryLog = new LogData(2003800, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData GetSimcardLog = new LogData(2003801, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData DeleteSimcardLog = new LogData(2003802, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveSimcardLog = new LogData(2003803, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardInitializeLog = new LogData(2003804, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardValidateLog = new LogData(2003805, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardStartImport = new LogData(2003806, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardStopImport = new LogData(2003807, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardProgess= new LogData(2003808, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData ImportSimcardSummary = new LogData(2003809, IMR.Suite.Common.LogLevel.Debug, Format);
            }
			public class SECUREAPP
            {
                public static readonly LogData GetOperatorSettings = new LogData(2003900, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData SaveOperatorSettings = new LogData(2003901, IMR.Suite.Common.LogLevel.Debug, Format);
                public static readonly LogData PairColumnsToImport = new LogData(2003902, IMR.Suite.Common.LogLevel.Warn, "No matching column found: {0}");
            }
        }

        private const string StopwatchKey = "StopwatchFilter.Value";

        private void Start(LogData type, string name)
        {
            IMRLog.AddToLog(type, new object[] { name });
        }

        private void Stop(LogData type, Stopwatch stopwatch, string name)
        {
            stopwatch.Stop();
            IMRLog.AddToLog(type, new object[] { name, stopwatch.Elapsed.ToString("ss'.'ff") });
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            actionContext.HttpContext.Items[StopwatchKey] = Stopwatch.StartNew();
            Start(EventID.API.EnterFunction, ((ControllerActionDescriptor)actionContext.ActionDescriptor).ControllerName + "/" + actionContext.ActionDescriptor.Name);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
            Stop(EventID.API.ExitFunction, (Stopwatch)context.HttpContext.Items[StopwatchKey], ((ControllerActionDescriptor)context.ActionDescriptor).ControllerName + "/" + context.ActionDescriptor.Name);
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            var actionName = request.Headers.Action.Split('/').Last();
            Start(EventID.WS.EnterFunction, actionName);
            return Stopwatch.StartNew();
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            var actionName = reply.Headers.Action.Split('/').Last();
            Stop(EventID.WS.ExitFunction, (Stopwatch)correlationState, actionName);
        }
    }
}