﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;

namespace SIMAX.Utils
{
    public class JavascriptResourceHelper
    {
        private static Dictionary<string, string> GenerateResourceDictionary(ResourceManager manager, CultureInfo cultureInfo)
        {
            var result = new Dictionary<string, string>();
            var resourceSet = manager.GetResourceSet(cultureInfo, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                result.Add(entry.Key.ToString().ToLower(), entry.Value.ToString());
            }
            return result;
        }

        public class JavaScriptResources
        {
            private CultureInfo _cultureInfo;

            public JavaScriptResources(CultureInfo cultureInfo)
            {
                _cultureInfo = cultureInfo;
            }

            public Dictionary<string, string> Common
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Common.Common.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Login
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Login.Login.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Grid
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Grid.Grid.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Popup
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Popup.Popup.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Notification
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Notification.Notification.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Distributor359
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Distributor359.Distributor359.ResourceManager, _cultureInfo);
                }
            }

            public Dictionary<string, string> Tracking
            {
                get
                {
                    return GenerateResourceDictionary(Resources.Tracking.Tracking.ResourceManager, _cultureInfo);
                }
            }
        }
    }
}
