﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using SIMAX.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class HistoryController : Controller
    {
        private IHistoryService _historyService;

        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpGet("{id}")]
        [ActionName("Location")]
        public ResponseListModel<HistoryModel> GetLocationHistory(long id)
        {
            return _historyService.GetLocationHistory(id);
        }

        [HttpGet("{id}")]
        [ActionName("Device")]
        public ResponseListModel<HistoryModel> GetDeviceHistory(long id)
        {
            return _historyService.GetDeviceHistory(id);
        }

        [HttpGet("{id}")]
        [ActionName("Meter")]
        public ResponseListModel<HistoryModel> GetMeterHistory(long id)
        {
            return _historyService.GetMeterHistory(id);
        }

        [HttpGet("{id}")]
        [ActionName("Issue")]
        public ResponseListModel<HistoryIssueModel> GetIssueHistory(long id)
        {
            return _historyService.GetIssueHistory(id);
        }

        [HttpGet("{id}")]
        [ActionName("Task")]
        public ResponseListModel<HistoryModel> GetTaskHistory(int id)
        {
            return _historyService.GetTaskHistory(id);
        }

        [HttpGet("{id}")]
        [ActionName("Simcard")]
        public ResponseListModel<HistoryModel> GetSimcardHistory(long id)
        {
            return _historyService.GetSimcardHistory(id);
        }
    }
}
