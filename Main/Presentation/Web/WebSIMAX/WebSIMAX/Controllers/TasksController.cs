﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;
using System.Net.Http;
using System.IO;
using System;
using SIMAX.Utils;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TasksController : Controller
    {
        private ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _tasksService.GetTask(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetTaskToEdit(int id)
        {
            return _tasksService.GetTaskToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _tasksService.GetTasks(request);
        }

        [HttpPut]
        public TaskModel Put([FromBody]FormValuesModel formModel)
        {
            return _tasksService.SaveTask(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _tasksService.DeleteTask(id);
        }

        [HttpGet("{id}/CheckIfAttachmentExists/{attachmentId}")]
        public string CheckIfAttachmentExists(int id, long attachmentId)
        {
            return _tasksService.CheckIfAttachmentExists(id, attachmentId);
        }

        [HttpGet("{id}/Attachments")]
        public ResponseListModel<FileModel> GetAttachments(long id)
        {
            return _tasksService.GetAttachments(id);
        }

        [HttpPost("Attachments")]
        public SaveFileModel SaveAttachments([FromBody]FileModel model)
        {
            return _tasksService.UploadAttachment(model);
        }

        [HttpPut("Attachments")]
        public ResponseListModel<long> DeleteAttachments([FromBody]FormValuesModel model)
        {
            return _tasksService.DeleteAttachments(model);
        }


        [HttpGet("{id}/Equipment")]
        public ResponseListModel<LocationEquipmentModel> GetEquipment(long id)
        {
            return _tasksService.GetEquipment(id);
        }

        [HttpPost("AvailableStatuses")]
        public ResponseListModel<TaskStatusModel> GetAvailableStatuses([FromBody]int[] ids)
        {
            return _tasksService.GetAvailableStatuses(ids);
        }

        [HttpPut("SaveTaskStatuses")]
        public object SaveTaskStatuses([FromBody]TaskStatusChangeModel model)
        {
            return _tasksService.SaveStatuses(model);
        }
    }
}
