﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Services;
using SIMAX.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DeviceConnectionsController : Controller
    {
        private IDeviceConnectionsService _dcService;
        private IDevicesService _devicesService;

        public DeviceConnectionsController(IDeviceConnectionsService dcService, IDevicesService devicesService)
        {
            _dcService = dcService;
            _devicesService = devicesService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null)
                return new GridModel();
            return _dcService.GetDeviceConnections(request);
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _devicesService.GetDevice(id);
        }
    }
}
