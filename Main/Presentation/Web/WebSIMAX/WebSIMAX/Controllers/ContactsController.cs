﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ContactsController : Controller
    {
        private IContactsService _contactsService;

        public ContactsController(IContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            ContactModel contact = _contactsService.GetContact(id);

            return _contactsService.GetContact(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetContactToEdit(int id)
        {
            return _contactsService.GetContactToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _contactsService.GetContacts(request);
        }

        [HttpPost("groups")]
        public GridModel PostGroups([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _contactsService.GetGroups(request);
        }

        [HttpPut]
        public ContactModel Put([FromBody]ContactFormValuesModel formModel)
        {
            return _contactsService.Save(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _contactsService.Delete(id);
        }
    }
}
