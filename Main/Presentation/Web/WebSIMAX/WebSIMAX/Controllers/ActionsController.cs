﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using SIMAX.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ActionsController : Controller
    {
        private IActionsService _actionService;

        public ActionsController(IActionsService actionService)
        {
            _actionService = actionService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _actionService.GetActions(request);
        }

        [HttpGet("{id}")]
        public ActionModel Get(long id)
        {
            return _actionService.GetAction(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetActionToEdit(int id)
        {
            return _actionService.GetActionToEdit(id);
        }

        [HttpPut]
        public ActionModel Put([FromBody]FormValuesModel formModel)
        {
            return _actionService.SaveAction(formModel);
        }

        [HttpDelete("{id}")]
        public ResponseDeleteModel Delete(int id)
        {
            return _actionService.DeleteAction(id);
        }
    }
}
