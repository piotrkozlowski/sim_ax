﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DevicesController : Controller
    {
        private IDevicesService _devicesService;

        public DevicesController(IDevicesService devicesService)
        {
            _devicesService = devicesService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _devicesService.GetDevice(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetDeviceToEdit(int id)
        {
            return _devicesService.GetDeviceToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _devicesService.GetDevices(request);
        }

        [HttpGet("{id}/Parent")]
        public ResponseListModel<DeviceHierarchyModel> GetParentdDevices(long id)
        {
            return _devicesService.GetParentDevices(id);
        }

        [HttpGet("{id}/Child")]
        public ResponseListModel<DeviceHierarchyModel> GetChildDevices(long id)
        {
            return _devicesService.GetChildDevices(id);
        }

        [HttpPut("Parent")]
        public ResponseListModel<DeviceHierarchyModel> SaveParentDevices([FromBody]DevicesEditModel editModel)
        {
            return _devicesService.SaveParentDevices(editModel);
        }

        [HttpPut("Child")]
        public ResponseListModel<DeviceHierarchyModel> SaveChildDevices([FromBody]DevicesEditModel editModel)
        {
            return _devicesService.SaveChildDevices(editModel);
        }

        [HttpPut]
        public DeviceModel Put([FromBody]FormValuesModel formModel)
        {
            return _devicesService.SaveDevice(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _devicesService.DeleteDevice(id);
        }

        [HttpPost("Meter")]
        public DeviceModel SaveDeviceCurrentMeter([FromBody]ObjectReferenceModel model)
        {
            return _devicesService.SaveDeviceCurrentMeter(model);
        }

        [HttpGet("{id}/Schemas")]
        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            return _devicesService.GetSchemas(id);
        }
    }
}
