﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class AlarmsController : Controller
    {
        private IAlarmsService _alarmsService;

        public AlarmsController(IAlarmsService alarmsService)
        {
            _alarmsService = alarmsService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _alarmsService.GetAlarm(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _alarmsService.GetAlarms(request);
        }

        [HttpGet("{idOperator}/summary")]
        public ResponseListModel<object> GetSummary(int idOperator)
        {
            return _alarmsService.GetAlarmsSummary(idOperator);
        }

        [HttpPost("ConfirmAlarms")]
        public GridModel ConfirmAlarms([FromBody]long[] ids)
        {
            return _alarmsService.ConfirmAlarms(ids);
        }
    }

}
