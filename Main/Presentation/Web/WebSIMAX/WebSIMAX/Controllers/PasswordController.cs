﻿using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using SIMAX.Models;

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    public class PasswordController : Controller
    {
        private IPasswordService _passwordService;

        public PasswordController(IPasswordService passwordService)
        {
            _passwordService = passwordService;
        }

        [HttpPost]
        public SimpleValueModel<string> Post([FromBody]PasswordResetEmailModel model)
        {
            var sessionStatus = _passwordService.Reset(model.User, model.Locale);
            return new SimpleValueModel<string>(sessionStatus);
        }

        [HttpPut]
        public PasswordResetResponseModel Put([FromBody]PasswordResetModel model)
        {
            return _passwordService.Change(model.Guid, model.Password, model.Locale);
        }
    }
}
