﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using SIMAX.Utils;
using SIMAX.Models;
using SIMAX.Utils.Helpers;
using Newtonsoft.Json;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using Microsoft.AspNetCore.Http;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private ISessionService _session;

        public LoginController(ISessionService session)
        {
            _session = session;
        }

        [HttpPost]
        public UserModel Post([FromBody] LoginModel login)
        {
            if (String.IsNullOrWhiteSpace(login.User) || String.IsNullOrWhiteSpace(login.Password))
                throw new StatusCodeException(500, SESSION_STATUS.LOGIN_NOT_VALID.ToString());

            var sessionInitData = _session.InitSession(login.User, login.Password);

            if (sessionInitData.Status != SESSION_STATUS.OK)
                throw new StatusCodeException(500, sessionInitData.Status.ToString());

            if (String.IsNullOrWhiteSpace(login.Locale)) login.Locale = "pl-PL";
            var loginResult = _session.Login(sessionInitData, login.User, login.Password, login.Locale);

            if (loginResult.Status != SESSION_STATUS.OK)
                throw new StatusCodeException(500, loginResult.Status.ToString());

            var loggedUser = _session.GetLoggedUser(loginResult);

            if (loggedUser == null)
            {
                throw new StatusCodeException(500, "Logged user is null");
            }

            var payload = new Dictionary<string, object>()
            {
                { "username", login.User }, { "session", JsonConvert.SerializeObject(loginResult) }, { "distributorId", loggedUser.IdDistributor }, { "operatorId", loggedUser.IdOperator }
            };

            var serialized = JsonConvert.SerializeObject(loggedUser, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            if (sessionInitData != null && sessionInitData.Error != null)
            {
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Error in downloading dictionaries");
            }

            HttpContext.Session.SetString(Common.LOGGED_USER, serialized);

            var token = JWTHelper.EncodeToken(payload);
            var user = new UserModel()
            {
                Username = login.User,
                Token = token,
                Permissions = GetUserPermissions(loggedUser),
                Id = loggedUser.IdOperator,
                IdDistributor = loggedUser.IdDistributor,
                Avatar = loggedUser.Actor.Avatar != null ? Convert.ToBase64String(loggedUser.Actor.Avatar) : null
            };

            return user;
        }

        [HttpDelete]
        public void Delete()
        {
            var logoutResult = _session.Logout();

            if (logoutResult != SESSION_STATUS.OK)
                throw new StatusCodeException(500, logoutResult.ToString());
        }

        private List<string> GetUserPermissions(OpOperator loggedUser)
        {
            var permissions = new List<string>();

            if (loggedUser.HasPermission(Activity.WEBSIMAX_MAIN_MENU_VIEW)) permissions.Add("WEBSIMAX_MAIN_MENU_VIEW");

            #region // dashboard
            if (loggedUser.HasPermission(Activity.WEBSIMAX_DASHBOARD_VIEW)) permissions.Add("DASHBOARD_VIEW");
            #endregion

            #region // distributors
            if (loggedUser.HasPermission(Activity.DISTRIBUTOR_LIST)) permissions.Add("DISTRIBUTOR_LIST");
            if (loggedUser.HasPermission(Activity.DISTRIBUTOR_EDIT)) permissions.Add("DISTRIBUTOR_EDIT");
            if (loggedUser.HasPermission(Activity.DISTRIBUTOR_ADD)) permissions.Add("DISTRIBUTOR_ADD");
            if (loggedUser.HasPermission(Activity.DISTRIBUTOR_DEL)) permissions.Add("DISTRIBUTOR_DEL");
            #endregion

            #region // measurements
            if (loggedUser.HasPermission(Activity.MEASUREMENTS_LIST)) permissions.Add("MEASUREMENTS_LIST");
            #endregion

            #region// locations
            if (loggedUser.HasPermission(Activity.LOCATION_LIST)) permissions.Add("LOCATION_LIST");
            if (loggedUser.HasPermission(Activity.LOCATION_EDIT)) permissions.Add("LOCATION_EDIT");
            if (loggedUser.HasPermission(Activity.LOCATION_ADD)) permissions.Add("LOCATION_ADD");
            if (loggedUser.HasPermission(Activity.LOCATION_DEL)) permissions.Add("LOCATION_DEL");
            if (loggedUser.HasPermission(Activity.LOCATION_EXPORT_OP)) permissions.Add("LOCATION_EXPORT");
            if (loggedUser.HasPermission(Activity.LOCATION_IMPORT_OP)) permissions.Add("LOCATION_IMPORT");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationParameters)) permissions.Add("LOCATION_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationHistory)) permissions.Add("LOCATION_HISTORY");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationGroups)) permissions.Add("LOCATION_GROUPS");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationInstalledDevices)) permissions.Add("LOCATION_INSTALLED_DEVICES");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationInstalledMeters)) permissions.Add("LOCATION_INSTALLED_METERS");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationSchema)) permissions.Add("LOCATION_SCHEMA");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationGeneralSchema)) permissions.Add("LOCATION_GENERAL_SCHEMA");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationExtendedParameters)) permissions.Add("LOCATION_EXTENDED_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationMap)) permissions.Add("LOCATION_MAP");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationMeasurement)) permissions.Add("LOCATION_MEASUREMENT");
            if (!loggedUser.HasPermission(Activity.LOCATION_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.LocationMeasurementPanel)) permissions.Add("LOCATION_MEASUREMENT_PANEL");
            #endregion

            #region// devices
            if (loggedUser.HasPermission(Activity.DEVICE_LIST)) permissions.Add("DEVICE_LIST");
            if (loggedUser.HasPermission(Activity.DEVICE_EDIT)) permissions.Add("DEVICE_EDIT");
            if (loggedUser.HasPermission(Activity.DEVICE_ADD))  permissions.Add("DEVICE_ADD");
            if (loggedUser.HasPermission(Activity.DEVICE_DEL)) permissions.Add("DEVICE_DEL");
            if (loggedUser.HasPermission(Activity.DEVICE_EXPORT_OP)) permissions.Add("DEVICE_EXPORT");
            if (loggedUser.HasPermission(Activity.DEVICE_IMPORT_OP)) permissions.Add("DEVICE_IMPORT");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceHistory)) permissions.Add("DEVICE_HISTORY");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceParameters)) permissions.Add("DEVICE_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceExtendedParameters)) permissions.Add("DEVICE_EXTENDED_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceParenntDevices)) permissions.Add("DEVICE_PARENT_DEVICES");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceChildDevices)) permissions.Add("DEVICE_CHILD_DEVICES");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceGeneralSchema)) permissions.Add("DEVICE_GENERAL_SCHEMA");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.DeviceMeasurements)) permissions.Add("DEVICE_MEASUREMENT");
            #endregion

            #region// meters
            if (loggedUser.HasPermission(Activity.METER_LIST)) permissions.Add("METER_LIST");
            if (loggedUser.HasPermission(Activity.METER_EDIT)) permissions.Add("METER_EDIT");
            if (loggedUser.HasPermission(Activity.METER_ADD)) permissions.Add("METER_ADD");
            if (loggedUser.HasPermission(Activity.METER_DEL)) permissions.Add("METER_DEL");
            if (loggedUser.HasPermission(Activity.METER_EXPORT_OP)) permissions.Add("METER_EXPORT");
            if (loggedUser.HasPermission(Activity.METER_IMPORT_OP)) permissions.Add("METER_IMPORT");
            if (!loggedUser.HasPermission(Activity.METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterHistory)) permissions.Add("METER_HISTORY");
            if (!loggedUser.HasPermission(Activity.METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterParameters)) permissions.Add("METER_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterDevices)) permissions.Add("METER_DEVICES");
            if (!loggedUser.HasPermission(Activity.METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterDevices)) permissions.Add("METER_ALARM_GROUPS");
            if (!loggedUser.HasPermission(Activity.METER_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterDevices)) permissions.Add("METER_ALARM_DEFINITIONS");
            if (!loggedUser.HasPermission(Activity.DEVICE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.MeterGeneralSchema))  permissions.Add("METER_GENERAL_SCHEMA");
            #endregion

            #region// operator
            if (loggedUser.HasPermission(Activity.OPERATOR_LIST)) permissions.Add("OPERATOR_LIST");
            if (loggedUser.HasPermission(Activity.OPERATOR_EDIT)) permissions.Add("OPERATOR_EDIT");
            if (loggedUser.HasPermission(Activity.OPERATOR_ADD)) permissions.Add("OPERATOR_ADD");
            if (loggedUser.HasPermission(Activity.OPERATOR_DEL)) permissions.Add("OPERATOR_DEL");
            if (loggedUser.HasPermission(Activity.OPERATOR_EXPORT_OP)) permissions.Add("OPERATOR_EXPORT");
            if (loggedUser.HasPermission(Activity.OPERATOR_IMPORT_OP)) permissions.Add("OPERATOR_IMPORT");
            if (loggedUser.HasPermission(Activity.OPERATOR_NOTIFICATIONS_LIST)) permissions.Add("OPERATOR_NOTIFICATIONS_LIST"); //uprawnienia do wyświetlania zakładki powiadomienia
            if (loggedUser.HasPermission(Activity.OPERATOR_PERMISSIONS_EDIT)) permissions.Add("OPERATOR_PERMISSIONS_EDIT");
            if (loggedUser.HasPermission(Activity.OPERATOR_PERMISSIONS_EFFECTIVE_PERMISSIONS)) permissions.Add("OPERATOR_EFFECTIVE_PERMISSIONS"); //widok zakladki obowiazujace uprawnienia
            if (loggedUser.HasPermission(Activity.OPERATOR_PACKAGE_NOTIFICATIONS_EDIT)) permissions.Add("OPERATOR_NOTIFICATIONS_EDIT"); // uprawnienia do edycji powiadomień
            if (loggedUser.HasPermission(Activity.OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE)) permissions.Add("OPERATOR_PERMISSIONS_CUSTOM_PERM_CHOOSE"); //widok zakladki niestandardowe uprawnienia
            if (loggedUser.HasPermission(Activity.OPERATOR_PERMISSIONS_ROLE_CHOOSE)) permissions.Add("OPERATOR_PERMISSIONS_ROLE_CHOOSE"); // widok zakladki role
            if (loggedUser.HasPermission(Activity.ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES)) permissions.Add("ALLOWED_OPERATOR_TO_EDIT_ACTIVITIES"); //uprawnienia do edycji zakladki role i niestandardowe uprawnienia 
            #endregion

            #region// issues
            if (loggedUser.HasPermission(Activity.ISSUE_LIST)) permissions.Add("ISSUE_LIST");
            if (loggedUser.HasPermission(Activity.ISSUE_ADD)) permissions.Add("ISSUE_ADD");
            if (loggedUser.HasPermission(Activity.ISSUE_EDIT)) permissions.Add("ISSUE_EDIT");
            if (loggedUser.HasPermission(Activity.ISSUE_DEL)) permissions.Add("ISSUE_DEL");
            if (!loggedUser.HasPermission(Activity.ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.IssueParameters)) permissions.Add("ISSUE_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.IssueHistory)) permissions.Add("ISSUE_HISTORY");
            if (!loggedUser.HasPermission(Activity.ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.IssueTasks)) permissions.Add("ISSUE_TASKS");
            if (!loggedUser.HasPermission(Activity.ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.IssueAttachments)) permissions.Add("ISSUE_ATTACHMENTS");
            if (!loggedUser.HasPermission(Activity.ISSUE_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.IssueEquipment)) permissions.Add("ISSUE_EQUIPMENT");
            #endregion

            #region// tasks
            if (loggedUser.HasPermission(Activity.TASK_LIST)) permissions.Add("TASK_LIST");
            if (loggedUser.HasPermission(Activity.TASK_ADD)) permissions.Add("TASK_ADD");
            if (loggedUser.HasPermission(Activity.TASK_EXPORT_OP)) permissions.Add("TASK_EXPORT");
            if (loggedUser.HasPermission(Activity.TASK_EDIT)) permissions.Add("TASK_EDIT");
            if (loggedUser.HasPermission(Activity.TASK_DEL)) permissions.Add("TASK_DEL");
            if (loggedUser.HasPermission(Activity.TASK_DEL)) permissions.Add("TASK_DEL");
            if (loggedUser.HasPermission(Activity.ALLOWED_TASK_STATUS_EDIT)) permissions.Add("ALLOWED_TASK_STATUS_EDIT");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.TaskParameters)) permissions.Add("TASK_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.TaskAttachments)) permissions.Add("TASK_ATTACHMENTS");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.TaskEquipment)) permissions.Add("TASK_EQUIPMENT");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.TaskHistory)) permissions.Add("TASK_HISTORY");
            #endregion

            #region// alarms
            if (loggedUser.HasPermission(Activity.ALARM_EVENT_LIST)) permissions.Add("ALARM_LIST");
            if (loggedUser.HasPermission(Activity.ALARM_EVENT_EDIT)) permissions.Add("ALARM_EDIT");
            if (loggedUser.HasPermission(Activity.ALARM_EVENT_DEL)) permissions.Add("ALARM_DEL");
            if (loggedUser.HasPermission(Activity.ALARMS_CONFIRMATION_ENABLED)) permissions.Add("ALARMS_CONFIRMATION_ENABLED");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.AlarmParameters)) permissions.Add("ALARM_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.AlarmMap)) permissions.Add("ALARM_MAP");
            if (!loggedUser.HasPermission(Activity.TASK_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.AlarmHistory)) permissions.Add("ALARM_HISTORY");
            #endregion

            #region// routes
            if (loggedUser.HasPermission(Activity.ROUTE_LIST)) permissions.Add("ROUTE_LIST");
            if (loggedUser.HasPermission(Activity.ROUTE_ADD)) permissions.Add("ROUTE_ADD");
            if (loggedUser.HasPermission(Activity.ROUTE_EDIT)) permissions.Add("ROUTE_EDIT");
            if (loggedUser.HasPermission(Activity.ROUTE_DEL)) permissions.Add("ROUTE_DEL");
            if (loggedUser.HasPermission(Activity.ROUTE_EXPORT_OP)) permissions.Add("ROUTE_EXPORT");
            if (loggedUser.HasPermission(Activity.ROUTE_IMPORT_OP)) permissions.Add("ROUTE_IMPORT");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.RoutePoints)) permissions.Add("ROUTE_POINTS");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.RouteAssignedOperators)) permissions.Add("ROUTE_ASSIGNED_OPERATOR");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.RouteParameters)) permissions.Add("ROUTE_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.RouteMap)) permissions.Add("ROUTE_MAP");
            #endregion

            #region// reports
            if (loggedUser.HasPermission(Activity.REPORTS_LIST)) permissions.Add("REPORTS_LIST");
            if (loggedUser.HasPermission(Activity.REPORT_ADD)) permissions.Add("REPORT_ADD");
            //if (loggedUser.HasPermission(Activity.REPORT_DEL)) permissions.Add("REPORT_DEL"); hide delete button for now (Piela)
            if (loggedUser.HasPermission(Activity.REPORT_EDIT)) permissions.Add("REPORT_EDIT");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.ReportParameters)) permissions.Add("REPORT_PARAMETERS");
            if (!loggedUser.HasPermission(Activity.ROUTE_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.ReportGenereation)) permissions.Add("REPORT_GENERATE");
            #endregion

            #region// SIM Cards
            if (loggedUser.HasPermission(Activity.SIM_CARD_LIST)) permissions.Add("SIM_CARD_LIST");
            if (loggedUser.HasPermission(Activity.SIM_CARD_ADD)) permissions.Add("SIM_CARD_ADD");
            if (loggedUser.HasPermission(Activity.SIM_CARD_EDIT)) permissions.Add("SIM_CARD_EDIT");
            if (loggedUser.HasPermission(Activity.SIM_CARD_DEL)) permissions.Add("SIM_CARD_DEL");
            if (!loggedUser.HasPermission(Activity.SIM_CARD_DETAILS_HIDDEN_UI_OBJECT_PROPERTIES, Enums.UIObjectProperties.SimCardHistory)) permissions.Add("SIM_CARD_HISTORY");
            if (loggedUser.HasPermission(Activity.SIM_CARD_EXPORT_OP)) permissions.Add("SIM_CARD_EXPORT_OP");
            if (loggedUser.HasPermission(Activity.SIM_CARD_IMPORT_OP)) permissions.Add("SIM_CARD_IMPORT_OP");
            #endregion

            #region// contacts
            if (loggedUser.HasPermission(Activity.ACTOR_LIST)) permissions.Add("CONTACT_LIST");
            if (loggedUser.HasPermission(Activity.ACTOR_ADD)) permissions.Add("CONTACT_ADD");
            if (loggedUser.HasPermission(Activity.ACTOR_EDIT)) permissions.Add("CONTACT_EDIT");
            if (loggedUser.HasPermission(Activity.ACTOR_DEL)) permissions.Add("CONTACT_DEL");            
            //if (loggedUser.HasPermission(Activity.ACTOR_EXPORT_OP)) 
            //    permissions.Add("CONTACT_EXPORT");
            if (!loggedUser.HasPermission(Activity.ACTOR_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.ActorGroups)) permissions.Add("CONTACT_GROUPS_EDIT");
            if (!loggedUser.HasPermission(Activity.ACTOR_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.ActorLocations)) permissions.Add("CONTACT_LOCATIONS_EDIT");
            if (loggedUser.HasPermission(Activity.ACTOR_DEL)) permissions.Add("CONTACT_DEL");
            if (loggedUser.HasPermission(Activity.ACTOR_IMPORT_OP)) permissions.Add("CONTACT_IMPORT");
            #endregion

            #region// actions

            if (loggedUser.HasPermission(Activity.ACTION_LIST)) permissions.Add("ACTION_LIST");
            if (loggedUser.HasPermission(Activity.ACTION_DETAILS_HIDDEN_UI_PROPERTIES, Enums.UIObjectProperties.ActionParameters)) permissions.Add("ACTION_PARAMETERS");

            #endregion

            #region// deviceconnections
            //TODO: uprawnienia
            if (loggedUser.HasPermission(Activity.DEVICE_CONNECTION_LIST, Enums.UIObjectProperties.ActionParameters))
                permissions.Add("DEVICE_CONNECTION_LIST");

            #endregion

            return permissions;
        }
    }
}
