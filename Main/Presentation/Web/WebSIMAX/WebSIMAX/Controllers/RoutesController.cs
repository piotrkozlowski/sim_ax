﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class RoutesController : Controller
    {
        private IRoutesService _routesService;

        public RoutesController(IRoutesService routesService)
        {
            _routesService = routesService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        { 
            return _routesService.GetRoute(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetRouteToEdit(int id)
        {
            return _routesService.GetRouteToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _routesService.GetRoutes(request);
        }

        [HttpPost("Locations")]
        public GridModel GetRoutePoints([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _routesService.GetRoutePoints(request);
        }

        [HttpPost("Operators")]
        public GridModel GetRouteOperators([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _routesService.GetRouteOperators(request);
        }

        [HttpPut]
        public RouteModel Put([FromBody]RouteFormValuesModel formModel)
        {
            return _routesService.SaveRoute(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _routesService.DeleteRoute(id);
        }

        [HttpPost("Import/Init")]
        public object InitializeImport([FromBody]ImportDataModel model)
        {
            return _routesService.InitializeImport(model);
        }

        [HttpGet("Import/{guid}/Start")]
        public object StartImport(string guid)
        {
            return _routesService.StartImport(guid);
        }

        [HttpGet("Import/{guid}/Stop")]
        public object StopImport(string guid)
        {
            return _routesService.StopImport(guid);
        }

        [HttpGet("Import/{guid}/Progress")]
        public object ImportProgress(string guid)
        {
            return _routesService.ImportProgress(guid);
        }

        [HttpGet("Import/{guid}/Summary")]
        public object ImportSummary(string guid)
        {
            return _routesService.ImportSummary(guid);
        }
    }
}
