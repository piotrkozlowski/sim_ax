﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;
using System;
using System.Net;
using SIMAX.Utils;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class IssuesController : Controller
    {
        private IIssuesService _issuesService;

        public IssuesController(IIssuesService issuesService)
        {
            _issuesService = issuesService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _issuesService.GetIssue(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetIssueToEdit(int id)
        {
            return _issuesService.GetIssueToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _issuesService.GetIssues(request);
        }

        [HttpPut]
        public IssueModel Put([FromBody]FormValuesModel formModel)
        {
            return _issuesService.SaveIssue(formModel);
        }

        [HttpGet("{id}/Attachments")]
        public ResponseListModel<FileModel> GetAttachments(long id)
        {
            return _issuesService.GetAttachments(id);
        }

        [HttpGet("{id}/CheckIfAttachmentExists/{attachmentId}")]
        public string CheckIfAttachmentExists(int id, long attachmentId)
        {
            var jwt = Request.Cookies["jwt"];
            if (!String.IsNullOrWhiteSpace(jwt))
            {
                return _issuesService.CheckIfAttachmentExists(id, attachmentId);
            }
            throw new StatusCodeException((int)HttpStatusCode.Unauthorized);
        }

        [HttpPost("Attachments")]
        public SaveFileModel SaveAttachments([FromBody]FileModel model)
        {
            return _issuesService.UploadAttachment(model);
        }

        [HttpPut("Attachments")]
        public ResponseListModel<long> DeleteAttachments([FromBody]FormValuesModel model)
        {
            return _issuesService.DeleteAttachments(model);
        }

        [HttpGet("{id}/Equipment")]
        public ResponseListModel<LocationEquipmentModel> GetEquipment(long id)
        {
            return _issuesService.GetEquipment(id);
        }

        [HttpDelete("{id}")]
        public ResponseDeleteModel Delete(int id)
        {
            return _issuesService.DeleteIssue(id);
        }

    }
}
