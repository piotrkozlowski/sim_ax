﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using SIMAX.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UsersController : Controller
    {
        private IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel req)
        {
            return _usersService.GetUsers(req);
        }

        [HttpGet("{id}")]
        public OperatorModel Get(int id)
        {
            return _usersService.GetUser(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetUserToEdit(int id)
        {
            return _usersService.GetUserToEdit(id);
        }

        [HttpPut]
        public OperatorModel Put([FromBody]FormValuesModel formModel)
        {
            return _usersService.SaveUser(formModel);
        }

        [HttpPut("Activity/Change")]
        public OperatorModel PutActivity([FromBody]SecurityChangeModel body)
        {
            return _usersService.SubmitActivities(body);
        }

        [HttpPut("Role/Change")]
        public OperatorModel PutRole([FromBody]SecurityChangeModel body)
        {
            return _usersService.SubmitRoles(body);
        }

        [HttpPost("Notification/Change")]
        public OperatorModel ChangeNotification([FromBody]NotificationModel model)
        {
            return _usersService.ChangeNotification(model);
        }

        [HttpPost("Activity/Reference")]
        public OperatorModel ChangeReferenceObject([FromBody]SecurityActivityModel model)
        {
            return _usersService.ChangeReferenceObject(model);
        }

        [HttpPost("Distributors/Change")]
        public ResponseListModel<int> ChangeAllowedDistributors([FromBody]FormValuesModel model)
        {
            return _usersService.ChangeAllowedDistributors(model);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _usersService.DeleteUser(id);
        }

        [HttpPost("Password/Change")]
        public PasswordResetResponseModel ChangePassword([FromBody] PasswordResetModel model)
        {            
            return _usersService.ChangePassword(model);
        }
    }
}
