﻿using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Collections.Generic;
using MsgPack.Serialization;
using System.IO;
using Newtonsoft.Json;
using System.Linq;
using System;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class DictionaryController : Controller
    {
        private IDictionaryService _dictionaryService;

        public DictionaryController(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }

        [HttpGet]
        [ActionName("Version")]
        public SimpleValueModel<int> GetVersion()
        {
            return new SimpleValueModel<int>(_dictionaryService.GetVersion());
        }

        [HttpGet]
        [ActionName("")]
        public SimpleValueModel<string> Get()
        {
            var dict = _dictionaryService.GetDictionaries();
            var serializer = SerializationContext.Default.GetSerializer<DictionaryModel>();
            var bytes = serializer.PackSingleObject(dict);
            return new SimpleValueModel<string>(Convert.ToBase64String(bytes));
        }
    }
}
