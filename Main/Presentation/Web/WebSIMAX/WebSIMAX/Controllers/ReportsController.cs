﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using SIMAX.Models;
using Microsoft.AspNetCore.Authorization;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860
namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ReportsController : Controller
    {
        private IReportsService _reportsService;

        public ReportsController(IReportsService reportsService)
        {
            _reportsService = reportsService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            return _reportsService.GetReports(request);
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _reportsService.GetReport(id);
        }        

        [HttpPut]
        public ReportModel Put([FromBody]FormValuesModel formModel)
        {
            return _reportsService.SaveReport(formModel);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetReportToEdit(int id)
        {
            return _reportsService.GetReportToEdit(id);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _reportsService.DeleteReport(id);
        }

        [HttpGet("{id}/Generate")]
        public GeneratedReportModel GenerateReport(int id)
        {
            return _reportsService.GenerateReport(id);
        }
    }
}
