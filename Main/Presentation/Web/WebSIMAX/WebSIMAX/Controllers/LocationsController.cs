﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;
using System;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class LocationsController : Controller
    {
        private ILocationsService _locationsService;

        public LocationsController(ILocationsService locationsService)
        {
            _locationsService = locationsService;
        }

        [HttpGet("{id}")]
        public LocationModel Get(long id)
        {
            return _locationsService.GetLocation(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetLocationToEdit(long id)
        {
            return _locationsService.GetLocationToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request) //OpLocation
        {
            if (request == null) return new GridModel();
            return _locationsService.GetLocations(request);
        }

        [HttpPut]
        public LocationModel Put([FromBody]LocationFormValuesModel formModel)
        {
            
            return _locationsService.SaveLocation(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(long id)
        {
            return _locationsService.DeleteLocation(id);
        }

        [HttpGet("{id}/Schema")]
        public SchemaModel GetSchema(long id)
        {
            return _locationsService.GetSchema(id);
        }

        [HttpGet("{id}/Schemas")]
        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            return _locationsService.GetSchemas(id);
        }

        [HttpPost("Import/Init")]
        public ImportStatusModel InitializeImport([FromBody]ImportDataModel model)
        {
            return _locationsService.InitializeImport(model);
        }

        [HttpGet("Import/{guid}/Start")]
        public object StartImport(string guid)
        {
            return _locationsService.StartImport(guid);
        }

        [HttpGet("Import/{guid}/Stop")]
        public object StopImport(string guid)
        {
            return _locationsService.StopImport(guid);
        }

        [HttpGet("Import/{guid}/Progress")]
        public ImportProgressModel ImportProgress(string guid)
        {
            return _locationsService.ImportProgress(guid);
        }

        [HttpGet("Import/{guid}/Summary")]
        public ImportStatusModel ImportSummary(string guid)
        {
            return _locationsService.ImportSummary(guid);
        }
    }
}
