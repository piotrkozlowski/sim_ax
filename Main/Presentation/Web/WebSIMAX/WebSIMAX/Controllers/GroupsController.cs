﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class GroupsController : Controller
    {
        private IGroupsService _groupsService;

        public GroupsController(IGroupsService groupsService)
        {
            _groupsService = groupsService;
        }

        [HttpGet]
        [ActionName("locations")]
        public ResponseListModel<GroupModel> GetLocationGroups()
        {
            return _groupsService.GetLocationGroups();
        }
    }
}
