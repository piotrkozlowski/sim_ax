﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class SimcardsController : Controller
    {
        private ISimcardsService _simcardService;

        public SimcardsController(ISimcardsService simcardService)
        {
            _simcardService = simcardService;
        }

        [HttpGet("{id}")]
        public object Get(int id)
        {
            return _simcardService.GetSimcard(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetSimcardToEdit(int id)
        {
            return _simcardService.GetSimcardToEdit(id);
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _simcardService.GetSimcards(request);
        }

        [HttpPut]
        public EntityModel Put([FromBody]FormValuesModel formModel)
        {
            return _simcardService.SaveSimcard(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _simcardService.DeleteSimcard(id);
        }

        [HttpPost("Import/Init")]
        public object InitializeImport([FromBody]ImportDataModel model)
        {
            return _simcardService.InitializeImport(model);
        }

        [HttpGet("Import/{guid}/Start")]
        public object StartImport(string guid)
        {
            return _simcardService.StartImport(guid);
        }

        [HttpGet("Import/{guid}/Stop")]
        public object StopImport(string guid)
        {
            return _simcardService.StopImport(guid);
        }

        [HttpGet("Import/{guid}/Progress")]
        public object ImportProgress(string guid)
        {
            return _simcardService.ImportProgress(guid);
        }

        [HttpGet("Import/{guid}/Summary")]
        public object ImportSummary(string guid)
        {
            return _simcardService.ImportSummary(guid);
        }
    }
}
