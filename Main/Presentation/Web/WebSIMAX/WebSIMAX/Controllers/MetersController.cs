﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Services;
using SIMAX.Models;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class MetersController : Controller
    {
        private IMetersService _metersService;

        public MetersController(IMetersService metersService)
        {
            _metersService = metersService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _metersService.GetMeters(request);
        }

        [HttpGet("{id}")]
        public MeterModel Get(int id)
        {
            return _metersService.GetMeter(id);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetMeterToEdit(int id)
        {
            return _metersService.GetMeterToEdit(id);
        }

        [HttpPut]
        public MeterModel Put([FromBody]FormValuesModel formModel)
        {
            return _metersService.SaveMeter(formModel);
        }

        [HttpDelete("{id}")]
        public object Delete(int id)
        {
            return _metersService.DeleteMeter(id);
        }

        [HttpPost("Device")]
        public MeterModel SaveMeterCurrentDevice([FromBody]ObjectReferenceModel model)
        {
            return _metersService.SaveMeterCurrentDevice(model);
        }

        [HttpGet("{id}/Schemas")]
        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            return _metersService.GetSchemas(id);
        }
    }
}
