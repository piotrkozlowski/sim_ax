﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SIMAX.Utils;
using SIMAX.Models;
using SIMAX.Services;
using System.Net;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.Text;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    public class AppController : Controller
    {
        private IAppService _appService;

        public AppController(IAppService appService)
        {
            _appService = appService;
        }

        [HttpGet("{culture}/Resources")]
        public ContentResult JsonResources(string culture)
        {
            var resources = new JavascriptResourceHelper.JavaScriptResources(new System.Globalization.CultureInfo(culture));
            
            var result = JsonConvert.SerializeObject(resources);

            return Content(result);
        }

        [HttpPost("Activities")]
        public GridModel GetActivities()
        {
            return _appService.GetActivities();
        }

        [HttpPost("Roles")]
        public GridModel GetRoles()
        {
            return _appService.GetRoles();
        }

        [HttpPost("Reference")]
        public GridModel GetReferenceObjectList([FromBody]GridRequestReferenceModel model)
        {
            return _appService.GetReferenceObjectList(model);
        }

        [HttpGet("Task/{id}/Attachments/{attachmentId}")]
        public FileResult GetTaskAttachment(int id, long attachmentId)
        {
            return GetAttachment(id, attachmentId, _appService.DownloadTaskAttachment);
        }

        [HttpGet("Issue/{id}/Attachments/{attachmentId}")]
        public FileResult GetIssueAttachment(int id, long attachmentId)
        {
            return GetAttachment(id, attachmentId, _appService.DownloadIssueAttachment);
        }

        private FileResult GetAttachment(int id, long attachmentId, DownloadAttachment downloadAttachment)
        {
            var jwt = Request.Cookies["jwt"];
            if (!String.IsNullOrWhiteSpace(jwt))
            {
                var opFile = downloadAttachment(id, attachmentId, jwt);
                if (opFile.FileBytes == null)
                {
                    opFile.FileBytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FileDoesNotExist);
                    opFile.PhysicalFileName += ".txt";
                }
                return File((byte[])opFile.FileBytes, "application/octet-stream", opFile.PhysicalFileName);
            }

            throw new StatusCodeException((int)HttpStatusCode.Unauthorized);
        }

        [HttpGet("Locations/Export/{token}")]
        public FileResult ExportLocationsXls(string token)
        {
            var bytes = ExportData(token,_appService.ExportLocations);
            if (bytes == null)
            {
                bytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FailedToExportFile);
                return File(bytes, "text/plain", "Locations.xls");
            }
            return File(bytes, "application/excel", "Locations.xls");
        }

        [HttpGet("Meters/Export/{token}")]
        public FileResult ExportMetersXls(string token)
        {
            var bytes = ExportData(token, _appService.ExportMeters);
            if (bytes == null) {
                bytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FailedToExportFile);
                return File(bytes, "text/plain", "Meters.xls");
            }
            return File(bytes, "application/excel", "Meters.xls");
        }

        [HttpGet("Devices/Export/{token}")]
        public FileResult ExportDevicessXls(string token)
        {
            var bytes = ExportData(token, _appService.ExportDevices);
            if (bytes == null)
            {
                bytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FailedToExportFile);
                return File(bytes, "text/plain", "Devices.xls");
            }
            return File(bytes, "application/excel", "Devices.xls");
        }

        [HttpGet("Tasks/Export/{token}")]
        public FileResult ExportTasksXls(string token)
        {
            var bytes = ExportData(token, _appService.ExportTasks);
            if (bytes == null)
            {
                bytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FailedToExportFile);
                return File(bytes, "text/plain", "Tasks.xls");
            }
            return File(bytes, "application/excel", "Tasks.xls");
        }

        [HttpGet("Routes/Export/{token}")]
        public FileResult ExportRoutesXls(string token)
        {
            var bytes = ExportData(token, _appService.ExportRoutes);
            if (bytes == null)
            {
                bytes = System.Text.Encoding.Default.GetBytes(Resources.Common.Common.FailedToExportFile);
                return File(bytes, "text/plain", "Routes.xls");
            }
            return File(bytes, "application/excel", "Routes.xls");
        }

        private byte[] ExportData(string token, Export exportData)
        {
            if (!String.IsNullOrWhiteSpace(token))
            {
                var jwt = Request.Cookies["jwt"];
                var bytes = exportData(jwt, token);
                return bytes;
            }
            throw new StatusCodeException((int)HttpStatusCode.Unauthorized);
        }

        [HttpPost("RequestModel")]
        public SimpleValueModel<string> SaveRequestModel([FromBody] GridRequestModel requestModel)
        {
            var token = _appService.SaveRequestModel(requestModel);           
            return new SimpleValueModel<string>(token);
        }

        [HttpPost("Excel")]
        public GridModel ImportExcelFile([FromBody]FileColumnModel model)
        {
            return _appService.ImportExcelFile(Convert.FromBase64String(model.FileBytes.Split(',')[1]), model.PhysicalFileName, model.Columns);
        }


        [HttpPost("ContextList")]
        public object GetContextList([FromBody] ContextListModel contextListModel)
        {
            return _appService.GetContextList(contextListModel);
        }

        [HttpPost("Logo")]
        public object GetLogo([FromBody] LogoModel model)
        {
            if (!String.IsNullOrWhiteSpace(model.jwt))
            {
                var logo = _appService.GetLogo(model.Id, model.jwt);
                try
                {
                    if (logo == null) return null;
                    return new { data = Convert.ToBase64String((byte[])logo.FileBytes), ext = logo.Extension };
                } catch
                {
                    return null;
                }
            }

            throw new StatusCodeException((int)HttpStatusCode.Unauthorized);
            
        }

        private delegate OpFile DownloadAttachment(int modelId, long attachmentId, string jwt);
        private delegate byte[] Export(string jwt, string token);
    }
}
