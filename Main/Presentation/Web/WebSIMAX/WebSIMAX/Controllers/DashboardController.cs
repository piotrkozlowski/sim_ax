﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DashboardController : Controller
    {
        private IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        [HttpGet]
        public DashboardModel Get()
        {
            return _dashboardService.GetDashboardConfig();
        }

        [HttpPost]
        public ResponseListModel<object> Post([FromBody]DashboardRequestModel request)
        {
            return _dashboardService.GetDashbordItems(request.Ids.ToArray(), request.GroupId);
        }
    }
}
