﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using SIMAX.Services;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DistributorsController : Controller
    {
        private IDistributorsService _distributorsService;

        public DistributorsController(IDistributorsService distributorsService)
        {
            _distributorsService = distributorsService;
        }

        [HttpPost]
        public GridModel Post([FromBody]GridRequestModel request)
        {
            if (request == null) return new GridModel();
            return _distributorsService.GetDistributors(request);
        }

        [HttpGet("{id}")]
        public DistributorModel Get(int id)
        {
            return _distributorsService.GetDistributor(id);
        }

        [HttpPost("All")]
        public GridModel GetAllDistributors([FromBody]GridRequestModel request)
        {
            return _distributorsService.GetAllDistributors(request);
        }

        [HttpGet("{id}/Edit")]
        public EntityModel GetDistributorToEdit(int id)
        {
            return _distributorsService.GetDistributorToEdit(id);
        }

        [HttpPut]
        public DistributorModel Put([FromBody]FormValuesModel formModel)
        {

            return _distributorsService.SaveDistributor(formModel);
        }

        [HttpDelete("{id}")]
        public ResponseDeleteModel Delete(int id)
        {
            return _distributorsService.DeleteDistributor(id);
        }
    }
}
