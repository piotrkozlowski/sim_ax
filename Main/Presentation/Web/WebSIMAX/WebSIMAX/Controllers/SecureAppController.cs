﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Services;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Models;
using System.Dynamic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class SecureAppController: Controller
    {
        private ISecureAppService _secureAppService;
        
        public SecureAppController(ISecureAppService secureAppService)
        {
            _secureAppService = secureAppService;
        }

        [HttpGet("{id}/Get")]
        public OperatorSettingsModel GetOperatorSettings(int id)
        {
            return _secureAppService.GetOperatorSettings(id);
        }

        [HttpPost("Save")]
        public bool SaveOperatorSettings([FromBody]OperatorSettingsModel request)
        {
            return _secureAppService.SaveOperatorSettings(request);
        }

    }
}
