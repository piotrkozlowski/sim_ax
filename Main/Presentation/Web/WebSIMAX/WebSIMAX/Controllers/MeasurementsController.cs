﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SIMAX.Services;
using SIMAX.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class MeasurementsController : Controller
    {
        private IMeasurementsService _measurementsService;

        public MeasurementsController(IMeasurementsService measurementsService)
        {
            _measurementsService = measurementsService;
        }

        [HttpPost]
        [ActionName("measurements-for-location")]
        public SimpleValueModel<string> GetMeasurementsForLocation([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetMeasurements(request.LocationIds, request.DeviceIds, request.MeterIds, request.Columns, request.DateFrom, request.DateTo));
        }

        [HttpPost]
        [ActionName("latest-measurements-for-location")]
        public SimpleValueModel<string> GetLatestMeasurementsForLocation([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetLatestMeasurements(request.LocationIds, request.DeviceIds, request.MeterIds, request.Columns));
        }

        [HttpPost]
        [ActionName("measurements-for-device")]
        public SimpleValueModel<string> GetMeasurementsForDevice([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetMeasurements(null, request.DeviceIds, null, request.Columns, request.DateFrom, request.DateTo));
        }

        [HttpPost]
        [ActionName("latest-measurements-for-device")]
        public SimpleValueModel<string> GetLatestMeasurementsForDevice([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetLatestMeasurements(null, request.DeviceIds, null, request.Columns));
        }

        [HttpPost]
        [ActionName("measurements-for-meter")]
        public SimpleValueModel<string> GetMeasurementsForMeter([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetMeasurements(null, null, request.MeterIds, request.Columns, request.DateFrom, request.DateTo));
        }

        [HttpPost]
        [ActionName("latest-measurements-for-meter")]
        public SimpleValueModel<string> GetLatestMeasurementsForMeter([FromBody]MeasurementsRequestModel request)
        {
            return new SimpleValueModel<string>(_measurementsService.GetLatestMeasurements(null, null, request.MeterIds, request.Columns));
        }
    }
}
