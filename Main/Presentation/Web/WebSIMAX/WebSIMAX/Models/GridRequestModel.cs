﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class GridRequestModel
    {
        public int PageSize { get; set; } = 20;
        public int PageNumber { get; set; } = 1;
        public List<ColumnModel> Columns { get; set; } = new List<ColumnModel>();
        public string Where { get; set; }
        public List<long> Ids { get; set; } = new List<long>();
    }
}
