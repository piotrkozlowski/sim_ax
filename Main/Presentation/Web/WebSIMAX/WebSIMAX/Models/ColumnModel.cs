﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ColumnModel
    {
        public ColumnModel()
        {

        }
        public long Id { get; set; }
        public string IdName { get; set; } = null;
        public string Caption { get; set; }
        public string Name { get; set; }
        public bool? SortDesc { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public int? ReferenceTypeId { get; set; }
        public int UnitId { get; set; } = 0;
        public string UnitName { get; set; } = "";
        public string[] Where { get; set; } = null;
        public string CellTemplate { get; set; }
        public object EditCellTemplate { get; set; }
        public int Width { get; set; }
        public bool IsEditable { get; set; }
        public int? Position { get; set; }
        public string Alignment { get; set; }
        public FormatModel Format { get; set; }
        public string ChartType { get; set; }
    }
}
