﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class IssueModel: EntityModel
    {
        public List<HistoryIssueModel> History { get; set; } = new List<HistoryIssueModel>();
        public List<TaskModel> Tasks { get; set; } = new List<TaskModel>();
        public long? IdLocation { get; set; }
    }
}
