﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class GridModel
    {
        public List<dynamic> Values { get; set; } = new List<object>();
        public long TotalCount { get; set; } = 0;
        public int HasErrorCode { get; set; } = 0;
    }
}
