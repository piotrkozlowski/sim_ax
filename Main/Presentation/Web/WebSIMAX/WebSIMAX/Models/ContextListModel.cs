﻿using IMR.Suite.Common;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ContextListModel
    {
        public long sourceType { get; set; }
        public long destinationType { get; set; }
        public object[] referenceValues { get; set; }
    }
}
