﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class LoginModel
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Locale { get; set; }
    }
}
