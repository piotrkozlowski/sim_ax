﻿using TypeLite;
using System.Collections.Generic;

namespace SIMAX.Models
{
    [TsClass]
    public class OperatorSettingsModel
    {
        public int IdOperator { get; set; }
        public string Settings { get; set; }
    }
}
