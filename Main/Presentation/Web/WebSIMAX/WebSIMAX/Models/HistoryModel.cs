﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class HistoryModel
    {
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public DateTime EventDate { get; set; }
        public string EventType { get; set; }
        public object EventObject { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
