﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class PasswordResetResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
