﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class FormValuesModel
    {
        public long? Id { get; set; }
        public List<FieldNameValueModel> Fields { get; set; } = new List<FieldNameValueModel>();
        public List<long> Values { get; set; } = new List<long>();
    }
}
