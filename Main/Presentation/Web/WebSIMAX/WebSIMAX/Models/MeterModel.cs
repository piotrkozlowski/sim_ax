﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class MeterModel : EntityModel
    {
        public List<EntityModel> Devices { get; set; } = new List<EntityModel>();
        public List<GroupModel> alarmGroups { get; set; } = new List<GroupModel>();
        public long? CurrentDeviceId { get; set; }
        public string CurrentDeviceName { get; set; }
        public bool AllowGrouping { get; set; }
    }
}
