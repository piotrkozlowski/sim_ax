﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class LocationModel : EntityModel
    {
        public List<DeviceModel> Devices { get; set; } = new List<DeviceModel>();
        public List<MeterModel> Meters { get; set; } = new List<MeterModel>();
        public List<GroupModel> Groups { get; set; } = new List<GroupModel>();
        public List<HistoryModel> History { get; set; } = new List<HistoryModel>();

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public long LocationId { get; set; }
        public bool AllowGrouping { get; set; }
    }
}
