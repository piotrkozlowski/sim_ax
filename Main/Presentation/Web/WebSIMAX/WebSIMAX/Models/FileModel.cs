﻿using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class FileModel
    {
        public int ParentEntityId { get; set; }
        public long Id { get; set; }
        public string PhysicalFileName { get; set; }
        public string FileBytes { get; set; }
        public double? Size { get; set; }
        public DateTime InsertDate { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public DateTime? LastDownloaded { get; set; }
        public bool IsBlocked { get; set; }


        public static FileModel FileModelFromOpFile(OpFile file)
        {
 

            return new FileModel()
            {
                Id = file.IdFile,
                PhysicalFileName = file.PhysicalFileName,
                Description = file.Description,
                InsertDate = file.InsertDate,
                IsBlocked = file.IsBlocked,
                LastDownloaded = file.LastDownloaded,
                Size = file.Size.HasValue ? (double?) Math.Round((((double)file.Size.Value) / 1024 / 1024), 2) : null,
                Version = file.Version
            };
        }
    }
}
