﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SecurityRoleModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ModuleName { get; set; }
        public string ValueString { get; set; }
    }
}
