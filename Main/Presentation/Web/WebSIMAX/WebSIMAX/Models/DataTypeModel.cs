﻿using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DataTypeModel
    {
        public string Caption { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public int? ReferenceTypeId { get; set; }
        public bool IsEditable { get; set; }
        public FormatModel Format { get; set; }
        public int UnitId { get; set; } = 0;
        public string UnitName { get; set; } = "";

        public static DataTypeModel DataTypeModelFromOpDataType(OpDataType op)
        {
            return new DataTypeModel()
            {
                Caption = op.ToString(),
                Type = op.DataTypeClass.IdDataTypeClass,
                TypeName = op.DataTypeClass.Name,
                ReferenceTypeId = op.ReferenceType != null ? op.ReferenceType.IdReferenceType : (int?)null,
                IsEditable = op.IsEditable,
                Format = FormatModel.FromOpDataTypeFormat(op.DataTypeFormat != null ? op.DataTypeFormat : null),
                UnitId = op.Unit != null ? op.Unit.IdUnit : 0,
                UnitName = op.Unit != null ? op.Unit.Name : ""
            };
        }
    }
}
