﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class FieldNameValueModel
    {
        public long NameId { get; set; }
        public object Value { get; set; }
    }
}
