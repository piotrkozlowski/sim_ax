﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class PasswordResetEmailModel
    {
        public string User { get; set; }
        public string Locale { get; set; }
    }
}
