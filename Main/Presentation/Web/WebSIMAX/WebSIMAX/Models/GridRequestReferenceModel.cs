﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class GridRequestReferenceModel : GridRequestModel
    {
        public long IdReferenceType { get; set; }
    }
}
