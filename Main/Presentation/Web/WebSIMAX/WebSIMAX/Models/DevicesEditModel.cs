﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DevicesEditModel
    {
        public long Id { get; set; }
        public List<DeviceHierarchyModel> Devices { get; set; }
    }
}
