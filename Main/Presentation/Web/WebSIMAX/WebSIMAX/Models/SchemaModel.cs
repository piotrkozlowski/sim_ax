﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SchemaModel
    {
        public byte[] Schema { get; set; }
        public List<SchemaElement> SchemaConfiguration { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }

    [TsClass]
    public class SchemaElement
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public long IdDataType { get; set; }
        public bool IsUnitVisible { get; set; }
        public object Value { get; set; }
        public int Type { get; set; }
        public bool IsEditable { get; set; }
        public string Description { get; set; }

        public List<SchemaElementApperance> ElementsApperance { get; set; }
    }

    [TsClass]
    public class SchemaElementApperance
    {
        public string Color { get; set; }
        public byte[] Image { get; set; }

        public string Condition { get; set; }

        public object Constraint1 { get; set; }
        public object Constraint2 { get; set; }
    }
}
