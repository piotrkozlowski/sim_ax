﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DeviceModel : EntityModel
    {
        public List<GroupModel> Locations { get; set; } = new List<GroupModel>();
        public List<EntityModel> Meters { get; set; } = new List<EntityModel>();
        public long? CurrentMeterId { get; set; }
        public string CurrentMeterName { get; set; }
    }
}
