﻿using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class TaskStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public TaskStatusModel(OpTaskStatus opStatus)
        {
            Id = opStatus.IdTaskStatus;
            Name = opStatus.ToString();
        }
    }
}
