﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ResponseDeleteModel
    {
        public bool Result { get; set; }
        public int HasErrorCode { get; set; } = 0;

        public ResponseDeleteModel()
        { }

        public ResponseDeleteModel(bool res, SESSION session = null)
        {
            Result = res;
            HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
        }
    }
}
