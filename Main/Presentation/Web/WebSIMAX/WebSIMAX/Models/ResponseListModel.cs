﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ResponseListModel<T>
    {
        public List<T> Result { get; set; } = new List<T>();
        public int HasErrorCode { get; set; } = 0;

        public ResponseListModel()
        { }

        public ResponseListModel(List<T> list, SESSION session = null)
        {
            Result = list;
            HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
        }
    }
}
