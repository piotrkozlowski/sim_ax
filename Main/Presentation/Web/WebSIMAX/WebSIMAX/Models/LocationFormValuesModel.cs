﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class LocationFormValuesModel : FormValuesModel
    {
        public List<long> Groups { get; set; }
        public List<long> Devices { get; set; }
        public List<long> Meters { get; set; }
    }
}
