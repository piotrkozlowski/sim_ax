﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DeviceConnectionModel : EntityModel
    {
        public long SerialNbrParent { get; set; }
        public long SerialNbr { get; set; }
        public double? ParentLat { get; set; }
        public double? ParentLon { get; set; }
        public double? DeviceLat { get; set; }
        public double? DeviceLon { get; set; }
        public string SerialNbrCol { get; set; }
        public string SerialNbrParentCol { get; set; }
    }
}
