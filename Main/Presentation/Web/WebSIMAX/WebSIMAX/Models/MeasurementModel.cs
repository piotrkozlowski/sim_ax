﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.DW;
using SIMAX.Utils;
using System;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class MeasurementModel
    {
        public object Value { get; set; }
        public object Date { get; set; }
        public long DataType { get; set; }
        public long? SerialNumber { get; set; }
        public long? MeterId { get; set; }
        public long? LocationId { get; set; }
        public bool IsDate { get; set; }

        public static MeasurementModel MeasurementModelFromOpDataArch(Measure op)
        {
            var date = op.Time.Truncate(TimeSpan.FromMinutes(1));
            var isDate = op.Value.GetType() == typeof(DateTime);

            return new MeasurementModel()
            {
                Date = date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                Value = isDate ? ((DateTime) op.Value).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds : op.Value,
                DataType = op.IdDataType,
                SerialNumber = op.SerialNbr,
                MeterId = op.IdMeter,
                LocationId = op.IdLocation,
                IsDate = isDate
            };
        }

        public static MeasurementModel MeasurementModelFromOpData(OpData op)
        {
            var date = op.Time.Truncate(TimeSpan.FromMinutes(1));

            return new MeasurementModel()
            {
                Date = date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                Value = op.Value,
                DataType = op.IdDataType,
                SerialNumber = op.SerialNbr,
                MeterId = op.IdMeter,
                LocationId = op.IdLocation
            };
        }
    }
}
