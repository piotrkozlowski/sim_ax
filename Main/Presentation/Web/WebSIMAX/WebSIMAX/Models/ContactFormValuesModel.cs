﻿using System;
using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ContactFormValuesModel : FormValuesModel
    {
        public List<long> locations { get; set; } = new List<long>();
        public List<long> Groups { get; set; } = new List<long>();
    }
}
