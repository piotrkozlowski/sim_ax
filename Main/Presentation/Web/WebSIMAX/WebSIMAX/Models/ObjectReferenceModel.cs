﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ObjectReferenceModel
    {
        public long Id { get; set; }
        public long ReferenceObjectId { get; set; }
    }
}
