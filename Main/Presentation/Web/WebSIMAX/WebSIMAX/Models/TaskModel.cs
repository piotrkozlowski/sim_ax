﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class TaskModel: EntityModel
    {
        public long? IdLocation { get; set; }
        public string TaskString { get; set; }
    }
}
