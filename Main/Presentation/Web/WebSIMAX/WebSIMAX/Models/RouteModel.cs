﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class RouteModel : EntityModel
    {      
        public List<EntityModel> Locations { get; set; } = new List<EntityModel>();
        public List<EntityModel> Operators { get; set; } = new List<EntityModel>();
    }
}
