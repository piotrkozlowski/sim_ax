﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SecurityChangeModel
    {
        public int OperatorId { get; set; }
        public List<int> Added { get; set; } = new List<int>();
        public List<int> Removed { get; set; } = new List<int>();
    }
}
