﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class PasswordResetModel
    {
        public string Guid { get; set; }
        public string Password { get; set; }
        public string Locale { get; set; }

        public string OldPassword { get; set; }
    }
}
