﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class TaskStatusChangeModel
    {
        public int StatusId { get; set; }
        public List<int> TaskIds { get; set; }
    }
}
