﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class UserModel
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string Avatar { get; set; }
        public int Id { get; set; }
        public int IdDistributor { get; set; }
        public List<string> Permissions { get; set; } = new List<string>();
    }
}
