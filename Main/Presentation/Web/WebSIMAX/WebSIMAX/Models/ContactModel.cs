﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ContactModel : EntityModel
    {
        public List<EntityModel> Locations { get; set; } = new List<EntityModel>();
        public List<GroupModel> Groups { get; set; } = new List<GroupModel>();
    }
}
