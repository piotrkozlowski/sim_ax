﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class PointModel
    {
        public long IdLocation { get; set; }
        public int IdTask { get; set; }
    }
}
