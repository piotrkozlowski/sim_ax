﻿using IMR.Suite.UI.Business.Objects.CORE;

namespace SIMAX.Models
{
    public class FormatModel
    {
        public string DateTimeFormat { get; set; }
        public bool IsRequired { get; set; }
        public long? TextMinLength { get; set; }
        public long? TextMaxLength { get; set; }
        public double? NumberMinValue { get; set; }
        public double? NumberMaxValue { get; set; }
        public long? NumberMinPrecision { get; set; }
        public long? NumberMaxPrecision { get; set; }
        public long? NumberMinScale { get; set; }
        public long? NumberMaxScale { get; set; }

        static public FormatModel FromOpDataTypeFormat(OpDataTypeFormat op)
        {
            if (op == null) return new FormatModel();
            return new FormatModel()
            {
                DateTimeFormat = op.DatetimeFormat,
                IsRequired = op.IsRequired.HasValue ? op.IsRequired.Value : false,
                TextMinLength = op.TextMinLength,
                TextMaxLength = op.TextMaxLength,
                NumberMinValue = op.NumberMinValue,
                NumberMaxValue = op.NumberMaxValue,
                NumberMinPrecision = op.NumberMinPrecision,
                NumberMaxPrecision = op.NumberMaxPrecision,
                NumberMinScale = op.NumberMinScale,
                NumberMaxScale = op.NumberMaxScale
            };
        }
    }
}
