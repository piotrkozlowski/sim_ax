﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DictionaryModel
    {
        public List<ColumnModel> Modules { get; set; } = new List<ColumnModel>();
        public List<ColumnModel> Distributors { get; set; } = new List<ColumnModel>();
        public List<ColumnModel> Units { get; set; } = new List<ColumnModel>();

        public object DataTypes { get; set; }
        public List<DataTypeGroupModel> DataTypeGroups { get; set; } = new List<DataTypeGroupModel>();
        public object DataTypeInGroups { get; set; }

        public object Views { get; set; }

        public Dictionary<int, List<ColumnModel>> References { get; set; } = new Dictionary<int, List<ColumnModel>>();
    }
}
