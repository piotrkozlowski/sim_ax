﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ModelAlarm: EntityModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
