﻿using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DataTypeGroupModel
    {
        public string Name { get; set; }
        public int IdDataTypeGroup { get; set; }
        public int? IdReferenceType { get; set; }
        public object ReferenceValue { get; set; }
        public int? IdParentGroup { get; set; }

        public static DataTypeGroupModel DataTypeGroupModelFromOpDataTypeGroup(OpDataTypeGroup op)
        {
            return new DataTypeGroupModel()
            {
                Name = op.Name,
                IdDataTypeGroup = op.IdDataTypeGroup,
                IdReferenceType = op.IdReferenceType,
                ReferenceValue = op.ReferenceValue,
                IdParentGroup = op.IdParentGroup
            };
        }
    }
}
