﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ViewColumnModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
