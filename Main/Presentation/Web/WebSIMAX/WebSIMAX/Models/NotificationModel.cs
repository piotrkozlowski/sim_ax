﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class NotificationModel
    {
        public int OperatorId { get; set; }
        public string Type { get; set; }
        public List<NotificationEntity> Set { get; set; }
        public List<NotificationEntity> Reset { get; set; }

        public List<int> DistributorIds { get; set; }
        public int ReferenceObjectId { get; set; }
        public string ReferenceType { get; set; }
    }

    [TsClass]
    public class NotificationEntity
    {
        public int Id { get; set; }
        public bool IsEmailValue { get; set; }
    }
}
