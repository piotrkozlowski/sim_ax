﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class OperatorModel : EntityModel
    {
        public List<FieldModel> ActorFields { get; set; } = new List<FieldModel>();
        public GridModel Roles { get; set; } = new GridModel();
        public GridModel Activities { get; set; } = new GridModel();
        public GridModel AllActivities { get; set; } = new GridModel();

        public GridModel NotificationIssues { get; set; } = new GridModel();
        public GridModel NotificationTasks { get; set; } = new GridModel();
        public GridModel NotificationWorkOrders { get; set; } = new GridModel();
        public GridModel NotificationLocations { get; set; } = new GridModel();
    }
}
