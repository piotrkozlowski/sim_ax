﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class EntityModel
    {
        public long Id { get; set; }
        public long ReferenceId { get; set; }
        public string Name { get; set; }
        public List<FieldModel> Fields { get; set; } = new List<FieldModel>();
        public List<FieldModel> DetailsFields { get; set; } = new List<FieldModel>();
        public int HasErrorCode { get; set; } = 0;
    }
}
