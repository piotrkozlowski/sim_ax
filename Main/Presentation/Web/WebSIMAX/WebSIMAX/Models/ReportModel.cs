﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ReportModel : EntityModel { }

    [TsClass]
    public class GeneratedReportModel
    {
        public int id { get; set; }
        public string resultString { get; set; }
        public int reportType { get; set; }
    }
}
