﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class HistoryIssueModel
    {
        public int Id { get; set; }
        public int IdIssue { get; set; }
        public int IdIssueStatus { get; set; }
        public string IssueStatusDesc { get; set; }
        public int IdOperator { get; set; }
        public string OperatorDesc { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
        public string ShortDescr { get; set; }
        public DateTime? Deadline { get; set; }
        public int? IdPriority { get; set; }
        public string PriorityDesc { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
