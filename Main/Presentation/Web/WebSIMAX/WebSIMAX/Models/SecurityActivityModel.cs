﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SecurityActivityModel: ObjectReferenceModel
    {
        public long OperatorId { get; set; }
    }
}
