﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class RouteFormValuesModel : FormValuesModel
    {
        public List<int> RemovedTasks { get; set; }
        public List<PointModel> Locations { get; set; }
        public List<long> RemovedOperators { get; set; }
        public List<long> Operators { get; set; }
    }
}
