﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SimpleValueModel<T>
    {
        public SimpleValueModel(T value) {
            Value = value;
        }
        public T Value { get; set; }
    }
}
