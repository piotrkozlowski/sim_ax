﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ImportStatusModel
    {
        public string Guid { get; set; }
        public List<SingleStatusModel> Statuses { get; set; } = new List<SingleStatusModel>();
        public int HasErrorCode { get; set; } = 0;
    }

    [TsClass]
    public class SingleStatusModel
    {
        public object Key { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }

        public SingleStatusModel(OpImportItemSummary summary)
        {
            Key = summary.Key;
            Description = summary.Description;
            Status = summary.ImportStatus.ToString("g");
            StatusId = (int)summary.ImportStatus;
        }
    }
}
