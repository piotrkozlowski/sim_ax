﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class HistoryTaskModel
    {
        public int Id { get; set; }
        public int IdTask { get; set; }
        public int IdTaskStatus { get; set; }
        public string TaskStatusDesc { get; set; }
        public int IdOperator { get; set; }
        public string OperatorDesc { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
