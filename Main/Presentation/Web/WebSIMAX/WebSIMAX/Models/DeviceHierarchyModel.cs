﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DeviceHierarchyModel
    {
        public long Id { get; set; }
        public string Device { get; set; }
        public long? DeviceId { get; set; }
        public int SlotNbr { get; set; }
        public string SlotType { get; set; }
        public string ProtocolIn { get; set; }
        public string ProtocolOut { get; set; }
        public bool Active { get; set; }
        public long ReferenceId { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
