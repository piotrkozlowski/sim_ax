﻿using System;
using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class MeasurementsRequestModel
    {
        public long[] LocationIds { get; set; }
        public long[] DeviceIds { get; set; }
        public long[] MeterIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public List<ColumnModel> Columns { get; set; }
    }
}
