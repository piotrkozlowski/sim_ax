﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIMAX.Models
{
    public class SavedRequestModel
    {
        public GridRequestModel GridRequestModel { get; set; }
        public string Token { get; set; }
    }
}
