﻿using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects.Custom;
using TypeLite;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

namespace SIMAX.Models
{
    [TsClass]
    public class FieldModel
    {
        public long Id { get; set; }
        public long Index { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public bool IsEditable { get; set; }
        public FormatModel Format { get; set; }
        public object Value { get; set; }
        public object ValueReferenceId { get; set; }
        public string unitName { get; set; }

        public static FieldModel FieldFromOpDataList(IMR.Suite.UI.Business.Objects.CORE.OpData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromDynamicValues(KeyValuePair<string, object> data)
        {
             return GetFieldModelFromDynamicValues(data);
        }

        public static FieldModel FieldFromOpDataList(OpOperatorData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpActorData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpDistributorData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpIssueData data)
        {
            return GetFieldModel(data);
        }
        
        public static FieldModel FieldFromOpDataList(IMR.Suite.UI.Business.Objects.Custom.OpAlarmData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpTaskData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpRouteData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpSimCardData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpActionData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromOpDataList(OpReportData data)
        {
            return GetFieldModel(data);
        }

        public static FieldModel FieldFromIdAndValue(long id, object value)
        {
            return new FieldModel()
            {
                Id = id,
                Value = value
            };
        }

        private static FieldModel GetFieldModel(dynamic data)
        {
            return new FieldModel()
            {
                Id = data.IdDataType,
                Index = data.Index != null ? data.Index : "",
                Name = data.IdDataType.ToString(),
                Caption = data.DataType != null ? data.DataType.ToString() : "",
                Type = data.DataType != null ? data.DataType.DataTypeClass.Name : "",
                TypeId = data.DataType != null ? data.DataType.DataTypeClass.IdDataTypeClass : 0,
                Format = FormatModel.FromOpDataTypeFormat(data.DataType != null ? data.DataType.DataTypeFormat : null),
                IsEditable = data.DataType != null ? data.DataType.IsEditable : true,
                Value = data.Value
            };
        }

        private static FieldModel GetFieldModelFromDynamicValues(dynamic data)
        {

            var fieldModel = new FieldModel()
            {
                Id = OpViewColumn.GetColumnIdDataType(data.Key),
                Name = ((string)data.Key.ToString()).ToLower(),
                Value = data.Value
            };
            return fieldModel;
        }
    }
}
