﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class SaveFileModel
    { 
        public string Status { get; set; }
        public FileModel FileModel { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
