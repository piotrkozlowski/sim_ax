﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ImportProgressModel
    {
        public string Status { get; set; }
        public int StatusId { get; set; }
        public long ItemsCount { get; set; }
        public long ProceedItemsCount { get; set; }
        public long SuccessCount { get; set; }
        public long WarningCount { get; set; }
        public long ErrorCount { get; set; }
        public int HasErrorCode { get; set; } = 0;

        public ImportProgressModel(OpImportProgressStep opProgress, SESSION session = null)
        {
            Status = opProgress.ImportStatus.ToString("g");
            StatusId = (int)opProgress.ImportStatus;
            ItemsCount = opProgress.ItemsCount;
            ProceedItemsCount = opProgress.ProceedItemsCount;
            SuccessCount = opProgress.ProceedSuccessItemsCount;
            WarningCount = opProgress.ProceedWarningItemsCount;
            ErrorCount = opProgress.ProceedErrorItemsCount;
            HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
        }
    }
}
