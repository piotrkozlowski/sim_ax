﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class LocationEquipmentModel
    {
        public string Device { get; set; }
        public string Meter { get; set; }
        //public long Id { get; set; }
        //public long? IdLocation { get; set; }
        //public long? IdMeter { get; set; }
        //public long? SerialNbr { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
