﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class GroupModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Distributor { get; set; }
        public long ReferenceId { get; set; }
        public bool Builtin { get; set; }
        public int HasErrorCode { get; set; } = 0;
    }
}
