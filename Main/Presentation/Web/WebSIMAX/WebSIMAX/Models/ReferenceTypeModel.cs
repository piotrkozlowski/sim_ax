﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ReferenceTypeModel
    {
        public int ReferenceTypeId { get; set; }
        public int TableName { get; set; }
    }
}
