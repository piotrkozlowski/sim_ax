﻿namespace SIMAX.Models
{
    public enum DashboardItemTypes : long
    {
        TILE,
        GAUGE,
        TABLE,
        CHART,
        MAP,
        SCHEMA
    }
}
