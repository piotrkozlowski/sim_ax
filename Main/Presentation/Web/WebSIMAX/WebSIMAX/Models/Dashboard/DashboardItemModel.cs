﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DashboardItemModel
    {
        public long Id { get; set; }
        public long Type { get; set; }
        public long Size { get; set; }
        public object Data { get; set; }
        public string Title { get; set; }
    }
}
