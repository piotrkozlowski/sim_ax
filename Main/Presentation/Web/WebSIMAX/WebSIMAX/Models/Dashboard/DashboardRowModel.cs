﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DashboardRowModel
    {
        public List<DashboardColumnModel> Columns { get; set; } = new List<DashboardColumnModel>();
    }
}
