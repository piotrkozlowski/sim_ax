﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DashboardModel: EntityModel
    {
        public List<DashboardRowModel> Rows { get; set; } = new List<DashboardRowModel>();
        public bool FitToScreen { get; set; }
        public bool UseMargin { get; set; }
        public bool UsePadding { get; set; }
        public List<int> Padding { get; set; }//top, right, bottom, left
        public List<int> Margin { get; set; }//top, right, bottom, left
        public bool KeepAspectRatio { get; set; }//Zachowaj proporcje obrazu
        public int? Width { get; set; }
        public int? MaxWidth { get; set; }
        public int? MaxHeight { get; set; }
    }
}
