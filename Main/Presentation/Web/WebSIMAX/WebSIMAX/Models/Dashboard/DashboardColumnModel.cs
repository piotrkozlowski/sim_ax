﻿using System.Collections.Generic;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DashboardColumnModel
    {
        public long Size { get; set; } = 12;
        public string Title { get; set; } = "";
        public List<DashboardItemModel> Items { get; set; } = new List<DashboardItemModel>();
        public int GroupId { get; set; }
        public int RefreshPeriod { get; set; }
        public bool BorderVisible { get; set; }
    }
}
