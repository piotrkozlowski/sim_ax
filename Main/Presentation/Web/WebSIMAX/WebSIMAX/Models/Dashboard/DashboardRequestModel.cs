﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class DashboardRequestModel
    {
        public List<long> Ids { get; set; } = new List<long>();
        public int GroupId { get; set; }
    }
}
