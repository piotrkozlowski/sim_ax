﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class ImportDataModel
    {
        public string[] Headers { get; set; }
        public object[][] Data { get; set; }
    }
}
