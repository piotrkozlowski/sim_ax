﻿using TypeLite;

namespace SIMAX.Models
{
    [TsClass]
    public class LogoModel
    {
        public int Id { get; set; }
        public string jwt { get; set; }
    }
}
