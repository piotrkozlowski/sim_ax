﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SIMAX.Services;
using Microsoft.AspNetCore.Http;
using SIMAX.Utils;
using System.Linq;
using SIMAX.Utils.Auth;
using System.ServiceModel;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Formatters;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using System.Globalization;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using NLog.Extensions.Logging;
using System.IO;
using IMR.Suite.Common;

namespace SIMAX
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => certificate.Subject.Contains("IMR-Web"));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ISessionService, SessionService>();
            services.AddTransient<ILocationsService, LocationsService>();
            services.AddTransient<IDevicesService, DevicesService>();
            services.AddTransient<IIssuesService, IssuesService>();
            services.AddTransient<ITasksService, TasksService>();
            services.AddTransient<IAlarmsService, AlarmsService>();
            services.AddTransient<IDictionaryService, DictionaryService>();
            services.AddTransient<IGroupsService, GroupsService>();
            services.AddTransient<IDashboardService, DashboardService>();
            services.AddTransient<IMeasurementsService, MeasurementsService>();
            services.AddTransient<IMetersService, MetersService>();
            services.AddTransient<IHistoryService, HistoryService>();
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IDistributorsService, DistributorsService>();
            services.AddTransient<IAppService, AppService>();
            services.AddTransient<IRoutesService, RoutesService>();
            services.AddTransient<IPasswordService, PasswordService>();
            services.AddTransient<IReportsService, ReportsService>();
            services.AddTransient<ISimcardsService, SimcardsService>();
            services.AddTransient<IContactsService, ContactsService>();
            services.AddTransient<IActionsService, ActionsService>();
            services.AddTransient<IDeviceConnectionsService, DeviceConnectionsService>();
            services.AddTransient<ISecureAppService, SecureAppService>();
            services.AddTransient<IDBCollector, IDBCollector>(factory =>
            {
                var endpointUrl = Configuration["WebServiceURL"];
                var endpoint = new EndpointAddress(endpointUrl);

                var WSBindingType = Configuration["WSBindingType"];
                if (String.IsNullOrEmpty(WSBindingType)) WSBindingType = "NetTcpBinding";

                Binding binding = null;

                if (WSBindingType.Equals("NetTcpBinding"))
                {
                    binding = new NetTcpBinding(SecurityMode.None) { MaxReceivedMessageSize = int.MaxValue, MaxBufferPoolSize = int.MaxValue };
                }
                else if (WSBindingType.Equals("WSHttpBinding_Transport"))
                {
                    var wsHttpBinding = new WSHttpBinding(SecurityMode.Transport) { MaxReceivedMessageSize = int.MaxValue, MaxBufferPoolSize = int.MaxValue };
                    wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                    wsHttpBinding.ReaderQuotas.MaxDepth = 32;
                    wsHttpBinding.ReaderQuotas.MaxStringContentLength = int.MaxValue;
                    wsHttpBinding.ReaderQuotas.MaxArrayLength = int.MaxValue;
                    wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 4096;
                    wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
                    binding = wsHttpBinding;
                }
                else if (WSBindingType.Equals("WSHttpBinding"))
                {
                    var wsHttpBinding = new WSHttpBinding(SecurityMode.None) { MaxReceivedMessageSize = int.MaxValue, MaxBufferPoolSize = int.MaxValue };
                    wsHttpBinding.ReaderQuotas.MaxDepth = 32;
                    wsHttpBinding.ReaderQuotas.MaxStringContentLength = int.MaxValue;
                    wsHttpBinding.ReaderQuotas.MaxArrayLength = int.MaxValue;
                    wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 4096;
                    wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
                    binding = wsHttpBinding;
                }

                binding.CloseTimeout = binding.OpenTimeout = binding.ReceiveTimeout = binding.SendTimeout = new TimeSpan(2, 0, 0);
  
                var channelFactory = new ChannelFactory<IDBCollector>(binding, endpoint);
                channelFactory.Endpoint.EndpointBehaviors.Add(new SIMAXBehavior());
                var clientProxy = channelFactory.CreateChannel();
                return clientProxy;
            });
            
            // Add framework services.
            services.AddMvc(options =>
            {
                options.Filters.Add(new StatusCodeExceptionAttribute());
                options.Filters.Add(new WebServiceExceptionAttribute());
                options.Filters.Add(new Logging());

                var formatter = options.OutputFormatters.Single(f => f.GetType() == typeof(JsonOutputFormatter)) as JsonOutputFormatter;
                formatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.OutputFormatters.Add(formatter);
            });
            services.AddMemoryCache();
            services.AddSession(options =>
            {
                options.CookieName = ".Simax.Session";
                options.IdleTimeout = TimeSpan.FromDays(10);
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                    builder.AllowCredentials();
                });
            });
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("pl-PL")
                };

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // for NLOG
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
            {
                env.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            }

            loggerFactory.AddNLog();
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            env.ConfigureNLog("NLog.config");

            app.UseMiddleware<AuthenticateJWTMiddleware>(new AuthenticateJWTOptions());
            app.UseCors("AllowAllOrigins");
            app.UseSession();
            app.UseDeveloperExceptionPage();
            app.UseDefaultFiles();
            app.UseMiddleware<CacheControlMiddleware>();
            app.UseMvc();
            app.UseStaticFiles();
        }
    }

    public class SIMAXBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new Logging());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }
}
