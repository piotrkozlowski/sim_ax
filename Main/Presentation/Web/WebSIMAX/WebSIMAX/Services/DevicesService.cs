﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace SIMAX.Services
{
    public interface IDevicesService
    {
        GridModel GetDevices(GridRequestModel req);
        DeviceModel SaveDevice(FormValuesModel req);
        DeviceModel GetDevice(long id);
        EntityModel GetDeviceToEdit(long id);
        ResponseDeleteModel DeleteDevice(long id);
        ResponseListModel<DeviceHierarchyModel> GetParentDevices(long id);
        ResponseListModel<DeviceHierarchyModel> GetChildDevices(long id);
        ResponseListModel<DeviceHierarchyModel> SaveParentDevices(DevicesEditModel editModel);
        ResponseListModel<DeviceHierarchyModel> SaveChildDevices(DevicesEditModel editModel);
        DeviceModel SaveDeviceCurrentMeter(ObjectReferenceModel model);
        ResponseListModel<SchemaModel> GetSchemas(long id);
    }

    public class DevicesService : BaseService, IDevicesService
    {
        public DevicesService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetDevices(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);

                FilterByIds(req, ref columns, Enums.ReferenceType.SerialNbr);

                SetStartTime();
                OpDevice[] opDevices = _dbClient.OpxDeviceFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.DEVICES.FilterDeviceLog, "OpxDeviceFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opDevices.Select(device =>
                    {
                        return Common.DictionaryToObject(device.DynamicValues, device.ToString(), device.SerialNbr);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning devices downloading.", e);
            }
        }

        private delegate OpDeviceHierarchy[] GetDevicesFunction(ref SESSION session, long id);
        private delegate void SaveDevicesFunction(ref SESSION session, OpDeviceHierarchy[] devices);

        public ResponseListModel<DeviceHierarchyModel> GetParentDevices(long id)
        {
            SetStartTime();
            var parentDevicesHierarchy = GetChildOrParentDevices(id, _dbClient.OpxGetParentDevices);
            SetEndTime(Logging.EventID.DEVICES.GetParentDevicesLog, "OpxGetParentDevices");
            return parentDevicesHierarchy;
        }

        public ResponseListModel<DeviceHierarchyModel> GetChildDevices(long id)
        {
            SetStartTime();
            var childDevicesHierarchy = GetChildOrParentDevices(id, _dbClient.OpxGetChildDevices);
            SetEndTime(Logging.EventID.DEVICES.GetChildDevicesLog, "OpxGetChildDevices");
            return childDevicesHierarchy;
        }

        private ResponseListModel<DeviceHierarchyModel> GetChildOrParentDevices(long id, GetDevicesFunction getDevices)
        {
            try
            {
                var session = _sessionService.GetSession();
                var devices = getDevices(ref session, id);
                base.CheckSession(session);

                var result = new List<DeviceHierarchyModel>();

                result.AddRange(devices.Select(dh =>
                {
                    return new DeviceHierarchyModel()
                    {
                        Id = dh.IdDeviceHierarchy,
                        Device = dh.Device != null ? dh.Device.ToString() : string.Empty,
                        DeviceId = dh.Device != null ? dh.Device.SerialNbr : 0,
                        ProtocolIn = dh.ProtocolIn != null ? dh.ProtocolIn.ToString() : string.Empty,
                        ProtocolOut = dh.ProtocolOut != null ? dh.ProtocolOut.ToString() : string.Empty,
                        SlotNbr = dh.SlotNbr,
                        SlotType = dh.SlotType != null ? dh.SlotType.ToString() : string.Empty,
                        Active = dh.IsActive,
                        ReferenceId = dh.Device != null ? dh.Device.IdDeviceType : 0
                    };
                }).ToList());
                return new ResponseListModel<DeviceHierarchyModel>(result, session);
            }
            catch (CommunicationException e)
            {
                throw new StatusCodeException((int)HttpStatusCode.ServiceUnavailable);
            }
            catch (Exception e)
            {
                throw new Exception("Issue concering related devices downloading.", e);
            }
        }

        public ResponseListModel<DeviceHierarchyModel> SaveParentDevices(DevicesEditModel editModel)
        {
            SetStartTime();
            var parentDevicesHierarchy = SaveParentOrChildDevices(editModel, _dbClient.OpxGetParentDevices, null);
            SetEndTime(Logging.EventID.DEVICES.SaveParentDevicesLog, "OpxGetParentDevices");
            return parentDevicesHierarchy;
        }

        public ResponseListModel<DeviceHierarchyModel> SaveChildDevices(DevicesEditModel editModel)
        {
            SetStartTime();
            var childDevicesHierarchy = SaveParentOrChildDevices(editModel, _dbClient.OpxGetChildDevices, null);
            SetEndTime(Logging.EventID.DEVICES.SaveChildDevicesLog, "OpxGetChildDevices");
            return childDevicesHierarchy;
        }

        private ResponseListModel<DeviceHierarchyModel> SaveParentOrChildDevices(DevicesEditModel editModel, GetDevicesFunction getDevices, SaveDevicesFunction saveDevices)
        {
            var session = _sessionService.GetSession();
            var devices = getDevices(ref session, editModel.Id);
            base.CheckSession(session);

            foreach(var device in devices)
            {
                var currentDevice = editModel.Devices.Find(d => d.Id == device.IdDeviceHierarchy);

                if (currentDevice != null)
                {
                    var opDevice = new OpDevice();
                    if (currentDevice.DeviceId.HasValue)
                    {
                        SetStartTime();
                        opDevice = _dbClient.OpxGetDevice(ref session, currentDevice.DeviceId.Value);
                        SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                        base.CheckSession(session);
                    }

                    if (!currentDevice.DeviceId.HasValue && device.Device != null)
                        device.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete;
                    else if (currentDevice.DeviceId.HasValue && currentDevice.Device == null)
                    {
                        device.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                        device.Device = opDevice;
                        device.IsActive = currentDevice.Active;
                    }
                    else if (currentDevice.DeviceId.HasValue && currentDevice.Device != null && (currentDevice.DeviceId.Value != device.Device.SerialNbr || currentDevice.Active != device.IsActive))
                    {
                        device.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Modified;
                        device.Device = opDevice;
                        device.IsActive = currentDevice.Active;
                    }
                }
            }

            saveDevices(ref session, devices);
            base.CheckSession(session);

            var result = new List<DeviceHierarchyModel>();

            result.AddRange(devices.Select(d => DeviceHierarchyModelFromOpDeviceHierarchy(d)).ToList());

            return new ResponseListModel<DeviceHierarchyModel>(result, session);
        }

        public DeviceModel GetDevice(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opDevice = _dbClient.OpxGetDevice(ref session, id);
                SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                base.CheckSession(session);

                if (opDevice != null)
                {
                    return DeviceModelFromOpDevice(opDevice, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return DeviceModelFromOpDevice(opDevice, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning device: " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device: " + id + " downloading.", e);
            }
        }

        public DeviceModel SaveDevice(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpDevice opDevice;
                if (model.Id != null)
                {
                    SetStartTime();
                    opDevice = _dbClient.OpxGetDevice(ref session, Convert.ToInt32(model.Id));
                    SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                    base.CheckSession(session);
                }
                else
                    opDevice = new OpDevice();

                if(opDevice == null)
                    opDevice = new OpDevice();

                #region Fields
                FillDataList(opDevice.DataList, model.Fields);
                #endregion

                SetStartTime();
                var savedDevice = _dbClient.OpxSaveDevice(ref session, opDevice);
                SetEndTime(Logging.EventID.DEVICES.SaveDeviceLog, "OpxSaveDevice");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedDevice != null)
                {
                    return DeviceModelFromOpDevice(savedDevice, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return DeviceModelFromOpDevice(savedDevice, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning device: " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device: " + model.Id + " saving.", e);
            }
        }

        public EntityModel GetDeviceToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opDevice = _dbClient.OpxGetDevice(ref session, id);
                SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opDevice.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    ReferenceId = opDevice.IdDeviceType,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device " + id + " downloading.",e);
            }
        }

        public ResponseDeleteModel DeleteDevice(long id)
        {
            var session = _sessionService.GetSession();
            OpDevice opDevice = null;
            try
            {
                SetStartTime();
                opDevice = _dbClient.OpxGetDevice(ref session, id);
                SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                base.CheckSession(session);
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device " + id + " downloading.", e);
            }
            try
            { 
                opDevice.IdDeviceStateType = (int)OpDeviceStateType.Enum.ERASED;
                SetStartTime();
                var result = _dbClient.OpxSaveDevice(ref session, opDevice);
                SetEndTime(Logging.EventID.DEVICES.SaveDeviceLog, "OpxSaveDevice");

                if (result != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device " + id + " saving.", e);
            }
        }

        public DeviceModel SaveDeviceCurrentMeter(ObjectReferenceModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                var opDevice = new OpDevice();
                try
                {
                    SetStartTime();
                    opDevice = _dbClient.OpxGetDevice(ref session, model.Id);
                    SetEndTime(Logging.EventID.DEVICES.SaveDeviceLog, "OpxGetDevice");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning device " + model.Id + " downloading.", e);
                }
                base.CheckSession(session);

                var opMeter = new OpMeter();
                try
                {
                    SetStartTime();
                    opMeter = _dbClient.OpxGetMeter(ref session, model.ReferenceObjectId);
                    SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning meter " + model.ReferenceObjectId + " downloading.", e);
                }
                base.CheckSession(session);

                if (opDevice.CurrentMeters != null)
                {
                    if (opDevice.CurrentMeters.Count > 0)
                    {
                        var opMeterToChange = opDevice.CurrentMeters.FirstOrDefault(m => m.IdMeter == opMeter.IdMeter);
                        if (opMeterToChange != null)
                        {
                            opMeterToChange.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                        }
                        else
                        {
                            opMeter.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                            opDevice.CurrentMeters.Add(opMeter);
                        }

                        var opMetersToDelete = opDevice.CurrentMeters.Where(m => m.IdMeter != opMeter.IdMeter).ToList();

                        if (opMetersToDelete != null && opMetersToDelete.Count > 0)
                        {
                            foreach (OpMeter met in opMetersToDelete)
                            {
                                met.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete;
                            }
                        }
                    }
                    else
                    {
                        opMeter.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                        opDevice.CurrentMeters.Add(opMeter);
                    }
                }
                else
                {
                    opDevice.CurrentMeters = new List<OpMeter>();
                    opMeter.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                    opDevice.CurrentMeters.Add(opMeter);
                }
                SetStartTime();
                opDevice = _dbClient.OpxSaveDevice(ref session, opDevice);
                SetEndTime(Logging.EventID.DEVICES.SaveDeviceLog, "OpxSaveDevice");

                if (opDevice != null)
                {
                    return DeviceModelFromOpDevice(opDevice, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return DeviceModelFromOpDevice(opDevice, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning device " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device " + model.Id + " saving.", e);
            }
        }

        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            var schemaLogData = Logging.EventID.DEVICES.GetDeviceSchemaLog;
            var entityLogData = Logging.EventID.DEVICES.GetDeviceLog;
            return base.GetSchemas(id, _dbClient.OpxGetDeviceSchema, _dbClient.OpxGetDevice, schemaLogData, entityLogData, "Device");
        }

        private DeviceModel DeviceModelFromOpDevice(OpDevice opDevice, bool replaceReferenceTypes = false, SESSION session = null)
        {
            DeviceModel result;

            if (opDevice != null)
            {
                var fields = opDevice.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();
                var details = opDevice.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();

                result = new DeviceModel()
                {
                    Id = opDevice.SerialNbr,
                    ReferenceId = opDevice.IdDeviceType,
                    Name = opDevice.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opDevice.DynamicValues) : fields,
                    DetailsFields = details,
                    Meters = opDevice.CurrentMeters.Select(meter => new EntityModel()
                    {
                        Id = meter.IdMeterType,
                        Name = meter.ToString(),
                        Fields = meter.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList()
                    }).ToList(),
                    Locations = opDevice.CurrentLocations.Select(l => new GroupModel()
                    {
                        Id = l.IdLocationType,
                        Name = l.ToString()
                    }).ToList(),
                    CurrentMeterId = opDevice.CurrentMeter != null ? (long?)opDevice.CurrentMeter.IdMeter : null,
                    CurrentMeterName = opDevice.CurrentMeter != null ? opDevice.CurrentMeter.ToString() : "",
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(result.Fields, opDevice.DataList);
            }
            else
            {
                result = new DeviceModel()
                {
                    Fields = new List<FieldModel>(),
                    DetailsFields = new List<FieldModel>(),
                    Meters = new List<EntityModel>(),
                    Locations = new List<GroupModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return result;
        }

        private DeviceHierarchyModel DeviceHierarchyModelFromOpDeviceHierarchy(OpDeviceHierarchy deviceHierarchy)
        {
            return new DeviceHierarchyModel()
            {
                Id = deviceHierarchy.IdDeviceHierarchy,
                Device = deviceHierarchy.Device != null ? deviceHierarchy.Device.ToString() : string.Empty,
                DeviceId = deviceHierarchy.Device != null ? deviceHierarchy.Device.SerialNbr : 0,
                ProtocolIn = deviceHierarchy.ProtocolIn != null ? deviceHierarchy.ProtocolIn.ToString() : string.Empty,
                ProtocolOut = deviceHierarchy.ProtocolOut != null ? deviceHierarchy.ProtocolOut.ToString() : string.Empty,
                SlotNbr = deviceHierarchy.SlotNbr,
                SlotType = deviceHierarchy.SlotType != null ? deviceHierarchy.SlotType.ToString() : string.Empty,
                Active = deviceHierarchy.IsActive
            };
        }
    }
}
