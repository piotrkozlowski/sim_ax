﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;

namespace SIMAX.Services
{
    public interface ISecureAppService
    {
        OperatorSettingsModel GetOperatorSettings(int idOperator);
        bool SaveOperatorSettings(OperatorSettingsModel request);
    }
    
    public class SecureAppService : BaseService, ISecureAppService
    {
        public SecureAppService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;            
        }

        public OperatorSettingsModel GetOperatorSettings(int idOperator)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                object[] opSettings = _dbClient.OpxGetOperatorSettings(ref session, idOperator, new long[] { DataType.OPERATOR_LOCAL_SETTINGS });
                SetEndTime(Logging.EventID.SECUREAPP.GetOperatorSettings);
                if (opSettings != null && opSettings.Length > 0)
                {
                    object[] objFiles = (object[])opSettings[0];
                    OpFile file = objFiles.Cast<OpFile>().FirstOrDefault(x => x.IdFile == objFiles.Cast<OpFile>().Max(m => m.IdFile));
                    string settings = ParseByteToSettings((byte[])file.FileBytes);
                    OperatorSettingsModel result = new OperatorSettingsModel()
                    {
                        IdOperator = idOperator,
                        Settings = settings
                    };
                    return result;
                }
                else
                {
                    return new OperatorSettingsModel();
                }
            }
            catch( Exception e)
            {
                throw e;
            }
        }

        public bool SaveOperatorSettings(OperatorSettingsModel request)
        {
            try
            {
                byte[] settings = ParseSettingsToByte(request.Settings);
                OpOperatorData odItem = new OpOperatorData()
                {
                    IdDataType = DataType.OPERATOR_LOCAL_SETTINGS,
                    Value = settings,
                    OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Modified
                };

                var session = _sessionService.GetSession();
                SetStartTime();
                _dbClient.OpxSaveOperatorSettings(ref session, request.IdOperator, new object[] { odItem });
                SetEndTime(Logging.EventID.SECUREAPP.SaveOperatorSettings);
                return true;               
            }
            catch(Exception e)
            {
                return false;
            }
        }

        private byte[] ParseSettingsToByte(string settings)
        { 
            XmlSerializer ser = new XmlSerializer(typeof(string));
            MemoryStream sr = new MemoryStream();
            ser.Serialize(sr, settings);
            byte[] bytes = sr.ToArray();

            using (MemoryStream outstream = new MemoryStream())
            {
                using (GZipStream zipstream = new GZipStream(outstream, CompressionMode.Compress))
                using (MemoryStream inputstream = new MemoryStream(bytes))
                    inputstream.CopyTo(zipstream);

                bytes = outstream.ToArray();
            }
            return bytes;
        }

        private string ParseByteToSettings(byte[] bytes)
        {
            byte[] settings = bytes;
                using (var instream = new MemoryStream(settings))
                using (var zipstream = new GZipStream(instream, CompressionMode.Decompress))
                using (var outstream = new MemoryStream())
                {
                    zipstream.CopyTo(outstream);
                    settings = outstream.ToArray();
                }

                XmlSerializer ser = new XmlSerializer(typeof(string));
                MemoryStream sr = new MemoryStream(settings);            
            var result = (string)ser.Deserialize(sr);
            return result;      
        }
    }
}
