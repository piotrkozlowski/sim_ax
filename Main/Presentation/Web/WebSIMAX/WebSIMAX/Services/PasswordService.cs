﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIMAX.Services
{
    public interface IPasswordService
    {
        string Reset(string user, string locale);
        PasswordResetResponseModel Change(string guid, string password, string locale);
    }

    public class PasswordService : TimeLogsService, IPasswordService
    {
        private IDBCollector _dbClient;

        public PasswordService(IDBCollector dbClient, ISessionService sessionService)
        {
            _dbClient = dbClient;
        }

        public string Reset(string user, string locale)
        {
            SESSION session = new SESSION() { Guid = "" };
            try
            {
                SetStartTime();
                _dbClient.OpxResetOperatorPassword(ref session, user, locale);
                SetEndTime(Logging.EventID.USERS.ResetOperatorPasswordLog, "OpxResetOperatorPassword");
                return session.Error != null ? session.Error.Msg : session.Status.ToString();
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator password reset.", e);
            }
        }

        public PasswordResetResponseModel Change(string guid, string password, string locale)
        {
            SESSION session = new SESSION() { Guid = "" };
            try
            {
                SetStartTime();
                _dbClient.OpxChangeOperatorPassword(ref session, null, guid, password, null, locale);
                SetEndTime(Logging.EventID.USERS.ChangeOperatorPasswordLog, "OpxChangeOperatorPassword");
                return new PasswordResetResponseModel()
                {
                    Status = session.Status.ToString(),
                    Message = session.Error.Msg
                };
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator password changing.", e);
            }
        }
    }
}
