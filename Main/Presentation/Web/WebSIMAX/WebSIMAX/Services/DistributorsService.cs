﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Utils;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.Net;
using static IMR.Suite.Common.Enums;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Services
{
    public interface IDistributorsService
    {
        GridModel GetDistributors(GridRequestModel req);
        DistributorModel GetDistributor(int id);
        EntityModel GetDistributorToEdit(int id);
        DistributorModel SaveDistributor(FormValuesModel model);
        ResponseDeleteModel DeleteDistributor(int id);
        GridModel GetAllDistributors(GridRequestModel req);
    }

    public class DistributorsService : BaseService, IDistributorsService
    {
        public DistributorsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService) { }

        public GridModel GetDistributors(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                var values = _dbClient.OpxDistributorFilter(ref session, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.DISTRIBUTORS.FilterDistributorLog, "OpxDistributorFilter");
                long totalCount = values.Length;

                base.CheckSession(session);
                var result = new GridModel()
                {
                    Values = values.Select(distributor =>
                    {
                        return Common.DictionaryToObject(distributor.DynamicValues, distributor.ToString(), distributor.IdDistributor);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributors downloading.", e);
            }
        }

        public DistributorModel GetDistributor(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opDistributor = _dbClient.OpxGetDistributor(ref session, id);
                SetEndTime(Logging.EventID.DISTRIBUTORS.GetDistributorLog, "OpxGetDistributor");
                base.CheckSession(session);

                if (opDistributor != null)
                {
                    return DistributorModelFromOpDistributor(opDistributor, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return DistributorModelFromOpDistributor(opDistributor, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning distributor " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributor " + id + " downloading.", e);
            }
        }

        public EntityModel GetDistributorToEdit(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opDistributor = _dbClient.OpxGetDistributor(ref session, id);
                SetEndTime(Logging.EventID.DISTRIBUTORS.GetDistributorLog, "OpxGetDistributor");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opDistributor.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddDistributorSpecialFieldsToFieldModel(opDistributor));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributor "+ id + " downloading.", e);
            }
        }

        public DistributorModel SaveDistributor(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpDistributor opDistributor;
                if (model.Id.HasValue)
                {
                    try
                    {
                        SetStartTime();
                        opDistributor = _dbClient.OpxGetDistributor(ref session, (int) model.Id.Value);
                        SetEndTime(Logging.EventID.DISTRIBUTORS.GetDistributorLog, "OpxGetDistributor");
                        base.CheckSession(session);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning distributor " + model.Id + " downloading.", e);
                    }
                }
                else
                    opDistributor = new OpDistributor();

                #region Fields
                FillDataList(opDistributor.DataList, model.Fields);
                #endregion

                SetStartTime();
                opDistributor = _dbClient.OpxSaveDistributor(ref session, opDistributor);
                SetEndTime(Logging.EventID.DISTRIBUTORS.SaveDistributorLog, "OpxSaveDistributor");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (opDistributor != null)
                {
                    return DistributorModelFromOpDistributor(opDistributor, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return DistributorModelFromOpDistributor(opDistributor, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning distributor " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributor " + model.Id + " saving.", e);
            }
        }

        public ResponseDeleteModel DeleteDistributor(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opDistributor = _dbClient.OpxGetDistributor(ref session, id);
                SetEndTime(Logging.EventID.DISTRIBUTORS.GetDistributorLog, "OpxGetDistributor");
                base.CheckSession(session);

                SetStartTime();
                _dbClient.OpxDeleteDistributor(ref session, opDistributor);
                SetEndTime(Logging.EventID.DISTRIBUTORS.DeleteDistributorLog, "OpxDeleteDistributor");
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    throw new StatusCodeException((int)HttpStatusCode.NotFound, session.Error.Msg);
                }

                return new ResponseDeleteModel(true, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributor "+ id + " downloading.", e);
            }
        }

        public GridModel GetAllDistributors(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var allDistributors = _dbClient.GetAllowedList(ref session, new ReferenceType[] { ReferenceType.IdDistributor }).FirstOrDefault();
                SetEndTime(Logging.EventID.COMMON.GetAllowedListLog, "GetAllowedList");

                base.CheckSession(session);

                SetStartTime();
                var distributors = _dbClient.GetDictionaries(ref session, new ReferenceType[] { ReferenceType.IdDistributor }).FirstOrDefault();
                SetEndTime(Logging.EventID.DICTIONARY.GetDictionariesLog, "GetDictionaries");
                base.CheckSession(session);

                var totalCount = (allDistributors as object[]).Count();

                var result = new GridModel()
                {
                    Values = (allDistributors as object[]).Select(el =>
                    {
                        var distributor = el as OpDistributor;
                        var dictionary = new Dictionary<string, object>();

                        dictionary.Add("DistributorId", distributor.IdDistributor);
                        dictionary.Add("Name", distributor.ToString());
                        dictionary.Add("IsEnabled", (distributors as object[]).ToList().Find(d => (d as OpDistributor).IdDistributor == distributor.IdDistributor) != null ? true : false);

                        return Common.DictionaryToObject(dictionary);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning distributors downloading.", e);
            }
        }

        private DistributorModel DistributorModelFromOpDistributor(OpDistributor opDistributor, bool replaceReferenceTypes = false, SESSION session = null)
        {
            DistributorModel result;

            if (opDistributor != null)
            {
                var fields = opDistributor.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                result = new DistributorModel()
                {
                    Id = opDistributor.IdDistributor,
                    Name = opDistributor.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opDistributor.DynamicValues) : fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(result.Fields, opDistributor.DataList);
            }
            else
            {
                result = new DistributorModel()
                {
                    Fields = new List<FieldModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return result;
        }

        private List<FieldModel> AddDistributorSpecialFieldsToFieldModel(OpDistributor distributor)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, distributor.IdDistributor, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_NAME, distributor.Name, "String", true));
            list.Add(PrepareFieldModel(DataType.HELPER_CITY, distributor.City, "String", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ADDRESS, distributor.Address, "String", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DESCR, distributor.IdDescr, "Integer", true));

            return list;
        }
    }
}
