﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SIMAX.Utils.Helpers;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using System.Collections.Generic;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using SIMAX.Utils;

namespace SIMAX.Services
{
    public interface ISessionService
    {
        SESSION InitSession(string user, string password);
        SESSION Login(SESSION session, string user, string password, string locale);
        SESSION_STATUS Logout();
        SESSION GetSession(string jwt = null);
        int GetDistributorId(string jwt = null);
        int GetOperatorId(string jwt = null);
        OpOperator GetLoggedUser(SESSION session);
    }

    public class SessionService : TimeLogsService, ISessionService
    {
        const string SESSION_KEY = "WEBSERVICE_SESSION_KEY";
        private IDBCollector _dbClient;
        private readonly IHttpContextAccessor _contextAccessor;

        public SessionService(IHttpContextAccessor contextAccessor, IDBCollector dbClient)
        {
            _contextAccessor = contextAccessor;
            _dbClient = dbClient;
        }

        public SESSION GetSession(string jwt = null)
        {
            if (jwt == null) jwt = _contextAccessor.HttpContext.Request.Headers["JWT"].First();
            var tokenData = JWTHelper.DecodeToken(jwt);
            return JsonConvert.DeserializeObject<SESSION>((string)tokenData["session"]);
        }

        public int GetDistributorId(string jwt = null)
        {
            if (jwt == null) jwt = _contextAccessor.HttpContext.Request.Headers["JWT"].First();
            var tokenData = JWTHelper.DecodeToken(jwt);
            return (int) tokenData["distributorId"];
        }

        public int GetOperatorId(string jwt = null)
        {
            if (jwt == null) jwt = _contextAccessor.HttpContext.Request.Headers["JWT"].First();
            var tokenData = JWTHelper.DecodeToken(jwt);
            return (int)tokenData["operatorId"];
        }

        public SESSION InitSession(string user, string password)
        {
            try
            {
                SetStartTime();
                var result = _dbClient.InitSession(user, IMR.Suite.Common.RSA.Encrypt(DataConverter.EncodePassword(password)));
                SetEndTime(Logging.EventID.SESSION.InitSessionLog, "InitSession");

                SetStartTime();
                _dbClient.InitDBCollector(null, null, null, Enums.Module.WebSIMAX);
                SetEndTime(Logging.EventID.SESSION.InitDBCollectorLog, "InitDBCollector");

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning session initializing.", e);
            }
        }

        public SESSION Login(SESSION session, string user, string password, string locale)
        {
            IMRLog.AddToLog(Logging.EventID.INFO.Login, new object[] { user });

            SetStartTime();
            var result = _dbClient.Login(session, user, IMR.Suite.Common.RSA.Encrypt(DataConverter.EncodePassword(password)), locale, 0);
            SetEndTime(Logging.EventID.SESSION.LoginLog, "Login");

            if (result.Status == SESSION_STATUS.OK)
            {
                List<object> oList = new List<object>();
                oList.Add((int)DBCollectorProperties.InitWebSimaxDataProvider);
                oList.Add(new List<object>() { null }.ToArray());

                SetStartTime();
                _dbClient.InitDBCollectorProperties(ref result, oList.ToArray());
                SetEndTime(Logging.EventID.SESSION.InitDBCollectorPropertiesLog, "InitDBCollectorProperties");
            }
            return result;
        }

        public OpOperator GetLoggedUser(SESSION session)
        {
            try {
                SetStartTime();
                var loggedUser = _dbClient.OpxGetLoggedOperator(ref session);
                SetEndTime(Logging.EventID.SESSION.GetLoggedOperatorLog, "OpxGetLoggedOperator");

                return loggedUser;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning logged operator info downloading.", e);
            }
        }

        public SESSION_STATUS Logout()
        {
            try
            {
                var session = GetSession();
                if (session != null)
                {
                    SetStartTime();
                    var result = _dbClient.Logout(session);
                    SetEndTime(Logging.EventID.SESSION.LogoutLog, "Logout");
                    if (result == null) return SESSION_STATUS.SESSION_NOT_VALID;
                    return result.Status;
                }
                else return SESSION_STATUS.SESSION_NOT_VALID;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning logout.", e);
            }
        }
    }
}
