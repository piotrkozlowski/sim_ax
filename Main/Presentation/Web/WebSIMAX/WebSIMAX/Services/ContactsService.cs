﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace SIMAX.Services
{
    public interface IContactsService
    {
        GridModel GetContacts(GridRequestModel req);
        GridModel GetGroups(GridRequestModel req);
        ContactModel Save(ContactFormValuesModel req);
        ContactModel GetContact(long id);
        EntityModel GetContactToEdit(long id);
        ResponseDeleteModel Delete(long id);
    }

    public class ContactsService : BaseService, IContactsService
    {     
        public ContactsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;            
        }

        public GridModel GetContacts(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpActor[] opActors = _dbClient.OpxActorFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.USERS.FilterActorLog, "OpxActorFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opActors.Select(actor =>
                    {
                        return Common.DictionaryToObject(actor.DynamicValues, actor.ToString(), actor.IdActor);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public GridModel GetGroups(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();

                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpActorGroup[] opActorGroups = _dbClient.OpxActorGroupFilter(ref session, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.CONTACTS.ActorGroupFilterLog, "OpxActorGroupFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opActorGroups.Select(group =>
                    {
                        return ActorGroupRow(group);
                    }).ToList(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ContactModel GetContact(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opActor = _dbClient.OpxGetActor(ref session, (int)id);
                SetEndTime(Logging.EventID.USERS.GetActorLog, "OpxGetActor");
                base.CheckSession(session);

                if (opActor != null)
                {
                    return ContactModelFromOpActor(opActor, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return ContactModelFromOpActor(opActor, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, String.Format("Issue concerning downloading contact {0}.", id));
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResponseDeleteModel Delete(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opActor = _dbClient.OpxGetActor(ref session, (int)id);
                SetEndTime(Logging.EventID.USERS.GetActorLog, "OpxGetActor");
                base.CheckSession(session);

                opActor.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete;
                SetStartTime();
                var res = _dbClient.OpxSaveActor(ref session, opActor);
                SetEndTime(Logging.EventID.USERS.SaveActorLog, "OpxSaveActor");

                if (res != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel GetContactToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opActor = _dbClient.OpxGetActor(ref session, (int)id);
                SetEndTime(Logging.EventID.USERS.GetActorLog, "OpxGetActor");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opActor.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddActorSpecialFieldsToFieldModel(opActor));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ContactModel Save(ContactFormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpActor opContact;
                int? contactId = null;
                var fieldId = model.Fields.Find(f => f.NameId == DataType.HELPER_ID_ACTOR);
                if (fieldId != null && !String.IsNullOrEmpty(fieldId.Value.ToString()))
                {
                    contactId = Convert.ToInt32(fieldId.Value);
                }
                else if (model.Id != null)
                {
                    contactId = Convert.ToInt32(model.Id);
                }

                if (contactId != null)
                {
                    try
                    {
                        SetStartTime();
                        opContact = _dbClient.OpxGetActor(ref session, Convert.ToInt32(contactId));
                        SetEndTime(Logging.EventID.USERS.GetActorLog, "OpxGetActor");
                        base.CheckSession(session);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning contact " + contactId + " downloading.", e);
                    }
                }
                else
                    opContact = new OpActor();

                if (opContact == null)
                    opContact = new OpActor();

                #region Fields
                FillDataList(opContact.DataList, model.Fields);
                #endregion

                if (model.Groups.Count > 0)
                {
                    List<OpActorGroup> groups = new List<OpActorGroup>();
                    foreach (long idGroup in model.Groups)
                    {
                        groups.Add(new OpActorGroup()
                        {
                            IdActorGroup = (int)idGroup,
                            OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New
                    });
                    }
                    foreach (OpActorGroup group in opContact.CurrentGroups)
                    {
                        if (!model.Groups.Contains(group.IdActorGroup))
                        {
                            var gr = group;
                            gr.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete;
                            groups.Add(gr);
                        }
                    }
                    opContact.CurrentGroups = groups;
                }

                SetStartTime();
                OpActor savedContact = _dbClient.OpxSaveActor(ref session, opContact);
                SetEndTime(Logging.EventID.USERS.SaveActorLog, "OpxSaveActor");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedContact != null)
                {
                    return ContactModelFromOpActor(savedContact, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return ContactModelFromOpActor(savedContact, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning contact " + model.Id + " saving.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }        

        private ContactModel ContactModelFromOpActor(OpActor opActor, bool replaceReferenceTypes = false, SESSION session = null)
        {
            ContactModel model;

            if (opActor != null)
            {
                var fields = opActor.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                model = new ContactModel();

                model.Id = opActor.IdActor;
                model.Name = opActor.ToString();
                model.Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opActor.DynamicValues) : fields;

                ReplaceReferenceIds(model.Fields, opActor.DataList);

                model.Groups = opActor.CurrentGroups != null ? opActor.CurrentGroups.Select(group =>
                {
                    return new GroupModel()
                    {
                        Id = group.IdActorGroup,
                        Name = group.Name,
                        Distributor = group.Distributor.Name,
                        Builtin = group.BuiltIn
                    };
                }).ToList() : new List<GroupModel>();

                model.Locations = opActor.CurrentLocations != null ? opActor.CurrentLocations.Select(loc =>
                {
                    EntityModel location = new EntityModel();

                    location.Id = loc.IdLocation;
                    location.Name = loc.ToString();
                    location.Fields = (loc.DynamicValues != null && loc.DynamicValues.Count > 0) ? loc.DynamicValues.Select(f => FieldModel.FieldFromDynamicValues(f)).ToList() : new List<FieldModel>();

                    return location;
                }).ToList() : new List<EntityModel>();

                model.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
            }
            else
            {
                model = new ContactModel() {
                    Fields = new List<FieldModel>(),
                    Groups = new List<GroupModel>(),
                    Locations = new List<EntityModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return model;
        }

        private List<FieldModel> AddActorSpecialFieldsToFieldModel(OpActor actor)
        {
            var list = new List<FieldModel>();
            //list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_STATUS, route.IdRouteStatus, "Integer", false));
            //list.Add(PrepareFieldModel(DataType.HELPER_ID_OPERATOR, route.IdOperatorExecutor, "Integer", false));
            //list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_TYPE, route.IdRouteType, "Integer", false));
            //list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_DEF, route.IdRouteDef, "Integer", false));
            //list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, route.IdDistributor, "integer", false));

            return list;
        }

        private dynamic ActorGroupRow(OpActorGroup group)
        {
            IDictionary<string, object> eo = new ExpandoObject() as IDictionary<string, object>;
            if (group == null) return eo;
            string key = "distributor";// DataType.HELPER_ID_DISTRIBUTOR.ToString();
            eo.Add(key, group.Distributor.Name);
            key = "name";// DataType.HELPER_NAME.ToString();
            eo.Add(key, group.Name);
            key = DataType.HELPER_ID_ACTOR_GROUP.ToString();
            eo.Add(key, group.IdActorGroup);
            key = "id";
            eo.Add(key, group.IdActorGroup);
            key = "builtin";
            eo.Add(key, group.BuiltIn);
            eo.Add("ValueString", group.ToString());
            return eo;
        }
    }
}
