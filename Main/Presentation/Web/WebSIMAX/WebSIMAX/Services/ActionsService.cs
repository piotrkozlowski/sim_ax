﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Models;
using IMR.Suite.UI.Business.Objects.CORE;
using SIMAX.Utils;
using IMR.Suite.Common;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace SIMAX.Services
{
    public interface IActionsService
    {
        GridModel GetActions(GridRequestModel req);
        ActionModel SaveAction(FormValuesModel req);
        ActionModel GetAction(long id);
        EntityModel GetActionToEdit(long id);
        ResponseDeleteModel DeleteAction(long id);
    }

    public class ActionsService : BaseService, IActionsService
    {
        public ActionsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public ResponseDeleteModel DeleteAction(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opAction = _dbClient.OpxGetAction(ref session, id);
                SetEndTime(Logging.EventID.ACTIONS.GetActionLog, "OpxGetAction");
                base.CheckSession(session);

                //_dbClient.OpxDeleteAction(ref session, opAction);
                base.CheckSession(session);

                if (opAction != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch(Exception e)
            {
                throw new Exception("Issue concerning with deleting action" + id, e);
            }
        }

        public ActionModel GetAction(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opAction = _dbClient.OpxGetAction(ref session, id);
                SetEndTime(Logging.EventID.ACTIONS.GetActionLog, "OpxGetAction");
                base.CheckSession(session);

                if (opAction != null)
                {
                    return ActionModelFromOpAction(opAction, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return ActionModelFromOpAction(opAction, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning action " + id + " downloading.");
                    }
                }
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch(Exception e)
            {
                throw new Exception("Issue concerning action " + id + " downloading.", e);
            }
        }

        public GridModel GetActions(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                // Uncomment when webservice will be ready 
                // var columns = req.Columns.Select(column => new ColumnInfo() { SortType = column.SortDesc.HasValue ? column.SortDesc.Value ? SortType.Desc : SortType.Asc : SortType.None, IdDataType = column.Id, Filter = column.Where, Name = column.Name }).ToArray();

                // remove when webservice will be ready
                var x = ListColumnModelToColumnInfoArray(req.Columns).ToList();
                x.Add(new ColumnInfo() { SortType = SortType.None, Filter = new string[] { "" }, IdDataType = 10129, Name = "ACT_10129_0" });
                var columns = x.ToArray();

                long totalCount = 0;
                SetStartTime();
                var opActions = _dbClient.OpxActionFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, 1, req.Where);
                SetEndTime(Logging.EventID.ACTIONS.FilterActionLog, "OpxActionFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opActions.Select(action =>
                    {
                        return Common.DictionaryToObject(action.DynamicValues, action.ToString(), action.IdAction);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning actions downloading.", e);
            }
        }

        public EntityModel GetActionToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opAction = _dbClient.OpxGetAction(ref session, id);
                SetEndTime(Logging.EventID.ACTIONS.GetActionLog, "OpxGetAction");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opAction.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddActionSpecialFieldsToFieldModel(opAction));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch(Exception e)
            {
                throw new Exception("Issue concerning action to edit downloading.", e);
            }
        }

        public ActionModel SaveAction(FormValuesModel req)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpAction opAction;
                if (req.Id != null)
                {
                    try
                    {
                        SetStartTime();
                        opAction = _dbClient.OpxGetAction(ref session, Convert.ToInt64(req.Id));
                        SetEndTime(Logging.EventID.ACTIONS.GetActionLog, "OpxGetAction");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning meter " + req.Id + " downloading.", e);
                    }
                    base.CheckSession(session);
                }
                else
                    opAction = new OpAction();

                #region Fields
                FillDataList(opAction.DataList, req.Fields);
                #endregion

                SetStartTime();
                var savedAction = _dbClient.OpxSaveAction(ref session, opAction);
                SetEndTime(Logging.EventID.ACTIONS.SaveActionLog, "OpxSaveAction");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }
                if (opAction != null)
                {
                    return ActionModelFromOpAction(opAction, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return ActionModelFromOpAction(opAction, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning meter " + req.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + req.Id + " saving.", e);
            }
        }

        private ActionModel ActionModelFromOpAction(OpAction action, bool replaceReferenceTypes = false, SESSION session = null)
        {
            ActionModel actionModel;

            if (action != null)
            {
                var fields = action.DynamicValues.Select(dv => FieldModel.FieldFromDynamicValues(dv)).ToList();

                actionModel = new ActionModel()
                {
                    Id = action.IdAction,
                    Name = action.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, action.DynamicValues) : fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(actionModel.Fields, action.DataList);
            }
            else
            {
                actionModel = new ActionModel()
                {
                    Fields = new List<FieldModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return actionModel;
        }

        private List<FieldModel> AddActionSpecialFieldsToFieldModel(OpAction location)
        {
            var list = new List<FieldModel>();

            return list;
        }
    }
}
