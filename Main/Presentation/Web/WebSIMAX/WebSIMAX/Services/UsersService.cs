﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Models;
using SIMAX.Utils;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.Diagnostics;
using System.Net;
using IMR.Suite.UI.Business.Objects;
using static IMR.Suite.Common.Enums;
using Microsoft.AspNetCore.Http;

namespace SIMAX.Services
{
    public interface IUsersService
    {
        GridModel GetUsers(GridRequestModel req);
        OperatorModel GetUser(int id);
        EntityModel GetUserToEdit(int id);
        OperatorModel SaveUser(FormValuesModel model);
        ResponseDeleteModel DeleteUser(int id);
        OperatorModel SubmitActivities(SecurityChangeModel model);
        OperatorModel SubmitRoles(SecurityChangeModel model);
        OperatorModel ChangeNotification(NotificationModel model);
        OperatorModel ChangeReferenceObject(SecurityActivityModel model);
        ResponseListModel<int> ChangeAllowedDistributors(FormValuesModel model);
        PasswordResetResponseModel ChangePassword(PasswordResetModel model);
    }

    public class UsersService : BaseService, IUsersService
    {
        public UsersService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public GridModel GetUsers(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                var values = _dbClient.OpxOperatorFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.USERS.FilterOperatorLog, "OpxOperatorFilter");
                base.CheckSession(session);
                
                var result = new GridModel()
                {
                    TotalCount = totalCount,
                    Values = values.Select(u =>
                    {
                        return Common.DictionaryToObject(u.DynamicValues, u.ToString(), u.IdOperator);
                    }).ToList(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operators downloading.",e );
            }
        }

        public OperatorModel GetUser(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opUser = _dbClient.OpxGetOperator(ref session, id);
                SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                base.CheckSession(session);

                if (opUser != null)
                {
                    return OperatorModelFromOpOperator(opUser, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return OperatorModelFromOpOperator(opUser, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning operator " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + id + " downloading.", e);
            }
        }

        public EntityModel GetUserToEdit(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opUser = _dbClient.OpxGetOperator(ref session, id);
                SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opUser.DataList.Select(u => FieldModel.FieldFromOpDataList(u)).ToList();
                fields.AddRange(opUser.Actor.DataList.Select(u => FieldModel.FieldFromOpDataList(u)).ToList());

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                long passwordId = 10049;
                foreach (var field in result.Fields)
                {
                    if (field.Id == passwordId)
                    {
                        field.Value = "***";
                    }
                }

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + id + " downloading.", e);
            }
        }

        public OperatorModel SaveUser(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpOperator opOperator;
                if (model.Id != null)
                {
                    try
                    {
                        SetStartTime();
                        opOperator = _dbClient.OpxGetOperator(ref session, Convert.ToInt32(model.Id));
                        SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning operator " + model.Id + " downloading.", e);
                    }
                    base.CheckSession(session);
                }
                else
                {
                    opOperator = new OpOperator();
                    opOperator.IdActor = 0;
                }

                #region Operator Fields
                var operatorDataTypes = DataType.GetHelperDataTypes(ReferenceType.IdOperator, true);
                FillDataList(opOperator.DataList, model.Fields.Where(field => operatorDataTypes.Contains(field.NameId)).ToList());
                #endregion

                #region Actor Fields
                var actorDataTypes = DataType.GetHelperDataTypes(ReferenceType.IdActor, true);
                FillDataList(opOperator.Actor.DataList, model.Fields.Where(field => actorDataTypes.Contains(field.NameId)).ToList());
                #endregion

                SetStartTime();
                var savedUser = _dbClient.OpxSaveOperator(ref session, opOperator);
                SetEndTime(Logging.EventID.USERS.SaveOperatorLog, "OpxSaveOperator");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    Debug.Write(session.Error.Msg);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedUser != null)
                {
                    return OperatorModelFromOpOperator(savedUser, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return OperatorModelFromOpOperator(savedUser, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning operator " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + model.Id + " saving.", e);
            }
        }

        public ResponseDeleteModel DeleteUser(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opOperator = new OpOperator();
                try
                {
                    SetStartTime();
                    opOperator = _dbClient.OpxGetOperator(ref session, id);
                    SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning operator " + id + " downloading.", e);
                }
                base.CheckSession(session);
                SetStartTime();
                _dbClient.OpxDeleteOperator(ref session, opOperator);
                SetEndTime(Logging.EventID.USERS.DeleteOperatorLog, "OpxDeleteOperator");
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    throw new StatusCodeException((int)HttpStatusCode.NotFound);
                }
                return new ResponseDeleteModel(true, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + id + " deleting.", e);
            }
        }

        public OperatorModel ChangeNotification(NotificationModel model)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opOperator = new OpOperator();
                try
                {
                    SetStartTime();
                    opOperator = _dbClient.OpxGetOperator(ref session, model.OperatorId);
                    SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning operator " + model.OperatorId + " downloading.", e);
                }
                base.CheckSession(session);
                
                foreach(var entity in model.Set)
                {
                    SetOrResetNotifity(ref opOperator, entity, model.Type, true);
                }

                foreach(var entity in model.Reset)
                {
                    SetOrResetNotifity(ref opOperator, entity, model.Type, false);
                }

                var opDistributors = new List<OpDistributor>();
                try
                {
                    foreach (var id in model.DistributorIds)
                    {
                        SetStartTime();
                        opDistributors.Add(_dbClient.OpxGetDistributor(ref session, id));
                        SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetDistributor");
                        base.CheckSession(session);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning distributors downloading.", e);
                }
                EmailNotificationOptions.NotificationType notificationType;
                if (model.Type == "task")
                    notificationType = EmailNotificationOptions.NotificationType.Task;
                else if (model.Type == "issue")
                    notificationType = EmailNotificationOptions.NotificationType.Issue;
                else if (model.Type == "workorder")
                    notificationType = EmailNotificationOptions.NotificationType.ShippingList;
                else
                    notificationType = EmailNotificationOptions.NotificationType.Location;

                opOperator.EmailNotificationOptions.SetNotify(opDistributors, notificationType);
                SetStartTime();
                opOperator = _dbClient.OpxSaveOperator(ref session, opOperator);
                SetEndTime(Logging.EventID.USERS.SaveOperatorLog, "OpxSaveOperator");
                base.CheckSession(session);

                if (opOperator != null)
                {
                    return OperatorModelFromOpOperator(opOperator, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return OperatorModelFromOpOperator(opOperator, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning operator " + model.OperatorId + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + model.OperatorId + " saving.", e);
            }
        }
        
        public OperatorModel SubmitActivities(SecurityChangeModel model)
        {
            return OperatorModelFromOpOperator(SubmitActivitiesOrRoles<OpActivity>(model.OperatorId, model.Added, model.Removed, Enums.ReferenceType.IdActivity), true);
        }

        public OperatorModel SubmitRoles(SecurityChangeModel model)
        {
            return OperatorModelFromOpOperator(SubmitActivitiesOrRoles<OpRole>(model.OperatorId, model.Added, model.Removed, Enums.ReferenceType.IdRole), true);
        }

        private OpOperator SubmitActivitiesOrRoles<T>(int operatorId, List<int> added, List<int> removed, Enums.ReferenceType referenceType)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opOperator = new OpOperator();
                try
                {
                    SetStartTime();
                    opOperator = _dbClient.OpxGetOperator(ref session, operatorId);
                    SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                }
                catch(Exception e)
                {
                    throw new Exception("Issue concerning operator " + operatorId + " downloading.", e);
                }
                base.CheckSession(session);
                var dictionary = new object();
                try
                {
                    SetStartTime();
                    dictionary = _dbClient.GetDictionaries(ref session, new Enums.ReferenceType[] { referenceType }).FirstOrDefault();
                    SetEndTime(Logging.EventID.DICTIONARY.GetDictionariesLog, "GetDictionaries");
                }
                catch(Exception e)
                {
                    throw new Exception("Issue concerning dictionaries downloading.", e);
                }
                base.CheckSession(session);

                foreach (var id in added)
                {
                    T itemToSomeAction = (T)Array.Find(dictionary as object[], d =>
                    {
                        var dict = (T)d;
                        if (dict is OpRole)
                            return (dict as OpRole).IdRole == id;
                        return (dict as OpActivity).IdActivity == id;
                    });

                    if (typeof(T) == typeof(OpRole))
                    {
                        (itemToSomeAction as OpRole).OpState = OpChangeState.New;
                        
                        opOperator.Roles.Add(itemToSomeAction as OpRole);
                    }
                    else if (typeof(T) == typeof(OpActivity))
                    {
                        (itemToSomeAction as OpActivity).OpState = OpChangeState.New;
                        
                        opOperator.AllActivities.Add(itemToSomeAction as OpActivity);
                    }
                }
                foreach(var id in removed)
                {
                    T itemToSomeAction = (T)Array.Find(dictionary as object[], d =>
                    {
                        var dict = (T)d;
                        if (dict is OpRole)
                            return (dict as OpRole).IdRole == id;
                        return (dict as OpActivity).IdActivity == id;
                    });

                    if (typeof(T) == typeof(OpRole))
                    {
                        opOperator.Roles.Find(a => a.IdRole == id).OpState = OpChangeState.Delete;
                    }
                    else if (typeof(T) == typeof(OpActivity))
                    {
                        opOperator.AllActivities.Find(a => a.IdActivity == id && a.IdOperatorActivity != null).OpState = OpChangeState.Delete;
                    }
                }
                SetStartTime();
                var result = _dbClient.OpxSaveOperator(ref session, opOperator);
                SetEndTime(Logging.EventID.USERS.SaveOperatorLog, "OpxSaveOperator");
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + operatorId + " saving.", e);
            }
        }

        public OperatorModel ChangeReferenceObject(SecurityActivityModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                var opOperator = new OpOperator();
                try
                {
                    SetStartTime();
                    opOperator = _dbClient.OpxGetOperator(ref session, (int)model.OperatorId);
                    SetEndTime(Logging.EventID.USERS.GetOperatorLog, "OpxGetOperator");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning operator " + model.OperatorId + " downloading.", e);
                }
                base.CheckSession(session);

                var activity = opOperator.AllActivities.Find(a => a.IdActivity == model.Id && a.IdOperatorActivity != null);
                activity.ReferenceValue = model.ReferenceObjectId;
                SetStartTime();
                opOperator = _dbClient.OpxSaveOperator(ref session, opOperator);
                SetEndTime(Logging.EventID.USERS.SaveOperatorLog, "OpxSaveOperator");

                if (opOperator != null)
                {
                    return OperatorModelFromOpOperator(opOperator, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return OperatorModelFromOpOperator(opOperator, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning operator " + model.OperatorId + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning operator " + model.OperatorId + " saving.", e);
            }
        }

        public ResponseListModel<int> ChangeAllowedDistributors(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                var parameters = new List<object>();
                parameters.Add((int)DBCollectorProperties.Relogin);
                parameters.Add(new object[] { model.Values.Select(v => (int)v).Cast<object>().ToArray() });
                SetStartTime();
                _dbClient.InitDBCollectorProperties(ref session, parameters.ToArray());
                SetEndTime(Logging.EventID.SESSION.InitDBCollectorPropertiesLog, "InitDBCollectorProperties");
                base.CheckSession(session);

                return new ResponseListModel<int>(model.Values.Select(v => (int)v).ToList(), session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning collector properties initalizing.", e);
            }
        }

        public PasswordResetResponseModel ChangePassword(PasswordResetModel model)
        {            
            var session = _sessionService.GetSession();
            var user = _sessionService.GetLoggedUser(session);
            SetStartTime();
            _dbClient.OpxChangeOperatorPassword(ref session, user.IdOperator, null, model.Password, model.OldPassword, null);
            SetEndTime(Logging.EventID.USERS.ChangeOperatorPasswordLog, "OpxChangeOperatorPassword");
            return new PasswordResetResponseModel()
            {
                Status = session.Status.ToString(),
                Message = session.Error.Msg,
                HasErrorCode = (session != null && session.Error != null && session.Error.Code != null && (int)session.Error.Code != 5) ? (int)session.Error.Code : 0
            };
        }

        private void SetOrResetNotifity(ref OpOperator opOperator, NotificationEntity entity, string model, bool isSet)
        {
            dynamic emailNotification, smsNotification;
            switch (model)
            {
                case "task":
                    emailNotification = (EmailNotificationOptions.TaskOptions)entity.Id;
                    smsNotification = (SmsNotificationOptions.TaskOptions)entity.Id;
                    break;
                case "issue":
                    emailNotification = (EmailNotificationOptions.IssueOptions)entity.Id;
                    smsNotification = (SmsNotificationOptions.IssueOptions)entity.Id;
                    break;
                case "location":
                    emailNotification = (EmailNotificationOptions.LocationOptions)entity.Id;
                    smsNotification = (SmsNotificationOptions.LocationOptions)entity.Id;
                    break;
                case "workorder":
                default:
                    emailNotification = (EmailNotificationOptions.WorkOrderOptions)entity.Id;
                    smsNotification = (SmsNotificationOptions.WorkOrderOptions)entity.Id;
                    break;
            }

            if (entity.IsEmailValue)
            {
                if (isSet)
                    opOperator.EmailNotificationOptions.SetNotify(emailNotification);
                else
                    opOperator.EmailNotificationOptions.ResetNotify(emailNotification);
            }
            else
            {
                if (isSet)
                    opOperator.SmsNotificationOptions.SetNotify(smsNotification);
                else
                    opOperator.SmsNotificationOptions.ResetNotify(smsNotification);
            }
        }

        private OperatorModel OperatorModelFromOpOperator(OpOperator opOperator, bool replaceReferenceTypes = false, SESSION sessionIn = null)
        {
            OperatorModel operatorModel;

            if (opOperator != null)
            {
                var fields = opOperator.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();
                var actorFields = opOperator.Actor.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();
                var allActivities = ActivitiesToModel(opOperator.AllActivities != null ? opOperator.AllActivities.ToArray() : new object[0]);
                var session = _sessionService.GetSession();

                foreach (var activity in allActivities.Values.Where(a => a.ReferenceObject != string.Empty))
                {
                    var totalCount = 0L;
                    SetStartTime();
                    var referenceObject = _dbClient.GetReferenceObjectList(ref session, out totalCount, (ReferenceType)activity.ReferenceTypeId, new object[] { activity.ReferenceObject }, null, 1, 1, "").FirstOrDefault();
                    SetEndTime(Logging.EventID.COMMON.GetRefObjectListLog, "GetReferenceObjectList");
                    if (referenceObject != null)
                        activity.ReferenceObject = referenceObject.ToString();
                }

                operatorModel = new OperatorModel()
                {
                    Id = opOperator.IdOperator,
                    Name = opOperator.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opOperator.DynamicValues) : fields,
                    ActorFields = replaceReferenceTypes ? ReplaceReferenceFields(actorFields, opOperator.Actor.DynamicValues) : actorFields,
                    Roles = RolesToModel(opOperator.Roles != null ? opOperator.Roles.ToArray() : new object[0]),
                    Activities = ActivitiesToModel(opOperator.Activities != null ? opOperator.Activities.ToArray() : new object[0]),
                    AllActivities = allActivities,
                    NotificationTasks = GetSpecifiedNotifications<EmailNotificationOptions.TaskOptions, SmsNotificationOptions.TaskOptions>(opOperator, EmailNotificationOptions.GetTaskOptions),
                    NotificationIssues = GetSpecifiedNotifications<EmailNotificationOptions.IssueOptions, SmsNotificationOptions.IssueOptions>(opOperator, EmailNotificationOptions.GetIssueOptions),
                    NotificationWorkOrders = GetSpecifiedNotifications<EmailNotificationOptions.WorkOrderOptions, SmsNotificationOptions.WorkOrderOptions>(opOperator, EmailNotificationOptions.GetWorkOrderOptions),
                    NotificationLocations = GetSpecifiedNotifications<EmailNotificationOptions.LocationOptions, SmsNotificationOptions.LocationOptions>(opOperator, EmailNotificationOptions.GetLocationOptions),
                    HasErrorCode = (sessionIn != null && sessionIn.Error != null && sessionIn.Error.Code != null) ? (int)sessionIn.Error.Code : 0
                };

                long passwordId = 10049;
                foreach (var field in operatorModel.Fields)
                {
                    if (field.Id == passwordId)
                    {
                        field.Value = "***";
                    }
                }

                ReplaceReferenceIds(operatorModel.Fields, opOperator.DataList);
            }
            else
            {
                operatorModel = new OperatorModel()
                {
                    Fields = new List<FieldModel>(),
                    ActorFields = new List<FieldModel>(),
                    Roles = new GridModel(),
                    Activities = new GridModel(),
                    AllActivities = new GridModel(),
                    NotificationTasks = new GridModel(),
                    NotificationIssues = new GridModel(),
                    NotificationWorkOrders = new GridModel(),
                    NotificationLocations = new GridModel(),
                    HasErrorCode = (sessionIn != null && sessionIn.Error != null && sessionIn.Error.Code != null) ? (int)sessionIn.Error.Code : 0
                };
            }

            return operatorModel;
        }

        private delegate List<object> GetOptions();

        private GridModel GetSpecifiedNotifications<Email, Sms>(OpOperator opOperator, GetOptions GetNotificationOptions)
        {
            try
            {
                var gridModel = new GridModel();

                var taskOptions = GetNotificationOptions();

                if (taskOptions != null)
                {
                    gridModel.TotalCount = taskOptions.Count;

                    gridModel.Values = taskOptions.Select(option =>
                    {
                        dynamic emailTaskOption = (Email)option;
                        dynamic smsTaskOption = (Sms)option;
                        var dictionary = new Dictionary<string, object>();

                        dictionary.Add("NotificationId", emailTaskOption.ToString("d"));
                        dictionary.Add("1", emailTaskOption.ToString("g"));
                        dictionary.Add("2", opOperator.EmailNotificationOptions != null ? opOperator.EmailNotificationOptions.GetNotify(emailTaskOption) : false);
                        dictionary.Add("3", opOperator.SmsNotificationOptions != null ? opOperator.SmsNotificationOptions.GetNotify(smsTaskOption) : false);

                        return Common.DictionaryToObject(dictionary);
                    }).ToList();
                }

                return gridModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning collector specified notifications downloading.", e);
            }
        }
    }
}
