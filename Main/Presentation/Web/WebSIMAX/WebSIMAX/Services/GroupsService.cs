﻿using SIMAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.Common;
using SIMAX.Utils;
using Microsoft.AspNetCore.Http;
using IMR.Suite.UI.Business.Objects.CORE;

namespace SIMAX.Services
{
    public interface IGroupsService
    {
        ResponseListModel<GroupModel> GetLocationGroups();
        //ResponseListModel<GroupModel> GetMeterGroups();
    }
    public class GroupsService: BaseService, IGroupsService
    {
        public GroupsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public ResponseListModel<GroupModel> GetLocationGroups()
        {
            try
            {
                var activeViews = GetActiveViews();
                var locationView = activeViews.FirstOrDefault(v => v.IdReferenceType == (int)Enums.ReferenceType.IdLocation);

                var result = new List<GroupModel>();
                var session = _sessionService.GetSession();

                if (locationView != null)
                {
                    long totalCount = 0;
                    var viewColumnType = locationView.GetViewColumn(DataType.HELPER_ID_LOCATION_TYPE, null, 0, Enums.ViewColumnType.BaseHelper, locationView.PrimaryKeyColumn);
                    var viewColumnName = locationView.GetViewColumn(DataType.LOCATION_NAME, null, 0, Enums.ViewColumnType.Standard, locationView.PrimaryKeyColumn);
                    var columnLocationType = viewColumnType.Name;
                    var columnLocationName = viewColumnName.Name;
                    var where = String.Format("[{0}] = {1}", columnLocationType, (int)Enums.LocationType.GroupingLocation);
                    var columns = new ColumnInfo[] { new ColumnInfo() { IdDataType = DataType.LOCATION_NAME, Name = columnLocationName } };
                    SetStartTime();
                    var groups = _dbClient.OpxLocationFilter(ref session, out totalCount, columns, 1, int.MaxValue - 200, where);
                    SetEndTime(Logging.EventID.LOCATIONS.FilterLocationLog, "OpxLocationFilter");
                    base.CheckSession(session);

                    result.AddRange(groups.Select(g => new GroupModel() { Id = g.IdLocation, Name = g.ToString() }).ToList());
                }
                return new ResponseListModel<GroupModel>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location groups downloading ", e);
            }
        }
        //this piece of crap doesn't wanna work
        // public ResponseListModel<GroupModel> GetMeterGroups()
        // {
        //     try 
        //     {
        //         var activeViews = GetActiveViews();
        //         var meterView = activeViews.FirstOrDefault(vw => vw.IdReferenceType == (int)Enums.ReferenceType.IdMeter);
        //         var result = new List<GroupModel>();
        //         var session = _sessionService.GetSession();

        //         if(meterView != null) 
        //         {
        //             long totalCount = 0;
        //             var viewColumnType = meterView.GetViewColumn(DataType.HELPER_ID_METER_TYPE, null, 0, Enums.ViewColumnType.BaseHelper, meterView.PrimaryKeyColumn);
        //             var viewColumnName = meterView.GetViewColumn(DataType.METER_NAME, null, 0, Enums.ViewColumnType.Standard, meterView.PrimaryKeyColumn);
        //             var columnMeterType = viewColumnType.Name;
        //             var columnMeterName = viewColumnName.Name;
        //             var where = String.Format("[{0}] = {1}", columnMeterType, (int)Enums.MeterType.Gallus2002);
        //             var columns = new ColumnInfo[] {
        //                 new ColumnInfo() {
        //                     IdDataType = DataType.METER_NAME, Name = columnMeterName
        //                 }
        //             };
        //             SetStartTime();
        //             var groups = _dbClient.OpxGetMeterAlarmGroup(ref session, out totalCount, columns, 1, int.MaxValue - 100, where);
        //             SetEndTime(Logging.EventID.METERS.FilterMeterLog, "OpxGetMeterAlarmGroup");
        //             base.CheckSession(session);
        //             result.AddRange(groups.Select(gr => new GroupModel() {
        //                 Id = gr.IdMeter, Name = gr.ToString()
        //             }).ToList());
        //         }
        //         return new ResponseListModel<GroupModel>(result, session);
        //     }
        //     catch (StatusCodeException e)
        //     {
        //         throw e;
        //     }
        //     catch (Exception e)
        //     {
        //         throw new Exception("Issue concerning meter groups downloading ", e);
        //     }
        // }
    }
}
