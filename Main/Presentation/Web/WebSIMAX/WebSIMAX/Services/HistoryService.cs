﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.Custom;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIMAX.Services
{
    public interface IHistoryService
    {
        ResponseListModel<HistoryModel> GetLocationHistory(long locationId);
        ResponseListModel<HistoryModel> GetDeviceHistory(long deviceId);
        ResponseListModel<HistoryModel> GetMeterHistory(long meterId);
        ResponseListModel<HistoryIssueModel> GetIssueHistory(long meterId);
        ResponseListModel<HistoryModel> GetTaskHistory(int meterId);
        ResponseListModel<HistoryModel> GetSimcardHistory(long simcardId);
    }

    public class HistoryService : BaseService, IHistoryService
    {
        public HistoryService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public ResponseListModel<HistoryModel> GetLocationHistory(long locationId)
        {
            SetStartTime();
            var locationHistory = GetHistory(_dbClient.OpxGetLocationHistory, locationId);
            SetEndTime(Logging.EventID.LOCATIONS.GetLocationHistoryLog, "OpxGetLocationHistory");
            return locationHistory;
        }

        public ResponseListModel<HistoryModel> GetDeviceHistory(long deviceId)
        {
            SetStartTime();
            var deviceHistory = GetHistory(_dbClient.OpxGetDeviceHistory, deviceId);
            SetEndTime(Logging.EventID.DEVICES.GetDeviceHistoryLog, "OpxGetDeviceHistory");
            return deviceHistory;
        }

        public ResponseListModel<HistoryModel> GetMeterHistory(long meterId)
        {
            SetStartTime();
            var meterHistory = GetHistory(_dbClient.OpxGetMeterHistory, meterId);
            SetEndTime(Logging.EventID.METERS.GetMeterHistoryLog, "OpxGetMeterHistory");
            return meterHistory;
        }

        public ResponseListModel<HistoryModel>GetSimcardHistory(long simcardId)
        {
            SetStartTime();
            var simcardsHistory = GetHistory(_dbClient.OpxGetSimCardHistory, simcardId);
            SetEndTime(Logging.EventID.SIMCARDS.GetSimcardHistoryLog, "OpxGetSimCardHistory");
            return simcardsHistory;
        }

        public ResponseListModel<HistoryIssueModel> GetIssueHistory(long issueId)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opIssue = _dbClient.OpxGetIssue(ref session, (int)issueId);
                SetEndTime(Logging.EventID.ISSUES.GetIssueLog, "OpxGetIssue");
                base.CheckSession(session);

                var result = new List<HistoryIssueModel>();

                if (opIssue != null && opIssue.IssueHistory != null)
                {
                    opIssue.IssueHistory.ForEach(data =>
                    {
                        var field = new HistoryIssueModel()
                        {
                            Id = data.IdIssueHistory,
                            IdIssue = data.IdIssue,
                            IdIssueStatus = data.IdIssueStatus,
                            IssueStatusDesc = data.IssueStatus != null ? data.IssueStatus.Description : "",
                            IdOperator = data.IdOperator,
                            OperatorDesc = data.Operator != null ? data.Operator.Description : "",
                            IdPriority = data.IdPriority,
                            PriorityDesc = data.Priority != null ? data.Priority.Descr.Description : "",
                            StartDate = data.StartDate,
                            EndDate = data.EndDate,
                            Deadline = data.Deadline,
                            Notes = data.Notes,
                            ShortDescr = data.ShortDescr
                        };
                        result.Add(field);
                    });
                }

                return new ResponseListModel<HistoryIssueModel>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
		
        public ResponseListModel<HistoryModel> GetTaskHistory(int taskId)
        {
            SetStartTime();
            var taskHistory = GetHistory(_dbClient.OpxGetTaskHistory, taskId);
            SetEndTime(Logging.EventID.LOCATIONS.GetLocationHistoryLog, "OpxGetTaskHistory");

            return taskHistory;
        }

        private ResponseListModel<HistoryModel> GetHistory<T>(GetHistoryFunction<T> getHistory, T id)
        {
            try
            {
                var session = _sessionService.GetSession();
                var history = getHistory(ref session, id);
                base.CheckSession(session);

                var result = new List<HistoryModel>();

                result.AddRange(history.Select(h => new HistoryModel()
                {
                    Description = h.Descr,
                    LongDescription = h.LongDescr,
                    EventDate = h.EventDate,
                    EventObject = h.EventObject,
                    EventType = h.EventType.ToString()
                }).ToList());

                return new ResponseListModel<HistoryModel>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning history downloading.", e);
            }
        }

        private delegate OpHistory[] GetHistoryFunction<T>(ref SESSION session, T id);
    }
}
