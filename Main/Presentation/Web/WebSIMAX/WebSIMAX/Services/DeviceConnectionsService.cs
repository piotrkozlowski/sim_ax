﻿using SIMAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using Microsoft.AspNetCore.Http;
using SIMAX.Utils;
using IMR.Suite.Common;

namespace SIMAX.Services
{
    public interface IDeviceConnectionsService
    {
        GridModel GetDeviceConnections(GridRequestModel req);
    }

    public class DeviceConnectionsService : BaseService, IDeviceConnectionsService
    {
        public DeviceConnectionsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public GridModel GetDeviceConnections(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                var values = _dbClient.OpxDeviceConnectionFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.DEVICECONNECTIONS.DeviceConntectionFilterLog, "OpxDeviceConnectionFilter");
                base.CheckSession(session);

                var activeViews = GetActiveViews();
                var deviceConnectionsView = activeViews.FirstOrDefault(v => v.IdReferenceType == (int)Enums.ReferenceType.IdDeviceConnection);

                string snColumnName = null, snParentColumnName = null, idLocationSnColumnnName = null, idLocationSnParentColumnnName = null, parentLatName = null, parentLonName = null, deviceLatName = null, deviceLonName = null;

                if (deviceConnectionsView != null)
                {
                    var snColumnn = deviceConnectionsView.GetViewColumn(DataType.HELPER_SERIAL_NBR, viewColumnType: Enums.ViewColumnType.BaseHelper);
                    if (snColumnn != null)
                    {
                        snColumnName = snColumnn.Name;
                        var idLocationSnColumnn = deviceConnectionsView.GetViewColumn(DataType.HELPER_ID_LOCATION, viewColumnType: Enums.ViewColumnType.BaseHelper, keyColumn: snColumnn);
                        if(idLocationSnColumnn != null)
                        {
                            idLocationSnColumnnName = idLocationSnColumnn.Name;
                            var deviceLat = deviceConnectionsView.GetViewColumn(DataType.LOCATION_LATITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: idLocationSnColumnn);
                            if (deviceLat != null)
                            {
                                deviceLatName = deviceLat.Name;
                            }
                            var deviceLon = deviceConnectionsView.GetViewColumn(DataType.LOCATION_LONGITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: idLocationSnColumnn);
                            if (deviceLon != null)
                            {
                                deviceLonName = deviceLon.Name;
                            }

                        }
                    }

                    var snParentColumn = deviceConnectionsView.GetViewColumn(DataType.HELPER_SERIAL_NBR_PARENT);
                    if (snParentColumn != null)
                    {
                        snParentColumnName = snParentColumn.Name;
                        var idLocationSnParentColumnn = deviceConnectionsView.GetViewColumn(DataType.HELPER_ID_LOCATION, viewColumnType: Enums.ViewColumnType.BaseHelper, keyColumn: snParentColumn);
                        if (idLocationSnParentColumnn != null)
                        {
                            idLocationSnParentColumnnName = idLocationSnParentColumnn.Name;
                            var parentLat = deviceConnectionsView.GetViewColumn(DataType.LOCATION_LATITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: idLocationSnParentColumnn);
                            if (parentLat != null)
                            {
                                parentLatName = parentLat.Name;
                            }
                            var parentLon = deviceConnectionsView.GetViewColumn(DataType.LOCATION_LONGITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: idLocationSnParentColumnn);
                            if(parentLon != null)
                            {
                                parentLonName = parentLon.Name;
                            }
                        }
                    }
                }

                var result = new GridModel()
                {

                    Values = values.Select(dc =>
                    {
                        var objId = dc.SerialNbrParent.ToString();
                        var obj = Common.DictionaryToObject(dc.DynamicValues, objId, dc.SerialNbrParent);
                        obj.ParentLat = parentLatName != null && dc.DynamicValues.ContainsKey(parentLatName) ? dc.DynamicValues[parentLatName] : null;
                        obj.ParentLon = parentLonName != null && dc.DynamicValues.ContainsKey(parentLonName) ? dc.DynamicValues[parentLonName] : null;

                        obj.DeviceLat = deviceLatName != null && dc.DynamicValues.ContainsKey(deviceLatName) ? dc.DynamicValues[deviceLatName] : null;
                        obj.DeviceLon = deviceLonName != null && dc.DynamicValues.ContainsKey(deviceLonName) ? dc.DynamicValues[deviceLonName] : null;

                        obj.SerialNbrParent = snParentColumnName != null && dc.DynamicValues.ContainsKey(snParentColumnName) ? dc.DynamicValues[snParentColumnName] : null;
                        obj.SerialNbr = snColumnName != null && dc.DynamicValues.ContainsKey(snColumnName) ? dc.DynamicValues[snColumnName] : null;

                        obj.SerialNbrCol = snColumnName;
                        obj.SerialNbrParentCol = snParentColumnName;
                        return obj;
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning device connections downloading.", e);
            }
        }
    }
}
