﻿using IMR.Suite.Common;
using System;
using System.Diagnostics;

namespace SIMAX.Services
{
    public class TimeLogsService
    {
        protected Stopwatch _startTime;

        protected void SetStartTime()
        {
            _startTime = Stopwatch.StartNew();
        }

        protected void SetEndTime(LogData logData, string methodName = "TODO")
        {
            if (_startTime != null)
            {
                _startTime.Stop();
                IMRLog.AddToLog(logData, new object[] { "GUID", methodName, _startTime.Elapsed.ToString("ss'.'ff") });
            }
        }

    }
}
