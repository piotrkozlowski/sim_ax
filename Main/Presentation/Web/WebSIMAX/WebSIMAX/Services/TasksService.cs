﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;

namespace SIMAX.Services
{
    public interface ITasksService : IImportService
    {
        GridModel GetTasks(GridRequestModel req);
        TaskModel SaveTask(FormValuesModel model);
        TaskModel GetTask(long id);
        ResponseDeleteModel DeleteTask(int id);
        EntityModel GetTaskToEdit(long id);
        ResponseListModel<LocationEquipmentModel> GetEquipment(long id);
        string CheckIfAttachmentExists(int modelId, long attachmentId);
        ResponseListModel<FileModel> GetAttachments(long id);
        SaveFileModel UploadAttachment(FileModel model);
        ResponseListModel<long> DeleteAttachments(FormValuesModel model);
        ResponseListModel<TaskStatusModel> GetAvailableStatuses(int[] taskStatusIds);
        ResponseListModel<TaskModel> SaveStatuses(TaskStatusChangeModel model);
    }

    public class TasksService : BaseService, ITasksService
    {
        public TasksService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public GridModel GetTasks(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                var values = _dbClient.OpxTaskFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.TASKS.FilterTaskLog, "OpxTaskFilter");
                base.CheckSession(session);
                
                var result = new GridModel()
                {
                    Values = values.Select(task =>
                    {
                        return Common.DictionaryToObject(task.DynamicValues, task);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning tasks downloading.", e);
            }
        }
        public TaskModel GetTask(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opTask = _dbClient.OpxGetTask(ref session, (int)id);
                SetEndTime(Logging.EventID.TASKS.GetTaskLog, "OpxGetTask");
                base.CheckSession(session);

                if (opTask != null)
                {
                    return TaskModelFromOpTask(opTask, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return TaskModelFromOpTask(opTask, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning task " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + id + " downloading.", e);
            }
        }

        public ResponseDeleteModel DeleteTask(int id)
        {
            var session = new SESSION();
            var opTask = new OpTask();
            try
            {
                session = _sessionService.GetSession();
                SetStartTime();
                opTask = _dbClient.OpxGetTask(ref session, id);
                SetEndTime(Logging.EventID.TASKS.GetTaskLog, "OpxGetTask");
                base.CheckSession(session);
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning task " + id + " downloading.", e);
            }

            try
            {
                opTask.IdTaskStatus = (int)OpTaskStatus.Enum.Canceled;

                SetStartTime();
                var result = _dbClient.OpxSaveTask(ref session, opTask);
                SetEndTime(Logging.EventID.TASKS.SaveTaskLog, "OpxSaveTask");

                if (result != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " saving.", e);
            }
        }

        public EntityModel GetTaskToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opTask = _dbClient.OpxGetTask(ref session, (int)id);
                SetEndTime(Logging.EventID.TASKS.GetTaskLog, "OpxGetTask");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opTask.DataList.Select(u => FieldModel.FieldFromOpDataList(u)).ToList();
                fields.AddRange(AddTaskSpecialFieldsToFieldModel(opTask));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
            };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + id + " downloading.", e);
            }
        }

        public TaskModel SaveTask(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpTask opTask;
                if (model.Id != null)
                {
                    try
                    {
                        SetStartTime();
                        opTask = _dbClient.OpxGetTask(ref session, Convert.ToInt32(model.Id));
                        SetEndTime(Logging.EventID.TASKS.GetTaskLog, "OpxGetTask");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning task " + model.Id + " downloading.", e);
                    }
                    base.CheckSession(session);
                }
                else
                    opTask = new OpTask();

                if (opTask == null)
                    opTask = new OpTask();

                #region Fields
                FillDataList(opTask.DataList, model.Fields);
                #endregion

                SetStartTime();
                var savedTask = _dbClient.OpxSaveTask(ref session, opTask);
                SetEndTime(Logging.EventID.TASKS.SaveTaskLog, "OpxSaveTask");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedTask != null)
                {
                    return TaskModelFromOpTask(savedTask, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return TaskModelFromOpTask(savedTask, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning task " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + model.Id + " saving.", e);
            }
        }

        public string CheckIfAttachmentExists(int modelId, long attachmentId)
        {
            var session = _sessionService.GetSession();
            SetStartTime();
            var message = base.CheckIfExists(modelId, attachmentId, _dbClient.OpxGetTaskAttachment, session);
            SetEndTime(Logging.EventID.TASKS.GetTaskAttachmentLog, "OpxGetTaskAttachment");
            return message;
        }

        public ResponseListModel<FileModel> GetAttachments(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var attachments = _dbClient.OpxGetTaskAttachment(ref session, (int)id, null);
                SetEndTime(Logging.EventID.TASKS.GetTaskAttachmentLog, "OpxGetTaskAttachment");
                base.CheckSession(session);

                var result = attachments.Select(dh => FileModel.FileModelFromOpFile(dh)).ToList();
                return new ResponseListModel<FileModel>( result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + id + " attachments downloading.", e);
            }
        }

        public SaveFileModel UploadAttachment(FileModel model)
        {
            SetStartTime();
            var savedFileModel = base.UploadAttachment(model, _dbClient.OpxSaveTaskAttachment);
            SetEndTime(Logging.EventID.TASKS.SaveTaskAttachmentLog);
            return savedFileModel;
        }

        public ResponseListModel<long> DeleteAttachments(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                _dbClient.OpxDeleteTaskAttachment(ref session, (int)model.Id, model.Values.ToArray());
                SetEndTime(Logging.EventID.TASKS.DeleteTaskAttachmentLog, "OpxDeleteTaskAttachment");
                base.CheckSession(session);

                return new ResponseListModel<long>( model.Values, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + model.Id + " attachments deleting.", e);
            }
        }

        public ResponseListModel<LocationEquipmentModel> GetEquipment(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var equipment = _dbClient.OpxGetLocationEquipment(ref session, id, true);
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationEquipmentLog, "OpxGetLocationEquipment");

                base.CheckSession(session);

                var result = equipment.Select(dh =>
                {
                    return new LocationEquipmentModel()
                    {
                        Device = dh.Device != null ? dh.Device.ToString() : "",
                        Meter = dh.Meter != null ? dh.Meter.ToString() : "",
                        StartTime = dh.StartTime,
                        EndTime = dh.EndTime
                    };
                }).ToList();
                return new ResponseListModel<LocationEquipmentModel>( result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning task " + id + " equipment downloading.", e);
            }
        }

        public ResponseListModel<TaskStatusModel> GetAvailableStatuses(int[] taskStatusIds)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opStatuses = _dbClient.OpxGetTaskStatusChangeList(ref session, taskStatusIds);
                SetEndTime(Logging.EventID.TASKS.GetTaskStatusChangeListLog, "OpxGetTaskStatusChangeList");
                base.CheckSession(session);

                if (opStatuses == null)
                    throw new Exception(session.Error.Msg);

                return new ResponseListModel<TaskStatusModel>( opStatuses.Select(s => new TaskStatusModel(s)).ToList(), session);
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch(Exception e)
            {
                throw new Exception("Issue concerning task available statuses downloading", e);
            }
        }

        public ResponseListModel<TaskModel> SaveStatuses(TaskStatusChangeModel model)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var updatedTasks = _dbClient.OpxChangeTaskStatus(ref session, model.TaskIds.ToArray(), model.StatusId);
                SetEndTime(Logging.EventID.TASKS.ChangeTaskStatusLog, "OpxChangeTaskStatus");
                base.CheckSession(session);

                return new ResponseListModel<TaskModel>( updatedTasks.Select(ut => TaskModelFromOpTask(ut)).ToList());
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch(Exception e)
            {
                throw new Exception("Issue concerning saving statuses", e);
            }
        }

        public ImportStatusModel InitializeImport(ImportDataModel model)
        {
            throw new NotImplementedException();
        }

        public object StartImport(string guid)
        {
            throw new NotImplementedException();
        }

        public object StopImport(string guid)
        {
            throw new NotImplementedException();
        }

        public ImportProgressModel ImportProgress(string guid)
        {
            throw new NotImplementedException();
        }

        public ImportStatusModel ImportSummary(string guid)
        {
            throw new NotImplementedException();
        }

        private TaskModel TaskModelFromOpTask(OpTask opTask, bool replaceReferenceTypes = false, SESSION session = null)
        {
            TaskModel retTask;

            if (opTask != null)
            {
                var fields = opTask.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                retTask = new TaskModel()
                {
                    Id = opTask.IdTask,
                    Name = opTask.ToString(),
                    IdLocation = opTask.IdLocation,
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opTask.DynamicValues) : fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(retTask.Fields, opTask.DataList);
            }
            else
            {
                retTask = new TaskModel()
                {
                    Fields = new List<FieldModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return retTask;
        }

        private List<FieldModel> AddTaskSpecialFieldsToFieldModel(OpTask task)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_TASK, task.IdTask, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_TASK_GROUP, task.IdTaskGroup, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_PLANNED_ROUTE, task.IdPlannedRoute, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_OPERATOR_REGISTERING, task.IdOperatorRegistering, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_OPERATOR_PERFORMER, task.IdOperatorPerformer, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_TASK_STATUS, task.IdTaskStatus, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_CREATION_DATE, task.CreationDate, "Date", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ACCEPTED, task.Accepted, "Boolean", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_OPERATOR_ACCEPTED, task.IdOperatorAccepted, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ACCEPTANCE_DATE, task.AcceptanceDate, "Date", true));
            list.Add(PrepareFieldModel(DataType.HELPER_NOTES, task.Notes, "Text", true));
            list.Add(PrepareFieldModel(DataType.HELPER_DEADLINE, task.Deadline, "Date", true));
            list.Add(PrepareFieldModel(DataType.HELPER_TOPIC_NUMBER, task.TopicNumber, "Text", true));
            list.Add(PrepareFieldModel(DataType.HELPER_PRIORITY, task.Priority, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_OPERATION_CODE, task.OperationCode, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_TASK_TYPE, task.IdTaskType, "Integer", true));

            return list;
        }
    }
}
