﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static IMR.Suite.Common.Enums;

namespace SIMAX.Services
{
    public interface IImportService
    {
        ImportStatusModel InitializeImport(ImportDataModel model);
        object StartImport(string guid);
        object StopImport(string guid);
        ImportProgressModel ImportProgress(string guid);
        ImportStatusModel ImportSummary(string guid);
    }

    public class BaseService : TimeLogsService
    {
        private static string ACTIVE_VIEWS = "ACTIVE_VIEWS";
        private static string ACTIVE_VIEWS_OPERATOR_ID = "ACTIVE_VIEWS_OPERATOR_ID";

        protected IDBCollector _dbClient;
        protected ISessionService _sessionService;
        protected readonly IHttpContextAccessor _accessor;

        public BaseService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService)
        {
            _accessor = httpContextAccessor;
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public void CheckSession(SESSION session)
        {
            if (session.Status == SESSION_STATUS.SESSION_NOT_VALID)
            {
                _accessor.HttpContext.Session.Clear();
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized);
            }
        }

        protected List<FieldModel> ReplaceReferenceFields(List<FieldModel> fields, Dictionary<string, object> dict)
        {
            var newFields = new List<FieldModel>(fields);

            if (dict != null)
                foreach (KeyValuePair<string, object> kvp in dict)
                {
                    var field = newFields.FirstOrDefault(f => f.Id == OpViewColumn.GetColumnIdDataType(kvp.Key));
                    if (field != null)
                    {
                        if (kvp.Value is DateTime) continue;
                        field.ValueReferenceId = field.Value;
                        field.Value = kvp.Value != null ? kvp.Value.ToString() : string.Empty;
                    }
                }

            return newFields;
        }

        protected void ReplaceReferenceIds<T>(List<FieldModel> fields, OpDataList<T> dataList) where T : IOpData, IOpDataProvider, new()
        {
            fields.ForEach(f =>
            {
                dataList.ForEach(d =>
                {
                    if (f.Id == d.IdDataType)
                    {
                        f.ValueReferenceId = d.Value;
                        f.Index = d.Index;
                    }
                });
            });
        }

        protected FieldModel PrepareFieldModel(long id, object value, string type, bool isEditable)
        {
            int typeId;
            switch (type)
            {
                case "Integer":
                    typeId = 1;
                    break;
                case "Real":
                    typeId = 2;
                    break;
                case "Date":
                    typeId = 6;
                    break;
                case "Boolean":
                    typeId = 7;
                    break;
                case "Text":
                default:
                    typeId = 3;
                    break;
            }

            return new FieldModel()
            {
                Id = id,
                Value = value,
                Type = type,
                TypeId = typeId,
                IsEditable = isEditable
            };
        }

        protected GridModel RolesToModel(object[] roles)
        {
            int totalCount;
            var gridModel = new GridModel();

            if (roles != null)
            {
                totalCount = roles.Count();

                gridModel.TotalCount = totalCount;
                gridModel.Values = roles.Select(r =>
                {
                    var role = r as OpRole;

                    var dictionary = new Dictionary<string, object>();
                    dictionary.Add("Name", role.Name);
                    dictionary.Add("Description", role.Descr != null ? role.Descr.Description : "");
                    dictionary.Add("ModuleName", role.Module != null ? role.Module.Name : "");
                    dictionary.Add("ValueString", role.ToString());
                    dictionary.Add("Id", role.IdRole);

                    return Common.DictionaryToObject(dictionary);
                }).ToList();
            }

            return gridModel;
        }

        protected GridModel ActivitiesToModel(object[] activities)
        {
            int totalCount;
            var gridModel = new GridModel();

            if (activities != null)
            {
                totalCount = activities.Count();

                gridModel.TotalCount = totalCount;
                gridModel.Values = activities.Select(a =>
                {
                    var activity = a as OpActivity;

                    var dictionary = new Dictionary<string, object>();
                    dictionary.Add("Description", activity.Descr != null ? activity.Descr.Description : activity.Name);
                    dictionary.Add("ReferenceType", activity.ReferenceType != null ? activity.ReferenceType.Name : "");
                    dictionary.Add("ReferenceObject", activity.ReferenceValue != null ? activity.ReferenceValue.ToString() : "");
                    dictionary.Add("Allow", activity.Allow);
                    dictionary.Add("ReferenceTypeId", activity.IdReferenceType);
                    dictionary.Add("ModuleId", activity.IdModule);
                    dictionary.Add("ValueString", activity.ToString());
                    dictionary.Add("Id", activity.IdActivity);
                    dictionary.Add("IdOperatorActivity", activity.IdOperatorActivity);

                    return Common.DictionaryToObject(dictionary, false);
                }).ToList();
            }

            return gridModel;
        }

        protected void FilterByIds(GridRequestModel req, ref ColumnInfo[] columns, Enums.ReferenceType reference)
        {
            if (req.Ids.Count > 0)
            {
                var activeViews = GetActiveViews();
                var view = activeViews.FirstOrDefault(v => v.IdReferenceType == (int)reference);
                if (view != null)
                {
                    var locationColumnId = view.PrimaryKeyColumn;
                    var where = "";
                    var columnExist = columns.FirstOrDefault(c => c.IdDataType == locationColumnId.IdDataType);
                    if (columnExist == null)
                    {
                        var col = new ColumnInfo() { SortType = SortType.None, IdDataType = locationColumnId.IdDataType, Name = locationColumnId.Name };
                        where = String.Format("[{0}] in ({1})", locationColumnId.Name, string.Join(",", req.Ids));
                        col.Filter = new string[] { where };
                        var columnsList = columns.ToList();
                        columnsList.Add(col);
                        columns = columnsList.ToArray();
                    }
                    else
                    {
                        where = String.Format("[{0}] in ({1})", columnExist.Name, string.Join(",", req.Ids));
                        columnExist.Filter = new string[] { where };
                    }
                    if (string.IsNullOrEmpty(req.Where))
                    {
                        req.Where = where;
                    }
                    else
                    {
                        req.Where += " and " + where;
                    }
                }
            }
        }

        protected void FillDataList<T>(OpDataList<T> dataList, List<FieldNameValueModel> fields) where T : IOpData, IOpDataProvider, new()
        {
            if (fields != null && fields.Count > 0)
            {
                foreach (var field in fields)
                {
                    if (field.Value is string && field.Value as string == "")
                        field.Value = null;
                    dataList.SetValue(field.NameId, field.Value);
                }
            }
        }

        protected void AddEntitiesBinding<T>(List<long> indexes, ref List<T> entities) where T : IOpChangeState, new()
        {
            if (indexes != null)
            {
                // delete
                entities.ForEach(e =>
                {
                    var entity = e as dynamic;
                    if (!indexes.Exists(i => i == entity.IdObject))
                        entity.OpState = OpChangeState.Delete;
                });

                // add new
                if (indexes.Count > 0)
                {
                    foreach (var i in indexes)
                    {
                        if (!entities.Exists(e => {
                            var entity = e as dynamic;
                            return entity.IdObject == i;
                        }))
                        {
                            dynamic newEntity = new T();
                            newEntity.OpState = OpChangeState.New;
                            newEntity.IdObject = i;
                            entities.Add(newEntity);
                        }
                    }
                }
            }
        }

        protected string CheckIfExists(int modelId, long attachmentId, GetAttachment getAttachment, SESSION session)
        {
            try
            {
                var attachment = getAttachment(ref session, modelId, new long[] { attachmentId }).FirstOrDefault();
                if (attachment.FileBytes == null) return Resources.Common.Common.FileDoesNotExist;
                return session.Error != null ? session.Error.Msg : session.Status.ToString();
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning attachment " + attachmentId + " downloading.", e);
            }

        }

        protected OpFile DownloadAttachment(string jwt, int modelId, long attachmentId, GetAttachment getAttachment)
        {
            try
            {
                var session = _sessionService.GetSession(jwt);
                SetStartTime();
                var attachment = getAttachment(ref session, modelId, new long[] { attachmentId }).FirstOrDefault();
                CheckSession(session);

                return attachment;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning attachment " + attachmentId + " downloading.", e);
            }
        }

        protected SaveFileModel UploadAttachment(FileModel model, SaveAttachment saveAttachment)
        {
            try
            {
                var session = _sessionService.GetSession();

                byte[] bytes = Convert.FromBase64String(model.FileBytes.Split(',')[1]);

                var newAttachment = new OpFile()
                {
                    PhysicalFileName = model.PhysicalFileName,
                    FileBytes = bytes,
                    Description = model.Description,
                    Version = null,
                    InsertDate = DateTime.Now,
                    Size = model.FileBytes.ToString().Length
                };

                var opFile = saveAttachment(ref session, model.ParentEntityId, newAttachment);
                CheckSession(session);
                var saveFileModel = new SaveFileModel();
                if (opFile != null)
                    saveFileModel.FileModel = FileModel.FileModelFromOpFile(opFile);
                if (session.Error != null)
                    saveFileModel.Status = session.Error.Msg;

                saveFileModel.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;

                return saveFileModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning attachment: " + model.PhysicalFileName + " saving.", e);
            }
        }

        protected object GetContextListObjects(ContextListModel contextListModel)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var contextList = _dbClient.GetContextList(ref session, (Enums.ReferenceType)contextListModel.sourceType, (Enums.ReferenceType)contextListModel.destinationType, contextListModel.referenceValues);
                SetEndTime(Logging.EventID.COMMON.GetContextListLog, "GetContextList");
                CheckSession(session);
                return contextList;
            }
            catch (StatusCodeException e)
            {
                throw new Exception("Cannot download contextList", e);
            }
        }

        protected bool HasPropertyOrField(System.Type obj, string name)
        {
            return obj.GetProperty(name) != null || obj.GetField(name) != null;
        }

        protected OpView[] GetActiveViews()
        {
            var activeViewsString = _accessor.HttpContext.Session.GetString(ACTIVE_VIEWS);
            var operatorIdString = _accessor.HttpContext.Session.GetString(ACTIVE_VIEWS_OPERATOR_ID);

            var actualOperatorId = _sessionService.GetOperatorId();

            if (activeViewsString == null || operatorIdString == null || (operatorIdString != null && JsonConvert.DeserializeObject<int>(operatorIdString) != actualOperatorId))
            {
                var session = _sessionService.GetSession();

                SetStartTime();
                var activeViews = _dbClient.OpxGetActiveView(ref session, null);
                SetEndTime(Logging.EventID.DICTIONARY.GetActiveViewsLog, "OpxGetActiveView");

                if (session.Error != null)
                {
                    IMRLog.AddToLog(Logging.EventID.ERRORS.ActiveViewSessionError, new object[] { session.Error.Msg });
                }

                if (activeViews != null) activeViews = activeViews.Where(view => view != null).ToArray();

                var serialized = JsonConvert.SerializeObject(activeViews, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    NullValueHandling = NullValueHandling.Ignore
                    
                });
                _accessor.HttpContext.Session.SetString(ACTIVE_VIEWS, serialized);
                _accessor.HttpContext.Session.SetString(ACTIVE_VIEWS_OPERATOR_ID, JsonConvert.SerializeObject(actualOperatorId));

                return activeViews;
            } else
            {
                var activeViews = JsonConvert.DeserializeObject<OpView[]>(activeViewsString, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    NullValueHandling = NullValueHandling.Ignore

                });
                return activeViews;
            }
        }

        protected byte[] ExportGrid(string jwt, string token, string REQUEST_MODEL, ExportMethod exportMethod)
        {
            try
            {
                var savedReq = _accessor.HttpContext.Session.GetString(REQUEST_MODEL);
                var deserializedSavedReq = JsonConvert.DeserializeObject<SavedRequestModel>(savedReq);
                var req = deserializedSavedReq.GridRequestModel;
                var bytes = new byte[0];
                if (token == deserializedSavedReq.Token)
                {
                    var session = _sessionService.GetSession(jwt);
                    var columns = ListColumnModelToColumnInfoArray(req.Columns);
                    SetStartTime();
                    bytes = exportMethod(ref session, columns, req.Where, null);
                    CheckSession(session);
                    if(session.Status == SESSION_STATUS.ERROR)
                    {
                        if ((ERROR_CODE)session.Error.Code == ERROR_CODE.InsufficientPermissions)
                            throw new StatusCodeException((int)HttpStatusCode.ExpectationFailed, "No permissions to export file");
                    }
                }
                return bytes;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concering downloading excel file", e);
            }
        }

        protected ImportStatusModel InitializeImport(ImportDataModel model, OpxImportInitialize initialize, OpxImportValidateData validateData, LogData initializeLogData, LogData validateLogData, string moduleName)
        {
            var session = _sessionService.GetSession();
            var importData = new OpImportData();
            importData.Headers = model.Headers;
            importData.Data = model.Data;

            SetStartTime();
            var guid = initialize(ref session, importData, null);
            SetEndTime(initializeLogData, "OpxImport" + moduleName + "Initialize");
            CheckSession(session);

            if (session.Error != null)
                throw new StatusCodeException((int)HttpStatusCode.ExpectationFailed);

            SetStartTime();
            var summary = validateData(ref session, guid);
            SetEndTime(validateLogData, "OpxImport" + moduleName + "ValidateData");
            CheckSession(session);

            var statusModel = new ImportStatusModel();
            statusModel.Guid = guid;

            foreach (var itemSummary in summary)
            {
                statusModel.Statuses.Add(new SingleStatusModel(itemSummary));
            }
            statusModel.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;

            return statusModel;
        }

        protected object StartImport(string guid, OpxImportStart importStart)
        {
            var session = _sessionService.GetSession();

            var status = importStart(ref session, guid);
            CheckSession(session);

            return status;
        }
        
        protected object StopImport(string guid, OpxImportStop importStop)
        {
            var session = _sessionService.GetSession();

            importStop(ref session, guid);
            CheckSession(session);

            return guid;
        }
        
        protected ImportProgressModel ImportProgress(string guid, OpxImportProgress importProgress)
        {
            var session = _sessionService.GetSession();

            var progress = importProgress(ref session, guid);
            CheckSession(session);

            return new ImportProgressModel(progress, session);
        }
        
        protected ImportStatusModel ImportSummary(string guid, OpxImportSummary importSummary)
        {
            var session = _sessionService.GetSession();

            var summary = importSummary(ref session, guid);
            CheckSession(session);

            var statusModel = new ImportStatusModel();
            statusModel.Guid = guid;

            foreach (var itemSummary in summary)
            {
                statusModel.Statuses.Add(new SingleStatusModel(itemSummary));
            }
            statusModel.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;

            return statusModel;
        }


        protected ResponseListModel<SchemaModel> GetSchemas<T>(long id, GetSchemasMethod method, GetEntityMethod<T> enityMethod, LogData schemasLogData, LogData entityLogData, string moduleName) where T : IOpDynamic, IOpObject<OpData>

        {
            try
            {
                var session = _sessionService.GetSession();

                SetStartTime();
                var schemas = method(ref session, id);
                SetEndTime(schemasLogData, "OpxGet" + moduleName);

                CheckSession(session);

                SetStartTime();
                var entity = enityMethod(ref session, id);
                SetEndTime(entityLogData, "OpxGet" + moduleName + "Schema");

                CheckSession(session);

                var result = schemas.Select(schema => SchemaModelFromOpSchema(schema, entity)).ToList();

                return new ResponseListModel<SchemaModel>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning schemas downloading.", e);
            }
        }

        protected SchemaModel SchemaModelFromOpSchema<T>(OpSchema schema, T entity, SESSION session = null) where T : IOpDynamic, IOpObject<OpData>
        {
            return new SchemaModel()
            {
                Schema = schema.Schema,
                SchemaConfiguration = schema.SchemaConfiguration != null ? schema.SchemaConfiguration.Select(sc => {

                    object value = null;
                    if (entity != null)
                    {
                        if (!String.IsNullOrEmpty(sc.Name))
                        {
                            try
                            {
                                value = entity.DynamicValues[sc.Name];
                            }
                            catch (Exception)
                            {

                            }
                        }

                        if (value == null)
                        {
                            var field = entity.DataList.Find(l => l.IdDataType == sc.IdDataType);
                            value = field != null ? field.Value : string.Empty;
                        }
                    }

                    if (value == null || (value is string && string.IsNullOrEmpty((string)value)))
                    {
                        value = sc.Value;
                    }

                    string description = null;
                    if (sc.DescriptionVisible)
                        description = sc.Description;
                    
                    return new SchemaElement()
                    {
                        X = sc.X,
                        Y = sc.Y,
                        Width = sc.Width,
                        Height = sc.Height,
                        Description = description,
                        IdDataType = sc.IdDataType,
                        IsUnitVisible = sc.UnitVisible,
                        Type = sc.DataType != null ? sc.DataType.IdDataTypeClass : 0,
                        Value = value,
                        IsEditable = sc.IsEditable,
                        ElementsApperance = sc.SchemaElementApperance != null ? sc.SchemaElementApperance.Select(sea => new SchemaElementApperance()
                        {
                            Color = sea.Color.HasValue ? Common.ColorToHEX(sea.Color.Value) : null,
                            Image = sea.Image,
                            Condition = sea.Condition,
                            Constraint1 = sea.Constraint1,
                            Constraint2 = sea.Constraint2
                        }).ToList() : null
                    };
                }).ToList() : null,
                HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
            };
        }

        protected delegate OpSchema[] GetSchemasMethod(ref SESSION session, long id);
        protected delegate T GetEntityMethod<T>(ref SESSION session, long id) where T : IOpDynamic, IOpObject<OpData>;

        protected delegate string OpxImportInitialize(ref SESSION session, object importData, string dllName);
        protected delegate OpImportItemSummary[] OpxImportValidateData(ref SESSION session, string guid);
        protected delegate ImportStatus OpxImportStart(ref SESSION session, string guid);
        protected delegate void OpxImportStop(ref SESSION session, string guid);
        protected delegate OpImportProgressStep OpxImportProgress(ref SESSION session, string guid);
        protected delegate OpImportItemSummary[] OpxImportSummary(ref SESSION session, string guid);
        
        protected ColumnInfo[] ListColumnModelToColumnInfoArray(List<ColumnModel> list)
        {
            return list.Select(column => new ColumnInfo() { SortType = column.SortDesc.HasValue ? column.SortDesc.Value ? SortType.Desc : SortType.Asc : SortType.None, IdDataType = column.Id, Filter = column.Where, Name = column.Name.ToUpper() }).ToArray();
        }

        protected delegate byte[] ExportMethod(ref SESSION session, ColumnInfo[] columns, string CustomWhereClause, string dllName);
        protected delegate OpFile SaveAttachment(ref SESSION session, int modelId, OpFile attachment);
        protected delegate OpFile[] GetAttachment(ref SESSION session, int modelId, long[] attachmentId);
        protected delegate T[] OpxFilter<T>(ref SESSION session, out long TotalCount, ColumnInfo[] Columns, int PageNbr, int PageSize, string CustomWhereClause);
    }

}
