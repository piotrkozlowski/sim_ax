﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace SIMAX.Services
{
    public interface ISimcardsService : IImportService
    {
        GridModel GetSimcards(GridRequestModel req);
        EntityModel SaveSimcard(FormValuesModel req);
        EntityModel GetSimcard(long id);
        EntityModel GetSimcardToEdit(long id);
        ResponseDeleteModel DeleteSimcard(long id);
    }
    public class SimcardsService : BaseService, ISimcardsService
    {
        public SimcardsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)        
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetSimcards(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();                
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpSimCard[] opSimCards = _dbClient.OpxSimCardFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.ROUTES.FilterRoutesLog, "OpxSimCardFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opSimCards.Select(sim =>
                    {
                        return Common.DictionaryToObject(sim.DynamicValues, sim.ToString(), sim.IdSimCard);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel GetSimcard(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opSimCard = _dbClient.OpxGetSimCard(ref session, (int)id);
                base.CheckSession(session);

                if (opSimCard != null)
                {
                    return SimcardModelFromOpSimCard(opSimCard, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return SimcardModelFromOpSimCard(opSimCard, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning simcard " + id + " downloading.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResponseDeleteModel DeleteSimcard(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opSimCard = _dbClient.OpxGetSimCard(ref session, (int)id);
                SetEndTime(Logging.EventID.SIMCARDS.GetSimcardLog, "OpxGetSimCard");
                base.CheckSession(session);

                SetStartTime();               
                _dbClient.OpxDeleteSimCard(ref session, opSimCard);
                SetEndTime(Logging.EventID.SIMCARDS.DeleteSimcardLog, "OpxDeleteSimCard");
                                
                return new ResponseDeleteModel(true, session);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel GetSimcardToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();              
                var opSimCard = _dbClient.OpxGetSimCard(ref session, (int)id);
                SetEndTime(Logging.EventID.SIMCARDS.GetSimcardLog, "OpxGetSimCard");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opSimCard.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddSimcardSpecialFieldsToFieldModel(opSimCard));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel SaveSimcard(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpSimCard opSimCard;
                int? simCardId = null;
                var fieldId = model.Fields.Find(f => f.NameId == DataType.HELPER_ID_SIM_CARD);
                if (fieldId != null && !String.IsNullOrEmpty(fieldId.Value.ToString()))
                {
                    simCardId = Convert.ToInt32(fieldId.Value);
                }
                else if (model.Id != null)
                {
                    simCardId = Convert.ToInt32(model.Id);
                }

                if (simCardId != null)
                {
                    try
                    {
                        SetStartTime();
                        opSimCard = _dbClient.OpxGetSimCard(ref session, Convert.ToInt32(simCardId));
                        SetEndTime(Logging.EventID.SIMCARDS.GetSimcardLog, "OpxGetSimCard");
                        base.CheckSession(session);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning simcard " + simCardId + " downloading.", e);
                    }
                }
                else
                    opSimCard = new OpSimCard();

                if (opSimCard == null)
                    opSimCard = new OpSimCard();

                #region Fields
                FillDataList(opSimCard.DataList, model.Fields);
                #endregion

                SetStartTime();
                OpSimCard savedSimcard = _dbClient.OpxSaveSimCard(ref session, opSimCard);
                SetEndTime(Logging.EventID.SIMCARDS.SaveSimcardLog, "OpxSaveSimCard");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedSimcard != null)
                {
                    return SimcardModelFromOpSimCard(savedSimcard, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return SimcardModelFromOpSimCard(savedSimcard, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning simcard " + model.Id + " saving.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ImportStatusModel InitializeImport(ImportDataModel model)
        {
            try
            {
                var initalizeLogData = Logging.EventID.SIMCARDS.ImportSimcardInitializeLog;
                var validateLogData = Logging.EventID.SIMCARDS.ImportSimcardValidateLog;
                return base.InitializeImport(model, _dbClient.OpxImportSimCardInitialize, _dbClient.OpxImportSimCardValidateData, initalizeLogData, validateLogData, "Simcards");
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning simcard initializing import", e);
            }
        }

        public object StartImport(string guid)
        {
            try
            {
                SetStartTime();
                var status = base.StartImport(guid, _dbClient.OpxImportSimCardStartImport);
                SetEndTime(Logging.EventID.SIMCARDS.ImportSimcardStartImport, "OpxImportSimCardStartImport");
                return status;

            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning simcard starting import", e);
            }
        }

        public object StopImport(string guid)
        {
            try
            {
                SetStartTime();
                var result = base.StopImport(guid, _dbClient.OpxImportSimCardStopImport);
                SetEndTime(Logging.EventID.SIMCARDS.ImportSimcardStopImport, "OpxImportSimCardStopImport");
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning simcard stopping import", e);
            }
        }

        public ImportProgressModel ImportProgress(string guid)
        {
            try
            {
                SetStartTime();
                var importProgress = base.ImportProgress(guid, _dbClient.OpxImportSimCardProgress);
                SetEndTime(Logging.EventID.SIMCARDS.ImportSimcardProgess, "OpxImportSimCardProgress");
                return importProgress;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning simcard progress import", e);
            }
        }

        public ImportStatusModel ImportSummary(string guid)
        {
            try
            {
                SetStartTime();
                var statusModel = base.ImportSummary(guid, _dbClient.OpxImportSimCardSummary);
                SetEndTime(Logging.EventID.SIMCARDS.ImportSimcardSummary, "OpxImportSimCardSummary");
                return statusModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location initializing import", e);
            }
        }

        private EntityModel SimcardModelFromOpSimCard(OpSimCard opSimCard, bool replaceReferenceTypes = false, SESSION session = null)
        {
            EntityModel model;

            if (opSimCard != null)
            {
                var fields = opSimCard.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                model = new EntityModel();

                model.Id = opSimCard.IdSimCard;
                model.Name = opSimCard.ToString();
                model.Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opSimCard.DynamicValues) : fields;
                //model.BasicFieldIds = DataType.GetHelperDataTypes(Enums.ReferenceType.IdSimCard, true);           
                model.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;

                ReplaceReferenceIds(model.Fields, opSimCard.DataList);
            }
            else
            {
                model = new EntityModel()
                {
                    Fields = new List<FieldModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return model;
        }

        private List<FieldModel> AddSimcardSpecialFieldsToFieldModel(OpSimCard sim)
        {
            var list = new List<FieldModel>();
            //list.Add(PrepareFieldModel(DataType.SIM_CARD_TARIFF, sim.SimCardTariff, "Integer", false)); //need add for tarif refTypeId = 43   
            //list.Add(PrepareFieldModel(DataType.SIM_CARD_TARIFF, sim.OperatorTariff, "Integer", false)); //need add for tarif refTypeId = 43         

            return list;
        }
    }
}