﻿using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace SIMAX.Services
{
    public interface IIssuesService
    {
        GridModel GetIssues(GridRequestModel req);
        IssueModel SaveIssue(FormValuesModel model);
        IssueModel GetIssue(long id);
        EntityModel GetIssueToEdit(long id);
        ResponseDeleteModel DeleteIssue(int id);
        ResponseListModel<LocationEquipmentModel> GetEquipment(long id);
        string CheckIfAttachmentExists(int modelId, long attachmentId);
        ResponseListModel<FileModel> GetAttachments(long id);
        SaveFileModel UploadAttachment(FileModel model);
        ResponseListModel<long> DeleteAttachments(FormValuesModel models);
    }

    public class IssuesService : BaseService, IIssuesService
    {
        public IssuesService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetIssues(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                var values = _dbClient.OpxIssueFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.ISSUES.FilterIssueLog);
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = values.Select(issue =>
                    {
                        return Common.DictionaryToObject(issue.DynamicValues, issue.ToString(), issue.IdIssue);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issues downloading.", e);
            }
        }

        public IssueModel GetIssue(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opIssue = _dbClient.OpxGetIssue(ref session, (int)id);
                SetEndTime(Logging.EventID.ISSUES.GetIssueLog, "OpxGetIssue");
                base.CheckSession(session);

                if (opIssue != null)
                {
                    return IssueModelFromOpIssue(opIssue, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return IssueModelFromOpIssue(opIssue, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Problem concerning issue " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id +  " downloading.", e);
            }
        }

        public EntityModel GetIssueToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opIssue = _dbClient.OpxGetIssue(ref session, (int)id);
                SetEndTime(Logging.EventID.ISSUES.GetIssueLog, "OpxGetIssue");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opIssue.DataList.Select(i => FieldModel.FieldFromOpDataList(i)).ToList();
                fields.AddRange(AddIssueSpecialFieldsToFieldModel(opIssue));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " downloading.", e);
            }
        }

        public IssueModel SaveIssue(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpIssue opIssue = null;
                if (model.Id != null)
                {
                    try
                    {
                        SetStartTime();
                        opIssue = _dbClient.OpxGetIssue(ref session, Convert.ToInt32(model.Id));
                        SetEndTime(Logging.EventID.ISSUES.GetIssueLog, "OpxGetIssue");
                        base.CheckSession(session);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Problem concerning issue " + model.Id + " downloading.", e);
                    }
                }

                if (opIssue == null)
                    opIssue = new OpIssue();

                AddEntitiesBinding(model.Values, ref opIssue.TaskList);

                #region Fields
                FillDataList(opIssue.DataList, model.Fields);
                #endregion

                var priority = model.Fields.FirstOrDefault(f => f.NameId == DataType.HELPER_PRIORITY);
                if (priority != null && (priority.Value == null || string.IsNullOrEmpty(priority.Value.ToString())))
                {
                    opIssue.DataList.SetValue(DataType.HELPER_PRIORITY, 1);
                }
                SetStartTime();
                var savedIssue = _dbClient.OpxSaveIssue(ref session, opIssue);
                SetEndTime(Logging.EventID.ISSUES.SaveIssueLog, "OpxSaveIssue");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedIssue != null)
                {
                    return IssueModelFromOpIssue(savedIssue, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return IssueModelFromOpIssue(savedIssue, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Problem concerning issue " + model.Id + " saving.");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + model.Id + " saving.", e);
            }
        }

        public ResponseDeleteModel DeleteIssue(int id)
        {
            var session = new SESSION();
            var opIssue = new OpIssue();
            try
            {
                session = _sessionService.GetSession();
                SetStartTime();
                opIssue = _dbClient.OpxGetIssue(ref session, id);
                SetEndTime(Logging.EventID.ISSUES.GetIssueLog, "OpxGetIssue");
                base.CheckSession(session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " downloading.", e);
            }

            try
            { 
            opIssue.IdIssueStatus = (int)OpIssueStatus.Enum.CANCELLED;
                SetStartTime();
                var result = _dbClient.OpxSaveIssue(ref session, opIssue);
                SetEndTime(Logging.EventID.ISSUES.SaveIssueLog, "OpxSaveIssue");

                if (result != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " saving.", e);
            }
        }

        public string CheckIfAttachmentExists(int modelId, long attachmentId)
        {
            var session = _sessionService.GetSession();
            return base.CheckIfExists(modelId, attachmentId, _dbClient.OpxGetIssueAttachment, session);
        }

        public ResponseListModel<FileModel> GetAttachments(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var attachments = _dbClient.OpxGetIssueAttachment(ref session, (int)id, null);
                SetEndTime(Logging.EventID.ISSUES.GetIssueAttachmentLog, "OpxGetIssueAttachment");
                base.CheckSession(session);

                var result = attachments.Select(dh =>
                {
                    return FileModel.FileModelFromOpFile(dh);
                }).ToList();
                return new ResponseListModel<FileModel>( result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " attachments downloading.", e);
            }
        }

        public SaveFileModel UploadAttachment(FileModel model)
        {
            SetStartTime();
            var savedFileModel = base.UploadAttachment(model, _dbClient.OpxSaveIssueAttachment);
            SetEndTime(Logging.EventID.ISSUES.SaveIssueAttachmentLog, "OpxSaveIssueAttachment");
            return savedFileModel;
        }

        public ResponseListModel<long> DeleteAttachments(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                _dbClient.OpxDeleteIssueAttachment(ref session, (int)model.Id, model.Values.ToArray());
                SetEndTime(Logging.EventID.ISSUES.DeleteIssueAttachmentLog, "OpxDeleteIssueAttachment");
                base.CheckSession(session);

                return new ResponseListModel<long>( model.Values, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + model.Id + " attachments deleting.", e);
            }
        }

        public ResponseListModel<LocationEquipmentModel> GetEquipment(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var equipment = _dbClient.OpxGetLocationEquipment(ref session, id, true);
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationEquipmentLog, "OpxGetLocationEquipment");

                base.CheckSession(session);

                var result = equipment.Select(dh =>
                {
                    return new LocationEquipmentModel()
                    {
                        Device = dh.Device != null ? dh.Device.ToString() : "",
                        Meter = dh.Meter != null ? dh.Meter.ToString() : "",
                        StartTime = dh.StartTime,
                        EndTime = dh.EndTime
                    };
                }).ToList();
                return new ResponseListModel<LocationEquipmentModel>( result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Problem concerning issue " + id + " equipment downloading.", e);
            }
        }

        private IssueModel IssueModelFromOpIssue(OpIssue opIssue, bool replaceReferenceTypes = false, SESSION session = null)
        {
            IssueModel retIssue;

            if (opIssue != null)
            {
                var fields = opIssue.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                retIssue = new IssueModel()
                {
                    Id = opIssue.IdIssue,
                    Name = opIssue.ToString(),
                    IdLocation = opIssue.IdLocation,
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opIssue.DynamicValues) : fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                if (opIssue.IssueHistory != null)
                {
                    retIssue.History = new List<HistoryIssueModel>();
                    opIssue.IssueHistory.ForEach(data =>
                    {
                        var field = new HistoryIssueModel()
                        {
                            Id = data.IdIssueHistory,
                            IdIssue = data.IdIssue,
                            IdIssueStatus = data.IdIssueStatus,
                            IssueStatusDesc = data.IssueStatus != null ? data.IssueStatus.Description : "",
                            IdOperator = data.IdOperator,
                            OperatorDesc = data.Operator != null ? data.Operator.Description : "",
                            IdPriority = data.IdPriority,
                            PriorityDesc = data.Priority != null ? data.Priority.Descr.Description : "",
                            StartDate = data.StartDate,
                            EndDate = data.EndDate,
                            Deadline = data.Deadline,
                            Notes = data.Notes,
                            ShortDescr = data.ShortDescr
                        };
                        retIssue.History.Add(field);
                    });
                }

                if (opIssue.TaskList != null)
                {
                    retIssue.Tasks = new List<TaskModel>();
                    retIssue.Tasks = opIssue.TaskList.Select(data =>
                        new TaskModel()
                        {
                            TaskString = data != null ? data.ToString() : "",
                            Fields = data.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList()

                        }).ToList();
                }

                ReplaceReferenceIds(retIssue.Fields, opIssue.DataList);
            }
            else
            {
                retIssue = new IssueModel()
                {
                    Fields = new List<FieldModel>(),
                    History = new List<HistoryIssueModel>(),
                    Tasks = new List<TaskModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return retIssue;
        }

        private List<FieldModel> AddIssueSpecialFieldsToFieldModel(OpIssue issue)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ISSUE, issue.IdIssue, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ISSUE_STATUS, issue.IdIssueStatus, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ISSUE_TYPE, issue.IdIssueType, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, issue.IdDistributor, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_REALIZATION_DATE, issue.RealizationDate, "Date", true));
            list.Add(PrepareFieldModel(DataType.HELPER_SHORT_DESCR, issue.ShortDescr, "Text", true));
            list.Add(PrepareFieldModel(DataType.HELPER_LONG_DESCR, issue.LongDescr, "Text", true));

            return list;
        }
    }
}
