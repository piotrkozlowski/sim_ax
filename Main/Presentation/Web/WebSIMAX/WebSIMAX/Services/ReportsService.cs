﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Utils;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;
using System.Net;
using static IMR.Suite.Common.Enums;
using Microsoft.AspNetCore.Http;
using IMR.Suite.UI.Business.Objects;

namespace SIMAX.Services
{
    public interface IReportsService
    {
        GridModel GetReports(GridRequestModel req);
        ReportModel GetReport(int id);        
        ReportModel SaveReport(FormValuesModel req);
        EntityModel GetReportToEdit(int id);
        ResponseDeleteModel DeleteReport(int id);
        GeneratedReportModel GenerateReport(int id);
    }

    public class ReportsService : BaseService, IReportsService
    {
        public ReportsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetReports(GridRequestModel req)
        {
            try
            {               
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpReport[] opReports = _dbClient.OpxReportFilter(ref session, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.REPORTS.FilterReportsLog, "OpxReportFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opReports.Select(report =>
                    {
                        return Common.DictionaryToObject(report.DynamicValues, report.ToString(), report.IdReport);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ReportModel GetReport(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opReport = _dbClient.OpxGetReport(ref session, (int)id);
                SetEndTime(Logging.EventID.REPORTS.GetReportLog, "OpxGetReport");
                base.CheckSession(session);

                return ReportModelFromOpReport(opReport, true);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning report " + id + " downloading.", e);
            }
        }       

        public ReportModel SaveReport(FormValuesModel model)
        {            
            try
            {
                var session = _sessionService.GetSession();

                OpReport opReport = new OpReport();
                if (model.Id.HasValue)
                {
                    opReport.IdReport = (int)model.Id.Value;
                }

                #region Fields
                if (model.Fields.Count > 0)
                {
                    foreach (var field in model.Fields)
                    {
                        if (field.Value is string && field.Value as string == "")
                            field.Value = null;
                        opReport.DataList.SetValue(field.NameId, field.Value);
                    }
                }
                #endregion
                
                SetStartTime();
                OpReport savedReport = _dbClient.OpxSaveReport(ref session, opReport);
                SetEndTime(Logging.EventID.REPORTS.SaveReportLog, "OpxSaveReport");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedReport != null)
                {
                    return ReportModelFromOpReport(savedReport, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return ReportModelFromOpReport(savedReport, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning report " + model.Id + " saving.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel GetReportToEdit(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opReport = _dbClient.OpxGetReport(ref session, id);
                SetEndTime(Logging.EventID.ROUTES.GetRoutesLog, "OpxGetReport");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opReport.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddReportSpecialFieldsToFieldModel(opReport));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResponseDeleteModel DeleteReport(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opReport = _dbClient.OpxGetReport(ref session, id);
                SetEndTime(Logging.EventID.REPORTS.GetReportLog, "OpxGetReport");
                base.CheckSession(session);
                                
                SetStartTime();
                _dbClient.OpxDeleteReport(ref session, opReport);
                SetEndTime(Logging.EventID.REPORTS.DeleteReportLog, "OpxDeleteReport");
                if (session.Error != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public GeneratedReportModel GenerateReport(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var reports = _dbClient.GetAllowedList(ref session, new ReferenceType[] { ReferenceType.IdReport });
                SetEndTime(Logging.EventID.COMMON.GetAllowedListLog, "GetAllowedList");
                base.CheckSession(session);

                var report = (OpReport)((object[])reports[0]).Where(r => ((OpReport)r).IdReport == id).FirstOrDefault();
                
                SetStartTime();
                var result = _dbClient.OpxGenerateReport(ref session, report, 120);
                SetEndTime(Logging.EventID.REPORTS.GenerateReportLog, "OpxGenerateReport");
                base.CheckSession(session);                
                return new GeneratedReportModel()
                {
                    id = report.IdReport,
                    resultString = (result == null)? false.ToString() : result.ToString(),
                    reportType = report.IdReportType
                };
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning generating report.", e);
            }
        }

        private ReportModel ReportModelFromOpReport(OpReport opReport, bool replaceReferenceTypes = false, SESSION session = null)
        {
            var fields = opReport.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

            ReportModel result = new ReportModel()
            {
                Id = opReport.IdReport,
                Name = opReport.ToString(),
                Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opReport.DynamicValues) : fields
            };

            result.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
            ReplaceReferenceIds(result.Fields, opReport.DataList);

            return result;
        }

        private List<FieldModel> AddReportSpecialFieldsToFieldModel(OpReport report)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_REPORT_TYPE, report.IdReportType, "Integer", false));
            return list;
        }
    }
}