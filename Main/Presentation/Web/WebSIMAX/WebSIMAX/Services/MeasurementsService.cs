﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.DW;
using Microsoft.AspNetCore.Http;
using MsgPack.Serialization;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace SIMAX.Services
{
    public interface IMeasurementsService
    {
        string GetMeasurements(long[] idLocation, long[] serialNbr, long[] idMeter, List<ColumnModel> columns, DateTime dateFrom, DateTime dateTo);
        string GetLatestMeasurements(long[] idLocation, long[] serialNbr, long[] idMeter, List<ColumnModel> columns);
    }

    public class MeasurementsService : BaseService, IMeasurementsService
    {
        public MeasurementsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public string GetMeasurements(long[] idLocation, long[] serialNbr, long[] idMeter, List<ColumnModel> columns, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var time = new TypeDateTimeCode();


                if (dateFrom == null && dateTo != null)
                {
                    time.DateTime1 = DateTime.SpecifyKind(dateTo, DateTimeKind.Local);
                    time.Mode = TypeDateTimeCode.ModeType.LesserEqual;
                }
                else if (dateFrom != null && dateTo == null)
                {
                    time.DateTime1 = DateTime.SpecifyKind(dateFrom, DateTimeKind.Local);
                    time.Mode = TypeDateTimeCode.ModeType.GreaterEqual;
                }
                else if (dateFrom == null && dateTo == null) time = null;
                else
                {
                    time.Mode = TypeDateTimeCode.ModeType.Between;
                    time.DateTime1 = DateTime.SpecifyKind(dateFrom, DateTimeKind.Local);
                    time.DateTime2 = DateTime.SpecifyKind(dateTo, DateTimeKind.Local);
                }
                
                var session = _sessionService.GetSession();
                var typeIds = columns.Select(column => column.Id).ToArray();

                SetStartTime();
                Measure[] opMeasurements = _dbClient.OpxMeasurementFilter(ref session, idLocation, serialNbr, idMeter, typeIds, time, null, "", 0);
                SetEndTime(Logging.EventID.MEASUREMENTS.FilterMeasurementsLog, "OpxMeasurementFilter");
                base.CheckSession(session);

                return PackMeasurments(opMeasurements);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning measurements downloading.", e);
            }
        }

        public string GetLatestMeasurements(long[] idLocation, long[] serialNbr, long[] idMeter, List<ColumnModel> columns)
        {
            try
            {
                var session = _sessionService.GetSession();
                var typeIds = columns.Select(column => column.Id).ToArray();
                SetStartTime();
                OpData[] opMeasurements = _dbClient.OpxLatestMeasurementFilter(ref session, idLocation, serialNbr, idMeter, typeIds, null, "", 0);
                SetEndTime(Logging.EventID.MEASUREMENTS.FilterLatestMeasurementsLog, "OpxLatestMeasurementFilter");
                base.CheckSession(session);

                return PackMeasurments(opMeasurements);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning latest measurements downloading.", e);
            }
        }

        private string PackMeasurments(OpData[] measurments)
        {
            var packTime = Stopwatch.StartNew();
            var result = PackMeasurments(measurments.Select(data => MeasurementModel.MeasurementModelFromOpData(data)).ToArray());
            packTime.Stop();
            IMRLog.AddToLog(Logging.EventID.INFO.Measurments, new object[] { "GUID", packTime.Elapsed.ToString("ss'.'ff") });

            return result;
        }

        private string PackMeasurments(Measure[] measurments)
        {
            var packTime = Stopwatch.StartNew();
            var result = PackMeasurments(measurments.Select(data => MeasurementModel.MeasurementModelFromOpDataArch(data)).ToArray());
            packTime.Stop();
            IMRLog.AddToLog(Logging.EventID.INFO.Measurments, new object[] { "GUID", packTime.Elapsed.ToString("ss'.'ff") });

            return result;
        }

        private string PackMeasurments(MeasurementModel[] measurments)
        {
            var serializer = SerializationContext.Default.GetSerializer<MeasurementModel[]>();
            var bytes = serializer.PackSingleObject(measurments);
            var base64 = Convert.ToBase64String(bytes);

            return base64;
        }
    }
}
