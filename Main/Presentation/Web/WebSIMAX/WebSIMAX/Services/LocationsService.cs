﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SIMAX.Services
{
    public interface ILocationsService : IImportService
    {
        GridModel GetLocations(GridRequestModel req);
        LocationModel SaveLocation(LocationFormValuesModel model);
        LocationModel GetLocation(long id);
        ResponseDeleteModel DeleteLocation(long id);
        EntityModel GetLocationToEdit(long id);
        SchemaModel GetSchema(long id);
        ResponseListModel<SchemaModel> GetSchemas(long id);
    }

    public class LocationsService : BaseService, ILocationsService
    {
        public LocationsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public GridModel GetLocations(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);

                FilterByIds(req, ref columns, Enums.ReferenceType.IdLocation);

                var values = _dbClient.OpxLocationFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                base.CheckSession(session);
                var result = new GridModel()
                {
                    Values = values.Select(location =>
                    {
                        var obj = Common.DictionaryToObject(location.DynamicValues, location.ToString(), location.IdLocation);
                        obj.Latitude = location.Latitude;
                        obj.Longitude = location.Longitude;
                        obj.IdLocation = location.IdLocation;
                        return obj;
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning locations downloading.", e);
            }
        }
        public LocationModel GetLocation(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opLocation = _dbClient.OpxGetLocation(ref session, id);
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationLog);
                base.CheckSession(session);

                if (opLocation != null)
                {
                    return LocationModelFromOpLocation(opLocation, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return LocationModelFromOpLocation(opLocation, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning location " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location " + id + " downloading.", e);
            }
        }

        public LocationModel SaveLocation(LocationFormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpLocation opLocation;
                if (model.Id.HasValue)
                {
                    try
                    {
                        SetStartTime();
                        opLocation = _dbClient.OpxGetLocation(ref session, model.Id.Value);
                        SetEndTime(Logging.EventID.LOCATIONS.GetLocationLog);
                        base.CheckSession(session);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning location " + model.Id + " downloading.", e);
                    }
                }
                else
                    opLocation = new OpLocation();
                AddEntitiesBinding(model.Groups, ref opLocation.ParentGroups);
                AddEntitiesBinding(model.Devices, ref opLocation.CurrentDevices);
                AddEntitiesBinding(model.Meters, ref opLocation.CurrentMeters);

                #region Fields
                FillDataList(opLocation.DataList, model.Fields);
                #endregion

                SetStartTime();
                opLocation = _dbClient.OpxSaveLocation(ref session, opLocation);
                SetEndTime(Logging.EventID.LOCATIONS.SaveLocationLog);
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (opLocation != null)
                {
                    return LocationModelFromOpLocation(opLocation, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return LocationModelFromOpLocation(opLocation, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning location " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                {
                    throw new Exception("Issue concerning location " + model.Id + " saving.", e);
                }
            }
        }

        public ResponseDeleteModel DeleteLocation(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opLocation = new OpLocation();
                try
                {
                    SetStartTime();
                    opLocation = _dbClient.OpxGetLocation(ref session, id);
                    SetEndTime(Logging.EventID.LOCATIONS.GetLocationLog);
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning location " + id + " downloading.", e);
                }
                base.CheckSession(session);

                opLocation.IdLocationStateType = (int)OpLocationStateType.Enum.DELETED;
                SetStartTime();
                var result = _dbClient.OpxSaveLocation(ref session, opLocation);
                SetEndTime(Logging.EventID.LOCATIONS.SaveLocationLog);
                if (result != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location " + id + " saving ", e);
            }
        }

        public EntityModel GetLocationToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opLocation = _dbClient.OpxGetLocation(ref session, id);
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationLog);
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opLocation.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddLocationSpecialFieldsToFieldModel(opLocation));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    ReferenceId = opLocation.IdDistributor,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location " + id + " downloading.", e);
            }
        }

        public SchemaModel GetSchema(long id)
        {
            try
            {
                var session = _sessionService.GetSession();

                SetStartTime();
                var locationSchema = _dbClient.OpxGetLocationSchema(ref session, id).FirstOrDefault();
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationSchemaLog);

                base.CheckSession(session);

                SetStartTime();
                var location = _dbClient.OpxGetLocation(ref session, id);
                SetEndTime(Logging.EventID.LOCATIONS.GetLocationLog);

                base.CheckSession(session);

                return locationSchema != null ? SchemaModelFromOpSchema(locationSchema, location, session) : null;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location schema " + id + " downloading.", e);
            }
        }

        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            var schemaLogData = Logging.EventID.LOCATIONS.GetLocationSchemaLog;
            var entityLogData = Logging.EventID.LOCATIONS.GetLocationLog;
            return base.GetSchemas(id, _dbClient.OpxGetLocationSchema, _dbClient.OpxGetLocation, schemaLogData, entityLogData, "Locations");
        }

        public ImportStatusModel InitializeImport(ImportDataModel model)
        {
            try
            {
                var initializeLogData = Logging.EventID.LOCATIONS.ImportLocationInitializeLog;
                var validateLogData = Logging.EventID.LOCATIONS.ImportLocationValidateDataLog;
                return base.InitializeImport(model, _dbClient.OpxImportLocationInitialize, _dbClient.OpxImportLocationValidateData, initializeLogData,validateLogData, "Locations");
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location initializing import", e);
            }
        }

        public object StartImport(string guid)
        {
            try
            {
                SetStartTime();
                var status = base.StartImport(guid, _dbClient.OpxImportLocationStartImport);
                SetEndTime(Logging.EventID.LOCATIONS.ImportLocationStartImport, "OpxImportLocationStartImport");
                return status;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location starting import", e);
            }
        }

        public object StopImport(string guid)
        {
            try
            {
                SetStartTime();
                var result = base.StopImport(guid, _dbClient.OpxImportLocationStopImport);
                SetEndTime(Logging.EventID.LOCATIONS.ImportLocationStopImport, "OpxImportLocationStopImport");
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning location stopping import", e);
            }
        }

        public ImportProgressModel ImportProgress(string guid)
        {
            try
            {
                SetStartTime();
                var importProgress = base.ImportProgress(guid, _dbClient.OpxImportLocationProgress);
                SetEndTime(Logging.EventID.LOCATIONS.ImportLocationProgress, "OpxImportLocationProgress");
                return importProgress;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning progress of import locations", e);
            }
        }

        public ImportStatusModel ImportSummary(string guid)
        {
            try
            {
                SetStartTime();
                var statusModel = base.ImportSummary(guid, _dbClient.OpxImportLocationSummary);
                SetEndTime(Logging.EventID.LOCATIONS.ImportLocationSummary, "OpxImportLocationSummary");
                return statusModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Error concerning location import summary", e);
            }
        }

        private LocationModel LocationModelFromOpLocation(OpLocation opLocation, bool replaceReferenceTypes = false, SESSION session = null)
        {
            LocationModel locationModel;

            if (opLocation != null)
            {
                var fields = opLocation.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();
                var details = opLocation.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();

                locationModel = new LocationModel()
                {
                    Id = opLocation.IdLocation,
                    Name = opLocation.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opLocation.DynamicValues) : fields,
                    DetailsFields = details,
                    Devices = opLocation.CurrentDevices.Select(device => new DeviceModel()
                    {
                        Id = device.SerialNbr,
                        Name = device.ToString(),
                        Fields = device.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList(),
                        ReferenceId = device.IdDeviceType
                    }).ToList(),
                    Meters = opLocation.CurrentMeters.Select(meter => new MeterModel()
                    {
                        Id = meter.IdMeter,
                        Name = meter.ToString(),
                        Fields = meter.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList(),
                        ReferenceId = meter.MeterType != null ? meter.MeterType.IdMeterTypeClass : 0
                    }).ToList(),
                    Groups = opLocation.ParentGroups.Select(g => new GroupModel()
                    {
                        Id = g.IdLocation,
                        Name = g.Name,
                        ReferenceId = opLocation.IdDistributor
                    }).ToList(),
                    Latitude = opLocation.Latitude,
                    Longitude = opLocation.Longitude,
                    AllowGrouping = opLocation.AllowGrouping,
                    ReferenceId = opLocation.IdDistributor,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(locationModel.Fields, opLocation.DataList);
            }
            else
            {
                locationModel = new LocationModel()
                {
                    Fields = new List<FieldModel>(),
                    DetailsFields = new List<FieldModel>(),
                    Devices = new List<DeviceModel>(),
                    Meters = new List<MeterModel>(),
                    Groups = new List<GroupModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return locationModel;
        }

        private List<FieldModel> AddLocationSpecialFieldsToFieldModel(OpLocation location)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_LOCATION, location.IdLocation, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_LOCATION_TYPE, location.IdLocationType, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_LOCATION_STATE_TYPE, location.IdLocationStateType, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, location.IdDistributor, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_LOCATION_IN_KPI, location.InKpi, "Boolean", true));
            list.Add(PrepareFieldModel(DataType.HELPER_LOCATION_ALLOW_GROUPING, location.AllowGrouping, "Boolean", true));

            return list;
        }
    }
}
