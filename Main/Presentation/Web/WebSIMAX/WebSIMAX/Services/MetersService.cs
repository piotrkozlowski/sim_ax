﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIMAX.Models;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using SIMAX.Utils;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Net;
using IMR.Suite.Common;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SIMAX.Services
{
    public interface IMetersService
    {
        GridModel GetMeters(GridRequestModel req);
        MeterModel GetMeter(int id);
        MeterModel SaveMeter(FormValuesModel model);
        ResponseDeleteModel DeleteMeter(int id);
        EntityModel GetMeterToEdit(int id);
        MeterModel SaveMeterCurrentDevice(ObjectReferenceModel model);
        ResponseListModel<SchemaModel> GetSchemas(long id);
    }

    public class MetersService : BaseService, IMetersService
    {

        public MetersService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetMeters(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpMeter[] opMeters = _dbClient.OpxMeterFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.METERS.FilterMeterLog, "OpxMeterFilter");

                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opMeters.Select(meter =>
                    {
                        return Common.DictionaryToObject(meter.DynamicValues, meter.ToString(), meter.IdMeter);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch(StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meters downloading.", e);
            }
        }

        public MeterModel GetMeter(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var meter = _dbClient.OpxGetMeter(ref session, id);
                SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                base.CheckSession(session);

                if (meter != null)
                {
                    return MeterModelFromOpMeter(meter, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return MeterModelFromOpMeter(meter, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning meter " + id + " downloading.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + id + " downloading.", e);
            }
        }

        public EntityModel GetMeterToEdit(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var meter = _dbClient.OpxGetMeter(ref session, id);
                SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = meter.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddMeterSpecialFieldsToFieldModel(meter));

                var result = new EntityModel()
                {
                    Id = meter.IdMeter,
                    Fields = fields,
                    ReferenceId = meter.MeterType != null ? meter.MeterType.IdMeterTypeClass : 0,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + id + " downloading.", e);
            }
        }

        public MeterModel SaveMeter(FormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpMeter opMeter;
                if (model.Id != null)
                {
                    try
                    {
                        SetStartTime();
                        opMeter = _dbClient.OpxGetMeter(ref session, Convert.ToInt32(model.Id));
                        SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Issue concerning meter " + model.Id + " downloading.", e);
                    }
                    base.CheckSession(session);
                }
                else
                    opMeter = new OpMeter();

                AddEntitiesBinding(model.Values, ref opMeter.CurrentDevices);
                // AddEntitiesBinding(model.Values, ref opMeter.alarmGroups);

                #region Fields
                FillDataList(opMeter.DataList, model.Fields);
                #endregion

                SetStartTime();
                var savedMeter = _dbClient.OpxSaveMeter(ref session, opMeter);
                SetEndTime(Logging.EventID.METERS.SaveMeterLog, "OpxSaveMeter");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedMeter != null)
                {
                    return MeterModelFromOpMeter(savedMeter, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return MeterModelFromOpMeter(savedMeter, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning meter " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + model.Id + " saving.", e);
            }
        }

        public ResponseDeleteModel DeleteMeter(int id)
        {
            try
            {
                var session = _sessionService.GetSession();
                var opMeter = new OpMeter();
                try
                {
                    SetStartTime();
                    opMeter = _dbClient.OpxGetMeter(ref session, id);
                    SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning meter " + id + " downloading.", e);
                }
                base.CheckSession(session);
                SetStartTime();
                _dbClient.OpxDeleteMeter(ref session, opMeter);
                SetEndTime(Logging.EventID.METERS.DeleteMeterLog, "OpxDeleteMeter");
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    throw new StatusCodeException((int)HttpStatusCode.NotFound, session.Error.Msg);
                }

                return new ResponseDeleteModel(true, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + id + " deleting.", e);
            }
        }

        public MeterModel SaveMeterCurrentDevice(ObjectReferenceModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                var opDevice = new OpDevice();
                try
                {
                    SetStartTime();
                    opDevice = _dbClient.OpxGetDevice(ref session, model.ReferenceObjectId);
                    SetEndTime(Logging.EventID.DEVICES.GetDeviceLog, "OpxGetDevice");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning device " + model.ReferenceObjectId + " downloading.", e);
                }
                base.CheckSession(session);

                var opMeter = new OpMeter();
                try
                {
                    SetStartTime();
                    opMeter = _dbClient.OpxGetMeter(ref session, model.Id);
                    SetEndTime(Logging.EventID.METERS.GetMeterLog, "OpxGetMeter");
                }
                catch (Exception e)
                {
                    throw new Exception("Issue concerning meter " + model.Id + " downloading.", e);
                }
                base.CheckSession(session);

                if (opMeter.CurrentDevices != null)
                {
                    if (opMeter.CurrentDevices.Count > 0)
                    {
                        var opDeviceToChange = opMeter.CurrentDevices.FirstOrDefault(m => m.SerialNbr == opDevice.SerialNbr);
                        if (opDeviceToChange != null)
                        {
                            opDeviceToChange.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                        }
                        else
                        {
                            opDevice.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                            opMeter.CurrentDevices.Add(opDevice);
                        }

                        var opDevicesToDelete = opMeter.CurrentDevices.Where(m => m.SerialNbr != opDevice.SerialNbr).ToList();

                        if (opDevicesToDelete != null && opDevicesToDelete.Count > 0)
                        {
                            foreach (OpDevice dev in opDevicesToDelete)
                            {
                                dev.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete;
                            }
                        }
                    }
                    else
                    {
                        opDevice.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                        opMeter.CurrentDevices.Add(opDevice);
                    }
                }
                else
                {
                    opMeter.CurrentDevices = new List<OpDevice>();
                    opDevice.OpState = IMR.Suite.UI.Business.Objects.OpChangeState.New;
                    opMeter.CurrentDevices.Add(opDevice);
                }
                SetStartTime();
                opMeter = _dbClient.OpxSaveMeter(ref session, opMeter);
                SetEndTime(Logging.EventID.METERS.SaveMeterLog, "OpxSaveMeter");

                if (opMeter != null)
                {
                    return MeterModelFromOpMeter(opMeter, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return MeterModelFromOpMeter(opMeter, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning meter " + model.Id + " saving.");
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning meter " + model.Id + " saving.", e);
            }
        }

        public ResponseListModel<SchemaModel> GetSchemas(long id)
        {
            var schemaLogData = Logging.EventID.METERS.GetMeterSchemaLog;
            var entityLogData = Logging.EventID.METERS.GetMeterLog;
            return base.GetSchemas(id, _dbClient.OpxGetMeterSchema, _dbClient.OpxGetMeter, schemaLogData, entityLogData, "Meters");
        }

        private MeterModel MeterModelFromOpMeter(OpMeter opMeter, bool replaceReferenceTypes = false, SESSION session = null)
        {
            MeterModel result;

            if (opMeter != null)
            {
                var fields = opMeter.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                result = new MeterModel()
                {
                    Id = opMeter.IdMeter,
                    Name = opMeter.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opMeter.DynamicValues) : fields,
                    Devices = opMeter.CurrentDevices.Select(device => new EntityModel()
                    {
                        Id = device.SerialNbr,
                        Name = device.ToString(),
                        Fields = device.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList()
                    }).ToList(),
                    // alarmGroups = opMeter.alarmGroups.Select(g => new GroupModel()
                    // {
                    //     Id = g.IdMeter,
                    //     Name = g.Name,
                    //     ReferenceId = opMeter.IdDistributor
                    // }).ToList(),
                    // AllowGrouping = opMeter.AllowGrouping,
                    CurrentDeviceId = opMeter.CurrentDevice != null ? (long?)opMeter.CurrentDevice.SerialNbr : null,
                    CurrentDeviceName = opMeter.CurrentDevice != null ? opMeter.CurrentDevice.ToString() : "",
                    ReferenceId = opMeter.MeterType.IdMeterTypeClass,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(result.Fields, opMeter.DataList);
            }
            else
            {
                result = new MeterModel()
                {
                    Fields = new List<FieldModel>(),
                    Devices = new List<EntityModel>(),
                    alarmGroups = new List<GroupModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return result;
        }

        private List<FieldModel> AddMeterSpecialFieldsToFieldModel(OpMeter meter)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_METER, meter.IdMeter, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_METER_TYPE, meter.IdMeterType, "Integer", true));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, meter.IdDistributor, "Integer", true));
            // list.Add(PrepareFieldModel(DataType.HELPER_LOCATIONS_ALLOW_GROUPING, meter.AllowGrouping, "Boolean", true));

            return list;
        }
    }
}
