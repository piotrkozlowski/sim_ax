﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SIMAX.Models;
using SIMAX.Utils;
using SIMAX.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TakeIo.Spreadsheet;
using static IMR.Suite.Common.Enums;

namespace SIMAX.Services
{
    public interface IAppService
    {
        GridModel GetActivities();
        GridModel GetRoles();
        GridModel GetReferenceObjectList(GridRequestReferenceModel referenceType);
        GridModel ImportExcelFile(byte[] byteArray, string fileName, List<ColumnModel> dictionaryColumns);
        OpFile DownloadTaskAttachment(int modelId, long attachmentId, string jwt);
        OpFile DownloadIssueAttachment(int modelId, long attachmentId, string jwt);
        object GetContextList(ContextListModel contextListModel);
        OpFile GetLogo(int idOperator, string jwt);
        byte[] ExportLocations(string jwt, string token);
        byte[] ExportMeters(string jwt, string token);
        byte[] ExportDevices(string jwt, string token);
        byte[] ExportTasks(string jwt, string token);
        byte[] ExportRoutes(string jwt, string token);
        string SaveRequestModel(GridRequestModel requestModel);
    }

    public class AppService : BaseService, IAppService
    {
        private static string REQUEST_MODEL = "REQUEST_MODEL";

        public AppService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetActivities()
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var result = _dbClient.GetDictionaries(ref session, new Enums.ReferenceType[] { Enums.ReferenceType.IdActivity }).FirstOrDefault();
                SetEndTime(Logging.EventID.DICTIONARY.GetDictionariesLog, "GetDictionaries");

                var totalCount = 0;
                var gridModel = new GridModel();

                if (result != null)
                {
                    totalCount = (result as object[]).Count();
                    gridModel.TotalCount = totalCount;
                    gridModel.Values = (result as object[]).Select(a =>
                    {
                        var activity = a as OpActivity;

                        var dictionary = new Dictionary<string, object>();
                        dictionary.Add("Activity", activity.Name);
                        dictionary.Add("Description", activity.Descr != null ? activity.Descr.Description : "");
                        dictionary.Add("ReferenceType", activity.ReferenceType != null ? activity.ReferenceType.Name : "");
                        dictionary.Add("ReferenceObject", activity.ReferenceValue);
                        dictionary.Add("Allow", activity.Allow);
                        dictionary.Add("Deny", activity.Deny);
                        dictionary.Add("Id", activity.IdActivity);
                        dictionary.Add("ReferenceTypeId", activity.IdReferenceType);
                        dictionary.Add("IdOperatorActivity", activity.IdOperatorActivity);
                        dictionary.Add("ModuleId", activity.IdModule);
                        dictionary.Add("ValueString", activity.ToString());

                        return Common.DictionaryToObject(dictionary);
                    }).ToList();
                    gridModel.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
                }

                return gridModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning activities table downloading.", e);
            }
        }

        public GridModel GetRoles()
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var result = _dbClient.GetDictionaries(ref session, new Enums.ReferenceType[] { Enums.ReferenceType.IdRole }).FirstOrDefault();
                SetEndTime(Logging.EventID.DICTIONARY.GetDictionariesLog, "GetDictionaries");

                var totalCount = 0;
                var gridModel = new GridModel();

                if (result != null)
                {
                    totalCount = (result as object[]).Count();
                    gridModel.TotalCount = totalCount;
                    gridModel.Values = (result as object[]).Select(r =>
                    {
                        var role = r as OpRole;

                        var dictionary = new Dictionary<string, object>();
                        dictionary.Add("Name", role.Name);
                        dictionary.Add("Description", role.Descr != null ? role.Descr.Description : "");
                        dictionary.Add("ModuleName", role.Module != null ? role.Module.Name : "");
                        dictionary.Add("Id", role.IdRole);
                        dictionary.Add("ValueString", role.ToString());

                        return Common.DictionaryToObject(dictionary);
                    }).ToList();
                    gridModel.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
                }

                return gridModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning roles table downloading.", e);
            }
        }

        public GridModel GetReferenceObjectList(GridRequestReferenceModel model)
        {
            try
            {
                var session = _sessionService.GetSession();
                var totalCount = 0L;

                var columns = model.Columns.Select(column => new ColumnInfo() { SortType = column.SortDesc.HasValue ? column.SortDesc.Value ? SortType.Desc : SortType.Asc : SortType.None, IdDataType = column.Id }).ToArray();
                SetStartTime();
                var values = _dbClient.GetReferenceObjectList(ref session, out totalCount, (ReferenceType)model.IdReferenceType, null, columns, model.PageNumber, model.PageSize, model.Where);
                SetEndTime(Logging.EventID.COMMON.GetRefObjectListLog, "GetReferenceObjectList");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = values.Select(value =>
                    {
                        var dynamicValue = (dynamic)value;
                        var dictionary = new Dictionary<string, object>();
                        string valueString = null;

                        if (base.HasPropertyOrField(value.GetType(), "DynamicValues"))
                        {
                            dictionary = dynamicValue.DynamicValues;
                            valueString = value.ToString();
                        }
                        dictionary.Add("Id", (value as IReferenceType).GetReferenceKey());
                        dictionary.Add("Name", (value as IReferenceType).GetReferenceValue().ToString());
                        dictionary.Add("ValueString", valueString);

                        return Common.DictionaryToObject(dictionary);
                    }).Skip((model.PageNumber - 1) * model.PageSize).Take(model.PageSize).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning reference object list downloading.", e);
            }
        }

        public GridModel ImportExcelFile(byte[] byteArray, string fileName, List<ColumnModel> dictionaryColumns)
        {
            try
            {
                var stream = new MemoryStream(byteArray);

                var ext = Path.GetExtension(fileName);
                var workSheet = Spreadsheet.Read(stream, ext);

                List<dynamic> values = new List<dynamic>();
                var columns = workSheet[0].ToList();

                for (var i = 0; i < columns.Count; i++)
                {
                    var col = columns[i];
                    foreach (var dictCol in dictionaryColumns)
                    {
                        if (col.ToString() == dictCol.Caption)
                        {
                            columns[i] = dictCol.Id.ToString();
                            break;
                        }
                        else if (col.ToString() == dictCol.Name)
                        {
                            columns[i] = dictCol.Id.ToString();
                            break;
                        }
                        else if (col.ToString() == dictCol.Id.ToString())
                        {
                            columns[i] = dictCol.Id.ToString();
                            break;
                        }
                        else if (dictionaryColumns.IndexOf(dictCol) == dictionaryColumns.Count - 1)
                            Logger.Add(Logging.EventID.SECUREAPP.PairColumnsToImport, new object[] { col });
                    }
                }

                foreach (var row in workSheet.Skip(1))
                {
                    var dictionary = new Dictionary<string, object>();
                    foreach (var cell in row.Select((value, index) => new { index, value }))
                    {
                        if (columns[cell.index] != null)
                            dictionary.Add(columns[cell.index].ToString(), cell.value);
                    }

                    values.Add(Common.DictionaryToObject(dictionary));
                }

                return new GridModel()
                {
                    TotalCount = workSheet.Count - 1,
                    Values = values
                };
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concering importing excel file.", e);
            }
        }

        public OpFile DownloadTaskAttachment(int modelId, long attachmentId, string jwt)
        {
            var attachment = base.DownloadAttachment(jwt, modelId, attachmentId, _dbClient.OpxGetTaskAttachment);
            SetEndTime(Logging.EventID.TASKS.GetTaskAttachmentLog, "OpxGetTaskAttachment");
            return attachment;
        }

        public OpFile DownloadIssueAttachment(int modelId, long attachmentId, string jwt)
        {
            var attachment = base.DownloadAttachment(jwt, modelId, attachmentId, _dbClient.OpxGetIssueAttachment);
            SetEndTime(Logging.EventID.ISSUES.GetIssueAttachmentLog, "OpxGetIssueAttachment");
            return attachment;
        }

        public byte[] ExportLocations(string jwt, string token)
        {
            var bytes = base.ExportGrid(jwt, token, REQUEST_MODEL, _dbClient.OpxExportLocation);
            SetEndTime(Logging.EventID.LOCATIONS.ExportLocationLog, "OpxExportLocation");
            return bytes;
        }

        public byte[] ExportMeters(string jwt, string token)
        {
            var bytes = base.ExportGrid(jwt, token, REQUEST_MODEL, _dbClient.OpxExportMeter);
            SetEndTime(Logging.EventID.METERS.ExportMeterLog, "OpxExportMeter");
            return bytes;
        }

        public byte[] ExportDevices(string jwt, string token)
        {
            var bytes = base.ExportGrid(jwt, token, REQUEST_MODEL, _dbClient.OpxExportDevice);
            SetEndTime(Logging.EventID.DEVICES.ExportDeviceLog, "OpxExportDevice");
            return bytes;
        }

        public byte[] ExportTasks(string jwt, string token)
        {
            var bytes = base.ExportGrid(jwt, token, REQUEST_MODEL, _dbClient.OpxExportTask);
            SetEndTime(Logging.EventID.TASKS.ExportTaskLog, "OpxExportTask");
            return bytes;
        }

        public byte[] ExportRoutes(string jwt, string token)
        {
            var bytes = base.ExportGrid(jwt, token, REQUEST_MODEL, _dbClient.OpxExportRoute);
            SetEndTime(Logging.EventID.ROUTES.ExportRoutesLog, "OpxExportRoute");
            return bytes;
        }

        public string SaveRequestModel(GridRequestModel request)
        {
            try
            {
                var session = _sessionService.GetSession();
                var payload = new Dictionary<string, object>()
                {
                    { "date", DateTime.Now }
                };
                var savedRequestModel = new SavedRequestModel();
                savedRequestModel.Token = JWTHelper.EncodeToken(payload);
                savedRequestModel.GridRequestModel = request;
                var serialized = JsonConvert.SerializeObject(savedRequestModel,
                Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });
                _accessor.HttpContext.Session.SetString(REQUEST_MODEL, serialized);
                return savedRequestModel.Token;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
        }

        public object GetContextList(ContextListModel contextListModel)
        {
            return GetContextListObjects(contextListModel);
        }

        public OpFile GetLogo(int idOperator, string jwt)
        {
            try
            {
                var session = _sessionService.GetSession(jwt);
                SetStartTime();
                var data = _dbClient.OpxGetOperatorSettings(ref session, idOperator, new long[] { DataType.DISTRIBUTOR_LOGO });
                SetEndTime(Logging.EventID.USERS.GetOperatorSettingsLog, "OpxGetOperatorSettings");
                base.CheckSession(session);

                if (data.Length == 0) return null;

                var logo = (OpFile) data[0];

                return logo;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concering download logo.", e);
            }
        }
    }
}
