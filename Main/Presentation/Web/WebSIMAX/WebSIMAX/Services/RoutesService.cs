﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.CORE;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;

namespace SIMAX.Services
{
    public interface IRoutesService : IImportService
    {
        GridModel GetRoutes(GridRequestModel req);
        GridModel GetRoutePoints(GridRequestModel req);
        GridModel GetRouteOperators(GridRequestModel req);
        RouteModel SaveRoute(RouteFormValuesModel req);
        RouteModel GetRoute(long id);
        EntityModel GetRouteToEdit(long id);
        ResponseDeleteModel DeleteRoute(long id);
    }
    public class RoutesService : BaseService, IRoutesService
    {
        private ILocationsService _locationService;

        public RoutesService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService, ILocationsService locationService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
            _locationService = locationService;
        }

        public GridModel GetRoutes(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);
                SetStartTime();
                OpRoute[] opRoutes = _dbClient.OpxRouteFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.ROUTES.FilterRoutesLog, "OpxRouteFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opRoutes.Select(route =>
                    {
                        return Common.DictionaryToObject(route.DynamicValues, route.ToString(), route.IdRoute);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public GridModel GetRoutePoints(GridRequestModel req)
        {
            if (string.IsNullOrEmpty(req.Where))
            {
                return new GridModel()
                {
                    Values = new List<dynamic>(),
                    TotalCount = 0,
                    HasErrorCode =  0
                };
            }
            try
            {
                var session = _sessionService.GetSession();
                var columns = ListColumnModelToColumnInfoArray(req.Columns);

                var activeViews = GetActiveViews();
                var locationView = activeViews.FirstOrDefault(v => v.IdReferenceType == (int)Enums.ReferenceType.IdLocation);
                if (locationView == null)
                {
                    throw new Exception("Location view does not exist");
                }

                var locationColumnId = locationView.PrimaryKeyColumn;

                // Check if column exist
                var where = "";
                var columnExist = columns.FirstOrDefault(c => c.Name == locationColumnId.Name);
                if (columnExist == null)
                {
                    // Add column
                    var col = new ColumnInfo() { SortType = SortType.None, IdDataType = locationColumnId.IdDataType, Name = locationColumnId.Name };
                    // req.Where should contains only ids
                    where = String.Format("[{0}] in ({1})", locationColumnId.Name, req.Where);
                    col.Filter = new string[] { where };
                    var columnsList = columns.ToList();
                    columnsList.Add(col);
                    columns = columnsList.ToArray();
                }
                else
                {
                    where = String.Format("[{0}] in ({1})", columnExist.Name, req.Where);
                    columnExist.Filter = new string[] { where };
                }

                long totalCount = 0;
                SetStartTime();
                var values = _dbClient.OpxLocationFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, where);
                SetEndTime(Logging.EventID.LOCATIONS.FilterLocationLog, "OpxLocationFilter");
                base.CheckSession(session);
                var result = new GridModel()
                {
                    Values = values.Select(location =>
                    {
                        var obj = Common.DictionaryToObject(location.DynamicValues, location.ToString(), location.IdLocation);
                        obj.Latitude = location.Latitude;
                        obj.Longitude = location.Longitude;
                        obj.LocationId = location.IdLocation;
                        return obj;
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning locations downloading.", e);
            }
        }

        public GridModel GetRouteOperators(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = new List<ColumnInfo>()
                {
                    new ColumnInfo()
                    {
                        IdDataType = DataType.HELPER_ID_OPERATOR,
                        Name = DataType.HELPER_ID_OPERATOR.ToString()
                    }                    
                }.ToArray();
                SetStartTime();
                OpOperator[] opOperators = _dbClient.OpxOperatorFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.ROUTES.FilterRoutesLog, "OpxOperatorFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = opOperators.Select(op =>
                    {
                        IDictionary<string, object> eo = new ExpandoObject() as IDictionary<string, object>;
                        eo.Add(new KeyValuePair<string, object>("id", op.IdOperator));
                        //eo.Add(new KeyValuePair<string, object>("name", op.DynamicValues.FirstOrDefault(x => x.Key == "10074").Value + " " + op.DynamicValues.FirstOrDefault(x => x.Key == "10075").Value));                    
                        eo.Add(new KeyValuePair<string, object>("name", op.ToString()));                        
                        return (dynamic) eo;                        
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public RouteModel GetRoute(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opRoute = _dbClient.OpxGetRoute(ref session, id);
                SetEndTime(Logging.EventID.ROUTES.GetRoutesLog, "OpxGetRoute");
                base.CheckSession(session);

                if (opRoute != null)
                {
                    return RouteModelFromOpRoute(opRoute, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return RouteModelFromOpRoute(opRoute, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning route " + id + " downloading.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResponseDeleteModel DeleteRoute(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opRoute = _dbClient.OpxGetRoute(ref session, id);
                SetEndTime(Logging.EventID.ROUTES.GetRoutesLog, "OpxGetRoute");
                base.CheckSession(session);

                opRoute.IdRouteStatus = (int)OpRouteStatus.Enum.Finished;
                SetStartTime();
                var res = _dbClient.OpxSaveRoute(ref session, opRoute);

                SetEndTime(Logging.EventID.ROUTES.SaveRouteLog, "OpxSaveRoute");
                if (res != null) return new ResponseDeleteModel(true, session);
                else return new ResponseDeleteModel(false, session);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntityModel GetRouteToEdit(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opRoute = _dbClient.OpxGetRoute(ref session, id);
                SetEndTime(Logging.EventID.ROUTES.GetRoutesLog, "OpxGetRoute");
                base.CheckSession(session);

                var fields = new List<FieldModel>();
                fields = opRoute.DataList.Select(d => FieldModel.FieldFromOpDataList(d)).ToList();
                fields.AddRange(AddRouteSpecialFieldsToFieldModel(opRoute));

                var result = new EntityModel()
                {
                    Id = id,
                    Fields = fields,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public RouteModel SaveRoute(RouteFormValuesModel model)
        {
            try
            {
                var session = _sessionService.GetSession();

                OpRoute opRoute = new OpRoute();
                if (model.Id.HasValue)
                {
                    opRoute.IdRoute = model.Id.Value;
                }

                #region Fields
                FillDataList(opRoute.DataList, model.Fields);
                #endregion

                #region Points
                if (model.Locations != null && model.RemovedTasks != null)
                {
                    List<OpTask> tasks = new List<OpTask>();
                    model.Locations.ForEach(location =>
                    {
                        tasks.Add(new OpTask()
                        {
                            IdLocation = location.IdLocation,
                            IdTask = location.IdTask
                        });
                    });
                    model.RemovedTasks.ForEach(taskId =>
                    {
                        tasks.Add(new OpTask()
                        {
                            IdTask = taskId,
                            OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete
                        });
                    });
                    opRoute.TaskList = tasks;
                }
                #endregion
                #region Operators
                if (model.Operators != null && model.RemovedOperators != null)
                {
                    List<OpOperator> operators = new List<OpOperator>();
                    foreach (int idOp in model.Operators)
                    {
                        operators.Add(new OpOperator()
                        {
                            IdOperator = idOp
                        });
                    }
                    foreach (int idOp in model.RemovedOperators)
                    {
                        operators.Add(new OpOperator()
                        {
                            IdOperator = idOp,
                            OpState = IMR.Suite.UI.Business.Objects.OpChangeState.Delete
                    });
                    }
                    opRoute.OperatorList = operators;
                }
                #endregion
                SetStartTime();
                OpRoute savedRoute = _dbClient.OpxSaveRoute(ref session, opRoute);
                SetEndTime(Logging.EventID.ROUTES.SaveRouteLog, "OpxSaveRoute");
                base.CheckSession(session);
                if (session.Status == SESSION_STATUS.ERROR)
                {
                    System.Diagnostics.Debug.Write(session.Error);
                    throw new StatusCodeException((int)HttpStatusCode.BadRequest, session.Error.Msg);
                }

                if (savedRoute != null)
                {
                    return RouteModelFromOpRoute(savedRoute, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return RouteModelFromOpRoute(savedRoute, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, "Issue concerning route " + model.Id + " saving.");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ImportStatusModel InitializeImport(ImportDataModel model)
        {
            try
            {
                var initializeLogData = Logging.EventID.ROUTES.ImportRouteInitializeLog;
                var validateLogData = Logging.EventID.ROUTES.ImportRouteValidateDataLog;
                return base.InitializeImport(model, _dbClient.OpxImportRouteInitialize, _dbClient.OpxImportRouteValidateData, initializeLogData, validateLogData, "Routes");
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning route initializing import", e);
            }
        }

        public object StartImport(string guid)
        {
            try
            {
                SetStartTime();
                var status = base.StartImport(guid, _dbClient.OpxImportRouteStartImport);
                SetEndTime(Logging.EventID.ROUTES.ImportRouteStartImport, "OpxImportRouteStartImport");
                return status;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning route starting import", e);
            }
        }

        public object StopImport(string guid)
        {
            try
            {
                SetStartTime();
                var result = base.StopImport(guid, _dbClient.OpxImportRouteStopImport);
                SetEndTime(Logging.EventID.ROUTES.ImportRouteStopImport, "OpxImportRouteStopImport");
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning route stopping import", e);
            }
        }

        public ImportProgressModel ImportProgress(string guid)
        {
            try
            {
                SetStartTime();
                var importProgress = base.ImportProgress(guid, _dbClient.OpxImportRouteProgress);
                SetEndTime(Logging.EventID.ROUTES.ImportRouteProgress, "OpxImportRouteProgress");
                return importProgress;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning progress of import routes", e);
            }
        }

        public ImportStatusModel ImportSummary(string guid)
        {
            try
            {
                SetStartTime();
                var statusModel = base.ImportSummary(guid, _dbClient.OpxImportRouteSummary);
                SetEndTime(Logging.EventID.ROUTES.ImportRouteSummary, "OpxImportRouteSummary");
                return statusModel;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Error concerning route import summary", e);
            }
        }

        private RouteModel RouteModelFromOpRoute(OpRoute opRoute, bool replaceReferenceTypes = false, SESSION session = null)
        {
            RouteModel model;

            if (opRoute != null)
            {
                var fields = opRoute.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();
                var activeViews = GetActiveViews();
                var routesView = activeViews.FirstOrDefault(v => v.IdReferenceType == (int)Enums.ReferenceType.IdTask);

                string latName = null, lonName = null;

                if (routesView != null)
                {
                    var locationColumnn = routesView.GetViewColumn(DataType.HELPER_ID_LOCATION, viewColumnType: Enums.ViewColumnType.BaseHelper, keyColumn: routesView.PrimaryKeyColumn);

                    if (locationColumnn != null)
                    {
                        var lat = routesView.GetViewColumn(DataType.LOCATION_LATITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: locationColumnn);
                        if (lat != null)
                            latName = lat.Name;
                        var lon = routesView.GetViewColumn(DataType.LOCATION_LONGITUDE, viewColumnType: Enums.ViewColumnType.Standard, keyColumn: locationColumnn);
                        if (lon != null)
                            lonName = lon.Name;
                    }

                }

                model = new RouteModel();

                model.Id = opRoute.IdRoute;
                model.Name = opRoute.ToString();
                model.Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opRoute.DynamicValues) : fields;
            
                model.Locations = opRoute.TaskList != null ? opRoute.TaskList.Select(t => {
                    var task = new EntityModel();
               
                    task.Id = t.IdLocation.Value;
                    task.ReferenceId = t.IdTask;
                    task.Name = t.ToString();
                    task.Fields = (t.DynamicValues != null && t.DynamicValues.Count > 0) ? t.DynamicValues.Select(f => FieldModel.FieldFromDynamicValues(f)).ToList() : new List<FieldModel>();

                    task.Fields.Add(new FieldModel() { Name = "Latitude", Value = latName != null && t.DynamicValues.ContainsKey(latName) ? t.DynamicValues[latName] : null });
                    task.Fields.Add(new FieldModel() { Name = "Longitude", Value = lonName != null && t.DynamicValues.ContainsKey(lonName) ? t.DynamicValues[lonName] : null });

                        return task;
                }).ToList() : new List<EntityModel>();

                model.Operators = opRoute.OperatorList != null ? opRoute.OperatorList.Select(t =>
                {
                    EntityModel op = new EntityModel();

                    op.Id = t.IdOperator;
                    op.Name = t.ToString();

                    return op;
                }).ToList() : new List<EntityModel>();

                model.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;

                ReplaceReferenceIds(model.Fields, opRoute.DataList);
            }
            else
            {
                model = new RouteModel()
                {
                    Fields = new List<FieldModel>(),
                    Locations = new List<EntityModel>(),
                    Operators = new List<EntityModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return model;
        }

        private List<FieldModel> AddRouteSpecialFieldsToFieldModel(OpRoute route)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_STATUS, route.IdRouteStatus, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_OPERATOR, route.IdOperatorExecutor, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_TYPE, route.IdRouteType, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ROUTE_DEF, route.IdRouteDef, "Integer", false));
            list.Add(PrepareFieldModel(DataType.HELPER_ID_DISTRIBUTOR, route.IdDistributor, "integer", false));

            return list;
        }
    }
}