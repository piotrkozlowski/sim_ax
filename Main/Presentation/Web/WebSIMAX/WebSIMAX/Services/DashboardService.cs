﻿using SIMAX.Models;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Dynamic;
using System;
using Newtonsoft.Json;
using SIMAX.Utils;
using Microsoft.AspNetCore.Http;
using IMR.Suite.UI.Business.Objects.CORE;

namespace SIMAX.Services
{
    public interface IDashboardService
    {
        DashboardModel GetDashboardConfig();
        ResponseListModel<object> GetDashbordItems(long[] ids, int groupId);
    }
    public class DashboardService : BaseService, IDashboardService
    {
        public DashboardService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public DashboardModel GetDashboardConfig()
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var dashboard = _dbClient.OpxGetOperatorDashboardConf(ref session, 0);
                SetEndTime(Logging.EventID.USERS.GetOperatorDashboardConfLog, "OpxGetOperatorDashboardConf");
                base.CheckSession(session);

                var groups = dashboard.Controls.GroupBy(i => i.Group.IdGroup);

                var result = new DashboardModel();
                result.FitToScreen = dashboard.FitToScreen;
                result.UseMargin = dashboard.UseMargin;
                result.UsePadding = dashboard.UsePadding;
                result.KeepAspectRatio = dashboard.KeepAspectRatio;
                result.Width = dashboard.Width;
                result.MaxWidth = dashboard.MaxWidth;
                result.MaxHeight = dashboard.MaxHeight;

                long rowSize = 0;

                var row = new DashboardRowModel();
                var addRow = true;
                foreach (var group in groups)
                {
                    var first = group.First();
                    addRow = true;
                    var column = new DashboardColumnModel()
                    {
                        Title = first.Group.ToString(),
                        RefreshPeriod = first.Group.RefreshPeriod,
                        Size = GetSizeFromPercent(first.Group.Width),
                        GroupId = group.Key,
                        BorderVisible = first.Group.BorderVisible
                    };
                    var sortedItems = group.OrderBy(i => i.IndexNbr);
                    foreach (var item in sortedItems)
                    {
                        long type;
                        if (item.Class == Enums.DashboardItemClass.TILE)
                        {
                            type = (long)DashboardItemTypes.TILE;
                        }
                        else if (item.Class == Enums.DashboardItemClass.GAUGE)
                        {
                            type = (long)DashboardItemTypes.GAUGE;
                        }
                        else if (item.Class == Enums.DashboardItemClass.GRID)
                        {
                            type = (long)DashboardItemTypes.TABLE;
                        }
                        else if (item.Class == Enums.DashboardItemClass.CHART)
                        {
                            type = (long)DashboardItemTypes.CHART;
                        }
                        else if (item.Class == Enums.DashboardItemClass.MAP)
                        {
                            type = (long)DashboardItemTypes.MAP;
                        }
                        else if (item.Class == Enums.DashboardItemClass.SCHEMA)
                        {
                            type = (long)DashboardItemTypes.SCHEMA;
                        }
                        else continue;

                        var dashBoardItem = new DashboardItemModel()
                        {
                            Id = item.IdType,
                            Type = type,
                            Title = item.Type.ToString(),
                            Size = GetSizeFromPercent(item.Width)
                        };
                        column.Items.Add(dashBoardItem);
                    }
                    if (rowSize + column.Size > 12)
                    {
                        var rowData = JsonConvert.SerializeObject(row);
                        DashboardRowModel rowClone = JsonConvert.DeserializeObject<DashboardRowModel>(rowData);
                        result.Rows.Add(rowClone);
                        row = new DashboardRowModel();
                        addRow = false;
                        rowSize = 0;
                    }
                    row.Columns.Add(column);
                    rowSize += column.Size;
                    if (rowSize == 12)
                    {
                        var rowData = JsonConvert.SerializeObject(row);
                        DashboardRowModel rowClone = JsonConvert.DeserializeObject<DashboardRowModel>(rowData);
                        result.Rows.Add(rowClone);
                        row = new DashboardRowModel();
                        addRow = false;
                        rowSize = 0;
                    }
                }
                if (addRow)
                {
                    var rowData = JsonConvert.SerializeObject(row);
                    DashboardRowModel rowClone = JsonConvert.DeserializeObject<DashboardRowModel>(rowData);
                    result.Rows.Add(rowClone);
                    row = new DashboardRowModel();
                }
                result.HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0;
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning collector dashboard config downloading.", e);
            }
        }

        public ResponseListModel<object> GetDashbordItems(long[] ids, int groupId)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var data = _dbClient.OpxGetOperatorDashboardData(ref session, 0, groupId, null, ids);
                SetEndTime(Logging.EventID.USERS.GetOperatorDashboardDataLog, "OpxGetOperatorDashboardData");
                base.CheckSession(session);

                var result = new List<object>();

                foreach (var d in data)
                {
                    if (d.Class == Enums.DashboardItemClass.TILE)
                    {
                        var tile = (OpTileControl)d;

                        var iconExtension = Enums.FileExtension.Unknown.ToString();
                        if (tile.Icon != null) {
                            try
                            {
                                iconExtension = tile.Icon.Extension.ToString();
                            } catch (Exception) {
                                IMRLog.AddToLog(Logging.EventID.ERRORS.FileExtensionProblem, new object[] { "OpxGetOperatorDashboardData", tile.IdType });
                            }
                        }

                        var backgroundImageExtension = Enums.FileExtension.Unknown.ToString();
                        if (tile.BackgroundImage != null)
                        {
                            try
                            {
                                backgroundImageExtension = tile.BackgroundImage.Extension.ToString();
                            }
                            catch (Exception)
                            {
                                IMRLog.AddToLog(Logging.EventID.ERRORS.FileExtensionProblem, new object[] { "OpxGetOperatorDashboardData", tile.IdType });
                            }
                        }

                        result.Add(new { id = tile.IdType,
                            value = tile.Amount,
                            amountVisible = tile.AmountVisible,
                            backgroundColor = ColorToRgba(tile.BackgroundColor),
                            backgroundImage = tile.BackgroundImage != null ? tile.BackgroundImage.FileBytes : null,
                            backgroundName = tile.BackgroundImage != null ? tile.BackgroundImage.PhysicalFileName : null,
                            backgroundImageExtension = backgroundImageExtension,
                            hoverColor = ColorToRgba(tile.HoverColor),
                            icon = tile.Icon != null ? tile.Icon.FileBytes : null,
                            iconName = tile.Icon != null ? tile.Icon.PhysicalFileName : null,
                            iconExtension = iconExtension,
                            iconVisible = tile.IconVisible,
                            texHeaderVisible = tile.TexHeaderVisible,
                            textHeader = tile.TextHeader,
                            referenceType = tile.ReferenceType,
                            referenceData = tile.ReferenceData,
                            borderRadius = tile.BorderRadius,
                            shadowColor = tile.ShadowColor.HasValue ? ColorToRgba(tile.ShadowColor) : null,
                            shadowHorizontalLenght = tile.ShadowHorizontalLenght,
                            shadowVerticalLenght = tile.ShadowVerticalLenght,
                            shadowBlurRadius = tile.ShadowBlurRadius,
                            shadowSpreadRadius = tile.ShadowSpreadRadius,
                        });
                    }
                    else if (d.Class == Enums.DashboardItemClass.GAUGE)
                    {
                        var gaude = (OpGaugeControl)d;
                        result.Add(new { id = gaude.IdType, min = gaude.MinValue, max = gaude.MaxValue, value = gaude.Value });
                    }
                    else if (d.Class == Enums.DashboardItemClass.GRID)
                    {
                        var grid = (OpGridControl)d;
                        var dataSource = new List<object>();
                        var columns = grid.Columns.Select(c => new { dataField = c.Name.ToLower(), caption = c.Caption }).ToList();
                        foreach (var row in grid.DataSource)
                        {
                            var eo = new ExpandoObject();
                            var eoColl = (ICollection<KeyValuePair<string, object>>)eo;
                            foreach (var value in row.Values)
                            {
                                eoColl.Add(new KeyValuePair<string, object>(value.Key.ToLower(), value.Value));
                            }

                            dataSource.Add(eo);
                        }

                        result.Add(new { id = grid.IdType, columns = columns, dataSource = dataSource });
                    }
                    else if (d.Class == Enums.DashboardItemClass.CHART)
                    {
                        var chart = (OpChartControl)d;
                        result.Add(new { id = chart.IdType, series = chart.SeriesList });
                    }
                    else if (d.Class == Enums.DashboardItemClass.MAP)
                    {
                        var map = (OpMapControl)d;
                        result.Add(new {
                            id = map.IdType,
                            markers = map.Markers,
                            polygons = map.Polygons.Select(p =>
                            {
                                return new
                                {
                                    coords = p.Coords,
                                    fillColor = p.FillColor.HasValue ? Common.ColorToHEX(p.FillColor.Value) : "#000000",
                                    lineColor = p.LineColor.HasValue ? Common.ColorToHEX(p.LineColor.Value) : "#000000",
                                    tooltip = p.Tooltip
                                };
                            }).ToList(),
                            ranges = map.Ranges.Select(r =>
                            {
                                return new
                                {
                                    coords = r.Coords,
                                    fillColor = r.FillColor.HasValue ? Common.ColorToHEX(r.FillColor.Value) : "#000000",
                                    lineColor = r.LineColor.HasValue ? Common.ColorToHEX(r.LineColor.Value) : "#000000",
                                    tooltip = r.Tooltip,
                                    radius = r.Radius
                                };
                            }).ToList()
                        });
                    }
                    else if (d.Class == Enums.DashboardItemClass.SCHEMA)
                    {
                        var schema = (OpSchemaControl)d;

                        result.Add(new { id = schema.IdType, schemas = schema.Schemas.Select(s => SchemaModelFromOpSchema<OpLocation>(s, null)) });
                    }
                    else continue;
                }
                return new ResponseListModel<object>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning collector dashboard items downloading.", e);
            }
        }

        private long GetSizeFromPercent(int percent)
        {
            return (long)Math.Ceiling(0.12 * percent);
        }

        private string ColorToRgba(System.Drawing.Color? c)
        {
            var color = c.HasValue ? c.Value : System.Drawing.Color.Gray;
            return color.R.ToString() + "," + color.G.ToString() + "," + color.B.ToString() + "," + color.A.ToString();
        }
    }
}
