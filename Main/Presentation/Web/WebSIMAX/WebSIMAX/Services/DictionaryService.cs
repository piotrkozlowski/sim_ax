﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom;
using Microsoft.AspNetCore.Http;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;

namespace SIMAX.Services
{
    public interface IDictionaryService
    {
        int GetVersion();
        DictionaryModel GetDictionaries();
    }

    public class DictionaryService : BaseService, IDictionaryService
    {
        private class Dictionaries
        {
            public List<OpDataType> DataTypes;
            public List<OpDataTypeGroup> DataTypeGroups;
            public List<OpDataTypeInGroup> DataTypeInGroups;

            public Dictionaries(object[] dataTypes, object[] dataTypeGroups, object[] dataTypeInGroups)
            {
                DataTypes = dataTypes.Select(d => (OpDataType)d).ToList();
                DataTypeGroups = dataTypeGroups.Select(d => (OpDataTypeGroup)d).ToList();
                DataTypeInGroups = dataTypeInGroups.Select(d => (OpDataTypeInGroup)d).ToList();
            }
        }

        public DictionaryService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
        }

        public int GetVersion()
        {
            var session = _sessionService.GetSession();
            SetStartTime();
            var version = _dbClient.GetDictVersion(ref session);
            SetEndTime(Logging.EventID.DICTIONARY.GetDictVersionLog, "GetDictVersion");

            base.CheckSession(session);

            if (session != null && session.Error != null)
            {
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Issue concering downloading dictionaries version");
            }

            return version;
        }

        public DictionaryModel GetDictionaries()
        {
            var tables = GetTables();
            object[] dataTypes;
            object[] dataTypeGroups;
            object[] dataTypeInGroups;

            try
            {
                dataTypes = (object[])tables[0];
                dataTypeGroups = (object[])tables[1];
                dataTypeInGroups = (object[])tables[2];
            }
            catch (Exception e)
            {
                //throw new Exception("Issue concering dictionary downloading.", e);
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Issue concering dictionary downloading. " + e.Message);
            }

            var dictionaries = new Dictionaries(dataTypes, dataTypeGroups, dataTypeInGroups);

            var dictionaryModel = new DictionaryModel();

            try
            {
                var references = new List<Enums.ReferenceType>();

                var dataTypesDictionary = new ExpandoObject() as IDictionary<string, object>;
                dictionaries.DataTypes.ForEach(d =>
                {
                    if (d.ReferenceType != null)
                        references.Add((Enums.ReferenceType)d.ReferenceType.IdReferenceType);
                    dataTypesDictionary.Add(d.IdDataType.ToString(), DataTypeModel.DataTypeModelFromOpDataType(d));
                });

                dictionaryModel.DataTypes = dataTypesDictionary;
                dictionaryModel.DataTypeGroups = dictionaries.DataTypeGroups.Select(d => DataTypeGroupModel.DataTypeGroupModelFromOpDataTypeGroup(d)).ToList();
                
                var dataTypeInGroupsDictionary = new ExpandoObject() as IDictionary<string, object>;
                dictionaries.DataTypeInGroups.ForEach(d =>
                {
                    var key = d.IdDataTypeGroup.ToString();
                    if (dataTypeInGroupsDictionary.ContainsKey(key))
                    {
                        var list = dataTypeInGroupsDictionary[key] as List<long>;
                        if (!list.Contains(d.IdDataType)) list.Add(d.IdDataType);
                    } else
                    {
                        var list = new List<long>();
                        list.Add(d.IdDataType);
                        dataTypeInGroupsDictionary.Add(key, list);
                    }
                });
                dictionaryModel.DataTypeInGroups = dataTypeInGroupsDictionary;

                references = references.Distinct().ToList();

                dictionaryModel.References = GetReferenceTables(references);

                dictionaryModel.Modules = GetTypes((object[])tables[3]);
                dictionaryModel.Distributors = GetTypes((object[])tables[4]);
                dictionaryModel.Units = GetTypes((object[])tables[5]);
            }
            catch (Exception e)
            {
                //throw new Exception("Issue concering dictionary downloading in others.", e);
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Issue concering dictionary downloading in others. " + e.Message);
            }

            try
            {
                var views = GetActiveViews();

                if (views.Length == 0)
                {
                    IMRLog.AddToLog(Logging.EventID.ERRORS.ViewsProblem, new object[] { "OpxGetActiveView" });
                }

                var viewsDictionary = new ExpandoObject() as IDictionary<string, object>;
                foreach(var v in views)
                {
                    var columns = v.Columns.Select(c =>
                    {
                        var column = new ViewColumnModel();
                        column.Id = c.IdDataType;
                        column.Name = c.Name;
                        return column;
                    }).ToList();

                    if (!viewsDictionary.ContainsKey(v.IdReferenceType.ToString()))
                        viewsDictionary.Add(v.IdReferenceType.ToString(), columns);
                    else
                        IMRLog.AddToLog(Logging.EventID.ERRORS.ViewsProblem2, new object[] { "OpxGetActiveView", v.IdReferenceType });
                }
                dictionaryModel.Views = viewsDictionary;
            }
            catch (Exception e)
            {
                //throw new Exception("Issue concering doctionary downloading in views.", e);
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Issue concering dictionary downloading in views. " + e.Message);
            }

            return dictionaryModel;
        }

        private object[] GetTables(Enums.ReferenceType[] additionalTables = null)
        {
            var tables = additionalTables != null ? additionalTables.ToList() : new List<Enums.ReferenceType>()
            {
                Enums.ReferenceType.IdDataType,
                Enums.ReferenceType.IdDataTypeGroup,
                Enums.ReferenceType.IdDataTypeInGroup,
                Enums.ReferenceType.IdModule,
                Enums.ReferenceType.IdDistributor,
                Enums.ReferenceType.IdUnit
            };

            var session = _sessionService.GetSession();

            var enter = DateTime.Now;
            SetStartTime();
            var result = _dbClient.GetDictionaries(ref session, tables.ToArray());
            SetEndTime(Logging.EventID.DICTIONARY.GetDictionariesLog, "GetDictionaries");
            var timespan = DateTime.Now - enter;
            var log = new LogData(2003003, LogLevel.Debug, "[{0}]-Exit function {1}. Duration: {2}[s].");
            IMRLog.AddToLog(log, new object[] { "GUID", "GetDictionaries", timespan.ToString("ss'.'ff") });

            base.CheckSession(session);

            if(session != null && session.Error != null)
            {
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized, "Error in downloading dictionaries");
            }

            return result;
        }

        private List<ColumnModel> GetTypes(object[] types)
        {
            var result = new List<ColumnModel>();
            foreach (var type in types)
            {
                try
                {
                    var referenceType = (IReferenceType)type;
                    result.Add(new ColumnModel()
                    {
                        Id = (int)referenceType.GetReferenceKey(),
                        Caption = referenceType.ToString()
                    });
                } catch (Exception e)
                {
                    IMRLog.AddToLog(Logging.EventID.ERRORS.ViewsProblem, new object[] { "GetTypes", type.GetType().Name });
                    // TODO: Ask Lukasz Piela about this exception
                }
            }

            return result;
        }

        private Dictionary<int, List<ColumnModel>> GetReferenceTables(List<Enums.ReferenceType> references = null)
        {
            var result = new Dictionary<int, List<ColumnModel>>();

            var dictionaries = GetTables(references.ToArray());
            for (var i = 0; i < dictionaries.Length; i++)
            {
                var dictionary = dictionaries[i];
                var id = references[i];
                try
                {
                    result.Add((int)id, GetTypes((object[])dictionary));
                }
                catch (Exception e)
                {
                    Debug.Write(e);
                }
            }

            return result;
        }

        private ColumnModel PrepareColumnModel(int id, string name, string typeName)
        {
            int typeId;
            switch (typeName)
            {
                case "Integer":
                    typeId = 1;
                    break;
                case "Real":
                    typeId = 2;
                    break;
                case "Date":
                    typeId = 6;
                    break;
                case "Boolean":
                    typeId = 7;
                    break;
                case "Text":
                default:
                    typeId = 3;
                    break;
            }

            return new ColumnModel()
            {
                Id = id,
                Caption = name,
                Type = typeId,
                TypeName = typeName
            };
        }
    }
}
