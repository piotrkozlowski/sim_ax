﻿using IMR.Suite.Common;
using IMR.Suite.Services.WebSimaxInterface.WebSimaxServiceReference;
using IMR.Suite.UI.Business.Objects.Custom;
using SIMAX.Models;
using SIMAX.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace SIMAX.Services
{
    public interface IAlarmsService
    {
        GridModel GetAlarms(GridRequestModel req);
        ModelAlarm GetAlarm(long id);
        ResponseListModel<object> GetAlarmsSummary(int idOperator);
        GridModel ConfirmAlarms(long[] ids);
    }

    public class AlarmsService : BaseService, IAlarmsService
    {

        public AlarmsService(IHttpContextAccessor httpContextAccessor, IDBCollector dbClient, ISessionService sessionService) : base(httpContextAccessor, dbClient, sessionService)
        {
            _dbClient = dbClient;
            _sessionService = sessionService;
        }

        public GridModel GetAlarms(GridRequestModel req)
        {
            try
            {
                var session = _sessionService.GetSession();
                long totalCount = 0;
                var columns = ListColumnModelToColumnInfoArray(req.Columns);

                FilterByIds(req, ref columns, Enums.ReferenceType.IdAlarmEvent);

                SetStartTime();
                var values = _dbClient.OpxAlarmEventFilter(ref session, out totalCount, columns, req.PageNumber, req.PageSize, req.Where);
                SetEndTime(Logging.EventID.ALARMS.FilterAlarmLog, "OpxAlarmEventFilter");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = values.Select(alarm =>
                    {
                        return Common.DictionaryToObject(alarm.DynamicValues, alarm.ToString(), alarm.IdAlarmEvent);
                    }).ToList(),
                    TotalCount = totalCount,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning alarms downloading.", e);
            }
        }

        public ResponseListModel<object> GetAlarmsSummary(int idOperator)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var values = _dbClient.OpxGetAlarmEventSummary(ref session, idOperator);
                SetEndTime(Logging.EventID.ALARMS.GetAlarmSummaryLog, "OpxGetAlarmEventSummary");
                base.CheckSession(session);

                var result = new List<object>();

                foreach (var v in values)
                {
                    var tile = (OpTileControl)v;
                    result.Add(new { text = tile.TextHeader, value = tile.Amount, reftype = tile.ReferenceType , refdata = tile.ReferenceData.Select(d => d.ToString())});
                }

                return new ResponseListModel<object>(result, session);
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning alarms counting.", e);
            }
        }

        public ModelAlarm GetAlarm(long id)
        {
            try
            {
                var session = _sessionService.GetSession();
                SetStartTime();
                var opAlarm = _dbClient.OpxGetAlarmEvent(ref session, (int)id);
                SetEndTime(Logging.EventID.ALARMS.GetAlarmLog, "OpxGetAlarmEvent");
                base.CheckSession(session);

                if (opAlarm != null)
                {
                    return AlarmModelFromOpAlarm(opAlarm, false, session);
                }
                else
                {
                    if (session != null && session.Error != null)
                    {
                        return AlarmModelFromOpAlarm(opAlarm, false, session);
                    }
                    else
                    {
                        throw new StatusCodeException(-1, String.Format("Issue concerning downloading alarm {0}.", id));
                    }
                }
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Issue concerning downloading alarm {0}.", id), e);
            }
        }

        public GridModel ConfirmAlarms(long[] ids)
        {
            try
            {
                var session = _sessionService.GetSession();

                SetStartTime();
                var alarms = _dbClient.OpxConfirmAlarmEvent(ref session, ids);
                SetEndTime(Logging.EventID.ALARMS.ConfirmAlarmLog, "OpxConfirmAlarmEvent");
                base.CheckSession(session);

                var result = new GridModel()
                {
                    Values = alarms.Select(alarm =>
                    {
                        return Common.DictionaryToObject(alarm.DynamicValues, alarm.ToString(), alarm.IdAlarmEvent);
                    }).ToList(),
                    TotalCount = alarms.Length,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
                return result;
            }
            catch (StatusCodeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("Issue concerning saving alarm events.", e);
            }
        }

        private ModelAlarm AlarmModelFromOpAlarm(IMR.Suite.UI.Business.Objects.DW.OpAlarmEvent opAlarm, bool replaceReferenceTypes = false, SESSION session = null)
        {
            ModelAlarm result;

            if (opAlarm != null)
            {
                var fields = opAlarm.DynamicValues.Select(d => FieldModel.FieldFromDynamicValues(d)).ToList();

                result = new ModelAlarm()
                {
                    Id = opAlarm.IdAlarmEvent,
                    Name = opAlarm.ToString(),
                    Fields = replaceReferenceTypes ? ReplaceReferenceFields(fields, opAlarm.DynamicValues) : fields,
                    Latitude = opAlarm.Location != null ? opAlarm.Location.Latitude : 0,
                    Longitude = opAlarm.Location != null ? opAlarm.Location.Longitude : 0,
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };

                ReplaceReferenceIds(result.Fields, opAlarm.DataList);
            }
            else
            {
                result = new ModelAlarm()
                {
                    Fields = new List<FieldModel>(),
                    HasErrorCode = (session != null && session.Error != null && session.Error.Code != null) ? (int)session.Error.Code : 0
                };
            }

            return result;
        }

        private List<FieldModel> AddAlarmSpecialFieldsToFieldModel(IMR.Suite.UI.Business.Objects.DW.OpAlarm alarm)
        {
            var list = new List<FieldModel>();
            list.Add(PrepareFieldModel(DataType.HELPER_ID_ALARM_TYPE, alarm.IdAlarmType, "Integer", false));

            return list;
        }

    }
}